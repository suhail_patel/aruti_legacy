﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region

Partial Class TnA_Shift_Policy_Global_Assign_wPgViewAssignShiftPolicy
    Inherits Basepage

#Region " Private Variable "
    Dim DisplayMessage As New CommonCodes
    Dim objEmp_Shift_Tran As clsEmployee_Shift_Tran
    Dim objEPolicy As clsemployee_policy_tran


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmViewAssigned_Shift"
    Private ReadOnly mstrModuleName1 As String = "frmViewAssigned_Policy"
    'Pinkal (06-May-2014) -- End

    Private mstrAdvanceFilter As String = ""

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEmp_Shift_Tran = New clsEmployee_Shift_Tran
            objEPolicy = New clsemployee_policy_tran


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                FillCombo()
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                Call SetVisibility()
                'Varsha Rana (17-Oct-2017) -- End
                RdShiftPolicy_SelectedIndexChanged(New Object(), New EventArgs())
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("AssignShift_EmpUnkId") IsNot Nothing Then
                    cboEmployee.SelectedValue = Session("AssignShift_EmpUnkId").ToString()
                    Session.Remove("AssignShift_EmpUnkId")
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Call objbtnSearch_Click(btnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END 
            End If

            'Pinkal (06-Oct-2015) -- Start
            'Enhancement - WORKING ON HYATT TNA CHANGES.

            If Me.ViewState("AdvanceFilter") IsNot Nothing Then
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString()
            End If

            'Pinkal (06-Oct-2015) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Pinkal (06-Oct-2015) -- Start
    'Enhancement - WORKING ON HYATT TNA CHANGES.

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("AdvanceFilter") Is Nothing Then
                Me.ViewState.Add("AdvanceFilter", mstrAdvanceFilter)
            Else
                Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender" & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (06-Oct-2015) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objSMaster As New clsshift_master
        Dim objPMaster As New clspolicy_master
        Dim dsList As New DataSet

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , , , , False)
            'Else
            '    dsList = objEMaster.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , , , , False)
            'End If

            dsList = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                 False, "Emp", True)

            'Shani(24-Aug-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboShiftType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objSMaster.getListForCombo("List", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objPMaster.getListForCombo("List", True)
            With cboPolicy
                .DataValueField = "policyunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :-" & ex.Message, Me)
        Finally
            dsList.Dispose()
            dsList = Nothing
            objEMaster = Nothing
            objCMaster = Nothing
            objSMaster = Nothing
        End Try

    End Sub

    Private Sub FillShiftGrid(ByVal blnIsstart As Boolean)
        Dim iTable As DataTable = Nothing
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If CBool(Session("AllowToViewAssignedShiftList")) = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End
            If blnIsstart = False Then

                Dim mdtDate1 As Date = Nothing
                Dim mdtDate2 As Date = Nothing

                If dtpDate1.IsNull = False Then
                    mdtDate1 = dtpDate1.GetDate.Date
                End If

                If dtpDate2.IsNull = False Then
                    mdtDate2 = dtpDate2.GetDate.Date
                End If

                'Pinkal (06-Oct-2015) -- Start
                'Enhancement - WORKING ON HYATT TNA CHANGES.
                'iTable = objEmp_Shift_Tran.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboShiftType.SelectedValue), CInt(cboShift.SelectedValue), "")

                'Pinkal (14-Oct-2015) -- Start
                'Enhancement - WORKING ON Daily Attendance Checking Report For Hyatt.
                'iTable = objEmp_Shift_Tran.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboShiftType.SelectedValue), CInt(cboShift.SelectedValue), mstrAdvanceFilter)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'iTable = objEmp_Shift_Tran.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboShiftType.SelectedValue), CInt(cboShift.SelectedValue), mstrAdvanceFilter, Session("AccessLevelFilterString"))
                iTable = objEmp_Shift_Tran.Get_List(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    Session("UserAccessModeSetting").ToString(), "List", _
                                                    mdtDate1.Date, _
                                                    mdtDate2.Date, _
                                                    CInt(cboEmployee.SelectedValue), _
                                                    CInt(cboShiftType.SelectedValue), _
                                                    CInt(cboShift.SelectedValue), mstrAdvanceFilter)
                'Shani(20-Nov-2015) -- End

                'Pinkal (14-Oct-2015) -- End



                'Pinkal (06-Oct-2015) -- End


            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("FillShiftGrid :- " & ex.Message, Me)
        Finally

            If iTable Is Nothing OrElse iTable.Rows.Count <= 0 Then
                iTable = New DataTable("List")
                iTable.Columns.Add("ecode", Type.GetType("System.String"))
                iTable.Columns.Add("ename", Type.GetType("System.String"))
                iTable.Columns.Add("effectivedate", Type.GetType("System.DateTime"))
                iTable.Columns.Add("shiftname", Type.GetType("System.String"))
                iTable.Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                iTable.Columns.Add("shifttranunkid", Type.GetType("System.Int32"))

                Dim drRow As DataRow = iTable.NewRow
                drRow("ecode") = "None"
                drRow("ename") = "None"
                drRow("effectivedate") = DBNull.Value
                drRow("shiftname") = "None"
                drRow("employeeunkid") = -1
                drRow("shifttranunkid") = -1
                iTable.Rows.Add(drRow)
            End If

            If iTable.Columns.Contains("IsCheck") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "IsCheck"
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                iTable.Columns.Add(dc)
            End If

            If iTable.Columns.Contains("emppolicytranunkid") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "emppolicytranunkid"
                dc.DataType = Type.GetType("System.Int32")
                dc.DefaultValue = -1
                iTable.Columns.Add(dc)
            End If

            If iTable.Columns.Contains("policyname") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "policyname"
                dc.DataType = Type.GetType("System.String")
                dc.DefaultValue = String.Empty
                iTable.Columns.Add(dc)
            End If

            dgvData.DataSource = iTable
            dgvData.DataBind()
            Me.ViewState.Add("ShiftList", iTable)
            iTable = Nothing
        End Try
    End Sub

    Private Sub FillPolicyGrid(ByVal blnIsstart As Boolean)
        Dim iTable As DataTable = Nothing
        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If CBool(Session("AllowToViewAssignedPolicyList")) = False Then Exit Sub
            'Varsha Rana (17-Oct-2017) -- End
            If blnIsstart = False Then

                Dim mdtDate1 As Date = Nothing
                Dim mdtDate2 As Date = Nothing

                If dtpDate1.IsNull = False Then
                    mdtDate1 = dtpDate1.GetDate.Date
                End If

                If dtpDate2.IsNull = False Then
                    mdtDate2 = dtpDate2.GetDate.Date
                End If


                'Pinkal (06-Oct-2015) -- Start
                'Enhancement - WORKING ON HYATT TNA CHANGES.

                'iTable = objEPolicy.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboPolicy.SelectedValue), "")

                'Pinkal (14-Oct-2015) -- Start
                'Enhancement - WORKING ON Daily Attendance Checking Report For Hyatt.
                'iTable = objEPolicy.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboPolicy.SelectedValue), mstrAdvanceFilter)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'iTable = objEPolicy.Get_List("List", mdtDate1.Date, mdtDate2.Date, CInt(cboEmployee.SelectedValue), CInt(cboPolicy.SelectedValue), mstrAdvanceFilter, Session("AccessLevelFilterString"))
                iTable = objEPolicy.Get_List(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             Session("UserAccessModeSetting").ToString(), _
                                              False, "List", mdtDate1.Date, _
                                             mdtDate2.Date, _
                                             CInt(cboEmployee.SelectedValue), _
                                             CInt(cboPolicy.SelectedValue), mstrAdvanceFilter)
                'Shani(20-Nov-2015) -- End

                'Pinkal (14-Oct-2015) -- End



                'Pinkal (06-Oct-2015) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("FillPolicyGrid :- " & ex.Message, Me)
        Finally
            If iTable Is Nothing OrElse iTable.Rows.Count <= 0 Then
                iTable = New DataTable("List")
                iTable.Columns.Add("ecode", Type.GetType("System.String"))
                iTable.Columns.Add("ename", Type.GetType("System.String"))
                iTable.Columns.Add("effectivedate", Type.GetType("System.DateTime"))
                iTable.Columns.Add("policyname", Type.GetType("System.String"))
                iTable.Columns.Add("employeeunkid", Type.GetType("System.Int32"))
                iTable.Columns.Add("emppolicytranunkid", Type.GetType("System.Int32"))

                Dim drRow As DataRow = iTable.NewRow
                drRow("ecode") = "None"
                drRow("ename") = "None"
                drRow("effectivedate") = DBNull.Value
                drRow("policyname") = "None"
                drRow("employeeunkid") = -1
                drRow("emppolicytranunkid") = -1
                iTable.Rows.Add(drRow)
            End If

            If iTable.Columns.Contains("IsCheck") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "IsCheck"
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                iTable.Columns.Add(dc)
            End If


            If iTable.Columns.Contains("shifttranunkid") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "shifttranunkid"
                dc.DataType = Type.GetType("System.Int32")
                dc.DefaultValue = -1
                iTable.Columns.Add(dc)
            End If

            If iTable.Columns.Contains("shiftname") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "shiftname"
                dc.DataType = Type.GetType("System.String")
                dc.DefaultValue = String.Empty
                iTable.Columns.Add(dc)
            End If


            dgvData.DataSource = iTable
            dgvData.DataBind()
            Me.ViewState.Add("PolicyList", iTable)
            iTable = Nothing
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = CBool(Session("AllowToAddShiftPolicyAssignment"))
            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                btnDelete.Enabled = CBool(Session("AllowToDeleteAssignedPolicy"))
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                btnDelete.Enabled = CBool(Session("AllowToDeleteAssignedShift"))
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            DisplayMessage.DisplayError("SetVisibility :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End
#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataTable = Nothing
            If CInt(RdShiftPolicy.SelectedValue) = 1 Then   'SHIFT
                dtTab = CType(Me.ViewState("ShiftList"), DataTable)
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then   'POLICY
                dtTab = CType(Me.ViewState("PolicyList"), DataTable)
            End If

            For i As Integer = 0 To dtTab.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvData.Rows(i)
                CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                dtTab.Rows(i)("IsCheck") = cb.Checked
            Next
            dtTab.AcceptChanges()

            If CInt(RdShiftPolicy.SelectedValue) = 1 Then   'SHIFT
                Me.ViewState("ShiftList") = dtTab
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then   'POLICY
                Me.ViewState("PolicyList") = dtTab
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try


            Dim dtTab As DataTable = Nothing
            If CInt(RdShiftPolicy.SelectedValue) = 1 Then   'SHIFT
                dtTab = CType(Me.ViewState("ShiftList"), DataTable)
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then   'POLICY
                dtTab = CType(Me.ViewState("PolicyList"), DataTable)
            End If

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
            dtTab.Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
            dtTab.AcceptChanges()


            If CInt(RdShiftPolicy.SelectedValue) = 1 Then   'SHIFT
                Me.ViewState("ShiftList") = dtTab
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then   'POLICY
                Me.ViewState("PolicyList") = dtTab
            End If

            Dim drcheckedRow() As DataRow = dtTab.Select("Ischeck = True")

            If drcheckedRow.Length = dtTab.Rows.Count Then
                CType(dgvData.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvData.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelect_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Radio Event"

    Protected Sub RdShiftPolicy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdShiftPolicy.SelectedIndexChanged
        Try

            cboEmployee.SelectedIndex = 0
            dtpDate1.SetDate = Nothing
            dtpDate2.SetDate = Nothing

            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                cboPolicy.SelectedIndex = 0
                cboPolicy.Enabled = False
                cboShift.Enabled = True
                cboShiftType.Enabled = True
                FillShiftGrid(True)
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                cboPolicy.Enabled = True
                cboShift.SelectedIndex = 0
                cboShift.Enabled = False
                cboShiftType.SelectedIndex = 0
                cboShiftType.Enabled = False
                FillPolicyGrid(True)
            End If

            dgvData_RowDataBound(New Object(), New WebControls.GridViewRowEventArgs(dgvData.Rows(0)))

        Catch ex As Exception
            DisplayMessage.DisplayError("RdShiftPolicy_SelectedIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Session("AssignShift_EmpUnkId") = cboEmployee.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END 

            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                Response.Redirect("~/TnA/Shift_Policy_Global_Assign/wPg_ShiftGlobalAssign.aspx")
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                Response.Redirect("~/TnA/Shift_Policy_Global_Assign/wPg_PolicyGlobalAssign.aspx")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnNew_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim iTable As DataTable = Nothing
        Try

            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                iTable = CType(Me.ViewState("ShiftList"), DataTable)
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                iTable = CType(Me.ViewState("PolicyList"), DataTable)
            End If

            Dim dtmp() As DataRow = iTable.Select("ischeck = True")
            If dtmp.Length <= 0 Then
                DisplayMessage.DisplayMessage("Please check atleast one employee in order to do operation on it.", Me)
                Exit Sub
            End If

            If dtmp.Length > 0 Then
                DeleteConfirmation.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("btnDelete_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub DeleteConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteConfirmation.buttonYes_Click
        Try
            DeleteReason.Reason = String.Empty
            DeleteReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("DeleteConfirmation_buttonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteReason.buttonDelReasonYes_Click
        Dim iTable As DataTable = Nothing
        Try

            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                iTable = CType(Me.ViewState("ShiftList"), DataTable)
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                iTable = CType(Me.ViewState("PolicyList"), DataTable)
            End If

            Dim dtmp() As DataRow = iTable.Select("ischeck = True")
            If dtmp.Length > 0 Then

                If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                    objEPolicy._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    objEPolicy._Voidreason = DeleteReason.Reason
                    objEPolicy._Voiduserunkid = CInt(Session("UserId"))
                    objEPolicy._Isvoid = True
                End If

                Dim blnFlag, blnShown As Boolean
                Dim iMessage As String = String.Empty
                For iRow As Integer = 0 To dtmp.Length - 1
                    If CInt(RdShiftPolicy.SelectedValue) = 1 Then

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEmp_Shift_Tran.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage, CInt(dtmp(iRow).Item("shiftunkid")))
                        objEmp_Shift_Tran.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage, CInt(dtmp(iRow).Item("shiftunkid")), Session("Database_Name").ToString())
                        'Shani(20-Nov-2015) -- End

                    ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEPolicy.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage)
                        objEPolicy.isUsed(CInt(dtmp(iRow).Item("employeeunkid")), CDate(dtmp(iRow).Item("effectivedate")), iMessage, Session("Database_Name").ToString())
                        'Shani(20-Nov-2015) -- End

                    End If

                    If iMessage <> "" Then
                        If blnShown = False Then
                            DisplayMessage.DisplayMessage(iMessage, Me)
                            blnShown = True
                            Continue For
                        Else
                            blnShown = True
                            Continue For
                        End If
                    End If

                    If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                        With objEmp_Shift_Tran
                            ._FormName = mstrModuleName
                            If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                            Else
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                            End If
                            ._ClientIP = Session("IP_ADD").ToString()
                            ._HostName = Session("HOST_NAME").ToString()
                            ._FromWeb = True
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        End With
                        blnFlag = objEmp_Shift_Tran.Void_Assigned_Shift(CInt(dtmp(iRow).Item("shifttranunkid")), CInt(dtmp(iRow).Item("employeeunkid")), DeleteReason.Reason, CInt(Session("UserId")), CDate(dtmp(iRow).Item("effectivedate")), CInt(dtmp(iRow).Item("shiftunkid")))
                    ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                        objEPolicy._Effectivedate = CDate(dtmp(iRow).Item("effectivedate"))
                        objEPolicy._Policyunkid = CInt(dtmp(iRow).Item("policyunkid"))
                        objEPolicy._Employeeunkid = CInt(dtmp(iRow).Item("employeeunkid"))
                        With objEPolicy
                            ._FormName = mstrModuleName
                            If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                            Else
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                            End If
                            ._ClientIP = Session("IP_ADD").ToString()
                            ._HostName = Session("HOST_NAME").ToString()
                            ._FromWeb = True
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        End With
                        blnFlag = objEPolicy.Delete(CInt(dtmp(iRow).Item("emppolicytranunkid")))
                        If blnFlag = False Then
                            DisplayMessage.DisplayMessage(objEPolicy._Message, Me)
                            Exit For
                        End If
                    End If

                Next

                If blnFlag = True Then
                    If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                        DisplayMessage.DisplayMessage("Shift removed successfully.", Me)
                        FillShiftGrid(True)
                    ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                        DisplayMessage.DisplayMessage("Policy removed successfully.", Me)
                        FillPolicyGrid(True)
                    End If
                End If

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError("DeleteReason_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                FillShiftGrid(False)
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                FillPolicyGrid(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch.Click :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            RdShiftPolicy.SelectedValue = "1"
            RdShiftPolicy_SelectedIndexChanged(New Object, New EventArgs())
            cboEmployee.SelectedValue = "0"
            cboShift.SelectedValue = "0"
            cboShiftType.SelectedValue = "0"
            dtpDate1.SetDate = Nothing
            dtpDate2.SetDate = Nothing
            dgvData.DataSource = Nothing
            'Pinkal (06-Oct-2015) -- Start
            'Enhancement - WORKING ON HYATT TNA CHANGES.
            mstrAdvanceFilter = ""
            'Pinkal (06-Oct-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (06-Oct-2015) -- Start
    'Enhancement - WORKING ON HYATT TNA CHANGES.

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (06-Oct-2015) -- End

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1.CloseButton_click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "Gridview Event"

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try


            If CInt(RdShiftPolicy.SelectedValue) = 1 Then
                dgvData.Columns(4).Visible = True   'SHIFT
                dgvData.Columns(5).Visible = False  'POLICY
            ElseIf CInt(RdShiftPolicy.SelectedValue) = 2 Then
                dgvData.Columns(4).Visible = False   'SHIFT
                dgvData.Columns(5).Visible = True   'POLICY 
            End If

            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.Cells(1).Text = "None" AndAlso e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                e.Row.Visible = False
                CType(dgvData.HeaderRow.FindControl("chkSelectAll"), CheckBox).Enabled = False
            Else
                CType(dgvData.HeaderRow.FindControl("chkSelectAll"), CheckBox).Enabled = True
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError("dgvData_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub

#End Region
 
    'Pinkal (06-Oct-2015) -- Start
    'Enhancement - WORKING ON HYATT TNA CHANGES.

#Region "LinkButton Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAllocation_Click :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

    'Pinkal (06-Oct-2015) -- End


 
    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
        Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
        Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.ID, Me.LblFromDate.Text)
        Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.ID, Me.LblShift.Text)
        Me.LblShiftType.Text = Language._Object.getCaption(Me.LblShiftType.ID, Me.LblShiftType.Text)
        Me.RdShiftPolicy.Items(0).Text = Language._Object.getCaption(Me.LblShift.ID, Me.RdShiftPolicy.Items(0).Text)
        Me.dgvData.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(1).FooterText, Me.dgvData.Columns(1).HeaderText)
        Me.dgvData.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
        Me.dgvData.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
        Me.dgvData.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
        Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")

        Language.setLanguage(mstrModuleName1)
        Me.lblPolicy.Text = Language._Object.getCaption("lblPolicy", Me.lblPolicy.Text)
        Me.RdShiftPolicy.Items(1).Text = Language._Object.getCaption("lblPolicy", Me.RdShiftPolicy.Items(1).Text)
        Me.dgvData.Columns(5).HeaderText = Language._Object.getCaption("lblPolicy", Me.dgvData.Columns(5).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub
 
End Class
