﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_PolicyGlobalAssign.aspx.vb" Inherits="TnA_Shift_Policy_Global_Assign_wPg_PolicyGlobalAssign"
    Title="Global Policy Assign" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Shift/Policy Assignment"></asp:Label>
                        </div>
                        <div class="panel-body">
                        
                      <%--  Pinkal (06-Nov-2017) -- Start
                        Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.--%>

                           <div id="FilterPolicyCriteria" class="panel-default" style="position: relative">
                                 <div id="FilterPolicyCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left">
                                            <asp:Label ID="lblPolicyFilterHeader" runat="server" Text="Policy Filter"></asp:Label>
                                        </div>
                                    </div>  
                                     <div id="FilterPolicyCriteriaBody" class="panel-body-default">
                                          <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="LblFilterPolicy" runat="server" Text="Policy"></asp:Label>
                                                    </td>
                                                    <td style="width: 85%">
                                                        <asp:DropDownList ID="cboFilterPolicy" runat="server" style="margin-top:5px" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    </td>
                                                </tr>
                                          </table>
                                     </div>
                            </div>
                            
                          <%--  Pinkal (06-Nov-2017) -- End--%>
                                
                            <div id="FilterCriteria" class="panel-default" style="position: relative">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Policy Assignment"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" />
                                            </td>
                                            <td style="width: 50%">
                                                <asp:CheckBox ID="chkFirstPolicy" runat="server" Text="Assign First Policy" AutoPostBack="true">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblPolicy" runat="server" Text="Policy" style="margin-top:5px"></asp:Label>
                                            </td>
                                            <td style="width: 85%" colspan="2">
                                                <asp:DropDownList ID="cboPolicy" runat="server" style="margin-top:5px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="3">
                                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" style="margin-top:5px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="3">
                                                <asp:Panel ID="pnl_dgvEmp" ScrollBars="Auto" Height="400px" runat="server">
                                                    <asp:UpdatePanel ID="UpdateEmployeeList" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="dgvEmp" runat="server" style="margin:auto" AutoGenerateColumns="false" CssClass="gridview"
                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkEmpSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpSelectAll_CheckedChanged" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkEmpSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpSelect_CheckedChanged" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhEcode" />
                                                                    <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="True" FooterText="dgcolhEName" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btnDefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

