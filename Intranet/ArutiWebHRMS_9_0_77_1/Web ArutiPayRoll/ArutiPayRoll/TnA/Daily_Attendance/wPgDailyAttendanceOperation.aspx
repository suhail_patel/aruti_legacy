﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPgDailyAttendanceOperation.aspx.vb" Inherits="TnA_GlobalTimesheetAddEditDelete_wPgDailyAttendanceOperation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>
    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                             <asp:Label ID="lblPageHeader" runat="server" Text="Daily Attendance"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblShift" runat="server" Text="Shift"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboShift" runat="server" AutoPostBack="False" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCheckIn" runat="server" CssClass="btnDefault" Text="Log In" />
                                        <asp:Button ID="btnCheckOut" runat="server" CssClass="btnDefault" Text="Log Out" />
                                    </div>
                                  
                                   
                                </div>
                            </div>
                            
                            
                            <div id="Div1" class="panel-default">
                             
                                <div id="Div3" class="panel-body-default">
                                                <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server" Style="margin-top: 5px;
                                                    max-height: 300px">
                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:BoundField DataField="logindate" HeaderText="Login Date" FooterText="dgcolhLoginDate" />
                                                            <asp:BoundField DataField="checkintime" HeaderText="LogIn Time" FooterText="dgcolhLogInTime" DataFormatString = "{0:HH:mm}" />
                                                            <asp:BoundField DataField="checkouttime" HeaderText="LogOut Time" FooterText="dgcolhLogOutTime" DataFormatString = "{0:HH:mm}" />
                                                            <asp:BoundField DataField="workedDuration" HeaderText="Worked Hour" FooterText="dgcolhworkhour" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                   
                                     <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 75%; text-align:Right">
                                                <asp:Label ID="lblTotalWorkedHour" runat="server" Text="Total Worked Hour :" Font-Bold = "true"  ></asp:Label>
                                            </td>
                                            <td style="width: 25% ; text-align:Left" colspan="3">
                                                <asp:Label ID="txtTotalWorkedHour" runat="server" Font-Bold = "True"></asp:Label>
                                            </td>
                                        </tr>
                                     </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

