﻿<%@ Page Title="Global Submit For Approval" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="OTSubmitForApproval.aspx.vb" Inherits="TnA_OT_Requisition_OTSubmitForApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        // Pinkal (10-Jan-2020) -- Start
        //  Enhancements -  Working on OT Requisistion for NMB.

        $("[id*=chkSelectAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkSelect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        // Pinkal (10-Jan-2020) -- End
        
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Submit For Approval"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 80%">
                                            <%-- <td style="width: 7%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 20%" colspan = "2">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>--%>
                                            <td style="width: 5%">
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 7%">
                                                <uc1:DateCtrl ID="dtpFromDate" runat="server" />
                                            </td>
                                            <td style="width: 3%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="400px" />
                                            </td>
                                        </tr>
                                        <tr style="width: 80%">
                                            <td style="width: 5%">
                                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 7%">
                                                <uc1:DateCtrl ID="dtpToDate" runat="server" />
                                            </td>
                                            <td style="width: 30%" colspan="3">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                                <div id="Div1" class="panel-default">
                                    <div id="Div3" class="panel-body-default">
                                        <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                            max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                            <asp:GridView ID="gvOTRequisitionList" runat="server" DataKeyNames="otrequisitiontranunkid, empcodename,employeeunkid,ischecked,IsGrp,plannedot_hours"
                                                AutoGenerateColumns="False" Width="100%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                ShowFooter="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objdgcolhSelect">
                                                        <HeaderTemplate>
                                                            <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                            <%--   <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />--%>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" />
                                                            <%--Pinkal (10-Jan-2020) -- End--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                            <%--<asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />--%>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                            <%--Pinkal (10-Jan-2020) -- End--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Employee" DataField="empcodename" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotreqemp" ItemStyle-Width="200px" />
                                                    <asp:BoundField HeaderText="Request Date" DataField="requestdate" ItemStyle-VerticalAlign="Top"
                                                        FooterText="colhotrequestdate" ItemStyle-Width="100px" />
                                                    <asp:BoundField HeaderText="Planned Start Time" DataField="plannedstart_time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="140px" FooterText="colhotreqplannedstart_time" />
                                                    <asp:BoundField HeaderText="Planned End Time" DataField="plannedend_time" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="140px" FooterText="colhotreqplannedend_time" />
                                                    <asp:BoundField HeaderText="Planned OT Hours" DataField="PlannedworkedDuration" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="130px" FooterText="colhotreqplannedot_hours" />
                                                    <asp:BoundField HeaderText="Request Reason" DataField="request_reason" ItemStyle-VerticalAlign="Top"
                                                        ItemStyle-Width="250px" FooterText="colhotrequestreason" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div id="btnfixedbottom" class="btn-default">
                                            <asp:Button ID="btnSubmitForApproval" runat="server" Text="Submit For Approval" CssClass="btndefault" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<uc2:DeleteReason ID="popupDeleteReason" runat="server" />
                    <uc3:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />--%>
                    <%--'Pinkal (23-Nov-2019) -- Start
                     'Enhancement NMB - Working On OT Enhancement for NMB.--%>
                    <uc3:ConfirmYesNo ID="popupConfirmationForOTCap" Title="" runat="server" Message="" />
                    <%--  'Pinkal (23-Nov-2019) -- End--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
