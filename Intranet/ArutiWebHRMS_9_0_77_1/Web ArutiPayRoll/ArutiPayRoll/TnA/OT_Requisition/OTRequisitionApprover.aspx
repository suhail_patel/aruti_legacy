﻿<%@ Page Title="OT Requisition Approver" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="OTRequisitionApprover.aspx.vb" Inherits="TnA_OT_Requisition_OTRequisitionApprover" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    
 // Pinkal (10-Jan-2020) -- Start
//  Enhancements -  Working on OT Requisistion for NMB.

    $("[id*=ChkAllAddEditEmployee]").live("click", function() {
        var chkHeader = $(this);
        var grid = $(this).closest("table");
        $("input[type=checkbox]", grid).each(function() {
            if (chkHeader.is(":checked")) {
                debugger;
                $(this).attr("checked", "checked");

            } else {
                $(this).removeAttr("checked");
            }
        });
    });

    $("[id*=ChkSelectAddEditEmployee]").live("click", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeader]", grid);
        var row = $(this).closest("tr")[0];

        debugger;
        if (!$(this).is(":checked")) {
            var row = $(this).closest("tr")[0];
            chkHeader.removeAttr("checked");
        } else {

            if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                chkHeader.attr("checked", "checked");
            }
        }
    });


    $("[id*=ChkAllSelectedEmp]").live("click", function() {
        var chkHeader = $(this);
        var grid = $(this).closest("table");
        $("input[type=checkbox]", grid).each(function() {
            if (chkHeader.is(":checked")) {
                debugger;
                $(this).attr("checked", "checked");

            } else {
                $(this).removeAttr("checked");
            }
        });
    });

    $("[id*=ChkSelectedEmp]").live("click", function() {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeader]", grid);
        var row = $(this).closest("tr")[0];

        debugger;
        if (!$(this).is(":checked")) {
            var row = $(this).closest("tr")[0];
            chkHeader.removeAttr("checked");
        } else {

            if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                chkHeader.attr("checked", "checked");
            }
        }
    });

    // Pinkal (10-Jan-2020) -- End
    
    </script>

     <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="OT Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="OT Approver List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" />
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="drpLevel" runat="server" AutoPostBack ="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver" Width="100%" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpApprover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            
                            <div id="scrollable-container" style="width: 100%; height: 350px; overflow: auto">
                                <asp:GridView ID="gvApproverList" DataKeyNames="tnamappingunkid" runat="server"
                                    AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit"  OnClick="lnkedit_Click"
                                                    CommandArgument='<%#Eval("tnamappingunkid") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click"
                                                    CommandArgument='<%#Eval("tnamappingunkid") %>' ToolTip="Delete">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Active"  OnClick = "lnkActive_Click"
                                                    CommandArgument='<%#Eval("tnamappingunkid") %>'>
                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                </asp:LinkButton>
                                                
                                                <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="DeActive"  OnClick ="lnkDeActive_Click"
                                                    CommandArgument='<%#Eval("tnamappingunkid") %>'>
                                                    <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                </asp:LinkButton>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Approver Name" DataField="Name" FooterText="dgcolhName" />
                                        <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhdepartmentname" />
                                        <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhjobname" />
                                        <asp:BoundField HeaderText="Mapped User" DataField="MappedUser" FooterText="dgcolhMappedUser" />
                                        <asp:BoundField HeaderText="External Approver" DataField="exappr" FooterText="dgcolhexappr" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupOTApproverAddEdit" BackgroundCssClass="modalBackground"
                        TargetControlID="lblCaption" runat="server" PopupControlID="pnlOTAddEditApprover"
                        CancelControlID="lblApproverInfo" />
                    <asp:Panel ID="pnlOTAddEditApprover" runat="server" CssClass="newpopup"
                        Style="display: none; width: 1000px;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader1" runat="server" Text="Add/ Edit OT Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 520px; overflow: auto">
                                <div id="Div7" class="panel-default" style="display: inline-block; width: 48%;">
                                    <div id="Div8" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblApproverInfo" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div9" class="panel-body-default">
                                        <div class="row2">
                                            <div style="width: 100%; height: 400px">
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                        <asp:CheckBox ID="chkAddEditExtApprover" runat="server" AutoPostBack="true" Text="Make External Approver" />
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                        <asp:Label ID="lblApproverName" runat="server" Text="Name" />
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                        <asp:DropDownList ID="drpAddEditApprover" runat="server" Width="350px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                        <asp:Label ID="lblApproverlevel" runat="server" Text="Level" />
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                        <asp:DropDownList ID="drpAddEditApproverLevel" runat="server" Width="350px">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="PnlApproverUser" runat="server">
                                                    <div class="row2">
                                                        <div class="ib" style="width: 15%">
                                                            <asp:Label ID="lblApproverUser" runat="server" Text="User" />
                                                        </div>
                                                        <div class="ib" style="width: 78%">
                                                            <asp:DropDownList ID="drpAddEditUser" runat="server" Width="350px">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                
                                                  <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                         <asp:CheckBox ID="chkHodCapForOT" runat="server" AutoPostBack="False" Text="HOD Cap for OT" />
                                                    </div>
                                                </div>
                                                
                                                 <%--'Pinkal (27-Dec-2019) -- Start
                                                'Enhancement - Changes related To OT NMB Testing.--%>
                                                <div class="row2">
                                                    <div class="ib" style="width: 68%; margin-bottom: 2px"> 
                                                        <asp:TextBox ID="txtSearchEmployee" runat="server" AutoPostBack="true"></asp:TextBox>
                                                    </div>                                                    
                                                    <div class="ib" style="width: 10%; text-align: right; font-weight: bold"> 
                                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover" ></asp:LinkButton>
                                                    </div> 
                                                    <div class="ib" style="width: 10%; text-align: right; font-weight: bold; margin-left: 10px"> 
                                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                  <%--'Pinkal (27-Dec-2019) -- End.--%>
                                                
                                                <div id="Div10" style="width: 100%; height: 190px; overflow: auto">
                                                    <asp:GridView ID="gvAddEditEmployee" runat="server" AutoGenerateColumns="false" Width="99%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" DataKeyNames="employeeunkid,employeecode"
                                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                  <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                  <%--<asp:CheckBox ID="ChkAllAddEditEmployee" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAllAddEditEmployee_CheckedChanged" />--%>
                                                                 <asp:CheckBox ID="ChkAllAddEditEmployee" runat="server" />
                                                                  <%--Pinkal (10-Jan-2020) -- End--%>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                  <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                  <%--<asp:CheckBox ID="ChkSelectAddEditEmployee" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelectAddEditEmployee_CheckedChanged"  />--%>
                                                                   <asp:CheckBox ID="ChkSelectAddEditEmployee" runat="server" />
                                                                   <%--Pinkal (10-Jan-2020) -- End--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Employee Code" DataField="employeecode" FooterText="dgcolhEcode" />
                                                            <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEname" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnAddEmployee" runat="server" Text="Add" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="Div12" class="panel-default" style="display: inline-block; width: 47%;">
                                    <div id="Div13" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblOTAddEditAssignedEmp" runat="server" Text="Selected Employee"
                                                Font-Bold="true" />
                                        </div>
                                    </div>
                                    
                                    <div id="Div14" class="panel-body-default">
                                        <div class="row2">
                                               <div class="ib" style="width: 99%; margin-bottom: 5px"> 
                                                      <asp:TextBox ID="txtAssignedEmpSearch" runat="server" AutoPostBack="true"></asp:TextBox>
                                               </div>            
                                        </div>
                                        <div class="row2">
                                            <div style="width: 100%; height: 350px; overflow: auto">
                                                <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="employeeunkid,tnamappingunkid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                                <%--<asp:CheckBox ID="ChkAllSelectedEmp" runat="server" AutoPostBack="true"  OnCheckedChanged="ChkAllSelectedEmp_CheckedChanged" />--%>
                                                                <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" />
                                                                <%--Pinkal (10-Jan-2020) -- End--%>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                            <%--Pinkal (10-Jan-2020) -- Start
                                                                  Enhancements -  Working on OT Requisistion for NMB.--%>
                                                               <%-- <asp:CheckBox ID="ChkSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged ="chkSelectedEmp_CheckedChanged"/>--%>
                                                                <asp:CheckBox ID="ChkSelectedEmp" runat="server"/>
                                                                 <%--Pinkal (10-Jan-2020) -- End--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Employee" DataField="ename" FooterText = "dgcolhEmpName" />
                                                        <asp:BoundField HeaderText="Department" DataField="edept" FooterText = "dgcolhEDept" />
                                                        <asp:BoundField HeaderText="Job" DataField="ejob" FooterText = "dgcolhEJob" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnDeleteEmployee" runat="server" Text="Delete" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                                                                
                            </div>
                            
                            <div class="panel-footer">
                                <asp:Button ID="btnpopupsave" runat="server" Text="Save" CssClass="btndefault" />
                                <asp:Button ID="btnpopupclose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                            
                        </div>
                         <%--'Pinkal (27-Dec-2019) -- Start
                         'Enhancement - Changes related To OT NMB Testing.--%>
                        <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                       <%-- 'Pinkal (27-Dec-2019) -- End--%>
                    </asp:Panel>
                    
                    <%--<cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        TargetControlID="lblApproverInfo" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblApproverName" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 600px;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader2" runat="server" Text="Add/ Edit Grievance Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 480px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblApproverInfo1" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="height: 300px; margin: auto; padding: auto">
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_level" runat="server" Text="Level"></asp:Label>
                                            </div>
                                            <div style="width: 80%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_level" runat="server" Width="450px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_user" runat="server" Text="User"></asp:Label>
                                            </div>
                                            <div style="width: 80%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_user" runat="server" Width="450px" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    
                                        <div id="Div11" style="width: 100%; height: 225px; overflow: auto">
                                            <asp:GridView ID="TvApproverUseraccess" runat="server" ShowHeader="false" AutoGenerateColumns="False"
                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="UserAccess" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnApproverUseraccessSave" runat="server" Text="Save" CssClass="btndefault" />
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>--%>
                    
                    
                    <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                    <uc9:ConfirmYesNo ID="confirmationSelectedEmp" runat="server" />
                    <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" />
                    <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" />
                    <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

