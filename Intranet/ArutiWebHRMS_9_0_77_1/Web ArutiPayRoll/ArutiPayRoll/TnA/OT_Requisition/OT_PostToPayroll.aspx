﻿<%@ Page Title="Post To Payroll" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="~/TnA/OT_Requisition/OT_PostToPayroll.aspx.vb" Inherits="TnA_OT_Requisition_OT_PostToPayroll"  %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        $("[id*=chkSelectAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkSelect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
        
        
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Post To Payroll"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblViewBy" runat="server" Text="View Type"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboViewBy" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 9%">
                                                <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="true"/>
                                            </td>
                                            <td style="width: 9%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 9%">
                                                <asp:Label ID="LblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="true"/>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnUnposting" runat="server" Text="UnPost" CssClass="btndefault" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnPosting" runat="server" Text="Post" CssClass="btndefault" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvdata" runat="server" Height="350px" ScrollBars="Auto" Width="100%">
                                                    <asp:GridView ID="gvOTRequisitionList" runat="server" DataKeyNames="otrequisitionprocesstranunkid,otrequisitiontranunkid,Isgroup,Particulars,employeeunkid,RDate"
                                                            AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"  ShowFooter="false">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkSelect" runat="server"  />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Particulars" HeaderText="Particulars" FooterText="dgcolhParticulars" />
                                                            <asp:BoundField DataField="actualstart_time" HeaderText="Actual Start Time" FooterText="dgcolhAStarttime" />
                                                            <asp:BoundField DataField="actualend_time" HeaderText="Actual End Time" FooterText="dgcolhAEndtime"  />
                                                            <asp:BoundField DataField="actualot_hours" HeaderText="Actual OT Hrs" FooterText="dgcolhActualOTHrs"/>
                                                            <asp:BoundField DataField="Period" HeaderText="Period" FooterText="dgcolhPeriod"/>
                                                            <asp:BoundField DataField="ischange" HeaderText="ischange" FooterText="objcolhIschange" Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" Enabled="false" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
