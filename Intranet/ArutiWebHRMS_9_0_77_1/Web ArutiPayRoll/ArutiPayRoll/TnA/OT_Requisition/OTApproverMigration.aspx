<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="OTApproverMigration.aspx.vb"
    Inherits="TnA_OT_Requisition_OTApproverMigration" Title="OT Approver Migration" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }

        $("[id*=chkSelectAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });

        $("[id*=chkSelect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });


        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtFrmSearch').val().length > 0) {
                $('#<%= dgOldApproverEmp.ClientID %> tbody tr').hide();
                $('#<%= dgOldApproverEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgOldApproverEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtFrmSearch').val() + '\')').parent().show();
            }
            else if ($('#txtFrmSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgOldApproverEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtFrmSearch').val('');
            $('#<%= dgOldApproverEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtFrmSearch').focus();
        }


        function ToSearching() {
            if ($('#txtToSearch').val().length > 0) {
                $('#<%= dgNewApproverAssignEmp.ClientID %> tbody tr').hide();
                $('#<%= dgNewApproverAssignEmp.ClientID %> tbody tr:first').show();
                $('#<%= dgNewApproverAssignEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtToSearch').val() + '\')').parent().show();
            }
            else if ($('#txtToSearch').val().length == 0) {
                resetToSearchValue();
            }
            if ($('#<%= dgNewApproverAssignEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtToSearch').val('');
            $('#<%= dgNewApproverAssignEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtToSearch').focus();
        }
        
        
    </script>

    <center>
        <asp:Panel ID="pnlmain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Approver Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%; text-align: left; border-bottom: 2px solid #DDD; margin-bottom: 10px;
                                                margin-top: 10px">
                                                <table style="width: 45%; margin-top: -5px">
                                                    <tr style="width: 100%">
                                                        <td style="width: 67%">
                                                            <asp:CheckBox ID="chkCapApprovers" runat="server" Text="HOD Cap OT Approvers" AutoPostBack="true">
                                                            </asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                            </td>
                                            <td style="width: 30%">
                                                <asp:CheckBox ID="chkShowInactiveApprovers" runat="server" AutoPostBack="true" Text="Inactive approver" />
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                            </td>
                                            <td style="width: 30%">
                                                <asp:CheckBox ID="chkShowInActiveEmployees" runat="server" AutoPostBack="true" Text="Inactive employee" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblOldApprover" runat="server" Text="From Approver"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboOldApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblNewApprover" runat="server" Text="To Approver"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboNewApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblOldLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboOldLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblNewLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboNewLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 45%" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="height: 5px; width: 100%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%; margin-bottom: 5px">
                                                            <input type="text" id="txtFrmSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="scrollable-container" style="vertical-align: top; overflow: auto; width: 100%;
                                                                height: 400px; margin-top: 5px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                <asp:GridView ID="dgOldApproverEmp" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="tnaapprovertranunkid,tnamappingunkid,employeeunkid">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkSelectAll" runat="server"  />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                                <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgEmployee" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 45%" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="height: 5px; width: 100%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <cc1:TabContainer ID="tabMain" runat="server" ActiveTabIndex="0">
                                                                <cc1:TabPanel ID="tbAssignedEmp" runat="server" HeaderText="Assigned Employee">
                                                                    <ContentTemplate>
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%; margin-bottom: 5px">
                                                                                    <input type="text" id="txtToSearch" name="txtSearch" placeholder="Type To Search Text"
                                                                                        maxlength="50" style="height: 25px; font: 100" onkeyup="ToSearching();" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <div id="scrollable-container2" style="vertical-align: top; overflow: auto; height: 350px"
                                                                                        onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                                        <asp:GridView ID="dgNewApproverAssignEmp" runat="server" AutoGenerateColumns="False"
                                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="tnaapprovertranunkid,tnamappingunkid,employeeunkid">
                                                                                            <Columns>
                                                                                                <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhdgAssignEmployee" />
                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                            </cc1:TabContainer>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnTransfer" runat="server" Text="Transfer" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
