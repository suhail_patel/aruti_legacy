﻿<%@ Page Title="Employee TimeSheet Approval List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeTimeSheetApprovalList.aspx.vb"
    Inherits="Budget_Timesheet_wPg_EmployeeTimeSheetApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Timesheet Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right; font-weight: bold; text-align: right; font-size: 12px">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Style="margin-right: 15px" Text="Allocations"
                                            CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 18%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 6%; text-align: right">
                                                <asp:Label ID="LblDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%" align="right">
                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 11%;">
                                                <asp:Label ID="lblDonor" runat="server" Text="Donor/Grant"></asp:Label>
                                            </td>
                                            <td style="width: 39%">
                                                <asp:DropDownList ID="cboDonor" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 39%" colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="LblProject" runat="server" Text="Project"></asp:Label>
                                            </td>
                                            <td style="width: 39%">
                                                <asp:DropDownList ID="cboProject" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="LblApprover" runat="server" Text="Approver"></asp:Label>
                                            </td>
                                            <td style="width: 39%" colspan="3">
                                                <asp:DropDownList ID="cboApprover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="LblActivity" runat="server" Text="Activity"></asp:Label>
                                            </td>
                                            <td style="width: 39%">
                                                <asp:DropDownList ID="cboActivity" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="LblApproverStatus" runat="server" Text="Approver Status"></asp:Label>
                                            </td>
                                            <td style="width: 39%" colspan="3">
                                                <asp:DropDownList ID="cboApproverStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="LblStatus" runat="server" Text="TimeSheet Status"></asp:Label>
                                            </td>
                                            <td style="width: 39%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 39%" colspan="3">
                                                <asp:CheckBox ID="chkMyApprovals" runat="server" Checked="true" Visible="false" Text="My Approvals"
                                                    AutoPostBack="true" />
                                            </td>
                                            <td style="width: 1%">
                                            </td>
                                            <td style="width: 11%">
                                            </td>
                                            <td style="width: 39%">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="vertical-align: top; ">
                                        <asp:DataGrid ID="dgvEmpTimesheet" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                            <ItemStyle CssClass="griviewitem" />
                                            <Columns>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" FooterText="dgcolhIsCheck">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                
                                                
                                                   <%--    Pinkal (28-Mar-2018) -- Start
                                                       Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details. --%>
                                                        <asp:TemplateColumn FooterText="objdgcolhShowDetails">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgShowDetails" runat="server" ImageUrl="~/images/Info_icons.png"
                                                                    CommandName="Show Details" ToolTip="Show Project Hour Details"  />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <%--    Pinkal (28-Mar-2018) -- End--%>
                                                
                                                <asp:BoundColumn DataField="Particulars" HeaderText="Particular" ReadOnly="true" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                    FooterText="dgcolhParticular"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Approver" HeaderText="Approver" HeaderStyle-Width="275px" ItemStyle-Width="275px" ReadOnly="true" FooterText="dgcolhApprover">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="approvaldate" HeaderText="Approval Date" HeaderStyle-Width="110px"
                                                    ItemStyle-Width="110px" ReadOnly="true" FooterText="dgcolhApprovalDate"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundname" HeaderText="Donor/Grant" ReadOnly="true" FooterText="dgcolhDonor">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundprojectname" HeaderText="Project Code" HeaderStyle-Width="100px" ItemStyle-Width="100px" ReadOnly="true"
                                                    FooterText="dgcolhProject"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activity_name" HeaderText="Activity Code" HeaderStyle-Width="100px" ItemStyle-Width="100px" ReadOnly="true"
                                                    FooterText="dgcolhActivity"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activity_hrs" HeaderText="Activity Hours" HeaderStyle-Width="75px" ItemStyle-Width="75px" ReadOnly="true"
                                                    FooterText="dgcolhHours"></asp:BoundColumn>
                                                    
                                                      <%--Pinkal (28-Mar-2018) -- Start
                                                      Enhancement - (RefNo: 199)  Working on Additional column to show the description filled by the employee, on timesheet submission screen, view completed submit for approval and timesheet approval].--%>
                                                     <asp:BoundColumn DataField="description" HeaderText="Description" HeaderStyle-Width="175px" ItemStyle-Width="175px" ReadOnly="true"
                                                    FooterText="dgcolhEmpDescription"></asp:BoundColumn>
                                                    <%--Pinkal (28-Mar-2018) -- End--%>
                                                    
                                                     <%--   'Pinkal (28-Jul-2018) -- Start
                                                               'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                                   <asp:BoundColumn DataField="submission_remark" FooterText="dgcolhSubmissionRemark" 
                                                    HeaderText="Submission Remark" ReadOnly="true"></asp:BoundColumn>
                                                    
                                                     <%--  'Pinkal (28-Jul-2018) -- End--%>
                                                    
                                                <asp:BoundColumn DataField="TotalActivityHours" HeaderText="Tot. Activity Hrs" HeaderStyle-Width="100px" ItemStyle-Width="100px" ReadOnly="true"
                                                    FooterText="dgcolhTotalActivityHours"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="status" HeaderText="Status" HeaderStyle-Width="200px" ItemStyle-Width="200px" ReadOnly="true" FooterText="dgcolhStatus">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="periodunkid" HeaderText="" FooterText="objdgcolhPeriodID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="" FooterText="objdgcolhEmployeeID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundsourceunkid" HeaderText="" FooterText="objdgcolhFundSourceID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="projectcodeunkid" HeaderText="" FooterText="objdgcolhProjectID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activityunkid" HeaderText="" FooterText="objdgcolhActivityID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="objdgcolhIsGrp" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="mapuserunkid" HeaderText="" FooterText="objdgcolhMappedUserId"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ApprovalStatusId" HeaderText="" FooterText="objdgcolhStatusID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Employee" HeaderText="" FooterText="objdgcolhEmployeeName"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="approveremployeeunkid" HeaderText="" FooterText="objdgcolhApproverEmployeeID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activitydate" HeaderText="" FooterText="objdgcolhActivityDate"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="priority" HeaderText="" FooterText="objdgcolhPriority"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="emptimesheetunkid" HeaderText="" FooterText="objdgcolhEmpTimesheetID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="timesheetapprovalunkid" HeaderText="" FooterText="objdgcolhTimesheetApprovalID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isexternalapprover" HeaderText="" FooterText="objdgcolhIsExternalApprover"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="tsapproverunkid" HeaderText="" FooterText="objdgcolhApproverunkid"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    
                                                <asp:BoundColumn DataField="ischecked" HeaderText="" FooterText="objdgcolhIsCheck"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ShiftHours" HeaderText="" FooterText="objdgcolhShiftHours"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ShiftHoursInSec" HeaderText="" FooterText="objdgcolhShiftHoursInSec"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TotalActivityHoursInMin" HeaderText="" FooterText="objdgcolhTotalActivityHoursInMin"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    
                                                     <%--    Pinkal (28-Mar-2018) -- Start
                                                       Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details. --%>
                                                       
                                                       <asp:BoundColumn DataField="percentage" HeaderText="" FooterText="objdgcolhpercentage"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                       <%--    Pinkal (28-Mar-2018) -- End--%>
                                                    
                                                 <%--   Pinkal (13-Aug-2018) -- Start
                                                           Enhancement - Changes For PACT [Ref #249,252]--%>
                                                      <asp:BoundColumn DataField="isholiday" HeaderText="" FooterText="objdgcolhIsHoliday"
                                                      ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <%--    Pinkal (13-Aug-2018) -- End--%>      
                                                      
                                                    
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnChangeStatus" runat="server" Text="Change Status" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-body">
                                <cc1:ModalPopupExtender ID="popupShowDetails" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="hdf_Details" PopupControlID="pnlShowDetails" DropShadow="true"
                                    CancelControlID="hdf_Details">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlShowDetails" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 550px">
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblpopupHeader" runat="server" Text="Project & Activity Hours Details"></asp:Label>
                                        </div>
                                        <div id="Div6" class="panel-body-default">
                                            <div style="text-align: left">
                                                <asp:Label ID="LblShowDetailsEmployee" runat="server" Text="Employee" Width="20%" />
                                                <asp:Label ID="objLblEmpVal" runat="server" Text="#Employee" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblShowDetailsPeriod" runat="server" Text="Period" Width="20%" />
                                                <asp:Label ID="objLblPeriodVal" runat="server" Text="#Period" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="Label1" runat="server" Text="Project" Width="20%" />
                                                <asp:Label ID="objLblProjectVal" runat="server" Text="#Project" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblDonorGrant" runat="server" Text="Donor/Grant" Width="20%" />
                                                <asp:Label ID="objLblDonorGrantVal" runat="server" Text="#Donor/Grant" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="Label2" runat="server" Text="Activity" Width="20%" />
                                                <asp:Label ID="objLblActivityVal" runat="server" Text="#Activity" Width="79%" />
                                            </div>
                                        </div>
                                        
                                        <div id="Div8" class="panel-body-default">
                                            <div id="Div9" style="max-height: 400px; overflow: auto; vertical-align: top">
                                                <asp:DataGrid ID="dgvProjectHrDetails" runat="server" AutoGenerateColumns="False"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" a HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Particulars" HeaderText="Particulars" ReadOnly="true"
                                                            FooterText="dgcolhParticulars"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Hours" HeaderText="Hours" ReadOnly="true" FooterText="dgcolhHours">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                            <div class="btn-default">
                                                <asp:Button ID="btnShowDetailsClose" runat="server" CssClass=" btnDefault" Text="Close" />
                                                <asp:HiddenField ID="hdf_Details" runat="server" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </asp:Panel>
                            </div>
                        
                    </div>
                    <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
