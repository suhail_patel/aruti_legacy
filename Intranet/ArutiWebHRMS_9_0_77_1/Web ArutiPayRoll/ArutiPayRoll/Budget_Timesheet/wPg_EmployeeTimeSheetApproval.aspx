﻿<%@ Page Title="Timesheet Approval" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_EmployeeTimeSheetApproval.aspx.vb" Inherits="Budget_Timesheet_wPg_EmployeeTimeSheetApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/MaskedTimeTextBox.ascx" TagName="MaskedTimeTextBox"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)

                if (charCode == 1)
                return false;

            if (charCode > 31 && (charCode <= 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Timesheet Approval"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 8%">
                                                <asp:Label ID="LblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 27%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 5%">
                                            </td>
                                            <td style="width: 8%;" valign="middle" rowspan="2">
                                                <asp:Label ID="LblRemarks" runat="server" Text="Remarks"></asp:Label>
                                            </td>
                                            <td style="width: 42%" valign="top" rowspan="2">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="5">
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="margin-top: 15px;
                                        margin-bottom: 15px; vertical-align: top">
                                        <asp:DataGrid ID="dgvEmpTimesheet" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                            <ItemStyle CssClass="griviewitem" />
                                            <Columns>
                                            
                                               <%--    Pinkal (28-Mar-2018) -- Start
                                                       Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details. --%>
                                                        <asp:TemplateColumn FooterText="objdgcolhShowDetails">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgShowDetails" runat="server" ImageUrl="~/images/Info_icons.png"
                                                                    CommandName="Show Details" ToolTip="Show Project Hour Details"  />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateColumn>
                                                        <%--    Pinkal (28-Mar-2018) -- End--%>
                                            
                                                <asp:BoundColumn DataField="Particulars" FooterText="dgcolhParticular" HeaderText="Particular"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="approvaldate" FooterText="dgcolhActivityDate" HeaderText="Approval Date"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activity_name" FooterText="dgcolhActivity" HeaderText="Activity Code"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Approver" FooterText="dgcolhApprover" HeaderText="Approver"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundname" FooterText="dgcolhDonor" HeaderText="Donor/Grant"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundprojectname" FooterText="dgcolhProject" HeaderText="Project Code"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                    
                                                    <%--Pinkal (28-Mar-2018) -- Start
                                                      Enhancement - (RefNo: 199)  Working on Additional column to show the description filled by the employee, on timesheet submission screen, view completed submit for approval and timesheet approval].--%>
                                                      <asp:BoundColumn DataField="description" FooterText="dgcolhEmpDescription" HeaderText="Description"
                                                    ReadOnly="true"></asp:BoundColumn>
                                                      
                                                      <%--Pinkal (28-Mar-2018) -- End--%>
                                                      
                                                        <%--   'Pinkal (28-Jul-2018) -- Start
                                                               'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]--%>
                                                   <asp:BoundColumn DataField="submission_remark" FooterText="dgcolhSubmissionRemark" 
                                                    HeaderText="Submission Remark" ReadOnly="true"></asp:BoundColumn>
                                                    
                                                     <%--  'Pinkal (28-Jul-2018) -- End--%>
                                                      
                                                    
                                                <asp:BoundColumn DataField="TotalActivityHours" FooterText="dgcolhTotalActivityHours"
                                                    HeaderText="Total Activity Hours" ReadOnly="true"></asp:BoundColumn>
                                                <asp:TemplateColumn FooterText="dgcolhEmpHours" HeaderText="Hours : Mins" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtHours" runat="server" AutoPostBack="true" MaxLength="2" onKeypress="return onlyNumbers(this, event);"
                                                            OnTextChanged="txtHours_TextChanged" Style="text-align: right" Width="30px" />
                                                        <asp:Label ID="lblColon" runat="server" Text=":" />
                                                        <asp:TextBox ID="txtMinutes" runat="server" AutoPostBack="true" MaxLength="2" onKeypress="return onlyNumbers(this, event);"
                                                            OnTextChanged="txtMinutes_TextChanged" Style="text-align: right" Width="30px" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="102px" />
                                                    <ItemStyle Width="102px" HorizontalAlign="Center" />
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="activity_hrs" FooterText="dgcolhHours" HeaderText="Activity Hours"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activity_hrsinMins" FooterText="objdgcolhActivityHrsInMins"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="percentage" FooterText="objdgcolhActivityPercentage"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhPeriodID" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeID" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="fundsourceunkid" FooterText="objdgcolhFundSourceID" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="projectcodeunkid" FooterText="objdgcolhProjectID" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activityunkid" FooterText="objdgcolhActivityID" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrp" ReadOnly="true" Visible="false">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="mapuserunkid" FooterText="objdgcolhMappedUserId" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ApprovalStatusId" FooterText="objdgcolhStatusID" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="Employee" FooterText="objdgcolhEmployeeName" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="approveremployeeunkid" FooterText="objdgcolhApproverEmployeeID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="activitydate" FooterText="objdgcolhActivityDate" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="priority" FooterText="objdgcolhPriority" ReadOnly="true"
                                                    Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="emptimesheetunkid" FooterText="objdgcolhEmpTimesheetID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="timesheetapprovalunkid" FooterText="objdgcolhTimesheetApprovalID"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isexternalapprover" FooterText="objdgcolhIsExternalApprover"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="TotalActivityHoursInMin" FooterText="objdgcolhTotalActivityHoursInMin"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="ShiftHoursInSec" FooterText="objdgcolhShiftHoursInSec"
                                                    ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    
                                                    
                                                     <%--   Pinkal (13-Aug-2018) -- Start
                                                           Enhancement - Changes For PACT [Ref #249,252]--%>
                                                      <asp:BoundColumn DataField="isholiday" HeaderText="" FooterText="objdgcolhIsHoliday"
                                                      ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                <%--    Pinkal (13-Aug-2018) -- End--%>      
                                                    
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btnDefault" />
                                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                            <div class="panel-body">
                                <cc1:ModalPopupExtender ID="popupShowDetails" runat="server" BackgroundCssClass="modalBackground"
                                    TargetControlID="hdf_Details" PopupControlID="pnlShowDetails" DropShadow="true"
                                    CancelControlID="hdf_Details">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnlShowDetails" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 550px">
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblpopupHeader" runat="server" Text="Project & Activity Hours Details"></asp:Label>
                                        </div>
                                        <div id="Div6" class="panel-body-default">
                                            <div style="text-align: left">
                                                <asp:Label ID="LblShowDetailsEmployee" runat="server" Text="Employee" Width="20%" />
                                                <asp:Label ID="objLblEmpVal" runat="server" Text="#Employee" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblShowDetailsPeriod" runat="server" Text="Period" Width="20%" />
                                                <asp:Label ID="objLblPeriodVal" runat="server" Text="#Period" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="Label1" runat="server" Text="Project" Width="20%" />
                                                <asp:Label ID="objLblProjectVal" runat="server" Text="#Project" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="LblDonorGrant" runat="server" Text="Donor/Grant" Width="20%" />
                                                <asp:Label ID="objLblDonorGrantVal" runat="server" Text="#Donor/Grant" Width="79%" />
                                            </div>
                                            <br />
                                            <div style="text-align: left">
                                                <asp:Label ID="Label2" runat="server" Text="Activity" Width="20%" />
                                                <asp:Label ID="objLblActivityVal" runat="server" Text="#Activity" Width="79%" />
                                            </div>
                                        </div>
                                        
                                        <div id="Div8" class="panel-body-default">
                                            <div id="Div9" style="max-height: 400px; overflow: auto; vertical-align: top">
                                                <asp:DataGrid ID="dgvProjectHrDetails" runat="server" AutoGenerateColumns="False"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" a HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="Particulars" HeaderText="Particulars" ReadOnly="true"
                                                            FooterText="dgcolhParticulars"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="Hours" HeaderText="Hours" ReadOnly="true" FooterText="dgcolhHours">
                                                        </asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                            <div class="btn-default">
                                                <asp:Button ID="btnShowDetailsClose" runat="server" CssClass=" btnDefault" Text="Close" />
                                                <asp:HiddenField ID="hdf_Details" runat="server" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </asp:Panel>
                            </div>
                        
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
