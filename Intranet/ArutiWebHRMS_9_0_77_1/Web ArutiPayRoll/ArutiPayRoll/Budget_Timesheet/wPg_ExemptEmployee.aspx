﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ExemptEmployee.aspx.vb"
    Inherits="Budget_Timesheet_wPg_ExemptEmployee" Title="Exempt Employee List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Exempt Employee List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Exempt Employee List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" Width="100%" />
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="drpEmployee" runat="server" Width="250px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container" style="width: 100%; height: 400px; overflow: auto">
                                <asp:GridView ID="gvExemptEmployeeList" runat="server" AutoGenerateColumns="False"
                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" DataKeyNames =  "tsempexemptunkid,employeeunkid"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateField FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                          ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete"  ToolTip="Delete" CommandArgument='<%#Eval("tsempexemptunkid")%>' OnClick="lnkdelete_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhempname" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                            <uc1:DeleteReason ID="popupDeleteReason" runat="server" />
                           <uc2:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                    </div>
                    
                    
                    <cc1:ModalPopupExtender ID="popupExemptEmployee" BackgroundCssClass="modalBackground"
                        TargetControlID="lblDate" runat="server" PopupControlID="pnlExemptEmployee" DropShadow="false"
                        CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlExemptEmployee" runat="server" CssClass="newpopup" Style="display: none;
                        width: 750px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="LblExemptEmployee" runat="server" Text="Exempt Employee"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div4" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblTitle" runat="server" Text="Exempt Employee"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div5" class="panel-body-default" style="text-align: left">
                                        <div class="row2">
                                            <div class="ib" style="width: 10%">
                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                            </div>
                                            <div class="ib" style="width: 25%">
                                                <uc3:DateCtrl ID="dtpDate" runat="server" AutoPostBack="false" Enabled="false" />
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ib" style="width: 95%;margin-left:10px">
                                               <asp:TextBox ID = "txtSearch" runat = "server" AutoPostBack="true"></asp:TextBox>
                                            </div>
                                            <div id="Div2" style="width: 100%; height: 350px; overflow: auto;margin-top:10px">
                                                <asp:GridView ID="gvExemptEmployee" runat="server" AutoGenerateColumns="False" Width="99%"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" DataKeyNames = "employeeunkid,ischecked" 
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged"  />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"  />
                                                            </ItemTemplate>
                                                             <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                             <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField = "EmpCodeName" HeaderText="Employee" FooterText="dgcolhemp" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnExemptEmployee" runat="server" CssClass="btndefault" Text="Exempt" />
                                            <asp:Button ID="btnExemptClose" runat="server" CssClass="btndefault" Text="Close" />
                                            <asp:HiddenField ID = "btnHiddenLvCancel" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                   
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
