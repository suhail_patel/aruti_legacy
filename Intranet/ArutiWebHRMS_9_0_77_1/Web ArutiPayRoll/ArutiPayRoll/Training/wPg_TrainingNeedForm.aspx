﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_TrainingNeedForm.aspx.vb"
    Inherits="Training_wPg_TrainingNeedForm" Title="Training Need form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js" type="text/javascript"></script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    
    <script type="text/javascript" language="javascript">

        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
        }
        function IsValidAttach() {
            debugger;
            var cbodoctype = $('#<%= cboScanDcoumentType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    

    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }

        function onlyNumbers(txtBox, e) {
            //        var e = event || evt; // for trans-browser compatibility
            //        var charCode = e.which || e.keyCode;
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }

        function CalcAmt() {
            var a = $$('txtPopUpCostQty').val().replace(/[^0-9\.-]+/g, "");
            var b = $$('txtPopUpCostRate').val().replace(/[^0-9\.-]+/g, "");
            var c = a * b;

            PageMethods.ConvertToCurrency(c, onSuccess, onFailure);

            function onSuccess(str) {
                $$('txtPopUpCostUnitAmount').val(str);
                $$('hfPopUpCostUnitAmount').val(str);
            }

            function onFailure(err) {
                alert(err);
            }

        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 98%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Need form"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetailHeader" runat="server" Text="Training Need Request"></asp:Label>
                                </div>
                                <div style="text-align: right;">
                                    <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                        Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div class="ib" style="width: 5%">
                                        <asp:Label ID="lblFormNo" runat="server" Text="Form No."></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 13%">
                                        <asp:TextBox ID="txtFormNo" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="ib" style="width: 23%">
                                        <asp:CheckBox ID="chkIncludeTraining" runat="server" Text="Include Trainings from Performance"
                                            AutoPostBack="true" />
                                    </div>
                                    <div class="ib" style="width: 5%">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 15%">
                                        <asp:DropDownList ID="cboPeriod" runat="server" Width="150px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ib" style="width: 6%">
                                        <asp:Label ID="lblDatefrom" runat="server" Text="Date From"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 15%">
                                        <uc3:DateCtrl ID="dtpDatefrom" runat="server" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 5%">
                                        <asp:Label ID="lblDateTo" runat="server" Text="To"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 13%">
                                        <uc3:DateCtrl ID="dtpDateTo" runat="server" />
                                    </div>
                                    <div class="ib" style="width: 8%">
                                        <asp:Label ID="lblDevRequire" runat="server" Text="Dev. Required"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 13%">
                                        <asp:DropDownList ID="cboDevRequire" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ib" style="width: 6%">
                                        <asp:Label ID="lblPriority" runat="server" Text="Priority"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 14%">
                                        <asp:DropDownList ID="cboPriority" runat="server" Width="150px">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ib" style="width: 7%">
                                        <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 23%">
                                        <asp:TextBox ID="txtGrievanceDesc" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row2" style="margin-top: 20px;">
                                    <div class="ib" style="width: 48%; padding-top: 30px;">
                                        <asp:Panel ID="pnl_dgvEmployeeList" runat="server" Width="100%" Height="230px" ScrollBars="Auto">
                                            <asp:GridView ID="dgvEmployeeList" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                Width="99%">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                        FooterText="objcolhCheckAll">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="employeeunkid" Visible="false" FooterText="objcolhEmpID">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="employeecode" FooterText="colhEmpCode" HeaderText="Code">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="employeename" FooterText="colhEmpName" HeaderText="Employee Name">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>                                    
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btndefault" />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btndefault" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                        <div class="panel-body" style="margin-top: 20px">
                            <div id="Div1" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblTraningNeedRequest" runat="server" Text="Training Need Request"></asp:Label>
                                </div>
                            </div>
                            <div id="Div2" class="panel-body-default">
                                <div class="row2">
                                    <div class="ib" style="width: 100%;">
                                        <asp:Panel ID="Panel1" runat="server" Width="100%" Height="300px" ScrollBars="Auto">
                                            <asp:GridView ID="dgvTrainingNeed" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                Width="99%" DataKeyNames="trainingneedformtranunkid, periodunkid, trainingcourseunkid, priority, isperformance_training, description">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="Edit" FooterText="colhedit" ItemStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit" CommandArgument="<%# Container.DataItemIndex %>"
                                                                CommandName="Change">
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="Delete" FooterText="colhdelete" ItemStyle-Width="2%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove">
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="2%" HeaderText="Cost" FooterText="colhAddCost">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkAddCost" runat="server" ToolTip="Add Cost" CommandArgument="<%# Container.DataItemIndex %>"
                                                                CommandName="AddCost">
                                                                        <i class="fa fa-money"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="2%" HeaderText="Attach." FooterText="colhAddAttachment">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkAddAttachment" runat="server" ToolTip="Add Attachment" CommandArgument="<%# Container.DataItemIndex %>"
                                                                CommandName="AddAttachment">
                                                                        <i class="fa fa-paperclip"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="trainingcoursename" HeaderText="Dev. Required" FooterText="colhDevRequired">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="TotalEmployee" HeaderText="Count" FooterText="colhCount">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="start_date" HeaderText="Start Date" FooterText="colhStartdate">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="end_date" HeaderText="End Date" FooterText="colhEnddate">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="priorityname" HeaderText="Priority" FooterText="colhPriority">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <div style="width: 15%; float: left; text-align: left;">
                                        <asp:Label ID="lblSubmission_remark" runat="server" Text="Submission Remark"></asp:Label>
                                    </div>
                                    <div style="width: 65%; float: left;">
                                        <asp:TextBox ID="txtSubmission_remark" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                    </div>
                                    <div>
                                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btndefault" Visible="false" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupTrainingCost" BackgroundCssClass="modalBackground"
                        TargetControlID="lblPopUpCost" runat="server" PopupControlID="pnlTrainingCost"
                        DropShadow="false">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlTrainingCost" runat="server" CssClass="newpopup" Style="display: none;
                        width: 800px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblPopUpCost" runat="server" Text="Training Cost"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div3" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblPopUpCostHeading2" runat="server" Text="Training Approx. Cost"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div4" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 7%">
                                            <asp:Label ID="lblPopUpCostItem" runat="server" Text="Item"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 21%">
                                            <asp:DropDownList ID="cboPopUpCostItem" runat="server" Width="145px">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblPopUpCostQty" runat="server" Text="Quantity"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 18%">
                                            <asp:TextBox runat="server" ID="txtPopUpCostQty" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"
                                                onfocusout="CalcAmt()"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblPopUpCostRate" runat="server" Text="Unit Price"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 18%">
                                            <asp:TextBox runat="server" ID="txtPopUpCostRate" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"
                                                onfocusout="CalcAmt()"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 7%">
                                            <asp:Label ID="lblPopUpCostUnitAmount" runat="server" Text="Amount"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 21%">
                                            <asp:TextBox runat="server" ID="txtPopUpCostUnitAmount" CssClass="RightTextAlign"
                                                ReadOnly="true" onKeypress="return onlyNumbers(this, event);" Style="width: 70%;
                                                float: left;"></asp:TextBox>
                                            <asp:DropDownList ID="cboPopUpCurrency" runat="server" Width="50px">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hfPopUpCostUnitAmount" runat="server" />
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblPopUpCostDescription" runat="server" Text="Description"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 49%">
                                            <asp:TextBox runat="server" ID="txtPopUpCostDescription" TextMode="MultiLine" Rows="3"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default" style="margin: 10px 0px 5px 0px;">
                                        <asp:Button ID="btnAddCost" runat="server" Text="Add" CssClass="btndefault" />
                                        <asp:Button ID="btnUpdateCost" runat="server" Text="Update" CssClass="btndefault" />
                                        <asp:Button ID="btnCancelCost" runat="server" Text="Cancel" CssClass="btndefault" />
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 100%">
                                            <asp:Panel ID="Panel2" runat="server" Width="100%" Height="230px" ScrollBars="Auto">
                                                <asp:GridView ID="dgvItemCost" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    Width="99%" DataKeyNames="trainingneedformcosttranunkid, trainingcostitemunkid, countryunkid, description">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhCostedit" HeaderText="Edit" ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCostedit" runat="server" CssClass="gridedit" ToolTip="Edit"
                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhCostdelete" HeaderText="Delete" ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCostdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="trainingcostitemname" FooterText="colhItem" HeaderText="Item">
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="quantity" FooterText="colhQty" HeaderText="Quantity" HeaderStyle-HorizontalAlign="Right"
                                                            ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="unitprice" FooterText="colhUnitCost" HeaderText="Unit Price"
                                                            HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                        <asp:BoundField DataField="amount" FooterText="colhAmount" HeaderText="Amount" HeaderStyle-HorizontalAlign="Right"
                                                            ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row2" style="text-align: right">
                                        <div class="ib" style="width: 9%">
                                            <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox runat="server" ID="txtGrandTotal" ReadOnly="true" onKeypress="return onlyNumbers(this, event);"
                                                Style="text-align: right;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCostSave" runat="server" Text="Save" CssClass="btndefault" Visible="false" />
                                        <asp:Button ID="btnCostClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    
                    <div id="ScanAttachment">
                                            <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                                                TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                                                CancelControlID="hdf_ScanAttchment">
                                            </cc1:ModalPopupExtender>
                                            <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                                                Style="display: none;">
                                                <div class="panel-primary" style="margin: 0px">
                                                    <div class="panel-heading">
                                                        <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                                        <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"></asp:Label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div id="Div36" class="panel-default">
                                                            <div id="Div37" class="panel-body-default">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                                                Width="200px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                                <div id="fileuploader">
                                                                                    <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse"
                                                                                        onclick="return IsValidAttach();" />
                                                                                </div>
                                                                            </asp:Panel>
                                                                            <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                                                Text="Browse" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td colspan="3" style="width: 100%">
                                                                            <asp:GridView ID="dgvAttchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="scanattachtranunkid, filepath, GUID" >
                                                                                <Columns>
                                                                                    <asp:TemplateField FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>"
                                                                                                    ToolTip="Delete"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" CommandArgument="<%# Container.DataItemIndex %>" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhFileName" />                                                                                    
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div class="btn-default">
                                                                    <div style="float: left">
                                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                    </div>
                                                                    <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                                    <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                    </asp:Panel>
                                            <uc2:Cnf_YesNo ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"     />
                                        </div>
                                        
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" _ShowSkill="true" />
                    <ucDel:DeleteReason ID="popupDeleteTrainNeedForm" runat="server" Title="Are you sure you want to delete selected Training Need Request?"
                        ValidationGroup="DeleteTrainNeedForm"  />
                    <ucDel:DeleteReason ID="popupDeleteTrainNeedCost" runat="server" Title="Are you sure you want to delete selected Cost Item?"
                        ValidationGroup="DeleteTrainNeedCost" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dgvTrainingNeed" EventName="RowCommand" />
                    <asp:PostBackTrigger ControlID="dgvAttchment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                url: "wPg_TrainingNeedForm.aspx?uploadimage=mSEfU19VPc4=",
                multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                    $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
    </script>
        
</asp:Content>
