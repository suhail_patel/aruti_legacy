﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_TrainingImpactList.aspx.vb" Inherits="wPg_TrainingImpactList" Title="Training Impact List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>
<input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Impact List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 70%;">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCourse" runat="server" Text="Course"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpCourse" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLineManager" runat="server" Text="Line Manager / Supervisor"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpLineManager" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 30%" colspan="2">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" Width="75px" CssClass="btndefault" />
                                        <asp:Button ID="BtnSearch" runat="server" Text="Search" Width="75px" CssClass="btndefault" />
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" Width="75px" CssClass="btndefault" />
                                    </div>
                                </div>
                                <asp:Panel ID="pnldgView" runat="server" ScrollBars="Auto" Width="100%">
                                    <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" Width="99%">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Select"
                                                            ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit"
                                                        CommandName="Select" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Manager" HeaderText="Line Manager / Supervisor" ReadOnly="True"
                                                FooterText="colhLineManager" />
                                            <asp:BoundColumn DataField="employee" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee" />
                                            <asp:BoundColumn DataField="status" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                            <asp:BoundColumn DataField="iscomplete" HeaderText="IsComplete" ReadOnly="True" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
