﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

Partial Class wpgTraning_Enroll_Cancel
    Inherits Basepage

#Region " Private Variable(s) "
    Dim strSearching As String = ""
    Dim msg As New CommonCodes
    Dim objEnroll_Cancel As New clsTraining_Enrollment_Tran
    Dim dsList As DataSet
    Dim objDic As New Dictionary(Of Integer, String)

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmTraningRegistraionCancelList"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Try         'Hemant (13 Aug 2020)
        Dim dsCombo As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objCourse As New clsTraining_Scheduling
        Dim objStatus As New clsMasterData
        Dim objBranch As New clsStation


        If (Session("LoginBy") = Global.User.en_loginby.User) Then
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'Else
            '    dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Employee", True)

            'Shani(24-Aug-2015) -- End
            'S.SANDEEP [ 04 MAY 2012 ] -- END

            'Sohail (23 Apr 2012) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables("Employee")
                .DataBind()
                .SelectedValue = 0
            End With
        Else
            Dim objglobalassess = New GlobalAccess
            objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
            cboEmployee.DataSource = objglobalassess.ListOfEmployee
            cboEmployee.DataTextField = "loginname"
            cboEmployee.DataValueField = "employeeunkid"
            cboEmployee.DataBind()
        End If

        dsCombo = objStatus.GetTraining_Status("Status", True)
        With cboStatus
            .DataValueField = "Id"
            .DataTextField = "NAME"
            .DataSource = dsCombo.Tables("Status")
            .DataBind()
            .SelectedValue = 0
        End With

        dsCombo = objCourse.getComboList("Course", True)
        With cboCourseTitle
            .DataValueField = "Id"
            .DataTextField = "Name"
            .DataSource = dsCombo.Tables("Course")
            .DataBind()
            .SelectedValue = 0
        End With

        dsCombo = objBranch.getComboList("branch", True)
        With cboBranch
            .DataValueField = "stationunkid"
            .DataTextField = "name"
            .DataSource = dsCombo.Tables("branch")
            .DataBind()
            .SelectedValue = 0
        End With
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim strSearching As String = String.Empty

        Try


            'Pinkal (12-Feb-2015) -- Start
            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
            If Session("LoginBy") = Global.User.en_loginby.User AndAlso CBool(Session("AllowToViewTrainingEnrollmentList")) = False Then Exit Sub
            'Pinkal (12-Feb-2015) -- End



            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objEnroll_Cancel.GetList("List", True)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEnroll_Cancel.GetList("List", True, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
            dsList = objEnroll_Cancel.GetList(Session("Database_Name"), _
                                              Session("UserId"), _
                                              Session("Fin_year"), _
                                              Session("CompanyUnkId"), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                              Session("UserAccessModeSetting"), True, _
                                              Session("IsIncludeInactiveEmp"), "List", True, "")
            'Shani(20-Nov-2015) -- End

            'Sohail (23 Apr 2012) -- End

            If CInt(cboBranch.SelectedValue) > 0 Then
                strSearching &= "AND status_id = " & CInt(cboBranch.SelectedValue) & " "
            End If


            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboCourseTitle.SelectedValue) > 0 Then
                strSearching &= "AND trainingschedulingunkid = " & CInt(cboCourseTitle.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                strSearching &= "AND status_id = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If dtApplyDate.IsNull = False AndAlso dtApplyDateTo.IsNull = False Then
                strSearching &= "AND EnrollDate >= " & eZeeDate.convertDate(dtApplyDate.GetDate) & " AND EnrollDate <= " & eZeeDate.convertDate(dtApplyDateTo.GetDate) & " "
            End If

            If dtCancelDate.IsNull = False AndAlso dtCancelDateTo.IsNull = False Then
                strSearching &= "AND CancelDate >= " & eZeeDate.convertDate(dtCancelDate.GetDate) & " AND CancelDate <= " & eZeeDate.convertDate(dtCancelDateTo.GetDate) & " "
            End If

            If txtRemark.Text.Trim <> "" Then
                strSearching &= "AND enroll_remark LIKE '%" & txtRemark.Text & "%'" & " "
            End If

            If radlstOperation.SelectedValue > 0 Then
                'If radlstOperation.Items.Item(0).Selected Then
                'strSearching &= "AND isvoid = 0 AND iscancel = 0 "
                ' Else
                If radlstOperation.Items(0).Selected Then
                    strSearching &= "AND iscancel = 1 "
                ElseIf radlstOperation.Items(1).Selected Then
                    strSearching &= "AND isvoid = 1 "
                End If

            End If
            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtTable = New DataView(dsList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsList.Tables("List")
            End If

            If dtTable IsNot Nothing Then
                dgView.DataSource = dtTable
                'dgView.DataKeyField = "traningenrolltranunkid"
                dgView.DataBind()
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                msg.DisplayError("Procedure FillGrid : " & ex.Message, Me)
            End If
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            cboBranch.SelectedValue = 0
            cboCourseTitle.SelectedValue = 0

            If (Session("loginBy") = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = 0
            End If

            cboStatus.SelectedValue = 0
            txtRemark.Text = ""
            'Dim iCnt As Integer = radlstOperation.Items.Count
            'For i As Integer = 0 To iCnt - 1
            '    Select Case i
            '        Case 0, 1, 2
            '            radlstOperation.Items(i).Selected = False
            '        Case Else
            '            radlstOperation.Items(i).Selected = True
            '    End Select
            'Next

            radlstOperation.SelectedItem.Selected = False
            radlstOperation.Items(2).Selected = True
            dtApplyDate.SetDate = Nothing
            dtApplyDateTo.SetDate = Nothing
            dtCancelDate.SetDate = Nothing
            dtCancelDateTo.SetDate = Nothing

        Catch ex As Exception
            msg.DisplayError("Procedure ClearObject : " & ex.Message, Me)
        End Try

    End Sub
#End Region

#Region " Form's Event(s) "
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("clsUser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            If (Page.IsPostBack = False) Then
                Dim clsUser As New User
                clsUser = CType(Session("clsuser"), User)

                Call FillCombo()
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'Call FillGrid()
                dgView.DataSource = New List(Of String)
                dgView.DataBind()

                'SHANI [01 FEB 2015]--END

                dtApplyDate.SetDate = Nothing
                dtApplyDateTo.SetDate = Nothing
                dtCancelDate.SetDate = Nothing
                dtCancelDateTo.SetDate = Nothing

            End If


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                dgView.Columns(3).Visible = True
            Else
                dgView.Columns(3).Visible = False
            End If
            'Pinkal (22-Mar-2012) -- End



        Catch ex As Exception
            msg.DisplayError("Page_Load :- " & ex.Message, Me)
        End Try

    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError("btnSearch_Click" & ex.Message, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ClearObject()
            Call FillGrid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError("btnReset_Click Event : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound

        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


        If (e.Item.ItemIndex >= 0) Then

            If dsList Is Nothing Then
                Dim objMaster As New clsMasterData
                dsList = objMaster.getComboListTrainingAwardMode("Mode", False)
                For Each dsRow As DataRow In dsList.Tables("Mode").Rows
                    objDic.Add(CInt(dsRow.Item("Id")), dsRow.Item("Name").ToString)
                Next
            End If


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.

            'If e.Item.Cells(4).Text <> "&nbsp;" And e.Item.Cells(4).Text <> "" Then
                '    If Session("DateFormat") <> Nothing Then
                '        e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat"))
                '    Else
            '    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text)
            'End If
                'End If
            'If e.Item.Cells(5).Text <> "&nbsp;" And e.Item.Cells(5).Text <> "" Then
                '    If Session("DateFormat") <> Nothing Then
                '        e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToString(Session("DateFormat"))
                '    Else
            '    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text)
            'End If
                'End If

            If e.Item.Cells(4).Text <> "&nbsp;" And e.Item.Cells(4).Text <> "" Then
                    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).Date.ToShortDateString
            End If

            If e.Item.Cells(5).Text <> "&nbsp;" And e.Item.Cells(5).Text <> "" Then
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
                End If


                'Pinkal (16-Apr-2016) -- End


            'SANDEEP (04 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If CInt(e.Item.Cells(8).Text) > 0 AndAlso CInt(e.Item.Cells(8).Text) <> enTrainingAward_Modes.RE_CATEGORIZE AndAlso objDic.ContainsKey(CInt(e.Item.Cells(8).Text)) = True Then
            '    e.Item.Cells(5).Text = objDic.Item(CInt(e.Item.Cells(8).Text)).ToString
            'End If
            If CInt(e.Item.Cells(10).Text) > 0 AndAlso CInt(e.Item.Cells(10).Text) <> enTrainingAward_Modes.RE_CATEGORIZE AndAlso objDic.ContainsKey(CInt(e.Item.Cells(10).Text)) = True Then
                e.Item.Cells(7).Text = objDic.Item(CInt(e.Item.Cells(10).Text)).ToString
            End If
            'SANDEEP (04 Apr 2012)-END

            'SANDEEP (04 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            If CBool(e.Item.Cells(9).Text) = True Then
                e.Item.Cells(9).Text = "Yes"
            Else
                e.Item.Cells(9).Text = "No"
            End If
            'SANDEEP (04 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

        End If

        Catch ex As Exception
            msg.DisplayError("dgView_ItemDataBound : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError("dgView_PageIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region


    'Anjan [09 October 2014] -- Start
    'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]--END

            Me.lblCancellationDateFrom.Text = Language._Object.getCaption(Me.lblCancellationDateFrom.ID, Me.lblCancellationDateFrom.Text)
            Me.lblCancellationDateTo.Text = Language._Object.getCaption(Me.lblCancellationDateTo.ID, Me.lblCancellationDateTo.Text)

            Me.lblEnrollmentDateTo.Text = Language._Object.getCaption(Me.lblEnrollmentDateTo.ID, Me.lblEnrollmentDateTo.Text)
            Me.lblEnrollmentdateFrom.Text = Language._Object.getCaption(Me.lblEnrollmentdateFrom.ID, Me.lblEnrollmentdateFrom.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.ID, Me.lblCourse.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)


            Me.dgView.Columns(4).HeaderText = Language._Object.getCaption(Me.dgView.Columns(4).FooterText, Me.dgView.Columns(4).HeaderText)
            Me.dgView.Columns(5).HeaderText = Language._Object.getCaption(Me.dgView.Columns(5).FooterText, Me.dgView.Columns(5).HeaderText)
            Me.dgView.Columns(6).HeaderText = Language._Object.getCaption(Me.dgView.Columns(6).FooterText, Me.dgView.Columns(6).HeaderText)
            Me.dgView.Columns(7).HeaderText = Language._Object.getCaption(Me.dgView.Columns(7).FooterText, Me.dgView.Columns(7).HeaderText)
            Me.dgView.Columns(8).HeaderText = Language._Object.getCaption(Me.dgView.Columns(8).FooterText, Me.dgView.Columns(8).HeaderText)
            Me.dgView.Columns(9).HeaderText = Language._Object.getCaption(Me.dgView.Columns(9).FooterText, Me.dgView.Columns(9).HeaderText)
            Me.radlstOperation.Items(0).Text = Language._Object.getCaption("radCancelled", Me.radlstOperation.Items(0).Text)
            Me.radlstOperation.Items(1).Text = Language._Object.getCaption("radVoid", Me.radlstOperation.Items(1).Text)
            Me.radlstOperation.Items(2).Text = Language._Object.getCaption("radShowAll", Me.radlstOperation.Items(2).Text)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [09 October 2014] -- End


End Class
