﻿<%@ Page Title="Employee Feedback List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmployeeFeedbackList.aspx.vb" Inherits="wPgEmployeeFeedbackList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Feedback List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%;">
                                        <tr style="width: 80%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCourseTitle" runat="server" Text="Course Title"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboCourseTitle" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" CssClass="btndefault" Text="New" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_ghvList" runat="server" Height="350px" ScrollBars="Auto">
                                    <asp:DataGrid ID="dgvList" runat="server" AutoGenerateColumns="False" Width="99%"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Select"
                                                            ToolTip="Select"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip=""
                                                        CommandName="Select" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                HeaderStyle-HorizontalAlign="Center">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="TrainingAttended" HeaderText="Training Attended" ReadOnly="true">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="feedbackdate" HeaderText="Feedback Date" ReadOnly="true"
                                                FooterText="colhFeedbackDate"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ECode" HeaderText="Code" ReadOnly="true" FooterText="colhECode">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="EName" HeaderText="Employee" ReadOnly="true" FooterText="colhEName">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="iscomplete" HeaderText="iscomplete" ReadOnly="true" Visible="false">
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup1" runat="server" Title="Are you Sure You Want To delete?:" />
                    <%--<asp:Panel ID="pnlpopup" runat="server" CssClass="modalPopup" Height="100%" Style="display: none"
                        Width="100%">
                        <asp:Panel ID="pnl2" runat="server" Style="cursor: move; background-color: #DDDDDD;
                            border: solid 1px Gray; color: Black">
                            <div>
                                <p>
                                    Are ou Sure You Want To delete?:</p>
                            </div>
                        </asp:Panel>
                        <table style="width: 180px;">
                            <tr>
                                <td>
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtreason" runat="server" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="ButDel" runat="server" Text="Delete" />
                                    <asp:Button ID="CancelButton" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="CancelButton" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreason">
                    </cc1:ModalPopupExtender>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
