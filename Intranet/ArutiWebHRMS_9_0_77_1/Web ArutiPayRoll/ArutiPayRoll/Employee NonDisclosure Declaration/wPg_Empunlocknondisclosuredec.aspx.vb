﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class Employee_NonDisclosure_Declaration_wPg_Empunlocknondisclosuredec
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmEmpUnlockNonDisclosureDeclaration"
    Private objlockunlockEmp As New clsEmpnondisclosuredec_lockunlock
#End Region

#Region " Column Enum "
    Private Enum enColumn
        EmpCode = 1
        EmpName = 2
        LockUnlockdate = 3
    End Enum
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillList()
            Else
                
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
          
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strfilter As String = ""
        Try

            dsList = objlockunlockEmp.GetList(CStr(Session("Database_Name")) _
                                       , CInt(Session("UserId")) _
                                       , CInt(Session("Fin_year")) _
                                       , CInt(Session("CompanyUnkId")) _
                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString) _
                                       , Session("UserAccessModeSetting").ToString _
                                       , True _
                                       , False _
                                       , "List" _
                                       , " AND hrempnondisclosuredec_lockunlock.islock = 1 " _
                                       )


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dc As New DataColumn("IsChecked", Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                dc.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dc)
            End If
          

            Dim mblnBlank As Boolean = False
            If dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = dsList.Tables(0).NewRow()
                dr("IsChecked") = False
                dsList.Tables(0).Rows.Add(dr)
                mblnBlank = True
            End If

            GvLockEmployeeList.DataSource = dsList.Tables(0)
            GvLockEmployeeList.DataBind()

            If mblnBlank = True Then
                GvLockEmployeeList.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            Dim intDays As Integer
            Integer.TryParse(txtUnlockDays.Text, intDays)

            If intDays < 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Unlock days cannot be less than Zero"), Me)
                Exit Sub
            ElseIf intDays > 31 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Unlock days cannot be greater than 31 days"), Me)
                Exit Sub
            End If
            
            Dim lst As List(Of GridViewRow) = (From r As GridViewRow In GvLockEmployeeList.Rows.Cast(Of GridViewRow)() Where (DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True) Select (r)).ToList
            If lst.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select atleast one employee to do further operation on it"), Me)
                Exit Sub
            End If

            
            popup_YesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False

            Dim intDays As Integer
            Integer.TryParse(txtUnlockDays.Text, intDays)

            objlockunlockEmp._Islock = False
            objlockunlockEmp._Nextlockdatetime = ConfigParameter._Object._CurrentDateAndTime.AddDays(intDays)
            objlockunlockEmp._Unlockuserunkid = CInt(Session("UserId"))
            objlockunlockEmp._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime

            objlockunlockEmp._AuditUserunkid = CInt(Session("UserId"))
            objlockunlockEmp._AuditDateTime = ConfigParameter._Object._CurrentDateAndTime
            objlockunlockEmp._ClientIp = Session("IP_ADD").ToString()
            objlockunlockEmp._HostName = Session("HOST_NAME").ToString()
            objlockunlockEmp._FormName = mstrModuleName
            objlockunlockEmp._IsFromWeb = True

            Dim lst As List(Of GridViewRow) = (From r As GridViewRow In GvLockEmployeeList.Rows.Cast(Of GridViewRow)() Where (DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True) Select (r)).ToList

            Dim dtTable As DataTable = Nothing

            If lst.Count > 0 Then
                dtTable = GridViewRowToDataTable(lst)

                If objlockunlockEmp.GlobalUnlockEmployee(dtTable) = False Then
                    DisplayMessage.DisplayMessage(objlockunlockEmp._Message, Me)
                    Exit Sub
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Checked employee(s) unlocked successfully."), Me)
                End If

                FillList()
            End If

            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function GridViewRowToDataTable(ByVal lst As List(Of GridViewRow)) As DataTable
        Dim dtTable As New DataTable
        Try
            dtTable.Columns.Add("ndlockunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("lockunlockdatetime", System.Type.GetType("System.DateTime"))
            dtTable.Columns.Add("lockuserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("unlockuserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("islock", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtTable.Columns.Add("nextlockdatetime", System.Type.GetType("System.DateTime"))
            dtTable.Columns.Add("loginemployeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1

            Dim dr As DataRow

            Dim intDays As Integer
            Integer.TryParse(txtUnlockDays.Text, intDays)

            For Each r As GridViewRow In lst
                dr = dtTable.NewRow

                dr.Item("ndlockunkid") = CInt(GvLockEmployeeList.DataKeys(r.RowIndex).Item("ndlockunkid"))
                dr.Item("employeeunkid") = CInt(GvLockEmployeeList.DataKeys(r.RowIndex).Item("employeeunkid"))
                dr.Item("employeecode") = GvLockEmployeeList.Rows(r.RowIndex).Cells(enColumn.EmpCode).Text
                dr.Item("employeename") = GvLockEmployeeList.Rows(r.RowIndex).Cells(enColumn.EmpName).Text
                dr.Item("lockunlockdatetime") = GvLockEmployeeList.Rows(r.RowIndex).Cells(enColumn.LockUnlockdate).Text
                dr.Item("lockuserunkid") = CInt(GvLockEmployeeList.DataKeys(r.RowIndex).Item("lockuserunkid"))
                dr.Item("unlockuserunkid") = CInt(GvLockEmployeeList.DataKeys(r.RowIndex).Item("unlockuserunkid"))
                dr.Item("islock") = CBool(GvLockEmployeeList.DataKeys(r.RowIndex).Item("islock"))
                dr.Item("nextlockdatetime") = ConfigParameter._Object._CurrentDateAndTime.AddDays(intDays)
                dr.Item("loginemployeeunkid") = CInt(GvLockEmployeeList.DataKeys(r.RowIndex).Item("loginemployeeunkid"))

                dtTable.Rows.Add(dr)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        Return dtTable
    End Function

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub GvLockEmployeeList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvLockEmployeeList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(enColumn.LockUnlockdate).Text <> "" AndAlso e.Row.Cells(enColumn.LockUnlockdate).Text <> "&nbsp;" Then
                    e.Row.Cells(enColumn.LockUnlockdate).Text = CDate(e.Row.Cells(enColumn.LockUnlockdate).Text).ToShortDateString() & " " & CDate(e.Row.Cells(enColumn.LockUnlockdate).Text).ToString("HH:mm")
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"


#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)
            Language._Object.setCaption(Me.lblUnlockDays.ID, Me.lblUnlockDays.Text)

            Language._Object.setCaption(Me.btnUnlock.ID, Me.btnUnlock.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            Language._Object.setCaption(GvLockEmployeeList.Columns(enColumn.EmpCode).FooterText, GvLockEmployeeList.Columns(enColumn.EmpCode).HeaderText)
            Language._Object.setCaption(GvLockEmployeeList.Columns(enColumn.EmpName).FooterText, GvLockEmployeeList.Columns(enColumn.EmpName).HeaderText)
            Language._Object.setCaption(GvLockEmployeeList.Columns(enColumn.LockUnlockdate).FooterText, GvLockEmployeeList.Columns(enColumn.LockUnlockdate).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex.Message, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblPageHeader2.Text = Language._Object.getCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)
            Me.lblUnlockDays.Text = Language._Object.getCaption(Me.lblUnlockDays.ID, Me.lblUnlockDays.Text)

            Me.btnUnlock.Text = Language._Object.getCaption(Me.btnUnlock.ID, Me.btnUnlock.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            GvLockEmployeeList.Columns(enColumn.EmpCode).HeaderText = Language._Object.getCaption(GvLockEmployeeList.Columns(enColumn.EmpCode).FooterText, GvLockEmployeeList.Columns(enColumn.EmpCode).HeaderText)
            GvLockEmployeeList.Columns(enColumn.EmpName).HeaderText = Language._Object.getCaption(GvLockEmployeeList.Columns(enColumn.EmpName).FooterText, GvLockEmployeeList.Columns(enColumn.EmpName).HeaderText)
            GvLockEmployeeList.Columns(enColumn.LockUnlockdate).HeaderText = Language._Object.getCaption(GvLockEmployeeList.Columns(enColumn.LockUnlockdate).FooterText, GvLockEmployeeList.Columns(enColumn.LockUnlockdate).HeaderText)

            popup_YesNo.Title = Language.getMessage(mstrModuleName, 5, "Confirmation")
            popup_YesNo.Message = Language.getMessage(mstrModuleName, 6, "Are you sure you want to unlock checked employee(s)?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Unlock days cannot be less than Zero")
            Language.setMessage(mstrModuleName, 2, "Unlock days cannot be greater than 31 days")
            Language.setMessage(mstrModuleName, 3, "Please Select atleast one employee to do further operation on it")
            Language.setMessage(mstrModuleName, 4, "Checked employee(s) unlocked successfully.")
            Language.setMessage(mstrModuleName, 5, "Confirmation")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to unlock checked employee(s)?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
