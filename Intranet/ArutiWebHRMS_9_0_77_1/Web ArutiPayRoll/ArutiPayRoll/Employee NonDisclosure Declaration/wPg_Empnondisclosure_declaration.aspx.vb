﻿Option Strict On

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports

Partial Class Employee_NonDisclosure_Declaration_wPg_Empnondisclosure_declaration
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmEmpNonDisclosure_Declaration"

    Private objND As clsEmpnondisclosure_declaration_tran
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
			Private blnIsPnlSignVisible As Boolean = False
            'Gajanan [13-June-2020] -- End

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            Else

                'Gajanan [13-June-2020] -- Start
                'Enhancement NMB Employee Declaration Changes.
                imgSignature.Visible = CBool(Session("AllowtoViewSignatureESS"))
                UPUploadSig.Visible = CBool(Session("AllowEmployeeToAddEditSignatureESS"))
                'Gajanan [13-June-2020] -- End
            End If

            'If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If Me.IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call GetControlCaptions()

                Call FillComBo()

                Dim str As String = Session("NonDisclosureDeclaration").ToString.Replace(vbCrLf, "<br />").Replace(ChrW(9), "&emsp;")
                If (CInt(Session("loginBy")) = Global.User.en_loginby.Employee) Then
                    Dim strEmpName As String = Session("E_Firstname").ToString & " " & Session("E_Othername").ToString & " " & Session("E_Surname").ToString
                    strEmpName = strEmpName.Replace("  ", " ")
                    str = str.Replace("#employeename#", "<B>" & strEmpName & "</B>")

                    objlblEmpName.Text = strEmpName
                Else
                    str = str.Replace("#employeename#", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;")
                End If
                lblNonDisclosureDeclaration.Text = str
                objlblEmpDeclareDate.Text = ConfigParameter._Object._CurrentDateAndTime.ToString("dd-MMM-yyyy")
                'Sohail (18 May 2020) -- Start
                'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
                'objlblWitness1Date.Text = ConfigParameter._Object._CurrentDateAndTime.ToString("dd-MMM-yyyy")
                'objlblWitness2Date.Text = ConfigParameter._Object._CurrentDateAndTime.ToString("dd-MMM-yyyy")
                'Dim objEmp As clsEmployee_Master
                'Dim objJob As clsJobs
                'If CInt(Session("NonDisclosureWitnesser1")) > 0 Then
                '    objEmp = New clsEmployee_Master
                '    objJob = New clsJobs
                '    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(Session("NonDisclosureWitnesser1"))
                '    If objEmp._Firstname.Trim <> "" Then
                '        objlblWitness1Name.Text = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
                '        objJob._Jobunkid = objEmp._Jobunkid
                '        objlblWitness1Job.Text = objJob._Job_Name
                '    End If
                'End If
                'If CInt(Session("NonDisclosureWitnesser2")) > 0 Then
                '    objEmp = New clsEmployee_Master
                '    objJob = New clsJobs
                '    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(Session("NonDisclosureWitnesser2"))
                '    If objEmp._Firstname.Trim <> "" Then
                '        objlblWitness2Name.Text = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname
                '        objJob._Jobunkid = objEmp._Jobunkid
                '        objlblWitness2Job.Text = objJob._Job_Name
                '    End If
                'End If
                'Sohail (18 May 2020) -- End
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
                pnlSign.Attributes.Add("style", "display:none")
            'Gajanan [13-June-2020] -- End

            Else
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
                blnIsPnlSignVisible = CBool(Me.ViewState("blnIsPnlSignVisible"))
                If blnIsPnlSignVisible Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "displaypanel", "displaysign();", True)
                End If
            'Gajanan [13-June-2020] -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            Me.ViewState("blnIsPnlSignVisible") = blnIsPnlSignVisible
            'Gajanan [13-June-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Methods / Functions "

    Private Sub FillComBo()
        Dim objEmployee As New clsEmployee_Master
        Try
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(Session("Employeeunkid"))
            If objEmployee._EmpSignature IsNot Nothing Then
                imgSignature.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=3"
            End If
            'Gajanan [13-June-2020] -- End

            'If (CInt(Session("loginBy")) = Global.User.en_loginby.Employee) Then

            'Else

            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub FillList()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function LockUnlockEmployee(ByVal blnIslock As Boolean) As Boolean
        Dim objLockUnlock As New clsEmpnondisclosuredec_lockunlock
        Try
            Dim mintAdLockId As Integer = 0
            Dim mblnisLock As Boolean = False
            Dim dtNextLockdate As Date = Nothing
            If objLockUnlock.isExist(CInt(Session("Employeeunkid")), mblnisLock, mintAdLockId, dtNextLockdate) Then
                If mintAdLockId > 0 AndAlso blnIslock = mblnisLock Then Return True
                objLockUnlock._Ndlockunkid = mintAdLockId
            End If

            objLockUnlock._Employeeunkid = CInt(Session("Employeeunkid"))
            objLockUnlock._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockUnlock._Islock = blnIslock
            objLockUnlock._Nextlockdatetime = Nothing

            If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                If blnIslock Then
                    objLockUnlock._Lockuserunkid = CInt(Session("UserId"))
                Else
                    objLockUnlock._Unlockuserunkid = CInt(Session("UserId"))
                End If
                objLockUnlock._Loginemployeeunkid = 0
                objLockUnlock._AuditUserunkid = CInt(Session("UserId"))
            ElseIf (CInt(Session("loginBy")) = Global.User.en_loginby.Employee) Then
                objLockUnlock._Lockuserunkid = 0
                objLockUnlock._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                If blnIslock = False Then
                    objLockUnlock._Unlockuserunkid = -999 'THIS MEANS EMPLOYEE HIMSELF UNLOCK IT BY COMPLETING LOCKING TENURE AND APPLY FOR NEW DECLARATION.
                    objLockUnlock._AuditUserunkid = -999
                End If
            End If

            objLockUnlock._ClientIp = Session("IP_ADD").ToString
            objLockUnlock._AuditDateTime = ConfigParameter._Object._CurrentDateAndTime
            objLockUnlock._HostName = Session("HOST_NAME").ToString
            objLockUnlock._FormName = mstrModuleName
            objLockUnlock._IsFromWeb = True

            If mintAdLockId <= 0 Then

                If objLockUnlock.Insert() = False Then
                    DisplayMessage.DisplayMessage(objLockUnlock._Message, Me)
                    Return False
                End If
            ElseIf mintAdLockId > 0 Then
                If objLockUnlock.Update() = False Then
                    DisplayMessage.DisplayMessage(objLockUnlock._Message, Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objLockUnlock = Nothing
        End Try
    End Function

    'Sohail (19 Feb 2020) -- Start
    'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
    Private Sub Send_Notification(ByVal lstWebEmail As List(Of clsEmailCollection))
        'Sohail (24 Jan 2022) - [lstWebEmail]
        Try
            'Sohail (24 Jan 2022) -- Start
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
            'If gobjEmailList.Count > 0 Then
            If lstWebEmail.Count > 0 Then
                'Sohail ((24 Jan 2022) -- End
                Dim objSendMail As New clsSendMail
                'Sohail (24 Jan 2022) -- Start
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
                'For Each obj In gobjEmailList
                For Each obj In lstWebEmail
                    'Sohail ((24 Jan 2022) -- End
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._FormName
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    'Sohail (29 May 2020) -- Start
                    'NMB Enhancement # : After employee Acknowledges, the email sent to him will have the declaration form as an attachment.
                    If obj._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = obj._FileName
                    End If
                    'Sohail (29 May 2020) -- End

                    Try
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                    Catch ex As Exception

                    End Try
                Next
                'Sohail (24 Jan 2022) -- Start
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
                'gobjEmailList.Clear()
                lstWebEmail.Clear()
                'Sohail ((24 Jan 2022) -- End
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            'Sohail (24 Jan 2022) -- Start
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
            'If gobjEmailList.Count > 0 Then
            '    gobjEmailList.Clear()
            'End If
            If lstWebEmail.Count > 0 Then
                lstWebEmail.Clear()
            End If
            'Sohail ((24 Jan 2022) -- End
        End Try
    End Sub
    'Sohail (19 Feb 2020) -- End


    'Gajanan [13-June-2020] -- Start
    'Enhancement NMB Employee Declaration Changes.
    Private Function IsAllWitnessOnLeave() As Boolean
        Try
            Dim objLeave As New clsleaveform

            Dim blnWitnesser1status As Boolean = False
            Dim blnWitnesser2status As Boolean = False


            If objLeave.isDayExist(False, CInt(Session("NonDisclosureWitnesser1")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = True Then
                blnWitnesser1status = True
            ElseIf objLeave.isDayExist(True, CInt(Session("NonDisclosureWitnesser1")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = True Then
                blnWitnesser1status = True
            End If

            If objLeave.isDayExist(False, CInt(Session("NonDisclosureWitnesser2")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = True Then
                blnWitnesser2status = True
            ElseIf objLeave.isDayExist(True, CInt(Session("NonDisclosureWitnesser2")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = True Then
                blnWitnesser2status = True
            End If

            If blnWitnesser1status AndAlso blnWitnesser2status Then
                Return True
            End If
            Return False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function

    'Gajanan [13-June-2020] -- End

#End Region

#Region "Button's Event"

    'Sohail (19 Feb 2020) -- Start
    'NMB Enhancement # : Provide close button on the page. If user doesnt want to click on the acknowledge button. Should just click on the close button.
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Sohail (19 Feb 2020) -- End

    Protected Sub btnAcknowledge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAcknowledge.Click
        Dim objND As New clsEmpnondisclosure_declaration_tran
        Try
            If CInt(Session("Employeeunkid")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Employee information not found."), Me)
                Exit Sub
                'Sohail (18 May 2020) -- Start
                'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
                'ElseIf objlblWitness1Name.Text.Trim = "" OrElse objlblWitness1Name.Text = "&nbsp;" Then
            ElseIf CInt(Session("NonDisclosureWitnesser1")) <= 0 Then
                'Sohail (18 May 2020) -- End
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, no witness is mapped for first witness. Please contact administrator."), Me)
                Exit Sub
                'Sohail (18 May 2020) -- Start
                'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
                'ElseIf objlblWitness2Name.Text.Trim = "" OrElse objlblWitness2Name.Text = "&nbsp;" Then
            ElseIf CInt(Session("NonDisclosureWitnesser2")) <= 0 Then
                'Sohail (18 May 2020) -- End
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, no witness is mapped for second witness. Please contact administrator."), Me)
                Exit Sub

                'Gajanan [13-June-2020] -- Start
                'Enhancement NMB Employee Declaration Changes.
            ElseIf chkconfirmSign.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, Please confirm your signature first."), Me)
                Exit Sub
            End If

            If IsAllWitnessOnLeave() = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, Today all witness are on leave so you can't acknowledge today."), Me)
                Exit Sub
            End If
            'Gajanan [13-June-2020] -- End


            Dim mdtDeclarationStartDate As Date = Nothing
            Dim mdtDeclarationEndDate As Date = Nothing
            Dim mintLockDaysAfterNonDisclosureDeclarationToDate As Integer = CInt(Session("LockDaysAfterNonDisclosureDeclarationToDate"))
            Dim mintNewEmpUnLockDaysforDecWithinAppointmentDate As Integer = CInt(Session("NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate"))

            If Session("NonDisclosureDeclarationFromDate") IsNot Nothing AndAlso Session("NonDisclosureDeclarationFromDate").ToString.Trim.ToString <> "" Then
                mdtDeclarationStartDate = eZeeDate.convertDate(Session("NonDisclosureDeclarationFromDate").ToString()).Date
            End If
            If Session("NonDisclosureDeclarationToDate") IsNot Nothing AndAlso Session("NonDisclosureDeclarationToDate").ToString.Trim.ToString <> "" Then
                mdtDeclarationEndDate = eZeeDate.convertDate(Session("NonDisclosureDeclarationToDate").ToString()).Date
                mdtDeclarationEndDate = mdtDeclarationEndDate.AddDays(mintLockDaysAfterNonDisclosureDeclarationToDate)
            End If

            If mdtDeclarationStartDate <> Nothing AndAlso eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) < eZeeDate.convertDate(mdtDeclarationStartDate) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot do declaration before") & " " & mdtDeclarationStartDate.ToString("dd-MMM-yyyy"), Me)
                Exit Sub
            End If

            If objND.isExist(CInt(Session("Employeeunkid")), CInt(Session("Fin_year"))) = True Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Declaration for the current financial year is already done."), Me)
                Exit Sub
            End If

            'Sohail (29 May 2020) -- Start
            'NMB Enhancement # : don't allow employee to Acknowledge if employee signature is not present in the system/
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = CInt(Session("Employeeunkid"))
            If objEmployee._EmpSignature Is Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, You cannot do declaration now. Please set your signature in the system."), Me)
                Exit Sub
            End If
            'Sohail (29 May 2020) -- End

            If mdtDeclarationEndDate <> Nothing AndAlso eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) > eZeeDate.convertDate(mdtDeclarationEndDate) Then

                'Sohail (29 May 2020) -- Start
                'NMB Enhancement # : don't allow employee to Acknowledge if employee signature is not present in the system/
                'Dim objEmployee As New clsEmployee_Master
                'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = CInt(Session("Employeeunkid"))
                'Sohail (29 May 2020) -- End

                Dim mdtDate As Date = objEmployee._Appointeddate.Date

                If objEmployee._Reinstatementdate <> Nothing AndAlso objEmployee._Appointeddate.Date < objEmployee._Reinstatementdate.Date Then
                    mdtDate = objEmployee._Reinstatementdate.Date
                End If

                If eZeeDate.convertDate(mdtDate.AddDays(mintNewEmpUnLockDaysforDecWithinAppointmentDate).Date) < eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) Then
                    Dim mintAdLockId As Integer = 0
                    Dim mblnisLock As Boolean = False
                    Dim dtNextLockdate As Date = Nothing
                    Dim objLockUnlock As New clsEmpnondisclosuredec_lockunlock
                    If objLockUnlock.isExist(CInt(Session("Employeeunkid")), mblnisLock, mintAdLockId, dtNextLockdate) = True Then
                        If mintAdLockId > 0 AndAlso mblnisLock = True Then 'Employee is already locked
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot do declaration now. Last date for declaration was") & " " & mdtDeclarationEndDate.ToString("dd-MMM-yyyy"), Me)
                            Exit Sub

                        ElseIf mintAdLockId > 0 AndAlso mblnisLock = False Then 'Employee is unlocked
                            If eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) > eZeeDate.convertDate(dtNextLockdate) Then
                                If LockUnlockEmployee(True) = False Then Exit Sub 'Lock Employee
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot do declaration now. Last date for declaration was") & " " & mdtDeclarationEndDate.ToString("dd-MMM-yyyy"), Me)
                                Exit Sub
                            Else
                                If LockUnlockEmployee(False) = False Then Exit Sub 'Allow to declare
                            End If
                        End If
                    Else
                        If LockUnlockEmployee(True) = False Then Exit Sub 'Lock Employee
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot do declaration now. Last date for declaration was") & " " & mdtDeclarationEndDate.ToString("dd-MMM-yyyy"), Me)
                        Exit Sub
                    End If
                Else
                    If LockUnlockEmployee(False) = False Then Exit Sub 'Allow to declare
                End If
            Else
                If LockUnlockEmployee(False) = False Then Exit Sub 'Allow to declare
            End If

            popupYesNo.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonYes_Click
        Dim objND As New clsEmpnondisclosure_declaration_tran
        Try
            If objND.isExist(CInt(Session("Employeeunkid")), CInt(Session("Fin_year"))) = True Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Declaration for the current financial year is already done."), Me)
                Exit Sub
            End If

            objND._Nondisclosuredeclarationtranunkid = -1
            objND._Employeeunkid = CInt(Session("Employeeunkid"))
            objND._Yearunkid = CInt(Session("Fin_year"))
            objND._Declaration_Date = ConfigParameter._Object._CurrentDateAndTime

            'Gajanan [28-Aug-2020] -- Start
            'objND._Witness1userunkid = CInt(Session("NonDisclosureWitnesser1"))
            If CInt(Session("Employeeunkid")) = CInt(Session("NonDisclosureWitnesser1")) Then
                objND._Witness1userunkid = CInt(Session("NonDisclosureWitnesser2"))
            Else
            objND._Witness1userunkid = CInt(Session("NonDisclosureWitnesser1"))
            End If
            'Gajanan [28-Aug-2020] -- End

            objND._Witness1_Date = ConfigParameter._Object._CurrentDateAndTime
            objND._Witness2userunkid = CInt(Session("NonDisclosureWitnesser2"))
            objND._Witness2_Date = ConfigParameter._Object._CurrentDateAndTime
            objND._Issubmitforapproval = False
            objND._Approvalstatusunkid = enApprovalStatus.PENDING
            objND._Finalapproverunkid = -1
            If CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                objND._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objND._Userunkid = -1
            Else
                objND._Userunkid = CInt(Session("UserId"))
                objND._Loginemployeeunkid = -1
            End If
            objND._Isweb = True
            objND._Isvoid = False
            objND._Voiddatetime = Nothing
            objND._Voidreason = ""
            objND._Voiduserunkid = -1
            objND._Voidloginemployeeunkid = -1
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objND._AuditUserId = CInt(Session("UserId"))
            Else
                objND._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objND._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objND._ClientIP = CStr(Session("IP_ADD"))
            objND._HostName = CStr(Session("HOST_NAME"))
            objND._FormName = mstrModuleName
		    'Gajanan [27-June-2020] -- Start
		    'Enhancement:Create AT-Non Disclosure Declaration Report
            objND._MacAddress = GetClientMac(CStr(Session("IP_ADD")))
		    'Gajanan [27-June-2020] -- End
            objND._xDataOp = Nothing
            If objND.Insert() = False Then
                If objND._Message <> "" Then
                    DisplayMessage.DisplayMessage(objND._Message, Me)
                End If
                'Sohail (19 Feb 2020) -- Start
                'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
            Else

                'Sohail (29 May 2020) -- Start
                'NMB Enhancement # : don't allow employee to Acknowledge if employee signature is not present in the system/
                Dim objEND As New clsEmployeeNonDisclosureDeclarationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

                objEND.SetDefaultValue()
                objEND._EmployeeId = CInt(Session("Employeeunkid"))
                Dim strEmpName As String = Session("E_Firstname").ToString & " " & Session("E_Othername").ToString & " " & Session("E_Surname").ToString
                strEmpName = strEmpName.Replace("  ", " ")
                objEND._EmployeeName = strEmpName

                objEND._ApplyUserAccessFilter = False

                objEND.generateReportNew(Session("Database_Name").ToString, _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   Session("UserAccessModeSetting").ToString, True, _
                                                   IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                   False, _
                                                   0, enPrintAction.None, enExportAction.PDF)
                'Sohail (29 May 2020) -- End

                Dim strFileName As String = objEND._FileNameAfterExported

                'Sohail (29 May 2020) -- Start
                'NMB Enhancement # : After employee Acknowledges, the email sent to him will have the declaration form as an attachment.
                'objND.SendMailToEmployee_User(CInt(Session("Employeeunkid")).ToString, Session("NonDisclosureDeclarationAcknowledgedNotificationUserIds").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name").ToString, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString(), True, False, enLogin_Mode.EMP_SELF_SERVICE, CInt(Session("Employeeunkid")), 0)
                objND.SendMailToEmployee_User(CInt(Session("Employeeunkid")).ToString, Session("NonDisclosureDeclarationAcknowledgedNotificationUserIds").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name").ToString, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString(), True, False, enLogin_Mode.EMP_SELF_SERVICE, CInt(Session("Employeeunkid")), 0, strFileName)
                'Sohail (29 May 2020) -- End

                'Sohail (24 Jan 2022) -- Start
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
                'Send_Notification()
                Send_Notification(objND._WebEmailList)
                'Sohail ((24 Jan 2022) -- End

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Declaration Submitted Successfully."), Me)
                'Sohail (19 Feb 2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Sohail (18 May 2020) -- Start
    'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        Dim objND As New clsEmpnondisclosure_declaration_tran
        Dim objEND As New clsEmployeeNonDisclosureDeclarationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If CInt(Session("Employeeunkid")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Employee information not found."), Me)
                Exit Sub
            End If

            If objND.isExist(CInt(Session("Employeeunkid")), CInt(Session("Fin_year"))) = False Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, Declaration for the current financial year is not done yet."), Me)
                Exit Sub
            End If

            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            If IsAllWitnessOnLeave() = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Today all witness are on leave so you can't preview this today."), Me)
                Exit Sub
            End If
            'Gajanan [13-June-2020] -- End

            objEND.SetDefaultValue()

            objEND._EmployeeId = CInt(Session("Employeeunkid"))
            Dim strEmpName As String = Session("E_Firstname").ToString & " " & Session("E_Othername").ToString & " " & Session("E_Surname").ToString
            strEmpName = strEmpName.Replace("  ", " ")
            objEND._EmployeeName = strEmpName

            objEND._ApplyUserAccessFilter = False

            objEND.generateReportNew(Session("Database_Name").ToString, _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                   Session("UserAccessModeSetting").ToString, True, _
                                                   Session("ExportReportPath").ToString, _
                                                   CBool(Session("OpenAfterExport")), _
                                                   0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objEND._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Sohail (18 May 2020) -- End


    'Gajanan [13-June-2020] -- Start
    'Enhancement NMB Employee Declaration Changes.
    Protected Sub btnConfirmSign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmSign.Click
        Try
            pnlSign.Visible = True
            If imgSignature.ImageUrl = Nothing Then
                imgSignature.Visible = False
                lblnosign.Visible = True
            Else
                imgSignature.Visible = True
                lblnosign.Visible = False
            End If
            blnIsPnlSignVisible = True
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "displaypanel", "displaysign();", True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub flUploadsig_btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flUploadsig.btnUpload_Click
        Try
            If flUploadsig.HasFile Then
                Select Case flUploadsig.FileName.Substring(flUploadsig.FileName.LastIndexOf(".") + 1).ToUpper
                    Case "JPG"
                    Case "JPEG"
                    Case "BMP"
                    Case "GIF"
                    Case "PNG"
                    Case Else
                        DisplayMessage.DisplayMessage("Please select proper Image", Me)
                        Exit Sub
                End Select
                Dim mintByes As Integer = 6291456
                If flUploadsig.FileBytes.LongLength <= mintByes Then   ' 6 MB = 6291456 Bytes
                    Dim mstrPath As String = "../images/" & flUploadsig.FileName
                    flUploadsig.SaveAs(Server.MapPath(mstrPath))
                    imgSignature.ImageUrl = mstrPath

                    Dim objEmployee As New clsEmployee_Master
                    Dim empSignature As Byte() = Nothing

                    'If imgSignature.ImageUrl.Trim <> "" AndAlso imgSignature.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                    '    empSignature = ImageCompression(Server.MapPath(imgSignature.ImageUrl))
                    'ElseIf imgSignature.ImageUrl.Trim = "" Then
                    '    empSignature = Nothing
                    'End If

                    'Dim blnflag As Boolean = objEmployee.UpdateSignature(CInt(Session("Employeeunkid")), empSignature, Nothing)


                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                    If imgSignature.ImageUrl.Trim <> "" AndAlso imgSignature.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                        objEmployee._EmpSignature = ImageCompression(Server.MapPath(imgSignature.ImageUrl))
                    ElseIf imgSignature.ImageUrl.Trim = "" Then
                        objEmployee._EmpSignature = Nothing
                    End If

                    Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("IsArutiDemo")), _
                                            CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                            CInt(Session("NoOfEmployees")), _
                                            CInt(Session("UserId")), False, _
                                            CStr(Session("UserAccessModeSetting")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), _
                                            CBool(Session("AllowToApproveEarningDeduction")), _
                                            ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                            CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                            CStr(Session("EmployeeAsOnDate")), , _
                            CBool(Session("IsImgInDataBase")))


                    If blnflag = False Then
                        DisplayMessage.DisplayMessage(objEmployee._Message, Me)
                    Else
                        lblnosign.Visible = False
                        imgSignature.Visible = True
                    End If

                Else
                    DisplayMessage.DisplayMessage("Sorry,You cannot upload file greater than " & (mintByes / 1024) / 1024 & " MB.", Me)
                End If
            Else
                DisplayMessage.DisplayMessage("Please Select the Employee Signature.", Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Gajanan [13-June-2020] -- End

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(lblPageHeader.ID, Me.Title)
            Language._Object.setCaption(Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Language._Object.setCaption(Me.lblDeclarationTitle.ID, Me.lblDeclarationTitle.Text)
            Language._Object.setCaption(Me.lblDeclaredBy.ID, Me.lblDeclaredBy.Text)
            Language._Object.setCaption(Me.lblEmpDeclareDate.ID, Me.lblEmpDeclareDate.Text)
            Language._Object.setCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            'Sohail (18 May 2020) -- Start
            'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
            'Language._Object.setCaption(Me.lblWitnessBy.ID, Me.lblWitnessBy.Text)
            'Language._Object.setCaption(Me.lblWitness1Date.ID, Me.lblWitness1Date.Text)
            'Language._Object.setCaption(Me.lblWitness2Date.ID, Me.lblWitness2Date.Text)
            'Language._Object.setCaption(Me.lblWitness1Name.ID, Me.lblWitness1Name.Text)
            'Language._Object.setCaption(Me.lblWitness2Name.ID, Me.lblWitness2Name.Text)
            'Sohail (18 May 2020) -- End
            Language._Object.setCaption(Me.btnAcknowledge.ID, Me.btnAcknowledge.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(Me.btnPreview.ID, Me.btnPreview.Text)
            Language._Object.setCaption(Me.btnConfirmSign.ID, Me.btnConfirmSign.Text)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, Me.Title)
            lblDetailHeader.Text = Language._Object.getCaption(Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            lblDeclarationTitle.Text = Language._Object.getCaption(Me.lblDeclarationTitle.ID, Me.lblDeclarationTitle.Text)
            lblDeclaredBy.Text = Language._Object.getCaption(Me.lblDeclaredBy.ID, Me.lblDeclaredBy.Text)
            lblEmpDeclareDate.Text = Language._Object.getCaption(Me.lblEmpDeclareDate.ID, Me.lblEmpDeclareDate.Text)
            lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            'Sohail (18 May 2020) -- Start
            'NMB Enhancement # : New report to preview Employee Non-Disclosure Declaration form in ESS and MSS.
            'lblWitnessBy.Text = Language._Object.getCaption(Me.lblWitnessBy.ID, Me.lblWitnessBy.Text)
            'lblWitness1Date.Text = Language._Object.getCaption(Me.lblWitness1Date.ID, Me.lblWitness1Date.Text)
            'lblWitness2Date.Text = Language._Object.getCaption(Me.lblWitness2Date.ID, Me.lblWitness2Date.Text)
            'lblWitness1Name.Text = Language._Object.getCaption(Me.lblWitness1Name.ID, Me.lblWitness1Name.Text)
            'lblWitness2Name.Text = Language._Object.getCaption(Me.lblWitness2Name.ID, Me.lblWitness2Name.Text)
            'Sohail (18 May 2020) -- End
            btnAcknowledge.Text = Language._Object.getCaption(Me.btnAcknowledge.ID, Me.btnAcknowledge.Text)
            btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text)
            btnConfirmSign.Text = Language._Object.getCaption(Me.btnConfirmSign.ID, Me.btnConfirmSign.Text)
            btnPreview.Text = Language._Object.getCaption(Me.btnPreview.ID, Me.btnPreview.Text)
            popupYesNo.Title = Language.getMessage(mstrModuleName, 7, "Are you sure you want to continue to make an acknowledgement?")
            popupYesNo.Message = Language.getMessage(mstrModuleName, 8, "You are about to make an acknowledgement to the confidentiality clause. Are you sure you want to continue?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Employee information not found.")
            Language.setMessage(mstrModuleName, 2, "Sorry, no witness is mapped for first witness. Please contact administrator.")
            Language.setMessage(mstrModuleName, 3, "Sorry, no witness is mapped for second witness. Please contact administrator.")
            Language.setMessage(mstrModuleName, 4, "Sorry, You cannot do declaration before")
            Language.setMessage(mstrModuleName, 5, "Sorry, Declaration for the current financial year is already done.")
            Language.setMessage(mstrModuleName, 6, "Sorry, You cannot do declaration now. Last date for declaration was")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to continue to make an acknowledgement?")
            Language.setMessage(mstrModuleName, 8, "You are about to make an acknowledgement to the confidentiality clause. Are you sure you want to continue?")
            Language.setMessage(mstrModuleName, 9, "Declaration Submitted Successfully.")
            Language.setMessage(mstrModuleName, 10, "Sorry, Declaration for the current financial year is not done yet.")
            Language.setMessage(mstrModuleName, 11, "Sorry, You cannot do declaration now. Please set your signature in the system.")
            Language.setMessage(mstrModuleName, 12, "Sorry, Please confirm your signature first.")
            Language.setMessage(mstrModuleName, 13, "Sorry, Today all witness are on leave so you can't acknowledge today.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
