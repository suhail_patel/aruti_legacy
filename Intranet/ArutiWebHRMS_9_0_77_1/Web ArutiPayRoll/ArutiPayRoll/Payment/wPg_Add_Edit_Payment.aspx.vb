﻿'************************************************************************************************************************************
'Purpose     : Recode due to complexity
'Date        : 11/Sep/2015
'Modified By : Nilay Mistry
'************************************************************************************************************************************

Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class Payroll_wPg_Add_Edit_Payment
    Inherits Basepage

#Region "Private Variables"

#Region "Comman Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmPayment_AddEdit"
    Private objPaymentTran As New clsPayment_tran
    Private objMaster As New clsMasterData
    Private objEmployee As New clsEmployee_Master
    Private objCommon As New clscommom_period_Tran
    Private DisplayMessage As New CommonCodes

    Private mintTransactionId As Integer = -1
    Private mintReferenceId As Integer = -1
    Private mintPaymentTypeId As Integer = -1
    Private mintPaymentTranunkid As Integer = -1
    Private mintRefSelectedIndex As Integer = -1
    Private mintBaseCountryId As Integer = -1

    '-----For base currency Balance Amount-----
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = -1
    Private mintPaidCurrId As Integer = -1
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mdecBaseCurrBalanceAmt As Decimal = 0
    Private mdecPaidCurrBalanceAmt As Decimal = 0
    Private mdecBalanceDueTag As Decimal = 0
    '--------------------------------------------

    Private mdecLastProjectedBalance As Decimal = 0
    '----------------- Savings ----------------
    Private mdecBalanceAmount As Decimal = 0
    '------------------------------------------
    Private mdecPaidCurrBalFormatedAmt As Decimal = 0

    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing

    Private mdtLoanSavingEffectiveDate As Date

#End Region

#Region "Loan/Advance"
    Private objLoan_Advance As clsLoan_Advance
    Private objLoan_Master As clsLoan_Scheme
    Private mstrLoanStatusData As String = String.Empty
    Private mblnIsFromStatus As Boolean = False
    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private mintEmployeeunkid As Integer = 0
    'Nilay (01-Apr-2016) -- End

#End Region

#Region "Savings"
    Private objEmployee_Saving As clsSaving_Tran
    Private objSaving_Master As clsSavingScheme
    Private objSavingStatus As clsSaving_Status_Tran
    Private mintPaymentListCount As Integer = 0
#End Region

#Region "PaySlip"
    Private objPayslip As clsTnALeaveTran
#End Region

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            Call SetLanguage()

            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objPaymentTran._WebFormName = "frmPayment_AddEdit"
            'objPaymentTran._WebIP = CStr(Session("IP_ADD"))
            'objPaymentTran._WebHostName = CStr(Session("HOST_NAME"))
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    objPaymentTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'End If

            If IsPostBack = False Then

                If Session("LoanStatusData") IsNot Nothing Then
                    mstrLoanStatusData = CStr(Session("LoanStatusData"))
                End If

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If Request.QueryString.Count > 0 Then
                '    Dim array() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                '    If array.Length > 0 Then
                '        mintPaymentTranunkid = CInt(array(0))
                '        mintReferenceId = CInt(array(1))
                '        mintTransactionId = CInt(array(2))
                '        mintPaymentTypeId = CInt(array(3))
                '        mdtLoanSavingEffectiveDate = CDate(array(4))
                '        If mintPaymentTranunkid > 0 Then
                '            mintRefSelectedIndex = CInt(array(5))
                '        Else
                '            mintPaymentListCount = CInt(array(5))
                '        End If
                '        If array.Length > 6 Then
                '            'If CStr(array(6)) <> Nothing Then
                '            '    mstrLoanStatusData = CStr(array(6))
                '            'End If
                '            mblnIsFromStatus = CBool(array(6))
                '        End If
                '    End If
                'End If
                If Session("PaymentTranunkid") IsNot Nothing Then
                    mintPaymentTranunkid = CInt(Session("PaymentTranunkid"))
                    Session("PaymentTranunkid") = Nothing
                End If
                If Session("ReferenceId") IsNot Nothing Then
                    mintReferenceId = CInt(Session("ReferenceId"))
                    'Session("ReferenceId") = Nothing
                End If
                If Session("TransactionId") IsNot Nothing Then
                    mintTransactionId = CInt(Session("TransactionId"))
                    Session("TransactionId") = Nothing
                End If
                If Session("PaymentTypeId") IsNot Nothing Then
                    mintPaymentTypeId = CInt(Session("PaymentTypeId"))
                    Session("PaymentTypeId") = Nothing
                End If
                If Session("LoanSavingEffectiveDate") IsNot Nothing Then
                    mdtLoanSavingEffectiveDate = CDate(Session("LoanSavingEffectiveDate"))
                    Session("LoanSavingEffectiveDate") = Nothing
                End If
                        If mintPaymentTranunkid > 0 Then
                    If Session("RefSelectedIndex") IsNot Nothing Then
                        mintRefSelectedIndex = CInt(Session("RefSelectedIndex"))
                        Session("RefSelectedIndex") = Nothing
                    End If
                        Else
                    If Session("PaymentListCount") IsNot Nothing Then
                        mintPaymentListCount = CInt(Session("PaymentListCount"))
                        Session("PaymentListCount") = Nothing
                        End If
                    End If
                If Session("IsFromStatus") IsNot Nothing Then
                    mblnIsFromStatus = CBool(Session("IsFromStatus"))
                    'Session("IsFromStatus") = Nothing
                End If
                'Nilay (06-Aug-2016) -- END

                'Nilay (25-Mar-2016) -- Start
                mdtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (25-Mar-2016) -- End

                Call FillCombo()
                Call SetVisbibility()
                Call FillInfo()

                Call cboCurrency_SelectedIndexChanged(Nothing, Nothing)

                If mdecBaseExRate <> 0 Then
                    mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
                Else
                    mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
                End If

                mdecPaidCurrBalFormatedAmt = CDec(Format(mdecPaidCurrBalanceAmt, Session("fmtCurrency").ToString))
                If mintPaymentTranunkid <= 0 Then
                    txtAmount.Text = Format(mdecPaidCurrBalFormatedAmt, Session("fmtCurrency").ToString)
                End If

                Call GetValue()

            Else

                mintPaymentTranunkid = CInt(Me.ViewState("Paymenttranunkid"))
                mintReferenceId = CInt((Me.ViewState("ReferenceId")))
                mintTransactionId = CInt(Me.ViewState("TransactionId"))
                mintPaymentTypeId = CInt(Me.ViewState("PaymentTypeId"))
                mintPaymentListCount = CInt(Me.ViewState("PaymentListCount"))
                mintRefSelectedIndex = CInt(Me.ViewState("RefSelectedIndex"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                '-----For base currency Balance Amount-----
                mstrBaseCurrSign = CStr(Me.ViewState("BaseCurrSign"))
                mintBaseCurrId = CInt(Me.ViewState("BaseCurrId"))
                mintPaidCurrId = CInt(Me.ViewState("PaidCurrId"))
                mdecBaseExRate = CDec(Me.ViewState("BaseExRate"))
                mdecPaidExRate = CDec(Me.ViewState("PaidExRate"))
                mdecBaseCurrBalanceAmt = CDec(Me.ViewState("BaseCurrBalanceAmt"))
                mdecPaidCurrBalanceAmt = CDec(Me.ViewState("PaidCurrBalanceAmt"))
                mdecBalanceDueTag = CDec(Me.ViewState("BalanceDueTag"))
                '-------------------------------------------------
                mdecLastProjectedBalance = CDec(Me.ViewState("LastProjectedBalance"))
                '-------------------- Savings --------------------
                mdecBalanceAmount = CDec(Me.ViewState("BalanceAmount"))
                mdtLoanSavingEffectiveDate = CDate(Me.ViewState("LoanSavingEffectiveDate"))
                '-------------------------------------------------
                mdecPaidCurrBalFormatedAmt = CDec(Me.ViewState("PaidCurrBalFormatedAmt"))
                mstrLoanStatusData = CStr(Me.ViewState("LoanStatusData"))
                mblnIsFromStatus = CBool(Me.ViewState("IsFromStatus"))
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                mintEmployeeunkid = CInt(Me.ViewState("EmployeeId"))
                'Nilay (01-Apr-2016) -- End
                'Sohail (24 Jun 2017) -- Start
                'Issue - 68.1 - There is no row at position 0.
                mdtPeriodStartDate = CDate(Me.ViewState("mdtPeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("mdtPeriodEndDate"))
                'Sohail (24 Jun 2017) -- End

                Call SetVisbibility()

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("Paymenttranunkid") = mintPaymentTranunkid
            Me.ViewState("ReferenceId") = mintReferenceId
            Me.ViewState("TransactionId") = mintTransactionId
            Me.ViewState("PaymentTypeId") = mintPaymentTypeId
            Me.ViewState("PaymentListCount") = mintPaymentListCount
            Me.ViewState("RefSelectedIndex") = mintRefSelectedIndex
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            '----------For base currency Balance Amount----------
            Me.ViewState("BaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("BaseCurrId") = mintBaseCurrId
            Me.ViewState("PaidCurrId") = mintPaidCurrId
            Me.ViewState("BaseExRate") = mdecBaseExRate
            Me.ViewState("PaidExRate") = mdecPaidExRate
            Me.ViewState("BaseCurrBalanceAmt") = mdecBaseCurrBalanceAmt
            Me.ViewState("PaidCurrBalanceAmt") = mdecPaidCurrBalanceAmt
            Me.ViewState("BalanceDueTag") = mdecBalanceDueTag
            '-----------------------------------------------------
            Me.ViewState("LastProjectedBalance") = mdecLastProjectedBalance
            '----------------------- Savings ---------------------
            Me.ViewState("BalanceAmount") = mdecBalanceAmount
            Me.ViewState("LoanSavingEffectiveDate") = mdtLoanSavingEffectiveDate
            '-----------------------------------------------------
            Me.ViewState("PaidCurrBalFormatedAmt") = mdecPaidCurrBalFormatedAmt
            Me.ViewState("LoanStatusData") = mstrLoanStatusData
            Me.ViewState("IsFromStatus") = mblnIsFromStatus
            'Nilay (01-Apr-2016) -- Start
            'ENHANCEMENT - Approval Process in Loan Other Operations
            Me.ViewState("EmployeeId") = mintEmployeeunkid
            'Nilay (01-Apr-2016) -- End
            'Sohail (24 Jun 2017) -- Start
            'Issue - 68.1 - There is no row at position 0.
            Me.ViewState("mdtPeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("mdtPeriodEndDate") = mdtPeriodEndDate
            'Sohail (24 Jun 2017) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreInit:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()

        Dim dsList As New DataSet
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim objExRate As New clsExchangeRate
        Dim objCommon As New clscommom_period_Tran

        Try
            dsList = objMaster.GetPaymentMode("PayMode")
            With cboPaymentMode
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("PayMode")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboPaymentMode_SelectedIndexChanged(cboPaymentMode, Nothing)

            dsList = objMaster.GetPaymentBy("PayBy")
            With cboPaymentBy
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("PayBy")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboPaymentBy_SelectedIndexChanged(cboPaymentBy, Nothing)

            dsList = objCompanyBank.GetComboList(CInt(Session("Companyunkid")), 1, "BankGrp")
            With cboBankGroup
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("BankGrp")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboBankGroup_SelectedIndexChanged(cboBankGroup, Nothing)

            dsList = objExRate.getComboList("ExRate", False)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                Else
                    mintBaseCountryId = 0
                End If

                With cboCurrency
                    .DataValueField = "countryunkid"
                    .DataTextField = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    .DataBind()
                    If mintBaseCountryId > 0 Then
                        .SelectedValue = mintBaseCountryId.ToString
                    Else
                        .SelectedValue = "0"
                    End If
                End With

                Dim dtPeriod As New DataTable
                dsList = objCommon.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), _
                                                   Session("Database_Name").ToString, CDate(Session("fin_startdate").ToString), _
                                                   "Period", True, 1)

                If mintPaymentTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                    dtPeriod = New DataView(dsList.Tables("Period")).ToTable
                Else
                    Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
                    If intFirstPeriodId > 0 Then
                        dtPeriod = New DataView(dsList.Tables("Period"), "periodunkid=" & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtPeriod = New DataView(dsList.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
                    End If
                End If

                With cboPeriod
                    .DataValueField = "periodunkid"
                    .DataTextField = "name"
                    .DataSource = dtPeriod
                    .DataBind()
                    If .Items.Count > 0 Then .SelectedIndex = 0
                End With

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillInfo()

        Dim decReceived As Decimal = 0
        Try

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            If mintTransactionId <= 0 Then Exit Sub
            'Nilay (06-Aug-2016) -- END

            If mintPaymentTranunkid > 0 Then
                objPaymentTran._Paymenttranunkid = mintPaymentTranunkid
                cboPaymentMode.Enabled = False
                cboPaymentBy.Enabled = False
                cboCurrency.Enabled = False
                'dtpPaymentDate.MaxDate = objPaymentTran._Paymentdate
                'dtpPaymentDate.MinDate = objPaymentTran._Paymentdate
            Else
                dtpPaymentDate.SetDate = CDate(Session("fin_enddate")).Date
            End If

            mdecBaseCurrBalanceAmt = mdecBalanceDueTag
            objlblMessage.Text = ""

            'dtpPaymentDate.Enabled = True

            Select Case mintReferenceId

                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

                    objLoan_Advance = New clsLoan_Advance
                    objLoan_Master = New clsLoan_Scheme
                    objLoan_Advance._Loanadvancetranunkid = mintTransactionId
                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    mintEmployeeunkid = objLoan_Advance._Employeeunkid
                    'Nilay (01-Apr-2016) -- End
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLoan_Advance._Employeeunkid
                    'Sohail (12 Mar 2018) -- Start
                    'TRA - issue #0001938: Loan balance issue still continues (written off loan) TRA Issue - 70.2 - entry was not getting inserted in loan balance tran table for the employees which were not active on loan start period date due to active employee concept changes and loan start period is assigned in 70.2.
                    'objCommon._Periodunkid(Session("Database_Name").ToString) = objLoan_Advance._Periodunkid
                    If mstrLoanStatusData.Trim.Length > 0 Then
                        Dim StrStatus() As String = mstrLoanStatusData.Split(CChar("|"))
                        objCommon._Periodunkid(Session("Database_Name").ToString) = CInt(StrStatus(8))
                    Else
                    objCommon._Periodunkid(Session("Database_Name").ToString) = objLoan_Advance._Periodunkid
                    End If
                    mdtPeriodStartDate = objCommon._Start_Date
                    mdtPeriodEndDate = objCommon._End_Date
                    'Sohail (12 Mar 2018) -- End
                    objLoan_Master._Loanschemeunkid = objLoan_Advance._Loanschemeunkid

                    txtPaymentOf.Text = CStr(IIf(objLoan_Advance._Isloan = True, Language.getMessage("frmLoanAdvanceList", 2, "Loan"), _
                                                                                 Language.getMessage("frmLoanAdvanceList", 3, "Advance")))
                    txtVoucherNo.Text = objLoan_Advance._Loanvoucher_No
                    ''Sohail (24 Jun 2017) -- Start
                    'Issue - 68.1 - There is no row at position 0.
                    'txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), CInt(Session("CompanyUnkId")))
                    Try
                    txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), CInt(Session("CompanyUnkId")))
                    Catch ex As Exception
                        'Do Nothing to ignore error if closed year period database does not exist
                    End Try
                    'Sohail (24 Jun 2017) -- End
                    txtPayPeriod.Text = objCommon._Period_Name

                    If mintPaymentTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then

                        txtBalanceDue.Text = Format(objLoan_Advance._Basecurrency_amount, Session("fmtCurrency").ToString)
                        mdecBalanceDueTag = objLoan_Advance._Basecurrency_amount

                        If mintPaymentTranunkid > 0 Then

                            objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTypeId, decReceived, , True)

                            If mintRefSelectedIndex = 0 Then
                                decReceived = 0
                                txtBalanceDue.Text = CStr(CDec(txtBalanceDue.Text) - decReceived)
                                mdecBalanceDueTag = CDec(txtBalanceDue.Text)
                            Else
                                txtBalanceDue.Text = CStr((CDec(txtBalanceDue.Text) + decReceived) - decReceived)
                                mdecBalanceDueTag = CDec(txtBalanceDue.Text) + decReceived
                            End If

                        Else
                            objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTypeId, decReceived)
                            txtBalanceDue.Text = CStr(CDec(txtBalanceDue.Text) - decReceived)
                            mdecBalanceDueTag = (CDec(txtBalanceDue.Text) + decReceived) - decReceived
                        End If

                    ElseIf mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then

                        'Sohail (14 Mar 2017) -- Start
                        'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                        'If objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.MONTHLY Then
                        'Sohail (23 May 2017) -- Start
                        'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                        'If objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.MONTHLY OrElse objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.BY_TENURE Then
                        If objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.MONTHLY OrElse objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.BY_TENURE OrElse objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.SIMPLE_INTEREST Then
                            'Sohail (23 May 2017) -- End
                            'Sohail (14 Mar 2017) -- End
                            dtpPaymentDate.Enabled = False
                        End If

                        If mintReferenceId = clsPayment_tran.enPaymentRefId.ADVANCE Then
                            txtBalanceDue.Text = Format(objLoan_Advance._Balance_Amount, Session("fmtCurrency").ToString)
                            mdecBalanceDueTag = CDec(objLoan_Advance._Balance_Amount)
                        Else
                            Dim objLoan As New clsLoan_Advance
                            'Nilay (25-Mar-2016) -- Start
                            'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo("List", dtpPaymentDate.GetDate.Date, "", mintTransactionId)
                            Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                      mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, _
                                                                                      "List", dtpPaymentDate.GetDate.Date, "", mintTransactionId)
                            'Nilay (25-Mar-2016) -- End

                            If dsLoan.Tables("List").Rows.Count > 0 Then
                                If dsLoan.Tables("List").Rows.Count > 0 Then
                                    txtBalanceDue.Text = Format(CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance")), Session("fmtCurrency").ToString)
                                    mdecBalanceDueTag = CDec(Format(CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance")), Session("fmtCurrency").ToString))
                                End If
                            End If
                        End If

                        If mintPaymentTranunkid > 0 Then
                            objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTypeId, decReceived, True, True)

                            If mintRefSelectedIndex = 0 Then
                                decReceived = 0
                                txtBalanceDue.Text = CStr(CDec(txtBalanceDue.Text) - decReceived)
                                mdecBalanceDueTag = CDec(txtBalanceDue.Text) - decReceived
                            Else
                                txtBalanceDue.Text = CStr(CDec(txtBalanceDue.Text) + decReceived)
                                mdecBalanceDueTag = CDec(txtBalanceDue.Text) + decReceived
                            End If
                        Else
                            objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTypeId, decReceived, True)
                            txtBalanceDue.Text = CStr((CDec(txtBalanceDue.Text) + decReceived) - decReceived)
                            mdecBalanceDueTag = (CDec(txtBalanceDue.Text) + decReceived) - decReceived
                        End If

                    End If
                    txtScheme.Text = CStr(IIf(objLoan_Advance._Isloan = True, objLoan_Master._Name, ""))

                Case clsPayment_tran.enPaymentRefId.PAYSLIP

                    objPayslip = New clsTnALeaveTran
                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'objPayslip._Tnaleavetranunkid = mintTransactionId
                    objPayslip._Tnaleavetranunkid(Nothing) = mintTransactionId
                    'Sohail (02 Jan 2017) -- End
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objPayslip._Employeeunkid
                    objCommon._Periodunkid(Session("Database_Name").ToString) = objPayslip._Payperiodunkid
                    'Sohail (12 Mar 2018) -- Start
                    'TRA - issue #0001938: Loan balance issue still continues (written off loan) TRA Issue - 70.2 - entry was not getting inserted in loan balance tran table for the employees which were not active on loan start period date due to active employee concept changes and loan start period is assigned in 70.2.
                    mdtPeriodStartDate = objCommon._Start_Date
                    mdtPeriodEndDate = objCommon._End_Date
                    'Sohail (12 Mar 2018) -- End
                    lblPayment.Visible = True
                    cboPaymentBy.SelectedValue = "1"
                    'Sohail (21 Apr 2020) -- Start
                    'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                    'cboPaymentBy.Enabled = False
                    'txtPercent.Enabled = False
                    'txtAmount.Read_Only = True
                    cboPaymentBy.Enabled = True
                    txtPercent.Enabled = True
                    txtAmount.Read_Only = False
                    'Sohail (21 Apr 2020) -- End

                    txtPaymentOf.Text = CStr(Language.getMessage("frmPayslipList", 4, "Payslip"))
                    txtVoucherNo.Text = objPayslip._Voucherno
                    txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), CInt(Session("CompanyUnkId")))
                    txtPayPeriod.Text = objCommon._Period_Name
                    txtBalanceDue.Text = objPayslip._Balanceamount.ToString(Session("fmtCurrency").ToString)
                    mdecBalanceDueTag = objPayslip._Balanceamount
                    txtAmount.Text = txtBalanceDue.Text

                    If mintPaymentTranunkid > 0 Then
                        objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTypeId, decReceived, True, True)

                        If mintRefSelectedIndex = 0 Then
                            decReceived = 0
                            txtBalanceDue.Text = CStr(CDec(txtBalanceDue.Text) - decReceived)
                            mdecBalanceDueTag = CDec(txtBalanceDue.Text) - decReceived
                        Else
                            txtBalanceDue.Text = CStr(CDec(txtBalanceDue.Text) - decReceived)
                            mdecBalanceDueTag = CDec(txtBalanceDue.Text) - decReceived
                        End If
                    End If

                    'Sohail (17 Mar 2020) -- Start
                    'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                    'Dim decRondingMultile As Decimal = 0

                    'Select Case CInt(Session("PaymentRoundingMultiple"))
                    '    Case enPaymentRoundingMultiple._1
                    '        decRondingMultile = 1
                    '    Case enPaymentRoundingMultiple._5
                    '        decRondingMultile = 5
                    '    Case enPaymentRoundingMultiple._10
                    '        decRondingMultile = 10
                    '    Case enPaymentRoundingMultiple._50
                    '        decRondingMultile = 50
                    '    Case enPaymentRoundingMultiple._100
                    '        decRondingMultile = 100
                    '    Case enPaymentRoundingMultiple._500
                    '        decRondingMultile = 500
                    '    Case enPaymentRoundingMultiple._1000
                    '        decRondingMultile = 1000
                    'End Select

                    'Select Case CInt(Session("PaymentRoundingType"))

                    '    Case enPaymentRoundingType.NONE
                    '        mdecBaseCurrBalanceAmt = mdecBalanceDueTag
                    '        objlblMessage.Text = ""

                    '    Case enPaymentRoundingType.AUTOMATIC

                    '        If decRondingMultile / 2 > mdecBalanceDueTag Mod decRondingMultile Then
                    '            mdecBaseCurrBalanceAmt = mdecBalanceDueTag - CDec(mdecBalanceDueTag Mod decRondingMultile)
                    '        Else
                    '            If CDec(mdecBalanceDueTag Mod decRondingMultile) <> 0 Then
                    '                mdecBaseCurrBalanceAmt = mdecBalanceDueTag - CDec(mdecBalanceDueTag Mod decRondingMultile) + decRondingMultile
                    '            End If
                    '        End If
                    '        objlblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString

                    '    Case enPaymentRoundingType.UP

                    '        If CDec(mdecBalanceDueTag Mod decRondingMultile) <> 0 Then
                    '            mdecBaseCurrBalanceAmt = mdecBalanceDueTag - CDec(mdecBalanceDueTag Mod decRondingMultile) + decRondingMultile
                    '        End If
                    '        objlblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString

                    '    Case enPaymentRoundingType.DOWN

                    '        mdecBaseCurrBalanceAmt = mdecBalanceDueTag - CDec(mdecBalanceDueTag Mod decRondingMultile)
                    '        objlblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString

                    'End Select
                    'Sohail (17 Mar 2020) -- End

                Case clsPayment_tran.enPaymentRefId.SAVINGS

                    objEmployee_Saving = New clsSaving_Tran
                    objSaving_Master = New clsSavingScheme
                    objSavingStatus = New clsSaving_Status_Tran

                    objEmployee_Saving._Savingtranunkid = mintTransactionId
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objEmployee_Saving._Employeeunkid
                    objCommon._Periodunkid(Session("Database_Name").ToString) = objEmployee_Saving._Payperiodunkid
                    'Sohail (12 Mar 2018) -- Start
                    'TRA - issue #0001938: Loan balance issue still continues (written off loan) TRA Issue - 70.2 - entry was not getting inserted in loan balance tran table for the employees which were not active on loan start period date due to active employee concept changes and loan start period is assigned in 70.2.
                    mdtPeriodStartDate = objCommon._Start_Date
                    mdtPeriodEndDate = objCommon._End_Date
                    'Sohail (12 Mar 2018) -- End
                    objSaving_Master._Savingschemeunkid = objEmployee_Saving._Savingschemeunkid
                    If mintPaymentTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then
                        mdecBalanceAmount = 0
                    Else
                        mdecBalanceAmount = CDec(objEmployee_Saving._Balance_Amount)
                    End If
                    txtPaymentOf.Text = "Savings"
                    txtVoucherNo.Text = objEmployee_Saving._Voucherno
                    txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), CInt(Session("CompanyUnkId")))
                    txtPayPeriod.Text = objCommon._Period_Name
                    txtScheme.Text = objSaving_Master._Savingschemename

                    If mintPaymentTypeId = clsPayment_tran.enPayTypeId.REPAYMENT OrElse _
                       mintPaymentTypeId = clsPayment_tran.enPayTypeId.WITHDRAWAL OrElse _
                       mintPaymentTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then

                        If mintPaymentTranunkid > 0 Then
                            objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTranunkid, decReceived, False, True)
                            txtBalanceDue.Text = Format(mdecBalanceAmount + decReceived, Session("fmtCurrency").ToString)
                            mdecBalanceDueTag = CDec(mdecBalanceAmount + decReceived)
                        Else
                            objPaymentTran.Get_Payment_Total(mintTransactionId, mintReferenceId, mintPaymentTypeId, decReceived)
                            txtBalanceDue.Text = Format((mdecBalanceAmount + decReceived) - decReceived, Session("fmtCurrency").ToString)
                            mdecBalanceDueTag = CDec((mdecBalanceAmount + decReceived) - decReceived)
                        End If

                    End If

            End Select

            mdtPeriodStartDate = objCommon._Start_Date
            mdtPeriodEndDate = objCommon._End_Date

            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            mdecBaseCurrBalanceAmt = mdecBalanceDueTag
            objlblMessage.Text = ""

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillInfo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()

        Dim objBankGrp As New clspayrollgroup_master
        Dim objBranch As New clsbankbranch_master
        Dim objCommon As New clscommom_period_Tran
        Try
            cboCurrency.SelectedValue = CStr(objPaymentTran._Countryunkid)

            If mintReferenceId <> clsPayment_tran.enPaymentRefId.PAYSLIP Then
                txtPercent.Text = CStr(objPaymentTran._Percentage)
                txtAmount.Text = Format(objPaymentTran._Expaidamt, Session("fmtCurrency").ToString)
                cboPaymentBy.SelectedValue = CStr(objPaymentTran._Paymentbyid)
            End If

            cboBranch.SelectedValue = CStr(objPaymentTran._Branchunkid)
            objBankGrp._Groupmasterunkid = CInt(objBranch._Bankgroupunkid)
            objBranch._Branchunkid = CInt(objPaymentTran._Branchunkid)
            cboBankGroup.SelectedValue = CStr(objBankGrp._Groupmasterunkid)
            objBankGrp = Nothing
            objBranch = Nothing

            txtChequeNo.Text = objPaymentTran._Chequeno
            cboAccountNo.Text = objPaymentTran._Accountno

            Select Case mintReferenceId
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    objPaymentTran._Employeeunkid = objPaymentTran._Employeeunkid
                Case clsPayment_tran.enPaymentRefId.PAYSLIP

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    objPaymentTran._Employeeunkid = objPaymentTran._Employeeunkid
            End Select

            objPaymentTran._Isvoid = objPaymentTran._Isvoid
            objPaymentTran._Is_Receipt = objPaymentTran._Is_Receipt

            If objPaymentTran._Paymentdate = Nothing Then
                dtpPaymentDate.SetDate = CDate(Session("fin_enddate")).Date
            Else
                dtpPaymentDate.SetDate = objPaymentTran._Paymentdate
            End If

            cboPaymentMode.SelectedValue = CStr(objPaymentTran._Paymentmodeid)

            If mintPaymentTranunkid > 0 Then
                mintReferenceId = objPaymentTran._Paymentrefid
                mintTransactionId = objPaymentTran._Payreftranunkid
                mintPaymentTypeId = objPaymentTran._PaymentTypeId
            Else
                objPaymentTran._Paymentrefid = objPaymentTran._Paymentrefid
                objPaymentTran._Payreftranunkid = objPaymentTran._Payreftranunkid
                objPaymentTran._PaymentTypeId = objPaymentTran._PaymentTypeId
            End If

            mintPaymentTranunkid = objPaymentTran._Paymenttranunkid

            Select Case mintReferenceId
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    If mintPaymentTranunkid <= 0 Then
                        objLoan_Advance._Periodunkid = objPaymentTran._Periodunkid
                    Else
                        objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
                    End If
                Case clsPayment_tran.enPaymentRefId.PAYSLIP

                    If mintPaymentTranunkid <= 0 Then
                        objPayslip._Payperiodunkid = objPaymentTran._Periodunkid
                    Else
                        objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
                    End If

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    If mintPaymentTranunkid <= 0 Then
                        objEmployee_Saving._Payperiodunkid = objPaymentTran._Periodunkid
                    Else
                        objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
                    End If

            End Select

            objPaymentTran._Userunkid = objPaymentTran._Userunkid
            objPaymentTran._Voiddatetime = objPaymentTran._Voiddatetime
            objPaymentTran._Voiduserunkid = objPaymentTran._Voiduserunkid
            txtAgainstVoucher.Text = objPaymentTran._Voucherno

            If objPaymentTran._Voucherref <> "" Then
                txtVoucherNo.Text = objPaymentTran._Voucherref
            End If
            txtRemarks.Text = objPaymentTran._Remarks

            objCommon._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If mintPaymentTranunkid <= 0 Then

                Select Case mintReferenceId
                    Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                        If CInt(cboPeriod.SelectedValue) > 0 Then
                            dtpPaymentDate.SetDate = CDate(objCommon._Start_Date).Date
                        End If
                    Case clsPayment_tran.enPaymentRefId.PAYSLIP
                        If CInt(cboPeriod.SelectedValue) > 0 Then
                            dtpPaymentDate.SetDate = CDate(objCommon._End_Date).Date
                        End If
                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        If CInt(cboPeriod.SelectedValue) > 0 Then
                            dtpPaymentDate.SetDate = CDate(objCommon._End_Date).Date
                        End If
                End Select

                If mintBaseCountryId > 0 Then
                    cboCurrency.SelectedValue = CStr(mintBaseCountryId)
                Else
                    cboCurrency.SelectedIndex = 0
                End If

                Call cboCurrency_SelectedIndexChanged(cboCurrency, Nothing)

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()

        objLoan_Advance = New clsLoan_Advance
        objLoan_Advance._Loanadvancetranunkid = mintTransactionId
        objPayslip = New clsTnALeaveTran
        'Sohail (02 Jan 2017) -- Start
        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
        'objPayslip._Tnaleavetranunkid = mintTransactionId
        objPayslip._Tnaleavetranunkid(Nothing) = mintTransactionId
        'Sohail (02 Jan 2017) -- End
        objEmployee_Saving = New clsSaving_Tran
        objEmployee_Saving._Savingtranunkid = mintTransactionId
        objCommon._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
        'objPaymentTran._Paymenttranunkid = mintPaymentTranunkid

        Try
            objPaymentTran._Percentage = CDec(IIf(txtPercent.Text.Trim = "", 0, txtPercent.Text))
            objPaymentTran._Branchunkid = CInt(cboBranch.SelectedValue)
            objPaymentTran._Chequeno = txtChequeNo.Text
            If CInt(cboAccountNo.SelectedValue) > 0 Then
                objPaymentTran._Accountno = cboAccountNo.Text
            Else
                objPaymentTran._Accountno = ""
            End If

            Select Case mintReferenceId
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    objPaymentTran._Employeeunkid = objLoan_Advance._Employeeunkid
                    objPaymentTran._Periodunkid = objLoan_Advance._Periodunkid

                Case clsPayment_tran.enPaymentRefId.PAYSLIP
                    objPaymentTran._Employeeunkid = objPayslip._Employeeunkid
                    objPaymentTran._Periodunkid = objPayslip._Payperiodunkid

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    objPaymentTran._Employeeunkid = objEmployee_Saving._Employeeunkid
                    objPaymentTran._Periodunkid = objEmployee_Saving._Payperiodunkid

            End Select

            If mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED OrElse mintPaymentTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then
                objPaymentTran._Is_Receipt = True
            Else
                objPaymentTran._Is_Receipt = False
            End If

            objPaymentTran._Paymentbyid = CInt(cboPaymentBy.SelectedValue)
            objPaymentTran._Paymentdate = dtpPaymentDate.GetDate.Date
            objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
            objPaymentTran._Paymentrefid = mintReferenceId
            objPaymentTran._Payreftranunkid = mintTransactionId
            objPaymentTran._PaymentTypeId = mintPaymentTypeId

            objPaymentTran._Userunkid = CInt(Session("UserId"))

            If mintPaymentTranunkid <= 0 Then
                objPaymentTran._Isvoid = False
                objPaymentTran._Voiddatetime = Nothing
                objPaymentTran._Voiduserunkid = -1
            Else
                objPaymentTran._Paymenttranunkid = mintPaymentTranunkid
                objPaymentTran._Isvoid = objPaymentTran._Isvoid
                objPaymentTran._Voiddatetime = objPaymentTran._Voiddatetime
                objPaymentTran._Voiduserunkid = objPaymentTran._Voiduserunkid
            End If

            objPaymentTran._Voucherno = txtAgainstVoucher.Text
            objPaymentTran._Voucherref = txtVoucherNo.Text

            If mintPaymentListCount > 0 Then
                objPaymentTran._IsInsertStatus = False
            Else
                objPaymentTran._IsInsertStatus = True
            End If

            objPaymentTran._Isglobalpayment = False
            objPaymentTran._strData = mstrLoanStatusData
            objPaymentTran._IsFromStatus = mblnIsFromStatus

            objPaymentTran._Basecurrencyid = mintBaseCurrId
            objPaymentTran._Baseexchangerate = mdecBaseExRate
            objPaymentTran._Paidcurrencyid = mintPaidCurrId
            objPaymentTran._Expaidrate = mdecPaidExRate

            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then 'Sohail (17 Mar 2020)
            If mdecPaidCurrBalFormatedAmt = CDec(Format(CDec(txtAmount.Text), Session("fmtCurrency").ToString)) Then

                If CDec(Session("CFRoundingAbove")) > 0 AndAlso Math.Abs(mdecBalanceDueTag - mdecBaseCurrBalanceAmt) <= CDec(Session("CFRoundingAbove")) Then
                    objPaymentTran._RoundingAdjustment = mdecBalanceDueTag - mdecBaseCurrBalanceAmt '*** Rounding Adjustment
                Else
                    objPaymentTran._RoundingAdjustment = 0 '*** Rounding Adjustment
                End If

                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = mdecPaidCurrBalanceAmt * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = mdecPaidCurrBalanceAmt * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = mdecPaidCurrBalanceAmt
            Else
                If CDec(Session("CFRoundingAbove")) > 0 AndAlso Math.Abs(mdecBalanceDueTag - mdecBaseCurrBalanceAmt) <= CDec(Session("CFRoundingAbove")) Then
                        'Sohail (17 Mar 2020) -- Start
                        'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                        'objPaymentTran._RoundingAdjustment = mdecBalanceDueTag - CDec(txtAmount.Text) '*** Rounding Adjustment
                        objPaymentTran._RoundingAdjustment = mdecBalanceDueTag - mdecBaseCurrBalanceAmt '*** Rounding Adjustment
                        'Sohail (17 Mar 2020) -- End
                Else
                    objPaymentTran._RoundingAdjustment = 0 '*** Rounding Adjustment
                End If

                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = CDec(txtAmount.Text) * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = CDec(txtAmount.Text) * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = CDec(txtAmount.Text)
            End If
                'Sohail (17 Mar 2020) -- Start
                'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
            Else
                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = CDec(txtAmount.Text) * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = CDec(txtAmount.Text) * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = CDec(txtAmount.Text)
            End If
            'Sohail (17 Mar 2020) -- End

            objPaymentTran._Countryunkid = CInt(cboCurrency.SelectedValue)
            objPaymentTran._Isapproved = False

            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                objPaymentTran._Roundingtypeid = CInt(Session("PaymentRoundingType"))
                objPaymentTran._Roundingmultipleid = CInt(Session("PaymentRoundingMultiple"))
            Else
                objPaymentTran._Roundingtypeid = 0
                objPaymentTran._Roundingmultipleid = 0
            End If

            objPaymentTran._Remarks = txtRemarks.Text.Trim
            objPaymentTran._PaymentDate_Periodunkid = CInt(cboPeriod.SelectedValue)

            'Nilay (05-May-2016) -- Start
            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objPaymentTran._WebIP = Session("IP_ADD").ToString
            'objPaymentTran._WebFormName = "frmPayment_AddEdit"
            'objPaymentTran._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetVisbibility()
        Try
            If CInt(Session("_PaymentVocNoType")) = 1 Then
                txtAgainstVoucher.Enabled = False
            Else
                txtAgainstVoucher.Enabled = True
            End If

            Select Case mintPaymentTypeId
                Case clsPayment_tran.enPayTypeId.PAYMENT
                    lblReceivedFrom.Visible = False
                    lblPaidTo.Visible = True
                    lblLoanAmount.Visible = True
                    lblBalanceDue.Visible = False
                    'lblPaidTo.Text = "Paid To"
                    'lblPayment.Text = "Payment"
                Case clsPayment_tran.enPayTypeId.RECEIVED
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 6, "Add / Edit Receipt")
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 7, "Receipt Information")
                    lblReceivedFrom.Visible = True
                    lblPaidTo.Visible = False
                    lblBalanceDue.Visible = True
                    lblLoanAmount.Visible = False
                    'lblPaidTo.Text = "Received From"
                    'lblPayment.Text = "Balance Due"
                    'Shani(18-Dec-2015) -- Start
                    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                    'Case clsPayment_tran.enPayTypeId.WITHDRAWAL
                    'Case clsPayment_tran.enPayTypeId.REPAYMENT
                Case clsPayment_tran.enPayTypeId.WITHDRAWAL, clsPayment_tran.enPayTypeId.REPAYMENT, clsPayment_tran.enPayTypeId.DEPOSIT
                    'Shani(18-Dec-2015) -- End
                    lblLoanAmount.Visible = True
                    lblPayment.Visible = False
                    lblBalanceDue.Visible = False
                    lblLoanAmount.Text = Language.getMessage(mstrModuleName, 5, "Savings")
            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisbibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetVisbibility:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean

        objLoan_Advance = New clsLoan_Advance
        objLoan_Advance._Loanadvancetranunkid = mintTransactionId
        objPayslip = New clsTnALeaveTran
        'Sohail (02 Jan 2017) -- Start
        'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
        'objPayslip._Tnaleavetranunkid = mintTransactionId
        objPayslip._Tnaleavetranunkid(Nothing) = mintTransactionId
        'Sohail (02 Jan 2017) -- End
        objEmployee_Saving = New clsSaving_Tran
        objEmployee_Saving._Savingtranunkid = mintTransactionId
        objCommon._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

        Dim objEmpBank As New clsEmployeeBanks
        Dim dtTable As New DataTable
        Dim dsList As New DataSet
        Try
            If CDec(txtBalanceDue.Text.Trim) <= 0 Or txtBalanceDue.Text.Trim = "" Then
                Select Case mintReferenceId
                    Case clsPayment_tran.enPaymentRefId.LOAN
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot make new payment. Reason : Loan is already paid to particular employee."), Me)
                        Return False
                    Case clsPayment_tran.enPaymentRefId.ADVANCE
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot make new payment. Reason : Due amount is already paid by employee."), Me)
                        Return False
                    Case clsPayment_tran.enPaymentRefId.PAYSLIP
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, you cannot make payment. Reason : Payslip amount is either Zero or Negative."), Me)
                        Return False
                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        If mintPaymentTypeId <> clsPayment_tran.enPayTypeId.DEPOSIT Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Saving Balance is Zero."), Me)
                            Return False
                        End If
                        
                End Select
            End If

            Select Case mintReferenceId

                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

                    If mintPaymentTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                        If CDec(txtAmount.Text) < mdecPaidCurrBalFormatedAmt Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, you cannot do any partial payment. Please do full payment."), Me)
                            Return False
                        End If

                    ElseIf mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then

                        dsList = objLoan_Advance.GetLastLoanBalance("List", mintTransactionId)
                        If dsList.Tables("List").Rows.Count > 0 Then
                            If eZeeDate.convertDate(dsList.Tables("List").Rows(0).Item("end_date").ToString) >= dtpPaymentDate.GetDate.Date Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Payment date should not less than last transactoin date") & " " & _
                                                              Format(eZeeDate.convertDate(dsList.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", Me)
                                Return False

                            End If
                        End If
                        dsList = Nothing

                        'Nilay (01-Apr-2016) -- Start
                        'ENHANCEMENT - Approval Process in Loan Other Operations
                        Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
                        Dim blnFlag As Boolean
                        blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPeriod.SelectedValue), mintEmployeeunkid.ToString, dtpPaymentDate.GetDate)
                        If blnFlag = False Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 39, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current period. In order to Receipt, please either Approve or Reject pending operations for current period."), Me)
                            Return False
                        End If
                        'Nilay (01-Apr-2016) -- End
                    End If

                    If mdtLoanSavingEffectiveDate.ToString <> "" AndAlso dtpPaymentDate.GetDate.Date < mdtLoanSavingEffectiveDate Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Sorry, Date should not be less than Loan/Advance Effective date") & " " & Format(mdtLoanSavingEffectiveDate, "dd-MMM-yyyy") & ".", Me)
                        Return False
                    End If

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    If mdtLoanSavingEffectiveDate.ToString <> "" AndAlso dtpPaymentDate.GetDate.Date < mdtLoanSavingEffectiveDate Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry, Date should not be less than Saving Effective date") & " " & Format(mdtLoanSavingEffectiveDate, "dd-MMM-yyyy") & ".", Me)
                        Return False
                    End If
            End Select

            If mdecBaseExRate = 0 AndAlso mdecPaidExRate = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry! No Exchange rate defined for selected currency for the given date."), Me)
                dtpPaymentDate.Focus()
                Return False
            End If

            If CInt(Session("_PaymentVocNoType")) = 0 Then
                If txtAgainstVoucher.Text.Trim = "" Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Voucher No. cannot be blank. Voucher No. is compulsory information."), Me)
                    txtAgainstVoucher.Focus()
                    Return False
                End If
            End If

            If CInt(cboPaymentMode.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Payment Mode is compulsory information. Please select Payment Mode to continue."), Me)
                cboPaymentMode.Focus()
                Return False
            End If

            If CInt(cboPaymentBy.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Payment By is compulsory information. Please select Payment By to continue.", ), Me)
                cboPaymentBy.Focus()
                Return False
            End If

            Select Case CInt(cboPaymentMode.SelectedValue)

                Case enPaymentMode.CHEQUE
                    If CInt(cboBankGroup.SelectedValue) <= 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), Me)
                        cboBankGroup.Focus()
                        Return False
                    End If
                    If CInt(cboBranch.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), Me)
                        cboBranch.Focus()
                        Return False
                    ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Please select Bank Account. Bank Account is mandatory information."), Me)
                        cboAccountNo.Focus()
                        Return False
                    End If

                    Select Case mintReferenceId

                        Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

                            If objEmpBank.isEmployeeBankExist(objLoan_Advance._Employeeunkid) = False Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If

                            dsList = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, "", _
                                                                                "List", objLoan_Advance._Employeeunkid.ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = Nothing

                        Case clsPayment_tran.enPaymentRefId.PAYSLIP

                            If objEmpBank.isEmployeeBankExist(objPayslip._Employeeunkid) = False Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, "", _
                                                                                "List", objPayslip._Employeeunkid.ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = Nothing

                            'Nilay (01-Apr-2016) -- Start
                            'ENHANCEMENT - Approval Process in Loan Other Operations
                            Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
                            Dim blnFlag As Boolean
                            blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPeriod.SelectedValue), objPayslip._Employeeunkid.ToString, dtpPaymentDate.GetDate)
                            If blnFlag = False Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 40, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current period. In order to make Payment, please either Approve or Reject pending operations for current period."), Me)
                                Return False
                            End If
                            'Nilay (01-Apr-2016) -- End

                        Case clsPayment_tran.enPaymentRefId.SAVINGS

                            If objEmpBank.isEmployeeBankExist(objEmployee_Saving._Employeeunkid) = False Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, "", _
                                                                                "List", objEmployee_Saving._Employeeunkid.ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = Nothing
                    End Select

                    If txtChequeNo.Text = "" Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Cheque No. cannot be blank. Please enter the cheque information."), Me)
                        txtChequeNo.Focus()
                        Return False
                    End If

                Case enPaymentMode.TRANSFER

                    If CInt(cboBankGroup.SelectedValue) <= 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), Me)
                        cboBankGroup.Focus()
                        Return False
                    End If

                    If CInt(cboBranch.SelectedValue) <= 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), Me)
                        cboBranch.Focus()
                        Return False
                    End If

                    Select Case mintReferenceId

                        Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

                            If objEmpBank.isEmployeeBankExist(objLoan_Advance._Employeeunkid) = False Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, "", _
                                                                                "List", objLoan_Advance._Employeeunkid.ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = Nothing

                        Case clsPayment_tran.enPaymentRefId.PAYSLIP

                            If objEmpBank.isEmployeeBankExist(objPayslip._Employeeunkid) = False Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, "", _
                                                                                "List", objPayslip._Employeeunkid.ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = Nothing

                        Case clsPayment_tran.enPaymentRefId.SAVINGS

                            If objEmpBank.isEmployeeBankExist(objEmployee_Saving._Employeeunkid) = False Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            dsList = objEmpBank.GetEmployeeWithOverDistribution(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, "", _
                                                                                "List", objEmployee_Saving._Employeeunkid.ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                    End Select

            End Select

            Select Case CInt(cboPaymentBy.SelectedValue)
                Case 1 'Value
                    If CDec(txtAmount.Text) <= 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Amount cannot be blank .Amount is compulsory information."), Me)
                        txtAmount.Focus()
                        Return False
                    End If
                Case 2 'Percent
                    If CDec(txtPercent.Text) <= 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Percent cannot be blank .Percent is compulsory information."), Me)
                        txtPercent.Focus()
                        Return False
                    End If
            End Select

            Dim decBalAmt As Decimal
            If mdecBaseExRate <> 0 Then
                decBalAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
            Else
                decBalAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
            End If

            If CDec(txtAmount.Text) > CDec(Format(decBalAmt, Session("fmtCurrency").ToString)) _
                AndAlso Not (mintPaymentTypeId = clsPayment_tran.enPayTypeId.DEPOSIT) Then

                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Amount cannot be greater than the Due Amount ( ") & decBalAmt.ToString & Language.getMessage(mstrModuleName, 20, " )."), Me)
                txtAmount.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Please select period."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If dtpPaymentDate.GetDate.Date < objCommon._Start_Date OrElse dtpPaymentDate.GetDate.Date > CDate(Session("fin_enddate")).Date Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Payment Date should be in between ") & objCommon._Start_Date.ToShortDateString & _
                                              Language.getMessage(mstrModuleName, 28, " And ") & CDate(Session("fin_enddate")).ToShortDateString & ".", Me)
                dtpPaymentDate.Focus()
                Return False
            End If

            Dim objPeriod As New clscommom_period_Tran
            Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            If intFirstOpenPeriod > 0 Then
                objPeriod._Periodunkid(Session("Database_Name").ToString) = intFirstOpenPeriod
                If dtpPaymentDate.GetDate.Date < objPeriod._Start_Date Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, Payment Date should be in between open periods and should not be less than") & " " & objCommon._Start_Date.ToShortDateString, Me)
                    dtpPaymentDate.Focus()
                    Return False
                End If
            End If

            If dtpPaymentDate.GetDate.Date < CDate(objCommon._Start_Date).Date OrElse _
                    dtpPaymentDate.GetDate.Date > CDate(objCommon._End_Date).Date Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, Payment Date should be in between selected period start date and end date."), Me)
                dtpPaymentDate.Focus()
                Return False
            End If

            If mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                Dim strEmpID As String = ""
                Select Case mintReferenceId
                    Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                        strEmpID = objLoan_Advance._Employeeunkid.ToString
                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        strEmpID = objEmployee_Saving._Employeeunkid.ToString
                End Select
                If strEmpID.Trim <> "" Then
                    If objPayslip.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpID, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for selected employee."), Me)
                        cboPeriod.Focus()
                        Return False
                    End If
                End If
            End If
            objMaster = Nothing
            objPeriod = Nothing

            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then

                If CBool(Session("DoNotAllowOverDeductionForPayment")) = True Then
                    Dim objTnALeave As New clsTnALeaveTran
                    dtTable = objTnALeave.GetOverDeductionList("List", objCommon._Periodunkid(Session("Database_Name").ToString), False)
                    If dtTable.Rows.Count > 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry! You can not do Payment now. Reason : Some of the employees having Over Deduction in this month."), Me)
                        Return False
                    End If
                End If

                If gobjEmailList.Count > 0 AndAlso CBool(Session("SetPayslipPaymentApproval")) = True Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sending Email(s) process is in progress from other module. Please wait for some time."), Me)
                    Return False
                End If

            End If

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidate:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Function

#End Region

#Region "ComboBox's Events"

    Protected Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsList As New DataSet
        Dim objCompanyBank As New clsCompany_Bank_tran

        Try
            dsList = objCompanyBank.GetComboList(CInt(Session("Companyunkid")), 2, "Branch", _
                                                 " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
            With cboBranch
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Branch")
                .DataBind()
                If mintPaymentTranunkid <= 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = CStr(objPaymentTran._Branchunkid)
                End If
            End With
            Call cboBranch_SelectedIndexChanged(cboBranch, Nothing)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboBankGroup_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboBankGroup_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Dim dsList As New DataSet
        Dim objCompanyBank As New clsCompany_Bank_tran

        Try
            dsList = objCompanyBank.GetComboListBankAccount("Account", CInt(Session("Companyunkid")), True, CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
            With cboAccountNo
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("Account")
                .DataBind()
                If mintPaymentTranunkid <= 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = CStr(mintPaymentTranunkid)
                End If
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboBranch_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboBranch_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboPaymentBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentBy.SelectedIndexChanged
        Try
            If mintPaymentTranunkid <= 0 Then
                txtPercent.Text = ""
            End If

            Select Case CInt(cboPaymentBy.SelectedValue)
                Case 1  ' Value
                    txtPercent.Enabled = False
                    txtAmount.Read_Only = False
                Case 2  'Percentage
                    txtPercent.Enabled = True
                    txtAmount.Read_Only = True
            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPaymentBy_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPaymentBy_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged
        Try
            Select Case CInt(cboPaymentMode.SelectedValue)
                Case 1
                    cboBankGroup.SelectedValue = "0"
                    cboBankGroup.Enabled = False
                    cboBranch.SelectedValue = "0"
                    cboBranch.Enabled = False
                    txtChequeNo.Text = ""
                    txtChequeNo.Enabled = False
                    cboAccountNo.SelectedValue = "0"
                    cboAccountNo.Enabled = False

                Case Else
                    cboBankGroup.Enabled = True
                    cboBranch.Enabled = True
                    txtChequeNo.Enabled = True
                    cboAccountNo.Enabled = True
            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPaymentMode_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPaymentMode_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged

        Dim objExRate As New clsExchangeRate
        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        objLoan_Advance = New clsLoan_Advance

        Try
            objlblExRate.Text = ""
            lblAmount.Text = Language.getMessage(mstrModuleName, 31, "Amount")

            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.GetDate.Date, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & _
                                        dtTable.Rows(0).Item("currency_sign").ToString & " "
                lblAmount.Text &= " (" & dtTable.Rows(0).Item("currency_sign").ToString & ")"
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If

            If mintReferenceId = clsPayment_tran.enPaymentRefId.LOAN AndAlso mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                Dim dsLoan As New DataSet
                'Nilay (25-Mar-2016) -- Start
                'dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo("List", dtpPaymentDate.GetDate.Date, "", mintTransactionId)
                dsLoan = objLoan_Advance.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                   mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, "List", _
                                                                   dtpPaymentDate.GetDate.Date, "", mintTransactionId)
                'Nilay (25-Mar-2016) -- End

                If dsLoan.Tables("List").Rows.Count > 0 Then
                    txtBalanceDue.Text = Format(CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance")), Session("fmtCurrency").ToString)
                    mdecBaseCurrBalanceAmt = CDec(Format(CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance")), Session("fmtCurrency").ToString))
                    mdecLastProjectedBalance = CDec(Format(CDec(dsLoan.Tables("List").Rows(0).Item("LastProjectedBalance")), Session("fmtCurrency").ToString))

                    'Sohail (17 Mar 2020) -- Start
                    'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                    'objlblMessage.Text = ""

                    'If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                    '    Dim decRondingMultile As Decimal = 0
                    '    Select Case CInt(Session("PaymentRoundingMultiple"))
                    '        Case enPaymentRoundingMultiple._1
                    '            decRondingMultile = 1
                    '        Case enPaymentRoundingMultiple._5
                    '            decRondingMultile = 5
                    '        Case enPaymentRoundingMultiple._10
                    '            decRondingMultile = 10
                    '        Case enPaymentRoundingMultiple._50
                    '            decRondingMultile = 50
                    '        Case enPaymentRoundingMultiple._100
                    '            decRondingMultile = 100
                    '        Case enPaymentRoundingMultiple._500
                    '            decRondingMultile = 500
                    '        Case enPaymentRoundingMultiple._1000
                    '            decRondingMultile = 1000
                    '    End Select
                    '    Select Case CInt(Session("PaymentRoundingType"))
                    '        Case enPaymentRoundingType.NONE
                    '            mdecBaseCurrBalanceAmt = mdecLastProjectedBalance
                    '            objlblMessage.Text = ""

                    '        Case enPaymentRoundingType.AUTOMATIC
                    '            If decRondingMultile / 2 > mdecLastProjectedBalance Mod decRondingMultile Then
                    '                mdecBaseCurrBalanceAmt = mdecLastProjectedBalance - CDec(mdecLastProjectedBalance Mod decRondingMultile)
                    '            Else
                    '                If CDec(mdecLastProjectedBalance Mod decRondingMultile) <> 0 Then
                    '                    mdecBaseCurrBalanceAmt = mdecLastProjectedBalance - CDec(mdecLastProjectedBalance Mod decRondingMultile) + decRondingMultile
                    '                End If
                    '            End If
                    '            objlblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString

                    '        Case enPaymentRoundingType.UP
                    '            If CDec(mdecBaseCurrBalanceAmt Mod decRondingMultile) <> 0 Then
                    '                mdecBaseCurrBalanceAmt = mdecBaseCurrBalanceAmt - CDec(mdecBaseCurrBalanceAmt Mod decRondingMultile) + decRondingMultile
                    '            End If
                    '            objlblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString

                    '        Case enPaymentRoundingType.DOWN
                    '            mdecBaseCurrBalanceAmt = mdecBaseCurrBalanceAmt - CDec(mdecBaseCurrBalanceAmt Mod decRondingMultile)
                    '            objlblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString
                    '    End Select
                    'End If
                    'Sohail (17 Mar 2020) -- End

                End If
            End If

            'Sohail (17 Mar 2020) -- Start
            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
            mdecBaseCurrBalanceAmt = mdecBalanceDueTag
                    objlblMessage.Text = ""
            'Sohail (17 Mar 2020) -- End

            If mdecBaseExRate <> 0 Then
                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
            Else
                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
            End If
            'Sohail (17 Mar 2020) -- Start
            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                    If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                        Dim decRondingMultile As Decimal = 0
                        Select Case CInt(Session("PaymentRoundingMultiple"))
                            Case enPaymentRoundingMultiple._1
                                decRondingMultile = 1
                            Case enPaymentRoundingMultiple._5
                                decRondingMultile = 5
                            Case enPaymentRoundingMultiple._10
                                decRondingMultile = 10
                            Case enPaymentRoundingMultiple._50
                                decRondingMultile = 50
                            Case enPaymentRoundingMultiple._100
                                decRondingMultile = 100
                            Case enPaymentRoundingMultiple._500
                                decRondingMultile = 500
                            Case enPaymentRoundingMultiple._1000
                                decRondingMultile = 1000
                        End Select
                Dim dicBalanceAmt As Decimal = mdecPaidCurrBalanceAmt
                        Select Case CInt(Session("PaymentRoundingType"))
                            Case enPaymentRoundingType.NONE
                        mdecPaidCurrBalanceAmt = dicBalanceAmt
                                objlblMessage.Text = ""

                            Case enPaymentRoundingType.AUTOMATIC
                        If decRondingMultile / 2 > dicBalanceAmt Mod decRondingMultile Then
                            mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                                Else
                            If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                                mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                    End If
                                End If
                                objlblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString

                            Case enPaymentRoundingType.UP
                        If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                            mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                End If
                                objlblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString

                            Case enPaymentRoundingType.DOWN
                        mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                                objlblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString
                        End Select
            If mdecBaseExRate <> 0 Then
                    mdecBaseCurrBalanceAmt = mdecPaidCurrBalanceAmt * mdecBaseExRate / mdecPaidExRate
            Else
                    mdecBaseCurrBalanceAmt = mdecPaidCurrBalanceAmt * mdecBaseExRate
                End If
            End If
            'Sohail (17 Mar 2020) -- End
            mdecPaidCurrBalFormatedAmt = CDec(Format(mdecPaidCurrBalanceAmt, Session("fmtCurrency").ToString))

            If CInt(cboPaymentBy.SelectedValue) = enPaymentBy.Percentage Then
                txtAmount.Text = Format(CDec(mdecPaidCurrBalanceAmt * CDec(txtPercent.Text) / 100), Session("fmtCurrency").ToString)
            Else
                txtAmount.Text = Format(mdecPaidCurrBalanceAmt, Session("fmtCurrency").ToString)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboCurrency_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboCurrency_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (04-Nov-2016) -- Start
    'Enhancements: Global Change Status feature requested by {Rutta}
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If (Convert.ToInt32(cboPeriod.SelectedValue) > 0) Then
                Dim objPeriod As clscommom_period_Tran = New clscommom_period_Tran()
                'objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = Convert.ToInt32(cboPeriod.SelectedValue);
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = Convert.ToInt32(cboPeriod.SelectedValue)
                'Sohail (24 Jun 2017) -- Start
                'Issue - 68.1 - There is no row at position 0.
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
                'Sohail (24 Jun 2017) -- End
                dtpPaymentDate.SetDate = Convert.ToDateTime(objPeriod._Start_Date)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : cboPeriod_SelectedIndexChanged : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (04-Nov-2016) -- End

#End Region

#Region "TextBox's Events"

    Protected Sub txtPercent_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercent.TextChanged

        Dim decPercentAmount As Decimal = 0
        Try
            If txtPercent.Text.Trim <> "" Then
                If txtPercent.Text.Trim <> "." Then
                    If CDec(txtPercent.Text) > 100 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please enter Proper Percentage."), Me)
                        txtPercent.Text = CStr(0)
                        Exit Sub
                    Else
                        If mdecBaseExRate <> 0 Then
                            decPercentAmount = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
                        Else
                            decPercentAmount = mdecBaseCurrBalanceAmt * mdecPaidExRate
                        End If

                        decPercentAmount = (decPercentAmount * CDec(txtPercent.Text)) / 100
                        txtAmount.Text = CStr(decPercentAmount)
                    End If
                End If
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtPercent_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtPercent_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Dim blnFlag As Boolean = False
        Try
            If IsValidate() = False Then Exit Sub

            Call SetValue()

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim dtActAdj As New DataTable
            If mintPaymentTranunkid <= 0 AndAlso mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                Dim objBudget As New clsBudget_MasterNew
                Dim intBudgetId As Integer = 0
                Dim dsList As DataSet
                dsList = objBudget.GetComboList("List", False, True, )
                If dsList.Tables(0).Rows.Count > 0 Then
                    intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
                End If
                If intBudgetId > 0 Then
                    Dim mstrAnalysis_Fields As String = ""
                    Dim mstrAnalysis_Join As String = ""
                    Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                    Dim mstrReport_GroupName As String = ""
                    Dim mstrAnalysis_TableName As String = ""
                    Dim mstrAnalysis_CodeField As String = ""
                    objBudget._Budgetunkid = intBudgetId
                    Dim strEmpList As String = objPaymentTran._Employeeunkid.ToString()
                    Dim objBudgetCodes As New clsBudgetcodes_master
                    Dim dsEmp As DataSet = Nothing
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                        Dim objEmp As New clsEmployee_Master
                        Dim objBudget_Tran As New clsBudget_TranNew
                        Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                        Controls_AnalysisBy.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(objPaymentTran._Periodunkid, intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                        dsEmp = objEmp.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                    Else
                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(objPaymentTran._Periodunkid, intBudgetId, 0, 0, strEmpList, 1)
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                        dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                        dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                        dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                        dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                        dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                        dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                        Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                        Dim mdicActivityCode As New Dictionary(Of Integer, String)
                        Dim objActivity As New clsfundactivity_Tran
                        Dim dsAct As DataSet = objActivity.GetList("Activity")
                        Dim objActAdjust As New clsFundActivityAdjustment_Tran
                        Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPeriodEndDate)
                        mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                        mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                        'Dim dsBalance As DataSet = objTnALeave.Get_Balance_List(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, True, "prtnaleave_tran.balanceamount > 0", "list", strEmpList, CInt(cboPayPeriod.SelectedValue), "")
                        If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                            'mdtTable.PrimaryKey = New DataColumn() {mdtTable.Columns("employeeunkid")}
                            'mdtTable.Merge(dsEmp.Tables(0))
                        End If
                        Dim intActivityId As Integer
                        Dim decTotal As Decimal = 0
                        Dim decActBal As Decimal = 0
                        Dim blnShowValidationList As Boolean = False
                        Dim blnIsExeed As Boolean = False
                        For Each pair In mdicActivityCode
                            intActivityId = pair.Key
                            decTotal = 0
                            decActBal = 0
                            blnIsExeed = False

                            If mdicActivity.ContainsKey(intActivityId) = True Then
                                decActBal = mdicActivity.Item(intActivityId)
                            End If

                            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                            If lstRow.Count > 0 Then
                                For Each dRow As DataRow In lstRow
                                    Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                    Dim decNetPay As Decimal = objPaymentTran._Amount
                                    decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                Next

                                If decTotal > decActBal Then
                                    blnShowValidationList = True
                                    blnIsExeed = True
                                End If
                                Dim dr As DataRow = dtActAdj.NewRow
                                dr.Item("fundactivityunkid") = intActivityId
                                dr.Item("transactiondate") = eZeeDate.convertDate(mdtPeriodEndDate).ToString()
                                dr.Item("remark") = Language.getMessage(mstrModuleName, 41, "Salary payment for the period of") & " " & txtPayPeriod.Text
                                dr.Item("isexeed") = blnIsExeed
                                dr.Item("Activity Code") = pair.Value
                                dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                                dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                                dtActAdj.Rows.Add(dr)
                            End If
                        Next

                        If blnShowValidationList = True Then
                            Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                            For Each r In dr
                                dtActAdj.Rows.Remove(r)
                            Next
                            dtActAdj.AcceptChanges()
                            dtActAdj.Columns.Remove("fundactivityunkid")
                            dtActAdj.Columns.Remove("transactiondate")
                            dtActAdj.Columns.Remove("remark")
                            dtActAdj.Columns.Remove("isexeed")
                            popupValidationList.Message = Language.getMessage(mstrModuleName, 42, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")
                            popupValidationList.Data_Table = dtActAdj
                            popupValidationList.Show()
                            Exit Try
                        End If
                    End If
                End If
            End If
            'Sohail (23 May 2017) -- End

            If CBool(Session("_IsDenominationCompulsory")) = True AndAlso CBool(Session("AddCashDenomination")) = True Then

                If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                    If CDec(txtAmount.Text) > 0 AndAlso (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse _
                                                         CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE) Then
                        If mintPaymentTranunkid <= 0 Then

                        End If
                    End If
                End If
            End If

            With objPaymentTran
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._Companyunkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If mintPaymentTranunkid > 0 Then
                'Nilay (25-Mar-2016) -- Start
                'blnFlag = objPaymentTran.Update(Session("Database_Name").ToString, CInt(Session("Fin_year")), DateAndTime.Now.Date)
                blnFlag = objPaymentTran.Update(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                               mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, DateAndTime.Now.Date)
                'Nilay (25-Mar-2016) -- End
            Else
                'Nilay (25-Mar-2016) -- Start
                'blnFlag = objPaymentTran.Insert(Session("Database_Name").ToString, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, _
                '                                CBool(Session("IsIncludeInactiveEmp").ToString), CInt(Session("PaymentVocNoType")), _
                '                                Session("PaymentVocPrefix").ToString, DateAndTime.Now.Date, True, "")
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'blnFlag = objPaymentTran.Insert(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, _
                '                                CBool(Session("IsIncludeInactiveEmp").ToString), CInt(Session("PaymentVocNoType")), _
                '                                Session("PaymentVocPrefix").ToString, DateAndTime.Now.Date, True, "")
                blnFlag = objPaymentTran.Insert(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, _
                                                CBool(Session("IsIncludeInactiveEmp").ToString), CInt(Session("PaymentVocNoType")), _
                                                Session("PaymentVocPrefix").ToString, DateAndTime.Now.Date, True, "", dtActAdj)
                'Sohail (23 May 2017) -- End
                'Nilay (25-Mar-2016) -- End
            End If

            Select Case mintReferenceId
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

                    If blnFlag = False And objPaymentTran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
                    End If

                Case clsPayment_tran.enPaymentRefId.PAYSLIP

                    If blnFlag = False And objPaymentTran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
                    Else
                        If CBool(Session("SetPayslipPaymentApproval")) = True Then
                            Dim intBankPaid As Integer = 0
                            Dim intCashPaid As Integer = 0
                            Dim decBankPaid As Decimal = 0
                            Dim decCashPaid As Decimal = 0

                            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
                                intCashPaid = 1
                                decCashPaid = CDec(Format(objPaymentTran._Expaidamt, Session("fmtCurrency").ToString))

                            ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                                intBankPaid = 1
                                decBankPaid = CDec(Format(objPaymentTran._Expaidamt, Session("fmtCurrency").ToString))
                            End If

                            'Sohail (16 Nov 2016) -- Start
                            'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                            'objPaymentTran.SendMailToApprover(True, CInt(Session("UserId")), objPaymentTran._Employeeunkid.ToString, _
                            '                                  objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), _
                            '                                  decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, _
                            '                                  txtRemarks.Text.Trim, Session("fmtCurrency").ToString, CInt(Session("CompanyUnkId")), _
                            '                                  CInt(Session("Fin_year")), Session("Database_Name").ToString, , _
                            '                                  enLogin_Mode.MGR_SELF_SERVICE, 0, _
                            '                                  CInt(cboCurrency.SelectedValue), Session("ArutiSelfServiceURL").ToString)
                            objPaymentTran.SendMailToApprover(True, CInt(Session("UserId")), objPaymentTran._Employeeunkid.ToString, _
                                                              objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), _
                                                              decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, _
                                                              txtRemarks.Text.Trim, Session("fmtCurrency").ToString, CInt(Session("CompanyUnkId")), _
                                                              CInt(Session("Fin_year")), Session("Database_Name").ToString, mdtPeriodStartDate, mdtPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, , _
                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, _
                                                              CInt(cboCurrency.SelectedValue), Session("ArutiSelfServiceURL").ToString)
                            'Sohail (16 Nov 2016) -- End

                        End If

                    End If

                Case clsPayment_tran.enPaymentRefId.SAVINGS

                    If blnFlag = False And objPaymentTran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
                    End If
            End Select

            Dim strURL As String = String.Empty

            If blnFlag Then
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations 

                 'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If mblnIsFromStatus = False Then
                '    mintPaymentTranunkid = objPaymentTran._Paymenttranunkid
                '    strURL = mintReferenceId & "|" & mintTransactionId & "|" & mintPaymentTypeId & "|" & mdtLoanSavingEffectiveDate.ToString.Trim
                '    Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)), False)
                'Else
                '    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
                'End If
                If mblnIsFromStatus = False Then
                    Session("ReferenceId") = mintReferenceId
                    Session("TransactionId") = mintTransactionId
                    Session("PaymentTypeId") = mintPaymentTypeId
                    Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
                End If
                'Nilay (06-Aug-2016) -- END
                'Nilay (01-Apr-2016) -- End

                'If mintPaymentTranunkid <= 0 Then
                '    'objPaymentTran = Nothing
                '    'objPaymentTran = New clsPayment_tran
                '    'Call GetValue()
                '    'txtAgainstVoucher.Focus()

                'End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            Session("LoanStatusData") = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim strURL As String = String.Empty
        Try
            If mblnIsFromStatus = False Then
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'strURL &= mintReferenceId & "|" & mintTransactionId & "|" & mintPaymentTypeId & "|" & mdtLoanSavingEffectiveDate.ToString
                'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)), False)
                Session("ReferenceId") = mintReferenceId
                Session("TransactionId") = mintTransactionId
                Session("PaymentTypeId") = mintPaymentTypeId
                Session("LoanSavingEffectiveDate") = mdtLoanSavingEffectiveDate
                Session("IsFromStatus") = Nothing
                Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Payment_List.aspx", False)
                'Nilay (06-Aug-2016) -- END
            Else
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'strURL &= mintTransactionId & "|" & mblnIsFromStatus.ToString & "|" & enLoanStatus.WRITTEN_OFF
                'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Status/wPg_LoanStatus_AddEdit.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)), False)
                Session("LoanAdvanceTranunkid") = mintTransactionId
                Session("IsFromLoan") = mblnIsFromStatus
                Session("StatusId") = enLoanStatus.WRITTEN_OFF
                Session("IsFromStatus") = Nothing
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Status/wPg_LoanStatus_AddEdit.aspx", False)
                'Nilay (06-Aug-2016) -- END
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            Session("LoanStatusData") = Nothing
        End Try
    End Sub

#End Region

#Region "Date Control Event"
    Protected Sub dtpPaymentDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPaymentDate.TextChanged
        Try

            Call cboCurrency_SelectedIndexChanged(cboCurrency, Nothing)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpPaymentDate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dtpPaymentDate_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            'Nilay (28-Aug-2015) -- Start
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbPayment", Me.lblDetialHeader.Text)
            'Nilay (28-Aug-2015) -- End

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lblPaymentOf.Text = Language._Object.getCaption(Me.lblPaymentOf.ID, Me.lblPaymentOf.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.ID, Me.lblPayYear.Text)
            Me.lblPaidTo.Text = Language._Object.getCaption(Me.lblPaidTo.ID, Me.lblPaidTo.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.ID, Me.lblPaymentDate.Text)
            Me.lblPaymentMode.Text = Language._Object.getCaption(Me.lblPaymentMode.ID, Me.lblPaymentMode.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.ID, Me.lblBankGroup.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblCheque.Text = Language._Object.getCaption(Me.lblCheque.ID, Me.lblCheque.Text)
            Me.lblPaymentBy.Text = Language._Object.getCaption(Me.lblPaymentBy.ID, Me.lblPaymentBy.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblVoucher.Text = Language._Object.getCaption(Me.lblVoucher.ID, Me.lblVoucher.Text)
            Me.lblAgainstVoucher.Text = Language._Object.getCaption(Me.lblAgainstVoucher.ID, Me.lblAgainstVoucher.Text)
            Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.ID, Me.lblValue.Text)
            Me.lblScheme.Text = Language._Object.getCaption(Me.lblScheme.ID, Me.lblScheme.Text)
            Me.lblLoanAmount.Text = Language._Object.getCaption(Me.lblLoanAmount.ID, Me.lblLoanAmount.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.ID, Me.lblAccountNo.Text)
            'Sohail (09 Mar 2015) -- Start
            'Enhancement - Provide Multi Currency on Saving scheme contribution.
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            'Sohail (09 Mar 2015) -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : SetLanguage : " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

#Region "Old Code before 11-Sep-15"

    '#Region " Private Varaibles "

    '    Private objPaymentTran As New clsPayment_tran
    '    Private objEmployee As New clsEmployee_Master
    '    Private objMasterData As New clsMasterData
    '    Private objCommon As New clscommom_period_Tran
    '    Dim msg As New CommonCodes

    '    'Anjan [04 June 2014] -- Start
    '    'ENHANCEMENT : Implementing Language,requested by Andrew
    '    Private ReadOnly mstrModuleName As String = "frmPayment_AddEdit"
    '    'Anjan [04 June 2014] -- End

    '    'SHANI (09 Mar 2015) -- Start
    '    'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '    Private mdecBaseExRate As Decimal = 0
    '    Private mdecPaidCurrBalanceAmt As Decimal = 0
    '    Private mdecBaseCurrBalanceAmt As Decimal = 0
    '    Private mdecPaidExRate As Decimal = 0
    '    Private mdecPaidCurrBalFormatedAmt As Decimal = 0
    '    Private txtAmountTag As Decimal = 0
    '    'SHANI (09 Mar 2015) -- End
    '    'Nilay (28-Aug-2015) -- Start
    '    Private mintBaseCountryId As Integer = 0
    '    'Nilay (28-Aug-2015) -- End

    '#Region " Loan/Advance "

    '    Private objLoan_Advance As New clsLoan_Advance
    '    Private objLoan_Master As New clsLoan_Scheme

    '#End Region

    '#Region " Savings "

    '    Private objEmployee_Saving As New clsSaving_Tran
    '    Private objSaving_Master As New clsSavingScheme
    '    Private objSavingStatus As New clsSaving_Status_Tran

    '#End Region

    '#Region " PaySlip "

    '    Dim objPayslip As New clsTnALeaveTran

    '#End Region

    '#End Region

    '#Region " Form's Events "

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try
    '            If Session("clsuser") Is Nothing Then
    '                Exit Sub
    '            End If

    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
    '                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '                Exit Sub
    '            End If



    '            'Anjan [04 June 2014] -- Start
    '            'ENHANCEMENT : Implementing Language,requested by Andrew
    '            Call SetLanguage()
    '            'Anjan [04 June 2014] -- End

    '            Blank_ModuleName()
    '            StrModuleName2 = "mnuLoan_Advance_Savings"
    '            objPaymentTran._WebFormName = "frmPayment_AddEdit"
    '            objPaymentTran._WebIP = Session("IP_ADD")
    '            objPaymentTran._WebHostName = Session("HOST_NAME")
    '            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                objPaymentTran._LoginEmployeeUnkid = Session("Employeeunkid")
    '            End If

    '            Dim clsuser As New User
    '            If (Page.IsPostBack = False) Then
    '                clsuser = CType(Session("clsuser"), User)
    '                If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                    'Sohail (30 Mar 2015) -- Start
    '                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '                    'Me.ViewState("Empunkid") = clsuser.UserID
    '                    Me.ViewState("Empunkid") = Session("UserId")
    '                    'Sohail (30 Mar 2015) -- End
    '                Else
    '                    'Sohail (30 Mar 2015) -- Start
    '                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
    '                    Me.ViewState("Empunkid") = Session("Employeeunkid")
    '                    'Sohail (30 Mar 2015) -- End
    '                End If
    '                If Request.QueryString("ProcessId") <> "" Then
    '                    Call IsFrom_List()
    '                    Call FillCombo()
    '                Else
    '                    Call FillCombo()
    '                End If
    '                Call Fill_Info()
    '                'Nilay (28-Aug-2015) -- Start
    '                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                'txtAmount.Text = Format(mdecPaidCurrBalFormatedAmt, Session("fmtCurrency"))
    '                If mintBaseCountryId > 0 Then
    '                    cboCurrency.SelectedValue = mintBaseCountryId
    '                Else
    '                    cboCurrency.SelectedIndex = 0
    '                End If
    '                'Nilay (28-Aug-2015) -- End

    '                'SHANI (09 Mar 2015) -- Start
    '                'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '            Else
    '                mdecBaseExRate = ViewState("mdecBaseExRate")
    '                mdecPaidCurrBalanceAmt = ViewState("mdecPaidCurrBalanceAmt")
    '                mdecBaseCurrBalanceAmt = ViewState("mdecBaseCurrBalanceAmt")
    '                mdecPaidExRate = ViewState("mdecPaidExRate")
    '                mdecPaidCurrBalFormatedAmt = ViewState("mdecPaidCurrBalFormatedAmt")
    '                txtAmountTag = ViewState("txtAmountTag")
    '                'SHANI (09 Mar 2015) -- End

    '                'Nilay (28-Aug-2015) -- Start
    '                mintBaseCountryId = Me.ViewState("BaseCountryId")
    '                'Nilay (28-Aug-2015) -- End

    '            End If
    '            Call SetVisibility()
    '        Catch ex As Exception
    '            msg.DisplayError("Procedure Page_Load : " & ex.Message, Me)
    '        Finally

    '            'SHANI [01 FEB 2015]-START
    '            'Enhancement - REDESIGN SELF SERVICE.
    '            'ToolbarEntry1.VisibleImageSaprator1(False)
    '            'ToolbarEntry1.VisibleImageSaprator2(False)
    '            'ToolbarEntry1.VisibleImageSaprator3(False)
    '            'ToolbarEntry1.VisibleImageSaprator4(False)
    '            'SHANI [01 FEB 2015]--END
    '        End Try
    '    End Sub

    '    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'ToolbarEntry1.VisibleSaveButton(False)
    '        'ToolbarEntry1.VisibleDeleteButton(False)
    '        'ToolbarEntry1.VisibleCancelButton(False)
    '        'ToolbarEntry1.VisibleNewButton(False)
    '        'ToolbarEntry1.VisibleModeFormButton(False)
    '        'ToolbarEntry1.VisibleExitButton(False)
    '        'SHANI [01 FEB 2015]--END
    '        'Sohail (09 Mar 2015) -- Start
    '        'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '        ViewState("mdecBaseExRate") = mdecBaseExRate
    '        ViewState("mdecPaidCurrBalanceAmt") = mdecPaidCurrBalanceAmt
    '        ViewState("mdecBaseCurrBalanceAmt") = mdecBaseCurrBalanceAmt
    '        ViewState("mdecPaidExRate") = mdecPaidExRate
    '        ViewState("mdecPaidCurrBalFormatedAmt") = mdecPaidCurrBalFormatedAmt
    '        ViewState("txtAmountTag") = txtAmountTag
    '        'Sohail (09 Mar 2015) -- End

    '        'Nilay (28-Aug-2015) -- Start
    '        Me.ViewState.Add("BaseCountryId", mintBaseCountryId)
    '        'Nilay (28-Aug-2015) -- End

    '    End Sub

    '    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '        Me.IsLoginRequired = True
    '    End Sub

    '    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
    '        Try
    '            ' If Not IsPostBack Then
    '            'Me.ViewState.Add("mdecBaseExRate", 0)
    '            'Me.ViewState.Add("mdecPaidExRate", 0)
    '            'Me.ViewState.Add("mintPaidCurrId", 0)
    '            'Me.ViewState.Add("mdecPaidCurrBalanceAmt", 0)
    '            'Me.ViewState.Add("mdecPaidCurrBalFormatedAmt", 0)
    '            'Call IsFrom_List()
    '            ' End If
    '        Catch ex As Exception
    '            msg.DisplayError("Page_PreRenderComplete Event : " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Private Method(s) & Function(s) "

    '    Private Sub FillCombo()
    '        Dim dsCombos As New DataSet
    '        Dim objCompanyBank As New clsCompany_Bank_tran
    '        Dim objExRate As New clsExchangeRate
    '        Dim objMaster As New clsMasterData
    '        Dim objCommon As New clscommom_period_Tran 'Sohail (09 Mar 2015)
    '        Try
    '            dsCombos = objMaster.GetPaymentMode("PayMode")
    '            With cboPaymentMode
    '                .DataValueField = "Id"
    '                .DataTextField = "Name"
    '                .DataSource = dsCombos.Tables("PayMode")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With
    '            Call cboPaymentMode_SelectedIndexChanged(Nothing, Nothing)

    '            dsCombos = objMaster.GetPaymentBy("PayBy")
    '            With cboPaymentBy
    '                .DataValueField = "Id"
    '                .DataTextField = "Name"
    '                .DataSource = dsCombos.Tables("PayBy")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With

    '            dsCombos = objCompanyBank.GetComboList(Session("CompanyUnkId"), 1, "BankGrp")
    '            With cboBankGroup
    '                .DataValueField = "id"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("BankGrp")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With

    '            dsCombos = objExRate.getComboList("ExRate", False)
    '            If dsCombos.Tables(0).Rows.Count > 0 Then
    '                Dim dtTable As DataTable = New DataView(dsCombos.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
    '                If dtTable.Rows.Count > 0 Then
    '                    Me.ViewState.Add("mstrBaseCurrSign", dtTable.Rows(0).Item("currency_sign").ToString)
    '                    Me.ViewState.Add("mintBaseCurrId", CInt(dtTable.Rows(0).Item("exchangerateunkid")))
    '                    'Nilay (28-Aug-2015) -- Start
    '                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
    '                Else
    '                    mintBaseCountryId = 0
    '                    'Nilay (28-Aug-2015) -- End
    '                End If
    '            End If

    '            With cboCurrency
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "currency_sign"
    '                .DataSource = dsCombos.Tables("ExRate")
    '                .DataBind()
    '                'Nilay (28-Aug-2015) -- Start
    '                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                '.SelectedIndex = 0
    '                If mintBaseCountryId > 0 Then
    '                    .SelectedValue = mintBaseCountryId
    '                Else
    '                    .SelectedIndex = 0
    '                End If
    '                'Nilay (28-Aug-2015) -- End
    '            End With

    '            'Sohail (09 Mar 2015) -- Start
    '            'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '            dsCombos = objCommon.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, 1, , , Session("Database_Name").ToString)

    '            'Nilay (28-Aug-2015) -- Start
    '            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt - Allow to take receipt, withdrawal in first open period only as interest and balance are re-calculated on end date process payroll.
    '            Dim mdtTable As DataTable = Nothing
    '            If CInt(Me.ViewState("PaymentTypeId")) = clsPayment_tran.enPayTypeId.PAYMENT Then
    '                mdtTable = New DataView(dsCombos.Tables("Period")).ToTable
    '            Else
    '                Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, CInt(Session("Fin_year")))
    '                If intFirstPeriodId > 0 Then
    '                    mdtTable = New DataView(dsCombos.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
    '                Else
    '                    mdtTable = New DataView(dsCombos.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
    '                End If
    '            End If
    '            'Nilay (28-Aug-2015) -- End

    '            With cboPeriod
    '                .DataValueField = "periodunkid"
    '                .DataTextField = "name"

    '                'Nilay (28-Aug-2015) -- Start
    '                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                '.DataSource = dsCombos.Tables("Period")
    '                '.DataBind()
    '                '.SelectedValue = 0
    '                .DataSource = mdtTable
    '                .DataBind()
    '                If .Items.Count > 0 Then .SelectedIndex = 0
    '                'Nilay (28-Aug-2015) -- End

    '            End With
    '            'Sohail (09 Mar 2015) -- End
    '        Catch ex As Exception
    '            msg.DisplayError("FillCombo : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub SetVisibility()
    '        Try
    '            If Session("_PaymentVocNoType") = 1 Then
    '                txtVoucherNo.Enabled = False
    '            Else
    '                txtVoucherNo.Enabled = True
    '            End If
    '            Select Case CInt(Me.ViewState("PaymentTypeId"))
    '                Case clsPayment_tran.enPayTypeId.PAYMENT
    '                    lblPaidTo.Text = "Paid To"
    '                    lblPayment.Text = "Payment"
    '                Case clsPayment_tran.enPayTypeId.RECEIVED
    '                    lblPaidTo.Text = "Received From"
    '                    lblPayment.Text = "Balance Due"
    '                Case clsPayment_tran.enPayTypeId.WITHDRAWAL
    '                Case clsPayment_tran.enPayTypeId.REPAYMENT
    '                    lblPayment.Text = "Savings"
    '            End Select
    '        Catch ex As Exception
    '            msg.DisplayError("SetVisbibility : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Function IsFrom_List() As Boolean
    '        Try
    '            If (Request.QueryString("ProcessId") <> "") Then
    '                Dim sProcess() As String = Nothing
    '                sProcess = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("ProcessId"))).Split("|")
    '                If sProcess.Length > 0 Then
    '                    Me.ViewState.Add("ReferenceId", sProcess(0))
    '                    Me.ViewState.Add("TransactonId", sProcess(1))
    '                    Me.ViewState.Add("PaymentTypeId", sProcess(2))
    '                    Me.ViewState.Add("PaymentTranId", sProcess(3))
    '                    Me.ViewState.Add("RowIdx", sProcess(4))
    '                    Try
    '                        Me.ViewState.Add("sData", sProcess(5))
    '                        Me.ViewState.Add("IsFrom_Status", sProcess(6))
    '                    Catch ex As Exception
    '                    End Try
    '                    objPaymentTran._Paymenttranunkid = Me.ViewState("PaymentTranId")
    '                    Me.ViewState.Add("_Branchunkid", objPaymentTran._Branchunkid)
    '                    Select Case CInt(Me.ViewState("ReferenceId"))
    '                        Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
    '                            objLoan_Advance._Loanadvancetranunkid = Me.ViewState("TransactonId")
    '                            Me.ViewState.Add("_Employeeunkid", objLoan_Advance._Employeeunkid)
    '                            Me.ViewState.Add("_Periodunkid", objLoan_Advance._Periodunkid)
    '                            Me.ViewState.Add("_Loanschemeunkid", objLoan_Advance._Loanschemeunkid)
    '                            Me.ViewState.Add("_Isloan", objLoan_Advance._Isloan)
    '                            Me.ViewState.Add("_Loanvoucher_No", objLoan_Advance._Loanvoucher_No)
    '                            Me.ViewState.Add("_Loan_Amount", objLoan_Advance._Loan_Amount)
    '                            Me.ViewState.Add("_Advance_Amount", objLoan_Advance._Advance_Amount)
    '                            Me.ViewState.Add("_Balance_Amount", objLoan_Advance._Balance_Amount)
    '                        Case clsPayment_tran.enPaymentRefId.PAYSLIP
    '                            objPayslip._Tnaleavetranunkid = Me.ViewState("TransactonId")
    '                            Me.ViewState.Add("_Employeeunkid", objPayslip._Employeeunkid)
    '                            Me.ViewState.Add("_Periodunkid", objPayslip._Payperiodunkid)
    '                            Me.ViewState.Add("_Voucherno", objPayslip._Voucherno)
    '                            Me.ViewState.Add("_Balanceamount", objPayslip._Balanceamount)
    '                        Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                            objEmployee_Saving._Savingtranunkid = Me.ViewState("TransactonId")
    '                            Me.ViewState.Add("_Employeeunkid", objEmployee_Saving._Employeeunkid)
    '                            Me.ViewState.Add("_Periodunkid", objEmployee_Saving._Payperiodunkid)
    '                            Me.ViewState.Add("_Savingschemeunkid", objEmployee_Saving._Savingschemeunkid)
    '                            Me.ViewState.Add("_Effectivedate", objEmployee_Saving._Effectivedate)
    '                            Me.ViewState.Add("_Balance_Amount", objEmployee_Saving._Balance_Amount)
    '                            Me.ViewState.Add("_Voucherno", objEmployee_Saving._Voucherno)
    '                    End Select
    '                End If
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("IsFrom_List : " & ex.Message, Me)
    '        End Try
    '    End Function

    '    Private Sub Fill_Info()
    '        Try
    '            objEmployee._Employeeunkid = Me.ViewState("_Employeeunkid")
    '            txtPaidTo.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

    '            Select Case CInt(Me.ViewState("ReferenceId"))

    '                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

    '                    txtPaymentOf.Text = IIf(CBool(Me.ViewState("_Isloan")), "Loan", "Advance")
    '                    txtVoucherNo.Text = Me.ViewState("_Loanvoucher_No")
    '                    txtPayYear.Text = objMasterData.GetFinancialYear_Name(CInt(Session("Fin_year")))
    '                    objCommon._Periodunkid = Me.ViewState("_Periodunkid")
    '                    Me.ViewState.Add("_Start_Date", objCommon._Start_Date)
    '                    txtPayPeriod.Text = objCommon._Period_Name
    '                    objLoan_Master._Loanschemeunkid = Me.ViewState("_Loanschemeunkid")
    '                    txtScheme.Text = objLoan_Master._Name
    '                    Select Case CInt(Me.ViewState("PaymentTypeId"))
    '                        Case clsPayment_tran.enPayTypeId.PAYMENT
    '                            'Nilay (28-Aug-2015) -- Start
    '                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                            'txtPayment.Text = Format(IIf(CBool(Me.ViewState("_Isloan")) = True, Me.ViewState("_Loan_Amount"), Me.ViewState("_Advance_Amount")), Session("fmtCurrency"))
    '                            txtPayment.Text = Format(objLoan_Advance._Basecurrency_amount, Session("fmtCurrency"))
    '                            'Nilay (28-Aug-2015) -- End

    '                        Case clsPayment_tran.enPayTypeId.RECEIVED

    '                            'Nilay (28-Aug-2015) -- Start
    '                            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                            'txtPayment.Text = Format(Me.ViewState("_Balance_Amount"), Session("fmtCurrency"))
    '                            If CInt(Me.ViewState("ReferenceId")) = clsPayment_tran.enPaymentRefId.ADVANCE Then
    '                                txtPayment.Text = Format(objLoan_Advance._Balance_Amount, Session("fmtCurrency"))
    '                                'txtPayment.Tag = CDec(objLoan_Advance._Balance_Amount)
    '                            Else
    '                                Dim decBalanceAmt As Decimal = 0
    '                                Dim objLoan As New clsLoan_Advance
    '                                Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo("List", dtdate.GetDate.Date, "", CInt(Me.ViewState("PaymentTranId")))
    '                                If dsLoan.Tables("List").Rows.Count > 0 Then
    '                                    With dsLoan.Tables("List").Rows(0)
    '                                        txtPayment.Text = Format(CDec(.Item("LastProjectedBalance")), Session("fmtCurrency"))
    '                                        'txtBalanceDue.Tag = CDec(.Item("LastProjectedBalance"))
    '                                    End With
    '                                End If
    '                            End If
    '                            'Nilay (28-Aug-2015) -- End

    '                    End Select

    '                Case clsPayment_tran.enPaymentRefId.PAYSLIP
    '                    txtPaymentOf.Text = "Payslip"
    '                    txtVoucherNo.Text = Me.ViewState("_Voucherno")
    '                    txtPayYear.Text = objMasterData.GetFinancialYear_Name(CInt(Session("Fin_year")))
    '                    objCommon._Periodunkid = Me.ViewState("_Periodunkid")
    '                    Me.ViewState.Add("_Start_Date", objCommon._Start_Date)
    '                    txtPayPeriod.Text = objCommon._Period_Name
    '                    txtPayment.Text = Format(Me.ViewState("_Balanceamount"), Session("fmtCurrency"))
    '                    txtAmount.Text = txtPayment.Text
    '                Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                    txtPaymentOf.Text = "Savings"
    '                    txtVoucherNo.Text = Me.ViewState("_Voucherno")
    '                    txtPayYear.Text = objMasterData.GetFinancialYear_Name(CInt(Session("Fin_year")))
    '                    objCommon._Periodunkid = Me.ViewState("_Periodunkid")
    '                    Me.ViewState.Add("_Start_Date", objCommon._Start_Date)
    '                    txtPayPeriod.Text = objCommon._Period_Name
    '                    objSaving_Master._Savingschemeunkid = Me.ViewState("_Savingschemeunkid")
    '                    txtScheme.Text = objSaving_Master._Savingschemename
    '            End Select

    '            dtdate.SetDate = ConfigParameter._Object._CurrentDateAndTime

    '            If Me.ViewState("PaymentTranId") > 0 Then
    '                objPaymentTran._Paymenttranunkid = Me.ViewState("PaymentTranId")
    '                'S.SANDEEP [ 04 DEC 2013 ] -- START
    '                cboPaymentMode.Enabled = False
    '                cboPaymentBy.Enabled = False
    '                cboCurrency.Enabled = False
    '                dtdate.Enabled = False
    '                txtAgainstVoucher.Enabled = False
    '                'S.SANDEEP [ 04 DEC 2013 ] -- END
    '            End If

    '            'S.SANDEEP [ 04 DEC 2013 ] -- START
    '            'If CInt(Me.ViewState("ReferenceId")) <> clsPayment_tran.enPaymentRefId.SAVINGS Then
    '            '    cboPaymentMode.Enabled = False
    '            '    cboPaymentBy.Enabled = False
    '            '    cboCurrency.Enabled = False
    '            '    dtdate.Enabled = False
    '            '    txtAgainstVoucher.Enabled = False
    '            'End If
    '            'S.SANDEEP [ 04 DEC 2013 ] -- END

    '            cboCurrency.SelectedValue = objPaymentTran._Countryunkid
    '            Call cboCurrency_SelectedIndexChanged(Nothing, Nothing)
    '            txtPercent.Text = CStr(objPaymentTran._Percentage)
    '            txtAmount.Text = Format(objPaymentTran._Expaidamt, Session("fmtCurrency"))
    '            cboPaymentBy.SelectedValue = objPaymentTran._Paymentbyid
    '            Call cboPaymentBy_SelectedIndexChanged(Nothing, Nothing)
    '            cboBranch.SelectedValue = objPaymentTran._Branchunkid

    '            Dim objBankGrp As New clspayrollgroup_master : Dim objBranch As New clsbankbranch_master

    '            objBranch._Branchunkid = CInt(objPaymentTran._Branchunkid)
    '            objBankGrp._Groupmasterunkid = CInt(objBranch._Bankgroupunkid)
    '            cboBankGroup.SelectedValue = CInt(objBankGrp._Groupmasterunkid)

    '            objBankGrp = Nothing : objBranch = Nothing

    '            txtChequeNo.Text = objPaymentTran._Chequeno
    '            cboAccountNo.Text = objPaymentTran._Accountno
    '            objPaymentTran._Employeeunkid = objPaymentTran._Employeeunkid
    '            objPaymentTran._Isvoid = objPaymentTran._Isvoid
    '            objPaymentTran._Is_Receipt = objPaymentTran._Is_Receipt
    '            If objPaymentTran._Paymentdate <> Nothing Then
    '                dtdate.SetDate = objPaymentTran._Paymentdate.Date
    '            End If
    '            cboPaymentMode.SelectedValue = objPaymentTran._Paymentmodeid
    '            Call cboPaymentMode_SelectedIndexChanged(Nothing, Nothing)

    '            objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
    '            objPaymentTran._Userunkid = objPaymentTran._Userunkid
    '            objPaymentTran._Voiddatetime = objPaymentTran._Voiddatetime
    '            objPaymentTran._Voiduserunkid = objPaymentTran._Voiduserunkid
    '            If objPaymentTran._Voucherno <> "" Then
    '                txtAgainstVoucher.Text = objPaymentTran._Voucherno
    '            End If
    '            txtRemarks.Text = objPaymentTran._Remarks 'Sohail (09 Mar 2015)

    '            'Nilay (28-Aug-2015) -- Start
    '            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '            objCommon._Periodunkid = CInt(cboPeriod.SelectedValue)
    '            'Nilay (28-Aug-2015) -- End

    '            Dim decReceived As Decimal = 0

    '            Select Case CInt(Me.ViewState("ReferenceId"))

    '                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE

    '                    If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT Then

    '                        objPaymentTran.Get_Payment_Total(Me.ViewState("TransactonId"), Me.ViewState("ReferenceId"), Me.ViewState("PaymentTypeId"), decReceived, , True)
    '                        If Me.ViewState("RowIdx") = 0 Then
    '                            decReceived = 0
    '                            txtPayment.Text = Format((CDec(txtPayment.Text) - decReceived), Session("fmtCurrency"))
    '                        Else
    '                            txtPayment.Text = Format((CDec(txtPayment.Text) + decReceived) - decReceived, Session("fmtCurrency"))
    '                        End If

    '                    ElseIf Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED Then

    '                        objPaymentTran.Get_Payment_Total(Me.ViewState("TransactonId"), Me.ViewState("ReferenceId"), Me.ViewState("PaymentTypeId"), decReceived, True)
    '                        If Me.ViewState("RowIdx") = 0 Then
    '                            decReceived = 0
    '                            txtPayment.Text = Format((CDec(txtPayment.Text) - decReceived), Session("fmtCurrency"))
    '                        Else
    '                            txtPayment.Text = Format((CDec(txtPayment.Text) + decReceived), Session("fmtCurrency"))
    '                        End If
    '                    End If

    '                    'Nilay (28-Aug-2015) -- Start
    '                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                    If CInt(cboPeriod.SelectedValue) > 0 Then
    '                        dtdate.SetDate = eZeeDate.convertDate(objCommon._Start_Date.ToString)
    '                    End If
    '                    'Nilay (28-Aug-2015) -- End

    '                Case clsPayment_tran.enPaymentRefId.PAYSLIP

    '                    objPaymentTran.Get_Payment_Total(Me.ViewState("TransactonId"), Me.ViewState("ReferenceId"), Me.ViewState("PaymentTypeId"), decReceived, True, True)

    '                    If Me.ViewState("RowIdx") = 0 Then
    '                        decReceived = 0
    '                    End If

    '                    txtPayment.Text = Format((CDec(txtPayment.Text) - decReceived), Session("fmtCurrency"))

    '                    'Nilay (28-Aug-2015) -- Start
    '                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                    If CInt(cboPeriod.SelectedValue) > 0 Then
    '                        dtdate.SetDate = eZeeDate.convertDate(objCommon._End_Date.ToString)
    '                    End If
    '                    'Nilay (28-Aug-2015) -- End

    '                Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                    'Sohail (12 Jan 2015) -- Start
    '                    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    '                    'If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.REPAYMENT Then
    '                    '    Call FinalRepaymentAmt()
    '                    'End If
    '                    If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.REPAYMENT OrElse Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.WITHDRAWAL Then
    '                        Call FinalRepaymentAmt()
    '                    End If
    '                    'Sohail (12 Jan 2015) -- End

    '                    'Nilay (28-Aug-2015) -- Start
    '                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                    If CInt(cboPeriod.SelectedValue) > 0 Then
    '                        dtdate.SetDate = eZeeDate.convertDate(objCommon._End_Date.ToString)
    '                    End If
    '                    'Nilay (28-Aug-2015) -- End
    '            End Select
    '            ' End If


    '            'Pinkal (03-Jun-2013) -- Start
    '            'Enhancement : TRA Changes
    '            If txtPayment.Text.Trim.Length <= 0 Then
    '                txtPayment.Text = CDec(0).ToString(Session("fmtCurrency"))
    '            End If
    '            'Pinkal (03-Jun-2013) -- End

    '            mdecBaseCurrBalanceAmt = CDec(txtPayment.Text)
    '            If mdecBaseExRate <> 0 Then
    '                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
    '            Else
    '                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
    '            End If
    '            mdecPaidCurrBalFormatedAmt = CDec(Format(mdecPaidCurrBalanceAmt, Session("fmtCurrency")))
    '            If Me.ViewState("PaymentTranId") <= 0 Then
    '                cboCurrency.SelectedIndex = 0
    '                txtAmount.Text = Format(mdecPaidCurrBalFormatedAmt, Session("fmtCurrency"))
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError("Fill_Info : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Function IsValidate() As Boolean
    '        Dim dtTable As DataTable
    '        Dim objEmpBank As New clsEmployeeBanks
    '        Dim ds As DataSet
    '        Dim mDecAmount As Decimal = 0
    '        Try
    '            Decimal.TryParse(txtPayment.Text, mDecAmount)
    '            If mDecAmount <= 0 Then
    '                Select Case CInt(Me.ViewState("ReferenceId"))
    '                    Case clsPayment_tran.enPaymentRefId.LOAN

    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot make new payment. Reason : Loan is already paid to particular employee."), Me)
    '                        'Anjan [04 June 2014] -- End



    '                        Return False
    '                    Case clsPayment_tran.enPaymentRefId.ADVANCE
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot make new payment. Reason : Due amount is already paid by employee."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    Case clsPayment_tran.enPaymentRefId.PAYSLIP
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, you cannot make payment. Reason : Payslip amount is either Zero or Negative."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Saving Balance is Zero."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                End Select
    '            End If

    '            If mdecBaseExRate <= 0 AndAlso mdecPaidExRate <= 0 Then
    '                'Anjan [04 June 2014] -- Start
    '                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                Language.setLanguage(mstrModuleName)
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry! No Exchange rate defined for selected currency for the given date."), Me)
    '                'Anjan [04 June 2014] -- End
    '                Return False
    '            End If

    '            If Session("_PaymentVocNoType") = 0 Then

    '                'Anjan [04 June 2014] -- Start
    '                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                Language.setLanguage(mstrModuleName)
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Voucher No. cannot be blank. Voucher No. is compulsory information."), Me)
    '                'Anjan [04 June 2014] -- End
    '                Return False
    '            End If

    '            If CInt(cboPaymentMode.SelectedValue) <= 0 Then
    '                'Anjan [04 June 2014] -- Start
    '                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                Language.setLanguage(mstrModuleName)
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Payment Mode is compulsory information. Please select Payment Mode to continue."), Me)
    '                'Anjan [04 June 2014] -- End
    '                Return False
    '            End If

    '            If CInt(cboPaymentBy.SelectedValue) <= 0 Then

    '                'Anjan [04 June 2014] -- Start
    '                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                Language.setLanguage(mstrModuleName)
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Payment By is compulsory information. Please select Payment By to continue."), Me)
    '                'Anjan [04 June 2014] -- End
    '                Return False
    '            End If

    '            Select Case CInt(cboPaymentMode.SelectedValue)
    '                Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER
    '                    If CInt(cboBankGroup.SelectedValue) <= 0 Then

    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), Me)
    '                        'Anjan [04 June 2014] -- End


    '                        Return False
    '                    End If
    '                    If CInt(cboBranch.SelectedValue) <= 0 Then
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Please select Bank Account. Bank Account is mandatory information."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    End If

    '                    If objEmpBank.isEmployeeBankExist(Me.ViewState("_Employeeunkid")) = False Then

    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    End If
    '                    ds = objEmpBank.GetEmployeeWithOverDistribution("List", Me.ViewState("_Employeeunkid"))
    '                    If ds.Tables("List").Rows.Count > 0 Then

    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), Me)
    '                        'Anjan [04 June 2014] -- End


    '                        Return False
    '                    End If

    '                    If txtChequeNo.Text = "" Then
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Cheque No. cannot be blank. Please enter the cheque information."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    End If
    '            End Select

    '            Select Case CInt(cboPaymentBy.SelectedValue)
    '                Case 1 'AMOUNT
    '                    Decimal.TryParse(txtAmount.Text, mDecAmount)
    '                    If mDecAmount <= 0 Then
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Amount cannot be blank. Amount is compulsory information."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    End If
    '                Case 2 'PERCENT VALUE
    '                    Decimal.TryParse(txtPercent.Text, mDecAmount)
    '                    If mDecAmount <= 0 Then
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Percent cannot be blank .Percent is compulsory information."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    ElseIf mDecAmount > 100 Then
    '                        msg.DisplayMessage("Percent cannot more than 100 %.Percent enter proper percentage.", Me)
    '                        Return False
    '                    End If
    '            End Select

    '            Select Case CInt(Me.ViewState("ReferenceId"))
    '                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
    '                    If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT Then
    '                        Dim mDecSetAmt As Decimal = 0
    '                        Decimal.TryParse(txtAmount.Text, mDecSetAmt)
    '                        Decimal.TryParse(txtPayment.Text, mDecAmount)

    '                        'Sohail (25 Mar 2015) -- Start
    '                        'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
    '                        'If mDecSetAmt < mDecAmount Then
    '                        If mDecSetAmt < mdecPaidCurrBalFormatedAmt Then
    '                            'Sohail (25 Mar 2015) -- End
    '                            'Anjan [04 June 2014] -- Start
    '                            'ENHANCEMENT : Implementing Language,requested by Andrew
    '                            Language.setLanguage(mstrModuleName)
    '                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, you cannot do any partial payment. Please do full payment."), Me)
    '                            'Anjan [04 June 2014] -- End
    '                            Return False
    '                        End If

    '                        'Nilay (28-Aug-2015) -- Start
    '                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '                    ElseIf Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED Then
    '                        Dim objLoan As New clsLoan_Advance
    '                        Dim dsLoan As DataSet = objLoan.GetLastLoanBalance("List", CInt(Me.ViewState("PaymentTranId")))
    '                        If dsLoan.Tables("List").Rows.Count > 0 Then
    '                            If dsLoan.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtdate.GetDate.Date) Then
    '                                Language.setLanguage(mstrModuleName)
    '                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Payment date should not less than last transactoin date") & " " & Format(eZeeDate.convertDate(dsLoan.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", Me)
    '                                Return False
    '                            End If
    '                        End If
    '                        'Nilay (28-Aug-2015) -- End

    '                    End If
    '            End Select



    '            Dim decBalAmt As Decimal
    '            If mdecBaseExRate <> 0 Then
    '                decBalAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
    '            Else
    '                decBalAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
    '            End If

    '            Decimal.TryParse(txtAmount.Text, mDecAmount)
    '            If mDecAmount > decBalAmt Then
    '                'Anjan [04 June 2014] -- Start
    '                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                Language.setLanguage(mstrModuleName)
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Amount cannot be greater than the Due Amount(") & decBalAmt.ToString & ").", Me)
    '                'Anjan [04 June 2014] -- End
    '                Return False
    '            End If

    '            'SHANI (09 Mar 2015) -- Start
    '            'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '            If CInt(cboPeriod.SelectedValue) <= 0 Then
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Please select period."), Me.Page)
    '                cboPeriod.Focus()
    '                Return False
    '            End If
    '            'SHANI (09 Mar 2015) -- End

    '            If dtdate.IsNull Then
    '                msg.DisplayMessage("Payment Date is mandatory information. Please enter Payment Date.", Me)
    '                Return False
    '            End If

    '            If dtdate.IsNull = False Then
    '                If IsDate(dtdate.GetDate) = False Then
    '                    msg.DisplayMessage("Payment Date is not in proper date format. Please check the date you have entered.", Me)
    '                    Return False
    '                End If
    '                If dtdate.GetDate < Me.ViewState("_Start_Date") OrElse dtdate.GetDate > CDate(Session("fin_enddate")).Date Then
    '                    'Anjan [04 June 2014] -- Start
    '                    'ENHANCEMENT : Implementing Language,requested by Andrew
    '                    Language.setLanguage(mstrModuleName)
    '                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Payment Date should be in between ") & CDate(Me.ViewState("_Start_Date")).Date.ToShortDateString & Language.getMessage(mstrModuleName, 28, " And ") & CDate(Session("fin_enddate")).Date.ToShortDateString & ".", Me)
    '                    'Anjan [04 June 2014] -- End
    '                    Return False
    '                End If

    '                'Sohail (12 Jan 2015) -- Start
    '                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    '                Dim objMaster As New clsMasterData
    '                Dim objCommon As New clscommom_period_Tran
    '                Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, CInt(Session("Fin_year")))
    '                If intFirstOpenPeriod > 0 Then
    '                    objCommon._Periodunkid = intFirstOpenPeriod
    '                    If dtdate.GetDate.Date < objCommon._Start_Date Then
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, Payment Date should be in between open periods and should not be less than") & " " & objCommon._Start_Date.ToShortDateString, Me)
    '                        Return False
    '                    End If
    '                End If

    '                'Sohail (09 Mar 2015) -- Start
    '                'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '                objCommon._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                If eZeeDate.convertDate(dtdate.GetDate.Date) < eZeeDate.convertDate(objCommon._Start_Date.Date) OrElse eZeeDate.convertDate(dtdate.GetDate.Date) > eZeeDate.convertDate(objCommon._End_Date.Date) Then
    '                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, Payment Date should be in between selected period start date and end date."), Me.Page)
    '                    Return False
    '                End If
    '                'Sohail (09 Mar 2015) -- End

    '                objMaster = Nothing
    '                objCommon = Nothing
    '                'Sohail (12 Jan 2015) -- End
    '            End If

    '            'Nilay (28-Aug-2015) -- Start
    '            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
    '            If CInt(Me.ViewState("PaymentTranId")) = clsPayment_tran.enPayTypeId.RECEIVED Then
    '                Dim strEmpID As String = ""
    '                Select Case CInt(Me.ViewState("ReferenceId"))
    '                    Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
    '                        strEmpID = objLoan_Advance._Employeeunkid.ToString
    '                    Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                        strEmpID = objEmployee_Saving._Employeeunkid.ToString
    '                End Select
    '                If strEmpID.Trim <> "" Then
    '                    Dim objTnA As New clsTnALeaveTran
    '                    If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpID, objCommon._End_Date, enModuleReference.Payroll) = True Then
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for selected employee."), Me)
    '                        cboPeriod.Focus()
    '                        Return False
    '                    End If
    '                End If
    '            End If
    '            'Nilay (28-Aug-2015) -- End


    '            If Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.PAYSLIP Then
    '                If Session("_DoNotAllowOverDeductionForPayment") = True Then
    '                    dtTable = (New clsTnALeaveTran).GetOverDeductionList("List", Me.ViewState("_Periodunkid"), False)
    '                    If dtTable.Rows.Count > 0 Then
    '                        'Anjan [04 June 2014] -- Start
    '                        'ENHANCEMENT : Implementing Language,requested by Andrew
    '                        Language.setLanguage(mstrModuleName)
    '                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry! You can not do Payment now. Reason : Some of the employees having Over Deduction in this month."), Me)
    '                        'Anjan [04 June 2014] -- End
    '                        Return False
    '                    End If
    '                End If
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            msg.DisplayError("IsValidate : " & ex.Message, Me)
    '        End Try
    '    End Function

    '    Private Sub SetValue()
    '        Try
    '            If Me.ViewState("PaymentTranId") > 0 Then
    '                objPaymentTran._Paymenttranunkid = Me.ViewState("PaymentTranId")
    '            End If
    '            objPaymentTran._Percentage = CDec(IIf(txtPercent.Text.Trim = "", 0, txtPercent.Text))
    '            objPaymentTran._Branchunkid = CInt(IIf(cboBranch.SelectedValue = "", 0, cboBranch.SelectedValue))
    '            objPaymentTran._Chequeno = txtChequeNo.Text
    '            If CInt(IIf(cboAccountNo.SelectedValue = "", 0, cboAccountNo.SelectedValue)) > 0 Then
    '                objPaymentTran._Accountno = cboAccountNo.Text
    '            Else
    '                objPaymentTran._Accountno = ""
    '            End If
    '            objPaymentTran._Employeeunkid = Me.ViewState("_Employeeunkid")
    '            If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED Then
    '                objPaymentTran._Is_Receipt = True
    '            Else
    '                objPaymentTran._Is_Receipt = False
    '            End If
    '            objPaymentTran._Paymentbyid = CInt(cboPaymentBy.SelectedValue)
    '            objPaymentTran._Paymentdate = dtdate.GetDate
    '            objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
    '            objPaymentTran._Paymentrefid = Me.ViewState("ReferenceId")
    '            objPaymentTran._Payreftranunkid = Me.ViewState("TransactonId")
    '            objPaymentTran._PaymentTypeId = Me.ViewState("PaymentTypeId")
    '            objPaymentTran._Periodunkid = Me.ViewState("_Periodunkid")
    '            objPaymentTran._Userunkid = Session("UserId")
    '            If Me.ViewState("PaymentTranId") > 0 Then
    '                objPaymentTran._Isvoid = objPaymentTran._Isvoid
    '                objPaymentTran._Voiddatetime = objPaymentTran._Voiddatetime
    '                objPaymentTran._Voiduserunkid = objPaymentTran._Voiduserunkid
    '            Else
    '                objPaymentTran._Isvoid = False
    '                objPaymentTran._Voiddatetime = Nothing
    '                objPaymentTran._Voiduserunkid = -1
    '            End If
    '            objPaymentTran._Voucherno = txtAgainstVoucher.Text
    '            objPaymentTran._Voucherref = txtVoucherNo.Text
    '            objPaymentTran._Isglobalpayment = False
    '            If Me.ViewState("sData") IsNot Nothing Then
    '                objPaymentTran._strData = Me.ViewState("sData").ToString.Replace("~", "|")
    '                objPaymentTran._IsFromStatus = CBool(Me.ViewState("IsFrom_Status"))
    '            End If
    '            objPaymentTran._Basecurrencyid = Me.ViewState("mintBaseCurrId")
    '            objPaymentTran._Baseexchangerate = mdecBaseExRate
    '            objPaymentTran._Paidcurrencyid = Me.ViewState("mintPaidCurrId")
    '            objPaymentTran._Expaidrate = mdecPaidExRate
    '            If mdecPaidCurrBalFormatedAmt = Format(CDec(txtAmount.Text), Session("fmtCurrency")) Then
    '                If mdecPaidExRate <> 0 Then
    '                    objPaymentTran._Amount = mdecPaidCurrBalanceAmt * mdecBaseExRate / mdecPaidExRate
    '                Else
    '                    objPaymentTran._Amount = mdecPaidCurrBalanceAmt * mdecBaseExRate
    '                End If
    '                objPaymentTran._Expaidamt = mdecPaidCurrBalanceAmt
    '            Else
    '                If mdecPaidExRate <> 0 Then
    '                    objPaymentTran._Amount = CDec(txtAmount.Text) * mdecBaseExRate / mdecPaidExRate
    '                Else
    '                    objPaymentTran._Amount = CDec(txtAmount.Text) * mdecBaseExRate
    '                End If
    '                objPaymentTran._Expaidamt = CDec(txtAmount.Text)
    '            End If
    '            objPaymentTran._Countryunkid = CInt(cboCurrency.SelectedValue)
    '            objPaymentTran._Isapproved = False
    '            objPaymentTran._Remarks = txtRemarks.Text.Trim 'Sohail (09 Mar 2015)
    '            'Nilay (28-Aug-2015) -- Start
    '            objPaymentTran._PaymentDate_Periodunkid = CInt(cboPeriod.SelectedValue)
    '            'Nilay (28-Aug-2015) -- End

    '        Catch ex As Exception
    '            msg.DisplayMessage("SetValue : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub FinalRepaymentAmt()
    '        Try
    '            Dim decReceived As Decimal = 0
    '            If Me.ViewState("PaymentTranId") > 0 Then
    '                objPaymentTran.Get_Payment_Total(Me.ViewState("TransactonId"), Me.ViewState("ReferenceId"), Me.ViewState("PaymentTypeId"), decReceived, False, True)
    '                txtPayment.Text = Format(CDec(Me.ViewState("_Balance_Amount")) + decReceived, Session("fmtCurrency"))
    '            Else
    '                objPaymentTran.Get_Payment_Total(Me.ViewState("TransactonId"), Me.ViewState("ReferenceId"), Me.ViewState("PaymentTypeId"), decReceived)
    '                txtPayment.Text = Format((CDec(Me.ViewState("_Balance_Amount")) + decReceived) - decReceived, Session("fmtCurrency"))
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("FinalRepaymentAmt : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Button's Events "

    '    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '        Dim blnFlag As Boolean = False
    '        Try
    '            If IsValidate() = False Then Exit Sub

    '            Call SetValue()

    '            If Me.ViewState("PaymentTranId") > 0 Then
    '                blnFlag = objPaymentTran.Update()
    '            Else
    '                blnFlag = objPaymentTran.Insert()
    '            End If

    '            Select Case CInt(Me.ViewState("ReferenceId"))
    '                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
    '                    If blnFlag = False And objPaymentTran._Message <> "" Then
    '                        msg.DisplayMessage(objPaymentTran._Message, Me)
    '                        Exit Sub
    '                    End If
    '                Case clsPayment_tran.enPaymentRefId.PAYSLIP
    '                    If blnFlag = False And objPaymentTran._Message <> "" Then
    '                        msg.DisplayMessage(objPaymentTran._Message, Me)
    '                        Exit Sub
    '                    Else
    '                        If Session("_SetPayslipPaymentApproval") = True Then
    '                            Dim intBankPaid As Integer = 0
    '                            Dim intCashPaid As Integer = 0
    '                            Dim decBankPaid As Decimal = 0
    '                            Dim decCashPaid As Decimal = 0
    '                            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
    '                                intCashPaid = 1
    '                                decCashPaid = CDec(Format(objPaymentTran._Expaidamt, Session("fmtCurrency")))
    '                            ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
    '                                intBankPaid = 1
    '                                decBankPaid = CDec(Format(objPaymentTran._Expaidamt, GUI.fmtCurrency))
    '                            End If
    '                            'S.SANDEEP [ 28 JAN 2014 ] -- START
    '                            'objPaymentTran.SendMailToApprover(True, Session("UserId"), objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency)
    '                            'Sohail (09 Mar 2015) -- Start
    '                            'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '                            'objPaymentTran.SendMailToApprover(True, Session("UserId"), objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency, , , , , enLogin_Mode.MGR_SELF_SERVICE, 0)
    '                            objPaymentTran.SendMailToApprover(True, Session("UserId"), objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, , , , , enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(cboCurrency.SelectedValue), Session("ArutiSelfServiceURL"))
    '                            'Sohail (09 Mar 2015) -- End
    '                            'S.SANDEEP [ 28 JAN 2014 ] -- END
    '                        End If
    '                    End If
    '                Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                    If blnFlag = False And objPaymentTran._Message <> "" Then
    '                        msg.DisplayMessage(objPaymentTran._Message, Me)
    '                        Exit Sub
    '                    End If
    '            End Select
    '            If Me.ViewState("IsFrom_Status") Is Nothing Then
    '                Dim strURL As String = String.Empty
    '                strURL &= "|" & Me.ViewState("ReferenceId") & "|" & Me.ViewState("TransactonId") & "|" & Me.ViewState("PaymentTypeId")
    '                strURL = Mid(strURL, 2)
    '                Response.Redirect("~/Payment/wPg_Payment_List.aspx?ProcessId=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)))
    '            Else
    '                Response.Redirect("~/Loan_Savings/wPg_LA_List.aspx")
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("btnSave_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try

    '            'SHANI [09 Mar 2015]-START
    '            'Enhancement - REDESIGN SELF SERVICE.
    '            'Response.Redirect("~\UserHome.aspx")
    '            If Me.ViewState("IsFrom_Status") Is Nothing Then
    '                Dim strURL As String = String.Empty
    '                strURL &= "|" & Me.ViewState("ReferenceId") & "|" & Me.ViewState("TransactonId") & "|" & Me.ViewState("PaymentTypeId")
    '                strURL = Mid(strURL, 2)
    '                Response.Redirect("~/Payment/wPg_Payment_List.aspx?ProcessId=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)))
    '            Else
    '                Response.Redirect("~/Loan_Savings/wPg_LA_List.aspx")
    '            End If
    '            'SHANI [09 Mar 2015]--END
    '        Catch ex As Exception
    '            msg.DisplayError("btnClose_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub


    '    'SHANI [01 FEB 2015]-START
    '    'Enhancement - REDESIGN SELF SERVICE.
    '    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    '    Try
    '    '        Response.Redirect("~\UserHome.aspx")
    '    '    Catch ex As Exception
    '    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    '    End Try
    '    'End Sub
    '    'SHANI [01 FEB 2015]--END

    '#End Region

    '#Region " Controls "

    '    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
    '        Dim dsCombos As New DataSet
    '        Dim objCompanyBank As New clsCompany_Bank_tran
    '        Try
    '            dsCombos = objCompanyBank.GetComboList(Session("CompanyUnkId"), 2, "Branch", " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
    '            With cboBranch
    '                .DataValueField = "id"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Branch")
    '                .DataBind()
    '                If Me.ViewState("PaymentTranId") <= 0 Then
    '                    .SelectedValue = 0
    '                Else
    '                    .SelectedValue = Me.ViewState("_Branchunkid")
    '                End If
    '            End With
    '        Catch ex As Exception
    '            msg.DisplayError("cboBankGroup_SelectedIndexChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
    '        Dim objCompanyBank As New clsCompany_Bank_tran
    '        Dim dsCombos As New DataSet
    '        Try
    '            dsCombos = objCompanyBank.GetComboListBankAccount("Account", Session("CompanyUnkId"), True, CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
    '            With cboAccountNo
    '                .DataValueField = "id"
    '                .DataTextField = "Name"
    '                .DataSource = dsCombos.Tables("Account")
    '                .DataBind()
    '                If Me.ViewState("PaymentTranId") <= 0 Then
    '                    .SelectedValue = 0
    '                Else
    '                    .SelectedValue = Me.ViewState("PaymentTranId")
    '                End If
    '            End With
    '        Catch ex As Exception
    '            msg.DisplayError("cboBranch_SelectedIndexChanged : " & ex.Message, Me)
    '        Finally
    '            objCompanyBank = Nothing
    '        End Try
    '    End Sub

    '    Private Sub cboPaymentBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymentBy.SelectedIndexChanged
    '        Try
    '            txtPercent.Text = ""
    '            Select Case CInt(cboPaymentBy.SelectedValue)
    '                Case 1
    '                    txtPercent.Enabled = False
    '                    txtAmount.Read_Only = False
    '                Case 2  'Percentage
    '                    txtPercent.Enabled = True
    '                    txtAmount.Read_Only = True
    '            End Select
    '        Catch ex As Exception
    '            msg.DisplayError("cboPaymentBy_SelectedIndexChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged
    '        Try
    '            Select Case CInt(cboPaymentMode.SelectedValue)
    '                Case 0, 1
    '                    cboBankGroup.SelectedValue = 0
    '                    cboBankGroup.Enabled = False
    '                    cboBranch.SelectedValue = 0
    '                    cboBranch.Enabled = False
    '                    txtChequeNo.Text = ""
    '                    txtChequeNo.Enabled = False
    '                    cboAccountNo.SelectedValue = 0
    '                    cboAccountNo.Enabled = False
    '                Case Else
    '                    cboBankGroup.Enabled = True
    '                    cboBranch.Enabled = True
    '                    txtChequeNo.Enabled = True
    '                    cboAccountNo.Enabled = True
    '            End Select
    '        Catch ex As Exception
    '            msg.DisplayError("cboPaymentMode_SelectedIndexChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
    '        Dim objExRate As New clsExchangeRate
    '        Dim dsList As DataSet
    '        Dim dtTable As DataTable
    '        Try
    '            lblText.Text = ""

    '            lblAmount.Text = Language.getMessage(mstrModuleName, 31, "Amount")
    '            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtdate.GetDate, True)
    '            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
    '            If dtTable.Rows.Count > 0 Then
    '                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
    '                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
    '                Me.ViewState.Add("mintPaidCurrId", CInt(dtTable.Rows(0).Item("exchangerateunkid")))
    '                lblText.Text = Format(mdecBaseExRate, Session("fmtCurrency")) & " " & Me.ViewState("BaseCurrSign") & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
    '                lblAmount.Text &= " (" & dtTable.Rows(0).Item("currency_sign").ToString & ")"
    '            Else
    '                mdecBaseExRate = 0
    '                Me.ViewState.Add("mdecPaidExRate", 0)
    '            End If

    '            'Sohail (09 Mar 2015) -- Start
    '            'Enhancement - Provide Multi Currency on Saving scheme contribution.
    '            If mdecBaseExRate <> 0 Then
    '                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
    '            Else
    '                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
    '            End If
    '            mdecPaidCurrBalFormatedAmt = CDec(Format(mdecPaidCurrBalanceAmt, GUI.fmtCurrency))
    '            If CInt(cboPaymentBy.SelectedValue) = enPaymentBy.Percentage Then
    '                Dim mdblPercent As Double = 0
    '                If txtPercent.Text.Trim <> "" Then
    '                    Double.TryParse(txtPercent.Text, mdblPercent)
    '                End If
    '                txtAmountTag = mdecPaidCurrBalanceAmt * mdblPercent / 100
    '            Else
    '                txtAmountTag = mdecPaidCurrBalanceAmt
    '            End If
    '            txtAmount.Text = Format(CDec(txtAmountTag), GUI.fmtCurrency)
    '            'Sohail (09 Mar 2015) -- End

    '        Catch ex As Exception
    '            msg.DisplayError("cboCurrency_SelectedIndexChanged : " & ex.Message, Me)
    '        End Try

    '    End Sub

    '    Protected Sub dtdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtdate.TextChanged
    '        Try
    '            Call cboCurrency_SelectedIndexChanged(Nothing, Nothing)
    '        Catch ex As Exception
    '            msg.DisplayError("dtdate_TextChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub txtPercent_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercent.TextChanged
    '        Try
    '            Dim mdblPercent As Double = 0 : Dim decPercentAmount As Decimal = 0
    '            If txtPercent.Text <> "" Then
    '                Double.TryParse(txtPercent.Text, mdblPercent)

    '                If mdblPercent <= 0 Then

    '                    'Anjan [04 June 2014] -- Start
    '                    'ENHANCEMENT : Implementing Language,requested by Andrew
    '                    Language.setLanguage(mstrModuleName)
    '                    msg.DisplayError(Language.getMessage(mstrModuleName, 1, "Please enter Proper Percentage."), Me)
    '                    'Anjan [04 June 2014] -- End
    '                    txtPercent.Text = ""
    '                    Exit Sub
    '                ElseIf mdblPercent > 100 Then
    '                    'Anjan [04 June 2014] -- Start
    '                    'ENHANCEMENT : Implementing Language,requested by Andrew
    '                    Language.setLanguage(mstrModuleName)
    '                    msg.DisplayError(Language.getMessage(mstrModuleName, 1, "Please enter Proper Percentage."), Me)
    '                    txtPercent.Text = ""
    '                    Exit Sub
    '                End If
    '                If mdecBaseExRate <> 0 Then
    '                    decPercentAmount = (mdecBaseCurrBalanceAmt * mdecPaidExRate) / mdecBaseExRate
    '                Else
    '                    decPercentAmount = mdecBaseCurrBalanceAmt * mdecPaidExRate
    '                End If
    '                decPercentAmount = (decPercentAmount * mdblPercent) / 100
    '                txtAmount.Text = Format(decPercentAmount, Session("fmtCurrency"))
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("txtPercent_TextChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub


    '    'SHANI [01 FEB 2015]-START
    '    'Enhancement - REDESIGN SELF SERVICE.
    '    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    '    Try
    '    '        Response.Redirect("~/Payment/wPg_Payment_List.aspx")
    '    '    Catch ex As Exception
    '    '        msg.DisplayError("ToolbarEntry1_GridMode_Click :- " & ex.Message, Me)
    '    '    End Try
    '    'End Sub
    '    'SHANI [01 FEB 2015]--END

    '#End Region

#End Region

    
End Class