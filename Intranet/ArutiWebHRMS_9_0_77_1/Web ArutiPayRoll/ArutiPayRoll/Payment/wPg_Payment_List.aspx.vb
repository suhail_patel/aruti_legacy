﻿'************************************************************************************************************************************
'Purpose     : Recode due to complexity
'Date        : 10/Sep/2015
'Modified By : Nilay Mistry
'************************************************************************************************************************************

Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class Payroll_wPg_Payment_List
    Inherits Basepage

#Region "Private Varibles"
    Private Shared ReadOnly mstrModuleName As String = "frmPaymentList"
    Private DisplayMessage As New CommonCodes
    Private objPaymentTran As New clsPayment_tran

    Private mstrProcessId As String = ""
    Private mintTransactionId As Integer = -1
    Private mintReferenceId As Integer = -1
    Private mintPaymentTypeId As Integer = -1
    Private mdtLoanSavingEffectiveDate As Date
    Private mintPaymenttranunkid As Integer = -1
    Private mdtPeriod_Startdate As Date
    Private mdtPeriod_Enddate As Date
    Private dtActAdj As New DataTable 'Sohail (23 May 2017)

#End Region

#Region "Page's Events"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            Call SetLanguage()

            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objPaymentTran._WebFormName = "frmPaymentList"
            'objPaymentTran._WebIP = CStr(Session("IP_ADD"))
            'objPaymentTran._WebHostName = CStr(Session("HOST_NAME"))
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    objPaymentTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'End If

            If IsPostBack = False Then
                
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If Request.QueryString.Count > 0 Then
                '    mstrProcessId = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString))
                '    Dim array() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                '    If array.Length > 0 Then
                '        mintReferenceId = CInt(array(0))
                '        mintTransactionId = CInt(array(1))
                '        mintPaymentTypeId = CInt(array(2))
                '        mdtLoanSavingEffectiveDate = CDate(array(3))
                '    End If
                'Else
                '    Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
                'End If
                If Session("ReferenceId") IsNot Nothing Then
                    mintReferenceId = CInt(Session("ReferenceId"))
                    'Session("ReferenceId") = Nothing
                    End If
                If Session("TransactionId") IsNot Nothing Then
                    mintTransactionId = CInt(Session("TransactionId"))
                    Session("TransactionId") = Nothing
                End If
                If Session("PaymentTypeId") IsNot Nothing Then
                    mintPaymentTypeId = CInt(Session("PaymentTypeId"))
                    Session("PaymentTypeId") = Nothing
                End If
                If Session("LoanSavingEffectiveDate") IsNot Nothing Then
                    mdtLoanSavingEffectiveDate = CDate(Session("LoanSavingEffectiveDate")).Date
                    Session("LoanSavingEffectiveDate") = Nothing
                End If
                'Nilay (06-Aug-2016) -- END

                Call FillCombo()
                Call FillList()
                Call SetVisibility()

            Else
                mstrProcessId = CStr(Me.ViewState("ProcessId"))
                mintReferenceId = CInt(Me.ViewState("ReferenceId"))
                mintTransactionId = CInt(Me.ViewState("TransactonId"))
                mintPaymentTypeId = CInt(Me.ViewState("PaymentTypeId"))
                mintPaymenttranunkid = CInt(Me.ViewState("Paymenttranunkid"))
                mdtLoanSavingEffectiveDate = CDate(Me.ViewState("LoanSavingEffectiveDate"))
                dtActAdj = CType(ViewState("dtActAdj"), DataTable) 'Sohail (23 May 2017)
            End If

            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP OrElse mintReferenceId = clsPayment_tran.enPaymentRefId.LOAN OrElse _
                   mintReferenceId = clsPayment_tran.enPaymentRefId.ADVANCE OrElse mintReferenceId = clsPayment_tran.enPaymentRefId.SAVINGS Then
                dgvPayment.Columns(0).Visible = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ProcessId") = mstrProcessId
            Me.ViewState("ReferenceId") = mintReferenceId
            Me.ViewState("TransactonId") = mintTransactionId
            Me.ViewState("PaymentTypeId") = mintPaymentTypeId
            Me.ViewState("Paymenttranunkid") = mintPaymenttranunkid
            Me.ViewState("LoanSavingEffectiveDate") = mdtLoanSavingEffectiveDate
            Me.ViewState("dtActAdj") = dtActAdj 'Sohail (23 May 2017)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreInit:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()

        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objMasterData As New clsMasterData
        Dim dsList As New DataSet

        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                     "Emp", _
                                                     True)

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("Emp")
                    .DataBind()
                    .SelectedValue = "0"
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If
            

            dsList = objMasterData.getComboListPAYYEAR(CInt(Session("Fin_year")), Session("FinancialYear_Name").ToString, CInt(Session("CompanyUnkId")), _
                                                       "Year", True)
            With cboPayYear
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Year")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, _
                                               CDate(Session("fin_startdate")), "Period", True)
            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()

        Dim dsList As New DataSet
        Dim dtTable As New DataTable
        Dim strSearch As String = String.Empty
        Dim objExrate As New clsExchangeRate

        Try
            'Session("AllowToViewPaymentList") Session("AccessLevelFilterString")
            If CBool(Session("AllowToViewPaymentList")) = True Then

                dsList = objPaymentTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), mdtPeriod_Startdate, mdtPeriod_Enddate, _
                                                Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), _
                                                "Payment", mintReferenceId, mintTransactionId, mintPaymentTypeId, True)

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearch &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboPayPeriod.SelectedValue) > 0 Then
                    strSearch &= "AND periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
                End If

                If CInt(cboPayYear.SelectedValue) > 0 Then
                    strSearch &= "AND yearunkid = " & CInt(cboPayYear.SelectedValue) & " "
                End If

                If txtPaidAmount.Text.Trim <> "" AndAlso txtPaidAmountTo.Text.Trim <> "" Then
                    If CDec(txtPaidAmount.Text) > 0 AndAlso CDec(txtPaidAmountTo.Text) > 0 Then
                        strSearch &= "AND Amount >= " & CDec(txtPaidAmount.Text) & " AND Amount <= " & CDec(txtPaidAmountTo.Text) & " "
                    End If
                End If
                
                If txtVoucherNo.Text.Trim <> "" Then
                    strSearch &= "AND Voc LIKE '%" & CStr(txtVoucherNo.Text) & "%'" & " "
                End If

                If dtpPaymentDate.IsNull = False Then
                    strSearch &= "AND Date = '" & eZeeDate.convertDate(dtpPaymentDate.GetDate.Date) & "'" & "  "
                End If

                If strSearch.Length > 0 Then
                    strSearch = strSearch.Substring(3)
                    dtTable = New DataView(dsList.Tables("Payment"), strSearch, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = dsList.Tables("Payment")
                End If

                dgvPayment.AutoGenerateColumns = False

                If dtTable IsNot Nothing Then
                    dgvPayment.DataSource = dtTable
                    dgvPayment.DataKeyField = "paymenttranunkid"
                    dgvPayment.DataBind()
                Else
                    dgvPayment.DataSource = New List(Of String)
                    dgvPayment.DataBind()
                End If


            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If mintReferenceId = clsPayment_tran.enPaymentRefId.LOAN Or mintReferenceId = clsPayment_tran.enPaymentRefId.ADVANCE Then

                If mintPaymentTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                    btnNew.Enabled = CBool(Session("AddLoanAdvancePayment"))
                    'dgvPayment.Columns(0).Visible = CBool(Session("EditLoanAdvancePayment"))
                    dgvPayment.Columns(1).Visible = CBool(Session("DeleteLoanAdvancePayment"))

                ElseIf mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                    btnNew.Enabled = CBool(Session("AddLoanAdvanceReceived"))
                    'dgvPayment.Columns(0).Visible = CBool(Session("EditLoanAdvanceReceived"))
                    dgvPayment.Columns(1).Visible = CBool(Session("DeleteLoanAdvanceReceived"))
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 10, "List of Receipts")
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 11, "Receipt List")
                End If
            ElseIf mintReferenceId = clsPayment_tran.enPaymentRefId.SAVINGS Then
                If mintPaymentTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then
                    btnNew.Enabled = CBool(Session("AddSavingsDeposit"))
                    dgvPayment.Columns(1).Visible = CBool(Session("DeleteSavingsDeposit"))
                Else
                    btnNew.Enabled = CBool(Session("AddSavingsPayment"))
                    dgvPayment.Columns(1).Visible = CBool(Session("DeleteSavingsPayment"))
                End If
            Else
                btnNew.Enabled = CBool(Session("AddPayment"))
                dgvPayment.Columns(1).Visible = CBool(Session("DeletePayment"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayMessage("SetVisibility:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " ComboBox Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                mdtPeriod_Startdate = CDate(eZeeDate.convertDate(objPeriod._Start_Date))
                mdtPeriod_Enddate = CDate(eZeeDate.convertDate(objPeriod._End_Date))
            Else
                mdtPeriod_Startdate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriod_Enddate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPayPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPayPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "DataGrid's Events"

    Protected Sub dgvPayment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvPayment.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemIndex >= 0 Then

                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                    Dim objExRate As New clsExchangeRate
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)
                    objExRate._ExchangeRateunkid = CInt(e.Item.Cells(9).Text)
                    e.Item.Cells(8).Text = objExRate._Currency_Sign
                    objExRate = Nothing

                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    'If Session("DateFormat").ToString <> Nothing Then
                    '    e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).Date.ToString(Session("DateFormat").ToString)
                    'Else
                    '    e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).ToShortDateString
                    'End If

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                    e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).Date.ToShortDateString
                    'Pinkal (16-Apr-2016) -- End

                    'Nilay (01-Apr-2016) -- End

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        If mintReferenceId = clsPayment_tran.enPaymentRefId.LOAN Or mintReferenceId = clsPayment_tran.enPaymentRefId.ADVANCE Then
                            If mintPaymentTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                                dgvPayment.Columns(0).Visible = False
                                dgvPayment.Columns(1).Visible = CBool(Session("DeleteLoanAdvancePayment"))
                            ElseIf mintPaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                                dgvPayment.Columns(0).Visible = CBool(Session("EditLoanAdvanceReceived"))
                                dgvPayment.Columns(1).Visible = CBool(Session("DeleteLoanAdvanceReceived"))
                            End If
                        ElseIf mintReferenceId = clsPayment_tran.enPaymentRefId.SAVINGS Then
                            dgvPayment.Columns(0).Visible = False
                            If mintPaymentTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then
                                dgvPayment.Columns(1).Visible = CBool(Session("DeleteSavingsDeposit"))
                            Else
                                dgvPayment.Columns(1).Visible = CBool(Session("DeleteSavingsPayment"))
                            End If
                        Else
                            dgvPayment.Columns(0).Visible = False
                            dgvPayment.Columns(1).Visible = CBool(Session("DeletePayment"))
                        End If
                    ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        dgvPayment.Columns(0).Visible = False
                        dgvPayment.Columns(1).Visible = False
                    End If

                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvPayment_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvPayment_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvPayment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvPayment.ItemCommand

        Dim intMaxId As Integer = -1
        Dim strFilter As String = ""
        Dim dsList As New DataSet

        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                If e.Item.ItemIndex >= 0 Then

                    If mintReferenceId <> clsPayment_tran.enPaymentRefId.PAYSLIP AndAlso mintReferenceId <> clsPayment_tran.enPaymentRefId.LOAN AndAlso _
                       mintReferenceId <> clsPayment_tran.enPaymentRefId.ADVANCE AndAlso mintReferenceId <> clsPayment_tran.enPaymentRefId.SAVINGS Then

                        objPaymentTran.Get_Max_PaymentId(mintTransactionId, mintReferenceId, mintPaymentTypeId, intMaxId)
                        If CInt(e.Item.Cells(12).Text) = intMaxId Then
                            dgvPayment.Columns(0).Visible = True
                        Else
                            dgvPayment.Columns(0).Visible = False
                        End If
                    End If

                    mintPaymenttranunkid = CInt(dgvPayment.DataKeys(e.Item.ItemIndex))
                    objPaymentTran._Paymenttranunkid = CInt(dgvPayment.DataKeys(e.Item.ItemIndex))

                    'Sohail (23 May 2017) -- Start
                    'Enhancement - 67.1 - Link budget with Payroll.
                    dtActAdj = New DataTable
                    Dim objTranPeriod As New clscommom_period_Tran
                    objTranPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(e.Item.Cells(10).Text)
                    'Sohail (23 May 2017) -- End

                    If e.CommandName.ToUpper = "EDIT" Then

                        If objPaymentTran._Paymentdate.Date < CDate(Session("fin_startdate")).Date Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded."), Me)
                            Exit Sub
                        End If

                        If objPaymentTran._Paymentrefid = clsPayment_tran.enPaymentRefId.PAYSLIP Then

                            strFilter = " prpayment_tran.periodunkid = " & CInt(e.Item.Cells(10).Text) & " AND isauthorized = 1 AND prpayment_tran.paymenttranunkid = " & CInt(e.Item.Cells(12).Text) & " "
                            'Sohail (23 May 2017) -- Start
                            'Enhancement - 67.1 - Link budget with Payroll.
                            'dsList = objPaymentTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                            '                                mdtPeriod_Startdate, mdtPeriod_Enddate, Session("UserAccessModeSetting").ToString, _
                            '                                True, CBool(Session("IsIncludeInactiveEmp")), "Authorized", _
                            '                                clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, _
                            '                                False, strFilter)
                            dsList = objPaymentTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                            objTranPeriod._Start_Date, objTranPeriod._End_Date, Session("UserAccessModeSetting").ToString, _
                                                            True, CBool(Session("IsIncludeInactiveEmp")), "Authorized", _
                                                            clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, _
                                                            False, strFilter)
                            'Sohail (23 May 2017) -- End
                            If dsList.Tables("Authorized").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry! This payments is already Authorized. Please Void Authorized payment for selected employee."), Me)
                                Exit Try
                            End If
                        End If

                        If CBool(Session("SetPayslipPaymentApproval")) = True Then
                            If objPaymentTran._Isapproved = True Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot edit this Payment. Reason: This Payment is Final Approved."), Me)
                                Exit Sub
                            End If

                            Dim objPaymentApproval As New clsPayment_approval_tran
                            dsList = objPaymentApproval.GetList("Approved", True, e.Item.Cells(12).Text.ToString, , , "levelunkid <> -1 AND priority <> -1 ")
                            If dsList.Tables("Approved").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, You cannot edit this Payment. Reason: This Payment is Approved by some Approvers."), Me)
                                Exit Sub
                            End If
                            objPaymentApproval = Nothing

                        End If

                        Select Case objPaymentTran._Paymentrefid
                            Case clsPayment_tran.enPaymentRefId.LOAN
                                Select Case objPaymentTran._PaymentTypeId
                                    Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
                                        Dim ObjLoan_Advance As New clsLoan_Advance
                                        ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                                        If ObjLoan_Advance._Isloan = True Then
                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Net_Amount Then
                                                Language.setLanguage(mstrModuleName)
                                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
                                                Exit Sub
                                            End If
                                        End If
                                End Select
                            Case clsPayment_tran.enPaymentRefId.ADVANCE
                                Select Case objPaymentTran._PaymentTypeId
                                    Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
                                        Dim ObjLoan_Advance As New clsLoan_Advance
                                        ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                                        If ObjLoan_Advance._Isloan = False Then
                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
                                                Language.setLanguage(mstrModuleName)
                                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
                                                Exit Sub
                                            End If
                                        End If
                                End Select

                            Case clsPayment_tran.enPaymentRefId.PAYSLIP

                            Case clsPayment_tran.enPaymentRefId.SAVINGS
                                Select Case objPaymentTran._PaymentTypeId
                                    Case clsPayment_tran.enPayTypeId.REPAYMENT
                                End Select
                        End Select

                        Dim strURL As String = ""

                        strURL = mintPaymenttranunkid.ToString & "|" & mstrProcessId & "|" & e.Item.ItemIndex.ToString

                        Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Add_Edit_Payment.aspx?" & _
                                          HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)), False)
                    End If

                    If e.CommandName.ToUpper = "DELETE" Then

                        If objPaymentTran._Paymentdate.Date < CDate(Session("fin_startdate")).Date Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Payment. Reason: This Payment is brought forwarded."), Me)
                            Exit Sub
                        End If

                        If objPaymentTran._Paymentrefid = clsPayment_tran.enPaymentRefId.PAYSLIP Then

                            strFilter = " prpayment_tran.periodunkid = " & CInt(e.Item.Cells(10).Text) & " AND isauthorized = 1 AND prpayment_tran.paymenttranunkid = " & CInt(e.Item.Cells(12).Text) & " "

                            'Sohail (23 May 2017) -- Start
                            'Enhancement - 67.1 - Link budget with Payroll.
                            'dsList = objPaymentTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                            '                                mdtPeriod_Startdate, mdtPeriod_Enddate, Session("UserAccessModeSetting").ToString, True, _
                            '                                CBool(Session("IsIncludeInactiveEmp")), "Authorized", _
                            '                                clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, False, strFilter)
                            dsList = objPaymentTran.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                            objTranPeriod._Start_Date, objTranPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, _
                                                            CBool(Session("IsIncludeInactiveEmp")), "Authorized", _
                                                            clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, False, strFilter)
                            'Sohail (23 May 2017) -- End
                            If dsList.Tables("Authorized").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry! This payments is already Authorized. Please Void Authorized payment for selected employee."), Me)
                                Exit Try
                            End If
                        End If

                        If CBool(Session("SetPayslipPaymentApproval")) = True Then
                            If objPaymentTran._Isapproved = True Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete this Payment. Reason: This Payment is Final Approved."), Me)
                                Exit Sub
                            End If

                            Dim objPaymentApproval As New clsPayment_approval_tran
                            dsList = objPaymentApproval.GetList("Approved", True, e.Item.Cells(12).Text.ToString, , , "levelunkid <> -1 AND priority <> -1 ")
                            If dsList.Tables("Approved").Rows.Count > 0 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot delete this Payment. Reason: This Payment is Approved by some Approvers."), Me)
                                Exit Sub
                            End If
                            objPaymentApproval = Nothing

                        End If

                        Select Case objPaymentTran._Paymentrefid

                            Case clsPayment_tran.enPaymentRefId.LOAN

                                Select Case objPaymentTran._PaymentTypeId
                                    
                                    Case clsPayment_tran.enPayTypeId.PAYMENT

                                        Dim ObjLoan_Advance As New clsLoan_Advance
                                        ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                                        If ObjLoan_Advance._Isloan = True Then
                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Loan_Amount Then
                                                Language.setLanguage(mstrModuleName)
                                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
                                                Exit Sub
                                            End If
                                        End If

                                    Case clsPayment_tran.enPayTypeId.RECEIVED
                                        Dim objPeriod As New clscommom_period_Tran
                                        objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(e.Item.Cells(13).Text)
                                        If objPeriod._Statusid = 0 Then ' 0 For Close
                                            Language.setLanguage(mstrModuleName)
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot delete this Loan/Advance payment. Reason : Payment date period is already closed."), Me)
                                            Exit Sub
                                        Else
                                            Dim objTnA As New clsTnALeaveTran
                                            If objTnA.IsPayrollProcessDone(CInt(e.Item.Cells(13).Text), e.Item.Cells(14).Text.ToString, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                                                Language.setLanguage(mstrModuleName)
                                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee."), Me)
                                                Exit Sub
                                            End If
                                        End If

                                        Dim objLoanAdvance As New clsLoan_Advance
                                        objLoanAdvance._Loanadvancetranunkid = mintTransactionId
                                        If objLoanAdvance._LoanStatus = 4 Then
                                            DisplayMessage.DisplayMessage("You cannot delete recieved payment. Reason : Loan/Advance status is Completed.", Me)
                                            Exit Sub
                                        End If

                                End Select

                            Case clsPayment_tran.enPaymentRefId.ADVANCE

                                Select Case objPaymentTran._PaymentTypeId
                                    
                                    Case clsPayment_tran.enPayTypeId.PAYMENT

                                        Dim ObjLoan_Advance As New clsLoan_Advance
                                        ObjLoan_Advance._Loanadvancetranunkid = objPaymentTran._Payreftranunkid
                                        If ObjLoan_Advance._Isloan = False Then
                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
                                                Language.setLanguage(mstrModuleName)
                                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
                                                Exit Sub
                                            End If
                                        End If

                                    Case clsPayment_tran.enPayTypeId.RECEIVED
                                        Dim objPeriod As New clscommom_period_Tran
                                        objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(e.Item.Cells(13).Text)
                                        If objPeriod._Statusid = 0 Then ' 0 For Close
                                            Language.setLanguage(mstrModuleName)
                                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot delete this Loan/Advance payment. Reason : Payment date period is already closed."), Me)
                                            Exit Sub
                                        Else
                                            Dim objTnA As New clsTnALeaveTran
                                            If objTnA.IsPayrollProcessDone(CInt(e.Item.Cells(13).Text), e.Item.Cells(14).Text.ToString, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                                                Language.setLanguage(mstrModuleName)
                                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot delete this Loan/Advance payment. Reason : Process Payroll is already done for the last date of selected period for the selected employee."), Me)
                                                Exit Sub
                                            End If
                                            'Sohail (27 Feb 2018) -- Start
                                            'Internal Issue : Dont allow to delete loan / advance one it is completed in 70.1.
                                            Dim objLoan As New clsLoan_Advance
                                            Dim ds As DataSet = objLoan.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), True, objPeriod._Start_Date, objPeriod._End_Date, objPeriod._End_Date, "", "List", "LN.loanadvancetranunkid = " & objPaymentTran._Payreftranunkid & " ", 0, False, "")
                                            If ds.Tables(0).Rows.Count > 0 Then
                                                If CInt(ds.Tables(0).Rows(0).Item("loan_statusunkid")) = CInt(enLoanStatus.WRITTEN_OFF) OrElse CInt(ds.Tables(0).Rows(0).Item("loan_statusunkid")) = CInt(enLoanStatus.COMPLETED) Then
                                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, You cannot delete this Loan/Advance payment. Reason : Loan/Advance is already completed."), Me)
                                                    Exit Sub
                                                End If
                                            End If
                                            'Sohail (27 Feb 2018) -- End
                                        End If
                                        'Sohail (07 May 2015) -- End
                                End Select

                            Case clsPayment_tran.enPaymentRefId.PAYSLIP

                                'Sohail (23 May 2017) -- Start
                                'Enhancement - 67.1 - Link budget with Payroll.
                                Dim objBudget As New clsBudget_MasterNew
                                Dim intBudgetId As Integer = 0
                                dsList = objBudget.GetComboList("List", False, True, )
                                If dsList.Tables(0).Rows.Count > 0 Then
                                    intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
                                End If
                                If intBudgetId > 0 Then
                                    Dim mstrAnalysis_Fields As String = ""
                                    Dim mstrAnalysis_Join As String = ""
                                    Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                                    Dim mstrReport_GroupName As String = ""
                                    Dim mstrAnalysis_TableName As String = ""
                                    Dim mstrAnalysis_CodeField As String = ""
                                    objBudget._Budgetunkid = intBudgetId
                                    Dim strEmpList As String = e.Item.Cells(14).Text.ToString()
                                    Dim objBudgetCodes As New clsBudgetcodes_master
                                    Dim dsEmp As DataSet = Nothing
                                    If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                                        Dim objEmp As New clsEmployee_Master
                                        Dim objBudget_Tran As New clsBudget_TranNew
                                        Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                                        Controls_AnalysisBy.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(e.Item.Cells(13).Text), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                                        dsEmp = objEmp.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objTranPeriod._Start_Date, objTranPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                                    Else
                                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(e.Item.Cells(13).Text), intBudgetId, 0, 0, strEmpList, 1)
                                    End If

                                    If dsList.Tables(0).Rows.Count > 0 Then
                                        dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                                        dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                                        dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                                        dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                                        dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                                        dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                                        dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                                        Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                                        Dim mdicActivityCode As New Dictionary(Of Integer, String)
                                        Dim objActivity As New clsfundactivity_Tran
                                        Dim dsAct As DataSet = objActivity.GetList("Activity")
                                        Dim objActAdjust As New clsFundActivityAdjustment_Tran
                                        Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, objTranPeriod._End_Date)
                                        mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                                        mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                                        Dim objPayment As New clsPayment_tran
                                        Dim dsBalance As DataSet = objPayment.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objTranPeriod._Start_Date, objTranPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, False, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, " prpayment_tran.paymenttranunkid = " & CInt(e.Item.Cells(12).Text) & " ", Nothing)
                                        If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                                            dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid")}
                                            dsBalance.Tables(0).Merge(dsEmp.Tables(0))
                                        End If
                                        Dim intActivityId As Integer
                                        Dim decTotal As Decimal = 0
                                        Dim decActBal As Decimal = 0
                                        Dim blnShowValidationList As Boolean = False
                                        Dim blnIsExeed As Boolean = False
                                        For Each pair In mdicActivityCode
                                            intActivityId = pair.Key
                                            decTotal = 0
                                            decActBal = 0
                                            blnIsExeed = False

                                            If mdicActivity.ContainsKey(intActivityId) = True Then
                                                decActBal = mdicActivity.Item(intActivityId)
                                            End If

                                            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                                            If lstRow.Count > 0 Then
                                                For Each dRow As DataRow In lstRow
                                                    Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                                    Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("Amount")) = False) Select (CDec(p.Item("Amount")))).Sum
                                                    decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                                Next

                                                'If decTotal > decActBal Then
                                                '    blnShowValidationList = True
                                                '    blnIsExeed = True
                                                'End If
                                                Dim dr As DataRow = dtActAdj.NewRow
                                                dr.Item("fundactivityunkid") = intActivityId
                                                dr.Item("transactiondate") = eZeeDate.convertDate(objTranPeriod._End_Date).ToString()
                                                dr.Item("remark") = Language.getMessage(mstrModuleName, 9, "Salary payments Voided for the period of") & " " & cboPayPeriod.Text
                                                dr.Item("isexeed") = blnIsExeed
                                                dr.Item("Activity Code") = pair.Value
                                                dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                                                dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                                                dtActAdj.Rows.Add(dr)
                                            End If
                                        Next

                                        If blnShowValidationList = True Then
                                            Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                                            For Each r In dr
                                                dtActAdj.Rows.Remove(r)
                                            Next
                                            dtActAdj.AcceptChanges()
                                            dtActAdj.Columns.Remove("fundactivityunkid")
                                            dtActAdj.Columns.Remove("transactiondate")
                                            dtActAdj.Columns.Remove("remark")
                                            dtActAdj.Columns.Remove("isexeed")
                                            popupValidationList.Message = Language.getMessage(mstrModuleName, 17, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")
                                            popupValidationList.Data_Table = dtActAdj
                                            popupValidationList.Show()
                                            Exit Try
                                        End If
                                    End If
                                End If
                                'Sohail (23 May 2017) -- End

                            Case clsPayment_tran.enPaymentRefId.SAVINGS

                                Select Case objPaymentTran._PaymentTypeId

                                    Case clsPayment_tran.enPayTypeId.REPAYMENT
                                        
                                End Select
                        End Select

                        Language.setLanguage(mstrModuleName)
                        popupDelReason.Title = Language.getMessage(mstrModuleName, 5, "Are you sure you want to delete this Payment?")
                        popupDelReason.Reason = ""
                        popupDelReason.Show()

                    End If

                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvPayment_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvPayment_ItemCommand:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            If mintReferenceId = clsPayment_tran.enPaymentRefId.PAYSLIP AndAlso dgvPayment.Items.Count > 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, You can not make new payment. Reason: Payment is already done in this period."), Me)
                Exit Try
            End If

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'Dim strURL As String = ""
            'Dim mintPaymentListCount As Integer = CInt(dgvPayment.Items.Count)
            'strURL = "-1" & "|" & mstrProcessId & "|" & mintPaymentListCount.ToString
            'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Add_Edit_Payment.aspx?" & _
            '                              HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)), False)

            Session("ReferenceId") = mintReferenceId
            Session("TransactionId") = mintTransactionId
            Session("PaymentTypeId") = mintPaymentTypeId
            Session("LoanSavingEffectiveDate") = mdtLoanSavingEffectiveDate
            Session("PaymentListCount") = CInt(dgvPayment.Items.Count)

            Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Add_Edit_Payment.aspx", False)
            'Nilay (06-Aug-2016) -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnNew_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Select Case mintReferenceId

                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    Session("ReferenceId") = Nothing
                    'Nilay (06-Aug-2016) -- END
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    'Nilay (06-Aug-2016) -- Start
                    'CHANGES : Replace Query String with Session and ViewState
                    Session("ReferenceId") = Nothing
                    'Nilay (06-Aug-2016) -- END
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/wPg_Employee_Saving_List.aspx", False)
            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            cboPayPeriod.SelectedValue = "0"
            cboPayYear.SelectedValue = "0"
            txtPaidAmount.Text = ""
            txtPaidAmountTo.Text = ""
            txtVoucherNo.Text = ""
            dtpPaymentDate.SetDate = Nothing
            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSearch_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDelReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelReason.buttonDelReasonYes_Click
        Try
            objPaymentTran._Isvoid = True
            objPaymentTran._Voiduserunkid = CInt(Session("UserId"))
            objPaymentTran._Voiddatetime = DateAndTime.Now.Date
            objPaymentTran._Voidreason = popupDelReason.Reason
            'Nilay (05-May-2016) -- Start
            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objPaymentTran._WebIP = Session("IP_ADD").ToString
            'objPaymentTran._WebFormName = "frmPaymentList"
            'objPaymentTran._WebHostName = Session("HOST_NAME").ToString
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    objPaymentTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'End If
            'Nilay (05-May-2016) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'If objPaymentTran.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                         mdtPeriod_Startdate, mdtPeriod_Enddate, Session("UserAccessModeSetting").ToString, True, _
            '                         CBool(Session("IsIncludeInactiveEmp")), mintPaymenttranunkid, DateAndTime.Now.Date, True) = False Then

            With objPaymentTran
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._Companyunkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If objPaymentTran.Delete(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                     mdtPeriod_Startdate, mdtPeriod_Enddate, Session("UserAccessModeSetting").ToString, True, _
                                     CBool(Session("IsIncludeInactiveEmp")), mintPaymenttranunkid, DateAndTime.Now.Date, True, , dtActAdj) = False Then
                'Sohail (23 May 2017) -- End
                DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
                Exit Sub
            Else
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
            End If

            'Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDelReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDelReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END


            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lblPaidAmount.Text = Language._Object.getCaption(Me.lblPaidAmount.ID, Me.lblPaidAmount.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.ID, Me.lblPaymentDate.Text)
            Me.lblVoucherno.Text = Language._Object.getCaption(Me.lblVoucherno.ID, Me.lblVoucherno.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.ID, Me.lblPayYear.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.dgvPayment.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(2).FooterText, Me.dgvPayment.Columns(2).HeaderText)
            Me.dgvPayment.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(3).FooterText, Me.dgvPayment.Columns(3).HeaderText)
            Me.dgvPayment.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(4).FooterText, Me.dgvPayment.Columns(4).HeaderText)
            Me.dgvPayment.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(5).FooterText, Me.dgvPayment.Columns(5).HeaderText)
            Me.dgvPayment.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(6).FooterText, Me.dgvPayment.Columns(6).HeaderText)
            Me.dgvPayment.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(7).FooterText, Me.dgvPayment.Columns(7).HeaderText)
            Me.dgvPayment.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvPayment.Columns(8).FooterText, Me.dgvPayment.Columns(8).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

    'Nilay (28-Aug-2015) -- Start
#Region "Old Code Before 10-Sep-2015"
    '#Region " Private Varaibles "

    '    Private objPaymentTran As New clsPayment_tran
    '    Dim msg As New CommonCodes

    '    'Anjan [04 June 2014] -- Start
    '    'ENHANCEMENT : Implementing Language,requested by Andrew
    '    Private ReadOnly mstrModuleName As String = "frmPaymentList"
    '    'Anjan [04 June 2014] -- End


    '#End Region

    '#Region " Form's Events "

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try
    '            If Session("clsuser") Is Nothing Then
    '                Exit Sub
    '            End If

    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
    '                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '                Exit Sub
    '            End If


    '            'Anjan [04 June 2014] -- Start
    '            'ENHANCEMENT : Implementing Language,requested by Andrew
    '            Call SetLanguage()
    '            'Anjan [04 June 2014] -- End


    '            Blank_ModuleName()
    '            StrModuleName2 = "mnuLoan_Advance_Savings"
    '            objPaymentTran._WebFormName = "frmPaymentList"
    '            objPaymentTran._WebIP = Session("IP_ADD")
    '            objPaymentTran._WebHostName = Session("HOST_NAME")
    '            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                objPaymentTran._LoginEmployeeUnkid = Session("Employeeunkid")
    '            End If

    '            Dim clsuser As New User
    '            If (Page.IsPostBack = False) Then
    '                clsuser = CType(Session("clsuser"), User)
    '                If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                    'Sohail (30 Mar 2015) -- Start
    '                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '                    'Me.ViewState("Empunkid") = clsuser.UserID
    '                    Me.ViewState("Empunkid") = Session("UserId")
    '                    'Sohail (30 Mar 2015) -- End
    '                Else
    '                    'Sohail (30 Mar 2015) -- Start
    '                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
    '                    Me.ViewState("Empunkid") = Session("Employeeunkid")
    '                    'Sohail (30 Mar 2015) -- End
    '                End If
    '                Call FillCombo()
    '            End If

    '            If (Request.QueryString("ProcessId") <> "") Then
    '                Call IsFrom_List()
    '                Call Fill_Info()
    '                Call Fill_Grid()
    '            End If
    '            Call SetVisibility()

    '            'SHANI [01 FEB 2015]-START
    '            'Enhancement - REDESIGN SELF SERVICE.
    '            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '            'SHANI [01 FEB 2015]--END
    '        Catch ex As Exception
    '            msg.DisplayError("Procedure Page_Load : " & ex.Message, Me)
    '        Finally

    '            'SHANI [01 FEB 2015]-START
    '            'Enhancement - REDESIGN SELF SERVICE.
    '            'ToolbarEntry1.VisibleImageSaprator1(False)
    '            'ToolbarEntry1.VisibleImageSaprator2(False)
    '            'ToolbarEntry1.VisibleImageSaprator3(False)
    '            'ToolbarEntry1.VisibleImageSaprator4(False)
    '            'SHANI [01 FEB 2015]--END
    '        End Try
    '    End Sub

    '    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'ToolbarEntry1.VisibleSaveButton(False)
    '        'ToolbarEntry1.VisibleDeleteButton(False)
    '        'ToolbarEntry1.VisibleCancelButton(False)
    '        'ToolbarEntry1.VisibleExitButton(False)
    '        'SHANI [01 FEB 2015]--END
    '    End Sub

    '    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '        Me.IsLoginRequired = True
    '    End Sub

    '    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
    '        Try
    '            If Not IsPostBack Then
    '                Call IsFrom_List()
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("Page_PreRenderComplete Event : " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Button's Event(s) "

    '    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
    '        Try
    '            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Then
    '                msg.DisplayMessage("Please Select Employee to Continue", Me)
    '                Exit Sub
    '            End If
    '            Call Fill_Grid()
    '        Catch ex As Exception
    '            msg.DisplayError("btnSearch_Click Event : " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
    '        Try
    '            Call ClearObject()
    '            dgView.DataSource = Nothing
    '            dgView.DataBind()
    '        Catch ex As Exception
    '            msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub


    '    'SHANI [01 FEB 2015]-START
    '    'Enhancement - REDESIGN SELF SERVICE.


    '    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    '    Try
    '    '        Response.Redirect("~\UserHome.aspx")
    '    '    Catch ex As Exception
    '    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    '    End Try
    '    'End Sub
    '    'SHANI [01 FEB 2015]--END

    '    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
    '        Try
    '            If Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.PAYSLIP AndAlso CType(Me.ViewState("mTable"), DataTable).Rows.Count > 0 Then

    '                'Anjan [04 June 2014] -- Start
    '                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                Language.setLanguage(mstrModuleName)
    '                msg.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, You can not make new payment. Reason: Payment is already done in this period."), Me)
    '                'Anjan [04 June 2014] -- End
    '                Exit Sub
    '            End If
    '            Dim sURL As String = String.Empty
    '            sURL = Me.ViewState("mProcessId") & "|" & "0" & "|" & "0"
    '            Response.Redirect("~/Payment/wPg_Add_Edit_Payment.aspx?ProcessId=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(sURL)))
    '        Catch ex As Exception
    '            msg.DisplayError("btnNew_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub voidReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles voidReason.buttonDelReasonYes_Click
    '        Try
    '            objPaymentTran._Isvoid = True
    '            objPaymentTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            objPaymentTran._Voiduserunkid = Session("UserId")
    '            objPaymentTran._Voidreason = voidReason.Reason
    '            objPaymentTran.Delete(CInt(Me.ViewState("Unkid")), Session("AccessLevelFilterString"))
    '            Call Fill_Grid()
    '        Catch ex As Exception
    '            msg.DisplayError("voidReason_buttonDelReasonYes_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Private Method(s) & Function(s) "

    '    Private Sub FillCombo()
    '        Dim dsCombos As New DataSet
    '        Dim objEmployee As New clsEmployee_Master
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim objMasterData As New clsMasterData
    '        Try

    '            If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                If CBool(Session("IsIncludeInactiveEmp")) = False Then
    '                    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
    '                Else
    '                    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
    '                End If
    '                With cboEmployee
    '                    .DataValueField = "employeeunkid"
    '                    .DataTextField = "employeename"
    '                    .DataSource = dsCombos.Tables("Employee")
    '                    .DataBind()
    '                    .SelectedValue = 0
    '                End With
    '            Else
    '                Dim objglobalassess = New GlobalAccess
    '                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '                cboEmployee.DataSource = objglobalassess.ListOfEmployee
    '                cboEmployee.DataTextField = "loginname"
    '                cboEmployee.DataValueField = "employeeunkid"
    '                cboEmployee.DataBind()
    '            End If

    '            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True, , , , Session("Database_Name"))
    '            With cboPayPeriod
    '                .DataValueField = "periodunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Period")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With

    '            dsCombos = objMasterData.getComboListPAYYEAR("Year", True, Session("Fin_year"), Session("FinancialYear_Name"))
    '            With cboPayYear
    '                .DataValueField = "Id"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Year")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With

    '        Catch ex As Exception
    '            msg.DisplayError("FillCombo : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub SetVisibility()
    '        Try
    '            If Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.LOAN Or Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE Then
    '                If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT Then
    '                    btnNew.Visible = Session("AddLoanAdvancePayment")

    '                    'Pinkal (12-Feb-2015) -- Start
    '                    'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '                    'Sohail (25 Mar 2015) -- Start
    '                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
    '                    'dgView.Columns(0).Visible = CBool(Session("EditLoanAdvancePayment"))
    '                    dgView.Columns(0).Visible = False
    '                    'Sohail (25 Mar 2015) -- End
    '                    dgView.Columns(1).Visible = CBool(Session("DeleteLoanAdvancePayment"))
    '                    'Pinkal (12-Feb-2015) -- End

    '                ElseIf Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED Then
    '                    btnNew.Visible = Session("AddLoanAdvanceReceived")

    '                    'Pinkal (12-Feb-2015) -- Start
    '                    'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '                    dgView.Columns(0).Visible = CBool(Session("EditLoanAdvanceReceived"))
    '                    dgView.Columns(1).Visible = CBool(Session("DeleteLoanAdvanceReceived"))
    '                    'Pinkal (12-Feb-2015) -- End


    '                End If
    '            ElseIf Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.SAVINGS Then
    '                btnNew.Visible = Session("AddSavingsPayment")
    '            Else
    '                btnNew.Visible = Session("AddPayment")
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("SetVisibility : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub Fill_Grid()
    '        Dim dsPayment As New DataSet
    '        Dim dtTable As New DataTable
    '        Dim StrSearching As String = String.Empty
    '        Try
    '            If Session("AllowToViewPaymentList") = False Then Exit Sub

    '            dsPayment = objPaymentTran.GetList("Payment", Me.ViewState("ReferenceId"), Me.ViewState("TransactonId"), Me.ViewState("PaymentTypeId"), , Session("AccessLevelFilterString"))

    '            If CInt(cboEmployee.SelectedValue) > 0 Then
    '                StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
    '            End If

    '            If CInt(cboPayPeriod.SelectedValue) > 0 Then
    '                StrSearching &= "AND periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " "
    '            End If

    '            If CInt(cboPayYear.SelectedValue) > 0 Then
    '                StrSearching &= "AND yearunkid = " & CInt(cboPayYear.SelectedValue) & " "
    '            End If

    '            Dim mDec_Amt_From, mDec_Amt_To As Decimal
    '            mDec_Amt_From = 0 : mDec_Amt_To = 0

    '            Decimal.TryParse(txtAmount.Text, mDec_Amt_From) : Decimal.TryParse(txtTo.Text, mDec_Amt_To)

    '            If mDec_Amt_From <> 0 AndAlso mDec_Amt_To <> 0 Then
    '                StrSearching &= "AND Amount >= " & mDec_Amt_From & " AND Amount <= " & mDec_Amt_To & " "
    '            End If

    '            If txtVoucherNo.Text.Trim <> "" Then
    '                StrSearching &= "AND Voc LIKE '%" & CStr(txtVoucherNo.Text) & "%'" & " "
    '            End If

    '            If dtdate.IsNull = False Then
    '                StrSearching &= "AND Date = '" & eZeeDate.convertDate(dtdate.GetDate) & "'" & "  "
    '            End If

    '            If StrSearching.Length > 0 Then
    '                StrSearching = StrSearching.Substring(3)
    '                dtTable = New DataView(dsPayment.Tables("Payment"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtTable = dsPayment.Tables("Payment")
    '            End If

    '            If Me.ViewState("mTable") IsNot Nothing Then
    '                Me.ViewState("mTable") = dtTable
    '            Else
    '                Me.ViewState.Add("mTable", dtTable)
    '            End If


    '            If dtTable IsNot Nothing Then
    '                dgView.DataSource = dtTable
    '                dgView.DataKeyField = "paymenttranunkid"
    '                dgView.DataBind()
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError("Fill_Grid : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Function IsFrom_List() As Boolean
    '        Try
    '            If (Request.QueryString("ProcessId") <> "") Then
    '                Me.ViewState.Add("mProcessId", Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("ProcessId"))))
    '                Dim sProcess() As String = Nothing
    '                sProcess = Server.UrlDecode(clsCrypto.Dicrypt(Request.QueryString("ProcessId"))).Split("|")
    '                If sProcess.Length > 0 Then
    '                    Me.ViewState.Add("ReferenceId", sProcess(0))
    '                    Me.ViewState.Add("TransactonId", sProcess(1))
    '                    Me.ViewState.Add("PaymentTypeId", sProcess(2))
    '                End If
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("IsFrom_List : " & ex.Message, Me)
    '        End Try
    '    End Function

    '    Private Sub ClearObject()
    '        Try
    '            txtAmount.Text = ""
    '            txtTo.Text = ""
    '            txtVoucherNo.Text = ""
    '            If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                cboEmployee.SelectedValue = 0
    '            End If
    '            cboPayPeriod.SelectedValue = 0
    '            cboPayYear.SelectedValue = 0
    '            dtdate.SetDate = Nothing
    '        Catch ex As Exception
    '            msg.DisplayError("ClearObject : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub Fill_Info()
    '        Dim objLoan_Advance As New clsLoan_Advance
    '        Try
    '            If Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE Or Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE Then
    '                objLoan_Advance._Loanadvancetranunkid = Me.ViewState("TransactonId")
    '                cboEmployee.SelectedValue = objLoan_Advance._Employeeunkid
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("Fill_Info : " & ex.Message, Me)
    '        Finally
    '            objLoan_Advance = Nothing
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Control's Event(s) "

    '    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
    '        Try
    '            dgView.CurrentPageIndex = e.NewPageIndex
    '            Call Fill_Grid()
    '        Catch ex As Exception
    '            msg.DisplayError("dgView_PageIndexChanged : " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
    '        Try
    '            If (e.Item.ItemIndex >= 0) Then
    '                If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                    If Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.LOAN Or Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE Then
    '                        If Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.PAYMENT Then
    '                            'Sohail (25 Mar 2015) -- Start
    '                            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
    '                            'dgView.Columns(0).Visible = Session("EditLoanAdvancePayment")
    '                            dgView.Columns(0).Visible = False
    '                            'Sohail (25 Mar 2015) -- End
    '                            dgView.Columns(1).Visible = Session("DeleteLoanAdvancePayment")
    '                        ElseIf Me.ViewState("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED Then
    '                            dgView.Columns(0).Visible = Session("EditLoanAdvanceReceived")
    '                            dgView.Columns(1).Visible = Session("DeleteLoanAdvanceReceived")
    '                        End If
    '                    ElseIf Me.ViewState("ReferenceId") = clsPayment_tran.enPaymentRefId.SAVINGS Then
    '                        'Sohail (25 Mar 2015) -- Start
    '                        'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
    '                        'dgView.Columns(0).Visible = Session("EditSavingsPayment")
    '                        dgView.Columns(0).Visible = False
    '                        'Sohail (25 Mar 2015) -- End
    '                        dgView.Columns(1).Visible = Session("DeleteSavingsPayment")
    '                    Else
    '                        dgView.Columns(0).Visible = False
    '                        dgView.Columns(1).Visible = Session("DeletePayment")
    '                    End If
    '                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                    dgView.Columns(0).Visible = False
    '                    dgView.Columns(1).Visible = False
    '                End If

    '                'S.SANDEEP [ 31 DEC 2013 ] -- START
    '                'e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).ToShortDateString
    '                If Session("DateFormat") <> Nothing Then
    '                    e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).Date.ToString(Session("DateFormat"))
    '                Else
    '                    e.Item.Cells(2).Text = eZeeDate.convertDate(e.Item.Cells(2).Text).ToShortDateString
    '                End If
    '                'S.SANDEEP [ 31 DEC 2013 ] -- END


    '                e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency"))
    '                Dim objExRate As New clsExchangeRate
    '                objExRate._ExchangeRateunkid = CInt(e.Item.Cells(9).Text)
    '                e.Item.Cells(8).Text = objExRate._Currency_Sign
    '                objExRate = Nothing
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("dgView_ItemDataBound : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
    '        Try
    '            If e.Item.Cells.Count > 0 Then
    '                Select Case e.CommandName.ToString.ToUpper
    '                    Case "SELECT"
    '                        'S.SANDEEP [ 20 JAN 2014 ] -- START
    '                        'If CDate(e.Item.Cells(11).Text).Date < CDate(Session("fin_startdate")).Date Then
    '                        '    msg.DisplayMessage("Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded.", Me)
    '                        '    Exit Sub
    '                        'End If

    '                        Dim dtDate As Date = Nothing
    '                        If Me.ViewState("mTable") IsNot Nothing Then
    '                            Dim dRow() As DataRow = CType(Me.ViewState("mTable"), DataTable).Select("paymenttranunkid = '" & CInt(dgView.DataKeys(e.Item.ItemIndex)) & "'")
    '                            If dRow.Length > 0 Then
    '                                dtDate = CDate(eZeeDate.convertDate(dRow(0).Item("Date").ToString)).Date
    '                            Else
    '                                dtDate = e.Item.Cells(11).Text
    '                            End If
    '                        Else
    '                            dtDate = e.Item.Cells(11).Text
    '                        End If

    '                        If dtDate.Date < CDate(Session("fin_startdate")).Date Then

    '                            'Anjan [04 June 2014] -- Start
    '                            'ENHANCEMENT : Implementing Language,requested by Andrew
    '                            Language.setLanguage(mstrModuleName)
    '                            msg.DisplayMessage("Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded.", Me)
    '                            'Anjan [04 June 2014] -- End
    '                            Exit Sub
    '                        End If
    '                        'S.SANDEEP [ 20 JAN 2014 ] -- END



    '                        Dim objPay As New clsPayment_tran
    '                        objPay._Paymenttranunkid = CInt(dgView.DataKeys(e.Item.ItemIndex))
    '                        Select Case objPay._Paymentrefid
    '                            Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
    '                                Select Case objPay._PaymentTypeId
    '                                    Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
    '                                        Dim ObjLoan_Advance As New clsLoan_Advance
    '                                        ObjLoan_Advance._Loanadvancetranunkid = objPay._Payreftranunkid
    '                                        If ObjLoan_Advance._Isloan = True Then
    '                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Net_Amount Then

    '                                                'Anjan [04 June 2014] -- Start
    '                                                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                                                Language.setLanguage(mstrModuleName)
    '                                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
    '                                                'Anjan [04 June 2014] -- End


    '                                                Exit Sub
    '                                            End If
    '                                        Else
    '                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
    '                                                'Anjan [04 June 2014] -- Start
    '                                                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                                                Language.setLanguage(mstrModuleName)
    '                                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot edit this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
    '                                                'Anjan [04 June 2014] -- End
    '                                                Exit Sub
    '                                            End If
    '                                        End If
    '                                End Select
    '                            Case clsPayment_tran.enPaymentRefId.ADVANCE
    '                            Case clsPayment_tran.enPaymentRefId.PAYSLIP
    '                            Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                        End Select
    '                        objPay = Nothing

    '                        Dim sURL As String = String.Empty
    '                        sURL = Me.ViewState("mProcessId") & "|" & CInt(dgView.DataKeys(e.Item.ItemIndex)).ToString & "|" & e.Item.ItemIndex
    '                        Response.Redirect("~/Payment/wPg_Add_Edit_Payment.aspx?ProcessId=" & HttpUtility.UrlEncode(clsCrypto.Encrypt(sURL)))

    '                    Case "DELETE"

    '                        'S.SANDEEP [ 20 JAN 2014 ] -- START
    '                        'If CDate(e.Item.Cells(11).Text).Date < CDate(Session("fin_startdate")).Date Then
    '                        '    msg.DisplayMessage("Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded.", Me)
    '                        '    Exit Sub
    '                        'End If

    '                        Dim dtDate As Date = Nothing
    '                        If Me.ViewState("mTable") IsNot Nothing Then
    '                            Dim dRow() As DataRow = CType(Me.ViewState("mTable"), DataTable).Select("paymenttranunkid = '" & CInt(dgView.DataKeys(e.Item.ItemIndex)) & "'")
    '                            If dRow.Length > 0 Then
    '                                dtDate = CDate(eZeeDate.convertDate(dRow(0).Item("Date").ToString)).Date
    '                            Else
    '                                dtDate = e.Item.Cells(11).Text
    '                            End If
    '                        Else
    '                            dtDate = e.Item.Cells(11).Text
    '                        End If

    '                        If dtDate.Date < CDate(Session("fin_startdate")).Date Then
    '                            'Anjan [04 June 2014] -- Start
    '                            'ENHANCEMENT : Implementing Language,requested by Andrew
    '                            Language.setLanguage(mstrModuleName)
    '                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, You cannot edit this Payment. Reason: This Payment is brought forwarded."), Me)
    '                            'Anjan [04 June 2014] -- End
    '                            Exit Sub
    '                        End If
    '                        'S.SANDEEP [ 20 JAN 2014 ] -- END

    '                        Dim objPay As New clsPayment_tran
    '                        objPay._Paymenttranunkid = CInt(dgView.DataKeys(e.Item.ItemIndex))
    '                        Select Case objPay._Paymentrefid
    '                            Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
    '                                Select Case objPay._PaymentTypeId
    '                                    Case clsPayment_tran.enPayTypeId.PAYMENT, clsPayment_tran.enPayTypeId.RECEIVED
    '                                        Dim ObjLoan_Advance As New clsLoan_Advance
    '                                        ObjLoan_Advance._Loanadvancetranunkid = objPay._Payreftranunkid
    '                                        If ObjLoan_Advance._Isloan = True Then
    '                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Net_Amount Then
    '                                                'Anjan [04 June 2014] -- Start
    '                                                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                                                Language.setLanguage(mstrModuleName)
    '                                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
    '                                                'Anjan [04 June 2014] -- End
    '                                                Exit Sub
    '                                            End If
    '                                        Else
    '                                            If ObjLoan_Advance._Balance_Amount < ObjLoan_Advance._Advance_Amount Then
    '                                                'Anjan [04 June 2014] -- Start
    '                                                'ENHANCEMENT : Implementing Language,requested by Andrew
    '                                                Language.setLanguage(mstrModuleName)
    '                                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete this Loan/Advance payment. Reason : Deduction or Repayment is already done against this."), Me)
    '                                                'Anjan [04 June 2014] -- End
    '                                                Exit Sub
    '                                            End If
    '                                        End If
    '                                End Select
    '                            Case clsPayment_tran.enPaymentRefId.ADVANCE
    '                            Case clsPayment_tran.enPaymentRefId.PAYSLIP
    '                            Case clsPayment_tran.enPaymentRefId.SAVINGS
    '                        End Select
    '                        objPay = Nothing
    '                        Me.ViewState.Add("Unkid", dgView.DataKeys(e.Item.ItemIndex))
    '                        voidReason.Show()

    '                End Select
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError("dgView_ItemCommand : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region " ToolBar Event "

    '    'SHANI [01 FEB 2015]-START
    '    'Enhancement - REDESIGN SELF SERVICE.
    '    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    '    Try
    '    '        If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '    '            ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '    '            Exit Sub
    '    '        End If
    '    '        Response.Redirect("~\Payment\wPg_Add_Edit_Payment.aspx")
    '    '    Catch ex As Exception
    '    '        msg.DisplayError("ToolbarEntry1_FormMode_Click :- " & ex.Message, Me)
    '    '    End Try
    '    'End Sub
    '    'SHANI [01 FEB 2015]--END
    '#End Region
#End Region
    'Nilay (28-Aug-2015) -- End

    
End Class