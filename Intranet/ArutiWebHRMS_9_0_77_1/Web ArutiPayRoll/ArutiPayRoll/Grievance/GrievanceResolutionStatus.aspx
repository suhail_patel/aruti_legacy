﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="GrievanceResolutionStatus.aspx.vb"
    Inherits="Grievance_GrievanceResolutionStatus" Title="Grievance Resolution Status" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Resolution Status"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblFilter" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                <div Class="row2">
                                    <div style="width: 12%;" class="ib">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee Name" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 22%;" class="ib">
                                        <asp:DropDownList ID="drpEmployee" runat="server" Width="250px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 10%;" class="ib">
                                        <asp:Label ID="lblGrrefno" runat="server" Text="Ref No." Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 18%;" class="ib">
                                        <asp:DropDownList ID="drpRefno" runat="server" Width="200px">
                                        </asp:DropDownList>
                                    </div>
                                    
                                    <div style="width: 10%;" class="ib">
                                        <asp:Label ID="lblApproverStatus" runat="server" Text="Approver Status" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 18%;" class="ib">
                                        <asp:DropDownList ID="drpApproverStatus" runat="server" Width="200px">
                                        </asp:DropDownList>
                                    </div>
                                    
                                </div>
                                
                                <div class="row2">
                                <div style="width: 12%;" class="ib">
                                        <asp:Label ID="lblEmployeeStatus" runat="server" Text="Employee Status" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 22%;" class="ib">
                                        <asp:DropDownList ID="drpEmployeeStatus" runat="server" Width="250px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="btn-default">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                        <div id="Div1" class="panel-default">
                            <div id="Div2" class="panel-body-default" style="position: relative">
                                <div class="row2">
                                    <div style="width: 100%">
                                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto">
                                            <asp:GridView ID="GvPreviousResponse" runat="server" AutoGenerateColumns="False"
                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:BoundField DataField="levelname" HeaderText="Approver Level" HeaderStyle-Width="10%"
                                                        FooterText="colhlevelname"></asp:BoundField>
                                                    <asp:BoundField DataField="name" HeaderText="Approver Name" HeaderStyle-Width="10%"
                                                        FooterText="colhApproverName"></asp:BoundField>
                                                    <asp:BoundField DataField="ApprovalResponse" HeaderText="Approver Resolution" FooterText="colhApproverStatus">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="qualifyremark" HeaderText="Qualifing Remark" FooterText="colhqualifyremark">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="responseremark" HeaderText="Response Remark" FooterText="colhresponseremark">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EmployeeStatus" HeaderText="Employee Status" FooterText="colhemployeestatus">
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EmpResponseRemark" HeaderText="Employee Response" FooterText="colhempresponseremark">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
