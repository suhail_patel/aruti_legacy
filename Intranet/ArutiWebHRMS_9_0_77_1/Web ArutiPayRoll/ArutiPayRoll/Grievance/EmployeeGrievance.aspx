﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="EmployeeGrievance.aspx.vb"
    Inherits="Grievance_EmployeeGrievance" Title="Employee Grievance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
    <%-- <script>
    function FileUploadChangeEvent() {
    var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
    if (fupld != null)
        fileUpLoadChange();    
}
    </script>--%>
    <%--'S.SANDEEP |16-MAY-2019| -- END--%>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script>
        function IsValidAttach() {
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }

    </script>

    <style type="text/css">
       
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="My Grievance"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblListGrDatefrom" runat="server" Text="Grievance Date From " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <uc1:DateCtrl ID="DtListGrDatefrom" runat="server" />
                                        </div>
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblListGrType" runat="server" Text="Grievance Type" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <asp:DropDownList ID="drpListGrType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblListGrDateto" runat="server" Text="Grievance Date To" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <uc1:DateCtrl ID="DtListGrDateto" runat="server" />
                                        </div>
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblListGrrefno" runat="server" Text="Ref No." Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <asp:TextBox ID="txtListGrrefno" runat="server" Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnListNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div style="width: 100%">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" >
                            <div id="scrollable-container" style="width: 100%; height: 350px; overflow: auto">
                                <asp:GridView ID="gvGrievanceList" runat="server" DataKeyNames="grievancemasterunkid,issubmitted"
                                    AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                    ShowFooter="false">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit" ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" CommandArgument='<%#Eval("grievancemasterunkid")%>'
                                                    ToolTip="Edit" OnClick="lnkedit_Click">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                    OnClick="lnkdelete_Click" CommandArgument='<%#Eval("grievancemasterunkid")%>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkviewresponse" runat="server" ToolTip="View Response" OnClick="lnkviewreport_Click"
                                                                        CommandArgument='<%#Eval("grievancemasterunkid")%>'>
                                                <i class="fa fa-eye"></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Grievance Ref No." DataField="Grievancerefno" ItemStyle-VerticalAlign="Top"
                                            FooterText="colhgrierefno" ItemStyle-Width="5%" />
                                        <asp:BoundField HeaderText="Grievance Type" DataField="Grievancetype" ItemStyle-VerticalAlign="Top"
                                            FooterText="colhgrietype" ItemStyle-Width="5%" />
                                        <asp:BoundField HeaderText="Date" DataField="grievancedate" ItemStyle-VerticalAlign="Top"
                                            FooterText="colhgriedate" ItemStyle-Width="8%"/>
                                              <asp:BoundField HeaderText="Against Employee" DataField="Againstemployee" ItemStyle-VerticalAlign="Top"
                                            FooterText="colhgrieagainstemp" ItemStyle-Width="8%"/>
                                        <asp:BoundField HeaderText="Status" DataField="Status" ItemStyle-VerticalAlign="Top"
                                            FooterText="colhgriestatus" ItemStyle-Width="8%"/>
                                        <asp:BoundField HeaderText="Grievance Description" DataField="grievance_description"
                                            ItemStyle-VerticalAlign="Top" ItemStyle-Width="15%" FooterText="colhgriedesc" />
                                        <asp:BoundField HeaderText="Grievance Relief Wanted" DataField="grievance_reliefwanted"
                                            ItemStyle-VerticalAlign="Top" ItemStyle-Width="15%" FooterText="colhgriereliefwanted" />
                                      
                                    </Columns>
                                </asp:GridView>
                            </div>
                                            </asp:Panel>
                                        </div>
                                        <%--'Hemant (29 May 2019) -- Start--%>
                                        <div class="btn-default">
                                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                        <%--'Hemant (29 May 2019) -- End--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupEmployeeGrievance" BackgroundCssClass="modalBackground"
                        TargetControlID="lblpopupEmployee" runat="server" PopupControlID="pnlGreApprover"
                        DropShadow="false" CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlGreApprover" runat="server" CssClass="newpopup" Style="display: none;
                        width: 800px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblCancelText1" Text="Grievance Add/ Edit" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <div class="row2">
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblPopUpEmployee" runat="server" Text="Employee" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <asp:DropDownList ID="drppopupEmployee" runat="server" Width="220px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblpopupdate" runat="server" Text="Date" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <uc1:DateCtrl ID="popupGrievancedate" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblpopupGreType" runat="server" Text="Grievance Type" />
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <asp:DropDownList ID="drppopupGreType" runat="server" Width="220px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblpopupRefno" runat="server" Text="Ref No." />
                                        </div>
                                        <div style="width: 28%;" class="ib">
                                            <asp:TextBox ID="txtpopupRefno" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 99%;">
                                            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                                <cc1:TabPanel runat="server" HeaderText="Grievance Description" ID="TabGre">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtpopupGreDesc" Width="99%" runat="server" Rows="10" TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel runat="server" HeaderText="Relief Wanted" ID="TabRelifWanted">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtpopupRelifWanted" Width="99%" runat="server" Rows="10" TextMode="MultiLine"></asp:TextBox>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel runat="server" HeaderText="Attach Document" ID="TabAttachDocument">
                                                    <ContentTemplate>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- START-- {UpdatePanel} -- ADDED END --%>
                                                        <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <div style="width: 15%; vertical-align: top;" class="ib">
                                                            <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                        </div>
                                                                <div style="width: 33%; vertical-align: top;" class="ib">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server" Width="250px">
                                                            </asp:DropDownList>
                                                        </div>
                                                                <div style="width: 14%; vertical-align: top;" class="ib">
                                                                    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                    <%--<asp:UpdatePanel ID="UPUpload" runat="server" Visible="false" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <uc9:FileUpload ID="flUpload" runat="server" />
                                                                </ContentTemplate>
                                                                    </asp:UpdatePanel>--%>
                                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                            <asp:Panel ID="pnl_ImageAdd" runat="server" Width="100%">
                                                                <div id="fileuploader">
                                                                    <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                                        value="Browse" />
                                                                </div>
                                                            </asp:Panel>
                                                            <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" Text="Browse" />
                                                        </div>
                                                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                <div style="width: 14%; vertical-align: top;" class="ib">
                                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                </div>
                                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                        <div style="width: 100%; max-height: 200px; overflow: auto">
                                                            <asp:GridView ID="gvGrievanceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                HeaderStyle-Font-Bold="false" DataKeyNames="GUID,scanattachtranunkid">
                                                                <Columns>
                                                                    <asp:TemplateField FooterText="objcohDelete" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="RemoveAttachment"
                                                                                    ToolTip="Delete"></asp:LinkButton>
                                                                            </span>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                            <%--0--%>
                                                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                            <asp:TemplateField FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <%--1--%>
                                                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                            <%--2--%>
                                                                    <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                        ItemStyle-HorizontalAlign="Right" />
                                                                            <%--3--%>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="gvGrievanceAttachment" />
                                                                <asp:PostBackTrigger ControlID="btnDownloadAll" />
                                                            </Triggers>
                                                            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                        </asp:UpdatePanel>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel runat="server" HeaderText="Appeal Remark" ID="TabApplealRemark" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtpopupApplealRemark" Width="99%" runat="server" Rows="3" TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                            </cc1:TabContainer>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSaveGreApprover" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnSaveSubmitGreApprover" runat="server" CssClass="btndefault" Text="Save And Submit" />
                                        <asp:Button ID="btnCloseGreApprover" runat="server" CssClass="btndefault" Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc7:Confirmation ID="PopupSubmitGrievance" runat="server"  />
                    <uc7:Confirmation ID="popupAttachment_YesNo" runat="server" />
                    <ucDel:DeleteReason ID="popupDeleteGrievanceReason" runat="server"  />
                    <uc7:Confirmation ID="popupConfirmationGrievanceReason" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
    
	    $(document).ready(function()
		{
		    ImageLoad();
        });
        
        
		function ImageLoad(){
		debugger;
		    if ($(".ajax-upload-dragdrop").length <= 0){
		    $("#fileuploader").uploadFile({
			    url: "EmployeeGrievance.aspx?uploadimage=mSEfU19VPc4=",
                method: "POST",
				dragDropStr: "",
				showStatusAfterSuccess:false,
                showAbort:false,
                showDone:false,
                maxFileSize: 1024 * 1024,
				fileName:"myfile",
				onSuccess:function(path,data,xhr){
				debugger;
				    $("#<%= btnSaveAttachment.ClientID %>").click();
                },
                onError:function(files,status,errMsg){
	                alert(errMsg);
                }
            });
        }
        }
        $('input[type=file]').live("click",function(){
		    if($(this).attr("class") != "flupload"){
		    debugger;
		    return IsValidAttach();
		    }
        });
    </script>

</asp:Content>
