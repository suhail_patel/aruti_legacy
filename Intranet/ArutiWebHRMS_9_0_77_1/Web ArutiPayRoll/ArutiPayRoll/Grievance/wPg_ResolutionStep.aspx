<%@ Page Title="Add / Edit Resolution Step" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ResolutionStep.aspx.vb" Inherits="Grievance_wPg_ResolutionStep" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Resolution Step"></asp:Label>
                            <asp:LinkButton ID="btnViewPreviousResolution" runat="server" Visible="false" ToolTip="view lower level resolution" Style="margin-top: -2px;">
                           <i class="fa fa-comments" style="color:#fff !important;font-size: 20px;"> </i>
                            </asp:LinkButton>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Resolution Step Info"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div class="ib" style="width: 9%">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 37%">
                                        <asp:DropDownList ID="drpApprover" runat="server" AutoPostBack="true" Width="430px">
                                        </asp:DropDownList>
                                    </div>
                                    
                                    <asp:Panel ID="pnl_Approverlevel" runat="server" Width="28%" CssClass="ibwm">
                                        <div class="ib" style="width: 28%">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level"></asp:Label>
                                    </div>
                                        <div class="ib" style="width: 60%">
                                        <asp:TextBox ID="txtLevel" runat="server" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    </asp:Panel>
                                    
                                    <div class="ib" style="width: 8%">
                                        <asp:Label ID="lblGrievanceDate" runat="server" Text="Date"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 12%">
                                        <uc3:DateCtrl ID="dtGrievanceDate" runat="server" AutoPostBack="false" Enabled="false" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 9%">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Against Employee"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 37%">
                                        <asp:DropDownList ID="cboEmpName" runat="server" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ib" style="width: 8%">
                                        <asp:Label ID="lblRefNo" runat="server" Text="Ref No."></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 40%">
                                        <asp:DropDownList ID="cboRefNo" runat="server" AutoPostBack="true" Width="460px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 9%">
                                        <asp:Label ID="lblGrievanceType" runat="server" Text="Grievance Type"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 37%">
                                        <asp:DropDownList ID="cboGrievanceType" runat="server" AutoPostBack="true" Enabled="false">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="ib" style="width: 8%">
                                        <asp:Label ID="lblAgainstEmp" runat="server" Text="Raised By"></asp:Label>
                                    </div>
                                    <div class="ib" style="width: 40%">
                                        <asp:TextBox ID="txtAgainstEmployee" ReadOnly="true" runat="server" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div class="ib" style="width: 100%">
                                        <cc1:TabContainer ID="tabMain" runat="server" ActiveTabIndex="0" Style="overflow: auto">
                                            <cc1:TabPanel ID="tbGrievanceDesc" runat="server" HeaderText="Grievance Description">
                                                
                                                <ContentTemplate>
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%; margin-bottom: 5px">
                                                                <asp:TextBox ID="txtGrievanceDesc" runat="server" TextMode="MultiLine" Rows="9" Style="margin-left: 5px;"
                                                                    Enabled="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="tbReliefReq" runat="server" HeaderText="Relief Requested">
                                                <ContentTemplate>
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%; margin-bottom: 5px">
                                                                        <asp:TextBox ID="txtReliefReq" runat="server" Style="margin-left: 5px;" TextMode="MultiLine"
                                                                            Rows="9" Enabled="false"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabAttachDocument" runat="server" HeaderText="Attach Document">
                                                <ContentTemplate>
                                                    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvGrievanceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                HeaderStyle-Font-Bold="false" DataKeyNames="GUID,scanattachtranunkid,localpath">
                                                                <Columns>
                                                                    <asp:TemplateField FooterText="objcohdownload" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="DownloadLink" runat="server" ToolTip="Delete" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                                    OnClick="lnkdownloadAttachment_Click">
                                                                                   <i class="fa fa-download"></i>
                                                                                </asp:LinkButton>
                                                                            </span> 
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                    <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                        ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div style="width: 100%;">
                                                                <asp:Button ID="btndownloadall" runat="server" Text="Download All" CssClass="btndefault" />
                                                            </div>
                                                     </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="gvGrievanceAttachment" />
                                                            <asp:PostBackTrigger ControlID="btndownloadall" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                        </cc1:TabContainer>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlData" runat="server">
                                    <div class="row2">
                                        <div class="ib" style="width: 60%; height: 280px">
                                            <div class="row2">
                                                <div class="ib" style="width: 25%">
                                                    <asp:Label ID="lblResponseType" runat="server" Text="Response Type"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 70%">
                                                    <asp:DropDownList ID="cboRepsonseType" runat="server" AutoPostBack="true" Width="480px">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 25%">
                                                    <asp:Label ID="lblRespnonseRemark" runat="server" Text="Respnonse Remark"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 68%">
                                                    <asp:TextBox ID="txtRespnonseRemark" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 25%">
                                                    <asp:Label ID="lblQualifingRemark" runat="server" Text="Qualifing Remark"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 68%">
                                                    <asp:TextBox ID="txtQualifingRemark" runat="server" TextMode="MultiLine" Rows="7"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ib" style="width: 37%; height: 280px; vertical-align: top">
                                            <div class="row2">
                                                <div class="ib" style="width: 30%">
                                                    <asp:Label ID="lblMeetingDate" runat="server" Text="Meeting Date"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 58%">
                                                    <uc3:DateCtrl ID="dtMeetingDate" runat="server" AutoPostBack="false" />
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 100%">
                                                    <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <asp:Panel ID="pnlGrid" runat="server" Style="max-height: 200px; overflow: auto"
                                                class="row2">
                                                <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="employeeunkid">
                                                    <RowStyle CssClass="griviewitem" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-CssClass="headerstyle" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="50px" ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center"
                                                            ItemStyle-Width="50px">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Enabled="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" CommandArgument="<%# Container.DataItemIndex %>"
                                                                    Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                            </ItemTemplate>
                                                            <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                            <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeecode" FooterText="dgcolhEcode" HeaderText="Code" />
                                                        <asp:BoundField DataField="employeename" FooterText="dgcolhEName" HeaderText="Employee" />
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                    </Columns>
                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="btn-default">
                                    <asp:Label ID="objlblError" runat="server" Text="" Font-Bold="true" Font-Size="Small"
                                        Style="color: Red; float: left"></asp:Label>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                    <asp:Button ID="btnSubmit" runat="server" Text="Save && Submit" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                        <uc2:Cnf_YesNo ID="Cnf_changeResponse_onedit" runat="server" />
                        <uc2:Cnf_YesNo ID="cnfSubmit" runat="server" />
                        <ucDel:DeleteReason ID="DeleteResolutionEmpReason" runat="server" />
                        <%--<PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />--%>
                    </div>
                    <cc1:ModalPopupExtender ID="popupGrePreviousResponse" BackgroundCssClass="modalBackground"
                        TargetControlID="lblPopupheader" runat="server" PopupControlID="pnlGrePreviousResponse"
                        DropShadow="false" CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlGrePreviousResponse" runat="server" CssClass="newpopup" Style="display: none;
                        width: 920px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblPopupheader" Text="Previous Lower Level Approver Resolution" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 15%">
                                            <asp:Label ID="lblPopupReferanceNo" runat="server" Text="Referance Number"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:Label ID="txtPopupReferanceNo" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnl_dgView" runat="server" Height="400px" ScrollBars="Auto">
                                        <asp:GridView ID="GvPreviousResponse" runat="server" AutoGenerateColumns="False"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="levelname" HeaderText="Approver Level" HeaderStyle-Width="10%"
                                                    FooterText="colhlevelname"></asp:BoundField>
                                                <asp:BoundField DataField="name" HeaderText="Approver Name" HeaderStyle-Width="10%"
                                                    FooterText="colhApproverName"></asp:BoundField>
                                                <asp:BoundField DataField="priority" HeaderText="Priority" HeaderStyle-Width="2%"
                                                    FooterText="colhPriority"></asp:BoundField>
                                                <asp:BoundField DataField="qualifyremark" HeaderText="Qualification Remark" HeaderStyle-Width="20%"
                                                    FooterText="colhqualifyremark"></asp:BoundField>
                                                <asp:BoundField DataField="responseremark" HeaderText="Response Remark" HeaderStyle-Width="20%"
                                                    FooterText="colhresponseremark"></asp:BoundField>
                                                <asp:BoundField DataField="EmpResponseRemark" HeaderText="Employee Response" HeaderStyle-Width="20%"
                                                    FooterText="colhempresponseremark"></asp:BoundField>
                                                    <asp:BoundField DataField="EmployeeStatus" HeaderText="Employee Status" HeaderStyle-Width="20%"
                                                    FooterText="colhempstatus"></asp:BoundField>
                                                <asp:BoundField DataField="EmployeeIds" HeaderText="Comitee Members" HeaderStyle-Width="20%"
                                                    FooterText="colhcomiteemember"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCloseGrePreviousResponse" runat="server" CssClass="btndefault"
                                            Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <%--'S.SANDEEP |25-JAN-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}--%>
                <%--  <Triggers>
                    <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
                </Triggers>--%>
                <%--'S.SANDEEP |25-JAN-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
