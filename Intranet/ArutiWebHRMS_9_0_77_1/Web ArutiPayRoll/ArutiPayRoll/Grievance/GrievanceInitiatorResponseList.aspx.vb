﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Grievance_GrievanceInitiatorResponseList
    Inherits Basepage

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmGrievanceInitiatorResponseList" 'Sohail (22 Nov 2018)
    Private objclsgreinitiater_response_tran As New clsgreinitiater_response_tran
    Private objclsgrievanceapprover_master As New clsgrievanceapprover_master
    Private objclsGrievance_Master As New clsGrievance_Master
    Dim DisplayMessage As New CommonCodes
    Private dtResolutionStepList As New DataTable

    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
    Dim dtIntiatorStatusList As DataTable
    'Gajanan [27-June-2019] -- End

#End Region

#Region "Page Methods"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If (Page.IsPostBack = False) Then
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                If IsPostBack = False Then
                    Call SetControlCaptions()
                    Call Language._Object.SaveValue()
                    Call SetLanguage()
                End If
                'Sohail (22 Nov 2018) -- End

                FillCombo()
                FillList(True)

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            Else
                If ViewState("dtIntiatorStatusList") IsNot Nothing Then
                    dtIntiatorStatusList = CType(ViewState("dtIntiatorStatusList"), DataTable)
                    If dtIntiatorStatusList.Rows.Count > 0 Then
                        GvIntiatorResponse.DataSource = dtIntiatorStatusList
                        GvIntiatorResponse.DataBind()
                    End If
                End If
                'Gajanan [27-June-2019] -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load: " + ex.Message, Me)
        End Try
    End Sub
    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dtIntiatorStatusList") = dtIntiatorStatusList
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
        End Try
    End Sub
    'Gajanan [27-June-2019] -- End
#End Region

#Region "Private Methods"
    Private Sub FillCombo()
        Try
            If CInt(Session("Employeeunkid")) > 0 Then
                Dim dsList As DataSet = Nothing
                Dim strFilter As String = ""

                dsList = objclsGrievance_Master.GetRefno("RefNoList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), CInt(Session("Employeeunkid")), "", True, strFilter, False)


                With drpRefno
                    .DataValueField = "grievancemasterunkid"
                    .DataTextField = "grievancerefno"
                    .DataSource = dsList.Tables("RefNoList")
                    .DataBind()
                End With

                With drpAgainstEmp
                    .DataValueField = "againstemployeeunkid"
                    .DataTextField = "Againstemployee"
                    .DataSource = dsList.Tables("RefNoList")
                    .DataBind()
                End With
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As New DataSet
        Dim strfilter As String = String.Empty
        Try
            If Convert.ToInt32(drpAgainstEmp.SelectedValue) > 0 Then
                strfilter += "and againstemployeeunkid = " & Convert.ToInt32(drpAgainstEmp.SelectedValue) & " "
            End If


            'Gajanan [24-June-2019] -- Start      

            'If Convert.ToInt32(drpRefno.SelectedValue) > 0 Then
            '    strfilter += "and refno = " & Convert.ToInt32(drpRefno.SelectedItem.ToString) & " "
            'End If

            If Convert.ToInt32(drpRefno.SelectedValue) > 0 Then
                strfilter += "and refno = '" & drpRefno.SelectedItem.ToString & "' "
            End If
            'Gajanan [24-June-2019] -- End


            If isblank Then
                strfilter = "and 1 = 2 "
            End If
            'If strfilter.Trim.Length > 0 Then strfilter = strfilter.Substring(3)

'Gajanan [4-July-2019] -- Start      
            Dim dsApprovers As New DataSet
            Dim eApprType As enGrievanceApproval
            Dim objAppr As New clsgrievanceapprover_master


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If

            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing)
            dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing)
            'Gajanan [24-OCT-2019] -- End

'Gajanan [4-July-2019] -- End


            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            'dsList = objclsgreinitiater_response_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), True, Nothing, False, strfilter, )
            dsList = objclsgreinitiater_response_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), True, Nothing, False, strfilter, CInt(Session("Employeeunkid")))
            'Gajanan [27-June-2019] -- End


            If dsList.Tables(0).Rows.Count <= 0 Then
                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                'dsList = objclsgreinitiater_response_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), True, Nothing, True, " 1=2")


                'Gajanan [4-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
                'dsList = objclsgreinitiater_response_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), True, Nothing, True, "1=2", CInt(Session("Employeeunkid")))
                dsList = objclsgreinitiater_response_tran.GetList("ResolutionStepList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), True, Nothing, True, "and 1=2", CInt(Session("Employeeunkid")))
                'Gajanan [4-July-2019] -- End


                'Gajanan [27-June-2019] -- End
                isblank = True
            End If


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            dsList.Tables(0).Columns.Add("approvername")


            For Each iRow As DataRow In dsList.Tables(0).Rows
                Dim drRow As DataRow() = Nothing
                If iRow("approverempid").ToString() = iRow("mapuserunkid").ToString() Then
                    drRow = dsApprovers.Tables(0).Select("approverempid = " & iRow("mapuserunkid").ToString() & "")
                Else
                    drRow = dsApprovers.Tables(0).Select("approverempid = " & iRow("approverempid").ToString() & "")
                End If


                If drRow.Length > 0 Then
                    iRow("approvername") = drRow(0)("name").ToString()
                End If
            Next
            'Gajanan [4-July-2019] -- End


            Dim mdtTran As DataTable = dsList.Tables(0)

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            dtIntiatorStatusList = mdtTran.Clone

            If isblank = False Then

                Dim strRefno As String = ""
                dtIntiatorStatusList.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow() As DataRow = Nothing
                Dim grpRow As DataRow = Nothing
                For Each drow As DataRow In mdtTran.Rows
                    If CStr(drow("grievancerefno")).Trim <> strRefno.Trim Then
                        grpRow = dtIntiatorStatusList.NewRow
                        grpRow("IsGrp") = True
                        grpRow("grievancerefno") = drow("grievancerefno")
                        grpRow("againstemployee") = drow("againstemployee")


                        strRefno = drow("grievancerefno").ToString()
                        dtIntiatorStatusList.Rows.Add(grpRow)
                        dtRow = mdtTran.Select(("grievancerefno = '" & strRefno.Trim & "' "))
                        If dtRow.Count > 0 Then
                            For Each irow As DataRow In dtRow
                                dtIntiatorStatusList.ImportRow(irow)
                            Next
                        End If
                    End If
                Next
            End If
            'Gajanan [27-June-2019] -- End


            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            'GvIntiatorResponse.DataSource = mdtTran

            If isblank = False Then
                GvIntiatorResponse.DataSource = dtIntiatorStatusList
                Me.ViewState.Add("dtIntiatorStatusList", dtIntiatorStatusList)
            Else
            GvIntiatorResponse.DataSource = mdtTran
                Me.ViewState.Add("dtIntiatorStatusList", mdtTran)
            End If

            'Gajanan [27-June-2019] -- End  

            GvIntiatorResponse.DataBind()
            If isblank = True Then
                GvIntiatorResponse.Rows(0).Visible = False
            End If

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            'Me.ViewState.Add("ResolutionStep", dsList.Tables("ResolutionStepList"))
            'Gajanan [27-June-2019] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
        End Try
    End Sub


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#DDD")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError("AddGroup" + ex.Message, Me)
        End Try

    End Sub
    'Gajanan [27-June-2019] -- End

#End Region

#Region "Gridview Methods"


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


    'Protected Sub GvIntiatorResponse_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvIntiatorResponse.RowCommand
    '    Try
    '        Dim dtResolutionStep As DataTable = CType(Me.ViewState("ResolutionStep"), DataTable)
    '        If e.CommandName = "View" Then
    '            Session.Add("ResolutionStepTranid", CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("resolutionsteptranunkid")))
    '            Session.Add("grievancemasteid", CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("grievancemasterunkid")))
    '            Session.Add("initiatortranunkid", CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("initiatortranunkid")))
    '            Session.Add("priority", CInt(dtResolutionStep.Rows(CInt(e.CommandArgument))("priority")))
    '            Response.Redirect(Session("rootpath").ToString & "Grievance/AgreeDisagreeResponse.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("GvIntiatorResponse_RowCommand :- " & ex.Message, Me)
    '    End Try
    'End Sub


    Protected Sub lnkView_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim dtResolutionStep As DataTable = CType(Me.ViewState("dtIntiatorStatusList"), DataTable)

            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            Session.Add("ResolutionStepTranid", CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("resolutionsteptranunkid")))
            Session.Add("grievancemasteid", CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("grievancemasterunkid")))
            Session.Add("initiatortranunkid", CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("initiatortranunkid")))
            Session.Add("priority", CInt(dtResolutionStep.Rows(CInt(lnk.CommandArgument))("priority")))
                Response.Redirect(Session("rootpath").ToString & "Grievance/AgreeDisagreeResponse.aspx", False)

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkView_Click:" + ex.Message, Me)
        End Try
    End Sub
    'Gajanan [27-June-2019] -- End


    Protected Sub GvIntiatorResponse_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvIntiatorResponse.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                If dt.Columns.Contains("IsGrp") = True AndAlso IsDBNull(dt.Rows(e.Row.RowIndex)("IsGrp")) = False AndAlso CBool(dt.Rows(e.Row.RowIndex)("IsGrp").ToString()) = True Then
                    Me.AddGroup(e.Row, "<b> " & dt.Rows(e.Row.RowIndex)("againstemployee").ToString() & " (" & dt.Rows(e.Row.RowIndex)("grievancerefno").ToString() & ")</b>", GvIntiatorResponse)
                End If


                'If CInt(GvIntiatorResponse.DataKeys(e.Row.RowIndex)("resolutionsteptranunkid").ToString) > 0 Then
                If IsDBNull(GvIntiatorResponse.DataKeys(e.Row.RowIndex)("resolutionsteptranunkid")) = False AndAlso CInt(GvIntiatorResponse.DataKeys(e.Row.RowIndex)("resolutionsteptranunkid").ToString) > 0 Then
                    'Gajanan [27-June-2019] -- End
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvIntiatorResponse, "colhMeetingDate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvIntiatorResponse, "colhMeetingDate", False, True)).Text).ToShortDateString()

                    Dim lnkselect As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)

                    If CInt(GvIntiatorResponse.DataKeys(e.Row.RowIndex)("issubmitted").ToString) = 1 AndAlso CInt(GvIntiatorResponse.DataKeys(e.Row.RowIndex)("initiatortranunkid").ToString) > 0 Then
                        lnkselect.Visible = False
                    Else
                        lnkselect.Visible = True
                    End If

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("GvIntiatorResponse_RowDataBound :" + ex.Message, Me)
        End Try

    End Sub

#End Region

#Region "Private Methods"

    Protected Sub btnSearch_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnSearch.Command
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Command :" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpAgainstEmp.SelectedIndex = 0
            drpRefno.SelectedIndex = 0
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click :" + ex.Message, Me)
        End Try

    End Sub
#End Region

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.lblAgainstEmp.ID, Me.lblAgainstEmp.Text)
            Language._Object.setCaption(Me.lblRefno.ID, Me.lblRefno.Text)

            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)

             Language._Object.setCaption(GvIntiatorResponse.Columns(1).FooterText, GvIntiatorResponse.Columns(1).HeaderText)
            Language._Object.setCaption(GvIntiatorResponse.Columns(2).FooterText, GvIntiatorResponse.Columns(2).HeaderText)
            Language._Object.setCaption(GvIntiatorResponse.Columns(3).FooterText, GvIntiatorResponse.Columns(3).HeaderText)
            Language._Object.setCaption(GvIntiatorResponse.Columns(4).FooterText, GvIntiatorResponse.Columns(4).HeaderText)
            Language._Object.setCaption(GvIntiatorResponse.Columns(5).FooterText, GvIntiatorResponse.Columns(5).HeaderText)
            Language._Object.setCaption(GvIntiatorResponse.Columns(6).FooterText, GvIntiatorResponse.Columns(6).HeaderText)
            Language._Object.setCaption(GvIntiatorResponse.Columns(7).FooterText, GvIntiatorResponse.Columns(7).HeaderText)

            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            Language._Object.setCaption(GvIntiatorResponse.Columns(8).FooterText, GvIntiatorResponse.Columns(8).HeaderText)
            'Gajanan [27-June-2019] -- End


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            Language._Object.setCaption(GvIntiatorResponse.Columns(9).FooterText, GvIntiatorResponse.Columns(9).HeaderText)
            'Gajanan [4-July-2019] -- End


            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblDetialHeader.Text = Language._Object.getCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblAgainstEmp.Text = Language._Object.getCaption(Me.lblAgainstEmp.ID, Me.lblAgainstEmp.Text)
            Me.lblRefno.Text = Language._Object.getCaption(Me.lblRefno.ID, Me.lblRefno.Text)
            
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            GvIntiatorResponse.Columns(1).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(1).FooterText, GvIntiatorResponse.Columns(1).HeaderText)
            GvIntiatorResponse.Columns(2).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(2).FooterText, GvIntiatorResponse.Columns(2).HeaderText)
            GvIntiatorResponse.Columns(3).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(3).FooterText, GvIntiatorResponse.Columns(3).HeaderText)
            GvIntiatorResponse.Columns(4).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(4).FooterText, GvIntiatorResponse.Columns(4).HeaderText)
            GvIntiatorResponse.Columns(5).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(5).FooterText, GvIntiatorResponse.Columns(5).HeaderText)
            GvIntiatorResponse.Columns(6).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(6).FooterText, GvIntiatorResponse.Columns(6).HeaderText)
            GvIntiatorResponse.Columns(7).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(7).FooterText, GvIntiatorResponse.Columns(7).HeaderText)
            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            GvIntiatorResponse.Columns(8).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(8).FooterText, GvIntiatorResponse.Columns(8).HeaderText)
            'Gajanan [27-June-2019] -- End


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
            GvIntiatorResponse.Columns(9).HeaderText = Language._Object.getCaption(GvIntiatorResponse.Columns(9).FooterText, GvIntiatorResponse.Columns(9).HeaderText)

            'Gajanan [4-July-2019] -- End


            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End
    
End Class
