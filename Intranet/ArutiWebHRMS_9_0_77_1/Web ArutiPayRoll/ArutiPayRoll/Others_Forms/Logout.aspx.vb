﻿
Partial Class Default2
    Inherits Basepage
    Dim clsdataopr As New eZeeCommonLib.clsDataOperation(True)
    Dim DisplayMessage As New CommonCodes
    Dim msql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("imgAcc") = ""
        Session("ActivePane") = Nothing
        'Sohail (04 Jun 2012) -- Start
        'TRA - ENHANCEMENT - View Online Memberes
        If HttpContext.Current.Session("LoginBy") Is Nothing Then

        ElseIf (Session("LoginBy") = Global.User.en_loginby.User) Then
            clsActiveUserEngine.Leaved("USER" & Session("UserId").ToString)
        Else
            clsActiveUserEngine.Leaved("EMP" & Session("Employeeunkid").ToString)
        End If
        HttpContext.Current.Session("LoginBy") = Nothing 'Sohail (08 May 2014)
        'Session("LoginBy") = Nothing
        'Sohail (04 Jun 2012) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Session("sessionexpired") = True
            Session("Login") = False
            Session("clsuser") = Nothing
            Session("objGlobalAccess") = Nothing
            Me.IsLoginRequired = False

            Response.Redirect("~/index.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError("Error in  Page_PreInit  on logout page", Me)
            DisplayMessage.DisplayError("Error in  Page_PreInit  on logout page" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    Public Sub dropTable()
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                msql = "Drop Table Cal_" & Session("UserId") & ""
            Else
                msql = "Drop Table Cal_" & Session("Employeeunkid") & ""
            End If
            clsdataopr.ExecNonQuery(msql)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError("Error in Drop table Function on Logout page ", Me)
            DisplayMessage.DisplayError("Error in Drop table Function on Logout page " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

End Class
