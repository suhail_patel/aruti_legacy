﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib.clsDataOperation
'S.SANDEEP |18-OCT-2021| -- START
Imports System.Data.SqlClient
Imports System.Net.Dns
'S.SANDEEP |18-OCT-2021| -- END

#End Region

Partial Class ChangePassword
    Inherits Basepage

#Region " Private Variables "
    Dim msg As New CommonCodes
    Dim objDataOperation As New eZeeCommonLib.clsDataOperation(True)
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmChangePassword"
    'Anjan [04 June 2014 ] -- End

    Private objCONN As SqlConnection
    Private bintUnqNum As Int64 = 0

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP |18-OCT-2021| -- START
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                oldPwd.Visible = False
                lblTitle.Visible = False
                lblResetTitle.Visible = True
                Try
                    If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                    Else
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                    End If

                Catch ex As Exception
                    HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                    HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                End Try

                If arr.Length = 4 Then
                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
                    HttpContext.Current.Session("UserId") = CInt(arr(2))
                    Dim objPwdReset As New clsPasswordResetRequest
                    Dim iMSg As String = ""
                    If objPwdReset.IsValidLink(Convert.ToInt64(arr(3)), iMSg) = False Then
                        If iMSg.Trim.Length > 0 Then
                            msg.DisplayMessage(iMSg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        End If
                    End If
                    bintUnqNum = Convert.ToInt64(arr(3))
                    Me.ViewState("bintUnqNum") = Convert.ToInt64(arr(3))
                    'If HttpContext.Current.Session("UserId") > 0 Then
                    '    Me.ViewState("Empunkid") = Session("UserId")
                    'ElseIf HttpContext.Current.Session("Employeeunkid") > 0 Then
                    '    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'End If
                End If



                Dim strError As String = ""
                If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                    msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Sub
                End If

                HttpContext.Current.Session("mdbname") = Session("Database_Name")
                gobjConfigOptions = New clsConfigOptions
                gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                ArtLic._Object = New ArutiLic(False)
                If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                    Dim objGroupMaster As New clsGroup_Master
                    objGroupMaster._Groupunkid = 1
                    ArtLic._Object.HotelName = objGroupMaster._Groupname
                End If

                If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                    msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If

                If ConfigParameter._Object._IsArutiDemo Then
                    If ConfigParameter._Object._IsExpire Then
                        msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                        Exit Try
                    Else
                        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                            msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        End If
                    End If
                End If

                Dim clsConfig As New clsConfigOptions
                clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                Else
                    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                End If

                Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                Dim objPwdOpt As New clsPassowdOptions
                Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser
                objPwdOpt = Nothing

                HttpContext.Current.Session("Login") = True

                strError = ""
                If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), 1) = False Then
                    msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Sub
                End If

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = Session("Employeeunkid")
                txtOldPassword.Text = objEmployee._Password
                txtOldPassword.Enabled = False


                Dim objUser As New clsUserAddEdit
                Dim cluser As User = Nothing

                If CInt(Session("UserId")) > 0 Then
                    cluser = New User(objUser._Username, objUser._Password, Session("mdbname"))
                    objUser._Userunkid = CInt(Session("UserId"))
                    HttpContext.Current.Session("clsuser") = cluser
                    HttpContext.Current.Session("UserName") = cluser.UserName
                    HttpContext.Current.Session("Firstname") = cluser.Firstname
                    HttpContext.Current.Session("Surname") = cluser.Surname
                    HttpContext.Current.Session("MemberName") = cluser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User

                    HttpContext.Current.Session("UserId") = cluser.UserID
                    HttpContext.Current.Session("Employeeunkid") = cluser.Employeeunkid
                    HttpContext.Current.Session("Password") = cluser.password
                    HttpContext.Current.Session("RoleID") = cluser.RoleUnkID
                    HttpContext.Current.Session("LangId") = cluser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    HttpContext.Current.Session("UserName") = HttpContext.Current.Session("U_UserName")
                    HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("U_MemberName")
                    HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("U_DisplayName")
                Else
                    objUser._Userunkid = CInt(Session("UserId"))
                    cluser = New User(objEmployee._Displayname, objEmployee._Password, Session("mdbname"), Session("Employeeunkid"))
                    HttpContext.Current.Session("UserName") = HttpContext.Current.Session("E_UserName")
                    HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("E_MemberName")
                    HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("E_DisplayName")

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee

                    HttpContext.Current.Session("clsuser") = cluser
                    HttpContext.Current.Session("UserName") = cluser.UserName
                    HttpContext.Current.Session("Firstname") = cluser.Firstname
                    HttpContext.Current.Session("Surname") = cluser.Surname
                    HttpContext.Current.Session("MemberName") = cluser.MemberName



                    HttpContext.Current.Session("UserId") = cluser.UserID
                    If cluser.Employeeunkid > 0 Then
                    HttpContext.Current.Session("Employeeunkid") = cluser.Employeeunkid
                    End If                    
                    HttpContext.Current.Session("Password") = cluser.password
                    HttpContext.Current.Session("RoleID") = cluser.RoleUnkID
                    HttpContext.Current.Session("LangId") = cluser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                End If
                objEmployee = Nothing

                GoTo link
            End If
            'S.SANDEEP |18-OCT-2021| -- END


            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If



            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
link:       Call SetLanguage()
            'Anjan [04 June 2014 ] -- End

            Dim clsuser As New User
            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)
                Dim objOption As New clsPassowdOptions

                'Sohail (11 May 2012) -- Start
                'TRA - ENHANCEMENT
                'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End
                    RequiredFieldValidator4.ValidationGroup = "novalidation" 'Old Password
                    RequiredFieldValidator1.ValidationGroup = "novalidation" 'New Password
                    RequiredFieldValidator2.ValidationGroup = "novalidation" 'Confirm Password    
                    txtNewPassword.ValidationGroup = "novalidation"
                    'RegExp1.ValidationGroup = "novalidation"
                    'RegExp1.Enabled = False


                    If objOption._ProhibitPasswordChange = True AndAlso CInt(Me.ViewState("Empunkid")) <> 1 Then
                        btnSave.Enabled = False
                    End If
                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")

                    'Sohail (30 Mar 2015) -- End
                    RequiredFieldValidator4.ValidationGroup = "password" 'Old Password
                    RequiredFieldValidator1.ValidationGroup = "password" 'New Password
                    RequiredFieldValidator2.ValidationGroup = "password" 'Confirm Password
                    txtNewPassword.ValidationGroup = "password"
                    'RegExp1.ValidationGroup = "password"
                    'PNReqE.TargetControlID = "RegExp1"
                    'RegExp1.Enabled = True

                    If objOption._ProhibitPasswordChange = True Then
                        btnSave.Enabled = False
                    End If
                End If
                'Sohail (11 May 2012) -- End
            Else
                bintUnqNum = Me.ViewState("bintUnqNum")
            End If
        Catch ex As Exception
            msg.DisplayError("Page_Load : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count < 0 Then
        Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("bintUnqNum") = bintUnqNum
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsDataValid() = False Then Exit Sub
            Dim objData As New clsDataOperation(True)
            'Sohail (11 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim objEmp As New clsEmployee_Master
            'objEmp._Employeeunkid = Me.ViewState("Empunkid")
            'objEmp._Password = txtConfirmPassword.Text
            'Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPassword.Text, "ezee").ToString()
            'objData.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)
            'objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = " & objEmp._Employeeunkid)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = Me.ViewState("Empunkid")
                objUser._Password = txtConfirmPassword.Text
                Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPassword.Text, "ezee").ToString()
                objData.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)
                objData.ExecNonQuery("UPDATE hrmsconfiguration..cfuser_master SET password = @Passwd WHERE userunkid = " & objUser._Userunkid)
                'S.SANDEEP [ 14 DEC 2012 ] -- START
                'objUser = Nothing
                'ENHANCEMENT : TRA CHANGES
                If Session("IsEmployeeAsUser") = True Then
                    objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = '" & objUser._EmployeeUnkid & "'")
                End If
                objUser = Nothing
                'S.SANDEEP [ 14 DEC 2012 ] -- END
            Else
                Dim objEmp As New clsEmployee_Master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = Me.ViewState("Empunkid")
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = Me.ViewState("Empunkid")
                'Shani(20-Nov-2015) -- End

                objEmp._Password = txtConfirmPassword.Text
                Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPassword.Text, "ezee").ToString()
                objData.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = " & objEmp._Employeeunkid)
                objData.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = " & objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))))
                'Shani(20-Nov-2015) -- End

                objEmp = Nothing

                'S.SANDEEP [ 14 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Session("IsEmployeeAsUser") = True Then
                    objData.ExecNonQuery("UPDATE hrmsconfiguration..cfuser_master SET password = @Passwd WHERE employeeunkid = '" & Me.ViewState("Empunkid") & "' AND companyunkid = '" & Session("CompanyUnkId") & "'")
                End If
                'S.SANDEEP [ 14 DEC 2012 ] -- END




            End If
            'Sohail (11 May 2012) -- End

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                SetUserTracingInfo(False, enUserMode.Password, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), True, Session("mdbname"))
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                SetUserTracingInfo(False, enUserMode.Password, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), True, Session("mdbname"))
            End If
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            Dim objPswd As New clsPassowdOptions
            Dim objPasswordHistory As New clsPasswordHistory
            If objPswd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPswd._IsPasswordHistory AndAlso _
               (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
            If Session("IsEmployeeAsUser") = True OrElse Session("LoginBy") = Global.User.en_loginby.Employee Then
                Dim intEmployeeId As Integer = CInt(Session("Employeeunkid"))
                If Session("LoginBy") = Global.User.en_loginby.User Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = Me.ViewState("Empunkid")
                    intEmployeeId = objUser._EmployeeUnkid
                End If
                With objPasswordHistory
                    ._Tranguid = Guid.NewGuid().ToString
                    ._Userunkid = -1
                    ._Employeeunkid = intEmployeeId
                    ._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
                    ._NewPassword = txtConfirmPassword.Text
                    ._OldPassword = txtOldPassword.Text
                    ._Companyunkid = CInt(Session("CompanyUnkId"))
                    ._ChangedUserunkid = Session("UserId")
                    ._Ip = CStr(Session("IP_ADD"))
                    ._Hostname = CStr(Session("HOST_NAME"))
                    ._Isweb = True
                        If .Insert() = False Then
                            msg.DisplayMessage(._Message, Me)
                            Exit Sub
                        End If
                End With
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                With objPasswordHistory
                    ._Tranguid = Guid.NewGuid().ToString
                    ._Userunkid = Session("UserId")
                    ._Employeeunkid = -1
                    ._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
                    ._NewPassword = txtConfirmPassword.Text
                    ._OldPassword = txtOldPassword.Text
                    ._Companyunkid = CInt(Session("CompanyUnkId"))
                    ._ChangedUserunkid = Session("UserId")
                    ._Ip = CStr(Session("IP_ADD"))
                    ._Hostname = CStr(Session("HOST_NAME"))
                    ._Isweb = True
                        If .Insert() = False Then
                            msg.DisplayMessage(._Message, Me)
                            Exit Sub
                        End If
                End With
            End If
            End If
            objPasswordHistory = Nothing
            objPswd = Nothing
            'Hemant (25 May 2021) -- End

            Dim objPwdReset As New clsPasswordResetRequest
            With objPwdReset
                ._Isaccessed = True
                ._Ischanged = True
                ._Changedatetime = Now
                ._Uniquecode = bintUnqNum
                If .Update() = False Then

                End If
            End With
            objPwdReset = Nothing

            'objEmp.Update()
            If objData.ErrorMessage = "" Then
                msg.DisplayMessage("Password Changed Successfully.", Me, "../Index.aspx")
            Else
                msg.DisplayMessage(objData.ErrorMessage, Me)
            End If
        Catch ex As Exception
            msg.DisplayError("btnSubmit_Click" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            If Request.QueryString.Count > 0 Then
                Response.Redirect("~\Index.aspx", False)
            Else
            Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError("btnClose_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Controls "
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebuton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebutton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region " Private Functions & Methods "

    Private Function IsDataValid() As Boolean
        Try

            'Sohail (11 May 2012) -- Start
            'TRA - ENHANCEMENT
            'If txtOldPassword.Text.Length <= 0 Then
            '    msg.DisplayMessage("Old Password is compulsory information. Please enter Old Password to continue.", Me)
            '    txtOldPassword.Focus()
            '    Return False
            'End If

            'If txtNewPassword.Text.Length <= 0 Then
            '    msg.DisplayMessage("New Password is compulsory information. Please enter New Password to continue.", Me)
            '    txtNewPassword.Focus()
            '    Return False
            'End If

            'If txtConfirmPassword.Text.Length <= 0 Then
            '    msg.DisplayMessage("Confirmation Password is compulsory information. Please enter Confirmation Password to continue.", Me)
            '    txtConfirmPassword.Focus()
            '    Return False
            'End If

            'If txtNewPassword.Text <> txtConfirmPassword.Text Then
            '    msg.DisplayMessage("Password does not match. Please enter correct password", Me)
            '    txtConfirmPassword.Focus()
            '    Return False
            'End If

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = ViewState("Empunkid")
                'S.SANDEEP |18-OCT-2021| -- START
                'If txtOldPassword.Text <> objUser._Password Then
                '    msg.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                '    'txtOldPassword.Focus()
                '    Return False
                'End If
                If oldPwd.Visible Then
                    If txtOldPassword.Text <> objUser._Password Then
                        msg.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                        'txtOldPassword.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |18-OCT-2021| -- END

                'S.SANDEEP [ 06 NOV 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim objPswd As New clsPassowdOptions
                'If objPswd._IsPasswordLenghtSet Then
                '    If txtNewPassword.Text.Trim.Length < objPswd._PasswordLength Then
                '        msg.DisplayMessage("Password length cannot be less than " & objPswd._PasswordLength & " character(s)", Me)
                '        'txtNewPassword.Focus()
                '        Return False
                '    End If
                'End If
                'objPswd = Nothing
                'S.SANDEEP [ 06 NOV 2012 ] -- END
            Else
                Dim objEmployee As New clsEmployee_Master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = Session("Employeeunkid")
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = Session("Employeeunkid")
                'Shani(20-Nov-2015) -- End

                'S.SANDEEP |18-OCT-2021| -- START
                'If txtOldPassword.Text <> objEmployee._Password Then
                '    msg.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                '    'txtOldPassword.Focus()
                '    Return False
                'End If
                If oldPwd.Visible Then
                    If txtOldPassword.Text <> objEmployee._Password Then
                        msg.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                        'txtOldPassword.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |18-OCT-2021| -- END
            End If
            'Sohail (11 May 2012) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End


            'S.SANDEEP [ 06 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objPswd As New clsPassowdOptions
            If objPswd._IsPasswordLenghtSet Then
                If txtNewPassword.Text.Trim.Length < objPswd._PasswordLength Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Password length cannot be less than ") & objPswd._PasswordLength & Language.getMessage(mstrModuleName, 8, " character(s)"), Me)
                    'txtNewPassword.Focus()
                    Return False
                End If
            End If

            Dim mRegx As System.Text.RegularExpressions.Regex
            If txtNewPassword.Text.Trim.Length > 0 Then
                If objPswd._IsPasswdPolicySet Then
                    If objPswd._IsUpperCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Password must contain atleast one upper case character."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsLowerCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Password must contain atleast one lower case character."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsNumeric_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Password must contain atleast one numeric digit."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                    If objPswd._IsSpecalChars_Mandatory Then
                        'S.SANDEEP [ 07 MAY 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                        'S.SANDEEP [ 07 MAY 2013 ] -- END
                        If mRegx.IsMatch(txtNewPassword.Text.Trim) = False Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Password must contain atleast one special character."), Me)
                            txtNewPassword.Focus()
                            Return False
                        End If
                    End If
                End If
            End If
            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If objPswd._IsPasswordHistory AndAlso (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
                Dim objPasswordHistory As New clsPasswordHistory
                Dim intTopCount As Integer
                Dim strFilter As String = String.Empty
                Dim strOrderBy As String = String.Empty
                If objPswd._PasswdLastUsedNumber > 0 Then
                    intTopCount = objPswd._PasswdLastUsedNumber
                End If

                If objPswd._PasswdLastUsedDays > 0 Then
                    Dim dtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
                    Dim dtStartDate As Date = dtEndDate.AddDays(-1 * objPswd._PasswdLastUsedDays)
                    strFilter &= "AND Convert(char(8),transactiondate,112) >= '" & eZeeDate.convertDate(dtStartDate) & "' AND Convert(char(8),transactiondate,112) <= '" & eZeeDate.convertDate(dtEndDate) & "' "
                End If

                strOrderBy = " order by transactiondate desc "
                Dim dsList As DataSet = Nothing

                If Session("IsEmployeeAsUser") = True OrElse Session("LoginBy") = Global.User.en_loginby.Employee Then
                    Dim intEmployeeId As Integer = CInt(Session("Employeeunkid"))
                    If Session("LoginBy") = Global.User.en_loginby.User Then
                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = Me.ViewState("Empunkid")
                        intEmployeeId = objUser._EmployeeUnkid
                    End If
                    dsList = objPasswordHistory.GetFilterDataList("List", -1, intEmployeeId, CInt(Session("CompanyUnkId")), intTopCount, strFilter, strOrderBy)
                ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                    dsList = objPasswordHistory.GetFilterDataList("List", CInt(Session("UserId")), -1, -1, intTopCount, strFilter, strOrderBy)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim drPassword() As DataRow = dsList.Tables(0).Select("new_password = '" & clsSecurity.Encrypt(txtNewPassword.Text, "ezee").ToString & "' ")
                    If drPassword.Length > 0 Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, Entered New Password is already Used. Please Enter another Password!!!"), Me)
                        txtNewPassword.Focus()
                        Exit Function
                    End If
                End If
                objPasswordHistory = Nothing
            End If
            'Hemant (25 May 2021) -- End
            objPswd = Nothing
            'S.SANDEEP [ 06 NOV 2012 ] -- END

            Return True
        Catch ex As Exception
            msg.DisplayError("IsValid : " & ex.Message, Me)
        End Try
    End Function


#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblTitle.Text = Language._Object.getCaption(mstrModuleName, Me.Title) 'SHANI [01 FEB 2015]-START 'Enhancement - REDESIGN SELF SERVICE.

            Me.lblOldPwd.Text = Language._Object.getCaption(Me.lblOldPwd.ID, Me.lblOldPwd.Text)
            Me.lblConfirmPwd.Text = Language._Object.getCaption(Me.lblConfirmPwd.ID, Me.lblConfirmPwd.Text)
            Me.lblNewPwd.Text = Language._Object.getCaption(Me.lblNewPwd.ID, Me.lblNewPwd.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnCancel.Text = Language._Object.getCaption(Me.btnCancel.ID, Me.btnCancel.Text).Replace("&", "")


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError("Procedure : SetLanguage : " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class