﻿#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.Diagnostics
Imports System.IO
Imports System.Net
#End Region

Partial Class Leave_wPg_PreviewDocuments
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmPreviewDocuments"
    Private DisplayMessage As New CommonCodes
    Private mstrPreviewIds As String = ""
    Private objDocument As New clsScan_Attach_Documents
    Private mstrFileName As String = ""

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then

                If Request.QueryString.ToString.Length > 0 Then
                    mstrPreviewIds = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString))
                End If

                If lvPreviewList.Items.Count <= 0 Then
                    lvPreviewList.DataSource = New List(Of String)
                    lvPreviewList.DataBind()
                End If

                Call FillList()
                If Not Page.ClientScript.IsStartupScriptRegistered("OpenNew") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenNew", "opennewtab('');", True)
                End If
            Else
                mstrPreviewIds = Me.ViewState("PreviewIds")
                mstrFileName = Me.ViewState("FileName")
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("PreviewIds", mstrPreviewIds)
            Me.ViewState.Add("FileName", mstrFileName)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillList()
        Try
            Dim dtTranTable As New DataTable


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call objDocument.GetList("List", mstrPreviewIds)

            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'Call objDocument.GetList(Session("Document_Path"), "List", mstrPreviewIds)
            Call objDocument.GetList(Session("Document_Path"), "List", mstrPreviewIds, , , , , , CBool(IIf(mstrPreviewIds.Trim.Length <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            'Shani(20-Nov-2015) -- End

            dtTranTable = objDocument._Datatable.Copy

            If dtTranTable IsNot Nothing AndAlso dtTranTable.Rows.Count > 0 Then

                lvPreviewList.AutoGenerateColumns = False
                lvPreviewList.DataSource = dtTranTable
                lvPreviewList.DataBind()

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "Leave/ProcessPendingLeave.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "DataGrid Events"

    Protected Sub lvPreviewList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvPreviewList.ItemCommand
        Try

            If e.CommandName.ToUpper = "PREVIEW" Then
                If e.Item.ItemIndex >= 0 Then
                    Dim strFile As String = CStr(e.Item.Cells(6).Text)
                    If strFile.Contains(Session("ArutiSelfServiceURL").ToString) Then
                        strFile = strFile.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(strFile, 1) <> "/" Then
                            strFile = "~/" & strFile
                        Else
                            strFile = "~" & strFile
                        End If

                        If IO.File.Exists(Server.MapPath(strFile)) Then
                            If clsScan_Attach_Documents.IsValidImage(Server.MapPath(strFile)) Then
                                img_picViewImage.ImageUrl = strFile
                                'Dim link As LinkButton = CType(e.Item.Cells(0).FindControl("lnkPreview"), LinkButton)
                                'link.PostBackUrl = strFile
                            Else

                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                                'Process.Start(Server.MapPath(strFile))
                                Dim url As String = Session("ArutiSelfServiceURL").ToString & strFile.Replace("~", "")
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenNew", "opennewtab('" & url & "')", True)
                                'SHANI (16 JUL 2015) -- End 


                            End If
                        Else
                            Dim StrMessage As String = "Unable to preview. Reson : File [ " & e.Item.Cells(4).Text & " ] has been " & _
                                                        "removed or renamed or moved to other location."
                            DisplayMessage.DisplayMessage(StrMessage, Me)
                        End If
                    Else
                        Dim StrMessage As String = "Unable to preview. Reson : File [ " & e.Item.Cells(4).Text & " ] has been " & _
                                                       "removed or renamed or moved to other location."
                        DisplayMessage.DisplayMessage(StrMessage, Me)
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvPreviewList_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lvPreviewList_ItemCommand:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
