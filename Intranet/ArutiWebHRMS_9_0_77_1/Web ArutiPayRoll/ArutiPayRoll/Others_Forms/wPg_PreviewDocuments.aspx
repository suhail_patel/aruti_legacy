﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_PreviewDocuments.aspx.vb"
    Inherits="Leave_wPg_PreviewDocuments" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>
<script type="text/javascript">
    function opennewtab(url)
    {
        if(url != ''){
            window.open(url);
        }
    }
</script>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 70%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Preview Documents"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Preview Documents"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnlPreview" runat="server" Height="400px" Width="775px">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td>
                                                                <%--<asp:ImageMap ID="imgMap_picViewImage" Height="390px" ImageAlign="Middle" Width="773px"
                                                                    runat="server">
                                                                </asp:ImageMap>--%>
                                                                <asp:Image ID="img_picViewImage" runat="server" Height="390px" ImageAlign="Middle"
                                                                    Width="773px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnl_lvPreviewList" runat="server" Width="100%" style="max-height:250px" ScrollBars="Auto">
                                        <asp:DataGrid ID="lvPreviewList" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                            HeaderStyle-Font-Bold="false" Width="99%">
                                            <Columns>
                                                <asp:TemplateColumn HeaderStyle-Width="60px" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPreview" runat="server" Text="Preview" target="" Font-Underline="false"
                                                            ToolTip="Preview" CommandName="Preview">                                                                                              
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="code" HeaderText="Code" FooterText="colhCode"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="names" HeaderText="Employee/Applicant" FooterText="colhName">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="document" HeaderText="Document" FooterText="colhDocument">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="filename" HeaderText="Filename" FooterText="colhFileName">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="doctype" HeaderText="objcolhDocType" Visible="false"
                                                    FooterText="objcolhDocType"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="filepath" HeaderText="objcolhFullPath" Visible="false"
                                                    FooterText="objcolhFullPath"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </asp:Panel>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass=" btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
