﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_AddEditClaimAndRequestList.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_AddEditClaimAndRequestList" Title="Add/Edit Claim & Request" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script>
        function IsValidAttach() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Claim & Request"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                <%--'Pinkal (20-Nov-2018) -- Start
                                'Enhancement - Working on P2P Integration for NMB.--%>
                                <div style="text-align: right">
                                    <asp:LinkButton ID="LnkViewDependants" runat="server" Text="View Depedents List"
                                        CssClass="lnkhover" Font-Bold="true"></asp:LinkButton>
                                </div>
                                <%--'Pinkal (20-Nov-2018) -- End--%>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 35%; border-right: 2px solid #DDD">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="3">
                                                        <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period" Width="0px"></asp:Label>
                                                        <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" Width="0px" Height="20px"
                                                            AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No"></asp:Label>
                                                    </td>
                                                    <td style="width: 25%">
                                                        <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lblDate" runat="server" Style="margin-left: 5px" Text="Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 40%">
                                                        <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="3">
                                                        <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="3">
                                                        <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" Enabled="False">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="objlblValue" runat="server" Text="Leave Form"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="3">
                                                        <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="true" Enabled="False">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <%--'Pinkal (20-Nov-2018) -- Start
                                                'Enhancement - Working on P2P Integration for NMB.--%>
                                                <%--<tr style="width: 100%">
                                                    <td style="width: 20%">
                                                    </td>
                                                    <td style="width: 80%; padding-top: 5px" colspan="3">
                                                        <asp:LinkButton ID="LnkViewDependants" runat="server" Font-Underline="true" Text="View Depedents List"
                                                            CssClass="lnkhover"></asp:LinkButton>
                                                    </td>
                                                </tr>--%>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%; vertical-align: top;">
                                                        <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="3">
                                                        <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--'Pinkal (20-Nov-2018) -- End--%>
                                            </table>
                                        </td>
                                        <td style="width: 65%; padding-left: 5px">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                                                    </td>
                                                      <%--   'Pinkal (04-Feb-2019) -- Start 
                                                              'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                              
                                                    <td style="width: 50%" colspan="3">
                                                        <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" Width="350px">
                                                        </asp:DropDownList>
                                                    </td>
                                                      <td style="width: 10%">
                                                        <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                    </td>
                                                     <td style="width: 20%;padding:0" colspan="1">
                                                        <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    
                                                      <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                    
                                                </tr>
                                                <%--'Pinkal (20-Nov-2018) -- Start
                                                'Enhancement - Working on P2P Integration for NMB.--%>
                                                <%--'Pinkal (30-Apr-2018) - Start
                                                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.--%>
                                                
                                                <%-- <tr style="width: 100%">
                                                   <td style="width: 14%">
                                                            <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                            Enabled="false" Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 14%">
                                                        <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                    </td>
                                                    <td style="width: 27%">
                                                        <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>
                                                      <td style="width: 11%">
                                                        <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                    </td>
                                                   
                                                    <td style="width: 13%">
                                                        <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                            Enabled="false" Visible="false"></asp:TextBox>
                                                        <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                     <td style="width: 14%">
                                                        <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 14%">
                                                        <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                    </td>
                                                    <td style="width: 27%">
                                                        <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="168px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 11%">
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="0"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                    </td>
                                                </tr>--%>
                                              
                                                <tr style="width: 100%">
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                    </td>
                                                     <td style="width: 50%" colspan="3">
                                                        <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="350px">
                                                        </asp:DropDownList>
                                                    </td>
                                                  <%--   'Pinkal (04-Feb-2019) -- Start 
                                                              'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                    </td>
                                                     <td style="width: 20%;padding:0" colspan="1">
                                                        <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                    </td>--%>
                                                      <td style="width: 10%">
                                                        <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                    </td>
                                                     <td style="width: 20%;padding:0" colspan="1">
                                                     
                                                      <%--'Pinkal (04-Feb-2020) -- Start
                                                      'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                                                        <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>--%>
                                                            <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="1" AutoPostBack="true"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                       <%-- 'Pinkal (04-Feb-2020) -- End--%>
                                                    </td>
                                                    
                                                    <%--       'Pinkal (04-Feb-2019) -- End  --%>
                                                </tr>
                                                
                                                <tr style="width: 100%">
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                    </td>
                                                     <td style="width: 50%" colspan="3">
                                                        <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" Width="350px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    
                                                  <%--  'Pinkal (04-Feb-2019) -- Start 
                                                              'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB. 
                                                    <td style="width: 10%">
                                                        <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                    </td>
                                                     <td style="width: 20%;padding:0" colspan="1">
                                                        <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                    </td>--%>
                                                      <td style="width: 13%">
                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                            <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                            <%--'Pinkal (07-Feb-2020) -- Start
                                                            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB 
                                                            <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="0"   onKeypress="return onlyNumbers(this,event);"></asp:TextBox>--%>
                                                            <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="1.00"   onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                            <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"  Enabled="false" Visible="false"></asp:TextBox>
                                                             <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                    </td>
                                                    <%--  'Pinkal (04-Feb-2019) -- End --%>
                                                </tr>
                                                
                                                <tr style="width: 100%">
                                                    <td style="width: 14%">
                                                        <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                            Enabled="false" Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 11%">
                                                        <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                  <%--   'Pinkal (04-Feb-2019) -- Start 
                                                              'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB. 
                                                    <td style="width: 13%">
                                                    <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                        <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                    <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="0"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                        <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                            Enabled="false" Visible="false"></asp:TextBox>
                                                        <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                    </td>--%>
                                                    <td style="width: 13%">
                                                            <asp:Label ID="LblCurrency" runat="server" Text="Currency"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                            <asp:DropDownList ID="cboCurrency" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <%--   'Pinkal (04-Feb-2019) -- End  -- %> 
                                                </tr>
                                                
                                                 <%--  'Pinkal (30-Apr-2018) - End--%>
                                                <%--'Pinkal (20-Nov-2018) -- End--%>
                                                <tr style="width: 100%">
                                                    <td colspan="5">
                                                        <cc1:TabContainer ID="tabRemarks" runat="server" ActiveTabIndex="0" Width="100%">
                                                            <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark"><ContentTemplate><asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox></ContentTemplate></cc1:TabPanel>
                                                            <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark"><ContentTemplate><asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox></ContentTemplate></cc1:TabPanel>
                                                        </cc1:TabContainer>
                                                    </td>
                                                    <%--'Pinkal (20-Nov-2018) -- Start
                                                     'Enhancement - Working on P2P Integration for NMB.--%>
                                                    <%-- <td style="width: 25%" valign="top" colspan="2">
                                                        <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                        <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                            Style="height: 53px; width: 100%" ReadOnly="true"></asp:TextBox>
                                                    </td>--%>
                                                   
                                                    <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    <td style="width: 15%;vertical-align: bottom;" align="right" colspan="2">
                                                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btnDefault" />
                                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btnDefault" Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%; margin-top: 10px">
                                    <caption>
                                        <tr>
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvData" runat="server" Height="350px" ScrollBars="Auto">
                                                    <asp:DataGrid ID="dgvData" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" HeaderStyle-Font-Bold="false"
                                                        ItemStyle-CssClass="griviewitem" ShowFooter="False" Style="margin: auto" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                HeaderText="Edit" ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                            ToolTip="Edit"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                HeaderText="Delete" ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" CommandName="attachment" ToolTip="Attachment">
                                                                            <i class="fa fa-paperclip" aria-hidden="true" style="font-size:20px;font-weight:bold"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc" />
                                                            <asp:BoundColumn DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route" />
                                                            <asp:BoundColumn DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" />
                                                            <asp:BoundColumn DataField="quantity" FooterText="dgcolhQty" HeaderStyle-HorizontalAlign="Right"
                                                                HeaderText="Quantity" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderStyle-HorizontalAlign="Right"
                                                                HeaderText="Unit Price" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="amount" FooterText="dgcolhAmount" HeaderStyle-HorizontalAlign="Right"
                                                                HeaderText="Amount" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="expense_remark" FooterText="dgcolhExpenseRemark" HeaderText="Expense Remark"
                                                                Visible="true" />
                                                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" Visible="false" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" Visible="false" />
                                                        </Columns>
                                                        <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                            Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </caption>
                                    <tr style="width: 100%">
                                        <td style="width: 100%" align="right">
                                            <div align="right">
                                                <asp:Label ID="lblGrandTotal" runat="server" Style="margin-right: 5px" Text="Grand Total"></asp:Label><asp:TextBox
                                                    ID="txtGrandTotal" runat="server" Style="text-align: right" Width="20%" Enabled="False"></asp:TextBox></div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="btnfixedbottom" class="btn-default">
                                    <asp:Button ID="btnScanAttchment" runat="server" Text="Scan/Attchment" Visible="false"
                                        CssClass="btndefault" />
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="newpopup" Width="60%" Style="display: none;">
                    <div class="panel-primary" style="margin: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label></div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <%--<div id="Div2" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                    </div>
                                </div>--%>
                                <div id="Div3" class="panel-body-default">
                                    <asp:Panel ID="pnlEmpDependentpopup" runat="server" Width="100%" Height="100%">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" FooterText="dgcolhName" />
                                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" FooterText="dgcolhGender" />
                                                            <asp:BoundColumn DataField="age" HeaderText="Age" ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAge" />
                                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhMonth" />
                                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" FooterText="dgcolhRelation" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="ModalPopupBG"
                    CancelControlID="btnEmppnlEmpDepedentsClose" PopupControlID="pnlEmpDepedents"
                    TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlEmpDepedents">
                </cc1:ModalPopupExtender>
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                    CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" TargetControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                    Style="display: none;">
                    <div class="panel-primary" style="margin: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div2" class="panel-default">
                                <div id="Div4" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                    Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                    <div id="fileuploader">
                                                        <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Add" />
                                                    </div>
                                                </asp:Panel>
                                                <%--'Hemant (30 Nov 2018) -- Start
                                                'Enhancement : Including Language Settings For Scan/Attachment Button--%>
                                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click" Text="Browse" />
                                                <%--<asp:Button ID="btnScanAttchment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click" Text="Browse" />--%>
                                                <%--'Hemant (30 Nov 2018) -- End--%>
                                                
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="3" style="width: 100%">
                                                <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                        ToolTip="Delete"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--0--%>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                        <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--1--%>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                        <%--2--%>
                                                        <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                        <%--3--%>
                                                        <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                        <%--4--%>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                        <div style="float: left">
                                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                        </div>
                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To delete?:" />
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                 <%--  'Pinkal (22-Oct-2018) -- Start
                   'Enhancement - Implementing Claim & Request changes For NMB .--%>
                <ucCfnYesno:Confirmation ID="popup_UnitPriceYesNo" runat="server" Message="" Title="Confirmation" />
                 <%--  'Pinkal (22-Oct-2018) -- End --%>
                 
                 
                  <%--  'Pinkal (04-Feb-2019) -- Start
                   'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                <ucCfnYesno:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Message="" Title="Confirmation" />
                 <%--  'Pinkal (04-Feb-2019) -- End --%>
                 
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_AddEditClaimAndRequestList.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>
<%--<asp:Panel ID="pblMian" runat="server" CssClass="pnlcontentmai" Width="100%">
        <table style="width: 100%;" id="tblmain">
            <tr style="width: 100%;">
                <td colspan="2">
                    <div id="Loginhe">
                        <uc1:Closebutton ID="Closebotton1" PageHeading="Add/Edit Claim & Request" runat="server" />
                    </div>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="border-right: solid 1px black; width: 40%">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 25%">
                                <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat." Width="100%"></asp:Label>
                            </td>
                            <td style="width: 75%" colspan="3">
                                <asp:DropDownList ID="cboExpCategory" runat="server" Width="100%" Height="20px" AutoPostBack="true">
                                </asp:DropDownList>
                                
                                <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period" Width="0px"></asp:Label>
                                 <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" Width="0px" Height="20px" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 25%">
                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" Width="100%"></asp:Label>
                            </td>
                            <td style="width: 30%">
                                <asp:TextBox ID="txtClaimNo" runat="server" Width="95%"></asp:TextBox>
                            </td>
                            <td style="width: 10%">
                                <asp:Label ID="lblDate" runat="server" Text="Date" Width="100%"></asp:Label>
                            </td>
                            <td style="width: 35%">
                                <uc2:DateCtrl ID="dtpDate" runat="server" Width="75" />
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 25%">
                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 75%">
                                <asp:DropDownList ID="cboEmployee" runat="server" Width="100%" Height="20px" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 25%">
                                <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 75%">
                                <asp:DropDownList ID="cboLeaveType" runat="server" Width="100%" Height="20px" AutoPostBack="true"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 25%">
                                <asp:Label ID="objlblValue" runat="server" Text="Leave Form"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 75%">
                                <asp:DropDownList ID="cboReference" runat="server" Width="100%" Height="20px" AutoPostBack="true"
                                    Enabled="False">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td colspan="3" style="font-weight: bold">
                                <asp:LinkButton ID="LnkViewDependants" runat="server" ForeColor="#006699" Text="View Depedents List"
                                    CssClass="lnkhover"></asp:LinkButton>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="border-right: solid 1px black; width: 60">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 10%">
                                <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                            </td>
                            <td colspan="5" style="width: 90%">
                                <asp:DropDownList ID="cboExpense" runat="server" Width="100%" Height="20px" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 10%">
                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                            </td>
                            <td style="width: 25%">
                                <asp:DropDownList ID="cboSectorRoute" runat="server" Width="100%" Height="20px" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 13%">
                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                            </td>
                            <td style="width: 20%">
                                <asp:TextBox ID="txtUoMType" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width: 12%">
                                <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label>
                            </td>
                            <td style="width: 20%">
                                <asp:TextBox ID="txtBalance" runat="server" Width="95%" CssClass="txttextalignright"
                                    Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 10%">
                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                            </td>
                            <td style="width: 25%">
                                <asp:TextBox ID="txtCosting" runat="server" Width="95%" CssClass="txttextalignright"
                                    Text="0.00" Enabled="false" Visible="false"></asp:TextBox>
                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                            </td>
                            <td style="width: 12%">
                                <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                            </td>
                            <td style="width: 20%">
                                <asp:TextBox ID="txtQty" runat="server" Width="95%" Style="text-align: right" Text="0"
                                    AutoPostBack="true" onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                            </td>
                            <td style="width: 12%">
                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                            </td>
                            <td style="width: 20%">
                                <asp:TextBox ID="txtUnitPrice" runat="server" Width="95%" Style="text-align: right"
                                    Text="0" onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td colspan="4" style="width: 64%">
                                <cc1:TabContainer ID="tabmain" runat="server" ActiveTabIndex="0" Width="100%" Height="35">
                                    <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark">
                                        <HeaderTemplate>
                                            Expense Remark
                                        </HeaderTemplate>
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine" Width="100%" Height="100%"></asp:TextBox>
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark">
                                        <ContentTemplate>
                                            <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" Width="100%"
                                                Height="100%"></asp:TextBox>
                                        </ContentTemplate>
                                    </cc1:TabPanel>
                                </cc1:TabContainer>
                            </td>
                            <td colspan="2" style="width: 35%">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btnDefault" Width="70px" />
                                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btnDefault" Width="70px"
                                    Visible="false" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr style="width: 100%">
                <td colspan="2">
                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" SkinID="selectgrid"
                        AllowPaging="True" ShowFooter="False" Width="100%" PageSize="10">
                        <Columns>
                            <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit" CommandName="Edit" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                        CommandName="Delete" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="expense" HeaderText="Expense" ReadOnly="true" FooterText="dgcolhExpense" />
                            <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                            <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"   ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                            <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"  ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                            <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"  ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                            <asp:BoundColumn DataField="expense_remark" HeaderText="dgcolhRemark" ReadOnly="true"
                                Visible="false" />
                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                Visible="false" />
                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                Visible="false" />
                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                        </Columns>
                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                    </asp:DataGrid>
                </td>
            </tr>
            <tr style="width: 100%">
                <td colspan="2" style="text-align: right">
                    <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                    <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="txttextalignright"></asp:TextBox>
                </td>
            </tr>
            <tr style="width: 100%">
                <td colspan="2" style="text-align: right">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" Width="70px" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" Width="70px" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="ModalPopupBG"
        CancelControlID="btnEmppnlEmpDepedentsClose" PopupControlID="pnlEmpDepedents"
        TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlEmpDepedents">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnlEmpDepedents" runat="server" Style="cursor: move; display: none;
        padding: 10px; width: 650px; border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
        -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
        -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
        BackColor="#5377A9" DefaultButton="btnEmppnlEmpDepedentsClose">
        <asp:Panel ID="pnlEmpDependentpopup" runat="server" Width="100%" Height="100%">
            <table>
                <tr>
                    <td style="text-align: center">
                        <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List" Width="630px"
                            ForeColor="White" Style="margin-left: 5px; font-size: medium" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                            SkinID="selectgrid" Width="100%" PageSize="20" ItemStyle-Height="7px" DataKeyField="">
                            <Columns>
                                <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right; overflow: auto; margin-left: 5px; margin-top: 5px;
                        margin-bottom: 10px;">
                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" Width="70px"
                            CssClass="btnDefault" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlpopup" Width="100%" Height="100%" runat="server" Style="display: none"
        CssClass="modalPopup">
        <asp:Panel ID="Panel2" runat="server" Style="cursor: move; background-color: #DDDDDD;
            border: solid 1px Gray; color: Black">
            <div>
                <p>
                    Are ou Sure You Want To delete?:</p>
            </div>
        </asp:Panel>
        <table style="width: 180px;">
            <tr>
                <td>
                    Reason:
                </td>
                <td>
                    <asp:TextBox ID="txtreasondel" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btnDefault" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                </td>
                <td>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <cc1:ModalPopupExtender ID="popup1" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="BtnCancel" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreasondel">
    </cc1:ModalPopupExtender>--%>
