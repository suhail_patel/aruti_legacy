﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class Claims_And_Expenses_wPg_AddEditClaimAndRequestList
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimMaster As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Pinkal (05-Sep-2020) -- End

    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False
    Private Shared ReadOnly mstrModuleName As String = "frmClaims_RequestAddEdit"
    Private ReadOnly mstrModuleName1 As String = "frmDependentsList"
    Private mintDeleteIndex As Integer = -1
    Private mdtAttchment As DataTable = Nothing
    Private blnShowAttchmentPopup As Boolean = False
    Private mdtFinalAttchment As DataTable = Nothing

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsEditClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

     'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Private mblnIsClaimFormBudgetMandatory As Boolean = False
    'Pinkal (20-Nov-2018) -- End


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestMasterId As Integer = 0
    Dim mintClaimRequestExpenseTranId As Integer = 0
    Dim mstrClaimRequestExpenseGUID As String = ""
    'Pinkal (07-Mar-2019) -- End

    'Pinkal (29-Aug-2019) -- Start
    'Enhancement NMB - Working on P2P Get Token Service URL.
    Dim mstrP2PToken As String = ""
    'Pinkal (29-Aug-2019) -- End



    'Pinkal (18-Mar-2021) -- Start
    'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
    Private mintExpenseIndex As Integer = -1
    'Pinkal (18-Mar-2021) -- End


#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()

            If IsPostBack = False AndAlso Request.QueryString("uploadimage") Is Nothing Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                Session.Remove("mdtFinalAttchment")
                Session.Remove("mdtAttchment")
                Dim mdtTran As DataTable = Nothing
                Me.ViewState.Add("mintExpenseCategoryID", 0)
                Me.ViewState.Add("ClaimRequestMasterId", 0)
                Me.ViewState.Add("mintEmployeeID", 0)
                Me.ViewState.Add("mintLeaveTypeId", 0)
                Me.ViewState.Add("mdtTran", mdtTran)
                Me.ViewState.Add("MaxDate", CDate(eZeeDate.convertDate("99981231")))
                Me.ViewState.Add("MinDate", CDate(eZeeDate.convertDate("17530101")))
                Me.ViewState.Add("PDate", ConfigParameter._Object._CurrentDateAndTime.Date)
                Me.ViewState.Add("mintLeaveFormID", -1)
                Me.ViewState.Add("mblnIsLeaveEncashment", False)
                Me.ViewState.Add("EditGuid", "Null")
                Me.ViewState.Add("EditCrtranunkid", 0)
                txtUnitPrice.Attributes.Add("onfocusin", "select();")
                SetVisibility()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                FillCombo()
                Dim objClaimMaster As New clsclaim_request_master
                Dim objClaimTran As New clsclaim_request_tran

                If Session("mintClaimRequestMasterId") IsNot Nothing Then
                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'Me.ViewState("ClaimRequestMasterId") = Session("mintClaimRequestMasterId");
                    'objClaimMaster._Crmasterunkid = CInt(Me.ViewState("ClaimRequestMasterId"))
                    mintClaimRequestMasterId = CInt(Session("mintClaimRequestMasterId"))
                    objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
                    Session("mintClaimRequestMasterId") = Nothing
                    'Pinkal (07-Mar-2019) -- End

                    If objClaimMaster._Crmasterunkid > 0 Then
                        Enable_Disable_Ctrls(False)
                    End If
                    If objClaimMaster._Referenceunkid > 0 Then
                        Me.ViewState("mintLeaveFormID") = objClaimMaster._Referenceunkid
                    End If
                End If


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'GetValue()
                GetValue(objClaimMaster)
                'Pinkal (05-Sep-2020) -- End



                objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId

                mdtTran = objClaimTran._DataTable
                Me.ViewState("mdtTran") = mdtTran
                Fill_Expense()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objClaimTran = Nothing
                objClaimMaster = Nothing
                'Pinkal (05-Sep-2020) -- End

                If mintClaimRequestMasterId > 0 Then
                    If Session("ClaimRequest_EmpUnkId") IsNot Nothing Then
                        cboEmployee.SelectedValue = Session("ClaimRequest_EmpUnkId").ToString()
                    End If
                End If
                cboExpCategory.Focus()

                If mdtFinalAttchment Is Nothing Then
                    Dim objAttchement As New clsScan_Attach_Documents
                    'Pinkal (16-Jul-2020) -- Start
                    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
                        'S.SANDEEP |04-SEP-2021| -- START
                        'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                        'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "ISNULL(hrdocuments_tran.scanattachrefid,-1) =  " & enScanAttactRefId.CLAIM_REQUEST, CInt(cboEmployee.SelectedValue))
                        Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "ISNULL(hrdocuments_tran.scanattachrefid,-1) =  " & enScanAttactRefId.CLAIM_REQUEST, CInt(cboEmployee.SelectedValue) _
                                                                , False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                        'S.SANDEEP |04-SEP-2021| -- END
                    End If
                    Dim strTranIds As String = String.Join(",", mdtTran.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                    If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                    'mdtFinalAttchment = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    mdtFinalAttchment = New DataView(objAttchement._Datatable, "transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    'Pinkal (16-Jul-2020) -- End
                    objAttchement = Nothing
                Else
                    mdtAttchment = mdtFinalAttchment.Copy
                End If
            Else
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                blnShowAttchmentPopup = CBool(Me.ViewState("blnShowAttchmentPopup"))
                mdtFinalAttchment = CType(Me.Session("mdtFinalAttchment"), DataTable)
                mblnIsEditClick = CBool(Me.ViewState("mblnIsEditClick"))
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
                mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsClaimFormBudgetMandatory"))
                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mintClaimRequestMasterId = CInt(Me.ViewState("ClaimRequestMasterId"))
                mintClaimRequestExpenseTranId = CInt(Me.ViewState("ClaimRequestExpenseTranId"))
                mstrClaimRequestExpenseGUID = CStr(Me.ViewState("ClaimRequestExpenseGUID"))
                If Me.ViewState("P2PToken") IsNot Nothing Then
                mstrP2PToken = Me.ViewState("P2PToken").ToString()
                Else
                    mstrP2PToken = ""
                End If



                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                mintExpenseIndex = CInt(Me.ViewState("mintExpenseIndex"))
                'Pinkal (18-Mar-2021) -- End

            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            If blnShowAttchmentPopup Then
                popup_ScanAttchment.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            Session.Remove("mintClaimRequestMasterId")
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Unload:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            Me.Session("mdtAttchment") = mdtAttchment
            Me.ViewState("blnShowAttchmentPopup") = blnShowAttchmentPopup
            Me.Session("mdtFinalAttchment") = mdtFinalAttchment
            Me.ViewState("mblnIsEditClick") = mblnIsEditClick
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
            Me.ViewState("IsClaimFormBudgetMandatory") = mblnIsClaimFormBudgetMandatory
            Me.ViewState("IsHRExpense") = mblnIsHRExpense
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("ClaimRequestMasterId") = mintClaimRequestMasterId
            Me.ViewState("ClaimRequestExpenseTranId") = mintClaimRequestExpenseTranId
            Me.ViewState("ClaimRequestExpenseGUID") = mstrClaimRequestExpenseGUID
            Me.ViewState("P2PToken") = mstrP2PToken


            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            Me.ViewState("mintExpenseIndex") = mintExpenseIndex
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEMaster As New clsEmployee_Master
        Try
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CInt(Me.ViewState("mintEmployeeID")) > 0 Then
                    blnSelect = False
                    intEmpId = CInt(Me.ViewState("mintEmployeeID"))
                End If
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = Me.ViewState("mintEmployeeID").ToString()
            End With
            objEMaster = Nothing


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.

            'Select Case CInt(Me.ViewState("mintExpenseCategoryID"))
            '    Case enExpenseType.EXP_LEAVE
            '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False)
            '    Case Else
            '        dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            'End Select

            Select Case CInt(Me.ViewState("mintExpenseCategoryID"))
                Case enExpenseType.EXP_LEAVE
                    dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, False)
                Case Else
                    dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            End Select

            'Pinkal (11-Sep-2019) -- End

            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = Me.ViewState("mintExpenseCategoryID").ToString()
            End With

            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())

            Dim objcommonMst As New clsCommon_Master
            If CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
                With cboSectorRoute
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
            End If

            dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With
            objcommonMst = Nothing

            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
                .DataBind()
            End With
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objCostCenter = Nothing
            'Pinkal (05-Sep-2020) -- End



            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (05-Sep-2020) -- End



        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            End If


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If mblnPaymentApprovalwithLeaveApproval AndAlso CInt(Me.ViewState("ClaimRequestMasterId")) > 0 Then
            If mblnPaymentApprovalwithLeaveApproval AndAlso mintClaimRequestMasterId > 0 Then
                'Pinkal (07-Mar-2019) -- End
                cboReference.Enabled = False
                cboEmployee.Enabled = False
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("SetVisibility" & ex.Message, Me)
        Finally
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub GetValue()
    Private Sub GetValue(ByVal objClaimMaster As clsclaim_request_master)
        'Pinkal (05-Sep-2020) -- End
        Try
            txtClaimNo.Text = objClaimMaster._Claimrequestno

            If CInt(Me.ViewState("mintEmployeeID")) > 0 Then
                cboEmployee.SelectedValue = Me.ViewState("mintEmployeeID").ToString()
            Else
                cboEmployee.SelectedValue = objClaimMaster._Employeeunkid.ToString()
            End If

            If CInt(Me.ViewState("mintExpenseCategoryID")) > 0 Then
                cboExpCategory.SelectedValue = Me.ViewState("mintExpenseCategoryID").ToString()
            Else
                cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid.ToString()
            End If
            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())

            If objClaimMaster._Transactiondate <> Nothing Then
                dtpDate.SetDate = objClaimMaster._Transactiondate
            Else
                dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
            End If

            Call dtpDate_TextChanged(dtpDate, New EventArgs)

            txtClaimRemark.Text = objClaimMaster._Claim_Remark

            If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                cboLeaveType.SelectedValue = Me.ViewState("mintLeaveTypeId").ToString()
            Else
                cboLeaveType.SelectedValue = objClaimMaster._LeaveTypeId.ToString()
            End If
            Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs()) '2Time

            cboReference.SelectedValue = objClaimMaster._Referenceunkid.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
            Dim mdtTran As DataTable = Nothing
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End

            Dim mdView As New DataView
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            If mdView.ToTable.Rows.Count > dgvData.PageSize Then
                dgvData.AllowPaging = True
            Else
                dgvData.AllowPaging = False
            End If
            dgvData.DataSource = mdView
            dgvData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D'")), Session("fmtCurrency").ToString())
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If
            If dgvData.Items.Count <= 0 Then
                dtpDate.Enabled = True
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If
            'Pinkal (10-Feb-2021) -- End


            If CInt(cboExpense.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboExpense.Focus()
                Return False
            End If

            If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQty.Focus()
                Return False
            End If


            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                cboSectorRoute.Focus()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objExpense = Nothing
                'Pinkal (05-Sep-2020) -- End
                Return False
            End If

            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(txtQty.Text) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objExpense = Nothing
                'Pinkal (05-Sep-2020) -- End
                Return False
            End If

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Return False
            End If
            'Pinkal (10-Jun-2020) -- End


            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            If CBool(Session("ClaimRemarkMandatoryForClaim")) AndAlso txtClaimRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Claim Remark is mandatory information. Please enter claim remark to continue."), Me)
                tabRemarks.ActiveTabIndex = 1
                txtClaimRemark.Focus()
                Return False
            End If
            'Pinkal (20-May-2019) -- End




            'Pinkal (20-APR-2018) -- Start
            'Bug - [0002189] Unable to apply for overtime on self service,We have an issue with pacra where they can apply for overtime normally when applying using desktop application but when the attempt to use self service
            'If CBool(Session("PaymentApprovalwithLeaveApproval"))Then

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 46, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboCurrency.Focus()
                Return False
            End If

            Dim blnIsHRExpense As Boolean = True
            If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                blnIsHRExpense = objExpense._IsHRExpense
            End If

            If blnIsHRExpense Then

                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then

                    Dim objapprover As New clsleaveapprover_master
                    Dim dtList As DataTable = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                                              CInt(Session("Fin_year")), _
                                                                              CInt(Session("CompanyUnkId")), _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                           False, -1, _
                                                                              CInt(cboEmployee.SelectedValue), -1, -1, Session("LeaveApproverForLeaveType").ToString(), Nothing)

                    If dtList.Rows.Count = 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user."), Me)
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objapprover = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Return False
                    End If

                    If dtList.Rows.Count > 0 Then
                        Dim objUsermapping As New clsapprover_Usermapping
                        Dim isUserExist As Boolean = False
                        For Each dr As DataRow In dtList.Rows
                            objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                            If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                        Next

                        If isUserExist = False Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please Map this employee's Leave Approver to system user."), Me)
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objUsermapping = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Return False
                        End If

                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objUsermapping = Nothing
                        'Pinkal (05-Sep-2020) -- End

                    End If

                    If CBool(Session("LeaveApproverForLeaveType")) Then
                        dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                                                     CInt(Session("Fin_year")), _
                                                                                     CInt(Session("CompanyUnkId")), _
                                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                                    False, -1, CInt(cboEmployee.SelectedValue), -1, CInt(cboLeaveType.SelectedValue), _
                                                                                     Session("LeaveApproverForLeaveType").ToString(), Nothing)

                        If dtList.Rows.Count = 0 Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Please Map this Leave type to this employee's Leave Approver(s)."), Me)
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtList IsNot Nothing Then dtList.Clear()
                            dtList = Nothing
                            objapprover = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Return False
                        End If

                    End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtList IsNot Nothing Then dtList.Clear()
                    dtList = Nothing
                    objapprover = Nothing
                    'Pinkal (05-Sep-2020) -- End


                ElseIf CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then

                    Dim objExapprover As New clsExpenseApprover_Master
                    Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(cboEmployee.SelectedValue), "List", Nothing)
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user."), Me)
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objExapprover = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Return False
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objExapprover = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If

            End If

            objExpense = Nothing

            Dim sMsg As String = String.Empty


            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                                  , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimRequestMasterId)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimMaster As New clsclaim_request_master
            'Pinkal (05-Sep-2020) -- End
            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                              , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                             , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimRequestMasterId, True)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster = Nothing
            'Pinkal (05-Sep-2020) -- End

            'Pinkal (22-Jan-2020) -- End


            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Function
            End If
            sMsg = ""



            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim mdtTran As DataTable = Nothing
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End

            If mdtTran IsNot Nothing Then
                Dim objExpBalance As New clsEmployeeExpenseBalance
                Dim dsBalance As DataSet = objExpBalance.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")), dtpDate.GetDate.Date _
                                                                                               , CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)), _
                                                                                               CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)))

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objExpBalance = Nothing
                'Pinkal (05-Sep-2020) -- End

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestExpenseTranId > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestExpenseTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestExpenseGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestExpenseGUID.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            DisplayMessage.DisplayMessage(Language.getMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Language.getMessage("clsclaim_request_master", 4, " ] time(s)."), Me)

                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            ' Exit Function
                            Return False
                            'Pinkal (05-Sep-2020) -- End

                        End If  'If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then

                    End If  'If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                End If  ' If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsBalance IsNot Nothing Then dsBalance.Clear()
                dsBalance = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If  '  If mdtTran IsNot Nothing Then


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End


            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                If CInt(cboLeaveType.SelectedValue) > 0 Then
                    If cboReference.Enabled = True AndAlso CInt(cboReference.SelectedValue) > 0 Then
                        Dim objlvtype As New clsleavetype_master : Dim objlvform As New clsleaveform
                        objlvtype._Leavetypeunkid = CInt(cboLeaveType.SelectedValue)
                        If objlvtype._EligibilityOnAfter_Expense > 0 Then
                            objlvform._Formunkid = CInt(cboReference.SelectedValue)
                            Dim objlvissuemst As New clsleaveissue_master : Dim intIssueDays As Decimal = 0
                            intIssueDays = objlvissuemst.GetEmployeeTotalIssue(CInt(cboEmployee.SelectedValue), CInt(cboLeaveType.SelectedValue), True, Nothing, Nothing, False, objlvform._Formunkid, -1)
                            If intIssueDays < objlvtype._EligibilityOnAfter_Expense Then
                                'Pinkal (25-May-2019) -- Start
                                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & objlvtype._EligibilityOnAfter_Expense & ").", Me)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type."), Me)
                                'Pinkal (25-May-2019) -- End
                                objlvform = Nothing : objlvtype = Nothing : objlvissuemst = Nothing
                                Return False
                            End If
                            objlvissuemst = Nothing
                        End If
                        objlvform = Nothing : objlvtype = Nothing
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Validation:-" & ex.Message, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboExpCategory.Enabled = iFlag
            cboEmployee.Enabled = iFlag
            cboPeriod.Enabled = iFlag
            dtpDate.Enabled = iFlag

            If mintClaimRequestMasterId > 0 Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'If objClaimMaster._Expensetypeid = enExpenseType.EXP_LEAVE Then
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    'Pinkal (05-Sep-2020) -- End
                    cboLeaveType.Enabled = iFlag
                End If
            Else
                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboLeaveType.Enabled = iFlag
                End If
            End If
            cboReference.Enabled = iFlag


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim mdtTran As DataTable = Nothing
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If
            'Pinkal (05-Sep-2020) -- End


            If mdtTran IsNot Nothing Then
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = CStr(mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                    cboCurrency.Enabled = True
                End If
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Enable_Disable_Controls:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = "0"
            cboSectorRoute.SelectedValue = "0"
            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
            txtExpRemark.Text = ""
            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtQty.Text = "0"
            txtQty.Text = "1"
            'Pinkal (04-Feb-2020) -- End

            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Text = "0"
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            mintEmpMaxCountDependentsForCR = 0
            cboCostCenter.SelectedValue = "0"
            mintClaimRequestExpenseTranId = 0
            mstrClaimRequestExpenseGUID = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If dgvData.Items.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please add atleast one expense in order to save."), Me)
                dgvData.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub SetValue(ByRef objClaimMaster As clsclaim_request_master)
        Try
            objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            'Pinkal (05-Sep-2020) -- End
            objClaimMaster._Cancel_Datetime = Nothing
            objClaimMaster._Cancel_Remark = ""
            objClaimMaster._Canceluserunkid = -1
            objClaimMaster._Iscancel = False
            objClaimMaster._Claimrequestno = txtClaimNo.Text
            objClaimMaster._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objClaimMaster._Expensetypeid = CInt(cboExpCategory.SelectedValue)
            objClaimMaster._Isvoid = False
            objClaimMaster._Loginemployeeunkid = -1
            objClaimMaster._Transactiondate = dtpDate.GetDateTime
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                objClaimMaster._Userunkid = CInt(Session("UserId"))
                objClaimMaster._Loginemployeeunkid = -1
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objClaimMaster._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objClaimMaster._Userunkid = -1
            End If
            objClaimMaster._Voiddatetime = Nothing
            objClaimMaster._Voiduserunkid = -1
            objClaimMaster._Claim_Remark = txtClaimRemark.Text
            objClaimMaster._FromModuleId = enExpFromModuleID.FROM_EXPENSE
            objClaimMaster._IsBalanceDeduct = False

            objClaimMaster._Statusunkid = 2 'DEFAULT PENDING

            Select Case CInt(cboExpCategory.SelectedValue)
                Case enExpenseType.EXP_LEAVE
                    objClaimMaster._Modulerefunkid = enModuleReference.Leave
                Case enExpenseType.EXP_MEDICAL
                    objClaimMaster._Modulerefunkid = enModuleReference.Medical
                Case enExpenseType.EXP_TRAINING
                    objClaimMaster._Modulerefunkid = enModuleReference.Training
                Case enExpenseType.EXP_MISCELLANEOUS
                    objClaimMaster._Modulerefunkid = enModuleReference.Miscellaneous
            End Select

            objClaimMaster._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            If CInt(cboExpCategory.SelectedValue) <> enExpenseType.EXP_MISCELLANEOUS Then

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                If cboLeaveType.SelectedValue IsNot Nothing AndAlso cboLeaveType.SelectedValue <> "" Then
                objClaimMaster._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
                End If
                If cboReference.SelectedValue IsNot Nothing AndAlso cboReference.SelectedValue <> "" Then
                objClaimMaster._Referenceunkid = CInt(cboReference.SelectedValue)
                End If
                'Pinkal (10-Feb-2021) -- End

            Else
                objClaimMaster._LeaveTypeId = 0
                objClaimMaster._Referenceunkid = 0
            End If

            objClaimMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            objClaimMaster._YearId = CInt(Session("Fin_Year"))

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmClaims_RequestAddEdit"
            'StrModuleName2 = "mnuUtilitiesMain"
            'StrModuleName3 = "mnuClaimsExpenses"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            objClaimMaster._CompanyID = CInt(Session("CompanyUnkId"))
            objClaimMaster._ArutiSelfServiceURL = Session("ArutiSelfServiceURL").ToString()
            If CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                objClaimMaster._LoginMode = enLogin_Mode.EMP_SELF_SERVICE
            ElseIf CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                objClaimMaster._LoginMode = enLogin_Mode.MGR_SELF_SERVICE
            End If

            'Pinkal (22-Oct-2021)-- Start
            'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
            objClaimMaster._FormName = "frmClaims_RequestAddEdit"
            objClaimMaster._ClientIP = Session("IP_ADD").ToString()
            objClaimMaster._HostName = Session("HOST_NAME").ToString()
            'Pinkal (22-Oct-2021)-- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttchment Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttchment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "FillAttachment", mstrModuleName)
            DisplayMessage.DisplayError("FillAttachment:-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtAttchment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtAttchment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Claim_Request
                dRow("scanattachrefid") = enScanAttactRefId.CLAIM_REQUEST


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("transactionunkid") = dgvData.Items(mintDeleteIndex).Cells(10).Text
                dRow("transactionunkid") = dgvData.Items(mintExpenseIndex).Cells(10).Text
                'Pinkal (18-Mar-2021) -- End

                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("GUID") = dgvData.Items(mintDeleteIndex).Cells(12).Text
                dRow("GUID") = dgvData.Items(mintExpenseIndex).Cells(12).Text
                'Pinkal (18-Mar-2021) -- End

                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))


                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.
                dRow("Documentype") = GetMimeType(strfullpath)
                Dim xDocumentData As Byte() = File.ReadAllBytes(strfullpath)

                'S.SANDEEP |15-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : Log Error Only
                'dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    CommonCodes.LogErrorOnly(ex)
                End Try
                'S.SANDEEP |15-AUG-2020| -- END

                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                'Pinkal (20-Nov-2018) -- End

                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END

            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtAttchment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            'DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
            DisplayMessage.DisplayError("AddDocumentAttachment:-" & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub AddExpense()
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End


            Dim dtmp() As DataRow = Nothing
            If mdtTran.Rows.Count > 0 Then

                dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                              "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")

                If dtmp.Length > 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If
            End If

            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End


            mblnIsHRExpense = objExpMaster._IsHRExpense
            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    End If
                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) > CDec(txtBalance.Text) Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtQty.Focus()
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMaster = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                End If
            End If

            If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMaster = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
            End If


            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMasterOld = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub

                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMasterOld = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
                objExpMasterOld = Nothing
            End If

            objExpMaster = Nothing

            Dim dRow As DataRow = mdtTran.NewRow

            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimRequestMasterId
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
            dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            dRow.Item("quantity") = txtQty.Text
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("loginemployeeunkid") = -1
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate

            mdtTran.Rows.Add(dRow)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdtTran
            Me.ViewState("mdtTran") = mdtTran.Copy()
            'Pinkal (05-Sep-2020) -- End

            Fill_Expense()
            Enable_Disable_Ctrls(False)
            Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub EditExpense()
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim iRow As DataRow() = Nothing

            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            If CInt(Me.ViewState("EditCrtranunkid")) > 0 Then
                iRow = mdtTran.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
            Else
                iRow = mdtTran.Select("GUID = '" & CStr(Me.ViewState("EditGuid")) & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Length > 0, CDec(txtQty.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Length > 0, CDec(txtQty.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If

                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                mblnIsHRExpense = objExpMaster._IsHRExpense

                mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""
                If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMaster = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If

                If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                            If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                                txtQty.Focus()
                                'Pinkal (05-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                objExpMaster = Nothing
                                'Pinkal (05-Sep-2020) -- End
                                Exit Sub
                            End If
                        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                            If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                                txtQty.Focus()
                                'Pinkal (05-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                objExpMaster = Nothing
                                'Pinkal (05-Sep-2020) -- End
                                Exit Sub
                            End If
                        End If

                    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                        If CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) > CDec(txtBalance.Text) Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    End If
                End If

                If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMaster = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                End If

                If dgvData.Items.Count >= 1 Then
                    Dim objExpMasterOld As New clsExpense_Master
                    Dim iEncashment As DataRow() = Nothing
                    If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                        iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                        iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                    End If

                    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMasterOld = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMasterOld = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                    objExpMasterOld = Nothing
                End If
                objExpMaster = Nothing

                iRow(0).Item("crtranunkid") = iRow(0).Item("crtranunkid")
                iRow(0).Item("crmasterunkid") = mintClaimRequestMasterId
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("quantity") = txtQty.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("loginemployeeunkid") = -1
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense
                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                iRow(0).AcceptChanges()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Me.ViewState("mdtTran") = mdtTran
                Me.ViewState("mdtTran") = mdtTran.Copy()
                'Pinkal (05-Sep-2020) -- End


                Fill_Expense()
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start 
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue))
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date)
            'Sohail (18 May 2019) -- End
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmployeeDepedentCountForCR : " & ex.Message, Me)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    'Pinkal (25-Oct-2018) -- End 


    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try

            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("GetP2PRequireData : " & ex.Message, Me)
        End Try
    End Sub

    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xCostCenterCode As String, ByVal xGLCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), Me)
                Return False
            ElseIf xCostCenterCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense."), Me)
                Return False
            End If

            mstrP2PToken = GetP2PToken()

            If xCostCenterCode.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then
                Dim mstrBgtRequestValidationP2PServiceURL As String = Session("BgtRequestValidationP2PServiceURL").ToString().Trim() & "?costCenter=" & xCostCenterCode.Trim() & "&glcode=" & xGLCode.Trim()
                Dim mstrGetData As String = ""
                Dim objMstData As New clsMasterData
                If objMstData.GetSetP2PWebRequest(mstrBgtRequestValidationP2PServiceURL, True, True, "CostcenterPassing", mstrGetData, mstrError, Nothing, "", mstrToken:=mstrP2PToken) = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objMstData = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Return False
                End If
                objMstData = Nothing
                'mstrGetData = "{""amount"":""10000""}"

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtAmount As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtAmount IsNot Nothing AndAlso dtAmount.Rows.Count > 0 Then
                        mdecBudgetAmount = CDec(dtAmount.Rows(0)("amount"))
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtAmount IsNot Nothing Then dtAmount.Clear()
                    dtAmount = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If

            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("CheckBudgetRequestValidationForP2P : " & ex.Message, Me)
            Return False
        End Try
    End Function

    Private Function UpdateDeleteAttachment(ByVal dr As DataRow, ByVal mblnDeleteAttachment As Boolean, ByVal mstrAUD As String) As Boolean
        Try
            If dr IsNot Nothing Then dr("AUD") = mstrAUD
            dr.AcceptChanges()

            If mblnDeleteAttachment Then
                Try
                    If dr IsNot Nothing Then File.Delete(dr("filepath").ToString())
                Catch ex As Exception
                End Try
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("UpdateDeleteAttachment  : " & ex.Message, Me)
            Return False
        End Try
    End Function


    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmployeeCostCenter : " & ex.Message, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function GetP2PToken() As String
        Dim mstrToken As String = ""
        Dim mstrError As String = ""
        Dim mstrGetData As String = ""
        Try
            If Session("GetTokenP2PServiceURL") IsNot Nothing AndAlso Session("GetTokenP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim objMstData As New clsMasterData
                Dim objCRUser As New clsCRP2PUserDetail
                If objMstData.GetSetP2PWebRequest(Session("GetTokenP2PServiceURL").ToString().Trim(), True, True, objCRUser.username, mstrGetData, mstrError, objCRUser, "", "") = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objMstData = Nothing
                    objCRUser = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Return mstrToken
                End If
                objCRUser = Nothing

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mstrToken = dtTable.Rows(0)("token").ToString()
                    End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End

                End If
                objMstData = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GetP2PToken : " & ex.Message, Me)
        End Try
        Return mstrToken
    End Function


    'Pinkal (22-Oct-2021)-- Start
    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.

    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
SendEmail:
                For Each objEmail In gobjEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    'Pinkal (22-Oct-2021)-- Start
                    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                    objSendMail._Form_Name = "frmClaims_RequestAddEdit"
                    objSendMail._ClientIP = Session("IP_ADD").ToString()
                    objSendMail._HostName = Session("HOST_NAME").ToString()
                    'Pinkal (22-Oct-2021)-- End


                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = intCompanyID(0)
                    End If
                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
                                                objEmail._ExportReportPath).ToString.Length > 0 Then
                        gobjEmailList.Remove(objEmail)
                        GoTo SendEmail
                    End If
                Next
                gobjEmailList.Clear()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2021)-- End

#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("mintClaimRequestMasterId") = Nothing
            Response.Redirect("~/Claims_And_Expenses/wPg_ClaimAndRequestList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then
                Exit Sub
            End If



            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If gRow IsNot Nothing Then gRow.ToList.Clear()
                gRow = Nothing
                'Pinkal (05-Sep-2020) -- End
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (05-Sep-2020) -- End

            mblnIsEditClick = False

            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Language.getMessage(mstrModuleName, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            If Validation() = False Then
                Exit Sub
            End If


            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If gRow IsNot Nothing Then gRow.ToList.Clear()
                gRow = Nothing
                'Pinkal (05-Sep-2020) -- End
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (05-Sep-2020) -- End

            mblnIsEditClick = True


            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End


            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Language.getMessage(mstrModuleName, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim mstrFolderName As String = ""
        Dim strFileName As String = ""
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimMaster As New clsclaim_request_master
        'Pinkal (05-Sep-2020) -- End
        Try
            If Is_Valid() = False Then Exit Sub

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
            Dim mdtTran As DataTable = Nothing
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            '  Call SetValue()
            Call SetValue(objClaimMaster)
            'Pinkal (05-Sep-2020) -- End


            Dim objExpense As New clsExpense_Master
            Dim xRow() As DataRow = Nothing

            For Each dRow As DataRow In mdtTran.Select("AUD <> 'D'")
                objExpense._Expenseunkid = CInt(dRow.Item("expenseunkid"))
                If objExpense._IsAttachDocMandetory = True Then

                    If CInt(dRow.Item("crtranunkid")) > 0 Then
                        xRow = mdtFinalAttchment.Select("transactionunkid = '" & CInt(dRow.Item("crtranunkid")) & "' AND AUD <> 'D' ")
                    ElseIf CStr(dRow.Item("GUID")).Trim.Length > 0 Then
                        xRow = mdtFinalAttchment.Select("GUID = '" & CStr(dRow.Item("GUID")) & "' AND AUD <> 'D' ")
                    End If
                    If xRow.Count <= 0 Then
                        DisplayMessage.DisplayMessage(dRow.Item("expense").ToString & " " & Language.getMessage(mstrModuleName, 22, "has mandatory document attachment. please attach document."), Me)
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpense = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                End If
            Next

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpense = Nothing
            'Pinkal (05-Sep-2020) -- End


            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                mblnPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            End If

            If mdtFinalAttchment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                For Each dRow As DataRow In mdtFinalAttchment.Rows
                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If mdsDoc IsNot Nothing Then mdsDoc.Clear()
                mdsDoc = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If

            With objClaimMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With


            If mintClaimRequestMasterId > 0 Then
                blnFlag = objClaimMaster.Update(Session("Database_Name").ToString, _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 True, mdtTran, ConfigParameter._Object._CurrentDateAndTime, mdtFinalAttchment, Nothing, mblnPaymentApprovalwithLeaveApproval)
            Else

                blnFlag = objClaimMaster.Insert(Session("Database_Name").ToString(), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 False, _
                                                mdtTran, ConfigParameter._Object._CurrentDateAndTime, mdtFinalAttchment, Nothing, mblnPaymentApprovalwithLeaveApproval)
            End If

            If blnFlag = False And objClaimMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objClaimMaster._Message, Me)
            End If

            If blnFlag = True Then

                If mintClaimRequestMasterId <= 0 AndAlso mblnIsClaimFormBudgetMandatory _
                    AndAlso mblnIsHRExpense = False AndAlso Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then

                    Dim mstrUserName As String = ""
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        mstrUserName = Session("UserName").ToString()
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        mstrUserName = Session("DisplayName").ToString()
                    End If

                    Dim objP2P As New clsCRP2PIntegration
                    Dim mstrP2PRequest As String = objClaimMaster.GenerateNewRequisitionRequestForP2P(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                                                                                                                                                           , CInt(Session("CompanyUnkId")), Session("UserAccessModeSetting").ToString() _
                                                                                                                                                                                                           , objClaimMaster._Employeeunkid, objClaimMaster._Claimrequestno, objClaimMaster._Transactiondate _
                                                                                                                                                                                                           , objClaimMaster._Claim_Remark, mstrUserName, mdtTran, mdtFinalAttchment, objP2P)

                    Dim objMstData As New clsMasterData
                    Dim mstrGetData As String = ""
                    Dim mstrError As String = ""
                    Dim mstrPostedData As String = ""

                    If objMstData.GetSetP2PWebRequest(Session("NewRequisitionRequestP2PServiceURL").ToString().Trim(), True, True, mstrP2PRequest, mstrGetData, mstrError, objP2P, mstrPostedData, mstrToken:=mstrP2PToken) = False Then

                        '/* START TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.

                        If mdtFinalAttchment IsNot Nothing AndAlso mdtFinalAttchment.Rows.Count > 0 Then
                            Dim dr As List(Of DataRow) = mdtFinalAttchment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").ToList()
                            If dr.Count > 0 Then
                                dr.ForEach(Function(x) UpdateDeleteAttachment(x, False, "D"))
                            End If
                        End If

                        objClaimMaster._Isvoid = True
                        If CInt(Session("Employeeunkid")) > 0 Then
                            objClaimMaster._VoidLoginEmployeeID = CInt(Session("Employeeunkid"))
                        ElseIf CInt(Session("UserId")) > 0 Then
                            objClaimMaster._Voiduserunkid = CInt(Session("UserId"))
                        End If
                        objClaimMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                        objClaimMaster._Voidreason = "Voided due to connection fail or error from P2P system."

                        With objClaimMaster
                            ._FormName = mstrModuleName
                            If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                            Else
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                            End If
                            ._ClientIP = Session("IP_ADD").ToString()
                            ._HostName = Session("HOST_NAME").ToString()
                            ._FromWeb = True
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        End With

                        If objClaimMaster.Delete(objClaimMaster._Crmasterunkid, CInt(Session("UserId")), Session("Database_Name").ToString(), CInt(Session("Fin_year")) _
                                                                   , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                   , True, mdtFinalAttchment, True, Nothing, mblnIsClaimFormBudgetMandatory) = False Then
                            DisplayMessage.DisplayMessage(objClaimMaster._Message, Me)
                            Exit Sub
                        End If

                        If mdtFinalAttchment IsNot Nothing AndAlso mdtFinalAttchment.Rows.Count > 0 Then
                            Dim dr As List(Of DataRow) = mdtFinalAttchment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") = "D" AndAlso x.Field(Of Integer)("scanattachtranunkid") > 0).ToList()
                            If dr.Count > 0 Then
                                dr.ForEach(Function(x) UpdateDeleteAttachment(x, True, "A"))
                            End If
                        End If

                        '/* END TO DELETE WHOLE CLAIM FORM WHEN P2P REQUEST IS NOT SUCCEED.
                        DisplayMessage.DisplayMessage(mstrError, Me)
                        Exit Sub

                    Else
                        '/* START TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                        'mstrGetData = "{""requisitionId"": ""NMBHR-011118-000012"",""message"": ""New requisition details saved successfully."",""status"": 200,""timestamp"": ""01-Nov-2018 12:58:59 PM""}"
                        If mstrGetData.Trim.Length > 0 Then
                            Dim dtTable As DataTable = JsonStringToDataTable(mstrGetData)
                            If objClaimMaster.UpdateP2PResponseToClaimMst(dtTable, objClaimMaster._Crmasterunkid, CInt(Session("UserId")), mstrPostedData) = False Then
                                DisplayMessage.DisplayMessage(objClaimMaster._Message, Me)
                            End If
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtTable IsNot Nothing Then dtTable.Clear()
                            dtTable = Nothing
                            'Pinkal (05-Sep-2020) -- End
                        End If
                        '/* END TO UPDATE CLAIM REQUEST P2P RESPONSE IN CLAIM REQUEST MASTER.
                    End If
                    objMstData = Nothing

                End If

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Claim Request saved successfully."), Me.Page, Session("rootpath").ToString & "Claims_And_Expenses/wPg_ClaimAndRequestList.aspx")
            End If

            'Pinkal (22-Oct-2021)-- Start
            'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
            If gobjEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = CInt(Session("CompanyUnkId"))
                    objThread.Start(arr)
                Else
                    Call Send_Notification(CInt(Session("CompanyUnkId")))
                End If
            End If
            'Pinkal (22-Oct-2021)-- End

        Catch ex As Exception
            DisplayMessage.DisplayError("btnSave_Click:-" & ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim iRow As DataRow() = Nothing

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End


            If CInt(Me.ViewState("EditCrtranunkid")) > 0 Then
                If popup_DeleteReason.Reason.Trim.Length > 0 Then 'Nilay (01-Feb-2015) -- txtreasondel.Text.Trim.Length > 0
                    iRow = mdtTran.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                    iRow(0).Item("isvoid") = True
                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text
                    iRow(0).Item("AUD") = "D"
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        iRow(0).Item("voiduserunkid") = Session("UserId")
                        iRow(0).Item("voidloginemployeeunkid") = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        iRow(0).Item("voiduserunkid") = -1
                        iRow(0).Item("voidloginemployeeunkid") = Session("Employeeunkid")
                    End If
                    iRow(0).AcceptChanges()

                    Dim dRow() As DataRow = Nothing
                    If CInt(Me.ViewState("EditCrtranunkid")) > 0 Then
                        dRow = mdtFinalAttchment.Select("transactionunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                    End If
                    If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                        For Each dtRow As DataRow In dRow
                            dtRow.Item("AUD") = "D"
                        Next
                    End If
                    mdtFinalAttchment.AcceptChanges()


                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Me.ViewState("mdtTran") = mdtTran
                    Me.ViewState("mdtTran") = mdtTran.Copy
                    'Pinkal (05-Sep-2020) -- End

                    Fill_Expense()
                    Clear_Controls()
                    btnEdit.Visible = False : btnAdd.Visible = True
                    cboExpense.Enabled = True
                    iRow = Nothing
                Else
                    popup_DeleteReason.Show() 'Nilay (01-Feb-2015) --  popup1.Show()
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Expense Delete process fail.", Me)
                'SHANI (06 JUN 2015) -- Start
                'Enhancement : Changes in C & R module given by Mr.Andrew.
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True
                iRow = Nothing
                'SHANI (06 JUN 2015) -- End 
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnScanAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanAttchment.Click
        Try
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            If dgvData.Items.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please add atleast one expense in order to save."), Me)
                dgvData.Focus()
                Exit Sub
            End If

            If mdtFinalAttchment Is Nothing Then
                Dim objAttchement As New clsScan_Attach_Documents
                mdtAttchment = objAttchement.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.CLAIM_REQUEST, mintClaimRequestMasterId, CStr(Session("Document_Path"))).Copy
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objAttchement = Nothing
                'Pinkal (05-Sep-2020) -- End
            Else
                mdtAttchment = mdtFinalAttchment.Copy
            End If
            Call FillAttachment()
            blnShowAttchmentPopup = True
            popup_ScanAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnScanAttchment_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Is_Valid() = False Then Exit Sub
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSaveAttachment_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtAttchment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtAttchment.AcceptChanges()
                mintDeleteIndex = 0
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonYes_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then
                mintDeleteIndex = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonNo_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnScanClose_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            mdtAttchment.Select("").ToList().ForEach(Function(x) Add_DataRow(x, mstrModuleName))
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnScanSave_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Private Function Add_DataRow(ByVal dr As DataRow, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing


            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            'If CInt(dgvData.Items(mintDeleteIndex).Cells(10).Text) > 0 Then
            '    xRow = mdtFinalAttchment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvData.Items(mintDeleteIndex).Cells(10).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'ElseIf CStr(dgvData.Items(mintDeleteIndex).Cells(12).Text).Trim.Length > 0 Then
            '    xRow = mdtFinalAttchment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvData.Items(mintDeleteIndex).Cells(12).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'End If

            If CInt(dgvData.Items(mintExpenseIndex).Cells(10).Text) > 0 Then
                xRow = mdtFinalAttchment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvData.Items(mintExpenseIndex).Cells(10).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvData.Items(mintExpenseIndex).Cells(12).Text).Trim.Length > 0 Then
                xRow = mdtFinalAttchment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvData.Items(mintExpenseIndex).Cells(12).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If
            'Pinkal (18-Mar-2021) -- End

            If xRow.Length <= 0 Then
                mdtFinalAttchment.ImportRow(dr)
            Else
                mdtFinalAttchment.Rows.Remove(xRow(0))
                mdtFinalAttchment.ImportRow(dr)
            End If


            mdtFinalAttchment.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
        Return True
    End Function

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If txtExpRemark.Text.Trim.Length <= 0 Then
                popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 45, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 26, "Do you wish to continue?")
                popup_ExpRemarkYesNo.Show()
            Else
                If mblnIsEditClick Then
                    EditExpense()
                Else
                    AddExpense()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsEditClick Then
                EditExpense()
            Else
                AddExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtAttchment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnDownloadAll_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Links Events"

    Protected Sub LnkViewDependants_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkViewDependants.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, True, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, True, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), True, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
                'SHANI (06 JUN 2015) -- Start
                'Enhancement : Changes in C & R module given by Mr.Andrew.
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboEmployee.SelectedValue), False, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
                'SHANI (06 JUN 2015) -- End 
            Else
                Exit Sub
            End If
            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()
            popupEmpDepedents.Show()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("LnkViewDependants_Click:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub dgvData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.DeleteCommand
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim iRow As DataRow() = Nothing

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End

            If CInt(e.Item.Cells(10).Text) > 0 Then
                iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    Me.ViewState("EditCrtranunkid") = iRow(0).Item("crtranunkid")
                    popup_DeleteReason.Reason = ""
                    popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popup1.Show()
                End If
            ElseIf CStr(e.Item.Cells(12).Text) <> "" Then
                iRow = mdtTran.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                End If

            End If
            iRow(0).AcceptChanges()

            Dim dRow() As DataRow = Nothing
            If CStr(e.Item.Cells(12).Text) <> "" Then
                dRow = mdtFinalAttchment.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
            End If
            If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                For Each dtRow As DataRow In dRow
                    dtRow.Item("AUD") = "D"
                Next
            End If
            mdtFinalAttchment.AcceptChanges()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdtTran
            Me.ViewState("mdtTran") = mdtTran.Copy()
            'Pinkal (05-Sep-2020) -- End

            Fill_Expense()
            Clear_Controls()
            btnEdit.Visible = False : btnAdd.Visible = True
            cboExpense.Enabled = True
            iRow = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If e.CommandName = "Edit" Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
                If Me.ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
                End If
                'Pinkal (05-Sep-2020) -- End


                Dim iRow As DataRow() = Nothing

                If CInt(e.Item.Cells(10).Text) > 0 Then
                    iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    iRow = mdtTran.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboExpense.SelectedValue = iRow(0).Item("expenseunkid").ToString
                    Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

                    cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid").ToString()
                    Call dtpDate_TextChanged(dtpDate, New EventArgs())

                    txtQty.Text = CDec(iRow(0).Item("quantity")).ToString
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString())).ToString()
                    txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboExpense.Enabled = False

                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()

                    If CInt(e.Item.Cells(10).Text) > 0 Then
                        Me.ViewState("EditCrtranunkid") = CStr(iRow(0).Item("crtranunkid"))
                    ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                        Me.ViewState("EditGuid") = CStr(iRow(0).Item("GUID"))
                    End If

                    mintClaimRequestExpenseTranId = CInt(e.Item.Cells(10).Text)
                    mstrClaimRequestExpenseGUID = e.Item.Cells(12).Text.ToString()

                End If

            ElseIf e.CommandName = "attachment" Then
                mdtAttchment = mdtFinalAttchment.Clone
                Dim xRow() As DataRow = Nothing
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    xRow = mdtFinalAttchment.Select("transactionunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    xRow = mdtFinalAttchment.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If xRow.Count > 0 Then
                    mdtAttchment = xRow.CopyToDataTable
                End If


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'mintDeleteIndex = e.Item.ItemIndex
                mintExpenseIndex = e.Item.ItemIndex
                'Pinkal (18-Mar-2021) -- End

                Call FillAttachment()
                blnShowAttchmentPopup = True
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(7).Text.Trim.Length > 0 Then  'Unit Price
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(8).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.CommandName = "Delete" Then
                    popup_YesNo.Message = Language.getMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                    mintDeleteIndex = e.Item.ItemIndex
                    popup_YesNo.Show()
                    'S.SANDEEP |16-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                ElseIf e.CommandName = "Download" Then
                    Dim xrow() As DataRow = Nothing

                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                        xrow = mdtAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                    Else
                        xrow = mdtAttchment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "'")
                    End If

                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                    'S.SANDEEP |16-MAY-2019| -- END
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvResponse_ItemCommand:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox Event(s)"

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim dsCombo As New DataSet
        Dim objExpMaster As New clsExpense_Master
        Dim objLeaveType As New clsleavetype_master
        Dim mdtLeaveType As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, "ISNULL(cr_expinvisible,0) = 0 AND ISNULL(cmexpense_master.isshowoness,0) = 1")
            Else
                dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboEmployee.SelectedValue) = "", 0, cboEmployee.SelectedValue)), True, 0, "ISNULL(cr_expinvisible,0) = 0")
            End If
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())


            If CBool(Session("SectorRouteAssignToEmp")) Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtSector As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                End With
            End If


            If CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE
                        Language.setLanguage(mstrModuleName)
                        objlblValue.Text = Language.getMessage(mstrModuleName, 12, "Leave Form")

                        If mintClaimRequestMasterId > 0 Then
                            cboLeaveType.Enabled = False
                        Else
                            cboLeaveType.Enabled = True
                            If cboReference.DataSource IsNot Nothing Then cboReference.SelectedIndex = 0
                            cboReference.Enabled = True
                        End If


                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", False)
                        End If

                        If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(Me.ViewState("mintLeaveTypeId")), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = Me.ViewState("mintLeaveTypeId").ToString()
                        End With
                        objLeaveType = Nothing

                    Case enExpenseType.EXP_MEDICAL
                        If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(Me.ViewState("mintLeaveTypeId")), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "Id"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = Me.ViewState("mintLeaveTypeId").ToString
                        End With
                        Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())

                        cboLeaveType.Enabled = False
                        objlblValue.Text = ""
                        cboReference.SelectedIndex = 0
                        cboReference.Enabled = False

                    Case enExpenseType.EXP_TRAINING
                        If CInt(Me.ViewState("mintLeaveTypeId")) > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & CInt(Me.ViewState("mintLeaveTypeId")), "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "Id"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = Me.ViewState("mintLeaveTypeId").ToString()
                        End With
                        Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())

                        cboLeaveType.SelectedIndex = 0
                        cboLeaveType.Enabled = False
                        Language.setLanguage(mstrModuleName)
                        objlblValue.Text = Language.getMessage(mstrModuleName, 13, "Training")
                        cboReference.DataSource = Nothing
                        dsCombo = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(CInt(cboEmployee.SelectedValue), True)
                        With cboReference
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With

                    Case enExpenseType.EXP_MISCELLANEOUS
                        cboLeaveType.SelectedValue = "0"
                        cboReference.SelectedValue = "0"
                        cboLeaveType.Enabled = False
                        cboReference.Enabled = False
                        objlblValue.Text = ""

                End Select

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If

            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing : cboReference.Enabled = False : cboLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            If mdtLeaveType IsNot Nothing Then mdtLeaveType.Clear()
            mdtLeaveType = Nothing
            objExpMaster = Nothing
            objLeaveType = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExpMaster As New clsExpense_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If CBool(Session("SectorRouteAssignToExpense")) Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                    .SelectedIndex = 0
                End With
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing
                'Pinkal (05-Sep-2020) -- End
                objAssignExpense = Nothing
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                Me.ViewState("mblnIsLeaveEncashment") = objExpMaster._IsLeaveEncashment
                cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.SelectedValue = "0"
                cboCostCenter.SelectedValue = "0"

                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If

                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    txtBalance.Text = "0.00"
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (05-Sep-2020) -- End

                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing
                    Dim blnApplyFilter As Boolean = True

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        blnApplyFilter = False
                    End If

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                        dsList = objLeave.GetList("List", _
                                                  Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                 False, True, blnApplyFilter, False, CInt(cboEmployee.SelectedValue), False, False, False, "", Nothing)

                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                        dsList = objLeave.GetList("List", _
                                                  Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                  False, True, blnApplyFilter, False, CInt(cboEmployee.SelectedValue), True, True, False, "", Nothing)

                    End If

                    dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        txtBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then

                            If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = CDate(Session("fin_enddate")).Date
                            If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            Else
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            End If

                        ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then

                            Dim mdtDate As Date = dtpDate.GetDate.Date
                            Dim mdtDays As Integer = 0

                            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                                If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                                    mdtDate = CDate(Session("fin_enddate")).Date
                                    mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                                Else
                                    If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                        mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                                    End If
                                End If

                            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                                If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                End If
                                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                            End If

                            Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                            If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                                intDiff = intDiff - 1
                            End If

                            txtBalanceAsOnDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        End If

                        txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 6, "Quantity")
                    End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objLeave = Nothing
                    'Pinkal (05-Sep-2020) -- End

                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = Math.Round(Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")), 6).ToString()
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    objEmpExpBal = Nothing
                End If
            Else
                txtUoMType.Text = ""
                txtUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                txtBalance.Text = "0.00"
                txtBalanceAsOnDate.Text = "0.00"
                cboCostCenter.Enabled = False
                cboCostCenter.SelectedValue = "0"
                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtQty.Text = "1"
                'Pinkal (04-Feb-2020) -- End
            End If


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
            Else
                txtQty.Text = "1"
            End If
            'Pinkal (04-Feb-2020) -- End

            If objExpMaster._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                lblBalance.Visible = True
                lblBalanceasondate.Visible = True
                txtBalance.Visible = True
                txtBalanceAsOnDate.Visible = True
                cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                cboCurrency.Enabled = False
            Else
                lblBalance.Visible = False
                lblBalanceasondate.Visible = False
                txtBalance.Visible = False
                txtBalanceAsOnDate.Visible = False


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
                Dim mdtTran As DataTable = Nothing
                If Me.ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
                End If
                'Pinkal (05-Sep-2020) -- End
                If mdtTran IsNot Nothing Then
                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = CStr(mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                    End If
                End If
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objleave As New clsleaveform
        Dim dsLeave As New DataSet
        Dim mdtLeaveForm As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If CInt(IIf(cboLeaveType.SelectedValue = "", 0, cboLeaveType.SelectedValue)) = 0 Then
                dsLeave = objleave.GetLeaveFormForExpense(0, "7", CInt(cboEmployee.SelectedValue), True, True, CInt(Me.ViewState("mintLeaveFormID")))
            Else
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "7", CInt(cboEmployee.SelectedValue), True, True, CInt(Me.ViewState("mintLeaveFormID")))
            End If

            If CInt(Session("mintLeaveFormID")) > 0 Then
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & CInt(Session("mintLeaveFormID")), "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = mdtLeaveForm.Copy
                .DataBind()
                If CInt(Me.ViewState("mintLeaveFormID")) < 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = Me.ViewState("mintLeaveFormID").ToString
                End If
            End With
            objleave = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            If mdtLeaveForm IsNot Nothing Then mdtLeaveForm.Clear()
            mdtLeaveForm = Nothing
            objleave = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "DatePicker Event(s)"
    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, cboSectorRoute.SelectedIndexChanged
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim objPeriod As New clscommom_period_Tran
            'Pinkal (05-Sep-2020) -- End

            If CDate(Me.ViewState("MinDate")).Date <= dtpDate.GetDate.Date And CDate(Me.ViewState("MaxDate")).Date >= dtpDate.GetDate.Date Then
                Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
                Dim objCosting As New clsExpenseCosting
                objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.GetDate)
                txtCosting.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString)
                txtCostingTag.Value = iCostingId.ToString()


                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                If iAmount > 0 Then
                    txtUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString)
                Else
                    txtUnitPrice.Text = "1.00"
                End If
                'Pinkal (07-Feb-2020) -- End


                Dim objExpnese As New clsExpense_Master
                objExpnese._Expenseunkid = CInt(cboExpense.SelectedValue)

                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'If objExpnese._Uomunkid = enExpUoM.UOM_QTY Then
                    'txtQty.Text = "0"
                    'Else
                    txtQty.Text = "1"
                    'End If
                    'Pinkal (04-Feb-2020) -- End

                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                    txtQty.Text = "1"
                End If

                If (sender.GetType().FullName = "System.Web.UI.WebControls.TextBox" AndAlso CType(sender, System.Web.UI.WebControls.TextBox).Parent.ID.ToUpper() = dtpDate.ID.ToUpper()) AndAlso CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboExpense.SelectedValue) > 0 Then
                    cboExpense_SelectedIndexChanged(New Object(), New EventArgs())
                End If

                objCosting = Nothing

                If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                    txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                    mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
                End If
                If objExpnese._IsConsiderDependants Then txtQty.Enabled = False Else txtQty.Enabled = True
                txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable

                objExpnese = Nothing

            Else
                dtpDate.SetDate = CDate(Me.ViewState("MinDate"))
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, Date should be between ") & CDate(Me.ViewState("MinDate")).Date & " and " & CDate(Me.ViewState("MaxDate")).Date & ".", Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub
#End Region

#Region "TextBox Event"
    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Try

            If txtQty.Text.Trim.Length > 0 Then
                If IsNumeric(txtQty.Text) = False Then

                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'txtQty.Text = "0"
                    txtQty.Text = "1"
                    'Pinkal (04-Feb-2020) -- End
                    Exit Sub
                End If
                If CInt(txtQty.Text) > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If CBool(Me.ViewState("mblnIsLeaveEncashment")) = False Then
                        txtUnitPrice.Enabled = True
                        If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                            txtUnitPrice.Text = CStr(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString()))
                            txtUnitPrice.Focus()
                        End If
                    Else
                        txtUnitPrice.Enabled = False
                    End If
                ElseIf CInt(txtQty.Text) <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                        txtUnitPrice.Text = CStr(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString()))
                    End If
                End If
            Else
                txtUnitPrice.Enabled = False
            End If

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            objExpense = Nothing
            'Pinkal (18-Mar-2021) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("txtQty_TextChanged:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub txtUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.TextChanged
        Try
            If IsNumeric(txtUnitPrice.Text) = False Then

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("txtUnitPrice_TextChanged:-" & ex.Message, Me)
        End Try

    End Sub
#End Region

    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbExpenseInformation", Me.lblDetialHeader.Text).Replace("&&", "&")
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Language._Object.getCaption(Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.ID, Me.lblSector.Text)
            Me.LnkViewDependants.Text = Language._Object.getCaption(Me.LnkViewDependants.ID, Me.LnkViewDependants.Text)
            Me.tbExpenseRemark.HeaderText = Language._Object.getCaption(Me.tbExpenseRemark.ID, Me.tbExpenseRemark.HeaderText)
            Me.tbClaimRemark.HeaderText = Language._Object.getCaption(Me.tbClaimRemark.ID, Me.tbClaimRemark.HeaderText)

            'Pinkal (23-Oct-2015) -- Start
            'Enhancement - Putting Domicile Address for TRA as per Dennis Requirement
            Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)
            'Pinkal (23-Oct-2015) -- End


            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            Me.lblBalanceasondate.Text = Language._Object.getCaption(Me.lblBalanceasondate.ID, Me.lblBalanceasondate.Text)
            'Pinkal (30-Apr-2018) - End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            'Pinkal (20-Nov-2018) -- End

            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvData.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)
            Me.dgvData.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)
            Me.dgvData.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(8).FooterText, Me.dgvData.Columns(8).HeaderText)

            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            btnScanAttchment.Text = Language._Object.getCaption(Me.btnScanAttchment.ID, Me.btnScanAttchment.Text).Replace("&", "")
            btnAddFile.Value = Language._Object.getCaption(Me.btnScanAttchment.ID, Me.btnScanAttchment.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- End

            Language.setLanguage(mstrModuleName1)
            Me.LblEmpDependentsList.Text = Language._Object.getCaption(mstrModuleName1, Me.LblEmpDependentsList.Text)
            Me.dgDepedent.Columns(0).HeaderText = Language._Object.getCaption(Me.dgDepedent.Columns(0).FooterText, Me.dgDepedent.Columns(0).HeaderText)
            Me.dgDepedent.Columns(1).HeaderText = Language._Object.getCaption(Me.dgDepedent.Columns(1).FooterText, Me.dgDepedent.Columns(1).HeaderText)
            Me.dgDepedent.Columns(2).HeaderText = Language._Object.getCaption(Me.dgDepedent.Columns(2).FooterText, Me.dgDepedent.Columns(2).HeaderText)
            Me.dgDepedent.Columns(3).HeaderText = Language._Object.getCaption(Me.dgDepedent.Columns(3).FooterText, Me.dgDepedent.Columns(3).HeaderText)
            Me.dgDepedent.Columns(4).HeaderText = Language._Object.getCaption(Me.dgDepedent.Columns(4).FooterText, Me.dgDepedent.Columns(4).HeaderText)
            Me.btnEmppnlEmpDepedentsClose.Text = Language._Object.getCaption("btnClose", Me.btnEmppnlEmpDepedentsClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
            Language.setMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Language.setMessage(mstrModuleName, 4, "Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 5, "Expense is mandatory information. Please select Expense to continue.")
            Language.setMessage(mstrModuleName, 6, "Quantity is mandatory information. Please enter Quantity to continue.")
            Language.setMessage(mstrModuleName, 8, "Please add atleast one expense in order to save.")
            Language.setMessage(mstrModuleName, 9, "Claim Request saved successfully.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot add same expense again in the below list.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set.")
            Language.setMessage(mstrModuleName, 12, "Leave Form")
            Language.setMessage(mstrModuleName, 13, "Training")
            Language.setMessage(mstrModuleName, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user.")
            Language.setMessage(mstrModuleName, 15, "Please Map this employee's Leave Approver to system user.")
            Language.setMessage(mstrModuleName, 16, "Please Map this Leave type to this employee's Leave Approver(s).")
            Language.setMessage(mstrModuleName, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user.")
            Language.setMessage(mstrModuleName, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
            Language.setMessage(mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set.")
            Language.setMessage(mstrModuleName, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Language.setMessage(mstrModuleName, 22, "has mandatory document attachment. please attach document.")
            Language.setMessage(mstrModuleName, 23, "Configuration Path does not Exist.")
            Language.setMessage(mstrModuleName, 24, "Sorry, you cannot set quantity greater than balance as on date set.")
            Language.setMessage(mstrModuleName, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
            Language.setMessage(mstrModuleName, 26, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Language.setMessage(mstrModuleName, 28, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount.")
            Language.setMessage(mstrModuleName, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
            Language.setMessage(mstrModuleName, 31, "Sorry,You cannot apply leave expense for this leave type.Reason : Eligibility to apply expense for this leave type does not match with eligibility days set on selected leave type.")
            Language.setMessage(mstrModuleName, 32, "Sorry, Date should be between")
            Language.setMessage(mstrModuleName, 33, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
            Language.setMessage(mstrModuleName, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
            Language.setMessage(mstrModuleName, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
            Language.setMessage(mstrModuleName, 45, "You have not set your expense remark.")
            Language.setMessage(mstrModuleName, 46, "Currency is mandatory information. Please select Currency to continue.")
            Language.setMessage(mstrModuleName, 47, "Claim Remark is mandatory information. Please enter claim remark to continue.")
            Language.setMessage(mstrModuleName, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
            Language.setMessage(mstrModuleName, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
            Language.setMessage(mstrModuleName, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")

            Language.setMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Language.setMessage("clsclaim_request_master", 4, " ] time(s).")
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")
            Language.setMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
