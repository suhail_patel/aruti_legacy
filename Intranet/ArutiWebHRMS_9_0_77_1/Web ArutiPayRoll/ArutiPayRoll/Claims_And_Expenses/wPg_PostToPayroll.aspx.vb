﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region

Partial Class wPg_PostToPayroll
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmClaimPosting"

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimPosting As clsclaim_process_Tran
    'Pinkal (05-Sep-2020) -- End

    Private DisplayMessage As New CommonCodes
    'Pinkal (04-Jul-2020) -- Start
    'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
    'Private mdtData As DataTable = Nothing
    'Pinkal (04-Jul-2020) -- End
#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'SetLanguage()
            'Pinkal (05-Sep-2020) -- End



            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmClaimPosting"
            'StrModuleName1 = "mnuUtilitiesMain"
            'StrModuleName2 = "mnuClaimsExpenses"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'objClaimPosting = New clsclaim_process_Tran
            SetLanguage()
            'Pinkal (05-Sep-2020) -- End

            If IsPostBack = False Then
                Call FillCombo()
            End If

            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            ' mdtData = CType(Me.ViewState("mdtData"), DataTable)
            'Pinkal (04-Jul-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load1 : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'Me.ViewState("mdtData") = mdtData
            'Pinkal (04-Jul-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objemployee As New clsEmployee_Master

            dsList = objemployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            False, "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objemployee = Nothing


            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List")
            dsList = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (04-Jul-2020) -- End
            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With

            Dim objExpMst As New clsExpense_Master
            dsList = objExpMst.getComboList(-1, True, "List")
            With cboExpense
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objExpMst = Nothing

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "List", True, 1)

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objPeriod = Nothing

            Dim objtranhead As New clsTransactionHead
            dsList = objtranhead.getComboList(Session("Database_Name").ToString, "List", True, , , , , , " typeof_id <> " & enTypeOf.Salary)
            With cboTranhead
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objtranhead.getComboList(Session("Database_Name").ToString, "List", False, , , , , , " typeof_id <> " & enTypeOf.Salary)
            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("tranheadunkid") = 0
            drRow("name") = ""
            dsList.Tables(0).Rows.InsertAt(drRow, 0)
            Me.ViewState.Add("TranHeads", dsList.Tables(0))

            objtranhead = Nothing

            With cboViewBy
                .Items.Clear()
                Language.setLanguage(mstrModuleName)
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Show Unposted Claim Transactions"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Show Posted Claim Transactions"))
                .SelectedIndex = 0
            End With

            dsList = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = "0"
                .DataBind()
            End With

            Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As DataSet = Nothing
        Dim mdtFilterData As DataTable = Nothing
        Dim strSearch As String = ""
        'Pinkal (04-Jul-2020) -- Start
        'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
        Dim mdtData As DataTable = Nothing
        'Pinkal (04-Jul-2020) -- End
        Try

            If isblank Then
                strSearch = "AND 1=2 "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                strSearch &= "AND cmexpense_master.expensetypeid = " & CInt(cboExpCategory.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND cmclaim_process_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If txtClaimNo.Text.Trim.Length > 0 Then
                strSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text.Trim & "%' "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                strSearch &= "AND cmclaim_process_tran.expenseunkid = " & CInt(cboExpense.SelectedValue) & " "
            End If

            If CInt(cboUOM.SelectedValue) > 0 Then
                strSearch &= "AND cmexpense_master.uomunkid = " & CInt(cboUOM.SelectedValue) & " "
            End If

            If CInt(cboViewBy.SelectedIndex) = 1 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND cmclaim_process_tran.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If


            If CInt(cboViewBy.SelectedIndex) = 0 Then
                strSearch &= "AND cmclaim_process_tran.isposted = 0 "
            Else
                strSearch &= "AND cmclaim_process_tran.isposted = 1 "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimPosting As New clsclaim_process_Tran
            'Pinkal (05-Sep-2020) -- End
            dsList = objClaimPosting.GetList("List", True, , strSearch)


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimPosting = Nothing
            'Pinkal (05-Sep-2020) -- End


            mdtFilterData = dsList.Tables(0)

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Rows.Count <= 0 Then isblank = True

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Columns.Contains("Isgroup") = False Then
                mdtFilterData.Columns.Add("Isgroup", Type.GetType("System.Boolean"))
                mdtFilterData.Columns("Isgroup").DefaultValue = False
            End If

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Columns.Contains("Ischecked") = False Then
                mdtFilterData.Columns.Add("ischecked", Type.GetType("System.Boolean"))
                mdtFilterData.Columns("ischecked").DefaultValue = False
            End If

            If mdtFilterData IsNot Nothing AndAlso mdtFilterData.Columns.Contains("ischange") = False Then
                mdtFilterData.Columns.Add("ischange", Type.GetType("System.Boolean"))
                mdtFilterData.Columns("ischange").DefaultValue = False
            End If

            If mdtData IsNot Nothing Then
                mdtData.Clear()
            End If

            If mdtFilterData.Rows.Count > 0 AndAlso isblank = False Then
                mdtData = mdtFilterData.Clone

                Dim mintClaimFormMstID As Integer = -1
                Dim drNewRow As DataRow = Nothing
                Dim intRowCount As Integer = -1
                For Each dr As DataRow In mdtFilterData.Rows

                    If mintClaimFormMstID <> CInt(dr("crmasterunkid")) Then
                        mintClaimFormMstID = CInt(dr("crmasterunkid"))
                        drNewRow = mdtData.NewRow
                        drNewRow("ischange") = False
                        drNewRow("Ischecked") = False
                        drNewRow("Isgroup") = True
                        drNewRow("crmasterunkid") = mintClaimFormMstID
                        drNewRow("crprocesstranunkid") = -1
                        drNewRow("crapprovaltranunkid") = -1
                        drNewRow("crtranunkid") = -1
                        drNewRow("employeeunkid") = -1
                        drNewRow("expenseunkid") = -1
                        drNewRow("secrouteunkid") = -1
                        drNewRow("costingunkid") = -1
                        drNewRow("uomunkid") = -1
                        drNewRow("periodunkid") = -1
                        drNewRow("Employee") = ""
                        drNewRow("Claim") = dr("Claim")
                        drNewRow("Expense") = ""
                        drNewRow("Sector") = ""
                        drNewRow("Period") = ""
                        drNewRow("Qtytrnhead") = ""
                        drNewRow("Amttrnhead") = ""
                        drNewRow("currency_sign") = ""
                        drNewRow("unitprice") = DBNull.Value
                        drNewRow("quantity") = DBNull.Value
                        drNewRow("amount") = DBNull.Value
                        drNewRow("qtytranheadunkid") = -1
                        drNewRow("amttranheadunkid") = -1
                        drNewRow("processedrate") = DBNull.Value
                        drNewRow("processedamt") = DBNull.Value
                        drNewRow("expense_remark") = ""
                        drNewRow("approveremployeeunkid") = -1
                        drNewRow("crapproverunkid") = -1

                        mdtData.Rows.Add(drNewRow)
                        intRowCount += 1
                    End If

                    drNewRow = mdtData.NewRow
                    drNewRow("ischange") = False
                    drNewRow("Ischecked") = False
                    drNewRow("Isgroup") = False
                    drNewRow("crmasterunkid") = mintClaimFormMstID
                    drNewRow("crprocesstranunkid") = dr("crprocesstranunkid")
                    drNewRow("crapprovaltranunkid") = dr("crapprovaltranunkid")
                    drNewRow("crtranunkid") = dr("crtranunkid")
                    drNewRow("employeeunkid") = dr("employeeunkid")
                    drNewRow("periodunkid") = dr("periodunkid")
                    drNewRow("expenseunkid") = dr("expenseunkid")
                    drNewRow("secrouteunkid") = dr("secrouteunkid")
                    drNewRow("costingunkid") = dr("costingunkid")
                    drNewRow("uomunkid") = dr("uomunkid")
                    drNewRow("Employee") = dr("Employee")
                    drNewRow("Claim") = ""
                    drNewRow("Expense") = dr("Expense")
                    drNewRow("Sector") = dr("Sector")
                    drNewRow("Period") = dr("Period")
                    drNewRow("Qtytrnhead") = dr("Qtytrnhead")
                    drNewRow("Amttrnhead") = dr("Amttrnhead")
                    drNewRow("unitprice") = dr("unitprice")
                    drNewRow("quantity") = dr("quantity")
                    drNewRow("amount") = dr("amount")
                    drNewRow("qtytranheadunkid") = dr("qtytranheadunkid")
                    drNewRow("amttranheadunkid") = dr("amttranheadunkid")
                    drNewRow("processedrate") = dr("processedrate")
                    drNewRow("processedamt") = dr("processedamt")
                    drNewRow("expense_remark") = dr("expense_remark")
                    drNewRow("approveremployeeunkid") = dr("approveremployeeunkid")
                    drNewRow("crapproverunkid") = dr("crapproverunkid")
                    drNewRow("currency_sign") = dr("currency_sign")

                    mdtData.Rows.Add(drNewRow)
                    intRowCount += 1
                Next

            ElseIf isblank Then

                mdtData = mdtFilterData.Clone
                Dim drNewRow As DataRow = mdtData.NewRow
                drNewRow("ischange") = 0
                drNewRow("Ischecked") = 0
                drNewRow("Isgroup") = 0
                drNewRow("crmasterunkid") = 0
                drNewRow("crprocesstranunkid") = 0
                drNewRow("crapprovaltranunkid") = 0
                drNewRow("crtranunkid") = 0
                drNewRow("employeeunkid") = 0
                drNewRow("expenseunkid") = 0
                drNewRow("secrouteunkid") = 0
                drNewRow("costingunkid") = 0
                drNewRow("uomunkid") = 0
                drNewRow("periodunkid") = 0
                drNewRow("qtytranheadunkid") = 0
                drNewRow("amttranheadunkid") = 0
                drNewRow("unitprice") = 0
                drNewRow("quantity") = 0
                drNewRow("amount") = 0

                mdtData.Rows.Add(drNewRow)
            End If

            dgvClaimPosting.DataSource = mdtData
            dgvClaimPosting.DataBind()

            If isblank Then dgvClaimPosting.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            If mdtData IsNot Nothing Then mdtData.Clear()
            mdtData = Nothing
            If mdtFilterData IsNot Nothing Then mdtFilterData.Clear()
            mdtFilterData = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Function UpdateRow(ByVal drRow As GridViewRow, ByVal mstrTranHeadName As String) As Boolean
        Try
            If drRow IsNot Nothing Then
                If CInt(radOperation.SelectedValue) = 1 Then  'QTY MAPPING
                    drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "colhQtytrnhead", False, True)).Text = mstrTranHeadName
                    drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "colhAmttrnhead", False, True)).Text = ""
                ElseIf CInt(radOperation.SelectedValue) = 2 Then  'AMT MAPPING
                    drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "colhQtytrnhead", False, True)).Text = ""
                    drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "colhAmttrnhead", False, True)).Text = mstrTranHeadName
                End If
                drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "objcolhischange", False, True)).Text = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboViewBy.SelectedIndex) = 1 And CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If
            Call FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboViewBy.SelectedIndex = 0
            cboEmployee.SelectedIndex = 0
            txtClaimNo.Text = ""
            cboExpCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboUOM.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0
            cboTranhead.SelectedIndex = 0
            dgvClaimPosting.DataSource = Nothing
            dgvClaimPosting.DataBind()
            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'If mdtData IsNot Nothing Then mdtData.Rows.Clear()
            'Pinkal (04-Jul-2020) -- End
            radOperation.SelectedValue = "1"
            Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)
            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'Session.Remove("mdtData")
            'Pinkal (04-Jul-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPosting.Click, btnUnposting.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            If CType(sender, Button).ID = btnPosting.ID Then
                If CInt(cboTranhead.SelectedValue) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Transaction head is compulsory information.Please Select Transaction head."), Me)
                    cboTranhead.Focus()
                    Exit Sub
                End If
            End If


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvClaimPosting.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True)


            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please check atleast one claim transaction to do futher operation on it."), Me)
                Exit Sub
            End If


            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                Dim objPrd As New clscommom_period_Tran

                objPrd._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)

                Dim mdtStartDate As Date = objPrd._Start_Date
                Dim mdtEndDate As Date = objPrd._End_Date
                objPrd = Nothing

                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                'Dim lstIDs As List(Of String) = (From p In mdtData Where (CBool(p.Item("Isgroup")) = False And CBool(p.Item("ischecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
                Dim lstIDs As List(Of String) = gRow.AsEnumerable().Select(Function(x) dgvClaimPosting.DataKeys(x.RowIndex).Values("employeeunkid").ToString()).Distinct.ToList()
                'Pinkal (04-Jul-2020) -- End

                Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

                Dim objLeaveTran As New clsTnALeaveTran
                If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objLeaveTran = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objLeaveTran = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If

            If CType(sender, Button).ID = btnPosting.ID Then
                gRow.ToList().ForEach(Function(x) UpdateRow(x, cboTranhead.SelectedItem.Text))
            ElseIf CType(sender, Button).ID = btnUnposting.ID Then
                gRow.ToList().ForEach(Function(x) UpdateRow(x, ""))
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList().Clear()
            gRow = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("btnPost_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Private Sub btnUnposting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUnposting.Click
    '    Try
    '        If CInt(cboPeriod.SelectedValue) <= 0 Then
    '            Language.setLanguage(mstrModuleName)
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
    '            cboPeriod.Focus()
    '            Exit Sub
    '        End If
    '        If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
    '            Dim drRow() As DataRow = mdtData.Select("Isgroup = 0 AND ischecked = 1")
    '            If drRow.Length <= 0 Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please check atleast one claim transaction to do futher operation on it."), Me)
    '                Exit Sub
    '            End If
    '            If CInt(cboPeriod.SelectedValue) > 0 Then
    '                Dim objPrd As New clscommom_period_Tran

    '                'Shani(20-Nov-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objPrd._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
    '                'Shani(20-Nov-2015) -- End

    '                Dim mdtStartDate As Date = objPrd._Start_Date
    '                Dim mdtEndDate As Date = objPrd._End_Date
    '                objPrd = Nothing

    '                Dim lstIDs As List(Of String) = (From p In mdtData Where (CBool(p.Item("Isgroup")) = False And CBool(p.Item("ischecked")) = True) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
    '                Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

    '                Dim objLeaveTran As New clsTnALeaveTran
    '                If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), Me)
    '                    Exit Sub
    '                End If
    '            End If
    '            Dim iRowIdx As Integer = -1
    '            Dim xGrdItem As DataGridItem = Nothing

    '            For i As Integer = 0 To drRow.Length - 1
    '                drRow(i)("periodunkid") = 0
    '                drRow(i)("qtytranheadunkid") = -1
    '                drRow(i)("amttranheadunkid") = -1
    '                drRow(i)("processedrate") = 0
    '                drRow(i)("processedamt") = 0
    '                'drRow(i)("ischecked") = False
    '                drRow(i)("ischange") = True
    '                iRowIdx = mdtData.Rows.IndexOf(drRow(i))
    '                If iRowIdx > 0 Then
    '                    xGrdItem = dgvClaimPosting.Items(iRowIdx)

    '                    'Pinkal (04-Feb-2019) -- Start
    '                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '                    'If TypeOf xGrdItem.Cells(7).Controls(1) Is DropDownList Then
    '                    '    CType(xGrdItem.Cells(7).Controls(1), DropDownList).SelectedValue = Nothing
    '                    'End If
    '                    'If TypeOf xGrdItem.Cells(8).Controls(1) Is DropDownList Then
    '                    '    CType(xGrdItem.Cells(8).Controls(1), DropDownList).SelectedValue = Nothing
    '                    'End If
    '                    If TypeOf xGrdItem.Cells(8).Controls(1) Is DropDownList Then
    '                        CType(xGrdItem.Cells(8).Controls(1), DropDownList).SelectedValue = Nothing
    '                    End If
    '                    If TypeOf xGrdItem.Cells(9).Controls(1) Is DropDownList Then
    '                        CType(xGrdItem.Cells(9).Controls(1), DropDownList).SelectedValue = Nothing
    '                    End If
    '                    'Pinkal (04-Feb-2019) -- End
    '                End If
    '            Next
    '            xGrdItem = Nothing
    '            mdtData.AcceptChanges()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimPosting As New clsclaim_process_Tran
        'Pinkal (05-Sep-2020) -- End
        Try

            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
            '    Dim drRow() As DataRow = mdtData.Select("Isgroup = 0 AND ischange = 1")
            '    If drRow.Length <= 0 Then
            '        Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please post/unpost atleast one claim transaction."), Me)
            '        Exit Sub
            '    End If


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvClaimPosting.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True And x.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "objcolhischange", False, True)).Text = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please post/unpost atleast one claim transaction."), Me)
                Exit Sub
            End If

            Dim mstrCrProcessIds As String = String.Join(",", (From p In gRow Select (dgvClaimPosting.DataKeys(p.RowIndex)("crprocesstranunkid").ToString)).ToArray())
            objClaimPosting._Userunkid = CInt(Session("UserId"))

            'Pinkal (04-Jul-2020) -- End

            With objClaimPosting
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If CInt(cboViewBy.SelectedIndex) = 0 Then

                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                'If objClaimPosting.Posting_Unposting_Claim(True, mdtData, ConfigParameter._Object._CurrentDateAndTime) = False Then
                If objClaimPosting.Posting_Unposting_Claim(True, CInt(cboPeriod.SelectedValue), CInt(IIf(CInt(radOperation.SelectedValue) = 1, CInt(cboTranhead.SelectedValue), 0)), CInt(IIf(CInt(radOperation.SelectedValue) = 2, CInt(cboTranhead.SelectedValue), 0)), mstrCrProcessIds) = False Then
                    'Pinkal (04-Jul-2020) -- End
                    DisplayMessage.DisplayMessage(objClaimPosting._Message, Me)
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Claim Transaction posting done successfully."), Me)
                    FillList(True)
                    btnReset_Click(sender, New EventArgs())
                End If
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then
                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                'If objClaimPosting.Posting_Unposting_Claim(False, mdtData, ConfigParameter._Object._CurrentDateAndTime) = False Then
                If objClaimPosting.Posting_Unposting_Claim(False, 0, CInt(IIf(CInt(radOperation.SelectedValue) = 1, CInt(cboTranhead.SelectedValue), 0)), CInt(IIf(CInt(radOperation.SelectedValue) = 2, CInt(cboTranhead.SelectedValue), 0)), mstrCrProcessIds) = False Then
                    'Pinkal (04-Jul-2020) -- End
                    DisplayMessage.DisplayMessage(objClaimPosting._Message, Me)
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Claim Transaction unposting done successfully."), Me)
                    FillList(True)
                    btnReset_Click(sender, New EventArgs())
                End If
            End If
            'End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList().Clear()
            gRow = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimPosting = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox Event(s) "

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        'Dim cb As CheckBox = CType(sender, CheckBox)
    '        'If dgvClaimPosting.Items.Count <= 0 Then Exit Sub
    '        'Dim j As Integer = 0
    '        'Dim dvData As DataView = mdtData.DefaultView

    '        'For i As Integer = 0 To dvData.ToTable.Rows.Count - 1
    '        '    Dim gvItem As DataGridItem = dgvClaimPosting.Items(j)
    '        '    CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '        '    ' Pinkal (04-Feb-2019) -- Start
    '        '    ' Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '        '    'Dim dRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(10).Text & "' AND Isgroup = '" & CBool(gvItem.Cells(9).Text) & "'")
    '        '    'If dRow.Length > 0 Then
    '        '    'dRow(0).Item("Ischecked") = cb.Checked
    '        '    Dim drRow = mdtData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crmasterunkid") = gvItem.Cells(11).Text AndAlso CBool(x.Field(Of Boolean)("Isgroup")) = CBool(gvItem.Cells(10).Text)).ToList()
    '        '    If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
    '        '        drRow.ToList().ForEach(Function(x) UpdateData(x, cb.Checked))
    '        '    End If
    '        '    'End If
    '        '    'dvData.Table.AcceptChanges()s
    '        '    j += 1
    '        'Next
    '        'dvData.Table.AcceptChanges()
    '        '' Pinkal (04-Feb-2019) -- End

    '        ''Pinkal (04-Jul-2020) -- Start
    '        ''Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
    '        ''Session("mdtData") = dvData.ToTable
    '        'mdtData = dvData.ToTable
    '        ''Pinkal (04-Jul-2020) -- End
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        Dim xItemIdx As Integer = gvItem.ItemIndex

    '        'Pinkal (04-Feb-2019) -- Start
    '        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '        'If CBool(gvItem.Cells(9).Text) = True Then
    '        '    Dim dRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(10).Text & "'")
    '        If CBool(gvItem.Cells(10).Text) = True Then
    '            Dim dRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(11).Text & "'")
    '            'Pinkal (04-Feb-2019) -- End
    '            For index As Integer = 0 To dRow.Length - 1
    '                Dim gItem As DataGridItem = dgvClaimPosting.Items(xItemIdx)
    '                CType(gItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '                dRow(index).Item("Ischecked") = cb.Checked
    '                mdtData.AcceptChanges()
    '                xItemIdx += 1
    '            Next
    '        Else
    '            If gvItem.Cells.Count > 0 Then
    '                mdtData.Rows(xItemIdx).Item("Ischecked") = cb.Checked
    '                mdtData.AcceptChanges()


    '                'Pinkal (04-Feb-2019) -- Start
    '                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '                'Dim dClientRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(10).Text & "' AND isgroup = 0 AND Ischecked = 1")
    '                'Dim dParentRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(10).Text & "' AND isgroup = 0 ")
    '                'Dim drParent() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(10).Text & "' AND isgroup = 1")

    '                Dim dClientRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(11).Text & "' AND isgroup = 0 AND Ischecked = 1")
    '                Dim dParentRow() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(11).Text & "' AND isgroup = 0 ")
    '                Dim drParent() As DataRow = mdtData.Select("crmasterunkid = '" & gvItem.Cells(11).Text & "' AND isgroup = 1")

    '                'Pinkal (04-Feb-2019) -- End

    '                If dClientRow.Length = dParentRow.Length Then
    '                    If drParent.Length > 0 Then
    '                        xItemIdx = mdtData.Rows.IndexOf(drParent(0))
    '                        Dim gItem As DataGridItem = dgvClaimPosting.Items(xItemIdx)
    '                        CType(gItem.FindControl("chkbox1"), CheckBox).Checked = True
    '                        drParent(0)("Ischecked") = True
    '                        drParent(0).AcceptChanges()
    '                    End If
    '                Else
    '                    If drParent.Length > 0 Then
    '                        xItemIdx = mdtData.Rows.IndexOf(drParent(0))
    '                        Dim gItem As DataGridItem = dgvClaimPosting.Items(xItemIdx)
    '                        CType(gItem.FindControl("chkbox1"), CheckBox).Checked = False
    '                        drParent(0)("Ischecked") = False
    '                        drParent(0).AcceptChanges()
    '                    End If
    '                End If
    '            End If
    '        End If

    '        Dim drRow() As DataRow = mdtData.Select("Ischecked = True")
    '        If drRow.Length = mdtData.Rows.Count Then
    '            CType(dgvClaimPosting.Controls(0).Controls(0).FindControl("chkHeder1"), CheckBox).Checked = True
    '        Else
    '            CType(dgvClaimPosting.Controls(0).Controls(0).FindControl("chkHeder1"), CheckBox).Checked = False
    '        End If


    '        'Pinkal (04-Jul-2020) -- Start
    '        'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
    '        'Session("mdtData") = mdtData
    '        'Pinkal (04-Jul-2020) -- End
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            cboTranhead.SelectedValue = "0"
            cboPeriod.SelectedValue = "0"
            If CInt(cboViewBy.SelectedIndex) = 0 Then
                cboTranhead.Enabled = True
                cboPeriod.Enabled = True
                radOperation.Enabled = True
                btnPosting.Visible = True
                btnUnposting.Visible = False
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then
                cboTranhead.Enabled = False
                radOperation.Enabled = False
                btnPosting.Visible = False
                btnUnposting.Visible = True
            End If

            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            'dgvClaimPosting.DataSource = Nothing
            FillList(True)
            'Pinkal (04-Jul-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("cboViewBy_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Protected Sub dgvClaimPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvClaimPosting.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(dgvClaimPosting.DataKeys(e.Row.RowIndex).Values("Isgroup")) = True Then
                    For i = 2 To dgvClaimPosting.Columns.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                    Dim chk As CheckBox = e.Row.Cells(0).FindControl("chkselect")
                    chk.Visible = False
                    e.Row.Cells(1).Font.Bold = True

                    e.Row.Cells(1).ColumnSpan = dgvClaimPosting.Columns.Count - 1
                    e.Row.Cells(0).CssClass = "GroupHeaderStyle"
                    e.Row.Cells(1).CssClass = "GroupHeaderStyle"

                ElseIf CBool(dgvClaimPosting.DataKeys(e.Row.RowIndex).Values("Isgroup")) = False Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "dgcolhUnitprice", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "dgcolhUnitprice", False, True)).Text), Session("fmtCurrency").ToString())   'UNIT PRICE
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "dgcolhQuantity", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "dgcolhQuantity", False, True)).Text), Session("fmtCurrency").ToString())                          'QTY
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "dgcolhAmount", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "dgcolhAmount", False, True)).Text), Session("fmtCurrency").ToString())   'AMOUNT
                End If
                e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvClaimPosting, "objcolhischange", False, True)).Text = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvClaimPosting_ItemDataBound : " & ex.Message, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.ID, Me.LblExpense.Text)
            Me.LblTranHead.Text = Language._Object.getCaption(Me.LblTranHead.ID, Me.LblTranHead.Text)
            Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.ID, Me.lblViewBy.Text)
            Me.LblUOM.Text = Language._Object.getCaption(Me.LblUOM.ID, Me.LblUOM.Text)
            Me.radOperation.Items(0).Text = Language._Object.getCaption("rdQtyTrnheadMapping", Me.radOperation.Items(0).Text)
            Me.radOperation.Items(1).Text = Language._Object.getCaption("rdAmtTranHeadMapping", Me.radOperation.Items(1).Text)
            Me.btnUnposting.Text = Language._Object.getCaption(Me.btnUnposting.ID, Me.btnUnposting.Text).Replace("&", "")
            Me.btnPosting.Text = Language._Object.getCaption(Me.btnPosting.ID, Me.btnPosting.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvClaimPosting.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(1).FooterText, Me.dgvClaimPosting.Columns(1).HeaderText)
            Me.dgvClaimPosting.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(2).FooterText, Me.dgvClaimPosting.Columns(2).HeaderText)
            Me.dgvClaimPosting.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(3).FooterText, Me.dgvClaimPosting.Columns(3).HeaderText)
            Me.dgvClaimPosting.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(4).FooterText, Me.dgvClaimPosting.Columns(4).HeaderText)
            Me.dgvClaimPosting.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(5).FooterText, Me.dgvClaimPosting.Columns(5).HeaderText)
            Me.dgvClaimPosting.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(6).FooterText, Me.dgvClaimPosting.Columns(6).HeaderText)
            Me.dgvClaimPosting.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(7).FooterText, Me.dgvClaimPosting.Columns(7).HeaderText)
            Me.dgvClaimPosting.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvClaimPosting.Columns(8).FooterText, Me.dgvClaimPosting.Columns(8).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

End Class
