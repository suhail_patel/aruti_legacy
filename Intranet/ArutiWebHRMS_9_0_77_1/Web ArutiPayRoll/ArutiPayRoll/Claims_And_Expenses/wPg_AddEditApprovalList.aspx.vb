﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Claims_And_Expenses_wPg_AddEditApprovalList
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeaveApprover As New clsleaveapprover_master
    'Private objExAssessorTran As New clsExpenseApprover_Tran
    'Dim objExAssessorMaster As New clsExpenseApprover_Master
    'Pinkal (05-Sep-2020) -- End
    Private Shared ReadOnly mstrModuleName As String = "frmExpApproverAddEdit"
    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.
    Dim dAEmp As DataTable = Nothing
    Dim mdtEmployee As DataTable = Nothing
    'Pinkal (11-Sep-2019) -- End

    'Pinkal (24-Dec-2019) -- Start
    'Enhancement - Claim Retirement Enhancements for NMB.
    Private mintExpenseCategoryId As Integer = 0
    'Pinkal (24-Dec-2019) -- End


    'Pinkal (27-Dec-2019) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    Dim strAdvanceSearch As String = ""
    'Pinkal (27-Dec-2019) -- End


#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()

            If IsPostBack = False Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                'Pinkal (24-Dec-2019) -- Start
                'Enhancement - Claim Retirement Enhancements for NMB.
                If Request.QueryString.Count > 0 Then
                    If Request.QueryString("ID") IsNot Nothing Then
                        mintExpenseCategoryId = CInt(Server.UrlDecode(Basepage.b64decode(Request.QueryString("ID"))))
                    End If
                End If
                'Pinkal (24-Dec-2019) -- End

                FillCombo()
                Call cboExCategory_SelectedIndexChanged(cboExCategory, New EventArgs)

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objExAssessorMaster As New clsExpenseApprover_Master
                Dim objExAssessorTran As New clsExpenseApprover_Tran
                'Pinkal (05-Sep-2020) -- End

                If Session("CrApproverUnkId") IsNot Nothing Then
                    Me.ViewState.Add("CrApproverUnkId", CInt(Session("CrApproverUnkId")))
                    objExAssessorMaster._crApproverunkid = CInt(Me.ViewState("CrApproverUnkId"))
                    cboEmployee.Enabled = False
                End If
                dAEmp = objExAssessorTran._DataTable

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'GetValue()
                GetValue(objExAssessorMaster)
                objExAssessorTran = Nothing
                objExAssessorMaster = Nothing
                'Pinkal (05-Sep-2020) -- End
               
                If Session("CrApproverUnkId") Is Nothing Then
                    If Session("Claim_ExpenseUnkID") IsNot Nothing Then
                        cboExCategory.SelectedValue = Session("Claim_ExpenseUnkID").ToString()
                    End If
                End If

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
            Else

                'Pinkal (24-Dec-2019) -- Start
                'Enhancement - Claim Retirement Enhancements for NMB.
                mintExpenseCategoryId = CInt(Me.ViewState("ExpenseCategoryId"))
                'Pinkal (24-Dec-2019) -- End


                'Pinkal (27-Dec-2019) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                strAdvanceSearch = Me.ViewState("EmpAdvanceSearch").ToString()
                'Pinkal (27-Dec-2019) -- End


                dAEmp = CType(Me.ViewState("mdtTran"), DataTable)
                mdtEmployee = CType(Me.ViewState("mdtEmployee"), DataTable)
                'Pinkal (11-Sep-2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'Pinkal (06-Jan-2016) -- Start
        'Enhancement - Working on Changes in SS for Leave Module.
        'Session.Remove("CrApproverUnkId")
        'Pinkal (06-Jan-2016) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub


    'Pinkal (11-Sep-2019) -- Start
    'Enhancement NMB - Working On Claim Retirement for NMB.

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            'Pinkal (24-Dec-2019) -- Start
            'Enhancement - Claim Retirement Enhancements for NMB.
            Me.ViewState("ExpenseCategoryId") = mintExpenseCategoryId
            'Pinkal (24-Dec-2019) -- End

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Me.ViewState("EmpAdvanceSearch") = strAdvanceSearch
            'Pinkal (27-Dec-2019) -- End

            Me.ViewState("mdtTran") = dAEmp
            Me.ViewState("mdtEmployee") = mdtEmployee
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (11-Sep-2019) -- End


#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("CrApproverUnkId") = Nothing
            Session("Claim_ExpenseUnkID") = Nothing

            'Pinkal (04-Jul-2020) -- Start
            'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            If mintExpenseCategoryId > 0 Then
                Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_Approval_List.aspx?ID=" & Server.UrlEncode(Basepage.b64encode(mintExpenseCategoryId.ToString())), False)
            Else
                Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_Approval_List.aspx", False)
            End If
            'Pinkal (04-Jul-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnNew_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            strAdvanceSearch = popupAdvanceFilter._GetFilterString
            ' strAdvanceSearch = popupAdvanceFilter._GetFilterString.Replace("ADF.", "")
            'Me.ViewState.Add("EmpAdvanceSearch", strAdvanceSearch)
            'Pinkal (27-Dec-2019) -- End
            Fill_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                Dim dRow As DataRow() = mdtEmployee.Select("employeeunkid = " & CInt(dgvAEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                    Add_DataRow(dRow(0))
                End If
            Next

            'Dim drRow() As DataRow = mdtEmployee.Select("ischeck = true")
            'If drRow.Length <= 0 Then
            '    DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
            '    Exit Sub
            'End If
            'For i As Integer = 0 To drRow.Length - 1
            '    Add_DataRow(drRow(i))
            'Next

            'Pinkal (27-Dec-2019) -- End

            Fill_Assigned_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAdd_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnDeleteA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            popYesNo.Title = "Aruti"
            Language.setLanguage(mstrModuleName)
            popYesNo.Message = Language.getMessage(mstrModuleName, 9, "Are you sure you want to delete Selected Employee?")
            popYesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAdd_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popYesNo.buttonYes_Click
        Try
            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvAssessor.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkAsgSelect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassigned."), Me)
                Exit Sub
            End If

            Dim drTemp As DataRow() = Nothing
            Dim objExpenseForm As New clsclaim_request_master
            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                If objExpenseForm.GetApproverPendingExpenseFormCount(CInt(dgvAssessor.DataKeys(gRow(xCount).DataItemIndex)("crapproverunkid")), CInt(dgvAssessor.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")).ToString()) <= 0 Then
                    drTemp = dAEmp.Select("employeeunkid = " & CInt(dgvAssessor.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) & " AND AUD <> 'D'")
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee."), Me)
                    'Exit For
                    Continue For
                End If
                If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    drTemp(0).Item("isvoid") = True
                    drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime.Date
                    drTemp(0).Item("voidreason") = ""
                    drTemp(0).Item("voiduserunkid") = Session("UserId")
                    dAEmp.AcceptChanges()
                End If
            Next

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpenseForm = Nothing
            'Pinkal (05-Sep-2020) -- End



            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'Dim dtAssessor As DataTable = CType(Session("AsgEmpList"), DataTable)
            'Dim dtAssessor As DataTable = dAEmp.Copy()
            ''Pinkal (11-Sep-2019) -- End


            'If dtAssessor Is Nothing Then Exit Sub
            'Dim drTemp As DataRow() = Nothing
            'Dim objUser As New User
            'Dim objExpenseForm As New clsclaim_request_master
            'Dim dtmp() As DataRow = dtAssessor.Select("ischeck=True AND AUD <> 'D' ")
            'If dtmp.Length <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    'Sohail (23 Mar 2019) -- Start
            '    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '    'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassigned."), Me)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassigned."), Me)
            '    'Sohail (23 Mar 2019) -- End
            '    Exit Sub
            'End If
            'For i As Integer = 0 To dtmp.Length - 1

            '    If objExpenseForm.GetApproverPendingExpenseFormCount(CInt(dtmp(i)("crapproverunkid")), CInt(dtmp(i)("employeeunkid")).ToString()) <= 0 Then
            '        drTemp = dtAssessor.Select("employeeunkid = " & CInt(dtmp(i)("employeeunkid")) & " AND AUD <> 'D'")
            '    Else
            '        Language.setLanguage(mstrModuleName)
            '        'Sohail (23 Mar 2019) -- Start
            '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '        'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee."), Me)
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee."), Me)
            '        'Sohail (23 Mar 2019) -- End
            '        Exit For
            '    End If

            '    If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
            '        drTemp(0).Item("AUD") = "D"
            '        drTemp(0).Item("isvoid") = True
            '        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime.Date
            '        drTemp(0).Item("voidreason") = ""
            '        drTemp(0).Item("voiduserunkid") = Session("UserId")
            '        dtAssessor.AcceptChanges()
            '    End If
            'Next

            ''Pinkal (11-Sep-2019) -- Start
            ''Enhancement NMB - Working On Claim Retirement for NMB.
            ''Session("AsgEmpList") = dtAssessor
            'dAEmp = dtAssessor
            'Pinkal (11-Sep-2019) -- End

            'Pinkal (27-Dec-2019) -- End


            popYesNo.Hide()

            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'txtAsgSearch.Text = ""
            'Pinkal (28-Apr-2020) -- End


            Fill_Assigned_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnYes_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExAssessorMaster As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End

        Try
            Dim mdtMapTran As DataTable = Nothing
            If Is_Valid_Data() = False Then Exit Sub

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'SetValue()
            SetValue(objExAssessorMaster)
            'Pinkal (05-Sep-2020) -- End

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmExpApproverAddEdit"
            'StrModuleName2 = "mnuUtilitiesMain"
            'StrModuleName3 = "mnuClaimsExpenses"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (22-Jun-2015) -- End

            If Me.ViewState("CrApproverUnkId") IsNot Nothing Then

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                'If objExAssessorMaster.Update(CType(Session("AsgEmpList"), DataTable), mdtMapTran) = True Then
                If objExAssessorMaster.Update(dAEmp, mdtMapTran) = True Then
                    'Pinkal (11-Sep-2019) -- End
                    Me.ViewState("CrApproverUnkId") = Nothing
                    DisplayMessage.DisplayMessage("Expense Approver defined successfully, with all access and usermapping.", Me)
                    Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_Approval_List.aspx", False)
                End If
            Else

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                'If objExAssessorMaster.Insert(CType(Session("AsgEmpList"), DataTable), mdtMapTran) = True Then
                If objExAssessorMaster.Insert(dAEmp, mdtMapTran) = True Then
                    'Pinkal (11-Sep-2019) -- End
                    DisplayMessage.DisplayMessage("Expense Approver defined successfully, with all access and usermapping.", Me)
                    Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_Approval_List.aspx", False)
                Else
                    DisplayMessage.DisplayMessage(objExAssessorMaster._Message, Me)
                    Exit Sub
                End If
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As DataSet = Nothing
        Dim objUsr As New clsUserAddEdit
        Dim objEmp As New clsEmployee_Master
        Try

            dsCombo = objUsr.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(975), CInt(Session("Fin_year")))

            cboUser.DataTextField = "Name"
            cboUser.DataValueField = "userunkid"
            cboUser.DataSource = dsCombo.Tables("User")
            cboUser.DataBind()


            Me.ViewState("UserList") = dsCombo.Tables("User")

            Dim dtTable As DataTable = Nothing


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (11-Sep-2019) -- End

            cboExCategory.DataTextField = "Name"
            cboExCategory.DataValueField = "Id"
            cboExCategory.DataSource = dtTable
            cboExCategory.DataBind()
            cboExCategory.SelectedValue = "0"

            'Pinkal (24-Dec-2019) -- Start
            'Enhancement - Claim Retirement Enhancements for NMB.
            If Request.QueryString.ToString().Length > 0 Then
                cboExCategory.SelectedValue = mintExpenseCategoryId.ToString()
                cboExCategory.Enabled = False
            End If
            'Pinkal (24-Dec-2019) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End



            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            'Dim blnInActiveEmp As Boolean = False
            'If Session("CrApproverUnkId") Is Nothing OrElse CInt(Session("CrApproverUnkId")) <= 0 Then
            '    blnInActiveEmp = False
            'Else
            '    blnInActiveEmp = True
            'End If



            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.

            'dsCombo = objEmp.GetList(Session("Database_Name").ToString(), _
            '                        CInt(Session("UserId")), _
            '                        CInt(Session("Fin_year")), _
            '                        CInt(Session("CompanyUnkId")), _
            '                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                        Session("UserAccessModeSetting").ToString(), _
            '                  True, blnInActiveEmp, "List", CBool(Session("ShowFirstAppointmentDate")), , , , , False)


            'Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & "," & clsEmployee_Master.EmpColEnum.Col_Section


            'dsCombo = objEmp.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
            '                        CInt(Session("UserId")), _
            '                        CInt(Session("Fin_year")), _
            '                        CInt(Session("CompanyUnkId")), _
            '                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                        Session("UserAccessModeSetting").ToString(), _
            '                        True, blnInActiveEmp, "List", -1, False, "", CBool(Session("ShowFirstAppointmentDate")), False, False, True, Nothing, True)

            'Pinkal (11-Sep-2019) -- End

            'mdtEmployee = dsCombo.Tables("List")


            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            'Me.ViewState.Add("mdtEmployee", mdtEmployee)
            'Dim dc As New DataColumn("ischeck", System.Type.GetType("System.Boolean"))
            'dc.DefaultValue = False
            'mdtEmployee.Columns.Add(dc)

            'If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 42, "Code")) Then
            '    mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 42, "Code")).ColumnName = "employeecode"
            'End If

            'If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) Then
            '    mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ColumnName = "name"
            'End If

            'If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 120, "Department")) Then
            '    mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 120, "Department")).ColumnName = "DeptName"
            'End If

            'If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 118, "Job")) Then
            '    mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 118, "Job")).ColumnName = "job_name"
            'End If

            'Pinkal (11-Sep-2019) -- End

            ' chkExternalApprover_CheckedChanged(Nothing, New EventArgs())

            If chkExternalApprover.Checked = False Then
                Dim blnApplyFilter As Boolean = True
                Dim blnSelect As Boolean = True
                Dim intEmpId As Integer = 0

                dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   Session("UserAccessModeSetting").ToString(), True, _
                                                   CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

                cboEmployee.DataSource = dsCombo
                cboEmployee.DataTextField = "EmpCodeName"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()

            Else
                Dim objUser As New clsUserAddEdit

                Dim dsList As DataSet = objUser.GetExternalApproverList("List", _
                                                         CInt(Session("CompanyUnkId")), _
                                                         CInt(Session("Fin_year")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), "975")
                objUser = Nothing


                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("name") = "Select"
                drRow("employeeunkid") = 0
                dsList.Tables(0).Rows.InsertAt(drRow, 0)


                cboEmployee.DataSource = dsList.Tables(0)
                cboEmployee.DataTextField = "Name"
                cboEmployee.DataValueField = "userunkid"
                cboEmployee.DataBind()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If

            'Pinkal (27-Dec-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objUsr = Nothing
            objEmp = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (27-Dec-2019) -- Start
    'Enhancement - Changes related To OT NMB Testing.

    'Private Sub Fill_Employee()
    '    'Dim mdtEmployee As DataTable
    '    Dim objEmployee As New clsEmployee_Master
    '    Dim dEmp As DataTable = Nothing
    '    Dim strSearch As String = String.Empty
    '    Try


    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        'If Me.ViewState("mdtEmployee") IsNot Nothing Then
    '        '    mdtEmployee = CType(Me.ViewState("mdtEmployee"), DataTable)
    '        'Else
    '        '    Exit Sub
    '        'End If
    '        If mdtEmployee Is Nothing Then Exit Sub
    '        'Pinkal (11-Sep-2019) -- End


    '        Dim mstrEmployeeIDs As String = objExAssessorMaster.GetClaimApproverEmployeeId(CInt(cboExCategory.SelectedValue), CInt(cboEmployee.SelectedValue), chkExternalApprover.Checked)
    '        If ViewState("EmpAdvanceSearch") IsNot Nothing Then
    '            strSearch &= "AND " & ViewState("EmpAdvanceSearch").ToString()
    '        End If


    '        If chkExternalApprover.Checked = False Then
    '            If CInt(cboEmployee.SelectedValue) > 0 Then
    '                strSearch &= "AND employeeunkid <> " & CInt(cboEmployee.SelectedValue) & " "
    '            End If
    '        End If

    '        If mstrEmployeeIDs.Trim.Length > 0 Then
    '            strSearch &= "AND employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
    '        End If

    '        If txtSearchEmp.Text.Trim.Length > 0 Then
    '            strSearch &= "AND (employeecode like '%" & txtSearchEmp.Text.Trim & "%'  OR name like '%" & txtSearchEmp.Text.Trim & "%') "
    '        End If


    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.


    '        'If strSearch.Length > 0 Then
    '        '    strSearch = strSearch.Substring(3)
    '        '    dEmp = New DataView(mdtEmployee, strSearch, "", DataViewRowState.CurrentRows).ToTable
    '        'Else
    '        '    dEmp = New DataView(mdtEmployee, "", "", DataViewRowState.CurrentRows).ToTable
    '        'End If


    '        'Session.Add("EmpList", dEmp)
    '        'dgvAEmployee.DataSource = dEmp
    '        'dgvAEmployee.DataBind()

    '        If strSearch.Length > 0 Then
    '            strSearch = strSearch.Substring(3)
    '            mdtEmployee = New DataView(mdtEmployee, strSearch, "", DataViewRowState.CurrentRows).ToTable
    '        Else
    '            mdtEmployee = New DataView(mdtEmployee, "", "", DataViewRowState.CurrentRows).ToTable
    '        End If

    '        dgvAEmployee.DataSource = mdtEmployee
    '        dgvAEmployee.DataBind()

    '        'Pinkal (11-Sep-2019) -- End

    '        If CInt(cboEmployee.SelectedValue) > 0 Then

    '            'Pinkal (11-Sep-2019) -- Start
    '            'Enhancement NMB - Working On Claim Retirement for NMB.
    '            'Dim mdtAssessor As DataTable = CType(Session("AsgEmpList"), DataTable)
    '            Dim mdtAssessor As DataTable = dAEmp.Copy()
    '            'Pinkal (11-Sep-2019) -- End
    '            objExAssessorTran._EmployeeAsonDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
    '            objExAssessorTran._ExpApproverMasterId = CInt(Me.ViewState("CrApproverUnkId"))
    '            mdtAssessor = objExAssessorTran._DataTable
    '            Fill_Assigned_Employee()
    '        End If
    '    Catch ex As Exception
    '        If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
    '            dgvAEmployee.PageIndex = 0
    '            dgvAEmployee.DataBind()
    '        Else
    '            Throw ex
    '            DisplayMessage.DisplayError("Fill_Employee :- " & ex.Message, Me)
    '        End If
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("Fill_Employee :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError("Fill_Employee :- " & ex.Message, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '    End Try

    'End Sub


    Private Sub Fill_Employee()
        Dim objEmp As New clsEmployee_Master
        Dim dEmp As DataTable = Nothing
        Dim strSearch As String = String.Empty
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExAssessorMaster As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try

            Dim blnInActiveEmp As Boolean = False

            Dim mstrEmployeeIDs As String = objExAssessorMaster.GetClaimApproverEmployeeId(CInt(cboExCategory.SelectedValue), CInt(cboEmployee.SelectedValue), chkExternalApprover.Checked)

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'If ViewState("EmpAdvanceSearch") IsNot Nothing Then
            '    strSearch &= "AND " & ViewState("EmpAdvanceSearch").ToString()
            'End If

            If strAdvanceSearch.Trim.Length > 0 Then
                strSearch &= "AND " & strAdvanceSearch.Trim()
            End If
            'Pinkal (27-Dec-2019) -- End




            If chkExternalApprover.Checked = False Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearch &= "AND hremployee_master.employeeunkid <> " & CInt(cboEmployee.SelectedValue) & " "
                End If
            End If


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	
            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    strSearch &= "AND hremployee_master.employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            'End If

            'If txtSearchEmp.Text.Trim.Length > 0 Then
            '    strSearch &= "AND (employeecode like '%" & txtSearchEmp.Text.Trim & "%'  OR name like '%" & txtSearchEmp.Text.Trim & "%') "
            'End If

            'Pinkal (28-Apr-2020) -- End


            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center


            Dim dsList As DataSet = objEmp.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                              Session("UserAccessModeSetting").ToString(), _
                                              True, blnInActiveEmp, "List", -1, False, strSearch, CBool(Session("ShowFirstAppointmentDate")), False, False, True, Nothing, True)




            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    mdtEmployee = New DataView(dsList.Tables("List"), "employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )", "", DataViewRowState.CurrentRows).ToTable()
            'Else
            '    mdtEmployee = dsList.Tables("List")
            'End If
            If mstrEmployeeIDs.Trim.Length > 0 Then
                mdtEmployee = New DataView(dsList.Tables("List"), "employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )", "", DataViewRowState.CurrentRows).ToTable().Copy
            Else
                mdtEmployee = dsList.Tables("List").Copy
            End If
            'Pinkal (05-Sep-2020) -- End


            Dim dc As New DataColumn("ischeck", System.Type.GetType("System.Boolean"))
            dc.DefaultValue = False
            mdtEmployee.Columns.Add(dc)

            If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 42, "Code")) Then
                mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 42, "Code")).ColumnName = "employeecode"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 46, "Employee Name")) Then
                mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 46, "Employee Name")).ColumnName = "name"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 120, "Department")) Then
                mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 120, "Department")).ColumnName = "DeptName"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage("clsEmployee_Master", 118, "Job")) Then
                mdtEmployee.Columns(Language.getMessage("clsEmployee_Master", 118, "Job")).ColumnName = "job_name"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 196, "Station")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 196, "Station")).ColumnName = "Station"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 197, "Dept. Group")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 197, "Dept. Group")).ColumnName = "DeptGroup"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 210, "Section Group")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 210, "Section Group")).ColumnName = "SectionGroup"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 198, "Section")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 198, "Section")).ColumnName = "Section"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 209, "Unit Group")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 209, "Unit Group")).ColumnName = "UnitGroup"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 199, "Unit")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 199, "Unit")).ColumnName = "Unit"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 200, "Job Group")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 200, "Job Group")).ColumnName = "JobGroup"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 208, "Team")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 208, "Team")).ColumnName = "Team"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 201, "Grade Group")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 201, "Grade Group")).ColumnName = "GradeGroup"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 202, "Grade")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 202, "Grade")).ColumnName = "Grade"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 203, "Grade Level")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 203, "Grade Level")).ColumnName = "GradeLevel"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 204, "Class Group")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 204, "Class Group")).ColumnName = "ClassGroup"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 205, "Class")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 205, "Class")).ColumnName = "Class"
            End If

            If mdtEmployee.Columns.Contains(Language.getMessage(mstrModuleName, 206, "Cost Center")) Then
                mdtEmployee.Columns(Language.getMessage(mstrModuleName, 206, "Cost Center")).ColumnName = "CostCenter"
            End If

            dgvAEmployee.DataSource = mdtEmployee
            dgvAEmployee.DataBind()


            If CInt(cboEmployee.SelectedValue) > 0 Then
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim mdtAssessor As DataTable = dAEmp.Copy()
                Dim objExAssessorTran As New clsExpenseApprover_Tran
                objExAssessorTran._EmployeeAsonDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
                objExAssessorTran._ExpApproverMasterId = CInt(Me.ViewState("CrApproverUnkId"))
                'mdtAssessor = objExAssessorTran._DataTable
                dAEmp = objExAssessorTran._DataTable
                Fill_Assigned_Employee()
                objExAssessorTran = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If

            'chkExternalApprover_CheckedChanged(Nothing, New EventArgs())

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgvAEmployee.PageIndex = 0
                dgvAEmployee.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError("Fill_Employee :- " & ex.Message, Me)
            End If
            DisplayMessage.DisplayError("Fill_Employee :- " & ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmp = Nothing
            objExAssessorMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try

    End Sub

    'Pinkal (27-Dec-2019) -- End


    Private Sub GetEmployeePageRecordNo()
        Try
            If dgvAEmployee.PageIndex > 0 Then
                Me.ViewState("FirstRecordNo") = (((dgvAEmployee.PageIndex + 1) * dgvAEmployee.Rows.Count) - dgvAEmployee.Rows.Count)
            Else
                If dgvAEmployee.Rows.Count < dgvAEmployee.PageSize Then
                    Me.ViewState("FirstRecordNo") = ((1 * dgvAEmployee.Rows.Count) - dgvAEmployee.Rows.Count)
                Else
                    Me.ViewState("FirstRecordNo") = ((1 * dgvAEmployee.PageSize) - dgvAEmployee.Rows.Count)
                End If

            End If
            Me.ViewState("LastRecordNo") = ((dgvAEmployee.PageIndex + 1) * dgvAEmployee.Rows.Count)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetEmployeePageRecordNo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetEmployeePageRecordNo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'Dim dtAssessor As DataTable = CType(Session("AsgEmpList"), DataTable)

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'Dim dtAssessor As DataTable = dAEmp.Copy()
            'Pinkal (27-Dec-2019) -- End


            'Pinkal (11-Sep-2019) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim objUser As New User
            'Pinkal (05-Sep-2020) -- End

            Dim mdtRow As DataRow = Nothing
            Dim dtmp() As DataRow = Nothing
            dtmp = dAEmp.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtmp.Length <= 0 Then
                mdtRow = dAEmp.NewRow
                mdtRow.Item("crapprovertranunkid") = -1
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtRow.Item("crapproverunkid") = objExAssessorTran._ExpApproverMasterId
                If Me.ViewState("CrApproverUnkId") IsNot Nothing Then
                    mdtRow.Item("crapproverunkid") = CInt(Me.ViewState("CrApproverUnkId"))
                Else
                    mdtRow.Item("crapproverunkid") = 0
                End If
                'Pinkal (05-Sep-2020) -- End

                mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                mdtRow.Item("userunkid") = Session("UserId")
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtRow.Item("ecode") = dRow.Item("employeecode")
                mdtRow.Item("ename") = dRow.Item("name")
                mdtRow.Item("edept") = dRow.Item("DeptName")
                mdtRow.Item("ejob") = dRow.Item("job_name")
                dAEmp.Rows.Add(mdtRow)

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                'Session("AsgEmpList") = dtAssessor

                'Pinkal (27-Dec-2019) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                'dAEmp = dtAssessor
                'Pinkal (27-Dec-2019) -- End

                'Pinkal (11-Sep-2019) -- End

            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            mdtRow = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Add_DataRow :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Add_DataRow :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee()
        Try
            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'Dim dtAssessor As DataTable = CType(Session("AsgEmpList"), DataTable)
            Dim dtAssessor As DataTable = dAEmp.Copy()
            'Pinkal (11-Sep-2019) -- End
            Dim dtAseView As DataView = dtAssessor.DefaultView
            dtAseView.RowFilter = " AUD <> 'D' "
            dgvAssessor.DataSource = dtAseView
            dgvAssessor.DataBind()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtAssessor IsNot Nothing Then dtAssessor.Clear()
            dtAssessor = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Assigned_Employee :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Assigned_Employee :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub GetAsgEmployeePageRecordNo()
        Try
            If dgvAssessor.PageIndex > 0 Then
                Me.ViewState("FirstAsgRecordNo") = (((dgvAssessor.PageIndex + 1) * dgvAssessor.Rows.Count) - dgvAssessor.Rows.Count)
            Else
                If dgvAssessor.Rows.Count < dgvAssessor.PageSize Then
                    Me.ViewState("FirstAsgRecordNo") = ((1 * dgvAssessor.Rows.Count) - dgvAssessor.Rows.Count)
                Else
                    Me.ViewState("FirstAsgRecordNo") = ((1 * dgvAssessor.PageSize) - dgvAssessor.Rows.Count)
                End If

            End If
            Me.ViewState("LastAsgRecordNo") = ((dgvAssessor.PageIndex + 1) * dgvAssessor.Rows.Count)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetAsgEmployeePageRecordNo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetAsgEmployeePageRecordNo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Is_Valid_Data() As Boolean
        'Pinkal (11-Sep-2019) -- Start
        'Enhancement NMB - Working On Claim Retirement for NMB.
        'Dim dtAssessor As DataTable = CType(Session("AsgEmpList"), DataTable)
        Dim dtAssessor As DataTable = dAEmp.Copy
        'Pinkal (11-Sep-2019) -- End
        Dim dtmp() As DataRow = Nothing
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Expense Approver is mandatory information. Please provide Expense Approver to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboExCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Expense Category is mandatory information. Please provide Expense Category to continue"), Me)
                cboExCategory.Focus()
                Return False
            End If

            If CInt(cboApproveLevel.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Expense Approver Level is mandatory information. Please provide Expense Approver Level to continue"), Me)
                cboApproveLevel.Focus()
                Return False
            End If

            If CInt(cboUser.SelectedValue) <= 0 Then
                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                If chkExternalApprover.Checked = False Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "User is mandatory information. Please provide user to continue"), Me)
                    cboUser.Focus()
                    Return False
                End If
                'Pinkal (01-Mar-2016) -- End
            End If


            If dtAssessor Is Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Expense Approver access is mandatory. Please provide Expense Approver access to save."), Me)
            Else
                dtAssessor.AcceptChanges()
                dtmp = dtAssessor.Select("AUD <> 'D'")
                If dtmp.Length <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Expense Approver access is mandatory. Please provide Expense Approver access to save."), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            dtmp = Nothing
            If dtAssessor IsNot Nothing Then dtAssessor.Clear()
            dtAssessor = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Function


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objExAssessorMaster As clsExpenseApprover_Master)
        'Pinkal (05-Sep-2020) -- End
        Try
            With objExAssessorMaster

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If Me.ViewState("CrApproverUnkId") IsNot Nothing Then
                    ._crApproverunkid = CInt(Me.ViewState("CrApproverUnkId"))
                End If
                ._Employeeunkid = CInt(cboEmployee.SelectedValue)
                ._crLevelunkid = CInt(cboApproveLevel.SelectedValue)
                ._Expensetypeid = CInt(cboExCategory.SelectedValue)
                ._Userunkid = CInt(Session("UserId"))
                ._Isexternalapprover = chkExternalApprover.Checked
                If chkExternalApprover.Checked = True Then
                    objExAssessorMaster._MappedUserId = CInt(cboEmployee.SelectedValue)
                Else
                    objExAssessorMaster._MappedUserId = CInt(cboUser.SelectedValue)
                End If
                ._Isvoid = False
                ._Voiddatetime = Nothing
                ._Voidreason = ""
                ._Voiduserunkid = -1
            End With
            'If Me.ViewState("CrApproverUnkId") IsNot Nothing Then
            '    With objExAssessorMaster
            '        ._Isvoid = objExAssessorMaster._Isvoid
            '        ._Voiddatetime = objExAssessorMaster._Voiddatetime
            '        ._Voidreason = objExAssessorMaster._Voidreason
            '        ._Voiduserunkid = objExAssessorMaster._Voiduserunkid
            '        ._crApproverunkid = CInt(Me.ViewState("CrApproverUnkId"))
            '    End With
            'Else
            '    With objExAssessorMaster
            '        ._Isvoid = False
            '        ._Voiddatetime = Nothing
            '        ._Voidreason = ""
            '        ._Voiduserunkid = -1
            '    End With
            'End If
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue:-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub GetValue()
    Private Sub GetValue(ByVal objExAssessorMaster As clsExpenseApprover_Master)
        'Pinkal (05-Sep-2020) -- End
        Try

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            chkExternalApprover.Checked = objExAssessorMaster._Isexternalapprover
            chkExternalApprover_CheckedChanged(Nothing, New EventArgs())
            'Pinkal (27-Dec-2019) -- End

            cboEmployee.SelectedValue = objExAssessorMaster._Employeeunkid.ToString()
            cboUser.SelectedValue = objExAssessorMaster._MappedUserId.ToString
            cboExCategory.SelectedValue = objExAssessorMaster._Expensetypeid.ToString()

            If Me.ViewState("CrApproverUnkId") IsNot Nothing AndAlso CInt(Me.ViewState("CrApproverUnkId")) > 0 Then
                chkExternalApprover.Enabled = False
                cboExCategory.Enabled = False
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Fill_Employee()
                cboExCategory_SelectedIndexChanged(cboExCategory, New EventArgs())
            End If
            cboApproveLevel.SelectedValue = objExAssessorMaster._crLevelunkid.ToString()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue:-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region "Control's Events"

    Protected Sub cboExCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objExLevel As New clsExApprovalLevel
            dsList = objExLevel.getListForCombo(CInt(cboExCategory.SelectedValue), "List", True)
            cboApproveLevel.DataValueField = "Id"
            cboApproveLevel.DataTextField = "Name"
            cboApproveLevel.DataSource = dsList.Tables(0)
            cboApproveLevel.DataBind()
            objExLevel = Nothing

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboExCategory_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboExCategory_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            If CInt(cboExCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Expense Category is mandatory information. Please provide Expense Category to continue"), Me)
                cboExCategory.Focus()
                cboEmployee.SelectedValue = "0"
                Exit Sub
            End If
            'Pinkal (18-Jun-2020) -- End


            If CInt(cboEmployee.SelectedValue) > 0 Then

                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                'Dim mdtAssessor As DataTable = CType(Session("AsgEmpList"), DataTable)
                'If mdtAssessor IsNot Nothing Then
                'mdtAssessor.Rows.Clear()
                'End If
                'Session("AsgEmpList") = mdtAssessor
                'Pinkal (11-Sep-2019) -- End
                If CBool(Session("IsEmployeeAsUser")) Then
                    Dim objUser As New clsUserAddEdit
                    Dim mintUserID As Integer = objUser.Return_UserId(CInt(cboEmployee.SelectedValue), CInt(Session("CompanyUnkId")))

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objUser = Nothing
                    'Pinkal (05-Sep-2020) -- End

                    Dim drRow() As DataRow = CType(Me.ViewState("UserList"), DataTable).Select("userunkid = " & mintUserID)
                    If drRow.Length > 0 Then
                        cboUser.SelectedValue = mintUserID.ToString()
                    Else
                        cboUser.SelectedValue = "0"
                    End If
                End If
                Fill_Employee()
            Else
                dgvAEmployee.DataSource = Nothing
                dgvAEmployee.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboEmployee_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "GridView Event"


    'Pinkal (27-Dec-2019) -- Start
    'Enhancement - Changes related To OT NMB Testing.

    'Protected Sub gvAEmployee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvAEmployee.PageIndexChanging
    '    Try
    '        dgvAEmployee.PageIndex = e.NewPageIndex
    '        Fill_Employee()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgView_PageIndexChanged :- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub dgvAssessor_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvAssessor.PageIndexChanging
    '    dgvAssessor.PageIndex = e.NewPageIndex
    '    Fill_Assigned_Employee()
    'End Sub

    'Pinkal (27-Dec-2019) -- End

#End Region

#Region "TextBox Event"


    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub txtSearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
    '    Try

    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        'If Session("EmpList") IsNot Nothing Then
    '        'Dim dvEmployee As DataView = CType(Session("EmpList"), DataTable).DefaultView
    '        If mdtEmployee IsNot Nothing Then
    '            Dim dvEmployee As DataView = mdtEmployee.DefaultView
    '            'Pinkal (11-Sep-2019) -- End

    '            If dvEmployee IsNot Nothing Then
    '                If dvEmployee.Table.Rows.Count > 0 Then
    '                    dvEmployee.RowFilter = "employeecode like '%" & txtSearchEmp.Text.Trim & "%'  OR name like '%" & txtSearchEmp.Text.Trim & "%' "
    '                    dgvAEmployee.DataSource = dvEmployee
    '                    dgvAEmployee.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtSearchEmp_TextChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub txtaSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAsgSearch.TextChanged
    '    Try

    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        'If Session("AsgEmpList") IsNot Nothing Then
    '        'Dim dvEmployee As DataView = CType(Session("AsgEmpList"), DataTable).DefaultView
    '        If dAEmp IsNot Nothing Then
    '            Dim dvEmployee As DataView = dAEmp.DefaultView
    '            If dvEmployee IsNot Nothing Then
    '                If dvEmployee.Table.Rows.Count > 0 Then
    '                    dvEmployee.RowFilter = "(ecode like '%" & txtAsgSearch.Text.Trim & "%'  OR ename like '%" & txtAsgSearch.Text.Trim & "%') AND AUD <> 'D' "
    '                    dgvAssessor.DataSource = dvEmployee
    '                    dgvAssessor.DataBind()
    '                End If
    '            End If
    '        End If

    '        'Pinkal (11-Sep-2019) -- End

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("txtAsgSearch_TextChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End

#End Region

#Region "Checkbox Event"


    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvAEmployee.Rows.Count <= 0 Then Exit Sub

    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        'Dim dtEmployee As DataTable = CType(Session("EmpList"), DataTable)

    '        If dgvAEmployee.Rows.Count > 0 Then
    '            For i As Integer = 0 To dgvAEmployee.Rows.Count - 1
    '                If mdtEmployee.Rows.Count - 1 < i Then Exit For
    '                Dim DtRow As DataRow() = mdtEmployee.Select("employeecode ='" & dgvAEmployee.Rows(i).Cells(1).Text.Trim & "'")
    '                If DtRow.Length > 0 Then
    '                    DtRow(0)("ischeck") = CBool(cb.Checked)
    '                    CType(dgvAEmployee.Rows(i).FindControl("ChkgvSelect"), CheckBox).Checked = cb.Checked
    '                End If
    '                mdtEmployee.AcceptChanges()
    '            Next
    '            'Session("EmpList") = dtEmployee
    '        End If

    '        'Pinkal (11-Sep-2019) -- End

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then

    '            'Pinkal (11-Sep-2019) -- Start
    '            'Enhancement NMB - Working On Claim Retirement for NMB.
    '            'Dim drRow() As DataRow = CType(Session("EmpList"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            Dim drRow() As DataRow = mdtEmployee.Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            'Pinkal (11-Sep-2019) -- End
    '            If drRow.Length > 0 Then
    '                drRow(0)("ischeck") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkAsgAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvAssessor.Rows.Count <= 0 Then Exit Sub

    '        'Pinkal (11-Sep-2019) -- Start
    '        'Enhancement NMB - Working On Claim Retirement for NMB.
    '        'Dim dtEmployee As DataTable = CType(Session("AsgEmpList"), DataTable)

    '        If dgvAssessor.Rows.Count > 0 Then
    '            For i As Integer = 0 To dgvAssessor.Rows.Count - 1
    '                If dAEmp.Rows.Count - 1 < i Then Exit For
    '                Dim DtRow As DataRow() = dAEmp.Select("ecode ='" & dgvAssessor.Rows(i).Cells(1).Text.Trim & "' AND AUD <> 'D' ") 'dgvAssessor.Rows(mintRowIndex).Cells(1).Text.Trim 
    '                If DtRow.Length > 0 Then
    '                    DtRow(0)("ischeck") = CBool(cb.Checked)
    '                    CType(dgvAssessor.Rows(i).FindControl("ChkAsgSelect"), CheckBox).Checked = cb.Checked
    '                End If
    '                dAEmp.AcceptChanges()
    '            Next
    '            'Session("AsgEmpList") = dtEmployee
    '        End If
    '        'Pinkal (11-Sep-2019) -- End

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkAsgAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkAsgSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then

    '            'Pinkal (11-Sep-2019) -- Start
    '            'Enhancement NMB - Working On Claim Retirement for NMB.
    '            'Dim drRow() As DataRow = CType(Session("AsgEmpList"), DataTable).Select("ecode = '" & gvRow.Cells(1).Text & "' AND AUD <> 'D' ")
    '            Dim drRow() As DataRow = dAEmp.Select("ecode = '" & gvRow.Cells(1).Text & "' AND AUD <> 'D' ")
    '            'Pinkal (11-Sep-2019) -- End
    '            If drRow.Length > 0 Then
    '                drRow(0)("ischeck") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkAsgSelectedEmp_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex.Message,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End

    Protected Sub chkExternalApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            lblUser.Visible = Not chkExternalApprover.Checked
            cboUser.Visible = Not chkExternalApprover.Checked
            If chkExternalApprover.Checked = False AndAlso (Me.ViewState("CrApproverUnkId") Is Nothing OrElse CInt(Me.ViewState("CrApproverUnkId")) <= 0) Then
                cboUser.SelectedValue = "0"
            End If

            dgvAEmployee.DataSource = Nothing
            dgvAEmployee.DataBind()



            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.

            'If chkExternalApprover.Checked = False Then
            '    'Pinkal (11-Sep-2019) -- Start
            '    'Enhancement NMB - Working On Claim Retirement for NMB.
            '    'Dim dtEmployee As DataTable = CType(Me.ViewState("mdtEmployee"), DataTable).Copy
            '    Dim dtEmployee As DataTable = mdtEmployee.Copy()
            '    'Pinkal (11-Sep-2019) -- End

            '    Dim drRow As DataRow = dtEmployee.NewRow
            '    drRow("name") = "Select"
            '    drRow("employeeunkid") = 0
            '    dtEmployee.Rows.InsertAt(drRow, 0)

            '    cboEmployee.DataSource = dtEmployee
            '    cboEmployee.DataTextField = "name"
            '    cboEmployee.DataValueField = "employeeunkid"
            '    cboEmployee.DataBind()
            'Else
            '    Dim objUser As New clsUserAddEdit

            '    Dim dsList As DataSet = objUser.GetExternalApproverList("List", _
            '                                             CInt(Session("CompanyUnkId")), _
            '                                             CInt(Session("Fin_year")), _
            '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
            '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), "975")
            '    objUser = Nothing


            '    Dim drRow As DataRow = dsList.Tables(0).NewRow
            '    drRow("name") = "Select"
            '    drRow("employeeunkid") = 0
            '    dsList.Tables(0).Rows.InsertAt(drRow, 0)


            '    cboEmployee.DataSource = dsList.Tables(0)
            '    cboEmployee.DataTextField = "Name"
            '    cboEmployee.DataValueField = "userunkid"
            '    cboEmployee.DataBind()
            'End If
            FillCombo()
            'Pinkal (27-Dec-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkExternalApprover_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkExternalApprover_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region "Links Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try         'Hemant (13 Aug 2020)
        popupAdvanceFilter.Show()
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            'Me.ViewState("EmpAdvanceSearch") = Nothing
            strAdvanceSearch = ""
            'Pinkal (27-Dec-2019) -- End
            Fill_Employee()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkReset_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkReset_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblDetialHeader.Text)

            Me.lblInfo.Text = Language._Object.getCaption("gbInfo", Me.lblInfo.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.ID, Me.lblApproverName.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.ID, Me.lblUser.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.ID, Me.lblApproveLevel.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.ID, Me.lblExpenseCat.Text)
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.ID, Me.btnDeleteA.Text).Replace("&", "")

            dgvAEmployee.Columns(1).HeaderText = Language._Object.getCaption(dgvAEmployee.Columns(1).FooterText, dgvAEmployee.Columns(1).HeaderText)
            dgvAEmployee.Columns(2).HeaderText = Language._Object.getCaption(dgvAEmployee.Columns(2).FooterText, dgvAEmployee.Columns(2).HeaderText)

            Me.lblAssignedEmployee.Text = Language._Object.getCaption("gbAssignedEmployee", Me.lblAssignedEmployee.Text)

            dgvAssessor.Columns(1).HeaderText = Language._Object.getCaption(dgvAssessor.Columns(1).FooterText, dgvAssessor.Columns(1).HeaderText)
            dgvAssessor.Columns(2).HeaderText = Language._Object.getCaption(dgvAssessor.Columns(2).FooterText, dgvAssessor.Columns(2).HeaderText)
            dgvAssessor.Columns(3).HeaderText = Language._Object.getCaption(dgvAssessor.Columns(3).FooterText, dgvAssessor.Columns(3).HeaderText)
            dgvAssessor.Columns(4).HeaderText = Language._Object.getCaption(dgvAssessor.Columns(4).FooterText, dgvAssessor.Columns(4).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Expense Approver is mandatory information. Please provide Expense Approver to continue.")
			Language.setMessage(mstrModuleName, 2, "Expense Category is mandatory information. Please provide Expense Category to continue")
			Language.setMessage(mstrModuleName, 3, "Expense Approver Level is mandatory information. Please provide Expense Approver Level to continue")
			Language.setMessage(mstrModuleName, 4, "User is mandatory information. Please provide user to continue")
			Language.setMessage(mstrModuleName, 5, "Expense Approver access is mandatory. Please provide Expense Approver access to save.")
			Language.setMessage(mstrModuleName, 6, "Please check atleast one of the employee to unassigned.")
			Language.setMessage(mstrModuleName, 9, "Are you sure you want to delete Selected Employee?")
			Language.setMessage(mstrModuleName, 10, "This Employee has Pending Expense Application Form.You cannot delete this employee.")
			Language.setMessage("clsEmployee_Master", 42, "Code")
			Language.setMessage("clsEmployee_Master", 46, "Employee Name")
			Language.setMessage("clsEmployee_Master", 118, "Job")
			Language.setMessage("clsEmployee_Master", 120, "Department")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
