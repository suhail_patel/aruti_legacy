﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading

#End Region
Partial Class Claims_And_Expenses_wPg_ClaimAndRequestList
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimReqestMaster As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Private objApproverTran As New clsclaim_request_approval_tran
    'Pinkal (05-Sep-2020) -- End

    Private Shared ReadOnly mstrModuleName As String = "frmClaims_RequestList"
    Private Shared ReadOnly mstrModuleName1 As String = "frmCancelExpenseForm"

    'Pinkal (04-Feb-2020) -- Start
    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
    Private objCONN As SqlConnection
    Private mintCrMasterId As Integer = 0
    'Pinkal (04-Feb-2020) -- End

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    Private mintCancelExpEmployeeId As Integer = 0
    'Pinkal (05-Sep-2020) -- End


#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    GC.Collect()
                    'Pinkal (05-Sep-2020) -- End

                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If


                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                If arr.Length = 4 Then

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    Me.ViewState.Add("ClaimFormEmpId", CInt(arr(1)))
                    Me.ViewState.Add("crmasterunkid", CInt(arr(2)))
                    Me.ViewState.Add("ExpenseCategoryID", CInt(arr(3)))

                    mintCrMasterId = CInt(arr(2))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                        Exit Sub
                    End If


                    HttpContext.Current.Session("mdbname") = Session("Database_Name")

                        'Pinkal (13-Aug-2020) -- Start
                        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                    gobjConfigOptions = New clsConfigOptions
                        'gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Pinkal (13-Aug-2020) -- End

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                        'Dim clsConfig As New clsConfigOptions
                        'clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        'Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        'Session("PaymentApprovalwithLeaveApproval") = clsConfig._PaymentApprovalwithLeaveApproval
                        'Session("fmtCurrency") = clsConfig._CurrencyFormat

                        'If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        '    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        'Else
                        '    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                        'End If

                        'Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim

                        Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                        Session("PaymentApprovalwithLeaveApproval") = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
                        Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                        If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                            Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                    End If

                        Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim

                        'Pinkal (05-Sep-2020) -- End


                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Dim base As New Basepage
                    If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(arr(1))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page)
                        Exit Try
                    End If


                    'Pinkal (27-Feb-2020) -- Start
                    'Enhancement - Changes related To OT NMB Testing.
                    Call GetDatabaseVersion()
                    'Pinkal (27-Feb-2020) -- End

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(arr(1))

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                    HttpContext.Current.Session("UserId") = -1
                    HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
                    HttpContext.Current.Session("UserName") = "ID " & " : " & objEmployee._Employeecode & vbCrLf & "Employee : " & objEmployee._Firstname & " " & objEmployee._Surname 'objEmp._Displayname
                    HttpContext.Current.Session("Password") = objEmployee._Password
                    HttpContext.Current.Session("LeaveBalances") = 0
                    HttpContext.Current.Session("MemberName") = "Emp. : (" & objEmployee._Employeecode & ") " & objEmployee._Firstname & " " & objEmployee._Surname
                    HttpContext.Current.Session("RoleID") = 0
                    HttpContext.Current.Session("LangId") = 1
                    HttpContext.Current.Session("Firstname") = objEmployee._Firstname
                    HttpContext.Current.Session("Surname") = objEmployee._Surname
                    HttpContext.Current.Session("DisplayName") = objEmployee._Displayname
                    HttpContext.Current.Session("Theme_id") = objEmployee._Theme_Id
                    HttpContext.Current.Session("Lastview_id") = objEmployee._LastView_Id


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objEmployee = Nothing
                        'Pinkal (05-Sep-2020) -- End


                    strError = ""
                    If base.SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "Index.aspx")
                        Exit Try
                    End If

                    strError = ""
                    If base.SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "Index.aspx")
                        Exit Try
                    End If

                    End If  '      If arr.Length = 4 Then

                End If   ' If Request.QueryString.Count > 0 Then

                'Pinkal (13-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            End If  ' If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
            'Pinkal (13-Aug-2020) -- End

            SetLanguage()

            If IsPostBack = False Then


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                SetDateFormat()
                'Pinkal (27-Aug-2020) -- End

                FillCombo()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                'objClaimReqestMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
                'Pinkal (05-Sep-2020) -- End

                Me.ViewState.Add("crmasterunkid", 0)
                Me.ViewState.Add("FirstRecordNo", 0)
                Me.ViewState.Add("LastRecordNo", 0)

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    lvClaimRequestList.Columns(2).Visible = True
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    lvClaimRequestList.Columns(2).Visible = False
                End If


                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                'If Session("ClaimRequest_EmpUnkId") IsNot Nothing Then
                '    cboEmployee.SelectedValue = Session("ClaimRequest_EmpUnkId").ToString()
                '    Session.Remove("ClaimRequest_EmpUnkId")
                '    If CInt(cboEmployee.SelectedValue) > 0 Then
                '        Call btnSearch_Click(btnSearch, Nothing)
                '    End If
                'End If
                'Pinkal (27-Aug-2020) -- End

              

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                If Request.QueryString.Count > 0 AndAlso CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    Fill_List()
                End If
                'Pinkal (04-Feb-2020) -- End

                SetVisibility()

            Else
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                mintCancelExpEmployeeId = CInt(Me.ViewState("CancelExpEmployeeId"))
                'Pinkal (05-Sep-2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
        Me.IsLoginRequired = True
        End If
    End Sub

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("CancelExpEmployeeId") = mintCancelExpEmployeeId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Pinkal (05-Sep-2020) -- End


#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (11-Sep-2019) -- End

            cboExpCategory.DataValueField = "id"
            cboExpCategory.DataTextField = "name"
            cboExpCategory.DataSource = dsCombo.Tables(0)
            cboExpCategory.DataBind()

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objEMaster = Nothing
            objPrd = Nothing
            objMasterData = Nothing
            'Pinkal (05-Sep-2020) -- End`
        End Try
    End Sub

    Private Sub Fill_List(Optional ByVal blnstatus As Boolean = False)
        Dim dsList As New DataSet
        Dim dTable As DataTable = Nothing
        Dim StrSearch As String = String.Empty

        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimReqestMaster As New clsclaim_request_master
        'Pinkal (05-Sep-2020) -- End

        Try

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("AllowtoViewClaimExpenseFormList ")) = False Then Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.expensetypeid = '" & CInt(cboExpCategory.SelectedValue) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.statusunkid = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If txtClaimNo.Text.Trim.Length > 0 Then
                StrSearch &= "AND cmclaim_request_master.claimrequestno LIKE '%" & txtClaimNo.Text & "%' "
            End If

            If dtpFDate.IsNull = False AndAlso dtpTDate.IsNull = False Then
                StrSearch &= "AND cmclaim_request_master.transactiondate >= '" & eZeeDate.convertDate(dtpFDate.GetDate) & "' AND cmclaim_request_master.transactiondate <= '" & eZeeDate.convertDate(dtpTDate.GetDate) & "' "
            End If


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee AndAlso Request.QueryString.Count > 0 Then
                StrSearch &= "AND cmclaim_request_master.crmasterunkid = " & mintCrMasterId
            End If
            'Pinkal (04-Feb-2020) -- End



            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If


            dsList = objClaimReqestMaster.GetList("List", CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                    , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                    , True, CBool(IIf(CInt(Session("LoginBy")) = Global.User.en_loginby.User, True, False)), StrSearch)

            dTable = New DataView(dsList.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable

            lvClaimRequestList.AllowPaging = False
            lvClaimRequestList.DataSource = dTable
            lvClaimRequestList.DataKeyField = "crmasterunkid"
            lvClaimRequestList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dTable IsNot Nothing Then dTable.Clear()
            dTable = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objClaimReqestMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                btnNew.Visible = CBool(Session("AllowtoAddClaimExpenseForm"))
                dgvData.Columns(0).Visible = CBool(Session("AllowtoEditClaimExpenseForm"))
                dgvData.Columns(1).Visible = CBool(Session("AllowtoDeleteClaimExpenseForm"))
                dgvData.Columns(2).Visible = CBool(Session("AllowtoCancelClaimExpenseForm"))

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee AndAlso Request.QueryString.Count > 0 Then
                btnNew.Visible = False
                btnSearch.Visible = False
                btnReset.Visible = False
                lvClaimRequestList.Columns(0).Visible = False
                lvClaimRequestList.Columns(1).Visible = False
                lvClaimRequestList.Columns(2).Visible = False
                'Pinkal (04-Feb-2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("SetVisibility:-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub CancelFillCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Try

            dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")).Date, "List", True, 1)
            cboCancelPeriod.DataValueField = "periodunkid"
            cboCancelPeriod.DataTextField = "name"
            cboCancelPeriod.DataSource = dsCombo.Tables(0)
            cboCancelPeriod.DataBind()
            cboCancelPeriod.SelectedValue = "0"

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            cboCancelExpCategory.DataValueField = "id"
            cboCancelExpCategory.DataTextField = "name"
            cboCancelExpCategory.DataSource = dsCombo.Tables(0)
            cboCancelExpCategory.DataBind()
            cboCancelExpCategory.SelectedValue = "0"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub CancelGetValue()
    Private Sub CancelGetValue(ByRef objClaimReqestMaster As clsclaim_request_master)
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objClaimReqestMaster._Employeeunkid
            txtCancelEmployee.Text = objEmployee._Employeecode & " - " & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtCancelEmployeeid.Value = objClaimReqestMaster._Employeeunkid.ToString()
            txtCancelClaimNo.Text = objClaimReqestMaster._Claimrequestno
            cboCancelExpCategory.SelectedValue = objClaimReqestMaster._Expensetypeid.ToString()
            dtpCancelDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            objClaimReqestMaster._Transactiondate = dtpCancelDate.GetDate
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmployee = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try
            Dim mdt As DataTable = (CType(ViewState("mdtTran"), DataTable))

            If mdt IsNot Nothing AndAlso mdt.Rows.Count > 0 Then

                If mdt.Columns.Contains("IsPosted") = False Then
                    mdt.Columns.Add("IsPosted", Type.GetType("System.Boolean"))
                    mdt.Columns("IsPosted").DefaultValue = False
                End If

                Dim mstrApprovalTranunkid As String = ""
                Dim objClaimProces As New clsclaim_process_Tran
                Dim dsList As DataSet = objClaimProces.GetList("List", True, , "cmclaim_process_tran.crmasterunkid = " & CInt(Me.ViewState("mintClaimRequestMasterId")))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        If CBool(dsList.Tables(0).Rows(i)("IsPosted")) Then
                            objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(dsList.Tables(0).Rows(i)("periodunkid"))

                            Dim drRow() As DataRow = mdt.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If objPeriod._Statusid = 2 Then 'enStatusType.Close
                                If drRow.Length > 0 Then
                                    drRow(0).Delete()
                                End If
                            Else
                                If drRow.Length > 0 Then
                                    drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                                End If
                            End If
                        Else
                            Dim drRow() As DataRow = mdt.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If drRow.Length > 0 Then
                                drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                            End If
                        End If
                        mstrApprovalTranunkid &= CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")) & ","
                    Next

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objPeriod = Nothing
                    'Pinkal (05-Sep-2020) -- End



                    If mstrApprovalTranunkid.Trim.Length > 0 Then mstrApprovalTranunkid = mstrApprovalTranunkid.Trim.Substring(0, mstrApprovalTranunkid.Trim.Length - 1)

                    Dim dRows() As DataRow = mdt.Select("crapprovaltranunkid Not in ( " & mstrApprovalTranunkid & ")")
                    If dRows.Length > 0 Then
                        For j As Integer = 0 To dRows.Length - 1
                            dRows(j)("IsPosted") = False
                        Next
                    End If
                Else
                    For Each dRow As DataRow In mdt.Rows
                        dRow("IsPosted") = False
                    Next
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                objClaimProces = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If



            mdt.AcceptChanges()
            Me.ViewState("CanceExpTable") = mdt


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdt = New DataView(mdt, "iscancel = 0", "", DataViewRowState.CurrentRows).ToTable
            mdt = New DataView(mdt, "iscancel = 0", "", DataViewRowState.CurrentRows).ToTable().Copy()
            'Pinkal (05-Sep-2020) -- End

            dgvData.DataSource = mdt
            dgvData.DataBind()
            If mdt.Rows.Count > 0 Then
                txtGrandTotal.Text = Format(CDec(mdt.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0")), Session("fmtCurrency").ToString())
            Else
                txtGrandTotal.Text = "0.00"
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdt
            Me.ViewState("mdtTran") = mdt.Copy()
            If mdt IsNot Nothing Then mdt.Clear()
            mdt = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If txtCancelRemark.Text.Trim.Length <= 0 Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Cancel Remark cannot be blank.Cancel Remark is required information."), Me)
                txtCancelRemark.Focus()
                Return False
            End If


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            If mdtTran Is Nothing Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Please Select atleast One Expense to cancel."), Me)
                Return False
            End If
            'Pinkal (05-Sep-2020) -- End

            Dim drRow() As DataRow = mdtTran.Select("Ischeck = True")
            If drRow.Length <= 0 Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Please Select atleast One Expense to cancel."), Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Function


    'Pinkal (22-Oct-2021)-- Start
    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.

    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
SendEmail:
                For Each objEmail In gobjEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    objSendMail._Form_Name = "frmCancelExpenseForm"
                    objSendMail._ClientIP = Session("IP_ADD").ToString()
                    objSendMail._HostName = Session("HOST_NAME").ToString()

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = intCompanyID(0)
                    End If
                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
                                                objEmail._ExportReportPath).ToString.Length > 0 Then
                        gobjEmailList.Remove(objEmail)
                        GoTo SendEmail
                    End If
                Next
                gobjEmailList.Clear()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2021)-- End

#End Region

#Region "Button's Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Session("ClaimRequest_EmpUnkId") = cboEmployee.SelectedValue
            End If
            Response.Redirect("~/Claims_And_Expenses/wPg_AddEditClaimAndRequestList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnNew_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Expense Category is compulsory information.Please Select Expense Category. "), Me)
                cboExpCategory.Focus()
                Exit Sub

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'ElseIf CInt(cboStatus.SelectedValue) <= 0 Then
            ElseIf CInt(cboStatus.SelectedValue) <= 0 AndAlso CInt(Session("Employeeunkid")) <= 0 Then
                'Pinkal (10-Feb-2021) -- End
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Status is compulsory information.Please Select Status. "), Me)
                cboStatus.Focus()
                Exit Sub
            End If
            'Pinkal (27-Aug-2020) -- End

            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_List:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Dim dtExpense As DataTable = Nothing
        Dim mdtAttachement As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimReqestMaster As clsclaim_request_master
        'Pinkal (05-Sep-2020) -- End

        Try
            If (popup_DeleteReason.Reason.Trim = "") Then
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                popup_DeleteReason.Show()
                Exit Sub
            End If



            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmClaims_RequestList"
            'StrModuleName2 = "mnuUtilitiesMain"
            'StrModuleName3 = "mnuClaimsExpenses"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
            'clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            'clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimReqestMaster = New clsclaim_request_master
            objClaimReqestMaster._Crmasterunkid = CInt(Me.ViewState("crmasterunkid"))
            objClaimReqestMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            'Pinkal (05-Sep-2020) -- End

            With objClaimReqestMaster
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text
                If CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                    ._VoidLoginEmployeeID = -1
                ElseIf CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                    ._VoidLoginEmployeeID = CInt(Session("Employeeunkid"))
                    ._Voiduserunkid = -1
                End If

                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            objClaimReqestMaster._CompanyID = CInt(Session("CompanyUnkId"))
            objClaimReqestMaster._ArutiSelfServiceURL = Session("ArutiSelfServiceURL").ToString()
            If CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                objClaimReqestMaster._LoginMode = enLogin_Mode.EMP_SELF_SERVICE
            ElseIf CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                objClaimReqestMaster._LoginMode = enLogin_Mode.MGR_SELF_SERVICE
            End If
            objClaimReqestMaster._Userunkid = CInt(Session("UserId"))
            'objClaimReqestMaster._WebFormName = mstrModuleName
            'Pinkal (13-Jul-2015) -- End

            Dim objClaimTran As New clsclaim_request_tran
            objClaimTran._ClaimRequestMasterId = CInt(Me.ViewState("crmasterunkid"))
            dtExpense = objClaimTran._DataTable
            objClaimTran = Nothing

            Dim objAttchement As New clsScan_Attach_Documents

            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", objClaimReqestMaster._Employeeunkid)
            Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", objClaimReqestMaster._Employeeunkid, False, Nothing, Nothing, "", CBool(IIf(CInt(objClaimReqestMaster._Employeeunkid) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim strTranIds As String = String.Join(",", dtExpense.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
            If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
            mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
            objAttchement = Nothing

            For Each dRow As DataRow In mdtAttachement.Rows
                dRow.Item("AUD") = "D"
            Next
            mdtAttachement.AcceptChanges()

            If mdtAttachement IsNot Nothing Then

                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                For Each dRow As DataRow In mdtAttachement.Rows
                    If dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            With objClaimReqestMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            objClaimReqestMaster.Delete(CInt(Me.ViewState("crmasterunkid")), _
                                        CInt(Session("UserId")), _
                                        Session("Database_Name").ToString(), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        Session("EmployeeAsOnDate").ToString(), _
                                        Session("UserAccessModeSetting").ToString(), True, mdtAttachement, True, Nothing)

            If (objClaimReqestMaster._Message.Length > 0) Then
                DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & objClaimReqestMaster._Message, Me)
            Else
                DisplayMessage.DisplayMessage("Entry Successfully deleted." & objClaimReqestMaster._Message, Me)
                Me.ViewState("crmasterunkid") = Nothing
            End If
            popup_DeleteReason.Dispose()
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dtExpense IsNot Nothing Then dtExpense.Clear()
            dtExpense = Nothing
            If mdtAttachement IsNot Nothing Then mdtAttachement.Clear()
            mdtAttachement = Nothing
            objClaimReqestMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnCancelSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSave.Click
        Try
            If Is_Valid() = False Then
                popupCancel.Show()
                Exit Sub
            End If

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmCancelExpenseForm"
            'StrModuleName2 = "mnuUtilitiesMain"
            'StrModuleName3 = "mnuClaimsExpenses"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
            'clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            'clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objApproverTran As New clsclaim_request_approval_tran
            objApproverTran._YearId = CInt(Session("Fin_year"))
            objApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))

With objApproverTran
                'Pinkal (07-Jan-2019) -- Start
                'AT Testing- Working on AT Testing for All Modules.
                '._FormName = mstrModuleName
                ._FormName = mstrModuleName1
                'Pinkal (07-Jan-2019) -- End
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With            

Dim blnflag As Boolean = objApproverTran.CancelExpense(CType(ViewState("mdtTran"), DataTable), enExpenseType.EXP_NONE, txtCancelRemark.Text, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, Nothing)
            objApproverTran = Nothing
            'Pinkal (05-Sep-2020) -- End

            If blnflag Then
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                Dim objClaimMaster As New clsclaim_request_master
                Dim objEmployee As New clsEmployee_Master

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objClaimReqestMaster._Employeeunkid
                'objClaimMaster._Crmasterunkid = CInt(Me.ViewState("crmasterunkid"))
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintCancelExpEmployeeId
                objClaimMaster._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
                'Pinkal (05-Sep-2020) -- End

                objClaimMaster._EmployeeCode = objEmployee._Employeecode
                objClaimMaster._EmployeeFirstName = objEmployee._Firstname
                objClaimMaster._EmployeeMiddleName = objEmployee._Othername
                objClaimMaster._EmployeeSurName = objEmployee._Surname
                objClaimMaster._EmpMail = objEmployee._Email

                With objClaimMaster
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With
                objClaimMaster.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, CInt(Session("UserId")), txtCancelRemark.Text.Trim)
                'Sohail (30 Nov 2017) -- End

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'objClaimMaster.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, CInt(Session("UserId")), txtCancelRemark.Text.Trim)
                objClaimMaster.SendMailToEmployee(CInt(cboEmployee.SelectedValue), txtCancelClaimNo.Text.Trim, 6, Company._Object._Companyunkid, "", "", enLogin_Mode.DESKTOP, -1, CInt(Session("UserId")), txtCancelRemark.Text.Trim)
                'Pinkal (05-Sep-2020) -- End


                'Pinkal (22-Oct-2021)-- Start
                'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                If gobjEmailList.Count > 0 Then
                    Dim objThread As Threading.Thread
                    If HttpContext.Current Is Nothing Then
                        objThread = New Threading.Thread(AddressOf Send_Notification)
                        objThread.IsBackground = True
                        Dim arr(1) As Object
                        arr(0) = CInt(Session("CompanyUnkId"))
                        objThread.Start(arr)
                    Else
                        Call Send_Notification(CInt(Session("CompanyUnkId")))
                    End If
                End If
                'Pinkal (22-Oct-2021) -- End


                'Sohail (30 Nov 2017) -- End

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objClaimMaster = Nothing
                objEmployee = Nothing
                'Pinkal (05-Sep-2020) -- End

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Expense cancellation process completed successfully."), Me)
                popupCancel.Hide()
                Fill_List()
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Sorry, Expense cancellation process fail."), Me)
                popupCancel.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnCancelSave_Click:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'FillCombo()
            'Fill_List(True)
            lvClaimRequestList.DataSource = Nothing
            lvClaimRequestList.DataBind()
            'Pinkal (05-Sep-2020) -- End

            txtClaimNo.Text = ""
            dtpFDate.SetDate = Nothing
            dtpTDate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click:-" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub lvClaimRequestList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvClaimRequestList.DeleteCommand
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimReqestMaster As New clsclaim_request_master
            If objClaimReqestMaster.isUsed(CInt(lvClaimRequestList.DataKeys(e.Item.ItemIndex))) = True Then
                DisplayMessage.DisplayMessage(objClaimReqestMaster._Message, Me)
                objClaimReqestMaster = Nothing
                Exit Sub
            End If
            objClaimReqestMaster = Nothing
            'Pinkal (05-Sep-2020) -- End

            If CInt(e.Item.Cells(10).Text) = 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot delete selected data. Reason : It is already approved."), Me)
                Exit Sub

            ElseIf CInt(e.Item.Cells(10).Text) = 2 Then

                Dim objClaimApproval As New clsclaim_request_approval_tran
                Dim dsPendingList As DataSet = objClaimApproval.GetApproverExpesneList("Approval", False, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                                    , Session("Database_Name").ToString(), CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString() _
                                                                                                                                    , CInt(e.Item.Cells(11).Text.ToString()), False, True, -1 _
                                                                                                                                    , " cmclaim_approval_tran.statusunkid <> 2 ", CInt(e.Item.Cells(13).Text.ToString()))
                objClaimApproval = Nothing

                If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, you cannot delete selected data. Reason:It is already approved by one or more approver(s)."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                    dsPendingList = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                dsPendingList = Nothing
                'Pinkal (05-Sep-2020) -- End


                '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Length > 0 Then
                    Dim objClaim As New clsclaim_request_tran
                    objClaim._ClaimRequestMasterId = CInt(e.Item.Cells(13).Text.ToString())
                    Dim dtTable As DataTable = objClaim._DataTable
                    objClaim = Nothing
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        'If xBudgetMandatoryCount > 0 Then
                        Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                        If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, you cannot delete selected data. Reason:It is already approved or pending for approval in P2P system."), Me)
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtTable IsNot Nothing Then dtTable.Clear()
                            dtTable = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If


                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.


                Me.ViewState("crmasterunkid") = CInt(lvClaimRequestList.DataKeys(e.Item.ItemIndex))
                Language.setLanguage(mstrModuleName)
                popup_DeleteReason.Title = Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Claim Request?")
                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()

            ElseIf CInt(e.Item.Cells(10).Text) = 3 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot delete selected data. Reason : It is already rejected."), Me)
                Exit Sub

            ElseIf CInt(e.Item.Cells(10).Text) = 6 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot delete selected data. Reason : It is already cancelled."), Me)
                Exit Sub

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("lvClaimRequestList_DeleteCommand:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lvClaimRequestList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvClaimRequestList.ItemCommand
        Try
            If e.CommandName = "Cancel" Then

                If CInt(e.Item.Cells(10).Text) <> 1 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot cancel selected data. Reason : It is not in approved status."), Me)
                    Exit Sub
                End If


                '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Length > 0 Then
                    Dim objClaim As New clsclaim_request_tran
                    objClaim._ClaimRequestMasterId = CInt(e.Item.Cells(13).Text.ToString())
                    Dim dtTable As DataTable = objClaim._DataTable
                    objClaim = Nothing
                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                        If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot cancel selected data. Reason:It is already approved in P2P system."), Me)
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtTable IsNot Nothing Then dtTable.Clear()
                            dtTable = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If
                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

                'Pinkal (04-Feb-2019) -- End


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objClaimReqestMaster As New clsclaim_request_master
                Dim objClaimTran As New clsclaim_request_tran
                Dim objApproverTran As New clsclaim_request_approval_tran
                Dim dtApprover As DataTable = Nothing
                Dim mdtTran As New DataTable
                'Pinkal (05-Sep-2020) -- End

                dtApprover = objApproverTran.GetMaxApproverForClaimForm(CInt(e.Item.Cells(12).Text), CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(13).Text) _
                                                                             , CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(e.Item.Cells(10).Text))
                If dtApprover.Rows.Count > 0 Then
                    CancelFillCombo()
                    Me.ViewState.Add("mintClaimRequestMasterId", CInt(e.Item.Cells(13).Text))
                    Me.ViewState.Add("mintClaimApproverId", CInt(dtApprover.Rows(0)("crapproverunkid")))
                    Me.ViewState.Add("mintClaimApproverEmpID", CInt(dtApprover.Rows(0)("approveremployeeunkid")))
                    Me.ViewState.Add("mintApproverPriority", CInt(dtApprover.Rows(0)("crpriority")))
                    objClaimReqestMaster._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'CancelGetValue()
                    objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                    objClaimReqestMaster._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
                    mintCancelExpEmployeeId = objClaimReqestMaster._Employeeunkid
                    CancelGetValue(objClaimReqestMaster)
                    'Pinkal (05-Sep-2020) -- End

                    objClaimTran._ClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))
                    mdtTran = objApproverTran.GetApproverExpesneList("List", False, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                , Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                , Session("EmployeeAsOnDate").ToString(), CInt(cboCancelExpCategory.SelectedValue) _
                                                                                                , False, True, CInt(Me.ViewState("mintClaimApproverId")), "" _
                                                                                                , CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing).Tables(0)

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Me.ViewState.Add("mdtTran", mdtTran)
                    Me.ViewState.Add("mdtTran", mdtTran.Copy())
                    'Pinkal (05-Sep-2020) -- End
                    Fill_Expense()
                    txtCancelRemark.Text = ""
                    popupCancel.Show()
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "There is no Expense Data to Cancel the Expense Form."), Me)
                End If


                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtApprover IsNot Nothing Then dtApprover.Clear()
                dtApprover = Nothing
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing
                objApproverTran = Nothing
                objClaimTran = Nothing
                objClaimReqestMaster = Nothing
                'Pinkal (05-Sep-2020) -- End


            ElseIf e.CommandName = "Edit" Then

                If CInt(e.Item.Cells(10).Text) = 1 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot edit selected data. Reason:It is already approved."), Me)
                    Exit Sub

                ElseIf CInt(e.Item.Cells(10).Text) = 2 Then


                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    Dim objClaimApproval As New clsclaim_request_approval_tran
                    Dim dsPendingList As DataSet = objClaimApproval.GetApproverExpesneList("Approval", False, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                                        , Session("Database_Name").ToString(), CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString() _
                                                                                                                                        , CInt(e.Item.Cells(11).Text.ToString()), False, True, -1 _
                                                                                                                                        , " cmclaim_approval_tran.statusunkid <> 2 ", CInt(e.Item.Cells(13).Text.ToString()))
                    objClaimApproval = Nothing

                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot edit selected data. Reason:It is already approved by one or more approver(s)."), Me)
                        Exit Sub
                    End If

                    '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                    If Session("NewRequisitionRequestP2PServiceURL").ToString().Length > 0 Then
                        Dim objClaim As New clsclaim_request_tran
                        objClaim._ClaimRequestMasterId = CInt(e.Item.Cells(13).Text.ToString())
                        Dim dtTable As DataTable = objClaim._DataTable
                        objClaim = Nothing

                        If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                            Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                            'Pinkal (04-Feb-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                            If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot edit selected data. Reason:It is already approved or pending for approval in P2P system."), Me)
                                'Pinkal (05-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                If dtTable IsNot Nothing Then dtTable.Clear()
                                dtTable = Nothing
                                'Pinkal (05-Sep-2020) -- End
                                Exit Sub
                            End If
                            'Pinkal (04-Feb-2019) -- End
                        End If
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dtTable IsNot Nothing Then dtTable.Clear()
                        dtTable = Nothing
                        'Pinkal (05-Sep-2020) -- End
                    End If
                    '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.

                    'Pinkal (20-Nov-2018) -- End

                    Session.Add("mintClaimRequestMasterId", CInt(lvClaimRequestList.DataKeys(e.Item.ItemIndex)))

                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Session("ClaimRequest_EmpUnkId") = cboEmployee.SelectedValue
                    End If
                    Response.Redirect("~/Claims_And_Expenses/wPg_AddEditClaimAndRequestList.aspx", False)

                ElseIf CInt(e.Item.Cells(10).Text) = 3 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot edit selected data. Reason:It is already rejected."), Me)
                    Exit Sub

                ElseIf CInt(e.Item.Cells(10).Text) = 6 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit selected data. Reason: It is already cancelled."), Me)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lvClaimRequestList_ItemCommand:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lvClaimRequestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvClaimRequestList.ItemDataBound
        Try

            'Pinkal (27-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Call SetDateFormat()
            'Pinkal (27-Aug-2020) -- End
            If e.Item.Cells.Count > 0 And e.Item.ItemIndex >= 0 Then
                Dim lnkCancel As LinkButton = CType(e.Item.Cells(2).FindControl("lnkCancel"), LinkButton)

                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	
                'Language.setLanguage(mstrModuleName)
                lnkCancel.Text = Language._Object.getCaption(Me.lvClaimRequestList.Columns(2).FooterText, Me.lvClaimRequestList.Columns(2).HeaderText)
                'Pinkal (28-Apr-2020) -- End

                'Pinkal (30-Mar-2020) -- Start
                'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
                If CBool(Session("AllowToCancelLeaveButRetainExpense")) AndAlso CInt(e.Item.Cells(11).Text) = enExpenseType.EXP_LEAVE AndAlso CInt(e.Item.Cells(10).Text) = 1 Then
                    lnkCancel.Visible = False
                End If
                'Pinkal (30-Mar-2020) -- End


                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	
                Dim mdcAppliedAmount As Decimal = 0
                If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString())
                Else
                Dim objClaimReqestTran As New clsclaim_request_tran
                    mdcAppliedAmount = objClaimReqestTran.GetAppliedExpenseAmount(CInt(e.Item.Cells(13).Text))
                e.Item.Cells(7).Text = Format(mdcAppliedAmount, Session("fmtCurrency").ToString())
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objClaimReqestTran = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If
                'Pinkal (28-Apr-2020) -- End




                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	

                If (e.Item.Cells(14).Text.Trim.Length > 0 AndAlso e.Item.Cells(14).Text <> "&nbsp;") AndAlso CInt(e.Item.Cells(10).Text) = 1 Then  'P2P REQUISTION ID
                    If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                        e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(15).Text), Session("fmtCurrency").ToString())
                    Else
                    e.Item.Cells(8).Text = Format(mdcAppliedAmount, Session("fmtCurrency").ToString())
                    End If
                Else
                    If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                        e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(16).Text), Session("fmtCurrency").ToString())
                Else
                        Dim objClaimReqestMaster As New clsclaim_request_master
                objClaimReqestMaster._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
                Dim mdcApprovedAmount As Decimal = objClaimReqestMaster.GetFinalApproverApprovedAmount(CInt(e.Item.Cells(12).Text), CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(13).Text))
                e.Item.Cells(8).Text = Format(mdcApprovedAmount, Session("fmtCurrency").ToString())
                        objClaimReqestMaster = Nothing
                End If
                End If
                'Pinkal (28-Apr-2020) -- End


                If e.Item.Cells(6).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(6).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToShortDateString
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lvClaimRequestList_ItemDataBound" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.Cells.Count > 0 And e.Row.RowIndex >= 0 Then
                e.Row.Cells(3).Text = CDec(e.Row.Cells(3).Text).ToString
                e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency").ToString())
                e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency").ToString)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvData_RowDataBound:-" & ex.Message, Me)
        End Try
        
    End Sub

#End Region

#Region "Control Event"

    Protected Sub chkCancelAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If dgvData.Rows.Count <= 0 Then Exit Sub
            Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
            For Each xRow As GridViewRow In dgvData.Rows
                If CBool(mdtTran.Rows(xRow.RowIndex)("IsPosted")) = True Then
                    CType(xRow.FindControl("ChkCancelSelect"), CheckBox).Checked = False
                Else
                mdtTran.Rows(xRow.RowIndex)("ischeck") = CBool(cb.Checked)
                CType(xRow.FindControl("ChkCancelSelect"), CheckBox).Checked = cb.Checked
                End If
            Next
                    mdtTran.AcceptChanges()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdtTran
            Me.ViewState("mdtTran") = mdtTran.Copy
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
            popupCancel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkAll_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub ChkCancelSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'Dim dRow() As DataRow = CType(Me.ViewState("mdtTran"), DataTable).Select("crapprovaltranunkid = '" & gvr.Cells(7).Text & "'")
                Dim dRow() As DataRow = CType(Me.ViewState("mdtTran"), DataTable).Select("crapprovaltranunkid = '" & gvr.Cells(8).Text & "'")  'crapprovaltranunkid
                'Pinkal (04-Feb-2019) -- End
                If dRow.Length > 0 Then
                    If CBool(dRow(0)("IsPosted")) = True Then
                        Language.setLanguage(mstrModuleName1)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period."), Me)
                        cb.Checked = False
                    Else
                    dRow(0).Item("ischeck") = cb.Checked
                    dRow(0).AcceptChanges()
                End If
            End If
            End If

            Dim drRow() As DataRow = CType(Me.ViewState("mdtTran"), DataTable).Select("ischeck = True")
            If drRow.Length > 0 Then
                If drRow.Length = dgvData.Rows.Count Then
                    CType(dgvData.HeaderRow.FindControl("chkCancelAll"), CheckBox).Checked = True
                Else
                    CType(dgvData.HeaderRow.FindControl("chkCancelAll"), CheckBox).Checked = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("chkAll_CheckedChanged :- " & ex.Message, Me)
        Finally
            popupCancel.Show()
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblPageHeader.Text = Language._Object.getCaption("eZeeHeader_Title", Me.lblPageHeader.Text).Replace("&&", "&")
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")

            Me.lvClaimRequestList.Columns(0).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(0).FooterText, Me.lvClaimRequestList.Columns(0).HeaderText)
            Me.lvClaimRequestList.Columns(1).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(1).FooterText, Me.lvClaimRequestList.Columns(1).HeaderText).Replace("&", "")
            Me.lvClaimRequestList.Columns(2).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(2).FooterText, Me.lvClaimRequestList.Columns(2).HeaderText)
            Me.lvClaimRequestList.Columns(3).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(3).FooterText, Me.lvClaimRequestList.Columns(3).HeaderText)
            Me.lvClaimRequestList.Columns(4).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(4).FooterText, Me.lvClaimRequestList.Columns(4).HeaderText)
            Me.lvClaimRequestList.Columns(5).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(5).FooterText, Me.lvClaimRequestList.Columns(5).HeaderText)
            Me.lvClaimRequestList.Columns(6).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(6).FooterText, Me.lvClaimRequestList.Columns(6).HeaderText)
            Me.lvClaimRequestList.Columns(7).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(7).FooterText, Me.lvClaimRequestList.Columns(7).HeaderText)
            Me.lvClaimRequestList.Columns(8).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(8).FooterText, Me.lvClaimRequestList.Columns(8).HeaderText)
            Me.lvClaimRequestList.Columns(9).HeaderText = Language._Object.getCaption(Me.lvClaimRequestList.Columns(9).FooterText, Me.lvClaimRequestList.Columns(9).HeaderText)


            Language.setLanguage(mstrModuleName1)

            Me.lblpopupHeader.Text = Language._Object.getCaption(mstrModuleName1, Me.lblpopupHeader.Text)
            Me.lblTitle.Text = Language._Object.getCaption("gbExpenseInformation", Me.lblTitle.Text)
            Me.lblCancelDate.Text = Language._Object.getCaption("lblDate", Me.lblCancelDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblCancelEmployee.Text = Language._Object.getCaption("lblEmployee", Me.lblCancelEmployee.Text)
            Me.lblCancelExpCategory.Text = Language._Object.getCaption("lblExpCategory", Me.lblCancelExpCategory.Text)
            Me.lblName.Text = Language._Object.getCaption(Me.lblName.ID, Me.lblName.Text)
            Me.lblCancelRemark.Text = Language._Object.getCaption(Me.lblCancelRemark.ID, Me.lblCancelRemark.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.btnCancelSave.Text = Language._Object.getCaption("btnSave", Me.btnCancelSave.Text).Replace("&", "")
            Me.btnCancelClose.Text = Language._Object.getCaption("btnClose", Me.btnCancelClose.Text).Replace("&", "")

            Me.dgvData.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(1).FooterText, Me.dgvData.Columns(1).HeaderText)
            Me.dgvData.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
   
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName1, 1, "Cancel Remark cannot be blank.Cancel Remark is required information.")
			Language.setMessage(mstrModuleName1, 2, "Please Select atleast One Expense to cancel.")
			Language.setMessage(mstrModuleName1, 3, "You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period.")
			Language.setMessage(mstrModuleName1, 4, "Expense cancellation process completed successfully.")
            Language.setMessage(mstrModuleName1, 5, "Sorry, Expense cancellation process fail.")

            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Claim Request?")
            Language.setMessage(mstrModuleName, 5, "There is no Expense Data to Cancel the Expense Form.")
			Language.setMessage(mstrModuleName, 6, "Configuration Path does not Exist.")
			Language.setMessage(mstrModuleName, 7, "Sorry, you cannot cancel selected data. Reason : It is not in approved status.")
			Language.setMessage(mstrModuleName, 8, "Sorry, you cannot edit selected data. Reason:It is already approved.")
			Language.setMessage(mstrModuleName, 9, "Sorry, you cannot edit selected data. Reason:It is already approved by one or more approver(s).")
			Language.setMessage(mstrModuleName, 10, "Sorry, you cannot edit selected data. Reason:It is already rejected.")
			Language.setMessage(mstrModuleName, 11, "Sorry, you cannot edit selected data. Reason: It is already cancelled.")
			Language.setMessage(mstrModuleName, 12, "Sorry, you cannot delete selected data. Reason : It is already approved.")
			Language.setMessage(mstrModuleName, 13, "Sorry, you cannot delete selected data. Reason:It is already approved by one or more approver(s).")
			Language.setMessage(mstrModuleName, 14, "Sorry, you cannot delete selected data. Reason : It is already rejected.")
			Language.setMessage(mstrModuleName, 15, "Sorry, you cannot delete selected data. Reason : It is already cancelled.")
			Language.setMessage(mstrModuleName, 16, "Sorry, you cannot edit selected data. Reason:It is already approved or pending for approval in P2P system.")
			Language.setMessage(mstrModuleName, 17, "Sorry, you cannot delete selected data. Reason:It is already approved or pending for approval in P2P system.")
			Language.setMessage(mstrModuleName, 18, "Sorry, you cannot cancel selected data. Reason:It is already approved in P2P system.")
            Language.setMessage(mstrModuleName, 19, "Expense Category is compulsory information.Please Select Expense Category.")
            Language.setMessage(mstrModuleName, 20, "Status is compulsory information.Please Select Status.")
			

		Catch Ex As Exception
            'Sohail (18 Mar 2019) -- Start
            'TWAWEZA Enhancement - Ref # 0003506 - 74.1 - Integration with WCF online portal on WCF Form statutory report.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (18 Mar 2019) -- End
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class
