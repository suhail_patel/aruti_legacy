﻿'Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading
Imports ArutiReports

#End Region

Public Class Claims_And_Expenses_wPg_ExpenseApproval
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objClaimMaster As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Private objExpApproverTran As New clsclaim_request_approval_tran
    'Pinkal (05-Sep-2020) -- End

    Private Shared ReadOnly mstrModuleName As String = "frmExpenseApproval"
    Private objCONN As SqlConnection
    Dim mintClaimFormEmpId As Integer = 0
    Private mblnIsExternalApprover As Boolean = False


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsExpenseEditClick As Boolean = False
    Private mblnIsExpenseApproveClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpense As Boolean = False
    Dim mblnIsClaimFormBudgetMandatory As Boolean = False
    Dim mintBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End

    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestApprovalExpTranId As Integer = 0
    Dim mstrClaimRequestApprovalExpGUID As String = ""
    'Pinkal (07-Mar-2019) -- End

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    GC.Collect()
                    'Pinkal (05-Sep-2020) -- End

                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If


                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 7 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))

                        Me.ViewState.Add("ClaimFormEmpId", CInt(arr(2)))
                        Me.ViewState.Add("FormApproverId", CInt(arr(3)))
                        Me.ViewState.Add("crmasterunkid", CInt(arr(4)))
                        Me.ViewState.Add("crapprovaltranunkid", CInt(arr(5)))
                        Me.ViewState.Add("ExpenseCategoryID", CInt(arr(6)))
                        mintClaimFormEmpId = CInt(arr(2))

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")

                        'Pinkal (13-Aug-2020) -- Start
                        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                        gobjConfigOptions = New clsConfigOptions
                        'gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Pinkal (13-Aug-2020) -- End

                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString) 'Sohail (28 Oct 2013)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                        'Dim clsConfig As New clsConfigOptions
                        'clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        'Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        'Session("PaymentApprovalwithLeaveApproval") = clsConfig._PaymentApprovalwithLeaveApproval
                        'Session("fmtCurrency") = clsConfig._CurrencyFormat

                        'If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        '    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        'Else
                        '    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                        'End If

                        'Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim
                        Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                        Session("PaymentApprovalwithLeaveApproval") = ConfigParameter._Object._PaymentApprovalwithLeaveApproval
                        Session("fmtCurrency") = ConfigParameter._Object._CurrencyFormat

                        If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", ConfigParameter._Object._ArutiSelfServiceURL)
                        End If
                        Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim

                        'Pinkal (05-Sep-2020) -- End


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try

                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname").ToString())
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objUser = Nothing
                        'Pinkal (05-Sep-2020) -- End

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        gobjLocalization._LangId = CInt(HttpContext.Current.Session("LangId"))


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                        'Dim objUserPrivilege As New clsUserPrivilege
                        'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                        'Session("AllowtoProcessClaimExpenseForm") = objUserPrivilege._AllowtoProcessClaimExpenseForm
                        'Session("AllowtoViewProcessClaimExpenseFormList") = objUserPrivilege._AllowtoViewProcessClaimExpenseFormList

                        Dim objExpApproverTran As New clsclaim_request_approval_tran
                        'Pinkal (05-Sep-2020) -- End



                        Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                            , Session("Database_Name").ToString(), CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString() _
                                                                                                                            , CInt(Me.ViewState("ExpenseCategoryID")), False, True, -1 _
                                                                                                                            , "crapprovaltranunkid = " & CInt(ViewState("crapprovaltranunkid")), CInt(Me.ViewState("crmasterunkid")), Nothing)
                        If dsList.Tables("List").Rows.Count > 0 Then

                            If CInt(dsList.Tables("List").Rows(0)("visibleid")) = 1 Then
                                Session.Abandon()
                                DisplayMessage.DisplayMessage("You cannot Edit this claim form detail. Reason: This Claim form  was already approved.", Me, "../Index.aspx")
                                'Pinkal (05-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                If dsList IsNot Nothing Then dsList.Clear()
                                dsList = Nothing
                                objExpApproverTran = Nothing
                                'Pinkal (05-Sep-2020) -- End
                                Exit Sub
                            End If
                            Session("crmasterunkid") = CInt(dsList.Tables("List").Rows(0)("crmasterunkid"))
                            Session("approverunkid") = CInt(dsList.Tables("List").Rows(0)("crapproverunkid"))
                            Session("approverEmpID") = CInt(dsList.Tables("List").Rows(0)("approveremployeeunkid"))
                            Session("Priority") = CInt(dsList.Tables("List").Rows(0)("crpriority"))
                            Session("isexternalapprover") = CBool(dsList.Tables("List").Rows(0)("isexternalapprover"))
                        End If


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objExpApproverTran = Nothing
                        'Pinkal (05-Sep-2020) -- End


                    End If
                Else
                    Exit Sub
                End If
            End If

            If IsPostBack = False Then
                'Pinkal (16-Jul-2020) -- Start
                'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            SetLanguage()
                'Pinkal (16-Jul-2020) -- End

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End


                'Pinkal (20-Nov-2020) -- Start
                'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
                If Session("CompName").ToString().ToUpper() = "KADCO" Then
                    lnkShowFuelConsumptionReport.Visible = True
                Else
                    lnkShowFuelConsumptionReport.Visible = False
                End If
                'Pinkal (20-Nov-2020) -- End


                If Session("crmasterunkid") IsNot Nothing AndAlso Session("approverunkid") IsNot Nothing AndAlso Session("approverEmpID") IsNot Nothing AndAlso Session("Priority") IsNot Nothing Then
                    Me.ViewState.Add("mintClaimRequestMasterId", Session("crmasterunkid"))
                    Me.ViewState.Add("mintClaimApproverId", Session("approverunkid"))
                    Me.ViewState.Add("mintClaimApproverEmpID", Session("approverEmpID"))
                    Me.ViewState.Add("mintApproverPriority", Session("Priority"))
                    If Session("blnIsFromLeave") IsNot Nothing Then
                        Me.ViewState.Add("mblnIsFromLeave", Session("blnIsFromLeave"))
                    Else
                        Me.ViewState.Add("mblnIsFromLeave", False)
                    End If
                    Dim mdtTran As DataTable = Nothing
                    Me.ViewState.Add("mdtTran", mdtTran)
                    Me.ViewState.Add("MaxDate", CDate(eZeeDate.convertDate("99981231")))
                    Me.ViewState.Add("MinDate", CDate(eZeeDate.convertDate("17530101")))
                    Me.ViewState.Add("PDate", ConfigParameter._Object._CurrentDateAndTime.Date)
                    Me.ViewState.Add("mblnIsLeaveEncashment", False)
                    Me.ViewState.Add("EditGuid", "Null")
                    Me.ViewState.Add("EditCrtranunkid", 0)

                    SetVisibility()
                    txtUnitPrice.Enabled = False
                    cboExpCategory.Enabled = False
                    cboPeriod.Enabled = False
                    dtpDate.Enabled = False


                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    Dim objClaimMaster As New clsclaim_request_master
                    Dim objClaimTran As New clsclaim_request_tran
                    Dim objExpApproverTran As New clsclaim_request_approval_tran

                    objClaimMaster._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
                    FillCombo(objClaimMaster._Employeeunkid)

                    If CBool(Me.ViewState("mblnIsFromLeave")) Then
                        Enable_Disable_Ctrls(False)
                    Else
                        Enable_Disable_Ctrls(True)
                    End If

                    'Call GetValue()
                    Call GetValue(objClaimMaster)

                    objClaimTran._ClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))

                    mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
                                                                                                    , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpCategory.SelectedValue) _
                                                                                                    , True, True, CInt(Me.ViewState("mintClaimApproverId")), "", CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing).Tables(0)

                    cboLeaveType.Enabled = False
                    mblnIsExternalApprover = CBool(Session("IsExternalApprover"))

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Me.ViewState("mdtTran") = mdtTran
                    Me.ViewState("mdtTran") = mdtTran.Copy()
                    'Pinkal (05-Sep-2020) -- End

                    Call Fill_Expense()

                    If mdtTran IsNot Nothing Then mdtTran.Clear()
                    mdtTran = Nothing

                    objExpApproverTran = Nothing
                    objClaimTran = Nothing
                    objClaimMaster = Nothing
                    'Pinkal (05-Sep-2020) -- End

                Else
                    Response.Redirect("~/Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
                End If
            Else
                mintClaimFormEmpId = CInt(Me.ViewState("ClaimFormEmpId"))
                mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))
                mblnIsExpenseEditClick = CBool(Me.ViewState("mblnIsExpenseEditClick"))
                mblnIsExpenseApproveClick = CBool(Me.ViewState("mblnIsExpenseApproveClick"))
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
                mblnIsClaimFormBudgetMandatory = CBool(Me.ViewState("IsClaimFormBudgetMandatory"))
                mblnIsHRExpense = CBool(Me.ViewState("IsHRExpense"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mintClaimRequestApprovalExpTranId = CInt(Me.ViewState("ClaimRequestApprovalExpTranId"))
                mstrClaimRequestApprovalExpGUID = Me.ViewState("ClaimRequestApprovalExpGUID").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'Pinkal (13-Aug-2020) -- Start
        'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Try
            Session.Remove("crmasterunkid")
            Session.Remove("approverunkid")
            Session.Remove("approverEmpID")
            Session.Remove("Priority")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("ClaimFormEmpId", mintClaimFormEmpId)

            If Session("IsExternalApprover") IsNot Nothing Then
                mblnIsExternalApprover = CBool(Session("IsExternalApprover"))
                Session("IsExternalApprover") = Nothing
            End If
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover
            Me.ViewState("mblnIsExpenseEditClick") = mblnIsExpenseEditClick
            Me.ViewState("mblnIsExpenseApproveClick") = mblnIsExpenseApproveClick
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
            Me.ViewState("IsClaimFormBudgetMandatory") = mblnIsClaimFormBudgetMandatory
            Me.ViewState("IsHRExpense") = mblnIsHRExpense
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("ClaimRequestApprovalExpTranId") = mintClaimRequestApprovalExpTranId
            Me.ViewState("ClaimRequestApprovalExpGUID") = mstrClaimRequestApprovalExpGUID
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub FillCombo()
    Private Sub FillCombo(ByVal xEmployeeId As Integer)
        'Pinkal (05-Sep-2020) -- End
        Dim objPrd As New clscommom_period_Tran
        Dim objEMaster As New clsEmployee_Master
        Dim dsCombo As New DataSet
        Try


            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'dsCombo = objPrd.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_Year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "List", True, 1)

            'With cboPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombo.Tables(0)
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
            'Pinkal (16-Jul-2020) -- End


            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())


            If CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objcommonMst As New clsCommon_Master
                dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
                With cboSectorRoute
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

            'dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
            '                             CInt(Session("UserId")), _
            '                             CInt(Session("Fin_year")), _
            '                             CInt(Session("CompanyUnkId")), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                             Session("UserAccessModeSetting").ToString(), True, _
            '                       True, "List", True, objClaimMaster._Employeeunkid, , , , , , , , , , , , , , , , False)

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                     True, "List", True, xEmployeeId, , , , , , , , , , , , , , , , False)


            'Pinkal (05-Sep-2020) -- End






            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                '.SelectedValue = objClaimMaster._Employeeunkid.ToString()
                .SelectedValue = xEmployeeId.ToString()
                'Pinkal (05-Sep-2020) -- End
            End With

            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtTable = dsCombo.Tables(0)
                Else
                    dtTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtTable
                .DataBind()
            End With
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objCostCenter = Nothing
            'Pinkal (05-Sep-2020) -- End


            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()

                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (05-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo:-" & ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            objEMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub Fill_Expense()
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If

            Dim mdView As DataView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D' AND iscancel = 0 "
            dgvData.DataSource = mdView
            dgvData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D' AND iscancel = 0")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0 ")), Session("fmtCurrency").ToString())
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If

            If dgvData.Items.Count <= 0 Then
                dtpDate.Enabled = True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Expense:-" & ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Sub GetValue()
    Private Sub GetValue(ByRef objClaimMaster As clsclaim_request_master)
        'Pinkal (05-Sep-2020) -- End
        Try
            'Pinkal (16-Jul-2020) -- Start
            'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report
            'Dim objEmployee As New clsEmployee_Master
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objClaimMaster._Employeeunkid
            'cboEmployee.SelectedValue = objClaimMaster._Employeeunkid.ToString()
            'Pinkal (16-Jul-2020) -- End

            If Me.ViewState("EmployeeId") Is Nothing Then
                Me.ViewState.Add("EmployeeId", objClaimMaster._Employeeunkid)
            Else
                Me.ViewState("EmployeeId") = objClaimMaster._Employeeunkid
            End If

            txtClaimNo.Text = objClaimMaster._Claimrequestno

            If CBool(Session("SectorRouteAssignToEmp")) Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboEmployee.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtTable
                    .DataBind()
                    .SelectedIndex = 0
                End With
                objAssignEmp = Nothing
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtTable IsNot Nothing Then dtTable.Clear()
                dtTable = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If

            dtpDate.SetDate = objClaimMaster._Transactiondate.Date
            Call dtpDate_TextChanged(dtpDate, Nothing)

            cboExpCategory.SelectedValue = objClaimMaster._Expensetypeid.ToString()
            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())
            txtClaimRemark.Text = objClaimMaster._Claim_Remark
            cboLeaveType.SelectedValue = objClaimMaster._LeaveTypeId.ToString()

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                Call cboLeaveType_SelectedIndexChanged(Nothing, Nothing)
            End If
            cboReference.SelectedValue = objClaimMaster._Referenceunkid.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError("GetValue:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            End If
            btnApprove.Visible = Not CBool(Me.ViewState("mblnIsFromLeave"))
            btnReject.Visible = Not CBool(Me.ViewState("mblnIsFromLeave"))
            btnOK.Visible = CBool(Me.ViewState("mblnIsFromLeave"))

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("SetVisibility:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            cboReference.Enabled = iFlag
            cboLeaveType.Enabled = iFlag

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If
            'Pinkal (05-Sep-2020) -- End

            If mdtTran IsNot Nothing Then
                If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboCurrency.Enabled = False
                    cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First().ToString()
                Else
                    cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                    cboCurrency.Enabled = True
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Enable_Disable_Controls:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = "0"
            cboSectorRoute.SelectedValue = "0"
            Call cboExpense_SelectedIndexChanged(cboExpense, Nothing)
            txtExpRemark.Text = ""

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtQty.Text = "0"
            txtQty.Text = "1"
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Text = "0"
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            mintEmpMaxCountDependentsForCR = 0
            mintClaimRequestApprovalExpTranId = 0
            mstrClaimRequestApprovalExpGUID = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If dgvData.Items.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please add atleast one expense in order to do futher operation."), Me)
                dgvData.Focus()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("Is_Valid:-" & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Function SaveData(ByVal sender As Object) As Boolean
        Dim blnFlag As Boolean = False
        Dim blnLastApprover As Boolean = False
        Dim mstrRejectRemark As String = ""
        Dim mintMaxPriority As Integer = -1
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
        Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
        Dim objExpApproverTran As New clsclaim_request_approval_tran
        Dim objClaimMst As New clsclaim_request_master
        'Pinkal (05-Sep-2020) -- End

        Try
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If

            Dim mintStatusID As Integer = 2 'PENDING
            Dim mintVisibleID As Integer = 2 'PENDING
            Dim mdtApprovalDate As DateTime = Nothing

            objExpApproverTran._DataTable = mdtTran

            Dim drRow() As DataRow = mdtTran.Select("AUD = ''")

            If drRow.Length > 0 Then
                For Each dr As DataRow In drRow
                    dr("AUD") = "U"
                    dr.AcceptChanges()
                Next
            End If

            If mdtTran.Columns.Contains("crapprovaltranunkid") Then
                mdtTran.Columns.Remove("crapprovaltranunkid")
            End If

            If CBool(Me.ViewState("mblnIsFromLeave")) Then Return True

            Dim mblnIssued As Boolean = False
            Dim objLeaveApprover As clsleaveapprover_master = Nothing
            Dim objExpAppr As New clsExpenseApprover_Master

            Dim dsList As DataSet = objExpApproverTran.GetApproverExpesneList("List", True, CBool(Session("PaymentApprovalwithLeaveApproval")) _
                                                                                                                  , Session("Database_Name").ToString(), CInt(Session("UserId")) _
                                                                                                                  , Session("EmployeeAsOnDate").ToString(), CInt(cboExpCategory.SelectedValue) _
                                                                                                                  , False, True, -1, "", CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing)



            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & CInt(Me.ViewState("mintApproverPriority")), "", DataViewRowState.CurrentRows).ToTable()
            Dim dtApproverTable As DataTable = New DataView(dsList.Tables(0), "crpriority >= " & CInt(Me.ViewState("mintApproverPriority")), "", DataViewRowState.CurrentRows).ToTable().Copy()

            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End


            mintMaxPriority = CInt(dtApproverTable.Compute("Max(crpriority)", "1=1"))

            objClaimMst._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
            objExpApproverTran._FormName = "frmExpenseApproval"
            objExpApproverTran._ClientIP = Session("IP_ADD").ToString()
            objExpApproverTran._HostName = Session("HOST_NAME").ToString()


 'Pinkal (22-Oct-2021)-- Start
            'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
            objClaimMst._FormName = "frmExpenseApproval"
            objClaimMst._ClientIP = Session("IP_ADD").ToString()
            objClaimMst._HostName = Session("HOST_NAME").ToString()
            'Pinkal (22-Oct-2021)-- End


            If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                objLeaveApprover = New clsleaveapprover_master
                objLeaveApprover._Approverunkid = CInt(Me.ViewState("mintClaimApproverId"))

                Dim objLeaveForm As New clsleaveform
                objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                If (objLeaveForm._Statusunkid = 7 OrElse objLeaveForm._Formunkid <= 0 OrElse mintMaxPriority = CInt(Me.ViewState("mintApproverPriority"))) Then mblnIssued = True

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objLeaveForm = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If

            'Pinkal (22-Jun-2015) -- Start
            'Enhancement - CHANGES IN CLAIM & REQUEST AS PER MR.ANDREW REQUIREMENT.

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmExpenseApproval"
            'StrModuleName2 = "mnuUtilitiesMain"
            'StrModuleName3 = "mnuClaimsExpenses"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (22-Jun-2015) -- End

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            Dim mintApproverApplicationStatusId As Integer = 2
            Dim mblnIsRejected As Boolean = False

            For i As Integer = 0 To dtApproverTable.Rows.Count - 1
                blnLastApprover = False
                Dim mintApproverEmpID As Integer = -1
                Dim mintApproverID As Integer = -1

                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then
                    objLeaveApprover._Approverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))
                    If CInt(Me.ViewState("mintClaimApproverId")) = objLeaveApprover._Approverunkid Then

                        If CType(sender, Button).ID.ToUpper = "BTNAPPROVE" Then
                            mintStatusID = 1
                            mintVisibleID = 1
                            mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) AndAlso mblnIssued Then blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID

                        ElseIf CType(sender, Button).ID.ToUpper = "BTNREMARKOK" Then
                            mintStatusID = 3
                            mintVisibleID = 3
                            mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                            blnLastApprover = True
                            mblnIsRejected = True
                            mintApproverApplicationStatusId = mintStatusID
                        End If

                    Else

                        Dim mintPriority As Integer = -1

                        Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & CInt(Me.ViewState("mintApproverPriority")) & " AND crapproverunkid = " & objLeaveApprover._Approverunkid)

                        If dRow.Length > 0 Then
                            mintStatusID = 2
                            mintVisibleID = 1
                            'Pinkal (13-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            mdtApprovalDate = Nothing
                            'Pinkal (13-Aug-2020) -- End
                        Else
                            mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))))
                            If mintPriority <= -1 Then
                                GoTo AssignApprover
                                mintStatusID = 2
                                mintVisibleID = 1
                                GoTo AssignApprover
                            ElseIf mblnIsRejected Then
                                mintVisibleID = -1
                            ElseIf CInt(Me.ViewState("mintApproverPriority")) = CInt(dtApproverTable.Rows(i)("crpriority")) Then 'Pinkal (13-Jul-2015) -- End
                                mintVisibleID = 1
                            ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                                mintVisibleID = 2
                            Else
                                mintVisibleID = -1
                            End If
                            mintStatusID = 2
                            mdtApprovalDate = Nothing
                        End If

                    End If
AssignApprover:
                    mintApproverEmpID = objLeaveApprover._leaveapproverunkid
                    mintApproverID = objLeaveApprover._Approverunkid


                Else    'If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso objClaimMst._Modulerefunkid = enModuleReference.Leave Then

                    objExpAppr._crApproverunkid = CInt(dtApproverTable.Rows(i)("crapproverunkid"))

                    If CInt(Me.ViewState("mintClaimApproverId")) = objExpAppr._crApproverunkid Then

                        If CType(sender, Button).ID.ToUpper = btnApprove.ID.ToUpper Then
                            mintStatusID = 1
                            mintVisibleID = 1
                            mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                            If mintMaxPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then blnLastApprover = True
                            mintApproverApplicationStatusId = mintStatusID

                        ElseIf CType(sender, Button).ID.ToUpper = btnRemarkOk.ID.ToUpper Then
                            mintStatusID = 3
                            mintVisibleID = 3
                            mdtApprovalDate = ConfigParameter._Object._CurrentDateAndTime
                            blnLastApprover = True
                            mblnIsRejected = True
                            mintApproverApplicationStatusId = mintStatusID
                        End If

                    Else
                        Dim mintPriority As Integer = -1

                        Dim dRow() As DataRow = dtApproverTable.Select("crpriority = " & CInt(Me.ViewState("mintApproverPriority")) & " AND crapproverunkid = " & objExpAppr._crApproverunkid)

                        If dRow.Length > 0 Then
                            mintStatusID = 2
                            mintVisibleID = 1
                            'Pinkal (13-Aug-2020) -- Start
                            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                            mdtApprovalDate = Nothing
                            'Pinkal (13-Aug-2020) -- End
                        Else
                            mintPriority = CInt(IIf(IsDBNull(dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))), -1, dtApproverTable.Compute("MIN(crpriority)", "crpriority >" & CInt(Me.ViewState("mintApproverPriority")))))

                            If mintPriority <= -1 Then
                                GoTo AssignApprover1
                                mintStatusID = 2
                                mintVisibleID = 1
                                GoTo AssignApprover
                            ElseIf mblnIsRejected Then
                                mintVisibleID = -1
                            ElseIf CInt(Me.ViewState("mintApproverPriority")) = CInt(dtApproverTable.Rows(i)("crpriority")) Then 'Pinkal (13-Jul-2015) -- End
                                mintVisibleID = 1
                            ElseIf mintPriority = CInt(dtApproverTable.Rows(i)("crpriority")) Then
                                mintVisibleID = 2
                            Else
                                mintVisibleID = -1
                            End If
                            mintStatusID = 2
                            mdtApprovalDate = Nothing
                        End If

                    End If
AssignApprover1:
                    mintApproverEmpID = objExpAppr._Employeeunkid
                    mintApproverID = objExpAppr._crApproverunkid
                End If

                objExpApproverTran._YearId = CInt(Session("Fin_Year"))
                objExpApproverTran._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                objExpApproverTran._EmployeeID = CInt(Me.ViewState("EmployeeId"))

                If blnLastApprover AndAlso mintStatusID = 1 Then
                    If mdtTran IsNot Nothing Then
                        For Each dRow As DataRow In mdtTran.Rows
                            Dim sMsg As String = String.Empty


                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            'sMsg = objClaimMaster.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                            '                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                            '                                                            , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), False)
                            sMsg = objClaimMst.IsValid_Expense(objClaimMst._Employeeunkid, CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                        , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), False)
                            'Pinkal (05-Sep-2020) -- End

                            If sMsg <> "" Then
                                DisplayMessage.DisplayMessage(sMsg, Me)
                                Return False
                            End If
                            sMsg = ""
                        Next
                    End If
                End If

                With objExpApproverTran
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID, Session("UserId"), Me.ViewState("mintClaimRequestMasterId"), Nothing, mstrRejectRemark, blnLastApprover, mdtApprovalDate) = False Then
                If objExpApproverTran.Insert_Update_ApproverData(mintApproverEmpID, mintApproverID, mintStatusID, mintVisibleID _
                                                                                        , CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime _
                                                                                        , CInt(Me.ViewState("mintClaimRequestMasterId")), Nothing, mstrRejectRemark _
                                                                                        , blnLastApprover, mdtApprovalDate) = False Then
                    Return False
                End If
            Next

            If mintApproverApplicationStatusId = 1 Then
'Pinkal (22-Oct-2021)-- Start
                'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                objExpApproverTran._FormName = "frmExpenseApproval"
                'Pinkal (22-Oct-2021)-- End
                objExpApproverTran.SendMailToApprover(CInt(cboExpCategory.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(Me.ViewState("mintClaimRequestMasterId")) _
                                                                            , txtClaimNo.Text.Trim, CInt(Me.ViewState("EmployeeId")), CInt(Me.ViewState("mintApproverPriority")), 1, "crpriority > " & CInt(Me.ViewState("mintApproverPriority")) _
                                                                            , Session("Database_Name").ToString(), Session("EmployeeAsOnDate").ToString(), CInt(Session("CompanyUnkId")) _
                                                                            , Session("ArutiSelfServiceURL").ToString, enLogin_Mode.MGR_SELF_SERVICE, , CInt(Session("UserId")), mstrModuleName)
            End If

            objClaimMst._Crmasterunkid = CInt(Me.ViewState("mintClaimRequestMasterId"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString
            If objClaimMst._Statusunkid = 1 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objClaimMst._Employeeunkid

                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email

                If objClaimMst._Modulerefunkid = enModuleReference.Leave AndAlso objClaimMst._Expensetypeid = enExpenseType.EXP_LEAVE AndAlso objClaimMst._Referenceunkid > 0 Then
                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objClaimMst._Referenceunkid
                    objLeaveForm._EmployeeCode = objEmployee._Employeecode
                    objLeaveForm._EmployeeFirstName = objEmployee._Firstname
                    objLeaveForm._EmployeeMiddleName = objEmployee._Othername
                    objLeaveForm._EmployeeSurName = objEmployee._Surname
                    objLeaveForm._EmpMail = objEmployee._Email
                    objLeaveForm._WebFrmName = "frmExpenseApproval"

                    If objLeaveForm._Statusunkid = 7 Then 'only Issued
                        Dim objCommonCode As New CommonCodes
                        Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
                        Dim mstrPath As String = objCommonCode.Export_ELeaveForm(Path, objLeaveForm._Formno, objLeaveForm._Employeeunkid, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                                     , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, True _
                                                                                                                     , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                        'Pinkal (16-Dec-2016) -- End


                        'Pinkal (27-Apr-2019) -- Start
                        'Enhancement - Audit Trail changes.
                        With objLeaveForm
                            ._FormName = mstrModuleName
                            If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                            Else
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                            End If
                            ._ClientIP = Session("IP_ADD").ToString()
                            ._HostName = Session("HOST_NAME").ToString()
                            ._FromWeb = True
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        End With
                        'Pinkal (27-Apr-2019) -- End


                        objLeaveForm.SendMailToEmployee(objClaimMst._Employeeunkid, cboLeaveType.Text, objLeaveForm._Startdate, objLeaveForm._Returndate, objLeaveForm._Statusunkid _
                                                                            , CInt(Session("CompanyUnkId")), "", Path, mstrPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "" _
                                                                            , CInt(Me.ViewState("mintClaimRequestMasterId")), objLeaveForm._Formno)


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objCommonCode = Nothing
                        'Pinkal (05-Sep-2020) -- End

                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objLeaveForm = Nothing
                    'Pinkal (05-Sep-2020) -- End
                Else
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "")
                    With objClaimMst
                        ._FormName = mstrModuleName
                        If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                        Else
                            ._AuditUserId = Convert.ToInt32(Session("UserId"))
                        End If
                        ._ClientIP = Session("IP_ADD").ToString()
                        ._HostName = Session("HOST_NAME").ToString()
                        ._FromWeb = True
                        ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                    End With
                    objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), "")
                End If
                objEmployee = Nothing

            ElseIf objClaimMst._Statusunkid = 3 Then

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objClaimMst._Employeeunkid
                objClaimMst._EmployeeCode = objEmployee._Employeecode
                objClaimMst._EmployeeFirstName = objEmployee._Firstname
                objClaimMst._EmployeeMiddleName = objEmployee._Othername
                objClaimMst._EmployeeSurName = objEmployee._Surname
                objClaimMst._EmpMail = objEmployee._Email
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRejectRemark.Text.Trim)
                With objClaimMst
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With
                objClaimMst.SendMailToEmployee(CInt(Me.ViewState("EmployeeId")), objClaimMst._Claimrequestno, objClaimMst._Statusunkid, CInt(Session("CompanyUnkId")), "", "", enLogin_Mode.MGR_SELF_SERVICE, -1, CInt(Session("UserId")), txtRejectRemark.Text.Trim)
                objEmployee = Nothing
            End If

            'Pinkal (22-Oct-2021)-- Start
            'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
            If gobjEmailList.Count > 0 Then
                Dim objThread As Threading.Thread
                If HttpContext.Current Is Nothing Then
                    objThread = New Threading.Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    Dim arr(1) As Object
                    arr(0) = CInt(Session("CompanyUnkId"))
                    objThread.Start(arr)
                Else
                    Call Send_Notification(CInt(Session("CompanyUnkId")))
                End If
            End If
            'Pinkal (22-Oct-2021) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtApproverTable IsNot Nothing Then dtApproverTable.Clear()
            dtApproverTable = Nothing
            objLeaveApprover = Nothing
            objExpAppr = Nothing
            'Pinkal (05-Sep-2020) -- End

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            Return False
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMst = Nothing
            objExpApproverTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Function

    Private Sub AddExpense()
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End

            Dim dtmp() As DataRow = Nothing
            If mdtTran.Rows.Count > 0 Then
                dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                            "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below."), Me)
                    Exit Sub
                End If
            End If

            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)


            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End

            mblnIsHRExpense = objExpMaster._IsHRExpense

            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory
            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMaster = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    End If
                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtQty.Focus()
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMaster = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                End If
            End If


            Dim sMsg As String = String.Empty

            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                               , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")))

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimMaster As New clsclaim_request_master
            'Pinkal (05-Sep-2020) -- End

            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), True)

            'Pinkal (22-Jan-2020) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If
            sMsg = ""

            If CheckForOccurrence(mdtTran) = False Then Exit Sub

            If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(IIf(txtQty.Text.Trim.Trim.Length > 0, CDec(txtQty.Text), 0)) * CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0))) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), Me)
                    Exit Sub
                End If
            End If

            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing

                If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMasterOld = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub

                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMasterOld = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
                objExpMasterOld = Nothing
            End If

            objExpMaster = Nothing

            Dim dRow As DataRow = mdtTran.NewRow
            dRow.Item("crapprovaltranunkid") = -1
            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = Me.ViewState("mintClaimRequestMasterId")
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
            dRow.Item("unitprice") = txtUnitPrice.Text
            dRow.Item("quantity") = txtQty.Text
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("iscancel") = False
            dRow.Item("canceluserunkid") = -1
            dRow.Item("cancel_datetime") = DBNull.Value
            dRow.Item("cancel_remark") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
            dRow.Item("ishrexpense") = mblnIsHRExpense
            dRow.Item("countryunkid") = CInt(cboCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate

            If mdtTran.Columns.Contains("tdate") Then
                dRow.Item("tdate") = eZeeDate.convertDate(dtpDate.GetDate.Date).ToString()
            End If
            mdtTran.Rows.Add(dRow)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdtTran
            Me.ViewState("mdtTran") = mdtTran.Copy()
            'Pinkal (05-Sep-2020) -- End

            Fill_Expense()
            Enable_Disable_Ctrls(False)
            Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub EditExpense()
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            Dim iRow As DataRow() = Nothing

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            'Pinkal (05-Sep-2020) -- End

            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            Dim mintGLCodeID As Integer = 0
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = ""
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""

            mblnIsHRExpense = objExpMaster._IsHRExpense
            mblnIsClaimFormBudgetMandatory = objExpMaster._IsBudgetMandatory

            mintGLCodeID = objExpMaster._GLCodeId
            mstrGLDescription = objExpMaster._Description
            If mblnIsClaimFormBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    cboCostCenter.Focus()
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMaster = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim, mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) * CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0))) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMaster = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
            End If


            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If CDec(txtQty.Text) > CDec(txtBalance.Text) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtQty.Focus()
                            'Pinkal (05-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objExpMaster = Nothing
                            'Pinkal (05-Sep-2020) -- End
                            Exit Sub
                        End If
                    End If

                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(txtBalance.Text) < CDec(CDec(txtQty.Text) * CDec(txtUnitPrice.Text)) Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtQty.Focus()
                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpMaster = Nothing
                        'Pinkal (05-Sep-2020) -- End
                        Exit Sub
                    End If
                End If
            End If


            Dim sMsg As String = String.Empty

            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                               , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")))


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objClaimMaster As New clsclaim_request_master
            'Pinkal (05-Sep-2020) -- End

            sMsg = objClaimMaster.IsValid_Expense(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                      , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), CInt(Me.ViewState("mintClaimRequestMasterId")), False)
            'Pinkal (22-Jan-2020) -- End

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If
            sMsg = ""

            If CheckForOccurrence(mdtTran) = False Then Exit Sub

            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMasterOld = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub

                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objExpMasterOld = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
                objExpMasterOld = Nothing
            End If
            objExpMaster = Nothing


            If CInt(Me.ViewState("EditCrtranunkid")) <> 0 Then
                iRow = mdtTran.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
            Else
                iRow = mdtTran.Select("GUID = '" & CStr(Me.ViewState("EditGuid")) & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing AndAlso iRow.Length > 0 Then
                Dim dtmp() As DataRow = Nothing
                If iRow IsNot Nothing Then
                    If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                        dtmp = mdtTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                             "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                    Else
                        dtmp = mdtTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtQty.Text.Trim.Length > 0, CDec(txtQty.Text), 0)) & _
                                                          "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                    End If

                    If dtmp.Length > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below."), Me)
                        Exit Sub
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    dtmp = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If

                iRow(0).Item("crapprovaltranunkid") = iRow(0).Item("crapprovaltranunkid")
                iRow(0).Item("crmasterunkid") = Me.ViewState("mintClaimRequestMasterId")
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("quantity") = txtQty.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtQty.Text)
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("iscancel") = False
                iRow(0).Item("canceluserunkid") = -1
                iRow(0).Item("cancel_datetime") = DBNull.Value
                iRow(0).Item("cancel_remark") = ""
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)

                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatory
                iRow(0).Item("ishrexpense") = mblnIsHRExpense

                iRow(0).Item("countryunkid") = CInt(cboCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboCurrency.SelectedValue), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate


                mdtTran.AcceptChanges()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Me.ViewState("mdtTran") = mdtTran
                Me.ViewState("mdtTran") = mdtTran.Copy()
                'Pinkal (05-Sep-2020) -- End

                Fill_Expense()
                Clear_Controls()
                btnEdit.Visible = False
                btnAdd.Visible = True
                cboExpense.Enabled = True

                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub ApproveExpense()
        Try
            If SaveData(btnApprove) Then
                If mintClaimFormEmpId > 0 Then
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True _
                                            , Session("mdbname").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")) _
                                            , Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    Session.Abandon()
                    Response.Redirect("../Index.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("ApproveExpense : " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Function GetEmployeeDepedentCountForCR() As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue))
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(CInt(cboEmployee.SelectedValue), dtpDate.GetDate.Date)
            'Sohail (18 May 2019) -- End
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtDependents IsNot Nothing Then dtDependents.Clear()
            dtDependents = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmployeeDepedentCountForCR : " & ex.Message, Me)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    'Pinkal (25-Oct-2018) -- End  


    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try

            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    mintEmpCostCenterId = GetEmployeeCostCenter()
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("GetP2PRequireData : " & ex.Message, Me)
        End Try
    End Sub

    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xCostCenterCode As String, ByVal xGLCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), Me)
                Return False
            ElseIf xCostCenterCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense."), Me)
                Return False
            End If

            If xCostCenterCode.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then
                Dim mstrBgtRequestValidationP2PServiceURL As String = Session("BgtRequestValidationP2PServiceURL").ToString().Trim() & "?costCenter=" & xCostCenterCode.Trim() & "&glcode=" & xGLCode.Trim()
                Dim mstrGetData As String = ""
                Dim objMstData As New clsMasterData
                If objMstData.GetSetP2PWebRequest(mstrBgtRequestValidationP2PServiceURL, True, True, "", mstrGetData, mstrError) = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objMstData = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Return False
                End If

                objMstData = Nothing
                'mstrGetData = "{""amount"":""10000""}"

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtAmount As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtAmount IsNot Nothing AndAlso dtAmount.Rows.Count > 0 Then
                        mdecBudgetAmount = CDec(dtAmount.Rows(0)("amount"))
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtAmount IsNot Nothing Then dtAmount.Clear()
                    dtAmount = Nothing
                    'Pinkal (05-Sep-2020) -- End
                End If

            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("CheckBudgetRequestValidationForP2P : " & ex.Message, Me)
            Return False
        End Try
    End Function

    Private Function GetEmployeeCostCenter() As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtpDate.GetDate.Date, True, CInt(cboEmployee.SelectedValue))
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmployeeCostCenter : " & ex.Message, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)) * CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0))) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End
            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("GetCurrencyRate : " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (04-Feb-2019) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Function CheckForOccurrence(ByVal mdtTran As DataTable) As Boolean
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExpBalance As New clsEmployeeExpenseBalance
        Dim dsBalance As DataSet = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If mdtTran IsNot Nothing Then

                dsBalance = objExpBalance.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")), dtpDate.GetDate.Date _
                                                                                               , CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)), CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)))

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestApprovalExpTranId > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestApprovalExpTranId And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestApprovalExpGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestApprovalExpGUID.Trim() And x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = CInt(cboExpense.SelectedValue) And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            DisplayMessage.DisplayMessage(Language.getMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Language.getMessage("clsclaim_request_master", 4, " ] time(s)."), Me)
                            Return False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("CheckForOccurrence : " & ex.Message, Me)
            Return False
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsBalance IsNot Nothing Then dsBalance.Clear()
            dsBalance = Nothing
            objExpBalance = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
        Return True
    End Function

    'Pinkal (07-Mar-2019) -- End


    'Pinkal (22-Oct-2021)-- Start
    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.

    Private Sub Send_Notification(ByVal intCompanyID As Object)
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
SendEmail:
                For Each objEmail In gobjEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._FormName
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If

                    'Pinkal (22-Oct-2021)-- Start
                    'Claim Request Email Notification Issue due to NMB Global Approval email sending changes.
                    objSendMail._Form_Name = "frmExpenseApproval"
                    objSendMail._ClientIP = Session("IP_ADD").ToString()
                    objSendMail._HostName = Session("HOST_NAME").ToString()
                    'Pinkal (22-Oct-2021)-- End

                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = CInt(intCompanyID)
                    Else
                        intCUnkId = intCompanyID(0)
                    End If
                    If objSendMail.SendMail(intCUnkId, CBool(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False)), _
                                                objEmail._ExportReportPath).ToString.Length > 0 Then
                        gobjEmailList.Remove(objEmail)
                        GoTo SendEmail
                    End If
                Next
                gobjEmailList.Clear()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2021)-- End


#End Region

#Region "Button Event"

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExpense As New clsExpense_Master
        Dim gRow As IEnumerable(Of DataGridItem) = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try

            If CInt(cboExpense.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboExpense.Focus()
                Exit Sub
            End If

            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                cboSectorRoute.Focus()
                Exit Sub
            End If

            If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Exit Sub
            End If
            'Pinkal (10-Jun-2020) -- End

            objExpense = Nothing

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboCurrency.Focus()
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End


            mblnIsExpenseEditClick = False
            mblnIsExpenseApproveClick = False

            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 25, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            objExpense = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExpense As New clsExpense_Master
        Dim gRow As IEnumerable(Of DataGridItem) = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try

            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                cboSectorRoute.Focus()
                Exit Sub
            End If

            If txtQty.Text.Trim() = "" OrElse CDec(txtQty.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtQty.Focus()
                Exit Sub
            End If


            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(IIf(txtQty.Text.Trim.Length > 0, txtQty.Text, 0)) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtQty.Focus()
                Exit Sub
            End If

            'Pinkal (04-Aug-2020) -- Start
            'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
            'objExpense = Nothing
            'Pinkal (04-Aug-2020) -- End


            'Pinkal (04-Feb-2019) -- End

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtQty.Text) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtQty.Focus()
                Exit Sub
            End If
            'Pinkal (10-Jun-2020) -- End


            objExpense = Nothing


            mblnIsExpenseEditClick = True
            mblnIsExpenseApproveClick = False
            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 25, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            objExpense = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            popupRejectRemark.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReject_Click:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End

            If CInt(Me.ViewState("EditCrtranunkid")) <> 0 Then

                If popup_DeleteReason.Reason.Trim.Length > 0 Then
                    iRow = mdtTran.Select("crtranunkid = '" & CInt(Me.ViewState("EditCrtranunkid")) & "' AND AUD <> 'D'")
                    iRow(0).Item("isvoid") = True

                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = popup_DeleteReason.Reason.Trim  'Nilay (01-Feb-2015) -- txtReason.Text
                    iRow(0).Item("AUD") = "D"
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        iRow(0).Item("voiduserunkid") = Session("UserId")
                        iRow(0).Item("voidloginemployeeunkid") = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        iRow(0).Item("voiduserunkid") = -1
                        iRow(0).Item("voidloginemployeeunkid") = Session("Employeeunkid")
                    End If
                    iRow(0).AcceptChanges()

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Me.ViewState("mdtTran") = mdtTran
                    Me.ViewState("mdtTran") = mdtTran.Copy()
                    'Pinkal (05-Sep-2020) -- End
                    Fill_Expense()
                    Clear_Controls()
                    btnEdit.Visible = False : btnAdd.Visible = True
                    cboExpense.Enabled = True
                    iRow = Nothing
                Else
                    popup_DeleteReason.Show() 'Nilay (01-Feb-2015) --  popupDelete.Show()
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Expense Delete process fail.", Me)
                Clear_Controls()
                btnEdit.Visible = False : btnAdd.Visible = True
                cboExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            iRow = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnRemarkOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemarkOk.Click
        Try
            If txtRejectRemark.Text.Trim.Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Rejection Remark cannot be blank. Rejection Remark is required information."), Me)
                txtRejectRemark.Focus()
                popupRejectRemark.Show()
                Exit Sub
            End If

            If Is_Valid() = False Then Exit Sub

            If SaveData(sender) Then
                If mintClaimFormEmpId > 0 Then
                    SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True, Session("mdbname").ToString() _
                                            , CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    Session.Abandon()
                    Response.Redirect("../Index.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnRemarkOk_Click:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If Is_Valid() = False Then Exit Sub

            mblnIsExpenseEditClick = False
            mblnIsExpenseApproveClick = True

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
            End If
            'Pinkal (05-Sep-2020) -- End


            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim drUnit() As DataRow = mdtTran.Select("unitprice <= 0")
                If drUnit.Length > 0 Then
                    popup_UnitPriceYesNo.Message = Language.getMessage(mstrModuleName, 18, "Some of the expense(s) had 0 unit price for selected employee.") & vbCrLf & _
                                                                       Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                    popup_UnitPriceYesNo.Show()
                    Exit Sub
                Else
                    ApproveExpense()
                End If
                drUnit = Nothing
            Else
                ApproveExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSave_Click:-" & ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If mintClaimFormEmpId > 0 Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True _
                                            , Session("mdbname").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")) _
                                            , Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                Session.Abandon()
                Response.Redirect("../Index.aspx", False)
            Else
                Response.Redirect(Session("rootpath").ToString() & "Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnViewAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAttchment.Click
        Dim mstrPreviewIds As String = ""
        Dim dtTable As New DataTable
        Try

            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmployee.SelectedValue), _
                                                                             enScanAttactRefId.CLAIM_REQUEST, _
                                                                             enImg_Email_RefId.Claim_Request, _
                                                                             CInt(Me.ViewState("mintClaimRequestMasterId")))

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            'S.SANDEEP |25-JAN-2019| -- END
            popup_ShowAttchment._Webpage = Me
            popup_ShowAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End`
        End Try
    End Sub

    Protected Sub popup_ShowAttchment_btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ShowAttchment.btnPreviewClose_Click
        Try
            popup_ShowAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick = True AndAlso mblnIsExpenseApproveClick = False Then
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 25, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = False Then
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Language.getMessage(mstrModuleName, 25, "You have not set your expense remark.") & Language.getMessage(mstrModuleName, 17, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = True Then
                ApproveExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick = True AndAlso mblnIsExpenseApproveClick = False Then
                EditExpense()
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = False Then
                AddExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkViewEmployeeAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewEmployeeAllocation.Click
        Try
            ViewEmployeeAllocation._EmployeeId = CInt(cboEmployee.SelectedValue)
            ViewEmployeeAllocation.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkViewEmployeeAllocation_Click:- " & ex.Message, Me)
        End Try

    End Sub


    'Pinkal (20-Nov-2020) -- Start
    'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
    Protected Sub lnkShowFuelConsumptionReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowFuelConsumptionReport.Click
        Dim objFuelConsumption As New clsFuelConsumptionReport(CInt(Session("LangId")), CInt(Session("CompanyUnkId")))
        Try
            objFuelConsumption.SetDefaultValue()
            objFuelConsumption.setDefaultOrderBy(0)
            objFuelConsumption._FromDate = New DateTime(dtpDate.GetDate.Year, dtpDate.GetDate.Month, 1)
            objFuelConsumption._ToDate = New DateTime(dtpDate.GetDate.Year, dtpDate.GetDate.Month, DateTime.DaysInMonth(dtpDate.GetDate.Year, dtpDate.GetDate.Month))
            objFuelConsumption._ExpCateId = CInt(cboExpCategory.SelectedValue)
            objFuelConsumption._ExpCateName = cboExpCategory.SelectedItem.Text
            objFuelConsumption._EmpUnkId = CInt(cboEmployee.SelectedValue)
            objFuelConsumption._EmpName = cboEmployee.SelectedItem.Text
            objFuelConsumption._StatusId = 1
            objFuelConsumption._StatusName = Language.getMessage("clsMasterData", 110, "Approved")
            objFuelConsumption._ShowRequiredQuanitty = True
            objFuelConsumption._FirstNamethenSurname = CBool(Session("FirstNamethenSurname"))
            objFuelConsumption._UserUnkid = CInt(Session("UserId"))
            objFuelConsumption._CompanyUnkId = CInt(Session("CompanyUnkId"))
            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            objFuelConsumption.generateReportNew(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                         CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                         Session("ExportReportPath").ToString, _
                                         CBool(Session("OpenAfterExport")), _
                                         0, Aruti.Data.enPrintAction.None, enExportAction.None)

            Session("objRpt") = objFuelConsumption._Rpt

            If objFuelConsumption._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objFuelConsumption = Nothing
        End Try
    End Sub
    'Pinkal (20-Nov-2020) -- End


#End Region

#Region "GridView Event"

    Protected Sub dgvData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.DeleteCommand
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
        'Pinkal (05-Sep-2020) -- End
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy()
            End If
            'Pinkal (05-Sep-2020) -- End

            If CInt(e.Item.Cells(10).Text) <> 0 Then
                iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    Me.ViewState("EditCrtranunkid") = iRow(0).Item("crtranunkid")
                    popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popupDelete.Show()
                    Exit Sub
                End If
            ElseIf CStr(e.Item.Cells(12).Text) <> "" Then
                iRow = mdtTran.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                    iRow(0).AcceptChanges()
                End If
            End If

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdtTran
            Me.ViewState("mdtTran") = mdtTran.Copy()
            'Pinkal (05-Sep-2020) -- End

            Fill_Expense()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            iRow = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (05-Sep-2020) -- End
        Try
            If e.CommandName = "Edit" Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If Me.ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
                End If
                'Pinkal (05-Sep-2020) -- End

                Dim iRow As DataRow() = Nothing

                If CInt(e.Item.Cells(10).Text) <> 0 Then
                    iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    iRow = mdtTran.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnAdd.Visible = False : btnEdit.Visible = True
                    cboExpense.SelectedValue = iRow(0).Item("expenseunkid").ToString()
                    Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

                    cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid").ToString()
                    Call dtpDate_TextChanged(dtpDate, New EventArgs())

                    txtQty.Text = CDec(iRow(0).Item("quantity")).ToString()
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString())).ToString()
                    txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboExpense.Enabled = False
                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    cboCostCenter.Enabled = False
                    mintClaimRequestApprovalExpTranId = CInt(e.Item.Cells(10).Text)
                    mstrClaimRequestApprovalExpGUID = e.Item.Cells(12).Text.ToString()

                    Enable_Disable_Ctrls(False)

                    If CInt(e.Item.Cells(10).Text) <> 0 Then
                        Me.ViewState("EditCrtranunkid") = CStr(iRow(0).Item("crtranunkid"))
                    ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                        Me.ViewState("EditGuid") = CStr(iRow(0).Item("GUID"))
                    End If
                End If


            ElseIf e.CommandName.ToUpper = "PREVIEW" Then

                Dim mstrPreviewIds As String = ""
                Dim dtTable As New DataTable
                dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmployee.SelectedValue), _
                                                                                     enScanAttactRefId.CLAIM_REQUEST, _
                                                                                     enImg_Email_RefId.Claim_Request, _
                                                                                     CInt(e.Item.Cells(10).Text))

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
                End If

                popup_ShowAttchment.ScanTranIds = mstrPreviewIds
                popup_ShowAttchment._Webpage = Me
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                'S.SANDEEP |25-JAN-2019| -- END
                popup_ShowAttchment.Show()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtTable IsNot Nothing Then dtTable.Clear()
                dtTable = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub dgvData_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgvData.PageIndexChanged
        Try

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(7).Text.Trim.Length > 0 Then  'Unit Price
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(8).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvData_ItemDataBound:-" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "DatePicker Event(s)"

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, cboSectorRoute.SelectedIndexChanged
        Try
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim objPeriod As New clscommom_period_Tran
            'Pinkal (05-Sep-2020) -- End

            If CDate(Me.ViewState("MinDate")).Date <= dtpDate.GetDate.Date And CDate(Me.ViewState("MaxDate")).Date >= dtpDate.GetDate.Date Then
                Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
                Dim objCosting As New clsExpenseCosting
                objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.GetDate)
                txtCosting.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())
                txtCostingTag.Value = iCostingId.ToString()
                txtUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())
                Dim objExpnese As New clsExpense_Master
                objExpnese._Expenseunkid = CInt(cboExpense.SelectedValue)

                If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then


                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'If objExpnese._Uomunkid = enExpUoM.UOM_QTY Then
                    '    txtQty.Text = "0"
                    'Else
                    txtQty.Text = "1"
                    'End If
                    'Pinkal (04-Feb-2020) -- End

                    ' objExpnese = Nothing

                ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                    txtQty.Text = "1"
                End If


                If (sender.GetType().FullName = "System.Web.UI.WebControls.TextBox" AndAlso CType(sender, System.Web.UI.WebControls.TextBox).Parent.ID.ToUpper() = dtpDate.ID.ToUpper()) AndAlso CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboExpense.SelectedValue) > 0 Then
                    cboExpense_SelectedIndexChanged(New Object(), New EventArgs())
                End If

                objCosting = Nothing

                If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                    txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                    mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
                End If

                txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable
                objExpnese = Nothing
            Else
                dtpDate.SetDate = CDate(Me.ViewState("PDate"))
                DisplayMessage.DisplayMessage("Sorry, Date should be between " & CDate(Me.ViewState("MinDate")).ToShortDateString & " and " & CDate(Me.ViewState("MaxDate")).ToShortDateString & ".", Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dtpDate_TextChanged" & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Control Event"

#End Region

#Region "ComboBox Event(s)"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
                Me.ViewState("MinDate") = CDate(eZeeDate.convertDate("17530101"))
                Me.ViewState("MaxDate") = CDate(eZeeDate.convertDate("99981231"))

                dtpDate.SetDate = objPeriod._Start_Date
                Me.ViewState("PDate") = objPeriod._Start_Date
                Me.ViewState("MinDate") = Session("fin_startdate")

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objPeriod = Nothing
                'Pinkal (05-Sep-2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim dsCombo As New DataSet
        'Pinkal (05-Sep-2020) -- End
        Try

            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(cboExpCategory.SelectedValue), True, "List", CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), True)

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (05-Sep-2020) -- End

            Dim dtTable As DataTable = Nothing
            'Pinkal (04-Aug-2020) -- Start
            'Enhancement NMB  - Working on Expense Setting related changes in Expense Approval for NMB.	
            If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtTable = New DataView(dsCombo.Tables(0), "cr_expinvisible = 0", "", DataViewRowState.CurrentRows).ToTable()
            End If
            'Pinkal (04-Aug-2020) -- End

            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End


            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE

                        Language.setLanguage(mstrModuleName)
                        objlblValue.Text = Language.getMessage(mstrModuleName, 14, "Leave Form")

                        Dim objLeaveType As New clsleavetype_master

                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, Session("Database_Name").ToString, "", False)
                        End If


                        'Pinkal (05-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objLeaveType = Nothing
                        'Pinkal (05-Sep-2020) -- End


                        With cboLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedIndex = 0
                        End With

                        If IsPostBack = True Then
                            Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs())
                        End If

                    Case enExpenseType.EXP_MEDICAL
                        'Dim obj
                    Case enExpenseType.EXP_TRAINING
                        dsCombo = clsTraining_Enrollment_Tran.GetEmployee_TrainingList(CInt(cboEmployee.SelectedValue), True)
                        With cboReference
                            .DataValueField = "Id"
                            .DataTextField = "Name"
                            .DataSource = dsCombo.Tables("List")
                            .DataBind()
                            .SelectedValue = "0"
                        End With

                    Case enExpenseType.EXP_MISCELLANEOUS
                        cboLeaveType.Enabled = False
                        cboReference.Enabled = False
                End Select

                If CInt(cboEmployee.SelectedValue) > 0 Then

                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(cboEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If
            Else
                objlblValue.Text = ""
                cboLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try
            If CBool(Session("SectorRouteAssignToExpense")) Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                    .SelectedIndex = 0
                End With

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing
                'Pinkal (05-Sep-2020) -- End

                objAssignExpense = Nothing
            End If
            'Pinkal (20-Feb-2019) -- End

            Dim objExpMaster As New clsExpense_Master
            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                Me.ViewState("mblnIsLeaveEncashment") = objExpMaster._IsLeaveEncashment

                cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.SelectedValue = "0"
                cboCostCenter.SelectedValue = "0"

                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter().ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If

                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End
                    txtBalance.Text = "0.00"

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (05-Sep-2020) -- End

                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then
                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing
                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                        dsList = objLeave.GetList("List", Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                  True, _
                                                  True, True, False, CInt(cboEmployee.SelectedValue), _
                                               False, False, False, "", Nothing, mblnIsExternalApprover)


                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                        dsList = objLeave.GetList("List", Session("Database_Name").ToString(), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                 Session("EmployeeAsOnDate").ToString(), _
                                                  Session("UserAccessModeSetting").ToString(), True, _
                                                  True, _
                                                  True, True, False, CInt(cboEmployee.SelectedValue), _
                                                 True, True, False, "", Nothing, mblnIsExternalApprover)

                    End If

                    dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then
                        txtBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then

                            If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = CDate(Session("fin_enddate")).Date
                            If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            Else
                                txtBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                            End If

                        ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then

                            Dim mdtDate As Date = dtpDate.GetDate.Date
                            Dim mdtDays As Integer = 0

                            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                                If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                                    mdtDate = CDate(Session("fin_enddate")).Date
                                    mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                                Else
                                    If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                        mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                                    End If
                                End If

                            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                                If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                                    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                                End If
                                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                            End If

                            Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                            If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                                intDiff = intDiff - 1
                            End If

                            txtBalanceAsOnDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        End If

                        txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 6, "Quantity")
                    End If


                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objLeave = Nothing
                    'Pinkal (05-Sep-2020) -- End

                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance
                    Dim dsBal As New DataSet
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtBalance.Text = CStr(Math.Round(Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")), 6))  'SHANI (06 JUN 2015) -- Start
                        txtBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    objEmpExpBal = Nothing
                End If
            Else
                txtUoMType.Text = ""
                txtUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                txtBalance.Text = "0.00"
                txtBalanceAsOnDate.Text = "0.00"
                cboCostCenter.Enabled = False
                cboCostCenter.SelectedValue = "0"

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtQty.Text = "1"
                'Pinkal (04-Feb-2020) -- End

            End If

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtQty.Text = GetEmployeeDepedentCountForCR().ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtQty.Text)
            Else
                txtQty.Text = "1"
            End If
            'Pinkal (04-Feb-2020) -- End

            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable

            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                lblBalance.Visible = True
                lblBalanceasondate.Visible = True
                txtBalance.Visible = True
                txtBalanceAsOnDate.Visible = True
                cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                cboCurrency.Enabled = False
            Else
                lblBalance.Visible = False
                lblBalanceasondate.Visible = False
                txtBalance.Visible = False
                txtBalanceAsOnDate.Visible = False

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim mdtTran As DataTable = CType(Me.ViewState("mdtTran"), DataTable)
                Dim mdtTran As DataTable = Nothing
                If Me.ViewState("mdtTran") IsNot Nothing Then
                    mdtTran = CType(Me.ViewState("mdtTran"), DataTable).Copy
                End If
                'Pinkal (05-Sep-2020) -- End


                If mdtTran IsNot Nothing Then
                    If mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboCurrency.Enabled = False
                        cboCurrency.SelectedValue = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First().ToString()
                    Else
                        cboCurrency.SelectedValue = mintBaseCountryId.ToString()
                        cboCurrency.Enabled = True
                    End If
                End If

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("cboExpense_SelectedIndexChanged:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform : Dim dsLeave As New DataSet
            dsLeave = objleave.getListForCombo(CInt(cboLeaveType.SelectedValue), "7", CInt(cboEmployee.SelectedValue), True, True)

            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = dsLeave.Tables("List")
                .DataBind()
                .SelectedIndex = 0
            End With

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            'Pinkal (05-Sep-2020) -- End

            objleave = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("cboLeaveType_SelectedIndexChanged:-" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "TextBox Event"
    Protected Sub txtQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQty.TextChanged
        Dim decQty As Decimal = 0
        Try
            Decimal.TryParse(txtQty.Text, decQty)
            If txtQty.Text.Trim.Length > 0 Then
                If IsNumeric(txtQty.Text) = False Then
                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'txtQty.Text = "0"
                    txtQty.Text = "1"
                    'Pinkal (04-Feb-2020) -- End
                    Exit Sub
                End If
                If CInt(txtQty.Text) > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If CBool(Me.ViewState("mblnIsLeaveEncashment")) = False Then
                        txtUnitPrice.Enabled = True
                        If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                            txtUnitPrice.Text = CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())).ToString
                            txtUnitPrice.Focus()
                        End If
                    Else
                        txtUnitPrice.Enabled = False
                    End If
                ElseIf decQty <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                        txtUnitPrice.Text = CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())).ToString()
                    End If
                End If
            Else
                txtUnitPrice.Enabled = False
            End If
            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            objExpense = Nothing
            'Pinkal (18-Mar-2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("txtQty_TextChanged:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub txtUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.TextChanged
        Try
            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("txtUnitPrice_TextChanged:-" & ex.Message, Me)
        End Try

    End Sub
#End Region


    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbExpenseInformation", Me.lblDetialHeader.Text)

            Me.lblName.Text = Language._Object.getCaption(Me.lblName.ID, Me.lblName.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Language._Object.getCaption(Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.btnRemarkClose.Text = Language._Object.getCaption(Me.btnRemarkClose.ID, Me.btnRemarkClose.Text)
            Me.btnRemarkOk.Text = Language._Object.getCaption(Me.btnRemarkOk.ID, Me.btnRemarkOk.Text)
            Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.ID, Me.btnOK.Text)
            Me.tbExpenseRemark.HeaderText = Language._Object.getCaption(Me.tbExpenseRemark.ID, Me.tbExpenseRemark.HeaderText)
            Me.tbClaimRemark.HeaderText = Language._Object.getCaption(Me.tbClaimRemark.ID, Me.tbClaimRemark.HeaderText)
            Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.ID, Me.lblSector.Text)

            'Pinkal (23-Oct-2015) -- Start
            'Enhancement - Putting Domicile Address for TRA as per Dennis Requirement
            Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)
            'Pinkal (23-Oct-2015) -- End

            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            lblBalanceasondate.Text = Language._Object.getCaption(Me.lblBalanceasondate.ID, Me.lblBalanceasondate.Text)
            'Pinkal (30-Apr-2018) - Start

            'Pinkal (20-Nov-2020) -- Start
            'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
            lnkShowFuelConsumptionReport.Text = Language._Object.getCaption(Me.lnkShowFuelConsumptionReport.ID, Me.lnkShowFuelConsumptionReport.Text)
            'Pinkal (20-Nov-2020) -- End

            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
            Me.btnReject.Text = Language._Object.getCaption(Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


            Me.dgvData.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(2).FooterText, Me.dgvData.Columns(2).HeaderText)
            Me.dgvData.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(3).FooterText, Me.dgvData.Columns(3).HeaderText)
            Me.dgvData.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(4).FooterText, Me.dgvData.Columns(4).HeaderText)
            Me.dgvData.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(5).FooterText, Me.dgvData.Columns(5).HeaderText)
            Me.dgvData.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(6).FooterText, Me.dgvData.Columns(6).HeaderText)
            Me.dgvData.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(7).FooterText, Me.dgvData.Columns(7).HeaderText)
            Me.dgvData.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvData.Columns(8).FooterText, Me.dgvData.Columns(8).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:-" & Ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
            Language.setMessage(mstrModuleName, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Language.setMessage(mstrModuleName, 4, "Expense is mandatory information. Please select Expense to continue.")
            Language.setMessage(mstrModuleName, 5, "Quantity is mandatory information. Please enter Quantity to continue.")
            Language.setMessage(mstrModuleName, 7, "Please add atleast one expense in order to do futher operation.")
            Language.setMessage(mstrModuleName, 8, "Rejection Remark cannot be blank. Rejection Remark is required information.")
            Language.setMessage(mstrModuleName, 9, "Sorry, you cannot add same expense again in the list below.")
            Language.setMessage(mstrModuleName, 10, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
            Language.setMessage(mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set.")
            Language.setMessage(mstrModuleName, 12, "Sorry, you cannot set amount greater than balance set.")
            Language.setMessage(mstrModuleName, 13, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Language.setMessage(mstrModuleName, 14, "Leave Form")
            Language.setMessage(mstrModuleName, 15, "Sorry, you cannot set quantity greater than balance as on date set.")
            Language.setMessage(mstrModuleName, 16, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Language.setMessage(mstrModuleName, 17, "Do you wish to continue?")
            Language.setMessage(mstrModuleName, 18, "Some of the expense(s) had 0 unit price for selected employee.")
            Language.setMessage(mstrModuleName, 19, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
            Language.setMessage(mstrModuleName, 20, "Sorry,you cannot apply for this expense.Reason : This expense claim amount is exceeded the budget amount.")
            Language.setMessage(mstrModuleName, 21, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Language.setMessage(mstrModuleName, 22, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
            Language.setMessage(mstrModuleName, 23, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
            Language.setMessage(mstrModuleName, 24, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
            Language.setMessage(mstrModuleName, 25, "You have not set your expense remark.")
            Language.setMessage(mstrModuleName, 26, "Currency is mandatory information. Please select Currency to continue.")
            Language.setMessage(mstrModuleName, 27, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
            Language.setMessage(mstrModuleName, 28, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
            Language.setMessage(mstrModuleName, 29, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")


            Language.setMessage("clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [")
            Language.setMessage("clsclaim_request_master", 4, " ] time(s).")
            Language.setMessage("clsExpCommonMethods", 6, "Quantity")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>


End Class
