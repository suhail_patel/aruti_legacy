﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Disciplin_Report_Rpt_Discipline_Status_Summary_Report
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDiscipline_Status_Summary_Report"
    Private objDiscipline_Summary As clsDiscipline_Status_Summary_Report
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objDisciplineStatus As New clsDiscipline_Status
            dsList = objDisciplineStatus.getComboList("Status", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboStatus.SelectedValue = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDiscipline_Summary.SetDefaultValue()

            objDiscipline_Summary._DisciplineStatusId = cboStatus.SelectedValue
            objDiscipline_Summary._DisciplineStatus = cboStatus.Text

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = objDiscipline_Summary._ReportName
            Me.lblPageHeader.Text = objDiscipline_Summary._ReportName
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objDiscipline_Summary = New clsDiscipline_Status_Summary_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objDiscipline_Summary._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objDiscipline_Summary._UserUnkId = CInt(Session("UserId"))
            objDiscipline_Summary.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CStr(Session("ExportReportPath")), _
                                                CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objDiscipline_Summary._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
