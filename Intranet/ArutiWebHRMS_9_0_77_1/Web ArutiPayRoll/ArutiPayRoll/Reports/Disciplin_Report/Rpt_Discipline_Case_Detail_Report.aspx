﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Discipline_Case_Detail_Report.aspx.vb" Inherits="Reports_Disciplin_Report_Rpt_Discipline_Case_Detail_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="Date" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 55%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Discipline Case Detail Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblDateFrom" runat="server" Text="Charge Date"></asp:Label></div>
                                        <div class="ib" style="width: 30%">
                                            <uc1:Date ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label></div>
                                        <div class="ib" style="width: 30%">
                                            <uc1:Date ID="dtpToDate" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblOffenceCategory" runat="server" Text="Offence Category"></asp:Label></div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboOffenceCategory" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblDisciplineType" runat="server" Text="Offence Decription"></asp:Label></div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboDisciplineType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label></div>
                                        <div class="ib" style="width: 70%">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <%--'S.SANDEEP |10-JUN-2020| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : DISICIPLINE REPORT {ALLOCATION DISPLAY BASED ON CHARGE DATE}--%>
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:CheckBox runat="server" ID="chkDisplayAllocationBasedOnChargeDate" Checked="true" Text="Display Employee Allocation Based on Charge Date" />
                                        </div>
                                    </div>
                                    <%--'S.SANDEEP |10-JUN-2020| -- END--%>
                                    
                                    <div class="row2">
                                        <div class="ib" style="width: 20%">
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:CheckBox runat="server" ID="chkIncludeInactiveEmployee" Checked="false" Text="Include Inactive Employee" />
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                    <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
