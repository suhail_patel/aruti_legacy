﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_Disciplin_Report_Rpt_DisciplinaryPenaltyReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmDisciplinaryPenaltyReport"
    Private objDisciplinaryPenalty As clsDisciplinaryPenaltyReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCategory As New clsCommon_Master
        Dim objDisciplineAction As New clsAction_Reason
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objCategory.getComboList(clsCommon_Master.enCommonMaster.OFFENCE_CATEGORY, True, "list")
            With cboOffenceCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            Call cboOffenceCategory_SelectedIndexChanged(New Object, New EventArgs())

            dsList = objDisciplineAction.getComboList("Action", True, True)
            With cboDisciplineAction
                .DataValueField = "actionreasonunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            dsList.Dispose()
            objEmployee = Nothing
            objCategory = Nothing
            objDisciplineAction = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboDisciplineType.SelectedValue = 0
            cboOffenceCategory.SelectedValue = 0
            cboDisciplineAction.SelectedValue = 0

            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing

            dtpEPFromDate.SetDate = Nothing
            dtpEPToDate.SetDate = Nothing

            dtpPFromDate.SetDate = Nothing
            dtpPToDate.SetDate = Nothing
            chkIncludeInactiveEmployee.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objDisciplinaryPenalty.SetDefaultValue()

            objDisciplinaryPenalty._EmployeeId = cboEmployee.SelectedValue
            objDisciplinaryPenalty._EmployeeName = cboEmployee.Text

            If dtpFromDate.IsNull = False AndAlso dtpToDate.IsNull = False Then
                objDisciplinaryPenalty._ChargeDateFrom = dtpFromDate.GetDate.Date
                objDisciplinaryPenalty._ChargeDateTo = dtpToDate.GetDate.Date
            End If

            If dtpEPFromDate.IsNull = False AndAlso dtpEPToDate.IsNull = False Then
                objDisciplinaryPenalty._PenEffDateFrom = dtpEPFromDate.GetDate.Date
                objDisciplinaryPenalty._PenEffDateTo = dtpEPToDate.GetDate.Date
            End If

            If dtpPFromDate.IsNull = False AndAlso dtpPToDate.IsNull = False Then
                objDisciplinaryPenalty._PenExpDateFrom = dtpPFromDate.GetDate.Date
                objDisciplinaryPenalty._PenExpDateTo = dtpPToDate.GetDate.Date
            End If

            objDisciplinaryPenalty._OffenceCategory = cboOffenceCategory.Text
            objDisciplinaryPenalty._OffenceCategoryId = CInt(cboOffenceCategory.SelectedValue)
            objDisciplinaryPenalty._OffenceDescrId = CInt(cboDisciplineType.SelectedValue)
            objDisciplinaryPenalty._OffenceDescription = cboDisciplineType.Text
            objDisciplinaryPenalty._ViewByIds = mstrStringIds
            objDisciplinaryPenalty._ViewIndex = mintViewIdx
            objDisciplinaryPenalty._ViewByName = mstrStringName
            objDisciplinaryPenalty._Analysis_Fields = mstrAnalysis_Fields
            objDisciplinaryPenalty._Analysis_Join = mstrAnalysis_Join
            objDisciplinaryPenalty._Analysis_OrderBy = mstrAnalysis_OrderBy
            objDisciplinaryPenalty._Report_GroupName = mstrReport_GroupName
            objDisciplinaryPenalty._ShowAllocationBasedOnChargeDate = chkDisplayAllocationBasedOnChargeDate.Checked
            objDisciplinaryPenalty._ShowAllocationBasedOnChargeDateString = chkDisplayAllocationBasedOnChargeDate.Text
            objDisciplinaryPenalty._ActionId = cboDisciplineAction.SelectedValue
            objDisciplinaryPenalty._ActionName = cboDisciplineAction.Text
            objDisciplinaryPenalty._IncludeInactiveEmployee = chkIncludeInactiveEmployee.Checked

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = objDisciplinaryPenalty._ReportName
            Me.lblPageHeader.Text = objDisciplinaryPenalty._ReportName
            Me.lblOffenceCategory.Text = Language._Object.getCaption(Me.lblOffenceCategory.ID, Me.lblOffenceCategory.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lblDisciplineType.Text = Language._Object.getCaption(Me.lblDisciplineType.ID, Me.lblDisciplineType.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text)
            Me.chkDisplayAllocationBasedOnChargeDate.Text = Language._Object.getCaption(Me.chkDisplayAllocationBasedOnChargeDate.ID, Me.chkDisplayAllocationBasedOnChargeDate.Text)
            Me.lblPegFrom.Text = Language._Object.getCaption(Me.lblPegFrom.ID, Me.lblPegFrom.Text)
            Me.lblPegTo.Text = Language._Object.getCaption(Me.lblPegTo.ID, Me.lblPegTo.Text)
            Me.lblPeffFrom.Text = Language._Object.getCaption(Me.lblPeffFrom.ID, Me.lblPeffFrom.Text)
            Me.lblPeffTo.Text = Language._Object.getCaption(Me.lblPeffTo.ID, Me.lblPeffTo.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lblDisciplineAction.Text = Language._Object.getCaption(Me.lblDisciplineAction.ID, Me.lblDisciplineAction.Text)

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objDisciplinaryPenalty = New clsDisciplinaryPenaltyReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = Me.ViewState("mstrStringIds")
                mstrStringName = Me.ViewState("mstrStringName")
                mintViewIdx = Me.ViewState("mintViewIdx")
                mstrAnalysis_Fields = Me.ViewState("mstrAnalysis_Fields")
                mstrAnalysis_Join = Me.ViewState("mstrAnalysis_Join")
                mstrAnalysis_OrderBy = Me.ViewState("mstrAnalysis_OrderBy")
                mstrReport_GroupName = Me.ViewState("mstrReport_GroupName")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objDisciplinaryPenalty._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objDisciplinaryPenalty._UserUnkId = CInt(Session("UserId"))
            objDisciplinaryPenalty._OpenAfterExport = False

            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objDisciplinaryPenalty._ExportReportPath = strFilePath

            objDisciplinaryPenalty.generateReportNew(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                strFilePath, _
                                                False, 0, enPrintAction.None, _
                                                enExportAction.None, _
                                                CInt(Session("Base_CurrencyId")))


            If objDisciplinaryPenalty._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objDisciplinaryPenalty._FileNameAfterExported
                Export.Show()
                Exit Sub
            End If

            'If Session("objRpt") IsNot Nothing Then
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboOffenceCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOffenceCategory.SelectedIndexChanged
        Dim objOffence As New clsDisciplineType
        Dim dsList As New DataSet
        Try
            dsList = objOffence.getComboList("List", True, CInt(cboOffenceCategory.SelectedValue))
            With cboDisciplineType
                .DataValueField = "disciplinetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objOffence = Nothing : dsList.Dispose()
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Private Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy._Hr_EmployeeTable_Alias = "EM"
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
