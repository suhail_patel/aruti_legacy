﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="rpt_GrievanceStatusAndNatureReport.aspx.vb"
    Inherits="Reports_Grievance_rpt_GrievanceStatusAndNatureReport" Title="Grievance Summary Report" %>

<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js" type="text/javascript"></script>

    <script src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Detail Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                <div style="text-align: right">
                                    <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="lblReportType" runat="server" Text="Report Type" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 70%;" class="ib">
                                        <asp:DropDownList ID="cboReportType" runat="server" Width="385px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2" style="width: 100%">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="objlbltitle" runat="server" Text="Status" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 70%;" class="ib">
                                        <asp:DropDownList ID="cboType" runat="server" Width="385px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc7:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
