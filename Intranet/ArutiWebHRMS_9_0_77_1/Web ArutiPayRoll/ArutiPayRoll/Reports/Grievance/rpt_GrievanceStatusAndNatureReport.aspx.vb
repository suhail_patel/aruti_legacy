﻿Imports Aruti.Data
Imports System.Data
Imports ArutiReports

Partial Class Reports_Grievance_rpt_GrievanceStatusAndNatureReport
    Inherits Basepage

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private objGrievanceStatusAndNature_Report As clsGrievanceStatusAndNature_Report
    Private ReadOnly mstrModuleName As String = "frmGrievanceStatusAndNatureReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
    Dim dtFinYearList As DataTable
    Private mdtFinStartDate As DateTime
    Private mdtFinEndDate As DateTime

#End Region

#Region " Private Function(s) & Method(s) "
    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objcommonmaster As New clsCommon_Master
        Dim objclsMasterData As New clsMasterData

        Try
            cboReportType.Items.Clear()
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Grievances Status Report"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Nature of Grievance Report"))
            Call cboReportType_SelectedIndexChanged(Nothing, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :" + ex.Message, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboReportType.SelectedIndex = 0
            cboType.SelectedIndex = 0
            Me.ViewState("StringIds") = ""
            Me.ViewState("ViewIdx") = -1
            Me.ViewState("StringName") = ""
            Me.ViewState("Analysis_Fields") = ""
            Me.ViewState("Analysis_Join") = ""
            Me.ViewState("Analysis_OrderBy") = ""
            Me.ViewState("Report_GroupName") = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            objGrievanceStatusAndNature_Report.SetDefaultValue()
            objGrievanceStatusAndNature_Report._ReportName = cboReportType.Text
            objGrievanceStatusAndNature_Report._IncludeInactiveEmp = False

            objGrievanceStatusAndNature_Report._ViewByIds = mstrStringIds
            objGrievanceStatusAndNature_Report._ViewIndex = mintViewIdx
            objGrievanceStatusAndNature_Report._ViewByName = mstrStringName
            objGrievanceStatusAndNature_Report._Analysis_Fields = mstrAnalysis_Fields
            objGrievanceStatusAndNature_Report._Analysis_Join = mstrAnalysis_Join
            objGrievanceStatusAndNature_Report._Analysis_OrderBy = mstrAnalysis_OrderBy
            objGrievanceStatusAndNature_Report._Report_GroupName = mstrReport_GroupName
            objGrievanceStatusAndNature_Report._Advance_Filter = mstrAdvanceFilter

            objGrievanceStatusAndNature_Report._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objGrievanceStatusAndNature_Report._OpenAfterExport = False

            If cboReportType.SelectedIndex = 0 Then
                objGrievanceStatusAndNature_Report._ReportId = 1
                If cboType.SelectedValue > 0 Then
                    objGrievanceStatusAndNature_Report._GrievanceResolutionType = cboType.SelectedValue
                    objGrievanceStatusAndNature_Report._GrievanceResolutionTypeName = cboType.SelectedItem.Text
                End If
            Else
                objGrievanceStatusAndNature_Report._ReportId = 2
                If cboType.SelectedValue > 0 Then
                    objGrievanceStatusAndNature_Report._GrievanceType = cboType.SelectedValue
                    objGrievanceStatusAndNature_Report._GrievanceTypeName = cboType.SelectedItem.Text
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter" & ex.Message, Me)
            Return False
        End Try

    End Function
#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objGrievanceStatusAndNature_Report.setDefaultOrderBy(0)

            objGrievanceStatusAndNature_Report._CompanyUnkId = Session("CompanyUnkId")
            objGrievanceStatusAndNature_Report._UserUnkId = Session("UserId")
            objGrievanceStatusAndNature_Report._UserAccessFilter = Session("AccessLevelFilterString")
            Call SetDateFormat()
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            objGrievanceStatusAndNature_Report.Generate_DetailReport(Session("Database_Name"), _
                                                                     Session("UserId"), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     Session("EmployeeAsOnDate"), _
                                                                     Session("UserAccessModeSetting"), True, _
                                                                     Session("Base_CurrencyId"))


            If objGrievanceStatusAndNature_Report._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objGrievanceStatusAndNature_Report._FileNameAfterExported

                Export.Show()

            End If


            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


#End Region

#Region "Control Event"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayMessage("lnkAnalysisBy_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            Me.ViewState("StringIds") = popupAnalysisBy._ReportBy_Ids
            Me.ViewState("StringName") = popupAnalysisBy._ReportBy_Name
            Me.ViewState("ViewIdx") = popupAnalysisBy._ViewIndex
            Me.ViewState("Analysis_Fields") = popupAnalysisBy._Analysis_Fields
            Me.ViewState("Analysis_Join") = popupAnalysisBy._Analysis_Join
            Me.ViewState("Analysis_OrderBy") = popupAnalysisBy._Analysis_OrderBy
            Me.ViewState("Report_GroupName") = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :-" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objGrievanceStatusAndNature_Report = New clsGrievanceStatusAndNature_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If IsPostBack = False Then
                Call FillCombo()
                Call ResetValue()
                Me.ViewState.Add("StringIds", "")
                Me.ViewState.Add("StringName", "")
                Me.ViewState.Add("ViewIdx", -1)
                Me.ViewState.Add("Analysis_Fields", "")
                Me.ViewState.Add("Analysis_Join", "")
                Me.ViewState.Add("Analysis_OrderBy", "")
                Me.ViewState.Add("Report_GroupName", "")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

 
    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            cboType.DataSource = Nothing

            If cboReportType.SelectedIndex = 0 Then
                Dim objMaster As New clsMasterData
                dsList = objMaster.GetGrievanceReportResolutionStatus(True, "StatusList")


                With cboType
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("StatusList")
                    .DataBind()
                    .SelectedIndex = 0
                End With

                objlbltitle.Text = Language.getMessage(mstrModuleName, 3, "Grievance Status")

            ElseIf cboReportType.SelectedIndex = 1 Then
                objlbltitle.Text = Language.getMessage(mstrModuleName, 3, "Grievance Nature")

                Dim objCMaster As New clsCommon_Master
                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "NatureList")


                With cboType
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("NatureList")
                    .DataBind()
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("cboReportType_SelectedIndexChanged" & ex.Message, Me)
        End Try
    End Sub

End Class
