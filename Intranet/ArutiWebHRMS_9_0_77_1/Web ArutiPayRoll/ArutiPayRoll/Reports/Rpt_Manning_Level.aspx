﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Manning_Level.aspx.vb" Inherits="Reports_Rpt_Manning_Level" Title="Manning Level Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 50%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Manning Level Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="Label1" runat="server" Text="Job Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpJobGroup" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%; text-align: center">
                                                <asp:Label ID="Label2" runat="server" Text="Job"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpJob" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPlannedCount" runat="server" Text="Planned From"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtPlannedFrom" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%; text-align: center">
                                                <asp:Label ID="lblPlannedTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtPlannedTo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAvailableCount" runat="server" Text="Available From"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtAvailableFrom" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%; text-align: center">
                                                <asp:Label ID="lblAvailableTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtAvailableTo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblVariance" runat="server" Text="Variance From"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtVarianceFrom" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%; text-align: center">
                                                <asp:Label ID="lblVarianceTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtVarianceTo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
