﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_MedicalClaim.aspx.vb" Inherits="Reports_Rpt_MedicalClaim" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 35%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Medical Claim Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblProvider" runat="server" Text="Provider"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="drpProvider" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkSelectallPeriodList" runat="server" AutoPostBack="true" Checked="false"
                                                    CssClass="chkbx" Text="Period" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                                <div style="border: 1px solid #DDD; height: 200px; overflow: auto; margin-top: 5px;
                                                    margin-bottom: 5px">
                                                    <asp:CheckBoxList ID="chkPeriod" runat="server" RepeatLayout="Flow">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkInActiveEmp" runat="server" Checked="false" CssClass="chkbx"
                                                    Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2">
                                                <asp:CheckBox ID="chkMyInvoice" runat="server" Checked="false" CssClass="chkbx" Text="My Invoice Only" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

