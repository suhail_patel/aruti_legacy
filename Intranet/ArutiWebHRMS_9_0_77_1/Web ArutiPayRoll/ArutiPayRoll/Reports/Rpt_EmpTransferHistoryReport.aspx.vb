﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region
Partial Class Reports_Rpt_EmpTransferHistoryReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmpTransfer_History_Report"
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mintViewIndex As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrAnalysis_OrderBy_GName As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                ResetValue()
            Else
                mstrViewByIds = ViewState("mstrViewByIds").ToString
                mstrViewByName = ViewState("mstrViewByName").ToString
                mintViewIndex = CInt(ViewState("mintViewIndex"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrAnalysis_OrderBy_GName = ViewState("mstrAnalysis_OrderBy_GName").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrViewByIds") = mstrViewByIds
            Me.ViewState("mstrViewByName") = mstrViewByName
            Me.ViewState("mintViewIndex") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrAnalysis_OrderBy_GName") = mstrAnalysis_OrderBy_GName
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim ObjEmp As New clsEmployee_Master
        Dim dsCombo As New DataSet
        Try
            Dim objMaster As New clsMasterData
            dsCombo = objMaster.GetEAllocation_Notification("Allocation", "", False, False)
            Dim dtTable As DataTable = New DataView(dsCombo.Tables(0), "Id NOT IN (" & enAllocation.JOBS & "," & enAllocation.JOB_GROUP & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable()

            Dim drRow As DataRow = dtTable.NewRow
            drRow("Id") = 0
            drRow("Name") = Language.getMessage(mstrModuleName, 1, "Select")
            dtTable.Rows.InsertAt(drRow, 0)

            With cboAllocation
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable.Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            dtTable = Nothing


            dsCombo = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                               Session("UserId"), _
                                               Session("Fin_year"), _
                                               Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               Session("UserAccessModeSetting"), True, _
                                               Session("IsIncludeInactiveEmp"), "Emp", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objMData = Nothing
            ObjEmp = Nothing
            dsCombo.Dispose()
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpStartdate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            cboAllocation.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            chkInactiveemp.Checked = False
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Public Function SetFilter(ByRef objEmpTransfer As clsEmpTransfer_History_Report) As Boolean
        Try
            objEmpTransfer.SetDefaultValue()

            If dtpStartdate.IsNull = False Then
                objEmpTransfer._FromDate = dtpStartdate.GetDate.Date
            Else
                objEmpTransfer._FromDate = Nothing
            End If
            If dtpToDate.IsNull = False Then
                objEmpTransfer._ToDate = dtpToDate.GetDate.Date
            Else
                objEmpTransfer._ToDate = Nothing
            End If
            objEmpTransfer._AllocationTypeId = CInt(cboAllocation.SelectedValue)
            objEmpTransfer._AllocationTypeName = cboAllocation.SelectedItem.Text
            objEmpTransfer._EmployeeID = CInt(cboEmployee.SelectedValue)
            objEmpTransfer._EmployeeName = cboEmployee.SelectedItem.Text
            objEmpTransfer._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpTransfer._ViewByIds = mstrViewByIds
            objEmpTransfer._ViewIndex = mintViewIndex
            objEmpTransfer._ViewByName = mstrViewByName
            objEmpTransfer._Analysis_Fields = mstrAnalysis_Fields
            objEmpTransfer._Analysis_Join = mstrAnalysis_Join
            objEmpTransfer._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpTransfer._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            objEmpTransfer._Report_GroupName = mstrReport_GroupName

            objEmpTransfer._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objEmpTransfer._OpenAfterExport = False
            objEmpTransfer._CompanyUnkId = CInt(Session("CompanyUnkId"))

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function

#End Region

#Region "Button Events"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objEmpTransfer As New clsEmpTransfer_History_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If CInt(cboAllocation.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Allocation is compulsory information.Please Select Allocation."), Me)
                cboAllocation.Focus()
                Exit Sub
            End If

            If SetFilter(objEmpTransfer) = False Then Exit Sub

            Call SetDateFormat()

            objEmpTransfer.Generate_DetailReport(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           Session("EmployeeAsOnDate").ToString(), _
                                           CStr(Session("UserAccessModeSetting")), True)

            If objEmpTransfer._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmpTransfer._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objEmpTransfer = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrAnalysis_OrderBy_GName = popupAnalysisBy._Analysis_OrderBy_GName
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.ID, Me.LblToDate.Text)
            Me.LblAllocation.Text = Language._Object.getCaption(Me.LblAllocation.ID, Me.LblAllocation.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)

            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Allocation is compulsory information.Please Select Allocation.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
