﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Rpt_CRSummaryReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private ObjCRSummary As New clsClaimRequestsummaryReport
    'Pinkal (05-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmCRSummaryReport"

    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try
            
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), _
                                                True, False, "Emp", True)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With
            ObjEmp = Nothing


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            'dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
            'Pinkal (10-Feb-2021) -- End
            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            cboExpenseCategory_SelectedIndexChanged(Nothing, Nothing)

            FillStatus()

            dsCombos = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With

            Dim objPeriod As New clscommom_period_Tran
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "Period", True)

            With cboFromPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            With cboToPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0).Copy
                .SelectedValue = 0
                .DataBind()
            End With
            objPeriod = Nothing


            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            dsCombos = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombos = objExchange.getComboList("List", True, False)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
            End With
            objExchange = Nothing
            'Pinkal (30-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            ObjEmp = Nothing
            ObjMaster = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillStatus()
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMasterData.getLeaveStatusList("List", "")

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtab
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objMasterData = Nothing
            dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpTranFromDate.SetDate = Nothing
            dtpTranToDate.SetDate = Nothing

            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0

            chkShowClaimStatus.Checked = True

            cboUOM.SelectedValue = 0
            cboFromPeriod.SelectedValue = 0
            cboToPeriod.SelectedValue = 0
            cboStatus.SelectedIndex = 0

            cboExpense.SelectedValue = 0

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            cboCurrency.SelectedValue = 0
            'Pinkal (30-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private Function SetFilter() As Boolean
    Private Function SetFilter(ByRef ObjCRSummary As clsClaimRequestsummaryReport) As Boolean
        'Pinkal (05-Sep-2020) -- End
        Try
            ObjCRSummary.SetDefaultValue()

            If (Not dtpTranFromDate.IsNull AndAlso dtpTranToDate.IsNull) OrElse (dtpTranFromDate.IsNull AndAlso Not dtpTranToDate.IsNull) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue."), Me)
                Return False

            ElseIf CInt(cboUOM.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "UOM is compulsory information.please select UOM."), Me)
                cboUOM.Focus()
                Return False

            ElseIf CInt(cboFromPeriod.SelectedValue) <= 0 And CInt(cboToPeriod.SelectedValue) > 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select From Period."), Me)
                cboFromPeriod.Focus()
                Exit Function

            ElseIf CInt(cboFromPeriod.SelectedValue) > 0 And CInt(cboToPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select To Period."), Me)
                cboToPeriod.Focus()
                Exit Function

            ElseIf cboToPeriod.SelectedIndex < cboFromPeriod.SelectedIndex Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, " To Period cannot be less than From Period."), Me)
                cboToPeriod.Focus()
                Exit Function
            End If

            If Not dtpTranFromDate.IsNull Then
                ObjCRSummary._FromDate = dtpTranFromDate.GetDate.Date
            Else
                ObjCRSummary._FromDate = Nothing
            End If
            If Not dtpTranToDate.IsNull Then
                ObjCRSummary._ToDate = dtpTranToDate.GetDate.Date
            Else
                ObjCRSummary._ToDate = Nothing
            End If
            ObjCRSummary._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            ObjCRSummary._ExpCateName = cboExpenseCategory.SelectedItem.Text.ToString()
            ObjCRSummary._EmpUnkId = CInt(cboEmployee.SelectedValue)
            ObjCRSummary._EmpName = cboEmployee.SelectedItem.Text.ToString
            If CInt(cboFromPeriod.SelectedValue) > 0 AndAlso CInt(cboToPeriod.SelectedValue) > 0 Then
                ObjCRSummary._StatusId = cboStatus.SelectedIndex
            Else
                ObjCRSummary._StatusId = CInt(cboStatus.SelectedValue)
            End If
            ObjCRSummary._StatusName = cboStatus.SelectedItem.Text.ToString
            ObjCRSummary._FirstNamethenSurname = ConfigParameter._Object._FirstNamethenSurname
            ObjCRSummary._UserUnkid = CInt(Session("UserId"))
            ObjCRSummary._CompanyUnkId = Company._Object._Companyunkid


            ObjCRSummary._ShowClaimFormStatus = chkShowClaimStatus.Checked


            ObjCRSummary._UOMId = CInt(cboUOM.SelectedValue)
            ObjCRSummary._UOM = cboUOM.SelectedItem.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboFromPeriod.SelectedValue)
            ObjCRSummary._FromPeriodId = CInt(cboFromPeriod.SelectedValue)
            ObjCRSummary._FromPeriod = cboFromPeriod.SelectedItem.Text
            ObjCRSummary._PeriodStartDate = objPeriod._Start_Date.Date

            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboToPeriod.SelectedValue)
            ObjCRSummary._ToPeriodId = CInt(cboToPeriod.SelectedValue)
            ObjCRSummary._ToPeriod = cboToPeriod.SelectedItem.Text
            ObjCRSummary._PeriodEndDate = objPeriod._End_Date.Date
            objPeriod = Nothing

            ObjCRSummary._ExpenseID = CInt(cboExpense.SelectedValue)
            ObjCRSummary._Expense = cboExpense.SelectedItem.Text

            ObjCRSummary._ViewByIds = mstrStringIds
            ObjCRSummary._ViewIndex = mintViewIdx
            ObjCRSummary._ViewByName = mstrStringName
            ObjCRSummary._Analysis_Fields = mstrAnalysis_Fields
            ObjCRSummary._Analysis_Join = mstrAnalysis_Join
            ObjCRSummary._Analysis_OrderBy = mstrAnalysis_OrderBy
            ObjCRSummary._Report_GroupName = mstrReport_GroupName


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            GUI.fmtCurrency = Session("fmtCurrency").ToString()
            'Pinkal (10-Feb-2021) -- End

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            ObjCRSummary._CurrencyId = CInt(cboCurrency.SelectedValue)
            ObjCRSummary._Currency = cboCurrency.SelectedItem.Text
            'Pinkal (30-Mar-2021) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()

            If Not IsPostBack Then
                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Pinkal (05-Sep-2020) -- End
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim ObjCRSummary As New clsClaimRequestsummaryReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (05-Sep-2020) -- End
        Try

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(ObjCRSummary) = False Then Exit Sub
            'Pinkal (05-Sep-2020) -- End


            ObjCRSummary.setDefaultOrderBy(0)

            Call SetDateFormat()

            ObjCRSummary.generateReportNew(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           Session("ExportReportPath").ToString, _
                                           CBool(Session("OpenAfterExport")), _
                                           0, Aruti.Data.enPrintAction.None, enExportAction.None)

            Session("objRpt") = ObjCRSummary._Rpt

            If ObjCRSummary._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            ObjCRSummary = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim objExpMst As New clsExpense_Master
            Dim dsCombo As DataSet = objExpMst.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            cboExpense.DataTextField = "Name"
            cboExpense.DataValueField = "Id"
            cboExpense.DataSource = dsCombo.Tables(0)
            cboExpense.DataBind()
            cboExpense.SelectedValue = 0

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objExpMst = Nothing
            'Pinkal (05-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub cboFromPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromPeriod.SelectedIndexChanged, cboToPeriod.SelectedIndexChanged
        Try
            If cboFromPeriod.SelectedValue > 0 AndAlso cboToPeriod.SelectedValue > 0 Then
                dtpTranFromDate.SetDate = Nothing
                dtpTranToDate.SetDate = Nothing
                dtpTranFromDate.Enabled = False
                dtpTranToDate.Enabled = False
                If cboStatus.DataSource IsNot Nothing Then cboStatus.DataSource = Nothing
                cboStatus.Items.Clear()
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 6, "Select"))
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 7, "Posted"))
                cboStatus.Items.Add(Language.getMessage(mstrModuleName, 8, "Not Posted"))
                cboStatus.SelectedIndex = 0
            Else
                dtpTranFromDate.Enabled = True
                dtpTranToDate.Enabled = True
                If cboStatus.DataSource Is Nothing AndAlso cboStatus.Items.Count > 0 Then cboStatus.Items.Clear()
                FillStatus()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Other Controls Events "
    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

        Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)

        Me.lblTranFromDate.Text = Language._Object.getCaption(Me.lblTranFromDate.ID, Me.lblTranFromDate.Text)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
        Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
        Me.lblTranToDate.Text = Language._Object.getCaption(Me.lblTranToDate.ID, Me.lblTranToDate.Text)
        Me.chkShowClaimStatus.Text = Language._Object.getCaption(Me.chkShowClaimStatus.ID, Me.chkShowClaimStatus.Text)
        Me.LblUOM.Text = Language._Object.getCaption(Me.LblUOM.ID, Me.LblUOM.Text)
        Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.ID, Me.LblExpense.Text)
        Me.LblPostedPeriodFrom.Text = Language._Object.getCaption(Me.LblPostedPeriodFrom.ID, Me.LblPostedPeriodFrom.Text)
        Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.ID, Me.LblTo.Text)

            'Pinkal (30-Mar-2021)-- Start
            'NMB Enhancement  -  Working on Employee Recategorization history Report.
            Me.LblCurrency.Text = Language._Object.getCaption(Me.LblCurrency.ID, Me.LblCurrency.Text)
            'Pinkal (30-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue.")
            Language.setMessage(mstrModuleName, 2, "UOM is compulsory information.please select UOM.")
            Language.setMessage(mstrModuleName, 3, "Please Select From Period.")
            Language.setMessage(mstrModuleName, 4, "Please Select To Period.")
            Language.setMessage(mstrModuleName, 5, " To Period cannot be less than From Period.")
            Language.setMessage(mstrModuleName, 6, "Select")
            Language.setMessage(mstrModuleName, 7, "Posted")
            Language.setMessage(mstrModuleName, 8, "Not Posted")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
