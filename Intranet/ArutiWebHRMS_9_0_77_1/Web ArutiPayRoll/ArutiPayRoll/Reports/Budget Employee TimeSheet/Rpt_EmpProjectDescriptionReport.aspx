﻿<%@ Page Title="Employee Project Code Time Sheet Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_EmpProjectDescriptionReport.aspx.vb" Inherits="Reports_Budget_Employee_TimeSheet_Rpt_EmpProjectDescriptionReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }

      
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 40%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Project Description Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                         <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboStatus" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        </table>
                                        <table style="width: 100%">
                                             <tr style="width: 100%">
                                                <td style="width: 50%">
                                                         <asp:CheckBox ID="chkShowDonor" runat="server" Text="Show Donor" Checked = "true" />
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:CheckBox ID="chkShowStatus" runat="server" Text="Show Status" Checked = "true" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 50%">
                                                        <asp:CheckBox ID="chkShowProject" runat="server" Text="Show Project" Checked = "true" />
                                                </td>
                                                <td style="width: 50%">
                                                    <asp:CheckBox ID="chkShowReportInHtml" runat="server" Text="Show Report In HTML" />
                                                </td>
                                            </tr>
                                             <tr style="width: 100%">
                                                <td style="width: 50%"></td>
                                                <td style="width: 50%;text-align: right; font-weight: bold">
                                                    <asp:LinkButton ID="btnSaveSettings" Text="Save Settings" runat="server" CssClass="lnkhover"></asp:LinkButton>
                                                </td>
                                            </tr>
                                    </table>
                                    <div class="btn-default"><asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
