﻿Option Strict On 'Shani(11-Feb-2016)


#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_MedicalClaim
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objClaim As clsMedicalClaimReport
    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private mstrModuleName As String = "frmMedicalClaimReport"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function "

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim objProvider As New clsinstitute_master
            dsList = objProvider.getListForCombo(True, "List", True, 1)
            With drpProvider
                .DataValueField = "instituteunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            Dim objPeriod As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", , , , , Session("Database_Name"))
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period")
            'Shani(20-Nov-2015) -- End

            With chkPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objClaim.setDefaultOrderBy(0)
            drpProvider.SelectedIndex = 0
            chkSelectallPeriodList.Checked = False
            chkInActiveEmp.Checked = False
            chkMyInvoice.Checked = True
            chkSelectAll_CheckedChanged(New Object(), New EventArgs())
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objClaim.SetDefaultValue()


            If CInt(drpProvider.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please Select atleast one Provider.", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select atleast one Provider."), Me)
                'Pinkal (06-May-2014) -- End
                drpProvider.Focus()
                Return False
            End If

            Dim StrPeriodIds, StrPeriodNames As String
            StrPeriodIds = String.Empty : StrPeriodNames = String.Empty

            For i As Integer = 0 To chkPeriod.Items.Count - 1
                If chkPeriod.Items(i).Selected Then
                    StrPeriodIds &= "," & chkPeriod.Items(i).Value
                    StrPeriodNames &= "," & chkPeriod.Items(i).Text
                End If
            Next

            If StrPeriodIds.Trim.Length <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please Select atleast one Period.", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please Select atleast one Period."), Me)
                'Pinkal (06-May-2014) -- End


                chkPeriod.Focus()
                Return False
            End If

            If StrPeriodIds.Trim.Length > 0 Then StrPeriodIds = Mid(StrPeriodIds, 2)
            If StrPeriodNames.Trim.Length > 0 Then StrPeriodNames = Mid(StrPeriodNames, 2)

            objClaim._ProviderId = CInt(drpProvider.SelectedValue)
            objClaim._ProviderName = drpProvider.SelectedItem.Text
            objClaim._PeriodIds = StrPeriodIds
            objClaim._PeriodName = StrPeriodNames
            objClaim._IncludeInactiveEmp = chkInActiveEmp.Checked
            objClaim._MyInvoiceOnly = chkMyInvoice.Checked

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            objClaim._AsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            'Sohail (18 May 2019) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objClaim = New clsMedicalClaimReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                FillCombo()
                ResetValue()
            End If

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objClaim._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objClaim._UserUnkId = CInt(Session("UserId"))
            objClaim._UserAccessFilter = CStr(Session("AccessLevelFilterString"))


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            'Pinkal (22-Nov-2012) -- End

            objClaim.setDefaultOrderBy(0)



            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objClaim.generateReport(0, enPrintAction.None, enExportAction.None)
            objClaim.generateReportNew(CStr(Session("Database_Name")), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                       CStr(Session("UserAccessModeSetting")), True, _
                                       CStr(Session("ExportReportPath")), _
                                       CBool(Session("OpenAfterExport")), 0, _
                                       enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objClaim._Rpt

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(11-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectallPeriodList.CheckedChanged
        Try
            For i As Integer = 0 To chkPeriod.Items.Count - 1
                chkPeriod.Items(i).Selected = chkSelectallPeriodList.Checked
            Next
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region



    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.LblProvider.Text = Language._Object.getCaption(Me.LblProvider.ID, Me.LblProvider.Text)
            Me.chkSelectallPeriodList.Text = Language._Object.getCaption(Me.chkSelectallPeriodList.ID, Me.chkSelectallPeriodList.Text)
            Me.chkInActiveEmp.Text = Language._Object.getCaption(Me.chkInActiveEmp.ID, Me.chkInActiveEmp.Text)
            Me.chkMyInvoice.Text = Language._Object.getCaption(Me.chkMyInvoice.ID, Me.chkMyInvoice.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End

 
End Class
