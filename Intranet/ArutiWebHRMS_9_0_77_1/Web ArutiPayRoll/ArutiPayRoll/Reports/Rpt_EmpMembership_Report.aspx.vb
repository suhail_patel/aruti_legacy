﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region


Partial Class Reports_Rpt_EmpMembership_Report
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmpMembership As clsEmployeeMembership
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private ReadOnly mstrModuleName As String = "frmEmployeeMembershipReport"
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objEmpType As New clsCommon_Master
            Dim dsList As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With
            objEmp = Nothing

            Dim objMembershipCategory As New clsCommon_Master
            dsList = objMembershipCategory.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "Membershipcategory")
            With cboMembershipCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Membershipcategory")
                .SelectedValue = 0
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpMembership.SetDefaultValue()
            objEmpMembership._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            objEmpMembership._EmployeeName = cboEmployee.SelectedItem.Text
            objEmpMembership._MembershipCategoryUnkid = CInt(cboMembershipCategory.SelectedValue)
            objEmpMembership._MembershipCategoryName = cboMembershipCategory.SelectedItem.Text
            objEmpMembership._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpMembership._UserAccessFilter = Session("AccessLevelFilterString")
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmpMembership._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboMembershipCategory.SelectedValue = 0
            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEmpMembership = New clsEmployeeMembership(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'ENHANCEMENT : LANGUAGE CHANGES
            Call SetLanguage()
            'S.SANDEEP [ 19 JUN 2014 ] -- END

            If Not IsPostBack Then
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    
    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmpMembership._CompanyUnkId = Session("CompanyUnkId")
            objEmpMembership._UserUnkId = Session("UserId")
            objEmpMembership._UserAccessFilter = Session("AccessLevelFilterString")
            objEmpMembership.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpMembership.generateReport(0, enPrintAction.None, enExportAction.None)
            objEmpMembership.generateReportNew(Session("Database_Name"), _
                                               Session("UserId"), _
                                               Session("Fin_year"), _
                                               Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               Session("UserAccessModeSetting"), True, _
                                               Session("ExportReportPath"), _
                                               Session("OpenAfterExport"), _
                                               0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objEmpMembership._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblMemCategory.Text = Language._Object.getCaption(Me.lblMemCategory.ID, Me.lblMemCategory.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

        Me.BtnReport.Text = Language._Object.getCaption(Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
        Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
        Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END

End Class
