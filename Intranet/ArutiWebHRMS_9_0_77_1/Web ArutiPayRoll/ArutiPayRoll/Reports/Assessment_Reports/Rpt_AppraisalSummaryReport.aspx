﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_AppraisalSummaryReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_AppraisalSummaryReport" %>

<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="uc" TagPrefix="numerictext" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Appraisal Summary Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboDepartment" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblCommendableletters" runat="server" Text="Commendable Letters"></asp:Label>
                                            </td>
                                            <td>
                                                <numerictext:uc ID="txtClFrom" runat="server" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblClTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td>
                                                <numerictext:uc ID="txtClto" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblImprovment" runat="server" Text="Letter Of Improvement"></asp:Label>
                                            </td>
                                            <td>
                                                <numerictext:uc ID="txtImpFrom" runat="server" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblImpTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td>
                                                <numerictext:uc ID="txtImpTo" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblWarning" runat="server" Text="Warning"></asp:Label>
                                            </td>
                                            <td>
                                                <numerictext:uc ID="txtWarningFrom" runat="server" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblWarningTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td>
                                                <numerictext:uc ID="txtWarningTo" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td colspan="3">
                                                <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
