﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_OmanAppraisalSummaryReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmOmanAppraisalSummaryReport"
    Private objAppraisalReport As clsAppraisalSummReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtTnAPeriodStartDate As Date = Nothing
    Private mdtTnAPeriodEndDate As Date = Nothing
    Private mdtPYPeriodStartDate As Date = Nothing
    Private mdtPYPeriodEndDate As Date = Nothing
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                              CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                              CStr(Session("UserAccessModeSetting")), _
                                              True, CBool(Session("IsIncludeInactiveEmp")), "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("FinStartDate"), "List", True, 0)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Payroll, 0, Session("Database_Name"), Session("FinStartDate"), "Payroll", True, 0)
            With cboOtherPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Payroll")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        Finally
            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboOtherPeriod.SelectedValue = 0
            mstrAdvanceFilter = ""
            mstrStringIds = String.Empty
            mstrStringName = String.Empty
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Assessment period is mandatory information. Please select Assessment period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboOtherPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, TnA period is mandatory information. Please select TnA period to continue"), Me)
                cboPeriod.Focus()
                Return False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter : " & ex.Message, Me)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            objAppraisalReport = New clsAppraisalSummReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        If Not IsPostBack Then
            Call FillCombo()
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objAppraisalReport.Generate_Appraisal_Summary_Report(mintViewIdx, _
                                                                 mstrAnalysis_Fields, _
                                                                 mstrAnalysis_Join, _
                                                                 mstrAnalysis_OrderBy, _
                                                                 mstrReport_GroupName, _
                                                                 mstrAdvanceFilter, _
                                                                 CInt(cboPeriod.SelectedValue), _
                                                                 CInt(cboEmployee.SelectedValue), _
                                                                 CInt(cboOtherPeriod.SelectedValue), _
                                                                 IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                 False, _
                                                                 mdtTnAPeriodStartDate, mdtTnAPeriodEndDate, _
                                                                 mdtPYPeriodStartDate, mdtPYPeriodEndDate, _
                                                                 UserAccessLevel._AccessLevelFilterString, _
                                                                 chkExcludeInactive.Checked, cboEmployee.SelectedItem.Text, _
                                                                 cboPeriod.SelectedItem.Text, cboOtherPeriod.SelectedItem.Text, _
                                                                 Session("fmtCurrency"), Session("Self_Assign_Competencies"), _
                                                                 chkExcludeZeroRating.Checked, chkExcludeInactive.Text, chkExcludeZeroRating.Text, _
                                                                 Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 CStr(Session("UserAccessModeSetting")), True)

            If objAppraisalReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objAppraisalReport._FileNameAfterExported
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReport_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboOtherPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOtherPeriod.SelectedIndexChanged
        Try
            If CInt(cboOtherPeriod.SelectedValue) <= 0 Then
                objlblDuration.Text = ""
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name")) = CInt(cboOtherPeriod.SelectedValue)

            mdtPYPeriodStartDate = objPeriod._Start_Date.Date
            mdtPYPeriodEndDate = objPeriod._End_Date.Date

            mdtTnAPeriodStartDate = objPeriod._TnA_StartDate.Date
            mdtTnAPeriodEndDate = objPeriod._TnA_EndDate.Date

            objlblDuration.Text = objPeriod._TnA_StartDate.ToShortDateString & " To " & objPeriod._TnA_EndDate.ToShortDateString
            objPeriod = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError("cboOtherPeriod_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
