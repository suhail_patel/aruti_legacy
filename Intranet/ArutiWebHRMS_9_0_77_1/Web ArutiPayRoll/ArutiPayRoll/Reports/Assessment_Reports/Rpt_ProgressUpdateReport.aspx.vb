﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_Assessment_Reports_Rpt_ProgressUpdateReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmProgressUpdateReport"
    Private objProgress As clsProgressUpdateReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private DisplayMessage As New CommonCodes
    Private mdtDisplayCol As DataTable

#End Region

#Region " Private Function "

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 8, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 9, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                mdtDisplayCol = dsList.Tables(0).Copy()

                Dim dCol As New DataColumn
                dCol.ColumnName = "ischeck"
                dCol.DataType = GetType(System.Boolean)
                dCol.DefaultValue = False
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhSelectCol"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhJoin"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhDisplay"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                For Each dtRow As DataRow In mdtDisplayCol.Rows
                    If CInt(dtRow.Item("Id")) = enAllocation.JOBS Then Continue For
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(estm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrstation_master AS estm ON estm.stationunkid = Alloc.stationunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrdepartment_group_master AS edgm ON edgm.deptgroupunkid = Alloc.deptgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edpt.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrdepartment_master AS edpt ON edpt.departmentunkid = Alloc.departmentunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsectiongroup_master AS esgm ON esgm.sectiongroupunkid = Alloc.sectiongroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esec.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsection_master AS esec ON esec.sectionunkid = Alloc.sectionunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eugm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunitgroup_master AS eugm ON eugm.unitgroupunkid = Alloc.unitgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eutm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunit_master AS eutm ON eutm.unitunkid = Alloc.unitunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.TEAM
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(etem.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrteam_master AS etem ON etem.teamunkid = Alloc.teamunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOB_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrjobgroup_master AS ejgm ON ejgm.jobgroupunkid = Jobs.jobgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOBS
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejbm.job_name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrjob_master AS ejbm ON ejbm.jobunkid = Jobs.jobunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASS_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclassgroup_master AS ecgm ON ecgm.classgroupunkid = Alloc.classgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASSES
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecls.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclasses_master AS ecls ON ecls.classesunkid = Alloc.classunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 900
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eggm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrgradegroup_master eggm ON grds.gradegroupunkid = eggm.gradegroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 901
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egdm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrgrade_master egdm ON grds.gradeunkid = egdm.gradeunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 902
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egdl.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrgradelevel_master egdl ON grds.gradelevelunkid = egdl.gradelevelunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.COST_CENTER
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecct.costcentername,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN prcostcenter_master AS ecct ON ecct.costcenterunkid = CC.costcenterunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                    End Select
                Next

                lvDisplayCol.DataSource = mdtDisplayCol
                lvDisplayCol.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim objStatus As New clsProgressUpdateReport(0, 0)
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objStatus.GetProgressStatus("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboStatus.SelectedValue = 0
            mstrAdvanceFilter = ""
            Call Fill_Column_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objProgress.SetDefaultValue()
            If cboPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please select atleast one column to display in report."), Me)
                lvDisplayCol.Focus()
                Return False
            End If

            objProgress._PeriodId = cboPeriod.SelectedValue
            objProgress._PeriodName = cboPeriod.SelectedItem.Text
            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty
            If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count > 0 Then
                iCols = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhSelectCol", False, False)).Text.ToString()).ToArray())
                iJoins = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhJoin", False, False)).Text.ToString()).ToArray())
                iDisplay = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhDisplay", False, False)).Text.ToString()).ToArray())
            End If
            objProgress._SelectedCols = iCols
            objProgress._SelectedJoin = iJoins
            objProgress._DisplayCols = iDisplay
            objProgress._AdvanceFilter = mstrAdvanceFilter
            objProgress._EmployeeId = cboEmployee.SelectedValue
            objProgress._EmployeeName = cboEmployee.SelectedItem.Text
            objProgress._ViewByIds = mstrStringIds
            objProgress._ViewIndex = mintViewIdx
            objProgress._ViewByName = mstrStringName
            objProgress._Analysis_Fields = mstrAnalysis_Fields
            objProgress._Analysis_Join = mstrAnalysis_Join
            objProgress._Analysis_OrderBy = mstrAnalysis_OrderBy
            objProgress._Report_GroupName = mstrReport_GroupName
            objProgress._FirstNamethenSurname = Session("FirstNamethenSurname")
            objProgress._StatusId = CInt(cboStatus.SelectedValue)
            objProgress._StatusName = cboStatus.SelectedItem.Text

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objProgress = New clsProgressUpdateReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call FillCombo()
                Call Fill_Column_List()
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty
            objProgress.ExportProgressUpdate(Session("Database_Name").ToString, _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                             CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)
            If strFileName.Trim.Length <= 0 Then strFileName = objProgress._FileNameAfterExported.Trim
            If strFileName.Contains(".xls") = False Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
            Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
            End If
            Export.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
