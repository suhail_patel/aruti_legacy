﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_EmployeeListAssessorReviewer.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_EmployeeListAssessorReviewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee List Assessor/Reviewer"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 35%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 65%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 35%">                                                
                                            </td>
                                            <td style="width: 65%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                            </td>                                            
                                        </tr>                                        
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
