﻿<%@ Page Title="Assessment Acknowledgement Report" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="Rpt_AssessmentAckReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_AssessmentAckReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtSearchEmp').val().length > 0) {
                $('#<%=dgvEmp.ClientID %> tbody tr').hide();
                $('#<%=dgvEmp.ClientID %> tbody tr:first').show();
                $('#<%=dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmp').val() + '\')').parent().show();
            }
            else if ($('#dgvEmp').val().length == 0) {
                resetFromSearchValue();
            }

            if ($('#<%=dgvEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        $("[id*=ChkAllSelectedEmp]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=ChkSelectedEmp]").live("click", function() {
            debugger;
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllSelectedEmp]", grid);
            var row = $(this).closest("tr")[0];

            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }

        });
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 43%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assessment Acknowledgement Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Style="width: 99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblReportMode" runat="server" Text="Report Mode"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboReportMode" runat="server" Style="width: 99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <input type="text" id="txtSearchEmp" name="txtSearch" placeholder="type search text"
                                                maxlength="50" style="height: 25px; font: 100; width: 96%" onkeyup="FromSearching();" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <div id="Div11" style="width: 100%; height: 200px; overflow: auto">
                                                <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" Width="99%"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" DataKeyNames="assessormasterunkid,assessorname">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelectedEmp" runat="server" />
                                                                <asp:HiddenField ID="hfemployeeunkid" runat="server" Value='<%#eval("assessormasterunkid") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="assessorname" HeaderText="" ReadOnly="true" FooterText="colhEmp" />
                                                        <asp:BoundField DataField="assessormasterunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                            Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblAckStatus" runat="server" Text="Ack. Status"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboAckStatus" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblAppointment" runat="server" Text="Appointment"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboAppointment" runat="server" Style="width: 99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="objlblCaption" runat="server" Text=""></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <div class="ibwm" style="width: 45%">
                                                <uc1:DateCtrl ID="dtpDate1" runat="server" />
                                            </div>
                                            <div class="ibwm" style="width: 7%">
                                                <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 45%">
                                                <uc1:DateCtrl ID="dtpDate2" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" Text="Consider Employee Active Status On Period Date" />
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
