﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Calibration_Report.aspx.vb" Inherits="Reports_Rpt_Calibration_Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }


        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        $("[id*=chkaHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkabox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 70%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assessemt Calibration Report"></asp:Label>
                        </div>
                        <div id="FilterCriteria" class="panel-body-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                <div style="text-align: right">
                                    <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table width="100%">
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </td>
                                        <td style="width: 45%">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 40%" rowspan="4" valign="top">
                                            <asp:Panel ID="Panel3" runat="server" Width="100%" Height="316px" ScrollBars="Auto">
                                                <asp:GridView ID="lvDisplayCol" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" DataKeyNames="objcolhSelectCol,objcolhJoin,objcolhDisplay,Id,Name">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="25">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="Name" HeaderText="Display Column On Report" />
                                                        <asp:BoundField DataField="objcolhSelectCol" HeaderText="objcolhSelectCol" Visible="false" />
                                                        <asp:BoundField DataField="objcolhJoin" HeaderText="objcolhJoin" Visible="false" />
                                                        <asp:BoundField DataField="objcolhDisplay" HeaderText="objcolhDisplay" Visible="false" />
                                                        <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            <asp:Label ID="lblReportPlanning" runat="server" Text="Report Type"></asp:Label>
                                        </td>
                                        <td style="width: 45%">
                                            <asp:DropDownList ID="cboReportType" runat="server" Width="99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </td>
                                        <td style="width: 45%">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 15%" valign="top">
                                            <asp:Label ID="lblProvisionalRating" runat="server" Text="Provisional Rating"></asp:Label>
                                            <asp:Label ID="lblCalibrationRating" runat="server" Text="Calibration Rating"></asp:Label>                                            
                                            <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No"></asp:Label>
                                        </td>
                                        <td style="width: 45%">
                                            <asp:Panel ID="Panel2" runat="server" Width="100%" Height="245px" ScrollBars="Auto">
                                                <asp:GridView ID="dgvRating" runat="server" AutoGenerateColumns="False" Width="99%"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="scrf,scrt,id,calibratnounkid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="25">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkaHeder1" runat="server" Enabled="true" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkabox1" runat="server" Enabled="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating"></asp:BoundField>
                                                        <asp:BoundField DataField="calibration_no" HeaderText="Calibration No" FooterText="dgcolhCalibrationNo">
                                                        </asp:BoundField>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btndefault" />
                                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    <asp:Button ID="BtnReport" runat="server" Text="Export" CssClass="btndefault" />
                                    <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                        <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                        <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
