﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_AssessmentAckReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentAckReport"
    Private objAckReport As clsAssessmentAckReport
    Dim mdtEmployee As DataTable
    Private mdtPeriodStart As Date = Nothing
    Private mdtPeriodEnd As Date = Nothing
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                 Session("UserId"), _
                                                 Session("Fin_year"), _
                                                 Session("CompanyUnkId"), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 Session("UserAccessModeSetting"), True, _
                                                 Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            Dim iCboItems() As ListItem = {New ListItem(Language.getMessage(mstrModuleName, 1, "Select"), 0), _
                                           New ListItem(Language.getMessage(mstrModuleName, 2, "Agree"), clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE), _
                                           New ListItem(Language.getMessage(mstrModuleName, 3, "Disagree"), clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE)}

            With cboAckStatus
                .Items.Clear()
                .Items.AddRange(iCboItems)
                .SelectedIndex = 2
                .SelectedValue = CType(cboAckStatus.SelectedItem, ListItem).Value
            End With

            dsList = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 1
                cboAppointment_SelectedIndexChanged(New Object, New EventArgs)
            End With

            With cboReportMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Assessor"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Reviewer"))
                .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboAckStatus.SelectedIndex = 2
            cboAppointment.SelectedValue = 1
            cboAppointment_SelectedIndexChanged(New Object, New EventArgs)
            cboEmployee.SelectedValue = 0
            cboReportMode.SelectedIndex = 0
            dtpDate1.SetDate = Nothing
            dtpDate2.SetDate = Nothing
            chkConsiderEmployeeTermination.Checked = False
            dgvEmp.DataSource = Nothing
            dgvEmp.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAckReport.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please select Period."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboReportMode.SelectedIndex) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please select Report mode."), Me)
                cboReportMode.Focus()
                Return False
            End If

            objAckReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objAckReport._PeriodName = cboPeriod.Text
            If cboReportMode.SelectedIndex > 0 Then
                objAckReport._ReportModeId = cboReportMode.SelectedIndex
                objAckReport._ReportModeName = cboReportMode.Text
            End If
            If CInt(cboAppointment.SelectedValue) > 0 Then
                objAckReport._AppointmentTypeId = cboAppointment.SelectedValue
                objAckReport._AppointmentTypeName = cboAppointment.Text
            End If
            If dtpDate1.IsNull = False Then objAckReport._Date1 = dtpDate1.GetDate
            If dtpDate2.IsNull = False Then objAckReport._Date2 = dtpDate2.GetDate
            objAckReport._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked
            objAckReport._StatusId = CType(cboAckStatus.SelectedItem, ListItem).Value
            objAckReport._StatusName = CType(cboAckStatus.SelectedItem, ListItem).Text
            objAckReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objAckReport._EmployeeName = cboEmployee.Text

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True)
            If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
                objAckReport._AsrRvrIds = String.Join(",", gRow.Select(Function(y) dgvEmp.DataKeys(y.RowIndex)("assessormasterunkid").ToString()).ToArray())
                objAckReport._AsrRvrNames = String.Join(",", gRow.Select(Function(y) dgvEmp.DataKeys(y.RowIndex)("assessorname").ToString()).ToArray())
            End If
            objAckReport._FirstNamethenSurname = Session("FirstNamethenSurname")
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub FillGrid()
        Dim objAsr As New clsAssessor
        Dim dsList As New DataSet
        Try
            Dim blnIsReviewer As Boolean = False
            Select Case cboReportMode.SelectedIndex
                Case 1  'ASSESSOR
                    blnIsReviewer = False
                Case 2  'REVIEWER
                    blnIsReviewer = True
            End Select
            dsList = objAsr.GetList(Session("Database_Name"), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting"), True, False, "List", blnIsReviewer, clsAssessor.enARVisibilityTypeId.VISIBLE, False, "", False)
            If dsList.Tables(0).Rows.Count > 0 Then
                mdtEmployee = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "employeecode", "assessorname", "assessormasterunkid")
                dgvEmp.DataSource = mdtEmployee
                dgvEmp.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objAsr = Nothing
        End Try
    End Sub

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objAckReport = New clsAssessmentAckReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call FillCombo()
            Else
                mdtPeriodStart = Me.ViewState("mdtStartDate")
                mdtPeriodEnd = Me.ViewState("mdtEndDate")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtStartDate") = mdtPeriodStart
            Me.ViewState("mdtEndDate") = mdtPeriodEnd
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty
            objAckReport.Export_Acknowledgement_Report(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       mdtPeriodStart, _
                                                       mdtPeriodEnd, _
                                                       CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If strFileName.Trim.Length <= 0 Then strFileName = objAckReport._FileNameAfterExported.Trim
            If strFileName.Contains(".xls") = False Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
            Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
            End If
            Export.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet
            Dim dtStartDate, dtEndDate As Date
            dtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            dtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
                dtStartDate = mdtPeriodStart
                dtEndDate = mdtPeriodEnd
            Else
                mdtPeriodStart = Nothing
                mdtPeriodEnd = Nothing
                dtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.SelectedItem.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub cboReportMode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportMode.SelectedIndexChanged
        Try
            If cboReportMode.SelectedIndex > 0 Then
                dgvEmp.Columns(0).HeaderText = cboReportMode.SelectedItem.Text
                Call FillGrid()
            Else
                dgvEmp.DataSource = Nothing
                dgvEmp.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s)"

    Private Sub chkConsiderEmployeeTermination_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConsiderEmployeeTermination.CheckedChanged
        Try
            If chkConsiderEmployeeTermination.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                mdtPeriodStart = objPrd._Start_Date
                mdtPeriodEnd = objPrd._End_Date
                objPrd = Nothing
            Else
                mdtPeriodStart = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                mdtPeriodEnd = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

End Class
