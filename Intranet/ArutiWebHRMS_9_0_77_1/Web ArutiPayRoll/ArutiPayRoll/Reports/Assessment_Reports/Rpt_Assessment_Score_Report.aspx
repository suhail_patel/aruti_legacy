﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Assessment_Score_Report.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_Assessment_Score_Report" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

   <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function checkHeader(control) {
        var chkHeader = control;
        var grid = control.closest("table");
            $("input[type=checkbox]", grid).each(function() {
            if (chkHeader.is(":checked")) {
                debugger;
                $(this).attr("checked", "checked");
                
            } else {
                $(this).removeAttr("checked");
            }
        });
    }
    
        function checkRowCheckbox(control, chkboxID, headerchkboxID) {
        var grid = control.closest("table");
            var chkHeader = $("[id*=" + headerchkboxID + "]", grid);
        var row = control.closest("tr")[0];
        
        debugger;
        if (!control.is(":checked")) {
            var row = control.closest("tr")[0];
            chkHeader.removeAttr("checked");
        } else {
                if ($("[id*=" + chkboxID + "]", grid).length == $("[id*=" + chkboxID + "]:checked", grid).length) {
                chkHeader.attr("checked", "checked");
            }
        }
    }
    
        $("[id*=chkpHeder]").live("click", function() {
        checkHeader($(this));       
});
    
        $("[id*=chkpbox]").live("click", function() {
            checkRowCheckbox($(this), 'chkpbox', 'chkpHeder');
    });
    
    
        $("[id*=chkaHeder]").live("click", function() {
        checkHeader($(this));       
    });
    
        $("[id*=chkabox]").live("click", function() {
            checkRowCheckbox($(this), 'chkabox', 'chkaHeder');
    });
    
        $("[id*=chkHeder]").live("click", function() {
        checkHeader($(this));       
    });
    
        $("[id*=chkbox]").live("click", function() {
            checkRowCheckbox($(this), 'chkbox', 'chkHeder');
    });

        function pageLoad(sender, args) {
            $("select").searchable();
        }
   
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assessment Score Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right; margin-right: 3%;">
                                        <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" TextAlign="Right"
                                            Text="Consider Employee Active Status On Period Date" />
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 35%; height: 465px; vertical-align: top">
                                            <div class="row2">
                                                <div class="ibwm" style="width: 28%">
                                                <asp:Label ID="lblRType" runat="server" Text="Report Type"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 70%">
                                                    <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true" Width="278px">
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 28%">
                                                    <asp:Label ID="lblSubRType" runat="server" Text="Sub Report Type"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 70%">
                                                    <asp:DropDownList ID="cboSubReportType" runat="server" AutoPostBack="true" Width="278px">
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 28%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 70%">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" Width="278px">
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 28%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 70%">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" Width="278px">
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 28%">
                                                <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 70%">
                                                    <asp:DropDownList ID="cboScoreOption" runat="server" Width="278px">
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 28%">
                                                <asp:Label ID="lblAllocation" runat="server" Text="Allocations"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 70%">
                                                    <asp:DropDownList ID="cboAllocations" runat="server" AutoPostBack="true" Width="278px">
                                                </asp:DropDownList>
                                                </div>
                                            </div>
                                            <asp:Panel ID="pnlAllocation" runat="server" Width="100%">
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 100%">
                                                        <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" Width="386px"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 100%">
                                                        <asp:Panel ID="Panel2" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                                            <asp:DataGrid ID="lvAllocation" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="98%">
                                                        <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="25">
                                                                <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkaHeder" runat="server" Enabled="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                            <asp:CheckBox ID="chkabox" runat="server" Enabled="true" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Name" />
                                                            <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="ib" style="width: 28%; height: 465px; vertical-align: top">
                                            <asp:Panel ID="objsp1" runat="server" Width="100%">
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 35%">
                                                        <asp:Label ID="lblYear" runat="server" Text="Financial Year"></asp:Label>
                                                    </div>
                                                    <div class="ibwm" style="width: 60%">
                                                        <asp:DropDownList ID="drpFinancialYear" runat="server" Width="190px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 100%">
                                                        <div class="row2">
                                                            <div class="ibwm" style="width: 100%">
                                                                <asp:Panel ID="Panel4" runat="server" Width="100%" Height="430px" ScrollBars="Auto">
                                                                    <asp:DataGrid ID="dgvPeriod" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                        HeaderStyle-Font-Bold="false" Width="98%">
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderStyle-Width="25px">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkpHeder" runat="server" Enabled="true" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkpbox" runat="server" Enabled="true" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="Name" HeaderText="Period" FooterText="dgcolhPeriod" />
                                                                            <asp:BoundColumn DataField="Periodunkid" HeaderText="objdgcolhPId" Visible="false"
                                                                                FooterText="objdgcolhPId" />
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="ibwm" style="width: 33%; height: 465px;">
                                            <asp:Panel ID="pnlCondition" runat="server" Width="100%">
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 100%">
                                                        <asp:CheckBox ID="chkOption" runat="server" Text="Consider on Probation if App. Date is"
                                                            AutoPostBack="true" />
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 20%">
                                                        <asp:Label ID="lblOverallCondition" runat="server" Text="Condition"></asp:Label>
                                                    </div>
                                                    <div class="ibwm" style="width: 30%">
                                                        <asp:DropDownList ID="cboCondition" runat="server" Width="110px">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="ibwm" style="width: 45%">
                                                        <datectrl:uc ID="dtpAppDate" runat="server" />
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 100%">
                                                        <asp:Panel ID="Panel3" runat="server" Width="100%" Height="365px" ScrollBars="Auto">
                                                            <asp:DataGrid ID="lvDisplayCol" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                HeaderStyle-Font-Bold="false" Width="98%">
                                                        <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="25">
                                                                <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkHeder" runat="server" Enabled="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                            <asp:CheckBox ID="chkbox" runat="server" Enabled="true" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="Name" HeaderText="Display Column On Report" />
                                                                    <asp:BoundColumn DataField="objcolhSelectCol" HeaderText="objcolhSelectCol" Visible="false" />
                                                                    <asp:BoundColumn DataField="objcolhJoin" HeaderText="objcolhJoin" Visible="false" />
                                                                    <asp:BoundColumn DataField="objcolhDisplay" HeaderText="objcolhDisplay" Visible="false" />
                                                            <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 100%">
                                                        <asp:CheckBox ID="chkShowEmpSalary" runat="server" Text="Show Employee Scale On Report" />
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
