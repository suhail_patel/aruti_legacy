﻿<%@ Page Title="Assessment Audit Report" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="Rpt_AssessmentAuditReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_AssessmentAuditReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 43%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assessment Audit Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblRType" runat="server" Text="Report Type"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboReportType" runat="server" Style="width: 99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Style="width: 99%" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboStatus" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblReportMode" runat="server" Text="Report Mode"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboReportMode" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" Text="Consider Employee Active Status On Period Date" />
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
