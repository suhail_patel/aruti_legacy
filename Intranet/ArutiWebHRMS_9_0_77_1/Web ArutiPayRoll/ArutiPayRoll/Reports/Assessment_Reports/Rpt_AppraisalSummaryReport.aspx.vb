﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_AppraisalSummaryReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAppraisalSummReport"
    Private objAppraisalReport As clsAppraisalSummReport
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objDept As New clsDepartment
            Dim dsList As DataSet = objDept.getComboList("Department", True)
            With cboDepartment
                .DataTextField = "name"
                .DataValueField = "departmentunkid"
                .DataSource = dsList.Tables("Department")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboDepartment.SelectedValue = 0
            txtClFrom.Text = ""
            txtClto.Text = ""
            txtImpFrom.Text = ""
            txtImpTo.Text = ""
            txtWarningFrom.Text = ""
            txtWarningTo.Text = ""
            chkIncludeInactiveEmp.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue : " & ex.Message, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAppraisalReport.SetDefaultValue()

            objAppraisalReport._YearId = CInt(Session("Fin_year"))
            objAppraisalReport._DepartmentId = CInt(cboDepartment.SelectedValue)
            objAppraisalReport._DepartmentName = cboDepartment.SelectedItem.Text
            If txtClFrom.Text.Trim.Length > 0 Then
                objAppraisalReport._CLFrom = CInt(txtClFrom.Text)
            End If
            If txtClto.Text.Trim.Length > 0 Then
                objAppraisalReport._CLTo = CInt(txtClto.Text)
            End If
            If txtImpFrom.Text.Trim.Length > 0 Then
                objAppraisalReport._ImpFrom = CInt(txtImpFrom.Text)
            End If
            If txtImpTo.Text.Length > 0 Then
                objAppraisalReport._ImpTo = CInt(txtImpTo.Text)
            End If
            If txtWarningFrom.Text.Trim.Length > 0 Then
                objAppraisalReport._WarningFrom = CInt(txtWarningFrom.Text)
            End If
            If txtWarningTo.Text.Trim.Length > 0 Then
                objAppraisalReport._WarningTo = CInt(txtWarningTo.Text)
            End If
            objAppraisalReport._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter : " & ex.Message, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            objAppraisalReport = New clsAppraisalSummReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        If Not IsPostBack Then
            Call FillCombo()
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            Call SetDateFormat()
            objAppraisalReport._Userunkid = Session("UserId")
            objAppraisalReport.generateReportNew(Session("Database_Name").ToString, _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                 CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                 CStr(Session("ExportReportPath")), _
                                                 CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                                 enExportAction.None, _
                                                 CInt(Session("Base_CurrencyId")))

            Session("objRpt") = objAppraisalReport._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReport_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
