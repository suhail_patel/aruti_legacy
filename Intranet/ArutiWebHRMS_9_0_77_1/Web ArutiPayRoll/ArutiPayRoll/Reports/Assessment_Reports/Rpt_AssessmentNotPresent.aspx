<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_AssessmentNotPresent.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_AssessmentNotPresent" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assessment Not Done"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblViewMode" runat="server" Text="View Mode"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboViewMode" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblReportMode" runat="server" Text="Report Mode"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboReportMode" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblAppointment" runat="server" Text="Appointment"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:DropDownList ID="cboAppointment" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="objlblCaption" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td style="width: 18%">
                                                <datectrl:uc ID="dtpDate1" runat="server" />
                                            </td>
                                            <td style="width: 6%">
                                                <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 18%">
                                                <datectrl:uc ID="dtpDate2" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="4">
                                                <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" Text="Consider Employee Active Status On Period Date" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
