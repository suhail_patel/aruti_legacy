﻿<%@ Page Title="Progress Update Report" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="Rpt_ProgressUpdateReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_ProgressUpdateReport" %>

<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        $("[id*=chkHeder]").live("click", function() {
            checkHeader($(this));
        });

        $("[id*=chkbox]").live("click", function() {
            checkRowCheckbox($(this), 'chkbox', 'chkHeder');
        });

        function pageLoad(sender, args) {
            $("select").searchable();
        }

        function checkHeader(control) {
            var chkHeader = control;
            var grid = control.closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        }

        function checkRowCheckbox(control, chkboxID, headerchkboxID) {
            var grid = control.closest("table");
            var chkHeader = $("[id*=" + headerchkboxID + "]", grid);
            var row = control.closest("tr")[0];

            debugger;
            if (!control.is(":checked")) {
                var row = control.closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {
                if ($("[id*=" + chkboxID + "]", grid).length == $("[id*=" + chkboxID + "]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 43%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Progress Update Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right;">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" Font-Underline="false"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 20%" class="ib">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </div>
                                        <div style="width: 70%" class="ib">
                                            <asp:DropDownList ID="cboStatus" runat="server" Style="width: 99%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 90%" class="ib">
                                            <asp:Panel ID="Panel3" runat="server" Width="100%" Height="250px" ScrollBars="Auto">
                                                <asp:DataGrid ID="lvDisplayCol" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="98%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="25">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder" runat="server" Enabled="true" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox" runat="server" Enabled="true" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="Name" HeaderText="Display Column On Report" />
                                                        <asp:BoundColumn DataField="objcolhSelectCol" HeaderText="objcolhSelectCol" Visible="false" />
                                                        <asp:BoundColumn DataField="objcolhJoin" HeaderText="objcolhJoin" Visible="false" />
                                                        <asp:BoundColumn DataField="objcolhDisplay" HeaderText="objcolhDisplay" Visible="false" />
                                                        <asp:BoundColumn DataField="Id" HeaderText="Id" Visible="false" />
                                                    </Columns>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnAdvFilter" runat="server" Text="Advance Filter" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc7:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
