﻿Option Strict On 'Shani(11 Feb 2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_EmployeeLeaveBalanceReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objEmpLeaveBalanceSummary As clsEmployeeLeaveBalanceListReport
    'Pinkal (11-Sep-2020) -- End

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmEmployeeLeaveBalanceList"
    'Pinkal (06-May-2014) -- End

    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise


            'If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    '    dsList = objEmployee.GetList("Employee", False, True)
            '    'Else
            '    '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            '    'End If
            '    dsList = objEmployee.GetList(Session("Database_Name").ToString(), _
            '                                 CInt(Session("UserId")), _
            '                                 CInt(Session("Fin_year")), _
            '                                 CInt(Session("CompanyUnkId")), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                 Session("UserAccessModeSetting").ToString(), True, _
            '                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
            '                                 CBool(Session("ShowFirstAppointmentDate")))
            '    'Shani(24-Aug-2015) -- End

            '    Dim dRow As DataRow = dsList.Tables(0).NewRow
            '    dRow("employeeunkid") = 0
            '    dRow("name") = "Select"
            '    dsList.Tables(0).Rows.InsertAt(dRow, 0)

            'ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'dsList = objEmployee.GetList("Employee", False, True, , , CInt(Session("Employeeunkid")), Session("AccessLevelFilterString"))
            '    dsList = objEmployee.GetList(Session("Database_Name").ToString(), _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                      Session("UserAccessModeSetting").ToString, True, _
            '                                      CBool(Session("IsIncludeInactiveEmp")), "Employee", _
            '                                      CBool(Session("ShowFirstAppointmentDate")), _
            '                                      CInt(Session("Employeeunkid")), , _
            '                                      "")
            '    'Shani(24-Aug-2015) -- End


            'End If

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)
            'Shani(11-Feb-2016) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing

            cboReportType.Items.Clear()
            Language.setLanguage(mstrModuleName)
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Leave Type Wise "))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Employee Wise "))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "Employee Without Leave Accrue"))

            Dim objLeave As New clsleavetype_master

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    dsList = objLeave.getListForCombo("Leave", False, , Session("Database_Name").ToString, "", True)
            'Else
            '    dsList = objLeave.getListForCombo("Leave", False, , Session("Database_Name").ToString, "", False)
            'End If
                dsList = objLeave.getListForCombo("Leave", False, , Session("Database_Name").ToString, "", False)
            'Pinkal (06-Dec-2019) -- End
            'Pinkal (25-May-2019) -- End


            With chkLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeave = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try
            If chkLeaveType.SelectedItem Is Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please Select atleast one Leave Type."), Me)
                Return False
            End If

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation", Me)
            DisplayMessage.DisplayError("Validation", Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Public Sub ResetValue()
        Try
            dtDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedIndex = 0
            cboReportType.SelectedIndex = 0

            chkSelectAll.Checked = True
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Call chkSelect_CheckedChanged(chkSelectAll, New System.EventArgs)
            chkInactiveemp.Checked = False
            'Nilay (01-Feb-2015) -- End


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC AndAlso chkIncludeCloseELC.Visible Then
                chkIncludeCloseELC.Checked = False
            End If
            'Pinkal (21-Jul-2014) -- End


            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objEmpLeaveBalanceSummary As clsEmployeeLeaveBalanceListReport) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objEmpLeaveBalanceSummary.SetDefaultValue()
            objEmpLeaveBalanceSummary._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEmpLeaveBalanceSummary._EmployeeName = cboEmployee.SelectedItem.Text
            objEmpLeaveBalanceSummary._LeaveTypeIds = GetLeaveType()
            objEmpLeaveBalanceSummary._ReportId = cboReportType.SelectedIndex
            objEmpLeaveBalanceSummary._ReportTypeName = cboReportType.SelectedItem.Text
            objEmpLeaveBalanceSummary._Date = dtDate.GetDate.Date
            objEmpLeaveBalanceSummary._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpLeaveBalanceSummary._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            'objEmpLeaveBalanceSummary._IncludeInactiveEmp = chkInactiveemp.Checked
            objEmpLeaveBalanceSummary._ViewByIds = mstrStringIds
            objEmpLeaveBalanceSummary._ViewIndex = mintViewIdx
            objEmpLeaveBalanceSummary._ViewByName = mstrStringName
            objEmpLeaveBalanceSummary._Analysis_Fields = mstrAnalysis_Fields
            objEmpLeaveBalanceSummary._Analysis_Join = mstrAnalysis_Join
            objEmpLeaveBalanceSummary._Analysis_OrderBy = mstrAnalysis_OrderBy
            objEmpLeaveBalanceSummary._Report_GroupName = mstrReport_GroupName


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            objEmpLeaveBalanceSummary._IncludeCloseELC = chkIncludeCloseELC.Checked
            'Pinkal (21-Jul-2014) -- End


            'Pinkal (25-Jul-2015) -- Start
            'Enhancement :Getting Problem in showing Leave Balance in Payslip in ELC Setting For (CIT) 
            objEmpLeaveBalanceSummary._DBStartdate = CDate(Session("fin_startdate")).Date
            objEmpLeaveBalanceSummary._DBEnddate = CDate(Session("fin_enddate")).Date
            'Pinkal (25-Jul-2015) -- End


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objEmpLeaveBalanceSummary._IncludeAccessFilterQry = False
            'Pinkal (06-Jan-2016) -- End


            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub SetLeaveType(ByVal blnchecked As Boolean)
        Try
            If chkLeaveType.Items.Count > 0 Then

                For i As Integer = 0 To chkLeaveType.Items.Count - 1
                    chkLeaveType.Items(i).Selected = blnchecked
                Next
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLeaveType :-  " & ex.Message, Me)
            DisplayMessage.DisplayError("SetLeaveType :-  " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function GetLeaveType() As String
        Dim mstrIDs As String = ""
        Try
            If chkLeaveType.Items.Count > 0 Then

                For i As Integer = 0 To chkLeaveType.Items.Count - 1
                    If chkLeaveType.Items(i).Selected = True Then
                        mstrIDs &= chkLeaveType.Items(i).Value & ","
                    End If
                Next
            End If

            If mstrIDs.Trim.Length > 0 Then
                mstrIDs = mstrIDs.Substring(0, mstrIDs.Trim.Length - 1)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetLeaveType :-  " & ex.Message, Me)
            DisplayMessage.DisplayError("GetLeaveType :-  " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return mstrIDs
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objEmpLeaveBalanceSummary = New clsEmployeeLeaveBalanceListReport
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                dtDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                chkSelectAll.Checked = True
                chkSelect_CheckedChanged(New Object, New EventArgs())
                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                '    chkIncludeCloseELC.Visible = True
                '    chkIncludeCloseELC.Checked = False
                'Else
                '    chkIncludeCloseELC.Visible = False
                '    chkIncludeCloseELC.Checked = False
                'End If
                'Pinkal (21-Jul-2014) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
            Else
                mstrStringIds = CStr(Me.ViewState("mstrStringIds"))
                mstrStringName = CStr(Me.ViewState("mstrStringName"))
                mintViewIdx = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysis_Fields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysis_Join = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysis_OrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReport_GroupName = CStr(Me.ViewState("mstrReport_GroupName"))
                'S.SANDEEP |17-MAR-2020| -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try          'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'S.SANDEEP |17-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrStringIds
            Me.ViewState("mstrStringName") = mstrStringName
            Me.ViewState("mintViewIdx") = mintViewIdx
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysis_Fields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysis_Join
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysis_OrderBy
            Me.ViewState("mstrReport_GroupName") = mstrReport_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |17-MAR-2020| -- END
#End Region

#Region " Button's Event(s) "

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objEmpLeaveBalanceSummary As New clsEmployeeLeaveBalanceListReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try
            If Validation() Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'If SetFilter() = False Then Exit Sub
                If SetFilter(objEmpLeaveBalanceSummary) = False Then Exit Sub
                'Pinkal (11-Sep-2020) -- End


                objEmpLeaveBalanceSummary._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objEmpLeaveBalanceSummary._UserUnkId = CInt(Session("UserId"))
                objEmpLeaveBalanceSummary._UserAccessFilter = Session("AccessLevelFilterString").ToString()
                FinancialYear._Object._YearUnkid = CInt(Session("Fin_year"))
                objEmpLeaveBalanceSummary.setDefaultOrderBy(0)

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                'If cboReportType.SelectedIndex <= 1 Then
                '    objEmpLeaveBalanceSummary.generateReportNew(Session("Database_Name"), _
                '                                                Session("UserId"), _
                '                                                Session("Fin_year"), _
                '                                                Session("CompanyUnkId"), _
                '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                Session("UserAccessModeSetting"), True, _
                '                                                Session("ExportReportPath"), _
                '                                                Session("OpenAfterExport"), _
                '                                                0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
                'ElseIf cboReportType.SelectedIndex > 1 Then
                '    objEmpLeaveBalanceSummary.generateReportNew(Session("Database_Name"), _
                '                                                Session("UserId"), _
                '                                                Session("Fin_year"), _
                '                                                Session("CompanyUnkId"), _
                '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                Session("UserAccessModeSetting"), True, _
                '                                                Session("ExportReportPath"), _
                '                                                Session("OpenAfterExport"), _
                '                                                cboReportType.SelectedIndex, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
                'End If


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'Pinkal (01-Dec-2016) -- Start
                'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
                If CInt(Session("Employeeunkid")) > 0 Then
                    objEmpLeaveBalanceSummary._UserName = Session("DisplayName").ToString()
                End If
                'Pinkal (01-Dec-2016) -- End


                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                objEmpLeaveBalanceSummary._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
                objEmpLeaveBalanceSummary._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
                'Pinkal (16-Dec-2016) -- End

                objEmpLeaveBalanceSummary.generateReportNew(Session("Database_Name").ToString(), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            dtDate.GetDate.Date, _
                                                            dtDate.GetDate.Date, _
                                                            Session("UserAccessModeSetting").ToString(), True, _
                                                            Session("ExportReportPath").ToString(), _
                                                            CBool(Session("OpenAfterExport")), _
                                                            cboReportType.SelectedIndex, enPrintAction.None, enExportAction.None, _
                                                            CInt(Session("Base_CurrencyId")))

                'Shani(11-Feb-2016) -- End

                Session("objRpt") = objEmpLeaveBalanceSummary._Rpt
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                If Session("objRpt") IsNot Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
                'Shani(11-Feb-2016) -- End


                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmpLeaveBalanceSummary = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            SetLeaveType(chkSelectAll.Checked)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelect_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'S.SANDEEP |17-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
#Region " Link Event(s) "

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            popupAnalysisBy._EffectiveDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |17-MAR-2020| -- END

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.Title = Language._Object.getCaption(mstrModuleName, objEmpLeaveBalanceSummary._ReportName)
            'Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objEmpLeaveBalanceSummary._ReportName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Pinkal (11-Sep-2020) -- End

            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.ID, Me.LblReportType.Text)
            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblLeaveType.Text = Language._Object.getCaption(Me.LblLeaveType.ID, Me.LblLeaveType.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.ID, Me.chkSelectAll.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub


End Class
