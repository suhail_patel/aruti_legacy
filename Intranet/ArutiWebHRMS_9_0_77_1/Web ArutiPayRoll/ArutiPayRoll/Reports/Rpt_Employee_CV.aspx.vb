﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_Employee_CV
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmpCV As clsEmp_PersonalParticular_Report


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeCVReport"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try


            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes

            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

           'S.SANDEEP [ 28 NOV 2013 ] -- START
            'dsList = ObjEmp.GetEmployeeList("Emp", True, False)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, False, , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", False, False, CInt(Session("Employeeunkid")), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            'Nilay (05-May-2016) -- Start
            'Dim blnSelect As Boolean = True
            'Dim intEmpId As Integer = 0
            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    blnSelect = False
            '    intEmpId = CInt(Session("Employeeunkid"))
            'End If
            'dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
            '                                Session("UserId"), _
            '                                Session("Fin_year"), _
            '                                Session("CompanyUnkId"), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                Session("UserAccessModeSetting"), True, _
            '                                Session("IsIncludeInactiveEmp"), "Emp", blnSelect, intEmpId)

            ''Shani(24-Aug-2015) -- End

            ''S.SANDEEP [ 28 NOV 2013 ] -- END

            ''Pinkal (08-Jun-2013) -- End
            'With drpemployee
            '    .DataValueField = "employeeunkid"
            '    .DataTextField = "employeename"
            '    .DataSource = dsList.Tables("Emp")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsList = ObjEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                CStr(Session("UserAccessModeSetting")), _
                                                True, CBool(Session("IsIncludeInactiveEmp")), _
                                                "EmpList")

                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("employeeunkid") = 0
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                'dRow.Item("employeename") = "Select"
                dRow.Item("EmpCodeName") = "Select"
                'Nilay (09-Aug-2016) -- End
                dsList.Tables(0).Rows.InsertAt(dRow, 0)
			
            With drpemployee
                .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables(0)
                .DataBind()
                    .SelectedValue = "0"
            End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpemployee.DataSource = objglobalassess.ListOfEmployee
                drpemployee.DataTextField = "loginname"
                drpemployee.DataValueField = "employeeunkid"
                drpemployee.DataBind()
            End If
            'Nilay (05-May-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpemployee.SelectedValue = 0
            chkDisplayHistroyInfo.Checked = True
            chkDisplayQualificationInfo.Checked = True
            chkDisplayTrainingInfo.Checked = True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objEmpCV.SetDefaultValue()

            If CInt(drpemployee.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select employee to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpemployee.Focus()
                Return False
            End If

            objEmpCV._DisplayHistroyInfo = chkDisplayHistroyInfo.Checked
            objEmpCV._DisplayQualificationInfo = chkDisplayQualificationInfo.Checked
            objEmpCV._DisplayTrainingInfo = chkDisplayTrainingInfo.Checked

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objEmpCV._EmployeeName = drpemployee.Text
            objEmpCV._EmployeeName = drpemployee.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            objEmpCV._EmployeeUnkid = drpemployee.SelectedValue

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If


            objEmpCV = New clsEmp_PersonalParticular_Report(enArutiReport.EmployeeCVReport, CInt(Session("LangId")), CInt(Session("CompanyUnkId")))


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                'For i As Integer = 0 To objAgeAnalysis.Field_OnDetailReport.Count - 1
                '    chkSort.Items.Add(objAgeAnalysis.Field_OnDetailReport.ColumnItem(i).DisplayName)
                'Next
                'chkSort.Items(0).Selected = True

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                chkDisplayHistroyInfo.Checked = True
                chkDisplayQualificationInfo.Checked = True
                chkDisplayTrainingInfo.Checked = True

                'Nilay (01-Feb-2015) -- End

                
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEmpCV.setDefaultOrderBy(0)
            objEmpCV._CompanyUnkId = Session("CompanyUnkId")
            objEmpCV._UserUnkId = Session("UserId")

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Pinkal (01-Dec-2016) -- Start
            'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
            If CInt(Session("Employeeunkid")) > 0 Then
                objEmpCV._UserName = Session("DisplayName").ToString()
            End If
            'Pinkal (01-Dec-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmpCV.generateReport(0, enPrintAction.None, enExportAction.None)
            objEmpCV.generateReportNew(Session("Database_Name"), _
                                       Session("UserId"), _
                                       Session("Fin_year"), _
                                       Session("CompanyUnkId"), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                       Session("UserAccessModeSetting"), True, _
                                       Session("ExportReportPath"), _
                                       Session("OpenAfterExport"), _
                                       0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objEmpCV._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    
    'Shani [ 24 DEC 2014 ] -- END
#End Region


    Protected Sub drpemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpemployee.DataBound
        Try
        If drpemployee.Items.Count > 0 Then
            For Each lstItem As ListItem In drpemployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            drpemployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.chkDisplayTrainingInfo.Text = Language._Object.getCaption(Me.chkDisplayTrainingInfo.ID, Me.chkDisplayTrainingInfo.Text)
            Me.chkDisplayQualificationInfo.Text = Language._Object.getCaption(Me.chkDisplayQualificationInfo.ID, Me.chkDisplayQualificationInfo.Text)
            Me.chkDisplayHistroyInfo.Text = Language._Object.getCaption(Me.chkDisplayHistroyInfo.ID, Me.chkDisplayHistroyInfo.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (06-May-2014) -- End



End Class
