﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Rpt_Employee_Distribution
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objDistribution As clsEmployeeDistributionReport
    Dim StrFinalPath As String = String.Empty

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeDistributionReport"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            dsCombos = objDistribution.GetDistributionBy("List")
            With drpDistribution
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = 1
            End With

            '--------- Year
            dsCombos = objDistribution.GetYearList("List", CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")))
            With drpyear
                .DataTextField = "Years"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            '--------- Month
            dsCombos = objDistribution.GetMonthList("List")
            With drpmonth
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpDistribution.SelectedValue = 1
            drpmonth.SelectedValue = 0
            chkViewBy.Checked = True
            chkInactiveemp.Checked = False
            dtasondate.SetDate = Nothing
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Call chkReportbyDate_CheckedChanged(chkViewBy, New System.EventArgs)
            'Nilay (01-Feb-2015) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtasondate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End

            objDistribution.SetDefaultValue()

            If chkViewBy.Checked = False Then
                If drpmonth.SelectedValue <= 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Month is mandatory information. Please select month to continue."), Me)
                    'Pinkal (06-May-2014) -- End
                    drpmonth.Focus()
                    Return False
                End If
            End If

            objDistribution._DistributionId = drpDistribution.SelectedValue

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objDistribution._DistributionName = drpDistribution.Text
            objDistribution._DistributionName = drpDistribution.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objDistribution._YearName = drpyear.Text
            objDistribution._YearName = drpyear.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End

            objDistribution._MonthId = drpmonth.SelectedValue
            objDistribution._MonthName = drpmonth.SelectedItem.Text
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'objDistribution._AsOnDate = dtasondate.GetDate.Date

            'If chkViewBy.Checked = True Then
            '    objDistribution._ViewTypeId = 1    'Date   
            'Else
            '    objDistribution._ViewTypeId = 2    'Month
            'End If
            'objDistribution._IsActive = chkInactiveemp.Checked

            Dim iDate As DateTime = Nothing
            Dim iMonth As String = CInt(drpmonth.SelectedValue)
            If chkViewBy.Checked = False Then
                If CInt(drpmonth.SelectedValue) < 10 Then
                    iMonth = CInt(drpmonth.SelectedValue).ToString("0#")
                End If
                iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(drpyear.SelectedItem.Text & iMonth & "01"))).AddDays(-1)
                objDistribution._EmplType_Date = eZeeDate.convertDate(iDate)
            End If


            If chkViewBy.Checked = True Then
                If Date.DaysInMonth(dtasondate.GetDate.Date.Year, dtasondate.GetDate.Month) = dtasondate.GetDate.Date.Day Then
                    Dim dt As Date = Nothing
                    If dtasondate.GetDate.Date.Month = 1 Then
                        dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dtasondate.GetDate.Date.Year - 1 & Format(12, "0#") & "01"))).AddDays(-1)
                        iDate = dt
                    Else
                        dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dtasondate.GetDate.Date.Year & Format(dtasondate.GetDate.Date.Month, "0#") & "01"))).AddDays(-1)
                        iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dt.Date.Year & CInt(dt.Date.Month - 1).ToString("0#") & "01"))).AddDays(-1)
                    End If

                    objDistribution._ViewTypeId = 2
                    objDistribution._MonthId = dtasondate.GetDate.Month
                    objDistribution._MonthName = MonthName(dtasondate.GetDate.Date.Month)
                    objDistribution._YearName = dtasondate.GetDate.Year
                    objDistribution._EmplType_Date = eZeeDate.convertDate(dtasondate.GetDate)
                Else
                objDistribution._ViewTypeId = 1    'Date   
                End If

                objDistribution._AsOnDate = IIf(iDate = Nothing, dtasondate.GetDate.Date, iDate)
            Else
                Dim dt As Date = Nothing
                If drpmonth.SelectedValue = 1 Then
                    dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(CInt(drpyear.Text) - 1 & Format(12, "0#") & "01"))).AddDays(-1)
                    iDate = dt
            Else
                    dt = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(CInt(drpyear.Text) & iMonth & "01"))).AddDays(-1)
                    iDate = DateAdd(DateInterval.Month, 1, CDate(eZeeDate.convertDate(dt.Date.Year & Format(dt.Date.Month - 1, "0#") & "01"))).AddDays(-1)
                End If


                objDistribution._MonthId = drpmonth.SelectedValue 'dt.Month
                objDistribution._MonthName = drpmonth.SelectedItem.Text
                objDistribution._YearName = drpyear.SelectedItem.Text
                objDistribution._ViewTypeId = 2    'Month
                objDistribution._AsOnDate = iDate
            End If
            objDistribution._IsActive = chkInactiveemp.Checked

            'Shani(24-Aug-2015) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objDistribution._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'Hemant (13 Aug 2020) -- Start
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (13 Aug 2020) -- End
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Function Export_to_Excel(ByVal flFileName As String, ByVal SavePath As String, ByVal objDataReader As System.Data.DataTable) As Boolean
        Dim strBuilder As New StringBuilder
        Dim blnFlag As Boolean = False
        Try

            ConfigParameter._Object._Companyunkid = Session("CompanyUnkId")
            Company._Object._Companyunkid = Session("CompanyUnkId")
            Aruti.Data.User._Object._Userunkid = Session("UserId")

            'HEADER PART

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'strBuilder.Append(" <TITLE>" & Closebotton1.PageHeading & " [ " & drpDistribution.SelectedItem.Text & " ]" & "</TITLE> " & vbCrLf)
            strBuilder.Append(" <TITLE>" & lblPageHeader.Text & " [ " & drpDistribution.SelectedItem.Text & " ]" & "</TITLE> " & vbCrLf)
            'Nilay (01-Feb-2015) -- End


            strBuilder.Append(" <BODY><FONT FACE =VERDANA FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=0 WIDTH=140%> " & vbCrLf)

            ConfigParameter._Object.GetReportSettings(CInt(enArutiReport.EmployeeDistributionReport))

            If ConfigParameter._Object._IsDisplayLogo = True Then
                strBuilder.Append(" <TR Width = 90 HEIGHT = 100> " & vbCrLf)
                strBuilder.Append(" <TD WIDTH='10%' align='left' HEIGHT = '20%'> " & vbCrLf)
                Dim objAppSett As New clsApplicationSettings
                Dim strImPath As String = objAppSett._ApplicationPath & "Data\Images\Logo.jpg"
                objAppSett = Nothing
                If Company._Object._Image IsNot Nothing Then
                    Company._Object._Image.Save(strImPath)
                End If

                strBuilder.Append(" <img SRC=""" & strImPath & """ ALT="""" HEIGHT = 100 WIDTH =100 />" & vbCrLf)
                strBuilder.Append(" </TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)
            End If

            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><b>Prepared By : </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%' align='left' ><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Session("UserName") & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='60%' colspan=15 align='center' > " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><B> " & Session("CompName") & " </B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR valign='middle'> " & vbCrLf)
            strBuilder.Append(" <TD width='10%'> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=2><B>" & "Date :" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='35' align='left'><FONT SIZE=2> " & vbCrLf)
            strBuilder.Append(Now.Date & vbCrLf)
            strBuilder.Append(" </FONT></TD> " & vbCrLf)
            strBuilder.Append(" <TD width='60%' colspan=15  align='center' > " & vbCrLf)


            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'strBuilder.Append(" <FONT SIZE=3><b> " & Closebotton1.PageHeading & " [ " & drpDistribution.SelectedItem.Text & " ]" & "</B></FONT> " & vbCrLf)
            strBuilder.Append(" <FONT SIZE=3><b> " & lblPageHeader.Text & " [ " & drpDistribution.SelectedItem.Text & " ]" & "</B></FONT> " & vbCrLf)
            'Nilay (01-Feb-2015) -- End




            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" <TD WIDTH='10%'> " & vbCrLf)
            strBuilder.Append(" &nbsp; " & vbCrLf)
            strBuilder.Append(" </TD> " & vbCrLf)
            strBuilder.Append(" </TR> " & vbCrLf)
            strBuilder.Append(" <TR> " & vbCrLf)
            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" <HR> " & vbCrLf)
            strBuilder.Append(" <B> " & objDistribution._FilterTitle & " </B><BR> " & vbCrLf)
            strBuilder.Append(" </HR> " & vbCrLf)
            strBuilder.Append(" <BR> " & vbCrLf)
            strBuilder.Append(" <TABLE BORDER=1 BORDERCOLOR =BLACK CELLSPACING =0 CELLPADDING =3 WIDTH=140%> " & vbCrLf)
            strBuilder.Append(" <TR ALIGN = CENTER VALIGN=TOP> " & vbCrLf)

            'Report Column Caption
            For j As Integer = 0 To objDataReader.Columns.Count - 1
                strBuilder.Append("<TD BORDER=1 WIDTH='60%' ALIGN='MIDDLE'><FONT SIZE=2><B>" & objDataReader.Columns(j).Caption & "</B></FONT></TD>" & vbCrLf)
            Next

            strBuilder.Append(" </TR> " & vbCrLf)

            'Data Part
            For i As Integer = 0 To objDataReader.Rows.Count - 1
                strBuilder.Append(" <TR> " & vbCrLf)
                For k As Integer = 0 To objDataReader.Columns.Count - 1
                    If k <= 1 Then
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    Else
                        strBuilder.Append("<TD BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2>" & objDataReader.Rows(i)(k) & "</FONT></TD>" & vbCrLf)
                    End If

                Next
                strBuilder.Append(" </TR> " & vbCrLf)
            Next

            strBuilder.Append(" <TR> " & vbCrLf)
            For k As Integer = 0 To objDataReader.Columns.Count - 1
                If k = 0 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & "Grand Total :" & "</B></FONT></TD>" & vbCrLf)
                ElseIf k <= 1 Then
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & "" & "</B></FONT></TD>" & vbCrLf)
                Else
                    strBuilder.Append("<TD  BORDER=1 BORDERCOLOR=BLACK ALIGN='RIGHT'><FONT SIZE=2><B>" & Format(CDec(objDistribution._dclColTot(k)), GUI.fmtCurrency) & "</B></FONT></TD>" & vbCrLf)
                End If
            Next
            strBuilder.Append(" </TR>  " & vbCrLf)

            strBuilder.Append(" </TABLE> " & vbCrLf)
            strBuilder.Append(" </HTML> " & vbCrLf)


            'Pinkal (10-Sep-2012) -- Start
            'Enhancement : TRA Changes

            'If SaveExcelfile(Server.MapPath("~/images/" & flFileName), strBuilder) Then
            '    Session("ExFileName") = Server.MapPath("~/images/" & flFileName)

            Dim StrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            If StrPath.Trim.Length > 0 Then
                SaveExcelfile(StrPath.Trim & "\" & flFileName, strBuilder)
                Session("ExFileName") = StrPath.Trim & "\" & flFileName

                'Shani [ 18 NOV 2015 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)

                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End


                'Shani [ 18 NOV 2014 ] -- END

                Return True
            Else
                Return False
            End If

            'Pinkal (10-Sep-2012) -- End

            Return blnFlag
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Export_to_Excel :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Export_to_Excel :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Function SaveExcelfile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New FileStream(fpath, FileMode.Create, FileAccess.Write)
        Dim strWriter As New StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SaveExcelfile:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SaveExcelfile:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objDistribution = New clsEmployeeDistributionReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                dtasondate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                chkViewBy.Checked = True
                chkReportbyDate_CheckedChanged(sender, e)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Try
            If Not SetFilter() Then Exit Sub

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'If ConfigParameter._Object._ExportReportPath.Trim = "" Then
            '    'Pinkal (06-May-2014) -- Start
            '    'Enhancement : Language Changes 
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Please set the Export Report Path to export report."), Me)
            '    'Pinkal (06-May-2014) -- End
            '    Exit Sub
            'End If
            'Pinkal (16-Apr-2016) -- End

            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            objDistribution._UserAccessFilter = Session("AccessLevelFilterString")
            'Pinkal (12-Nov-2012) -- End

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDistribution.Export_Employee_Distribution(True)
            objDistribution.Export_Employee_Distribution(Session("Database_Name"), _
                                                   Session("UserId"), _
                                                   Session("Fin_year"), _
                                                   Session("CompanyUnkId"), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   Session("UserAccessModeSetting"), True, True)
            'Shani(24-Aug-2015) -- End

            Dim dtFinalTable As DataTable = objDistribution._dtFinal
            Dim strFilePath As String = "Employee_Distribution_" & drpDistribution.SelectedItem.Text.Trim.Replace(" ", "_") & "_" & Now.Date.ToString("yyyyMMdd") & Format(Now, "hhmmss") & ".xls"
            Export_to_Excel(strFilePath, Session("ExportReportPath"), dtFinalTable)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnExport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnExport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "CheckBox Event"

    Protected Sub chkReportbyDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkViewBy.CheckedChanged
        Try
            If chkViewBy.Checked = True Then
                drpmonth.Enabled = False
                drpyear.Enabled = False
                dtasondate.Enabled = True
            Else
                dtasondate.Enabled = False
                drpmonth.Enabled = True
                drpyear.Enabled = True
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkReportbyDate_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkReportbyDate_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End

            Me.lblDistribution.Text = Language._Object.getCaption(Me.lblDistribution.ID, Me.lblDistribution.Text)
            Me.Label2.Text = Language._Object.getCaption(Me.Label2.ID, Me.Label2.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.ID, Me.lblYear.Text)
            Me.lblAsOnDate.Text = Language._Object.getCaption(Me.lblAsOnDate.ID, Me.lblAsOnDate.Text)
            Me.chkViewBy.Text = Language._Object.getCaption(Me.chkViewBy.ID, Me.chkViewBy.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

            Me.BtnExport.Text = Language._Object.getCaption(Me.BtnExport.ID, Me.BtnExport.Text).Replace("&", "")
            Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End




End Class
