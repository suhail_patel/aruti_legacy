﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_MonthlyPayrollReport.aspx.vb" Inherits="Reports_Rpt_MonthlyPayrollReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 90%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Monthly Payroll Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                            <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboReportType" runat="server" Width="99%" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                            <asp:Label ID="lblMembership" runat="server" Text="Membership"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboMembership" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                            <asp:Label ID="lblStaffExitReason" runat="server" Text="Exit Reason"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboExitReason" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                            <asp:Label ID="Label1" runat="server" Text="Cat. Reason"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="cboCatReason" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                        </td>
                                                        <td style="text-align: right">
                                                            <asp:LinkButton ID="lnkSaveLeaveSelection" runat="server" Text="Save Selection" Font-Overline="false"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                        </td>
                                                        <td>
                                                            <asp:Panel ID="pnlLeaves" runat="server" ScrollBars="auto" Width="99%">
                                                                <div style="border: 1px solid #DDD; height: 80px; overflow: auto; margin-top: 10px;
                                                                    margin-bottom: 10px">
                                                                    <asp:CheckBoxList ID="lvLeaveType" runat="server" RepeatDirection="Vertical">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkSkipAbsentTnA" runat="server" Text="Skip Absent From TnA" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 18%">
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkIncludePendingApprovedforms" runat="server" Text="Include Pending/Approve Form(s)" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%" valign="top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="text-align: right">
                                                            <asp:LinkButton ID="lnkSaveHeadSelection" runat="server" Text="Save Selection" Font-Overline="false"></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:Panel ID="pnlCHeades" runat="server" ScrollBars="auto" Width="99%">
                                                                <div style="border: 1px solid #DDD; height: 247px; overflow: auto; margin-top: 10px;
                                                                    margin-bottom: 10px">
                                                                    <asp:CheckBoxList ID="lvCustomTranHead" runat="server" RepeatDirection="Vertical">
                                                                    </asp:CheckBoxList>
                                                                </div>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:CheckBox ID="chkincludesystemretirement" runat="server" Text="Include Retirement Done By System" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc9:Export runat="server" ID="Export" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
