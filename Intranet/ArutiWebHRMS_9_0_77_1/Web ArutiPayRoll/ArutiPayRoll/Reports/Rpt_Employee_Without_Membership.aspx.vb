﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Employee_Without_Membership
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objWOMembership As clsEmployee_without_Membership


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmp_WithoutMembership_Report"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                               Session("UserId"), _
                                               Session("Fin_year"), _
                                               Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               Session("UserAccessModeSetting"), True, _
                                               Session("IsIncludeInactiveEmp"), "Emp", True)
            'Shani(24-Aug-2015) -- End
            With drpEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpEmployee.SelectedValue = 0
            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objWOMembership.SetDefaultValue()
            objWOMembership._EmployeeUnkid = drpEmployee.SelectedValue

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objWOMembership._EmployeeName = drpEmployee.Text
            objWOMembership._EmployeeName = drpEmployee.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            objWOMembership._IncludeInactiveEmp = chkInactiveemp.Checked

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objWOMembership._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objWOMembership = New clsEmployee_without_Membership(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objWOMembership.setDefaultOrderBy(0)
            objWOMembership._CompanyUnkId = Session("CompanyUnkId")
            objWOMembership._UserUnkId = Session("UserId")
            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objWOMembership._UserAccessFilter = Session("AccessLevelFilterString")
            'S.SANDEEP [ 12 NOV 2012 ] -- END

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objWOMembership.generateReport(0, enPrintAction.None, enExportAction.None)
            objWOMembership.generateReportNew(Session("Database_Name"), _
                                              Session("UserId"), _
                                              Session("Fin_year"), _
                                              Session("CompanyUnkId"), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                              Session("UserAccessModeSetting"), True, _
                                              Session("ExportReportPath"), _
                                              Session("OpenAfterExport"), _
                                              0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objWOMembership._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Protected Sub drpEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.DataBound
        Try         'Hemant (13 Aug 2020)
            If drpEmployee.Items.Count > 0 Then
                For Each lstItem As ListItem In drpEmployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                drpEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End



            Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End

End Class
