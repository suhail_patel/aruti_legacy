﻿<%@ Page Title="Leave Application Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_LeaveApplicationReport.aspx.vb" Inherits="Reports_Rpt_LeaveApplicationReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx"  TagName="Export" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 55%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Application Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Style="font-weight: bold; font-size: 12px" Font-Underline="false" Text="Analysis By"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                      <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblApplicationFrom" runat="server" Text="Application From"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc2:DateCtrl ID="dtpApplicationFrom" runat="server"/>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="LblApplicationTo" runat="server" Text="Application To"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc2:DateCtrl ID="dtpApplicationTo" runat="server"/>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblStartDate" runat="server" Text="Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc2:DateCtrl ID="dtpStartDate" runat="server"/>
                                            </td>
                                            <td style="width: 25%">
                                                <asp:Label ID="LblEndDate" runat="server" Text="End Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%">
                                                <uc2:DateCtrl ID="dtpEndDate" runat="server"/>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td  colspan = "3">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblLeaveName" runat="server" Text="Leave"></asp:Label>
                                            </td>
                                             <td  colspan = "3">
                                                <asp:DropDownList ID="cboLeave" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                       <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                             <td  colspan = "3">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AnalysisBy ID="popAnalysisby" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                 <Triggers>
                         <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
