﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Reports_Rpt_FuelConsumptionReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmFuelConsumptionReport"
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Try

            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), _
                                                True, False, "Emp", True)


            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With
            ObjEmp = Nothing

            dsCombos = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            cboExpenseCategory_SelectedIndexChanged(Nothing, Nothing)

            FillStatus()

            dsCombos = clsExpCommonMethods.Get_UoM(True, "List")
            With cboUOM
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            ObjEmp = Nothing
            ObjMaster = Nothing
        End Try
    End Sub

    Private Sub FillStatus()
        Dim objMasterData As New clsMasterData
        Dim dsCombos As New DataSet
        Try
            dsCombos = objMasterData.getLeaveStatusList("List", "")

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombos.Tables(0), "statusunkid IN (0,1,2,3,6)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "name"
                .DataSource = dtab
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objMasterData = Nothing
            dsCombos.Clear()
            dsCombos = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpTranFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpTranToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboExpenseCategory.SelectedValue = 0
            cboEmployee.SelectedValue = 0
            cboExpense.SelectedValue = 0
            cboUOM.SelectedValue = 0
            cboStatus.SelectedIndex = 0
            chkShowRequestedQty.Checked = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function SetFilter(ByRef objFuelConsumption As clsFuelConsumptionReport) As Boolean
        Try
            objFuelConsumption.SetDefaultValue()

            If (dtpTranFromDate.IsNull AndAlso dtpTranToDate.IsNull) OrElse (Not dtpTranFromDate.IsNull AndAlso dtpTranToDate.IsNull) OrElse (dtpTranFromDate.IsNull AndAlso Not dtpTranToDate.IsNull) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue."), Me)
                Return False

            ElseIf CInt(cboExpenseCategory.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Expense Category is compulsory information.please select Expense Category."), Me)
                Return False
            End If

            If Not dtpTranFromDate.IsNull Then
                objFuelConsumption._FromDate = dtpTranFromDate.GetDate.Date
            Else
                objFuelConsumption._FromDate = Nothing
            End If
            If Not dtpTranToDate.IsNull Then
                objFuelConsumption._ToDate = dtpTranToDate.GetDate.Date
            Else
                objFuelConsumption._ToDate = Nothing
            End If
            objFuelConsumption._ExpCateId = CInt(cboExpenseCategory.SelectedValue)
            objFuelConsumption._ExpCateName = cboExpenseCategory.SelectedItem.Text.ToString()
            objFuelConsumption._EmpUnkId = CInt(cboEmployee.SelectedValue)
            objFuelConsumption._EmpName = cboEmployee.SelectedItem.Text.ToString
            objFuelConsumption._ExpenseID = CInt(cboExpense.SelectedValue)
            objFuelConsumption._Expense = cboExpense.SelectedItem.Text
            objFuelConsumption._UOMId = CInt(cboUOM.SelectedValue)
            objFuelConsumption._UOM = cboUOM.SelectedItem.Text
            objFuelConsumption._StatusId = CInt(cboStatus.SelectedValue)
            objFuelConsumption._StatusName = cboStatus.SelectedItem.Text.ToString
            objFuelConsumption._ShowRequiredQuanitty = chkShowRequestedQty.Checked

            objFuelConsumption._FirstNamethenSurname = CBool(Session("FirstNamethenSurname"))
            objFuelConsumption._UserUnkid = CInt(Session("UserId"))
            objFuelConsumption._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objFuelConsumption._ViewByIds = mstrStringIds
            objFuelConsumption._ViewIndex = mintViewIdx
            objFuelConsumption._ViewByName = mstrStringName
            objFuelConsumption._Analysis_Fields = mstrAnalysis_Fields
            objFuelConsumption._Analysis_Join = mstrAnalysis_Join
            objFuelConsumption._Analysis_OrderBy = mstrAnalysis_OrderBy
            objFuelConsumption._Report_GroupName = mstrReport_GroupName

            GUI.fmtCurrency = Session("fmtCurrency").ToString()

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()

            If Not IsPostBack Then
                GC.Collect()
                Call FillCombo()
                Call ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Dim objFuelConsumption As New clsFuelConsumptionReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try
            If SetFilter(objFuelConsumption) = False Then Exit Sub

            objFuelConsumption.setDefaultOrderBy(0)

            Call SetDateFormat()

            objFuelConsumption.generateReportNew(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           Session("ExportReportPath").ToString, _
                                           CBool(Session("OpenAfterExport")), _
                                           0, Aruti.Data.enPrintAction.None, enExportAction.None)

            Session("objRpt") = objFuelConsumption._Rpt

            If objFuelConsumption._Rpt IsNot Nothing Then ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objFuelConsumption = Nothing
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Private Sub cboExpenseCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboExpenseCategory.SelectedIndexChanged
        Try
            Dim objExpMst As New clsExpense_Master
            Dim dsCombo As DataSet = objExpMst.getComboList(CInt(cboExpenseCategory.SelectedValue), True, "List")
            cboExpense.DataTextField = "Name"
            cboExpense.DataValueField = "Id"
            cboExpense.DataSource = dsCombo.Tables(0)
            cboExpense.DataBind()
            cboExpense.SelectedValue = 0

            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objExpMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Other Controls Events "

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.lblTranFromDate.Text = Language._Object.getCaption(Me.lblTranFromDate.ID, Me.lblTranFromDate.Text)
            Me.lblTranToDate.Text = Language._Object.getCaption(Me.lblTranToDate.ID, Me.lblTranToDate.Text)
            Me.LblExpenseCategory.Text = Language._Object.getCaption(Me.LblExpenseCategory.ID, Me.LblExpenseCategory.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.ID, Me.LblExpense.Text)
            Me.LblUOM.Text = Language._Object.getCaption(Me.LblUOM.ID, Me.LblUOM.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.chkShowRequestedQty.Text = Language._Object.getCaption(Me.chkShowRequestedQty.ID, Me.chkShowRequestedQty.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From and To dates are mandatory information. Please check both dates to continue.")
            Language.setMessage(mstrModuleName, 2, "Expense Category is compulsory information.please select Expense Category.")
        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
