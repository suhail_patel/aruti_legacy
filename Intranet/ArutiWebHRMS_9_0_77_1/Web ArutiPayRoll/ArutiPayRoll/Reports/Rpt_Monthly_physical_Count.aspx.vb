﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_Monthly_physical_Count
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEPhysicalReport As clsEmpMonthlyPhysicalReport


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmpMonthlyPhysicalReport"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Try
            '--------- Year

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'dsCombos = objEPhysicalReport.GetYearList( "List", CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")))
            dsCombos = objEPhysicalReport.GetYearList(CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), "List")
            'Shani(15-Feb-2016) -- End

            With drpyear
                .DataTextField = "Years"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            '--------- Month
            dsCombos = objEPhysicalReport.GetMonthList("List")
            With drpmonth
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsCombos.Dispose()
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpmonth.SelectedValue = 0
            drpyear.SelectedIndex = 0
            chkInactiveemp.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            objEPhysicalReport.SetDefaultValue()

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'If drpmonth.SelectedValue <= 0 Then

            '    'Pinkal (06-May-2014) -- Start
            '    'Enhancement : Language Changes 
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select month to continue."), Me)
            '    'Pinkal (06-May-2014) -- End
            '    drpmonth.Focus()
            '    Return False
            'End If

            'objEPhysicalReport._CMonthId = drpmonth.SelectedValue

            ''Pinkal (24-Apr-2013) -- Start
            ''Enhancement : TRA Changes

            ''objEPhysicalReport._CMonthName = drpmonth.Text
            ''objEPhysicalReport._CYearName = drpyear.Text

            'objEPhysicalReport._CMonthName = drpmonth.SelectedItem.Text
            'objEPhysicalReport._CYearName = drpyear.SelectedItem.Text

            ''Pinkal (24-Apr-2013) -- End

            'If drpmonth.SelectedValue = 1 Then
            '    objEPhysicalReport._PMonthId = 12
            '    objEPhysicalReport._PMonthName = MonthName(12, False)

            '    'Pinkal (24-Apr-2013) -- Start
            '    'Enhancement : TRA Changes
            '    'objEPhysicalReport._PYearName = CInt(drpyear.Text) - 1
            '    objEPhysicalReport._PYearName = CInt(drpyear.SelectedItem.Text) - 1
            '    'Pinkal (24-Apr-2013) -- End

            'Else
            '    objEPhysicalReport._PMonthId = CInt(drpmonth.SelectedValue) - 1
            '    objEPhysicalReport._PMonthName = MonthName(CInt(drpmonth.SelectedValue) - 1, False)

            '    'Pinkal (24-Apr-2013) -- Start
            '    'Enhancement : TRA Changes
            '    'objEPhysicalReport._PYearName = drpyear.Text
            '    objEPhysicalReport._PYearName = drpyear.SelectedItem.Text
            '    'Pinkal (24-Apr-2013) -- End

            'End If
            'objEPhysicalReport._IsActive = chkInactiveemp.Checked

            If drpmonth.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select month to continue."), Me)
                drpmonth.Focus()
                Return False
            End If

            objEPhysicalReport._CMonthId = drpmonth.SelectedValue
            objEPhysicalReport._CMonthName = drpmonth.Text
            objEPhysicalReport._CYearName = drpyear.Text

            Dim iCurr_Date, iPrev_Date As String
            iCurr_Date = "" : iPrev_Date = ""

            iCurr_Date = eZeeDate.convertDate(DateAdd(DateInterval.Month, 0, eZeeDate.convertDate(drpyear.Text & Format(CInt(drpmonth.SelectedValue), "0#") & "01")).AddDays(-1))

            

            If drpmonth.SelectedValue = 1 Then
                objEPhysicalReport._PMonthId = 12
                objEPhysicalReport._PMonthName = MonthName(12, False)
                objEPhysicalReport._PYearName = CInt(drpyear.Text) - 1
                iPrev_Date = eZeeDate.convertDate(DateAdd(DateInterval.Month, 0, eZeeDate.convertDate(CInt(drpyear.Text) - 1 & "12" & "01")).AddDays(-1))
            Else
                objEPhysicalReport._PMonthId = CInt(drpmonth.SelectedValue) - 1
                objEPhysicalReport._PMonthName = MonthName(CInt(drpmonth.SelectedValue) - 1, False)
                objEPhysicalReport._PYearName = drpyear.Text
                iPrev_Date = eZeeDate.convertDate(DateAdd(DateInterval.Month, 0, eZeeDate.convertDate(drpyear.Text & Format(CInt(drpmonth.SelectedValue) - 1, "0#") & "01")).AddDays(-1))
            End If

            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : INCLUSION OF INACTIVE EMPLOYEE
            objEPhysicalReport._IsActive = chkInactiveemp.Checked
            'Pinkal (24-Jun-2011) -- End

            objEPhysicalReport._iCDate = iCurr_Date
            objEPhysicalReport._iPDate = iPrev_Date

            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEPhysicalReport._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End
            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEPhysicalReport = New clsEmpMonthlyPhysicalReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objEPhysicalReport._CompanyUnkId = Session("CompanyUnkId")
            objEPhysicalReport._UserUnkId = Session("UserId")
            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEPhysicalReport._UserAccessFilter = Session("AccessLevelFilterString")
            'S.SANDEEP [ 12 NOV 2012 ] -- END
            objEPhysicalReport.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEPhysicalReport.generateReport(0, enPrintAction.None, enExportAction.None)
            objEPhysicalReport.generateReportNew(Session("Database_Name"), _
                                                 Session("UserId"), _
                                                 Session("Fin_year"), _
                                                 Session("CompanyUnkId"), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 Session("UserAccessModeSetting"), True, _
                                                 Session("ExportReportPath"), _
                                                 Session("OpenAfterExport"), 0, _
                                                 enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objEPhysicalReport._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END
#End Region

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End

        Me.Label2.Text = Language._Object.getCaption(Me.Label2.ID, Me.Label2.Text)
        Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.ID, Me.lblYear.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

End Class
