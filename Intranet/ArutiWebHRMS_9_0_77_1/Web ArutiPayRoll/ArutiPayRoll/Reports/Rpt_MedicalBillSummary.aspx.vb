﻿Option Strict On 'Shani(11-Feb-2016)

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_MedicalBillSummary
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objBillSummary As clsMedicalBillsummary

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private mstrModuleName As String = "frmMedicalBillsummary"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim dsList As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate")).Date)

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            'Shani(24-Aug-2015) -- End
            With drpEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objEmp = Nothing



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, CInt(Session("fin_year")), "Period", True, , , , Session("Database_Name"))

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, CInt(Session("fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")), "Period", True)
            'Shani(11-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            Me.ViewState.Add("Period", dsList)
            With drpFromPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            With drpToPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period").Copy
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            Dim objInstitute As New clsinstitute_master
            dsList = objInstitute.getListForCombo(True, "Institute", True, 1)
            With drpProvider
                .DataValueField = "instituteunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Institute")
                .SelectedValue = CStr(0)
                .DataBind()
            End With


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 

            Language.setLanguage(mstrModuleName)
            With drpReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Medical Bill Summary Employee Wise"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "Medical Bill Summary Service Provider Wise"))
                .SelectedIndex = 0
            End With

            'Pinkal (06-May-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objBillSummary.setDefaultOrderBy(0)
            drpEmployee.SelectedIndex = 0
            drpFromPeriod.SelectedIndex = 0
            drpReportType.SelectedIndex = 0
            drpToPeriod.SelectedIndex = 0
            drpProvider.SelectedIndex = 0
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Call drpReportType_SelectedIndexChanged(drpReportType, New System.EventArgs)
            'Nilay (01-Feb-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objBillSummary.SetDefaultValue()

            objBillSummary._ReportTypeId = CInt(drpReportType.SelectedIndex)
            objBillSummary._ReportTypeName = drpReportType.SelectedItem.Text

            objBillSummary._FromPeriodId = CInt(drpFromPeriod.SelectedValue)
            objBillSummary._FromPeriodName = drpFromPeriod.SelectedItem.Text

            objBillSummary._ToPeriodId = CInt(drpToPeriod.SelectedValue)
            objBillSummary._ToPeriodName = drpToPeriod.SelectedItem.Text


            Dim StrPeriodIds, StrPeriodNames As String
            StrPeriodIds = String.Empty : StrPeriodNames = String.Empty

            If Me.ViewState("Period") IsNot Nothing Then

                If CType(Me.ViewState("Period"), DataSet).Tables.Count <= 0 Then Return False

                Dim dsPeriod As DataSet = CType(Me.ViewState("Period"), DataSet)

                Dim drRow() As DataRow = dsPeriod.Tables(0).Select("periodunkid >= " & CInt(drpFromPeriod.SelectedValue) & " AND periodunkid<= " & CInt(drpToPeriod.SelectedValue))

                If drRow.Length > 0 Then
                    Dim mstrPeriod As String = ""
                    For i As Integer = 0 To drRow.Length - 1
                        mstrPeriod &= drRow(i)("periodunkid").ToString() & ","
                    Next

                    If mstrPeriod.Trim.Length > 0 Then
                        mstrPeriod = mstrPeriod.Substring(0, mstrPeriod.Trim.Length - 1)
                    End If

                    objBillSummary._mstrPeriodId = mstrPeriod

                End If

            End If

            If CInt(drpReportType.SelectedIndex) = 0 Then
                objBillSummary._EmployeeId = CInt(drpEmployee.SelectedValue)
                If CInt(drpEmployee.SelectedValue) > 0 Then
                    objBillSummary._EmployeeName = drpEmployee.SelectedItem.Text
                End If

            ElseIf drpReportType.SelectedIndex = 1 Then
                objBillSummary._ProviderId = CInt(drpProvider.SelectedValue)
                If CInt(drpProvider.SelectedValue) > 0 Then
                    objBillSummary._ProviderName = drpProvider.SelectedItem.Text
                End If

                End If


                Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Function Validation() As Boolean
        Try
            If CInt(drpFromPeriod.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("From Period is compulsory information.Please Select From Period.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "From Period is compulsory information.Please Select From Period."), Me)
                'Pinkal (06-May-2014) -- End


                drpFromPeriod.Focus()
                Return False

            ElseIf CInt(drpToPeriod.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "To Period is compulsory information.Please Select To Period."), Me)
                'Pinkal (06-May-2014) -- End
                drpToPeriod.Focus()
                Return False
            End If

            If drpToPeriod.SelectedIndex < drpFromPeriod.SelectedIndex Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage(" To Period cannot be less than From Period.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, " To Period cannot be less than From Period."), Me)
                'Pinkal (06-May-2014) -- End
                drpToPeriod.Focus()
                Exit Function
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation", Me)
            DisplayMessage.DisplayMessage("Validation :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objBillSummary = New clsMedicalBillsummary(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Not IsPostBack Then
                FillCombo()
                ResetValue()
                drpReportType_SelectedIndexChanged(sender, e)
            End If


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Validation() Then
                If SetFilter() = False Then Exit Sub

                objBillSummary._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objBillSummary._UserUnkId = CInt(Session("UserId"))
                objBillSummary._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
                GUI.fmtCurrency = CStr(Session("fmtCurrency"))
                objBillSummary.setDefaultOrderBy(CInt(drpReportType.SelectedIndex))



                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objBillSummary.generateReport(CInt(drpReportType.SelectedIndex), enPrintAction.None, enExportAction.None)
                objBillSummary.generateReportNew(CStr(Session("Database_Name")), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                 CStr(Session("ExportReportPath")), _
                                                 CBool(Session("OpenAfterExport")), _
                                                 CInt(drpReportType.SelectedIndex), _
                                                 enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
                'Shani(24-Aug-2015) -- End

                Session("objRpt") = objBillSummary._Rpt
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                If Session("objRpt") IsNot Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
                'Shani(11-Feb-2016) -- End

                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "ComboBox Event"

    Private Sub drpReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpReportType.SelectedIndexChanged
        Try
            If CInt(drpReportType.SelectedIndex) <= 0 Then
                drpProvider.SelectedIndex = 0
                drpEmployee.Enabled = True
                drpProvider.Enabled = False
            Else
                drpEmployee.SelectedIndex = 0
                drpProvider.Enabled = True
                drpEmployee.Enabled = False
            End If
            'objBillSummary.setDefaultOrderBy(CInt(drpReportType.SelectedIndex))
            'txtOrderBy.Text = objBillSummary.OrderByDisplay
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpReportType_SelectedIndexChanged", Me)
            DisplayMessage.DisplayError("drpReportType_SelectedIndexChanged", Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        Me.lblFromPeriod.Text = Language._Object.getCaption(Me.lblFromPeriod.ID, Me.lblFromPeriod.Text)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.LblProvider.Text = Language._Object.getCaption(Me.LblProvider.ID, Me.LblProvider.Text)
        Me.LblToPeriod.Text = Language._Object.getCaption(Me.LblToPeriod.ID, Me.LblToPeriod.Text)
        Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.ID, Me.lblReportType.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub


    'Pinkal (06-May-2014) -- End


End Class
