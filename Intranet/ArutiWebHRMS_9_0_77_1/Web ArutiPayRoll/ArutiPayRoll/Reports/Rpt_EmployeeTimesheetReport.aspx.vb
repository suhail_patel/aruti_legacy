﻿Option Strict On 'Shani(11-Feb-2016) 

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_EmployeeTimesheetReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Dim objEmployeeTimeSheet As clsEmployeeTimesheetReport

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mstrModuleName As String = "frmEmployeeTimeSheetReport"
    'Pinkal (06-May-2014) -- End


    'Gajanan [30-JUL-2019] -- START
    'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrAnalysisFields As String = ""
    Private mstrAnalysisJoin As String = ""
    Private mstrAnalysisOrderBy As String = ""
    Private mstrReportGroupName As String = ""
    Private mstrAnalysisOrderByGName As String = ""
    'Gajanan [30-JUL-2019] -- END


#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet


            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise


            'If Session("LoginBy") = Global.User.en_loginby.User Then

            '    'Shani(20-Nov-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'If ConfigParameter._Object._IsIncludeInactiveEmp = True Then
            '    '    dsList = objEmp.GetEmployeeList("Employee", True, False)
            '    'Else
            '    '    dsList = objEmp.GetEmployeeList("Employee", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            '    'End If
            '    dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Employee", True)
            '    'Shani(20-Nov-2015) -- End
            'ElseIf Session("LoginBy") = Global.User.en_loginby.Employee Then

            '    'Shani(20-Nov-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'dsList = objEmp.GetEmployeeList("Employee", False, True, CInt(Session("Employeeunkid")), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            '    dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Employee", False, CInt(Session("Employeeunkid")))
            '    'Shani(20-Nov-2015) -- End

                'End If

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                            blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            'Shani(11-Feb-2016) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmp = Nothing

       
            Dim objShift As New clsNewshift_master
            dsList = objShift.getListForCombo("List", True)
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With

            'Pinkal (27-May-2014) -- Start
            'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]

            Dim dtAttCode As DataTable = objEmployeeTimeSheet.GetFilterForAttCode()
            With cboAttCode
                .DataValueField = "Id"
                .DataTextField = "leavename"
                .DataSource = dtAttCode
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            'Pinkal (27-May-2014) -- End



            'Gajanan [30-JUL-2019] -- START
            'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
            If IsNothing(Session("EmpTimesheetSetting")) = False AndAlso Session("EmpTimesheetSetting").ToString().Length > 0 Then
                Dim ar() As String = Session("EmpTimesheetSetting").ToString.Trim.Split(CChar("|"))
                If ar.Length > 0 Then
                    chkShowTimingsIn24Hrs.Checked = CBool(ar(0).ToString())
                    If CBool(Session("PolicyManagementTNA")) = False Then
                        chkShowHoliday.Checked = CBool(ar(1).ToString())
                        chkShowLeaveType.Checked = CBool(ar(2).ToString())
                        If ar.Length > 4 Then
                            chkBreaktime.Checked = CBool(ar(4).ToString())
                            chkShift.Checked = CBool(ar(5).ToString())

                            If ar.Length - 1 < 6 OrElse ar(6).ToString().Trim.Length <= 0 Then
                                chkShowBaseHrs.Checked = False
                            Else
                                chkShowBaseHrs.Checked = CBool(ar(6).ToString())
                            End If
                            If ar.Length - 1 < 7 OrElse ar(7).ToString.Trim.Length <= 0 Then
                                chkShowTotalAbsentDays.Checked = False
                            Else
                                chkShowTotalAbsentDays.Checked = CBool(ar(7).ToString())
                            End If

                            'Pinkal (08-Aug-2019) -- Start
                            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                            If ar.Length - 1 < 8 OrElse ar(8).ToString.Trim.Length <= 0 Then
                                chkIncludeOFFHrsInBaseHrs.Checked = True
                            Else
                                chkIncludeOFFHrsInBaseHrs.Checked = CBool(ar(8).ToString())
                            End If
                            'Pinkal (08-Aug-2019) -- End
                        End If

                            'Pinkal (13-Sep-2021)-- Start
                            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
                    Else
                        If ar.Length > 4 Then
                            If ar.Length - 1 < 9 OrElse ar(9).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If Session("CompanyGroupName").ToString().ToUpper() <> "COLAS LTD" Then
                                    chkShowLateComingInShortHrs.Checked = False
                                Else
                                chkShowLateComingInShortHrs.Checked = True
                                End If
                                'Pinkal (09-Dec-2021)-- End
                            Else
                                chkShowLateComingInShortHrs.Checked = CBool(ar(9).ToString())
                            End If

                            If ar.Length - 1 < 10 OrElse ar(10).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If Session("CompanyGroupName").ToString().ToUpper() <> "COLAS LTD" Then
                                    chkShowActualTimingAfterRoundOff.Checked = False
                                Else
                                chkShowActualTimingAfterRoundOff.Checked = True
                                End If
                                'Pinkal (09-Dec-2021)-- End
                            Else
                                chkShowActualTimingAfterRoundOff.Checked = CBool(ar(10).ToString())
                            End If

                            If ar.Length - 1 < 11 OrElse ar(11).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If Session("CompanyGroupName").ToString().ToUpper() <> "COLAS LTD" Then
                                    chkShowShortHrsOnBaseHrs.Checked = False
                                Else
                                chkShowShortHrsOnBaseHrs.Checked = True
                                End If
                                'Pinkal (09-Dec-2021)-- End
                            Else
                                chkShowShortHrsOnBaseHrs.Checked = CBool(ar(11).ToString())
                            End If
                            chkShowShortHrsOnBaseHrs_CheckedChanged(chkShowShortHrsOnBaseHrs, New EventArgs())

                            If ar.Length - 1 < 12 OrElse ar(12).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If Session("CompanyGroupName").ToString().ToUpper() <> "COLAS LTD" Then
                                    chkDisplayShortHrsWhenEmpAbsent.Checked = False
                                Else
                                chkDisplayShortHrsWhenEmpAbsent.Checked = True
                                End If
                                'Pinkal (09-Dec-2021)-- End
                            Else
                                chkDisplayShortHrsWhenEmpAbsent.Checked = CBool(ar(12).ToString())
                            End If

                            If ar.Length - 1 < 13 OrElse ar(13).ToString.Trim.Length <= 0 Then
                                'Pinkal (09-Dec-2021)-- Start
                                'Voltamp Report Setting Issue.
                                If Session("CompanyGroupName").ToString().ToUpper() <> "COLAS LTD" Then
                                    chkShowAbsentAfterEmpTerminated.Checked = False
                                Else
                                chkShowAbsentAfterEmpTerminated.Checked = True
                                End If
                                'Pinkal (09-Dec-2021)-- End
                            Else
                                chkShowAbsentAfterEmpTerminated.Checked = CBool(ar(13).ToString())
                            End If

                        End If
                        'Pinkal (13-Sep-2021)-- End
                    End If
                    chkShowEachEmpOnNewPage.Checked = CBool(ar(3).ToString())
                End If

            End If
            'Gajanan [30-JUL-2019] -- END



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboEmployee.SelectedValue = CStr(0)
            cboShift.SelectedValue = CStr(0)
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

            'Pinkal (27-May-2014) -- Start
            'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]
            cboAttCode.SelectedValue = CStr(0)
            'Pinkal (27-May-2014) -- End

            objEmployeeTimeSheet.setDefaultOrderBy(0)
            chkInactiveemp.Checked = False
            chkShowEachEmpOnNewPage.Checked = True


            'Gajanan [30-JUL-2019] -- START
            'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            'Gajanan [30-JUL-2019] -- END

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            If Session("CompanyGroupName").ToString().ToUpper() <> "COLAS LTD" Then
                chkShowAbsentAfterEmpTerminated.Visible = False
                chkShowLateComingInShortHrs.Visible = False
                chkShowShortHrsOnBaseHrs.Visible = False
                chkShowActualTimingAfterRoundOff.Visible = False
                chkDisplayShortHrsWhenEmpAbsent.Visible = False
            Else
                chkShowAbsentAfterEmpTerminated.Visible = True
                chkShowLateComingInShortHrs.Visible = True
                chkShowShortHrsOnBaseHrs.Visible = True
                chkShowActualTimingAfterRoundOff.Visible = True
                chkDisplayShortHrsWhenEmpAbsent.Visible = True
            End If

            If Session("CompName").ToString().ToUpper() = "NMB BANK PLC" Then
                chkShift.Visible = False
                chkShowTotalAbsentDays.Visible = False
                chkIncludeOFFHrsInBaseHrs.Visible = False
                chkShowHoliday.Visible = False
                chkBreaktime.Visible = False
                chkShowLeaveType.Visible = False
                chkShowBaseHrs.Visible = False
                chkShowAbsentAfterEmpTerminated.Checked = False
                chkShowLateComingInShortHrs.Visible = False
                chkShowShortHrsOnBaseHrs.Visible = False
                chkShowActualTimingAfterRoundOff.Visible = False
                chkDisplayShortHrsWhenEmpAbsent.Visible = False
            End If
            'Pinkal (13-Sep-2021)-- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmployeeTimeSheet.SetDefaultValue()
            objEmployeeTimeSheet._EmpId = CInt(cboEmployee.SelectedValue)
            objEmployeeTimeSheet._EmpName = cboEmployee.SelectedItem.Text

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
                objEmployeeTimeSheet._Shiftunkid = objEmp._Shiftunkid
                objEmployeeTimeSheet._EmpCode = objEmp._Employeecode
                objEmployeeTimeSheet._DepartmentId = objEmp._Departmentunkid
                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmp._Departmentunkid
                objEmployeeTimeSheet._Department = objDept._Name
            End If

            objEmployeeTimeSheet._FromDate = dtpFromDate.GetDate.Date
            objEmployeeTimeSheet._ToDate = dtpToDate.GetDate.Date
            objEmployeeTimeSheet._IsActive = chkInactiveemp.Checked

            objEmployeeTimeSheet._Shiftunkid = CInt(cboShift.SelectedValue)
            objEmployeeTimeSheet._ShiftName = cboShift.SelectedItem.Text
            objEmployeeTimeSheet._ShowEachEmployeeOnNewPage = chkShowEachEmpOnNewPage.Checked
            objEmployeeTimeSheet._IsPolicyManagementTNA = CBool(Session("PolicyManagementTNA"))


            'Pinkal (27-May-2014) -- Start
            'Enhancement : OMAN Changes [Adding Leave Day Fraction In Login Summary FOR Getting Proper Leave Day Fraction]
            objEmployeeTimeSheet._AttenId = CInt(cboAttCode.SelectedValue)
            Dim dtAttCode() As DataRow = objEmployeeTimeSheet.GetFilterForAttCode().Select("Id = " & CInt(cboAttCode.SelectedValue))
            If dtAttCode.Length > 0 Then
                objEmployeeTimeSheet._AttCode = dtAttCode(0)("leavetypecode").ToString()
            End If
            'Pinkal (27-May-2014) -- End



            'Gajanan [30-JUL-2019] -- START
            'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
            objEmployeeTimeSheet._ShowHoliday = chkShowHoliday.Checked
            objEmployeeTimeSheet._ShowLeaveType = chkShowLeaveType.Checked
            objEmployeeTimeSheet._ShowTiming24Hrs = chkShowTimingsIn24Hrs.Checked
            objEmployeeTimeSheet._ShowBreakTime = chkBreaktime.Checked
            objEmployeeTimeSheet._ShowShift = chkShift.Checked
            objEmployeeTimeSheet._ShowBaseHrs = chkShowBaseHrs.Checked
            objEmployeeTimeSheet._ShowTotalAbsentDays = chkShowTotalAbsentDays.Checked
            objEmployeeTimeSheet._DBstartDate = Convert.ToDateTime(Session("fin_startdate")).Date
            objEmployeeTimeSheet._DBEndDate = Convert.ToDateTime(Session("fin_enddate")).Date
            objEmployeeTimeSheet._YearId = CInt(Session("Fin_year"))

            objEmployeeTimeSheet._ViewByIds = mstrViewByIds
            objEmployeeTimeSheet._ViewIndex = mintViewIndex
            objEmployeeTimeSheet._ViewByName = mstrViewByName
            objEmployeeTimeSheet._Analysis_Fields = mstrAnalysisFields
            objEmployeeTimeSheet._Analysis_Join = mstrAnalysisJoin
            objEmployeeTimeSheet._Analysis_OrderBy = mstrAnalysisOrderBy
            objEmployeeTimeSheet._Report_GroupName = mstrReportGroupName

            'Gajanan [30-JUL-2019] -- END

            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            objEmployeeTimeSheet._IncludeOFFHrsinTotalBaseHrs = chkIncludeOFFHrsInBaseHrs.Checked
            'Pinkal (08-Aug-2019) -- End

            'objEmployeeTimeSheet._ViewByIds = mstrStringIds
            'objEmployeeTimeSheet._ViewIndex = mintViewIdx
            'objEmployeeTimeSheet._ViewByName = mstrStringName
            'objEmployeeTimeSheet._Analysis_Fields = mstrAnalysis_Fields
            'objEmployeeTimeSheet._Analysis_Join = mstrAnalysis_Join
            'objEmployeeTimeSheet._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objEmployeeTimeSheet._Report_GroupName = mstrReport_GroupName
            'objEmployeeTimeSheet._AdvanceFilter = mstrAdvanceFilter

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objEmployeeTimeSheet._IncludeAccessFilterQry = False
            'Pinkal (06-Jan-2016) -- End

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            objEmployeeTimeSheet._ShowAbsentAfterEmpTerminated = chkShowAbsentAfterEmpTerminated.Checked
            objEmployeeTimeSheet._ShowLateComingEaryGoingAsShortHrs = chkShowLateComingInShortHrs.Checked
            objEmployeeTimeSheet._ShowShortHrsOnBaseHrs = chkShowShortHrsOnBaseHrs.Checked
            objEmployeeTimeSheet._ShowActualTimingAfterRoundOff = chkShowActualTimingAfterRoundOff.Checked
            objEmployeeTimeSheet._DisplayShortHrsWhenEmpAbsent = chkDisplayShortHrsWhenEmpAbsent.Checked
            'Pinkal (13-Sep-2021)-- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter :- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objEmployeeTimeSheet = New clsEmployeeTimesheetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End
            If Not IsPostBack Then
                Call FillCombo()
                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date


                'Pinkal (09-Dec-2021)-- Start
                'Voltamp Report Setting Issue.
                ResetValue()
                'Pinkal (09-Dec-2021)-- End.

                'Gajanan [30-JUL-2019] -- START
                'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
                pnlShowTiming.Visible = Not CBool(Session("PolicyManagementTNA"))
                pnlShift.Visible = Not CBool(Session("PolicyManagementTNA"))
                pnlShowTotalAbsentDays.Visible = Not CBool(Session("PolicyManagementTNA"))
                chkShowHoliday.Visible = Not CBool(Session("PolicyManagementTNA"))
                chkBreaktime.Visible = Not CBool(Session("PolicyManagementTNA"))
                'Gajanan [30-JUL-2019] -- END


                'Pinkal (08-Aug-2019) -- Start
                'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
                chkIncludeOFFHrsInBaseHrs.Visible = Not CBool(Session("PolicyManagementTNA"))
                'Pinkal (08-Aug-2019) -- End

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    lnkSetAnalysis.Visible = True
                Else
                    lnkSetAnalysis.Visible = False
                End If


                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If Session("CompName").ToString().ToUpper() = "NMB BANK PLC" Then
                    chkShift.Visible = False
                    chkShowTotalAbsentDays.Visible = False
                    chkIncludeOFFHrsInBaseHrs.Visible = False
                    chkShowHoliday.Visible = False
                    chkBreaktime.Visible = False
                    chkShowLeaveType.Visible = False
                    chkShowBaseHrs.Visible = False
                End If
                'Pinkal (27-Aug-2020) -- End



            Else

                'Gajanan [30-JUL-2019] -- START
                'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
                mstrViewByIds = CStr(Me.ViewState("mstrStringIds"))
                mstrViewByName = CStr(Me.ViewState("mstrStringName"))
                mintViewIndex = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysisFields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysisJoin = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReportGroupName = CStr(Me.ViewState("mstrReport_GroupName "))
                'Gajanan [30-JUL-2019] -- END

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub


    'Gajanan [30-JUL-2019] -- START
    'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrViewByIds
            Me.ViewState("mstrStringName") = mstrViewByName
            Me.ViewState("mintViewIdx") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysisFields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysisJoin
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysisOrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
        End Try
    End Sub
    'Gajanan [30-JUL-2019] -- ENDProtected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
       

#End Region

#Region " Button's Event(s) "

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objEmployeeTimeSheet._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEmployeeTimeSheet._UserUnkId = CInt(Session("UserId"))
            objEmployeeTimeSheet._UserAccessFilter = CStr(Session("AccessLevelFilterString"))


            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            objEmployeeTimeSheet._IsPolicyManagementTNA = CBool(Session("PolicyManagementTNA"))
            'Shani(11-Feb-2016) -- End



            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Pinkal (01-Dec-2016) -- Start
            'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
            If CInt(Session("Employeeunkid")) > 0 Then
                objEmployeeTimeSheet._UserName = Session("DisplayName").ToString()
            End If
            'Pinkal (01-Dec-2016) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeTimeSheet.generateReport(0, enPrintAction.None, enExportAction.None)
            objEmployeeTimeSheet.generateReportNew(CStr(Session("Database_Name")), _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                   CStr(Session("ExportReportPath")), _
                                                   CBool(Session("OpenAfterExport")), _
                                                   0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Shani(20-Nov-2015) -- End

            Session("objRpt") = objEmployeeTimeSheet._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            If Session("objRpt") IsNot Nothing Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(11-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Gajanan [30-JUL-2019] -- START
    'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
    
    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysisFields = popAnalysisby._Analysis_Fields
            mstrAnalysisJoin = popAnalysisby._Analysis_Join
            mstrAnalysisOrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysisOrderByGName = popAnalysisby._Analysis_OrderBy_GName
            mstrReportGroupName = popAnalysisby._Report_GroupName

        Catch ex As Exception

            DisplayMessage.DisplayError("popAnalysisby_buttonApply_Click :- " & ex.Message, Me)

        End Try
    End Sub
    'Gajanan [30-JUL-2019] -- END


#End Region

#Region " Link Event(s) "
    'Gajanan [30-JUL-2019] -- START
    'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAnalysisBy_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = CInt(Session("CompanyUnkId"))


            'Pinkal (08-Aug-2019) -- Start
            'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
            'objConfig._EmpTimesheetSetting = chkShowTimingsIn24Hrs.Checked.ToString() & "|" & chkShowHoliday.Checked.ToString() & "|" & chkShowLeaveType.Checked.ToString() & "|" & _
            '                                                    chkShowEachEmpOnNewPage.Checked.ToString() & "|" & chkBreaktime.Checked.ToString() & "|" & chkShift.Checked.ToString() & "|" & _
            '                                                    chkShowBaseHrs.Checked.ToString() & "|" & chkShowTotalAbsentDays.Checked.ToString()

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            'objConfig._EmpTimesheetSetting = chkShowTimingsIn24Hrs.Checked.ToString() & "|" & chkShowHoliday.Checked.ToString() & "|" & chkShowLeaveType.Checked.ToString() & "|" & _
            '                                                    chkShowEachEmpOnNewPage.Checked.ToString() & "|" & chkBreaktime.Checked.ToString() & "|" & chkShift.Checked.ToString() & "|" & _
            '                                                    chkShowBaseHrs.Checked.ToString() & "|" & chkShowTotalAbsentDays.Checked.ToString() & "|" & chkIncludeOFFHrsInBaseHrs.Checked.ToString()

            objConfig._EmpTimesheetSetting = chkShowTimingsIn24Hrs.Checked.ToString() & "|" & chkShowHoliday.Checked.ToString() & "|" & chkShowLeaveType.Checked.ToString() & "|" & _
                                                                chkShowEachEmpOnNewPage.Checked.ToString() & "|" & chkBreaktime.Checked.ToString() & "|" & chkShift.Checked.ToString() & "|" & _
                                                               chkShowBaseHrs.Checked.ToString() & "|" & chkShowTotalAbsentDays.Checked.ToString() & "|" & chkIncludeOFFHrsInBaseHrs.Checked.ToString() & "|" & _
                                                                chkShowLateComingInShortHrs.Checked.ToString() & "|" & chkShowActualTimingAfterRoundOff.Checked.ToString() & "|" & _
                                                                chkShowShortHrsOnBaseHrs.Checked.ToString() & "|" & chkDisplayShortHrsWhenEmpAbsent.Checked.ToString() & "|" & _
                                                                chkShowAbsentAfterEmpTerminated.Checked.ToString()


            'Pinkal (13-Sep-2021)-- End

            'Pinkal (08-Aug-2019) -- End


            If objConfig.updateParam() = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Filters saved successfully."), Me)
                objConfig.Refresh()
                Session("EmpTimesheetSetting") = objConfig._EmpTimesheetSetting
            End If
            objConfig = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkSave_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Gajanan [30-JUL-2019] -- END

#End Region

    'Pinkal (13-Sep-2021)-- Start
    'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.

#Region "CheckBox Event"
    Private Sub chkShowShortHrsOnBaseHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShowShortHrsOnBaseHrs.CheckedChanged
        Try
            chkShowLateComingInShortHrs.Enabled = False
            chkShowLateComingInShortHrs.Checked = chkShowShortHrsOnBaseHrs.Checked
            chkDisplayShortHrsWhenEmpAbsent.Enabled = chkShowShortHrsOnBaseHrs.Checked
            If chkShowShortHrsOnBaseHrs.Checked = False Then
                chkDisplayShortHrsWhenEmpAbsent.Checked = chkShowShortHrsOnBaseHrs.Checked
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

    'Pinkal (13-Sep-2021)-- End

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, objEmployeeTimeSheet._ReportName)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

        Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
        Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.ID, Me.LblShift.Text)
        Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.ID, Me.LblFromDate.Text)
        Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.ID, Me.lblToDate.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
        Me.chkShowEachEmpOnNewPage.Text = Language._Object.getCaption(Me.chkShowEachEmpOnNewPage.ID, Me.chkShowEachEmpOnNewPage.Text)
        Me.chkShowHoliday.Text = Language._Object.getCaption(Me.chkShowHoliday.ID, Me.chkShowHoliday.Text)
        Me.chkBreaktime.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
        Me.chkShowTimingsIn24Hrs.Text = Language._Object.getCaption(Me.chkShowTimingsIn24Hrs.ID, Me.chkShowTimingsIn24Hrs.Text)
        Me.chkShowLeaveType.Text = Language._Object.getCaption(Me.chkShowLeaveType.ID, Me.chkShowLeaveType.Text)
        Me.chkShift.Text = Language._Object.getCaption(Me.chkShift.ID, Me.chkShift.Text)
        Me.chkShowBaseHrs.Text = Language._Object.getCaption(Me.chkShowBaseHrs.ID, Me.chkShowBaseHrs.Text)
        Me.chkShowTotalAbsentDays.Text = Language._Object.getCaption(Me.chkShowTotalAbsentDays.ID, Me.chkShowTotalAbsentDays.Text)

        'Pinkal (08-Aug-2019) -- Start
        'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.
        Me.chkIncludeOFFHrsInBaseHrs.Text = Language._Object.getCaption(Me.chkIncludeOFFHrsInBaseHrs.ID, Me.chkIncludeOFFHrsInBaseHrs.Text)
        'Pinkal (08-Aug-2019) -- End

            'Pinkal (13-Sep-2021)-- Start
            'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.
            Me.chkShowAbsentAfterEmpTerminated.Text = Language._Object.getCaption(Me.chkShowAbsentAfterEmpTerminated.ID, Me.chkShowAbsentAfterEmpTerminated.Text)
            Me.chkShowLateComingInShortHrs.Text = Language._Object.getCaption(Me.chkShowLateComingInShortHrs.ID, Me.chkShowLateComingInShortHrs.Text)
            Me.chkShowShortHrsOnBaseHrs.Text = Language._Object.getCaption(Me.chkShowShortHrsOnBaseHrs.ID, Me.chkShowShortHrsOnBaseHrs.Text)
            Me.chkDisplayShortHrsWhenEmpAbsent.Text = Language._Object.getCaption(Me.chkDisplayShortHrsWhenEmpAbsent.ID, Me.chkDisplayShortHrsWhenEmpAbsent.Text)
            Me.chkShowActualTimingAfterRoundOff.Text = Language._Object.getCaption(Me.chkShowActualTimingAfterRoundOff.ID, Me.chkShowActualTimingAfterRoundOff.Text)
            'Pinkal (13-Sep-2021)-- End

        Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.ID, Me.lnkSave.Text)


        Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Filters saved successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages : " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
