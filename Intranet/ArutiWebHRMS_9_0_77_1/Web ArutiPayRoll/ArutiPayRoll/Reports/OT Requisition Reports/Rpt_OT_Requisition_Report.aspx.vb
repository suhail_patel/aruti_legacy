﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_OT_Requisition_Reports_Rpt_OT_Requisition_Report
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Dim objOTRequisitionDetail As New clsOTRequisition_Report
    'Pinkal (03-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmOTRequisition_Report"
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing

    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty


#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try

            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'cboReportType.Items.Clear()
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 4, "OT Requisition Detail Report"))
            'cboReportType.Items.Add(Language.getMessage(mstrModuleName, 5, "OT Requisition Summary Report"))
            'cboReportType.SelectedIndex = 0
            'Pinkal (15-Jan-2020) -- End

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            Dim objEmp As New clsEmployee_Master

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString, True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
            End With
            objEmp = Nothing

            dsList.Clear()
            dsList = Nothing
            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), FinancialYear._Object._Database_Start_Date.Date _
                                                               , "List", True, 0, False)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With
            objPeriod = Nothing

            dsList.Clear()
            dsList = Nothing
            Dim objTranHead As New clsTransactionHead
            dsList = objTranHead.getComboList(Session("Database_Name").ToString(), "List", True)

            With cboNormalOTTranHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboHolidayOTTranHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = "0"
            End With
            dsList.Clear()
            dsList = Nothing
            objTranHead = Nothing


            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'cboReportType_SelectedIndexChanged(cboReportType, New EventArgs())
            'Pinkal (15-Jan-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        End Try
    End Sub

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet = Nothing
        Dim mstrTranHeadIds As String = ""
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.OTRequisition_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrTranHeadIds = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                If mstrTranHeadIds.Trim.Length > 0 Then
                    Dim ar() As String = mstrTranHeadIds.Trim().Split(CChar(CStr(",")))
                    If ar.Length > 0 Then
                        cboNormalOTTranHead.SelectedValue = ar(0).ToString()
                        cboHolidayOTTranHead.SelectedValue = ar(1).ToString()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GetValue : " & ex.Message, Me)
        Finally
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (03-Sep-2020) -- End
            objUserDefRMode = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try

            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'cboReportType.SelectedIndex = 0
            'cboReportType_SelectedIndexChanged(cboReportType, New EventArgs())
            'Pinkal (15-Jan-2020) -- End

            cboPeriod.SelectedIndex = 0
            cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
            cboEmployee.SelectedIndex = 0
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue : " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByVal objOTRequisitionDetail As clsOTRequisition_Report) As Boolean
        'Pinkal (03-Sep-2020) -- End
        Try
            objOTRequisitionDetail.SetDefaultValue()

            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
            'objOTRequisitionDetail._ReportId = CInt(cboReportType.SelectedIndex)
            'objOTRequisitionDetail._ReportTypeName = cboReportType.SelectedItem.Text
            objOTRequisitionDetail._ReportName = lblPageHeader.Text
            'Pinkal (15-Jan-2020) -- End

            objOTRequisitionDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            objOTRequisitionDetail._Period = cboPeriod.SelectedItem.Text
            objOTRequisitionDetail._EmployeeID = CInt(cboEmployee.SelectedValue)
            objOTRequisitionDetail._EmployeeName = cboEmployee.SelectedItem.Text
            objOTRequisitionDetail._PeriodStartDate = mdtPeriodStartDate
            objOTRequisitionDetail._PeriodEndDate = mdtPeriodEndDate
            objOTRequisitionDetail._NormalOTTranHeadId = CInt(cboNormalOTTranHead.SelectedValue)
            objOTRequisitionDetail._HolidayOTTranHeadId = CInt(cboHolidayOTTranHead.SelectedValue)
            objOTRequisitionDetail._ViewByIds = mstrViewByIds
            objOTRequisitionDetail._ViewIndex = mintViewIndex
            objOTRequisitionDetail._ViewByName = mstrViewByName
            objOTRequisitionDetail._Analysis_Fields = mstrAnalysisFields
            objOTRequisitionDetail._Analysis_Join = mstrAnalysisJoin
            objOTRequisitionDetail._Analysis_OrderBy = mstrAnalysisOrderBy
            objOTRequisitionDetail._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objOTRequisitionDetail._Report_GroupName = mstrReportGroupName
            objOTRequisitionDetail._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objOTRequisitionDetail._OpenAfterExport = False

            GUI.fmtCurrency = Session("fmtCurrency").ToString()
            objOTRequisitionDetail._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objOTRequisitionDetail._UserUnkId = CInt(Session("UserId"))
            objOTRequisitionDetail._UserAccessFilter = CStr(Session("AccessLevelFilterString"))

            If CInt(Session("Employeeunkid")) > 0 Then
                objOTRequisitionDetail._UserName = Session("DisplayName").ToString()
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objOTRequisitionDetail._IncludeAccessFilterQry = False

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter : " & ex.Message, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If Not IsPostBack Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                SetLanguage()
                Call FillCombo()
                ResetValue()
                GetValue()
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then lnkAnalysisBy.Visible = False
            Else
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objOTRequisitionDetail As clsOTRequisition_Report
        'Pinkal (03-Sep-2020) -- End

        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub

            ElseIf CInt(cboNormalOTTranHead.SelectedValue) <= 0 AndAlso CInt(cboHolidayOTTranHead.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Transaction Head mapping is compulsory information.Please map any one transaction head."), Me)
                cboNormalOTTranHead.Focus()
                Exit Sub
            End If

            SetDateFormat()


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'If SetFilter() = False Then Exit Sub
            objOTRequisitionDetail = New clsOTRequisition_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If SetFilter(objOTRequisitionDetail) = False Then Exit Sub
            'Pinkal (03-Sep-2020) -- End



            objOTRequisitionDetail.Generate_OTRequisitionDetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                                         , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(mdtPeriodEndDate), Session("UserAccessModeSetting").ToString() _
                                                                                         , True, True, True)


            If objOTRequisitionDetail._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objOTRequisitionDetail._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objOTRequisitionDetail = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnSaveSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            objUserDefRMode._Reportunkid = enArutiReport.OTRequisition_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = IIf(CInt(cboNormalOTTranHead.SelectedValue) > 0, CInt(cboNormalOTTranHead.SelectedValue), 0).ToString() & "," & IIf(CInt(cboHolidayOTTranHead.SelectedValue) > 0, CInt(cboHolidayOTTranHead.SelectedValue), 0).ToString()
            intUnkid = objUserDefRMode.isExist(enArutiReport.OTRequisition_Report, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            If mblnFlag Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Selection Saved Successfully."), Me)
            Else
                DisplayMessage.DisplayMessage(objUserDefRMode._Message, Me)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objUserDefRMode = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    'Pinkal (15-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion Reports for NMB.
    'Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
    '    Try
    '        If CInt(cboReportType.SelectedIndex) = 0 Then    'Detail Report
    '            GetValue()
    '            cboNormalOTTranHead.Enabled = True
    '            cboHolidayOTTranHead.Enabled = True
    '            btnSaveSettings.Enabled = True

    '        ElseIf CInt(cboReportType.SelectedIndex) = 1 Then   'Summary Report
    '            cboNormalOTTranHead.SelectedValue = "0"
    '            cboNormalOTTranHead.Enabled = False
    '            cboHolidayOTTranHead.SelectedValue = "0"
    '            cboHolidayOTTranHead.Enabled = False
    '            btnSaveSettings.Enabled = False
    '        End If
    '        cboPeriod.SelectedIndex = 0
    '        cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("cboReportType_SelectedIndexChanged : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Pinkal (15-Jan-2020) -- End

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                LblPeriodValue.Text = objPeriod._TnA_StartDate.Date & " - " & objPeriod._TnA_EndDate.Date
                mdtPeriodStartDate = objPeriod._TnA_StartDate.Date
                mdtPeriodEndDate = objPeriod._TnA_EndDate.Date
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objPeriod = Nothing
                'Pinkal (03-Sep-2020) -- End
            Else
                LblPeriodValue.Text = ""
                mdtPeriodStartDate = Nothing
                mdtPeriodEndDate = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAnalysisBy_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysisFields = popupAnalysisBy._Analysis_Fields
            mstrAnalysisJoin = popupAnalysisBy._Analysis_Join
            mstrAnalysisOrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReportGroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysisOrderByGName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAnalysisBy_buttonApply_Click :- " & ex.Message, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAnalysisBy_buttonClose_Click :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Pinkal (15-Jan-2020) -- Start
        'Enhancements -  Working on OT Requisistion Reports for NMB.
        'Me.lblPageHeader.Text = Language._Object.getCaption("gbFilterCriteria", objOTRequisitionDetail._ReportName)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Pinkal (15-Jan-2020) -- End
        Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

        Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)

        'Pinkal (15-Jan-2020) -- Start
        'Enhancements -  Working on OT Requisistion Reports for NMB.
        'Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.ID, Me.LblReportType.Text)
        'Pinkal (15-Jan-2020) -- End

        Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.ID, Me.LblPeriod.Text)
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
        Me.LblNormalOT.Text = Language._Object.getCaption(Me.LblNormalOT.ID, Me.LblNormalOT.Text)
        Me.LblHolidayOT.Text = Language._Object.getCaption(Me.LblHolidayOT.ID, Me.LblHolidayOT.Text)

        Me.btnSaveSettings.Text = Language._Object.getCaption(Me.btnSaveSettings.ID, Me.btnSaveSettings.Text).Replace("&", "")
        Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Transaction Head mapping is compulsory information.Please map any one transaction head.")
            Language.setMessage(mstrModuleName, 3, "Selection Saved Successfully.")
            Language.setMessage(mstrModuleName, 4, "OT Requisition Detail Report")
            Language.setMessage(mstrModuleName, 5, "OT Requisition Summary Report")

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages :- " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
