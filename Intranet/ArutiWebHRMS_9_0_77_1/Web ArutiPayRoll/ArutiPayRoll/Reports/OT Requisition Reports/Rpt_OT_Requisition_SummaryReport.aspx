﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" Title = "OT Requisition Summary Report"
    CodeFile="Rpt_OT_Requisition_SummaryReport.aspx.vb" Inherits="Reports_OT_Requisition_Reports_Rpt_OT_Requisition_SummaryReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="OT Requisition Summary Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                          
                                       <%-- <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:Label ID="LblPeriodValue" runat="server" Text = "" />
                                            </td>
                                        </tr>--%>
                                          <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc9:Export runat="server" ID="Export" />
                    </div>
                      <uc1:AnalysisBy ID="popupAnalysisBy" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
