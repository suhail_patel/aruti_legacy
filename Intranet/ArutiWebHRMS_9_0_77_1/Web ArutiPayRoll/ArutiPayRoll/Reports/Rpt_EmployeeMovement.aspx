﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_EmployeeMovement.aspx.vb" Inherits="Reports_Rpt_EmployeeMovement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx"  TagName="Export" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 50%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Movement Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position:relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="drpemployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtFromDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtToDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblAllocationReason" runat="server" Text="Change Reason"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboAllocationReason" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblFromscale" runat="server" Text="From Scale"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtFromScale" runat="server" Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%; text-align: center">
                                                <asp:Label ID="lblToScale" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtToScale" runat="server" Style="text-align: right"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:CheckBox ID="chkShowEmpScale" runat="server" Text="Show Employee Scale" Checked="false"
                                                    CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:Export runat="server" ID="Export" />

                </ContentTemplate>
                 <Triggers>
                 <asp:PostBackTrigger ControlID="Export" />
</Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
