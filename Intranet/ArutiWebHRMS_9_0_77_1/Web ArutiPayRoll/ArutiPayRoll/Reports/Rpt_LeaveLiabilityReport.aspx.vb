﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
#End Region

Partial Class Reports_Rpt_LeaveLiabilityReport
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmLeaveLiability_Report"

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeaveLiability As clsLeaveLiability_Report
    'Pinkal (11-Sep-2020) -- End



    'Private mstrAdvanceFilter As String = String.Empty
    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty

#End Region

#Region " Page's Events "
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            Call SetLanguage()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeaveLiability = New clsLeaveLiability_Report
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End

            If IsPostBack = False Then

                'Pinkal (30-Jun-2021)-- Start
                'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
                If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                    chkOtherEarningBasicSal.Enabled = False
                Else
                    chkOtherEarningBasicSal.Enabled = True
                End If
                'Pinkal (30-Jun-2021) -- End

                Call FillCombo()
                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                dtpAsonDate.SetDate = Nothing
                'Pinkal (03-May-2019) -- End

                'Pinkal (30-Jun-2021)-- Start
                'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
                GetValue()
                'Pinkal (30-Jun-2021) -- End
            Else
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
            'Hemant (13 Aug 2020) -- Start

         
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "
    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objEmp As New clsEmployee_Master
            Dim objLeave As New clsleavetype_master
            Dim objTransactionHead As New clsTransactionHead

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, True, CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With
                objEmp = Nothing

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.


                'Pinkal (06-Dec-2019) -- Start
                'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
                'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", True)
                'Else
                '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
                'End If
                    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
                'Pinkal (06-Dec-2019) -- End

                'Pinkal (25-May-2019) -- End


                With cboLeave
                    .DataValueField = "leavetypeunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("Leave")
                    .DataBind()
                    .SelectedValue = "0"
                End With
                objLeave = Nothing


                'Pinkal (30-Jun-2021)-- Start
                'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
                'dsList = objTransactionHead.getComboList(CStr(Session("Database_Name")), "OtherEarning", True, , , enTypeOf.Other_Earnings)
                dsList = objTransactionHead.getComboList(CStr(Session("Database_Name")), "OtherEarning", True)
                'Pinkal (30-Jun-2021) -- End
                With cboOtherEarning
                    .DataValueField = "tranheadunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("OtherEarning")
                    .DataBind()
                    .SelectedValue = "0"
                End With

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objTransactionHead = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = "0"
            cboLeave.SelectedValue = "0"

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            'chkOtherEarningBasicSal.Checked = False
            'cboOtherEarning.SelectedValue = "0"
            'pnl_OtherEarning.Enabled = False
            'Pinkal (30-Jun-2021) -- End

            mstrViewByIds = ""
            mintViewIndex = 0
            mstrViewByName = ""
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            dtpAsonDate.SetDate = Nothing
            'Pinkal (03-May-2019) -- End


            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            GetValue()
            'Pinkal (30-Jun-2021) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLeaveLiability As clsLeaveLiability_Report) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objLeaveLiability.SetDefaultValue()
            objLeaveLiability.setDefaultOrderBy(0)
            objLeaveLiability._EmployeeId = CInt(cboEmployee.SelectedValue)
            objLeaveLiability._Employee = cboEmployee.SelectedItem.Text
            objLeaveLiability._LeaveId = CInt(cboLeave.SelectedValue)
            objLeaveLiability._LeaveName = cboLeave.SelectedItem.Text
            objLeaveLiability._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            objLeaveLiability._YearId = CInt(Session("Fin_year"))
            objLeaveLiability._ViewByIds = mstrViewByIds
            objLeaveLiability._ViewIndex = mintViewIndex
            objLeaveLiability._ViewByName = mstrViewByName
            objLeaveLiability._Analysis_Fields = mstrAnalysisFields
            objLeaveLiability._Analysis_Join = mstrAnalysisJoin
            objLeaveLiability._Analysis_OrderBy = mstrAnalysisOrderBy
            objLeaveLiability._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objLeaveLiability._Report_GroupName = mstrReportGroupName

            If chkOtherEarningBasicSal.Checked = True Then
                objLeaveLiability._OtherEarningTranId = CInt(cboOtherEarning.SelectedValue)
            Else
                objLeaveLiability._OtherEarningTranId = 0
            End If

            objLeaveLiability._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            objLeaveLiability._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objLeaveLiability._OpenAfterExport = False

            objLeaveLiability._LeaveLiabilitySetting = CStr(Session("LeaveLiabilityReportSetting"))

            GUI.fmtCurrency = Session("fmtCurrency").ToString()


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If dtpAsonDate.IsNull = False Then
                objLeaveLiability._AsonDate = dtpAsonDate.GetDate.Date
            Else
                objLeaveLiability._AsonDate = Nothing
            End If

            objLeaveLiability._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
            objLeaveLiability._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
            objLeaveLiability._DatabaseStartDate = CDate(Session("fin_startdate")).Date
            objLeaveLiability._DatabaseEndDate = CDate(Session("fin_enddate")).Date
            'Pinkal (03-May-2019) -- End


            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            objLeaveLiability._OtherEarningTranHead = cboOtherEarning.SelectedItem.Text
            'Pinkal (30-Jun-2021) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter :- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetFilter :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function


    'Pinkal (30-Jun-2021)-- Start
    'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Leave_Liability_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                Dim mstrIds As String = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                If mstrIds.Trim.Length > 0 Then
                    Dim ar() As String = mstrIds.Trim().Split(CChar(","))
                    If ar.Length > 0 Then
                        chkIncludeLeaveBF.Checked = CBool(ar(0))
                        If ar.Length > 1 Then
                            chkOtherEarningBasicSal.Checked = True
                            cboOtherEarning.SelectedValue = CInt(ar(1)).ToString()
                        End If
                    End If
                    chkOtherEarningBasicSal_CheckedChanged(chkOtherEarningBasicSal, New EventArgs())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Pinkal (30-Jun-2021) -- End


#End Region

#Region " Button's Events "
    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveLiability As New clsLeaveLiability_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End
        Try


            If chkOtherEarningBasicSal.Checked = True AndAlso CInt(cboOtherEarning.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Please select transaction head."), Me)
                cboOtherEarning.Focus()
                Exit Sub
            End If

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.

            If chkOtherEarningBasicSal.Checked AndAlso dtpAsonDate.IsNull Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "As on Date is compulsory information.Please select As on Date."), Me)
                Exit Sub
            End If

            If chkOtherEarningBasicSal.Checked AndAlso dtpAsonDate.IsNull = False Then

                Dim mdtDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date

                If dtpAsonDate.IsNull = False Then
                    mdtDate = dtpAsonDate.GetDate.Date
                End If

                Dim objMaster As New clsMasterData
                Dim xPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, mdtDate, CInt(Session("Fin_year")))
                objMaster = Nothing

                Dim objPeriod As New clscommom_period_Tran
                If xPeriodId > 0 Then
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = xPeriodId
                End If

                Dim objTnALeaveTran As New clsTnALeaveTran

                Dim mstrEmployeeIds As String = ""
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    mstrEmployeeIds = CInt(cboEmployee.SelectedValue).ToString()
                End If

                If objTnALeaveTran.IsPayrollProcessDone(xPeriodId, mstrEmployeeIds, Nothing, enModuleReference.Payroll, Nothing, Nothing) = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "you can not generate this report.Reason:Payroll Process is not done for the current open period.Please do payroll process for the current period."), Me)
                    Exit Sub
                End If

                objTnALeaveTran = Nothing
                objPeriod = Nothing

            End If

            'Pinkal (30-Jun-2021) -- End




            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objLeaveLiability) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End




            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            objLeaveLiability.Generate_DetailReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString, _
                                                    CStr(Session("UserAccessModeSetting")), True, CInt(Session("Base_CurrencyId")))

            If objLeaveLiability._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objLeaveLiability._FileNameAfterExported
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 


                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveLiability = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysisFields = popAnalysisby._Analysis_Fields
            mstrAnalysisJoin = popAnalysisby._Analysis_Join
            mstrAnalysisOrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysisOrderByGName = popAnalysisby._Analysis_OrderBy_GName
            mstrReportGroupName = popAnalysisby._Report_GroupName

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popAnalysisby_buttonApply_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " CheckBox's Events "
    Protected Sub chkOtherEarningBasicSal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherEarningBasicSal.CheckedChanged
        Try
            If chkOtherEarningBasicSal.Checked = True Then
                pnl_OtherEarning.Enabled = True
            Else
                cboOtherEarning.SelectedValue = "0"
                pnl_OtherEarning.Enabled = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkOtherEarningBasicSal_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkOtherEarningBasicSal_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " LinkButton Event "
    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAnalysisBy_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkAnalysisBy_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (30-Jun-2021)-- Start
    'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.

    Private Sub lnkSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkSave.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Try
            objUserDefRMode = New clsUserDef_ReportMode()
            objUserDefRMode._Reportunkid = enArutiReport.Leave_Liability_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 0
            Dim intUnkid As Integer = -1

            If chkOtherEarningBasicSal.Checked AndAlso CInt(cboOtherEarning.SelectedValue) > 0 Then
                objUserDefRMode._EarningTranHeadIds = IIf(chkIncludeLeaveBF.Checked, 1, 0).ToString() & "," & CInt(cboOtherEarning.SelectedValue).ToString()
            Else
                objUserDefRMode._EarningTranHeadIds = IIf(chkIncludeLeaveBF.Checked, 1, 0).ToString()
            End If

            intUnkid = objUserDefRMode.isExist(enArutiReport.Leave_Liability_Report, 0, 0, 0)

            objUserDefRMode._Reportmodeunkid = intUnkid
            If intUnkid <= 0 Then
                objUserDefRMode.Insert()
            Else
                objUserDefRMode.Update()
            End If
            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Setting saved successfully."), Me)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Pinkal (30-Jun-2021) -- End

#End Region

    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.ID, Me.lblLeaveName.Text)
            Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.chkOtherEarningBasicSal.Text = Language._Object.getCaption(Me.chkOtherEarningBasicSal.ID, Me.chkOtherEarningBasicSal.Text)
            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Me.LblAsonDate.Text = Language._Object.getCaption(Me.LblAsonDate.ID, Me.LblAsonDate.Text)
            'Pinkal (03-May-2019) -- End

            'Pinkal (30-Jun-2021)-- Start
            'Netiz / Scania Enhancements : Working on Leave Liability Report Enhancements.
            Me.chkIncludeLeaveBF.Text = Language._Object.getCaption(Me.chkIncludeLeaveBF.ID, Me.chkIncludeLeaveBF.Text)
            Me.lnkSave.Text = Language._Object.getCaption(Me.lnkSave.ID, Me.lnkSave.Text)
            'Pinkal (30-Jun-2021) -- End

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage :- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Please select transaction head.")
            Language.setMessage(mstrModuleName, 2, "Setting saved successfully.")
            Language.setMessage(mstrModuleName, 3, "As on Date is compulsory information.Please select As on Date.")
            Language.setMessage(mstrModuleName, 4, "you can not generate this report.Reason:Payroll Process is not done for the current open period.Please do payroll process for the current period.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
