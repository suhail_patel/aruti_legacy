﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.Globalization

#End Region

Partial Class Reports_Rpt_Daily_Time_Sheet
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmDailyTimeSheet"
    Private objDailyTimeSheet As clsDailyTimeSheet

    Private mstrViewByIds As String = String.Empty
    Private mstrViewByName As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrAnalysisFields As String = ""
    Private mstrAnalysisJoin As String = ""
    Private mstrAnalysisOrderBy As String = ""
    Private mstrReportGroupName As String = ""
    Private mstrAnalysisOrderByGName As String = ""


#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            objEmp = Nothing

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            'Dim objShift As New clsNewshift_master
            'dsList = objShift.getListForCombo("List", True)
            'With cboShift
            '    .DataValueField = "shiftunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = CStr(0)
            '    .DataBind()
            'End With
            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SHIFT_TYPE, True, "List")
            With cboShiftType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            Call cboShiftType_SelectedIndexChanged(New Object(), New EventArgs())
            'S.SANDEEP |29-MAR-2019| -- END
            

            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 1, "Daily Timesheet"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 2, "Daily Timesheet On Duty"))
            cboReportType.Items.Add(Language.getMessage(mstrModuleName, 3, "Daily Timesheet Off Duty"))
            cboReportType.SelectedIndex = 0

            Dim objMData As New clsMasterData
            dsList = objMData.GetEAllocation_Notification("List")
            Dim drow As DataRow = dsList.Tables("List").NewRow
            drow.Item("Id") = 0
            drow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Select Allocation for Report")
            dsList.Tables("List").Rows.InsertAt(drow, 0)
            With cboReportColumn
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtDate.SetDate = DateTime.Now
            cboEmployee.SelectedValue = CStr(0)
            cboShift.SelectedValue = CStr(0)
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = -1
            chkInactiveemp.Checked = False
            cboReportType.SelectedIndex = 0
            cboReportColumn.SelectedValue = CStr(Session("SelectedAllocationForDailyTimeSheetReport_Voltamp"))
            chkShowEmployeeStatus.Checked = CBool(Session("ShowEmployeeStatusForDailyTimeSheetReport_Voltamp"))
            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            cboShiftType.SelectedValue = CStr(0)
            Call cboShiftType_SelectedIndexChanged(New Object(), New EventArgs())
            'S.SANDEEP |29-MAR-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objDailyTimeSheet.SetDefaultValue()
            objDailyTimeSheet._Date = dtDate.GetDate
            objDailyTimeSheet._EmpId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
            objDailyTimeSheet._EmpName = cboEmployee.SelectedItem.Text
            objDailyTimeSheet._IsActive = chkInactiveemp.Checked
            objDailyTimeSheet._Shiftunkid = CInt(IIf(cboShift.SelectedValue = "", 0, cboShift.SelectedValue))
            objDailyTimeSheet._ShiftName = cboShift.SelectedItem.Text
            objDailyTimeSheet._ViewByIds = mstrViewByIds
            objDailyTimeSheet._ViewIndex = mintViewIndex
            objDailyTimeSheet._ViewByName = mstrViewByName
            objDailyTimeSheet._Analysis_Fields = mstrAnalysisFields
            objDailyTimeSheet._Analysis_Join = mstrAnalysisJoin
            objDailyTimeSheet._Analysis_OrderBy = mstrAnalysisOrderBy
            objDailyTimeSheet._Report_GroupName = mstrReportGroupName
            objDailyTimeSheet._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objDailyTimeSheet._ReportTypeName = cboReportType.Text
            objDailyTimeSheet._SelectedAllocationId = CInt(IIf(cboReportColumn.SelectedValue = "", 0, cboReportColumn.SelectedValue))
            objDailyTimeSheet._AllocationName = cboReportColumn.SelectedItem.Text
            objDailyTimeSheet._ShowEmployeeStatus = chkShowEmployeeStatus.Checked

            'S.SANDEEP |29-MAR-2019| -- START
            'ENHANCEMENT : 0003630 {PAPAYE}.
            objDailyTimeSheet._ShiftTypeId = CInt(IIf(cboShiftType.SelectedValue = "", 0, cboShiftType.SelectedValue))
            objDailyTimeSheet._ShiftTypeName = cboShiftType.SelectedItem.Text
            'S.SANDEEP |29-MAR-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFilter:- " & ex.Message, Me)
        End Try
    End Function

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.LblShift.Text = Language._Object.getCaption(Me.LblShift.ID, Me.LblShift.Text)
            Me.lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Me.LblReportType.Text = Language._Object.getCaption(Me.LblReportType.ID, Me.LblReportType.Text)
            Me.gbShowColumns.Text = Language._Object.getCaption(Me.gbShowColumns.ID, Me.gbShowColumns.Text)
            Me.chkShowEmployeeStatus.Text = Language._Object.getCaption(Me.chkShowEmployeeStatus.ID, Me.chkShowEmployeeStatus.Text)
            Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.ID, Me.lblAllocation.Text)
            Me.lnkSaveChanges.Text = Language._Object.getCaption(Me.lnkSaveChanges.ID, Me.lnkSaveChanges.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Page Event(S) "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objDailyTimeSheet = New clsDailyTimeSheet(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            Call SetLanguage()

            If Not IsPostBack Then
                Call FillCombo()
                Call ResetValue()
            Else
                mstrViewByIds = CStr(Me.ViewState("mstrStringIds"))
                mstrViewByName = CStr(Me.ViewState("mstrStringName"))
                mintViewIndex = CInt(Me.ViewState("mintViewIdx"))
                mstrAnalysisFields = CStr(Me.ViewState("mstrAnalysis_Fields"))
                mstrAnalysisJoin = CStr(Me.ViewState("mstrAnalysis_Join"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("mstrAnalysis_OrderBy"))
                mstrReportGroupName = CStr(Me.ViewState("mstrReport_GroupName "))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreInit:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrStringIds") = mstrViewByIds
            Me.ViewState("mstrStringName") = mstrViewByName
            Me.ViewState("mintViewIdx") = mintViewIndex
            Me.ViewState("mstrAnalysis_Fields") = mstrAnalysisFields
            Me.ViewState("mstrAnalysis_Join") = mstrAnalysisJoin
            Me.ViewState("mstrAnalysis_OrderBy") = mstrAnalysisOrderBy
            Me.ViewState("mstrReport_GroupName ") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Button Event(S) "

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objDailyTimeSheet._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objDailyTimeSheet._UserUnkId = CInt(Session("UserId"))
            Call SetDateFormat()
            objDailyTimeSheet.setDefaultOrderBy(0)
            objDailyTimeSheet.generateReportNew(Session("Database_Name").ToString, _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                           CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           CStr(Session("ExportReportPath")), _
                                           CBool(Session("OpenAfterExport")), 0, enPrintAction.None, _
                                           enExportAction.None, _
                                           CInt(Session("Base_CurrencyId")))
            Session("objRpt") = objDailyTimeSheet._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReport_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popAnalysisby_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popAnalysisby.buttonApply_Click
        Try
            mstrViewByIds = popAnalysisby._ReportBy_Ids
            mstrViewByName = popAnalysisby._ReportBy_Name
            mintViewIndex = popAnalysisby._ViewIndex
            mstrAnalysisFields = popAnalysisby._Analysis_Fields
            mstrAnalysisJoin = popAnalysisby._Analysis_Join
            mstrAnalysisOrderBy = popAnalysisby._Analysis_OrderBy
            mstrAnalysisOrderByGName = popAnalysisby._Analysis_OrderBy_GName
            mstrReportGroupName = popAnalysisby._Report_GroupName

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popAnalysisby_buttonApply_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popAnalysisby_buttonApply_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case CInt(cboReportType.SelectedIndex)
                Case 0
                    pnlColumns.Visible = True
                Case 1, 2
                    pnlColumns.Visible = False
            End Select
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboReportType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboReportType_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |29-MAR-2019| -- START
    'ENHANCEMENT : 0003630 {PAPAYE}.
    Private Sub cboShiftType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboShiftType.SelectedIndexChanged
        Try
            Dim objShift As New clsNewshift_master : Dim dsList As New DataSet
            dsList = objShift.getListForCombo("List", True, CInt(IIf(cboShiftType.SelectedValue = "", 0, cboShiftType.SelectedValue)))
            Dim dtTable As DataTable = Nothing
            If CInt(IIf(cboShiftType.SelectedValue = "", 0, cboShiftType.SelectedValue)) <= 0 Then
                dtTable = dsList.Tables(0).Select("shiftunkid = 0").CopyToDataTable()
            Else
                dtTable = dsList.Tables(0)
            End If
            With cboShift
                .DataValueField = "shiftunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .SelectedValue = CStr(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboShiftType_SelectedIndexChanged:- " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |29-MAR-2019| -- END

#End Region

#Region " Link Event(s) "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popAnalysisby.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAnalysisBy_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkSaveChanges_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSaveChanges.Click
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = CInt(Session("Companyunkid"))
            objConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp = CInt(IIf(cboReportColumn.SelectedValue = "", 0, cboReportColumn.SelectedValue))
            objConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp = chkShowEmployeeStatus.Checked
            objConfig.updateParam()
            Session("SelectedAllocationForDailyTimeSheetReport_Voltamp") = objConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp
            Session("ShowEmployeeStatusForDailyTimeSheetReport_Voltamp") = objConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkSaveChanges_Click :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

End Class
