﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Err.aspx.vb" Inherits="Err" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <link href="Help/alert/sweetalert.css" rel="stylesheet" type="text/css" />
    
    <script src="Help/alert/sweetalert.min.js" type="text/javascript"></script>
    <script src="Help/alert/dialogs.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="sweet-overlay" style="display:block;">
    <div id="sweetalert" class="sweet-alert showSweetAlert visible" data-custom-class="" data-has-cancel-button="true" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="true" data-animation="pop" data-timer="null" style="display: block; margin-top: -152px; z-index: 99999999; top: 378.5px; left: 720px;"><div id="dragme" style="padding:10px;"></div><div class="sa-icon sa-error" style="display: none;">
      <span class="sa-x-mark">
        <span class="sa-line sa-left"></span>
        <span class="sa-line sa-right"></span>
      </span>
    </div><div class="sa-icon sa-warning" style="display: none;opacity: 1.04;">
      <span class="sa-body"></span>
      <span class="sa-dot"></span>
    </div><div class="sa-icon sa-info" style="display: block;"></div><div class="sa-icon sa-success" style="display: none;">
      <span class="sa-line sa-tip"></span>
      <span class="sa-line sa-long"></span>

      <div class="sa-placeholder"></div>
      <div class="sa-fix"></div>
    </div><div class="sa-icon sa-custom" style="display: none;"></div><h2></h2>
    <p style="display: block;"><asp:Label ID="lblMessage" runat="server" Text="" /></p>
    <fieldset style="padding-right: 8px;">
      <input type="text" tabindex="-1" placeholder="">
      <div class="sa-input-error"></div>
    </fieldset><div class="sa-error-container">
      <div class="icon">!</div>
      <p>Not valid!</p>
    </div><div class="sa-button-container">
      <button class="cancel" type="button" tabindex="-1" style="display: inline-block; box-shadow: none;" onclick="javascript:history.go(-1);">Go Back!</button>      
      
    </div></div>
    </div>
    </form>
</body>
</html>
