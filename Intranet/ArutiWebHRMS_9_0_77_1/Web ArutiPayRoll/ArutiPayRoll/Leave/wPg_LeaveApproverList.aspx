﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LeaveApproverList.aspx.vb"
    Inherits="Leave_wPg_LeaveApproverList" Title="Leave Approver List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="confirmyesno" TagPrefix="cnfpopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
				var scroll = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
	 if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll.Y).val());
          
    }
}


            $("[id*=ChkAll]").live("click", function() {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function() {
                    if (chkHeader.is(":checked")) {
                        debugger;
                        if ($(this).is(":visible")) {
                            $(this).attr("checked", "checked");
                        }

                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=ChkgvSelect]").live("click", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkHeader]", grid);
                var row = $(this).closest("tr")[0];

                debugger;
                if (!$(this).is(":checked")) {
                    var row = $(this).closest("tr")[0];
                    chkHeader.removeAttr("checked");
                } else {

                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });


    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approver List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Leave Approver List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" />
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="drpLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <%--<uc4:ComboList ID="" runat="server" AutoPostBack="true" />--%>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver" Width="100%" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpApprover" runat="server">
                                                </asp:DropDownList>
                                                <%--<uc4:ComboList ID="" runat="server" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                height: 350px; overflow: auto">
                                <asp:DataGrid ID="dgView" runat="server" Width="99%" AutoGenerateColumns="False"
                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Edit"
                                                        ToolTip="Edit"></asp:LinkButton>
                                                </span>
                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit"
                                                        CommandName="" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                        ToolTip="Delete"></asp:LinkButton>
                                                </span>
                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="65px" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMapUser" runat="server" Text="Map User" Font-Underline="false"
                                                    CommandName="MapUser" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMapLeaveType" runat="server" Text="Map Leave Type" Font-Underline="false"
                                                    CommandName="MapLeaveType" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="65px" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkActiveApprover" runat="server" Text="Active" Font-Underline="false"
                                                    CommandName="Active" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-Width="65px" ItemStyle-Width="65px" HeaderStyle-HorizontalAlign="Center">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkInactiveApprover" runat="server" Text="Inactive" Font-Underline="false"
                                                    CommandName="Inactive" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="levelname" HeaderText="Level" ReadOnly="True" FooterText="colhLevel" />
                                        <asp:BoundColumn DataField="name" HeaderText="Approver Name" ReadOnly="True" FooterText="colhApproverName" />
                                        <asp:BoundColumn DataField="departmentname" HeaderText="Department" ReadOnly="True"
                                            FooterText="colhDepartment" />
                                        <asp:BoundColumn DataField="sectionname" HeaderText="Section" ReadOnly="True" FooterText="colhSection" />
                                        <asp:BoundColumn DataField="jobname" HeaderText="Job" ReadOnly="True" FooterText="colhJob" />
                                        <asp:BoundColumn DataField="approverunkid" HeaderText="ApproverId" ReadOnly="True"
                                            Visible="false" />
                                        <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                        <asp:BoundColumn DataField="ExAppr" HeaderText="External Approver" ReadOnly="true" FooterText="colhIsExternalApprover"></asp:BoundColumn>
                                    </Columns>
                                    <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                </asp:DataGrid>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupUserMapping" BackgroundCssClass="modalBackground"
                        TargetControlID="LblUserMapping" runat="server" PopupControlID="pnlUserMapPopup"
                        DropShadow="true" CancelControlID="btnMapClose" />
                    <asp:Panel ID="pnlUserMapPopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 375px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblUserMapping" runat="server" Text="User Mapping" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblApproverMap" runat="server" Text="Approver" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="LblApproverVal" runat="server" Text="" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblLevelMap" runat="server" Text="Approver Level" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="LblLevelVal" runat="server" Text="" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblUser" runat="server" Text="User" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:DropDownList ID="drpUser" runat="server" Width="180px">
                                                    </asp:DropDownList>
                                                    <%--<uc4:ComboList ID="" runat="server" Width="200" Height="20" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnMapSave" runat="server" Text="Save" CssClass="btndefault" />
                                            <asp:Button ID="btnMapDelete" runat="server" Text="Delete" CssClass="btndefault" />
                                            <asp:Button ID="btnMapClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupLeaveMapping" BackgroundCssClass="modalBackground"
                        TargetControlID="LblLeaveMapping" runat="server" PopupControlID="pnlLeaveMappingPopup"
                        DropShadow="true" CancelControlID="btnLeaveMapClose" />
                    <asp:Panel ID="pnlLeaveMappingPopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="Label1" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div4" class="panel-default">
                                    <div id="Div5" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblLeaveMapping" runat="server" Text="Leave Type Mapping" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div6" class="panel-body-default">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblApproverLeaveMap" runat="server" Text="Approver" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="LblApproverLeaveMapVal" runat="server" Text="" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblLevelLeaveMap" runat="server" Text="Approver Level" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="LblLevelLeaveMapVal" runat="server" Text="" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnLeaveMapSave" runat="server" Text="Save" CssClass="btndefault" />
                                            <asp:Button ID="btnLeaveMapClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                    <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                        height: 350px; overflow: auto">
                                        <asp:GridView ID="GvLeaveTypeMapping" runat="server" AutoGenerateColumns="false"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames = "leavetypeunkid,leavetypecode">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                     <%-- 'Pinkal (28-Apr-2020) -- Start
                                                              'Optimization  - Working on Process Optimization and performance for require module.
                                                        <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged" /> --%>
                                                        <asp:CheckBox ID="ChkAll" runat="server" />
                                                        <%--'Pinkal (28-Apr-2020) -- End  --%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                    <%-- 'Pinkal (28-Apr-2020) -- Start
                                                              'Optimization  - Working on Process Optimization and performance for require module.
                                                        <asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged" />--%>
                                                         <asp:CheckBox ID="ChkgvSelect" runat="server" />
                                                        <%--'Pinkal (28-Apr-2020) -- End  --%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="leavetypecode" HeaderText="Leave Code" ReadOnly="True"
                                                    FooterText="dgcolhLeaveCode" />
                                                <asp:BoundField DataField="leavename" HeaderText="Leave Type" ReadOnly="True" FooterText="dgcolhLeaveType" />
                                                <asp:BoundField DataField="ispaid" HeaderText="IsPaid" ReadOnly="True" FooterText="dgcolhPaidLeave" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ucDel:DeleteReason ID="popup1" runat="server" Title="Are you Sure You Want To delete?:" />
                    <cnfpopup:confirmyesno ID="popupActive" runat="server" />
                    <cnfpopup:confirmyesno ID="popupInactive" runat="server" />
                    <%--<asp:Panel ID="pnlpopup" Width="100%" Height="100%" runat="server" Style="display: none"
                        CssClass="modalPopup">
                        <asp:Panel ID="Panel2" runat="server" Style="cursor: move; background-color: #DDDDDD;
                            border: solid 1px Gray; color: Black">
                            <div>
                                <p>
                                    Are you sure you want to delete this Approver?</p>
                            </div>
                        </asp:Panel>
                        <table style="width: 180px;">
                            <tr>
                                <td>
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtreasondel" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td align="right">
                                    <asp:Button ID="BtnDelete" runat="server" Text="Delete" />
                                    <asp:Button ID="BtnCancel" runat="server" Text="Cancel" />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="BtnCancel" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreasondel">
                    </cc1:ModalPopupExtender>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
