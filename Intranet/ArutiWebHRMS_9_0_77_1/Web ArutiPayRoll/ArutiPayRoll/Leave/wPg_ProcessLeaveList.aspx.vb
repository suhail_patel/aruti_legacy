﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class Leave_wPg_ProcessLeaveList
    Inherits Basepage

#Region "Private Variable"
    Private Shared ReadOnly mstrModuleName As String = "frmProcessLeaveList"
    Private ReadOnly mstrModuleName4 As String = "frmLeaveExpense"
    Private ReadOnly mstrModuleNameClaim As String = "frmClaims_RequestAddEdit"

    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private ReadOnly mstrModuleName2 As String = "frmDependentsList"
    'Pinkal (01-Mar-2016) -- End


    Private displaymessage As New CommonCodes


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objApprover As New clsleaveapprover_master
    'Private objPendingLeave As New clspendingleave_Tran
    'Pinkal (11-Sep-2020) -- End

    Private mstrEmployeeIDs As String = ""
    Private mintApproverunkid As Integer = -1
    Private mintLeaveFormUnkId As Integer = -1
    Private mblnClaimRequest As Boolean = False
    Private mdtProcessList As DataTable = Nothing
    'Private dsPedingList As DataSet = Nothing
    Private mdtExpense As DataTable = Nothing

    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private mblnIsExternalApprover As Boolean = False
    'Pinkal (01-Mar-2016) -- End


    'Pinkal (04-Oct-2017) -- Start
    'Bug - Problem solving For TRA Leave Process List Issue.
    Private mdsPendingList As DataSet = Nothing
    'Pinkal (04-Oct-2017) -- End


#End Region

#Region "Page Event(S)"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            SetLanguage()

            If IsPostBack = False Then


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                Call FillCombo()

                dtpApplyDate.SetDate = Nothing
                dtpStartDate.SetDate = Nothing
                dtpEndDate.SetDate = Nothing

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    Dim objApprover As New clsleaveapprover_master
                    'Pinkal (11-Sep-2020) -- End

                    'Pinkal (01-Apr-2020) -- Start
                    'ENHANCEMENT NMB:  Searching Issue when approver don't have any pending leave application it was showing all the leave application which was not under this approver.
                    Dim objMapping As New clsapprover_Usermapping
                    objMapping.GetData(enUserType.Approver, , CInt(Session("UserId")), )
                    objApprover._Approverunkid = objMapping._Approverunkid
                    mintApproverunkid = objApprover._Approverunkid
                    objMapping = Nothing
                    'Pinkal (01-Apr-2020) --End

                    Dim dtList As New DataTable
                    dtList = objApprover.GetEmployeeApprover(Session("Database_Name").ToString, _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                             False, CInt(Session("UserId")))
                    If dtList.Rows.Count > 0 Then
                        txtApprover.Text = dtList.Rows(0)("employeename").ToString
                    End If


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objApprover = Nothing
                    'Pinkal (11-Sep-2020) -- End

                Else
                    chkMyApprovals.Checked = False
                    chkMyApprovals.Visible = False
                    txtApprover.Text = ""
                End If
            Else
                mstrEmployeeIDs = Me.ViewState("mstrEmployeeIDs").ToString
                mintApproverunkid = CInt(Me.ViewState("Approverunkid"))
                mintLeaveFormUnkId = CInt(Me.ViewState("mintLeaveFormUnkId"))
                mblnClaimRequest = CBool(Me.ViewState("mblnClaimRequest"))
                mdtProcessList = CType(Session("ProcessList"), DataTable)
                'dsPedingList = CType(Session("dsPedingList"), DataSet)

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))
                'Pinkal (01-Mar-2016) -- End


                'Pinkal (04-Oct-2017) -- Start
                'Bug - Problem solving For TRA Leave Process List Issue.
                mdsPendingList = CType(Session("mdsPendingList"), DataSet)
                'Pinkal (04-Oct-2017) -- End

                'Pinkal (01-Apr-2020) -- Start
                'ENHANCEMENT NMB:  Searching Issue when approver don't have any pending leave application it was showing all the leave application which was not under this approver.


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'If mintApproverunkid > 0 Then objApprover._Approverunkid = mintApproverunkid
                'Pinkal (11-Sep-2020) -- End


                'Pinkal (01-Apr-2020) -- End

            End If

            If mdtProcessList IsNot Nothing Then
                dgView.DataSource = mdtProcessList
                dgView.DataBind()
            End If

            If mblnClaimRequest Then
                popupClaimRequest.Show()
            End If
        Catch ex As Exception
            displaymessage.DisplayError("Page_Load1:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrEmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("Approverunkid") = mintApproverunkid
            Me.ViewState("mintLeaveFormUnkId") = mintLeaveFormUnkId
            Me.ViewState("mblnClaimRequest") = mblnClaimRequest
            Session("ProcessList") = mdtProcessList

            'Pinkal (04-Oct-2017) -- Start
            'Bug - Problem solving For TRA Leave Process List Issue.
            Session("mdsPendingList") = mdsPendingList
            'Pinkal (04-Oct-2017) -- End

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover
            'Pinkal (01-Mar-2016) -- End



        Catch ex As Exception
            displaymessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Private Method"

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objLeaveType As New clsleavetype_master

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End

        Dim dsFill As DataSet = Nothing
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            dsFill = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataTextField = "EmpCodeName"
                cboEmployee.DataSource = dsFill.Tables(0).Copy
                cboEmployee.DataBind()
                'Nilay (09-Aug-2016) -- End


                'Pinkal (27-Apr-2016) -- Start
                'Enhancement -Bug Solve For CCK
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                dsFill = objApprover.GetEmployeeFromUser(Session("Database_Name").ToString(), CInt(Session("Fin_year")) _
                                                                            , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                                            , CBool(Session("IsIncludeInactiveEmp")), CInt(Session("UserId")), CBool(Session("AllowtoApproveLeave")), CBool(Session("AllowIssueLeave")))

                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataTextField = "EmpCodeName"
                cboEmployee.DataSource = dsFill.Tables(0).Copy
                cboEmployee.DataBind()
                'Nilay (09-Aug-2016) -- End

                'Pinkal (27-Apr-2016) -- End
            End If

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'cboEmployee.DataValueField = "employeeunkid"
            'cboEmployee.DataTextField = "employeename"
            'cboEmployee.DataSource = dsFill.Tables(0).Copy
            'cboEmployee.DataBind()
            'Nilay (09-Aug-2016) -- End

            Dim lstIDs As List(Of String) = (From p In dsFill.Tables(0) Where (CInt(p.Item("employeeunkid")) > 0) Select (p.Item("employeeunkid").ToString)).Distinct.ToList
            mstrEmployeeIDs = String.Join(",", CType(lstIDs.ToArray(), String()))


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If lstIDs IsNot Nothing Then lstIDs.Clear()
            lstIDs = Nothing
            'Pinkal (11-Sep-2020) -- End


            dsFill = Nothing


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsFill = objMaster.getLeaveStatusList("Status", True)
            dsFill = objMaster.getLeaveStatusList("Status", Session("ApplicableLeaveStatus").ToString(), True)
            'Pinkal (03-Jan-2020) -- End


            cboStatus.DataValueField = "statusunkid"
            cboStatus.DataTextField = "name"
            cboStatus.DataSource = dsFill.Tables("Status")
            cboStatus.DataBind()


            cboStatus.SelectedValue = "2"   'FOR PENDING STATUS BY DEFAULT"

            dsFill = Nothing


            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'dsFill = objLeaveType.getListForCombo("LeaveType", True)
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsFill = objLeaveType.getListForCombo("LeaveType", True, -1, Session("Database_Name").ToString(), "", True)
            Else
                dsFill = objLeaveType.getListForCombo("LeaveType", True, -1, Session("Database_Name").ToString(), "", False)
            End If
            'Pinkal (06-Dec-2019) -- End


            cboLeaveType.DataValueField = "leavetypeunkid"
            cboLeaveType.DataTextField = "name"
            cboLeaveType.DataSource = dsFill.Tables("LeaveType")
            cboLeaveType.DataBind()


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'displaymessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            displaymessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            objLeaveType = Nothing
            objEmployee = Nothing
            objMaster = Nothing
            objApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Public Sub BindgridData()
        Dim strSearching As String = ""
        Dim dtLeaveForm As New DataTable
        Dim dsPedingList As DataSet = Nothing
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("ViewLeaveProcessList")) = False Then Exit Sub
            End If
            Dim objpending As New clspendingleave_Tran
            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearching = "AND lvleaveform.employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboLeaveType.SelectedValue) > 0 Then
                strSearching &= "AND lvleaveform.leavetypeunkid =" & CInt(cboLeaveType.SelectedValue) & " "
            End If

            If txtFormNo.Text.Trim.Length > 0 Then
                strSearching &= "AND lvleaveform.FormNo like '%" & txtFormNo.Text.Trim & "%' "
            End If

            If dtpApplyDate.IsNull = False Then
                strSearching &= "AND lvleaveform.applydate='" & eZeeDate.convertDate(dtpApplyDate.GetDate) & "' "
            End If

            If dtpStartDate.IsNull = False Then
                strSearching &= "AND lvleaveform.startdate >='" & eZeeDate.convertDate(dtpStartDate.GetDate) & "' "
            End If

            If dtpEndDate.IsNull = False Then
                strSearching &= "AND lvleaveform.returndate <='" & eZeeDate.convertDate(dtpEndDate.GetDate) & "' "
            End If

            strSearching &= "AND visibleid <> -1 "

            If CInt(cboStatus.SelectedValue) > 0 Then
                If CInt(cboStatus.SelectedValue) = 6 Then
                    strSearching &= "AND iscancelform = 1"
                Else
                    strSearching &= "AND iscancelform = 0 AND lvleaveform.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                End If
            End If
            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If


            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'dsPedingList = objpending.GetList("PendingProcess", _
            '                                  Session("Database_Name").ToString, _
            '                                  CInt(Session("UserId")), _
            '                                  CInt(Session("Fin_year")), _
            '                                  CInt(Session("CompanyUnkId")), _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                  False, True, strSearching, Nothing)

            dsPedingList = objpending.GetList("PendingProcess", _
                                              Session("Database_Name").ToString, _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                            False, True, strSearching, Nothing, chkIncludeClosedFYTransactions.Checked, CInt(cboEmployee.SelectedValue))

            'Pinkal (15-Mar-2019) -- End

            strSearching = ""

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objApprover As New clsleaveapprover_master
                'Pinkal (11-Sep-2020) -- End

                If dsPedingList.Tables("PendingProcess").Rows.Count > 0 Then
                    Dim drRow As DataRow() = dsPedingList.Tables("PendingProcess").Select("mapuserunkid = " & CInt(Session("UserId")), "priority desc")
                    If drRow.Length > 0 Then
                        Dim objmapuser As New clsapprover_Usermapping
                        objmapuser.GetData(Aruti.Data.enUserType.Approver, CInt(drRow(0)("leaveapproverunkid")))
                        objApprover._Approverunkid = objmapuser._Approverunkid
                        mintApproverunkid = objApprover._Approverunkid
                    End If
                End If

                Dim objApproverLevel As New clsapproverlevel_master
                objApproverLevel._Levelunkid = objApprover._Levelunkid
                If chkMyApprovals.Checked AndAlso objApproverLevel._Levelunkid > 0 Then
                    strSearching &= "AND mapuserunkid = " & CInt(Session("UserId"))
                Else
                    chkMyApprovals.Checked = False
                    If mstrEmployeeIDs.Trim.Length > 0 Then
                        'Pinkal (15-Mar-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        If chkIncludeClosedFYTransactions.Checked = False Then
                        strSearching &= "AND employeeunkid  in (" & mstrEmployeeIDs & ")"
                        End If
                        'Pinkal (15-Mar-2019) -- End
                    End If
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objApprover = Nothing
                'Pinkal (11-Sep-2020) -- End


                If objApproverLevel._Levelunkid > 0 Then
                    If strSearching.Length > 0 Then
                        strSearching = strSearching.Substring(3)
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), strSearching, "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), "", "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    End If
                Else
                    If strSearching.Length > 0 Then
                        strSearching = strSearching.Substring(3)
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), strSearching, "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), "", "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                    End If
                End If
            Else
                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), strSearching, "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                Else
                    dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), "", "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            If dtLeaveForm IsNot Nothing Then
                'Session("PendingLeaveTran") = dtLeaveForm
                Dim dtTable As DataTable = dtLeaveForm.Clone
                Dim dtRow As DataRow = Nothing
                Dim strform As String = ""
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))

                For Each dRow As DataRow In dtLeaveForm.Rows
                    If strform.Trim <> dRow("FormNo").ToString.Trim Then
                        dtRow = dtTable.NewRow
                        strform = dRow("FormNo").ToString.Trim
                        dtRow("FormNo") = strform
                        dtRow("IsGrp") = True
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dcol As DataColumn In dtLeaveForm.Columns
                        dtRow(dcol.ColumnName) = dRow(dcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                dtTable.AcceptChanges()
                dgView.DataSource = dtTable

                mdtProcessList = dtTable

                'Pinkal (04-Oct-2017) -- Start
                'Bug - Problem solving For TRA Leave Process List Issue.
                mdsPendingList = dsPedingList
                'Pinkal (04-Oct-2017) -- End


                dgView.DataKeyField = "pendingleavetranunkid"
                dgView.DataBind()
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    dgView.Columns(0).Visible = False
                    dgView.Columns(2).Visible = False
                Else
                    dgView.Columns(0).Visible = CBool(Session("ChangeLeaveFormStatus"))
                    If CBool(Session("IsAutomaticIssueOnFinalApproval")) = True Then
                        dgView.Columns(2).Visible = False
                    Else
                        dgView.Columns(2).Visible = CBool(Session("AllowIssueLeave"))
                    End If
                End If
            Else
                displaymessage.DisplayMessage("Error in BindgridData Method.", Me)
                Exit Sub
            End If
        Catch ex As Exception
            displaymessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsPedingList IsNot Nothing Then dsPedingList.Clear()
            dsPedingList = Nothing
            If dtLeaveForm IsNot Nothing Then dtLeaveForm.Clear()
            dtLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub LoadExpenseControls(ByVal intApproverUnkId As Integer)
        Dim objClaim As New clsclaim_request_master
        Try
            Dim intClaimMasterUnkId As Integer = objClaim.GetClaimRequestMstIDFromLeaveForm(mintLeaveFormUnkId)
            objClaim._Crmasterunkid = intClaimMasterUnkId

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            If CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                If objClaim._Statusunkid <> 1 AndAlso objClaim._Statusunkid <> 2 Then
                    displaymessage.DisplayMessage("There is no Leave expense data for this leave form.", Me)
                    Exit Sub
                End If
            Else
                If objClaim._Crmasterunkid <= 0 Then
                    displaymessage.DisplayMessage("There is no Leave expense data for this leave form.", Me)
                    Exit Sub
                End If
            End If

            Call FillExpenseCombo(objClaim)

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            'Call GetValue(objClaim, intApproverUnkId)
            If GetValue(objClaim, intApproverUnkId) = False Then
                mblnClaimRequest = False
            Else
                mblnClaimRequest = True
            End If
            'Pinkal (10-Jan-2017) -- End

            Call Fill_Expense()
            If mblnClaimRequest Then popupClaimRequest.Show()
        Catch ex As Exception
            displaymessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaim = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#Region "Expense"

    Private Sub FillExpenseCombo(ByVal objClaimMst As clsclaim_request_master)
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Dim objEMaster As New clsEmployee_Master
        Dim objLeaveType As New clsleavetype_master
        Dim objleave As New clsleaveform

        Try
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List")
            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With


            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If



            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString, _
            '                                        CInt(Session("UserId")), _
            '                                        CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        Session("UserAccessModeSetting").ToString, True, _
            '                                        CBool(Session("IsIncludeInactiveEmp")), "List", False, _
            '                                        objClaimMst._Employeeunkid, , , , , , , , , , , , , , , , blnApplyFilter)

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString, _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    Session("UserAccessModeSetting").ToString, True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "List", False, _
                                                  objClaimMst._Employeeunkid, , , , , , , , , , , , , , , , False)

            'Pinkal (01-Mar-2016) -- End


            With cboClaimEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            Dim mdtLeave As DataTable = Nothing
            If CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)
                    Case enExpenseType.EXP_LEAVE
                        objlblValue.Text = Language.getMessage(mstrModuleNameClaim, 12, "Leave Form")
                        dsCombo = objLeaveType.getListForCombo("List", True, 1)
                        If objClaimMst._LeaveTypeId > 0 Then
                            mdtLeave = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & objClaimMst._LeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeave = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboClaimLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = mdtLeave
                            .DataBind()
                            .SelectedValue = objClaimMst._LeaveTypeId.ToString
                        End With

                        'Pinkal (20-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())
                        'Pinkal (20-Feb-2019) -- End

                        objLeaveType = Nothing
                End Select
            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing
                cboReference.DataBind() : txtDomicileAddress.Text = ""
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtLeave IsNot Nothing Then mdtLeave.Clear()
            mdtLeave = Nothing
            'Pinkal (11-Sep-2020) -- End


            Dim objExpMaster As New clsExpense_Master
            dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboClaimEmployee.SelectedValue) = "", 0, cboClaimEmployee.SelectedValue)), True)
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (11-Sep-2020) -- End


            If CInt(IIf(cboClaimLeaveType.SelectedValue = "", 0, cboClaimLeaveType.SelectedValue)) = 0 Then
                dsCombo = objleave.GetLeaveFormForExpense(0, "2,7", CInt(cboClaimEmployee.SelectedValue), True, True, mintLeaveFormUnkId)
            Else
                dsCombo = objleave.GetLeaveFormForExpense(CInt(cboClaimLeaveType.SelectedValue), "2,7", CInt(cboClaimEmployee.SelectedValue), True, True, mintLeaveFormUnkId)
            End If

            If mintLeaveFormUnkId > 0 Then
                mdtLeave = New DataView(dsCombo.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
            End If

            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = mdtLeave.Copy
                .DataBind()
                If mintLeaveFormUnkId < 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = mintLeaveFormUnkId.ToString
                End If
            End With



            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If CBool(Session("SectorRouteAssignToEmp")) = True AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(CInt(cboClaimEmployee.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtTable
                    .DataBind()
                End With
                objAssignEmp = Nothing

            ElseIf CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objcommonMst As New clsCommon_Master
                dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
                With cboSectorRoute
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
                objcommonMst = Nothing
            End If

            'Pinkal (20-Feb-2019) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtClaimTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtClaimTable = dsCombo.Tables(0)
                Else
                    dtClaimTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtClaimTable
                .DataBind()
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtClaimTable IsNot Nothing Then dtClaimTable.Clear()
            dtClaimTable = Nothing
            objCostCenter = Nothing
            'Pinkal (11-Sep-2020) -- End


            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With CboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                End If
                drRow = Nothing
            End With
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'displaymessage.DisplayMessage("FillExpenseCombo" & ex.Message, Me)
            displaymessage.DisplayError("FillExpenseCombo" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            objEMaster = Nothing
            objLeaveType = Nothing
            objleave = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    'Private Sub GetValue(ByVal objClaimMst As clsclaim_request_master, ByVal xApproverUnkId As Integer)
    Private Function GetValue(ByVal objClaimMst As clsclaim_request_master, ByVal xApproverUnkId As Integer) As Boolean
        'Pinkal (10-Jan-2017) -- End
        Try
            txtClaimNo.Text = objClaimMst._Claimrequestno
            txtClaimNo.Attributes.Add("claimunkid", objClaimMst._Crmasterunkid.ToString)
            txtClaimRemark.Text = objClaimMst._Claim_Remark
            If objClaimMst._Transactiondate <> Nothing Then
                dtpDate.SetDate = objClaimMst._Transactiondate.Date
            Else
                dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            If CInt(cboClaimEmployee.SelectedValue) > 0 Then
                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboClaimEmployee.SelectedValue)

                Dim objState As New clsstate_master
                objState._Stateunkid = objEmployee._Domicile_Stateunkid
                Dim mstrCountry As String = ""
                Dim objCountry As New clsMasterData
                Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                If dsCountry.Tables("List").Rows.Count > 0 Then
                    mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                End If
                dsCountry.Clear()
                dsCountry = Nothing
                Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                           IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                           IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                           IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                           mstrCountry

                txtDomicileAddress.Text = strAddress
                objState = Nothing
                objCountry = Nothing
                objEmployee = Nothing
            Else
                txtDomicileAddress.Text = ""
            End If
            txtUoMType.Text = "" : txtClaimBalance.Text = ""
            txtUnitPrice.Enabled = True : txtUnitPrice.Text = "0.00"

            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then


                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                Dim mstrFilter As String = ""
                Dim mblnIsNoClaimCheck As Boolean = False

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim objleaveapprover As New clsleaveapprover_master
                'Dim objApproveLevel As New clsapproverlevel_master
                'Pinkal (11-Sep-2020) -- End


                Dim objExpenseApproval As New clsclaim_request_approval_tran

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                ' Dim dsApprovalList As DataSet = Nothing
                'objleaveapprover._Approverunkid = mintApproverunkid
                'objApproveLevel._Levelunkid = objleaveapprover._Levelunkid
                'Pinkal (11-Sep-2020) -- End



NoClaimExpenseFromApprover:

                'mdtExpense = objExpenseApproval.GetApproverExpesneList("Approval", False, _
                '                                                       CBool(Session("PaymentApprovalwithLeaveApproval")), _
                '                                                       Session("Database_Name").ToString(), _
                '                                                       CInt(Session("UserId")), _
                '                                                       Session("EmployeeAsOnDate").ToString(), _
                '                                                       enExpenseType.EXP_LEAVE, False, True, _
                '                                                       CInt(IIf(CBool(Session("PaymentApprovalwithLeaveApproval")), xApproverUnkId, -1)), "", _
                '                                                       objClaimMst._Crmasterunkid).Tables(0)


                mdtExpense = objExpenseApproval.GetApproverExpesneList("Approval", False, _
                                                                       CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                       Session("Database_Name").ToString(), _
                                                                       CInt(Session("UserId")), _
                                                                       Session("EmployeeAsOnDate").ToString(), _
                                                                       enExpenseType.EXP_LEAVE, False, True, _
                                                             CInt(IIf(CBool(Session("PaymentApprovalwithLeaveApproval")), xApproverUnkId, -1)), mstrFilter, _
                                                                       objClaimMst._Crmasterunkid).Tables(0)


                If mdtExpense Is Nothing OrElse mdtExpense.Rows.Count <= 0 Then
                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                    'Exit sub
                    If mblnIsNoClaimCheck = False Then
                        xApproverUnkId = -1
                        mstrFilter = "cmclaim_approval_tran.statusunkid = 1  AND (CONVERT(CHAR(8), cmclaim_request_master.transactiondate, 112) IS NOT NULL OR CONVERT(CHAR(8), cmclaim_request_master.transactiondate, 112) <> '') "
                        mblnIsNoClaimCheck = True
                        GoTo NoClaimExpenseFromApprover
                    End If
                    mdtExpense = Nothing
                    displaymessage.DisplayMessage("There is no Leave expense data for this leave form.", Me)
                    Return False
                    'Pinkal (10-Jan-2017) -- End
                End If

                If mblnIsNoClaimCheck = True Then
                    mdtExpense = New DataView(mdtExpense, "crpriority = MAX(crpriority)", "", DataViewRowState.CurrentRows).ToTable
                End If


                'Pinkal (10-Jan-2017) -- End

            Else
                Dim objClaimApprover As New clsclaim_request_approval_tran
                Dim mdtData As DataTable = objClaimApprover.GetEmployeeLastApprovedExpenseDetail(objClaimMst._Crmasterunkid) 'ClaimRequestMasterId
                If mdtData Is Nothing OrElse mdtData.Rows.Count <= 0 Then
                    mdtData = Nothing
                    displaymessage.DisplayMessage("There is no Leave expense data for this leave form.", Me)
                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                    'Exit sub
                    Return False
                    'Pinkal (10-Jan-2017) -- End
                End If
                Dim objClaimTran As New clsclaim_request_tran
                objClaimTran._ClaimRequestMasterId = objClaimMst._Crmasterunkid
                mdtExpense = objClaimTran._DataTable
                objClaimTran = Nothing
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'displaymessage.DisplayMessage("GetValue:-" & ex.Message, Me)
            displaymessage.DisplayError("GetValue:-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        Finally
        End Try
        Return True
    End Function

    Private Sub Fill_Expense()
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtExpense
            Dim mdtTran As DataTable = Nothing
            If mdtExpense IsNot Nothing Then
                mdtTran = mdtExpense.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            Dim mdView As New DataView
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            If mdView.ToTable.Rows.Count > dgvData.PageSize Then
                dgvData.AllowPaging = True
            Else
                dgvData.AllowPaging = False
            End If
            dgvData.DataSource = mdView
            dgvData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D'")), Session("fmtCurrency").ToString)
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'displaymessage.DisplayMessage("Fill_Expense:-" & ex.Message, Me)
            displaymessage.DisplayError("Fill_Expense:-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#End Region

#Region "Button Event(S)"

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If chkIncludeClosedFYTransactions.Checked Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Employee is compulsory information.Please Select Employee."), Me)
                    cboEmployee.Focus()
                    Exit Sub
                ElseIf CInt(cboLeaveType.SelectedValue) <= 0 Then
                    displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                    cboLeaveType.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (15-Mar-2019) -- End
            Call BindgridData()
        Catch ex As Exception
            displaymessage.DisplayError("BtnSearch_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnclose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            displaymessage.DisplayError("Btnclose_Click:- " & ex.Message, Me)
        End Try
    End Sub

#Region "Expense"
    Protected Sub btnClaimClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClaimClose.Click
        Try
            mblnClaimRequest = False
            popupClaimRequest.Hide()
        Catch ex As Exception
            displaymessage.DisplayError("btnClaimClose_Click:- " & ex.Message, Me)
        End Try
    End Sub

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Protected Sub btnViewScanAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewScanAttchment.Click
        Dim mstrPreviewIds As String = ""
        Dim dtTable As New DataTable
        Try
            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboClaimEmployee.SelectedValue), _
                                                                             enScanAttactRefId.CLAIM_REQUEST, _
                                                                             enImg_Email_RefId.Claim_Request, _
                                                                             CInt(txtClaimNo.Attributes("claimunkid")))

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboClaimEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            'S.SANDEEP |25-JAN-2019| -- END
            popup_ShowAttchment._Webpage = Me
            popup_ShowAttchment.Show()
        Catch ex As Exception
            displaymessage.DisplayError(ex.Message, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popup_ShowAttchment_btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ShowAttchment.btnPreviewClose_Click
        Try
            popup_ShowAttchment.Hide()
        Catch ex As Exception
            displaymessage.DisplayError("FillEmployeeCombo:- " & ex.Message, Me)
        End Try
    End Sub
    'Shani (20-Aug-2016) -- End

#End Region

#End Region

#Region "Gridview Event(S)"

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try

            If e.Item.ItemType = ListItemType.Header Then
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    e.Item.Cells(3).Text = e.Item.Cells(5).Text
                    e.Item.Cells(3).ColumnSpan = 3
                    e.Item.Cells(5).Visible = False
                Else
                    e.Item.Cells(3).Text = e.Item.Cells(4).Text
                    e.Item.Cells(3).ColumnSpan = 2
                    e.Item.Cells(4).Visible = False
                End If
            End If

          

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.SelectedItem Then
                If CBool(e.Item.Cells(24).Text) = True Then
                    CType(e.Item.Cells(0).FindControl("Imgchange"), LinkButton).Visible = False
                    CType(e.Item.Cells(1).FindControl("lnkLeaveExpense"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkIssueLeave"), LinkButton).Visible = False

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        e.Item.Cells(1).Text = e.Item.Cells(3).Text
                        e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1
                        e.Item.Cells(1).Style.Add("text-align", "left")
                        e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Item.Cells(0).Visible = False
                    Else
                        If CBool(Session("ChangeLeaveFormStatus")) = True Then
                            e.Item.Cells(0).Visible = True
                            e.Item.Cells(0).Text = e.Item.Cells(3).Text
                            e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(0).Style.Add("text-align", "left")
                            e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(1).Visible = False
                        Else
                            e.Item.Cells(1).Text = e.Item.Cells(3).Text
                            e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(1).Style.Add("text-align", "left")
                            e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(0).Visible = False
                        End If
                    End If
                    For i = 2 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        e.Item.Cells(3).Attributes.Add("FormNo", e.Item.Cells(3).Text.Trim)
                        e.Item.Cells(3).Text = e.Item.Cells(5).Text.Trim
                        e.Item.Cells(3).ColumnSpan = 3
                        'e.Item.Cells(4).Visible = False
                        e.Item.Cells(5).Visible = False
                    Else
                        e.Item.Cells(3).Attributes.Add("FormNo", e.Item.Cells(3).Text.Trim)
                        e.Item.Cells(3).Text = e.Item.Cells(4).Text.Trim
                        e.Item.Cells(3).ColumnSpan = 2
                        e.Item.Cells(4).Visible = False
                    End If

                    If e.Item.Cells(1).HasControls Then
                        Dim lnkLeaveExpense As LinkButton = DirectCast(e.Item.Cells(1).FindControl("lnkLeaveExpense"), LinkButton)
                        lnkLeaveExpense.Text = Language._Object.getCaption(mstrModuleName4, dgView.Columns(1).HeaderText).Replace("&", "")
                        lnkLeaveExpense.ToolTip = Language._Object.getCaption(mstrModuleName4, dgView.Columns(1).HeaderText).Replace("&", "")
                    End If

                    If e.Item.Cells(2).HasControls Then
                        Dim lnkIssueLeave As LinkButton = DirectCast(e.Item.Cells(2).FindControl("lnkIssueLeave"), LinkButton)
                        lnkIssueLeave.Text = Language._Object.getCaption("btnIssueLeave", dgView.Columns(2).HeaderText).Replace("&", "")
                        lnkIssueLeave.ToolTip = Language._Object.getCaption("btnIssueLeave", dgView.Columns(2).HeaderText).Replace("&", "")
                    End If



                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    SetDateFormat()
                    'Pinkal (16-Apr-2016) -- End


                    If (e.Item.ItemIndex >= 0) Then


                        'Pinkal (16-Apr-2016) -- Start
                        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                        'If Session("DateFormat").ToString <> Nothing Then
                        '    e.Item.Cells(9).Text = eZeeDate.convertDate(e.Item.Cells(9).Text).Date.ToString(Session("DateFormat").ToString)
                        '    e.Item.Cells(10).Text = eZeeDate.convertDate(e.Item.Cells(10).Text).Date.ToString(Session("DateFormat").ToString)
                        'Else
                        '    e.Item.Cells(9).Text = eZeeDate.convertDate(e.Item.Cells(9).Text).Date.ToShortDateString
                        '    e.Item.Cells(10).Text = eZeeDate.convertDate(e.Item.Cells(10).Text).Date.ToShortDateString
                        'End If
                            e.Item.Cells(9).Text = eZeeDate.convertDate(e.Item.Cells(9).Text).Date.ToShortDateString
                            e.Item.Cells(10).Text = eZeeDate.convertDate(e.Item.Cells(10).Text).Date.ToShortDateString
                        'Pinkal (16-Apr-2016) -- End


						'Pinkal (04-Oct-2017) -- Start
                        'Bug - Problem solving For TRA Leave Process List Issue.
                        'If dsPedingList Is Nothing Then Exit Sub
                        If mdtProcessList Is Nothing orelse mdsPendingList is nothing Then Exit Sub
						'Pinkal (04-Oct-2017) -- End

                        Dim mstrStaus As String = String.Empty

                        'Pinkal (04-Oct-2017) -- Start
                        'Bug - Problem solving For TRA Leave Process List Issue.
                        'Dim dList As DataTable = New DataView(mdtProcessList, "employeeunkid = " & CInt(e.Item.Cells(14).Text) & " AND formunkid = " & CInt(e.Item.Cells(13).Text), "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                        Dim dList As DataTable = New DataView(mdsPendingList.Tables(0), "employeeunkid = " & CInt(e.Item.Cells(14).Text) & " AND formunkid = " & CInt(e.Item.Cells(13).Text), "formunkid desc,priority asc", DataViewRowState.CurrentRows).ToTable
                        'Pinkal (04-Oct-2017) -- End



                        If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then

                            Dim dr As DataRow() = dList.Select("priority >= " & CInt(e.Item.Cells(19).Text))

                            If dr.Length > 0 Then

                                'Pinkal (16-May-2019) -- Start
                                'Enhancement - Working on Leave UAT Changes for NMB.
                                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                                'Pinkal (16-May-2019) -- End

                                For i As Integer = 0 To dr.Length - 1

                                    If CInt(e.Item.Cells(16).Text) = 1 Then
                                        Language.setLanguage(mstrModuleName)
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & e.Item.Cells(6).Text
                                        mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & " " & info1.ToTitleCase(e.Item.Cells(6).Text.ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For

                                    ElseIf CInt(e.Item.Cells(16).Text) = 2 Then

                                        If CInt(dr(i)("statusunkid")) = 1 Then
                                            Language.setLanguage(mstrModuleName)
                                            'Pinkal (16-May-2019) -- Start
                                            'Enhancement - Working on Leave UAT Changes for NMB.
                                            'mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & dr(i)("approvername").ToString()
                                            mstrStaus = Language.getMessage(mstrModuleName, 11, "Approved By :-  ") & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                                            'Pinkal (16-May-2019) -- End
                                            Exit For

                                        ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                                            Language.setLanguage(mstrModuleName)
                                            'Pinkal (16-May-2019) -- Start
                                            'Enhancement - Working on Leave UAT Changes for NMB.
                                            'mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & dr(i)("approvername").ToString()
                                            mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & " " & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                                            'Pinkal (16-May-2019) -- End
                                            Exit For

                                        ElseIf CInt(dr(i)("statusunkid")) = 4 Then
                                            Language.setLanguage(mstrModuleName)
                                            'Pinkal (16-May-2019) -- Start
                                            'Enhancement - Working on Leave UAT Changes for NMB.
                                            'mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & dr(i)("approvername").ToString()
                                            mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & " " & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                                            'Pinkal (16-May-2019) -- End
                                            Exit For

                                        ElseIf CInt(dr(i)("statusunkid")) = 7 Then
                                            Language.setLanguage(mstrModuleName)
                                            'Pinkal (16-May-2019) -- Start
                                            'Enhancement - Working on Leave UAT Changes for NMB.
                                            'mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & dr(i)("Issueusername").ToString()
                                            mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & " " & info1.ToTitleCase(dr(i)("Issueusername").ToString().ToLower())
                                            'Pinkal (16-May-2019) -- End
                                            Exit For
                                        End If

                                    ElseIf CInt(e.Item.Cells(16).Text) = 3 Then
                                        Language.setLanguage(mstrModuleName)
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & e.Item.Cells(6).Text
                                        mstrStaus = Language.getMessage(mstrModuleName, 12, "Rejected By :-  ") & " " & info1.ToTitleCase(e.Item.Cells(6).Text.ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For

                                    ElseIf CInt(e.Item.Cells(16).Text) = 4 Then
                                        Language.setLanguage(mstrModuleName)
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & e.Item.Cells(6).Text
                                        mstrStaus = Language.getMessage(mstrModuleName, 13, "Re-Scheduled By :- ") & " " & info1.ToTitleCase(e.Item.Cells(6).Text.ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For

                                    ElseIf CInt(e.Item.Cells(16).Text) = 7 Then
                                        Language.setLanguage(mstrModuleName)
                                        'Pinkal (16-May-2019) -- Start
                                        'Enhancement - Working on Leave UAT Changes for NMB.
                                        'mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & dr(i)("Issueusername").ToString()
                                        mstrStaus = Language.getMessage(mstrModuleName, 14, "Issued By :-  ") & " " & info1.ToTitleCase(dr(i)("Issueusername").ToString().ToLower())
                                        'Pinkal (16-May-2019) -- End
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        If mstrStaus <> "" Then
                            e.Item.Cells(12).Text = mstrStaus
                        End If
                        If e.Item.Cells(11).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(11).Text.Trim <> "" Then
                            e.Item.Cells(11).Text = Math.Round(CDec(e.Item.Cells(11).Text), 2).ToString
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            displaymessage.DisplayError("dgView_ItemDataBound :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Dim dtList As New DataTable
        Try


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            mblnIsExternalApprover = CBool(e.Item.Cells(25).Text)
            'Pinkal (01-Mar-2016) -- End


            If e.CommandName = "Select" Then
                If CInt(Session("UserId")) <> CInt(e.Item.Cells(17).Text) Then
                    Language.setLanguage(mstrModuleName)
                    displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "You can't Edit this Leave detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(21).Text) = True Then
                    Language.setLanguage(mstrModuleName)
                    displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "You can't Edit this Leave detail. Reason: This Leave is already Cancelled."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(16).Text) = 7 Then 'FOR ISSUED
                    Language.setLanguage(mstrModuleName)
                    displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already issued."), Me)
                    Exit Sub
                End If


                'START FOR CHECK WHETHER USER HAS MULTIPLE APPROVER OR NOT
                Dim dsList As DataSet
                Dim objMapping As New clsapprover_Usermapping


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objApprover As New clsleaveapprover_master
                'Pinkal (11-Sep-2020) -- End


                dsList = objMapping.GetList("Mapping", enUserType.Approver)

                If dsList.Tables("Mapping").Rows.Count > 0 Then
                    Dim dtMapList As DataTable = New DataView(dsList.Tables("Mapping"), "userunkid= " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable
                    If dtMapList.Rows.Count > 0 Then
                        objApprover._Approverunkid = CInt(e.Item.Cells(18).Text)   ' -- CHECK FOR MAP APPROVER UNKID
                    End If
                End If
                'END FOR CHECK WHETHER USER HAS MULTIPLE APPROVER OR NOT


                'Pinkal (04-Oct-2017) -- Start
                'Bug - Problem solving For TRA Leave Process List Issue.
                dtList = New DataView(mdsPendingList.Tables(0), "employeeunkid = " & CInt(e.Item.Cells(14).Text) & " AND formunkid = " & CInt(e.Item.Cells(13).Text) & " AND leaveapproverunkid <> " & CInt(e.Item.Cells(23).Text), "", DataViewRowState.CurrentRows).ToTable
                'dtList = New DataView(mdtProcessList, "employeeunkid = " & CInt(e.Item.Cells(14).Text) & " AND formunkid = " & CInt(e.Item.Cells(13).Text) & " AND leaveapproverunkid <> " & CInt(e.Item.Cells(23).Text), "", DataViewRowState.CurrentRows).ToTable
                'Pinkal (04-Oct-2017) -- End
                

                If dtList.Rows.Count > 0 Then
                    Dim objapproverlevel As New clsapproverlevel_master
                    objapproverlevel._Levelunkid = objApprover._Levelunkid

                    For i As Integer = 0 To dtList.Rows.Count - 1
                        If objapproverlevel._Priority > CInt(dtList.Rows(i)("Priority")) Then
                            Dim dList As DataTable = New DataView(dtList, "levelunkid = " & CInt(dtList.Rows(i)("levelunkid")) & " AND (statusunkid = 1 or statusunkid = 7 )  ", "", DataViewRowState.CurrentRows).ToTable
                            If dList.Rows.Count > 0 Then Continue For

                            If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit this Leave detail. Reason: This Leave approval is still pending."), Me)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), Me)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 4 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), Me)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 7 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already issued."), Me)
                                Exit Sub
                            End If
                        ElseIf objapproverlevel._Priority <= CInt(dtList.Rows(i)("Priority")) Then
                            If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Leave detail. Reason: This Leave approval is already approved."), Me)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), Me)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 4 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), Me)
                                Exit Sub

                            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 7 Then
                                Language.setLanguage(mstrModuleName)
                                displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already issued."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                End If


                'Pinkal (04-Oct-2017) -- Start
                'Bug - Problem solving For TRA Leave Process List Issue.
                Dim drPendingLeaveTran() As DataRow = mdsPendingList.Tables(0).Select("pendingleavetranunkid = " & CInt(dgView.DataKeys(e.Item.ItemIndex)))
                'Dim drPendingLeaveTran() As DataRow = mdtProcessList.Select("pendingleavetranunkid = " & CInt(dgView.DataKeys(e.Item.ItemIndex)))
                'Pinkal (04-Oct-2017) -- End

                

                Dim mdtStartdate As DateTime = Nothing
                If drPendingLeaveTran.Length <= 0 Then
                    mdtStartdate = CDate(e.Item.Cells(9).Text)
                Else
                    mdtStartdate = eZeeDate.convertDate(drPendingLeaveTran(0)("startdate").ToString()).Date
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                drPendingLeaveTran = Nothing
                'Pinkal (11-Sep-2020) -- End


                If mdtStartdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date _
                      And objApprover._leaveapproverunkid = CInt(e.Item.Cells(15).Text) Then
                    If CInt(e.Item.Cells(16).Text) = 1 Then   'FOR APPROVED
                        Language.setLanguage(mstrModuleName)
                        displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "You can't Edit this Leave detail. Reason: This Leave is already approved."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(16).Text) = 7 Then 'FOR ISSUED
                        Language.setLanguage(mstrModuleName)
                        displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already issued."), Me)
                        Exit Sub

                    ElseIf CInt(e.Item.Cells(16).Text) = 3 Then   'FOR REJECTED
                        Language.setLanguage(mstrModuleName)
                        displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), Me)
                        Exit Sub

                    ElseIf CInt(e.Item.Cells(16).Text) = 4 Then 'FOR RE-SCHEDULED
                        Language.setLanguage(mstrModuleName)
                        displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), Me)
                        Exit Sub
                    End If
                ElseIf mdtStartdate.Date > ConfigParameter._Object._CurrentDateAndTime.Date _
                                         And objApprover._leaveapproverunkid = CInt(e.Item.Cells(15).Text) Then
                    If CInt(e.Item.Cells(16).Text) = 3 Then   'FOR REJECTED
                        Language.setLanguage(mstrModuleName)
                        displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected."), Me)
                        Exit Sub

                    ElseIf CInt(e.Item.Cells(16).Text) = 4 Then 'FOR RE-SCHEDULED
                        Language.setLanguage(mstrModuleName)
                        displaymessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled."), Me)
                        Exit Sub
                    End If
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objApprover = Nothing
                'Pinkal (11-Sep-2020) -- End


                Session.Add("pendingleavetranunkid", CInt(dgView.DataKeys(e.Item.ItemIndex)))
                Session.Add("formunkid", CInt(e.Item.Cells(13).Text))
                Session.Add("employeeunkid", CInt(e.Item.Cells(14).Text))
                Session("approverEmpUnkid") = CInt(e.Item.Cells(15).Text)
                Session.Add("Approvertranunkid", CInt(e.Item.Cells(18).Text))
                Session.Add("priority", CInt(e.Item.Cells(19).Text))

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                Session.Add("IsExternalApprover", CBool(e.Item.Cells(25).Text))
                'Pinkal (01-Mar-2016) -- End

                Response.Redirect(Session("rootpath").ToString & "Leave/wPg_ProcessLeaveAddEdit.aspx", False)
            ElseIf e.CommandName = "Expense" Then
                mintLeaveFormUnkId = CInt(e.Item.Cells(13).Text)
                LoadExpenseControls(CInt(e.Item.Cells(23).Text))

            ElseIf e.CommandName = "Issue" Then

                If CBool(e.Item.Cells(21).Text) = True Then
                    displaymessage.DisplayMessage("You can't Issue this Leave. Reason: This Leave is already Cancelled.", Me)
                    Exit Sub
                End If

                'START FOR CHECK WHETHER USER HAS MULTIPLE APPROVER OR NOT
                Dim dsList As New DataSet
                Dim objMapping As New clsapprover_Usermapping
                Dim objApprover As New clsleaveapprover_master
                Dim objpending As New clspendingleave_Tran

                dsList = objMapping.GetList("Mapping", enUserType.Approver)
                If dsList.Tables("Mapping").Rows.Count > 0 Then
                    Dim dList As DataTable = New DataView(dsList.Tables("Mapping"), "userunkid = '" & Session("UserId").ToString & "'", "", DataViewRowState.CurrentRows).ToTable
                    If dList.Rows.Count > 0 Then
                        objApprover._Approverunkid = CInt(e.Item.Cells(18).Text)
                    End If
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                'Pinkal (11-Sep-2020) -- End

                'END FOR CHECK WHETHER USER HAS MULTIPLE APPROVER OR NOT

                'START FOR CHECK WHETHER FORM IS APPROVED BY HIGH AUTHORITY OR NOT
                Dim objapproverlevel As New clsapproverlevel_master
                objapproverlevel._Levelunkid = objApprover._Levelunkid
                Dim dsEmpApprover As DataSet = objpending.GetEmployeeApproverListWithPriority(CInt(e.Item.Cells(14).Text), -1)
                Dim dsPendingApprover As DataSet = objpending.GetEmployeeApproverListWithPriority(CInt(e.Item.Cells(14).Text), CInt(e.Item.Cells(13).Text))

                If dsEmpApprover.Tables(0).Rows.Count <> dsPendingApprover.Tables(0).Rows.Count Then
                    'START ISSUE BUTTON IS ONLY ENABLE WHEN LEAVE FORM IS APPROVE

                    If CBool(Session("AllowIssueLeave")) = True Then
10:
                        If objpending.GetApproverMaxPriorityStatusId(CInt(e.Item.Cells(13).Text), CInt(e.Item.Cells(14).Text), _
                            CInt(IIf(IsDBNull(dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")), -1, dsPendingApprover.Tables("List").Compute("Max(priority)", "1=1")))) = 1 _
                                           AndAlso CInt(e.Item.Cells(22).Text) <= 0 Then

                            'Pinkal (04-Oct-2017) -- Start
                            'Bug - Problem solving For TRA Leave Process List Issue.
                            Dim dtTable As DataTable = mdsPendingList.Tables(0)
                            'Dim dtTable As DataTable = mdtProcessList
                            'Pinkal (04-Oct-2017) -- End
                            

                            Dim drPending() As DataRow = dtTable.Select("pendingleavetranunkid = " & CInt(dgView.DataKeys(e.Item.ItemIndex)))
                            If drPending.Length <= 0 Then Exit Sub
                            Session("L_StartDate") = drPending(0)("startdate").ToString()
                            Session("L_EndDate") = drPending(0)("enddate").ToString()
                            Session("approverunkid") = e.Item.Cells(15).Text.ToString
                            Session("leaveapproverunkid") = e.Item.Cells(23).Text.ToString
                            Dim drRow() As DataRow = dsPendingApprover.Tables(0).Select("priority = MAX(priority) AND statusunkid = 1")
                            If drRow.Length > 0 Then
                                objApprover._Approverunkid = CInt(drRow(0)("approverunkid"))
                                Session("EmpMaxapproverunkid") = objApprover._leaveapproverunkid
                                'Pinkal (03-May-2019) -- Start
                                'Enhancement - Working on Leave UAT Changes for NMB.
                                Session.Add("pendingleavetranunkid", CInt(drRow(0)("pendingleavetranunkid")))
                                'Pinkal (03-May-2019) -- End
                            End If
                            Session.Add("IsExternalApprover", CBool(e.Item.Cells(25).Text))
                            Response.Redirect("LeaveIssue.aspx?ProcessId=" & b64encode(Val(e.Item.Cells(13).Text).ToString), False)

                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dtTable IsNot Nothing Then dtTable.Clear()
                            dtTable = Nothing
                            'Pinkal (11-Sep-2020) -- End

                        Else
                            If CInt(e.Item.Cells(13).Text) > 0 Then
                                Dim objLStatus As New clsleaveform
                                objLStatus._Formunkid = CInt(e.Item.Cells(13).Text)
                                If objLStatus._Statusunkid = 7 Then
                                    displaymessage.DisplayMessage("Sorry, You cannot issue leave. Reason it is already issued.", Me)
                                Else
                                    displaymessage.DisplayMessage("Sorry, You are not authorized to issue this leave.", Me)
                                End If
                                'Pinkal (11-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                objLStatus = Nothing
                                'Pinkal (11-Sep-2020) -- End
                            End If
                            Exit Sub
                        End If
                    End If
                Else
                    If CBool(Session("AllowIssueLeave")) = True Then
                        GoTo 10
                    Else
                        displaymessage.DisplayMessage("Sorry, You are not authorized to issue this leave.", Me)
                        Exit Sub
                    End If
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsPendingApprover IsNot Nothing Then dsPendingApprover.Clear()
                dsPendingApprover = Nothing
                If dsEmpApprover IsNot Nothing Then dsEmpApprover.Clear()
                dsEmpApprover = Nothing
                objpending = Nothing
                objMapping = Nothing
                objapproverlevel = Nothing
                objApprover = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            displaymessage.DisplayError("dgView_ItemCommand :- " & ex.Message, Me)
        End Try
    End Sub

#Region "Expense"

    'Shani (20-Aug-2016) -- Start
    'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                popup_ShowAttchment.ClearPreviewDataSource()
                'Pinkal (10-Jan-2017) -- End

                If e.CommandName.ToUpper = "PREVIEW" Then
                    Dim mstrPreviewIds As String = ""
                    Dim dtTable As New DataTable



                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                    'dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboClaimEmployee.SelectedValue), _
                    '                                                                enScanAttactRefId.CLAIM_REQUEST, _
                    '                                                                enImg_Email_RefId.Claim_Request, _
                    '                                                                CInt(e.Item.Cells(8).Text))

                    If CInt(e.Item.Cells(8).Text) > 0 Then
                    dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboClaimEmployee.SelectedValue), _
                                                                                         enScanAttactRefId.CLAIM_REQUEST, _
                                                                                         enImg_Email_RefId.Claim_Request, _
                                                                                         CInt(e.Item.Cells(8).Text))
                    End If

                    'Pinkal (10-Jan-2017) -- End



                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
                    End If

                    popup_ShowAttchment.ScanTranIds = mstrPreviewIds
                    'S.SANDEEP |25-JAN-2019| -- START
                    'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                    popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboClaimEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                    'S.SANDEEP |25-JAN-2019| -- END
                    popup_ShowAttchment._Webpage = Me
                    popup_ShowAttchment.Show()

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If
            End If
        Catch ex As Exception
            displaymessage.DisplayError("FillEmployeeCombo:- " & ex.Message, Me)
        End Try
    End Sub
    'Shani (20-Aug-2016) -- End

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(5).Text.Trim.Length > 0 Then  'Unit Price
                    e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency").ToString())
                End If

                If e.Item.Cells(6).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString())
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'displaymessage.DisplayMessage("dgvData_ItemDataBound:-" & ex.Message, Me)
            displaymessage.DisplayError("dgvData_ItemDataBound:-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#End Region

#Region "Control Event(S)"

    Protected Sub lnkClaimDepedents_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClaimDepedents.Click
        Try
            If CInt(cboClaimEmployee.SelectedValue) <= 0 Then
                displaymessage.DisplayMessage(Language.getMessage(mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboClaimEmployee.Focus()
                Exit Sub
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboClaimEmployee.SelectedValue), False, True, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboClaimEmployee.SelectedValue), False, True, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsList = objDependant.GetQualifiedDepedant(CInt(cboClaimEmployee.SelectedValue), True, False, dtpDate.GetDate.Date)
                dsList = objDependant.GetQualifiedDepedant(CInt(cboClaimEmployee.SelectedValue), True, False, dtpDate.GetDate.Date, dtAsOnDate:=dtpDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
            Else
                Exit Sub
            End If
            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()
            popupEmpDepedents.Show()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDependant = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'displaymessage.DisplayMessage("lnkClaimDepedents_Click :" & ex.Message, Me)
            displaymessage.DisplayError("lnkClaimDepedents_Click :" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    'Pinkal (20-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

#Region "ComboBox Event"

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try
            If CBool(Session("SectorRouteAssignToExpense")) Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                    .SelectedIndex = 0
                End With

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing
                'Pinkal (11-Sep-2020) -- End

                objAssignExpense = Nothing
            End If
        Catch ex As Exception
            displaymessage.DisplayError("cboExpense_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If cboExpense.SelectedValue <> "" Then
            cboExpense_SelectedIndexChanged(sender, e)
            End If
            'Pinkal (26-Feb-2019) -- End
        Catch ex As Exception
            displaymessage.DisplayError("cboExpCategory_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region

    'Pinkal (20-Feb-2019) -- End

    Private Sub SetLanguage()
        Try

            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (15-Mar-2019) -- End

            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, "Leave Process List")
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, "Leave Process List")
            Me.lblDetialHeader.Text = Language._Object.getCaption(mstrModuleName, "Leave Process List")

            Me.lblLeaveType.Text = Language._Object.getCaption(Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.ID, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblApplyDate.Text = Language._Object.getCaption(Me.lblApplyDate.ID, Me.lblApplyDate.Text)
            Me.lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.ID, Me.lblFormNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)


            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Me.chkIncludeClosedFYTransactions.Text = Language._Object.getCaption(Me.chkIncludeClosedFYTransactions.ID, Me.chkIncludeClosedFYTransactions.Text)
            'Pinkal (15-Mar-2019) -- End


            Me.Btnclose.Text = Language._Object.getCaption(Me.Btnclose.ID, Me.Btnclose.Text).Replace("&", "")

            dgView.Columns(1).HeaderText = Language._Object.getCaption(dgView.Columns(1).FooterText, dgView.Columns(1).HeaderText)
            dgView.Columns(2).HeaderText = Language._Object.getCaption(dgView.Columns(2).FooterText, dgView.Columns(2).HeaderText)
            dgView.Columns(3).HeaderText = Language._Object.getCaption(dgView.Columns(3).FooterText, dgView.Columns(3).HeaderText)
            dgView.Columns(4).HeaderText = Language._Object.getCaption(dgView.Columns(4).FooterText, dgView.Columns(4).HeaderText)
            dgView.Columns(5).HeaderText = Language._Object.getCaption(dgView.Columns(5).FooterText, dgView.Columns(5).HeaderText)
            dgView.Columns(6).HeaderText = Language._Object.getCaption(dgView.Columns(6).FooterText, dgView.Columns(6).HeaderText)
            dgView.Columns(7).HeaderText = Language._Object.getCaption(dgView.Columns(7).FooterText, dgView.Columns(7).HeaderText)
            dgView.Columns(8).HeaderText = Language._Object.getCaption(dgView.Columns(8).FooterText, dgView.Columns(8).HeaderText)
            dgView.Columns(9).HeaderText = Language._Object.getCaption(dgView.Columns(9).FooterText, dgView.Columns(9).HeaderText)
            dgView.Columns(10).HeaderText = Language._Object.getCaption(dgView.Columns(10).FooterText, dgView.Columns(10).HeaderText)
            dgView.Columns(11).HeaderText = Language._Object.getCaption(dgView.Columns(11).FooterText, dgView.Columns(11).HeaderText)
            dgView.Columns(12).HeaderText = Language._Object.getCaption(dgView.Columns(12).FooterText, dgView.Columns(12).HeaderText)
            dgView.Columns(13).HeaderText = Language._Object.getCaption(dgView.Columns(13).FooterText, dgView.Columns(13).HeaderText)
            dgView.Columns(20).HeaderText = Language._Object.getCaption(dgView.Columns(20).FooterText, dgView.Columns(20).HeaderText)


            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (15-Mar-2019) -- End

            Language.setLanguage(mstrModuleNameClaim)

            Me.lblpopupHeader.Text = Language._Object.getCaption(mstrModuleNameClaim, Me.lblpopupHeader.Text)
            Me.Label5.Text = Language._Object.getCaption(mstrModuleNameClaim, Me.Label5.Text)

            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmpAddEdit.Text = Language._Object.getCaption("lblEmployee", Me.lblEmpAddEdit.Text)
            Me.lblLeaveTypeAddEdit.Text = Language._Object.getCaption("lblLeaveType", Me.lblLeaveTypeAddEdit.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Language._Object.getCaption(Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Language._Object.getCaption(Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Language._Object.getCaption(Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblBalanceAddEdit.Text = Language._Object.getCaption("lblBalance", Me.lblBalanceAddEdit.Text)
            Me.lblQty.Text = Language._Object.getCaption(Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Language._Object.getCaption(Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Language._Object.getCaption(Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblSector.Text = Language._Object.getCaption(Me.lblSector.ID, Me.lblSector.Text)
            Me.tbExpenseRemark.HeaderText = Language._Object.getCaption(Me.tbExpenseRemark.ID, Me.tbExpenseRemark.HeaderText)
            Me.tbClaimRemark.HeaderText = Language._Object.getCaption(Me.tbClaimRemark.ID, Me.tbClaimRemark.HeaderText)
            Me.LblDomicileAdd.Text = Language._Object.getCaption(Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)
            Me.lnkClaimDepedents.Text = Language._Object.getCaption("lnkViewDependants", Me.lnkClaimDepedents.Text)
            Me.btnClaimClose.Text = Language._Object.getCaption("btnClose", Me.btnClaimClose.Text).Replace("&", "")

            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            Me.lblBalanceasondate.Text = Language._Object.getCaption(lblBalanceasondate.ID, Me.lblBalanceasondate.Text)
            'Pinkal (30-Apr-2018) - End

            dgvData.Columns(0).HeaderText = Language._Object.getCaption(dgvData.Columns(0).FooterText, dgvData.Columns(0).HeaderText)
            dgvData.Columns(1).HeaderText = Language._Object.getCaption(dgvData.Columns(1).FooterText, dgvData.Columns(1).HeaderText)
            dgvData.Columns(2).HeaderText = Language._Object.getCaption(dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            dgvData.Columns(3).HeaderText = Language._Object.getCaption(dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            dgvData.Columns(4).HeaderText = Language._Object.getCaption(dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            dgvData.Columns(5).HeaderText = Language._Object.getCaption(dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            dgvData.Columns(6).HeaderText = Language._Object.getCaption(dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)


            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (15-Mar-2019) -- End

            Language.setLanguage(mstrModuleName2)

            Me.LblEmpDependentsList.Text = Language._Object.getCaption(mstrModuleName2, Me.LblEmpDependentsList.Text)
            Me.Label6.Text = Language._Object.getCaption(mstrModuleName2, Me.Label6.Text)
            Me.btnEmppnlEmpDepedentsClose.Text = Language._Object.getCaption("btnClose", Me.btnEmppnlEmpDepedentsClose.Text).Replace("&", "")

            dgDepedent.Columns(0).HeaderText = Language._Object.getCaption(dgDepedent.Columns(0).FooterText, dgDepedent.Columns(0).HeaderText)
            dgDepedent.Columns(1).HeaderText = Language._Object.getCaption(dgDepedent.Columns(1).FooterText, dgDepedent.Columns(1).HeaderText)
            dgDepedent.Columns(2).HeaderText = Language._Object.getCaption(dgDepedent.Columns(2).FooterText, dgDepedent.Columns(2).HeaderText)
            dgDepedent.Columns(3).HeaderText = Language._Object.getCaption(dgDepedent.Columns(3).FooterText, dgDepedent.Columns(3).HeaderText)
            dgDepedent.Columns(4).HeaderText = Language._Object.getCaption(dgDepedent.Columns(4).FooterText, dgDepedent.Columns(4).HeaderText)
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            displaymessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

   

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "You can't Edit this Leave detail. Reason: You are logged in into another user login.")
            Language.setMessage(mstrModuleName, 3, "You can't Edit this Leave detail. Reason: This Leave approval is still pending.")
            Language.setMessage(mstrModuleName, 4, "You can't Edit this Leave detail. Reason: This Leave is already rejected.")
            Language.setMessage(mstrModuleName, 5, "You can't Edit this Leave detail. Reason: This Leave is already re-scheduled.")
            Language.setMessage(mstrModuleName, 6, "You can't Edit this Leave detail. Reason: This Leave approval is already approved.")
            Language.setMessage(mstrModuleName, 7, "You can't Edit this Leave detail. Reason: This Leave is already approved.")
            Language.setMessage(mstrModuleName, 9, "You can't Edit this Leave detail. Reason: This Leave is already issued.")
            Language.setMessage(mstrModuleName, 10, "You can't Edit this Leave detail. Reason: This Leave is already Cancelled.")
            Language.setMessage(mstrModuleName, 11, "Approved By :-")
            Language.setMessage(mstrModuleName, 12, "Rejected By :-")
            Language.setMessage(mstrModuleName, 13, "Re-Scheduled By :-")
            Language.setMessage(mstrModuleName, 14, "Issued By :-")
            Language.setMessage(mstrModuleName, 16, "Employee is compulsory information.Please Select Employee.")
            Language.setMessage(mstrModuleName, 17, "Leave Type is compulsory information.Please Select Leave Type.")

            Language.setMessage(mstrModuleNameClaim, 12, "Leave Form")
            Language.setMessage(mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue.")


        Catch Ex As Exception
            displaymessage.DisplayError("SetMessages : " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
