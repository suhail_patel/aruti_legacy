﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Leave_wPg_AssignIssueUser_Emp
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private objissueUser As New clsissueUser_Tran

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmAssignIssueUser_Employee"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End

                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If IsPostBack = False Then

                FillCombo()

                If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 Then
                    drpIssueUser.AutoPostBack = True
                    LblToIssueUser.Visible = True
                    drpToIssueUser.Visible = True
                    drpIssueUser.Enabled = False
                    objissueUser._Issueusertranunkid = CInt(Session("issueusertranunkid"))
                    objissueUser.GetList("List", objissueUser._Issueuserunkid)
                    Me.ViewState.Add("SelectedEmployee", objissueUser._dtEmployee)
                    GetValue()
                Else
                    drpIssueUser.AutoPostBack = False
                    LblToIssueUser.Visible = False
                    drpToIssueUser.Visible = False
                    Me.ViewState.Add("SelectedEmployee", objissueUser._dtEmployee)
                End If
                FillEmployeeList()

                Me.ViewState.Add("FirstRecordNo", 0)
                Me.ViewState.Add("LastRecordNo", GvEmployee.PageSize)

                Me.ViewState.Add("FirstSelectedEmpRNo", 0)
                Me.ViewState.Add("LastSelectedEmpRNo", GvSelectedEmployee.PageSize)

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreInit : " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (30-Nov-2013) -- Start
    'Enhancement : Oman Changes

    'Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
    '    Try
    '        Session("issueusertranunkid") = Nothing
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Page_Unload :- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Pinkal (30-Nov-2013) -- End


#End Region

#Region " Private Methods"

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim mblnAdUser As Boolean = False
        Try
            'FOR ISSUE USER

            'dsFill = objIssueUser.GetUserWithIssueLeavePrivilage("IssueUser", True)
            'drpIssueUser.DataValueField = "userunkid"
            'drpIssueUser.DataTextField = "username"
            'drpIssueUser.DataSource = dsFill.Tables("IssueUser")
            'drpIssueUser.DataBind()

            Dim objUser As New clsUserAddEdit
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            If drOption.Length > 0 Then
                If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            End If



            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            'dsFill = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, CInt(Session("CompanyUnkId")), 264)
            dsFill = objUser.getNewComboList("User", 0, True, CInt(Session("CompanyUnkId")), CStr(264), CInt(Session("Fin_year")))
            'Pinkal (01-Mar-2016) -- End


            drpIssueUser.DataTextField = "name"
            drpIssueUser.DataValueField = "userunkid"
            drpIssueUser.DataSource = dsFill.Tables("User")
            drpIssueUser.DataBind()

            objOption = Nothing
            objUser = Nothing


            If Me.ViewState("IssueUserList") Is Nothing Then
                Me.ViewState.Add("IssueUserList", dsFill.Tables("IssueUser"))
            End If


            'FOR DEPARTMENT
            Dim objDepartment As New clsDepartment
            dsFill = objDepartment.getComboList("Department", True)
            drpDepartment.DataValueField = "departmentunkid"
            drpDepartment.DataTextField = "name"
            drpDepartment.DataSource = dsFill.Tables("Department")
            drpDepartment.DataBind()

            ''FOR FILTER SECTION
            dsFill = Nothing
            Dim objSection As New clsSections
            dsFill = objSection.getComboList("Section", True)
            drpSection.DataValueField = "sectionunkid"
            drpSection.DataTextField = "name"
            drpSection.DataSource = dsFill.Tables("Section")
            drpSection.DataBind()

            ''FOR FILTER Job
            dsFill = Nothing
            Dim objjob As New clsJobs
            dsFill = objjob.getComboList("Job", True)
            drpJob.DataValueField = "jobunkid"
            drpJob.DataTextField = "name"
            drpJob.DataSource = dsFill.Tables("Job")
            drpJob.DataBind()


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Try
            drpIssueUser.SelectedValue = objissueUser._Issueuserunkid.ToString()

            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 Then
                drpIssueUser_SelectedIndexChanged(New Object, New EventArgs())
            End If

            SelectedEmplyeeList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As DataSet = Nothing
        Dim dtEmployee As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try

            Dim objEmployee As New clsEmployee_Master

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsEmployee = objEmployee.GetList("Employee", True, , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'Else
            '    dsEmployee = objEmployee.GetList("Employee", True)
            'End If


            'If dsEmployee IsNot Nothing Then
            '    dsEmployee.Tables(0).Columns.Add("Select", Type.GetType("System.Boolean"))
            '    dsEmployee.Tables(0).Columns("Select").DefaultValue = False
            'End If

            'Dim mstrEmployeeIDs As String = objissueUser.GetEmployeeIdFromIssueUser()
            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    strSearch &= "AND employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            'End If

            '            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 Then

            'FillEmployee:
            '                If CBool(Session("IsIncludeInactiveEmp")) = False Then

            '                    If strSearch.Length > 0 Then
            '                        strSearch = strSearch.Substring(3)
            '                        dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            '                    Else
            '                        dtEmployee = dsEmployee.Tables(0)
            '                    End If

            '                Else
            '                    If strSearch.Length > 0 Then
            '                        strSearch = strSearch.Substring(3)
            '                        dtEmployee = New DataView(dsEmployee.Tables("Employee"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            '                    Else
            '                        dtEmployee = dsEmployee.Tables("Employee")
            '                    End If

            '                End If


            '            Else

            '                GoTo FillEmployee

            '            End If

            Dim mstrEmployeeIDs As String = objissueUser.GetEmployeeIdFromIssueUser()
            If mstrEmployeeIDs.Trim.Length > 0 Then
                strSearch &= "AND hremployee_master.employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
                    End If

                    If strSearch.Length > 0 Then
                        strSearch = strSearch.Substring(3)
                    End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'dsEmployee = objEmployee.GetList(Session("Database_Name"), Session("UserId"), _
            '                                 Session("Fin_year"), _
            '                                 Session("CompanyUnkId"), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 Session("UserAccessModeSetting"), True, _
            '                                 Session("IsIncludeInactiveEmp"), _
            '                                 "Employee", _
            '                                 Session("ShowFirstAppointmentDate"), , , _
            '                                 strSearch)

            dsEmployee = objEmployee.GetList(Session("Database_Name").ToString(), CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                           Session("UserAccessModeSetting").ToString(), True, _
                                           False, _
                                            "Employee", _
                                           CBool(Session("ShowFirstAppointmentDate")), -1, False, strSearch, False, True)

            'Pinkal (06-Jan-2016) -- End


            If dsEmployee IsNot Nothing Then
                dsEmployee.Tables(0).Columns.Add("Select", Type.GetType("System.Boolean"))
                dsEmployee.Tables(0).Columns("Select").DefaultValue = False
            End If

            dtEmployee = New DataView(dsEmployee.Tables("Employee"), "", "", DataViewRowState.CurrentRows).ToTable

            'Shani(24-Aug-2015) -- End

            Session.Add("IssueUserEmpList", dtEmployee)
            GvEmployee.DataSource = dtEmployee
            GvEmployee.DataBind()


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillEmployeeList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillEmployeeList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SelectedEmplyeeList()
        Dim strSearch As String = String.Empty
        Dim dtEmployee As DataTable = Nothing
        Try
            If Me.ViewState("SelectedEmployee") Is Nothing Then Exit Sub
            Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'If Session("issueusertranunkid") IsNot Nothing OrElse dtSelectedEmp.Columns.Contains("Select") = False Then
            '    dtSelectedEmp.Columns.Add("Select", Type.GetType("System.Boolean"))
            '    dtSelectedEmp.Columns("Select").DefaultValue = False
            'End If

            If dtSelectedEmp.Columns.Contains("Select") = False Then
                dtSelectedEmp.Columns.Add("Select", Type.GetType("System.Boolean"))
                dtSelectedEmp.Columns("Select").DefaultValue = False
            End If

            'Pinkal (06-Jan-2016) -- End


            strSearch = "AND AUD <> 'D' "

            If CInt(drpDepartment.SelectedValue) > 0 Then
                strSearch &= "AND departmentunkid=" & CInt(drpDepartment.SelectedValue) & " "
            End If
            If CInt(drpSection.SelectedValue) > 0 Then
                strSearch &= "AND sectionunkid=" & CInt(drpSection.SelectedValue)
            End If
            If CInt(drpJob.SelectedValue) > 0 Then
                strSearch &= "AND jobunkid=" & CInt(drpJob.SelectedValue)
            End If


            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtEmployee = New DataView(dtSelectedEmp, strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = dtSelectedEmp
            End If

            GvSelectedEmployee.DataSource = dtEmployee
            GvSelectedEmployee.DataBind()
            Me.ViewState("SelectedEmp") = dtEmployee

            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 AndAlso CInt(drpToIssueUser.SelectedValue) > 0 Then
                UpdateMigratedEmployee()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SelectedEmplyeeList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SelectedEmplyeeList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            If dtEmployee IsNot Nothing Then dtEmployee.Dispose()
        End Try

    End Sub

    Private Sub UpdateMigratedEmployee()
        Try

            Dim mdtTran As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtTran.Select("AUD <> 'D'")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        If drRow(i)("AUD").ToString() <> "A" Then
                            drRow(i)("oldissueuserunkid") = CInt(drpIssueUser.SelectedValue)
                            drRow(i)("oldissueuser") = drpIssueUser.SelectedItem.Text
                        End If
                        drRow(i)("issueuserunkid") = CInt(drpToIssueUser.SelectedValue)
                        drRow(i)("issueuser") = drpToIssueUser.SelectedItem.Text
                        drRow(i).AcceptChanges()
                    Next
                End If
            End If
            Me.ViewState("SelectedEmployee") = mdtTran
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("UpdateMigratedEmployee:- " & ex.Message, Me)
            DisplayMessage.DisplayError("UpdateMigratedEmployee:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(drpIssueUser.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage("Issue User is compulsory information.Please Select Issue User.", Me)
                drpIssueUser.Focus()
                Return False
            End If

            If Me.ViewState("IssueUserList") IsNot Nothing AndAlso CInt(drpIssueUser.SelectedValue) > 0 AndAlso drpIssueUser.Enabled Then
                Dim dRow As DataRow() = CType(Me.ViewState("IssueUserList"), DataTable).Select("userunkid = " & CInt(drpIssueUser.SelectedValue))
                If dRow.Length > 0 Then
                    If dRow(0)("email").ToString().Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage("There is no email address specified for the selected user. Please set the valid email address for this user from Aruti Configuration -> User Creation -> User Add/Edit.", Me)
                        drpIssueUser.Focus()
                        Return False
                    End If
                End If
            End If

            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 Then
                If CInt(drpToIssueUser.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("To Issue User is compulsory information.Please Select To Issue User.", Me)
                    drpToIssueUser.Focus()
                    Return False
                End If

                If CInt(drpToIssueUser.SelectedValue) > 0 AndAlso drpToIssueUser.Visible AndAlso drpToIssueUser.Enabled Then
                    Dim dRow As DataRow() = CType(Me.ViewState("ToIssueUserList"), DataTable).Select("userunkid = " & CInt(drpToIssueUser.SelectedValue))
                    If dRow.Length > 0 Then
                        If dRow(0)("email").ToString().Trim.Length <= 0 Then
                            DisplayMessage.DisplayMessage("There is no email address specified for the selected user. Please set the valid email address for this user from Aruti Configuration -> User Creation -> User Add/Edit.", Me)
                            drpToIssueUser.Focus()
                            Return False
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Validation:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Sub GetEmployeePageRecordNo()
        Try
            If GvEmployee.PageIndex > 0 Then
                Me.ViewState("FirstRecordNo") = (((GvEmployee.PageIndex + 1) * GvEmployee.Rows.Count) - GvEmployee.Rows.Count)
            Else
                If GvEmployee.Rows.Count < GvEmployee.PageSize Then
                    Me.ViewState("FirstRecordNo") = ((1 * GvEmployee.Rows.Count) - GvEmployee.Rows.Count)
                Else
                    Me.ViewState("FirstRecordNo") = ((1 * GvEmployee.PageSize) - GvEmployee.Rows.Count)
                End If

            End If
            Me.ViewState("LastRecordNo") = ((GvEmployee.PageIndex + 1) * GvEmployee.Rows.Count)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetEmployeePageRecordNo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetEmployeePageRecordNo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetSelectedEmpPageRecordNo()
        Try
            If GvSelectedEmployee.PageIndex > 0 Then
                Me.ViewState("FirstSelectedEmpRNo") = (((GvSelectedEmployee.PageIndex + 1) * GvSelectedEmployee.Rows.Count) - GvSelectedEmployee.Rows.Count)
            Else
                If GvSelectedEmployee.Rows.Count < GvSelectedEmployee.PageSize Then
                    Me.ViewState("FirstSelectedEmpRNo") = ((1 * GvSelectedEmployee.Rows.Count) - GvSelectedEmployee.Rows.Count)
                Else
                    Me.ViewState("FirstSelectedEmpRNo") = ((1 * GvSelectedEmployee.PageSize) - GvSelectedEmployee.Rows.Count)
                End If
            End If
                Me.ViewState("LastSelectedEmpRNo") = ((GvSelectedEmployee.PageIndex + 1) * GvSelectedEmployee.Rows.Count)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetSelectedEmpPageRecordNo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetSelectedEmpPageRecordNo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DropDown Event"

    Private Sub drpIssueUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpIssueUser.SelectedIndexChanged
        Try
            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 Then
                Dim dsFill As DataSet = objissueUser.GetUserWithIssueLeavePrivilage(CInt(Session("Fin_year")), "IssueUser", True)
                drpToIssueUser.DataValueField = "userunkid"
                drpToIssueUser.DataTextField = "username"
                drpToIssueUser.DataSource = New DataView(dsFill.Tables(0), "userunkid <> " & CInt(drpIssueUser.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                drpToIssueUser.DataBind()

                If Me.ViewState("ToIssueUserList") Is Nothing Then
                    Me.ViewState.Add("ToIssueUserList", New DataView(dsFill.Tables(0), "userunkid <> " & CInt(drpIssueUser.SelectedValue), "", DataViewRowState.CurrentRows).ToTable)
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpIssueUser_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("drpIssueUser_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If GvEmployee.Rows.Count <= 0 Then Exit Sub
            Dim dtEmployee As DataTable = CType(Session("IssueUserEmpList"), DataTable)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'GetEmployeePageRecordNo()
            'If Me.ViewState("FirstRecordNo") IsNot Nothing AndAlso Me.ViewState("LastRecordNo") IsNot Nothing Then
            If GvEmployee.Rows.Count > 0 Then
                'Dim mintFirstRecord As Integer = Me.ViewState("FirstRecordNo")
                'Dim mintLastRecord As Integer = Me.ViewState("LastRecordNo")
                'Dim mintrowindex As Integer = 0
                'For i As Integer = mintFirstRecord To mintLastRecord - 1
                For i As Integer = 0 To GvEmployee.Rows.Count - 1
                    If dtEmployee.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = dtEmployee.Select("employeecode = '" & GvEmployee.Rows(i).Cells(1).Text.Trim & "'") 'SHANI [01 FEB 2015]--GvEmployee.Rows(mintrowindex).Cells(1).Text.Trim
                    If drRow.Length > 0 Then
                        drRow(0)("Select") = cb.Checked
                        Dim gvRow As GridViewRow = GvEmployee.Rows(i)  'SHANI [01 FEB 2015]--GvEmployee.Rows(mintrowindex)
                        CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = cb.Checked
                    End If
                    dtEmployee.AcceptChanges()
                    'mintrowindex += 1
                Next
                'SHANI [01 FEB 2015]--END
                Session("EmpList") = dtEmployee

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkAll_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = CType(Session("IssueUserEmpList"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
                If drRow.Length > 0 Then
                    drRow(0)("Select") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkgvSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkgvSelect_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkSelectedEmpAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If GvSelectedEmployee.Rows.Count <= 0 Then Exit Sub
            Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'GetSelectedEmpPageRecordNo()
            'If Me.ViewState("FirstSelectedEmpRNo") IsNot Nothing AndAlso Me.ViewState("LastSelectedEmpRNo") IsNot Nothing Then
            If GvSelectedEmployee.Rows.Count > 0 Then
                'Dim mintFirstRecord As Integer = Me.ViewState("FirstSelectedEmpRNo")
                'Dim mintLastRecord As Integer = Me.ViewState("LastSelectedEmpRNo")
                'Dim mintrowindex As Integer = 0
                'For i As Integer = mintFirstRecord To mintLastRecord - 1
                For i As Integer = 0 To GvSelectedEmployee.Rows.Count - 1
                    If dtSelectedEmp.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = dtSelectedEmp.Select("employeecode = '" & GvSelectedEmployee.Rows(i).Cells(2).Text.Trim & "'") 'SHANI [01 FEB 2015]-GvSelectedEmployee.Rows(mintrowindex).Cells(2).Text.Trim 
                    If drRow.Length > 0 Then
                        drRow(0)("Select") = cb.Checked
                        Dim gvRow As GridViewRow = GvSelectedEmployee.Rows(i) 'SHANI [01 FEB 2015]--GvSelectedEmployee.Rows(mintrowindex)
                        CType(gvRow.FindControl("ChkgvSelectedEmp"), CheckBox).Checked = cb.Checked
                    End If
                    dtSelectedEmp.AcceptChanges()
                    'mintrowindex += 1
                Next
                'SHANI [01 FEB 2015]--END
                Me.ViewState("SelectedEmployee") = dtSelectedEmp
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkSelectedEmpAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkSelectedEmpAll_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkgvSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = CType(Me.ViewState("SelectedEmployee"), DataTable).Select("employeecode = '" & gvRow.Cells(2).Text & "'")
                If drRow.Length > 0 Then
                    drRow(0)("Select") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkgvSelectedEmp_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkgvSelectedEmp_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim dvEmployee As DataView = CType(Session("IssueUserEmpList"), DataTable).DefaultView
            If dvEmployee IsNot Nothing Then
                If dvEmployee.Table.Rows.Count > 0 Then
                    dvEmployee.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%' "
                    GvEmployee.DataSource = dvEmployee
                    GvEmployee.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearch_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtSearch_TextChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub GvEmployee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvEmployee.PageIndexChanging
        Try
            GvEmployee.PageIndex = e.NewPageIndex
            GetEmployeePageRecordNo()
            GvEmployee.DataSource = CType(Session("IssueUserEmpList"), DataTable)
            GvEmployee.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvEmployee_PageIndexChanging :- " & ex.Message, Me)
            DisplayMessage.DisplayError("GvEmployee_PageIndexChanging :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub GvEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvEmployee.RowDataBound
        Try
            If e.Row.Cells.Count > 1 Then

                If e.Row.RowIndex > -1 Then
                    Dim dRow() As DataRow = CType(Session("IssueUserEmpList"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "'")

                    If dRow.Length > 0 Then
                        If IsDBNull(dRow(0).Item("select")) Then dRow(0).Item("select") = False

                        If CBool(dRow(0).Item("select")) = True Then
                            Dim gvRow As GridViewRow = e.Row
                            CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = CBool(dRow(0).Item("select"))
                        End If

                    End If

                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvEmployee_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GvEmployee_RowDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub GvSelectedEmployee_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvSelectedEmployee.PageIndexChanging
        Try
            GvSelectedEmployee.PageIndex = e.NewPageIndex
            GetSelectedEmpPageRecordNo()
            GvSelectedEmployee.DataSource = CType(Me.ViewState("SelectedEmployee"), DataTable)
            GvSelectedEmployee.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvSelectedEmployee_PageIndexChanging :- " & ex.Message, Me)
            DisplayMessage.DisplayError("GvSelectedEmployee_PageIndexChanging :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub GvSelectedEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvSelectedEmployee.RowDataBound
        Try
            If e.Row.Cells.Count > 1 Then

                If e.Row.RowIndex > -1 Then
                    Dim dRow() As DataRow = CType(Me.ViewState("SelectedEmployee"), DataTable).Select("employeecode = '" & e.Row.Cells(2).Text & "'")

                    If dRow.Length > 0 Then
                        If IsDBNull(dRow(0).Item("select")) Then dRow(0).Item("select") = False

                        If CBool(dRow(0).Item("select")) = True Then
                            Dim gvRow As GridViewRow = e.Row
                            CType(gvRow.FindControl("ChkgvSelectedEmp"), CheckBox).Checked = CBool(dRow(0).Item("select"))
                        End If

                    End If

                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvSelectedEmployee_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GvSelectedEmployee_RowDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then Exit Sub

            Dim dtEmployee As DataTable = CType(Session("IssueUserEmpList"), DataTable)

            Dim drRow() As DataRow = dtEmployee.Select("Select = true")
            If drRow.Length <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            Dim dtRow As DataRow
            Dim count As Integer = 0


            Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)

            'If dtSelectedEmp.Columns.Contains("Select") = False Then
            '    dtSelectedEmp.Columns.Add("Select", Type.GetType("System.Boolean"))
            '    dtSelectedEmp.Columns("Select").DefaultValue = False
            'End If



            For i As Integer = 0 To drRow.Length - 1
                count = CInt(dtSelectedEmp.Compute("count(employeeunkid)", "employeeunkid=" & CInt(drRow(i)("employeeunkid")) & " AND AUD <> 'D'"))
                If count > 0 Then
                    Dim dr() As DataRow = Nothing
                    dr = dtSelectedEmp.Select("employeeunkid=" & CInt(drRow(i)("employeeunkid")))
                    If dr.Length > 0 Then

                        'Pinkal (09-Jan-2013) -- Start
                        'Enhancement : TRA Changes

                        'DisplayMessage.DisplayMessage("Some of the employee(s) are already exist in the selected employee List.Please Select Other employee(s).", Me)
                        'Exit Sub

                        Continue For

                        'Pinkal (09-Jan-2013) -- End
                    End If
                Else

                    dtRow = dtSelectedEmp.NewRow

                    If Session("issueusertranunkid") Is Nothing AndAlso CInt(Session("issueusertranunkid")) <= 0 Then
                        dtRow("issueusertranunkid") = -1
                        dtRow("issueuserunkid") = CInt(drpIssueUser.SelectedValue)
                        dtRow("issueuser") = drpIssueUser.SelectedItem.Text.Trim
                    Else
                        dtRow("issueusertranunkid") = -1
                        dtRow("issueuserunkid") = CInt(drpToIssueUser.SelectedValue)
                        dtRow("issueuser") = drpToIssueUser.SelectedItem.Text.Trim
                    End If

                    dtRow("oldissueuserunkid") = -1
                    dtRow("oldissueuser") = ""
                    dtRow("employeeunkid") = CInt(drRow(i)("employeeunkid"))
                    dtRow("employeecode") = drRow(i)("employeecode").ToString().Trim
                    dtRow("employee") = drRow(i)("name").ToString().Trim
                    dtRow("departmentunkid") = CInt(drRow(i)("departmentunkid"))
                    dtRow("department") = drRow(i)("deptName").ToString().Trim
                    dtRow("sectionunkid") = CInt(drRow(i)("sectionunkid"))
                    dtRow("section") = drRow(i)("section").ToString().Trim
                    dtRow("jobunkid") = CInt(drRow(i)("jobunkid"))
                    dtRow("job") = drRow(i)("job_name").ToString().Trim
                    dtRow("AUD") = "A"
                    dtRow("GUID") = Guid.NewGuid.ToString
                    dtSelectedEmp.Rows.Add(dtRow)
                End If
            Next

            SelectedEmplyeeList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAdd_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnAdd_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try

            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 AndAlso CInt(drpToIssueUser.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("To Issue User is compulsory information.Please Select To Issue User.", Me)
                drpToIssueUser.Focus()
                Exit Sub
            End If

            Dim dtSelectedEmp As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
            If dtSelectedEmp Is Nothing OrElse dtSelectedEmp.Columns.Contains("Select") = False Then Exit Sub
            Dim drRow() As DataRow = dtSelectedEmp.Select("Select = true")
            If drRow.Length <= 0 Then
                DisplayMessage.DisplayMessage("Selected Employee is compulsory information.Please Check atleast One Selected Employee.", Me)
                Exit Sub
            End If

            Dim drTemp As DataRow() = Nothing

            For i As Integer = 0 To drRow.Length - 1

                If CInt(drRow(i)("issueusertranunkid")) = -1 Then
                    drTemp = dtSelectedEmp.Select("GUID = '" & drRow(i)("GUID").ToString() & "'")
                Else
                    drTemp = dtSelectedEmp.Select("issueusertranunkid = " & CInt(drRow(i)("issueusertranunkid")))
                End If

                If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                End If
            Next
            If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 AndAlso CInt(drpToIssueUser.SelectedValue) > 0 Then
                UpdateMigratedEmployee()
            End If
            SelectedEmplyeeList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDelete_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnDelete_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            Dim mdtTran As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)

            Dim count As Integer = CInt(mdtTran.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtTran.Rows.Count = 0 Or count = 0 Or GvSelectedEmployee.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            objissueUser._Userunkid = CInt(Session("UserId"))
            objissueUser._dtEmployee = mdtTran

            With objissueUser
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If Session("issueusertranunkid") Is Nothing AndAlso CInt(Session("issueusertranunkid")) <= 0 Then
                blnFlag = objissueUser.Insert()
            Else

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                If CInt(drpToIssueUser.SelectedValue) > 0 Then UpdateMigratedEmployee()
                'Pinkal (06-Jan-2016) -- End
                blnFlag = objissueUser.Migration_Insert()
            End If


            If blnFlag = False And objissueUser._Message <> "" Then
                DisplayMessage.DisplayMessage("btnSave_Click:- " & objissueUser._Message, Me)
            End If

            If blnFlag Then
                Session("IssueUserEmpList") = Nothing
                Dim dtTable As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
                dtTable.Rows.Clear()
                Me.ViewState("SelectedEmployee") = dtTable
                Session("issueusertranunkid") = Nothing
                Me.ViewState("FirstRecordNo") = Nothing
                Me.ViewState("LastRecordNo") = Nothing
                Me.ViewState("FirstSelectedEmpRNo") = Nothing
                Me.ViewState("LastSelectedEmpRNo") = Nothing
                Me.ViewState("IssueUserList") = Nothing
                Me.ViewState("ToIssueUserList") = Nothing
                drpIssueUser.SelectedIndex = 0
                txtSearch.Text = ""
                drpDepartment.SelectedIndex = 0
                drpJob.SelectedIndex = 0
                drpSection.SelectedIndex = 0
                FillEmployeeList()
                GvSelectedEmployee.DataSource = Nothing
                GvSelectedEmployee.DataBind()
                If Session("issueusertranunkid") IsNot Nothing AndAlso CInt(Session("issueusertranunkid")) > 0 Then
                    Session("issueusertranunkid") = Nothing
                    DisplayMessage.DisplayMessage("Entry Migrated Sucessfully.", Me)

                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.
                    'Re(sponse.Redirect(Session("servername") & "~/Leave/wPg_AssignIssueUserList.aspx"))
                    Response.Redirect("~/Leave/wPg_AssignIssueUserList.aspx", False)
                    'Pinkal (06-Jan-2016) -- End
                Else
                    DisplayMessage.DisplayMessage("Entry Saved Sucessfully.", Me)
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            SelectedEmplyeeList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSearch_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpDepartment.SelectedIndex = 0
            drpSection.SelectedIndex = 0
            drpJob.SelectedIndex = 0
            SelectedEmplyeeList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'SHANI [01 FEB 2015]--END
        'Shani [ 24 DEC 2014 ] -- END

        Try

            'Pinkal (30-Nov-2013) -- Start
            'Enhancement : Oman Changes
            Session("issueusertranunkid") = Nothing
            'Pinkal (30-Nov-2013) -- End


            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx")
            Response.Redirect("~\Leave\wPg_AssignIssueUserList.aspx", False)
            'SHANI [09 Mar 2015]--END 
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Session("issueusertranunkid") = Nothing
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

   
    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
        'SHANI [01 FEB 2015]--END

        Me.lblCaption.Text = Language._Object.getCaption(mstrModuleName, Me.lblCaption.Text)

        Me.LblIssueUser.Text = Language._Object.getCaption(Me.LblIssueUser.ID, Me.LblIssueUser.Text)
        Me.LblToIssueUser.Text = Language._Object.getCaption(Me.LblToIssueUser.ID, Me.LblToIssueUser.Text)
        Me.lblFilterDepartment.Text = Language._Object.getCaption(Me.lblFilterDepartment.ID, Me.lblFilterDepartment.Text)
        Me.lblFilterSection.Text = Language._Object.getCaption(Me.lblFilterSection.ID, Me.lblFilterSection.Text)
        Me.lblFilterjob.Text = Language._Object.getCaption(Me.lblFilterjob.ID, Me.lblFilterjob.Text)

        Me.GvEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.GvEmployee.Columns(1).FooterText, Me.GvEmployee.Columns(1).HeaderText)
        Me.GvEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.GvEmployee.Columns(2).FooterText, Me.GvEmployee.Columns(2).HeaderText)

        Me.GvSelectedEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(1).FooterText, Me.GvSelectedEmployee.Columns(1).HeaderText)
        Me.GvSelectedEmployee.Columns(3).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(3).FooterText, Me.GvSelectedEmployee.Columns(3).HeaderText)
        Me.GvSelectedEmployee.Columns(4).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(4).FooterText, Me.GvSelectedEmployee.Columns(4).HeaderText)
        Me.GvSelectedEmployee.Columns(5).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(5).FooterText, Me.GvSelectedEmployee.Columns(5).HeaderText)
        Me.GvSelectedEmployee.Columns(6).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(6).FooterText, Me.GvSelectedEmployee.Columns(6).HeaderText)

        Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
        Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
        Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
   
End Class
