'Imports System
'Imports System.Configuration
'Imports System.Web
'Imports System.Web.Security
'Imports System.Web.UI
'Imports System.Web.UI.WebControls
'Imports System.Web.UI.WebControls.WebParts
'Imports System.Web.UI.HtmlControls
'Imports System.Threading
Imports System.Data
Imports eZeeCommonLib
Imports eZeeCommonLib.DisplayError
Imports eZeeCommonLib.clsDataOperation
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.IO
Imports System.Web.Security
Imports System.DirectoryServices

Partial Class Index
    Inherits Basepage

#Region " Private Variables "
    Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
    Dim ds As New DataSet
    Dim mdbname As String = ""
    Dim msql As String
    Dim objGlobalAccess As New GlobalAccess
    ' Dim objTest As New Test
    Dim clsuser As New User
    'Dim MessageBox As New MessageBox
    Private Shared DisplayMessage As New CommonCodes
    Private _pStDate As DateTime
    Dim mainds As DataSet
    'Sohail (28 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Private objCONN As SqlConnection
    'Sohail (28 Oct 2013) -- End
    Private mintOneCompanyUnkId As String = 0 'Sohail (02 May 2014) 

    'Anjan [20 January 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew.
    Private ReadOnly mstrModuleName As String = "frmChangePassword"
    'Anjan [20 January 2014] -- End
    Private ReadOnly mstrModuleNameLogin As String = "frmLogin" 'Sohail (11 Aug 2021)


    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.
    Dim objPwdOpt As clsPassowdOptions
    Dim mintAuthenticationMode As enAuthenticationMode = enAuthenticationMode.BASIC_AUTHENTICATION
    Dim mdtPwdOpt As DataTable = Nothing
    'Pinkal (03-Apr-2017) -- End

#End Region

#Region " Public Properties "

    Public Property Fin_Start_Date() As DateTime
        Get
            Return _pStDate
        End Get
        Set(ByVal value As DateTime)
            value = _pStDate
        End Set
    End Property

    Private _pEndDate As DateTime
    Public Property Fin_End_Date() As DateTime
        Get
            Return _pEndDate
        End Get
        Set(ByVal value As DateTime)
            value = _pEndDate
        End Set
    End Property
#End Region

    'Sohail (02 May 2014) -- Start
    'Enhancement - Hide companies list if access not given to users.
#Region " Page's Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            objPwdOpt = New clsPassowdOptions
            'Pinkal (03-Apr-2017) -- End

            If IsPostBack = False Then
                ' BindYear() 
                txtloginname.Focus()

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                HttpContext.Current.Session("mdbname") = "hrmsConfiguration"
                'Sohail (23 Apr 2012) -- End

                'Sohail (28 Oct 2013) -- Start
                'TRA - ENHANCEMENT
                'objCONN = Nothing

                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                'Sohail (28 Oct 2013) -- End

                'Sohail (02 May 2014) -- Start
                'Enhancement - Hide companies list if access not given to users.
                'Call BindCompany()

                'ddlCompany_SelectedIndexChanged(sender, e)
                Call BindCompany(0)
                If ddlCompany.Items.Count = 1 Then
                    mintOneCompanyUnkId = CInt(ddlCompany.Items(0).Value)

                    If mintOneCompanyUnkId > 0 Then
                        LblCompanyCode.Visible = False
                        txtCompanyCode.Visible = False
                        rfvCompanyCode.Enabled = False
                        'Sohail [16 May 2014] -- Start
                        'ENHANCEMENT : New login flow
                        lblForgetCompanyCode.Visible = False
                        txtForgetCompanyCode.Visible = False
                        rfvForgetCompanyCode.Enabled = False
                        'Sohail [16 May 2014] -- End
                    Else
                        LblCompanyCode.Visible = True
                        txtCompanyCode.Visible = True
                        rfvCompanyCode.Enabled = True
                        'Sohail [16 May 2014] -- Start
                        'ENHANCEMENT : New login flow
                        lblForgetCompanyCode.Visible = True
                        txtForgetCompanyCode.Visible = True
                        rfvForgetCompanyCode.Enabled = True
                        'Sohail [16 May 2014] -- End
                    End If
                    'Sohail (11 Aug 2021) -- Start
                    'NMB Enhancement :  : Don't show company code option when there is a connection or any other issue on login in self service.
                ElseIf ddlCompany.Items.Count > 1 Then
                    txtCompanyCode.Visible = True
                    rfvCompanyCode.Enabled = True
                    txtForgetCompanyCode.Visible = True
                    rfvForgetCompanyCode.Enabled = True
                    'Sohail (11 Aug 2021) -- End
                End If
                If Not Page.ClientScript.IsStartupScriptRegistered("hideccode") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "hideccode", "hidecode();", True)
                End If
                'Sohail (02 May 2014) -- End
                ' BindEmployee()
                ' BindUser()
                'txtPwd.Focus()
                txtloginname.Focus()


                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                If objPwdOpt._DataTable IsNot Nothing AndAlso objPwdOpt._DataTable.Rows.Count > 0 Then
                    Dim drRow = From dr In objPwdOpt._DataTable Where dr("passwdoptionid") = enPasswordOption.USER_LOGIN_MODE Select dr
                    If drRow.Count > 0 Then
                        mintAuthenticationMode = CInt(drRow(0)("passwdminlen"))
                    Else
                        mintAuthenticationMode = enAuthenticationMode.BASIC_AUTHENTICATION
                    End If
                    drRow = Nothing
                End If
                'Pinkal (03-Apr-2017) -- End

                'Sohail (11 Aug 2021) -- Start
                'NMB Enhancement :  : Don't show company code option when there is a connection or any other issue on login in self service.
                If gobjLocalization Is Nothing Then
                    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, "hrmsConfiguration")
                    gobjLocalization._LangId = 1
                End If
                Call SetLanguage()
                Call Language._Object.SaveValue()
                'Sohail (11 Aug 2021) -- End


            Else
                mdbname = CType(Me.ViewState("mdbname"), String)
                mintOneCompanyUnkId = Me.ViewState("mintOneCompanyUnkId")

                'Sohail [16 May 2014] -- Start
                'ENHANCEMENT : New login flow
                If mintOneCompanyUnkId > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "hideccode", "hidecode(true);", True)
                    rfvCompanyCode.Enabled = False
                    rfvForgetCompanyCode.Enabled = False
                End If
                'Sohail [16 May 2014] -- End

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                mintAuthenticationMode = CType(Me.ViewState("AuthenticationMode"), enAuthenticationMode)
                'Pinkal (03-Apr-2017) -- End

            End If


            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            If mintAuthenticationMode <> enAuthenticationMode.BASIC_AUTHENTICATION Then
                lnkForgotPassword.Visible = False
            End If
            'Pinkal (03-Apr-2017) -- End


        Catch ex As Exception
            If ex.Message.ToLower.Contains("network") = True OrElse ex.Message.ToLower.Contains("database") = True OrElse ex.Message.ToLower.Contains("login") = True Then
                'DisplayMessage.DisplayMessage(ex.Message, Me)
                lblErrorMsg.Text = Language.getMessage(mstrModuleNameLogin, 8, "Sorry, We are not able to connect aruti database server. Please check if database server is reachable or try again after some time or contact aruti support team.")
            Else
                'DisplayMessage.DisplayError(ex.Message, Me)
                lblErrorMsg.Text = ex.Message.ToString
            End If
            pnlMain.Visible = False
            pnlError.Visible = True
        End Try

    End Sub

    'Varsha (10 Nov 2017) -- Start
    'Company Default SS theme [Anatory Rutta / CCBRT] - (RefNo: 55) - Provide option for Organisation to set company default SS theme to all employees
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            'Sohail (28 Mar 2018) -- Start
            'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            pnlBottom.Visible = False
            'Sohail (28 Mar 2018) -- End
            'Sohail (11 Aug 2021) -- Start
            'NMB Enhancement :  : Don't show company code option when there is a connection or any other issue on login in self service.
            pnlMain.Visible = True
            'Sohail (11 Aug 2021) -- End
            HttpContext.Current.Session("mdbname") = "hrmsConfiguration"

            'objCONN = Nothing
            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            KillIdleSQLSessions()
            'S.SANDEEP |17-MAR-2020| -- END
            If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN = New SqlConnection
                objCONN.ConnectionString = constr
                objCONN.Open()
                HttpContext.Current.Session("gConn") = objCONN
            End If
            Dim intCompanyID As Integer = 0
            If Company._Object IsNot Nothing Then
                intCompanyID = Company._Object._Companyunkid
            End If
            Dim objMasterData As New clsMasterData
            Dim intThemeId As Integer = objMasterData.GetDefaultThemeId(intCompanyID, Nothing)

            'Sohail (28 Mar 2018) -- Start
            'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            'pnlBottom.Visible = False
            'Sohail (28 Mar 2018) -- End

            If intThemeId = enSelfServiceTheme.Blue Then
                HttpContext.Current.Session("Theme") = "Blue"
            ElseIf intThemeId = enSelfServiceTheme.Black Then
                HttpContext.Current.Session("Theme") = "Black"
            ElseIf intThemeId = enSelfServiceTheme.Green Then
                HttpContext.Current.Session("Theme") = "Green"
            ElseIf intThemeId = enSelfServiceTheme.Yellow Then
                HttpContext.Current.Session("Theme") = "Yellow"
                'Sohail (28 Mar 2018) -- Start
                'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            ElseIf intThemeId = CInt(enSelfServiceTheme.Brown) Then
                HttpContext.Current.Session("Theme") = "brown"

                'Dim objCompany As New clsCompany_Master
                'objCompany._Companyunkid = Company._Object._Companyunkid
                'Dim a As Drawing.Bitmap
                'If objCompany._Image IsNot Nothing Then
                '    a = objCompany._Image
                'Else
                '    a = New Drawing.Bitmap(16, 16, Drawing.Imaging.PixelFormat.Format24bppRgb)
                '    a.MakeTransparent()
                'End If
                'Dim ms As New System.IO.MemoryStream
                'a.Save(ms, Drawing.Imaging.ImageFormat.Png)
                'Dim byteImage() As Byte = ms.ToArray
                'Session("CompanyLogo") = Convert.ToBase64String(byteImage)
                'imgCompLogo.ImageUrl = "data:image/png;base64," + Session("CompanyLogo")
                'pnlBottom.Visible = True
                'Sohail (28 Mar 2018) -- End
            End If
            Page.Theme = Session("Theme")

            'Sohail (28 Mar 2018) -- Start
            'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            If objGroupMaster._Groupname.ToUpper.Trim = "AMANA BANK" Then
                'imgIBank.Visible = True
                pnlBottom.Visible = True
            Else
                'imgIBank.Visible = False
                pnlBottom.Visible = False
            End If
            'Sohail (28 Mar 2018) -- End
        Catch ex As Exception
            If ex.Message.ToLower.Contains("network") = True OrElse ex.Message.ToLower.Contains("database") = True OrElse ex.Message.ToLower.Contains("login") = True Then
                DisplayMessage.DisplayMessage(ex.Message, Me)
            Else
                DisplayMessage.DisplayError(ex.Message, Me)
            End If
        End Try

    End Sub
    'Varsha (10 Nov 2017)) -- End

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mdbname", mdbname)
        Me.ViewState.Add("mintOneCompanyUnkId", mintOneCompanyUnkId)

        'Pinkal (03-Apr-2017) -- Start
        'Enhancement - Working On Active directory Changes for PACRA.
        Me.ViewState.Add("AuthenticationMode", mintAuthenticationMode)
        'Pinkal (03-Apr-2017) -- End

    End Sub

    'Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    Try
    '        Session("servername") = System.Configuration.ConfigurationManager.AppSettings("servername")
    '        If Session("cur_theme") = "" Or Session("cur_theme") Is Nothing Then Session("cur_theme") = "blue"
    '        Me.Theme = Session("cur_theme")
    '        Session("themepath") = Session("servername") & "app_themes/" & Session("cur_theme")
    '        Session("Login") = False
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Error in Basepage->Page_PreInit Event", WebDisplayMessage.DisplayMessage.ShowStyle.Critical)
    '        Throw ex
    '    End Try
    'End Sub
#End Region
    'Sohail (02 May 2014) -- End

#Region " Methods & Functions "
    'Private Sub BindYear()
    '    Try
    '        'ds = clsDataOpr.WExecQuery("select yearunkid,financialyear_name from  hrmsConfiguration..cffinancial_year_tran join hrmsConfiguration..cfcompany_master on hrmsConfiguration..cffinancial_year_tran.companyunkid=hrmsConfiguration..cfcompany_master.companyunkid Where isactive  = 1 group by financialyear_name,yearunkid order by financialyear_name desc ", "cffinancial_year_tran")
    '        ds = clsDataOpr.WExecQuery("select financialyear_name from  hrmsConfiguration..cffinancial_year_tran join hrmsConfiguration..cfcompany_master on hrmsConfiguration..cffinancial_year_tran.companyunkid=hrmsConfiguration..cfcompany_master.companyunkid Where isactive  = 1 and hrmsConfiguration..cffinancial_year_tran.isclosed = 0   group by financialyear_name order by financialyear_name desc ", "cffinancial_year_tran")
    '        ddlyear.DataSource = ds.Tables(0)
    '        ddlyear.DataValueField = "financialyear_name"
    '        ddlyear.DataTextField = "financialyear_name"
    '        ddlyear.DataBind()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Error In BindYear", Me)
    '    End Try
    'End Sub

    Private Sub BindCompany(ByVal intUserID As Integer)
        Try
            Dim ds As DataSet

            'S.SANDEEP |07-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT {HARDCODE ON GROUP NAME}
            'msql = "select DISTINCT c.name as name, c.companyunkid  from  hrmsConfiguration..cffinancial_year_tran y " & vbCrLf
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            If objGroupMaster._Groupname.ToUpper.Trim <> "NMB PLC" Then
            msql = "select DISTINCT c.name as name, c.companyunkid  from  hrmsConfiguration..cffinancial_year_tran y " & vbCrLf
            Else
                msql = "select TOP 1 c.name as name, c.companyunkid  from  hrmsConfiguration..cffinancial_year_tran y " & vbCrLf
            End If
            'S.SANDEEP |07-MAR-2020| -- END

            msql &= " join hrmsConfiguration..cfcompany_master c on y.companyunkid=c.companyunkid  "
            'Sohail (02 May 2014) -- Start
            'Enhancement - Hide companies list if access not given to users.
            msql &= "JOIN hrmsConfiguration..cfcompanyaccess_privilege ON y.yearunkid = hrmsConfiguration..cfcompanyaccess_privilege.yearunkid "
            msql &= "Where  c.isactive = 1  and y.isclosed = 0 "
            If intUserID > 0 Then
                msql &= "AND hrmsConfiguration..cfcompanyaccess_privilege.userunkid = @userunkid "
                'Sohail (13 Dec 2018) - [" & intUserID & "] = [@userunkid]
            End If
            'Sohail (02 May 2014) -- End
            msql &= " ORDER BY c.companyunkid " 'Sohail (23 Dec 2013)
            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1                
            clsDataOpr.ClearParameters()
            If intUserID > 0 Then
                clsDataOpr.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            End If
            'Sohail (13 Dec 2018) -- End
            ds = clsDataOpr.WExecQuery(msql, "cfcompany_master")
            ddlCompany.DataSource = ds.Tables(0)
            ddlCompany.DataValueField = "companyunkid"
            ddlCompany.DataTextField = "name"
            ddlCompany.DataBind()
            If ddlCompany.Items.Count > 0 Then ddlCompany.SelectedIndex = 0

            ddlCompany_SelectedIndexChanged(ddlCompany, New System.EventArgs)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError("Error In BindCompany Method", Me)
            DisplayMessage.DisplayError("Error In BindCompany Method" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Private Sub BindEmployee()
    '    Try
    '        msql = "select Top 1 yearunkid,database_name,start_date,end_date from  hrmsConfiguration..cffinancial_year_tran where companyunkid  =  " & ddlCompany.SelectedValue & " Order By yearunkid Desc " & vbCrLf
    '        ds = clsDataOpr.WExecQuery(msql, "cffinancial_year_tran")
    '        If Not IsNothing(ds) Then
    '            mdbname = ds.Tables(0).Rows(0)("database_name").ToString
    '            objGlobalAccess.StartDate = ds.Tables(0).Rows(0)("start_date").ToString
    '            objGlobalAccess.EndDate = ds.Tables(0).Rows(0)("end_date").ToString
    '            Session("Fin_year") = ds.Tables(0).Rows(0)("yearunkid").ToString
    '            Session("fin_startdate") = ds.Tables(0).Rows(0)("start_date").ToString
    '            Session("fin_enddate") = ds.Tables(0).Rows(0)("end_date").ToString
    '            msql = " select employeeunkid,isnull(displayname,0) as displayname   from " & mdbname & "..hremployee_master order by displayname" & vbCrLf
    '            ds = clsDataOpr.WExecQuery(msql, "cffinancial_year_tran")
    '        End If

    '        ddlEmp.DataSource = ds.Tables(0)
    '        ddlEmp.DataValueField = "employeeunkid"
    '        ddlEmp.DataTextField = "displayname"
    '        ddlEmp.DataBind()
    '        ddlEmp.SelectedIndex = 0

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Error In BindEmployee Method", Me)
    '    End Try
    'End Sub
    'Private Sub BindUser()
    '    Try
    '        msql = "select Top 1 yearunkid,database_name,start_date,end_date,financialyear_name from  hrmsConfiguration..cffinancial_year_tran where companyunkid  =  " & ddlCompany.SelectedValue & " Order By yearunkid Desc " & vbCrLf
    '        ds = clsDataOpr.WExecQuery(msql, "cffinancial_year_tran")

    '        If Not IsNothing(ds) Then
    '            mdbname = ds.Tables(0).Rows(0)("database_name").ToString
    '            Session("Fin_year") = ds.Tables(0).Rows(0)("yearunkid").ToString
    '            Session("Fin_year_name") = ds.Tables(0).Rows(0)("financialyear_name").ToString
    '            objGlobalAccess.StartDate = ds.Tables(0).Rows(0)("start_date").ToString
    '            objGlobalAccess.EndDate = ds.Tables(0).Rows(0)("end_date").ToString
    'End If
    '        ds = clsDataOpr.WExecQuery("select Userunkid,username from hrmsConfiguration..cfuser_master", "cfuser_master")


    '        'ddlEmp.DataSource = ds.Tables(0)
    '        'ddlEmp.DataValueField = "Userunkid"
    '        'ddlEmp.DataTextField = "userName"

    '        'ddlEmp.DataBind()
    '        'ddlEmp.SelectedIndex = 0
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Error In BindUser Method ", Me)
    '    End Try
    'End Sub
    'Sohail (05 Jul 2011) -- Start

    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    'Private Sub GetCompanyYearInfo(ByVal strCompUnkId As Integer)


    '    msql = "SELECT  cfcompany_master.companyunkid " & _
    '                ", cfcompany_master.code AS CompCode " & _
    '                ", cfcompany_master.name AS CompName " & _
    '                ", cffinancial_year_tran.yearunkid " & _
    '                ", cffinancial_year_tran.financialyear_name " & _
    '                ", cffinancial_year_tran.database_name " & _
    '                ", cffinancial_year_tran.start_date " & _
    '                ", cffinancial_year_tran.end_date " & _
    '                ",hrmsConfiguration..cfcompany_master.countryunkid " & _
    '                "FROM    hrmsConfiguration..cfcompany_master " & _
    '                         "JOIN hrmsConfiguration..cffinancial_year_tran ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
    '                         "AND cfcompany_master.isactive = 1 " & _
    '                         "AND cffinancial_year_tran.isclosed = 0 " & _
    '                "WHERE cfcompany_master.companyunkid = " & strCompUnkId & " "

    '    mainds = clsDataOpr.WExecQuery(msql, "List")

    '    For Each dsRow As DataRow In mainds.Tables("List").Rows
    '        Session("CompCode") = dsRow.Item("CompCode")
    '        Session("CompName") = dsRow.Item("CompName")
    '        Session("Fin_year") = dsRow.Item("yearunkid")
    '        Session("FinancialYear_Name") = dsRow.Item("financialyear_name")
    '        Session("Database_Name") = dsRow.Item("database_name")
    '        Session("fin_startdate") = dsRow.Item("start_date")
    '        Session("fin_enddate") = dsRow.Item("end_date")

    '        'Pinkal (24-May-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("Compcountryid") = dsRow.Item("countryunkid")
    '        'Pinkal (24-May-2013) -- End

    '    Next

    'End Sub
    'Sohail (30 Mar 2015) -- End
    'Sohail (05 Jul 2011) -- End

    'Sohail (02 May 2014) -- Start
    'Enhancement - Hide companies list if access not given to users.
    'Private Function SetCompanySessions(ByVal intCompanyID As Integer, ByVal intLanguageID As Integer) As Boolean
    '    Try

    '        'S.SANDEEP [ 09 JUN 2014 ] -- START
    '        If Session("mdbname") IsNot Nothing Then
    '            Dim objMasterData As New clsMasterData
    '            objMasterData.Update_IDM_Active_Status(intCompanyID, Session("mdbname"))
    '            objMasterData = Nothing
    '        End If
    '        'S.SANDEEP [ 09 JUN 2014 ] -- END


    '        'TODO Change all global objects
    '        gobjConfigOptions = New clsConfigOptions
    '        gobjConfigOptions._Companyunkid = intCompanyID
    '        ConfigParameter._Object._Companyunkid = intCompanyID
    '        Company._Object._Companyunkid = intCompanyID

    '        'Sohail (30 Apr 2013) -- Start
    '        'TRA - Company and Employee Licence Check
    '        If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Company > ConfigParameter._Object._NoOfCompany Then
    '            DisplayMessage.DisplayMessage("Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance.", Me.Page)
    '            Exit Try
    '            'Sohail (23 Dec 2013) -- Start
    '            'Enhancement - Anrew's request
    '            'ElseIf ConfigParameter._Object._IsArutiDemo = True AndAlso ddlCompany.SelectedIndex <> 0 Then
    '            '    DisplayMessage.DisplayMessage("Sorry, You can use only first company in Aruti Demo. Please contact Aruti Support team for assistance.", Me.Page)
    '            '    Exit Sub
    '            'Sohail (23 Dec 2013) -- End
    '        End If
    '        'Company._Object._Total_Active_Employee_AsOnFromDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        'Company._Object._Total_Active_Employee_AsOnToDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        Company._Object._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Employee_ForAllCompany > ConfigParameter._Object._NoOfEmployees Then
    '            DisplayMessage.DisplayMessage("Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance.", Me.Page)
    '            Exit Try
    '        End If
    '        'Sohail (30 Apr 2013) -- End

    '        gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
    '        gobjLocalization._LangId = intLanguageID

    '        'Anjan [04 June 2014] -- Start
    '        'ENHANCEMENT : Implementing Language,requested by Andrew
    '        Call SetLanguage()
    '        'Anjan [04 June 2014] -- End

    '        Dim clsConfig As New clsConfigOptions
    '        clsConfig._Companyunkid = intCompanyID
    '        Session("CompanyUnkId") = intCompanyID
    '        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
    '        Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting
    '        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
    '        Session("SickSheetNotype") = clsConfig._SickSheetNotype
    '        Session("fmtCurrency") = clsConfig._CurrencyFormat
    '        Session("AllowEditAddress") = clsConfig._AllowEditAddress
    '        Session("AllowEditPersonalInfo") = clsConfig._AllowEditPersonalInfo
    '        Session("AllowEditEmergencyAddress") = clsConfig._AllowEditEmergencyAddress
    '        Session("LoanApplicationNoType") = clsConfig._LoanApplicationNoType
    '        Session("LoanApplicationPrifix") = clsConfig._LoanApplicationPrifix
    '        Session("NextLoanApplicationNo") = clsConfig._NextLoanApplicationNo
    '        Session("BatchPostingNotype") = clsConfig._BatchPostingNotype
    '        Session("BatchPostingPrifix") = clsConfig._BatchPostingPrifix
    '        Session("DoNotAllowOverDeductionForPayment") = clsConfig._DoNotAllowOverDeductionForPayment
    '        Session("PaymentVocNoType") = clsConfig._PaymentVocNoType
    '        Session("IsDenominationCompulsory") = clsConfig._IsDenominationCompulsory
    '        Session("SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
    '        Session("AssetDeclarationInstruction") = clsConfig._AssetDeclarationInstruction
    '        Session("Document_Path") = clsConfig._Document_Path
    '        Session("IgnoreZeroValueHeadsOnPayslip") = clsConfig._IgnoreZeroValueHeadsOnPayslip
    '        Session("ShowLoanBalanceOnPayslip") = clsConfig._ShowLoanBalanceOnPayslip
    '        Session("ShowSavingBalanceOnPayslip") = clsConfig._ShowSavingBalanceOnPayslip
    '        Session("ShowEmployerContributionOnPayslip") = clsConfig._ShowEmployerContributionOnPayslip
    '        Session("ShowAllHeadsOnPayslip") = clsConfig._ShowAllHeadsOnPayslip
    '        Session("LogoOnPayslip") = clsConfig._LogoOnPayslip
    '        Session("PayslipTemplate") = clsConfig._PayslipTemplate
    '        Session("LeaveTypeOnPayslip") = clsConfig._LeaveTypeOnPayslip
    '        Session("ShowCumulativeAccrual") = clsConfig._ShowCumulativeAccrualOnPayslip
    '        Session("Base_CurrencyId") = clsConfig._Base_CurrencyId
    '        Session("ShowCategoryOnPayslip") = clsConfig._ShowCategoryOnPayslip
    '        Session("ShowInformationalHeadsOnPayslip") = clsConfig._ShowInformationalHeadsOnPayslip
    '        Session("PaymentRoundingType") = clsConfig._PaymentRoundingType
    '        Session("PaymentRoundingMultiple") = clsConfig._PaymentRoundingMultiple
    '        'Sohail (17 Dec 2014) -- Start
    '        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
    '        Session("CFRoundingAbove") = clsConfig._CFRoundingAbove
    '        'Sohail (17 Dec 2014) -- End
    '        Session("ShowBirthDateOnPayslip") = clsConfig._ShowBirthDateOnPayslip
    '        Session("ShowAgeOnPayslip") = clsConfig._ShowAgeOnPayslip
    '        Session("ShowNoOfDependantsOnPayslip") = clsConfig._ShowNoOfDependantsOnPayslip
    '        Session("ShowMonthlySalaryOnPayslip") = clsConfig._ShowMonthlySalaryOnPayslip
    '        Session("OT1HourHeadID") = clsConfig._OT1HourHeadId
    '        Session("OT2HourHeadID") = clsConfig._OT2HourHeadId
    '        Session("OT3HourHeadID") = clsConfig._OT3HourHeadId
    '        Session("OT4HourHeadID") = clsConfig._OT4HourHeadId
    '        Session("OT1HourHeadName") = clsConfig._OT1HourHeadName
    '        Session("OT2HourHeadName") = clsConfig._OT2HourHeadName
    '        Session("OT3HourHeadName") = clsConfig._OT3HourHeadName
    '        Session("OT4HourHeadName") = clsConfig._OT4HourHeadName
    '        Session("OT1AmountHeadID") = clsConfig._OT1AmountHeadId
    '        Session("OT2AmountHeadID") = clsConfig._OT2AmountHeadId
    '        Session("OT3AmountHeadID") = clsConfig._OT3AmountHeadId
    '        Session("OT4AmountHeadID") = clsConfig._OT4AmountHeadId
    '        Session("OT1AmountHeadName") = clsConfig._OT1AmountHeadName
    '        Session("OT2AmountHeadName") = clsConfig._OT2AmountHeadName
    '        Session("OT3AmountHeadName") = clsConfig._OT3AmountHeadName
    '        Session("OT4AmountHeadName") = clsConfig._OT4AmountHeadName
    '        Session("ShowSalaryOnHoldOnPayslip") = clsConfig._ShowSalaryOnHoldOnPayslip
    '        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
    '            Session("ArutiSelfServiceURL") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
    '        Else
    '            Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
    '        End If

    '        Session("AllowEditExperience") = clsConfig._AllowEditExperience
    '        Session("AllowDeleteExperience") = clsConfig._AllowDeleteExperience
    '        Session("AllowAddSkills") = clsConfig._AllowAddSkills
    '        Session("AllowEditSkills") = clsConfig._AllowEditSkills
    '        Session("AllowDeleteSkills") = clsConfig._AllowDeleteSkills
    '        Session("AllowAddDependants") = clsConfig._AllowAddDependants
    '        Session("AllowDeleteDependants") = clsConfig._AllowDeleteDependants
    '        Session("IsDependant_AgeLimit_Set") = clsConfig._IsDependant_AgeLimit_Set
    '        Session("AllowEditDependants") = clsConfig._AllowEditDependants
    '        Session("AllowAddIdentity") = clsConfig._AllowAddIdentity
    '        Session("AllowDeleteIdentity") = clsConfig._AllowDeleteIdentity
    '        Session("AllowEditIdentity") = clsConfig._AllowEditIdentity
    '        Session("AllowAddMembership") = clsConfig._AllowAddMembership
    '        Session("AllowDeleteMembership") = clsConfig._AllowDeleteMembership
    '        Session("AllowEditMembership") = clsConfig._AllowEditMembership
    '        Session("AllowAddQualifications") = clsConfig._AllowAddQualifications
    '        Session("AllowDeleteQualifications") = clsConfig._AllowDeleteQualifications
    '        Session("AllowEditQualifications") = clsConfig._AllowEditQualifications
    '        Session("AllowAddReference") = clsConfig._AllowAddReference
    '        Session("AllowDeleteReference") = clsConfig._AllowDeleteReference
    '        Session("AllowEditReference") = clsConfig._AllowEditReference
    '        Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
    '        Session("IsBSCObjectiveSaved") = clsConfig._IsBSCObjectiveSaved
    '        Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
    '        Session("AllowAddExperience") = clsConfig._AllowAddExperience

    '        Session("IsAllocation_Hierarchy_Set") = clsConfig._IsAllocation_Hierarchy_Set
    '        Session("Allocation_Hierarchy") = clsConfig._Allocation_Hierarchy

    '        Session("AllowChangeCompanyEmail") = clsConfig._AllowChangeCompanyEmail
    '        Session("AllowToViewPersonalSalaryCalculationReport") = clsConfig._AllowToViewPersonalSalaryCalculationReport
    '        Session("AllowToViewEDDetailReport") = clsConfig._AllowToViewEDDetailReport

    '        Session("SickSheetNotype") = clsConfig._SickSheetNotype
    '        Session("LeaveApproverForLeaveType") = clsConfig._IsLeaveApprover_ForLeaveType
    '        Session("LeaveFormNoType") = clsConfig._LeaveFormNoType


    '        Session("CompanyDomain") = clsConfig._CompanyDomain.ToString.Trim

    '        Session("Notify_EmplData") = clsConfig._Notify_EmplData
    '        Session("Notify_Allocation") = clsConfig._Notify_Allocation
    '        Session("DatafileExportPath") = clsConfig._DatafileExportPath
    '        Session("DatafileName") = clsConfig._DatafileName
    '        Session("Accounting_TransactionReference") = clsConfig._Accounting_TransactionReference
    '        Session("Accounting_JournalType") = clsConfig._Accounting_JournalType
    '        Session("Accounting_JVGroupCode") = clsConfig._Accounting_JVGroupCode

    '        If clsConfig._LeaveBalanceSetting <= 0 Then
    '            Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
    '        Else
    '            Session("LeaveBalanceSetting") = clsConfig._LeaveBalanceSetting
    '        End If

    '        Session("AllowToviewPaysliponEss") = clsConfig._AllowToviewPaysliponEss
    '        Session("ViewPayslipDaysBefore") = clsConfig._ViewPayslipDaysBefore

    '        Session("IsImgInDataBase") = clsConfig._IsImgInDataBase

    '        Session("AllowToAddEditImageForESS") = clsConfig._AllowToAddEditImageForESS

    '        Session("ShowFirstAppointmentDate") = clsConfig._ShowFirstAppointmentDate

    '        Session("BatchPostingNotype") = clsConfig._BatchPostingNotype
    '        Session("NotifyPayroll_Users") = clsConfig._Notify_Payroll_Users
    '        Session("EFTIntegration") = clsConfig._EFTIntegration
    '        Session("DatafileName") = clsConfig._DatafileName
    '        Session("AccountingSoftWare") = clsConfig._AccountingSoftWare
    '        Session("MobileMoneyEFTIntegration") = clsConfig._MobileMoneyEFTIntegration 'Sohail (08 Dec 2014)

    '        Session("_LoanVocNoType") = clsConfig._LoanVocNoType
    '        Session("_PaymentVocNoType") = clsConfig._PaymentVocNoType
    '        Session("_DoNotAllowOverDeductionForPayment") = clsConfig._DoNotAllowOverDeductionForPayment
    '        Session("_IsDenominationCompulsory") = clsConfig._IsDenominationCompulsory
    '        Session("_SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
    '        Session("SavingsVocNoType") = clsConfig._SavingsVocNoType
    '        Session("AllowAssessor_Before_Emp") = clsConfig._AllowAssessor_Before_Emp
    '        Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
    '        Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
    '        Session("PolicyManagementTNA") = clsConfig._PolicyManagementTNA

    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes
    '        Session("IsAutomaticIssueOnFinalApproval") = clsConfig._IsAutomaticIssueOnFinalApproval
    '        'Pinkal (01-Feb-2014) -- End

    '        'Sohail (28 May 2014) -- Start
    '        'Enhancement - Staff Requisition.
    '        Session("StaffReqFormNoType") = clsConfig._StaffReqFormNoType
    '        Session("ApplyStaffRequisition") = clsConfig._ApplyStaffRequisition
    '        'Sohail (28 May 2014) -- End

    '        'Pinkal (23-Sep-2014) -- Start
    '        'Enhancement - [FINCA UGANDA]  IF THIS SETTIGN IS FALSE THEN IT WILL ALLOW TO APPLY LEAVE WHETHER PERIOD IS CLOSED. 
    '        Session("ClosePayrollPeriodIfLeaveIssue") = clsConfig._ClosePayrollPeriodIfLeaveIssue
    '        'Pinkal (23-Sep-2014) -- End


    '        'Shani [ 04 SEP 2014 ] -- START
    '        Session("PaymentApprovalwithLeaveApproval") = clsConfig._PaymentApprovalwithLeaveApproval
    '        Session("ClaimRequestVocNoType") = ConfigParameter._Object._ClaimRequestVocNoType
    '        'Shani [ 04 SEP 2014 ] -- END

    '        'Shani [ 17 SEP 2014 ] -- START
    '        'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    '        Session("SickSheetTemplate") = clsConfig._SickSheetTemplate
    '        Session("SickSheetAllocationId") = clsConfig._SickSheetAllocationId
    '        'Shani [ 17 SEP 2014 ] -- END


    '        'Shani [ 22 OCT 2014 ] -- START
    '        'Implementing "Custom Payroll Report" under Web As One of the Report Type
    '        Session("CustomPayrollReportAllocIds") = clsConfig._CustomPayrollReportAllocIds
    '        Session("CustomPayrollReportHeadsIds") = clsConfig._CustomPayrollReportHeadsIds
    '        'Shani [ 22 OCT 2014 ] -- END

    '        'Pinkal (03-Nov-2014) -- Start
    '        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    '        Session("Notify_IssuedLeave_Users") = clsConfig._Notify_IssuedLeave_Users
    '        'Pinkal (03-Nov-2014) -- End

    '        'Shani [ 31 OCT 2014 ] -- START
    '        'Enhancement-Implementing New Perfromance Module in Web. (Allocate By SandeepSir)
    '        Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
    '        Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
    '        Session("CascadingTypeId") = clsConfig._CascadingTypeId
    '        Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
    '        Session("ScoringOptionId") = clsConfig._ScoringOptionId
    '        Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
    '        Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
    '        'Shani [ 31 OCT 2014 ] -- END

    '        'S.SANDEEP [29 JAN 2015] -- START
    '        Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
    '        'S.SANDEEP [29 JAN 2015] -- END
    '        msql = " select employeeunkid,(isnull(Firstname,'') + ' ' + isnull(othername,' ') + ' ' + isnull(surname ,' '))  as loginname from " & Session("mdbname") & "..hremployee_master "

    '        'If rtbEmployee.Checked = True Then
    '        '    Dim sName As String = String.Empty
    '        '    sName = txtloginname.Text.Trim
    '        '    If sName.Contains("'") Then
    '        '        sName = sName.Replace("'", "''")
    '        '    End If
    '        '    msql &= "   where displayname = '" & sName & "' " & vbCrLf

    '        'Else
    '        '    Dim objUser As New clsUserAddEdit
    '        '    Dim mintUserUnkid As Integer
    '        '    clsuser = Session("clsuser")
    '        '    mintUserUnkid = objUser.IsValidLogin(txtloginname.Text, clsuser.password, enLoginMode.USER, "hrmsConfiguration")
    '        '    If mintUserUnkid <= 0 AndAlso objUser._Message <> "" Then
    '        '        If objUser._ShowChangePasswdfrm = True AndAlso objUser._RetUserId > 0 Then
    '        '            Session("ExUId") = objUser._RetUserId
    '        '            radYesNo.Show()
    '        '            Exit Function
    '        '        Else
    '        '            DisplayMessage.DisplayMessage(objUser._Message, Me)
    '        '            Exit Function
    '        '        End If
    '        '    End If
    '        '    objUser = Nothing

    '        'End If

    '        If Session("LoginBy") = Global.User.en_loginby.Employee Then

    '    Dim sName As String = String.Empty
    '    sName = txtloginname.Text.Trim
    '    If sName.Contains("'") Then
    '        sName = sName.Replace("'", "''")
    '    End If
    '    msql &= "   where displayname = '" & sName & "' " & vbCrLf

    '        ElseIf Session("LoginBy") = Global.User.en_loginby.User Then

    '    Dim objUser As New clsUserAddEdit
    '    Dim mintUserUnkid As Integer
    '    clsuser = Session("clsuser")
    '    mintUserUnkid = objUser.IsValidLogin(txtloginname.Text, clsuser.password, enLoginMode.USER, "hrmsConfiguration")
    '    If mintUserUnkid <= 0 AndAlso objUser._Message <> "" Then
    '        If objUser._ShowChangePasswdfrm = True AndAlso objUser._RetUserId > 0 Then
    '            Session("ExUId") = objUser._RetUserId
    '            radYesNo.Show()
    '            Exit Function
    '        Else
    '            DisplayMessage.DisplayMessage(objUser._Message, Me)
    '            Exit Function
    '        End If
    '    End If
    '    objUser = Nothing

    'End If

    '        msql &= " order by loginname " & vbCrLf
    '        ds = clsDataOpr.WExecQuery(msql, "cffinancial_year_tran")

    '        objGlobalAccess.ListOfEmployee = ds.Tables(0)

    '        Dim objCompany As New clsCompany_Master
    '        objCompany._Companyunkid = intCompanyID
    '        Session("gstrCompanyName") = objCompany._Name

    '        'Dim objPsWd As New clsPassowdOptions
    '        'Session("IsEmployeeAsUser") = objPsWd._IsEmployeeAsUser

    '        Session("CompanyBankGroupId") = objCompany._Bankgroupunkid
    '        Session("CompanyBankBranchId") = objCompany._Branchunkid



    '        Dim objMaster As New clsMasterData


    '        Session("UserAccessLevel") = objMaster.GetUserAccessLevel(CInt(Session("UserId")), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("UserAccessModeSetting"))
    '        If Session("LoginBy") = Global.User.en_loginby.Employee Then
    '            Session("UserAccessLevel") = ""
    '        End If

    '        If Session("UserAccessLevel") = "" Then
    '            Session("AccessLevelFilterString") = " AND 1 = 1  "

    '            'Sohail (20 May 2014) -- Start
    '            'Enhancement - Provide Analysis By on Reports
    '            Session("AccessLevelBranchFilterString") = " "
    '            Session("AccessLevelDepartmentGroupFilterString") = " "
    '            Session("AccessLevelDepartmentFilterString") = " "
    '            Session("AccessLevelSectionGroupFilterString") = " "
    '            Session("AccessLevelSectionFilterString") = " "
    '            Session("AccessLevelUnitGroupFilterString") = " "
    '            Session("AccessLevelUnitFilterString") = " "
    '            Session("AccessLevelTeamFilterString") = " "
    '            Session("AccessLevelJobGroupFilterString") = " "
    '            Session("AccessLevelJobFilterString") = " "
    '            Session("AccessLevelClassGroupFilterString") = " "
    '            Session("AccessLevelClassFilterString") = " "
    '            'Sohail (20 May 2014) -- End
    '        Else
    '            Session("AccessLevelFilterString") = UserAccessLevel._AccessLevelFilterString

    '            'Sohail (20 May 2014) -- Start
    '            'Enhancement - Provide Analysis By on Reports
    '            Session("AccessLevelBranchFilterString") = UserAccessLevel._AccessLevelBranchFilterString
    '            Session("AccessLevelDepartmentGroupFilterString") = UserAccessLevel._AccessLevelDepartmentGroupFilterString
    '            Session("AccessLevelDepartmentFilterString") = UserAccessLevel._AccessLevelDepartmentFilterString
    '            Session("AccessLevelSectionGroupFilterString") = UserAccessLevel._AccessLevelSectionGroupFilterString
    '            Session("AccessLevelSectionFilterString") = UserAccessLevel._AccessLevelSectionFilterString
    '            Session("AccessLevelUnitGroupFilterString") = UserAccessLevel._AccessLevelUnitGroupFilterString
    '            Session("AccessLevelUnitFilterString") = UserAccessLevel._AccessLevelUnitFilterString
    '            Session("AccessLevelTeamFilterString") = UserAccessLevel._AccessLevelTeamFilterString
    '            Session("AccessLevelJobGroupFilterString") = UserAccessLevel._AccessLevelJobGroupFilterString
    '            Session("AccessLevelJobFilterString") = UserAccessLevel._AccessLevelJobFilterString
    '            Session("AccessLevelClassGroupFilterString") = UserAccessLevel._AccessLevelClassGroupFilterString
    '            Session("AccessLevelClassFilterString") = UserAccessLevel._AccessLevelClassFilterString
    '            'Sohail (20 May 2014) -- End
    '        End If


    '        If rtbUser.Checked = True Then
    '            objGlobalAccess.userid = clsuser.UserID

    '        Else
    '            objGlobalAccess.employeeid = clsuser.Employeeunkid

    '        End If
    '        'Session("clsuser") = clsuser
    '        Session("objGlobalAccess") = objGlobalAccess





    '        Try
    '            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
    '                'Session("IP_ADD") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR")).AddressList(0).ToString
    '                'Session("HOST_NAME") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR").ToString).HostName.ToString()
    '                Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
    '                Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
    '            Else
    '                Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
    '                Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
    '            End If

    '        Catch ex As Exception
    '            Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
    '            Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
    '        End Try


    '        If Session("LoginBy") = Global.User.en_loginby.Employee Then
    '            SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
    '        ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
    '            SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
    '        End If





    '        If Request.QueryString.Count > 0 Then
    '            If Request.QueryString("setup").ToLower = "database" Then
    '                Dim ClsComCode As New CommonCodes
    '                ClsComCode.PkgDtaStrCompany()
    '            End If
    '        End If


    '        'If ConfigParameter._Object._IsExpire Then
    '        '    DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
    '        '    Exit Try
    '        'Else
    '        '    If ConfigParameter._Object._IsArutiDemo = True Then 'IsDemo
    '        '        DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '        '    Else 'IsLicence
    '        '        Response.Redirect("~/UserHome.aspx", False)
    '        '    End If
    '        'End If


    '        Return True

    '    Catch ex As Exception

    '    End Try
    'End Function
    'Sohail (02 May 2014) -- End

    'S.SANDEEP [ 22 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Function isValidData() As Boolean
        'Issue: This has been removed now due IDAMS (Oracle), so it has to be allowed -> andrew muga.
        'Dim mRegx As System.Text.RegularExpressions.Regex
        ''Anjan (21 Dec 2012)-Start
        ''ENHANCEMENT : TRA COMMENTS on Andrew's Request
        ''mRegx = New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9 ]*$")
        ''If mRegx.IsMatch(txtloginname.Text.Trim) = False Then
        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\%'*'+?\\<>:].*$")
        'If mRegx.IsMatch(txtloginname.Text.Trim) = True Then
        '    'Anjan (21 Dec 2012)-Start
        '    DisplayMessage.DisplayMessage("Special characters not allowed in login.", Me)
        '    Return False
        'End If
        Return True
    End Function

    Private Function Passwd_Change(ByVal intUId As Integer, ByVal objU As clsUserAddEdit, ByRef intVal As Integer) As Boolean
        Try
            objU._Userunkid = intUId
            If objU._Password <> txtPwd.Text Then
                DisplayMessage.DisplayMessage("Invalid Username or Password!! Please try again !!!", Me)
                Return False
            Else
                'S.SANDEEP [ 06 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Session("ExUId") = intUId
                'S.SANDEEP [ 06 DEC 2012 ] -- END
                intVal = 1
            End If

            Dim objPwdOpt As New clsPassowdOptions


            If objPwdOpt._IsPasswdPolicySet Then
                If objPwdOpt._IsPasswordLenghtSet Then
                    If txtPwd.Text.Length < objPwdOpt._PasswordLength Then
                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                        Return True
                    End If
                End If
                Dim mRegx As System.Text.RegularExpressions.Regex
                If objPwdOpt._IsUpperCase_Mandatory Then
                    mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                    If mRegx.IsMatch(txtPwd.Text) = False Then
                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                        Return True
                    End If
                End If
                If objPwdOpt._IsLowerCase_Mandatory Then
                    mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                    If mRegx.IsMatch(txtPwd.Text) = False Then
                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                        Return True
                    End If
                End If
                If objPwdOpt._IsNumeric_Mandatory Then
                    mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                    If mRegx.IsMatch(txtPwd.Text) = False Then
                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                        Return True
                    End If
                End If
                If objPwdOpt._IsSpecalChars_Mandatory Then
                    'S.SANDEEP [ 07 MAY 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                    mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                    'S.SANDEEP [ 07 MAY 2013 ] -- END
                    If mRegx.IsMatch(txtPwd.Text) = False Then
                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                        Return True
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Passwd_Change : " & ex.Message.ToString, Me)
        End Try
    End Function
    'S.SANDEEP [ 22 NOV 2012 ] -- END

    'Sohail (27 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    '<WebMethod()> Public Overloads Shared Function CheckLicence(ByVal strModuleName As String) As Boolean
    '    If ConfigParameter._Object._IsArutiDemo = True Then 'IsDemo
    '        If ConfigParameter._Object._IsExpire = False Then
    '            Return True
    '            'Else
    '            'Return False
    '        End If
    '        'If ArtLic._Object.DaysLeft <= -1 Then
    '        '    Return False
    '        'End If
    '        Return True
    '    End If

    '    Select Case strModuleName.Trim.ToUpper

    '        Case "PAYROLL_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
    '                Return False
    '            End If

    '        Case "LOAN_AND_SAVINGS_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
    '                Return False
    '            End If

    '        Case "EMPLOYEE_BIODATA_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
    '                Return False
    '            End If

    '        Case "LEAVE_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
    '                Return False
    '            End If

    '        Case "TIME_AND_ATTENDANCE_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
    '                Return False
    '            End If

    '        Case "EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
    '                Return False
    '            End If

    '        Case "TRAININGS_NEEDS_ANALYSIS"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) = False Then
    '                Return False
    '            End If

    '        Case "ON_JOB_TRAINING_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
    '                Return False
    '            End If

    '        Case "RECRUITMENT_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
    '                Return False
    '            End If

    '        Case "ONLINE_JOB_APPLICATIONS_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Online_Job_Applications_Management) = False Then
    '                Return False
    '            End If

    '        Case "EMPLOYEE_DISCIPLINARY_CASES_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
    '                Return False
    '            End If

    '        Case "MEDICAL_BILLS_AND_CLAIMS_MANAGEMENT"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
    '                Return False
    '            End If

    '        Case "EMPLOYEE_SELF_SERVICE"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Self_Service) = False Then
    '                Return False
    '            End If

    '        Case "MANAGER_SELF_SERVICE"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False Then
    '                Return False
    '            End If

    '        Case "EMPLOYEE_ASSETS_DECLARATIONS"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
    '                Return False
    '            End If

    '        Case "CUSTOM_REPORT_ENGINE"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Custom_Report_Engine) = False Then
    '                Return False
    '            End If

    '        Case "INTEGRATION_ACCOUNTING_SYSTEM_ERPS"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Accounting_System_ERPs) = False Then
    '                Return False
    '            End If

    '        Case "INTEGRATION_ELECTRONIC_BANKING_INTERFACE"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Electronic_Banking_Interface) = False Then
    '                Return False
    '            End If

    '        Case "Integration_Time_Attendance_Devices"
    '            If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices) = False Then
    '                Return False
    '            End If




    '        Case Else
    '            Return False
    '    End Select

    '    Return True

    'End Function
    'Sohail (27 Apr 2013) -- End



    'Pinkal (12-Sep-2017) -- Start
    'Enhancement - Solving AD User Login Problem..

    Private Sub CheckLoginADAuthentication(ByVal xUserLoginModeID As Integer, ByRef strError As String)
        Try
            If xUserLoginModeID <> enAuthenticationMode.BASIC_AUTHENTICATION Then

                Dim objConfig As New clsConfigOptions
                objConfig.IsValue_Changed("ADIPAddress", "-999", Session("ADIPAddress"))
                objConfig.IsValue_Changed("ADDomain", "-999", Session("ADDomain"))
                objConfig.IsValue_Changed("ADDomainUser", "-999", Session("ADDomainUser"))
                objConfig.IsValue_Changed("ADDomainUserPwd", "-999", Session("ADDomainUserPwd"))

                If Session("ADDomainUserPwd").Trim.Length > 0 Then
                    Session("ADDomainUserPwd") = clsSecurity.Decrypt(Session("ADDomainUserPwd").ToString().Trim, "ezee")
                Else
                    Session("ADDomainUserPwd") = ""
                End If
                objConfig = Nothing


                'Pinkal (20-APR-2018) -- Start
                'Bug - [0002194] Login issue, noticed a strange behavior on self service, Users are able to login with wrong password. Our manager found out this. 
                If IsAuthenticated(strError, Session("ADIPAddress").ToString().Trim, Session("ADDomain").ToString().Trim, txtloginname.Text.Trim, txtPwd.Text.Trim) = False Then
                    'S.SANDEEP [21-NOV-2018] -- START
                    'DisplayMessage.DisplayMessage(strError, Me)
                    If strError.Contains("The server is not operational.") Then
                        DisplayMessage.DisplayMessage("Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.", Me)
                    Else
                    DisplayMessage.DisplayMessage(strError, Me)
                    End If
                    'S.SANDEEP [21-NOV-2018] -- END
                    Exit Sub
                End If
                'Pinkal (20-APR-2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("CheckLoginADAuthentication:- " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (12-Sep-2017) -- End


#End Region

#Region " Button's Events "
    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlogin.Click
        Dim arrGroupName() As String = {"AAM RESOURCES", "HUMAN RESOURCE MANAGEMENT OFFICE", "CROWN PAINTS ( K) LTD", "ZAMBIA REVENUE AUTHORITY"} 'Put group name in captial letters only


        Try

            gobjConfigOptions = New clsConfigOptions

            ArtLic._Object = New ArutiLic(False)
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            ArtLic._Object.HotelName = objGroupMaster._Groupname

            If ConfigParameter._Object._IsArutiDemo Then 'Check if Licence is not activated (If it is Demo)


                '*** checking for kenya AAM Resources
                'If Today.Date > eZeeDate.convertDate(acore32.core.HD) AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = True Then
                '    Exit Sub
                'End If

                'If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then


                '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
                'S.SANDEEP |24-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
                'If eZeeDate.convertDate(Today.Date) > acore32.core.HD Then
                '    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page)
                '    Exit Sub
                'End If
                If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then
                If eZeeDate.convertDate(Today.Date) > acore32.core.HD Then
                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page)
                    Exit Sub
                End If
            End If
                'S.SANDEEP |24-NOV-2020| -- END
                '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END

                
            End If


            'Pinkal (12-Feb-2015) -- Start
            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

            'If rtbEmployee.Checked = True Then
            '    If CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
            '        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
            '        Exit Sub
            '    End If
            'ElseIf rtbUser.Checked = True Then
            'If CheckLicence("MANAGER_SELF_SERVICE") = False Then
            '        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
            '        Exit Sub
            '    End If
            'End If

            'Pinkal (12-Feb-2015) -- End


            If isValidData() = False Then
                Exit Sub
            End If

            HttpContext.Current.Session("Login") = False

            Dim intCompanyId As Integer = 0


            'Pinkal (12-Feb-2015) -- Start
            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

            'If rtbEmployee.Checked = True Then

            If mintOneCompanyUnkId > 0 Then
                intCompanyId = mintOneCompanyUnkId
            Else


                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                'msql = "SELECT  companyunkid " & _
                '       "FROM    hrmsConfiguration..cfcompany_master " & _
                '       "WHERE   isactive = 1 " & _
                '        "AND code = '" & txtCompanyCode.Text & "' "

                msql = "SELECT  companyunkid " & _
                       "FROM    hrmsConfiguration..cfcompany_master " & _
                       "WHERE   isactive = 1 " & _
                        "AND code =@code"

                clsDataOpr.ClearParameters()
                clsDataOpr.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, txtCompanyCode.Text.Trim)
                'Pinkal (03-Apr-2017) -- End

                ds = clsDataOpr.ExecQuery(msql, "company")

                If ds.Tables(0).Rows.Count > 0 Then
                    intCompanyId = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

                Else
                    DisplayMessage.DisplayMessage("Sorry, Invalid Company Code", Me)
                    txtCompanyCode.Focus()
                    Exit Sub

                End If
            End If

            Dim strError As String = ""
            If GetCompanyYearInfo(strError, intCompanyId) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Sub
            End If
            mdbname = Session("Database_Name")
            HttpContext.Current.Session("mdbname") = mdbname
            HttpContext.Current.Session("CompanyUnkId") = intCompanyId
            'Sohail (15 May 2018) -- Start
            'Internal issue - Object refrence not set error when user password expired and clicked on Yes button on are you sure popup in 72.1.
            Session("DateFormat") = "dd-MMM-yyyy"
            Session("DateSeparator") = "-"
            'Sohail (15 May 2018) -- End

            'objCONN = Nothing
            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            KillIdleSQLSessions()
            'S.SANDEEP |17-MAR-2020| -- END
            If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN = New SqlConnection
                objCONN.ConnectionString = constr
                objCONN.Open()
                HttpContext.Current.Session("gConn") = objCONN
            End If

            'End If

            'Pinkal (12-Feb-2015) -- End

            Dim objPwdOpt As New clsPassowdOptions
            Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser



            Dim clsUser As New User
            'Pinkal (31-Jan-2020) -- Start
            'Enhancements -  problem in self service login when username having pre or post space.
            'clsUser = New User(txtloginname.Text, txtPwd.Text, Session("mdbname"))
            clsUser = New User(txtloginname.Text.Trim(), txtPwd.Text, Session("mdbname"))
            'Pinkal (31-Jan-2020) -- End

            If clsUser.UserID = -1 AndAlso clsUser.Employeeunkid = -1 Then
                DisplayMessage.DisplayMessage(clsUser._Message, Me)
                Session("sessionexpired") = False
                Exit Sub
            End If

            Dim iUsrId As Integer = clsUser.UserID : Dim objUsr As New clsUserAddEdit : Dim iValue As Integer = -1
            If iUsrId > 0 Then

                If Session("IsEmployeeAsUser") = True AndAlso clsUser.LoginBy = Global.User.en_loginby.Employee Then
                    'Pinkal (12-Feb-2015) -- End
                    objUsr._Userunkid = iUsrId
                    If intCompanyId <> objUsr._EmployeeCompanyUnkid Then
                        DisplayMessage.DisplayMessage("Sorry, No such employee exists in the selected company.", Me)
                        Exit Sub
                    End If

                ElseIf Session("IsEmployeeAsUser") = True AndAlso clsUser.LoginBy = Global.User.en_loginby.User Then
                    'Pinkal (12-Feb-2015) -- End
                    'Sohail [16 May 2014] -- Start
                    'ENHANCEMENT : New login flow
                    'If clsUser.IsAccessGiven(iUsrId, Session("Fin_year")) = False Then
                    '    DisplayMessage.DisplayMessage("Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator.", Me)
                    '    Exit Sub
                    'End If
                    'Sohail [16 May 2014] -- End
                End If


                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.

                If mintAuthenticationMode = enAuthenticationMode.BASIC_AUTHENTICATION Then

                If objPwdOpt._IsUsrNameLengthSet = True Then
                    If txtloginname.Text.Trim.Length < objPwdOpt._UsrNameLength Then
                        DisplayMessage.DisplayMessage("Username does not match with the policy set for username. Please change your username.", Me)
                        ModalPopupExtender3.Show()
                        Exit Sub
                    End If
                    End If  'objPwdOpt._IsUsrNameLengthSet

                If objPwdOpt._IsPasswordExpiredSet = True Then
                    If Passwd_Change(iUsrId, objUsr, iValue) = True Then
                        txtUsername.Text = txtloginname.Text
                        ModalPopupExtender2.Show()
                        Exit Sub
                    Else
                        If iValue <= 0 Then
                            Exit Sub
                        End If
                    End If
                    End If 'objPwdOpt._IsPasswordExpiredSet

                If objPwdOpt._IsPasswdPolicySet = True Then

                    If objPwdOpt._IsPasswordLenghtSet Then
                        If txtPwd.Text.Length < objPwdOpt._PasswordLength Then
                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                            Session("ExUId") = iUsrId
                            txtUsername.Text = txtloginname.Text
                            ModalPopupExtender2.Show()
                            Exit Sub
                        End If
                        End If 'objPwdOpt._IsPasswordLenghtSet

                    If objPwdOpt._IsPasswdPolicySet Then

                        Dim mRegx As System.Text.RegularExpressions.Regex
                        If objPwdOpt._IsUpperCase_Mandatory Then
                            mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                            If mRegx.IsMatch(txtPwd.Text) = False Then
                                DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                                Session("ExUId") = iUsrId
                                txtUsername.Text = txtloginname.Text
                                ModalPopupExtender2.Show()
                                Exit Sub
                            End If
                            End If 'objPwdOpt._IsUpperCase_Mandatory

                        If objPwdOpt._IsLowerCase_Mandatory Then
                            mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                            If mRegx.IsMatch(txtPwd.Text) = False Then
                                DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                                Session("ExUId") = iUsrId
                                txtUsername.Text = txtloginname.Text
                                ModalPopupExtender2.Show()
                                Exit Sub
                            End If
                            End If 'objPwdOpt._IsLowerCase_Mandatory

                        If objPwdOpt._IsNumeric_Mandatory Then
                            mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                            If mRegx.IsMatch(txtPwd.Text) = False Then
                                DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                                Session("ExUId") = iUsrId
                                txtUsername.Text = txtloginname.Text
                                ModalPopupExtender2.Show()
                                Exit Sub
                            End If
                            End If 'objPwdOpt._IsNumeric_Mandatory

                        If objPwdOpt._IsSpecalChars_Mandatory Then
                            mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                            If mRegx.IsMatch(txtPwd.Text) = False Then
                                DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
                                Session("ExUId") = iUsrId
                                txtUsername.Text = txtloginname.Text
                                ModalPopupExtender2.Show()
                                Exit Sub
                            End If

                            End If 'objPwdOpt._IsSpecalChars_Mandatory

                        End If 'objPwdOpt._IsPasswdPolicySet

                    End If  'objPwdOpt._IsPasswdPolicySet

                    'Pinkal (03-Apr-2017) -- Start
                    'Enhancement - Working On Active directory Changes for PACRA.

                ElseIf (objPwdOpt._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION OrElse objPwdOpt._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION) AndAlso iUsrId <> 1 Then

                    strError = ""

                    'Pinkal (12-Sep-2017) -- Start
                    'Enhancement - Solving AD User Login Problem.
                    CheckLoginADAuthentication(objPwdOpt._UserLogingModeId, strError)

                    'Pinkal (20-APR-2018) -- Start
                    'Bug - [0002194] Login issue, noticed a strange behavior on self service, Users are able to login with wrong password. Our manager found out this. 
                    If strError <> "" Then

                        'S.SANDEEP [21-NOV-2018] -- START
                        'DisplayMessage.DisplayMessage("CheckLoginADAuthentication :- " & strError, Me, "index.aspx")
                        If strError.Contains("The server is not operational.") Then
                            DisplayMessage.DisplayMessage("Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.", Me, "index.aspx")
                        Else
                        DisplayMessage.DisplayMessage("CheckLoginADAuthentication :- " & strError, Me, "index.aspx")
                        End If
                        'S.SANDEEP [21-NOV-2018] -- END
                        Exit Sub
                    End If
                    'Pinkal (20-APR-2018) -- End

                    'Pinkal (12-Sep-2017) -- End

                    'Pinkal (03-Apr-2017) -- End

                End If  'CInt(Session("AuthenticationMode")) = enAuthenticationMode.BASIC_AUTHENTICATION


                'Pinkal (12-Sep-2017) -- Start
                'Enhancement - Solving AD User Login Problem..
            ElseIf clsUser.UserID <= 0 AndAlso clsUser.LoginBy = Global.User.en_loginby.Employee AndAlso clsUser.Employeeunkid > 0 _
                AndAlso (objPwdOpt._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION OrElse objPwdOpt._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION) Then

                strError = ""
                CheckLoginADAuthentication(objPwdOpt._UserLogingModeId, strError)

                'Pinkal (20-APR-2018) -- Start
                'Bug - [0002194] Login issue, noticed a strange behavior on self service, Users are able to login with wrong password. Our manager found out this. 
                If strError <> "" Then

                    'S.SANDEEP [21-NOV-2018] -- START
                    'DisplayMessage.DisplayMessage("CheckLoginADAuthentication :- " & strError, Me, "index.aspx")
                    If strError.Contains("The server is not operational.") Then
                        DisplayMessage.DisplayMessage("Sorry, Invalid Username or Password. Please Provide correct Username or Password to login.", Me, "index.aspx")
                    Else
                    DisplayMessage.DisplayMessage("CheckLoginADAuthentication :- " & strError, Me, "index.aspx")
                    End If
                    'S.SANDEEP [21-NOV-2018] -- END
                    Exit Sub
                End If
                'Pinkal (20-APR-2018) -- End

                'Pinkal (12-Sep-2017) -- End

            End If  'iUsrId > 0

            'Pinkal (03-Apr-2017) -- End

            HttpContext.Current.Session("LoginBy") = clsUser.LoginBy
            Call GetDatabaseVersion()

            If Session("LoginBy") = Global.User.en_loginby.User Then

                HttpContext.Current.Session("UserId") = HttpContext.Current.Session("U_UserID")
                HttpContext.Current.Session("Employeeunkid") = HttpContext.Current.Session("U_Employeeunkid")
                HttpContext.Current.Session("UserName") = HttpContext.Current.Session("U_UserName")
                HttpContext.Current.Session("Password") = HttpContext.Current.Session("U_Password")
                HttpContext.Current.Session("LeaveBalances") = HttpContext.Current.Session("U_LeaveBalances")
                HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("U_MemberName")
                HttpContext.Current.Session("RoleID") = HttpContext.Current.Session("U_RoleID")
                HttpContext.Current.Session("LangId") = HttpContext.Current.Session("U_LangId")
                HttpContext.Current.Session("Firstname") = HttpContext.Current.Session("U_Firstname")
                HttpContext.Current.Session("Surname") = HttpContext.Current.Session("U_Surname")
                HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("U_DisplayName")
                'Sohail (24 Nov 2016) -- Start
                'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                HttpContext.Current.Session("Theme_id") = Session("U_Theme_id")
                HttpContext.Current.Session("Lastview_id") = Session("U_Lastview_id")
                'Sohail (24 Nov 2016) -- End

                If CheckLicence("MANAGER_SELF_SERVICE") = False Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
                    Exit Sub
                End If

            ElseIf Session("LoginBy") = Global.User.en_loginby.Employee Then

                HttpContext.Current.Session("UserId") = HttpContext.Current.Session("E_UserID")
                HttpContext.Current.Session("Employeeunkid") = HttpContext.Current.Session("E_Employeeunkid")
                HttpContext.Current.Session("UserName") = HttpContext.Current.Session("E_UserName")
                HttpContext.Current.Session("Password") = HttpContext.Current.Session("E_Password")
                HttpContext.Current.Session("LeaveBalances") = HttpContext.Current.Session("E_LeaveBalances")
                HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("E_MemberName")
                HttpContext.Current.Session("RoleID") = HttpContext.Current.Session("E_RoleID")
                HttpContext.Current.Session("LangId") = 1 ' by default set custom1 for employee login for all. 
                HttpContext.Current.Session("Firstname") = HttpContext.Current.Session("E_Firstname")
                HttpContext.Current.Session("Surname") = HttpContext.Current.Session("E_Surname")
                HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("E_DisplayName")
                'Sohail (24 Nov 2016) -- Start
                'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                HttpContext.Current.Session("Theme_id") = Session("E_Theme_id")
                HttpContext.Current.Session("Lastview_id") = Session("E_Lastview_id")
                'Sohail (24 Nov 2016) -- End

                If CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
                    Exit Sub
                End If

            End If

            HttpContext.Current.Session("clsuser") = clsUser

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            If CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Black Then
                HttpContext.Current.Session("Theme") = "black"
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Green Then
                HttpContext.Current.Session("Theme") = "green"
                'Sohail (28 Mar 2018) -- Start
                'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = CInt(enSelfServiceTheme.Yellow) Then
                HttpContext.Current.Session("Theme") = "yellow"
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = CInt(enSelfServiceTheme.Brown) Then
                HttpContext.Current.Session("Theme") = "brown"
                'Sohail (28 Mar 2018) -- End
            Else
                HttpContext.Current.Session("Theme") = "blue"
            End If

            Session("menuView") = CInt(HttpContext.Current.Session("Lastview_id"))
            'Sohail (24 Nov 2016) -- End


            Dim blnPasswordExpired As Boolean = False
            strError = ""
            If SetUserSessions(strError, blnPasswordExpired) = False Then
                If strError.Trim <> "" Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Try
                End If
                If blnPasswordExpired = True Then
                    radYesNo.Show()
                    Exit Try
                End If
            End If



            '*** checking for kenya AAM Resources
            'If ConfigParameter._Object._IsExpire AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = True Then
            '    Exit Sub
            'End If

            Call SetLanguage()



            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
            'S.SANDEEP |24-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
            'If ConfigParameter._Object._IsExpire Then
            '    DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
            '    Exit Try
            'Else
            '    If ConfigParameter._Object._IsArutiDemo = True AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'IsDemo
            '        DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '        'Pinkal (11-Sep-2020) -- Start
            '        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            '        strError = ""
            '        If SetCompanySessions(strError, intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If
            '        'Pinkal (11-Sep-2020) -- End

            '    Else 'IsLicence
            '        strError = ""
            '        If SetCompanySessions(strError, intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If
            '        Response.Redirect("~/UserHome.aspx", False)
            '    End If
            'End If
            If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then
            If ConfigParameter._Object._IsExpire Then
                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
                Exit Try
            Else
                If ConfigParameter._Object._IsArutiDemo = True AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'IsDemo
                    DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    strError = ""
                    If SetCompanySessions(strError, intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Pinkal (11-Sep-2020) -- End

                Else 'IsLicence
            strError = ""
            If SetCompanySessions(strError, intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Sub
            End If
                    Response.Redirect("~/UserHome.aspx", False)
                End If
            End If
            Else
                If SetCompanySessions(strError, intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Sub
                End If
                Response.Redirect("~/UserHome.aspx", False)
            End If
            'S.SANDEEP |24-NOV-2020| -- END
            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END

            


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'strError = ""
            'If SetCompanySessions(strError, intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
            '    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '    Exit Sub
            'End If
            'Pinkal (11-Sep-2020) -- End

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("setup").ToLower = "database" Then
                    Dim ClsComCode As New CommonCodes
                    ClsComCode.PkgDtaStrCompany()
                End If
            End If


        Catch ex As Exception
            'Throw ex
            DisplayMessage.DisplayError("Error in BtnLogin_click event : " & ex.Message.ToString, Me)
        Finally
        End Try

    End Sub

    'Sohail (02 May 2014) -- Start
    'Enhancement - Hide companies list if access not given to users.
    'Sohail (17 Dec 2018) -- Start
    'NMB Enhancement - Security issues in Self Service
    'Protected Sub btnOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpen.Click
    '    Try
    '        objCONN = Nothing
    '        If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
    '            Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '            Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
    '            constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
    '            constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
    '            objCONN = New SqlConnection
    '            objCONN.ConnectionString = constr
    '            objCONN.Open()
    '            HttpContext.Current.Session("gConn") = objCONN
    '        End If

    '        Dim strError As String = ""
    '        If SetCompanySessions(strError, CInt(ddlCompany.SelectedValue), CInt(HttpContext.Current.Session("LangId"))) = False Then
    '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
    '            Exit Sub
    '        End If

    '        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Error in btnOpen_Click event" & ex.Message.ToString, Me)
    '    End Try
    'End Sub
    'Sohail (17 Dec 2018) -- End
    'Sohail (02 May 2014) -- End


    'S.SANDEEP |18-OCT-2021| -- START
    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    '    Dim intCompanyId As Integer = 0

    '    'Pinkal (02-Jan-2015) -- Start
    '    'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
    '    Dim objSend As New clsSendMail
    '    'Pinkal (02-Jan-2015) -- End
    '    Try

    '        'Sohail (02 May 2014) -- Start
    '        'Enhancement - Hide companies list if access not given to users.
    '        'Sohail [16 May 2014] -- Start
    '        'ENHANCEMENT : New login flow
    '        'msql = "SELECT  companyunkid " & _
    '        '   "FROM    hrmsConfiguration..cfcompany_master " & _
    '        '   "WHERE   isactive = 1 " & _
    '        '           "AND code = '" & txtForgetCompanyCode.Text & "' "
    '        If mintOneCompanyUnkId > 0 Then
    '            intCompanyId = mintOneCompanyUnkId
    '        Else

    '            msql = "SELECT  companyunkid " & _
    '                    "FROM    hrmsConfiguration..cfcompany_master " & _
    '               "WHERE   isactive = 1 " & _
    '                       "AND code = @code "
    '            'Sohail (13 Dec 2018) - ['" & txtForgetCompanyCode.Text & "'] = [@code]

    '            'Sohail (13 Dec 2018) -- Start
    '            'NMB Enhancement : Security issues in Self Service in 75.1                
    '            clsDataOpr.ClearParameters()
    '            clsDataOpr.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, txtForgetCompanyCode.Text)
    '            'Sohail (13 Dec 2018) -- End
    '            ds = clsDataOpr.ExecQuery(msql, "company")

    '            If ds.Tables(0).Rows.Count > 0 Then
    '                intCompanyId = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

    '            Else
    '                DisplayMessage.DisplayMessage("Sorry, Invalid Company Code", Me)
    '                txtForgetCompanyCode.Focus()
    '                Exit Sub

    '            End If
    '        End If
    '        'Sohail [16 May 2014] -- End

    '        'If ds.Tables(0).Rows.Count > 0 Then
    '        'intCompanyId = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

    '        'Sohail (30 Mar 2015) -- Start
    '        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '        'GetCompanyYearInfo(intCompanyId)
    '        Dim strError As String = ""
    '        If GetCompanyYearInfo(strError, intCompanyId) = False Then
    '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
    '            Exit Sub
    '        End If
    '        'Sohail (30 Mar 2015) -- End
    '        mdbname = Session("Database_Name")
    '        HttpContext.Current.Session("mdbname") = mdbname

    '        'objCONN = Nothing
    '        'S.SANDEEP |17-MAR-2020| -- START
    '        'ISSUE/ENHANCEMENT : PM ERROR
    '        KillIdleSQLSessions()
    '        'S.SANDEEP |17-MAR-2020| -- END
    '        If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
    '        If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
    '            Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '            Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
    '            constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
    '            constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
    '            objCONN = New SqlConnection
    '            objCONN.ConnectionString = constr
    '            objCONN.Open()
    '            HttpContext.Current.Session("gConn") = objCONN
    '        End If
    '        'Else
    '        '    DisplayMessage.DisplayMessage("Sorry, Invalid Company Code", Me)
    '        '    txtForgetCompanyCode.Focus()
    '        '    Exit Sub
    '        'End If
    '        'Sohail (02 May 2014) -- End

    '        'Pinkal (02-Jan-2015) -- Start
    '        'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
    '        'Sohail (30 Mar 2015) -- Start
    '        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '        'SetCompanySessions(intCompanyId, 0)
    '        strError = ""
    '        If SetUserSessions(strError) = False Then
    '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
    '            Exit Sub
    '        End If

    '        strError = ""
    '        'Sohail (13 Sep 2019) -- Start
    '        'Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
    '        'If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
    '        If SetCompanySessions(strError, intCompanyId, 0) = False Then
    '            'Sohail (13 Sep 2019) -- End
    '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
    '            Exit Sub
    '        End If
    '        'Sohail (30 Mar 2015) -- End
    '        'Pinkal (02-Jan-2015) -- End

    '        ds = Nothing
    '        msql = ""
    '        msql = "SELECT employeeunkid FROM " & Session("mdbname") & "..hremployee_master WHERE employeecode = @employeecode"
    '        'Sohail (13 Dec 2018) - ['" & txtEmpCode.Text.Trim & "'] = [@employeecode]

    '        'Sohail (13 Dec 2018) -- Start
    '        'NMB Enhancement : Security issues in Self Service in 75.1                
    '        clsDataOpr.ClearParameters()
    '        clsDataOpr.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, txtEmpCode.Text.Trim)
    '        'Sohail (13 Dec 2018) -- End
    '        ds = clsDataOpr.WExecQuery(msql, "List")

    '        Dim strBody As New StringBuilder

    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Dim objEmp As New clsEmployee_Master

    '            'Shani(20-Nov-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'objEmp._Employeeunkid = CInt(ds.Tables(0).Rows(0)("employeeunkid"))
    '            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(ds.Tables(0).Rows(0)("employeeunkid"))
    '            'Shani(20-Nov-2015) -- End

    '            If objEmp._Email.Trim.Length > 0 Then
    '                Dim objCmp As New clsCompany_Master
    '                'Sohail (02 May 2014) -- Start
    '                'Enhancement - Hide companies list if access not given to users.
    '                'objCmp._Companyunkid = IIf(ddlCompany.SelectedValue = "", 0, ddlCompany.SelectedValue)
    '                objCmp._Companyunkid = intCompanyId
    '                'Sohail (02 May 2014) -- End
    '                If objCmp._Mailserverip.ToString.Trim.Length > 0 Then
    '                    Dim objMail As New System.Net.Mail.MailMessage(objCmp._Senderaddress, objEmp._Email)

    '                    strBody.Append("<HTML><BODY>")
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    strBody.Append("Dear <b>" & objEmp._Firstname & " " & objEmp._Surname & "</b></span></p>")
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You recently requested for Retrieval of your Aruti Self Service login password.Please find details below :</span></p>")
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px; display:block;'>")
    '                    strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;Your Username is <b>" & objEmp._Displayname & "</b></span></p>")
    '                    'strBody.Append(vbCrLf)
    '                    strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px; display:block;'>")
    '                    strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;Your Password is <b>" & objEmp._Password & "</b></span></p>")
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px; display:block;'>")
    '                    strBody.Append("<b>&nbsp;&nbsp;&nbsp;&nbsp;For and On behalf of : " & objCmp._Name & "</b></span></p>")
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                    strBody.Append("</span></p>")
    '                    strBody.Append(vbCrLf)
    '                    strBody.Append("</BODY></HTML>")


    '                    objMail.Body = strBody.ToString
    '                    objMail.Subject = "Your Password for given employee code : '" & objEmp._Employeecode & "'"
    '                    objMail.IsBodyHtml = True
    '                    Dim SmtpMail As New System.Net.Mail.SmtpClient()
    '                    SmtpMail.Host = objCmp._Mailserverip
    '                    SmtpMail.Port = objCmp._Mailserverport
    '                    If objCmp._Username.Trim <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(objCmp._Username, objCmp._Password)
    '                    SmtpMail.EnableSsl = objCmp._Isloginssl


    '                    'Pinkal (02-Jan-2015) -- Start
    '                    'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
    '                    objSend._Subject = objMail.Subject
    '                    objSend._ToEmail = objEmp._Email
    '                    objSend._Form_Name = Me.Page.Title

    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    objSend._LogEmployeeUnkid = objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate")))
    '                    'Shani(20-Nov-2015) -- End

    '                    objSend._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
    '                    objSend._UserUnkid = -1
    '                    objSend._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
    '                    objSend._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
    '                    objSend._Message = strBody.ToString()

    '                    'Pinkal (02-Jan-2015) -- End


    '                    SmtpMail.Send(objMail)

    '                    'Pinkal (02-Jan-2015) -- Start
    '                    'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
    '                    'Sohail (30 Nov 2017) -- Start
    '                    'SUMATRA Enhancement � SUMATRA � issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '                    'Call objSend.Insert_Email_Tran_Log()
    '                    Call objSend.Insert_Email_Tran_Log(objCmp._Senderaddress)
    '                    'Sohail (30 Nov 2017) -- End
    '                    'Pinkal (02-Jan-2015) -- End

    '                    DisplayMessage.DisplayMessage("Details were Sent to your E-Mail account registered with Aruti. Please check your E-Mail account", Me)
    '                    ModalPopupExtender1.Hide()
    '                Else
    '                    DisplayMessage.DisplayMessage("Sorry, We cannot send the password. Reason : No email account is configured for the selected company.", Me)
    '                End If
    '            Else
    '                DisplayMessage.DisplayMessage("Sorry, We cannot send the password. Reason : No email address is specified for this employee code.", Me)
    '            End If
    '        Else
    '            DisplayMessage.DisplayMessage("Sorry, Invalid Employee Code. Please enter valid Employee Code.", Me)
    '            ModalPopupExtender1.Show()
    '            txtEmpCode.Focus()
    '        End If
    '    Catch ex As Exception
    '        'Pinkal (02-Jan-2015) -- Start
    '        'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
    '        objSend.Insert_Email_Tran_Log(ex.Message)
    '        'Pinkal (02-Jan-2015) -- End
    '        If ex.Message.Contains("Fail") Then
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'DisplayMessage.DisplayError(ex.Message, Me)
    '            DisplayMessage.DisplayMessage("Please check your Internet settings.", Me)
    '            'Sohail (23 Mar 2019) -- End
    '        ElseIf ex.Message.Contains("secure") Then
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'DisplayMessage.DisplayError(ex.Message, Me)
    '            DisplayMessage.DisplayMessage("Please Uncheck Login using SSL setting.", Me)
    '            'Sohail (23 Mar 2019) -- End
    '        Else
    '            DisplayMessage.DisplayError(ex.Message, Me)
    '        End If
    '    Finally
    '        'txtEmpCode.Text = ""
    '    End Try
    'End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim intCompanyId As Integer = 0

        'Pinkal (02-Jan-2015) -- Start
        'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
        Dim objSend As New clsSendMail
        'Pinkal (02-Jan-2015) -- End
        Try

            'Sohail (02 May 2014) -- Start
            'Enhancement - Hide companies list if access not given to users.
            'Sohail [16 May 2014] -- Start
            'ENHANCEMENT : New login flow
            'msql = "SELECT  companyunkid " & _
            '   "FROM    hrmsConfiguration..cfcompany_master " & _
            '   "WHERE   isactive = 1 " & _
            '           "AND code = '" & txtForgetCompanyCode.Text & "' "
            If mintOneCompanyUnkId > 0 Then
                intCompanyId = mintOneCompanyUnkId
            Else

                msql = "SELECT  companyunkid " & _
                        "FROM    hrmsConfiguration..cfcompany_master " & _
                   "WHERE   isactive = 1 " & _
                           "AND code = @code "
                'Sohail (13 Dec 2018) - ['" & txtForgetCompanyCode.Text & "'] = [@code]

                'Sohail (13 Dec 2018) -- Start
                'NMB Enhancement : Security issues in Self Service in 75.1                
                clsDataOpr.ClearParameters()
                clsDataOpr.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, txtForgetCompanyCode.Text)
                'Sohail (13 Dec 2018) -- End
                ds = clsDataOpr.ExecQuery(msql, "company")

                If ds.Tables(0).Rows.Count > 0 Then
                    intCompanyId = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

                Else
                    DisplayMessage.DisplayMessage("Sorry, Invalid Company Code", Me)
                    txtForgetCompanyCode.Focus()
                    Exit Sub

                End If
            End If
            'Sohail [16 May 2014] -- End

            'If ds.Tables(0).Rows.Count > 0 Then
            'intCompanyId = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

            'Sohail (30 Mar 2015) -- Start
            'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
            'GetCompanyYearInfo(intCompanyId)
            Dim strError As String = ""
            If GetCompanyYearInfo(strError, intCompanyId) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Sub
            End If
            'Sohail (30 Mar 2015) -- End
            mdbname = Session("Database_Name")
            HttpContext.Current.Session("mdbname") = mdbname

            'objCONN = Nothing
            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            KillIdleSQLSessions()
            'S.SANDEEP |17-MAR-2020| -- END
            If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN = New SqlConnection
                objCONN.ConnectionString = constr
                objCONN.Open()
                HttpContext.Current.Session("gConn") = objCONN
            End If
            'Else
            '    DisplayMessage.DisplayMessage("Sorry, Invalid Company Code", Me)
            '    txtForgetCompanyCode.Focus()
            '    Exit Sub
            'End If
            'Sohail (02 May 2014) -- End

            'Pinkal (02-Jan-2015) -- Start
            'Enhancement - INCLUDING EMAIL TRAN LOG WHEN SENDING EMAIL IN WEB FOR FORGET PASSWORD LINK.
            'Sohail (30 Mar 2015) -- Start
            'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
            'SetCompanySessions(intCompanyId, 0)
            strError = ""
            If SetUserSessions(strError) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Sub
            End If

            strError = ""
            'Sohail (13 Sep 2019) -- Start
            'Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
            'If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
            If SetCompanySessions(strError, intCompanyId, 0) = False Then
                'Sohail (13 Sep 2019) -- End
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Sub
            End If
            'Sohail (30 Mar 2015) -- End
            'Pinkal (02-Jan-2015) -- End

            ds = Nothing
            msql = ""
            msql = "SELECT employeeunkid FROM " & Session("mdbname") & "..hremployee_master WHERE employeecode = @employeecode"
            'Sohail (13 Dec 2018) - ['" & txtEmpCode.Text.Trim & "'] = [@employeecode]

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1                
            clsDataOpr.ClearParameters()
            clsDataOpr.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, txtEmpCode.Text.Trim)
            'Sohail (13 Dec 2018) -- End
            ds = clsDataOpr.WExecQuery(msql, "List")

            Dim strBody As New StringBuilder

            If ds.Tables(0).Rows.Count > 0 Then
                Dim objEmp As New clsEmployee_Master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = CInt(ds.Tables(0).Rows(0)("employeeunkid"))
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(ds.Tables(0).Rows(0)("employeeunkid"))
                'Shani(20-Nov-2015) -- End

                If objEmp._Email.Trim.Length > 0 Then
                    Dim objCmp As New clsCompany_Master
                    'Sohail (02 May 2014) -- Start
                    'Enhancement - Hide companies list if access not given to users.
                    'objCmp._Companyunkid = IIf(ddlCompany.SelectedValue = "", 0, ddlCompany.SelectedValue)
                    objCmp._Companyunkid = intCompanyId
                    'Sohail (02 May 2014) -- End
                    If objCmp._Mailserverip.ToString.Trim.Length > 0 Then

                        Dim objPwdReset As New clsPasswordResetRequest
                        Dim uNum As Int64 = 0 : uNum = objPwdReset.GenerateUniqueCode()
                        Dim blnFlag As Boolean = False
                        Dim StrLink As String = ""
                        Dim iUser As Integer = 0
                        If Session("UserId") IsNot Nothing Then
                            iUser = Session("UserId")
                        End If
                        StrLink = Session("ArutiSelfServiceURL").ToString() & "/Others_Forms/ChangePassword.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & CInt(ds.Tables(0).Rows(0)("employeeunkid")).ToString & "|" & iUser.ToString & "|" & uNum.ToString))
                        Dim blnRFlag As Boolean = False
                        blnRFlag = objPwdReset.IsRequestExists(CInt(ds.Tables(0).Rows(0)("employeeunkid")))
                        If blnRFlag Then
                            DisplayMessage.DisplayMessage("", Me)
                            ModalPopupExtender1.Hide()
                            Exit Sub
                        End If
                        With objPwdReset
                            ._Changedatetime = Nothing
                            ._Companyunkid = intCompanyId
                            ._Employeeunkid = CInt(ds.Tables(0).Rows(0)("employeeunkid"))
                            ._FormName = mstrModuleName
                            If Session("IP_ADD") Is Nothing Then
                                SetIP_HOST()
                            End If
                            ._IP = CStr(Session("IP_ADD"))
                            ._Host = CStr(Session("HOST_NAME"))
                            ._Isaccessed = False
                            ._Ischanged = False
                            ._Requestdate = Now
                            ._Uniquecode = uNum
                            ._Userunkind = 0
                            ._Linkissued = StrLink
                            If .Insert() = False Then
                                Exit Sub
                            End If
                        End With

                        Dim objMail As New System.Net.Mail.MailMessage(objCmp._Senderaddress, objEmp._Email)

                        strBody.Append("<HTML><BODY>")
                        strBody.Append(vbCrLf)
                        strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        strBody.Append("Dear <b>" & objEmp._Firstname & " " & objEmp._Surname & "</b></span></p>")
                        strBody.Append(vbCrLf)
                        strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                        strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You recently requested for Retrieval of your Aruti Self Service login password.Please click below link in order to reset it.</span></p>")
                        strBody.Append(vbCrLf)

                        'If clsConfig._ArutiSelfServiceURL = "http://" & HttpContext.Current.Request.ApplicationPath Then
                        '    Session("ArutiSelfServiceURL") = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath
                        'Else
                        '    Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
                        'End If

                        strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px; display:block;'>")

                        'S.SANDEEP |18-OCT-2021| -- START
                        'strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;<b> Link: </b>" & Session("ArutiSelfServiceURL").ToString() & "/Others_Forms/ChangePassword.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId.ToString & "|" & CInt(ds.Tables(0).Rows(0)("employeeunkid")).ToString & "|" & iUser.ToString)) & "</span></p>")
                        strBody.Append("&nbsp;&nbsp;&nbsp;&nbsp;<b> Link: </b>" & StrLink & "</span></p>")
                        'S.SANDEEP |18-OCT-2021| -- END

                        strBody.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px; display:block;'>")
                        strBody.Append("<b>&nbsp;&nbsp;&nbsp;&nbsp;For and On behalf of : " & objCmp._Name & "</b></span></p>")
                        strBody.Append(vbCrLf)
                        strBody.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                        strBody.Append("</span></p>")
                        strBody.Append(vbCrLf)
                        strBody.Append("</BODY></HTML>")


                        objMail.Body = strBody.ToString
                        objMail.Subject = "Your Password for given employee code : '" & objEmp._Employeecode & "'"
                        objMail.IsBodyHtml = True
                        Dim SmtpMail As New System.Net.Mail.SmtpClient()
                        SmtpMail.Host = objCmp._Mailserverip
                        SmtpMail.Port = objCmp._Mailserverport
                        If objCmp._Username.Trim <> "" Then SmtpMail.Credentials = New System.Net.NetworkCredential(objCmp._Username, objCmp._Password)
                        SmtpMail.EnableSsl = objCmp._Isloginssl


                        objSend._Subject = objMail.Subject
                        objSend._ToEmail = objEmp._Email
                        objSend._FormName = Me.Page.Title

                        objSend._LoginEmployeeunkid = objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate")))

                        objSend._ClientIP = Session("IP_ADD").ToString()
                        objSend._HostName = Session("HOST_NAME").ToString()
                        objSend._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                        objSend._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        objSend._FromWeb = True
                        objSend._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
                        objSend._UserUnkid = -1
                        objSend._SenderAddress = objEmp._Firstname & " " & objEmp._Surname
                        objSend._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                        objSend._Message = strBody.ToString()

                        SmtpMail.Send(objMail)

                        Call objSend.Insert_Email_Tran_Log(objCmp._Senderaddress)

                        DisplayMessage.DisplayMessage("Details were Sent to your E-Mail account registered with Aruti. Please check your E-Mail account", Me)
                        ModalPopupExtender1.Hide()
                    Else
                        DisplayMessage.DisplayMessage("Sorry, We cannot send the password. Reason : No email account is configured for the selected company.", Me)
                    End If
                Else
                    DisplayMessage.DisplayMessage("Sorry, We cannot send the password. Reason : No email address is specified for this employee code.", Me)
                End If
            Else
                DisplayMessage.DisplayMessage("Sorry, Invalid Employee Code. Please enter valid Employee Code.", Me)
                ModalPopupExtender1.Show()
                txtEmpCode.Focus()
            End If
        Catch ex As Exception           
            objSend.Insert_Email_Tran_Log(ex.Message)
            If ex.Message.Contains("Fail") Then
                DisplayMessage.DisplayMessage("Please check your Internet settings.", Me)
            ElseIf ex.Message.Contains("secure") Then
                DisplayMessage.DisplayMessage("Please Uncheck Login using SSL setting.", Me)
            Else
                DisplayMessage.DisplayError("Procedure : btnSubmit_Click : " & ex.Message.ToString, Me)
            End If
        Finally        
        End Try
    End Sub
    'S.SANDEEP |18-OCT-2021| -- END

    Protected Sub btnChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = Session("ExUId")

            If txtOldPasswd.Text <> objUser._Password Then
                DisplayMessage.DisplayMessage("Incorrect Old password. Please enter your correct Old Password", Me)
                ModalPopupExtender2.Show()
                Exit Sub
            End If

            If txtNewPasswd.Text <> txtConfirmPasswd.Text Then
                DisplayMessage.DisplayMessage("Password does not match. Please enter correct password", Me)
                ModalPopupExtender2.Show()
                Exit Sub
            End If

            'Sohail (02 May 2014) -- Start
            'Enhancement - Hide companies list if access not given to users.
            'objCONN = Nothing
            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            KillIdleSQLSessions()
            'S.SANDEEP |17-MAR-2020| -- END
            If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN = New SqlConnection
                objCONN.ConnectionString = constr
                objCONN.Open()
                HttpContext.Current.Session("gConn") = objCONN
            End If
            'Sohail (02 May 2014) -- End

            Dim objPswd As New clsPassowdOptions
            If objPswd._IsPasswordLenghtSet Then
                If txtNewPasswd.Text.Trim.Length < objPswd._PasswordLength Then
                    DisplayMessage.DisplayMessage("Password length cannot be less than " & objPswd._PasswordLength & " character(s)", Me)
                    ModalPopupExtender2.Show()
                    Exit Sub
                End If
            End If

            Dim mRegx As System.Text.RegularExpressions.Regex
            If txtNewPasswd.Text.Trim.Length > 0 Then
                If objPswd._IsPasswdPolicySet Then
                    If objPswd._IsUpperCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
                        If mRegx.IsMatch(txtNewPasswd.Text.Trim) = False Then
                            DisplayMessage.DisplayMessage("Password must contain atleast one upper case character.", Me)
                            ModalPopupExtender2.Show()
                            Exit Sub
                        End If
                    End If
                    If objPswd._IsLowerCase_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
                        If mRegx.IsMatch(txtNewPasswd.Text.Trim) = False Then
                            DisplayMessage.DisplayMessage("Password must contain atleast one lower case character.", Me)
                            ModalPopupExtender2.Show()
                            Exit Sub
                        End If
                    End If
                    If objPswd._IsNumeric_Mandatory Then
                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
                        If mRegx.IsMatch(txtNewPasswd.Text.Trim) = False Then
                            DisplayMessage.DisplayMessage("Password must contain atleast one numeric digit.", Me)
                            ModalPopupExtender2.Show()
                            Exit Sub
                        End If
                    End If
                    If objPswd._IsSpecalChars_Mandatory Then
                        'S.SANDEEP [ 07 MAY 2013 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
                        'S.SANDEEP [ 07 MAY 2013 ] -- END
                        If mRegx.IsMatch(txtNewPasswd.Text.Trim) = False Then
                            DisplayMessage.DisplayMessage("Password must contain atleast one special character.", Me)
                            ModalPopupExtender2.Show()
                            Exit Sub
                        End If
                    End If
                End If
            End If
            If objPswd._IsPasswordExpiredSet Then
                If objUser._Exp_Days.ToString.Trim.Length = 8 Then
                    Dim dtExDate As Date = CDate(eZeeDate.convertDate(objUser._Exp_Days.ToString))
                    dtExDate = dtExDate.AddDays(objPswd._PasswordMaxLen)
                    objUser._Exp_Days = CInt(eZeeDate.convertDate(dtExDate))
                End If
            End If
            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If objPswd._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION AndAlso objPswd._IsPasswordHistory AndAlso (objPswd._PasswdLastUsedNumber > 0 OrElse objPswd._PasswdLastUsedDays > 0) Then
                Dim objPasswordHistory As New clsPasswordHistory
                Dim intTopCount As Integer
                Dim strFilter As String = String.Empty
                Dim strOrderBy As String = String.Empty
                If objPswd._PasswdLastUsedNumber > 0 Then
                    intTopCount = objPswd._PasswdLastUsedNumber
                End If

                If objPswd._PasswdLastUsedDays > 0 Then
                    Dim dtEndDate As Date = ConfigParameter._Object._CurrentDateAndTime.Date
                    Dim dtStartDate As Date = dtEndDate.AddDays(-1 * objPswd._PasswdLastUsedDays)
                    strFilter &= "AND Convert(char(8),transactiondate,112) >= '" & eZeeDate.convertDate(dtStartDate) & "' AND Convert(char(8),transactiondate,112) <= '" & eZeeDate.convertDate(dtEndDate) & "' "
                End If

                strOrderBy = " order by transactiondate desc "
                Dim dsList As DataSet = Nothing

                If objUser._EmployeeUnkid > 0 AndAlso (Session("IsEmployeeAsUser") = True OrElse Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    Dim intEmployeeId As Integer = CInt(Session("Employeeunkid"))
                    If Session("LoginBy") = Global.User.en_loginby.User Then
                        intEmployeeId = objUser._EmployeeUnkid
                    End If
                    dsList = objPasswordHistory.GetFilterDataList("List", -1, intEmployeeId, CInt(Session("CompanyUnkId")), intTopCount, strFilter, strOrderBy)
                ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                    dsList = objPasswordHistory.GetFilterDataList("List", CInt(Session("UserId")), -1, -1, intTopCount, strFilter, strOrderBy)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim drPassword() As DataRow = dsList.Tables(0).Select("new_password = '" & clsSecurity.Encrypt(txtNewPasswd.Text, "ezee").ToString & "' ")
                    If drPassword.Length > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, Entered New Password is already Used. Please Enter another Password!!!"), Me)
                        txtNewPasswd.Focus()
                        Exit Sub
                    End If
                End If
                objPasswordHistory = Nothing
            End If
            'Hemant (25 May 2021) -- End
            objUser._Password = txtConfirmPasswd.Text
            'Hemant (25 May 2021) -- Start
            'ENHANCEMENT : OLD-387 - Baylor - Password History Configuration
            If objUser._EmployeeUnkid > 0 AndAlso Session("IsEmployeeAsUser") = True Then
                objUser._IsSaveAsEmployee = True
            End If
            objUser._OldPassword = txtOldPasswd.Text
            objUser._CompanyUnkid = CInt(Session("CompanyUnkId"))
            objUser._ChangedUserunkid = objUser._Userunkid
            If Session("IP_ADD") Is Nothing Then
                SetIP_HOST()
            End If
            objUser._ClientIP = CStr(Session("IP_ADD"))
            objUser._Hostname = CStr(Session("HOST_NAME"))
            objUser._FromWeb = True
            'Hemant (25 May 2021) -- End
            If objUser.Update(True) = True Then
                'S.SANDEEP [ 14 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Session("IsEmployeeAsUser") = True Then
                    Dim mstrPasswd As String = clsSecurity.Encrypt(txtConfirmPasswd.Text, "ezee").ToString()
                    clsDataOpr.AddParameter("@Passwd", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPasswd)
                    clsDataOpr.ExecNonQuery("UPDATE " & Session("mdbname") & "..hremployee_master SET password = @Passwd WHERE employeeunkid = '" & objUser._EmployeeUnkid & "'")
                End If
                'S.SANDEEP [ 14 DEC 2012 ] -- END

                DisplayMessage.DisplayMessage("Password changed successfully. Please login with new password.", Me)
                ModalPopupExtender2.Hide()
            End If
            objPswd = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError("Procedure : btnChange_Click : " & ex.Message.ToString, Me)
        End Try
    End Sub

    Protected Sub btnUSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUSubmit.Click
        Try
            If txtOldUName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Old Username is mandatory information. Please provide Old Username.", Me)
                txtOldUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            If txtNewUName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("New Username is mandatory information. Please provide New Username.", Me)
                txtNewUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            If txtConfUName.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("New Username is mandatory information. Please provide New Username.", Me)
                txtConfUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            'Sohail (02 May 2014) -- Start
            'Enhancement - Hide companies list if access not given to users.
            'objCONN = Nothing
            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            KillIdleSQLSessions()
            'S.SANDEEP |17-MAR-2020| -- END
            If HttpContext.Current.Session("gConn") IsNot Nothing Then objCONN = HttpContext.Current.Session("gConn")
            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN = New SqlConnection
                objCONN.ConnectionString = constr
                objCONN.Open()
                HttpContext.Current.Session("gConn") = objCONN
            End If
            'Sohail (02 May 2014) -- End

            Dim mintUserId As Integer : Dim objUser As New clsUserAddEdit

            mintUserId = objUser.Return_UserId(txtOldUName.Text.Trim, "hrmsConfiguration", enLoginMode.USER)

            objUser._Userunkid = mintUserId

            If mintUserId > 0 Then
                If txtUPassword.Text <> objUser._Password Then
                    DisplayMessage.DisplayMessage("Password does not match. Please provide correct password.", Me)
                    txtUPassword.Focus() : ModalPopupExtender3.Show()
                    Exit Sub
                End If
            Else
                If txtUPassword.Text.Trim.Length <= 0 Then
                    DisplayMessage.DisplayMessage("Password is mandatory information. Please provide Password.", Me)
                    txtUPassword.Focus() : ModalPopupExtender3.Show()
                    Exit Sub
                End If
            End If

            If mintUserId <= 0 Then
                DisplayMessage.DisplayMessage("Old Username does not exist in the system. Please provide correct username.", Me)
                txtOldUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            Dim mRegx As System.Text.RegularExpressions.Regex
            mRegx = New System.Text.RegularExpressions.Regex("^[a-zA-Z0-9 ]*$")

            If mRegx.IsMatch(txtOldUName.Text.Trim) = False Then
                DisplayMessage.DisplayMessage("Special characters not allowed in username.", Me)
                txtOldUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            If mRegx.IsMatch(txtNewUName.Text.Trim) = False Then
                DisplayMessage.DisplayMessage("Special characters not allowed in username.", Me)
                txtNewUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            If mRegx.IsMatch(txtConfUName.Text.Trim) = False Then
                DisplayMessage.DisplayMessage("Special characters not allowed in username.", Me)
                txtConfUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            If txtUPassword.Text <> objUser._Password Then
                DisplayMessage.DisplayMessage("Password does not match. Please provide correct password.", Me)
                txtUPassword.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            Dim objPwd As New clsPassowdOptions
            If objPwd._IsUsrNameLengthSet = True Then
                If txtNewUName.Text.Trim.Length < objPwd._UsrNameLength Then
                    Dim SMessage As String = "Username cannot be less then " & objPwd._UsrNameLength & " character(s)."
                    DisplayMessage.DisplayMessage(SMessage, Me)
                    txtNewUName.Focus() : ModalPopupExtender3.Show()
                    Exit Sub
                End If
            End If

            If txtNewUName.Text <> txtConfUName.Text Then
                DisplayMessage.DisplayMessage("Username does not match. Please provide correct username.", Me)
                txtConfUName.Focus() : ModalPopupExtender3.Show()
                Exit Sub
            End If

            objUser._Userunkid = mintUserId
            objUser._Username = txtConfUName.Text.Trim

            If objUser.Update(True) = True Then
                'S.SANDEEP [ 14 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If Session("IsEmployeeAsUser") = True Then
                    clsuser.UpdateEmpData(mintUserId)
                End If
                'S.SANDEEP [ 14 DEC 2012 ] -- END

                DisplayMessage.DisplayMessage("Username changed successfully. Please login with new username.", Me)
                ModalPopupExtender3.Hide()
            Else
                DisplayMessage.DisplayMessage(objUser._Message, Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("btnUSubmit_Click : " & ex.Message, Me)
        End Try
    End Sub



#Region "    btnLogin_Click b4 Allowing user to switch from MSS to ESS and vice versa if he is imported Employee as User "
    'Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlogin.Click
    '    Dim arrGroupName() As String = {"AAM RESOURCES"}

    '    Try

    '        gobjConfigOptions = New clsConfigOptions

    '        ArtLic._Object = New ArutiLic(False)
    '        Dim objGroupMaster As New clsGroup_Master
    '        objGroupMaster._Groupunkid = 1
    '        ArtLic._Object.HotelName = objGroupMaster._Groupname

    '        If ConfigParameter._Object._IsArutiDemo Then 'Check if Licence is not activated (If it is Demo)


    '            'Anjan [28 November 2014] -- Start
    '            'ENHANCEMENT : checking for kenya AAM Resources
    '            If Today.Date > eZeeDate.convertDate(acore32.core.HD) AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = True Then
    '                Exit Sub
    '            End If
    '            'Anjan [28 November 2014] -- End

    '            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
    '                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page)
    '                Exit Sub
    '            End If
    '        End If


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    '        'If rtbEmployee.Checked = True Then
    '        '    If CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
    '        '        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
    '        '        Exit Sub
    '        '    End If
    '        'ElseIf rtbUser.Checked = True Then
    '        'If CheckLicence("MANAGER_SELF_SERVICE") = False Then
    '        '        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
    '        '        Exit Sub
    '        '    End If
    '        'End If

    '        'Pinkal (12-Feb-2015) -- End


    '        If isValidData() = False Then
    '            Exit Sub
    '        End If

    '        Session("Login") = False

    '        Dim intCompanyId As Integer = 0


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    '        'If rtbEmployee.Checked = True Then

    '        If mintOneCompanyUnkId > 0 Then
    '            intCompanyId = mintOneCompanyUnkId
    '        Else

    '            msql = "SELECT  companyunkid " & _
    '                   "FROM    hrmsConfiguration..cfcompany_master " & _
    '                   "WHERE   isactive = 1 " & _
    '                    "AND code = '" & txtCompanyCode.Text & "' "

    '            ds = clsDataOpr.ExecQuery(msql, "company")

    '            If ds.Tables(0).Rows.Count > 0 Then
    '                intCompanyId = CInt(ds.Tables(0).Rows(0).Item("companyunkid"))

    '            Else
    '                DisplayMessage.DisplayMessage("Sorry, Invalid Company Code", Me)
    '                txtCompanyCode.Focus()
    '                Exit Sub

    '            End If
    '        End If

    '        GetCompanyYearInfo(intCompanyId)
    '        mdbname = Session("Database_Name")
    '        HttpContext.Current.Session("mdbname") = mdbname
    '        HttpContext.Current.Session("CompanyUnkId") = intCompanyId

    '        objCONN = Nothing
    '        If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
    '            Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '            Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
    '            constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
    '            constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
    '            objCONN = New SqlConnection
    '            objCONN.ConnectionString = constr
    '            objCONN.Open()
    '            HttpContext.Current.Session("gConn") = objCONN
    '        End If

    '        'End If

    '        'Pinkal (12-Feb-2015) -- End

    '        Dim objPwdOpt As New clsPassowdOptions
    '        Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser

    '        Dim clsUser As New User

    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '        'clsUser = New User(txtloginname.Text, txtPwd.Text, IIf(rtbEmployee.Checked, Global.User.en_loginby.Employee, Global.User.en_loginby.User), Session("mdbname"))
    '        clsUser = New User(txtloginname.Text, txtPwd.Text, Session("mdbname"))
    '        'Pinkal (12-Feb-2015) -- End



    '        If clsUser.UserID = -1 AndAlso clsUser.Employeeunkid = -1 Then
    '            DisplayMessage.DisplayMessage(clsUser._Message, Me)
    '            Session("sessionexpired") = False
    '            Exit Sub
    '        End If

    '        Dim iUsrId As Integer = clsUser.UserID : Dim objUsr As New clsUserAddEdit : Dim iValue As Integer = -1
    '        If iUsrId > 0 Then

    '            'Pinkal (12-Feb-2015) -- Start
    '            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '            'If Session("IsEmployeeAsUser") = True AndAlso rtbEmployee.Checked = True Then
    '            If Session("IsEmployeeAsUser") = True AndAlso clsUser.LoginBy = Global.User.en_loginby.Employee Then
    '                'Pinkal (12-Feb-2015) -- End
    '                objUsr._Userunkid = iUsrId
    '                If intCompanyId <> objUsr._EmployeeCompanyUnkid Then
    '                    DisplayMessage.DisplayMessage("Sorry, No such employee exists in the selected company.", Me)
    '                    Exit Sub
    '                End If

    '                'Pinkal (12-Feb-2015) -- Start
    '                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '                'ElseIf Session("IsEmployeeAsUser") = True AndAlso rtbUser.Checked = True Then
    '            ElseIf Session("IsEmployeeAsUser") = True AndAlso clsUser.LoginBy = Global.User.en_loginby.User Then
    '                'Pinkal (12-Feb-2015) -- End
    '                'Sohail [16 May 2014] -- Start
    '                'ENHANCEMENT : New login flow
    '                'If clsUser.IsAccessGiven(iUsrId, Session("Fin_year")) = False Then
    '                '    DisplayMessage.DisplayMessage("Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator.", Me)
    '                '    Exit Sub
    '                'End If
    '                'Sohail [16 May 2014] -- End
    '            End If
    '            'Dim objPwdOpt As New clsPassowdOptions
    '            If objPwdOpt._IsUsrNameLengthSet = True Then
    '                If txtloginname.Text.Trim.Length < objPwdOpt._UsrNameLength Then
    '                    DisplayMessage.DisplayMessage("Username does not match with the policy set for username. Please change your username.", Me)
    '                    ModalPopupExtender3.Show()
    '                    Exit Sub
    '                End If
    '            End If
    '            If objPwdOpt._IsPasswordExpiredSet = True Then
    '                If Passwd_Change(iUsrId, objUsr, iValue) = True Then
    '                    txtUsername.Text = txtloginname.Text
    '                    ModalPopupExtender2.Show()
    '                    Exit Sub
    '                Else
    '                    If iValue <= 0 Then
    '                        Exit Sub
    '                    End If
    '                End If
    '            End If
    '            If objPwdOpt._IsPasswdPolicySet = True Then
    '                If objPwdOpt._IsPasswordLenghtSet Then
    '                    If txtPwd.Text.Length < objPwdOpt._PasswordLength Then
    '                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                        Session("ExUId") = iUsrId
    '                        txtUsername.Text = txtloginname.Text
    '                        ModalPopupExtender2.Show()
    '                        Exit Sub
    '                    End If
    '                End If

    '                If objPwdOpt._IsPasswdPolicySet Then
    '                    Dim mRegx As System.Text.RegularExpressions.Regex
    '                    If objPwdOpt._IsUpperCase_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    If objPwdOpt._IsLowerCase_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    If objPwdOpt._IsNumeric_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    If objPwdOpt._IsSpecalChars_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        End If

    '        Session("LoginBy") = clsUser.LoginBy

    '        Session("sessionexpired") = False
    '        Session("Login") = True
    '        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/" 'Sohail (28 May 2014)
    '        'Sohail (20 Feb 2015) -- Start
    '        Call GetDatabaseVersion()
    '        'Sohail (20 Feb 2015) -- End

    '        Session("UserId") = clsUser.UserID
    '        Session("Employeeunkid") = clsUser.Employeeunkid
    '        Session("UserName") = clsUser.UserName
    '        Session("LeaveBalances") = clsUser.LeaveBalances
    '        Session("MemberName") = clsUser.MemberName
    '        Session("RoleID") = clsUser.RoleUnkID


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    '        'If rtbEmployee.Checked = True Then
    '        '    HttpContext.Current.Session("LangId") = 1  ' by default set custom1 for employee login for all. 
    '        '    Session("LoginBy") = Global.User.en_loginby.Employee
    '        'Else
    '        '    HttpContext.Current.Session("LangId") = clsUser.LanguageUnkid
    '        '    Session("LoginBy") = Global.User.en_loginby.User
    '        'End If

    '        If Session("LoginBy") = Global.User.en_loginby.Employee Then
    '            HttpContext.Current.Session("LangId") = 1  ' by default set custom1 for employee login for all. 
    '        ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
    '            HttpContext.Current.Session("LangId") = clsUser.LanguageUnkid
    '        End If

    '        'Pinkal (12-Feb-2015) -- End


    '        Session("Firstname") = clsUser.Firstname
    '        Session("Surname") = clsUser.Surname



    '        Dim objUserPrivilege As New clsUserPrivilege
    '        objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

    '        '*** Payroll
    '        Session("AllowGlobalPayment") = objUserPrivilege._AllowGlobalPayment
    '        Session("DeletePayment") = objUserPrivilege._DeletePayment
    '        Session("AllowToViewGlobalVoidPaymentList") = objUserPrivilege._AllowToViewGlobalVoidPaymentList
    '        Session("AllowToApprovePayment") = objUserPrivilege._AllowToApprovePayment
    '        Session("AllowToVoidApprovedPayment") = objUserPrivilege._AllowToVoidApprovedPayment
    '        Session("AllowToViewBatchPostingList") = objUserPrivilege._AllowToViewBatchPostingList
    '        Session("AllowToAddBatchPosting") = objUserPrivilege._AllowToAddBatchPosting
    '        Session("AllowToEditBatchPosting") = objUserPrivilege._AllowToEditBatchPosting
    '        Session("AllowToDeleteBatchPosting") = objUserPrivilege._AllowToDeleteBatchPosting
    '        Session("AllowToPostBatchPostingToED") = objUserPrivilege._AllowToPostBatchPostingToED
    '        Session("AddPayment") = objUserPrivilege._AddPayment
    '        Session("AllowToViewPaymentList") = objUserPrivilege._AllowToViewPaymentList
    '        Session("AddCashDenomination") = objUserPrivilege._AddCashDenomination
    '        Session("AllowToViewEmpEDList") = objUserPrivilege._AllowToViewEmpEDList
    '        Session("AddEarningDeduction") = objUserPrivilege._AddEarningDeduction
    '        Session("EditEarningDeduction") = objUserPrivilege._EditEarningDeduction

    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '        Session("DeleteEarningDeduction") = objUserPrivilege._DeleteEarningDeduction
    '        Session("AllowAuthorizePayslipPayment") = objUserPrivilege._AllowAuthorizePayslipPayment
    '        Session("AllowVoidAuthorizedPayslipPayment") = objUserPrivilege._AllowVoidAuthorizedPayslipPayment
    '        'Pinkal (12-Feb-2015) -- End

    '        Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
    '        Session("AllowToViewSalaryChangeList") = objUserPrivilege._AllowToViewSalaryChangeList
    '        Session("AllowToApproveSalaryChange") = objUserPrivilege._AllowToApproveSalaryChange

    '        '*** Loan & Advance
    '        Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
    '        Session("AllowtoApproveAdvance") = objUserPrivilege._AllowtoApproveAdvance
    '        Session("AddPendingLoan") = objUserPrivilege._AddPendingLoan
    '        Session("EditPendingLoan") = objUserPrivilege._EditPendingLoan
    '        Session("AllowToViewProcessLoanAdvanceList") = objUserPrivilege._AllowToViewProcessLoanAdvanceList
    '        Session("AllowToViewProceedingApprovalList") = objUserPrivilege._AllowToViewProceedingApprovalList
    '        Session("AllowToViewLoanAdvanceList") = objUserPrivilege._AllowToViewLoanAdvanceList
    '        Session("DeleteLoanAdvance") = objUserPrivilege._DeleteLoanAdvance
    '        Session("EditLoanAdvance") = objUserPrivilege._EditLoanAdvance
    '        Session("AllowChangeLoanAvanceStatus") = objUserPrivilege._AllowChangeLoanAvanceStatus
    '        Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
    '        Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
    '        Session("Is_Report_Assigned") = clsArutiReportClass.Is_Report_Assigned(enArutiReport.BBL_Loan_Report, Session("UserId"), Session("CompanyUnkId"))
    '        Session("DeletePendingLoan") = objUserPrivilege._DeletePendingLoan
    '        Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
    '        Session("AllowChangePendingLoanAdvanceStatus") = objUserPrivilege._AllowChangePendingLoanAdvanceStatus
    '        Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
    '        Session("EditLoanAdvancePayment") = objUserPrivilege._EditLoanAdvancePayment
    '        Session("DeleteLoanAdvancePayment") = objUserPrivilege._DeleteLoanAdvancePayment
    '        Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
    '        Session("EditLoanAdvanceReceived") = objUserPrivilege._EditLoanAdvanceReceived
    '        Session("DeleteLoanAdvanceReceived") = objUserPrivilege._DeleteLoanAdvanceReceived
    '        Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
    '        Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan

    '        '*** Savings
    '        Session("AddSavingsPayment") = objUserPrivilege._AddSavingsPayment
    '        Session("EditSavingsPayment") = objUserPrivilege._EditSavingsPayment
    '        Session("DeleteSavingsPayment") = objUserPrivilege._DeleteSavingsPayment
    '        Session("AllowToViewEmployeeSavingsList") = objUserPrivilege._AllowToViewEmployeeSavingsList
    '        Session("AddEmployeeSavings") = objUserPrivilege._AddEmployeeSavings
    '        Session("EditEmployeeSavings") = objUserPrivilege._EditEmployeeSavings
    '        Session("DeleteEmployeeSavings") = objUserPrivilege._DeleteEmployeeSavings
    '        Session("AllowChangeSavingStatus") = objUserPrivilege._AllowChangeSavingStatus

    '        '*** HR
    '        Session("AddDependant") = objUserPrivilege._AddDependant
    '        Session("EditDependant") = objUserPrivilege._EditDependant
    '        Session("DeleteDependant") = objUserPrivilege._DeleteDependant
    '        Session("ViewDependant") = objUserPrivilege._AllowToViewEmpDependantsList
    '        Session("AddEmployeeAssets") = objUserPrivilege._AddEmployeeAssets
    '        Session("EditEmployeeAssets") = objUserPrivilege._EditEmployeeAssets
    '        Session("DeleteEmployeeAssets") = objUserPrivilege._DeleteEmployeeAssets
    '        Session("ViewCompanyAssetList") = objUserPrivilege._AllowToViewCompanyAssetList
    '        Session("AddEmployeeSkill") = objUserPrivilege._AddEmployeeSkill
    '        Session("EditEmployeeSkill") = objUserPrivilege._EditEmployeeSkill
    '        Session("DeleteEmployeeSkill") = objUserPrivilege._DeleteEmployeeSkill
    '        Session("ViewEmpSkillList") = objUserPrivilege._AllowToViewEmpSkillList
    '        Session("AddEmployeeQualification") = objUserPrivilege._AddEmployeeQualification
    '        Session("EditEmployeeQualification") = objUserPrivilege._EditEmployeeQualification
    '        Session("DeleteEmployeeQualification") = objUserPrivilege._DeleteEmployeeQualification
    '        Session("ViewEmpQualificationList") = objUserPrivilege._AllowToViewEmpQualificationList
    '        Session("AddEmployeeReferee") = objUserPrivilege._AddEmployeeReferee
    '        Session("EditEmployeeReferee") = objUserPrivilege._EditEmployeeReferee
    '        Session("DeleteEmployeeReferee") = objUserPrivilege._DeleteEmployeeReferee
    '        Session("ViewEmpReferenceList") = objUserPrivilege._AllowToViewEmpReferenceList
    '        Session("AddEmployeeExperience") = objUserPrivilege._AddEmployeeExperience
    '        Session("EditEmployeeExperience") = objUserPrivilege._EditEmployeeExperience
    '        Session("DeleteEmployeeExperience") = objUserPrivilege._DeleteEmployeeExperience
    '        Session("ViewEmpExperienceList") = objUserPrivilege._AllowToViewEmpExperienceList
    '        Session("AddEmployee") = objUserPrivilege._AddEmployee
    '        Session("EditEmployee") = objUserPrivilege._EditEmployee
    '        Session("DeleteEmployee") = objUserPrivilege._DeleteEmployee
    '        Session("ViewEmployee") = objUserPrivilege._AllowToViewEmpList
    '        Session("SetReinstatementdate") = objUserPrivilege._AllowtoSetEmpReinstatementDate
    '        Session("ChangeConfirmationDate") = objUserPrivilege._AllowtoChangeConfirmationDate
    '        Session("ChangeAppointmentDate") = objUserPrivilege._AllowtoChangeAppointmentDate
    '        Session("SetEmployeeBirthDate") = objUserPrivilege._AllowtoSetEmployeeBirthDate
    '        Session("SetEmpSuspensionDate") = objUserPrivilege._AllowtoSetEmpSuspensionDate
    '        Session("SetEmpProbationDate") = objUserPrivilege._AllowtoSetEmpProbationDate
    '        Session("SetEmploymentEndDate") = objUserPrivilege._AllowtoSetEmploymentEndDate
    '        Session("SetLeavingDate") = objUserPrivilege._AllowtoSetLeavingDate
    '        Session("ChangeRetirementDate") = objUserPrivilege._AllowtoChangeRetirementDate
    '        Session("ViewScale") = objUserPrivilege._AllowTo_View_Scale
    '        Session("ViewBenefitList") = objUserPrivilege._AllowToViewEmpBenefitList
    '        Session("AllowtoChangeBranch") = objUserPrivilege._AllowtoChangeBranch
    '        Session("AllowtoChangeDepartmentGroup") = objUserPrivilege._AllowtoChangeDepartmentGroup
    '        Session("AllowtoChangeDepartment") = objUserPrivilege._AllowtoChangeDepartment
    '        Session("AllowtoChangeSectionGroup") = objUserPrivilege._AllowtoChangeSectionGroup
    '        Session("AllowtoChangeSection") = objUserPrivilege._AllowtoChangeSection
    '        Session("AllowtoChangeUnitGroup") = objUserPrivilege._AllowtoChangeUnitGroup
    '        Session("AllowtoChangeUnit") = objUserPrivilege._AllowtoChangeUnit
    '        Session("AllowtoChangeTeam") = objUserPrivilege._AllowtoChangeTeam
    '        Session("AllowtoChangeJobGroup") = objUserPrivilege._AllowtoChangeJobGroup
    '        Session("AllowtoChangeJob") = objUserPrivilege._AllowtoChangeJob
    '        Session("AllowtoChangeGradeGroup") = objUserPrivilege._AllowtoChangeGradeGroup
    '        Session("AllowtoChangeGrade") = objUserPrivilege._AllowtoChangeGrade
    '        Session("AllowtoChangeGradeLevel") = objUserPrivilege._AllowtoChangeGradeLevel
    '        Session("AllowtoChangeClassGroup") = objUserPrivilege._AllowtoChangeClassGroup
    '        Session("AllowtoChangeClass") = objUserPrivilege._AllowtoChangeClass
    '        Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
    '        Session("AllowtoChangeCompanyEmail") = objUserPrivilege._AllowtoChangeCompanyEmail
    '        Session("AllowToAddEditPhoto") = objUserPrivilege._AllowToAddEditPhoto
    '        Session("AllowToDeletePhoto") = objUserPrivilege._AllowToDeletePhoto
    '        Session("AllowToViewScale") = objUserPrivilege._AllowTo_View_Scale

    '        '*** Leave
    '        Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
    '        Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
    '        Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense
    '        Session("AddLeaveForm") = objUserPrivilege._AddLeaveForm
    '        Session("EditLeaveForm") = objUserPrivilege._EditLeaveForm
    '        Session("DeleteLeaveForm") = objUserPrivilege._DeleteLeaveForm
    '        Session("ViewLeaveFormList") = objUserPrivilege._AllowToViewLeaveFormList
    '        Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
    '        Session("ViewLeaveProcessList") = objUserPrivilege._AllowToViewLeaveProcessList
    '        Session("ChangeLeaveFormStatus") = objUserPrivilege._AllowChangeLeaveFormStatus
    '        Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance
    '        Session("AddPlanLeave") = objUserPrivilege._AddPlanLeave
    '        Session("EditPlanLeave") = objUserPrivilege._EditPlanLeave
    '        Session("DeletePlanLeave") = objUserPrivilege._DeletePlanLeave
    '        Session("ViewLeavePlannerList") = objUserPrivilege._AllowToViewLeavePlannerList
    '        Session("ViewPlannedLeaveViewer") = objUserPrivilege._AllowtoViewPlannedLeaveViewer
    '        Session("ViewLeaveViewer") = objUserPrivilege._AllowToViewLeaveViewer
    '        Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
    '        Session("AllowToCancelPreviousDateLeave") = objUserPrivilege._AllowToCancelPreviousDateLeave
    '        Session("AllowtoMigrateLeaveApprovers") = objUserPrivilege._AllowtoMigrateLeaveApprover
    '        Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave
    '        Session("AllowToViewLeaveApproverList") = objUserPrivilege._AllowToViewLeaveApproverList
    '        Session("AddLeaveApprover") = objUserPrivilege._AddLeaveApprover
    '        Session("EditLeaveApprover") = objUserPrivilege._EditLeaveApprover
    '        Session("DeleteLeaveApprover") = objUserPrivilege._DeleteLeaveApprover
    '        Session("AllowToAssignIssueUsertoEmp") = objUserPrivilege._AllowToAssignIssueUsertoEmp
    '        Session("AllowToMigrateIssueUser") = objUserPrivilege._AllowToMigrateIssueUser
    '        Session("AllowtoEndELC") = objUserPrivilege._AllowToEndLeaveCycle
    '        Session("AllowMapApproverWithUser") = objUserPrivilege._AllowMapApproverWithUser
    '        Session("AllowtoMapLeaveType") = objUserPrivilege._AllowtoMapLeaveType
    '        Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '        Session("AllowPrintLeaveForm") = objUserPrivilege._AllowPrintLeaveForm
    '        Session("AllowPreviewLeaveForm") = objUserPrivilege._AllowPreviewLeaveForm
    '        'Pinkal (12-Feb-2015) -- End


    '        '*** Time & Attendance
    '        Session("AllowToAddEditDeleteGlobalTimesheet") = objUserPrivilege._AllowToAddEditDeleteGlobalTimesheet

    '        '*** Medical
    '        Session("AddMedicalClaim") = objUserPrivilege._AddMedicalClaim
    '        Session("EditMedicalClaim") = objUserPrivilege._EditMedicalClaim
    '        Session("DeleteMedicalClaim") = objUserPrivilege._DeleteMedicalClaim
    '        Session("AllowToExportMedicalClaim") = objUserPrivilege._AllowToExportMedicalClaim
    '        Session("AllowToCancelExportedMedicalClaim") = objUserPrivilege._AllowToCancelExportedMedicalClaim
    '        Session("AllowToAddMedicalSickSheet") = objUserPrivilege._AllowToAddMedicalSickSheet
    '        Session("AllowToEditMedicalSickSheet") = objUserPrivilege._AllowToEditMedicalSickSheet
    '        Session("AllowToDeleteMedicalSickSheet") = objUserPrivilege._AllowToDeleteMedicalSickSheet
    '        Session("AllowToPrintMedicalSickSheet") = objUserPrivilege._AllowToPrintMedicalSickSheet
    '        'Pinkal (12-Sep-2014) -- Start
    '        'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
    '        Session("RevokeUserAccessOnSicksheet") = objUserPrivilege._RevokeUserAccessOnSicksheet
    '        'Pinkal (12-Sep-2014) -- End


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '        Session("AllowToViewEmpSickSheetList") = objUserPrivilege._AllowToViewEmpSickSheetList
    '        Session("AllowToViewMedicalClaimList") = objUserPrivilege._AllowToViewMedicalClaimList
    '        Session("AllowToSaveMedicalClaim") = objUserPrivilege._AllowToSaveMedicalClaim
    '        Session("AllowToFinalSaveMedicalClaim") = objUserPrivilege._AllowToFinalSaveMedicalClaim
    '        'Pinkal (12-Feb-2015) -- End


    '        '*** Assessment
    '        Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._Allow_UnlockFinalSaveBSCPlanning
    '        Session("EditEmployeeAssessment") = objUserPrivilege._EditEmployeeAssessment
    '        Session("DeleteEmployeeAssessment") = objUserPrivilege._DeleteEmployeeAssessment
    '        Session("Allow_UnlockCommittedGeneralAssessment") = objUserPrivilege._Allow_UnlockCommittedGeneralAssessment
    '        Session("AllowToViewSelfAssessmentList") = objUserPrivilege._AllowToViewSelfAssessmentList
    '        Session("AddAssessmentAnalysis") = objUserPrivilege._AddAssessmentAnalysis
    '        Session("EditAssessmentAnalysis") = objUserPrivilege._EditAssessmentAnalysis
    '        Session("DeleteAssessmentAnalysis") = objUserPrivilege._DeleteAssessmentAnalysis
    '        Session("AllowToViewAssessorAssessmentList") = objUserPrivilege._AllowToViewAssessorAssessmentList
    '        Session("AllowToViewReviewerAssessmentList") = objUserPrivilege._AllowToViewReviewerAssessmentList
    '        Session("AllowToAddReviewerGeneralAssessment") = objUserPrivilege._AllowToAddReviewerGeneralAssessment
    '        Session("AllowToEditReviewerGeneralAssessment") = objUserPrivilege._AllowToEditReviewerGeneralAssessment
    '        Session("AllowToDeleteReviewerGeneralAssessment") = objUserPrivilege._AllowToDeleteReviewerGeneralAssessment
    '        Session("Allow_UnlockCommittedBSCAssessment") = objUserPrivilege._Allow_UnlockCommittedBSCAssessment
    '        Session("AllowToEditSelfBSCAssessment") = objUserPrivilege._AllowToEditSelfBSCAssessment
    '        Session("AllowToDeleteSelfBSCAssessment") = objUserPrivilege._AllowToDeleteSelfBSCAssessment
    '        Session("AllowToViewSelfAssessedBSCList") = objUserPrivilege._AllowToViewSelfAssessedBSCList
    '        Session("AllowToAddAssessorBSCAssessment") = objUserPrivilege._AllowToAddAssessorBSCAssessment
    '        Session("AllowToEditAssessorBSCAssessment") = objUserPrivilege._AllowToEditAssessorBSCAssessment
    '        Session("AllowToDeleteAssessorBSCAssessment") = objUserPrivilege._AllowToDeleteAssessorBSCAssessment
    '        Session("AllowToViewAssessorAssessedBSCList") = objUserPrivilege._AllowToViewAssessorAssessedBSCList
    '        Session("AllowToAddReviewerBSCAssessment") = objUserPrivilege._AllowToAddReviewerBSCAssessment
    '        Session("AllowToEditReviewerBSCAssessment") = objUserPrivilege._AllowToEditReviewerBSCAssessment
    '        Session("AllowToDeleteReviewerBSCAssessment") = objUserPrivilege._AllowToDeleteReviewerBSCAssessment
    '        Session("AllowToViewReviewerAssessedBSCList") = objUserPrivilege._AllowToViewReviewerAssessedBSCList
    '        Session("AllowToViewLevel3EvaluationList") = objUserPrivilege._AllowToViewLevel3EvaluationList
    '        Session("AllowToAddLevelIIIEvaluation") = objUserPrivilege._AllowToAddLevelIIIEvaluation
    '        Session("AllowToEditLevelIIIEvaluation") = objUserPrivilege._AllowToEditLevelIIIEvaluation
    '        Session("AllowToDeleteLevelIIIEvaluation") = objUserPrivilege._AllowToDeleteLevelIIIEvaluation
    '        Session("AllowToSave_CompleteLevelIIIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIIIEvaluation
    '        Session("Allow_FinalSaveBSCPlanning") = objUserPrivilege._Allow_FinalSaveBSCPlanning
    '        Session("AllowToMigrateAssessor_Reviewer") = objUserPrivilege._AllowToMigrateAssessor_Reviewer

    '        '*** Recruitment
    '        Session("AllowToApproveEmployee") = objUserPrivilege._AllowToApproveEmployee
    '        Session("AllowtoApproveApplicantEligibility") = objUserPrivilege._AllowtoApproveApplicantEligibility
    '        Session("AllowtoDisapproveApplicantEligibility") = objUserPrivilege._AllowtoDisapproveApplicantEligibility
    '        Session("AllowtoViewInterviewAnalysisList") = objUserPrivilege._AllowtoViewInterviewAnalysisList
    '        Session("AllowToViewShortListApplicants") = objUserPrivilege._AllowToViewShortListApplicants
    '        Session("AllowToApproveApplicantFilter") = objUserPrivilege._AllowToApproveApplicantFilter
    '        Session("AllowToRejectApplicantFilter") = objUserPrivilege._AllowToRejectApplicantFilter
    '        Session("AllowToApproveFinalShortListedApplicant") = objUserPrivilege._AllowToApproveFinalShortListedApplicant
    '        Session("AllowToDisapproveFinalShortListedApplicant") = objUserPrivilege._AllowToDisapproveFinalShortListedApplicant
    '        'Sohail (28 May 2014) -- Start
    '        'Enhancement - Staff Requisition.
    '        Session("AllowToViewStaffRequisitionList") = objUserPrivilege._AllowToViewStaffRequisitionList
    '        Session("AllowToAddStaffRequisition") = objUserPrivilege._AllowToAddStaffRequisition
    '        Session("AllowToEditStaffRequisition") = objUserPrivilege._AllowToEditStaffRequisition
    '        Session("AllowToDeleteStaffRequisition") = objUserPrivilege._AllowToDeleteStaffRequisition
    '        'Sohail (28 May 2014) -- End
    '        'Pinkal (19-Jun-2014) -- Start
    '        'Enhancement : TRA Changes Leave Enhancement
    '        Session("AllowToViewStaffRequisitionApprovals") = objUserPrivilege._AllowToViewStaffRequisitionApprovals
    '        Session("AllowToCancelStaffRequisition") = objUserPrivilege._AllowToCancelStaffRequisition
    '        Session("AllowToApproveStaffRequisition") = objUserPrivilege._AllowToApproveStaffRequisition
    '        'Pinkal (19-Jun-2014) -- End


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    '        '*** TRANING

    '        Session("AllowToViewLevel1EvaluationList") = objUserPrivilege._AllowToViewLevel1EvaluationList
    '        Session("AllowToAddLevelIEvaluation") = objUserPrivilege._AllowToAddLevelIEvaluation
    '        Session("AllowToEditLevelIEvaluation") = objUserPrivilege._AllowToEditLevelIEvaluation
    '        Session("AllowToDeleteLevelIEvaluation") = objUserPrivilege._AllowToDeleteLevelIEvaluation
    '        Session("AllowToSave_CompleteLevelIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIEvaluation
    '        Session("AllowToPrintLevelIEvaluation") = objUserPrivilege._AllowToPrintLevelIEvaluation
    '        Session("AllowToPreviewLevelIEvaluation") = objUserPrivilege._AllowToPreviewLevelIEvaluation
    '        Session("AllowToViewTrainingEnrollmentList") = objUserPrivilege._AllowToViewTrainingEnrollmentList
    '        Session("AddTrainingEnrollment") = objUserPrivilege._AddTrainingEnrollment
    '        Session("EditTrainingEnrollment") = objUserPrivilege._EditTrainingEnrollment
    '        Session("DeleteTrainingEnrollment") = objUserPrivilege._DeleteTrainingEnrollment

    '        '*** ASSET DECLARATION

    '        Session("AddAssetDeclaration") = objUserPrivilege._Allow_AddAssetDeclaration
    '        Session("EditAssetDeclaration") = objUserPrivilege._Allow_EditAssetDeclaration
    '        Session("DeleteAssetDeclaration") = objUserPrivilege._Allow_DeleteAssetDeclaration
    '        Session("FinalSaveAssetDeclaration") = objUserPrivilege._Allow_FinalSaveAssetDeclaration
    '        Session("UnlockFinalSaveAssetDeclaration") = objUserPrivilege._Allow_UnlockFinalSaveAssetDeclaration
    '        Session("ViewAssetsDeclarationList") = objUserPrivilege._AllowToViewAssetsDeclarationList


    '        'Pinkal (12-Feb-2015) -- End


    '        If Session("LoginBy") IsNot Nothing Then
    '            If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                clsActiveUserEngine.Leaved("USER" & Session("UserId").ToString)
    '            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                clsActiveUserEngine.Leaved("EMP" & Session("Employeeunkid").ToString)
    '            End If
    '        End If
    '        Session("clsuser") = clsUser


    '        Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
    '        Session("DateFormat") = culture.DateTimeFormat.ShortDatePattern.ToString


    '        'Pinkal (12-Feb-2015) -- Start
    '        'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
    '        'If rtbEmployee.Checked = True Then

    '        '    'Anjan [28 November 2014] -- Start
    '        '    'ENHANCEMENT : checking for kenya AAM Resources
    '        '    If ConfigParameter._Object._IsExpire AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = True Then
    '        '        Exit Sub
    '        '    End If
    '        '    'Anjan [28 November 2014] -- End

    '        '    If ConfigParameter._Object._IsExpire Then
    '        '        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
    '        '        Exit Try
    '        '    Else
    '        '        If ConfigParameter._Object._IsArutiDemo = True AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'IsDemo
    '        '            DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '        '        Else 'IsLicence
    '        '            Response.Redirect("~/UserHome.aspx", False)
    '        '        End If
    '        '    End If
    '        '    If SetCompanySessions(intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
    '        '        Exit Sub
    '        '    End If


    '        'Else
    '        '    'Anjan [28 November 2014] -- Start
    '        '    'ENHANCEMENT : checking for kenya AAM Resources
    '        '    If ConfigParameter._Object._IsExpire AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = True Then
    '        '        Exit Sub
    '        '    End If
    '        '    'Anjan [28 November 2014] -- End

    '        '    If ConfigParameter._Object._IsExpire Then
    '        '        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
    '        '        Exit Try
    '        '    End If

    '        '    Call BindCompany(CInt(Session("UserId")))
    '        '    If ddlCompany.Items.Count > 0 Then
    '        '        If ddlCompany.Items.Count = 1 Then
    '        '            objCONN = Nothing
    '        '            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
    '        '                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '        '                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
    '        '                constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
    '        '                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
    '        '                objCONN = New SqlConnection
    '        '                objCONN.ConnectionString = constr
    '        '                objCONN.Open()
    '        '                HttpContext.Current.Session("gConn") = objCONN
    '        '            End If

    '        '            If SetCompanySessions(CInt(ddlCompany.SelectedValue), CInt(HttpContext.Current.Session("LangId"))) = False Then
    '        '                Exit Try
    '        '            End If

    '        '            If ConfigParameter._Object._IsArutiDemo = True AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'IsDemo
    '        '                DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '        '            Else
    '        '                Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '        '            End If

    '        '            Exit Try
    '        '        Else
    '        '            If ConfigParameter._Object._IsArutiDemo = True AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'IsDemo
    '        '                DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me) ', Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx"                   
    '        '            End If
    '        '            popCompanySelection.Show()
    '        '        End If
    '        '    Else
    '        '        DisplayMessage.DisplayMessage("Sorry, You cannot login to system. Reason : Access to any company is not given to you. Please contact Administrator.", Me.Page)
    '        '        Exit Try
    '        '    End If
    '        'End If

    '        'Anjan [28 November 2014] -- Start
    '        'ENHANCEMENT : checking for kenya AAM Resources
    '        If ConfigParameter._Object._IsExpire AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = True Then
    '            Exit Sub
    '        End If
    '        'Anjan [28 November 2014] -- End

    '        If ConfigParameter._Object._IsExpire Then
    '            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
    '            Exit Try
    '        Else
    '            If ConfigParameter._Object._IsArutiDemo = True AndAlso arrGroupName.Contains(objGroupMaster._Groupname.ToUpper) = False Then 'IsDemo
    '                DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '            Else 'IsLicence
    '                Response.Redirect("~/UserHome.aspx", False)
    '            End If
    '        End If
    '        If SetCompanySessions(intCompanyId, CInt(HttpContext.Current.Session("LangId"))) = False Then
    '            Exit Sub
    '        End If

    '        'Pinkal (12-Feb-2015) -- End


    '    Catch ex As Exception
    '        'Throw ex
    '        DisplayMessage.DisplayError("Error in BtnLogin_click event" & ex.Message.ToString, Me)
    '    Finally
    '    End Try

    'End Sub
#End Region

#Region "    btnLogin_Click b4 change flow of login (giving compoany code text box for employee login and company combo on popup for manager) "
    'Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlogin.Click
    '    Try


    '        'Sohail (28 Oct 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
    '            Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
    '            Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
    '            constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
    '            constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
    '            objCONN = New SqlConnection
    '            objCONN.ConnectionString = constr
    '            objCONN.Open()
    '            HttpContext.Current.Session("gConn") = objCONN
    '        End If
    '        'Sohail (28 Oct 2013) -- End

    '        'Sohail (27 Apr 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        gobjConfigOptions = New clsConfigOptions

    '        ArtLic._Object = New ArutiLic(False)
    '        'Anjan [28 Mar 2014] -- Start
    '        'ENHANCEMENT : Requested by Rutta
    '        Dim objGroupMaster As New clsGroup_Master
    '        objGroupMaster._Groupunkid = 1
    '        ArtLic._Object.HotelName = objGroupMaster._Groupname
    '        'Anjan [28 Mar 2014 ] -- End

    '        'Sohail (23 Dec 2013) -- Start
    '        'Enhancement - License
    '        If ConfigParameter._Object._IsArutiDemo Then 'Check if Licence is not activated (If it is Demo)
    '            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
    '                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page)
    '                Exit Sub
    '            End If
    '        End If
    '        'Sohail (23 Dec 2013) -- End
    '        'Anjan [28 Mar 2014] -- Start
    '        'ENHANCEMENT : Requested by Rutta
    '        'If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
    '        '    Dim objGroupMaster As New clsGroup_Master
    '        '    objGroupMaster._Groupunkid = 1
    '        '    ArtLic._Object.HotelName = objGroupMaster._Groupname
    '        'End If
    '        'Anjan [28 Mar 2014 ] -- End

    '        If rtbEmployee.Checked = True Then
    '            If CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
    '                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
    '                Exit Sub
    '            End If
    '        ElseIf rtbUser.Checked = True Then
    '            If CheckLicence("MANAGER_SELF_SERVICE") = False Then
    '                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page)
    '                Exit Sub
    '            End If
    '        End If
    '        'Sohail (27 Apr 2013) -- End


    '        'S.SANDEEP [ 22 NOV 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ' If rtbUser.Checked = True Then
    '        If isValidData() = False Then
    '            Exit Sub
    '        End If
    '        'End If
    '        'S.SANDEEP [ 22 NOV 2012 ] -- END

    '        Session("Login") = False

    '        'Sohail (04 Jun 2012) -- Start
    '        'TRA - ENHANCEMENT - View Online Memberes
    '        If Session("LoginBy") IsNot Nothing Then
    '            If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                clsActiveUserEngine.Leaved("USER" & Session("UserId").ToString)
    '            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                clsActiveUserEngine.Leaved("EMP" & Session("Employeeunkid").ToString)
    '            End If
    '        End If
    '        'Sohail (04 Jun 2012) -- End

    '        'TODO Change all global objects
    '        gobjConfigOptions = New clsConfigOptions
    '        gobjConfigOptions._Companyunkid = ddlCompany.SelectedValue
    '        ConfigParameter._Object._Companyunkid = gobjConfigOptions._Companyunkid
    '        Company._Object._Companyunkid = ddlCompany.SelectedValue

    '        'Sohail (30 Apr 2013) -- Start
    '        'TRA - Company and Employee Licence Check
    '        If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Company > ConfigParameter._Object._NoOfCompany Then
    '            DisplayMessage.DisplayMessage("Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance.", Me.Page)
    '            Exit Try
    '            'Sohail (23 Dec 2013) -- Start
    '            'Enhancement - Anrew's request
    '            'ElseIf ConfigParameter._Object._IsArutiDemo = True AndAlso ddlCompany.SelectedIndex <> 0 Then
    '            '    DisplayMessage.DisplayMessage("Sorry, You can use only first company in Aruti Demo. Please contact Aruti Support team for assistance.", Me.Page)
    '            '    Exit Sub
    '            'Sohail (23 Dec 2013) -- End
    '        End If
    '        'Company._Object._Total_Active_Employee_AsOnFromDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        'Company._Object._Total_Active_Employee_AsOnToDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        Company._Object._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
    '        If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Employee_ForAllCompany > ConfigParameter._Object._NoOfEmployees Then
    '            DisplayMessage.DisplayMessage("Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance.", Me.Page)
    '            Exit Try
    '        End If
    '        'Sohail (30 Apr 2013) -- End



    '        'Pinkal (03-Jan-2014) -- Start
    '        'Enhancement : TRA Changes

    '        Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
    '        Session("DateFormat") = culture.DateTimeFormat.ShortDatePattern.ToString

    '        'Pinkal (03-Jan-2014) -- End

    '        'Sohail (23 Apr 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Dim clsConfig As New clsConfigOptions
    '        clsConfig._Companyunkid = CInt(ddlCompany.SelectedValue)
    '        Session("CompanyUnkId") = CInt(ddlCompany.SelectedValue)
    '        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
    '        Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting
    '        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
    '        Session("SickSheetNotype") = clsConfig._SickSheetNotype
    '        Session("fmtCurrency") = clsConfig._CurrencyFormat
    '        Session("AllowEditAddress") = clsConfig._AllowEditAddress
    '        Session("AllowEditPersonalInfo") = clsConfig._AllowEditPersonalInfo
    '        Session("AllowEditEmergencyAddress") = clsConfig._AllowEditEmergencyAddress
    '        Session("LoanApplicationNoType") = clsConfig._LoanApplicationNoType
    '        Session("LoanApplicationPrifix") = clsConfig._LoanApplicationPrifix
    '        Session("NextLoanApplicationNo") = clsConfig._NextLoanApplicationNo
    '        'Sohail (18 May 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        Session("BatchPostingNotype") = clsConfig._BatchPostingNotype
    '        Session("BatchPostingPrifix") = clsConfig._BatchPostingPrifix
    '        Session("DoNotAllowOverDeductionForPayment") = clsConfig._DoNotAllowOverDeductionForPayment
    '        Session("PaymentVocNoType") = clsConfig._PaymentVocNoType
    '        Session("IsDenominationCompulsory") = clsConfig._IsDenominationCompulsory
    '        Session("SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
    '        'Sohail (18 May 2013) -- End
    '        Session("AssetDeclarationInstruction") = clsConfig._AssetDeclarationInstruction
    '        Session("Document_Path") = clsConfig._Document_Path
    '        'Sohail (22 May 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        Session("IgnoreZeroValueHeadsOnPayslip") = clsConfig._IgnoreZeroValueHeadsOnPayslip
    '        Session("ShowLoanBalanceOnPayslip") = clsConfig._ShowLoanBalanceOnPayslip
    '        Session("ShowSavingBalanceOnPayslip") = clsConfig._ShowSavingBalanceOnPayslip
    '        Session("ShowEmployerContributionOnPayslip") = clsConfig._ShowEmployerContributionOnPayslip
    '        Session("ShowAllHeadsOnPayslip") = clsConfig._ShowAllHeadsOnPayslip
    '        Session("LogoOnPayslip") = clsConfig._LogoOnPayslip
    '        Session("PayslipTemplate") = clsConfig._PayslipTemplate
    '        Session("LeaveTypeOnPayslip") = clsConfig._LeaveTypeOnPayslip
    '        Session("ShowCumulativeAccrual") = clsConfig._ShowCumulativeAccrualOnPayslip
    '        'Sohail (22 May 2012) -- End
    '        Session("Base_CurrencyId") = clsConfig._Base_CurrencyId 'Sohail (22 Mar 2013)
    '        Session("ShowCategoryOnPayslip") = clsConfig._ShowCategoryOnPayslip 'Sohail (31 Aug 2013)
    '        Session("ShowInformationalHeadsOnPayslip") = clsConfig._ShowInformationalHeadsOnPayslip 'Sohail (19 Sep 2013)
    '        'Sohail (03 Dec 2013) -- Start
    '        'Enhancement - Oman
    '        Session("PaymentRoundingType") = clsConfig._PaymentRoundingType
    '        Session("PaymentRoundingMultiple") = clsConfig._PaymentRoundingMultiple
    '        'Sohail (03 Dec 2013) -- End
    '        'Sohail (23 Dec 2013) -- Start
    '        'Enhancement - Oman
    '        Session("ShowBirthDateOnPayslip") = clsConfig._ShowBirthDateOnPayslip
    '        Session("ShowAgeOnPayslip") = clsConfig._ShowAgeOnPayslip
    '        Session("ShowNoOfDependantsOnPayslip") = clsConfig._ShowNoOfDependantsOnPayslip
    '        Session("ShowMonthlySalaryOnPayslip") = clsConfig._ShowMonthlySalaryOnPayslip
    '        'Sohail (23 Dec 2013) -- End
    '        'Sohail (09 Jan 2014) -- Start
    '        'Enhancement - Show OT hours in OT Amount Head caption for Voltamp
    '        Session("OT1HourHeadID") = clsConfig._OT1HourHeadId
    '        Session("OT2HourHeadID") = clsConfig._OT2HourHeadId
    '        Session("OT3HourHeadID") = clsConfig._OT3HourHeadId
    '        Session("OT4HourHeadID") = clsConfig._OT4HourHeadId
    '        Session("OT1HourHeadName") = clsConfig._OT1HourHeadName
    '        Session("OT2HourHeadName") = clsConfig._OT2HourHeadName
    '        Session("OT3HourHeadName") = clsConfig._OT3HourHeadName
    '        Session("OT4HourHeadName") = clsConfig._OT4HourHeadName
    '        Session("OT1AmountHeadID") = clsConfig._OT1AmountHeadId
    '        Session("OT2AmountHeadID") = clsConfig._OT2AmountHeadId
    '        Session("OT3AmountHeadID") = clsConfig._OT3AmountHeadId
    '        Session("OT4AmountHeadID") = clsConfig._OT4AmountHeadId
    '        Session("OT1AmountHeadName") = clsConfig._OT1AmountHeadName
    '        Session("OT2AmountHeadName") = clsConfig._OT2AmountHeadName
    '        Session("OT3AmountHeadName") = clsConfig._OT3AmountHeadName
    '        Session("OT4AmountHeadName") = clsConfig._OT4AmountHeadName
    '        'Sohail (09 Jan 2014) -- End
    '        Session("ShowSalaryOnHoldOnPayslip") = clsConfig._ShowSalaryOnHoldOnPayslip 'Sohail (24 Feb 2014)

    '        'Sohail (01 Jan 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
    '            Session("ArutiSelfServiceURL") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
    '        Else
    '            Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
    '        End If
    '        'Sohail (01 Jan 2013) -- End

    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("AllowEditExperience") = clsConfig._AllowEditExperience
    '        Session("AllowDeleteExperience") = clsConfig._AllowDeleteExperience
    '        Session("AllowAddSkills") = clsConfig._AllowAddSkills
    '        Session("AllowEditSkills") = clsConfig._AllowEditSkills
    '        Session("AllowDeleteSkills") = clsConfig._AllowDeleteSkills
    '        Session("AllowAddDependants") = clsConfig._AllowAddDependants
    '        Session("AllowDeleteDependants") = clsConfig._AllowDeleteDependants
    '        Session("IsDependant_AgeLimit_Set") = clsConfig._IsDependant_AgeLimit_Set
    '        Session("AllowEditDependants") = clsConfig._AllowEditDependants
    '        Session("AllowAddIdentity") = clsConfig._AllowAddIdentity
    '        Session("AllowDeleteIdentity") = clsConfig._AllowDeleteIdentity
    '        Session("AllowEditIdentity") = clsConfig._AllowEditIdentity
    '        Session("AllowAddMembership") = clsConfig._AllowAddMembership
    '        Session("AllowDeleteMembership") = clsConfig._AllowDeleteMembership
    '        Session("AllowEditMembership") = clsConfig._AllowEditMembership

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'Session("AllowEditQualifications") = clsConfig._AllowEditQualifications
    '        Session("AllowAddQualifications") = clsConfig._AllowAddQualifications
    '        'Pinkal (22-Nov-2012) -- End


    '        Session("AllowDeleteQualifications") = clsConfig._AllowDeleteQualifications
    '        Session("AllowEditQualifications") = clsConfig._AllowEditQualifications
    '        Session("AllowAddReference") = clsConfig._AllowAddReference
    '        Session("AllowDeleteReference") = clsConfig._AllowDeleteReference
    '        Session("AllowEditReference") = clsConfig._AllowEditReference
    '        Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
    '        Session("IsBSCObjectiveSaved") = clsConfig._IsBSCObjectiveSaved
    '        Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
    '        Session("AllowAddExperience") = clsConfig._AllowAddExperience
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END

    '        'Anjan (25 Oct 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    '        Session("IsAllocation_Hierarchy_Set") = clsConfig._IsAllocation_Hierarchy_Set
    '        Session("Allocation_Hierarchy") = clsConfig._Allocation_Hierarchy
    '        'Anjan (25 Oct 2012)-End 

    '        'S.SANDEEP [ 06 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("AllowChangeCompanyEmail") = clsConfig._AllowChangeCompanyEmail
    '        'S.SANDEEP [ 06 DEC 2012 ] -- END
    '        'Sohail (23 Apr 2012) -- End

    '        'Sohail (25 Mar 2013) -- Start
    '        'TRA - ENHANCEMENT
    '        Session("AllowToViewPersonalSalaryCalculationReport") = clsConfig._AllowToViewPersonalSalaryCalculationReport
    '        Session("AllowToViewEDDetailReport") = clsConfig._AllowToViewEDDetailReport
    '        'Sohail (25 Mar 2013) -- End

    '        'Pinkal (25-APR-2012) -- Start
    '        'Enhancement : TRA Changes
    '        Session("SickSheetNotype") = clsConfig._SickSheetNotype
    '        Session("LeaveApproverForLeaveType") = clsConfig._IsLeaveApprover_ForLeaveType
    '        Session("LeaveFormNoType") = clsConfig._LeaveFormNoType
    '        'Pinkal (25-APR-2012) -- End


    '        'S.SANDEEP [ 18 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("CompanyDomain") = clsConfig._CompanyDomain.ToString.Trim
    '        'S.SANDEEP [ 18 DEC 2012 ] -- END

    '        'S.SHARMA [ 22 JAN 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("Notify_EmplData") = clsConfig._Notify_EmplData
    '        Session("Notify_Allocation") = clsConfig._Notify_Allocation
    '        Session("DatafileExportPath") = clsConfig._DatafileExportPath
    '        Session("DatafileName") = clsConfig._DatafileName
    '        Session("Accounting_TransactionReference") = clsConfig._Accounting_TransactionReference
    '        Session("Accounting_JournalType") = clsConfig._Accounting_JournalType
    '        Session("Accounting_JVGroupCode") = clsConfig._Accounting_JVGroupCode

    '        'S.SHARMA [ 22 JAN 2013 ] -- END




    '        'Pinkal (04-Mar-2013) -- Start
    '        'Enhancement : TRA Changes

    '        If clsConfig._LeaveBalanceSetting <= 0 Then
    '            Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
    '        Else
    '            Session("LeaveBalanceSetting") = clsConfig._LeaveBalanceSetting
    '        End If

    '        'Pinkal (04-Mar-2013) -- End



    '        'Pinkal (12-Mar-2013) -- Start
    '        'Enhancement : TRA Changes

    '        Session("AllowToviewPaysliponEss") = clsConfig._AllowToviewPaysliponEss
    '        Session("ViewPayslipDaysBefore") = clsConfig._ViewPayslipDaysBefore

    '        'Pinkal (12-Mar-2013) -- End


    '        'Pinkal (27-Mar-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("IsImgInDataBase") = clsConfig._IsImgInDataBase
    '        'Pinkal (27-Mar-2013) -- End


    '        'Pinkal (01-Apr-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("AllowToAddEditImageForESS") = clsConfig._AllowToAddEditImageForESS
    '        'Pinkal (01-Apr-2013) -- End



    '        'Pinkal (24-Apr-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("ShowFirstAppointmentDate") = clsConfig._ShowFirstAppointmentDate
    '        'Pinkal (24-Apr-2013) -- End


    '        'Pinkal (24-May-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("BatchPostingNotype") = clsConfig._BatchPostingNotype
    '        Session("NotifyPayroll_Users") = clsConfig._Notify_Payroll_Users
    '        Session("EFTIntegration") = clsConfig._EFTIntegration
    '        Session("DatafileName") = clsConfig._DatafileName
    '        Session("AccountingSoftWare") = clsConfig._AccountingSoftWare
    '        'Pinkal (24-May-2013) -- End

    '        'S.SANDEEP [ 29 May 2013 ] -- START
    '        'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '        Session("_LoanVocNoType") = clsConfig._LoanVocNoType
    '        Session("_PaymentVocNoType") = clsConfig._PaymentVocNoType
    '        Session("_DoNotAllowOverDeductionForPayment") = clsConfig._DoNotAllowOverDeductionForPayment
    '        Session("_IsDenominationCompulsory") = clsConfig._IsDenominationCompulsory
    '        Session("_SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
    '        'S.SANDEEP [ 29 May 2013 ] -- END


    '        'Pinkal (03-Jun-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("SavingsVocNoType") = clsConfig._SavingsVocNoType
    '        'Pinkal (03-Jun-2013) -- End

    '        'S.SANDEEP [ 09 AUG 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("AllowAssessor_Before_Emp") = clsConfig._AllowAssessor_Before_Emp
    '        'S.SANDEEP [ 09 AUG 2013 ] -- END

    '        'S.SANDEEP [ 13 AUG 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
    '        'S.SANDEEP [ 13 AUG 2013 ] -- END

    '        'S.SANDEEP [ 22 OCT 2013 ] -- START
    '        Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
    '        'S.SANDEEP [ 22 OCT 2013 ] -- END



    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes
    '        Session("PolicyManagementTNA") = clsConfig._PolicyManagementTNA
    '        'Pinkal (01-Feb-2014) -- End


    '        Dim objCompany As New clsCompany_Master
    '        objCompany._Companyunkid = ddlCompany.SelectedValue
    '        Session("gstrCompanyName") = objCompany._Name

    '        'S.SANDEEP [ 14 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Dim clsUser As New User
    '        Dim objPsWd As New clsPassowdOptions
    '        Session("IsEmployeeAsUser") = objPsWd._IsEmployeeAsUser
    '        'S.SANDEEP [ 14 DEC 2012 ] -- END



    '        'Pinkal (24-May-2013) -- Start
    '        'Enhancement : TRA Changes
    '        Session("CompanyBankGroupId") = objCompany._Bankgroupunkid
    '        Session("CompanyBankBranchId") = objCompany._Branchunkid
    '        'Pinkal (24-May-2013) -- End



    '        'S.SANDEEP [ 22 NOV 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Dim iUsrId As Integer = 0 : Dim objUsr As New clsUserAddEdit : Dim iValue As Integer = -1
    '        iUsrId = objUsr.Return_UserId(txtloginname.Text, "hrmsConfiguration", enLoginMode.USER)
    '        If iUsrId > 0 Then
    '            'S.SANDEEP [ 14 DEC 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            If Session("IsEmployeeAsUser") = True AndAlso rtbEmployee.Checked = True Then
    '                objUsr._Userunkid = iUsrId
    '                If CInt(ddlCompany.SelectedValue) <> objUsr._EmployeeCompanyUnkid Then
    '                    DisplayMessage.DisplayMessage("Sorry, No such employee exists in the selected company.", Me)
    '                    Exit Sub
    '                End If
    '            ElseIf Session("IsEmployeeAsUser") = True AndAlso rtbUser.Checked = True Then
    '                If clsUser.IsAccessGiven(iUsrId, Session("Fin_year")) = False Then
    '                    DisplayMessage.DisplayMessage("Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator.", Me)
    '                    Exit Sub
    '                End If
    '            End If
    '            'S.SANDEEP [ 14 DEC 2012 ] -- END
    '            Dim objPwdOpt As New clsPassowdOptions
    '            If objPwdOpt._IsUsrNameLengthSet = True Then
    '                If txtloginname.Text.Trim.Length < objPwdOpt._UsrNameLength Then
    '                    DisplayMessage.DisplayMessage("Username does not match with the policy set for username. Please change your username.", Me)
    '                    ModalPopupExtender3.Show()
    '                    Exit Sub
    '                End If
    '            End If
    '            If objPwdOpt._IsPasswordExpiredSet = True Then
    '                If Passwd_Change(iUsrId, objUsr, iValue) = True Then
    '                    txtUsername.Text = txtloginname.Text
    '                    ModalPopupExtender2.Show()
    '                    Exit Sub
    '                Else
    '                    If iValue <= 0 Then
    '                        Exit Sub
    '                    End If
    '                End If
    '            End If
    '            'Anjan [ 05 Feb 2013 ] -- Start
    '            'ENHANCEMENT : TRA CHANGES
    '            If objPwdOpt._IsPasswdPolicySet = True Then
    '                If objPwdOpt._IsPasswordLenghtSet Then
    '                    If txtPwd.Text.Length < objPwdOpt._PasswordLength Then
    '                        DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                        Session("ExUId") = iUsrId
    '                        txtUsername.Text = txtloginname.Text
    '                        ModalPopupExtender2.Show()
    '                        Exit Sub
    '                    End If
    '                End If

    '                If objPwdOpt._IsPasswdPolicySet Then
    '                    Dim mRegx As System.Text.RegularExpressions.Regex
    '                    If objPwdOpt._IsUpperCase_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[A-Z]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    If objPwdOpt._IsLowerCase_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[a-z]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    If objPwdOpt._IsNumeric_Mandatory Then
    '                        mRegx = New System.Text.RegularExpressions.Regex("[0-9]")
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                    If objPwdOpt._IsSpecalChars_Mandatory Then
    '                        'S.SANDEEP [ 07 MAY 2013 ] -- START
    '                        'ENHANCEMENT : TRA CHANGES
    '                        'mRegx = New System.Text.RegularExpressions.Regex("^.*[\[\]^$.|?*+()\\~`!@#%&\-_+={}'&quot;&lt;&gt;:;,\ ].*$")
    '                        mRegx = New System.Text.RegularExpressions.Regex("[^a-zA-Z0-9]")
    '                        'S.SANDEEP [ 07 MAY 2013 ] -- END
    '                        If mRegx.IsMatch(txtPwd.Text) = False Then
    '                            DisplayMessage.DisplayMessage("Password does not match with policy enforced. Please change your password now.", Me)
    '                            Session("ExUId") = iUsrId
    '                            txtUsername.Text = txtloginname.Text
    '                            ModalPopupExtender2.Show()
    '                            Exit Sub
    '                        End If
    '                    End If
    '                End If
    '            End If
    '            'Anjan [ 05 Feb 2013 ] -- End
    '        End If
    '        'S.SANDEEP [ 22 NOV 2012 ] -- END

    '        'S.SANDEEP [ 06 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'Dim clsuser As New User(txtloginname.Text, txtPwd.Text, IIf(rtbEmployee.Checked, Global.User.en_loginby.Employee, Global.User.en_loginby.User), mdbname)
    '        'Dim clsUser As New User
    '        'Dim objPsWd As New clsPassowdOptions
    '        'Session("IsEmployeeAsUser") = objPsWd._IsEmployeeAsUser
    '        clsUser = New User(txtloginname.Text, txtPwd.Text, IIf(rtbEmployee.Checked, Global.User.en_loginby.Employee, Global.User.en_loginby.User), mdbname)
    '        'S.SANDEEP [ 06 DEC 2012 ] -- END

    '        If clsUser.UserID = -1 And clsUser.Employeeunkid = -1 Then
    '            'lblLoginMsg.Text = clsuser._Message
    '            DisplayMessage.DisplayMessage(clsUser._Message, Me)
    '            Session("sessionexpired") = False
    '        Else
    '            Session("sessionexpired") = False
    '            Session("Login") = True
    '            Session("mdbname") = mdbname
    '            Session("UserId") = clsUser.UserID
    '            Session("Employeeunkid") = clsUser.Employeeunkid
    '            Session("UserName") = clsUser.UserName
    '            Session("LeaveBalances") = clsUser.LeaveBalances
    '            Session("MemberName") = clsUser.MemberName 'Sohail (04 Jun 2012)
    '            Session("RoleID") = clsUser.RoleUnkID 'Sohail (04 Jun 2012)
    '            'S.SANDEEP [ 20 NOV 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            If rtbEmployee.Checked = True Then
    '                HttpContext.Current.Session("LangId") = 1  ' by default set custom1 for employee login for all. 


    '            Else
    '                HttpContext.Current.Session("LangId") = clsUser.LanguageUnkid
    '            End If

    '            'S.SANDEEP [ 20 NOV 2012 ] -- END



    '            'Sohail (07 Dec 2013) -- Start
    '            'Enhancement - OMAN
    '            gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
    '            gobjLocalization._LangId = objUsr._Languageunkid
    '            'Sohail (07 Dec 2013) -- End


    '            'Pinkal (09-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("Firstname") = clsUser.Firstname
    '            Session("Surname") = clsUser.Surname
    '            'Pinkal (09-Nov-2012) -- End


    '            ' msql = " select employeeunkid,isnull(displayname,0) as loginname from " & Session("mdbname") & "..hremployee_master "
    '            msql = " select employeeunkid,(isnull(Firstname,'') + ' ' + isnull(othername,' ') + ' ' + isnull(surname ,' '))  as loginname from " & Session("mdbname") & "..hremployee_master "
    '            If rtbEmployee.Checked = True Then
    '                'S.SANDEEP [ 12 JULY 2013 ] -- START
    '                'ENHANCEMENT : OTHER CHANGES
    '                'msql &= "   where displayname = '" & txtloginname.Text.Trim & "' " & vbCrLf
    '                Dim sName As String = String.Empty
    '                sName = txtloginname.Text.Trim
    '                If sName.Contains("'") Then
    '                    sName = sName.Replace("'", "''")
    '                End If
    '                msql &= "   where displayname = '" & sName & "' " & vbCrLf
    '                'S.SANDEEP [ 12 JULY 2013 ] -- END
    '                Session("LoginBy") = Global.User.en_loginby.Employee
    '            Else
    '                Session("LoginBy") = Global.User.en_loginby.User
    '                'Aruti.Data.User._Object._Userunkid = clsuser.UserID


    '                'Sohail (11 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                Dim objUser As New clsUserAddEdit
    '                Dim mintUserUnkid As Integer
    '                mintUserUnkid = objUser.IsValidLogin(txtloginname.Text, txtPwd.Text, enLoginMode.USER, "hrmsConfiguration")
    '                If mintUserUnkid <= 0 AndAlso objUser._Message <> "" Then
    '                    'S.SANDEEP [ 09 NOV 2012 ] -- START
    '                    'ENHANCEMENT : TRA CHANGES
    '                    'DisplayMessage.DisplayMessage(objUser._Message, Me)
    '                    'Exit Sub
    '                    If objUser._ShowChangePasswdfrm = True AndAlso objUser._RetUserId > 0 Then
    '                        Session("ExUId") = objUser._RetUserId
    '                        radYesNo.Show()
    '                        Exit Sub
    '                    Else
    '                        DisplayMessage.DisplayMessage(objUser._Message, Me)
    '                        Exit Sub
    '                    End If
    '                    'S.SANDEEP [ 09 NOV 2012 ] -- END
    '                End If
    '                objUser = Nothing
    '                'Sohail (11 May 2012) -- End

    '            End If
    '            msql &= " order by loginname " & vbCrLf
    '            ds = clsDataOpr.WExecQuery(msql, "cffinancial_year_tran")

    '            objGlobalAccess.ListOfEmployee = ds.Tables(0)

    '            'gobjConfigOptions = New clsConfigOptions

    '            'gobjConfigOptions._Companyunkid = ddlCompany.SelectedValue

    '            'FinancialYear._Object._YearUnkid = CInt(Session("Fin_year"))

    '            'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'FinancialYear._Object._YearUnkid = CInt(Session("Fin_year"))
    '            'ConfigParameter._Object._Companyunkid = ddlCompany.SelectedValue
    '            'S.SANDEEP [ 27 APRIL 2012 ] -- END

    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Dim objUserPrivilege As New clsUserPrivilege
    '            objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
    '            Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
    '            Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
    '            Session("AllowtoApproveAdvance") = objUserPrivilege._AllowtoApproveAdvance
    '            'Sohail (23 Apr 2012) -- End

    '            Session("AddPendingLoan") = objUserPrivilege._AddPendingLoan

    '            'Sohail (18 May 2013) -- Start
    '            'TRA - ENHANCEMENT
    '            Session("AllowGlobalPayment") = objUserPrivilege._AllowGlobalPayment
    '            Session("DeletePayment") = objUserPrivilege._DeletePayment
    '            Session("AllowToViewGlobalVoidPaymentList") = objUserPrivilege._AllowToViewGlobalVoidPaymentList
    '            Session("AllowToApprovePayment") = objUserPrivilege._AllowToApprovePayment
    '            Session("AllowToVoidApprovedPayment") = objUserPrivilege._AllowToVoidApprovedPayment
    '            'Sohail (18 May 2013) -- End


    '            'Anjan (30 May 2012)-Start
    '            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '            Session("AddDependant") = objUserPrivilege._AddDependant
    '            Session("EditDependant") = objUserPrivilege._EditDependant
    '            Session("DeleteDependant") = objUserPrivilege._DeleteDependant

    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("ViewDependant") = objUserPrivilege._AllowToViewEmpDependantsList
    '            'Pinkal (22-Nov-2012) -- End



    '            Session("AddEmployeeAssets") = objUserPrivilege._AddEmployeeAssets
    '            Session("EditEmployeeAssets") = objUserPrivilege._EditEmployeeAssets
    '            Session("DeleteEmployeeAssets") = objUserPrivilege._DeleteEmployeeAssets

    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("ViewCompanyAssetList") = objUserPrivilege._AllowToViewCompanyAssetList
    '            'Pinkal (22-Nov-2012) -- End


    '            Session("AddEmployeeSkill") = objUserPrivilege._AddEmployeeSkill
    '            Session("EditEmployeeSkill") = objUserPrivilege._EditEmployeeSkill
    '            Session("DeleteEmployeeSkill") = objUserPrivilege._DeleteEmployeeSkill

    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("ViewEmpSkillList") = objUserPrivilege._AllowToViewEmpSkillList
    '            'Pinkal (22-Nov-2012) -- End



    '            Session("AddEmployeeQualification") = objUserPrivilege._AddEmployeeQualification
    '            Session("EditEmployeeQualification") = objUserPrivilege._EditEmployeeQualification
    '            Session("DeleteEmployeeQualification") = objUserPrivilege._DeleteEmployeeQualification

    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("ViewEmpQualificationList") = objUserPrivilege._AllowToViewEmpQualificationList
    '            'Pinkal (22-Nov-2012) -- End


    '            Session("AddEmployeeReferee") = objUserPrivilege._AddEmployeeReferee
    '            Session("EditEmployeeReferee") = objUserPrivilege._EditEmployeeReferee
    '            Session("DeleteEmployeeReferee") = objUserPrivilege._DeleteEmployeeReferee


    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("ViewEmpReferenceList") = objUserPrivilege._AllowToViewEmpReferenceList
    '            'Pinkal (22-Nov-2012) -- End


    '            Session("AddEmployeeExperience") = objUserPrivilege._AddEmployeeExperience
    '            Session("EditEmployeeExperience") = objUserPrivilege._EditEmployeeExperience
    '            Session("DeleteEmployeeExperience") = objUserPrivilege._DeleteEmployeeExperience


    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("ViewEmpExperienceList") = objUserPrivilege._AllowToViewEmpExperienceList
    '            'Pinkal (22-Nov-2012) -- End



    '            'Pinkal (19-Nov-2012) -- Start
    '            'Enhancement : TRA Changes

    '            Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
    '            Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
    '            Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense

    '            'Pinkal (19-Nov-2012) -- End


    '            'Pinkal (22-Nov-2012) -- Start
    '            'Enhancement : TRA Changes

    '            Session("AddEmployee") = objUserPrivilege._AddEmployee
    '            Session("EditEmployee") = objUserPrivilege._EditEmployee
    '            Session("DeleteEmployee") = objUserPrivilege._DeleteEmployee
    '            Session("ViewEmployee") = objUserPrivilege._AllowToViewEmpList
    '            Session("SetReinstatementdate") = objUserPrivilege._AllowtoSetEmpReinstatementDate
    '            Session("ChangeConfirmationDate") = objUserPrivilege._AllowtoChangeConfirmationDate
    '            Session("ChangeAppointmentDate") = objUserPrivilege._AllowtoChangeAppointmentDate
    '            Session("SetEmployeeBirthDate") = objUserPrivilege._AllowtoSetEmployeeBirthDate
    '            Session("SetEmpSuspensionDate") = objUserPrivilege._AllowtoSetEmpSuspensionDate
    '            Session("SetEmpProbationDate") = objUserPrivilege._AllowtoSetEmpProbationDate
    '            Session("SetEmploymentEndDate") = objUserPrivilege._AllowtoSetEmploymentEndDate
    '            Session("SetLeavingDate") = objUserPrivilege._AllowtoSetLeavingDate
    '            Session("ChangeRetirementDate") = objUserPrivilege._AllowtoChangeRetirementDate
    '            Session("ViewScale") = objUserPrivilege._AllowTo_View_Scale

    '            Session("ViewBenefitList") = objUserPrivilege._AllowToViewEmpBenefitList

    '            Session("AllowtoChangeBranch") = objUserPrivilege._AllowtoChangeBranch
    '            Session("AllowtoChangeDepartmentGroup") = objUserPrivilege._AllowtoChangeDepartmentGroup
    '            Session("AllowtoChangeDepartment") = objUserPrivilege._AllowtoChangeDepartment
    '            Session("AllowtoChangeSectionGroup") = objUserPrivilege._AllowtoChangeSectionGroup
    '            Session("AllowtoChangeSection") = objUserPrivilege._AllowtoChangeSection
    '            Session("AllowtoChangeUnitGroup") = objUserPrivilege._AllowtoChangeUnitGroup
    '            Session("AllowtoChangeUnit") = objUserPrivilege._AllowtoChangeUnit
    '            Session("AllowtoChangeTeam") = objUserPrivilege._AllowtoChangeTeam
    '            Session("AllowtoChangeJobGroup") = objUserPrivilege._AllowtoChangeJobGroup
    '            Session("AllowtoChangeJob") = objUserPrivilege._AllowtoChangeJob
    '            Session("AllowtoChangeGradeGroup") = objUserPrivilege._AllowtoChangeGradeGroup
    '            Session("AllowtoChangeGrade") = objUserPrivilege._AllowtoChangeGrade
    '            Session("AllowtoChangeGradeLevel") = objUserPrivilege._AllowtoChangeGradeLevel
    '            Session("AllowtoChangeClassGroup") = objUserPrivilege._AllowtoChangeClassGroup
    '            Session("AllowtoChangeClass") = objUserPrivilege._AllowtoChangeClass
    '            Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
    '            Session("AllowtoChangeCompanyEmail") = objUserPrivilege._AllowtoChangeCompanyEmail


    '            Session("AddLeaveForm") = objUserPrivilege._AddLeaveForm
    '            Session("EditLeaveForm") = objUserPrivilege._EditLeaveForm
    '            Session("DeleteLeaveForm") = objUserPrivilege._DeleteLeaveForm
    '            Session("DeleteLeaveForm") = objUserPrivilege._DeleteLeaveForm
    '            Session("ViewLeaveFormList") = objUserPrivilege._AllowToViewLeaveFormList
    '            Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave

    '            Session("ViewLeaveProcessList") = objUserPrivilege._AllowToViewLeaveProcessList
    '            Session("ChangeLeaveFormStatus") = objUserPrivilege._AllowChangeLeaveFormStatus
    '            Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance

    '            Session("AddPlanLeave") = objUserPrivilege._AddPlanLeave
    '            Session("EditPlanLeave") = objUserPrivilege._EditPlanLeave
    '            Session("DeletePlanLeave") = objUserPrivilege._DeletePlanLeave
    '            Session("ViewLeavePlannerList") = objUserPrivilege._AllowToViewLeavePlannerList

    '            Session("ViewPlannedLeaveViewer") = objUserPrivilege._AllowtoViewPlannedLeaveViewer
    '            Session("ViewLeaveViewer") = objUserPrivilege._AllowToViewLeaveViewer

    '            'Pinkal (22-Nov-2012) -- End






    '            'Anjan (30 May 2012)-End 

    '            'Pinkal (28-MAY-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
    '            Session("AllowToCancelPreviousDateLeave") = objUserPrivilege._AllowToCancelPreviousDateLeave
    '            'Pinkal (28-MAY-2012) -- End



    '            'Anjan (22 Nov 2012)-Start
    '            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    '            Session("AllowToSaveMedicalClaim") = objUserPrivilege._AllowToSaveMedicalClaim
    '            Session("AllowToFinalSaveMedicalClaim") = objUserPrivilege._AllowToFinalSaveMedicalClaim
    '            Session("AddMedicalClaim") = objUserPrivilege._AddMedicalClaim
    '            Session("EditMedicalClaim") = objUserPrivilege._EditMedicalClaim
    '            Session("DeleteMedicalClaim") = objUserPrivilege._DeleteMedicalClaim
    '            Session("AllowToExportMedicalClaim") = objUserPrivilege._AllowToExportMedicalClaim
    '            Session("AllowToCancelExportedMedicalClaim") = objUserPrivilege._AllowToCancelExportedMedicalClaim
    '            Session("AllowToAddMedicalSickSheet") = objUserPrivilege._AllowToAddMedicalSickSheet
    '            Session("AllowToEditMedicalSickSheet") = objUserPrivilege._AllowToEditMedicalSickSheet
    '            Session("AllowToDeleteMedicalSickSheet") = objUserPrivilege._AllowToDeleteMedicalSickSheet
    '            Session("AllowToPrintMedicalSickSheet") = objUserPrivilege._AllowToPrintMedicalSickSheet

    '            'Anjan (22 Nov 2012)-End 

    '            'S.SANDEEP [ 23 NOV 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            'ASSESSMENT
    '            Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._Allow_UnlockFinalSaveBSCPlanning
    '            Session("EditEmployeeAssessment") = objUserPrivilege._EditEmployeeAssessment
    '            Session("DeleteEmployeeAssessment") = objUserPrivilege._DeleteEmployeeAssessment
    '            Session("Allow_UnlockCommittedGeneralAssessment") = objUserPrivilege._Allow_UnlockCommittedGeneralAssessment
    '            Session("AllowToViewSelfAssessmentList") = objUserPrivilege._AllowToViewSelfAssessmentList
    '            Session("AddAssessmentAnalysis") = objUserPrivilege._AddAssessmentAnalysis
    '            Session("EditAssessmentAnalysis") = objUserPrivilege._EditAssessmentAnalysis
    '            Session("DeleteAssessmentAnalysis") = objUserPrivilege._DeleteAssessmentAnalysis
    '            Session("AllowToViewAssessorAssessmentList") = objUserPrivilege._AllowToViewAssessorAssessmentList
    '            Session("AllowToViewReviewerAssessmentList") = objUserPrivilege._AllowToViewReviewerAssessmentList
    '            Session("AllowToAddReviewerGeneralAssessment") = objUserPrivilege._AllowToAddReviewerGeneralAssessment
    '            Session("AllowToEditReviewerGeneralAssessment") = objUserPrivilege._AllowToEditReviewerGeneralAssessment
    '            Session("AllowToDeleteReviewerGeneralAssessment") = objUserPrivilege._AllowToDeleteReviewerGeneralAssessment
    '            Session("Allow_UnlockCommittedBSCAssessment") = objUserPrivilege._Allow_UnlockCommittedBSCAssessment
    '            Session("AllowToEditSelfBSCAssessment") = objUserPrivilege._AllowToEditSelfBSCAssessment
    '            Session("AllowToDeleteSelfBSCAssessment") = objUserPrivilege._AllowToDeleteSelfBSCAssessment
    '            Session("AllowToViewSelfAssessedBSCList") = objUserPrivilege._AllowToViewSelfAssessedBSCList
    '            Session("AllowToAddAssessorBSCAssessment") = objUserPrivilege._AllowToAddAssessorBSCAssessment
    '            Session("AllowToEditAssessorBSCAssessment") = objUserPrivilege._AllowToEditAssessorBSCAssessment
    '            Session("AllowToDeleteAssessorBSCAssessment") = objUserPrivilege._AllowToDeleteAssessorBSCAssessment
    '            Session("AllowToViewAssessorAssessedBSCList") = objUserPrivilege._AllowToViewAssessorAssessedBSCList
    '            Session("AllowToAddReviewerBSCAssessment") = objUserPrivilege._AllowToAddReviewerBSCAssessment
    '            Session("AllowToEditReviewerBSCAssessment") = objUserPrivilege._AllowToEditReviewerBSCAssessment
    '            Session("AllowToDeleteReviewerBSCAssessment") = objUserPrivilege._AllowToDeleteReviewerBSCAssessment
    '            Session("AllowToViewReviewerAssessedBSCList") = objUserPrivilege._AllowToViewReviewerAssessedBSCList
    '            'S.SANDEEP [ 23 NOV 2012 ] -- END



    '            'Pinkal (18-Dec-2012) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowtoMigrateLeaveApprovers") = objUserPrivilege._AllowtoMigrateLeaveApprover
    '            Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave
    '            Session("AllowToViewLeaveApproverList") = objUserPrivilege._AllowToViewLeaveApproverList
    '            Session("AddLeaveApprover") = objUserPrivilege._AddLeaveApprover
    '            Session("EditLeaveApprover") = objUserPrivilege._EditLeaveApprover
    '            Session("DeleteLeaveApprover") = objUserPrivilege._DeleteLeaveApprover
    '            Session("AllowToAssignIssueUsertoEmp") = objUserPrivilege._AllowToAssignIssueUsertoEmp
    '            Session("AllowToMigrateIssueUser") = objUserPrivilege._AllowToMigrateIssueUser
    '            'Pinkal (18-Dec-2012) -- End


    '            'Sohail (23 Nov 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            Session("AllowToViewProcessLoanAdvanceList") = objUserPrivilege._AllowToViewProcessLoanAdvanceList
    '            Session("AllowToViewLevel3EvaluationList") = objUserPrivilege._AllowToViewLevel3EvaluationList
    '            Session("AllowToAddLevelIIIEvaluation") = objUserPrivilege._AllowToAddLevelIIIEvaluation
    '            Session("AllowToEditLevelIIIEvaluation") = objUserPrivilege._AllowToEditLevelIIIEvaluation
    '            Session("AllowToDeleteLevelIIIEvaluation") = objUserPrivilege._AllowToDeleteLevelIIIEvaluation
    '            Session("AllowToSave_CompleteLevelIIIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIIIEvaluation
    '            'Sohail (23 Nov 2012) -- End

    '            'Pinkal (04-Mar-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowtoEndELC") = objUserPrivilege._AllowToEndLeaveCycle
    '            'Pinkal (04-Mar-2013) -- End


    '            'Pinkal (01-Apr-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowToAddEditPhoto") = objUserPrivilege._AllowToAddEditPhoto
    '            Session("AllowToDeletePhoto") = objUserPrivilege._AllowToDeletePhoto
    '            'Pinkal (01-Apr-2013) -- End



    '            'Pinkal (06-Apr-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowMapApproverWithUser") = objUserPrivilege._AllowMapApproverWithUser
    '            Session("AllowtoMapLeaveType") = objUserPrivilege._AllowtoMapLeaveType
    '            'Pinkal (06-Apr-2013) -- End



    '            'Pinkal (30-Apr-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowToViewScale") = objUserPrivilege._AllowTo_View_Scale
    '            'Pinkal (30-Apr-2013) -- End



    '            'Pinkal (24-May-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowToViewBatchPostingList") = objUserPrivilege._AllowToViewBatchPostingList
    '            Session("AllowToAddBatchPosting") = objUserPrivilege._AllowToAddBatchPosting
    '            Session("AllowToEditBatchPosting") = objUserPrivilege._AllowToEditBatchPosting
    '            Session("AllowToDeleteBatchPosting") = objUserPrivilege._AllowToDeleteBatchPosting
    '            Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
    '            Session("AllowToPostBatchPostingToED") = objUserPrivilege._AllowToPostBatchPostingToED
    '            'Pinkal (24-May-2013) -- End

    '            'S.SANDEEP [ 29 May 2013 ] -- START
    '            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    '            Session("AllowToViewProceedingApprovalList") = objUserPrivilege._AllowToViewProceedingApprovalList
    '            Session("AllowToViewLoanAdvanceList") = objUserPrivilege._AllowToViewLoanAdvanceList
    '            Session("DeleteLoanAdvance") = objUserPrivilege._DeleteLoanAdvance
    '            Session("EditLoanAdvance") = objUserPrivilege._EditLoanAdvance
    '            Session("AllowChangeLoanAvanceStatus") = objUserPrivilege._AllowChangeLoanAvanceStatus
    '            Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
    '            Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived

    '            Session("Is_Report_Assigned") = clsArutiReportClass.Is_Report_Assigned(enArutiReport.BBL_Loan_Report, Session("UserId"), Session("CompanyUnkId"))
    '            Session("EditPendingLoan") = objUserPrivilege._EditPendingLoan
    '            Session("DeletePendingLoan") = objUserPrivilege._DeletePendingLoan
    '            Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
    '            Session("AllowChangePendingLoanAdvanceStatus") = objUserPrivilege._AllowChangePendingLoanAdvanceStatus
    '            Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
    '            Session("EditLoanAdvancePayment") = objUserPrivilege._EditLoanAdvancePayment
    '            Session("DeleteLoanAdvancePayment") = objUserPrivilege._DeleteLoanAdvancePayment
    '            Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
    '            Session("EditLoanAdvanceReceived") = objUserPrivilege._EditLoanAdvanceReceived
    '            Session("DeleteLoanAdvanceReceived") = objUserPrivilege._DeleteLoanAdvanceReceived
    '            Session("AddSavingsPayment") = objUserPrivilege._AddSavingsPayment
    '            Session("EditSavingsPayment") = objUserPrivilege._EditSavingsPayment
    '            Session("DeleteSavingsPayment") = objUserPrivilege._DeleteSavingsPayment
    '            Session("AddPayment") = objUserPrivilege._AddPayment
    '            Session("DeletePayment") = objUserPrivilege._DeletePayment
    '            Session("AllowToViewPaymentList") = objUserPrivilege._AllowToViewPaymentList
    '            Session("AddCashDenomination") = objUserPrivilege._AddCashDenomination
    '            Session("AddPendingLoan") = objUserPrivilege._AddPendingLoan
    '            'S.SANDEEP [ 29 May 2013 ] -- END

    '            'Pinkal (03-Jun-2013) -- Start
    '            'Enhancement : TRA Changes

    '            Session("AllowToViewEmployeeSavingsList") = objUserPrivilege._AllowToViewEmployeeSavingsList
    '            Session("AddEmployeeSavings") = objUserPrivilege._AddEmployeeSavings
    '            Session("EditEmployeeSavings") = objUserPrivilege._EditEmployeeSavings
    '            Session("DeleteEmployeeSavings") = objUserPrivilege._DeleteEmployeeSavings
    '            Session("AllowChangeSavingStatus") = objUserPrivilege._AllowChangeSavingStatus

    '            'Pinkal (03-Jun-2013) -- End


    '            'Pinkal (12-Jun-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
    '            Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
    '            'Pinkal (12-Jun-2013) -- End


    '            'Pinkal (18-Jun-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
    '            Session("AllowToViewEmpEDList") = objUserPrivilege._AllowToViewEmpEDList
    '            Session("AddEarningDeduction") = objUserPrivilege._AddEarningDeduction
    '            Session("EditEarningDeduction") = objUserPrivilege._EditEarningDeduction
    '            Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
    '            'Pinkal (18-Jun-2013) -- End
    '            'Sohail (24 Dec 2013) -- Start
    '            'Enhancement - Send link in salary change notification
    '            Session("AllowToViewSalaryChangeList") = objUserPrivilege._AllowToViewSalaryChangeList
    '            Session("AllowToApproveSalaryChange") = objUserPrivilege._AllowToApproveSalaryChange
    '            'Sohail (24 Dec 2013) -- End

    '            'S.SANDEEP [ 23 JULY 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            Session("Allow_FinalSaveBSCPlanning") = objUserPrivilege._Allow_FinalSaveBSCPlanning
    '            'S.SANDEEP [ 23 JULY 2013 ] -- END


    '            'Pinkal (20-Sep-2013) -- Start
    '            'Enhancement : TRA Changes
    '            Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave
    '            Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave
    '            'Pinkal (20-Sep-2013) -- End

    '            'S.SANDEEP [ 28 OCT 2013 ] -- START
    '            Session("AllowToMigrateAssessor_Reviewer") = objUserPrivilege._AllowToMigrateAssessor_Reviewer
    '            'S.SANDEEP [ 28 OCT 2013 ] -- END


    '            'S.SANDEEP [ 31 DEC 2013 ] -- START
    '            Session("AllowToApproveEmployee") = objUserPrivilege._AllowToApproveEmployee
    '            Session("AllowtoApproveApplicantEligibility") = objUserPrivilege._AllowtoApproveApplicantEligibility
    '            Session("AllowtoDisapproveApplicantEligibility") = objUserPrivilege._AllowtoDisapproveApplicantEligibility
    '            Session("AllowtoViewInterviewAnalysisList") = objUserPrivilege._AllowtoViewInterviewAnalysisList
    '            Session("AllowToViewShortListApplicants") = objUserPrivilege._AllowToViewShortListApplicants
    '            Session("AllowToApproveApplicantFilter") = objUserPrivilege._AllowToApproveApplicantFilter
    '            Session("AllowToRejectApplicantFilter") = objUserPrivilege._AllowToRejectApplicantFilter
    '            Session("AllowToApproveFinalShortListedApplicant") = objUserPrivilege._AllowToApproveFinalShortListedApplicant
    '            Session("AllowToDisapproveFinalShortListedApplicant") = objUserPrivilege._AllowToDisapproveFinalShortListedApplicant
    '            'S.SANDEEP [ 31 DEC 2013 ] -- END


    '            'S.SANDEEP [ 05 MARCH 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            Dim objMaster As New clsMasterData


    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'UserAccessLevel._AccessLevel = objMaster.GetUserAccessLevel
    '            Session("UserAccessLevel") = objMaster.GetUserAccessLevel(CInt(Session("UserId")), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("UserAccessModeSetting"))
    '            'S.SANDEEP [ 21 MAY 2012 ] -- START
    '            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    '            If Session("LoginBy") = Global.User.en_loginby.Employee Then
    '                Session("UserAccessLevel") = ""
    '            End If
    '            'S.SANDEEP [ 21 MAY 2012 ] -- END
    '            If Session("UserAccessLevel") = "" Then
    '                Session("AccessLevelFilterString") = " AND 1 = 1 "

    '            Else
    '                'Select Case CInt(Session("UserAccessModeSetting"))
    '                '    Case enAllocation.BRANCH
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.stationunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.DEPARTMENT_GROUP
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.deptgroupunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.DEPARTMENT
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.departmentunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.SECTION_GROUP
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.sectiongroupunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.SECTION
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.sectionunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.UNIT_GROUP
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.unitgroupunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.UNIT
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.unitunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.TEAM
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.teamunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.JOB_GROUP
    '                '        Session("AccessLevelFilterString") = " AND  hremployee_master.jobgroupunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case enAllocation.JOBS
    '                '        Session("AccessLevelFilterString") = " AND hremployee_master.jobunkid IN (" & Session("UserAccessLevel") & ") "
    '                '    Case Else
    '                '        Session("AccessLevelFilterString") = " AND 1 = 1 "
    '                'End Select
    '                Session("AccessLevelFilterString") = UserAccessLevel._AccessLevelFilterString
    '            End If
    '            'Sohail (23 Apr 2012) -- End

    '            'S.SANDEEP [ 05 MARCH 2012 ] -- END

    '            If rtbUser.Checked = True Then
    '                objGlobalAccess.userid = clsUser.UserID
    '                'Aruti.Data.User._Object._Userunkid = clsuser.UserID
    '            Else
    '                objGlobalAccess.employeeid = clsUser.Employeeunkid

    '            End If
    '            Session("clsuser") = clsUser
    '            Session("objGlobalAccess") = objGlobalAccess

    '            'Pinkal (12-Oct-2011) -- Start
    '            'Enhancement : TRA Changes
    '            'Sohail (23 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'GUI.fmtCurrency = ConfigParameter._Object._CurrencyFormat
    '            'Sohail (23 Apr 2012) -- End
    '            'Pinkal (12-Oct-2011) -- End

    '            'Sohail (20 Apr 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'eZeeCommonLib.eZeeDatabase.change_database(mdbname)
    '            Session("mdbname") = mdbname
    '            'Sohail (20 Apr 2012) -- End



    '            'Pinkal (03-Sep-2012) -- Start
    '            'Enhancement : TRA Changes

    '            Try
    '                If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
    '                    'Session("IP_ADD") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR")).AddressList(0).ToString
    '                    'Session("HOST_NAME") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR").ToString).HostName.ToString()
    '                    Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
    '                    Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
    '                Else
    '                    Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
    '                    Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
    '                End If

    '            Catch ex As Exception
    '                Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
    '                Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
    '            End Try


    '            'Pinkal (03-Sep-2012) -- End





    '            'S.SANDEEP [ 21 MAY 2012 ] -- START
    '            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    '            If Session("LoginBy") = Global.User.en_loginby.Employee Then
    '                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
    '            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
    '                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
    '            End If
    '            'S.SANDEEP [ 21 MAY 2012 ] -- END



    '            'S.SANDEEP [ 04 July 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            '                Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
    '            '                Session("HOST_NAME") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR")).HostName.ToString
    '            'S.SANDEEP [ 04 July 2012 ] -- END


    '            If Request.QueryString.Count > 0 Then
    '                If Request.QueryString("setup").ToLower = "database" Then
    '                    Dim ClsComCode As New CommonCodes
    '                    ClsComCode.PkgDtaStrCompany()
    '                End If
    '            End If

    '            'Anjan [ 25 Apr 2013 ] -- Start
    '            'ENHANCEMENT : License CHANGES

    '            If ConfigParameter._Object._IsExpire Then
    '                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page)
    '                Exit Try
    'Else
    '                'If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
    '                '    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page)
    '                '    Exit Try
    '                'End If

    '                If ConfigParameter._Object._IsArutiDemo = True Then 'IsDemo
    '                    DisplayMessage.DisplayMessage("You have " & ConfigParameter._Object._DaysLeft & " days Left after today.\n\nThank you for evaluating Aruti. \n\nTo obtain a commercial license contact us at aruti@aruti.com today.\n\nFor more information visit us at www.aruti.com.", Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
    '                Else 'IsLicence
    '                    Response.Redirect("~/UserHome.aspx", False)
    '                End If
    'End If
    '            'Anjan [ 25 Apr 2013 ] -- End




    '        End If

    '    Catch ex As Exception
    '        'Throw ex
    '        DisplayMessage.DisplayError("Error in BtnLogin_click event" & ex.Message.ToString, Me)
    '    Finally
    '    End Try

    'End Sub
#End Region
#End Region

#Region " Combobox Events "
    'Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
    '    Try
    '        If rtbEmployee.Checked = True Then
    '            BindCompany()
    '            BindEmployee()
    '        Else
    '            BindCompany()
    '            BindUser()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Error In ddlYear_SelectedIndexChanged", Me)
    '    End Try
    'End Sub

    Protected Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        Try
            'If rtbEmployee.Checked = True Then
            '    BindEmployee()
            'Else
            '    BindUser()
            'End If

            'Sohail (30 Mar 2015) -- Start
            'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
            'GetCompanyYearInfo(ddlCompany.SelectedValue)
            Dim strError As String = ""
            If GetCompanyYearInfo(strError, ddlCompany.SelectedValue) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Sub
            End If
            'Sohail (30 Mar 2015) -- End
            'txtfinyear.Text = Session("FinancialYear_Name") 'Sohail (17 Dec 2018)
            mdbname = Session("Database_Name")

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            HttpContext.Current.Session("mdbname") = mdbname
            'Sohail (23 Apr 2012) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("Error In ddlCompany_SelectedIndexChanged Event", Me)
        End Try
    End Sub
#End Region

#Region " Radio Butttons Events "


    'Pinkal (12-Feb-2015) -- Start
    'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    Protected Sub rtbEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rtbEmployee.CheckedChanged
        Try
            If rtbUser.Checked Then
                rtbUser.Checked = False
                rtbEmployee.Checked = True
                'S.SANDEEP [ 12 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lnkForgotPassword.Visible = True
                'S.SANDEEP [ 12 SEP 2012 ] -- END
            Else
                rtbUser.Checked = True
                rtbEmployee.Checked = False
            End If
            If rtbEmployee.Checked = True Then
                rtbUser.Checked = False
                ' ddlyear.Visible = True
                'Sohail (17 Dec 2018) -- Start
                'lblYear.Visible = True
                'lblcompany.Visible = True
                'Sohail (17 Dec 2018) -- End
                ddlCompany.Visible = True
                '  BindYear()
                '''''BindCompany()
                ' BindEmployee()

                'Sohail (02 May 2014) -- Start
                'Enhancement - Hide companies list if access not given to users.
                lblEmployeeCode.Visible = True
                txtEmpCode.Visible = True
                'Sohail (02 May 2014) -- End

            Else
                rtbEmployee.Checked = False
                ' ddlYear.Visible = False
                ' ddlCompany.Visible = False
                'lblYear.Visible = False
                ' lblcompany.Visible = False
                '' BindUser()

            End If

            'Sohail (02 May 2014) -- Start
            'Enhancement - Hide companies list if access not given to users.
            LblCompanyCode.Visible = True
            txtCompanyCode.Visible = True
            If mintOneCompanyUnkId > 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "hideccode", "hidecode(true);", True)
                rfvCompanyCode.Enabled = False
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "hideccode", "hidecode(false);", True)
                rfvCompanyCode.Enabled = True
            End If
            'Sohail (02 May 2014) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in rtbEmployee_CheckedChanged Event", Me)
        End Try
    End Sub

    Protected Sub rtbuser_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rtbUser.CheckedChanged
        Try
            If rtbUser.Checked Then
                rtbUser.Checked = True

                'S.SANDEEP [ 12 SEP 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lnkForgotPassword.Visible = False
                'S.SANDEEP [ 12 SEP 2012 ] -- END

                rtbEmployee.Checked = False
            Else
                rtbUser.Checked = False
                rtbEmployee.Checked = True
            End If
            If rtbEmployee.Checked = True Then
                rtbUser.Checked = False
                '  ddlyear.Visible = True
                ddlCompany.Visible = True
                'Sohail (17 Dec 2018) -- Start
                'lblYear.Visible = True
                'lblcompany.Visible = True
                'Sohail (17 Dec 2018) -- End
                ' BindYear()
                ''BindCompany()
                'BindEmployee()

                'Sohail (02 May 2014) -- Start
                'Enhancement - Hide companies list if access not given to users.
                lblEmployeeCode.Visible = True
                txtEmpCode.Visible = True
                'Sohail (02 May 2014) -- End
            Else
                rtbEmployee.Checked = False
                '  ddlYear.Visible = False
                ' ddlCompany.Visible = False
                ' lblYear.Visible = False
                'lblcompany.Visible = False
                ' BindUser()
            End If
            'Sohail (02 May 2014) -- Start
            'Enhancement - Hide companies list if access not given to users.
            LblCompanyCode.Visible = False
            txtCompanyCode.Visible = False
            rfvCompanyCode.Enabled = False
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "hideccode", "hidecode(true);", True)
            'Sohail (02 May 2014) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error In rtbuser_CheckedChanged Event", Me)
        End Try
    End Sub

    'Pinkal (12-Feb-2015) -- End

    'S.SANDEEP [ 09 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Protected Sub radYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYesNo.buttonNo_Click
        Try
            radYesNo.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("Procedure : radYesNo_buttonNo_Click : " & ex.Message.ToString, Me)
        End Try
    End Sub

    Protected Sub radYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYesNo.buttonYes_Click
        Try
            txtUsername.Text = txtloginname.Text
            ModalPopupExtender2.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("Procedure : radYesNo_buttonYes_Click : " & ex.Message.ToString, Me)
        End Try
    End Sub
    'S.SANDEEP [ 09 NOV 2012 ] -- END
#End Region

#Region " Other Controls Events "

    'S.SANDEEP [ 12 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Protected Sub lnkForgotPassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkForgotPassword.Click
        txtEmpCode.Text = ""
        ModalPopupExtender1.Show()
    End Sub
    'S.SANDEEP [ 12 SEP 2012 ] -- END

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew

    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)
            'Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.btnCancelCP.Text = Language._Object.getCaption("btnCancel", Me.btnCancelCP.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")

            Me.lblOldPwd.Text = Language._Object.getCaption(Me.lblOldPwd.ID, Me.lblOldPwd.Text)
            Me.lblConfirmPwd.Text = Language._Object.getCaption(Me.lblConfirmPwd.ID, Me.lblConfirmPwd.Text)
            Me.lblNewPwd.Text = Language._Object.getCaption(Me.lblNewPwd.ID, Me.lblNewPwd.Text)
            Me.lblUserCP.Text = Language._Object.getCaption("lblUser", Me.lblUserCP.Text)

            Language.setMessage(mstrModuleNameLogin, 8, "Sorry, We are not able to connect aruti database server. Please check if database server is reachable or try again after some time or contact aruti support team.")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : SetLanguage : " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014 ] -- End




End Class
