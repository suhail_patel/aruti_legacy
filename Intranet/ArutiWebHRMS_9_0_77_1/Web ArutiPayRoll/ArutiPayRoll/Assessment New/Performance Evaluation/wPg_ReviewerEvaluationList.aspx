﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ReviewerEvaluationList.aspx.vb"
    Inherits="Assessment_New_Performance_Evaluation_wPg_ReviewerEvaluationList" Title="Performance Evaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, args) {
            SetGeidScrolls();
        }
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Performance Evaluation"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblReviewer" runat="server" Text="Reviewer"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboReviewer" runat="server" Width="95%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblYear" runat="server" Text="Year"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboYear" runat="server" Width="95%">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="95%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="95%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblViewType" runat="server" Text="View Type"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboViewType" runat="server" Width="95%">
                                                </asp:DropDownList>
                                                <asp:CheckBox ID="chkShowcommitted" runat="server" Text="Show committed" Visible="false" />
                                                <asp:CheckBox ID="chkShowUncommitted" runat="server" Text="Show Uncommitted" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="Start New Assessment" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search for Existing Assessment" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" Width="90px" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" Width="90px" />
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnl_lbassesslist" runat="server" Width="100%" ScrollBars="Auto" CssClass="gridscroll">
                                <asp:DataGrid ID="lvAssesmentList" runat="server" AutoGenerateColumns="false" Width="99%"
                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:BoundColumn DataField="viewmode" HeaderText="Mode" FooterText="" />
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="lnkAssessEmp" runat="server" CommandName="Assess" Text="Assess Employee"
                                                        Style="text-decoration: none;"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="imgDelete" runat="server" CssClass="griddelete" CommandName="Delete"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="imgUnlock" runat="server" CssClass="gridiconunlock" CommandName="Unlock"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="employee" HeaderText="Employee" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="year" HeaderText="Year" FooterText="" />
                                        <asp:BoundColumn DataField="rl_assessmentdate" HeaderText="Assessment Date" FooterText="" />
                                        <asp:BoundColumn DataField="sl_score" HeaderText="Self Score" FooterText="" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right" />
                                        <asp:BoundColumn DataField="al_score" HeaderText="Assessor Score" FooterText="" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right" />
                                        <asp:BoundColumn DataField="rl_score" HeaderText="Reviewer Score" FooterText="" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right" />
                                        <asp:BoundColumn DataField="sl_analysisunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="al_analysisunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="rl_analysisunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="periodunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="yearunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="statusunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="assessgroupunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="assessmodeid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="smodeid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="iscommitted" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="assessormasterunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="assessoremployeeunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="userunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="reviewerunkid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="operationviewid" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="sl_iscommitted" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="al_iscommitted" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="rl_iscommitted" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="sl_committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="al_committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="rl_committeddatetime" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="sl_assessmentdate" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="al_assessmentdate" HeaderText="" FooterText="" Visible="false" />
                                        <asp:BoundColumn DataField="assessmentdate" HeaderText="" FooterText="" Visible="false" />
                                        <%--'S.SANDEEP |19-AUG-2020| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : Language Changes--%>
                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Info"
                                            ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="lnkDescription" runat="server" ToolTip="View Rating Info" OnClick="lnkInfo_Click"><i class="fa fa-info-circle" style="font-size:19px; color:Blue;"></i></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <%--'S.SANDEEP |19-AUG-2020| -- END--%>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:Panel>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_YesNo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 300px" DefaultButton="btnYes">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="Title" />
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div3" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblMessage" runat="server" Text="Message :" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="3" Style="resize: none"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                            <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                                            <asp:HiddenField ID="hdf_analysisunkid" runat="server" />
                                            <asp:HiddenField ID="hdf_PeriodID" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popup_ComputeYesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btn_ValidNo" PopupControlID="pnl_ComputeValid" TargetControlID="hdf_Valid">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ComputeValid" runat="server" CssClass="newpopup" Style="display: none;
                        width: 500px" DefaultButton="btn_ValidYes">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblComputeTitle" runat="server" Text="Title"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div14" class="panel-default">
                                    <div id="Div17" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblComputeMessage" runat="server" Text="Message :"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtVoidMessage" runat="server" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btn_ValidYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                            <asp:Button ID="btn_ValidNo" runat="server" Text="No" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_Valid" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HiddenField ID="hdf_ItemIndex" runat="server" />
                    <asp:HiddenField ID="hdf_CommondName" runat="server" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc3:ConfirmYesNo ID="cnfEditOperation" runat="server" />
                    <%--'S.SANDEEP |19-AUG-2020| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : Language Changes--%>
                    <cc1:ModalPopupExtender ID="popupinfo" BackgroundCssClass="modalBackground" TargetControlID="lblRatingInfo"
                        runat="server" PopupControlID="pnlRatingInfo" CancelControlID="btnCloseRating" />
                    <asp:Panel ID="pnlRatingInfo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 500px; top: 30px;" Height="313px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblRatingInfo" runat="server" Text="Ratings Information"></asp:Label>
                            </div>
                            <div class="panel-body" style="height: 274px; overflow: auto">
                                <div id="Div6" class="panel-default">
                                    <div id="Div7" style="width: 99%; height: 200px; overflow: auto;">
                                        <asp:GridView ID="gvRating" runat="server" AutoGenerateColumns="False" Width="99%"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="scrf" HeaderText="Score From" FooterText="dgcolhScrFrm"
                                                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="scrt" HeaderText="Score To" FooterText="dgcolhScrTo" HeaderStyle-Width="100px"
                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <asp:Button ID="btnCloseRating" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--'S.SANDEEP |19-AUG-2020| -- END--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
