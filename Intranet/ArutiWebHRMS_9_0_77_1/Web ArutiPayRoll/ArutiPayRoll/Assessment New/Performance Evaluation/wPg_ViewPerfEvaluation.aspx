﻿<%@ Page Title="View Performance Evaluation" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_ViewPerfEvaluation.aspx.vb" Inherits="wPg_ViewPerfEvaluation" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfDialog" TagPrefix="cf1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);
        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="View Performance Evaluation"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;" id="Details">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <%--'S.SANDEEP |12-FEB-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Performance Assessment Changes}--%>
                                                <%--AutoPostBack="true"--%>    
                                                <%--'S.SANDEEP |12-FEB-2019| -- END--%>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <%--'S.SANDEEP |12-FEB-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Performance Assessment Changes}--%>
                                                <%--AutoPostBack="true"--%>    
                                                <%--'S.SANDEEP |12-FEB-2019| -- END--%>
                                            </td>
                                            <td align="right" style="width: 20%">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" Width="77px" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" Width="77px" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="objlblCaption" runat="server" Text="#value"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div3" class="panel-body-default">
                                    <table id="tblGrids" style="width: 100%">
                                        <tr style="width: 100%;" id="Grids">
                                            <td style="width: 100%">
                                                <asp:Panel ID="objpnlBSC" runat="server" Visible="false">
                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                        max-width: 1130px; height: 450px; overflow: auto" class="gridscroll">
                                                        <asp:DataGrid ID="dgvBSC" runat="server" Width="200%" AutoGenerateColumns="false"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="Field1" HeaderText="" FooterText="objdgcolhBSCField1" />
                                                                <asp:BoundColumn DataField="Field2" HeaderText="" FooterText="objdgcolhBSCField2" />
                                                                <asp:BoundColumn DataField="Field3" HeaderText="" FooterText="objdgcolhBSCField3" />
                                                                <asp:BoundColumn DataField="Field4" HeaderText="" FooterText="objdgcolhBSCField4" />
                                                                <asp:BoundColumn DataField="Field5" HeaderText="" FooterText="objdgcolhBSCField5" />
                                                                <asp:BoundColumn DataField="Field6" HeaderText="" FooterText="objdgcolhBSCField6" />
                                                                <asp:BoundColumn DataField="Field7" HeaderText="" FooterText="objdgcolhBSCField7" />
                                                                <asp:BoundColumn DataField="Field8" HeaderText="" FooterText="objdgcolhBSCField8" />
                                                                <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="dgcolhSDate" />
                                                                <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="dgcolhEDate" />
                                                                <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="dgcolhCompleted" />
                                                                <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhBSCWeight"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="eself" HeaderText="Self - Score" FooterText="dgcolheselfBSC"
                                                                    ItemStyle-HorizontalAlign="Center" />
                                                                <asp:BoundColumn DataField="eremark" HeaderText="Self - Remark" FooterText="dgcolheremarkBSC" />
                                                                <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" FooterText="dgcolhaselfBSC" />
                                                                <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkBSC" />
                                                                <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreBSC" />
                                                                <asp:BoundColumn DataField="rself" HeaderText="Reviewer - Score" FooterText="dgcolhrselfBSC" />
                                                                <asp:BoundColumn DataField="rremark" HeaderText="Reviewer - Remark" FooterText="dgcolhrremarkBSC" />
                                                                <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpBSC" Visible="false" />
                                                                <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdBSC" Visible="false" />
                                                                <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhScaleMasterId"
                                                                    Visible="false" />
                                                                <%--'S.SANDEEP |12-MAR-2019| -- START--%>
                                                                <%--'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}--%>
                                                                <%--<asp:BoundColumn DataField="dgoaltype" HeaderText="Goal Type" FooterText="dgcolhgoaltype" />
                                                                <asp:BoundColumn DataField="dgoalvalue" HeaderText="Goal Value" FooterText="dgcolhgoalvalue" />--%>
                                                                <%--'S.SANDEEP |12-MAR-2019| -- END--%>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="objpnlGE" runat="server" Visible="false">
                                                    <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                                        max-width: 1130px; overflow: auto" class="gridscroll">
                                                        <asp:DataGrid ID="dgvGE" runat="server" Width="99.5%" AutoGenerateColumns="false"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                                    ItemStyle-Width="35%" HeaderStyle-Width="35%" />
                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                                    Visible="false" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="eself" HeaderText="Self - Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolheselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="escore" HeaderText="Final Score" FooterText="objdgcolhedisplayGE"
                                                                    ItemStyle-HorizontalAlign="Right" Visible="false" />
                                                                <asp:BoundColumn DataField="eremark" HeaderText="Self - Remark" FooterText="dgcolheremarkGE"
                                                                    ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                                <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolhaselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="ascore" HeaderText="Final Score" FooterText="objdgcolhadisplayGE"
                                                                    ItemStyle-HorizontalAlign="Right" Visible="false" />
                                                                <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkGE"
                                                                    ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                                <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreGE"
                                                                    ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="rself" HeaderText="Reviewer - Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolhrselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="rscore" HeaderText="Final Score" FooterText="objdgcolhrdisplayGE"
                                                                    ItemStyle-HorizontalAlign="Right" Visible="false" />
                                                                <asp:BoundColumn DataField="rremark" HeaderText="Reviewer - Remark" FooterText="dgcolhrremarkGE"
                                                                    ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                                <asp:BoundColumn DataField="scalemasterunkid" HeaderText="" FooterText="objdgcolhscalemasterunkidGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="competenciesunkid" HeaderText="" FooterText="objdgcolhcompetenciesunkidGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="assessgroupunkid" HeaderText="" FooterText="objdgcolhassessgroupunkidGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                                <asp:BoundColumn DataField="IsPGrp" HeaderText="" FooterText="objdgcolhIsPGrpGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                            Font-Underline="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="objpnlCItems" runat="server" Visible="false">
                                                    <div id="scrollable-container2" onscroll="$(scroll2.Y).val(this.scrollTop);" style="width: 100%;
                                                        max-width: 1130px; height: 450px; overflow: auto" class="gridscroll">
                                                        <asp:GridView ID="dgvItems" runat="server" Width="99%" AutoGenerateColumns="false"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;" id="NextPribtn">
                                            <td>
                                                <table id="Table1" style="width: 100%;" runat="server">
                                                    <tr style="width: 100%;">
                                                        <td align="left" style="width: 43%">
                                                            <table style="width: 100%; border-color: #DDD" border="1" cellspacing="0">
                                                                <tr style="width: 100%;">
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue1" runat="server" Width="100%" Text="" Font-Bold="true"
                                                                            Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue2" runat="server" Width="100%" Text="" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue3" runat="server" Width="100%" Text="" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue4" runat="server" Width="100%" Text="" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:Button ID="btnBack" runat="server" Text="Previous" CssClass="btnDefault" Width="25px" />
                                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnDefault" Width="25px" />
                                        </div>
                                        <div>
                                            <asp:Button ID="btnAppRejAssessment" runat="server" Text="Agree/Disagree Assessmnet"
                                                CssClass="btndefault" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_ComInfo" runat="server" BackgroundCssClass="ModalPopupBG2"
                        CancelControlID="btnSClose" PopupControlID="pnl_CompInfo" TargetControlID="hdnfieldDelReason">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_CompInfo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px; z-index: 100002!important;">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="Label2" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div14" class="panel-default">
                                    <div id="Div17" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblCompInfoHeader" runat="server" Text="Description"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtData" runat="server" TextMode="MultiLine" Width="97%" Height="100px"
                                                        Style="margin-bottom: 5px;" ReadOnly="true" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSClose" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                                CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popup_AppRejAssessment" runat="server" BackgroundCssClass="ModalPopupBG2"
                        CancelControlID="btnAppRejAss_Close" PopupControlID="pnl_AppRejAssessment" TargetControlID="hdfAppRejAss">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_AppRejAssessment" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px;">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblApprejAssessmentHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div class="panel-default">
                                    <div id="Div34" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblAppRejAssessment_AssessorPart" runat="server" Text="Assessor Part" />
                                        </div>
                                    </div>
                                    <div class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblAppRejAssessement_Assessor_Status" runat="server" Text="Status" />
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:RadioButton ID="radAssessment_Assessor_Approve" runat="server" Text="Agree"
                                                        GroupName="Assessment_Assessor" />
                                                    <asp:RadioButton ID="radAssessment_Assessor_Reject" runat="server" Text="Disagree"
                                                        GroupName="Assessment_Assessor" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%; vertical-align: text-top">
                                                    <asp:Label ID="lblAppRejAssessement_Assessor_Remark" runat="server" Text="Remark" />
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:TextBox ID="txtAppRejAssessment_Assessor_Remark" runat="server" TextMode="MultiLine"
                                                        Rows="3"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlAppRejAssessment_Reviewer" runat="server">
                                    <div class="panel-default">
                                        <div id="Div6" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lblAppRejAssessment_ReviewerPart" runat="server" Text="Reviewer Part" />
                                            </div>
                                        </div>
                                        <div class="panel-body-default">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 25%">
                                                        <asp:Label ID="lblAppRejAssessement_Reviewer_Status" runat="server" Text="Status" />
                                                    </td>
                                                    <td style="width: 75%">
                                                        <asp:RadioButton ID="radAssessment_Reviewer_Approve" runat="server" Text="Agree"
                                                            GroupName="Assessment_Reviewer" />
                                                        <asp:RadioButton ID="radAssessment_Reviewer_Reject" runat="server" Text="Disagree"
                                                            GroupName="Assessment_Reviewer" />
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 25%; vertical-align: text-top">
                                                        <asp:Label ID="lblAppRejAssessement_Reviewer_Remark" runat="server" Text="Remark" />
                                                    </td>
                                                    <td style="width: 75%">
                                                        <asp:TextBox ID="txtAppRejAssessment_Reviewer_Remark" runat="server" TextMode="MultiLine"
                                                            Rows="3"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div id="Div4" class="panel-default">
                                    <div id="Div5" class="panel-body-default" style="padding-top: 0px;">
                                        <div class="btn-default" style="margin-top: 0px">
                                            <asp:Button ID="btnpostComment" runat="server" Text="Post Comment" Style="margin-right: 10px"
                                                CssClass="btnDefault" />
                                            <asp:Button ID="btnAppRejAss_Close" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                                CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdfAppRejAss" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cf1:CnfDialog ID="cnfEdit" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
