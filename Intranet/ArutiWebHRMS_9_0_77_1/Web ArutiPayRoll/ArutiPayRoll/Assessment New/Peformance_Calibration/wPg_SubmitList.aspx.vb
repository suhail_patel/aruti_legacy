﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Imports System.Web.UI.DataVisualization.Charting
Imports System.IO
Imports System.IO.Packaging
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Net.Dns

#End Region

Partial Class wPg_SubmitList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmScoreCalibrationList"
    Private ReadOnly mstrModuleName1 As String = "frmScoreCalibrationAddEdit"
    Private blnShowApplyCalibrationpopup As Boolean = False
    Private objScoreCalibrateApproval As New clsScoreCalibrationApproval
    Private mdtApplyNew As DataTable = Nothing
    Private mdtFullDataTable As DataTable = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mView As DataView = Nothing
    Private mlbnIsSubmitted As Boolean = False
    Private mdtListDataTable As DataTable = Nothing
    Private mintCalibrationUnkid As Integer = 0
    Private mintEmployeeUnkid As Integer = 0
    Private mintPeriodUnkid As Integer = 0
    Private mblnIsGrp As Boolean = False
    Private mintgrpId As Integer = 0
    Private mblnIsExpand As Boolean = False
    Private mdtRatings As DataTable = Nothing
    Private mdtDataTable As DataTable = Nothing
    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private mblnShowHPOCurve As Boolean = False
    'S.SANDEEP |27-JUL-2019| -- END

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private mblnShowMyReport As Boolean = False
    'S.SANDEEP |26-AUG-2019| -- END
    Private objCONN As SqlConnection 'S.SANDEEP |16-AUG-2019| -- START -- END
    Private mstrChartAdvanceFilter As String = String.Empty
    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Private mstrReportAdvanceFilter As String = String.Empty
    'S.SANDEEP |25-OCT-2019| -- END

#End Region

#Region " Common Part "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim objRating As New clsAppraisal_Rating
        Try
            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            With cboRptEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("List").Copy()
                .SelectedValue = 0
                .DataBind()
            End With
            'S.SANDEEP |26-AUG-2019| -- END

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            With cboSPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            With cboChPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            With cboRptPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            Call cboRptPeriod_SelectedIndexChanged(cboRptPeriod, New EventArgs())
            'S.SANDEEP |26-AUG-2019| -- END

            Dim objCScoreMaster As New clsComputeScore_master
            dsList = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cbochDisplay
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-JUL-2019| -- END

            dsList = objScoreCalibrateApproval.GetStatusList("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objRating.getComboList("List", True)
            With cbocRating
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'With cboPRatingFrm
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0).Copy()
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'With cboPRatingTo
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0).Copy()
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'mdtRatings = dsList.Tables(0).Copy()

            If dsList.Tables(0).Select("Id > 0").Length > 0 Then
                mdtRatings = dsList.Tables(0).Select("Id > 0").CopyToDataTable()
            Else
                mdtRatings = dsList.Tables(0).Copy()
            End If
            dsList = (New clscalibrate_approver_master).GetList("List", True, "", 0, Nothing, True)
            With cboRptApprover
                .DataValueField = "mapuserunkid"
                .DataTextField = "dapprover"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            'S.SANDEEP |26-AUG-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private Sub Fill_FRating()
        Try
            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False)
            gvFilterRating.AutoGenerateColumns = False
            gvFilterRating.DataSource = dsList
            gvFilterRating.DataBind()
            objRating = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_FRating : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END    

#End Region

#Region " Form's Event "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub

                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If arr.Length = 3 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    'Blank_ModuleName()
                    'clsCommonATLog._WebFormName = "frmScoreCalibrationAddEdit"
                    'StrModuleName2 = "mnuAssessment"
                    'StrModuleName3 = "mnuSetups"
                    'clsCommonATLog._WebClientIP = Session("IP_ADD")
                    'clsCommonATLog._WebHostName = Session("HOST_NAME")

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    mintPeriodUnkid = CInt(arr(2))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    Call SetControlCaptions()
                    Call SetMessages()
                    Call Language._Object.SaveValue()
                    Call SetLanguage()

                    Call FillCombo()

                    If mintPeriodUnkid > 0 Then
                        cboSPeriod.SelectedValue = mintPeriodUnkid
                    End If

                    blnShowApplyCalibrationpopup = True
                    cboSPeriod.Enabled = False : btnSReset.Enabled = False : btnISearch.Enabled = False : lnkAllocation.Visible = False
                    Call btnISearch_Click(New Object(), New EventArgs)
                    HttpContext.Current.Session("Login") = True
                    gvCalibrateList.Columns(0).Visible = False
                    gvCalibrateList.Columns(1).Visible = False
                    btnNew.Visible = False
                    GoTo lnk
                End If
            End If
            'S.SANDEEP |16-AUG-2019| -- END

            If IsPostBack = False Then
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                'S.SANDEEP |27-JUL-2019| -- END
                Call FillCombo()
            Else
                blnShowApplyCalibrationpopup = Me.ViewState("blnShowApplyCalibrationpopup")
                mdtApplyNew = Me.ViewState("mdtApplyNew")
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter")
                mdtFullDataTable = Me.ViewState("mdtFullDataTable")
                mView = Me.ViewState("mView")
                mlbnIsSubmitted = Me.ViewState("mlbnIsSubmitted")
                mdtListDataTable = Me.ViewState("mdtListDataTable")
                mintCalibrationUnkid = Me.ViewState("mintCalibrationUnkid")
                mintEmployeeUnkid = Me.ViewState("mintEmployeeUnkid")
                mintPeriodUnkid = Me.ViewState("mintPeriodUnkid")
                mblnIsGrp = Me.ViewState("mblnIsGrp")
                mdtRatings = Me.ViewState("mdtRatings")
                mdtDataTable = Me.ViewState("mdtDataTable")
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                mblnShowHPOCurve = Me.ViewState("mblnShowHPOCurve")
                'S.SANDEEP |27-JUL-2019| -- END

                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                mblnShowMyReport = Me.ViewState("mblnShowMyReport")
                'S.SANDEEP |26-AUG-2019| -- END
                mstrChartAdvanceFilter = Me.ViewState("mstrChartAdvanceFilter")

                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                mstrReportAdvanceFilter = Me.ViewState("mstrReportAdvanceFilter")
                'S.SANDEEP |25-OCT-2019| -- END
            End If
lnk:        If blnShowApplyCalibrationpopup Then
                popupApproverUseraccess.Show()
            End If
            If CInt(Session("ScoreCalibrationFormNotype")) = 1 Then
                txtICalibrationNo.Enabled = False
            Else
                txtICalibrationNo.Enabled = True
            End If

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            If mblnShowHPOCurve Then
                popupHPOChart.Show()
            End If
            'S.SANDEEP |27-JUL-2019| -- END

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            If mblnShowMyReport Then
                popupMyReport.Show()
            End If
            'S.SANDEEP |26-AUG-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load1 : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
        Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("blnShowApplyCalibrationpopup") = blnShowApplyCalibrationpopup
            Me.ViewState("mdtApplyNew") = mdtApplyNew
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("mdtFullDataTable") = mdtFullDataTable
            Me.ViewState("mView") = mView
            Me.ViewState("mlbnIsSubmitted") = mlbnIsSubmitted
            Me.ViewState("mdtListDataTable") = mdtListDataTable

            Me.ViewState("mintCalibrationUnkid") = mintCalibrationUnkid
            Me.ViewState("mintEmployeeUnkid") = mintEmployeeUnkid
            Me.ViewState("mintPeriodUnkid") = mintPeriodUnkid
            Me.ViewState("mblnIsGrp") = mblnIsGrp
            Me.ViewState("mdtRatings") = mdtRatings
            Me.ViewState("mdtDataTable") = mdtDataTable

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            Me.ViewState("mblnShowHPOCurve") = mblnShowHPOCurve
            'S.SANDEEP |27-JUL-2019| -- END

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            Me.ViewState("mblnShowMyReport") = mblnShowMyReport
            'S.SANDEEP |26-AUG-2019| -- END

            Me.ViewState("mstrChartAdvanceFilter") = mstrChartAdvanceFilter

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Me.ViewState("mstrReportAdvanceFilter") = mstrReportAdvanceFilter
            'S.SANDEEP |25-OCT-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " List Methods "

#Region " Button's Event(s) "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            mstrAdvanceFilter = ""
            txtICalibrationNo.Text = ""
            txtIRemark.Text = ""
            cboSPeriod.Enabled = True : txtICalibrationNo.Enabled = True
            lnkAllocation.Enabled = True : btnISearch.Enabled = True : btnSReset.Enabled = True
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'radApplyToAll.Checked = False
            'radApplyToChecked.Checked = False
            'S.SANDEEP |27-JUL-2019| -- END
            cbocRating.SelectedValue = 0
            'cboPRatingFrm.SelectedValue = 0
            'cboPRatingTo.SelectedValue = 0
            gvApplyCalibration.DataSource = Nothing
            gvApplyCalibration.DataBind()
            blnShowApplyCalibrationpopup = True
            popupApproverUseraccess.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnNew_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue"), Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Request.QueryString.Count <= 0 Then
            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
            Else
                Session.Abandon()
                Response.Redirect("~/Index.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAll.Click
        Try
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, 0, True, "", "")
            'gvMyReport.AutoGenerateColumns = False
            'gvMyReport.DataSource = mdtDataTable
            'gvMyReport.DataBind()
            'popupMyReport.Show()
            Dim dsApprover As New DataSet
            dsApprover = objScoreCalibrateApproval.GetApproverInfo(CInt(Session("UserId")))
            If dsApprover.Tables.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage("frmApproveRejectCalibrationAddEdit", 100, "Sorry, No approver defined with the selected login."), Me)
                Exit Sub
            End If
            If dsApprover.Tables(0).Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage("frmApproveRejectCalibrationAddEdit", 100, "Sorry, No approver defined with the selected login."), Me)
                Exit Sub
            End If
            Call btnRptReset_Click(btnRptReset, New EventArgs())
            mblnShowMyReport = True
            popupMyReport.Show()
            'S.SANDEEP |26-AUG-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("btnShowAll_Click : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |12-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : PA CHANGES
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Try
            If objScoreCalibrateApproval.Void_Calibration(mblnIsGrp, _
                                                          mintCalibrationUnkid, _
                                                          mintEmployeeUnkid, _
                                                          mintPeriodUnkid, _
                                                          Session("Database_Name"), _
                                                          Session("CompanyUnkId"), _
                                                          Session("Fin_year"), _
                                                          ConfigParameter._Object._CurrentDateAndTime, _
                                                          enAuditType.DELETE, _
                                                          Session("UserId"), _
                                                          Session("IP_ADD"), _
                                                          Session("HOST_NAME"), mstrModuleName, delReason.Reason, True) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Information deleted Successfully."), Me)
                FillGrid()
                mblnIsGrp = False : mintCalibrationUnkid = 0 : mintEmployeeUnkid = 0 : mintPeriodUnkid = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("delReason_buttonDelReasonYes_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cnfDelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfDelete.buttonYes_Click
        Try
            delReason.Title = Language.getMessage(mstrModuleName, 3, "Enter reason to void data.")
            delReason.Reason = ""
            delReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("cnfDelete_buttonYes_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |12-JUL-2019| -- END


    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Protected Sub btnDisplayCurve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplayCurve.Click
        Try
            mstrChartAdvanceFilter = ""
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
            mblnShowHPOCurve = True
            popupHPOChart.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnChGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChGenerate.Click
        Try
            If cboChPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage("frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboChPeriod.Focus()
                Exit Sub
            End If

            Dim dsDataSet As New DataSet
            Dim objPerformance As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            'S.SANDEEP |22-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'dsDataSet = objPerformance.GenerateChartData(CInt(cboChPeriod.SelectedValue), _
            '                                                Session("Database_Name").ToString, _
            '                                                CInt(Session("UserId")), _
            '                                                CInt(Session("Fin_year")), _
            '                                                CInt(Session("CompanyUnkId")), _
            '                                             CStr(Session("UserAccessModeSetting")), True, False, "List", , , , , IIf(cbochDisplay.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cbochDisplay.SelectedValue), mstrChartAdvanceFilter)
            dsDataSet = objPerformance.GenerateChartData(CInt(cboChPeriod.SelectedValue), _
                                                            Session("Database_Name").ToString, _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                         CStr(Session("UserAccessModeSetting")), True, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         False, "List", , , , , IIf(cbochDisplay.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cbochDisplay.SelectedValue), mstrChartAdvanceFilter)
            'S.SANDEEP |22-OCT-2019| -- END

            If dsDataSet.Tables.Count > 0 Then
                dgvCalibData.DataSource = dsDataSet.Tables(0)
                dgvCalibData.DataBind()

                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                If dgvCalibData.Items.Count > 0 Then
                    Dim gvr As DataGridItem = Nothing
                    'S.SANDEEP |21-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : {HPO Chart Issue}
                    'gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(5).Text = "N").FirstOrDefault()
                    'If gvr IsNot Nothing Then
                    '    gvr.ForeColor = Color.Blue
                    'End If
                    'gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(5).Text = "A").FirstOrDefault()
                    'If gvr IsNot Nothing Then
                    '    gvr.ForeColor = Color.Tomato
                    'End If
                    'Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(5))
                    'If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                    '    For Each item In xItems
                    '        item.Visible = False
                    '    Next
                    'End If

                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "N").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Blue
                    End If
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "A").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Tomato
                    End If
                    Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(6))
                    If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                        For Each item In xItems
                            item.Visible = False
                        Next
                    End If
                    'S.SANDEEP |21-SEP-2019| -- END
                End If
                'S.SANDEEP |26-AUG-2019| -- END

                chHpoCurve.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 100, "Ratings")
                chHpoCurve.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 101, "%")
                chHpoCurve.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Format = "{0.##}%"

                'S.SANDEEP |26-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Calibration Issue}
                'S.SANDEEP |21-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {HPO Chart Issue}
                'chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.Enabled = True
                'S.SANDEEP |21-SEP-2019| -- END

                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Interval = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffset = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.MajorGrid.Enabled = False
                chHpoCurve.ChartAreas(0).AxisY.Maximum = 105
                chHpoCurve.ChartAreas(0).AxisY.Minimum = 0
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 0
                'S.SANDEEP |26-SEP-2019| -- END

                chHpoCurve.Series(0).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "np")
                chHpoCurve.Series(1).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "ap")

                chHpoCurve.Titles(0).Text = Language.getMessage(mstrModuleName, 200, "Performance HPO Curve") & vbCrLf & _
                                      cboChPeriod.SelectedItem.Text

                chHpoCurve.Titles(0).Font = New Font(chHpoCurve.Font.Name, 11, FontStyle.Bold)

                chHpoCurve.Series(0).LabelFormat = "{0.##}%"
                chHpoCurve.Series(1).LabelFormat = "{0.##}%"

                chHpoCurve.Series(0).IsVisibleInLegend = False
                chHpoCurve.Series(1).IsVisibleInLegend = False

                chHpoCurve.Series(0).Color = Color.Blue
                chHpoCurve.Series(1).Color = Color.Tomato

                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Language.getMessage(mstrModuleName, 102, "Normal Distribution")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chHpoCurve.Series(0).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                If dsDataSet.Tables("Chart").Rows.Count > 0 Then
                    legendItem2.Name = Language.getMessage(mstrModuleName, 103, "Actual Performance") & dsDataSet.Tables("Chart").Rows(0)("litem").ToString()
                Else
                    legendItem2.Name = Language.getMessage(mstrModuleName, 103, "Actual Performance")
                End If
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chHpoCurve.Series(1).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem2)

                chHpoCurve.Series(0).ToolTip = chHpoCurve.Legends(0).CustomItems(0).Name & " : is #VAL "
                chHpoCurve.Series(1).ToolTip = chHpoCurve.Legends(0).CustomItems(1).Name & " : is #VAL "

                For i As Integer = 0 To chHpoCurve.Series.Count - 1
                    For Each dp As DataPoint In chHpoCurve.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next
            Else
                DisplayMessage.DisplayMessage(Language.getMessage("frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnChGenerate_Click " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |27-JUL-2019| -- END

    'S.SANDEEP |14-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : BOOLEAN VARIABLE WAS NOT SET TO FALSE, RESULT IN OPENING OF POP-UP AGAIN
    Protected Sub btnChClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChClose.Click
        Try
            mblnShowHPOCurve = False
            popupHPOChart.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnChClose_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |14-AUG-2019| -- END

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Protected Sub btnCloseMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseMyReport.Click
        Try
            mblnShowMyReport = False
            popupMyReport.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnCloseMyReport_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnRptShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptShow.Click
        Try
            Dim strFilter As String = String.Empty
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilter &= "AND iData.calibratnounkid = '" & CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptApprover.SelectedValue = "", 0, cboRptApprover.SelectedValue)) > 0 Then
                strFilter &= "AND Fn.userunkid = '" & CInt(IIf(cboRptApprover.SelectedValue = "", 0, cboRptApprover.SelectedValue)) & "'"
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, 0, True, strFilter, "", True)
            mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, 0, True, strFilter, "", True, False, mstrReportAdvanceFilter)
            'S.SANDEEP |25-OCT-2019| -- END
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = mdtDataTable
            gvMyReport.DataBind()
            popupMyReport.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnRptShow_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnRptReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptReset.Click
        Try
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            mstrReportAdvanceFilter = ""
            'S.SANDEEP |25-OCT-2019| -- END
            cboRptCalibNo.SelectedValue = 0
            cboRptEmployee.SelectedValue = 0
            cboRptApprover.SelectedValue = 0
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = Nothing
            gvMyReport.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnRptReset_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnExportMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportMyReport.Click
        Try
            If gvMyReport.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, there is no data in order to export."), Me)
                Exit Sub
            End If

            Dim strFilterCaption As String = ""
            If CInt(IIf(cboRptPeriod.SelectedValue = "", 0, cboRptPeriod.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 20, "Period :") & " " & cboRptPeriod.SelectedItem.Text & " "
            End If
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 21, "Calibration No :") & " " & cboRptCalibNo.SelectedItem.Text & " "
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 22, "Employee :") & " " & cboRptEmployee.SelectedItem.Text & " "
            End If
            If CInt(IIf(cboRptApprover.SelectedValue = "", 0, cboRptApprover.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 23, "Approver :") & " " & cboRptApprover.SelectedItem.Text & " "
            End If
            If strFilterCaption.Trim.Length > 0 Then strFilterCaption = Mid(strFilterCaption, 2)
            Dim objRpt As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objRpt.Export_Calibration_Report(Session("UserId"), _
                                             Session("CompanyUnkId"), _
                                             mdtDataTable, strFilterCaption, _
                                             Language.getMessage(mstrModuleName, 24, "Approver Wise Status"), _
                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If objRpt._FileNameAfterExported.Trim <> "" Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.ClearContent()
                response.Clear()
                response.ContentType = "text/plain"
                response.AddHeader("Content-Disposition", "attachment; filename=" + objRpt._FileNameAfterExported + ";")
                response.TransmitFile(IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objRpt._FileNameAfterExported)
                response.Flush()
                response.Buffer = False
                response.End()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnExportMyReport_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END

    Protected Sub AdvanceFilter1_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvanceFilter1.buttonApply_Click
        Try
            mstrChartAdvanceFilter = AdvanceFilter1._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError("AdvanceFilter1_buttonApply_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnChReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChReset.Click
        Try
            mstrChartAdvanceFilter = ""
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnChReset_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnImportCalibScore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportCalibScore.Click
        Try
            'Dim strPath As String = Server.MapPath("~/images/Config_Option_Design.xlsx")
            'Dim ds = Aruti.Data.modGlobal.OpenXML_Import(strPath)
            'If ds.Tables(0).Rows.Count > 0 Then

            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnImportCalibScore_Click : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Protected Sub AdvRptFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvRptFilter.buttonApply_Click
        Try
            mstrReportAdvanceFilter = AdvRptFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError("AdvRptFilter_buttonApply_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |25-OCT-2019| -- END


#End Region

#Region " Private Methods "

    Private Sub FillGrid()
        Dim strFilter As String = String.Empty
        Try
            If txtBatchNo.Text.Trim.Length > 0 Then
                strFilter &= "AND calibration_no = '" & txtBatchNo.Text & "' "
            End If

            If CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) > 0 Then
                strFilter &= "AND hrassess_computescore_approval_tran.statusunkid = '" & cboStatus.SelectedValue & "' "
            End If

            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND hrassess_computescore_approval_tran.employeeunkid = '" & cboEmployee.SelectedValue & "' "
            End If

            If dtDateFrom.IsNull = False Then
                strFilter &= "AND CONVERT(NVARCHAR(8), transactiondate, 112) >= '" & eZeeDate.convertDate(dtDateFrom.GetDate()).ToString() & "' "
            End If

            If dtDateTo.IsNull = False Then
                strFilter &= "AND CONVERT(NVARCHAR(8), transactiondate, 112) <= '" & eZeeDate.convertDate(dtDateTo.GetDate()).ToString() & "' "
            End If

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'mdtListDataTable = objScoreCalibrateApproval.GetList(CInt(cboPeriod.SelectedValue), _
            '                                                     Session("Database_Name"), _
            '                                                     Session("UserId"), _
            '                                                     Session("Fin_year"), _
            '                                                     Session("CompanyUnkId"), _
            '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                     Session("UserAccessModeSetting"), True, False, "List", strFilter)
            mdtListDataTable = objScoreCalibrateApproval.GetList(CInt(cboPeriod.SelectedValue), _
                                                                 Session("Database_Name"), _
                                                                 Session("UserId"), _
                                                                 Session("Fin_year"), _
                                                                 Session("CompanyUnkId"), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                 Session("UserAccessModeSetting"), True, False, "List", strFilter, Nothing, True)
            'S.SANDEEP |25-OCT-2019| -- END


            gvCalibrateList.AutoGenerateColumns = False
            If mdtListDataTable.Rows.Count > 0 Then
                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                'gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True).CopyToDataTable()
                gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True Or x.Field(Of Boolean)("ispgrp") = True).CopyToDataTable()
                'S.SANDEEP |25-OCT-2019| -- END

                'gvCalibrateList.DataSource = mdtListDataTable
                gvCalibrateList.DataBind()
            Else
                gvCalibrateList.DataSource = mdtListDataTable
                gvCalibrateList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("FillGrid : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Links Event(s) "

    Protected Sub lnkPlus_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintgrpId = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
            mblnIsExpand = True
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = gvCalibrateList.DataKeys(row.RowIndex)("grpid") Or x.Field(Of Boolean)("isgrp") = True).CopyToDataTable()
            gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = gvCalibrateList.DataKeys(row.RowIndex)("grpid") Or x.Field(Of Boolean)("isgrp") = True Or x.Field(Of Boolean)("ispgrp") = True).CopyToDataTable()
            'S.SANDEEP |25-OCT-2019| -- END

            'gvCalibrateList.DataSource = mdtListDataTable
            gvCalibrateList.DataBind()
            mintgrpId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkPlus_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkMins_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintgrpId = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
            mblnIsExpand = False
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True).CopyToDataTable()
            gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True Or x.Field(Of Boolean)("ispgrp") = True).CopyToDataTable()
            'S.SANDEEP |25-OCT-2019| -- END
            gvCalibrateList.DataBind()
            mintgrpId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkMins_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CBool(gvCalibrateList.DataKeys(row.RowIndex)("isgrp").ToString) Then
                mintCalibrationUnkid = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
                mintEmployeeUnkid = 0 : mintPeriodUnkid = 0
                mblnIsGrp = True
            Else
                mintCalibrationUnkid = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
                mintEmployeeUnkid = gvCalibrateList.DataKeys(row.RowIndex)("employeeunkid")
                mintPeriodUnkid = gvCalibrateList.DataKeys(row.RowIndex)("periodunkid")
                mblnIsGrp = False
            End If
            cnfDelete.Title = "Aruti"
            cnfDelete.Message = Language.getMessage(mstrModuleName, 1, "Are you sure you wanted to delete information.")
            cnfDelete.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkdelete_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CBool(gvCalibrateList.DataKeys(row.RowIndex)("isgrp").ToString) Then
                cboSPeriod.SelectedValue = gvCalibrateList.DataKeys(row.RowIndex)("periodunkid")
                Dim objScoreCompute As New clsComputeScore_master
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                'radApplyToAll.Checked = False
                'radApplyToChecked.Checked = False
                'S.SANDEEP |27-JUL-2019| -- END
                cbocRating.SelectedValue = 0
                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                'cboPRatingFrm.SelectedValue = 0
                'cboPRatingTo.SelectedValue = 0
                'S.SANDEEP |26-AUG-2019| -- END
                'S.SANDEEP |12-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CLEARING CONTROLS AFTER SETTING VALUE
                txtIRemark.Text = ""
                'S.SANDEEP |12-AUG-2019| -- END
                txtICalibrationNo.Text = row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                cboSPeriod.Enabled = False : txtICalibrationNo.Enabled = False
                lnkAllocation.Enabled = False : btnISearch.Enabled = False : btnSReset.Enabled = False
                Call FillApplyCalibratedList("ISNULL(CSM.calibratnounkid,0) = '" & gvCalibrateList.DataKeys(row.RowIndex)("grpid") & "'")
                blnShowApplyCalibrationpopup = True
                'S.SANDEEP |26-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Calibration Issue}
                Call Fill_FRating()
                'S.SANDEEP |26-SEP-2019| -- END
                popupApproverUseraccess.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkedit_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkChAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChAllocation.Click
        Try
            AdvanceFilter1._Hr_EmployeeTable_Alias = "hremployee_master"
            AdvanceFilter1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkChAllocation_Click : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Protected Sub lnkRptAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRptAdvFilter.Click
        Try
            mstrReportAdvanceFilter = ""
            AdvRptFilter._Hr_EmployeeTable_Alias = "AEM"
            AdvRptFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkRptAdvFilter_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |25-OCT-2019| -- END

#End Region

#Region " Grid Event(s) "

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim intCount As Integer = 4 : Dim strCaption As String = String.Empty
                If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("ispgrp").ToString) = True Then
                    For i = 4 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(3).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(1).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(2).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(3).CssClass = "MainGroupHeaderStyleLeft"
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False
                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton) : lnkdelete.Visible = False

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    For i = 4 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(3).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(1).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(2).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(3).CssClass = "GroupHeaderStylecompLeft"

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text = ""
                    End If
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text = ""
                    End If

                    If mintgrpId = Convert.ToInt64(gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid")) Then
                        Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton)
                        Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton)
                        If mblnIsExpand Then
                            lnkMins.Visible = True
                            lnkPlus.Visible = False
                        Else
                            lnkMins.Visible = False
                            lnkPlus.Visible = True
                        End If
                    End If

                    If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
                        strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                        Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 10, "Date") & " [" & oDate & "]"
                    End If

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = False Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkedit.Visible = False : lnkdelete.Visible = True

                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False
                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "")
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text <> "&nbsp;" Then
                        Dim mDecValue = 0
                        mDecValue = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text)
                        Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mDecValue & " AND scrt >= " & mDecValue)
                        If dr.Length > 0 Then
                            e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpRating", False, True)).Text = dr(0)("name")
                        End If
                    End If

                    If gvCalibrateList.DataKeys(e.Row.RowIndex)("calibratescore").ToString.Trim.Length > 0 Then
                        Dim mDecValue = 0
                        mDecValue = CDec(gvCalibrateList.DataKeys(e.Row.RowIndex)("calibratescore").ToString)
                        Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mDecValue & " AND scrt >= " & mDecValue)
                        If dr.Length > 0 Then
                            e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcRating", False, True)).Text = dr(0)("name")
                        End If
                    End If
                End If
                If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("statusunkid").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.NotSubmitted Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkedit.Visible = False : lnkdelete.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvCalibrateList_RowDataBound : " & ex.Message, Me)
        End Try
    End Sub

    'Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
    '                e.Row.BackColor = Color.Silver
    '                e.Row.ForeColor = Color.Black
    '                e.Row.Font.Bold = True
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
    '                Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
    '                lnkedit.Visible = True : lnkdelete.Visible = True
    '                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text <> "&nbsp;" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text = ""
    '                End If
    '                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text <> "&nbsp;" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text = ""
    '                End If

    '                If mintgrpId = Convert.ToInt64(gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid")) Then
    '                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton)
    '                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton)
    '                    If mblnIsExpand Then
    '                        lnkMins.Visible = True
    '                        lnkPlus.Visible = False
    '                    Else
    '                        lnkMins.Visible = False
    '                        lnkPlus.Visible = True
    '                    End If
    '                End If

    '                'S.SANDEEP |26-AUG-2019| -- START
    '                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '                Dim strCaption As String = ""
    '                Dim strUser As String = gvCalibrateList.DataKeys(e.Row.RowIndex)("calibuser").ToString()
    '                strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("allocation").ToString().Trim <> "" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser & " (" & gvCalibrateList.DataKeys(e.Row.RowIndex)("allocation").ToString() & ") "
    '                Else
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser
    '                End If
    '                'S.SANDEEP |26-AUG-2019| -- END

    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
    '                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
    '                    Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 10, "Date") & " [" & oDate & "]"
    '                End If
    '            Else
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
    '                Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
    '                lnkedit.Visible = False : lnkdelete.Visible = True

    '                Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False
    '                Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False

    '                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "")
    '                End If

    '                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text <> "&nbsp;" Then
    '                    Dim mDecValue = 0
    '                    mDecValue = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text)
    '                    Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mDecValue & " AND scrt >= " & mDecValue)
    '                    If dr.Length > 0 Then
    '                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpRating", False, True)).Text = dr(0)("name")
    '                    End If
    '                End If

    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("calibratescore").ToString.Trim.Length > 0 Then
    '                    Dim mDecValue = 0
    '                    mDecValue = CDec(gvCalibrateList.DataKeys(e.Row.RowIndex)("calibratescore").ToString)
    '                    Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mDecValue & " AND scrt >= " & mDecValue)
    '                    If dr.Length > 0 Then
    '                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcRating", False, True)).Text = dr(0)("name")
    '                    End If
    '                End If

    '            End If
    '            If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("statusunkid").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.NotSubmitted Then
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
    '                Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
    '                lnkedit.Visible = False : lnkdelete.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("gvCalibrateList_RowDataBound : " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |25-OCT-2019| -- END


    Protected Sub gvMyReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMyReport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(gvMyReport.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    'S.SANDEEP |04-OCT-2019| -- START
                    'e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
                    'mdtDataTable.Select("grpid = '" & gvMyReport.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "

                    Dim strCaption As String = ""
                    Dim strUser As String = gvMyReport.DataKeys(e.Row.RowIndex)("calibuser").ToString()
                    Dim strallocation As String = gvMyReport.DataKeys(e.Row.RowIndex)("allocation").ToString()

                    If strUser.Trim.Length <= 0 Then
                        strUser = mdtDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = CInt(gvMyReport.DataKeys(e.Row.RowIndex)("grpid")) And x.Field(Of Boolean)("isgrp") = False).Select(Function(x) x.Field(Of String)("calibuser")).FirstOrDefault()
                    End If
                    If strallocation.Trim.Length <= 0 Then
                        strallocation = mdtDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = CInt(gvMyReport.DataKeys(e.Row.RowIndex)("grpid")) And x.Field(Of Boolean)("isgrp") = False).Select(Function(x) x.Field(Of String)("allocation")).FirstOrDefault()
                    End If

                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
                    mdtDataTable.Select("grpid = '" & gvMyReport.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "

                    If strallocation.Trim <> "" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser & " (" & strallocation & ") "
                    Else
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser
                    End If
                    'S.SANDEEP |04-OCT-2019| -- END

                Else
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvMyReport_RowDataBound : " & ex.Message, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
#Region " Combobox Event(s) "

    Protected Sub cboRptPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRptPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objScoreCalibrateApproval.GetComboListCalibrationNo("List", CInt(cboRptPeriod.SelectedValue), True)
            With cboRptCalibNo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboRptPeriod_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

#Region " AddEdit Methods "

#Region " Button's Event(s) "

    Protected Sub btnISearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISearch.Click
        Try
            If CInt(IIf(cboSPeriod.SelectedValue = "", 0, cboSPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue"), Me)
                Exit Sub
            End If
            Call FillApplyCalibratedList()
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            Call Fill_FRating()
            'S.SANDEEP |26-AUG-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("btnISearch_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnIApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIApply.Click
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'If radApplyToChecked.Checked = False AndAlso radApplyToAll.Checked = False Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Sorry, Please select atleast one of the operation in order to apply changes in the list."), Me)
            '    Exit Sub
            'End If

            'If radApplyToChecked.Checked = True Then
            '    If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score."), Me)
            '        Exit Sub
            '    End If
            'End If
                If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score."), Me)
                    Exit Sub
                End If
            'S.SANDEEP |27-JUL-2019| -- END

            If CInt(IIf(cbocRating.SelectedValue = "", 0, cbocRating.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue."), Me)
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'If radApplyToChecked.Checked = True Then
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            'Else
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            'End If
                gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            'S.SANDEEP |27-JUL-2019| -- END
            Dim intFirstValue As Integer = 0
            For Each iRow In gRow
                Dim mRow As DataRow() = Nothing
                mRow = mdtApplyNew.Select("Code = '" & iRow.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text & "'")
                If mRow.Length > 0 Then
                    Dim dr() As DataRow = mdtRatings.Select("id = '" & cbocRating.SelectedValue & "'")
                    Dim rnd As New Random : Dim random As Integer = 0
                    If intFirstValue <= 0 Then
                        random = rnd.Next(dr(0)("scrf"), dr(0)("scrt"))
                    Else
                        random = rnd.Next(intFirstValue, dr(0)("scrt"))
                        If intFirstValue = random Then
                            random = rnd.Next(intFirstValue, dr(0)("scrt"))
                        End If
                    End If
                    intFirstValue = random
                    mRow(0)("calibrated_score") = IIf(random <= 0, CDec(dr(0)("scrt")), random)
                    mRow(0)("isChanged") = True
                    mRow(0)("calibration_remark") = txtIRemark.Text
                    'S.SANDEEP |26-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                    'mRow(0)("calibRating") = cbocRating.SelectedItem.Text
                    If CInt(cbocRating.SelectedValue) > 0 Then
                    mRow(0)("calibRating") = cbocRating.SelectedItem.Text
                    Else
                        mRow(0)("calibRating") = ""
                    End If
                    'S.SANDEEP |26-AUG-2019| -- END
                End If
            Next
            mdtApplyNew.AcceptChanges()
            gvApplyCalibration.AutoGenerateColumns = False
            gvApplyCalibration.DataSource = mdtApplyNew
            gvApplyCalibration.DataBind()
            txtListSearch.Focus()
            ClearData()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnIApply_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cnfRemark_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfRemark.buttonYes_Click
        Try
            Call SaveData(mlbnIsSubmitted)
        Catch ex As Exception
            DisplayMessage.DisplayError("cnfRemark_buttonYes_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnISave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISave.Click
        Try
            If IsValidData() = False Then Exit Sub
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            If gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).Text = "" Or _
                                                     x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Text = "").Count > 0 Then
                cnfRemark.Title = "Aruti"
                cnfRemark.Message = Language.getMessage(mstrModuleName1, 16, "Are you sure you want to save without calibration rating(s)?")
                cnfRemark.Show()
                Exit Sub
            End If
            Call SaveData(False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnISave_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnISubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISubmit.Click
        Try
            If IsValidData() = False Then Exit Sub
            mlbnIsSubmitted = True
            If mdtApplyNew IsNot Nothing AndAlso gvApplyCalibration.Rows.Count > 0 Then

                'If mintCalibrationUnkid > 0 Then
                Dim iTotalRow, iChangedRow As Integer : iTotalRow = 0 : iChangedRow = 0
                iTotalRow = mdtApplyNew.Rows.Count() : iChangedRow = mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Decimal)("calibrated_score") > 0).Count()
                If iTotalRow <> iChangedRow Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot do submit operation as you have not provided score to some of the employee(s), Please assign socre to those employee(s) which are in red.", Me)
                    Dim gvRow As IEnumerable(Of GridViewRow) = gvApplyCalibration.Rows.Cast(Of GridViewRow)()
                    Dim oRow As IEnumerable(Of DataRow) = mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isChanged") = False)
                    For Each iRow In oRow
                        Dim strCode As String = iRow("Code").ToString()
                        Dim gr = gvRow.Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text = strCode).Select(Function(X) X).First()
                        gr.ForeColor = Color.Red
                    Next
                    Exit Sub
                End If
                'End If

                Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
                If gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).Text = "&nbsp;").Count > 0 Then
                    cnfRemark.Title = "Aruti"
                    cnfRemark.Message = Language.getMessage(mstrModuleName1, 7, "Are you sure you want to submit without remark?")
                    cnfRemark.Show()
                    Exit Sub
                End If

                Call SaveData(True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnISubmit_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            blnShowApplyCalibrationpopup = False
            popupApproverUseraccess.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnIClose_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
    End Sub

    Protected Sub btnSReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSReset.Click
        Try
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'cboPRatingFrm.SelectedValue = 0 : cboPRatingTo.SelectedValue = 0
            'S.SANDEEP |26-AUG-2019| -- END
            mstrAdvanceFilter = "" : If txtListSearch.Text.Trim.Length > 0 Then txtListSearch.Text = ""
            Call FillApplyCalibratedList()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSReset_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Private Method(s) "

    Private Sub FillApplyCalibratedList(Optional ByVal strExtraFilter As String = "")
        Dim strFilter As String = String.Empty
        Try
            If mstrAdvanceFilter.Trim.Length > 0 Then
                strFilter &= "AND " & mstrAdvanceFilter
            End If
            If strExtraFilter.Trim.Length > 0 Then
                strFilter &= "AND " & strExtraFilter
            End If

            If cboSPeriod.Enabled = True Then
                strFilter &= "AND ISNULL(CSM.calibrated_score,0) <= 0 "
            End If

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'mdtFullDataTable = objScoreCalibrateApproval.GetListForSubmit(CInt(cboSPeriod.SelectedValue), Session("Database_Name"), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting"), True, False, "List", strFilter)
            mdtFullDataTable = objScoreCalibrateApproval.GetListForSubmit(CInt(cboSPeriod.SelectedValue), _
                                                                          Session("Database_Name"), _
                                                                          Session("UserId"), _
                                                                          Session("Fin_year"), _
                                                                          Session("CompanyUnkId"), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                          Session("UserAccessModeSetting"), True, False, "List", _
                                                                          strFilter, Nothing, CBool(Session("IsCompanyNeedReviewer")))
            'S.SANDEEP |25-OCT-2019| -- END

            If mdtFullDataTable.Rows.Count > 0 Then
                txtICalibrationNo.Text = mdtFullDataTable.Rows(0)("calibration_no")
                txtIRemark.Text = mdtFullDataTable.Rows(0)("calibration_remark")
            End If
            mdtApplyNew = mdtFullDataTable.Copy()
            gvApplyCalibration.DataSource = mdtApplyNew
            gvApplyCalibration.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("FillApplyCalibratedList : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub SaveData(ByVal blnIsSubmitted As Boolean)
        Dim objScore As New clsComputeScore_master
        Try
            mdtApplyNew.AcceptChanges()
            Dim dt As DataTable = Nothing
            Dim eStatus As clsScoreCalibrationApproval.enCalibrationStatus
            If blnIsSubmitted Then
                eStatus = clsScoreCalibrationApproval.enCalibrationStatus.Submitted
            Else
                eStatus = clsScoreCalibrationApproval.enCalibrationStatus.NotSubmitted
            End If
            'If mintCalibrationUnkid <= 0 Then
            'dt = New DataView(mdtApplyNew, "isChanged = true", "", DataViewRowState.CurrentRows).ToTable()
            'Else
            dt = New DataView(mdtApplyNew, "", "", DataViewRowState.CurrentRows).ToTable()
            'End If
            dt = New DataView(mdtApplyNew, "", "", DataViewRowState.CurrentRows).ToTable()
            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                'S.SANDEEP |16-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                'If objScore.Update_Calibration(mdtApplyNew, _
                '                               Session("Database_Name"), _
                '                               CInt(Session("CompanyUnkId")), _
                '                               Session("Fin_year"), _
                '                               txtIRemark.Text, _
                '                               eStatus, _
                '                               ConfigParameter._Object._CurrentDateAndTime, _
                '                               enAuditType.ADD, _
                '                               Session("UserId"), _
                '                               Session("IP_ADD").ToString(), _
                '                               Session("HOST_NAME").ToString(), _
                '                               mstrModuleName1, _
                '                               True, _
                '                               txtICalibrationNo.Text, _
                '                               blnIsSubmitted) = True Then
                Dim strGeneratedCalibNo As String = String.Empty

                'S.SANDEEP |05-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                Dim intGeneratedCalibUnkid As Integer = 0
                'If objScore.Update_Calibration(mdtApplyNew, _
                '                               Session("Database_Name"), _
                '                               CInt(Session("CompanyUnkId")), _
                '                               Session("Fin_year"), _
                '                               txtIRemark.Text, _
                '                               eStatus, _
                '                               ConfigParameter._Object._CurrentDateAndTime, _
                '                               enAuditType.ADD, _
                '                               Session("UserId"), _
                '                               Session("IP_ADD").ToString(), _
                '                               Session("HOST_NAME").ToString(), _
                '                               mstrModuleName1, _
                '                               True, _
                '                               txtICalibrationNo.Text, _
                '                               blnIsSubmitted, _
                '                               strGeneratedCalibNo) = True Then

                If objScore.Update_Calibration(mdtApplyNew, _
                                               Session("Database_Name"), _
                                               CInt(Session("CompanyUnkId")), _
                                               Session("Fin_year"), _
                                               txtIRemark.Text, _
                                               eStatus, _
                                               ConfigParameter._Object._CurrentDateAndTime, _
                                               enAuditType.ADD, _
                                               Session("UserId"), _
                                               Session("IP_ADD").ToString(), _
                                               Session("HOST_NAME").ToString(), _
                                               mstrModuleName1, _
                                               True, _
                                               txtICalibrationNo.Text, _
                                               blnIsSubmitted, _
                                           strGeneratedCalibNo, _
                                           intGeneratedCalibUnkid) = True Then
                    'S.SANDEEP |05-SEP-2019| -- END

                    'S.SANDEEP |16-AUG-2019| -- END


                    'S.SANDEEP |14-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : 2 Messages was Required
                    If blnIsSubmitted Then
                        'S.SANDEEP |16-AUG-2019| -- START
                        'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                        Dim mstrEmpids As String = String.Join(",", mdtApplyNew.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                        'S.SANDEEP |05-SEP-2019| -- START
                        'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                        'objScoreCalibrateApproval.SendNotification(1, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), Session("CompanyUnkId"), Session("Fin_year"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("UserId"), CInt(cboSPeriod.SelectedValue), cboSPeriod.SelectedItem.Text, strGeneratedCalibNo, Session("EmployeeAsOnDate"), mstrEmpids, mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), Session("UserName"), -1, "", mstrEmpids, Nothing)
                        objScoreCalibrateApproval.SendNotification(1, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), Session("CompanyUnkId"), Session("Fin_year"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("UserId"), CInt(cboSPeriod.SelectedValue), cboSPeriod.SelectedItem.Text, strGeneratedCalibNo, Session("EmployeeAsOnDate"), mstrEmpids, mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), Session("UserName"), -1, "", mstrEmpids, Nothing, intGeneratedCalibUnkid)
                        'S.SANDEEP |05-SEP-2019| -- END


                        'S.SANDEEP |16-AUG-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 9, "Information Submitted Successfully."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 15, "Information Saved Successfully."), Me)
                    End If
                    'S.SANDEEP |14-AUG-2019| -- END
                    blnShowApplyCalibrationpopup = False
                    popupApproverUseraccess.Hide()
                    FillGrid()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("SaveData : " & ex.Message, Me)
        Finally
            objScore = Nothing
        End Try
    End Sub

    Private Sub ClearData()
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'radApplyToChecked.Checked = False
            'radApplyToAll.Checked = False
            'S.SANDEEP |27-JUL-2019| -- END
            cbocRating.SelectedValue = 0
            txtIRemark.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("ClearData : " & ex.Message, Me)
        End Try
    End Sub

    Public Function IsValidData() As Boolean
        Try
            If txtICalibrationNo.Enabled = True AndAlso txtICalibrationNo.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue."), Me)
                Return False
            End If

            If txtICalibrationNo.Enabled = True AndAlso txtICalibrationNo.Text.Trim.Length > 0 Then
                Dim objComputeScore As New clsComputeScore_master
                If objComputeScore.IsCalibrationNoExist(txtICalibrationNo.Text.Trim) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 8, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue."), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidData : " & ex.Message, Me)
        End Try
    End Function

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private Sub FilterData()
        Try
            Dim dt As New DataTable
            Dim dtfinal As New DataTable
            Dim oView As DataView = mdtApplyNew.DefaultView
            Dim StrSearch As String = String.Empty
            If txtListSearch.Text.Trim.Length > 0 Then
                StrSearch = "Code LIKE '%" & txtListSearch.Text & "%' OR " & _
                            "EmployeeName LIKE '%" & txtListSearch.Text & "%' OR " & _
                            "JobTitle LIKE '%" & txtListSearch.Text & "%' "
            End If
            oView.RowFilter = StrSearch
            dt = oView.ToTable() : dtfinal = dt.Clone()
            If dt IsNot Nothing Then
                If gvFilterRating.Rows.Count > 0 Then
                    Dim gvr = gvFilterRating.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkFRating"), CheckBox).Checked = True)
                    If gvr IsNot Nothing AndAlso gvr.Count > 0 Then
                        For Each rItem In gvr
            Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0
                            mDecFromScore = CDec(gvFilterRating.DataKeys(rItem.RowIndex)("scrf").ToString())
                            mDecToScore = CDec(gvFilterRating.DataKeys(rItem.RowIndex)("scrt").ToString())
                            StrSearch = "(Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
                            If dt.Rows.Count > 0 Then
                                If dt.Select(StrSearch).Length > 0 Then
                                    dtfinal.Merge(dt.Select(StrSearch).CopyToDataTable().Copy, True)
                                End If
                    Else
                                If dt.Select(StrSearch).Length > 0 Then
                                    dtfinal = dt.Select(StrSearch).CopyToDataTable()
                    End If
                End If
                        Next
                    ElseIf gvr IsNot Nothing AndAlso gvr.Count <= 0 Then
                        dtfinal = dt.Copy
                End If
                End If
            End If

            gvApplyCalibration.AutoGenerateColumns = False
            gvApplyCalibration.DataSource = dtfinal
            gvApplyCalibration.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError("FilterData : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

#Region " Link's Event(s) "

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    'Protected Sub lnkFilterData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFilterData.Click
    '    Try
    '        Dim oView As DataView = mdtApplyNew.DefaultView
    '        Dim StrSearch As String = String.Empty
    '        If txtSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "Code LIKE '%" & txtSearch.Text & "%' OR " & _
    '                        "EmployeeName LIKE '%" & txtSearch.Text & "%' OR " & _
    '                        "JobTitle LIKE '%" & txtSearch.Text & "%' "
    '        End If

    '        Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0

    '        If cboPRatingFrm.SelectedIndex > 0 AndAlso cboPRatingTo.SelectedIndex > 0 Then
    '            If cboPRatingTo.SelectedIndex < cboPRatingFrm.SelectedIndex Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 11, "Sorry, To rating cannot be less than From rating. Please select higher rating to continue."), Me)
    '                Exit Sub
    '            Else
    '                Dim dr() As DataRow = mdtRatings.Select("id = '" & cboPRatingFrm.SelectedValue & "'")
    '                If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
    '                dr = mdtRatings.Select("id = '" & cboPRatingTo.SelectedValue & "'")
    '                If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
            '    If StrSearch.Trim.Length > 0 Then
            '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
            '    Else
            '        StrSearch &= " (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
            '    End If
    '            End If
    '        ElseIf cboPRatingFrm.SelectedIndex > 0 Then
    '            Dim dr() As DataRow = mdtRatings.Select("id = '" & cboPRatingFrm.SelectedValue & "'")
    '            If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
            '    If StrSearch.Trim.Length > 0 Then
            '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & ") "
            '    Else
    '                StrSearch &= " (Provision_Score >= " & mDecFromScore & ") "
            '    End If
    '        ElseIf cboPRatingTo.SelectedIndex > 0 Then
    '            Dim dr() As DataRow = mdtRatings.Select("id = '" & cboPRatingTo.SelectedValue & "'")
    '            If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
            '    If StrSearch.Trim.Length > 0 Then
            '        StrSearch &= " AND (Provision_Score <= " & mDecToScore & ") "
            '    Else
            '        StrSearch &= " (Provision_Score <= " & mDecToScore & ") "
            '    End If
    '        End If


    '        'If txtScoreFrom.Text.Trim.Length > 0 Then Decimal.TryParse(txtScoreFrom.Text, mDecFromScore)
    '        'If txtScoreTo.Text.Trim.Length > 0 Then Decimal.TryParse(txtScoreTo.Text, mDecToScore)

    '        'If txtScoreFrom.Text.Trim.Length > 0 AndAlso txtScoreTo.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '        '    Else
    '        '        StrSearch &= " (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '        '    End If
    '        'ElseIf txtScoreFrom.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & ") "
    '        '    Else
    '        '        StrSearch &= "(Provision_Score >= " & mDecFromScore & ") "
    '        '    End If
    '        'ElseIf txtScoreTo.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score <= " & mDecToScore & ") "
    '        '    Else
    '        '        StrSearch &= " (Provision_Score <= " & mDecToScore & ") "
    '        '    End If
    '        'End If

    '        oView.RowFilter = StrSearch
    '        gvApplyCalibration.AutoGenerateColumns = False
    '        gvApplyCalibration.DataSource = oView
    '        gvApplyCalibration.DataBind()
    '        'mView = oView
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("lnkFilterData_Click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |26-AUG-2019| -- END

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAllocation_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkCopyData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyData.Click
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'If radApplyToChecked.Checked = False AndAlso radApplyToAll.Checked = False Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Sorry, Please select atleast one of the operation in order to apply changes in the list."), Me)
            '    Exit Sub
            'End If

            'If radApplyToChecked.Checked = True Then
            '    If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score."), Me)
            '        Exit Sub
            '    End If
            'End If

            'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            'If radApplyToChecked.Checked = True Then
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            'Else
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            'End If
                If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score."), Me)
                    Exit Sub
                End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
                gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            'S.SANDEEP |27-JUL-2019| -- END

            For Each iRow In gRow
                Dim mRow As DataRow() = Nothing
                mRow = mdtApplyNew.Select("Code = '" & iRow.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text & "'")
                If mRow.Length > 0 Then
                    mRow(0)("calibrated_score") = Format(CDbl(iRow.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSProvision", False, True)).Text), "###################0.#0")
                    mRow(0)("isChanged") = True
                    mRow(0)("calibration_remark") = txtIRemark.Text
                    Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mRow(0)("calibrated_score") & " AND scrt >= " & mRow(0)("calibrated_score"))
                    If dr.Length > 0 Then
                        mRow(0)("calibRating") = dr(0)("name")
                    End If
                End If
            Next
            mdtApplyNew.AcceptChanges()
            gvApplyCalibration.AutoGenerateColumns = False
            gvApplyCalibration.DataSource = mdtApplyNew
            gvApplyCalibration.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkCopyData_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInfo.Click
        Try
            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False)
            gvRating.AutoGenerateColumns = False
            gvRating.DataSource = dsList
            gvRating.DataBind()
            objRating = Nothing
            popupinfo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkInfo_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvApplyCalibration.Rows.Count <= 0 Then Exit Sub
            Dim dvEmployee As DataView = mdtApplyNew.DefaultView
            For i As Integer = 0 To gvApplyCalibration.Rows.Count - 1
                Dim gvRow As GridViewRow = gvApplyCalibration.Rows(i)
                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
                Dim dRow() As DataRow = CType(Me.ViewState("mdtApplyNew"), DataTable).Select("Code = '" & gvRow.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("iCheck") = cb.Checked
                End If
                dvEmployee.Table.AcceptChanges()
            Next
            Me.ViewState("mdtApplyNew") = dvEmployee.ToTable
        Catch ex As Exception
            DisplayMessage.DisplayError("chkHeder1_CheckedChanged : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                Dim dRow() As DataRow = mdtApplyNew.Select("Code = '" & gvr.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("iCheck") = cb.Checked
                End If
                mdtApplyNew.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("chkbox1_CheckedChanged : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Protected Sub chkfrating_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            FilterData()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkfrating_CheckedChanged : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END


#End Region

#Region " Grid Event(s) "

    Protected Sub gvApplyCalibration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApplyCalibration.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSProvision", False, True)).Text <> "&nbsp;" Then
                    'S.SANDEEP |26-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                    'Dim mDecValue As = 0
                    Dim mDecValue As Decimal = 0
                    'S.SANDEEP |26-AUG-2019| -- END
                    mDecValue = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSProvision", False, True)).Text)
                    Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mDecValue & " AND scrt >= " & mDecValue)
                    If dr.Length > 0 Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhprovRating", False, True)).Text = dr(0)("name")
                    End If
                End If
                If gvApplyCalibration.DataKeys(e.Row.RowIndex)("calibrated_score").ToString.Trim.Length > 0 Then
                    Dim mDecValue = 0
                    mDecValue = CDec(gvApplyCalibration.DataKeys(e.Row.RowIndex)("calibrated_score").ToString)
                    If mDecValue > 0 Then
                        Dim dr() As DataRow = mdtRatings.Select("scrf <= " & mDecValue & " AND scrt >= " & mDecValue)
                        If dr.Length > 0 Then
                            e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Text = dr(0)("name")
                        End If
                    End If
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvApplyCalibration_RowDataBound : " & ex.Message, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
#Region " Textbox Event(s) "

    Protected Sub txtListSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtListSearch.TextChanged
        Try
            'Dim oView As DataView = mdtApplyNew.DefaultView
            'Dim StrSearch As String = String.Empty
            'If txtSearch.Text.Trim.Length > 0 Then
            '    StrSearch = "Code LIKE '%" & txtSearch.Text & "%' OR " & _
            '                "EmployeeName LIKE '%" & txtSearch.Text & "%' OR " & _
            '                "JobTitle LIKE '%" & txtSearch.Text & "%' "
            'End If
            'oView.RowFilter = StrSearch
            'gvApplyCalibration.AutoGenerateColumns = False
            'gvApplyCalibration.DataSource = oView
            'gvApplyCalibration.DataBind()
            FilterData()
        Catch ex As Exception
            DisplayMessage.DisplayError("txtListSearch_TextChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

    'S.SANDEEP |11-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : PA CHANGES
    Private Sub SetControlCaptions()
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.lblBatchNo.ID, Me.lblBatchNo.Text)
            Language._Object.setCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.lblSubmitDateFrom.ID, Me.lblSubmitDateFrom.Text)
            Language._Object.setCaption(Me.lblSubmitDateTo.ID, Me.lblSubmitDateTo.Text)
            Language._Object.setCaption(Me.btnShowAll.ID, Me.btnShowAll.Text)
            Language._Object.setCaption(Me.btnNew.ID, Me.btnNew.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(Me.lblMyReport.ID, Me.lblMyReport.Text)
            Language._Object.setCaption(Me.btnCloseMyReport.ID, Me.btnCloseMyReport.Text)

            Language._Object.setCaption(Me.lblRptPeriod.ID, Me.lblRptPeriod.Text)
            Language._Object.setCaption(Me.lblRptCalibNo.ID, Me.lblRptCalibNo.Text)
            Language._Object.setCaption(Me.btnRptShow.ID, Me.btnRptShow.Text)
            Language._Object.setCaption(Me.lblRptApprover.ID, Me.lblRptApprover.Text)
            Language._Object.setCaption(Me.lblRptEmployee.ID, Me.lblRptEmployee.Text)
            Language._Object.setCaption(Me.btnRptReset.ID, Me.btnRptReset.Text)
            Language._Object.setCaption(Me.btnExportMyReport.ID, Me.btnExportMyReport.Text)

            Language._Object.setCaption(gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)

            Language._Object.setCaption(gvMyReport.Columns(0).FooterText, gvMyReport.Columns(0).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(1).FooterText, gvMyReport.Columns(1).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(2).FooterText, gvMyReport.Columns(2).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(3).FooterText, gvMyReport.Columns(3).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(4).FooterText, gvMyReport.Columns(4).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(5).FooterText, gvMyReport.Columns(5).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(6).FooterText, gvMyReport.Columns(6).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(7).FooterText, gvMyReport.Columns(7).HeaderText)


            Language.setLanguage(mstrModuleName1)

            Language._Object.setCaption(Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            Language._Object.setCaption(Me.lblFilter.ID, Me.lblFilter.Text)
            Language._Object.setCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Language._Object.setCaption(Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            Language._Object.setCaption(Me.btnISearch.ID, Me.btnISearch.Text)
            Language._Object.setCaption(Me.btnSReset.ID, Me.btnSReset.Text)
            Language._Object.setCaption(Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Language._Object.setCaption(Me.lblCRating.ID, Me.lblCRating.Text)
            Language._Object.setCaption(Me.lnkCopyData.ID, Me.lnkCopyData.Text)
            Language._Object.setCaption(Me.lblIRemark.ID, Me.lblIRemark.Text)
            Language._Object.setCaption(Me.btnIApply.ID, Me.btnIApply.Text)

            Language._Object.setCaption(Me.btnISave.ID, Me.btnISave.Text)
            Language._Object.setCaption(Me.btnISubmit.ID, Me.btnISubmit.Text)
            Language._Object.setCaption(Me.btnIClose.ID, Me.btnIClose.Text)
            Language._Object.setCaption(Me.lblRatingInfo.ID, Me.lblRatingInfo.Text)
            Language._Object.setCaption(Me.btnCloseRating.ID, Me.btnCloseRating.Text)

            Language._Object.setCaption(Me.lblFilterData.ID, Me.lblFilterData.Text)
            Language._Object.setCaption(gvFilterRating.Columns(1).FooterText, gvFilterRating.Columns(1).HeaderText)

            Language._Object.setCaption(gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(6).FooterText, gvApplyCalibration.Columns(6).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(7).FooterText, gvApplyCalibration.Columns(7).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(8).FooterText, gvApplyCalibration.Columns(8).HeaderText)

            Language._Object.setCaption(gvRating.Columns(0).FooterText, gvRating.Columns(0).HeaderText)
            Language._Object.setCaption(gvRating.Columns(1).FooterText, gvRating.Columns(1).HeaderText)
            Language._Object.setCaption(gvRating.Columns(2).FooterText, gvRating.Columns(2).HeaderText)
            'S.SANDEEP |27-JUL-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("SetControlCaptions : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblBatchNo.Text = Language._Object.getCaption(Me.lblBatchNo.ID, Me.lblBatchNo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblSubmitDateFrom.Text = Language._Object.getCaption(Me.lblSubmitDateFrom.ID, Me.lblSubmitDateFrom.Text)
            Me.lblSubmitDateTo.Text = Language._Object.getCaption(Me.lblSubmitDateTo.ID, Me.lblSubmitDateTo.Text)
            Me.btnShowAll.Text = Language._Object.getCaption(Me.btnShowAll.ID, Me.btnShowAll.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text)
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text)
            Me.lblMyReport.Text = Language._Object.getCaption(Me.lblMyReport.ID, Me.lblMyReport.Text)
            Me.btnCloseMyReport.Text = Language._Object.getCaption(Me.btnCloseMyReport.ID, Me.btnCloseMyReport.Text)

            gvCalibrateList.Columns(3).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            gvCalibrateList.Columns(4).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            gvCalibrateList.Columns(5).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            gvCalibrateList.Columns(6).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            gvCalibrateList.Columns(7).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            gvCalibrateList.Columns(8).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)

            gvMyReport.Columns(0).HeaderText = Language._Object.getCaption(gvMyReport.Columns(0).FooterText, gvMyReport.Columns(0).HeaderText)
            gvMyReport.Columns(1).HeaderText = Language._Object.getCaption(gvMyReport.Columns(1).FooterText, gvMyReport.Columns(1).HeaderText)
            gvMyReport.Columns(2).HeaderText = Language._Object.getCaption(gvMyReport.Columns(2).FooterText, gvMyReport.Columns(2).HeaderText)
            gvMyReport.Columns(3).HeaderText = Language._Object.getCaption(gvMyReport.Columns(3).FooterText, gvMyReport.Columns(3).HeaderText)
            gvMyReport.Columns(4).HeaderText = Language._Object.getCaption(gvMyReport.Columns(4).FooterText, gvMyReport.Columns(4).HeaderText)
            gvMyReport.Columns(5).HeaderText = Language._Object.getCaption(gvMyReport.Columns(5).FooterText, gvMyReport.Columns(5).HeaderText)
            gvMyReport.Columns(6).HeaderText = Language._Object.getCaption(gvMyReport.Columns(6).FooterText, gvMyReport.Columns(6).HeaderText)
            gvMyReport.Columns(7).HeaderText = Language._Object.getCaption(gvMyReport.Columns(7).FooterText, gvMyReport.Columns(7).HeaderText)

            Language.setLanguage(mstrModuleName1)

            Me.lblCalibrateHeading.Text = Language._Object.getCaption(Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            Me.lblFilter.Text = Language._Object.getCaption(Me.lblFilter.ID, Me.lblFilter.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblSPeriod.Text = Language._Object.getCaption(Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            Me.btnISearch.Text = Language._Object.getCaption(Me.btnISearch.ID, Me.btnISearch.Text)
            Me.btnSReset.Text = Language._Object.getCaption(Me.btnSReset.ID, Me.btnSReset.Text)
            Me.lblCalibrationNo.Text = Language._Object.getCaption(Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Me.lblCRating.Text = Language._Object.getCaption(Me.lblCRating.ID, Me.lblCRating.Text)
            Me.lnkCopyData.Text = Language._Object.getCaption(Me.lnkCopyData.ID, Me.lnkCopyData.Text)
            Me.lblIRemark.Text = Language._Object.getCaption(Me.lblIRemark.ID, Me.lblIRemark.Text)
            Me.btnIApply.Text = Language._Object.getCaption(Me.btnIApply.ID, Me.btnIApply.Text)
            'Me.lblpRatingFrm.Text = Language._Object.getCaption(Me.lblpRatingFrm.ID, Me.lblpRatingFrm.Text)
            'Me.lblpRatingTo.Text = Language._Object.getCaption(Me.lblpRatingTo.ID, Me.lblpRatingTo.Text)
            'Me.lnkFilterData.Text = Language._Object.getCaption(Me.lnkFilterData.ID, Me.lnkFilterData.Text)
            Me.btnISave.Text = Language._Object.getCaption(Me.btnISave.ID, Me.btnISave.Text)
            Me.btnISubmit.Text = Language._Object.getCaption(Me.btnISubmit.ID, Me.btnISubmit.Text)
            Me.btnIClose.Text = Language._Object.getCaption(Me.btnIClose.ID, Me.btnIClose.Text)
            Me.lblRatingInfo.Text = Language._Object.getCaption(Me.lblRatingInfo.ID, Me.lblRatingInfo.Text)
            Me.btnCloseRating.Text = Language._Object.getCaption(Me.btnCloseRating.ID, Me.btnCloseRating.Text)

            gvApplyCalibration.Columns(1).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            gvApplyCalibration.Columns(2).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            gvApplyCalibration.Columns(3).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            gvApplyCalibration.Columns(4).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            gvApplyCalibration.Columns(5).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)
            gvApplyCalibration.Columns(6).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(6).FooterText, gvApplyCalibration.Columns(6).HeaderText)
            gvApplyCalibration.Columns(7).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(7).FooterText, gvApplyCalibration.Columns(7).HeaderText)
            gvApplyCalibration.Columns(8).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(8).FooterText, gvApplyCalibration.Columns(8).HeaderText)

            gvRating.Columns(0).HeaderText = Language._Object.getCaption(gvRating.Columns(0).FooterText, gvRating.Columns(0).HeaderText)
            gvRating.Columns(1).HeaderText = Language._Object.getCaption(gvRating.Columns(1).FooterText, gvRating.Columns(1).HeaderText)
            gvRating.Columns(2).HeaderText = Language._Object.getCaption(gvRating.Columns(2).FooterText, gvRating.Columns(2).HeaderText)

            'S.SANDEEP |27-JUL-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("SetLanguage : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |11-JUL-2019| -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage("frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage("frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period.")
            Language.setMessage(mstrModuleName, 1, "Are you sure you wanted to delete information.")
            Language.setMessage(mstrModuleName, 2, "Information deleted Successfully.")
            Language.setMessage(mstrModuleName, 3, "Enter reason to void data.")
            Language.setMessage(mstrModuleName, 20, "Period :")
            Language.setMessage(mstrModuleName, 21, "Calibration No :")
            Language.setMessage(mstrModuleName, 22, "Employee :")
            Language.setMessage(mstrModuleName, 23, "Approver :")
            Language.setMessage(mstrModuleName, 24, "Approver Wise Status")
            Language.setMessage(mstrModuleName, 25, "Sorry, there is no data in order to export.")
            Language.setMessage(mstrModuleName, 100, "Ratings")
            Language.setMessage(mstrModuleName, 101, "%")
            Language.setMessage(mstrModuleName, 102, "Normal Distribution")
            Language.setMessage(mstrModuleName, 103, "Actual Performance")
            Language.setMessage(mstrModuleName, 200, "Performance HPO Curve")
            Language.setMessage(mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue")
            Language.setMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score.")
            Language.setMessage(mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue.")
            Language.setMessage(mstrModuleName1, 5, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue.")
            Language.setMessage(mstrModuleName1, 7, "Are you sure you want to submit without remark?")
            Language.setMessage(mstrModuleName1, 8, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue.")
            Language.setMessage(mstrModuleName1, 9, "Information Submitted Successfully.")
            Language.setMessage(mstrModuleName1, 10, "Date")
            Language.setMessage(mstrModuleName1, 13, "Total")
            Language.setMessage(mstrModuleName1, 15, "Information Saved Successfully.")
            Language.setMessage(mstrModuleName1, 16, "Are you sure you want to save without calibration rating(s)?")
            Language.setMessage(mstrModuleName1, 19, "By")

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages:- " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
