﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ApprovalList.aspx.vb" Inherits="wPg_ApprovalList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvFilter" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvRptFilter" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="Date" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <style>
        .vl
        {
            border-left: 1px solid black;
            height: 15px;
        }
    </style>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Score Calibration Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria/Approver Info."></asp:Label>
                                    </div>
                                    <div style="float: right">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                            Visible="false"></asp:LinkButton>
                                        <asp:LinkButton ID="lnklstInfo" runat="server" ToolTip="View Rating Information"
                                            Text="Show Ratings" Font-Underline="false"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </div>
                                        <div style="width: 33%" class="ib">
                                            <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 6%" class="ib">
                                            <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                        </div>
                                        <div style="width: 23%" class="ib">
                                            <asp:TextBox ID="txtApprover" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblLevel" runat="server" Text="Level"></asp:Label>
                                        </div>
                                        <div style="width: 15%" class="ib">
                                            <asp:TextBox ID="txtLevel" runat="server" ReadOnly="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <hr style="margin: 9px; margin-left: -1px;" />
                                    <div class="row2">
                                        <div style="width: 5%" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </div>
                                        <div style="width: 20%" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 6%" class="ib">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        </div>
                                        <div style="width: 20%" class="ib">
                                            <asp:DropDownList ID="cboStatus" runat="server" Width="234px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                            <%--<asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>--%>
                                            <asp:Label ID="lblRemark" runat="server" Text="Calib. Remark"></asp:Label>
                                            <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                        </div>
                                        <div style="width: 28%" class="ib">
                                            <asp:TextBox ID="txtRemark" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 10%" class="ib">
                                            <asp:Label ID="lblPScoreFrom" runat="server" Text="Prov. Rating From"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbopRatingFrm" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 2%" class="ib">
                                            <asp:Label ID="lblPScTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbopRatingTo" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 11%" class="ib">
                                            <asp:Label ID="lblCScoreForm" runat="server" Text="Calb. Rating From"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbocRatingFrm" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 3%" class="ib">
                                            <asp:Label ID="lblCScTo" runat="server" Text="To"></asp:Label>
                                        </div>
                                        <div style="width: 5%" class="ib">
                                            <asp:DropDownList ID="cbocRatingTo" runat="server" Width="66px">
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 8%" class="ib">
                                            <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No"></asp:Label>
                                        </div>
                                        <div style="width: 28%" class="ib">
                                            <asp:TextBox ID="txtCalibrationNo" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Font-Bold="true"
                                                Checked="true" OnCheckedChanged="chkMyApproval_CheckedChanged" AutoPostBack="true"
                                                Visible="false" />
                                            <asp:Button ID="btnShowAll" runat="server" Text="My Report" CssClass="btndefault" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                            <asp:Button ID="btnDisplayCurve" runat="server" Text="Display HPO Curve" CssClass="btnDefault" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                        </div>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="scrollable-container" style="width: 100%; height: 250px; overflow: auto">
                            <asp:GridView ID="gvCalibrateList" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,lusername,allocation,calibuser,ispgrp,pgrpid"
                                runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                ToolTip="Approve/Reject" OnClick="lnkedit_Click"><i class="fa fa-tasks" style="font-size:14px; color:Red;"></i>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPlus" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                ToolTip="Expand" OnClick="lnkPlus_Click"><i class="fa fa-plus" style="font-size:14px;color:Blue"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lnkMins" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                ToolTip="Collapse" OnClick="lnkMins_Click" Visible="false"><i class="fa fa-minus" style="font-size:14px;color:Blue"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgEmployee" />
                                    <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhlstpRating" />
                                    <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                    <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhsacrating" />
                                    <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhsacremark" />
                                    <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                    <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhlstcRating" />
                                    <asp:BoundField DataField="apcalibremark" HeaderText="Calib. Remark" FooterText="dgcolheRemark" />
                                    <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                    <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhsApprovalRemark" />
                                    <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                    <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgiStatus" />
                                    <%--<asp:BoundField DataField="username" HeaderText="Approver" FooterText="dgApprover" />--%>
                                    <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgLevelName" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div id="btnfixedbottom" class="btn-default">
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        TargetControlID="lblCalibrateHeading" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblFilter" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 900px; top: 30px;" Height="535px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblCalibrateHeading" runat="server" Text="BSC Calibration"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblFilter" runat="server" Text="Search Criteria" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="margin: auto; padding: auto; height: auto;">
                                        <div class="row2">
                                            <div style="width: 7%" class="ib">
                                                <asp:Label ID="lblSPeriod" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div style="width: 89%" class="ib">
                                                <asp:DropDownList ID="cboSPeriod" runat="server" Width="762px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row2">
                                            <%--<div style="width: 15%" class="ib">
                                                <asp:Label ID="lblICalibrationNo" runat="server" Text="Calibration No."></asp:Label>
                                            </div>
                                            <div style="width: 18%" class="ib">
                                                <asp:TextBox ID="txtICalibrationNo" runat="server"></asp:TextBox>
                                            </div>--%>
                                            <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                            <%--<div style="width: 16%" class="ib">
                                                <asp:RadioButton ID="radApplyToChecked" runat="server" Text="Apply to Checked" GroupName="rad">
                                                </asp:RadioButton>
                                            </div>
                                            <div style="width: 11%" class="ib">
                                                <asp:RadioButton ID="radApplyToAll" runat="server" Text="Apply to All" GroupName="rad">
                                                </asp:RadioButton>
                                            </div>
                                            <div style="width: 9%" class="ib">
                                                <asp:Label ID="lblCRating" runat="server" Text="Calib. Rating"></asp:Label>
                                            </div>
                                            <div style="width: 24%" class="ib">
                                                <asp:DropDownList ID="cbocRating" runat="server" Width="207px">
                                                </asp:DropDownList>
                                            </div>--%>
                                            <div style="width: 13%" class="ib">
                                                <asp:Label ID="lblCRating" runat="server" Text="Calibration Rating"></asp:Label>
                                            </div>
                                            <div style="width: 23%" class="ib">
                                                <asp:DropDownList ID="cbocRating" runat="server" Width="200px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 14%;" class="ib">
                                                <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                                <%--<asp:Label ID="lblIRemark" runat="server" Text="Remark"></asp:Label>--%>
                                                <asp:Label ID="lblIRemark" runat="server" Text="Calibration Remark"></asp:Label>
                                                <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                            </div>
                                            <div style="width: 30%; vertical-align: bottom;" class="ib">
                                                <asp:TextBox ID="txtIRemark" runat="server" Width="101%"></asp:TextBox>
                                            </div>
                                            <div style="width: 11%; margin-left: 5px;" class="ib">
                                                <asp:Button ID="btnIApply" runat="server" Text="Apply" CssClass="btndefault" />
                                            </div>
                                            <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <%--'S.SANDEEP |26-AUG-2019| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3--%>
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblFilterData" runat="server" Text="Search"></asp:Label>
                                    </div>
                                    <div style="width: 90%;" class="ib">
                                        <asp:TextBox ID="txtListSearch" runat="server" Width="99%" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                    <%--<div style="width: 48%; margin-left: 7px" class="ib">
                                        <asp:TextBox ID="txtSearch" runat="server" Width="424px"></asp:TextBox>
                                    </div>
                                    <div style="width: 13%;" class="ib">
                                        <asp:Label ID="lblpRatingFrm" runat="server" Text="Prov. Rating From"></asp:Label>
                                    </div>
                                    <div style="width: 11%;" class="ib">
                                        <asp:DropDownList ID="cboRatingFrom" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 2%;" class="ib">
                                        <asp:Label ID="lblpRatingTo" runat="server" Text="To"></asp:Label>
                                    </div>
                                    <div style="width: 11%;" class="ib">
                                        <asp:DropDownList ID="cboRatingTo" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 1%;" class="ib">
                                        <asp:LinkButton ID="lnkFilterData" runat="server" Text="Filter" Font-Underline="false"
                                            Font-Bold="true"></asp:LinkButton>
                                    </div>--%>
                                    <%--'S.SANDEEP |26-AUG-2019| -- END--%>
                                </div>
                                <div class="row2">
                                    <div style="float: left; width: 19%; height: 204px;">
                                        <div>
                                            <div style="width: 100%; height: 200px;">
                                                <asp:Panel ID="pnlRatingF" runat="server" Height="200px" Width="100%" ScrollBars="Auto">
                                                    <asp:GridView ID="gvFilterRating" runat="server" AutoGenerateColumns="False" Width="95%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="scrf,scrt,id">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="20">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkFRating" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>'
                                                                        OnCheckedChanged="chkfrating_CheckedChanged" AutoPostBack="true" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="name" HeaderText="Filter By Calib. Rating" FooterText="dgcolhFRating" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                    </div>
                                </div>
                                    </div>
                                    <div id="Div4" style="width: 80%; height: 204px; overflow: auto;">
                                        <asp:GridView ID="gvApplyCalibration" DataKeyNames="employeeunkid,periodunkid,calibratnounkid,calibratescore,code,lusername"
                                            runat="server" AutoGenerateColumns="False" Width="120%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="25">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>'
                                                        AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhSEmployee" />
                                            <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="dgcolhSJob" />
                                            <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhprovRating" />
                                                <%--'S.SANDEEP |26-AUG-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3--%>
                                                <asp:BoundField DataField="lrating" HeaderText="" FooterText="dgcolhlastlvlrating" />
                                                <asp:BoundField DataField="lcalibremark" HeaderText="" FooterText="dgcolhlastlvlremark" />
                                                <%--'S.SANDEEP |26-AUG-2019| -- END--%>
                                                <asp:BoundField DataField="acrating" HeaderText="Calib. Rating" FooterText="dgcolhcalibRating" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                            <%--<asp:BoundField DataField="calibration_remark" HeaderText="Remark" FooterText="dgcolhcalibrationremark"
                                                ItemStyle-Wrap="true" />--%>
                                                <asp:BoundField DataField="acremark" HeaderText="Calibration Remark" FooterText="dgcolhcalibrationremark"
                                                ItemStyle-Wrap="true" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                </div>
                            </div>
                                    <div class="row2">
                                <div style="width: 6%; margin-left: 10px;" class="ib">
                                            <asp:Label ID="lblApprRemark" runat="server" Text="Remark"></asp:Label>
                                    </div>
                                <div style="width: 90%;" class="ib">
                                    <asp:TextBox ID="txtApprRemark" runat="server" Width="100%"></asp:TextBox>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div id="Div1" runat="server" style="float: left">
                                    <asp:LinkButton ID="lnkInfo" runat="server" ToolTip="View Rating Information"><i class="fa fa-info-circle" style="font-size:25px;color:blue"></i></asp:LinkButton>
                                </div>
                                <asp:Button ID="btnIApprove" runat="server" Text="Approve" CssClass="btndefault" />
                                <asp:Button ID="btnIReject" runat="server" Text="Reject" CssClass="btndefault" />
                                <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupinfo" BackgroundCssClass="modalBackground" TargetControlID="lblRatingInfo"
                        runat="server" PopupControlID="pnlRatingInfo" CancelControlID="btnCloseRating" />
                    <asp:Panel ID="pnlRatingInfo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 500px; top: 30px;" Height="313px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblRatingInfo" runat="server" Text="Ratings Information"></asp:Label>
                            </div>
                            <div class="panel-body" style="height: 274px; overflow: auto">
                                <div id="Div5" class="panel-default">
                                    <div id="Div7" style="width: 99%; height: 200px; overflow: auto;">
                                        <asp:GridView ID="gvRating" runat="server" AutoGenerateColumns="False" Width="99%"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="scrf" HeaderText="Score From" FooterText="dgcolhScrFrm"
                                                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="scrt" HeaderText="Score To" FooterText="dgcolhScrTo" HeaderStyle-Width="100px"
                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <asp:Button ID="btnCloseRating" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupMyReport" BackgroundCssClass="modalBackground" TargetControlID="lblMyReport"
                        runat="server" PopupControlID="pnlMyReport" CancelControlID="lblMyReport" />
                    <%--'S.SANDEEP |14-AUG-2019| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : WRONG BUTTON SET--%>
                    <%--REMOVED : CancelControlID="btnChClose"--%>
                    <%--ADDED : CancelControlID="btnCloseMyReport"--%>
                    <%--'S.SANDEEP |14-AUG-2019| -- END--%>
                    <asp:Panel ID="pnlMyReport" runat="server" CssClass="newpopup" Style="display: none;
                        width: 1024px; top: 30px;" Height="535px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblMyReport" runat="server" Text="My Report"></asp:Label>                                
                                <div style="float: right;">
                                    <asp:LinkButton ID="lnkRptAdvFilter" runat="server" Text="Advance Filter" Font-Underline="false"
                                        ForeColor="White"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="panel-body" style="max-height: 500px; overflow: auto; height: 440px">
                                <br />
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblRptPeriod" runat="server" Text="Period"></asp:Label>
                                    </div>
                                    <div style="width: 35%;" class="ib">
                                        <asp:DropDownList ID="cboRptPeriod" runat="server" Width="350px" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 10%;" class="ib">
                                        <asp:Label ID="lblRptCalibNo" runat="server" Text="Calibration No"></asp:Label>
                                    </div>
                                    <div style="width: 30%;" class="ib">
                                        <asp:DropDownList ID="cboRptCalibNo" runat="server" Width="286px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 6%;" class="ib">
                                        <asp:Button ID="btnRptShow" runat="server" Text="Show" CssClass="btndefault" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 6%; margin-left: 10px;" class="ib">
                                        <asp:Label ID="lblRptApprover" runat="server" Text="Approver"></asp:Label>
                                    </div>
                                    <div style="width: 35%;" class="ib">
                                        <asp:DropDownList ID="cboRptApprover" runat="server" Width="350px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 10%;" class="ib">
                                        <asp:Label ID="lblRptEmployee" runat="server" Text="Employee"></asp:Label>
                                    </div>
                                    <div style="width: 30%;" class="ib">
                                        <asp:DropDownList ID="cboRptEmployee" runat="server" Width="286px">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="width: 6%;" class="ib">
                                        <asp:Button ID="btnRptReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                    <hr />
                                </div>
                                <div class="row2">
                                    <div id="Div6" style="width: 100%; height: 325px; overflow: auto;">
                                        <asp:GridView ID="gvMyReport" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,allocation,calibuser"
                                        runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhMEmployee" />
                                            <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="dgcolhMJob"
                                                Visible="false" />
                                            <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhMprovRating" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                            <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhMacrating" />
                                            <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhMacremark" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                            <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhMcalibRating" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                                            <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                                            <%--<asp:BoundField DataField="calibration_remark" HeaderText="Remark" FooterText="dgcolhMcalibrationremark"
                                                ItemStyle-Wrap="true" />--%>
                                            <asp:BoundField DataField="apcalibremark" HeaderText="Calibration Remark" FooterText="dgcolhMcalibrationremark"
                                                ItemStyle-Wrap="true" />
                                            <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhMApprovalRemark" />
                                            <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                                            <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgcolhMiStatus" />
                                            <%--<asp:BoundField DataField="username" HeaderText="Approver" FooterText="dgcolhMusername" />--%>
                                            <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgcolhMlevelname" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            </div>
                            <div class="panel-footer">                                
                                <div style="float: left">
                                    <asp:Button ID="btnExportMyReport" runat="server" Text="Export" CssClass="btndefault" />
                                </div>
                                <asp:Button ID="btnCloseMyReport" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc2:ConfirmYesNo ID="cnfConfirm" runat="server" Title="Aruti" />
                    <%--'S.SANDEEP |27-JUL-2019| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB--%>
                    <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modalBackground" TargetControlID="lblHPOCurver"
                        runat="server" PopupControlID="pnlHPOChart" CancelControlID="lblHPOCurver" />
                    <asp:Panel ID="pnlHPOChart" runat="server" CssClass="newpopup" Style="display: none;
                        width: 920px; top: 30px;" Height="545px">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblHPOCurver" runat="server" Text="HPO Chart"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 450px; overflow: auto">
                                <div id="Div8" class="panel-default">
                                    <div id="Div9" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblChGenerate" runat="server" Text="Generate Criteria" />
                                        </div>
                                        <div style="float: right;">
                                            <asp:LinkButton ID="lnkChAllocation" runat="server" Text="Allocation" Font-Underline="false"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="Div10" class="panel-body-default" style="margin: auto; padding: auto; height: auto;">
                                        <div class="row2">
                                            <div style="width: 7%" class="ib">
                                                <asp:Label ID="lblchPeriod" runat="server" Text="Period"></asp:Label>
                                            </div>
                                            <div style="width: 28%" class="ib">
                                                <asp:DropDownList ID="cboChPeriod" runat="server" Width="240px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 10%" class="ib">
                                                <asp:Label ID="lblChDisplayType" runat="server" Text="Display Type"></asp:Label>
                                            </div>
                                            <div style="width: 22%" class="ib">
                                                <asp:DropDownList ID="cbochDisplay" runat="server" Width="192px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 25%;" class="ib">
                                                <asp:Button ID="btnChGenerate" runat="server" Text="Generate" CssClass="btndefault" />
                                                <asp:Button ID="btnChReset" runat="server" Text="Reset" CssClass="btndefault" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <asp:Panel ID="Panel4" runat="server" Height="165px" ScrollBars="Auto">
                                        <asp:DataGrid ID="dgvCalibData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            ShowHeader="false">
                                        </asp:DataGrid>
                                    </asp:Panel>
                                </div>
                                <div class="row2">
                                    <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                        BorderlineWidth="2" Width="890px" Height="300px" AntiAliasing="All">
                                        <Titles>
                                            <asp:Title Name="Title1">
                                            </asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                            </asp:Series>
                                            <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                            </asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="ChartArea1">
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        <Legends>
                                            <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                            </asp:Legend>
                                        </Legends>
                                    </asp:Chart>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnChClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <%--'S.SANDEEP |27-JUL-2019| -- END--%>
                    <uc7:AdvFilter ID="AdvanceFilter1" runat="server" />
                    <uc8:AdvRptFilter ID="AdvRptFilter" runat="server" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportMyReport" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
