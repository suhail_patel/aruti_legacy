﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_CalibrateApprover.aspx.vb" Inherits="wPg_CalibrateApprover" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtEmployeeSearch').val().length > 0) {
                $('#<%=dgvEmp.ClientID %> tbody tr').hide();
                $('#<%=dgvEmp.ClientID %> tbody tr:first').show();
                $('#<%=dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtEmployeeSearch').val() + '\')').parent().show();
            }

            else if ($('#txtSelectedEmployee').val().length > 0) {
                $('#<%=dgvApproverEmp.ClientID %> tbody tr').hide();
                $('#<%=dgvApproverEmp.ClientID %> tbody tr:first').show();
                $('#<%=dgvApproverEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSelectedEmployee').val() + '\')').parent().show();
            }


            else if ($('#dgvEmp').val().length == 0) {
                resetFromSearchValue();
            }
            else if ($('#dgvApproverEmp').val().length == 0) {
                resetFromSearchValue();
            }

            if ($('#<%=dgvEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if ($('#<%=dgvApproverEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#dgvEmp').val('');
            $('#<%= dgvEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#dgvEmp').focus();
        }




        $("[id*=ChkAllSelectedEmp]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=ChkSelectedEmp]").live("click", function() {
            debugger;
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllSelectedEmp]", grid);
            var row = $(this).closest("tr")[0];

            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }

        });
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Score Calibration Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Approver(s) List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <asp:Panel ID="pnl_filterlevel" runat="server" Width="30%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" />
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="drpLevel" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            </asp:Panel>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver" Width="100%" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpApprover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" Visible="false" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpStatus" runat="server" Visible="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="250px">
                                            <asp:GridView ID="gvApproverList" DataKeyNames="mappingunkid,isactive,IsGrp,Level"
                                                runat="server" AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>                                        
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>' ToolTip="Delete">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" CssClass="gridedit" OnClick="lnkedit_Click"
                                                                CommandArgument='<%#Eval("mappingunkid") %>' ToolTip="Edit">
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField FooterText="dgcolhActiveInactive" HeaderText="Active/Inactive Approver"
                                                        Visible="false">
                                                        <ItemTemplate>
                                                <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Make Approver Active" OnClick="lnkActive_Click"
                                                    CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="Make Approver Inactive"
                                                                OnClick="lnkDeActive_Click" CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Approver Name" DataField="approver" />                                        
                                    </Columns>
                                </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        TargetControlID="lblApproverInfo" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblHeader" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 80%;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblApproverInfo" runat="server" Text="Add/ Edit Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 600px; overflow: auto; display: flex;
                                justify-content: space-between; align-content: flex-start; flex-direction: row;">
                                <div id="Div7" class="panel-default rel" style="width: 50%;">
                                    <div id="Div8" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblHeader" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                        <div style="text-align: right">
                                            <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                                Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkSearch" runat="server" Style="vertical-align: top" Font-Bold="true"
                                                OnClick="lnkSearch_Click"><i class="fa fa-search" style="font-size:18px;color:Blue"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lnkReset" runat="server" Style="vertical-align: top" Font-Bold="true"
                                                OnClick="lnkReset_Click"><i class="fa fa-repeat" style="font-size:18px;color:Green"></i></asp:LinkButton>
                                    </div>
                                    </div>
                                    <div id="Div9" class="panel-body-default">
                                        <asp:Panel ID="pnl_approverlevel" runat="server" Width="100%">
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_level" runat="server" Text="Level"></asp:Label>
                                            </div>
                                            <div style="width: 50%" class="ib">
                                                    <asp:DropDownList ID="drpApproverUseraccess_level" runat="server" Width="400px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        </asp:Panel>
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_user" runat="server" Text="User"></asp:Label>
                                            </div>
                                            <div style="width: 50%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_user" runat="server" Width="400px" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>                                        
                                        <div class="row2">
                                            <div style="width: 100%" class="ib">
                                                <input type="text" id="txtEmployeeSearch" name="txtSearch" placeholder="type search text"
                                                    maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                            </div>
                                        </div>
                                        <div id="Div11" style="width: 100%; height: 275px; overflow: auto">
                                            <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" Width="99%"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkSelectedEmp" runat="server" />
                                                            <asp:HiddenField ID="hfemployeeunkid" runat="server" Value='<%#eval("employeeunkid") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                    <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhEmp" />
                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="btn-default btn-bottom">
                                        <asp:Button ID="btnAssign" runat="server" Text="Assign" CssClass="btndefault" />
                                    </div>
                                </div>
                                <div id="Div12" class="panel-default" style="width: 50%;">
                                    <div id="Div13" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblGreAddEditLeaveApprover" runat="server" Text="Selected Employee"
                                                Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div14" class="panel-body-default">
                                        <div class="row2">
                                            <div style="width: 100%" class="ib">
                                                <input type="text" id="txtSelectedEmployee" name="txtSearch" placeholder="type search text"
                                                    maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 100%; height: 350px; overflow: auto">
                                                <asp:GridView ID="dgvApproverEmp" runat="server" AutoGenerateColumns="false" Width="99%"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="employeeunkid,mappingtranunkid"
                                                    EmptyDataText="No Employee Selected">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelectedEmp" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEmpName" />
                                                        <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhEDept" />
                                                        <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhEJob" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnAddeditDelete" runat="server" Text="Delete" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                    <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" Title="Are You Sure You Want Delete Approval ?" />
                    <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" Title="Confirmation"
                        Message="Are You Sure You Want To Deactive  This Approver?" />
                    <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" Title="Confirmation"
                        Message="Are You Sure You Want To Active This Approver?" />
                    <uc7:AdvanceFilter ID="AdvanceFilter1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
