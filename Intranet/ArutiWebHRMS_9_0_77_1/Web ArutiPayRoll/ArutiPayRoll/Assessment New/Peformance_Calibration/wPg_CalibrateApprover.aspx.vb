﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Imports System.Web.Services

#End Region

Partial Class wPg_CalibrateApprover
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private objApproverMaster As New clscalibrate_approver_master
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Private objApproverTran As New clscalibrate_approver_tran
    Private mstrAdvanceFilter As String = String.Empty
    Private mblnIsEditMode As Boolean = False
    'Gajanan [15-April-2020] -- End

    Private ReadOnly mstrModuleName As String = "frmCalibrationApproverList"
    Private ReadOnly mstrModuleName1 As String = "frmCalibrationApproverAddedit"
    Private blnpopupApproverUseraccess As Boolean = False
    Private mintMappingUnkid As Integer
    Private currentId As String = ""
    Private Index As Integer
    Private mblActiveDeactiveApprStatus As Boolean

#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                Call SetMessages()
                'S.SANDEEP |27-JUL-2019| -- END
                Call Language._Object.SaveValue()
                Call SetLanguage()
                ListFillCombo()
                If CBool(Session("AllowToViewCalibrationApproverMaster")) Then
                    FillList(False)
                Else
                    FillList(True)
                End If
                SetVisibility()
            Else

                'Gajanan [15-April-2020] -- Start
                'Enhancement:Worked On Calibration Approver Change
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter")
                mblnIsEditMode = CBool(ViewState("mblnIsEditMode"))
				'Gajanan [15-April-2020] -- End

                mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                mblActiveDeactiveApprStatus = CBool(ViewState("mblActiveDeactiveApprStatus"))
                blnpopupApproverUseraccess = ViewState("blnpopupApproverUseraccess")

                FillList(False)
                If blnpopupApproverUseraccess Then
                    popupApproverUseraccess.Show()
                End If

			    'Gajanan [15-April-2020] -- Start
			    'Enhancement:Worked On Calibration Approver Change
                setPopupvisibility()
			    'Gajanan [15-April-2020] -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupApproverUseraccess") = blnpopupApproverUseraccess
            ViewState("mintMappingUnkid") = mintMappingUnkid
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus

            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
            ViewState("mblnIsEditMode") = mblnIsEditMode
            'Gajanan [15-April-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " List Methods "

    Private Sub AddLinkCol()
        Try

            For Each gRow As GridViewRow In gvApproverList.Rows

                If IsDBNull(gvApproverList.DataKeys(gRow.RowIndex)("IsGrp")) = False AndAlso CBool(gvApproverList.DataKeys(gRow.RowIndex)("IsGrp")) = True Then
                    Me.AddGroup(gRow, gvApproverList.DataKeys(gRow.RowIndex)("Level").ToString(), gvApproverList, False)

                ElseIf IsDBNull(gvApproverList.DataKeys(gRow.RowIndex)("isactive")) = False Then

                    If gRow.Cells(getColumnID_Griview(gvApproverList, "Acive/Inactive Approver", False, False)).FindControl("lnkactive") Is Nothing = False AndAlso _
                       gRow.Cells(getColumnID_Griview(gvApproverList, "Acive/Inactive Approver", False, False)).FindControl("lnkdeactive") Is Nothing = False Then

                        Dim lnkactive As LinkButton = gRow.Cells(getColumnID_Griview(gvApproverList, "Acive/Inactive Approver", False, False)).FindControl("lnkactive")
                        Dim lnkdeactive As LinkButton = gRow.Cells(getColumnID_Griview(gvApproverList, "Acive/Inactive Approver", False, False)).FindControl("lnkdeactive")


                        If gvApproverList.DataKeys(gRow.RowIndex)("isactive") = False Then
                            lnkdeactive.Visible = True
                            lnkactive.Visible = False
                        Else
                            lnkdeactive.Visible = True
                            lnkactive.Visible = False
                        End If




                    End If



                End If




                'If gRow.Cells(mintUpdateProgressIndex).FindControl("lnkUpdateProgress") Is Nothing Then
                '    If CBool(dgvData.DataKeys(gRow.RowIndex)("isfinal")) = True Then
                '        If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                '            Dim lnk As New LinkButton
                '            lnk.ID = "lnkUpdateProgress"
                '            lnk.CommandName = "UpdateProgress"
                '            lnk.Text = ""
                '            lnk.CssClass = "updatedata123"
                '            lnk.Style.Add("color", "red")
                '            lnk.Style.Add("font-size", "20px")
                '            gRow.Cells(mintUpdateProgressIndex).Controls.Add(lnk)
                '        End If
                '    End If
                'End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try

            strSearching &= "AND TAM.iscalibrator = 0 "

            If CInt(drpLevel.SelectedValue) > 0 Then
                strSearching &= "AND TAM.levelunkid = " & CInt(drpLevel.SelectedValue) & " "
            End If

            If CInt(drpApprover.SelectedValue) > 0 Then
                strSearching &= "AND TAM.mappingunkid = " & CInt(drpApprover.SelectedValue) & " "
            End If

            If CInt(drpStatus.SelectedValue) = 1 Then
                strSearching &= "AND TAM.isactive  = 1 "
            ElseIf CInt(drpStatus.SelectedValue) = 2 Then
                strSearching &= "AND TAM.isactive  = 0 "
            End If

            If isblank Or CBool(Session("AllowToViewCalibrationApproverMaster")) = False Then
                strSearching = "AND 1 = 2 "
            End If

            If strSearching.Trim.Length > 0 Then strSearching = strSearching.Substring(3)

            dsApproverList = objApproverMaster.GetList("List", True, strSearching)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objApproverMaster.GetList("List", True, "1 = 2", 0, Nothing, True)
                isblank = True
            End If

            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "Level", DataViewRowState.CurrentRows).ToTable()
                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("Level")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLeaveName = drow("Level").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                dtTable.AcceptChanges()
                gvApproverList.DataSource = dtTable
                gvApproverList.DataBind()
                If isblank = True Then
                    gvApproverList.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToAddCalibrationApproverMaster"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView, Optional ByVal isfromRowbound As Boolean = True)
        Try

            If isfromRowbound = False Then
                rw.Visible = True
                Dim row As GridViewRow = rw
                row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
                Dim cell As TableCell = New TableCell()
                cell.Text = title
                cell.ColumnSpan = gd.Columns.Count
                row.Visible = True
                row.Cells.Add(cell)
                gd.Controls(0).Controls.Add(row)
                gd.Controls(0).Controls.Remove(rw)

            Else
                rw.Visible = False
                Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
                row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
                Dim cell As TableCell = New TableCell()
                cell.Text = title
                cell.ColumnSpan = gd.Columns.Count
                row.Visible = True
                row.Cells.Add(cell)
                gd.Controls(0).Controls.Add(row)
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Private Sub ListFillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With drpStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

            Dim objLevel As New clscalibrate_approverlevel_master
            Dim dsList As DataSet = objLevel.getListForCombo("List", True)
            drpLevel.DataTextField = "name"
            drpLevel.DataValueField = "levelunkid"
            drpLevel.DataSource = dsList.Tables(0).Copy()
            drpLevel.DataBind()

            drpApproverUseraccess_level.DataTextField = "name"
            drpApproverUseraccess_level.DataValueField = "levelunkid"
            drpApproverUseraccess_level.DataSource = dsList.Tables(0).Copy()
            drpApproverUseraccess_level.DataBind()

            Dim objApprover As New clscalibrate_approver_master
            Dim dsApprList As DataSet

            dsApprList = objApprover.GetList("List", True, "", 0, Nothing, True, False)

            drpApprover.DataTextField = "approver"
            drpApprover.DataValueField = "mappingunkid"
            drpApprover.DataSource = dsApprList.Tables("List")
            drpApprover.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub SetValueToPopup()
        Try
            drpApproverUseraccess_level.SelectedValue = objApproverMaster._Levelunkid
            drpApproverUseraccess_user.SelectedValue = objApproverMaster._Mapuserunkid
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            If mintMappingUnkid > 0 Then
                setPopupvisibility()
                FillEmployee()
                FillApproverEmployeeList()
            End If
            'Gajanan [15-April-2020] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

#Region " Button Event "
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            mstrAdvanceFilter = ""
            FillAddEditCombo()
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            objApproverMaster._Mappingunkid = lnkedit.CommandArgument.ToString()
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            mintMappingUnkid = objApproverMaster._Mappingunkid
            mblnIsEditMode = True
            'Gajanan [15-April-2020] -- End
            SetValueToPopup()
            popupApproverUseraccess.Show()
            blnpopupApproverUseraccess = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objApproverMaster._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkdelete.CommandArgument.ToString())

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            If objApproverMaster.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), Me)
                Exit Sub
            End If
            'S.SANDEEP |27-JUL-2019| -- END

            confirmapproverdelete.Show()
            confirmapproverdelete.Title = "Confirmation"
            confirmapproverdelete.Message = Language.getMessage(mstrModuleName1, 5, "Are You Sure You Want To Delete This Approver?")

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkedit_Click:" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkActive).NamingContainer, GridViewRow)
            Dim blnFlag As Boolean = False

            mintMappingUnkid = CInt(lnkActive.CommandArgument)
            mblActiveDeactiveApprStatus = True

            objApproverMaster._Mappingunkid = CInt(lnkActive.CommandArgument.ToString())
            popupconfirmActiveAppr.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError("lnkActive_Click" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkDeActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkDeActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkDeActive).NamingContainer, GridViewRow)

            mintMappingUnkid = CInt(lnkDeActive.CommandArgument)
            mblActiveDeactiveApprStatus = False

            objApproverMaster._Mappingunkid = CInt(lnkDeActive.CommandArgument)

            'If objApproverMaster.isApproverInGrievanceUsed(objApproverMaster._Mappingunkid) Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
            '    Exit Sub
            'End If

            popupconfirmDeactiveAppr.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            FillAddEditCombo()
            blnpopupApproverUseraccess = True
            popupApproverUseraccess.Show()
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            FillEmployee()
            setPopupvisibility()
            'Gajanan [15-April-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupconfirmActiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmActiveAppr.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue(mstrModuleName)
            blnFlag = objApproverMaster.InActiveApprover(mintMappingUnkid, CInt(Session("CompanyUnkId")), True)

            If blnFlag = False And objApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage("popupconfirmActiveAppr_buttonYes_Click :- " & objApproverMaster._Message, Me)
            Else
                FillList(False)
                mintMappingUnkid = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupconfirmActiveAppr_buttonYes_Click :" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupconfirmDeactiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmDeactiveAppr.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue(mstrModuleName)
            blnFlag = objApproverMaster.InActiveApprover(mintMappingUnkid, CInt(Session("CompanyUnkId")), False)
            If blnFlag = False And objApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage("popupconfirmDeactiveAppr_buttonYes_Click :- " & objApproverMaster._Message, Me)
            Else
                FillList(False)
                mintMappingUnkid = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupconfirmDeactiveAppr_buttonYes_Click :" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub DeleteApprovalReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteApprovalReason.buttonDelReasonYes_Click
        Try
            SetAtValue(mstrModuleName)
            objApproverMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproverMaster._Voidreason = DeleteApprovalReason.Reason
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objApproverMaster._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objApproverMaster.Delete(CInt(mintMappingUnkid)) = False Then
                DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
                Exit Sub
            End If
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            mintMappingUnkid = -1
            'Gajanan [15-April-2020] -- End

            FillList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " AddEdit Methods "

#Region " Private Function "

    Dim dsList As DataSet
    Private Sub FillAddEditCombo()
        Try
            Dim objUser As New clsUserAddEdit
            dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CInt(enUserPriviledge.AllowToApproveRejectCalibratedScore).ToString(), CInt(Session("Fin_year")), True)
            drpApproverUseraccess_user.DataSource = dsList.Tables("User")
            drpApproverUseraccess_user.DataTextField = "name"
            drpApproverUseraccess_user.DataValueField = "userunkid"
            drpApproverUseraccess_user.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            If drpApproverUseraccess_level.SelectedValue = 0 Then
                DisplayMessage.DisplayMessage("Approver Level cannot be blank. Approver Level is required information.", Me)
                Return False
            ElseIf drpApproverUseraccess_user.SelectedValue = 0 Then
                DisplayMessage.DisplayMessage("Approver User cannot be blank. Approver User is required information.", Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function

    Private Sub SetValue()
        Try
            If mintMappingUnkid > 0 Then
                objApproverMaster._Mappingunkid = mintMappingUnkid
            End If
            objApproverMaster._Levelunkid = Convert.ToInt32(drpApproverUseraccess_level.SelectedValue)
            objApproverMaster._Mapuserunkid = Convert.ToInt32(drpApproverUseraccess_user.SelectedValue)
            objApproverMaster._Isactive = True

            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            objApproverMaster._VisibilityType = CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.VISIBLE)
            objApproverMaster._IsCalibrator = False
            'Gajanan [15-April-2020] -- End

            Call SetAtValue(mstrModuleName1)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetAtValue(ByVal strFormName As String)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objApproverMaster._AuditUserId = CInt(Session("UserId"))
                objApproverTran._AuditUserid = CInt(Session("UserId"))
            End If
            objApproverMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproverMaster._ClientIP = CStr(Session("IP_ADD"))
            objApproverMaster._HostName = CStr(Session("HOST_NAME"))
            objApproverMaster._FormName = strFormName
            objApproverMaster._IsFromWeb = True
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change

            objApproverTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objApproverTran._ClientIp = CStr(Session("IP_ADD"))
            objApproverTran._HostName = CStr(Session("HOST_NAME"))
            objApproverTran._FormName = strFormName
            objApproverTran._IsFromWeb = True
            'Gajanan [15-April-2020] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Private Sub ResetAddedit()
        Try
            drpApproverUseraccess_level.SelectedIndex = 0
            drpApproverUseraccess_user.SelectedIndex = 0
            popupApproverUseraccess.Hide()
            blnpopupApproverUseraccess = False
            FillList(False)
            mintMappingUnkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Public Sub FillUserAccess(ByVal dt As DataTable)
        Try
            'TvApproverUseraccess.DataSource = dt
            'TvApproverUseraccess.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change


    Private Sub FillEmployee()
        Try

            If drpApproverUseraccess_user.SelectedValue > 0 Then
                Dim objEmployee As New clsEmployee_Master
                Dim dsEmployee As DataSet = Nothing
                Dim strSearch As String = String.Empty
                If strSearch.Length > 0 Then
                    strSearch = strSearch.Substring(3)
                End If


                Dim blnInActiveEmp As Boolean = False
                If (Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId")) <= 0) Then
                    blnInActiveEmp = False
                Else
                    blnInActiveEmp = True
                End If


                If mstrAdvanceFilter.Trim.Length > 0 Then
                    strSearch &= "AND " & mstrAdvanceFilter
                End If


                dsEmployee = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                blnInActiveEmp, _
                                                "Employee", _
                                                False, , , , , , , , , , , , , , , mstrAdvanceFilter, True)

                


                If dsEmployee IsNot Nothing Then
                    dsEmployee.Tables(0).Columns.Add("Select", Type.GetType("System.Boolean"))
                    dsEmployee.Tables(0).Columns("Select").DefaultValue = False
                End If



                Dim dtEmployee As DataTable = Nothing
                Dim StrEmpIds As String = ""
                StrEmpIds = objApproverMaster.GetAssignedEmpCSVLevelWise(Convert.ToInt32(drpApproverUseraccess_level.SelectedValue))
                If StrEmpIds.Trim.Length > 0 Then
                    dtEmployee = New DataView(dsEmployee.Tables("Employee"), "employeeunkid NOT IN (" & StrEmpIds & ")", "", DataViewRowState.CurrentRows).ToTable
                Else
                dtEmployee = dsEmployee.Tables("Employee")
                End If
                dgvEmp.DataSource = dtEmployee
                dgvEmp.DataBind()

            Else
                dgvEmp.DataSource = Nothing
                dgvEmp.DataBind()

                dgvApproverEmp.DataSource = Nothing
                dgvApproverEmp.DataBind()

            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgvEmp.PageIndex = 0
                dgvEmp.DataBind()

            Else
                Throw ex
                DisplayMessage.DisplayError(ex.Message, Me)
            End If
        End Try
    End Sub


    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Private Sub setPopupvisibility()
        Try
        If mintMappingUnkid > 0 Then
            drpApproverUseraccess_level.Enabled = False
            drpApproverUseraccess_user.Enabled = False
        Else
            drpApproverUseraccess_level.Enabled = True
            drpApproverUseraccess_user.Enabled = True
        End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub
    'Gajanan [15-April-2020] -- End

    Private Sub FillApproverEmployeeList()
        Try
            If mintMappingUnkid Then
                Dim dsemployee As DataTable = objApproverTran.GetApproverTran(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, mintMappingUnkid, Nothing)
                dgvApproverEmp.DataSource = dsemployee
                dgvApproverEmp.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub
    'Gajanan [15-April-2020] -- End
#End Region

#Region " Dropdown Event "

    Protected Sub drpApproverUseraccess_user_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApproverUseraccess_user.SelectedIndexChanged
        'Dim objUser As New clsUserAddEdit
        Try
            '    Dim dtTable As DataTable = Nothing
            '    If CInt(drpApproverUseraccess_user.SelectedValue) > 0 Then
            '        dtTable = objUser.GetUserAccessList(Convert.ToString(Session("UserAccessModeSetting")), CInt(drpApproverUseraccess_user.SelectedValue), Convert.ToInt32(Session("CompanyUnkId")))
            '        dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            '    End If
            '    FillUserAccess(dtTable)
            '    dtTable = Nothing
            FillEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'objUser = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event "

    Protected Sub btnApproverUseraccessClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessClose.Click
        Try
            blnpopupApproverUseraccess = False
            popupApproverUseraccess.Hide()
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Protected Sub btnApproverUseraccessSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessSave.Click
    '    Dim blnFlag As Boolean = False
    '    Try
    '        If Validation() = False Then Exit Sub

    '        SetValue()

    '        blnFlag = objApproverMaster.Insert()

    '        If blnFlag = False And objApproverMaster._Message <> "" Then
    '            DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
    '        End If

    '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Approver Saved Successfully"), Me)
    '        ResetAddedit()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex.Message, Me)
    '    End Try

    'End Sub

    Protected Sub confirmapproverdelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmapproverdelete.buttonYes_Click
        Try
            DeleteApprovalReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpLevel.SelectedIndex = 0
            drpApprover.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            Call FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change

    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            'If mdtTran.Rows.Count = 0 Or count = 0 Or GvSelectedEmployee.Rows.Count <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), Me)
            '    Exit Sub
            'End If

            Dim dtemployee As DataTable = objApproverTran._DataList
            For Each row As GridViewRow In dgvEmp.Rows
                Dim chk As CheckBox = CType(row.FindControl("ChkSelectedEmp"), CheckBox)
                Dim hfemployee As HiddenField = CType(row.FindControl("hfemployeeunkid"), HiddenField)

                If chk.Checked Then
                    Dim drow As DataRow = dtemployee.NewRow
                    drow("employeeunkid") = hfemployee.Value
                    dtemployee.Rows.Add(drow)
                    chk.Checked = False
                End If
            Next

            SetValue()


            Dim StrEmpIds As String = ""
            StrEmpIds = objApproverMaster.GetAssignedEmpCSVLevelWise(Convert.ToInt32(drpApproverUseraccess_level.SelectedValue))
            If StrEmpIds.Trim.Length > 0 AndAlso dtemployee.Rows.Count > 0 Then
                Dim dTmp() As DataRow = Nothing
                dTmp = dtemployee.Select("employeeunkid NOT IN(" & StrEmpIds & ")")
                If dTmp.Length > 0 Then
                    dtemployee = dTmp.CopyToDataTable()
                ElseIf dTmp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage("clscalibrate_approver_master", 5, "Selected employee(s) are already mapped on same level with different approver .Please assign new employee(s) to map with this level."), Me)
                    Exit Sub
                End If
            End If

            If mintMappingUnkid > 0 Then

                objApproverTran._DataList = dtemployee
                objApproverTran._Mappingunkid = mintMappingUnkid
                objApproverTran.Insert_CalibarteApproverData(Nothing)
            Else
                If objApproverMaster.isExist(objApproverMaster._Mapuserunkid, False) Then
                    If mblnIsEditMode Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "This approver is already map.please select new user to map."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "This approver is alredy exist to update data please edit from list."), Me)
                    End If
                    Exit Sub
                End If


                objApproverMaster.Insert(dtemployee, False)
                mintMappingUnkid = objApproverMaster._Mappingunkid
            End If

            If mblnIsEditMode Then
                popupApproverUseraccess.Hide()
            Else
                mintMappingUnkid = -1
                drpApproverUseraccess_user.SelectedValue = 0
                Call drpApproverUseraccess_user_SelectedIndexChanged(New Object(), New EventArgs)
            End If

            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 6, "Employee assigned successfully to selected approver."), Me)



        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnAddeditDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddeditDelete.Click
        Dim blnFlag As Boolean = False
        Try

            'If mdtTran.Rows.Count = 0 Or count = 0 Or GvSelectedEmployee.Rows.Count <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), Me)
            '    Exit Sub
            'End If
            Dim mdicEmployee As New Dictionary(Of Integer, Integer)


            For Each row As GridViewRow In dgvApproverEmp.Rows
                Dim chk As CheckBox = CType(row.FindControl("ChkSelectedEmp"), CheckBox)

                Dim empid As Integer = dgvApproverEmp.DataKeys(row.RowIndex)("employeeunkid").ToString()
                Dim mappingtranunkid As Integer = dgvApproverEmp.DataKeys(row.RowIndex)("mappingtranunkid").ToString()


                If chk.Checked Then
                    mdicEmployee.Add(mappingtranunkid, empid)
                End If
            Next

            SetValue()

            objApproverTran._Mappingunkid = mintMappingUnkid
            objApproverTran.Delete_CalibarteApproverData(mdicEmployee, Nothing)
            FillApproverEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try

    End Sub

    Protected Sub AdvanceFilter1_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvanceFilter1.buttonApply_Click
        Try
            mstrAdvanceFilter = AdvanceFilter1._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Gajanan [15-April-2020] -- End


#End Region

#Region " Gridview Event "

    Protected Sub gvApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvApproverList.DataKeys(e.Row.RowIndex)("mappingunkid").ToString) > 0 Then
                    oldid = dt.Rows(e.Row.RowIndex)("Level").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("Level").ToString(), gvApproverList)
                        currentId = oldid
                    Else
                        Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("lnkactive"), LinkButton)
                        Dim lnkDeactive As LinkButton = TryCast(e.Row.FindControl("lnkDeactive"), LinkButton)
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                        If CBool(Session("AllowToEditCalibrationApproverMaster")) Then
                            lnkedit.Visible = True
                        Else
                            lnkedit.Visible = False
                        End If

                        If CBool(Session("AllowToDeleteCalibrationApproverMaster")) Then
                            lnkdelete.Visible = True
                        Else
                            lnkdelete.Visible = False
                        End If

                        If CBool(Session("AllowToActivateCalibrationApproverMaster")) Then
                            lnkactive.Visible = True
                        Else
                            lnkactive.Visible = False
                        End If

                        If CBool(Session("AllowToDeactivateCalibrationApproverMaster")) Then
                            lnkDeactive.Visible = True
                        Else
                            lnkDeactive.Visible = False
                        End If

                        If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                            If dt.Rows(e.Row.RowIndex)("isactive").ToString() = True Then
                                lnkactive.Visible = False
                                lnkDeactive.Visible = True
                            Else
                                lnkactive.Visible = True
                                lnkDeactive.Visible = False
                            End If
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub
#End Region

#Region " Link Event(s) "

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            AdvanceFilter1._Hr_EmployeeTable_Alias = "hremployee_master"
            AdvanceFilter1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call FillEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            mstrAdvanceFilter = ""
            Call FillEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region
#End Region

#Region " Language "

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            If Me.Title.Trim.Length > 0 Then
                Language._Object.setCaption(mstrModuleName, Me.Title)
            End If

            Language._Object.setCaption(mstrModuleName, Me.lblCaption.Text)

            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Language._Object.setCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Language._Object.setCaption(Me.btnNew.ID, Me.btnNew.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)

            Language._Object.setCaption(gvApproverList.Columns(2).FooterText, gvApproverList.Columns(2).HeaderText)
            Language._Object.setCaption(gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)

            Language.setLanguage(mstrModuleName1)
            Language._Object.setCaption(mstrModuleName1, Me.lblHeader.Text)
            Language._Object.setCaption(Me.lblHeader.ID, Me.lblHeader.Text)
            Language._Object.setCaption(Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Language._Object.setCaption(Me.lblApproverUseraccess_level.ID, Me.lblApproverUseraccess_level.Text)
            Language._Object.setCaption(Me.lblApproverUseraccess_user.ID, Me.lblApproverUseraccess_user.Text)

            Language._Object.setCaption(Me.btnApproverUseraccessClose.ID, Me.btnApproverUseraccessClose.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            gvApproverList.Columns(2).HeaderText = Language._Object.getCaption(gvApproverList.Columns(2).FooterText, gvApproverList.Columns(2).HeaderText)

            Language.setLanguage(mstrModuleName1)
            Me.lblHeader.Text = Language._Object.getCaption(Me.lblHeader.ID, Me.lblHeader.Text).Replace("&", "")
            Me.lblApproverInfo.Text = Language._Object.getCaption(Me.lblApproverInfo.ID, Me.lblApproverInfo.Text).Replace("&", "")
            Me.lblApproverUseraccess_level.Text = Language._Object.getCaption(Me.lblApproverUseraccess_level.ID, Me.lblApproverUseraccess_level.Text).Replace("&", "")
            Me.lblApproverUseraccess_user.Text = Language._Object.getCaption(Me.lblApproverUseraccess_user.ID, Me.lblApproverUseraccess_user.Text).Replace("&", "")

            Me.btnApproverUseraccessClose.Text = Language._Object.getCaption(Me.btnApproverUseraccessClose.ID, Me.btnApproverUseraccessClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 6, "Approver Saved Successfully")
            Language.setMessage(mstrModuleName1, 5, "Are You Sure You Want To Delete This Approver?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
