﻿<%@ Page Title="Approve/Reject Performance Planning" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgPerformance_Planning.aspx.vb" Inherits="wPgPerformance_Planning" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cofirm" TagPrefix="cnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);
        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });
        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
            SetGeidScrolls();
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    
    </script>

    <%--'S.SANDEEP |04-DEC-2019| -- START--%>
    <%--'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB--%>
    <script type="text/javascript">
        function Print() {
            var printWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
            printWin.document.write(document.getElementById("<%=divhtml.ClientID %>").innerHTML);
            printWin.document.close();
            printWin.focus();
            printWin.print();
            printWin.close();
        }
    </script>
    <%--'S.SANDEEP |04-DEC-2019| -- END--%>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Approve/Reject Performance Planning"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default" style=": none">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%;">
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%;">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                <%--'S.SANDEEP |12-FEB-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Performance Assessment Changes}--%>
                                                <%--AutoPostBack="true"--%>
                                                <%--'S.SANDEEP |12-FEB-2019| -- END--%>
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblPeriod" Style="margin-left: 10px" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 30%;">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true"></asp:DropDownList>
                                                <%--'S.SANDEEP |12-FEB-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Performance Assessment Changes}--%>
                                                <%--AutoPostBack="true"--%>
                                                <%--'S.SANDEEP |12-FEB-2019| -- END--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left;">
                                            <asp:Label ID="objlblCurrentStatus" runat="server" Font-Bold="true" Font-Size="Small"
                                                ForeColor="Red"></asp:Label>
                                        </div>
                                        <asp:Label ID="objlblTotalWeight" runat="server" Font-Bold="true" Font-Size="Small"></asp:Label>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div17" class="panel-default" style="padding: 0px">
                                <div id="Div19" class="panel-body-default">
                                    <asp:MultiView ID="mvPlanning" runat="server" ActiveViewIndex="0">
                                        <asp:View ID="BSC" runat="server">
                                            <asp:Panel ID="pnlBSC" runat="server" Width="100%" Height="100%">
                                                <div id="Div11" class="panel-default">
                                                    <div id="Div12" class="panel-heading-default">
                                                        <div style="text-align: center">
                                                            <asp:Label ID="lblBSC" runat="server" Text="Employee Goals" Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div13" class="panel-body-default">
                                                        <table style="width: 100%;">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="pnl_dgvPData" runat="server" Style="width: 100%; height: 400px; overflow: auto;
                                                                        max-width: 1090px" CssClass="gridscroll">
                                                                        <asp:GridView ID="dgvPData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                            Width="99.5%">
                                                                            <Columns>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                        <asp:View ID="Competency" runat="server">
                                            <asp:Panel ID="pnlCmpt" runat="server" Width="100%" Height="100%">
                                                <div id="Div14" class="panel-default">
                                                    <div id="Div15" class="panel-heading-default">
                                                        <div style="text-align: center">
                                                            <asp:Label ID="lblCmpt" runat="server" Text="Employee Competencies" Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div16" class="panel-body-default">
                                                        <table id="Planning_Cmpt" style="width: 100%;">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="pnl_dgvCData" runat="server" Style="width: 100%; height: 400px; overflow: auto;"
                                                                        CssClass="gridscroll">
                                                                        <asp:DataGrid ID="dgvCData" runat="server" Style="margin: auto" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="99%" AutoGenerateColumns="false">
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="ccategory" HeaderText="Competence Category" FooterText="objcolhCGroup"
                                                                                    Visible="false" />
                                                                                <asp:BoundColumn DataField="competencies" HeaderText="Competencies" FooterText="" />
                                                                                <asp:BoundColumn DataField="assigned_weight" HeaderStyle-HorizontalAlign="Right"
                                                                                    ItemStyle-HorizontalAlign="Right" HeaderText="Weight" FooterText="" />
                                                                                <asp:BoundColumn DataField="isfinal" Visible="false" />
                                                                                <asp:BoundColumn DataField="opstatusid" HeaderText="" FooterText="" Visible="false" />
                                                                                <asp:BoundColumn DataField="masterunkid" Visible="false" />
                                                                                <asp:BoundColumn DataField="competenciesunkid" Visible="false" />
                                                                                <asp:BoundColumn DataField="isGrp" Visible="false" />
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderText="Info." ItemStyle-Width="90px">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                                            Font-Underline="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                        <asp:View ID="CustomItems" runat="server">
                                            <asp:Panel ID="Panel1" runat="server" Width="100%" Height="100%">
                                                <div id="Div21" class="panel-default">
                                                    <div id="Div22" class="panel-heading-default">
                                                        <div style="text-align: center">
                                                            <asp:Label ID="lblCustomItemHeader" runat="server" Text="Custom Items" Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div23" class="panel-body-default">
                                                        <table id="Table1" style="width: 100%;">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="Panel2" runat="server" Style="width: 100%; height: 400px; overflow: auto;"
                                                                        CssClass="gridscroll">
                                                                        <asp:GridView ID="dgvItems" runat="server" Style="margin: auto" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="99%" AutoGenerateColumns="false">
                                                                            <Columns>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div id="Div24" class="panel-heading-default">
                                                        <div style="text-align: left">
                                                            <asp:LinkButton ID="btnBack" runat="server" Text="Back" Style="text-decoration: none;
                                                                font-weight: bold; font-size: 13px;" />
                                                            <asp:LinkButton ID="btnForward" runat="server" Text="Next" Style="text-decoration: none;
                                                                font-weight: bold; font-size: 13px;" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                    </asp:MultiView>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <div style="float: left">
                                            <asp:Panel ID="btnNavigation" runat="server">
                                                <asp:Button ID="btnPrevious" runat="server" Text="Previous" CssClass="btndefault" />
                                                <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btndefault" />
                                                <asp:Button ID="btnSubmitforApproval" runat="server" Text="Submit for Approval" CssClass="btndefault" />
                                                <asp:Button ID="btnApproveSubmitted" runat="server" Text="Approve/Reject Planning"
                                                    CssClass="btndefault" />
                                                <%--'S.SANDEEP |12-MAR-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}--%>
                                                <%--<asp:Button ID="btnUnlockFinalSave" runat="server" Text="Unlock Final Save" CssClass="btndefault" />--%>
                                                <asp:Button ID="btnUnlockFinalSave" runat="server" Text="Unlock Final Approved" CssClass="btndefault" />
                                                <%--'S.SANDEEP |12-MAR-2019| -- END--%>
                                                <asp:Button ID="btnCommitGoals" runat="server" Text="Commit Performance Plans" CssClass="btndefault" />
                                                <asp:Button ID="btnOpenGoals" runat="server" Text="Open Performance Plans" CssClass="btndefault" />
                                                <asp:Button ID="btnViewTemplate" runat="server" Text="View/Print Template" CssClass="btnDefault" />
                                                <%--'S.SANDEEP |04-DEC-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB--%>
                                                <asp:Button ID="btnViewPlanningReport" runat="server" Text="View/Print Planning"
                                                    CssClass="btndefault" />
                                                <%--'S.SANDEEP |04-DEC-2019| -- END--%>
                                                <%--'S.SANDEEP |22-MAR-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : ISSUE ON PA--%>
                                                <%--Visible="false" -- ADDED--%>
                                                <%--'S.SANDEEP |22-MAR-2019| -- END--%>
                                            </asp:Panel>
                                        </div>
                                        <div>
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_SubmitApproval" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnSubmitNo" PopupControlID="pnl_SubmitApproval" TargetControlID="hdf_SubmitApproval">
                    </cc1:ModalPopupExtender>
                    <cc1:ModalPopupExtender ID="popup_ApproverRejYesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnAppRejNo" PopupControlID="pnl_ApproverRejYesNo" TargetControlID="hdf_ApproverRejYesNo">
                    </cc1:ModalPopupExtender>
                    <cc1:ModalPopupExtender ID="popup_ApprovrReject" runat="server" TargetControlID="hdf_ApprovrReject"
                        CancelControlID="hdf_ApprovrReject" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                        PopupControlID="pnl_ApprovrReject" Drag="True" BehaviorID="popupApprovr">
                    </cc1:ModalPopupExtender>
                    <cc1:ModalPopupExtender ID="popup_ApprovrFinalYesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnFinalNo" PopupControlID="pnl_ApprovrFinalYesNo" TargetControlID="hdf_ApprvoerFinalYesNo">
                    </cc1:ModalPopupExtender>
                    <cc1:ModalPopupExtender ID="popup_UnclokFinalSave" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnUnlockNo" PopupControlID="pnl_UnlocakFinalSave" TargetControlID="hdf_btnUnlockYesNO">
                    </cc1:ModalPopupExtender>
                    <%--'S.SANDEEP |09-JUL-2019| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : PA CHANGES--%>
                    <cc1:ModalPopupExtender ID="popup_VoidUpdateProgress" runat="server" BackgroundCssClass="ModalPopupBG"
                        PopupControlID="pnl_VoidUpdateProgress" TargetControlID="hdf_btnVUPYesNO"> <%--CancelControlID="btnUnlockNo"--%>
                    </cc1:ModalPopupExtender>
                    <%--'S.SANDEEP |09-JUL-2019| -- END--%>
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_SubmitApproval" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 400px" DefaultButton="btnSubmitYes">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblSubmitTitle" runat="server" Text="Aruti"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div1" class="panel-default">
                                                <%--<div id="Div2" class="panel-heading-default">
                                                                <div style="float: left;">
                                                                    <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                                                </div>
                                                            </div>--%>
                                                <div id="Div3" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblSubmitMessage" runat="server" Text="Message :" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnSubmitYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnSubmitNo" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_SubmitApproval" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_ApproverRejYesNo" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 400px" DefaultButton="btnAppRejYes">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblAppRejTitle" runat="server" Text="Aruti"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div4" class="panel-default">
                                                <%-- <div id="Div5" class="panel-heading-default">
                                                                <div style="float: left;">
                                                                    <asp:Label ID="Label2" runat="server" Text="Title"></asp:Label>
                                                                </div>
                                                            </div>--%>
                                                <div id="Div6" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblAppRejMessage" runat="server" Text="Message :"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnAppRejYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnAppRejNo" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_ApproverRejYesNo" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_ApprovrReject" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 600px">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblApproveRejectPlanningHeader" runat="server" Text="Approve/Reject Performance Planning"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div2" class="panel-default">
                                                <%-- <div id="Div5" class="panel-heading-default">
                                                                <div style="float: left;">
                                                                    <asp:Label ID="Label2" runat="server" Text="Title"></asp:Label>
                                                                </div>
                                                            </div>--%>
                                                <div id="Div5" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%" valign="top">
                                                                <asp:Label ID="lblComment" runat="server" Text="Comment"></asp:Label>
                                                            </td>
                                                            <td style="width: 80%">
                                                                <asp:TextBox ID="txtComments" TextMode="MultiLine" Rows="4" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:HiddenField ID="hdf_ApprovrReject" runat="server" />
                                                        <asp:Button ID="btnApRejFinalSave" runat="server" Text="Approve" CssClass="btndefault" />
                                                        <asp:Button ID="btnOpen_Changes" runat="server" Text="Reject" CssClass="btndefault" />
                                                        <asp:Button ID="btnApRejClose" runat="server" Text="Close" CssClass="btndefault" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_ApprovrFinalYesNo" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 400px" DefaultButton="btnAppRejYes">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblFinalTitle" runat="server" Text="Title"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div7" class="panel-default">
                                                <%-- <div id="Div5" class="panel-heading-default">
                                                                <div style="float: left;">
                                                                    <asp:Label ID="Label2" runat="server" Text="Title"></asp:Label>
                                                                </div>
                                                            </div>--%>
                                                <div id="Div8" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblFinalMessage" runat="server" Text="Message :" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnFinalYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnOpenYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnFinalNo" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_ApprvoerFinalYesNo" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_UnlocakFinalSave" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 400px" DefaultButton="btnUnlockYes">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblUnlockTitel" runat="server" Text="Title"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div9" class="panel-default">
                                                <%--<div id="Div5" class="panel-heading-default" style="height:50px">
                                                    <div style="float: left;">
                                                        
                                                    </div>
                                                </div>--%>
                                                <div id="Div10" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblUnlockMessage" runat="server" Text="Message :" />
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:TextBox ID="txtUnlockReason" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnUnlockYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnUnlockNo" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_btnUnlockYesNO" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <%--'S.SANDEEP |09-JUL-2019| -- START--%>
                        <%--'ISSUE/ENHANCEMENT : PA CHANGES--%>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_VoidUpdateProgress" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 400px" DefaultButton="btnVUPYes">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblVoidUpdateProgress" runat="server" Text="Title"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div27" class="panel-default">
                                                <div id="Div28" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblVoidUpdateProgressMsg" runat="server" Text="Message :" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnVUPYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnVUPNo" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_btnVUPYesNO" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <%--'S.SANDEEP |09-JUL-2019| -- END--%>
                        <tr>
                            <td>
                                <cc1:ModalPopupExtender ID="popupCnf" runat="server" BackgroundCssClass="ModalPopupBG"
                                    CancelControlID="" PopupControlID="pnl_popupcnf" TargetControlID="hdf_popupcnf">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnl_popupcnf" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 400px; z-index: 999999" DefaultButton="btncnfYes">
                                    <div class="panel-primary" style="margin: 0px">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblcnfTitle" runat="server" Text="Aruti"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div id="Div18" class="panel-default">
                                                <div id="Div20" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblcnfTMessage" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btncnfYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btncnfNo" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_popupcnf" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="cnfCommitGoals" style="width: 100%">
                            <td style="width: 100%">
                                <cnf:Cofirm ID="cnfGoalsCommit" runat="server" />
                            </td>
                        </tr>
                        <tr id="Tr1" style="width: 100%">
                            <td style="width: 100%">
                                <cnf:Cofirm ID="cnfGoalsOpen" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <cc1:ModalPopupExtender ID="popup_ComInfo" runat="server" BackgroundCssClass="ModalPopupBG2"
                        CancelControlID="btnSClose" PopupControlID="pnl_CompInfo" TargetControlID="hdnfieldDelReason">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_CompInfo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px; z-index: 100002!important;">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div25" class="panel-default">
                                    <div id="Div26" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Label ID="lblTitle" runat="server" Text="Description"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtData" runat="server" TextMode="MultiLine" Width="97%" Height="100px"
                                                        Style="margin-bottom: 5px;" ReadOnly="true" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSClose" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                                CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--'S.SANDEEP |04-DEC-2019| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB--%>
                    <cc1:ModalPopupExtender ID="popupEmpAssessForm" runat="server" CancelControlID="btnEmpReportingClose"
                        PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2" PopupDragHandleControlID="pnlEmpReporting">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="newpopup" Style="width: 90%;
                        height: 588px" DefaultButton="btnEmpReportingClose" ScrollBars="Auto">
                        <table>
                            <tr>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="btn-default">
                                        <asp:Button ID="btnPlanningPring" runat="server" Text="Print" CssClass="btnDefault"
                                            OnClientClick="javascript:Print()" />
                                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <%--'S.SANDEEP |04-DEC-2019| -- END--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
