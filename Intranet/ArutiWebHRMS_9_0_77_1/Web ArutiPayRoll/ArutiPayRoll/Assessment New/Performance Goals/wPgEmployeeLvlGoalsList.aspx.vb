﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class wPgEmployeeLvlGoalsList
    Inherits Basepage

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmEmpFieldsList"
    Private ReadOnly mstrModuleName_Emp1 As String = "objfrmAddEditEmpField1"
    Private ReadOnly mstrModuleName_Emp2 As String = "objfrmAddEditEmpField2"
    Private ReadOnly mstrModuleName_Emp3 As String = "objfrmAddEditEmpField3"
    Private ReadOnly mstrModuleName_Emp4 As String = "objfrmAddEditEmpField4"
    Private ReadOnly mstrModuleName_Emp5 As String = "objfrmAddEditEmpField5"
    Private ReadOnly mstrModuleName_AppRej As String = "frmApproveRejectPlanning"

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private ReadOnly mstrModuleName_AppRejGoals As String = "frmAppRejGoalAccomplishmentList"
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 136
    Private ReadOnly mstrModuleName_upd_percent As String = "frmUpdateFieldValue"
    'S.SANDEEP [29-NOV-2017] -- END


    Private objEmpField1 As New clsassess_empfield1_master
    Private objEmpField2 As New clsassess_empfield2_master
    Private objEmpField3 As New clsassess_empfield3_master
    Private objEmpField4 As New clsassess_empfield4_master
    Private objEmpField5 As New clsassess_empfield5_master
    Private DisplayMessage As New CommonCodes
    Private mblnpopupShow_EmpField1 As Boolean = False
    Private mblnpopupShow_EmpField2 As Boolean = False
    Private mblnpopupShow_EmpField3 As Boolean = False
    Private mblnpopupShow_EmpField4 As Boolean = False
    Private mblnpopupShow_EmpField5 As Boolean = False
    'Private mblnpopupShow_AproverReject As Boolean = False

    'SHANI (18 JUN 2015) -- Start
    'Private objEUpdateProgress As New clsassess_empupdate_tran
    'Private mintUpdateProgressIndex As Integer = -1
    'Private mintFieldUnkid As Integer = 0
    'Private mintLinkedFld As Integer = 0
    'Private mintFieldTypeId As Integer = 0
    'Private mdtPeriodStartDate As Date = Nothing
    'Private mstrDefinedGoal As String = ""
    'Private mintTableFieldTranUnkId As Integer = 0
    'Private mblnUpdateProgress As Boolean = False
    'Private mintEmpUpdateTranunkid As Integer = 0
    'SHANI (18 JUN 2015) -- End


    'S.SANDEEP [05 DEC 2015] -- START
    Private mintAssessorMasterId As Integer = 0
    Private mintAssessorEmployeeId As Integer = 0
    'S.SANDEEP [05 DEC 2015] -- END

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Private mblnpopup_Accomplishment As Boolean = False
    'Private mdtAccomplishmentData As DataTable = Nothing
    Private objCONN As SqlConnection
    'Private mintEmpUnkId As Integer = -1
    'Private mintPeriodUnkid As Integer = -1
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 136
    'Dim mdtAttachement As DataTable = Nothing
    'Dim mblnAttachmentPopup As Boolean = False
    'Dim mintDeleteIndex As Integer = 0
    'S.SANDEEP [29-NOV-2017] -- END

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    'Dim mintGoalTypeId As Integer = 0
    'Dim mdblGoalValue As Double = 0
    'S.SANDEEP [01-OCT-2018] -- END

    'S.SANDEEP |08-JAN-2019| -- START
    'Private mblnpopup_UnlockEmp As Boolean = False
    'Private objUnlock As New clsassess_plan_eval_lockunlock
    'Private ReadOnly mstrModuleName_unlockemployee As String = "frmUnlockEmployee"
    'Private mdtUnlockEmployeePrdEndDate As Date = Nothing
    'Private mdtUnlockEmployee As DataTable = Nothing
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |18-JAN-2019| -- START
    Private strPerspectiveColName As String = String.Empty
    Private mintPerspectiveColIndex As Integer = -1
    Private strGoalValueColName As String = String.Empty
    Private mintGoalValueColIndex As Integer = -1
    'S.SANDEEP |18-JAN-2019| -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mstrGroupName As String = String.Empty
    Private mintUoMColIndex As Integer = -1
    Private mstrUoMColName As String = String.Empty
    'S.SANDEEP |12-FEB-2019| -- END

'S.SANDEEP |25-FEB-2019| -- START
'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    'Private mdblPercentage As Double = 0
    'Private mdtGoalAccomplishment As DataTable = Nothing
'S.SANDEEP |25-FEB-2019| -- END
#End Region


#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0 AndAlso Request.QueryString.AllKeys(0).Contains("uploadimage") = False) AndAlso IsPostBack = False Then
            '    'Sohail (02 Apr 2019) -- Start
            '    'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
            '    If Request.QueryString.Count <= 0 Then Exit Sub
            '    'Sohail (02 Apr 2019) -- End
            '    objCONN = Nothing
            '    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
            '        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
            '        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
            '        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
            '        objCONN = New SqlConnection
            '        objCONN.ConnectionString = constr
            '        objCONN.Open()
            '        HttpContext.Current.Session("gConn") = objCONN
            '    End If
            '    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
            '    If arr.Length = 4 Then
            '        Try
            '            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
            '                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
            '                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
            '            Else
            '                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
            '                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
            '            End If

            '        Catch ex As Exception
            '            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
            '            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
            '        End Try
                    'Blank_ModuleName()
                    'clsCommonATLog._WebFormName = "frmAppRejGoalAccomplishmentList"
                    'StrModuleName2 = "mnuAssessment"
                    'StrModuleName3 = "mnuSetups"
                    'clsCommonATLog._WebClientIP = Session("IP_ADD")
                    'clsCommonATLog._WebHostName = Session("HOST_NAME")

            '        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
            '        HttpContext.Current.Session("UserId") = CInt(arr(1))
            '        mintEmpUnkId = CInt(arr(2))
            '        mintPeriodUnkid = CInt(arr(3))
            '        'Shani(24-JAN-2017) -- Start
            '        'Enhancement : 
            '        'Dim intUpdateTranUnkid As Integer = CInt(arr(4))
            '        'Shani(24-JAN-2017) -- End

            '        Dim strError As String = ""
            '        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If

            '        HttpContext.Current.Session("mdbname") = Session("Database_Name")
            '        gobjConfigOptions = New clsConfigOptions
            '        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
            '        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            '        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            '        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

            '        ArtLic._Object = New ArutiLic(False)
            '        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
            '            Dim objGroupMaster As New clsGroup_Master
            '            objGroupMaster._Groupunkid = 1
            '            ArtLic._Object.HotelName = objGroupMaster._Groupname
            '        End If

            '        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
            '            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '            Exit Sub
            '        End If

            '        If ConfigParameter._Object._IsArutiDemo Then
            '            If ConfigParameter._Object._IsExpire Then
            '                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
            '                Exit Try
            '            Else
            '                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
            '                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
            '                    Exit Try
            '                End If
            '            End If
            '        End If

            '        Dim clsConfig As New clsConfigOptions
            '        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
            '        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
            '            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
            '        Else
            '            Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
            '        End If

            '        'Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
            '        'Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
            '        'Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
            '        'Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
            '        'Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
            '        'Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
            '        'Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
            '        'Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
            '        'Session("CascadingTypeId") = clsConfig._CascadingTypeId
            '        'Session("ScoringOptionId") = clsConfig._ScoringOptionId
            '        'Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
            '        'Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
            '        'Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
            '        'Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
            '        Session("DateFormat") = clsConfig._CompanyDateFormat
            '        Session("DateSeparator") = clsConfig._CompanyDateSeparator
            '        Session("GoalsAccomplishedRequiresApproval") = clsConfig._GoalsAccomplishedRequiresApproval

            '        'S.SANDEEP [29-NOV-2017] -- START
            '        'ISSUE/ENHANCEMENT : REF-ID # (38,41)
            '        Session("Ntf_FinalAcknowledgementUserIds") = clsConfig._Ntf_FinalAcknowledgementUserIds
            '        Session("Ntf_GoalsUnlockUserIds") = clsConfig._Ntf_GoalsUnlockUserIds
            '        'S.SANDEEP [29-NOV-2017] -- END


            '        SetDateFormat()

            '        Dim objUser As New clsUserAddEdit
            '        objUser._Userunkid = CInt(Session("UserId"))
            '        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
            '        Call GetDatabaseVersion()
            '        Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
            '        HttpContext.Current.Session("clsuser") = clsuser
            '        HttpContext.Current.Session("UserName") = clsuser.UserName
            '        HttpContext.Current.Session("Firstname") = clsuser.Firstname
            '        HttpContext.Current.Session("Surname") = clsuser.Surname
            '        HttpContext.Current.Session("MemberName") = clsuser.MemberName
            '        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
            '        HttpContext.Current.Session("UserId") = clsuser.UserID
            '        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
            '        HttpContext.Current.Session("Password") = clsuser.password
            '        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
            '        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

            '        strError = ""
            '        If SetUserSessions(strError) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If

            '        strError = ""
            '        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If
            '        Dim objUserPrivilege As New clsUserPrivilege
            '        objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
            '        'Session("AllowtoAddAssessorEvaluation") = objUserPrivilege._AllowtoAddAssessorEvaluation



            '        'Shani(24-JAN-2017) -- Start
            '        'Enhancement : 
            '        'Dim objEmpUpdate As New clsassess_empupdate_tran
            '        'objEmpUpdate._Empupdatetranunkid = intUpdateTranUnkid

            '        'If objEmpUpdate._GoalAccomplishmentStatusid <> clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending Then
            '        '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 14, "Sorry, Goal detail is already updated with the selected link."), Me.Page, Session("rootpath") & "Index.aspx")
            '        '    Exit Sub
            '        'End If
            '        Dim dsList As DataSet = objEmpField1.GetAccomplishemtn_DisplayList(mintEmpUnkId, mintPeriodUnkid, clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending, , "List")
            '        If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then
                    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 14, "Sorry, Goal detail is already updated with the selected link."), Me.Page, Session("rootpath") & "Index.aspx")
                    '    Exit Sub
                    'End If
            '        'Shani(24-JAN-2017) -- End
            '        'mblnpopup_Accomplishment = True
            '    End If
            'End If
            If IsPostBack = False Then
                Call SetFormViewSetting()
                Call FillCombo()
                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                Call SetVisibility()
                'S.SANDEEP [28 MAY 2015] -- END
                Me.ViewState.Add("iOwnerRefId", 0)
                Me.ViewState.Add("mintLinkedFieldId", 0)
                'Me.ViewState.Add("mdtFinal", Nothing)
                Me.ViewState.Add("AddRowIndex", 0)
                Me.ViewState.Add("EditRowIndex", 0)
                Me.ViewState.Add("DeleteRowIndex", 0)
                Me.ViewState.Add("AddFieldUnkid", "")
                Me.ViewState.Add("DeleteScreenId", "")
                Me.ViewState.Add("DeleteColumnName", "")

                'S.SANDEEP [04 JUN 2015] -- START

                'SHANI (06 JUN 2015) -- Start
                'Call BtnSearch_Click(New Object, New EventArgs)
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then

                    'Shani(25-JAN-2017) -- Start
                    ' Getting message in defualt period not selected 
                    'Call BtnSearch_Click(New Object, New EventArgs)
                    If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                    Call BtnSearch_Click(New Object, New EventArgs)
                    End If
                    'Shani(25-JAN-2017) -- End


                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    'Shani(24-JAN-2017) -- Start
                    'Enhancement : 
                    'cmnuOperation.Visible = False

                    'S.SANDEEP |05-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'mnuAppRejGoalAccomplished.Visible = False
                    'S.SANDEEP |05-MAR-2019| -- END

                    'Shani(24-JAN-2017) -- End
                    'Shani (26-Sep-2016) -- End

                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    cmnuOperation.Visible = False
                    'Pinkal (15-Mar-2019) -- End

                Else

                    'Shani(24-JAN-2017) -- Start
                    'Enhancement : 
                    'If CBool(Session("GoalsAccomplishedRequiresApproval")) = True Then
                    '    cmnuOperation.Visible = CBool(Session("AllowToApproveGoalsAccomplishment"))
                    'Else
                    '    cmnuOperation.Visible = False
                    'End If

                    'S.SANDEEP |05-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'If CBool(Session("GoalsAccomplishedRequiresApproval")) = True Then
                    '    mnuAppRejGoalAccomplished.Visible = CBool(Session("AllowToApproveGoalsAccomplishment"))
                    'Else
                    '    mnuAppRejGoalAccomplished.Visible = False
                    'End If
                    'S.SANDEEP |05-MAR-2019| -- END
                    
                    'Shani(24-JAN-2017) -- End
                End If
                'SHANI (06 JUN 2015) -- End 

                'S.SANDEEP [04 JUN 2015] -- END

                ''S.SANDEEP [05 DEC 2015] -- START
                'Me.ViewState.Add("AssessorMasterId", 0)
                'Me.ViewState.Add("AssessorEmployeeId", 0)
                ''S.SANDEEP [05 DEC 2015] -- END

                'S.SANDEEP |05-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If Request.QueryString.Count > 0 Then
                '    cboEmployee.SelectedValue = mintEmpUnkId
                '    cboPeriod.SelectedValue = mintPeriodUnkid
                '    Call mnuAppRejGoalAccomplished_Click(mnuAppRejGoalAccomplished, New EventArgs)

                '    If mblnpopup_Accomplishment = True Then
                '        popup_GoalAccomplishment.Show()
                '    End If
                'End If
                'S.SANDEEP |05-MAR-2019| -- END

            Else
                'SHANI (18 JUN 2015) -- Start
                'mintUpdateProgressIndex = Me.ViewState("mintUpdateProgressIndex")
                'mintFieldUnkid = Me.ViewState("mintFieldUnkid")
                'mintLinkedFld = Me.ViewState("mintLinkedFld")
                'mintFieldTypeId = Me.ViewState("mintFieldTypeId")
                'mdtPeriodStartDate = Me.ViewState("mdtPeriodStartDate")
                'mstrDefinedGoal = Me.ViewState("mstrDefinedGoal")
                'mintTableFieldTranUnkId = Me.ViewState("mintTableFieldTranUnkId")
                'mblnUpdateProgress = Me.ViewState("mblnUpdateProgress")
                'mintEmpUpdateTranunkid = Me.ViewState("mintEmpUpdateTranunkid")
                'SHANI (18 JUN 2015) -- End 

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'mblnpopup_Accomplishment = Me.ViewState("mblnpopup_Accomplishment")
                'mdtAccomplishmentData = Me.ViewState("mdtAccomplishmentData")
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                'mintGoalTypeId = Me.ViewState("mintGoalTypeId")
                'mdblGoalValue = Me.ViewState("mdblGoalValue")
                'mdblPercentage = Me.ViewState("mdblPercentage")
                'mdtGoalAccomplishment = Me.ViewState("mdtGoalAccomplishment")
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP [01 JUL 2015] -- START
                'If Me.ViewState("mdtFinal") IsNot Nothing Then
                '    dgvData.DataSource = CType(Me.ViewState("mdtFinal"), DataTable)
                '    dgvData.DataBind()
                'End If
                Call AddLinkCol()
                'If Me.ViewState("mdtFinal") IsNot Nothing Then
                '    dgvData.DataSource = CType(Me.ViewState("mdtFinal"), DataTable)
                '    dgvData.DataBind()
                'Else
                '    dgvData.DataSource = Nothing : dgvData.DataBind()
                'End If
                'S.SANDEEP [01 JUL 2015] -- END

                mblnpopupShow_EmpField1 = Me.ViewState("popupShow_EmpField1")
                If mblnpopupShow_EmpField1 Then
                    pop_objfrmAddEditEmpField1.Show()
                End If

                mblnpopupShow_EmpField2 = Me.ViewState("popupShow_EmpField2")
                If mblnpopupShow_EmpField2 Then
                    pop_objfrmAddEditEmpField2.Show()
                End If

                mblnpopupShow_EmpField3 = Me.ViewState("popupShow_EmpField3")
                If mblnpopupShow_EmpField3 Then
                    pop_objfrmAddEditEmpField3.Show()
                End If

                mblnpopupShow_EmpField4 = Me.ViewState("popupShow_EmpField4")
                If mblnpopupShow_EmpField4 Then
                    pop_objfrmAddEditEmpField4.Show()
                End If

                mblnpopupShow_EmpField5 = Me.ViewState("popupShow_EmpField5")
                If mblnpopupShow_EmpField5 Then
                    pop_objfrmAddEditEmpField5.Show()
                End If

                'mblnpopupShow_AproverReject = Me.ViewState("mblnpopupShow_AproverReject")
                'If mblnpopupShow_AproverReject Then
                '    popup_ApprovrReject.Show()
                'End If


                'SHANI (18 JUN 2015) -- Start
                'If mblnUpdateProgress Then
                '    popup_UpdateProgress.Show()
                'End If
                'SHANI (18 JUN 2015) -- End 


                'S.SANDEEP [29 JAN 2015] -- START
                'If (Session("LoginBy") = Global.User.en_loginby.User) Then
                '    btnApproveSubmitted.Visible = Session("Allow_FinalSaveBSCPlanning")
                '    btnUnlockFinalSave.Visible = Session("Allow_UnlockFinalSaveBSCPlanning")
                'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                '    btnApproveSubmitted.Visible = False
                '    btnUnlockFinalSave.Visible = False
                'End If
                'S.SANDEEP [29 JAN 2015] -- END


                'S.SANDEEP [05 DEC 2015] -- START
                mintAssessorMasterId = Me.ViewState("AssessorMasterId")
                mintAssessorEmployeeId = Me.ViewState("AssessorEmployeeId")
                'S.SANDEEP [05 DEC 2015] -- END

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'If mblnpopup_Accomplishment Then
                '    popup_GoalAccomplishment.Show()
                'End If
                'Shani (26-Sep-2016) -- End

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 136
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
                'mblnAttachmentPopup = Convert.ToBoolean(Me.ViewState("mblnCancelAttachmentPopup"))
                'mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                'S.SANDEEP [29-NOV-2017] -- END

                'S.SANDEEP |08-JAN-2019| -- START
                'mblnpopup_UnlockEmp = Me.ViewState("mblnpopup_UnlockEmp")
                'mdtUnlockEmployeePrdEndDate = Me.ViewState("mdtUnlockEmployeePrdEndDate")
                'mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
                'S.SANDEEP |08-JAN-2019| -- END

                'S.SANDEEP |18-JAN-2019| -- START
                strPerspectiveColName = Me.ViewState("strPerspectiveColName")
                mintPerspectiveColIndex = Me.ViewState("mintPerspectiveColIndex")
                strGoalValueColName = Me.ViewState("strGoalValueColName")
                mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
                'S.SANDEEP |18-JAN-2019| -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                mstrGroupName = Me.ViewState("mstrGroupName")
                mintUoMColIndex = Me.ViewState("mintUoMColIndex")
                mstrUoMColName = Me.ViewState("mstrUoMColName")
                'S.SANDEEP |12-FEB-2019| -- END

            End If

            'S.SANDEEP [29 JAN 2015] -- START
            'btnUnlockFinalSave.Visible = False
            'btnApproveSubmitted.Visible = False
            'btnSubmitApproval.Visible = False

            ''SHANI (18 JUN 2015) -- Start
            'btnUpdatePercentage.Visible = False
            'btnOperation.Visible = False
            'SHANI (18 JUN 2015) -- End 

            'S.SANDEEP [29 JAN 2015] -- END


            'S.SANDEEP [23 DEC 2015] -- START

            ''S.SANDEEP [10 DEC 2015] -- START
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    btnCommitGoals.Visible = Session("SkipApprovalFlowInPlanning")
            '    btnOpenGoals.Visible = Session("SkipApprovalFlowInPlanning")
            'Else
            '    btnCommitGoals.Visible = False
            '    btnOpenGoals.Visible = False
            'End If
            ''S.SANDEEP [10 DEC 2015] -- END

            'S.SANDEEP [23 DEC 2015] -- END


            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 136

            'If mblnAttachmentPopup Then
            '    popup_ScanAttchment.Show()
            'End If

            'S.SANDEEP |08-JAN-2019| -- START
            'If mblnpopup_UnlockEmp Then
            '    popup_unlockemp.Show()
            'End If
            'S.SANDEEP |08-JAN-2019| -- END

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'S.SANDEEP [29-NOV-2017] -- END




            'S.SANDEEP [06 Jan 2016] -- START
            Call SetLanguage()
            'S.SANDEEP [06 Jan 2016] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'S.SANDEEP |11-APR-2019| -- START
        'If Request.QueryString.Count <= 0 Then
        '    Me.IsLoginRequired = True
        'End If
            Me.IsLoginRequired = True
        'S.SANDEEP |11-APR-2019| -- END
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("popupShow_EmpField1") Is Nothing Then
                Me.ViewState.Add("popupShow_EmpField1", mblnpopupShow_EmpField1)
            Else
                Me.ViewState("popupShow_EmpField1") = mblnpopupShow_EmpField1
            End If

            If Me.ViewState("popupShow_EmpField2") Is Nothing Then
                Me.ViewState.Add("popupShow_EmpField2", mblnpopupShow_EmpField2)
            Else
                Me.ViewState("popupShow_EmpField2") = mblnpopupShow_EmpField2
            End If

            If Me.ViewState("popupShow_EmpField3") Is Nothing Then
                Me.ViewState.Add("popupShow_EmpField3", mblnpopupShow_EmpField3)
            Else
                Me.ViewState("popupShow_EmpField3") = mblnpopupShow_EmpField3
            End If

            If Me.ViewState("popupShow_EmpField4") Is Nothing Then
                Me.ViewState.Add("popupShow_EmpField4", mblnpopupShow_EmpField4)
            Else
                Me.ViewState("popupShow_EmpField4") = mblnpopupShow_EmpField4
            End If

            If Me.ViewState("popupShow_EmpField5") Is Nothing Then
                Me.ViewState.Add("popupShow_EmpField5", mblnpopupShow_EmpField5)
            Else
                Me.ViewState("popupShow_EmpField5") = mblnpopupShow_EmpField5
            End If

            'If Me.ViewState("mblnpopupShow_AproverReject") Is Nothing Then
            '    Me.ViewState.Add("mblnpopupShow_AproverReject", mblnpopupShow_AproverReject)
            'Else
            '    Me.ViewState("mblnpopupShow_AproverReject") = mblnpopupShow_AproverReject
            'End If

            'SHANI (18 JUN 2015) -- Start
            'Me.ViewState("mintUpdateProgressIndex") = mintUpdateProgressIndex
            'Me.ViewState("mintFieldUnkid") = mintFieldUnkid
            'Me.ViewState("mintLinkedFld") = mintLinkedFld
            'Me.ViewState("mintFieldTypeId") = mintFieldTypeId
            'Me.ViewState("mdtPeriodStartDate") = mdtPeriodStartDate
            'Me.ViewState("mstrDefinedGoal") = mstrDefinedGoal
            'Me.ViewState("mintTableFieldTranUnkId") = mintTableFieldTranUnkId
            'Me.ViewState("mintEmpUpdateTranunkid") = mintEmpUpdateTranunkid
            'Me.ViewState("mblnUpdateProgress") = mblnUpdateProgress
            'SHANI (18 JUN 2015) -- End 


            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            'Me.ViewState("mintGoalTypeId") = mintGoalTypeId
            'Me.ViewState("mdblGoalValue") = mdblGoalValue
            'Me.ViewState("mdblPercentage") = mdblPercentage
            'Me.ViewState("mdtGoalAccomplishment") = mdtGoalAccomplishment
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP [05 DEC 2015] -- START
            Me.ViewState("AssessorMasterId") = mintAssessorMasterId
            Me.ViewState("AssessorEmployeeId") = mintAssessorEmployeeId
            'S.SANDEEP [05 DEC 2015] -- END

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'Me.ViewState("mblnpopup_Accomplishment") = mblnpopup_Accomplishment
            'Me.ViewState("mdtAccomplishmentData") = mdtAccomplishmentData
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 136
            'Me.ViewState("FieldAttachement") = mdtAttachement
            'Me.ViewState("mblnCancelAttachmentPopup") = mblnAttachmentPopup
            'Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            'S.SANDEEP [29-NOV-2017] -- END

            'S.SANDEEP |08-JAN-2019| -- START
            'Me.ViewState("mblnpopup_UnlockEmp") = mblnpopup_UnlockEmp
            'Me.ViewState("mdtUnlockEmployeePrdEndDate") = mdtUnlockEmployeePrdEndDate
            'Me.ViewState("mdtUnlockEmployee") = mdtUnlockEmployee
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP |18-JAN-2019| -- START
            If Me.ViewState("strPerspectiveColName") IsNot Nothing Then
                If CStr(Me.ViewState("strPerspectiveColName")).Trim.Length <= 0 Then Me.ViewState("strPerspectiveColName") = strPerspectiveColName
            Else
                Me.ViewState("strPerspectiveColName") = strPerspectiveColName
            End If
            If Me.ViewState("mintPerspectiveColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintPerspectiveColIndex")) <= 0 Then Me.ViewState("mintPerspectiveColIndex") = mintPerspectiveColIndex
            Else
                Me.ViewState("mintPerspectiveColIndex") = mintPerspectiveColIndex
            End If
            If Me.ViewState("strGoalValueColName") IsNot Nothing Then
                If CStr(Me.ViewState("strGoalValueColName")).Trim.Length <= 0 Then Me.ViewState("strGoalValueColName") = strGoalValueColName
            Else
                Me.ViewState("strGoalValueColName") = strGoalValueColName
            End If
            If Me.ViewState("mintGoalValueColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintGoalValueColIndex")) < 0 Then Me.ViewState("mintGoalValueColIndex") = mintGoalValueColIndex
            Else
                Me.ViewState("mintGoalValueColIndex") = mintGoalValueColIndex
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If Me.ViewState("mstrUoMColName") IsNot Nothing Then
                If Me.ViewState("mstrUoMColName").ToString().Trim.Length <= 0 Then Me.ViewState("mstrUoMColName") = mstrUoMColName
            Else
                Me.ViewState("mstrUoMColName") = mstrUoMColName
            End If
            If Me.ViewState("mstrGroupName") IsNot Nothing Then
                If Me.ViewState("mstrGroupName").ToString().Trim.Length <= 0 Then Me.ViewState("mstrGroupName") = mstrGroupName
            Else
                Me.ViewState("mstrGroupName") = mstrGroupName
            End If
            If Me.ViewState("mintUoMColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintUoMColIndex")) < 0 Then Me.ViewState("mintUoMColIndex") = mintUoMColIndex
            Else
                Me.ViewState("mintUoMColIndex") = mintUoMColIndex
            End If
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender" & ex.Message, Me)
        End Try


        'SHANI (16 APR 2015)-START
        lblEmpField1Remark1.ToolTip = lblEmpField1Remark1.Text
        lblEmpField1Remark2.ToolTip = lblEmpField1Remark2.Text
        lblEmpField1Remark3.ToolTip = lblEmpField1Remark3.Text

        lblEmpField2Remark1.ToolTip = lblEmpField2Remark1.Text
        lblEmpField2Remark2.ToolTip = lblEmpField2Remark2.Text
        lblEmpField2Remark3.ToolTip = lblEmpField2Remark3.Text

        lblEmpField3Remark1.ToolTip = lblEmpField3Remark1.Text
        lblEmpField3Remark2.ToolTip = lblEmpField3Remark2.Text
        lblEmpField3Remark3.ToolTip = lblEmpField3Remark3.Text

        lblEmpField4Remark1.ToolTip = lblEmpField4Remark1.Text
        lblEmpField4Remark2.ToolTip = lblEmpField4Remark2.Text
        lblEmpField4Remark3.ToolTip = lblEmpField4Remark3.Text

        lblEmpField5Remark1.ToolTip = lblEmpField5Remark1.Text
        lblEmpField5Remark2.ToolTip = lblEmpField5Remark2.Text
        lblEmpField5Remark3.ToolTip = lblEmpField5Remark3.Text
        'SHANI (16 APR 2015)--END 

    End Sub
#End Region

#Region "Private Methods"
    Private Sub SetFormViewSetting()
        Dim objFMaster As New clsAssess_Field_Master(True)
        Try
            If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                cboPerspective.Enabled = True
            Else
                cboPerspective.Enabled = False
            End If

            objlblField1.Text = objFMaster._Field1_Caption
            If objlblField1.Text = "" Then
                cboFieldValue1.Enabled = False
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                cboFieldValue1.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If
            objlblField2.Text = objFMaster._Field2_Caption
            If objlblField2.Text = "" Then
                cboFieldValue2.Enabled = False
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                cboFieldValue2.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If
            objlblField3.Text = objFMaster._Field3_Caption
            If objlblField3.Text = "" Then
                cboFieldValue3.Enabled = False
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                cboFieldValue3.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If
            objlblField4.Text = objFMaster._Field4_Caption
            If objlblField4.Text = "" Then
                cboFieldValue4.Enabled = False
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                cboFieldValue4.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If
            objlblField5.Text = objFMaster._Field5_Caption
            If objlblField5.Text = "" Then
                cboFieldValue5.Enabled = False
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                cboFieldValue5.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            'S.SANDEEP [09 FEB 2015] -- START
            btnGlobalAssign.Visible = Session("FollowEmployeeHierarchy")
            'S.SANDEEP [09 FEB 2015] -- END

            'If Session("CascadingTypeId") = enPACascading.STRICT_CASCADING Then
            '    dgvData.Columns(0).Visible = False : dgvData.Columns(0).Visible = False
            'End If
            'btnGlobalAssign.Visible = CBool(Session("FollowEmployeeHierarchy"))
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFormViewSetting :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    'dsList = objEmp.GetEmployeeList("List", True, True, Session("Employeeunkid"), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session(""))
            '    dsList = objEmp.GetEmployeeList("Emp", False, True, Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            'S.SANDEEP [05 DEC 2015] -- START
            Dim strFilterQry As String = String.Empty
            'S.SANDEEP [05 DEC 2015] -- END

            'S.SANDEEP [10 DEC 2015] -- START
            Dim blnApplyFilter As Boolean = False 'Shani(14-APR-2016) --  [True-False]
            'S.SANDEEP [10 DEC 2015] -- END


            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
                'S.SANDEEP [05 DEC 2015] -- START
            Else

                'Shani(14-APR-2016) -- Start
                'If Session("SkipApprovalFlowInPlanning") = True Then
                'Shani(14-APR-2016) -- End
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master
                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]

                If dsMapEmp.Tables("List").Rows.Count > 0 Then

                    mintAssessorMasterId = CInt(dsMapEmp.Tables("List").Rows(0)("Id"))
                    mintAssessorEmployeeId = CInt(dsMapEmp.Tables("List").Rows(0)("EmpId"))

                    dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                    strFilterQry = " hremployee_master.employeeunkid IN "
                    csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                    'Shani(14-APR-2016) -- Start
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                    'Shani(14-APR-2016) -- End
                End If

                '/* TO REMOVE EMPLOYEE FROM COMBO IF HE IS ASSESSOR -- START
                'strFilterQry = " hremployee_master.employeeunkid NOT IN "
                'If dsMapEmp.Tables("List").Rows.Count > 0 Then
                '    csvIds = String.Join(",", dsMapEmp.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("EmpId").ToString()).ToArray())
                'End If
                'If csvIds.Trim.Length > 0 Then
                '    strFilterQry &= "(" & csvIds & ")"
                'Else
                '    strFilterQry = ""
                'End If
                '/* TO REMOVE EMPLOYEE FROM COMBO IF HE IS ASSESSOR -- END

                objEval = Nothing

                'Shani(14-APR-2016) -- Start
                'End If
                'Shani(14-APR-2016) -- End

                'S.SANDEEP [05 DEC 2015] -- END

            End If
            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyFilter) 'S.SANDEEP [05 DEC 2015] {strFilterQry}
            'S.SANDEEP [10 DEC 2015] -- START {blnApplyFilter} -- END

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    .SelectedValue = 0
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    .SelectedValue = Session("Employeeunkid")
                End If
            End With
            Dim objPerspective As New clsassess_perspective_master
            dsList = objPerspective.getComboList("List", True)
            With cboPerspective
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END



            'S.SANDEEP [04 JUN 2015] -- START

            'Shani (09-May-2016) -- Start
            'Dim mintCurrentPeriodId As Integer = 0
            'If dsList.Tables(0).Rows.Count > 1 Then
            '    mintCurrentPeriodId = dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1).Item("periodunkid")
            'End If
            'Shani (09-May-2016) -- End
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'S.SANDEEP [04 JUN 2015] -- END

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                'S.SANDEEP [04 JUN 2015] -- START
                .SelectedValue = mintCurrentPeriodId
                'S.SANDEEP [04 JUN 2015] -- END
            End With

            ''S.SANDEEP |08-JAN-2019| -- START
            'With cboUnlockPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0).Copy
            '    .DataBind()
            'End With
            ''S.SANDEEP |08-JAN-2019| -- END

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)

            ''S.SANDEEP [29-NOV-2017] -- START
            ''ISSUE/ENHANCEMENT : REF-ID # 136
            'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            'With cboScanDcoumentType
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("List")
            '    .DataBind()
            'End With
            ''S.SANDEEP [29-NOV-2017] -- END

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}

            'S.SANDEEP |18-JAN-2019| -- START
            Dim intGoalInclusionTypeId As Integer = CInt(Session("GoalTypeInclusion"))
            'S.SANDEEP |18-JAN-2019| -- END
            dsList = (New clsMasterData).GetGoalTypeList(intGoalInclusionTypeId, "List", False) 'S.SANDEEP |18-JAN-2019| -- START {intGoalInclusionTypeId} -- END
            With cboEmpField1GoalType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy()
                .DataBind()
                .SelectedValue = enGoalType.GT_QUALITATIVE
            End With
            Call cboGoalType_SelectedIndexChanged(cboEmpField1GoalType, New EventArgs())

            With cboEmpField2GoalType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy()
                .DataBind()
                .SelectedValue = enGoalType.GT_QUALITATIVE
            End With
            Call cboGoalType_SelectedIndexChanged(cboEmpField2GoalType, New EventArgs())

            With cboEmpField3GoalType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy()
                .DataBind()
                .SelectedValue = enGoalType.GT_QUALITATIVE
            End With
            Call cboGoalType_SelectedIndexChanged(cboEmpField3GoalType, New EventArgs())

            With cboEmpField4GoalType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy()
                .DataBind()
                .SelectedValue = enGoalType.GT_QUALITATIVE
            End With
            Call cboGoalType_SelectedIndexChanged(cboEmpField4GoalType, New EventArgs())

            With cboEmpField5GoalType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy()
                .DataBind()
                .SelectedValue = enGoalType.GT_QUALITATIVE
            End With
            Call cboGoalType_SelectedIndexChanged(cboEmpField5GoalType, New EventArgs())

            'Dim item As System.Web.UI.WebControls.ListItem
            'With cboChangeBy
            '    .Items.Clear()
            '    item = New System.Web.UI.WebControls.ListItem
            '    item.Text = Language.getMessage(mstrModuleName_upd_percent, 100, "Select")
            '    item.Value = 0
            '    item.Attributes.Add(Language.getMessage(mstrModuleName_upd_percent, 100, "Select"), 0)
            '    .Items.Add(item)

            '    item = New System.Web.UI.WebControls.ListItem
            '    item.Text = Language.getMessage(mstrModuleName_upd_percent, 101, "Percentage")
            '    item.Value = 1
            '    item.Attributes.Add(Language.getMessage(mstrModuleName_upd_percent, 101, "Percentage"), 1)
            '    .Items.Add(item)

            '    item = New System.Web.UI.WebControls.ListItem
            '    item.Text = Language.getMessage(mstrModuleName_upd_percent, 102, "Value")
            '    item.Value = 2
            '    item.Attributes.Add(Language.getMessage(mstrModuleName_upd_percent, 102, "Value"), 2)
            '    .Items.Add(item)

            '    .DataBind()
            '    .SelectedIndex = 0
            'End With
            ''S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |08-JAN-2019| -- START
            'dsList = objUnlock.GetLockTypeComboList("List", True)
            'With cboLockType
            '    .DataValueField = "Id"
            '    .DataTextField = "Name"
            '    .DataSource = dsList.Tables("List")
            '    .DataBind()
            'End With
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.GOAL_UNIT_OF_MEASURE, True, "List")
            With cboEmpField1UomType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
            With cboEmpField2UomType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
            With cboEmpField3UomType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
            With cboEmpField4UomType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
            With cboEmpField5UomType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
            objCMaster = Nothing
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim mdtFinal As New DataTable
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Dim objFMaster = New clsAssess_Field_Master(True)

        'SHANI [09 Mar 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        Dim intGridWidth As Integer = 0
        'SHANI [09 Mar 2015]--END 

        Try

            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No data defined for asseessment caption settings screen."), Me)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period."), Me)
                Exit Sub
            End If
            objMap = Nothing

            'S.SANDEEP [04 JUN 2015] -- START
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                If CBool(Session("AllowtoViewEmployeeGoalsList")) = False Then Exit Sub
            End If
            'S.SANDEEP [04 JUN 2015] -- END


            'S.SANDEEP |13-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : PERSPECTIVE NOT DISPLAYED WHEN CASCADING SETTING IS ON {0003574}
            'dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", Session("CascadingTypeId"))
            'S.SANDEEP |13-MAR-2019| -- END


            If dtFinal.Columns.Contains("Field1") = False Then
                dtFinal.Columns.Add("Field1", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field2") = False Then
                dtFinal.Columns.Add("Field2", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field3") = False Then
                dtFinal.Columns.Add("Field3", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field4") = False Then
                dtFinal.Columns.Add("Field4", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field5") = False Then
                dtFinal.Columns.Add("Field5", GetType(System.String)).DefaultValue = ""
            End If

            If cboPerspective.Enabled = True AndAlso CInt(cboPerspective.SelectedValue) > 0 Then
                iSearch &= "AND perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' "
            End If

            If cboFieldValue1.Enabled = True AndAlso CInt(cboFieldValue1.SelectedValue) > 0 Then
                iSearch &= "AND empfield1unkid = '" & CInt(cboFieldValue1.SelectedValue) & "' "
            End If

            If cboFieldValue2.Enabled = True AndAlso CInt(cboFieldValue2.SelectedValue) > 0 Then
                iSearch &= "AND empfield2unkid = '" & CInt(cboFieldValue2.SelectedValue) & "' "
            End If

            If cboFieldValue3.Enabled = True AndAlso CInt(cboFieldValue3.SelectedValue) > 0 Then
                iSearch &= "AND empfield3unkid = '" & CInt(cboFieldValue3.SelectedValue) & "' "
            End If

            If cboFieldValue4.Enabled = True AndAlso CInt(cboFieldValue4.SelectedValue) > 0 Then
                iSearch &= "AND empfield4unkid = '" & CInt(cboFieldValue4.SelectedValue) & "' "
            End If

            If cboFieldValue5.Enabled = True AndAlso CInt(cboFieldValue5.SelectedValue) > 0 Then
                iSearch &= "AND empfield5unkid = '" & CInt(cboFieldValue5.SelectedValue) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                iSearch &= "AND CStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)

                'Shani(14-Sep-2015) -- Start
                'Issue: TRA Training Comments & Changes Requested By Dennis
                'mdtFinal = New DataView(dtFinal, iSearch, "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
                mdtFinal = New DataView(dtFinal, iSearch, "perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
                'Shani(14-Sep-2015) -- End
            Else

                'Shani(14-Sep-2015) -- Start
                'Issue: TRA Training Comments & Changes Requested By Dennis
                'mdtFinal = New DataView(dtFinal, "", "empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
                mdtFinal = New DataView(dtFinal, "", "perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
                'Shani(14-Sep-2015) -- End
            End If

            If mdtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = mdtFinal.NewRow
                mdtFinal.Rows.Add(iRow)
            End If

            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Dim intTotalWidth As Integer = 0
            If iPlan IsNot Nothing Then
                For index As Integer = 0 To iPlan.Length - 1
                    intTotalWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, iPlan(index), Session("CompanyUnkId"))
                Next
            End If
            'SHANI [09 Mar 2015]--END 

            If dgvData.Columns.Count > 0 Then dgvData.Columns.Clear()
            Call Add_GridColumns()

            'S.SANDEEP [05 DEC 2015] -- START

            'S.SANDEEP [10 DEC 2015] -- START
            'If Session("SkipApprovalFlowInPlanning") = True Then
            '    dgvData.Columns(0).Visible = True
            '    dgvData.Columns(1).Visible = True
            '    dgvData.Columns(2).Visible = True
            'End If
            'S.SANDEEP [10 DEC 2015] -- END

            'S.SANDEEP [05 DEC 2015] -- END


            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If Session("CascadingTypeId") = enPACascading.STRICT_CASCADING Then
                'SHANI (16 APR 2015)-START
                dgvData.Columns(0).Visible = False
                dgvData.Columns(2).Visible = False
                'SHANI (16 APR 2015)--END 
                intGridWidth = 25
            Else
                intGridWidth = 75
            End If
            'SHANI [09 Mar 2015]--END 


            'S.SANDEEP |18-JAN-2019| -- START
            strPerspectiveColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE)).Select(Function(x) x.ColumnName).First()
            strGoalValueColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE)).Select(Function(x) x.ColumnName).First()
            mstrUoMColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) x.ColumnName = "UoMType").Select(Function(x) x.ColumnName).First()
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            mstrGroupName = "" : objlblCurrentStatus.Text = ""
            If mdtFinal.Rows.Count > 0 Then
                'S.SANDEEP |07-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004244}
                'mstrGroupName = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of String)("STypeName") <> "").Select(Function(x) x.Field(Of String)("STypeName")).FirstOrDefault()
                mstrGroupName = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of String)("STypeName") <> "").Select(Function(x) x.Field(Of String)("STypeName")).DefaultIfEmpty().First()
                If mstrGroupName Is Nothing Then mstrGroupName = ""
                'S.SANDEEP |07-NOV-2019| -- END

                mdtFinal.Columns.Remove("STypeName")
                If mstrGroupName Is Nothing Then mstrGroupName = ""
            End If
            If mstrGroupName.Trim.Length > 0 Then objlblCurrentStatus.Text = "Current Status : " & mstrGroupName
            'S.SANDEEP |12-FEB-2019| -- END

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                dgvCol.HtmlEncode = False
                'SHANI [01 FEB 2015]--END
                If dgvData.Columns.Contains(dgvCol) = True Then Continue For

                'S.SANDEEP [02 JUL 2015] -- START
                'Shani [ 10 DEC 2014 ] -- START
                'If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                '    If dgvCol.DataField.StartsWith("Owr") Then
                '        dgvCol.Visible = False
                '    End If
                '    'If dCol.ColumnName.StartsWith("Owr") Then
                '    '    dgvCol.Visible = False
                '    'End If
                'End If

                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                If dgvCol.DataField.StartsWith("Field") Then
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties.Count <= 0 Then dgvCol.Visible = False
                End If
                'S.SANDEEP |18-JAN-2020| -- END

                If dgvCol.DataField.StartsWith("Owr") Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [02 JUL 2015] -- END



                'If dCol.Caption.Length <= 0 Then

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
                If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" _
                        Or dCol.ColumnName = "vuGoalAccomplishmentStatus" Then
                    'Shani (26-Sep-2016) -- End


                    'Shani [ 10 DEC 2014 ] -- END
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then

                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'dgvCol.HeaderStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
                        'dgvCol.ItemStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))

                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.

                        'S.SANDEEP |12-MAR-2019| -- START
                        'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
                        'Dim decColumnWidth As Decimal = 0
                        'If intTotalWidth > 0 Then
                        '    decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)) * 95 / intTotalWidth
                        '    dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        '    dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'Else
                        '    decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        '    dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        '    dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'End If

                        Dim decColumnWidth As Decimal = 0
                        decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompanyUnkId"))
                        dgvCol.HeaderStyle.Width = Unit.Pixel(decColumnWidth)
                        dgvCol.ItemStyle.Width = Unit.Pixel(decColumnWidth)
                        'S.SANDEEP |12-MAR-2019| -- END

                        'SHANI [21 Mar 2015]--END

                        'SHANI [09 Mar 2015]--END
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                                'SHANI [09 Mar 2015]-START
                                'Enhancement - REDESIGN SELF SERVICE.
                                '    Else
                                '        intGridWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                            End If
                            'Else
                            '    intGridWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                            'SHANI [09 Mar 2015]--END 
                        End If
                    End If
                End If
                'SHANI (18 JUN 2015) -- Start
                If dCol.ColumnName.ToUpper = "VUPROGRESS" Then
                    dgvCol.Visible = False
                    Call Add_UpdateProgressFiled(dCol)
                End If
                'SHANI (18 JUN 2015) -- End 
                dgvData.Columns.Add(dgvCol)

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                mdtFinal.Columns(dCol.ColumnName).ExtendedProperties.Add("index", dgvData.Columns.IndexOf(dgvCol))
                'Pinkal (16-Apr-2016) -- End


                'S.SANDEEP |18-JAN-2019| -- START
                If dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                End If
                If dgvCol.Visible = True Then
                    If strPerspectiveColName = dCol.ColumnName Then
                        mintPerspectiveColIndex = dgvData.Columns.IndexOf(dgvCol)
                    End If
                End If
                If dgvCol.Visible = True Then
                    If strGoalValueColName = dCol.ColumnName Then
                        mintGoalValueColIndex = dgvData.Columns.IndexOf(dgvCol)
                    End If
                End If
                'S.SANDEEP |18-JAN-2019| -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                If mstrUoMColName = dCol.ColumnName Then
                    mintUoMColIndex = dgvData.Columns.IndexOf(dgvCol)
                End If
                'S.SANDEEP |12-FEB-2019| -- END
                

            Next

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Dim idgvWidth As Integer = 0
            'For i As Integer = 0 To dgvData.Columns.Count - 1
            '    idgvWidth = idgvWidth + CInt(dgvData.Columns(i).ItemStyle.Width.Value)
            'Next
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'pnl_dgvData.Height = Unit.Pixel(400)
            'SHANI [01 FEB 2015]--END


            'S.SANDEEP [01 JUL 2015] -- START
            'If Me.ViewState("mintUpdateProgressIndex") IsNot Nothing Then
            '    'S.SANDEEP |12-FEB-2019| -- START
            '    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            '    'dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '    'dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            '    'S.SANDEEP |05-MAR-2019| -- START
            '    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            '    'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
            '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
            '    'Else
            '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            '    'End If
            '    If CBool(mdtFinal.Rows(0)("isfinal")) = False Then
                'dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                'dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                'Else
            '        'S.SANDEEP |16-AUG-2019| -- START
            '        'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            '        'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            '        'Else
            '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            '        'End If
                '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        'S.SANDEEP |16-AUG-2019| -- END
                'End If
            '    'S.SANDEEP |05-MAR-2019| -- END
            '    'S.SANDEEP |12-FEB-2019| -- END
                    'End If
            ''S.SANDEEP [01 JUL 2015] -- END


            'S.SANDEEP |10-AUG-2021| -- START
            'For Each dgvRow As DataRow In mdtFinal.Rows
            '    If CBool(dgvRow("isfinal")) = True Then
            '        dgvData.Columns(0).Visible = False : dgvData.Columns(1).Visible = False : dgvData.Columns(2).Visible = False
            '        dgvData.Style.Add("color", "blue")
            '        Exit For
            '        ' ''S.SANDEEP [10 DEC 2015] -- START
            '        ''If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '        ''    If Session("SkipApprovalFlowInPlanning") = True Then
            '        ''        btnCommitGoals.Visible = False : btnOpenGoals.Visible = True
            '        ''    End If
            '        ''Else
            '        ''    btnCommitGoals.Visible = False : btnOpenGoals.Visible = False
            '        ''End If
            '        ' ''S.SANDEEP [10 DEC 2015] -- END

            '        'dgvData.Style.Add("color", "blue")

            '        ''S.SANDEEP [01 JUL 2015] -- START
            '        'If Me.ViewState("mintUpdateProgressIndex") IsNot Nothing Then
            '        '    'S.SANDEEP |12-FEB-2019| -- START
            '        '    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            '        '    'dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = True
            '        '    'dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = True
            '        '    'S.SANDEEP |05-MAR-2019| -- START
            '        '    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            '        '    'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '        '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
            '        '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
            '        '    'Else
            '        '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            '        '    'End If
            '        '    If CBool(mdtFinal.Rows(0)("isfinal")) = False Then
            '        '        dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        '        dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                    '    'Else
            '        '        'S.SANDEEP |16-AUG-2019| -- START
            '        '        'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            '        '        'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '        '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            '        '        'Else
            '        '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = True
            '        '        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = True
            '        '        'End If
                    '    '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
            '        '        'S.SANDEEP |16-AUG-2019| -- END
            '        'End If
            '        '    'S.SANDEEP |05-MAR-2019| -- END
            '        '    'S.SANDEEP |12-FEB-2019| -- END
                    '    'End If
            '        ''Exit For
            '        ''S.SANDEEP [01 JUL 2015] -- END
            '    ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then

            '        ''S.SANDEEP [10 DEC 2015] -- START
                    '        'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '        '    If Session("SkipApprovalFlowInPlanning") = True Then
            '        '        btnCommitGoals.Visible = True : btnOpenGoals.Visible = False
            '        '    End If
                    '        'Else
            '        '    btnCommitGoals.Visible = False : btnOpenGoals.Visible = False
                    '        'End If
            '        ''S.SANDEEP [10 DEC 2015] -- END

            '        dgvData.Columns(0).Visible = False : dgvData.Columns(1).Visible = False : dgvData.Columns(2).Visible = False
            '        dgvData.Style.Add("color", "Green")
            '        Exit For
            '    Else

            '        'S.SANDEEP [16 JUN 2015] -- START
            '        'dgvData.Columns(0).Visible = True : dgvData.Columns(1).Visible = True : dgvData.Columns(2).Visible = True
            '        If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '            dgvData.Columns(0).Visible = True : dgvData.Columns(1).Visible = True : dgvData.Columns(2).Visible = True
            '        Else
            '            dgvData.Columns(0).Visible = CBool(Session("AllowtoAddEmployeeGoals"))
            '            dgvData.Columns(1).Visible = CBool(Session("AllowtoEditEmployeeGoals"))
            '            dgvData.Columns(2).Visible = CBool(Session("AllowtoDeleteEmployeeGoals"))
            '        End If
            '        'S.SANDEEP [16 JUN 2015] -- END

            '        dgvData.Style.Add("color", "black")
            '        Exit For
                    '    End If
            'Next
            For Each dgvRow As DataRow In mdtFinal.Rows
                If CBool(dgvRow("isfinal")) = True Then
                    dgvData.Columns(0).Visible = False : dgvData.Columns(1).Visible = False : dgvData.Columns(2).Visible = False
                    'dgvData.Style.Add("color", "blue")

                    If Session("BSC_StatusColors").Keys.Count > 0 Then
                        If Session("BSC_StatusColors").ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                            dgvData.Style.Add("color", ColorTranslator.ToHtml(Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.FINAL_SAVE))))))
                            objlblCurrentStatus.ForeColor = Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.FINAL_SAVE))))
                        Else
                            dgvData.Style.Add("color", "blue")
                            objlblCurrentStatus.ForeColor = Color.Blue
                        End If
                    Else
                        dgvData.Style.Add("color", "blue")
                        objlblCurrentStatus.ForeColor = Color.Blue
                    End If

                    Exit For
                ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    dgvData.Columns(0).Visible = False : dgvData.Columns(1).Visible = False : dgvData.Columns(2).Visible = False
                    'dgvData.Style.Add("color", "Green")
                    If Session("BSC_StatusColors").Keys.Count > 0 Then
                        If Session("BSC_StatusColors").ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                            dgvData.Style.Add("color", ColorTranslator.ToHtml(Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.SUBMIT_APPROVAL))))))
                            objlblCurrentStatus.ForeColor = Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                        Else
                            dgvData.Style.Add("color", "Green")
                            objlblCurrentStatus.ForeColor = Color.Green
                        End If
                    Else
                    dgvData.Style.Add("color", "Green")
                        objlblCurrentStatus.ForeColor = Color.Green
                    End If
                    Exit For
                Else
                    Dim strHex As String = ""
                    Select Case CInt(dgvRow("opstatusid"))
                        Case enObjective_Status.OPEN_CHANGES
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.OPEN_CHANGES), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.OPEN_CHANGES))), ColorTranslator.ToHtml(Color.Black))
                        Case enObjective_Status.NOT_SUBMIT
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.NOT_SUBMIT), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.NOT_SUBMIT))), ColorTranslator.ToHtml(Color.Red))
                        Case enObjective_Status.NOT_COMMITTED
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.NOT_COMMITTED), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.NOT_COMMITTED))), ColorTranslator.ToHtml(Color.Purple))
                        Case enObjective_Status.FINAL_COMMITTED
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.FINAL_COMMITTED), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.FINAL_COMMITTED))), ColorTranslator.ToHtml(Color.Blue))
                        Case enObjective_Status.PERIODIC_REVIEW
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.PERIODIC_REVIEW), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.PERIODIC_REVIEW))), ColorTranslator.ToHtml(Color.Brown))
                    End Select
                    dgvData.Style.Add("color", strHex)
                    objlblCurrentStatus.ForeColor = ColorTranslator.FromHtml(strHex)
                    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        dgvData.Columns(0).Visible = True : dgvData.Columns(1).Visible = True : dgvData.Columns(2).Visible = True
                    Else
                        dgvData.Columns(0).Visible = CBool(Session("AllowtoAddEmployeeGoals"))
                        dgvData.Columns(1).Visible = CBool(Session("AllowtoEditEmployeeGoals"))
                        dgvData.Columns(2).Visible = CBool(Session("AllowtoDeleteEmployeeGoals"))
                    End If
                    Exit For
                End If
            Next
            'S.SANDEEP |10-AUG-2021| -- END

            'S.SANDEEP [05 DEC 2015] -- START

            'S.SANDEEP [10 DEC 2015] -- START
            'If Session("SkipApprovalFlowInPlanning") = True Then
            '    dgvData.Columns(0).Visible = CBool(Session("AllowtoAddEmployeeGoals"))
            '    dgvData.Columns(1).Visible = CBool(Session("AllowtoEditEmployeeGoals"))
            '    dgvData.Columns(2).Visible = CBool(Session("AllowtoDeleteEmployeeGoals"))
            'End If
            'S.SANDEEP [10 DEC 2015] -- END

            'S.SANDEEP [05 DEC 2015] -- END

            If CInt(Session("CascadingTypeId")) = enPACascading.STRICT_CASCADING Then
                dgvData.Columns(0).Visible = False : dgvData.Columns(2).Visible = False
            End If

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'dgvData.Width = Unit.Pixel(intGridWidth)

            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'dgvData.Width = Unit.Percentage(99.5)
            dgvData.Width = Unit.Pixel(intTotalWidth)
            'S.SANDEEP |12-MAR-2019| -- END

            'SHANI [09 Mar 2015]--END 

            'SHANI (18 JUN 2015) -- Start
            'Me.ViewState("mdtFinal") = mdtFinal
            'SHANI (18 JUN 2015) -- End

            dgvData.DataSource = mdtFinal
            dgvData.DataBind()

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 29, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

            'SHANI (18 JUN 2015) -- Start
            'Me.ViewState("mdtFinal") = mdtFinal
            'SHANI (18 JUN 2015) -- End 

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Grid" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Add_GridColumns()
        Try
            '************* ADD
            Dim iTempField As New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Shani [ 10 DEC 2014 ] -- START
            '

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [09 Mar 2015]--END 

            'Shani [ 10 DEC 2014 ] -- END
            iTempField.FooterText = "ObjAdd"

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.HeaderText = "Add"
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            iTempField.HeaderStyle.Width = Unit.Pixel(25)
            iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [01 FEB 2015]--END 
            dgvData.Columns.Add(iTempField)

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Shani [ 10 DEC 2014 ] -- START
            '

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [09 Mar 2015]--END

            'Shani [ 10 DEC 2014 ] -- END
            iTempField.FooterText = "objEdit"

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.HeaderText = "Edit"
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            iTempField.HeaderStyle.Width = Unit.Pixel(25)
            iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [01 FEB 2015]--END 
            dgvData.Columns.Add(iTempField)

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Shani [ 10 DEC 2014 ] -- START
            '

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [09 Mar 2015]--END

            'Shani [ 10 DEC 2014 ] -- END

            iTempField.FooterText = "objDelete"

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.HeaderText = "Delete"
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            iTempField.HeaderStyle.Width = Unit.Pixel(25)
            iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [01 FEB 2015]--END 
            dgvData.Columns.Add(iTempField)

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            dgvData.Columns(0).Visible = CBool(Session("AllowtoAddEmployeeGoals"))
            dgvData.Columns(1).Visible = CBool(Session("AllowtoEditEmployeeGoals"))
            dgvData.Columns(2).Visible = CBool(Session("AllowtoDeleteEmployeeGoals"))
            'S.SANDEEP [28 MAY 2015] -- END

            If Session("CascadingTypeId") = enPACascading.STRICT_CASCADING Then
                dgvData.Columns(0).Visible = False  'ADD
                dgvData.Columns(2).Visible = False  'DELETE
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Add_GridColumns :-" & ex.Message, Me)
        End Try
    End Sub


    'SHANI (18 JUN 2015) -- Start
    Private Sub Add_UpdateProgressFiled(ByVal dcol As DataColumn)
        Try
            Dim iTempField As New TemplateField()
            iTempField.FooterText = dcol.ColumnName
            iTempField.HeaderText = dcol.Caption
            iTempField.HeaderStyle.Width = Unit.Pixel(70)
            iTempField.ItemStyle.Width = Unit.Pixel(70)
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.Visible = False
            dgvData.Columns.Add(iTempField)
            'mintUpdateProgressIndex = dgvData.Columns.IndexOf(iTempField)
            ''S.SANDEEP [01 JUL 2015] -- START
            'Me.ViewState("mintUpdateProgressIndex") = mintUpdateProgressIndex
            ''S.SANDEEP [01 JUL 2015] -- END

            ''S.SANDEEP [16 JUN 2015] -- START
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    'S.SANDEEP |12-FEB-2019| -- START
            '    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            '    'dgvData.Columns(mintUpdateProgressIndex).Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
            '    dgvData.Columns(mintUpdateProgressIndex).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
            '    'btnUpdateSave.Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
            '    'S.SANDEEP |12-FEB-2019| -- END
            'End If
            ''S.SANDEEP [16 JUN 2015] -- END

            ''S.SANDEEP |05-MAR-2019| -- START
            ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'dgvData.Columns(mintUpdateProgressIndex).Visible = False
            ''S.SANDEEP |05-MAR-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("Add_UpdateProgressFiled:- " & ex.Message, Me)
        End Try
    End Sub

    'SHANI (18 JUN 2015) -- End 

    Private Sub AddLinkCol()
        Try
        For Each gRow As GridViewRow In dgvData.Rows
            If gRow.RowType = DataControlRowType.DataRow Then
                If gRow.Cells(0).FindControl("imgAdd") Is Nothing Then
                    Dim imgAdd As New ImageButton()
                    imgAdd.ID = "imgAdd"
                    imgAdd.Attributes.Add("Class", "objAddBtn")
                    imgAdd.Attributes.Add("onclick", "getscrollPosition()")
                    imgAdd.CommandName = "objAdd"
                    imgAdd.ImageUrl = "~/images/add_16.png"
                    imgAdd.ToolTip = "New"
                    RemoveHandler imgAdd.Click, AddressOf imgAdd_Click
                    AddHandler imgAdd.Click, AddressOf imgAdd_Click
                    gRow.Cells(0).Controls.Add(imgAdd)
                End If
                If gRow.Cells(1).FindControl("imgEdit") Is Nothing Then
                    Dim imgEdit As New ImageButton()
                    imgEdit.ID = "imgEdit"
                    imgEdit.Attributes.Add("Class", "objAddBtn")
                    imgEdit.Attributes.Add("onclick", "getscrollPosition()")
                    imgEdit.CommandName = "objEdit"
                    imgEdit.ImageUrl = "~/images/Edit.png"
                    imgEdit.ToolTip = "Edit"
                    RemoveHandler imgEdit.Click, AddressOf imgEdit_Click
                    AddHandler imgEdit.Click, AddressOf imgEdit_Click
                    gRow.Cells(1).Controls.Add(imgEdit)
                End If
                If gRow.Cells(2).FindControl("imgDelete") Is Nothing Then
                    Dim imgDelete As New ImageButton()
                    imgDelete.ID = "imgDelete"
                    imgDelete.Attributes.Add("Class", "objAddBtn")
                    imgDelete.Attributes.Add("onclick", "getscrollPosition()")
                    imgDelete.CommandName = "objDelete"
                    imgDelete.ImageUrl = "~/images/remove.png"
                    imgDelete.ToolTip = "Delete"
                    RemoveHandler imgDelete.Click, AddressOf imgDelete_Click
                    AddHandler imgDelete.Click, AddressOf imgDelete_Click
                    gRow.Cells(2).Controls.Add(imgDelete)
                End If
            End If
        Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub Genrate_AddmanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnAdd" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Language.getMessage(mstrModuleName, 26, "Add") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                Next
            End If
            dlmnuAdd.DataSource = dsList
            dlmnuAdd.DataBind()

            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Genrate_AddmanuItem :-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub Genrate_EditmanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            Dim iclmOrgname As New DataColumn("lnkOriginal", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            dsList.Tables(0).Columns.Add(iclmOrgname)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnEdt" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Language.getMessage(mstrModuleName, 27, "Edit") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    dsList.Tables(0).Rows(i)("lnkOriginal") = dsList.Tables(0).Rows(i)("fieldcaption").ToString
                Next
            End If
            dlmnuEdit.DataSource = dsList
            dlmnuEdit.DataBind()
            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Genrate_EditmanuItem :-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub Genrate_DeletemanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            Dim iclmOrgname As New DataColumn("lnkOriginal", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            dsList.Tables(0).Columns.Add(iclmOrgname)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Language.getMessage(mstrModuleName, 28, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    dsList.Tables(0).Rows(i)("lnkOriginal") = dsList.Tables(0).Rows(i)("fieldcaption").ToString
                Next
            End If
            dlmnuDelete.DataSource = dsList
            dlmnuDelete.DataBind()

            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Genrate_DeletemanuItem :-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub EmpField1_load(ByVal p_mintEmpField1Unkid As Integer, ByVal p_enEmpField1Action As enAction)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField1Unkid As Integer = p_mintEmpField1Unkid 'Empployee Field1 unkId (Identity Column)
        Dim mdtEmpField1Owner As New DataTable 'Empployee Field1 Table
        Dim enEmpField1Action As enAction = p_enEmpField1Action 'Empployee Field1 Actions
        Dim mdicEmpField1FieldData As New Dictionary(Of Integer, String) 'Empployee Field1 Dictionary
        Dim mintEmpField1FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field1 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField1LinkedFieldId As Integer = 0 'Empployee Field1 LinkedFieldID
        Try
            mblnpopupShow_EmpField1 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField1LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtEmpField1Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField1LinkedFieldId <> mintEmpField1FieldUnkid Then
                objEmpField1tabcRemarks.Enabled = False : pnl_RightEmpField1.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField1GoalType.Enabled = pnl_RightEmpField1.Enabled
                txtEmpField1GoalValue.Enabled = pnl_RightEmpField1.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField1.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END

            End If
            mdtEmpField1Owner = objOwrOwner.Get_Data(mintEmpField1Unkid, enWeight_Types.WEIGHT_FIELD1)

            If enEmpField1Action = enAction.EDIT_ONE Then
                objEmpField1._Empfield1unkid = mintEmpField1Unkid
            End If

            Me.ViewState.Add("mdtEmpField1Owner", mdtEmpField1Owner)
            Me.ViewState.Add("mintEmpField1Unkid", mintEmpField1Unkid)
            Me.ViewState.Add("mdicEmpField1FieldData", mdicEmpField1FieldData)
            Me.ViewState.Add("mintEmpField1FieldUnkid", mintEmpField1FieldUnkid)
            Me.ViewState.Add("menEmpField1Action", enEmpField1Action)
            Me.ViewState.Add("mintEmpField1LinkedFieldId", mintEmpField1LinkedFieldId)

            Call Set_EmpField1Form_Information()
            Call FillCombo_EmpField1()
            Call Fill_Data_EmpField1()
            Call GetEmpField1Value()
        Catch ex As Exception
            DisplayMessage.DisplayError("EmpField1_load :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub EmpField2_load(ByVal p_mintEmpField2Unkid As Integer, ByVal p_enEmpField2Action As enAction, Optional ByVal p_mintEmpField2ParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField2Unkid As Integer = p_mintEmpField2Unkid 'Empployee Field2 unkId (Identity Column)
        Dim menEmpField2Action As enAction = p_enEmpField2Action 'Empployee Field2 Actions
        Dim mdicEmpField2FieldData As New Dictionary(Of Integer, String) 'Empployee Field2 Dictionary
        Dim mintEmpField2ParentId As Integer = p_mintEmpField2ParentId 'Empployee Field2 ParentID
        Dim mdtEmpField2Owner As DataTable 'Empployee Field2 Table
        Dim mintEmpField2FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field2 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField2LinkedFieldId As Integer = -1  'Empployee Field2 LinkedFieldID
        Try
            mblnpopupShow_EmpField2 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField2LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtEmpField2Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField2LinkedFieldId <> mintEmpField2FieldUnkid Then
                objEmpField2tabcRemarks.Enabled = False : pnl_RightEmpField2.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField2GoalType.Enabled = pnl_RightEmpField2.Enabled
                txtEmpField2GoalValue.Enabled = pnl_RightEmpField2.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField2.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            If menEmpField2Action = enAction.EDIT_ONE Then
                objEmpField2._Empfield2unkid = mintEmpField2Unkid
            End If

            mdtEmpField2Owner = objOwrOwner.Get_Data(mintEmpField2Unkid, enWeight_Types.WEIGHT_FIELD2)


            Me.ViewState.Add("mdtEmpField2Owner", mdtEmpField2Owner)
            Me.ViewState.Add("mintEmpField2Unkid", mintEmpField2Unkid)
            Me.ViewState.Add("menEmpField2Action", menEmpField2Action)
            Me.ViewState.Add("mdicEmpField2FieldData", mdicEmpField2FieldData)
            Me.ViewState.Add("mintEmpField2FieldUnkid", mintEmpField2FieldUnkid)
            Me.ViewState.Add("mintEmpField2LinkedFieldId", mintEmpField2LinkedFieldId)
            Me.ViewState.Add("mintEmpField2ParentId", mintEmpField2ParentId)


            Call Set_EmpField2Form_Information()
            Call Fill_Data_EmpField2()
            Call FillCombo_EmpField2()
            Call GetEmpField2Value()
            If mintEmpField2ParentId > 0 Then
                Try
                    cboEmpField2EmpFieldValue1.SelectedValue = mintEmpField2ParentId
                    Call cboEmpField2EmpFieldValue1_SelectedIndexChanged(cboEmpField2EmpFieldValue1, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub EmpField3_load(ByVal p_mintEmpField3Unkid As Integer, ByVal p_enEmpField3Action As enAction, Optional ByVal p_mintEmpField3ParentId As Integer = 0, Optional ByVal p_mintEmpField3MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField3Unkid As Integer = p_mintEmpField3Unkid 'Empployee Field3 unkId (Identity Column)
        Dim menEmpField3Action As enAction = p_enEmpField3Action 'Empployee Field3 Actions
        Dim mdicEmpField3FieldData As New Dictionary(Of Integer, String) 'Empployee Field3 Dictionary
        Dim mintEmpField3ParentId As Integer = p_mintEmpField3ParentId 'Empployee Field3 ParentID
        Dim mintEmpField3MainParentId As Integer = p_mintEmpField3MainParentId 'Empployee Field3 ParentID
        Dim mdtEmpField3Owner As DataTable 'Empployee Field3 Table
        Dim mintEmpField3FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field3 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField3LinkedFieldId As Integer = -1  'Empployee Field3 LinkedFieldID
        Try
            mblnpopupShow_EmpField3 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField3LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtEmpField3Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField3LinkedFieldId <> mintEmpField3FieldUnkid Then
                objEmpField3tabcRemarks.Enabled = False : pnl_RightEmpField3.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField3GoalType.Enabled = pnl_RightEmpField3.Enabled
                txtEmpField3GoalValue.Enabled = pnl_RightEmpField3.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField3.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            If menEmpField3Action = enAction.EDIT_ONE Then
                objEmpField3._Empfield3unkid = mintEmpField3Unkid
            End If
            mdtEmpField3Owner = objOwrOwner.Get_Data(mintEmpField3Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtEmpField3Owner", mdtEmpField3Owner)
            Me.ViewState.Add("mintEmpField3Unkid", mintEmpField3Unkid)
            Me.ViewState.Add("menEmpField3Action", menEmpField3Action)
            Me.ViewState.Add("mdicEmpField3FieldData", mdicEmpField3FieldData)
            Me.ViewState.Add("mintEmpField3FieldUnkid", mintEmpField3FieldUnkid)
            Me.ViewState.Add("mintEmpField3LinkedFieldId", mintEmpField3LinkedFieldId)
            Me.ViewState.Add("mintEmpField3ParentId", mintEmpField3ParentId)
            Me.ViewState.Add("mintEmpField3MainParentId", mintEmpField3MainParentId)


            Call Set_EmpField3Form_Information()
            Call Fill_Data_EmpField3()
            Call FillCombo_EmpField3()
            Call GetEmpField3Value()
            If mintEmpField3ParentId > 0 Then
                Try
                    cboEmpField3EmpFieldValue2.SelectedValue = mintEmpField3ParentId
                    Call cboEmpField3EmpFieldValue2_SelectedIndexChanged(cboEmpField3EmpFieldValue2, Nothing)
                Catch ex As Exception
                    Throw ex

                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("EmpField3_load :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub EmpField4_load(ByVal p_mintEmpField4Unkid As Integer, ByVal p_enEmpField4Action As enAction, Optional ByVal p_mintEmpField4ParentId As Integer = 0, Optional ByVal p_mintEmpField4MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField4Unkid As Integer = p_mintEmpField4Unkid 'Empployee Field3 unkId (Identity Column)
        Dim menEmpField4Action As enAction = p_enEmpField4Action 'Empployee Field3 Actions
        Dim mdicEmpField4FieldData As New Dictionary(Of Integer, String) 'Empployee Field3 Dictionary
        Dim mintEmpField4ParentId As Integer = p_mintEmpField4ParentId 'Empployee Field3 ParentID
        Dim mintEmpField4MainParentId As Integer = p_mintEmpField4MainParentId 'Empployee Field3 ParentID
        Dim mdtEmpField4Owner As DataTable 'Empployee Field3 Table
        Dim mintEmpField4FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field3 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField4LinkedFieldId As Integer = -1  'Empployee Field3 LinkedFieldID
        Try
            mblnpopupShow_EmpField4 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField4LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtEmpField4Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField4LinkedFieldId <> mintEmpField4FieldUnkid Then
                objEmpField4tabcRemarks.Enabled = False : pnl_RightEmpField4.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField4GoalType.Enabled = pnl_RightEmpField4.Enabled
                txtEmpField4GoalValue.Enabled = pnl_RightEmpField4.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField4.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            If menEmpField4Action = enAction.EDIT_ONE Then
                objEmpField4._Empfield4unkid = mintEmpField4Unkid
            End If
            mdtEmpField4Owner = objOwrOwner.Get_Data(mintEmpField4Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtEmpField4Owner", mdtEmpField4Owner)
            Me.ViewState.Add("mintEmpField4Unkid", mintEmpField4Unkid)
            Me.ViewState.Add("menEmpField4Action", menEmpField4Action)
            Me.ViewState.Add("mdicEmpField4FieldData", mdicEmpField4FieldData)
            Me.ViewState.Add("mintEmpField4FieldUnkid", mintEmpField4FieldUnkid)
            Me.ViewState.Add("mintEmpField4LinkedFieldId", mintEmpField4LinkedFieldId)
            Me.ViewState.Add("mintEmpField4ParentId", mintEmpField4ParentId)
            Me.ViewState.Add("mintEmpField4MainParentId", mintEmpField4MainParentId)


            Call Set_EmpField4Form_Information()
            Call Fill_Data_EmpField4()
            Call FillCombo_EmpField4()
            Call GetEmpField4Value()
            If mintEmpField4ParentId > 0 Then
                Try
                    cboEmpField4EmpFieldValue3.SelectedValue = mintEmpField4ParentId
                    Call cboEmpField4EmpFieldValue3_SelectedIndexChanged(cboEmpField4EmpFieldValue3, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("EmpField4_load :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub EmpField5_load(ByVal p_mintEmpField5Unkid As Integer, ByVal p_enEmpField5Action As enAction, Optional ByVal p_mintEmpField5ParentId As Integer = 0, Optional ByVal p_mintEmpField5MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField5Unkid As Integer = p_mintEmpField5Unkid 'Empployee Field3 unkId (Identity Column)
        Dim menEmpField5Action As enAction = p_enEmpField5Action 'Empployee Field3 Actions
        Dim mdicEmpField5FieldData As New Dictionary(Of Integer, String) 'Empployee Field3 Dictionary
        Dim mintEmpField5ParentId As Integer = p_mintEmpField5ParentId 'Empployee Field3 ParentID
        Dim mintEmpField5MainParentId As Integer = p_mintEmpField5MainParentId 'Empployee Field3 ParentID
        Dim mdtEmpField5Owner As DataTable 'Empployee Field3 Table
        Dim mintEmpField5FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field3 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField5LinkedFieldId As Integer = -1  'Empployee Field3 LinkedFieldID
        Try
            mblnpopupShow_EmpField5 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField5LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtEmpField5Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField5LinkedFieldId <> mintEmpField5FieldUnkid Then
                objEmpField5tabcRemarks.Enabled = False : pnl_RightEmpField5.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField5GoalType.Enabled = pnl_RightEmpField5.Enabled
                txtEmpField5GoalValue.Enabled = pnl_RightEmpField5.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField5.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            If menEmpField5Action = enAction.EDIT_ONE Then
                objEmpField5._Empfield5unkid = mintEmpField5Unkid
            End If
            mdtEmpField5Owner = objOwrOwner.Get_Data(mintEmpField5Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtEmpField5Owner", mdtEmpField5Owner)
            Me.ViewState.Add("mintEmpField5Unkid", mintEmpField5Unkid)
            Me.ViewState.Add("menEmpField5Action", menEmpField5Action)
            Me.ViewState.Add("mdicEmpField5FieldData", mdicEmpField5FieldData)
            Me.ViewState.Add("mintEmpField5FieldUnkid", mintEmpField5FieldUnkid)
            Me.ViewState.Add("mintEmpField5LinkedFieldId", mintEmpField5LinkedFieldId)
            Me.ViewState.Add("mintEmpField5ParentId", mintEmpField5ParentId)
            Me.ViewState.Add("mintEmpField5MainParentId", mintEmpField5MainParentId)


            Call Set_EmpField5Form_Information()
            Call Fill_Data_EmpField5()
            Call FillCombo_EmpField5()
            Call GetEmpField5Value()

            If mintEmpField5ParentId > 0 Then
                Try
                    cboEmpField5EmpFieldValue4.SelectedValue = mintEmpField5ParentId
                    Call cboEmpField5EmpFieldValue4_SelectedIndexChanged(cboEmpField5EmpFieldValue4, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("EmpField5_load :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub Set_EmpField1Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
        Try
            lblheaderEmpField1.Text = Language.getMessage(mstrModuleName_Emp1, 1, "Add/Edit Employee") & " " & objFieldMaster._Field1_Caption & " " & _
                      Language.getMessage(mstrModuleName_Emp1, 2, "Information")

            objEmpField1lblField1.Text = Language.getMessage(mstrModuleName_Emp1, 15, "Goal Owner") & " " & objFieldMaster._Field1_Caption

            objEmpField1lblEmpField1.Text = objFieldMaster._Field1_Caption
            hdf_txtEmpField1EmpField1.Value = objFieldMaster._Field1Unkid

            Select Case Session("CascadingTypeId")

                'Shani (16-Sep-2016) -- Start
                'Enhancement - 
                'Case enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT, enPACascading.LOOSE_CASCADING, enPACascading.LOOSE_GOAL_ALIGNMENT
                Case enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT
                    'Shani (16-Sep-2016) -- End

                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'cboEmpField1Perspective.Enabled = True
                    lblPerspective.Visible = True
                    cboEmpField1Perspective.Enabled = True

                    objEmpField1lblField1.Visible = False
                    cboEmpField1FieldValue1.Visible = False
                    pnlEmpField1lblField1.Visible = False


                    'S.SANDEEP |12-FEB-2019| -- END
                Case Else
                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'cboEmpField1Perspective.Enabled = False

                    'S.SANDEEP |13-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : PERSPECTIVE NOT DISPLAYED WHEN CASCADING SETTING IS ON {0003574}
                    'lblPerspective.Visible = False
                    'cboEmpField1Perspective.Visible = False
                    cboEmpField1Perspective.Enabled = False
                    'S.SANDEEP |13-MAR-2019| -- END

                    'S.SANDEEP |12-FEB-2019| -- END


            End Select
            If CInt(Me.ViewState("mintOwrField1FieldUnkid")) = CInt(Me.ViewState("mintOwrField1LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField1tabpRemark1.Visible = False
                    objEmpField1tabpRemark1.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END

                Else
                    objEmpField1tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField1Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblEmpField1Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField1FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField1FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField1tabpRemark2.Visible = False
                    objEmpField1tabpRemark2.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField1tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    hdf_lblEmpField1Remark2.Value = objFieldMaster._Field7Unkid
                    lblEmpField1Remark2.Text = objFieldMaster._Field7_Caption
                    If mdicEmpField1FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField1FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField1tabpRemark3.Visible = False
                    objEmpField1tabpRemark3.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField1tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    hdf_lblEmpField1Remark3.Value = objFieldMaster._Field8Unkid
                    lblEmpField1Remark3.Text = objFieldMaster._Field8_Caption
                    If mdicEmpField1FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField1FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField1FieldData.Keys.Count > 0 Then objEmpField1tabcRemarks.Enabled = True
                pnl_EmpField1dgvower.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField1SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField1tabcRemarks.Enabled = False : pnl_RightEmpField1.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField1GoalType.Enabled = pnl_RightEmpField1.Enabled
                txtEmpField1GoalValue.Enabled = pnl_RightEmpField1.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField1.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END

            End If

            Dim objEmp As New clsEmployee_Master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            hdf_txtEmpField1EmployeeName.Value = CInt(cboEmployee.SelectedValue)
            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField1EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField1EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Set_EmpField1Form_Information" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField2Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField2.Text = Language.getMessage(mstrModuleName_Emp2, 1, "Add/Edit Employee") & " " & objFieldMaster._Field2_Caption & " " & _
                      Language.getMessage(mstrModuleName_Emp2, 2, "Information")

            objEmpField2lblEmpField1.Text = objFieldMaster._Field1_Caption
            hdf_cboEmpField2EmpFieldValue1.Value = objFieldMaster._Field1Unkid

            objEmpField2lblEmpField2.Text = objFieldMaster._Field2_Caption
            hdf_txtEmpField2EmpField2.Value = objFieldMaster._Field2Unkid

            If CInt(Me.ViewState("mintEmpField2FieldUnkid")) = CInt(Me.ViewState("mintEmpField2LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField2tabpRemark1.Visible = False
                    objEmpField2tabpRemark1.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField2tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField2Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblEmpField2Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField2FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField2FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField2tabpRemark2.Visible = False
                    objEmpField2tabpRemark2.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField2tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    lblEmpField2Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_lblEmpField2Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField2FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField2FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField2tabpRemark3.Visible = False
                    objEmpField2tabpRemark3.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField2tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    lblEmpField2Remark3.Text = objFieldMaster._Field8_Caption
                    'SHANI (16 APR 2015)-START
                    'hdf_lblEmpField3Remark2.Value = objFieldMaster._Field8Unkid
                    hdf_lblEmpField2Remark3.Value = objFieldMaster._Field8Unkid
                    'SHANI (16 APR 2015)--END 
                    If mdicEmpField2FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField2FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField2FieldData.Keys.Count > 0 Then objEmpField2tabcRemarks.Enabled = True
                pnl_EmpField2dgvower.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField2SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField2tabcRemarks.Enabled = False : pnl_RightEmpField2.Enabled = False : objEmpField2tabcRemarks.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField2GoalType.Enabled = pnl_RightEmpField2.Enabled
                txtEmpField2GoalValue.Enabled = pnl_RightEmpField2.Enabled

                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField2.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            Dim objEmp As New clsEmployee_Master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField2EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField2EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Set_EmpField2Form_Information :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField3Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField3.Text = Language.getMessage(mstrModuleName_Emp3, 1, "Add/Edit Employee") & " " & objFieldMaster._Field3_Caption & " " & _
                      Language.getMessage(mstrModuleName_Emp3, 2, "Information")

            objEmpField3lblEmpField1.Text = objFieldMaster._Field1_Caption

            objEmpField3lblEmpField2.Text = objFieldMaster._Field2_Caption
            hdf_cboEmpField3EmpFieldValue2.Value = objFieldMaster._Field2Unkid

            objEmpField3lblOwrField3.Text = objFieldMaster._Field3_Caption
            hdf_txtEmpField3EmpField3.Value = objFieldMaster._Field3Unkid

            If CInt(Me.ViewState("mintEmpField3FieldUnkid")) = CInt(Me.ViewState("mintEmpField3LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField3tabpRemark1.Visible = False
                    objEmpField3tabpRemark1.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField3tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField3Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblEmpField3Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField3FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField3FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField3tabpRemark2.Visible = False
                    objEmpField3tabpRemark2.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField3tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    lblEmpField3Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_lblEmpField3Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField3FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField3FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField3tabpRemark3.Visible = False
                    objEmpField3tabpRemark3.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField3tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    lblEmpField3Remark3.Text = objFieldMaster._Field8_Caption
                    hdf_lblEmpField3Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicEmpField3FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField3FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField3FieldData.Keys.Count > 0 Then objEmpField3tabcRemarks.Enabled = True
                pnl_EmpField3dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField3SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField3tabcRemarks.Enabled = False : pnl_RightEmpField3.Enabled = False : objEmpField3tabcRemarks.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField3GoalType.Enabled = pnl_RightEmpField3.Enabled
                txtEmpField3GoalValue.Enabled = pnl_RightEmpField3.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField3.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            Dim objEmp As New clsEmployee_Master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField3Employee.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField3Employee.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Set_EmpField3Form_Information :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField4Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField4.Text = Language.getMessage(mstrModuleName_Emp4, 1, "Add/Edit Employee") & " " & objFieldMaster._Field4_Caption & " " & _
                      Language.getMessage(mstrModuleName_Emp4, 2, "Information")

            objEmpField4lblEmpField1.Text = objFieldMaster._Field1_Caption
            objEmpField4lblEmpField2.Text = objFieldMaster._Field2_Caption
            objEmpField4lblEmpField3.Text = objFieldMaster._Field3_Caption
            hdf_cboEmpField4EmpFieldValue3.Value = objFieldMaster._Field3Unkid

            objEmpField4lblEmpField4.Text = objFieldMaster._Field4_Caption
            hdf_objEmpField4txtEmpField4.Value = objFieldMaster._Field4Unkid
            If CInt(Me.ViewState("mintEmpField4FieldUnkid")) = CInt(Me.ViewState("mintEmpField4LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField4tabpRemark1.Visible = False
                    objEmpField4tabpRemark1.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField4tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField4Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_txtEmpField4Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField4FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField4FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField4tabpRemark2.Visible = False
                    objEmpField4tabpRemark2.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField4tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    'SHANI (16 APR 2015)-START
                    'lblEmpField4Remark1.Text = objFieldMaster._Field7_Caption
                    lblEmpField4Remark2.Text = objFieldMaster._Field7_Caption
                    'SHANI (16 APR 2015)--END 
                    hdf_txtEmpField4Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField4FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField4FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField4tabpRemark3.Visible = False
                    objEmpField4tabpRemark3.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField4tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    'SHANI (16 APR 2015)-START
                    'lblEmpField4Remark1.Text = objFieldMaster._Field7_Caption
                    lblEmpField4Remark3.Text = objFieldMaster._Field7_Caption
                    'SHANI (16 APR 2015)--END 
                    hdf_txtEmpField4Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicEmpField4FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField4FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField4FieldData.Keys.Count > 0 Then objEmpField4tabcRemarks.Enabled = True
                pnl_EmpField4dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField4SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField4tabcRemarks.Enabled = False : pnl_RightEmpField4.Enabled = False : objEmpField4tabcRemarks.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField4GoalType.Enabled = pnl_RightEmpField4.Enabled
                txtEmpField4GoalValue.Enabled = pnl_RightEmpField4.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField4.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            Dim objEmp As New clsEmployee_Master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField4Employee.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField4Employee.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Set_EmpField4Form_Information :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField5Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField5.Text = Language.getMessage(mstrModuleName_Emp5, 1, "Add/Edit Employee") & " " & objFieldMaster._Field5_Caption & " " & _
                      Language.getMessage(mstrModuleName_Emp5, 2, "Information")

            objEmpField5lblEmpField1.Text = objFieldMaster._Field1_Caption
            objEmpField5lblEmpField2.Text = objFieldMaster._Field2_Caption
            objEmpField5lblEmpField3.Text = objFieldMaster._Field3_Caption
            objEmpField5lblEmpField4.Text = objFieldMaster._Field4_Caption
            hdf_cboEmpField5EmpFieldValue4.Value = objFieldMaster._Field4Unkid

            objEmpField5lblEmpField5.Text = objFieldMaster._Field5_Caption
            hdf_objEmpField5txtEmpField5.Value = objFieldMaster._Field5Unkid

            If CInt(Me.ViewState("mintEmpField5FieldUnkid")) = CInt(Me.ViewState("mintEmpField5LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField5tabpRemark1.Visible = False                    '
                    objEmpField5tabpRemark1.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField5tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    hdf_txtEmpField5Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField5FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField5FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField5tabpRemark2.Visible = False    
                    objEmpField5tabpRemark2.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField5tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    hdf_txtEmpField5Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField5FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField5FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objEmpField5tabpRemark3.Visible = False
                    objEmpField5tabpRemark3.Enabled = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else
                    objEmpField5tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    hdf_txtEmpField5Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicEmpField5FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField5FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField5FieldData.Keys.Count > 0 Then objEmpField5tabcRemarks.Enabled = True
                pnl_EmpField5dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField5SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField5tabcRemarks.Enabled = False : pnl_RightEmpField5.Enabled = False : objEmpField5tabcRemarks.Enabled = False
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                cboEmpField5GoalType.Enabled = pnl_RightEmpField5.Enabled
                txtEmpField5GoalValue.Enabled = pnl_RightEmpField5.Enabled
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                pnl_RightEmpField5.Visible = False
                'S.SANDEEP |12-FEB-2019| -- END
            End If

            Dim objEmp As New clsEmployee_Master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField5Employee.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField5Employee.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError("Set_EmpField5Form_Information :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField1()
        Dim objMData As New clsMasterData
        Dim objOwrField1 As New clsassess_owrfield1_master
        Dim dsList As New DataSet
        Try
            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField1Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            ''S.SANDEEP [05 DEC 2015] -- START
            ''dsList = objOwrField1.getComboList("List", True, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), , True)
            dsList = objOwrField1.getComboList(eZeeDate.convertDate(Session("EmployeeAsOnDate")), "List", True, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), , True)
            ''S.SANDEEP [05 DEC 2015] -- END
            'If Session("CompanyGroupName").ToUpper = "NMB PLC" Then
            '    Select Case CInt(Session("CascadingTypeId"))
            '        Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING
            '            If (New clsassess_empfield1_master).IsGoalOwner(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue)) Then
            '                dsList = objOwrField1.getComboList(eZeeDate.convertDate(Session("EmployeeAsOnDate")), "List", True, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), , True)
            '            Else
            '                dsList = (New clsassess_empfield1_master).GetOwnerAssignedGoals(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            '            End If
            '    End Select
            'Else
            'dsList = objOwrField1.getComboList(eZeeDate.convertDate(Session("EmployeeAsOnDate")), "List", True, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), , True)
            'End If
            'S.SANDEEP |27-NOV-2020| -- START

            With cboEmpField1FieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            If Session("CascadingTypeId") = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                'Shani [ 10 DEC 2014 ] -- START
                '
                'dsList = objMData.Get_BSC_Perspective("List", True)
                Dim objPerspective As New clsassess_perspective_master
                dsList = objPerspective.getComboList("List", True)
                'Shani [ 10 DEC 2014 ] -- END
                With cboEmpField1Perspective
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo_EmpField1 :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField2()
        Dim objMData As New clsMasterData
        Dim objEmpField1 As New clsassess_empfield1_master
        Dim dsList As New DataSet
        Try
            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True)
            With cboEmpField2EmpFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField2Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo_EmpField2 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField3()
        Dim objMData As New clsMasterData
        Dim objEmpField2 As New clsassess_empfield2_master
        Dim dsList As New DataSet
        Try
            dsList = objEmpField2.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField3MainParentId")), "List", True)
            With cboEmpField3EmpFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField3Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo_EmpField3 :- " & ex.Message, Me)

        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField4()
        Dim objMData As New clsMasterData
        Dim objEmpField3 As New clsassess_empfield3_master
        Dim dsList As New DataSet
        Try
            dsList = objEmpField3.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField4MainParentId")), "List", True)
            With cboEmpField4EmpFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField4Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo_EmpField4 :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField5()
        Dim objMData As New clsMasterData
        Dim objField4 As New clsassess_empfield4_master
        Dim dsList As New DataSet
        Try
            dsList = objField4.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Me.ViewState("mintEmpField5MainParentId"), "List", True)
            With cboEmpField5EmpFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField5Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo_EmpField5 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField1()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField1Owner As DataTable = CType(Me.ViewState("mdtEmpField1Owner"), DataTable)
        Try


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, CInt(cboPeriod.SelectedValue))
            dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, Session("Database_Name"), CInt(cboPeriod.SelectedValue))
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField1Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField1Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField1Owner.AutoGenerateColumns = False
            If txtEmpField1SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField1SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField1SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField1Owner.DataSource = dtOwnerView
            dgvEmpField1Owner.DataBind()
            Me.ViewState("mdtEmpField1Owner") = mdtEmpField1Owner
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Data_EmpField1 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField2()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField2Owner As DataTable = CType(Me.ViewState("mdtEmpField2Owner"), DataTable)
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, CInt(cboPeriod.SelectedValue))
            dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, Session("Database_Name"), CInt(cboPeriod.SelectedValue))
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField2Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField2Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField2Owner.AutoGenerateColumns = False
            If txtEmpField2SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField2SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField2SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvEmpField2Owner.DataSource = dtOwnerView
            dgvEmpField2Owner.DataBind()
            Me.ViewState("mdtEmpField2Owner") = mdtEmpField2Owner

        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Data_EmpField2 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField3()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField3Owner As DataTable = CType(Me.ViewState("mdtEmpField3Owner"), DataTable)
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, CInt(cboPeriod.SelectedValue))
            dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, Session("Database_Name"), CInt(cboPeriod.SelectedValue))
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField3Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField3Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField3Owner.AutoGenerateColumns = False
            If txtEmpField3SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField3SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField3SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField3Owner.DataSource = dtOwnerView
            dgvEmpField3Owner.DataBind()
            Me.ViewState("mdtEmpField3Owner") = mdtEmpField3Owner
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Data_EmpField3 :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField4()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField4Owner As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, CInt(cboPeriod.SelectedValue))
            dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, Session("Database_Name"), CInt(cboPeriod.SelectedValue))
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField4Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField4Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"

            dgvEmpField4Owner.AutoGenerateColumns = False
            If txtEmpField4SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField4SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField4SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField4Owner.DataSource = dtOwnerView
            dgvEmpField4Owner.DataBind()
            Me.ViewState("mdtEmpField4Owner") = mdtEmpField4Owner
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Data_EmpField4 :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField5()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField4Owner As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, CInt(cboPeriod.SelectedValue))
            dList = objEmployee.GetEmployee_Access(CInt(cboEmployee.SelectedValue), 0, Session("Database_Name"), CInt(cboPeriod.SelectedValue))
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    'S.SANDEEP |27-APR-2019| -- START
                    'If mdtEmpField4Owner.Rows.Count > 0 Then
                    '    Dim dRow As DataRow() = mdtEmpField4Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                    '    If dRow.Length > 0 Then
                    '        dtRow.Item("ischeck") = True
                    '    End If
                    'End If
                    If mdtEmpField4Owner IsNot Nothing Then
                        If mdtEmpField4Owner.Rows.Count > 0 Then
                            Dim dRow As DataRow() = mdtEmpField4Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dtRow.Item("ischeck") = True
                            End If
                        End If
                    End If
                    'S.SANDEEP |27-APR-2019| -- END
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField5Owner.AutoGenerateColumns = False
            If txtEmpField5SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField5SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField5SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField5Owner.DataSource = dtOwnerView
            dgvEmpField5Owner.DataBind()
            Me.ViewState("mdtEmpField4Owner") = mdtEmpField4Owner
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Data : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField1Value()
        Dim enEmpField1Action As enAction = CType(Me.ViewState("menEmpField1Action"), enAction)
        Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
        Try
            If objEmpField1._Enddate <> Nothing Then
                dtpEmpField1EndDate.SetDate = objEmpField1._Enddate
            End If
            txtEmpField1EmpField1.Text = objEmpField1._Field_Data
            objEmpField1._Isfinal = objEmpField1._Isfinal
            Try
                cboEmpField1FieldValue1.SelectedValue = objEmpField1._Owrfield1unkid
            Catch ex As Exception
                Throw ex
            End Try
            'Shani (16-Sep-2016) -- Start
            'Enhancement -
            Call cboEmpField1FieldValue1_SelectedIndexChanged(cboEmpField1FieldValue1, Nothing)
            'Shani (16-Sep-2016) -- End
            txtEmpField1Percent.Text = objEmpField1._Pct_Completed
            If objEmpField1._Startdate <> Nothing Then
                dtpEmpField1StartDate.SetDate = objEmpField1._Startdate
            End If
            cboEmpField1Status.SelectedValue = IIf(objEmpField1._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField1._Statusunkid)
            objEmpField1._Userunkid = Session("UserId")
            txtEmpField1Weight.Text = CDec(objEmpField1._Weight)
            If enEmpField1Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField1FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField1Unkid")), enWeight_Types.WEIGHT_FIELD1)
                If mdicEmpField1FieldData.Keys.Count > 0 Then
                    If mdicEmpField1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField1Remark1.Value = "", 0, hdf_lblEmpField1Remark1.Value))) Then
                        txtEmpField1Remark1.Text = mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark1.Value))
                    End If
                    If mdicEmpField1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField1Remark2.Value = "", 0, hdf_lblEmpField1Remark2.Value))) Then
                        txtEmpField1Remark2.Text = mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark2.Value))
                    End If
                    If mdicEmpField1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField1Remark3.Value = "", 0, hdf_lblEmpField1Remark3.Value))) Then
                        txtEmpField1Remark3.Text = mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
            If cboEmpField1Perspective.Enabled = True Then
                cboEmpField1Perspective.SelectedValue = objEmpField1._Perspectiveunkid
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}

            'S.SANDEEP |18-JAN-2019| -- START
            'cboEmpField1GoalType.SelectedValue = objEmpField1._GoalTypeid
            If cboEmpField1GoalType.SelectedValue = objEmpField1._GoalTypeid Then
                cboEmpField1GoalType.SelectedValue = objEmpField1._GoalTypeid
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            If enEmpField1Action <> enAction.EDIT_ONE Then
                If CInt(Session("GoalTypeInclusion")) > 0 Then
                    cboEmpField1GoalType.SelectedValue = CInt(Session("GoalTypeInclusion"))
                End If
            End If
            'S.SANDEEP |27-NOV-2020| -- END

            Call cboGoalType_SelectedIndexChanged(cboEmpField1GoalType, New EventArgs())
            txtEmpField1GoalValue.Text = objEmpField1._GoalValue
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboEmpField1UomType.SelectedValue = objEmpField1._UnitOfMeasure
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmpField1Value :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField2Value()
        Dim enEmpField2Action As enAction = CType(Me.ViewState("menEmpField2Action"), enAction)
        Dim mdicEmpFie2d1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField2EmpFieldValue1.SelectedValue = objEmpField2._Empfield1unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField2._Enddate <> Nothing Then
                dtpEmpField2EndDate.SetDate = objEmpField2._Enddate
            End If
            txtEmpField2EmpField2.Text = objEmpField2._Field_Data
            txtEmpField2Percent.Text = objEmpField2._Pct_Completed
            If objEmpField2._Startdate <> Nothing Then
                dtpEmpField2StartDate.SetDate = objEmpField2._Startdate
            End If
            cboEmpField2Status.SelectedValue = IIf(objEmpField2._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField2._Statusunkid)
            txtEmpField2Weight.Text = CDec(objEmpField2._Weight)
            If enEmpField2Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpFie2d1FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField2Unkid")), enWeight_Types.WEIGHT_FIELD2)
                If mdicEmpFie2d1FieldData.Keys.Count > 0 Then
                    If mdicEmpFie2d1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField2Remark1.Value = "", 0, hdf_lblEmpField2Remark1.Value))) Then
                        txtEmpField2Remark1.Text = mdicEmpFie2d1FieldData(CInt(hdf_lblEmpField2Remark1.Value))
                    End If
                    If mdicEmpFie2d1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField2Remark2.Value = "", 0, hdf_lblEmpField2Remark2.Value))) Then
                        txtEmpField2Remark2.Text = mdicEmpFie2d1FieldData(CInt(hdf_lblEmpField2Remark2.Value))
                    End If
                    If mdicEmpFie2d1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField2Remark3.Value = "", 0, hdf_lblEmpField2Remark3.Value))) Then
                        txtEmpField2Remark3.Text = mdicEmpFie2d1FieldData(CInt(hdf_lblEmpField2Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}

            'S.SANDEEP |18-JAN-2019| -- START
            'cboEmpField2GoalType.SelectedValue = objEmpField2._GoalTypeid
            If cboEmpField2GoalType.SelectedValue = objEmpField2._GoalTypeid Then
                cboEmpField2GoalType.SelectedValue = objEmpField2._GoalTypeid
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            If enEmpField2Action <> enAction.EDIT_ONE Then
                If CInt(Session("GoalTypeInclusion")) > 0 Then
                    cboEmpField2GoalType.SelectedValue = CInt(Session("GoalTypeInclusion"))
                End If
            End If
            'S.SANDEEP |27-NOV-2020| -- END

            Call cboGoalType_SelectedIndexChanged(cboEmpField2GoalType, New EventArgs())
            txtEmpField2GoalValue.Text = objEmpField2._GoalValue
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboEmpField2UomType.SelectedValue = objEmpField2._UnitOfMeasure
            'S.SANDEEP |12-FEB-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmpField2Value :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField3Value()
        Dim enEmpField3Action As enAction = CType(Me.ViewState("menEmpField3Action"), enAction)
        Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField3EmpFieldValue2.SelectedValue = objEmpField3._Empfield2unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField3._Enddate <> Nothing Then
                dtpEmpField3EndDate.SetDate = objEmpField3._Enddate
            End If
            txtEmpField3EmpField3.Text = objEmpField3._Field_Data
            txtEmpField3Percent.Text = objEmpField3._Pct_Completed
            If objEmpField3._Startdate <> Nothing Then
                dtpEmpField3StartDate.SetDate = objEmpField3._Startdate
            End If
            cboEmpField3Status.SelectedValue = IIf(objEmpField3._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField3._Statusunkid)
            txtEmpField3Weight.Text = CDec(objEmpField3._Weight)
            If enEmpField3Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField3FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField3Unkid")), enWeight_Types.WEIGHT_FIELD3)
                If mdicEmpField3FieldData.Keys.Count > 0 Then
                    If mdicEmpField3FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField3Remark1.Value = "", 0, hdf_lblEmpField3Remark1.Value))) Then
                        txtEmpField3Remark1.Text = mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark1.Value))
                    End If
                    If mdicEmpField3FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField3Remark2.Value = "", 0, hdf_lblEmpField3Remark2.Value))) Then
                        txtEmpField3Remark2.Text = mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark2.Value))
                    End If
                    If mdicEmpField3FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField3Remark3.Value = "", 0, hdf_lblEmpField3Remark3.Value))) Then
                        txtEmpField3Remark3.Text = mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}

            'S.SANDEEP |18-JAN-2019| -- START
            'cboEmpField3GoalType.SelectedValue = objEmpField3._GoalTypeid
            If cboEmpField3GoalType.SelectedValue = objEmpField3._GoalTypeid Then
                cboEmpField3GoalType.SelectedValue = objEmpField3._GoalTypeid
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            If enEmpField3Action <> enAction.EDIT_ONE Then
                If CInt(Session("GoalTypeInclusion")) > 0 Then
                    cboEmpField3GoalType.SelectedValue = CInt(Session("GoalTypeInclusion"))
                End If
            End If
            'S.SANDEEP |27-NOV-2020| -- END

            Call cboGoalType_SelectedIndexChanged(cboEmpField3GoalType, New EventArgs())
            txtEmpField3GoalValue.Text = objEmpField3._GoalValue
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboEmpField3UomType.SelectedValue = objEmpField3._UnitOfMeasure
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("GetEmpField3Value :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField4Value()
        Dim enEmpField4Action As enAction = CType(Me.ViewState("menEmpField4Action"), enAction)
        Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField4EmpFieldValue3.SelectedValue = objEmpField4._Empfield3unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField4._Enddate <> Nothing Then
                dtpEmpField4EndDate.SetDate = objEmpField4._Enddate
            End If
            objEmpField4txtEmpField4.Text = objEmpField4._Field_Data
            txtEmpField4Percent.Text = objEmpField4._Pct_Completed
            If objEmpField4._Startdate <> Nothing Then
                dtpEmpField4StartDate.SetDate = objEmpField4._Startdate
            End If
            cboEmpField4Status.SelectedValue = IIf(objEmpField4._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField4._Statusunkid)
            txtEmpField4Weight.Text = CDec(objEmpField4._Weight)
            If enEmpField4Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField4FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField4FieldUnkid")), enWeight_Types.WEIGHT_FIELD4)
                If mdicEmpField4FieldData.Keys.Count > 0 Then
                    If mdicEmpField4FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField4Remark1.Value = "", 0, hdf_txtEmpField4Remark1.Value))) Then
                        txtEmpField4Remark1.Text = mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark1.Value))
                    End If
                    If mdicEmpField4FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField4Remark2.Value = "", 0, hdf_txtEmpField4Remark2.Value))) Then
                        txtEmpField4Remark2.Text = mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark2.Value))
                    End If
                    If mdicEmpField4FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField4Remark3.Value = "", 0, hdf_txtEmpField4Remark3.Value))) Then
                        txtEmpField4Remark3.Text = mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}

            'S.SANDEEP |18-JAN-2019| -- START
            'cboEmpField4GoalType.SelectedValue = objEmpField4._GoalTypeid
            If cboEmpField4GoalType.SelectedValue = objEmpField4._GoalTypeid Then
                cboEmpField4GoalType.SelectedValue = objEmpField4._GoalTypeid
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            If enEmpField4Action <> enAction.EDIT_ONE Then
                If CInt(Session("GoalTypeInclusion")) > 0 Then
                    cboEmpField4GoalType.SelectedValue = CInt(Session("GoalTypeInclusion"))
                End If
            End If
            'S.SANDEEP |27-NOV-2020| -- END

            Call cboGoalType_SelectedIndexChanged(cboEmpField4GoalType, New EventArgs())
            txtEmpField4GoalValue.Text = objEmpField4._GoalValue
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboEmpField4UomType.SelectedValue = objEmpField4._UnitOfMeasure
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("GetValue : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField5Value()
        Dim enEmpField5Action As enAction = CType(Me.ViewState("menEmpField5Action"), enAction)
        Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField5EmpFieldValue4.SelectedValue = objEmpField5._Empfield4unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField5._Enddate <> Nothing Then
                dtpEmpField5EndDate.SetDate = objEmpField5._Enddate
            End If
            objEmpField5txtEmpField5.Text = objEmpField5._Field_Data
            txtEmpField5Percent.Text = objEmpField5._Pct_Completed
            If objEmpField5._Startdate <> Nothing Then
                dtpEmpField5StartDate.SetDate = objEmpField5._Startdate
            End If
            cboEmpField5Status.SelectedValue = IIf(objEmpField5._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField5._Statusunkid)
            txtEmpField5Weight.Text = CDec(objEmpField5._Weight)
            If enEmpField5Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField5FieldData = objInfoField.Get_Data(Me.ViewState("mintEmpField5Unkid"), enWeight_Types.WEIGHT_FIELD5)
                If mdicEmpField5FieldData.Keys.Count > 0 Then
                    If mdicEmpField5FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField5Remark1.Value = "", 0, hdf_txtEmpField5Remark1.Value))) Then
                        txtEmpField5Remark1.Text = mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark1.Value))
                    End If
                    If mdicEmpField5FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField5Remark2.Value = "", 0, hdf_txtEmpField5Remark2.Value))) Then
                        txtEmpField5Remark2.Text = mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark2.Value))
                    End If
                    If mdicEmpField5FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField5Remark3.Value = "", 0, hdf_txtEmpField5Remark3.Value))) Then
                        txtEmpField5Remark3.Text = mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}

            'S.SANDEEP |18-JAN-2019| -- START
            'cboEmpField5GoalType.SelectedValue = objEmpField5._GoalTypeid
            If cboEmpField5GoalType.SelectedValue = objEmpField5._GoalTypeid Then
                cboEmpField5GoalType.SelectedValue = objEmpField5._GoalTypeid
            End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            If enEmpField5Action <> enAction.EDIT_ONE Then
                If CInt(Session("GoalTypeInclusion")) > 0 Then
                    cboEmpField5GoalType.SelectedValue = CInt(Session("GoalTypeInclusion"))
                End If
            End If
            'S.SANDEEP |27-NOV-2020| -- END

            Call cboGoalType_SelectedIndexChanged(cboEmpField5GoalType, New EventArgs())
            txtEmpField5GoalValue.Text = objEmpField5._GoalValue
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            cboEmpField5UomType.SelectedValue = objEmpField5._UnitOfMeasure
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("GeEmpField5tValue :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidData_EmpField1() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField1Action As enAction = CType(Me.ViewState("menEmpField1Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            Select Case Session("CascadingTypeId")
                Case enPACascading.STRICT_CASCADING, enPACascading.STRICT_GOAL_ALIGNMENT
                    If CInt(cboEmpField1FieldValue1.SelectedValue) <= 0 Then
                        iMsg = Language.getMessage(mstrModuleName_Emp1, 5, "Sorry Owner, ") & objFieldMaster._Field1_Caption & _
                               Language.getMessage(mstrModuleName_Emp1, 6, " is mandatory information. Please select ") & objFieldMaster._Field1_Caption & _
                               Language.getMessage(mstrModuleName_Emp1, 7, " to continue.")
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        cboFieldValue1.Focus()
                        Return False
                    End If
            End Select

            If txtEmpField1EmpField1.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp1, 8, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName_Emp1, 9, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName_Emp1, 7, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                txtEmpField1EmpField1.Focus()
                Return False
            End If

            Select Case Session("CascadingTypeId")
                Case enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT, enPACascading.LOOSE_CASCADING, enPACascading.LOOSE_GOAL_ALIGNMENT
                    If CInt(cboEmpField1FieldValue1.SelectedValue) <= 0 Then
                        'Shani [ 10 DEC 2014 ] -- START
                        '
                        'If CInt(cboPerspective.SelectedValue) <= 0 Then

                        'S.SANDEEP [10 DEC 2015] -- START
                        If CInt(IIf(cboEmpField1Perspective.SelectedValue = "", 0, cboEmpField1Perspective.SelectedValue)) <= 0 Then
                            'If CInt(cboEmpField1Perspective.SelectedValue) <= 0 Then
                            'S.SANDEEP [10 DEC 2015] -- END

                            'Shani [ 10 DEC 2014 ] -- END
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 16, "Sorry, Perspective is mandatory information. Please select Perspective to continue."), Me)
                            cboPerspective.Focus()
                            Return False
                        End If
                    End If
            End Select


            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtEmpField1Weight.Text.Trim.Length <= 0 Then txtEmpField1Weight.Text = "0"
            If txtEmpField1Percent.Text.Trim.Length <= 0 Then txtEmpField1Percent.Text = "0"
            'Pinkal (15-Mar-2019) -- End



            If CInt(Me.ViewState("mintEmpField1FieldUnkid")) = CInt(Me.ViewState("mintEmpField1LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField1Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 11, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField1Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD1, iWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField1LinkedFieldId")), 0, CInt(cboEmployee.SelectedValue), enEmpField1Action, CInt(Me.ViewState("mintEmpField1Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField1Weight.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |24-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : {Extra Validation Removed}
                'iWeight = 0 : Decimal.TryParse(txtEmpField1Percent.Text, iWeight)
                'If iWeight > 100 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 18, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                '    txtEmpField1Percent.Focus()
                '    Return False
                'End If
                'S.SANDEEP |24-JUN-2019| -- END

                If CInt(cboEmpField1Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField1Status.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            If pnl_RightEmpField1.Visible = True Then
                If CInt(IIf(cboEmpField1GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField1GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    'S.SANDEEP |11-APR-2019| -- START
                    Dim objValue As Decimal = 0
                    Decimal.TryParse(txtEmpField1GoalValue.Text, objValue)
                    'If txtEmpField1GoalValue.Enabled = True AndAlso CDec(txtEmpField1GoalValue.Text) <= 0 Then
                    If txtEmpField1GoalValue.Enabled = True AndAlso objValue <= 0 Then
                        'S.SANDEEP |11-APR-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue."), Me)
                        txtEmpField1GoalValue.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-APR-2019| -- START
                'If cboEmpField1UomType.Enabled = True AndAlso CInt(IIf(cboEmpField1UomType.SelectedValue = "", 0, cboEmpField1UomType.SelectedValue)) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                '    cboEmpField1UomType.Focus()
                '    Return False
                'End If
                If CInt(IIf(cboEmpField1GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField1GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    If cboEmpField1UomType.Enabled = True AndAlso CInt(IIf(cboEmpField1UomType.SelectedValue = "", 0, cboEmpField1UomType.SelectedValue)) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                        cboEmpField1UomType.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |05-APR-2019| -- END

            End If
            'S.SANDEEP |12-FEB-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidData_EmpField1 :-" & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField2() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField2Action As enAction = CType(Me.ViewState("menEmpField2Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField2EmpFieldValue1.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp2, 3, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName_Emp2, 4, " is mandatory information. Please select ") & objFieldMaster._Field1_Caption & _
                       Language.getMessage(mstrModuleName_Emp2, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField2EmpFieldValue1.Focus()
                Return False
            End If

            If txtEmpField2EmpField2.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp2, 3, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Language.getMessage(mstrModuleName_Emp2, 6, " is mandatory information. Please provide ") & objFieldMaster._Field2_Caption & _
                       Language.getMessage(mstrModuleName_Emp2, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                txtEmpField2EmpField2.Focus()
                Return False
            End If

            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtEmpField2Weight.Text.Trim.Length <= 0 Then txtEmpField2Weight.Text = "0"
            If txtEmpField2Percent.Text.Trim.Length <= 0 Then txtEmpField2Percent.Text = "0"
            'Pinkal (15-Mar-2019) -- End

            If CInt(Me.ViewState("mintEmpField2FieldUnkid")) = CInt(Me.ViewState("mintEmpField2LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField2Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 8, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField2Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD2, iWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField2LinkedFieldId")), 0, CInt(cboEmployee.SelectedValue), enEmpField2Action, Me.ViewState("mintEmpField2Unkid"))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField2Weight.Focus()
                        Return False
                    End If
                End If

                'S.SANDEEP |24-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : {Extra Validation Removed}
                'iWeight = 0 : Decimal.TryParse(txtEmpField2Percent.Text, iWeight)
                'If iWeight > 100 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                '    txtEmpField2Percent.Focus()
                '    Return False
                'End If
                'S.SANDEEP |24-JUN-2019| -- END

                If CInt(cboEmpField2Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 7, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField2Status.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            If pnl_RightEmpField2.Visible = True Then
                If CInt(IIf(cboEmpField2GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField2GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    'S.SANDEEP |11-APR-2019| -- START
                    Dim objValue As Decimal = 0
                    Decimal.TryParse(txtEmpField2GoalValue.Text, objValue)
                    'If txtEmpField2GoalValue.Enabled = True AndAlso CDec(txtEmpField2GoalValue.Text) <= 0 Then
                    If txtEmpField2GoalValue.Enabled = True AndAlso objValue <= 0 Then
                        'S.SANDEEP |11-APR-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue."), Me)
                        txtEmpField2GoalValue.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-APR-2019| -- START
                'If cboEmpField2UomType.Enabled = True AndAlso CInt(IIf(cboEmpField2UomType.SelectedValue = "", 0, cboEmpField2UomType.SelectedValue)) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                '    cboEmpField2UomType.Focus()
                '    Return False
                'End If
                If CInt(IIf(cboEmpField2GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField2GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    If cboEmpField2UomType.Enabled = True AndAlso CInt(IIf(cboEmpField2UomType.SelectedValue = "", 0, cboEmpField2UomType.SelectedValue)) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                        cboEmpField2UomType.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |05-APR-2019| -- END

            End If
            'S.SANDEEP |12-FEB-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidData_EmpField2 :-" & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField3() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField3Action As enAction = CType(Me.ViewState("menEmpField3Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField3EmpFieldValue2.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp3, 3, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Language.getMessage(mstrModuleName_Emp3, 4, " is mandatory information. Please select ") & objFieldMaster._Field2_Caption & _
                       Language.getMessage(mstrModuleName_Emp3, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField3EmpFieldValue2.Focus()
                Return False
            End If

            If txtEmpField3EmpField3.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp3, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName_Emp3, 6, " is mandatory information. Please provide ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName_Emp3, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                txtEmpField3EmpField3.Focus()
                Return False
            End If

            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtEmpField3Weight.Text.Trim.Length <= 0 Then txtEmpField3Weight.Text = "0"
            If txtEmpField3Percent.Text.Trim.Length <= 0 Then txtEmpField3Percent.Text = "0"
            'Pinkal (15-Mar-2019) -- End

            If CInt(Me.ViewState("mintEmpField3FieldUnkid")) = CInt(Me.ViewState("mintEmpField3LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                'SHANI (14 May 2015) -- Start
                'Decimal.TryParse(txtEmpField3Weight.Text, 0)
                Decimal.TryParse(txtEmpField3Weight.Text, iWeight)
                'SHANI (14 May 2015) -- End
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 8, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField3Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD3, iWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField3LinkedFieldId")), 0, CInt(cboEmployee.SelectedValue), enEmpField3Action, CInt(Me.ViewState("mintEmpField3Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField3Weight.Focus()
                        Return False
                    End If
                End If

                'S.SANDEEP |24-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : {Extra Validation Removed}
                'iWeight = 0 : Decimal.TryParse(txtEmpField3Percent.Text, 0)
                'If iWeight > 100 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                '    txtEmpField3Percent.Focus()
                '    Return False
                'End If
                'S.SANDEEP |24-JUN-2019| -- END

                If CInt(cboEmpField3Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 7, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField3Status.Focus()
                    Return False
                End If
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            If pnl_RightEmpField3.Visible = True Then
                If CInt(IIf(cboEmpField3GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField3GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    'S.SANDEEP |11-APR-2019| -- START
                    Dim objValue As Decimal = 0
                    Decimal.TryParse(txtEmpField3GoalValue.Text, objValue)
                    'If txtEmpField3GoalValue.Enabled = True AndAlso CDec(txtEmpField3GoalValue.Text) <= 0 Then
                    If txtEmpField3GoalValue.Enabled = True AndAlso objValue <= 0 Then
                        'S.SANDEEP |11-APR-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue."), Me)
                        txtEmpField3GoalValue.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-APR-2019| -- START
                'If cboEmpField3UomType.Enabled = True AndAlso CInt(IIf(cboEmpField3UomType.SelectedValue = "", 0, cboEmpField3UomType.SelectedValue)) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                '    cboEmpField3UomType.Focus()
                '    Return False
                'End If
                If CInt(IIf(cboEmpField3GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField3GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    If cboEmpField3UomType.Enabled = True AndAlso CInt(IIf(cboEmpField3UomType.SelectedValue = "", 0, cboEmpField3UomType.SelectedValue)) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                        cboEmpField3UomType.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |05-APR-2019| -- END
            End If
            'S.SANDEEP |12-FEB-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidData_EmpField3 :- " & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField4() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField4Action As enAction = CType(Me.ViewState("menEmpField4Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField4EmpFieldValue3.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp4, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName_Emp4, 4, " is mandatory information. Please select ") & objFieldMaster._Field3_Caption & _
                       Language.getMessage(mstrModuleName_Emp4, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField4EmpFieldValue3.Focus()
                Return False
            End If

            If objEmpField4txtEmpField4.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp4, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName_Emp4, 6, " is mandatory information. Please provide ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName_Emp4, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objEmpField4txtEmpField4.Focus()
                Return False
            End If

            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtEmpField4Weight.Text.Trim.Length <= 0 Then txtEmpField4Weight.Text = "0"
            If txtEmpField4Percent.Text.Trim.Length <= 0 Then txtEmpField4Percent.Text = "0"
            'Pinkal (15-Mar-2019) -- End

            If CInt(Me.ViewState("mintEmpField4FieldUnkid")) = CInt(Me.ViewState("mintEmpField4LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField4Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField4Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD4, iWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField4LinkedFieldId")), 0, CInt(cboEmployee.SelectedValue), enEmpField4Action, CInt(Me.ViewState("mintEmpField4Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField4Weight.Focus()
                        Return False
                    End If
                End If

                'S.SANDEEP |24-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : {Extra Validation Removed}
                'iWeight = 0 : Decimal.TryParse(txtEmpField4Percent.Text, iWeight)
                'If iWeight > 100 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                '    txtEmpField4Percent.Focus()
                '    Return False
                'End If
                'S.SANDEEP |24-JUN-2019| -- END

                If CInt(cboEmpField4Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 8, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField4Status.Focus()
                    Return False
                End If
            End If

            If pnl_RightEmpField4.Visible = True Then
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                If CInt(IIf(cboEmpField4GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField4GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    'S.SANDEEP |11-APR-2019| -- START
                    Dim objValue As Decimal = 0
                    Decimal.TryParse(txtEmpField4GoalValue.Text, objValue)
                    'If txtEmpField4GoalValue.Enabled = True AndAlso CDec(txtEmpField4GoalValue.Text) <= 0 Then
                    If txtEmpField4GoalValue.Enabled = True AndAlso objValue <= 0 Then
                        'S.SANDEEP |11-APR-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue."), Me)
                        txtEmpField4GoalValue.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-APR-2019| -- START
                'If cboEmpField4UomType.Enabled = True AndAlso CInt(IIf(cboEmpField4UomType.SelectedValue = "", 0, cboEmpField4UomType.SelectedValue)) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                '    cboEmpField4UomType.Focus()
                '    Return False
                'End If
                If CInt(IIf(cboEmpField4GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField4GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    If cboEmpField4UomType.Enabled = True AndAlso CInt(IIf(cboEmpField4UomType.SelectedValue = "", 0, cboEmpField4UomType.SelectedValue)) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                        cboEmpField4UomType.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |05-APR-2019| -- END


                'S.SANDEEP |12-FEB-2019| -- END
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidData_EmpField4 :- " & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField5() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim menEmpField5Action As enAction = CType(Me.ViewState("menEmpField5Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField5EmpFieldValue4.SelectedValue) <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp5, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName_Emp5, 4, " is mandatory information. Please select ") & objFieldMaster._Field4_Caption & _
                       Language.getMessage(mstrModuleName_Emp5, 5, " to continue.")
                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField5EmpFieldValue4.Focus()
                Return False
            End If

            If objEmpField5txtEmpField5.Text.Trim.Length <= 0 Then
                iMsg = Language.getMessage(mstrModuleName_Emp5, 3, "Sorry, ") & objFieldMaster._Field5_Caption & _
                       Language.getMessage(mstrModuleName_Emp5, 6, " is mandatory information. Please provide ") & objFieldMaster._Field5_Caption & _
                       Language.getMessage(mstrModuleName_Emp5, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objEmpField5txtEmpField5.Focus()
                Return False
            End If

            'Pinkal (15-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtEmpField5Weight.Text.Trim.Length <= 0 Then txtEmpField5Weight.Text = "0"
            If txtEmpField5Percent.Text.Trim.Length <= 0 Then txtEmpField5Percent.Text = "0"
            'Pinkal (15-Mar-2019) -- End

            If CInt(Me.ViewState("mintEmpField5FieldUnkid")) = CInt(Me.ViewState("mintEmpField5LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField5Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField5Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD5, iWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField5LinkedFieldId")), 0, CInt(cboEmployee.SelectedValue), menEmpField5Action, CInt(Me.ViewState("mintEmpField5Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField5Weight.Focus()
                        Return False
                    End If
                End If

                'S.SANDEEP |24-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : {Extra Validation Removed}
                'iWeight = 0 : Decimal.TryParse(txtEmpField5Percent.Text, iWeight)
                'If iWeight > 100 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                '    txtEmpField5Percent.Focus()
                '    Return False
                'End If
                'S.SANDEEP |24-JUN-2019| -- END

                If CInt(cboEmpField5Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 8, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField5Status.Focus()
                    Return False
                End If
            End If

            If pnl_RightEmpField5.Visible = True Then
                'S.SANDEEP [01-OCT-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
                If CInt(IIf(cboEmpField5GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField5GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    'S.SANDEEP |11-APR-2019| -- START
                    Dim objValue As Decimal = 0
                    Decimal.TryParse(txtEmpField5GoalValue.Text, objValue)
                    'If txtEmpField5GoalValue.Enabled = True AndAlso CDec(txtEmpField5GoalValue.Text) <= 0 Then
                    If txtEmpField5GoalValue.Enabled = True AndAlso objValue <= 0 Then
                        'S.SANDEEP |11-APR-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 100, "Sorry, Goal Value is mandatory information. Please set goal value to continue."), Me)
                        txtEmpField5GoalValue.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP [01-OCT-2018] -- END

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                'S.SANDEEP |05-APR-2019| -- START
                'If cboEmpField5UomType.Enabled = True AndAlso CInt(IIf(cboEmpField5UomType.SelectedValue = "", 0, cboEmpField5UomType.SelectedValue)) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                '    cboEmpField5UomType.Focus()
                '    Return False
                'End If
                If CInt(IIf(cboEmpField5GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField5GoalType.SelectedValue)) = enGoalType.GT_QUANTITATIVE Then
                    If cboEmpField5UomType.Enabled = True AndAlso CInt(IIf(cboEmpField5UomType.SelectedValue = "", 0, cboEmpField5UomType.SelectedValue)) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 101, "Sorry, Unit of Measure is mandatory information. Please select unit of measure to continue."), Me)
                        cboEmpField5UomType.Focus()
                        Return False
                    End If
                End If
                'S.SANDEEP |05-APR-2019| -- END


                'S.SANDEEP |12-FEB-2019| -- END
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidData_EmpField5 :-" & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub SetEmpField1Value()
        Try
            objEmpField1._Empfield1unkid = CInt(Me.ViewState("mintEmpField1Unkid"))
            objEmpField1._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD1
            objEmpField1._Employeeunkid = CInt(cboEmployee.SelectedValue)
            If dtpEmpField1EndDate.IsNull = False Then
                objEmpField1._Enddate = dtpEmpField1EndDate.GetDate
            End If
            objEmpField1._Field_Data = txtEmpField1EmpField1.Text
            objEmpField1._Fieldunkid = CInt(Me.ViewState("mintEmpField1FieldUnkid"))
            objEmpField1._Isfinal = False
            objEmpField1._Isvoid = False
            objEmpField1._Owrfield1unkid = CInt(cboEmpField1FieldValue1.SelectedValue)
            objEmpField1._Pct_Completed = txtEmpField1Percent.Text
            objEmpField1._Periodunkid = CInt(cboPeriod.SelectedValue)
            If dtpEmpField1StartDate.IsNull = False Then
                objEmpField1._Startdate = dtpEmpField1StartDate.GetDate
            End If
            objEmpField1._Statusunkid = CInt(cboEmpField1Status.SelectedValue)
            objEmpField1._Userunkid = Session("UserId")
            objEmpField1._Voiddatetime = Nothing
            objEmpField1._Voidreason = ""
            objEmpField1._Voiduserunkid = -1
            objEmpField1._Weight = txtEmpField1Weight.Text
            objEmpField1._Yearunkid = 0
            If cboEmpField1Perspective.Enabled = True AndAlso CStr(cboEmpField1Perspective.SelectedValue) <> "" Then
                If CInt(cboEmpField1Perspective.SelectedValue) > 0 Then
                    objEmpField1._Perspectiveunkid = CInt(cboEmpField1Perspective.SelectedValue)
                End If
            End If

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objEmpField1._GoalTypeid = CInt(IIf(cboEmpField1GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField1GoalType.SelectedValue))
            objEmpField1._GoalValue = CDbl(IIf(txtEmpField1GoalValue.Text.Trim = "", 0, txtEmpField1GoalValue.Text))
            'S.SANDEEP [01-OCT-2018] -- END


            'S.SANDEEP [05 DEC 2015] -- START

            'S.SANDEEP [10 DEC 2015] -- START
            'If Session("SkipApprovalFlowInPlanning") = True Then
            '    objEmpField1._AssessorMasterId = mintAssessorMasterId
            '    objEmpField1._AssessorEmployeeId = mintAssessorEmployeeId
            '    objEmpField1._SkipApprovalFlow = Session("SkipApprovalFlowInPlanning")
            '    objEmpField1._StatusTypeId = enObjective_Status.FINAL_SAVE
            'End If
            'S.SANDEEP [10 DEC 2015] -- END

            'S.SANDEEP [05 DEC 2015] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objEmpField1._UnitOfMeasure = CInt(IIf(cboEmpField1UomType.SelectedValue = "", 0, cboEmpField1UomType.SelectedValue))
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("SeEmpField1tValue :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEmpField2Value()
        Try
            objEmpField2._Empfield2unkid = Me.ViewState("mintEmpField2Unkid")
            objEmpField2._Empfield1unkid = CInt(cboEmpField2EmpFieldValue1.SelectedValue)
            objEmpField2._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD2
            If dtpEmpField2EndDate.IsNull = False Then
                objEmpField2._Enddate = dtpEmpField2EndDate.GetDate
            End If
            objEmpField2._Field_Data = txtEmpField2EmpField2.Text
            objEmpField2._Fieldunkid = Me.ViewState("mintEmpField2FieldUnkid")
            objEmpField2._Isvoid = False
            objEmpField2._Pct_Completed = txtEmpField2Percent.Text
            If dtpEmpField2StartDate.IsNull = False Then
                objEmpField2._Startdate = dtpEmpField2StartDate.GetDate
            End If
            objEmpField2._Statusunkid = CInt(cboEmpField2Status.SelectedValue)
            objEmpField2._Userunkid = Session("UserId")
            objEmpField2._Voiddatetime = Nothing
            objEmpField2._Voidreason = ""
            objEmpField2._Voiduserunkid = -1
            objEmpField2._Weight = txtEmpField2Weight.Text
            objEmpField2._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmpField2._Periodunkid = CInt(cboPeriod.SelectedValue)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objEmpField2._GoalTypeid = CInt(IIf(cboEmpField2GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField2GoalType.SelectedValue))
            objEmpField2._GoalValue = CDbl(IIf(txtEmpField2GoalValue.Text.Trim = "", 0, txtEmpField2GoalValue.Text))
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objEmpField2._UnitOfMeasure = CInt(IIf(cboEmpField2UomType.SelectedValue = "", 0, cboEmpField2UomType.SelectedValue))
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("SetEmpField2Value :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEmpField3Value()
        Try
            objEmpField3._Empfield3unkid = Me.ViewState("mintEmpField3Unkid")
            objEmpField3._Empfield2unkid = CInt(cboEmpField3EmpFieldValue2.SelectedValue)
            objEmpField3._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD3
            If dtpEmpField3EndDate.IsNull = False Then
                objEmpField3._Enddate = dtpEmpField3EndDate.GetDate
            End If
            objEmpField3._Field_Data = txtEmpField3EmpField3.Text
            objEmpField3._Fieldunkid = CInt(Me.ViewState("mintEmpField3FieldUnkid"))
            objEmpField3._Isvoid = False
            objEmpField3._Pct_Completed = txtEmpField3Percent.Text
            If dtpEmpField3StartDate.IsNull = False Then
                objEmpField3._Startdate = dtpEmpField3StartDate.GetDate
            End If
            objEmpField3._Statusunkid = CInt(cboEmpField3Status.SelectedValue)
            objEmpField3._Userunkid = Session("UserId")
            objEmpField3._Voiddatetime = Nothing
            objEmpField3._Voidreason = ""
            objEmpField3._Voiduserunkid = -1
            objEmpField3._Weight = txtEmpField3Weight.Text
            objEmpField3._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmpField3._Periodunkid = CInt(cboPeriod.SelectedValue)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objEmpField3._GoalTypeid = CInt(IIf(cboEmpField3GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField3GoalType.SelectedValue))
            objEmpField3._GoalValue = CDbl(IIf(txtEmpField3GoalValue.Text.Trim = "", 0, txtEmpField3GoalValue.Text))
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objEmpField3._UnitOfMeasure = CInt(IIf(cboEmpField3UomType.SelectedValue = "", 0, cboEmpField3UomType.SelectedValue))
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("SetEmpField3Value :- " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEmpField4Value()
        Try
            objEmpField4._Empfield4unkid = Me.ViewState("mintEmpField4Unkid")
            objEmpField4._Empfield3unkid = CInt(cboEmpField4EmpFieldValue3.SelectedValue)
            objEmpField4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
            If dtpEmpField4EndDate.IsNull = False Then
                objEmpField4._Enddate = dtpEmpField4EndDate.GetDate
            Else
                objEmpField4._Enddate = Nothing
            End If
            objEmpField4._Field_Data = objEmpField4txtEmpField4.Text
            objEmpField4._Fieldunkid = CInt(Me.ViewState("mintEmpField4FieldUnkid"))
            objEmpField4._Isvoid = False
            objEmpField4._Pct_Completed = txtEmpField4Percent.Text
            If dtpEmpField4StartDate.IsNull = False Then
                objEmpField4._Startdate = dtpEmpField4StartDate.GetDate
            Else
                objEmpField4._Startdate = Nothing
            End If
            objEmpField4._Statusunkid = CInt(cboEmpField4Status.SelectedValue)
            objEmpField4._Userunkid = Session("UserId")
            objEmpField4._Voiddatetime = Nothing
            objEmpField4._Voidreason = ""
            objEmpField4._Voiduserunkid = -1
            objEmpField4._Weight = txtEmpField4Weight.Text
            objEmpField4._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmpField4._Periodunkid = CInt(cboPeriod.SelectedValue)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objEmpField4._GoalTypeid = CInt(IIf(cboEmpField4GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField4GoalType.SelectedValue))
            objEmpField4._GoalValue = CDbl(IIf(txtEmpField4GoalValue.Text.Trim = "", 0, txtEmpField4GoalValue.Text))
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objEmpField4._UnitOfMeasure = CInt(IIf(cboEmpField4UomType.SelectedValue = "", 0, cboEmpField4UomType.SelectedValue))
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("SetEmpField4Value :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEmpField5Value()
        Try
            objEmpField5._Empfield5unkid = Me.ViewState("mintEmpField5Unkid")
            objEmpField5._Empfield4unkid = CInt(cboEmpField5EmpFieldValue4.SelectedValue)
            objEmpField5._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            If dtpEmpField5EndDate.IsNull = False Then
                objEmpField5._Enddate = dtpEmpField5EndDate.GetDate
            Else
                objEmpField5._Enddate = Nothing
            End If
            objEmpField5._Field_Data = objEmpField5txtEmpField5.Text
            objEmpField5._Fieldunkid = CInt(Me.ViewState("mintEmpField5FieldUnkid"))
            objEmpField5._Isvoid = False
            objEmpField5._Pct_Completed = txtEmpField5Percent.Text
            If dtpEmpField5StartDate.IsNull = False Then
                objEmpField5._Startdate = dtpEmpField5StartDate.GetDate
            Else
                objEmpField5._Startdate = Nothing
            End If
            objEmpField5._Statusunkid = CInt(cboEmpField5Status.SelectedValue)
            objEmpField5._Userunkid = Session("UserId")
            objEmpField5._Voiddatetime = Nothing
            objEmpField5._Voidreason = ""
            objEmpField5._Voiduserunkid = -1
            objEmpField5._Weight = txtEmpField5Weight.Text
            objEmpField5._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmpField5._Periodunkid = CInt(cboPeriod.SelectedValue)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objEmpField5._GoalTypeid = CInt(IIf(cboEmpField5GoalType.SelectedValue = "", enGoalType.GT_QUALITATIVE, cboEmpField5GoalType.SelectedValue))
            objEmpField5._GoalValue = CDbl(IIf(txtEmpField5GoalValue.Text.Trim = "", 0, txtEmpField5GoalValue.Text))
            'S.SANDEEP [01-OCT-2018] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            objEmpField5._UnitOfMeasure = CInt(IIf(cboEmpField5UomType.SelectedValue = "", 0, cboEmpField5UomType.SelectedValue))
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("SetEmpField5Value" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalEmpOperation_Emp1(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField1Owner"), DataTable)
            If mdtEmp IsNot Nothing Then
                Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtEmp.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField1Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtEmp.Rows.Add(dRow)
                    End If
                    mdtEmp.AcceptChanges()
                    Me.ViewState("mdtEmpField1Owner") = mdtEmp
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GoalEmpOperation_Emp1 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalEmpOperation_Emp2(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField2Owner"), DataTable)
            If mdtEmp IsNot Nothing Then
                Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtEmp.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField2Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtEmp.Rows.Add(dRow)
                    End If
                    mdtEmp.AcceptChanges()
                    Me.ViewState("mdtEmpField2Owner") = mdtEmp
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GoalEmpOperation_Emp2 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalEmpOperation_Emp3(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField3Owner"), DataTable)
            If mdtEmp IsNot Nothing Then
                Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtEmp.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField3Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtEmp.Rows.Add(dRow)
                    End If
                    mdtEmp.AcceptChanges()
                    Me.ViewState("mdtEmpField3Owner") = mdtEmp
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GoalEmpOperation_Emp3 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalEmpOperation_Emp4(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
            If mdtEmp IsNot Nothing Then
                Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtEmp.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField4Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtEmp.Rows.Add(dRow)
                    End If
                    mdtEmp.AcceptChanges()
                    Me.ViewState("mdtEmpField4Owner") = mdtEmp
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GoalEmpOperation_Emp4 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalEmpOperation_Emp5(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField5Owner"), DataTable)
            If mdtEmp IsNot Nothing Then
                Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtEmp.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField5Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtEmp.Rows.Add(dRow)
                    End If
                    mdtEmp.AcceptChanges()
                    Me.ViewState("mdtEmpField5Owner") = mdtEmp
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GoalEmpOperation_Emp5 :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Private Sub ApproverReject_load(ByVal strName As String)
    '    Try
    '        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '        Dim objFMaster = New clsAssess_Field_Master(True)
    '        objlblCaption.Text = strName
    '        dgvAppRejdata.AutoGenerateColumns = False
    '        Dim iColName As String = String.Empty

    '        Dim iPlan() As String = Nothing
    '        If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
    '            iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
    '        End If

    '        For Each dCol As DataColumn In mdtFinal.Columns
    '            iColName = "" : iColName = "obj" & dCol.ColumnName
    '            Dim dgvCol As New BoundField()
    '            dgvCol.FooterText = iColName
    '            dgvCol.ReadOnly = True
    '            dgvCol.DataField = dCol.ColumnName
    '            dgvCol.HeaderText = dCol.Caption
    '            If dgvAppRejdata.Columns.Contains(dgvCol) = True Then Continue For
    '            If dCol.Caption.Length <= 0 Then
    '                dgvCol.Visible = False
    '            End If

    '            'S.SANDEEP [ 16 JAN 2015 ] -- START
    '            If dCol.ColumnName = "Emp" Then
    '                dgvCol.Visible = False
    '            End If
    '            'S.SANDEEP [ 16 JAN 2015 ] -- END

    '            If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
    '                If dgvCol.DataField.StartsWith("Owr") Then
    '                    dgvCol.Visible = False
    '                End If
    '            End If

    '            If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
    '                dgvCol.Visible = False
    '            Else
    '                If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
    '                    'S.SANDEEP |15-MAR-2019| -- START
    '                    'dgvCol.HeaderStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
    '                    'dgvCol.ItemStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
    '                    dgvCol.HeaderStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompanyUnkid")))
    '                    dgvCol.ItemStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompanyUnkid")))
    '                    'S.SANDEEP |15-MAR-2019| -- END

    '                    If iPlan IsNot Nothing Then
    '                        If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
    '                            dgvCol.Visible = False
    '                        End If
    '                    End If
    '                End If
    '            End If

    '            dgvAppRejdata.Columns.Add(dgvCol)
    '        Next
    '        dgvAppRejdata.DataSource = mdtFinal
    '        dgvAppRejdata.DataBind()

    '        Dim iFinal As Boolean = False
    '        If CBool(mdtFinal.Rows(0)("isfinal")) = True Then
    '            btnApRejFinalSave.Enabled = False
    '        Else
    '            btnApRejFinalSave.Enabled = True
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("ApproverReject_load :-" & ex.Message, Me)
    '    End Try
    'End Sub

    Private Function Get_Assessor(ByRef iAssessorEmpId As Integer, ByRef sAssessorName As String) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet
            'Shani(01-MAR-2016) -- Start
            'Enhancement
            'Dim objBSC As New clsBSC_Analysis_Master
            'dLst = objBSC.getAssessorComboList("List", False, False, CInt(Session("Userid")))
            Dim objBSC As New clsevaluation_analysis_master
            dLst = objBSC.getAssessorComboList(CStr(Session("Database_Name")), _
                                               CInt(Session("Userid")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               True, True, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

            'Shani(14-FEB-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
            'Shani(01-MAR-2016) -- End

            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                iAssessorEmpId = objBSC.GetAssessorEmpId(iAssessor)
                sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
            End If
            objBSC = Nothing
            Return iAssessor
        Catch ex As Exception
            DisplayMessage.DisplayError("Get_Assessor :-" & ex.Message, Me)
        End Try
    End Function

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisibility()
        Try

            'S.SANDEEP [10 DEC 2015] -- START
            'btnGlobalAssign.Visible = CBool(Session("AllowtoPerformGlobalAssignEmployeeGoals"))
            If Session("FollowEmployeeHierarchy") = True Then
                btnGlobalAssign.Visible = CBool(Session("AllowtoPerformGlobalAssignEmployeeGoals"))
            End If
            'S.SANDEEP [10 DEC 2015] -- END
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.

            'S.SANDEEP |05-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    btnSubmitForGoalAccomplished.Visible = CBool(Session("AllowToSubmitForGoalAccomplished"))
            'End If
            'S.SANDEEP |05-MAR-2019| -- END

            'Varsha Rana (17-Oct-2017) -- End
            'S.SANDEEP [16 JUN 2015] -- START
            'btnUpdatePercentage.Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
            'If btnGlobalAssign.Visible = False AndAlso btnUpdatePercentage.Visible = False Then
            '    btnOperation.Visible = False
            'End If
            'S.SANDEEP [16 JUN 2015] -- END

            'S.SANDEEP |08-JAN-2019| -- START
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                btnUnlockEmployee.Visible = False
            End If
            'S.SANDEEP |08-JAN-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError("SetVisibility : " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)


    'Private Sub Fill_Accomplishment_Grid()

    '    Dim iSearch As String = String.Empty
    '    Dim objFMaster As New clsAssess_Field_Master
    '    Dim intGridWidth As Integer = 0
    '    'Shani(24-JAN-2017) -- Start
    '    'Enhancement : 
    '    'Dim dtFinal As DataTable
    '    'Dim intLinkedFld As Integer = 0
    '    Dim strFiledName As String = ""
    '    Dim dsFinal As DataSet
    '    'Shani(24-JAN-2017) -- End
    '    Try
    '        mdtAccomplishmentData = Nothing

    '        If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 4, "Sorry, No data defined for asseessment caption settings screen."), Me)
    '            Exit Sub
    '        End If

    '        If (New clsAssess_Field_Mapping).isExist(CInt(cboPeriod.SelectedValue)) = False Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 5, "Sorry no field is mapped with the selected period."), Me)
    '            Exit Sub
    '        End If

    '        'Shani(24-JAN-2017) -- Start
    '        'Enhancement : 
    '        'intLinkedFld = (New clsAssess_Field_Mapping).Get_Map_FieldId(cboPeriod.SelectedValue)

    '        ''If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '        ''    If CBool(Session("AllowtoViewEmployeeGoalsList")) = False Then Exit Sub
    '        ''End If

    '        'dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", , True)

    '        'If CInt(cboAccomplishmentStatus.SelectedValue) > 0 Then
    '        '    iSearch &= "AND vuGoalAccomplishmentStatusId = '" & CInt(cboAccomplishmentStatus.SelectedValue) & "' "
    ''End If

    '        'If iSearch.Trim.Length > 0 Then
    '        '    iSearch = iSearch.Substring(3)
    '        '    mdtAccomplishmentData = New DataView(dtFinal, iSearch, "perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
    '        'Else
    '        '    mdtAccomplishmentData = New DataView(dtFinal, "", "perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
    '        'End If

    '        'If mdtAccomplishmentData Is Nothing Then Exit Sub

    '        'dgv_Accomplishment_data.AutoGenerateColumns = False
    '        'Dim iColName As String = String.Empty

    '        'Dim iPlan() As String = Nothing
    '        'If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
    '        '    iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
    '        'End If

    '        'Dim dgvCol As BoundField = dgv_Accomplishment_data.Columns(2)
    '        'dgvCol.DataField = CStr("Field" & intLinkedFld.ToString)
    '        'dgvCol.HeaderText = mdtAccomplishmentData.Columns(CStr("Field" & intLinkedFld.ToString)).Caption
    '        'For Each dgvRow As DataRow In mdtAccomplishmentData.Rows
    '        '    If CBool(dgvRow("isfinal")) = True Then
    '        '        dgv_Accomplishment_data.Columns(0).Visible = True : dgv_Accomplishment_data.Columns(1).Visible = True
    '        '        dgv_Accomplishment_data.Style.Add("color", "blue")
    '        '        Exit For
    '        '    ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
    '        '        dgv_Accomplishment_data.Columns(0).Visible = False : dgv_Accomplishment_data.Columns(1).Visible = False
    '        '        dgv_Accomplishment_data.Style.Add("color", "Green")
    '        '        Exit For
    '        '    Else
    '        '        dgv_Accomplishment_data.Columns(0).Visible = False : dgv_Accomplishment_data.Columns(1).Visible = False
    '        '        dgv_Accomplishment_data.Style.Add("color", "black")
    '        '        Exit For
    '        '    End If
    '        'Next
    '        'dgv_Accomplishment_data.Width = Unit.Percentage(99.5)
    '        'dgv_Accomplishment_data.DataSource = mdtAccomplishmentData
    '        'dgv_Accomplishment_data.DataBind()

    '        'Dim objFMapping As New clsAssess_Field_Mapping
    '        'Dim xTotalWeight As Double = 0
    '        'xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
    '        'If xTotalWeight > 0 Then
    '        '    objlblTotalWeight.Text = Language.getMessage(mstrModuleName_AppRejGoals, 14, "Total Weight Assigned :") & " " & xTotalWeight.ToString
    '        'Else
    '        '    objlblTotalWeight.Text = ""
    '        'End If
    '        'objFMapping = Nothing

    '        strFiledName = (New clsAssess_Field_Mapping).Get_Map_FieldName(cboPeriod.SelectedValue)

    '        dsFinal = objEmpField1.GetAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAccomplishmentStatus.SelectedValue), , "List")

    '        If dsFinal Is Nothing Then Exit Sub

    '        mdtAccomplishmentData = New DataView(dsFinal.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable



    '        dgv_Accomplishment_data.AutoGenerateColumns = False
    '        dgv_Accomplishment_data.Columns(2).HeaderText = strFiledName

    '        'For Each dgvRow As DataRow In mdtAccomplishmentData.Rows
    '        '    If CBool(dgvRow("isfinal")) = True Then
    '        '        dgv_Accomplishment_data.Columns(0).Visible = True : dgv_Accomplishment_data.Columns(1).Visible = True
    '        '        dgv_Accomplishment_data.Style.Add("color", "blue")
    '        '        Exit For
    '        '    ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
    '        '        dgv_Accomplishment_data.Columns(0).Visible = False : dgv_Accomplishment_data.Columns(1).Visible = False
    '        '        dgv_Accomplishment_data.Style.Add("color", "Green")
    '        '        Exit For
    '        '    Else
    '        '        dgv_Accomplishment_data.Columns(0).Visible = False : dgv_Accomplishment_data.Columns(1).Visible = False
    '        '        dgv_Accomplishment_data.Style.Add("color", "black")
    '        '        Exit For
    '        '    End If
    '        'Next
    '        dgv_Accomplishment_data.Width = Unit.Percentage(99.5)
    '        dgv_Accomplishment_data.DataSource = mdtAccomplishmentData
    '        dgv_Accomplishment_data.DataBind()

    '        'Dim objFMapping As New clsAssess_Field_Mapping
    '        'Dim xTotalWeight As Double = 0
    '        'xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
    '        'If xTotalWeight > 0 Then
    '        '    objlblTotalWeight.Text = Language.getMessage(mstrModuleName_AppRejGoals, 14, "Total Weight Assigned :") & " " & xTotalWeight.ToString
    '        'Else
    '        '    objlblTotalWeight.Text = ""
    '        'End If
    '        'objFMapping = Nothing
    '        'Shani(24-JAN-2017) -- End
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Fill_Accomplishment_Grid" & ex.Message, Me)
    '    Finally
    '    End Try
    'End Sub
    'Shani (26-Sep-2016) -- End

    'Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
    '    Dim dRow As DataRow
    '    Try
    '        Dim dtRow() As DataRow = mdtAttachement.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
    '        If dtRow.Length <= 0 Then
    '            dRow = mdtAttachement.NewRow
    '            dRow("scanattachtranunkid") = -1
    '            dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
    '            dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
    '            dRow("modulerefid") = enImg_Email_RefId.Appraisal_Module
    '            dRow("scanattachrefid") = enScanAttactRefId.ASSESSMENT
    '            dRow("transactionunkid") = mintEmpUpdateTranunkid
    '            dRow("filepath") = ""
    '            dRow("filename") = f.Name
    '            dRow("filesize") = f.Length / 1024
    '            dRow("attached_date") = Today.Date
    '            dRow("orgfilepath") = strfullpath
    '            dRow("GUID") = Guid.NewGuid
    '            dRow("AUD") = "A"
    '            dRow("form_name") = mstrModuleName_upd_percent
    '            dRow("userunkid") = CInt(Session("userid"))
    '            'S.SANDEEP |25-JAN-2019| -- START
    '            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
    '            Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
    '            dRow("file_data") = xDocumentData
    '            'S.SANDEEP |25-JAN-2019| -- END
    '    Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 6, "Selected information is already present for particular employee."), Me)
    '            Exit Sub
    '    End If
    '        mdtAttachement.Rows.Add(dRow)
    '        Call FillAttachment()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("AddDocumentAttachment:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Private Sub FillAttachment()
    '    Dim dtView As DataView
    '    Try
    '        If mdtAttachement Is Nothing Then Exit Sub
    '        dtView = New DataView(mdtAttachement, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
    '        dgv_Attchment.AutoGenerateColumns = False
    '        dgv_Attchment.DataSource = dtView
    '        dgv_Attchment.DataBind()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("FillAttachment:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'S.SANDEEP |08-JAN-2019| -- START
    Private Function LockEmployee() As Boolean
        Dim objLockEmp As New clsassess_plan_eval_lockunlock
        Try
            Dim mintLockId As Integer = 0
            Dim mblnisLock As Boolean = False
            Dim mdtNextLockDate As Date = Nothing
            If objLockEmp.isExist(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING, "", mblnisLock, mintLockId, mdtNextLockDate) Then
                If mintLockId > 0 And mblnisLock = True Then Return True
            End If
            If objLockEmp.isValidDate(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING, ConfigParameter._Object._CurrentDateAndTime) = True Then
                Return False
            End If

            objLockEmp._AssessorMasterunkid = 0
            objLockEmp._Assessmodeid = 0
            objLockEmp._Auditdatetime = Now
            objLockEmp._Audittype = enAuditType.ADD
            objLockEmp._Audituserunkid = Session("UserId")
            objLockEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objLockEmp._Form_Name = mstrModuleName
            objLockEmp._Hostname = Session("HOST_NAME")
            objLockEmp._Ip = Session("IP_ADD")
            objLockEmp._Islock = True
            objLockEmp._Isunlocktenure = False
            objLockEmp._Isweb = True
            objLockEmp._Locktypeid = clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING
            objLockEmp._Lockunlockdatetime = Now
            objLockEmp._Lockuserunkid = Session("UserId")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                objLockEmp._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objLockEmp._Nextlockdatetime = Nothing
            objLockEmp._Periodunkid = CInt(cboPeriod.SelectedValue)
            objLockEmp._Unlockdays = 0
            objLockEmp._Unlockreason = ""
            objLockEmp._Unlockuserunkid = 0
            If objLockEmp.Insert() = False Then
                If objLockEmp._Message.Trim.Length > 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'eZeeMsgBox.Show(objLockEmp._Message, enMsgBoxStyle.Information)
                    DisplayMessage.DisplayMessage(objLockEmp._Message, Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("LockEmployee : " & ex.Message, Me)
        Finally
            objLockEmp = Nothing
        End Try
    End Function
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |18-JAN-2019| -- START
    Public Sub MergeRows(ByVal gridView As GridView)
        Try
            If gridView.Rows.Count > 1 AndAlso mintPerspectiveColIndex > 0 Then
                For rowIndex As Integer = gridView.Rows.Count - 2 To 0 Step -1
                    Dim row As GridViewRow = gridView.Rows(rowIndex)
                    Dim previousRow As GridViewRow = gridView.Rows(rowIndex + 1)
                    If row.Cells(mintPerspectiveColIndex).Text = previousRow.Cells(mintPerspectiveColIndex).Text Then
                        row.Cells(mintPerspectiveColIndex).RowSpan = If(previousRow.Cells(mintPerspectiveColIndex).RowSpan < 2, 2, previousRow.Cells(mintPerspectiveColIndex).RowSpan + 1)
                        previousRow.Cells(mintPerspectiveColIndex).Visible = False
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |18-JAN-2019| -- END

    'Private Sub SetTagValue(ByVal dtTab As DataTable, ByVal blnIsEdit As Boolean)
    '    Try
    '        If dtTab.Rows.Count > 0 Then
    '            Dim iMaxTranId As Integer = 0
    '            If IsDBNull(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
    '                iMaxTranId = Convert.ToInt32(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
    '            End If
    '            If blnIsEdit Then
    '                Dim iSecondLastTranId As Integer = 0
    '                Dim iList As List(Of Integer) = dtTab.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()
    '                If iList.Count > 0 Then
    '                    Dim dr() As DataRow = Nothing
    '                    If iList.Min() = iMaxTranId Then
    '                        dr = dtTab.Select("empupdatetranunkid = " & iMaxTranId)
    '                        If dr.Length > 0 Then
    '                            cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
    '                            cboChangeBy.Enabled = False
    '                            cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '                            Select Case cboChangeBy.SelectedIndex
    '                                Case 1
    '                                    txtNewPercentage.Attributes("lstval") = 0
    '                                Case 2
    '                                    txtNewValue.Attributes("lstval") = 0
    '                            End Select
    '                        End If
    '                    Else
    '                        Try
    '                            iSecondLastTranId = iList.Item(iList.IndexOf(iMaxTranId) + 1)
    '                        Catch ex As Exception
    '                            iSecondLastTranId = iMaxTranId
    '                        End Try
    '                        dr = dtTab.Select("empupdatetranunkid = " & iSecondLastTranId)
    '                        If dr.Length > 0 Then
    '                            cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
    '                            cboChangeBy.Enabled = False
    '                            cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '                            Select Case cboChangeBy.SelectedIndex
    '                                Case 1
    '                                    txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
    '                                Case 2
    '                                    txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
    '                            End Select
    '                        End If
    '                    End If
    '                Else
    '                    txtNewPercentage.Attributes("lstval") = 0
    '                    txtNewValue.Attributes("lstval") = 0
    '                End If
    '            Else
    '                If iMaxTranId > 0 Then
    '                    Dim dr() As DataRow = dtTab.Select("empupdatetranunkid = " & iMaxTranId)
    '                    If dr.Length > 0 Then
    '                        cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
    '                        cboChangeBy.Enabled = False
    '                        cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '                        Select Case cboChangeBy.SelectedIndex
    '                            Case 1
    '                                txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
    '                            Case 2
    '                                txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
    '                        End Select
    '                    Else
    '                        txtNewPercentage.Attributes("lstval") = 0
    '                        txtNewValue.Attributes("lstval") = 0
    '                    End If
    '                Else
    '                    txtNewPercentage.Attributes("lstval") = 0
    '                    txtNewValue.Attributes("lstval") = 0
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("SetTagValue : " & ex.Message, Me)
    '    End Try
    'End Sub

    'S.SANDEEP |08-DEC-2020| -- START
    'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
    Private Function IsValidGoalOperation() As Boolean
        Try
            Select Case CInt(Session("CascadingTypeId"))
                Case enPACascading.LOOSE_CASCADING, enPACascading.STRICT_CASCADING
                    If CBool(Session("FollowEmployeeHierarchy")) Then
                        Dim StrMsg As String = ""
                        StrMsg = (New clsassess_empfield1_master).IsValidGoalOperation(CInt(cboPeriod.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), CInt(cboEmployee.SelectedValue), Session("Database_Name").ToString)
                        If StrMsg.Trim.Length > 0 Then
                            DisplayMessage.DisplayMessage(StrMsg, Me)
                            Return False
                        End If
                    End If
            End Select
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Function
    'S.SANDEEP |08-DEC-2020| -- END
    
#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
                   CInt(cboPeriod.SelectedValue) <= 0 Then

                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
                End If
                'Pinkal (15-Mar-2019) -- End
                Exit Sub
            End If


            'S.SANDEEP [10 DEC 2015] -- START
            If Session("SkipApprovalFlowInPlanning") = True Then
                Dim intU_EmpId As Integer = 0
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    Dim objUserAddEdit As New clsUserAddEdit
                    objUserAddEdit._Userunkid = Session("UserId")
                    intU_EmpId = objUserAddEdit._EmployeeUnkid
                    objUserAddEdit = Nothing
                End If

                If intU_EmpId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) Then
                    DisplayMessage.DisplayMessage("You cannot access your own goals from MSS, Switch to ESS if you want to access your goals or Remove the Skip the Approval Setting", Me)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [10 DEC 2015] -- END


            Call Fill_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnSearch_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            cboPerspective.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboFieldValue1.SelectedValue = 0
            cboFieldValue2.SelectedValue = 0
            cboFieldValue3.SelectedValue = 0
            cboFieldValue4.SelectedValue = 0
            cboFieldValue5.SelectedValue = 0
            Me.ViewState("mdtFinal") = Nothing
            dgvData.DataSource = Nothing
            dgvData.DataBind()

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'pnl_dgvData.Height = 0
            'SHANI [01 FEB 2015]--END
            Call cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)
            objlblTotalWeight.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReset_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField1Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField1Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField1Action"), enAction)
        Dim mdtEmpField1Owner As DataTable = CType(Me.ViewState("mdtEmpField1Owner"), DataTable)
        Dim mdicEmpField1FieldData As New Dictionary(Of Integer, String) '= CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField1() = False Then Exit Sub
            Call SetEmpField1Value()

            'S.SANDEEP |25-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If hdf_lblEmpField1Remark1.Value.Trim.Length > 0 Then
                mdicEmpField1FieldData.Add(CInt(hdf_lblEmpField1Remark1.Value), txtEmpField1Remark1.Text)
            End If
            If hdf_lblEmpField1Remark2.Value.Trim.Length > 0 Then
                mdicEmpField1FieldData.Add(CInt(hdf_lblEmpField1Remark2.Value), txtEmpField1Remark2.Text)
            End If
            If hdf_lblEmpField1Remark3.Value.Trim.Length > 0 Then
                mdicEmpField1FieldData.Add(CInt(hdf_lblEmpField1Remark3.Value), txtEmpField1Remark3.Text)
            End If
            'S.SANDEEP |25-FEB-2019| -- END

            With objEmpField1
                ._FormName = mstrModuleName_Emp1
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objEmpField1.Update(mdicEmpField1FieldData, mdtEmpField1Owner)
            Else
                iblnFlag = objEmpField1.Insert(mdicEmpField1FieldData, mdtEmpField1Owner)
            End If
            If iblnFlag = False Then
                If objEmpField1._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEmpField1._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 13, "Sorry, problem in saving Owner Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp1, 12, "Employee Goals are saved successfully."), Me)
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                txtEmpField1Remark1.Text = "" : txtEmpField1Remark2.Text = "" : txtEmpField1Remark3.Text = "" : txtEmpField1SearchEmp.Text = ""
                'S.SANDEEP |12-FEB-2019| -- END

                If menAction = enAction.ADD_CONTINUE Then
                    objEmpField1 = New clsassess_empfield1_master
                    Call GetEmpField1Value()
                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'txtEmpField1Remark1.Text = "" : txtEmpField1Remark2.Text = "" : txtEmpField1Remark3.Text = "" : txtEmpField1SearchEmp.Text = ""
                    'S.SANDEEP |12-FEB-2019| -- END

                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnEmpField1Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField1Save_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField1Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField1Cancel.Click
        Try
            mblnpopupShow_EmpField1 = False
            pop_objfrmAddEditEmpField1.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField1Cancel_Click:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField2Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField2Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField2Action"), enAction)
        Dim mdtEmpField2Owner As DataTable = CType(Me.ViewState("mdtEmpField2Owner"), DataTable)
        Dim mdicEmpField2FieldData As New Dictionary(Of Integer, String) '= CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField2() = False Then Exit Sub
            Call SetEmpField2Value()

            'S.SANDEEP |25-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If hdf_lblEmpField2Remark1.Value.Trim.Length > 0 Then
                mdicEmpField2FieldData.Add(CInt(hdf_lblEmpField2Remark1.Value), txtEmpField2Remark1.Text)
            End If
            If hdf_lblEmpField2Remark2.Value.Trim.Length > 0 Then
                mdicEmpField2FieldData.Add(CInt(hdf_lblEmpField2Remark2.Value), txtEmpField2Remark2.Text)
            End If
            If hdf_lblEmpField2Remark3.Value.Trim.Length > 0 Then
                mdicEmpField2FieldData.Add(CInt(hdf_lblEmpField2Remark3.Value), txtEmpField2Remark3.Text)
            End If
            'S.SANDEEP |25-FEB-2019| -- END

            With objEmpField2
                ._FormName = mstrModuleName_Emp2
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objEmpField2.Update(mdicEmpField2FieldData, mdtEmpField2Owner)
            Else
                iblnFlag = objEmpField2.Insert(mdicEmpField2FieldData, mdtEmpField2Owner)
            End If
            If iblnFlag = False Then
                If objEmpField2._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEmpField2._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 9, "Sorry, problem in saving Employee Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp2, 10, "Employee Goals are saved successfully."), Me)
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                txtEmpField2Remark1.Text = "" : txtEmpField2Remark2.Text = "" : txtEmpField2Remark3.Text = "" : txtEmpField2SearchEmp.Text = ""
                'S.SANDEEP |12-FEB-2019| -- END

                If menAction = enAction.ADD_CONTINUE Then
                    objEmpField2 = New clsassess_empfield2_master
                    Call GetEmpField2Value()
                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'txtEmpField2Remark1.Text = "" : txtEmpField2Remark2.Text = "" : txtEmpField2Remark3.Text = "" : txtEmpField2SearchEmp.Text = ""
                    'S.SANDEEP |12-FEB-2019| -- END
                    If CInt(Me.ViewState("mintEmpField2ParentId")) > 0 Then
                        cboEmpField2EmpFieldValue1.SelectedValue = CInt(Me.ViewState("mintEmpField2ParentId"))
                    End If
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnEmpField2Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField2Save_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField2Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField2Cancel.Click
        Try
            mblnpopupShow_EmpField2 = False
            pop_objfrmAddEditEmpField2.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField2Cancel_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField3Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField3Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField3Action"), enAction)
        Dim mdtEmpField3Owner As DataTable = CType(Me.ViewState("mdtEmpField3Owner"), DataTable)
        Dim mdicEmpField3FieldData As New Dictionary(Of Integer, String) ' = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField3() = False Then Exit Sub
            Call SetEmpField3Value()

            'S.SANDEEP |25-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If hdf_lblEmpField3Remark1.Value.Trim.Length > 0 Then
                mdicEmpField3FieldData.Add(CInt(hdf_lblEmpField3Remark1.Value), txtEmpField3Remark1.Text)
            End If
            If hdf_lblEmpField3Remark2.Value.Trim.Length > 0 Then
                mdicEmpField3FieldData.Add(CInt(hdf_lblEmpField3Remark3.Value), txtEmpField3Remark2.Text)
            End If
            If hdf_lblEmpField3Remark3.Value.Trim.Length > 0 Then
                mdicEmpField3FieldData.Add(CInt(hdf_lblEmpField3Remark3.Value), txtEmpField3Remark3.Text)
            End If
            'S.SANDEEP |25-FEB-2019| -- END

            With objEmpField3
                ._FormName = mstrModuleName_Emp3
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objEmpField3.Update(mdicEmpField3FieldData, mdtEmpField3Owner)
            Else
                iblnFlag = objEmpField3.Insert(mdicEmpField3FieldData, mdtEmpField3Owner)
            End If
            If iblnFlag = False Then
                If objEmpField3._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEmpField3._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 9, "Sorry, problem in saving Employee Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp3, 10, "Employee Goals are saved successfully."), Me)
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                txtEmpField3Remark1.Text = "" : txtEmpField3Remark2.Text = "" : txtEmpField3Remark3.Text = ""
                'S.SANDEEP |12-FEB-2019| -- END

                If menAction = enAction.ADD_CONTINUE Then
                    objEmpField3 = New clsassess_empfield3_master
                    Call GetEmpField3Value()
                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

                    'txtEmpField3Remark1.Text = "" : txtEmpField3Remark2.Text = "" : txtEmpField3Remark3.Text = "" 

                    'S.SANDEEP |12-FEB-2019| -- END

                    If CInt(Me.ViewState("mintEmpField3ParentId")) > 0 Then
                        cboEmpField3EmpFieldValue2.SelectedValue = CInt(Me.ViewState("mintEmpField3ParentId"))
                    End If
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnEmpField3Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField3Save_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField3Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField3Cancel.Click
        Try
            mblnpopupShow_EmpField3 = False
            pop_objfrmAddEditEmpField3.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField3Cancel_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField4Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField4Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField4Action"), enAction)
        Dim mdtEmpField4Owner As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
        Dim mdicEmpField4FieldData As New Dictionary(Of Integer, String) '= CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField4() = False Then Exit Sub
            Call SetEmpField4Value()

            'S.SANDEEP |25-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If hdf_txtEmpField4Remark1.Value.Trim.Length > 0 Then
                mdicEmpField4FieldData.Add(CInt(hdf_txtEmpField4Remark1.Value), txtEmpField4Remark1.Text)
            End If
            If hdf_txtEmpField4Remark2.Value.Trim.Length > 0 Then
                mdicEmpField4FieldData.Add(CInt(hdf_txtEmpField4Remark2.Value), txtEmpField4Remark2.Text)
            End If
            If hdf_txtEmpField4Remark3.Value.Trim.Length > 0 Then
                mdicEmpField4FieldData.Add(CInt(hdf_txtEmpField4Remark3.Value), txtEmpField4Remark3.Text)
            End If
            'S.SANDEEP |25-FEB-2019| -- END

            With objEmpField4
                ._FormName = mstrModuleName_Emp4
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objEmpField4.Update(mdicEmpField4FieldData, mdtEmpField4Owner)
            Else
                iblnFlag = objEmpField4.Insert(mdicEmpField4FieldData, mdtEmpField4Owner)
            End If

            If iblnFlag = False Then
                If objEmpField4._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEmpField4._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 9, "Sorry, problem in saving Employee Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp4, 10, "Employee Goals are saved successfully."), Me)
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                txtEmpField4Remark1.Text = "" : txtEmpField4Remark2.Text = "" : txtEmpField4Remark3.Text = ""
                'S.SANDEEP |12-FEB-2019| -- END
                If menAction = enAction.ADD_CONTINUE Then
                    objEmpField4 = New clsassess_empfield4_master
                    Call GetEmpField4Value()

                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    'txtEmpField4Remark1.Text = "" : txtEmpField4Remark2.Text = "" : txtEmpField4Remark3.Text = ""

                    'S.SANDEEP |12-FEB-2019| -- END
                    If Me.ViewState("mintEmpField4ParentId") > 0 Then
                        cboEmpField4EmpFieldValue3.SelectedValue = Me.ViewState("mintEmpField4ParentId")
                    End If
                    Call Fill_Grid()
                Else
                    Fill_Grid()
                    Call btnEmpField4Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField4Save_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField4Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField4Cancel.Click
        Try
            mblnpopupShow_EmpField4 = False
            pop_objfrmAddEditEmpField4.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField4Cancel_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField5Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField5Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField5Action"), enAction)
        Dim mdtEmpField5Owner As DataTable = CType(Me.ViewState("mdtEmpField5Owner"), DataTable)
        Dim mdicEmpField5FieldData As New Dictionary(Of Integer, String) '= CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField5() = False Then Exit Sub
            Call SetEmpField5Value()

            'S.SANDEEP |25-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If hdf_txtEmpField5Remark1.Value.Trim.Length > 0 Then
                mdicEmpField5FieldData.Add(CInt(hdf_txtEmpField5Remark1.Value), txtEmpField5Remark1.Text)
            End If
            If hdf_txtEmpField5Remark2.Value.Trim.Length > 0 Then
                mdicEmpField5FieldData.Add(CInt(hdf_txtEmpField5Remark2.Value), txtEmpField5Remark2.Text)
            End If
            If hdf_txtEmpField5Remark3.Value.Trim.Length > 0 Then
                mdicEmpField5FieldData.Add(CInt(hdf_txtEmpField5Remark3.Value), txtEmpField5Remark3.Text)
            End If
            'S.SANDEEP |25-FEB-2019| -- END

            With objEmpField5
                ._FormName = mstrModuleName_Emp5
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objEmpField5.Update(mdicEmpField5FieldData, mdtEmpField5Owner)
            Else
                iblnFlag = objEmpField5.Insert(mdicEmpField5FieldData, mdtEmpField5Owner)
            End If
            If iblnFlag = False Then
                If objEmpField5._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEmpField5._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 9, "Sorry, problem in saving Employee Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_Emp5, 10, "Employee Goals are saved successfully."), Me)
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                txtEmpField5Remark1.Text = "" : txtEmpField5Remark2.Text = "" : txtEmpField5Remark3.Text = ""
                'S.SANDEEP |12-FEB-2019| -- END

                If menAction = enAction.ADD_CONTINUE Then
                    objEmpField5 = New clsassess_empfield5_master
                    Call GetEmpField5Value()
                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    txtEmpField5Remark1.Text = "" : txtEmpField5Remark2.Text = "" : txtEmpField5Remark3.Text = ""
                    'S.SANDEEP |12-FEB-2019| -- END

                    If Me.ViewState("mintEmpField5ParentId") > 0 Then
                        cboEmpField5EmpFieldValue4.SelectedValue = Me.ViewState("mintEmpField5ParentId")
                    End If
                    Fill_Grid()
                Else
                    Fill_Grid()
                    Call btnEmpField5Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnEmpField5Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField5Cancel.Click
        Try
            mblnpopupShow_EmpField5 = False
            pop_objfrmAddEditEmpField5.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnEmpField5Cancel_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        'Dim mdtFinal As DataTable = Nothing 'CType(......, DataTable)
        Dim mstrScreenId As String = Me.ViewState("DeleteScreenId")
        Dim mstrScreeName As String = Me.ViewState("DeleteColumnName")
        Try
            If txtMessage.Text.Trim.Length > 0 Then

                Select Case mstrScreenId.Split("|")(1)
                    Case 1
                        If CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield1unkid")) > 0 Then ' CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield1unkid")) > 0 Then
                            objEmpField1._Isvoid = True
                            objEmpField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField1._Voidreason = txtMessage.Text
                            objEmpField1._Voiduserunkid = Session("UserId")
                            With objEmpField1
                                ._FormName = mstrModuleName_Emp1
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            If objEmpField1.Delete(CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield1unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objEmpField1._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objEmpField1._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                             " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                             " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 4, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 2
                        If CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield2unkid")) > 0 Then 'CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield2unkid")) > 0 Then
                            objEmpField2._Isvoid = True
                            objEmpField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField2._Voidreason = txtMessage.Text
                            objEmpField2._Voiduserunkid = Session("UserId")
                            With objEmpField2
                                ._FormName = mstrModuleName_Emp2
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            If objEmpField2.Delete(CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield2unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objEmpField2._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objEmpField2._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                            " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                            " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 4, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 3
                        If CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield3unkid")) > 0 Then 'CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield3unkid")) > 0 Then
                            objEmpField3._Isvoid = True
                            objEmpField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField3._Voidreason = txtMessage.Text
                            objEmpField3._Voiduserunkid = Session("UserId")
                            With objEmpField3
                                ._FormName = mstrModuleName_Emp3
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            If objEmpField3.Delete(CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield3unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objEmpField3._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objEmpField3._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                           " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                           " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 4, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 4
                        If CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield4unkid")) > 0 Then 'CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield4unkid")) > 0 Then
                            objEmpField4._Isvoid = True
                            objEmpField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField4._Voidreason = txtMessage.Text
                            objEmpField4._Voiduserunkid = Session("UserId")
                            With objEmpField4
                                ._FormName = mstrModuleName_Emp4
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            If objEmpField4.Delete(CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield4unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objEmpField4._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objEmpField4._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                             " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                             " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 4, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 5
                        If CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield5unkid")) > 0 Then 'CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield5unkid")) > 0 Then
                            objEmpField5._Isvoid = True
                            objEmpField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objEmpField5._Voidreason = txtMessage.Text
                            objEmpField5._Voiduserunkid = Session("UserId")
                            With objEmpField5
                                ._FormName = mstrModuleName_Emp5
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._Loginemployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            If objEmpField5.Delete(CInt(dgvData.DataKeys(Me.ViewState("DeleteRowIndex"))("empfield5unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objEmpField5._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objEmpField5._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                           " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                           " " & mstrScreeName & " " & Language.getMessage(mstrModuleName, 4, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                End Select
            Else
                popup_YesNo.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnYes_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnOpration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOperation.Click
        Try
            'popup_Opration.X = CInt(hdf_locationx.Value)
            'popup_Opration.Y = CInt(hdf_locationy.Value)
            'popup_Opration.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnOpration_Click :-" & ex.Message, Me)
        End Try
    End Sub

    'Protected Sub btnSubmitApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitApproval.Click
    '    Try
    '        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '        If dgvData.Rows.Count > 0 Then

    '            Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
    '            If dtemp.Length <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period."), Me)
    '                Exit Sub
    '            End If

    '            If CBool(mdtFinal.Rows(0)("isfinal")) = True Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, Balance Score Card Planning is already approved for selected employee and period. ."), Me)
    '                Exit Sub
    '            ElseIf mdtFinal.Rows(0)("opstatusid") = enObjective_Status.SUBMIT_APPROVAL Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, Balance Score Card Planning is already sent for approval for selected employee and period. ."), Me)
    '                Exit Sub
    '            End If

    '            Dim objAssessor As New clsAssessor_tran
    '            Dim sMsg As String = String.Empty
    '            sMsg = objAssessor.IsAssessorPresent(CInt(cboEmployee.SelectedValue))
    '            If sMsg <> "" Then
    '                DisplayMessage.DisplayMessage(sMsg, Me)
    '                Exit Sub
    '            End If
    '            objAssessor = Nothing

    '            Dim objFMapping As New clsAssess_Field_Mapping
    '            sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
    '            If sMsg.Trim.Length > 0 Then
    '                DisplayMessage.DisplayMessage(sMsg, Me)
    '                objFMapping = Nothing
    '                Exit Sub
    '            End If
    '            If objFMapping IsNot Nothing Then objFMapping = Nothing

    '            'SHANI [01 FEB 2015]-START


    '            popup_SubmitApproval.Title = "Aruti" 'SHANI [01 FEB 2015]--lblSubmitTitle.Text = "Aruti"
    '            popup_SubmitApproval.Message = Language.getMessage(mstrModuleName, 13, "You are about to Submit this Balance Score Card Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
    '                                                                  "Do you wish to continue?") ' SHANI [01 FEB 2015]--lblSubmitMessage.Text
    '            popup_SubmitApproval.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnSubmitApproval_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnSubmitYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitYes.Click
    'Protected Sub btnSubmitYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_SubmitApproval.buttonYes_Click
    '    'SHANI [01 FEB 2015]--END
    '    Try

    '        Dim objEStatusTran As New clsassess_empstatus_tran
    '        objEStatusTran._Assessoremployeeunkid = 0
    '        objEStatusTran._Assessormasterunkid = 0
    '        objEStatusTran._Commtents = ""
    '        objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '        objEStatusTran._Isunlock = False
    '        objEStatusTran._Loginemployeeunkid = 0
    '        objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
    '        objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
    '        objEStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
    '        objEStatusTran._Userunkid = Session("UserId")

    '        If objEStatusTran.Insert() = False Then
    '            DisplayMessage.DisplayMessage(objEStatusTran._Message, Me)
    '            Exit Sub
    '        Else
    '            If CStr(Session("ArutiSelfServiceURL")).Trim.Length > 0 Then
    '                If CInt(cboPeriod.SelectedIndex) <= 0 Then
    '                    Dim objperiod As New clscommom_period_Tran

    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'objperiod._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                    'objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
    '                    '                                        objperiod._Yearunkid, Session("FinancialYear_Name"), , , _
    '                    '                                        enLogin_Mode.DESKTOP, 0, 0)
    '                    objperiod._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
    '                    objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
    '                                                            CInt(cboPeriod.SelectedValue), _
    '                                                            objperiod._Yearunkid, Session("FinancialYear_Name"), _
    '                                                            Session("Database_Name"), Session("CompanyUnkId"), , _
    '                                                            enLogin_Mode.DESKTOP, 0, Session("UserId"))
    '                    'Shani(20-Nov-2015) -- End

    '                    objperiod = Nothing
    '                End If
    '            End If
    '            Call Fill_Grid()
    '        End If
    '        objEStatusTran = Nothing
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub btnApproveSubmitted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveSubmitted.Click
    '    Try
    '        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '        If dgvData.Rows.Count > 0 Then
    '            Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
    '            If dtemp.Length <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period."), Me)
    '                Exit Sub
    '            End If

    '            If CBool(mdtFinal.Rows(0)("isfinal")) <> True AndAlso mdtFinal.Rows(0)("opstatusid") <> enObjective_Status.SUBMIT_APPROVAL Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, Balance Score Card Planning is yet not submitted for selected employee and period."), Me)
    '                Exit Sub
    '            End If

    '            Dim iFinal As Boolean = False
    '            If CBool(mdtFinal.Rows(0)("isfinal")) = True Then
    '                iFinal = True
    '            ElseIf mdtFinal.Rows(0)("opstatusid") = enObjective_Status.SUBMIT_APPROVAL Then
    '                iFinal = False
    '            End If
    '            If iFinal = True Then
    '                Dim objAnalysis_Master As New clsevaluation_analysis_master
    '                'S.SANDEEP |22-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
    '                'If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '                '    Exit Sub
    '                'End If

    '                'If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '                '    Exit Sub
    '                'End If

    '                'If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '                '    Exit Sub
    '                'End If

    '                If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '    Exit Sub
    'End If

    '                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '    Exit Sub
    'End If

    '                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '    Exit Sub
    'End If
    '                'S.SANDEEP |22-JUL-2019| -- END

    '                objAnalysis_Master = Nothing
    '            End If
    '            popup_ApproverRejYesNo.Title = "Aruti" 'SHANI [01 FEB 2015] -- lblAppRejTitle.Text
    '            popup_ApproverRejYesNo.Message = Language.getMessage(mstrModuleName, 14, "You are about to Approve/Disapprove this Balance Score Card Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
    '                                                   "Do you wish to continue?") 'SHANI [01 FEB 2015] -- lblAppRejMessage.Text
    '            popup_ApproverRejYesNo.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnApproveSubmitted_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnAppRejYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppRejYes.Click
    'Protected Sub btnAppRejYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ApproverRejYesNo.buttonYes_Click
    '    'SHANI [01 FEB 2015]--END
    '    Try
    '        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '        If dgvData.Rows.Count > 0 Then
    '            Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
    '            Dim iDisplayText As String = String.Empty
    '            iDisplayText = dtemp(0).Item("Emp").ToString.Trim & ", " & Language.getMessage(mstrModuleName, 17, "For Period : ") & " " & cboPeriod.SelectedItem.Text
    '            Call ApproverReject_load(iDisplayText)
    '            mblnpopupShow_AproverReject = True
    '            popup_ApprovrReject.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAppRejYes_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnApRejClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApRejClose.Click
    '    Try
    '        mblnpopupShow_AproverReject = False
    '        popup_ApprovrReject.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnApRejClose_Click" & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnApRejFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApRejFinalSave.Click, btnOpen_Changes.Click
    '    Dim btn As Button = CType(sender, Button)
    '    Dim iAssessorMasterId, iAssessorEmpId As Integer
    '    Dim sAssessorName As String = ""
    '    iAssessorMasterId = 0 : iAssessorEmpId = 0
    '    Try
    '        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
    '        If iAssessorMasterId <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRej, 1, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), Me)
    '            Exit Sub
    '        End If

    '        lblFinalTitle.Text = "Aruti"
    '        If btn.ID.ToUpper = "BTNAPREJFINALSAVE" Then
    '            lblFinalMessage.Text = Language.getMessage(mstrModuleName_AppRej, 2, "You are about to Approve this BSC Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & "Do you wish to continue?")
    '            btnOpenYes.Visible = False
    '            btnFinalYes.Visible = True
    '        ElseIf btn.ID.ToUpper = "BTNOPEN_CHANGES" Then
    '            lblFinalMessage.Text = Language.getMessage(mstrModuleName_AppRej, 3, "You are about to Disapprove this BSC Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & "Do you wish to continue?")
    '            btnOpenYes.Visible = True
    '            btnFinalYes.Visible = False
    '        End If
    '        popup_ApprovrFinalYesNo.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnApRejFinalSave_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnFinalYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalYes.Click
    '    Dim xDataTable As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '    Dim iAssessorMasterId, iAssessorEmpId As Integer
    '    Dim sAssessorName As String = ""
    '    iAssessorMasterId = 0 : iAssessorEmpId = 0
    '    Try
    '        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
    '        Dim objEmpField1 As New clsassess_empfield1_master
    '        Dim xRow() As DataRow = Nothing
    '        If xDataTable IsNot Nothing Then
    '            xRow = xDataTable.Select("OwnerIds <> ''")
    '        End If
    '        Dim blnFlag As Boolean = False
    '        If xRow.Length > 0 Then
    '            blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow)
    '        End If

    '        Dim objStatus As New clsassess_empstatus_tran
    '        objStatus._Assessoremployeeunkid = iAssessorMasterId
    '        objStatus._Assessormasterunkid = iAssessorEmpId
    '        objStatus._Commtents = txtComments.Text
    '        objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '        objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
    '        objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
    '        objStatus._Statustypeid = enObjective_Status.FINAL_SAVE
    '        objStatus._Isunlock = False
    '        objStatus._Userunkid = Session("Userid")
    '        If objStatus.Insert() = False Then
    '            DisplayMessage.DisplayMessage(objStatus._Message, Me)
    '            Exit Sub
    '        Else
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False)

    '            'S.SANDEEP [01-Feb-2018] -- START
    '            'ISSUE/ENHANCEMENT : {#0001965}
    '            'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , Session("Ntf_GoalsUnlockUserIds").ToString())
    '            objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
    '            'S.SANDEEP [01-Feb-2018] -- END

    '            'Sohail (30 Nov 2017) -- End
    '            objEmpField1 = Nothing
    '        End If
    '        objStatus = Nothing
    '        Call Fill_Grid()
    '        mblnpopupShow_AproverReject = False
    '        popup_ApprovrReject.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnFinalYes_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnOpenYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenYes.Click
    '    Dim xDataTable As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '    Dim iAssessorMasterId, iAssessorEmpId As Integer
    '    Dim sAssessorName As String = ""
    '    iAssessorMasterId = 0 : iAssessorEmpId = 0
    '    Try
    '        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
    '        Dim objStatus As New clsassess_empstatus_tran
    '        objStatus._Assessoremployeeunkid = iAssessorMasterId
    '        objStatus._Assessormasterunkid = iAssessorEmpId
    '        objStatus._Commtents = txtComments.Text
    '        objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '        objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
    '        objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
    '        objStatus._Statustypeid = enObjective_Status.OPEN_CHANGES
    '        objStatus._Isunlock = False
    '        objStatus._Userunkid = Session("Userid")

    '        If objStatus.Insert() = False Then
    '            DisplayMessage.DisplayMessage(objStatus._Message, Me)
    '            Exit Sub
    '        Else
    '            Dim objEmpField1 As New clsassess_empfield1_master
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False)

    '            'S.SANDEEP [01-Feb-2018] -- START
    '            'ISSUE/ENHANCEMENT : {#0001965}
    '            'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False, CInt(Session("CompanyUnkId")), , , Session("Ntf_GoalsUnlockUserIds").ToString())
    '            objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
    '            'S.SANDEEP [01-Feb-2018] -- END

    '            'Sohail (30 Nov 2017) -- End
    '            objEmpField1 = Nothing
    '        End If
    '        objStatus = Nothing
    '        Call Fill_Grid()
    '        mblnpopupShow_AproverReject = False
    '        popup_ApprovrReject.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnOpenYes_Click :-" & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnUnlockFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlockFinalSave.Click
    '    Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '    Try
    '        If dgvData.Rows.Count > 0 Then
    '            Dim dtemp() As DataRow = mdtFinal.Select("Emp <> ''")
    '            If dtemp.Length <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, Balance Score Card Planning is not planned for selected employee and period."), Me)
    '                Exit Sub
    '            End If

    '            If CBool(mdtFinal.Rows(0)("isfinal")) <> True Then
    '                'S.SANDEEP |12-MAR-2019| -- START
    '                'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
    '                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Balance Score Card Planning is yet not final saved for selected employee and period."), Me)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, Balance Score Card Planning is yet not final approved for selected employee and period."), Me)
    '                'S.SANDEEP |12-MAR-2019| -- END
    '                Exit Sub
    '            End If

    '            'S.SANDEEP |22-JUL-2019| -- START
    '            'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
    '            'Dim objAnalysis_Master As New clsevaluation_analysis_master
    '            'If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '            '    Exit Sub
    '            'End If

    '            'If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '            '    Exit Sub
    '            'End If

    '            'If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '            '    Exit Sub
    '            'End If
    '            'objAnalysis_Master = Nothing

    'Dim objAnalysis_Master As New clsevaluation_analysis_master
    '            If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '    Exit Sub
    'End If

    '            If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '    Exit Sub
    'End If

    '            If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot unlock BSC Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
    '    Exit Sub
    'End If
    'objAnalysis_Master = Nothing
    '            'S.SANDEEP |22-JUL-2019| -- END

    '            Dim iMsgText As String = ""
    '            iMsgText = Language.getMessage(mstrModuleName, 22, "You are about to unlock the employee goals. This will result in opening of all operation on employee goals.") & vbCrLf & _
    '                       Language.getMessage(mstrModuleName, 23, "Do you wish to continue?")

    '            lblUnlockTitel.Text = "Aruti"
    '            lblUnlockMessage.Text = iMsgText
    '            txtUnlockReason.Visible = False
    '            popup_UnclokFinalSave.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnUnlockFinalSave_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnUnlockYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlockYes.Click
    '    Try
    '        If txtUnlockReason.Visible = False Then
    '            txtUnlockReason.Visible = True
    '            lblUnlockMessage.Text = Language.getMessage(mstrModuleName, 24, "Enter reason to unlock")
    '            popup_UnclokFinalSave.Show()

    '        ElseIf txtUnlockReason.Visible = True Then

    '            'S.SANDEEP [15-Feb-2018] -- START
    '            'ISSUE/ENHANCEMENT : {#38}
    '            Dim iAssessorMasterId, iAssessorEmpId As Integer
    '            Dim sAssessorName As String = ""
    '            iAssessorMasterId = 0 : iAssessorEmpId = 0
    '            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
    '            Dim objEmpField1 As New clsassess_empfield1_master
    '            'S.SANDEEP [15-Feb-2018] -- END

    '            If txtUnlockReason.Text.Trim.Length > 0 Then
    '                Dim objEStatusTran As New clsassess_empstatus_tran
    '                objEStatusTran._Assessoremployeeunkid = 0
    '                objEStatusTran._Assessormasterunkid = 0
    '                objEStatusTran._Commtents = txtUnlockReason.Text
    '                objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '                objEStatusTran._Isunlock = True
    '                objEStatusTran._Loginemployeeunkid = 0
    '                objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
    '                objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
    '                objEStatusTran._Userunkid = CInt(Session("UserID"))
    '                If objEStatusTran.Insert() = False Then
    '                    DisplayMessage.DisplayMessage(objEStatusTran._Message, Me)
    '                    Exit Sub
    '                Else
    '                    'S.SANDEEP [15-Feb-2018] -- START
    '                    'ISSUE/ENHANCEMENT : {#38}

    '                    'S.SANDEEP [09-MAR-2018] -- START
    '                    'ISSUE/ENHANCEMENT : {#Internal Issues}
    '                    'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
    '                    objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtUnlockReason.Text, False, True, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
    '                    'S.SANDEEP [09-MAR-2018] -- END

    '                    'S.SANDEEP [15-Feb-2018] -- END
    '                    Call Fill_Grid()
    '                End If
    '                objEStatusTran = Nothing
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnUnlockYes_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub btnGlobalAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGlobalAssign.Click
        Try
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmpGlobalAssignGoals.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnGlobalAssign_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Protected Sub btnUpdatePercentage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePercentage.Click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmpUpdateparcentage.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnUpdatePercentage_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub


    'SHANI (18 JUN 2015) -- Start
    'Protected Sub btnUpdateSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSave.Click
    '    Try
    '        If UpdateIsValidInfo() = False Then Exit Sub

    '        If txtUpdateRemark.Text.Trim.Length <= 0 Then
    '            lblUpdateMessages.Text = Language.getMessage("frmUpdateFieldValue", 6, "Sorry, you have not entered the remark. Would you like to save the changes without remark?")
    '            hdf_UpdateYesNo.Value = "TXTREMARK"
    '            popup_UpdateYesNO.Show()
    '            Exit Sub
    '        End If
    '        Call Save_UpdateProgress()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnUpdateSave_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnUpdateClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateClose.Click
    '    Try
    '        popup_UpdateProgress.Hide()
    '        mblnUpdateProgress = False
    '        Call Fill_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnUpdateClose_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnupdateYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdateYes.Click
    '    Try
    '        If hdf_UpdateYesNo.Value.ToUpper = "TXTREMARK" Then
    '            Call Save_UpdateProgress()
    '        ElseIf hdf_UpdateYesNo.Value.ToUpper = "DELETE" Then
    '            popup_UpdateProgressDeleteReason.Title = "Delete Reason:"
    '            popup_UpdateProgressDeleteReason.Show()
    '        End If
    '        hdf_UpdateYesNo.Value = ""
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnupdateYes_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnupdateNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdateNo.Click
    '    Try
    '        If hdf_UpdateYesNo.Value.ToUpper = "TXTREMARK" Then
    '        ElseIf hdf_UpdateYesNo.Value.ToUpper = "DELETE" Then
    '            mintEmpUpdateTranunkid = 0
    '        End If
    '        hdf_UpdateYesNo.Value = ""
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("FillEmployeeCombo:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub popup_UpdateProgressDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UpdateProgressDeleteReason.buttonDelReasonYes_Click
    '    Try
    '        objEUpdateProgress._Isvoid = True
    '        objEUpdateProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '        objEUpdateProgress._Voidreason = popup_UpdateProgressDeleteReason.Reason
    '        objEUpdateProgress._Voiduserunkid = CInt(Session("UserId"))

    '        If objEUpdateProgress.Delete(mintEmpUpdateTranunkid) = True Then
    '            Call Fill_History()
    '            popup_UpdateProgressDeleteReason.Reason = ""
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("FillEmployeeCombo:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub popup_UpdateProgressDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UpdateProgressDeleteReason.buttonDelReasonNo_Click
    '    Try
    '        mintEmpUpdateTranunkid = 0
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("FillEmployeeCombo:- " & ex.Message, Me)
    '    End Try
    'End Sub
    ''SHANI (18 JUN 2015) -- End 


    ''S.SANDEEP [23 DEC 2015] -- START

    ' ''S.SANDEEP [10 DEC 2015] -- START
    ''Protected Sub btnCommitGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitGoals.Click
    ''    Try
    ''        If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
    ''           CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
    ''            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
    ''            Exit Sub
    ''        End If

    ''        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    ''        If dgvData.Rows.Count > 0 Then
    ''            If CBool(mdtFinal.Rows(0)("isfinal")) = True Then
    ''                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, Balance Score Card Planning is already approved for selected employee and period. ."), Me)
    ''                Exit Sub
    ''            End If

    ''            Dim objFMapping As New clsAssess_Field_Mapping
    ''            Dim sMsg As String = String.Empty
    ''            sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
    ''            If sMsg.Trim.Length > 0 Then
    ''                DisplayMessage.DisplayMessage(sMsg, Me)
    ''                objFMapping = Nothing
    ''                Exit Sub
    ''            End If
    ''            If objFMapping IsNot Nothing Then objFMapping = Nothing

    ''            popcommitgoals.Title = "Aruti"
    ''            popcommitgoals.Message = "You are about to commit the goals for (" & cboEmployee.SelectedItem.Text & "), once committed these goals will be transferred to the employee and you won’t be able to Add/Edit/Delete them. Are you sure you want to Commit the Goals?"

    ''            popcommitgoals.Show()
    ''        End If
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError("btnCommitGoals_Click:- " & ex.Message, Me)
    ''    Finally
    ''    End Try
    ''End Sub

    ''Protected Sub btnOpenGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenGoals.Click
    ''    Try
    ''        If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
    ''           CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
    ''            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
    ''            Exit Sub
    ''        End If

    ''        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    ''        If dgvData.Rows.Count > 0 Then
    ''            If CBool(mdtFinal.Rows(0)("isfinal")) <> True Then
    ''                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Balance Score Card Planning is yet not final saved for selected employee and period."), Me)
    ''                Exit Sub
    ''            End If

    ''            Dim objFMapping As New clsAssess_Field_Mapping
    ''            Dim strMsg As String = String.Empty
    ''            strMsg = objFMapping.Is_Update_Progress_Started(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
    ''            objFMapping = Nothing
    ''            If strMsg.Trim.Length > 0 Then
    ''                DisplayMessage.DisplayMessage(strMsg, Me)
    ''                Exit Sub
    ''            End If

    ''            Dim objAnalysis_Master As New clsevaluation_analysis_master
    ''            If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    ''                DisplayMessage.DisplayMessage("Failed to Open Goals because Performance Evaluation has started for these Goals.", Me)
    ''                Exit Sub
    ''            End If

    ''            If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    ''                DisplayMessage.DisplayMessage("Failed to Open Goals because Performance Evaluation has started for these Goals.", Me)
    ''                Exit Sub
    ''            End If

    ''            If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    ''                DisplayMessage.DisplayMessage("Failed to Open Goals because Performance Evaluation has started for these Goals.", Me)
    ''                Exit Sub
    ''            End If
    ''            objAnalysis_Master = Nothing


    ''            popopengoals.Title = "Aruti"
    ''            popopengoals.Message = " Are you sure you want to Open the goals for (" & cboEmployee.SelectedItem.Text & ")? Goals will be removed from the employee until you commit them."

    ''            popopengoals.Show()
    ''        End If
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError("btnOpenGoals_Click:- " & ex.Message, Me)
    ''    End Try
    ''End Sub

    ''Protected Sub popcommitgoals_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popcommitgoals.buttonYes_Click
    ''    Dim xDataTable As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    ''    Dim iAssessorMasterId, iAssessorEmpId As Integer
    ''    Dim sAssessorName As String = ""
    ''    iAssessorMasterId = 0 : iAssessorEmpId = 0
    ''    Try
    ''        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
    ''        Dim objEmpField1 As New clsassess_empfield1_master
    ''        Dim xRow() As DataRow = Nothing
    ''        If xDataTable IsNot Nothing Then
    ''            xRow = xDataTable.Select("OwnerIds <> ''")
    ''        End If
    ''        Dim blnFlag As Boolean = False
    ''        If xRow.Length > 0 Then
    ''            blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow)
    ''        End If

    ''        Dim objStatus As New clsassess_empstatus_tran
    ''        objStatus._Assessoremployeeunkid = iAssessorMasterId
    ''        objStatus._Assessormasterunkid = iAssessorEmpId
    ''        objStatus._Commtents = txtComments.Text
    ''        objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
    ''        objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
    ''        objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
    ''        objStatus._Statustypeid = enObjective_Status.FINAL_SAVE
    ''        objStatus._Isunlock = False
    ''        objStatus._Userunkid = Session("Userid")
    ''        If objStatus.Insert() = False Then
    ''            DisplayMessage.DisplayMessage(objStatus._Message, Me)
    ''            Exit Sub
    ''        Else
    ''            objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False)
    ''            objEmpField1 = Nothing
    ''        End If
    ''        objStatus = Nothing
    ''        Call Fill_Grid()
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError("btnCommitGoals_Click:- " & ex.Message, Me)
    ''    End Try
    ''End Sub
    ' ''S.SANDEEP [10 DEC 2015] -- END

    ''S.SANDEEP [23 DEC 2015] -- END


    ''Shani (26-Sep-2016) -- Start
    ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Protected Sub btnAccomplishmentClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentClose.Click
    '    Try
    '        mblnpopup_Accomplishment = False
    '        popup_GoalAccomplishment.Hide()
    '        If Request.QueryString.Count > 0 Then
    '            Response.Redirect("~/Index.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishmentClose_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentReset.Click
    '    Try
    '        cboAccomplishmentStatus.SelectedValue = 0
    '        Call Fill_Accomplishment_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishmentReset_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentSearch.Click
    '    Try
    '        Call Fill_Accomplishment_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishmentSearch_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishment_No_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishment_No.Click
    '    Try
    '        hdfAccomplishment.Value = ""
    '        popup_Accomplishment_YesNo.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishment_No_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    ''Shani(24-JAN-2017) -- Start
    ''Enhancement : 
    ''Protected Sub btnAccomplishment_yes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishment_yes.Click
    ''    Try
    ''        If hdfAccomplishment.Value.Trim.Length <= 0 Then Exit Sub

    ''        If txtAccomplishment_Message.Text.Trim.Length <= 0 Then
    ''            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 13, "Sorry, Remark cannot be blank."), Me)
    ''            popup_Accomplishment_YesNo.Show()
    ''            Exit Sub
    ''        End If
    ''        Dim strRemark As String = txtAccomplishment_Message.Text


    ''        Dim iAssessorMasterId, iAssessorEmpId As Integer
    ''        Dim sAssessorName As String = ""
    ''        iAssessorMasterId = 0 : iAssessorEmpId = 0
    ''        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)

    ''        If iAssessorMasterId <= 0 Then
    ''            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), Me)
    ''            Exit Sub
    ''        End If
    ''        Dim iAray() As String = hdfAccomplishment.Value.Split(",")
    ''        If iAray.Length = 2 Then
    ''            Dim objEUpdateProgress As New clsassess_empupdate_tran
    ''            objEUpdateProgress._Empupdatetranunkid = iAray(0)
    ''            If iAray(1).ToString = "A" Then
    ''                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
    ''            Else
    ''                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
    ''            End If

    ''            objEUpdateProgress._GoalAccomplishmentRemark = strRemark

    ''            If objEUpdateProgress.Update() Then
    ''                objEUpdateProgress._WebFrmName = mstrModuleName_AppRejGoals
    ''                If iAray(1).ToString = "A" Then
    ''                    objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
    ''                                                              sAssessorName, strRemark, True, _
    ''                                                              iAray(0), _
    ''                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, Session("UserId"))
    ''                Else
    ''                    objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
    ''                                                              sAssessorName, strRemark, False, _
    ''                                                              iAray(0), _
    ''                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, Session("UserId"))
    ''                End If
    ''                Call Fill_Accomplishment_Grid()
    ''            Else
    ''                DisplayMessage.DisplayMessage(objEUpdateProgress._Message, Me)
    ''                Exit Sub
    ''            End If
    ''        End If
    ''        hdfAccomplishment.Value = ""
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError("btnAccomplishment_yes_Click:- " & ex.Message, Me)
    ''    End Try
    ''End Sub
    ' ''Shani (26-Sep-2016) -- End

    'Protected Sub btnAccomplishment_yes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishment_yes.Click
    '    Dim strMessage As String = ""
    '    Try
    '        If hdfAccomplishment.Value.Trim.Length <= 0 Then Exit Sub


    '        Dim iAssessorMasterId, iAssessorEmpId As Integer
    '        Dim sAssessorName As String = ""
    '        iAssessorMasterId = 0 : iAssessorEmpId = 0
    '        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)

    '        If iAssessorMasterId <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), Me)
    '            Exit Sub
    '        End If

    '        For Each dgRow As GridViewRow In dgv_Accomplishment_data.Rows
    '            If CType(dgRow.Cells(2).FindControl("chkSelect"), CheckBox).Checked = True Then
    '                Dim dRow As DataRow = mdtAccomplishmentData.Rows(dgRow.RowIndex)

    '                If CInt(dRow.Item("empupdatetranunkid")) <= 0 Then Exit Sub

    '                Dim mintEmpUpdateTranunkid As Integer = CInt(dRow.Item("empupdatetranunkid"))

    '                If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                    dgRow.Style.Add("background", "red")
    '                    strMessage &= " * " & Language.getMessage(mstrModuleName_AppRejGoals, 6, "Sorry you can not approved this goal,reason goal alrady approved status") & "<br />"
    '                End If

    '                If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                    dgRow.Style.Add("background", "red")
    '                    strMessage &= " * " & Language.getMessage(mstrModuleName_AppRejGoals, 7, "Sorry you can not approved this goal,reason goal alrady rejected status") & "<br />"
    '                End If

    '                objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
    '                objEUpdateProgress._Pct_Completed = CDec(CType(dgRow.Cells(5).FindControl("txtPerComp"), TextBox).Text)
    '                If hdfAccomplishment.Value.ToString = "A" Then
    '                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
    '            Else
    '                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
    '            End If

    '                objEUpdateProgress._GoalAccomplishmentRemark = txtAccomplishmentRemark.Text
    '                If objEUpdateProgress.Update() = False Then
    '                    dgRow.Style.Add("background", "red")
    '                    strMessage &= " * " & objEUpdateProgress._Message & "<br />"
    '                End If
    '            End If
    '        Next


    '        If strMessage.Trim.Length > 0 Then
    '            DisplayMessage.DisplayMessage(strMessage, Me)
    '            Exit Sub
    '        Else
    '                objEUpdateProgress._WebFrmName = mstrModuleName_AppRejGoals
    '            'Sohail (30 Nov 2017) -- Start
    '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '            'objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
    '            '                                              sAssessorName, txtAccomplishmentRemark.Text, _
    '            '                                              IIf(hdfAccomplishment.Value.ToString = "A", True, False), _
    '            '                                              cboPeriod.SelectedItem.Text, _
    '            '                                                  enLogin_Mode.MGR_SELF_SERVICE, 0, Session("UserId"))
    '                    objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
    '                                                          sAssessorName, txtAccomplishmentRemark.Text, _
    '                                                          IIf(hdfAccomplishment.Value.ToString = "A", True, False), _
    '                                                         cboPeriod.SelectedItem.Text, CInt(Session("CompanyUnkId")), _
    '                                                              enLogin_Mode.MGR_SELF_SERVICE, 0, Session("UserId"))
    '            'Sohail (30 Nov 2017) -- End
    '                Call Fill_Accomplishment_Grid()
    '        End If
    '        hdfAccomplishment.Value = ""
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishment_yes_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentApprove.Click
    '    Dim strMessage As String = ""
    '    Try
    '        If mdtAccomplishmentData Is Nothing Then Exit Sub

    '        If txtAccomplishmentRemark.Text.Trim.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 27, "Remark is mandatory inforamation,please enter remark first."), Me)
    '            Exit Sub
    '        End If
    '        hdfAccomplishment.Value = "A"

    '        'S.SANDEEP [15-Feb-2018] -- START
    '        'ISSUE/ENHANCEMENT : {#136}
    '        'lblAccomplishment_Message.Text = Language.getMessage(mstrModuleName_AppRejGoals, 2, "are you sure appoved this goal accomplishment?")
    '        lblAccomplishment_Message.Text = Language.getMessage(mstrModuleName_AppRejGoals, 2, "Are you sure, You want to Appoved this goal accomplishment?")
    '        'S.SANDEEP [15-Feb-2018] -- END

    '        popup_Accomplishment_YesNo.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishmentApprove_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentReject.Click
    '    Try
    '        If mdtAccomplishmentData Is Nothing Then Exit Sub

    '        If txtAccomplishmentRemark.Text.Trim.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 27, "Remark is mandatory inforamation,please enter remark first."), Me)
    '            Exit Sub
    '        End If
    '        hdfAccomplishment.Value = "R"
    '        'S.SANDEEP [15-Feb-2018] -- START
    '        'ISSUE/ENHANCEMENT : {#136}
    '        'lblAccomplishment_Message.Text = Language.getMessage(mstrModuleName_AppRejGoals, 2, "are you sure appoved this goal accomplishment?")
    '        lblAccomplishment_Message.Text = Language.getMessage(mstrModuleName_AppRejGoals, 2, "Are you sure, You want to Reject this goal accomplishment?")
    '        'S.SANDEEP [15-Feb-2018] -- END
    '        popup_Accomplishment_YesNo.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnAccomplishmentReject_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnSubmitForGoalAccomplished_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitForGoalAccomplished.Click
    '    Try
    '        If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation."), Me)
    '            Exit Sub
    '                End If

    '        Dim dsFinal As DataSet = (New clsassess_empfield1_master).GetAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), _
    '                                                                                                  CInt(cboPeriod.SelectedValue), _
    '                                                                                                  0, , _
    '                                                                                                  "List")

    '        If dsFinal Is Nothing OrElse dsFinal.Tables(0).Select("Accomplished_statusId = " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " AND IsGrp = FALSE").Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, Pending data not found in system."), Me)
    '                Exit Sub
    '            End If

    '        Dim objEUpdateProgress As New clsassess_empupdate_tran
    '        Dim enLogin As enLogin_Mode = enLogin_Mode.MGR_SELF_SERVICE
    '        Dim intloginEmpid As Integer = 0
    '        Dim intloginUserid As Integer = 0
    '        If Session("LoginBy") = Global.User.en_loginby.Employee Then
    '            enLogin = enLogin_Mode.EMP_SELF_SERVICE
    '            intloginEmpid = Session("Employeeunkid")
    '        ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
    '            enLogin = enLogin_Mode.MGR_SELF_SERVICE
    '            intloginUserid = Session("UserId")
    '        End If
    '        'Sohail (30 Nov 2017) -- Start
    '        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
    '        'objEUpdateProgress.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
    '        '                                              CInt(cboPeriod.SelectedValue), _
    '        '                                              FinancialYear._Object._YearUnkid, _
    '        '                                              FinancialYear._Object._FinancialYear_Name, _
    '        '                                              FinancialYear._Object._DatabaseName, objEUpdateProgress._Empupdatetranunkid, _
    '        '                                              Company._Object._Companyunkid, _
    '        '                                              ConfigParameter._Object._ArutiSelfServiceURL, _
    '        '                                              enLogin, intloginEmpid, intloginUserid)
    'objEUpdateProgress.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
    '                                              CInt(cboPeriod.SelectedValue), _
    '                                                      Session("Fin_year"), _
    '                                                      Session("FinancialYear_Name"), _
    '                                                      Session("Database_Name"), objEUpdateProgress._Empupdatetranunkid, _
    '                                                      CInt(Session("CompanyUnkId")), _
    '                                                      Session("ArutiSelfServiceURL"), _
    '                                              enLogin, intloginEmpid, intloginUserid)
    '        'Sohail (30 Nov 2017) -- End
    '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Mail send Sucussfully."), Me)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnSubmitForGoalAccomplished_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Shani(24-JAN-2017) -- End


    'S.SANDEEP [29-NOV-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 136
    'Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    If Session("Imagepath") Is Nothing Then Exit Sub
    '    If Session("Imagepath").ToString.Trim <> "" Then
    '        Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
    '        mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
    '        AddDocumentAttachment(f, f.FullName)
    '        Call FillAttachment()
    '        popup_ScanAttchment.Show()
    '    End If
    '    Session.Remove("Imagepath")
    'End Sub

    'Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
    '    Try
    '        If mintDeleteIndex > 0 Then mintDeleteIndex = 0
    '        popup_ScanAttchment.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("popup_AttachementYesNo_buttonNo_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
    '    Try
    '        If mintDeleteIndex >= 0 Then
    '            mdtAttachement.Rows(mintDeleteIndex)("AUD") = "D"
    '            mdtAttachement.AcceptChanges()
    '            mintDeleteIndex = 0
    '            Call FillAttachment()
    '            popup_ScanAttchment.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("popup_AttachementYesNo_buttonYes_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
    '    Try
    '        mblnAttachmentPopup = False
    '        popup_ScanAttchment.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnScanClose_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
    '    Try
    '        Dim objDocument As New clsScan_Attach_Documents
    '        Dim mdsDoc As DataSet
    '        Dim mstrFolderName As String = ""
    '        mdsDoc = objDocument.GetDocFolderName("Docs")
    '        Dim strFileName As String = ""
    '        For Each dRow As DataRow In mdtAttachement.Rows
    '            mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
    '            If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then

    '                'Pinkal (20-Nov-2018) -- Start
    '                'Enhancement - Working on P2P Integration for NMB.
    '                'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))
    '                strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))
    '                'Pinkal (20-Nov-2018) -- End
    '                If System.IO.File.Exists(CStr(dRow("orgfilepath"))) Then
    '                    Dim strPath As String = Session("ArutiSelfServiceURL").ToString
    '                    If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
    '                        strPath += "/"
    '                    End If
    '                    strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

    '                    If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
    '                        System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
    '                    End If

    '                    System.IO.File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
    '                    dRow("fileuniquename") = strFileName
    '                    dRow("filepath") = strPath
    '                    dRow.AcceptChanges()
    '                Else
    '                    'Sohail (23 Mar 2019) -- Start
    '                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                    'DisplayMessage.DisplayError("File connot exitst localpath", Me)
    '                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
    '                    'Sohail (23 Mar 2019) -- End
    '                    Exit Sub
    '                End If
    '            ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
    '                strFileName = dRow("fileuniquename").ToString()
    '                Dim strServerPath As String = dRow("filepath").ToString()
    '                Dim strPath As String = Session("ArutiSelfServiceURL").ToString

    '                If strServerPath.Contains(strPath) Then
    '                    strServerPath = strServerPath.Replace(strPath, "")
    '                    If Strings.Left(strServerPath, 1) <> "/" Then
    '                        strServerPath = "~/" & strServerPath
    '                    Else
    '                        strServerPath = "~" & strServerPath

    '                    End If

    '                    If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
    '                        strPath += "/"
    '                    End If
    '                    strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

    '                    If System.IO.File.Exists(Server.MapPath(strServerPath)) Then

    '                        If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
    '                            System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
    '                        End If

    '                        System.IO.File.Move(Server.MapPath(strServerPath), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))

    '                        dRow("filepath") = strPath
    '                        dRow.AcceptChanges()
    '                    Else
    '                        'Sohail (23 Mar 2019) -- Start
    '                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                        'DisplayMessage.DisplayError("File connot exitst localpath", Me)
    '                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
    '                        'Sohail (23 Mar 2019) -- End
    '                        Exit Sub
    '                    End If
    '                Else
    '                    'Sohail (23 Mar 2019) -- Start
    '                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                    'DisplayMessage.DisplayError("File connot exitst localpath", Me)
    '                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
    '                    'Sohail (23 Mar 2019) -- End
    '                    Exit Sub
    '                End If

    '            ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
    '                Dim strFilepath As String = dRow("filepath").ToString()
    '                Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
    '                If strFilepath.Contains(strArutiSelfService) Then
    '                    strFilepath = strFilepath.Replace(strArutiSelfService, "")
    '                    If Strings.Left(strFilepath, 1) <> "/" Then
    '                        strFilepath = "~/" & strFilepath
    '                    Else
    '                        strFilepath = "~" & strFilepath
    '                    End If

    '                    If System.IO.File.Exists(Server.MapPath(strFilepath)) Then
    '                        System.IO.File.Delete(Server.MapPath(strFilepath))
    '                    Else
    '                        'Sohail (23 Mar 2019) -- Start
    '                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                        'DisplayMessage.DisplayError("File connot exitst localpath", Me)
    '                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
    '                        'Sohail (23 Mar 2019) -- End
    '                        Exit Sub
    '                    End If
    '                Else
    '                    'Sohail (23 Mar 2019) -- Start
    '                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                    'DisplayMessage.DisplayError("File connot exitst localpath", Me)
    '                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
    '                    'Sohail (23 Mar 2019) -- End
    '                    Exit Sub
    '                End If

    '            End If
    '        Next

    '        objDocument._Datatable = mdtAttachement
    '        objDocument.InsertUpdateDelete_Documents()

    '        If objDocument._Message <> "" Then
    '            DisplayMessage.DisplayError(objDocument._Message, Me)
    '            Exit Sub
    '        End If

    '        mblnAttachmentPopup = False
    '        popup_ScanAttchment.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnScanSave_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [29-NOV-2017] -- END


    'S.SANDEEP |08-JAN-2019| -- START
    'Protected Sub btnUnlockEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlockEmployee.Click
    '    popup_unlockemp.Show()
    'End Sub
    'S.SANDEEP |08-JAN-2019| -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Protected Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP |12-FEB-2019| -- END

#End Region

#Region "GridView Event"

    'SHANI (18 JUN 2015) -- Start
    '

    'Protected Sub dgvData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvData.RowCommand
    '    Try
    '        If e.CommandName = "UpdateProgress" Then

    '            'S.SANDEEP [04 Jan 2016] -- START
    '            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
    '               CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
    '                'Pinkal (15-Mar-2019) -- Start
    '                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
    '                Else
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
    '                End If
    '                'Pinkal (15-Mar-2019) -- End
    '                Exit Sub
    '            End If

    '            'S.SANDEEP |22-JUL-2019| -- START
    '            'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
    '            'Dim objEval As New clsevaluation_analysis_master
    '            'If objEval.isExist(enAssessmentMode.SELF_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
    '            '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
    '            '    objEval = Nothing
    '            '    Exit Sub
    '            'End If
    '            'If objEval.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
    '            '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
    '            '    objEval = Nothing
    '            '    Exit Sub
    '            'End If
    '            'If objEval.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
    '            '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
    '            '    objEval = Nothing
    '            '    Exit Sub
    '            'End If
    '            'If objEval IsNot Nothing Then objEval = Nothing
    '            'S.SANDEEP [04 Jan 2016] -- END

                'Dim objEval As New clsevaluation_analysis_master
    '            If objEval.isExist(enAssessmentMode.SELF_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , , , , , False) = True Then
                '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                '    objEval = Nothing
                '    Exit Sub
                'End If
    '            If objEval.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , , , , , False) = True Then
                '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                '    objEval = Nothing
                '    Exit Sub
                'End If
    '            'S.SANDEEP |26-AUG-2019| -- START
    '            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '            'If objEval.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
    '            If objEval.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , , , , , False) = True Then
    '                'S.SANDEEP |26-AUG-2019| -- END
                '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                '    objEval = Nothing
                '    Exit Sub
                'End If
                'If objEval IsNot Nothing Then objEval = Nothing

    '            'S.SANDEEP |22-JUL-2019| -- END

    '            Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            Call UpdateProgressLoad(gridRow)
    '            popup_UpdateProgress.Show()
    '            mblnUpdateProgress = True
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvData_RowCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI (18 JUN 2015) -- End
    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                Dim imgAdd As New ImageButton()
                imgAdd.ID = "imgAdd"
                imgAdd.Attributes.Add("Class", "objAddBtn")
                'Shani [ 10 DEC 2014 ] -- START
                '
                imgAdd.Attributes.Add("onclick", "getscrollPosition()")
                'Shani [ 10 DEC 2014 ] -- END
                imgAdd.CommandName = "objAdd"
                imgAdd.ImageUrl = "~/images/add_16.png"
                imgAdd.ToolTip = "New"
                AddHandler imgAdd.Click, AddressOf imgAdd_Click
                e.Row.Cells(0).Controls.Add(imgAdd)

                Dim imgEdit As New ImageButton()
                imgEdit.ID = "imgEdit"
                imgEdit.Attributes.Add("Class", "objAddBtn")
                'Shani [ 10 DEC 2014 ] -- START
                '
                imgEdit.Attributes.Add("onclick", "getscrollPosition()")
                'Shani [ 10 DEC 2014 ] -- END
                imgEdit.CommandName = "objEdit"
                imgEdit.ImageUrl = "~/images/Edit.png"
                imgEdit.ToolTip = "Edit"
                AddHandler imgEdit.Click, AddressOf imgEdit_Click
                e.Row.Cells(1).Controls.Add(imgEdit)

                Dim imgDelete As New ImageButton()
                imgDelete.ID = "imgDelete"
                imgDelete.Attributes.Add("Class", "objAddBtn")
                'Shani [ 10 DEC 2014 ] -- START
                '
                imgDelete.Attributes.Add("onclick", "getscrollPosition()")
                'Shani [ 10 DEC 2014 ] -- END
                imgDelete.CommandName = "objDelete"
                imgDelete.ImageUrl = "~/images/remove.png"
                imgDelete.ToolTip = "Delete"
                AddHandler imgDelete.Click, AddressOf imgDelete_Click
                e.Row.Cells(2).Controls.Add(imgDelete)


                'SHANI (18 JUN 2015) -- Start
                'Dim lnk As New LinkButton
                'lnk.ID = "lnkUpdateProgress"
                'lnk.CommandName = "UpdateProgress"

                ''SHANI (23 JUN 2015) -- Start
                ''lnk.Text = CType(Me.ViewState("mdtFinal"), DataTable).Rows(e.Row.RowIndex)("vuprogress")
                'lnk.Text = ""
                'lnk.CssClass = "updatedata123"
                'lnk.Style.Add("color", "red")
                'lnk.Style.Add("font-size", "20px")
                ''SHANI (23 JUN 2015) -- End
                'e.Row.Cells(mintUpdateProgressIndex).Controls.Add(lnk)
                'SHANI (18 JUN 2015) -- End 

                ''Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                ''Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'Dim intIndex As Integer = 0
                'intIndex = 0 'CType(......, DataTable).Columns("St_Date").ExtendedProperties("index")

                ''If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                ''    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                ''End If

                'intIndex = 0 'CType(......, DataTable).Columns("Ed_Date").ExtendedProperties("index")
                ''If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                ''    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                ''End If
                ''Pinkal (16-Apr-2016) -- End

                ''S.SANDEEP |18-JAN-2019| -- START
                'If mintGoalValueColIndex < 0 Then mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
                ''S.SANDEEP |05-MAR-2019| -- START
                ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If mintUoMColIndex < 0 Then mintUoMColIndex = Me.ViewState("mintUoMColIndex")
                'If mstrUoMColName.Trim.Length <= 0 Then mstrUoMColName = Me.ViewState("mstrUoMColName")
                ''S.SANDEEP |05-MAR-2019| -- END

                'If mintGoalValueColIndex <> -1 Then
                '    'S.SANDEEP |30-JAN-2019| -- START
                '    If e.Row.Cells(mintGoalValueColIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(mintGoalValueColIndex).Text.Trim <> "&nbsp;" Then
                '        'S.SANDEEP |12-FEB-2019| -- START
                '        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                '        'e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency"))
                '        If mintUoMColIndex < 0 Then
                '    e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency"))
                '        Else
                '            Dim iUoM As String = ""
                '            'If CType(......, DataTable).Rows.Count > 0 Then
                '            '    iUoM = CType(......, DataTable).Rows(e.Row.RowIndex)(mstrUoMColName).ToString()
                '            'End If
                '            e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency")) & " " & iUoM
                '        End If
                '        'S.SANDEEP |12-FEB-2019| -- END
                'End If
                '    'S.SANDEEP |30-JAN-2019| -- END
                'End If
                ''S.SANDEEP |18-JAN-2019| -- END

                If mintUoMColIndex < 0 Then mintUoMColIndex = Me.ViewState("mintUoMColIndex")
                If mintGoalValueColIndex <> -1 Then
                    If e.Row.Cells(mintGoalValueColIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(mintGoalValueColIndex).Text.Trim <> "&nbsp;" Then
                        If mintUoMColIndex < 0 Then
                            If mintGoalValueColIndex <> -1 Then e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency"))
                        Else
                            If mintGoalValueColIndex <> -1 Then e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency")) & " " & dgvData.DataKeys(e.Row.RowIndex)("UoMType").ToString()
                            End If
                        End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvData_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub

    'Protected Sub dgvAppRejdata_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAppRejdata.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
    '            If CBool(mdtFinal.Rows(e.Row.RowIndex)("isfinal")) = True Then
    '                e.Row.Style.Add("color", "blue")
    '            ElseIf CInt(mdtFinal.Rows(e.Row.RowIndex)("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
    '                e.Row.Style.Add("color", "Green")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvAppRejdata_RowDataBound :- " & ex.Message, Me)
    '    End Try

    'End Sub

    'Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand
    '    Try
    '        If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
    '            If e.Item.ItemIndex < 0 Then Exit Sub

    '            If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '            CType(e.Item.Cells(0).FindControl("imgEdit"), ImageButton).Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
    '            CType(e.Item.Cells(1).FindControl("imgDelete"), ImageButton).Visible = CBool(Session("AllowToDeletePercentCompletedEmployeeGoals"))
    '            End If

    '            If e.CommandName = "objEdit" Then
    '                'Shani (26-Sep-2016) -- Start
    '                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
    '                    If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 10, "Sorry, You can not edit this recoed.reason: this record is approved "), Me)
    '                        Exit Sub
    '                    End If
    '                    If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected "), Me)
    '                        Exit Sub
    '                    End If
    '                End If
    '                'Shani (26-Sep-2016) -- End
    '                mintEmpUpdateTranunkid = CInt(e.Item.Cells(7).Text)
    '                objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
    '                Call SetTagValue(mdtGoalAccomplishment, True)
    '                Call GetUpdateValue()
    '            ElseIf e.CommandName = "objDelete" Then
    '                'Shani (26-Sep-2016) -- Start
    '                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
    '                    If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 10, "Sorry, You can not edit this recoed.reason: this record is approved "), Me)
    '                        Exit Sub
    '                    End If
    '                    If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected "), Me)
    '                        Exit Sub
    '                    End If
    '                End If
    '                'Shani (26-Sep-2016) -- End
    '                mintEmpUpdateTranunkid = CInt(e.Item.Cells(7).Text)
    '                lblUpdateMessages.Text = Language.getMessage(mstrModuleName_upd_percent, 7, "Are you sure you want to delete this progress for the selected employee?")
    '                hdf_UpdateYesNo.Value = "DELETE"
    '                popup_UpdateYesNO.Show()
    '                'S.SANDEEP [29-NOV-2017] -- START
    '                'ISSUE/ENHANCEMENT : REF-ID # 136
    '            ElseIf e.CommandName = "objAttach" Then
    '                mintEmpUpdateTranunkid = CInt(e.Item.Cells(7).Text)
    '                Dim objScanAttachment As New clsScan_Attach_Documents
    '                objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
    '                Dim xRow As DataRow() = objScanAttachment._Datatable.Select("scanattachrefid = '" & enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & mstrModuleName_upd_percent & "'")
    '                If xRow IsNot Nothing AndAlso xRow.Count > 0 Then
    '                    mdtAttachement = xRow.CopyToDataTable
    '                Else
    '                    mdtAttachement = objScanAttachment._Datatable.Clone
    '                End If
    '                mblnAttachmentPopup = True
    '                Call FillAttachment()
    '                popup_ScanAttchment.Show()
    '                'S.SANDEEP [29-NOV-2017] -- END
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvHistory_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Protected Sub dgv_Accomplishment_data_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgv_Accomplishment_data.RowCommand
    '    Dim intUpdateTranUnkid As Integer = 0
    '    Try
    '        If e.CommandName = "Approved" Then
    '            Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            intUpdateTranUnkid = mdtAccomplishmentData.Rows(gridRow.RowIndex)("empupdatetranunkid")
    '            If intUpdateTranUnkid <= 0 Then Exit Sub

    '            If mdtAccomplishmentData Is Nothing Then Exit Sub

    '            Dim drow() As DataRow = mdtAccomplishmentData.Select("empupdatetranunkid ='" & intUpdateTranUnkid & "'")
    '            If drow.Length <= 0 Then Exit Sub

    '            If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 6, "Sorry you can not approved this goal,reason goal already approved status"), Me)
    '                Exit Sub
    '            End If

    '            If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 7, "Sorry you can not approved this goal,reason goal already rejected status"), Me)
    '                Exit Sub
    '            End If
    '            hdfAccomplishment.Value = CStr(intUpdateTranUnkid) & ",A"

    '            lblAccomplishment_Message.Text = Language.getMessage(mstrModuleName, 2, "are you sure appoved this goal accomplishment?")
    '            popup_Accomplishment_YesNo.Show()
    '        ElseIf e.CommandName = "Rejected" Then
    '            Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            intUpdateTranUnkid = mdtAccomplishmentData.Rows(gridRow.RowIndex)("empupdatetranunkid")
    '            If intUpdateTranUnkid <= 0 Then Exit Sub

    '            If mdtAccomplishmentData Is Nothing Then Exit Sub

    '            Dim drow() As DataRow = mdtAccomplishmentData.Select("empupdatetranunkid ='" & intUpdateTranUnkid & "'")
    '            If drow.Length <= 0 Then Exit Sub

    '            If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 8, "Sorry you can not reject this goal,reason goal already approved status"), Me)
    '                Exit Sub
    '            End If

    '            If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 9, "Sorry you can not reject this goal,reason goal already rejected status"), Me)
    '                Exit Sub
    '            End If

    '            hdfAccomplishment.Value = CStr(intUpdateTranUnkid) & ",R"

    '            lblAccomplishment_Message.Text = Language.getMessage(mstrModuleName_AppRejGoals, 3, "are you sure rejected this goal accomplishment?")
    '            popup_Accomplishment_YesNo.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgv_Accomplishment_data_RowCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub
    ''Shani (26-Sep-2016) -- End

    ''Shani(24-JAN-2017) -- Start
    ''Enhancement : 
    'Protected Sub dgv_Accomplishment_data_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgv_Accomplishment_data.RowDataBound

    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If mdtAccomplishmentData Is Nothing Then Exit Sub
    '            Dim drow As DataRow = mdtAccomplishmentData.Rows(e.Row.RowIndex)

    '            If CBool(drow.Item("IsGrp")) = True Then
    '                CType(e.Row.Cells(2).FindControl("chkSelect"), CheckBox).Visible = False
    '                CType(e.Row.Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '                'S.SANDEEP |12-FEB-2019| -- START
    '                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '                'e.Row.Cells(2).CssClass = "MainGroupHeaderStyle"
    '                'e.Row.Cells(3).CssClass = "MainGroupHeaderStyle"
    '                'e.Row.Cells(4).CssClass = "MainGroupHeaderStyle"
    '                'e.Row.Cells(5).CssClass = "MainGroupHeaderStyle"
    '                'e.Row.Cells(6).CssClass = "MainGroupHeaderStyle"
    '                ''S.SANDEEP [07-APR-2017] -- START
    '                ''ISSUE/ENHANCEMENT : Allowed Zero Score is Removed (BY Mistake)
    '                'e.Row.Cells(7).CssClass = "MainGroupHeaderStyle"
    '                ''S.SANDEEP [07-APR-2017] -- END

    '                'e.Row.Cells(3).ColumnSpan = 4

    '                'S.SANDEEP |18-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : PROGRESS UPDATE COL. SPAN
    '                'e.Row.Cells(2).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                'e.Row.Cells(3).CssClass = "MainGroupHeaderStyleLeftAlign"
    '                'e.Row.Cells(4).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                'e.Row.Cells(5).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                'e.Row.Cells(6).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                'e.Row.Cells(7).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"

                    'e.Row.Cells(2).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
                    'e.Row.Cells(3).CssClass = "MainGroupHeaderStyleLeftAlign"
    '                e.Row.Cells(4).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(4).Visible = False
    '                e.Row.Cells(5).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(5).Visible = False
    '                e.Row.Cells(6).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(6).Visible = False
    '                e.Row.Cells(7).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(7).Visible = False
    '                'S.SANDEEP |18-JUL-2019| -- END

    '                'S.SANDEEP |12-FEB-2019| -- END

    '                e.Row.Cells(5).Text = ""
    '                e.Row.Cells(6).Text = ""
    '            Else
    '                If CInt(drow.Item("Accomplished_statusId")) <> clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending Then
    '                    CType(e.Row.Cells(2).FindControl("chkSelect"), CheckBox).Visible = False
    '                End If
    '                'e.Row.Cells(4).Text = ""

    '                e.Row.Cells(4).Text = drow.Item("Goal_status")

    '            End If

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgv_Accomplishment_data_RowDataBound:- " & ex.Message, Me)
    '    End Try
    'End Sub
    ''Shani(24-JAN-2017) -- End

    ''S.SANDEEP [29-NOV-2017] -- START
    ''ISSUE/ENHANCEMENT : REF-ID # 136
    'Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
    '    Try
    '        If e.Item.ItemIndex >= 0 Then
    '            Dim xrow() As DataRow = Nothing
    '            'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
    '            If CInt(e.Item.Cells(3).Text) > 0 Then
    '                xrow = mdtAttachement.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
    '            Else
    '                xrow = mdtAttachement.Select("GUID = '" & e.Item.Cells(2).Text & "'")
    '            End If

    '            If e.CommandName = "Delete" Then
    '                If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
    '                    popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
    '                    mintDeleteIndex = mdtAttachement.Rows.IndexOf(xrow(0))
    '                    'popup_ScanAttchment.Show()
    '                    popup_AttachementYesNo.Show()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvResponse_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [29-NOV-2017] -- END


    'S.SANDEEP |18-JAN-2019| -- START
    Protected Sub dgvData_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvData.PreRender
        Try
            MergeRows(dgvData)
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvData_PreRender : " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JAN-2019| -- END

#End Region

#Region "Control Event"

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("AddRowIndex") = row.RowIndex

            'S.SANDEEP |08-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
            If IsValidGoalOperation() = False Then Exit Sub
            'S.SANDEEP |08-DEC-2020| -- END

            Genrate_AddmanuItem()
            'Shani [ 10 DEC 2014 ] -- START
            '
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & pnl_dgvData.ClientID & ");", True)
            'Shani [ 10 DEC 2014 ] -- END
            popAddButton.X = CInt(hdf_locationx.Value)
            popAddButton.Y = CInt(hdf_locationy.Value)
            popAddButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("imgAdd_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("EditRowIndex") = row.RowIndex

            'S.SANDEEP |08-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
            If IsValidGoalOperation() = False Then Exit Sub
            'S.SANDEEP |08-DEC-2020| -- END

            Genrate_EditmanuItem()
            'Shani [ 10 DEC 2014 ] -- START
            '
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & pnl_dgvData.ClientID & ");", True)
            'Shani [ 10 DEC 2014 ] -- END

            'S.SANDEEP |14-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            'popEditButton.X = CInt(hdf_locationx.Value)
            'popEditButton.Y = CInt(hdf_locationy.Value)
            If hdf_locationx.Value.Trim.Length > 0 AndAlso hdf_locationy.Value.Trim.Length > 0 Then
            popEditButton.X = CInt(hdf_locationx.Value)
            popEditButton.Y = CInt(hdf_locationy.Value)
            End If
            'S.SANDEEP |14-MAR-2020| -- END
            popEditButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("imgEdit_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("DeleteRowIndex") = row.RowIndex

            'S.SANDEEP |08-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
            If IsValidGoalOperation() = False Then Exit Sub
            'S.SANDEEP |08-DEC-2020| -- END

            Genrate_DeletemanuItem()
            'Shani [ 10 DEC 2014 ] -- START
            '
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & pnl_dgvData.ClientID & ");", True)
            'Shani [ 10 DEC 2014 ] -- END
            popDeleteButton.X = CInt(hdf_locationx.Value)
            popDeleteButton.Y = CInt(hdf_locationy.Value)
            popDeleteButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("imgDelete_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            'Dim iItem As DataListItem = TryCast(ilnk.NamingContainer, DataListItem)
            If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation."), Me)
                Exit Sub
            End If

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            'Select CInt(Session("CascadingTypeId"))
            '    Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING
            'If (New clsassess_empfield1_master).IsGoalOwner(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue)) = False Then
            '    Dim StrMsg As String = ""
            '    StrMsg = (New clsassess_empfield1_master).AllowedToAddGoals(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            '    If StrMsg.Trim.Length > 0 Then
            '        DisplayMessage.DisplayMessage(StrMsg, Me)
            '        Exit Sub
            '    End If
            'End If
            'End Select
            'S.SANDEEP |27-NOV-2020| -- END

            'S.SANDEEP |08-JAN-2019| -- START
            If CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid")) <= 0 Then
                Dim objPeriod As New clscommom_period_Tran
                Dim intDaysAfter As Integer = 0 : Dim iStartDate As DateTime = Nothing
                objPeriod._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                intDaysAfter = objPeriod._LockDaysAfter
                If intDaysAfter > 0 Then
                    iStartDate = objPeriod._Start_Date.AddDays(intDaysAfter)
                End If
                objPeriod = Nothing
                If iStartDate <> Nothing Then
                    If ConfigParameter._Object._CurrentDateAndTime.Date > iStartDate Then
                        If LockEmployee() Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, you cannot do planning as last date for planning has been passed.") & Language.getMessage(mstrModuleName, 35, "Please contact your administrator/manager to do futher operation on it."), Me)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |08-JAN-2019| -- END

            Select Case CInt(ilnk.CommandArgument.ToString.Split("|")(1))
                Case 1
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField1_load(-1, enAction.ADD_CONTINUE)
                    pop_objfrmAddEditEmpField1.Show()
                Case 2
                    Dim mintOwrField2ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField2_load(-1, enAction.ADD_CONTINUE, mintOwrField2ParentId)
                    pop_objfrmAddEditEmpField2.Show()
                Case 3
                    Dim mintOwrField3ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield2unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield2unkid"))
                    Dim mintOwrField3MainParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField3_load(-1, enAction.ADD_CONTINUE, mintOwrField3ParentId, mintOwrField3MainParentId)
                    pop_objfrmAddEditEmpField3.Show()
                Case 4
                    Dim mintOwrField4ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield3unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield3unkid"))
                    Dim mintOwrField4MainParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield2unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield2unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField4_load(-1, enAction.ADD_CONTINUE, mintOwrField4ParentId, mintOwrField4MainParentId)
                    pop_objfrmAddEditEmpField4.Show()
                Case 5
                    Dim mintOwrField5ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield4unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield4unkid"))
                    Dim mintOwrField5MainParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("AddRowIndex")))("empfield3unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield3unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField5_load(-1, enAction.ADD_CONTINUE, mintOwrField5ParentId, mintOwrField5MainParentId)
                    pop_objfrmAddEditEmpField5.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAdd_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim irow As DataListItem = CType(ilnk.NamingContainer, DataListItem)
            Dim hdf As HiddenField = CType(dlmnuEdit.Items(irow.ItemIndex).FindControl("objEditOrignalName"), HiddenField)
            'Dim mdtFinal As DataTable = Nothing 'CType(......, DataTable)

            Select Case CInt(ilnk.CommandArgument.ToString.Split("|")(1))
                Case 1
                    If CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid")) > 0 Then
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField1_load(CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid")), enAction.EDIT_ONE)
                        pop_objfrmAddEditEmpField1.Show()
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 2
                    If CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid")) > 0 Then
                        Dim mintOwrField2ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField2_load(CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid")), enAction.EDIT_ONE, mintOwrField2ParentId)
                        pop_objfrmAddEditEmpField2.Show()
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                         " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 3
                    If CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid")) > 0 Then
                        Dim mintOwrField3ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid"))
                        Dim mintOwrField3MainParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField3_load(CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid")), enAction.EDIT_ONE, mintOwrField3ParentId, mintOwrField3MainParentId)
                        pop_objfrmAddEditEmpField3.Show()
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 4
                    If CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid")) > 0 Then
                        Dim mintOwrField4ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid"))
                        Dim mintOwrField4MainParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField4_load(CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid")), enAction.EDIT_ONE, mintOwrField4ParentId, mintOwrField4MainParentId)
                        pop_objfrmAddEditEmpField4.Show()
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 5
                    'SHANI (24 APR 2015)-START
                    'ISSUE REPORTED BY ANDREW ON NWB TRAINING ON PA
                    'If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield5unkid")) > 0 > 0 Then
                    If CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield5unkid")) > 0 Then
                        'SHANI (24 APR 2015)--END
                        Dim mintOwrField5ParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid"))
                        Dim mintOwrField5MainParentId As Integer = CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid")) 'CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField5_load(CInt(dgvData.DataKeys(CInt(Me.ViewState("EditRowIndex")))("empfield5unkid")), enAction.EDIT_ONE, mintOwrField5ParentId, mintOwrField5MainParentId)
                        pop_objfrmAddEditEmpField5.Show()
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Language.getMessage(mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkEdit_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim irow As DataListItem = CType(ilnk.NamingContainer, DataListItem)
            Dim hdf As HiddenField = CType(dlmnuDelete.Items(irow.ItemIndex).FindControl("objDeleteOrignalName"), HiddenField)
            Me.ViewState("DeleteScreenId") = ilnk.CommandArgument.ToString().Trim
            Me.ViewState("DeleteColumnName") = hdf.Value

            'S.SANDEEP |27-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
            'Dim xMsg As String = ""
            'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            'If xMsg.Trim.Length > 0 Then
            '    DisplayMessage.DisplayMessage(xMsg, Me)
            '    Exit Sub
            'End If
            'S.SANDEEP |27-NOV-2020| -- END

            lblTitle.Text = "Aruti"
            Dim iMsg As String = Language.getMessage(mstrModuleName, 19, "You are about to remove the information from employee level, this will delete all child linked to this parent") & vbCrLf & Language.getMessage(mstrModuleName, 20, "Are you sure you want to delete?")
            lblMessage.Text = iMsg
            txtMessage.Text = ""
            popup_YesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkDelete_Click :-" & ex.Message, Me)
        End Try
    End Sub

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Protected Sub mnuAppRejGoalAccomplished_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAppRejGoalAccomplished.Click
    '    Try
    '        If CInt(cboEmployee.SelectedValue) <= 0 Or _
    '               CInt(cboPeriod.SelectedValue) <= 0 Then
    '            'Pinkal (15-Mar-2019) -- Start
    '            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 14, "Period is mandatory information. Please select Period to continue."), Me)
    '            Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 1, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
    '            End If
    '            'Pinkal (15-Mar-2019) -- End
    '            Exit Sub
    '        End If

    '        'S.SANDEEP |12-FEB-2019| -- START
    '        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '        Dim dsList As DataSet = (New clsMasterData).Get_Goal_Accomplishement_Status("List", True)
    '        'Dim dsList As DataSet = (New clsMasterData).Get_Goal_Accomplishement_Status("List", False)

    '        'S.SANDEEP |12-FEB-2019| -- END

    '        With cboAccomplishmentStatus
    '            .DataValueField = "Id"
    '            .DataTextField = "Name"
    '            .DataSource = dsList.Tables("List")
    '            .DataBind()
    '            .SelectedValue = 0
    '        End With
    '        Call Fill_Accomplishment_Grid()
    '        mblnpopup_Accomplishment = True
    '        popup_GoalAccomplishment.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("mnuAppRejGoalAccomplished_Click:- " & ex.Message, Me)
    '    End Try

    'End Sub
    'Shani (26-Sep-2016) -- End


#End Region

#Region "ComboBox Event(S)"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True,False, CInt(IIf(cboPerspective.SelectedValue = "", 0, cboPerspective.SelectedValue)))
            With cboFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue1_SelectedIndexChanged(cboFieldValue1, Nothing)

            'S.SANDEEP [01 JUL 2015] -- START
            Me.ViewState("mdtFinal") = Nothing
            'S.SANDEEP [01 JUL 2015] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                BtnSearch_Click(New Object(), New EventArgs())
            End If
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError("cboEmployee_SelectedIndexChanged :-" & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |22-MAR-2019| -- START
    'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
    Protected Sub cboPerspective_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPerspective.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, False, CInt(IIf(cboPerspective.SelectedValue = "", 0, cboPerspective.SelectedValue)))
            With cboFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue1_SelectedIndexChanged(cboFieldValue1, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError("cboPerspective_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |22-MAR-2019| -- END

    Protected Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField2.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue1.SelectedValue), "List", True)
            With cboFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue2_SelectedIndexChanged(cboFieldValue2, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue1_SelectedIndexChanged :-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboFieldValue2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue2.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField3.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue2.SelectedValue), "List", True)
            With cboFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue3_SelectedIndexChanged(cboFieldValue3, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue2_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField4.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue3.SelectedValue), "List", True)
            With cboFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue4_SelectedIndexChanged(cboFieldValue4, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue3_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub cboFieldValue4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFieldValue4.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objEmpField5.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue4.SelectedValue), "List", True)
            With cboFieldValue5
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue4_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboEmpField2EmpFieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField2EmpFieldValue1.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField2EmpFieldValue1.SelectedValue)
            If CInt(cboEmpField2EmpFieldValue1.SelectedValue) > 0 Then
                Dim objEmpField1 As New clsassess_empfield1_master
                objEmpField1._Empfield1unkid = CInt(cboEmpField2EmpFieldValue1.SelectedValue)
                objEmpField1 = Nothing
            End If
            Me.ViewState("mintEmpField2ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError("cboEmpField2EmpFieldValue1_SelectedIndexChanged :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub cboEmpField3EmpFieldValue2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField3EmpFieldValue2.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField3EmpFieldValue2.SelectedValue)
            If CInt(cboEmpField3EmpFieldValue2.SelectedValue) > 0 Then
                Dim objEmpField2 As New clsassess_empfield2_master
                Dim objEmpField1 As New clsassess_empfield1_master
                objEmpField2._Empfield2unkid = CInt(cboEmpField3EmpFieldValue2.SelectedValue)
                objEmpField1._Empfield1unkid = objEmpField2._Empfield1unkid
                objEmpField3txtEmpField1.Text = objEmpField1._Field_Data
                objEmpField2 = Nothing : objEmpField2 = Nothing
            Else
                objEmpField3txtEmpField1.Text = ""
            End If
            Me.ViewState("mintEmpField3ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError("cboEmpFieldValue2_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboEmpField4EmpFieldValue3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField4EmpFieldValue3.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField4EmpFieldValue3.SelectedValue)
            If CInt(cboEmpField4EmpFieldValue3.SelectedValue) > 0 Then
                Dim objField3 As New clsassess_empfield3_master
                Dim objField2 As New clsassess_empfield2_master
                Dim objField1 As New clsassess_empfield1_master
                objField3._Empfield3unkid = CInt(cboEmpField4EmpFieldValue3.SelectedValue)
                objField2._Empfield2unkid = objField3._Empfield2unkid
                objField1._Empfield1unkid = objField2._Empfield1unkid
                objEmpField4txtEmpField2.Text = objField2._Field_Data
                objEmpField4txtEmpField1.Text = objField1._Field_Data
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                objEmpField4txtEmpField1.Text = ""
                objEmpField4txtEmpField2.Text = ""
            End If
            Me.ViewState("mintEmpField4ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError("cboEmpField4EmpFieldValue3_SelectedIndexChanged" & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboEmpField5EmpFieldValue4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpField5EmpFieldValue4.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField5EmpFieldValue4.SelectedValue)
            If CInt(cboEmpField5EmpFieldValue4.SelectedValue) > 0 Then
                Dim objField4 As New clsassess_empfield4_master
                Dim objField3 As New clsassess_empfield3_master
                Dim objField2 As New clsassess_empfield2_master
                Dim objField1 As New clsassess_empfield1_master
                objField4._Empfield4unkid = CInt(cboEmpField5EmpFieldValue4.SelectedValue)
                objField3._Empfield3unkid = objField4._Empfield3unkid
                objField2._Empfield2unkid = objField3._Empfield2unkid
                objField1._Empfield1unkid = objField2._Empfield1unkid

                objEmpField5txtEmpField1.Text = objField1._Field_Data
                objEmpField5txtEmpField2.Text = objField2._Field_Data
                objEmpField5txtEmpField3.Text = objField3._Field_Data
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                objEmpField5txtEmpField1.Text = "" : objEmpField5txtEmpField2.Text = "" : objEmpField5txtEmpField3.Text = ""
            End If
            Me.ViewState("mintEmpField5ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError("cboEmpField5EmpFieldValue4_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Shani (16-Sep-2016) -- Start
    'Enhancement - 
    Protected Sub cboEmpField1FieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField1FieldValue1.SelectedIndexChanged
        Try
            If Session("CascadingTypeId") <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                Dim dsList As DataSet = (New clsassess_perspective_master).getComboListByOwnerUnkid(CInt(cboEmpField1FieldValue1.SelectedValue), "List")
                With cboEmpField1Perspective
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("cboEmpField1FieldValue1_SelectedIndexChanged:- " & ex.Message, Me)
        End Try
    End Sub
    'Shani (16-Sep-2016) -- End

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    'Protected Sub cboChangeBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim iList As List(Of ListItem) = cboChangeBy.Items.Cast(Of ListItem)().ToList()

            'Select Case cboChangeBy.SelectedIndex
    '            Case 0  'SELECT
    '                txtNewPercentage.Enabled = False
    '                txtNewValue.Enabled = False
    '                txtTotalPercentage.Enabled = False
    '                txtTotalValue.Enabled = False
    '            Case 1  'PERCENTAGE
    '                txtNewPercentage.Enabled = True
    '                txtNewValue.Enabled = False
    '                txtTotalPercentage.Enabled = True
    '                txtTotalValue.Enabled = False
    '            Case 2 'VALUE
    '                txtNewPercentage.Enabled = False
    '                txtNewValue.Enabled = True
    '                txtTotalPercentage.Enabled = False
    '                txtTotalValue.Enabled = True
            'End Select

    '        'Select Case cboChangeBy.SelectedIndex
    '        '    Case 0
    '        '        txtChangeBy.Enabled = False
    '        '        objUpdateCaption1.Text = ""
    '        '        objUpdateCaption2.Text = ""
    '        '        'S.SANDEEP |18-JAN-2019| -- START
    '        '        objlblCaption3.Text = ""
    '        '        objlblCaption4.Text = ""
    '        '        'S.SANDEEP |18-JAN-2019| -- END
    '        '    Case 1
    '        '        txtChangeBy.Enabled = True
    '        '        'S.SANDEEP |18-JAN-2019| -- START
    '        '        'objUpdateCaption1.Text = cboChangeBy.SelectedItem.Text
    '        '        'objUpdateCaption2.Text = iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
    '        '        objUpdateCaption1.Text = Language.getMessage(mstrModuleName_upd_percent, 202, "Current") & " " & cboChangeBy.SelectedItem.Text
    '        '        objUpdateCaption2.Text = Language.getMessage(mstrModuleName_upd_percent, 203, "Final") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
    '        '        objlblCaption3.Text = Language.getMessage(mstrModuleName_upd_percent, 201, "Last") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
    '        '        objlblCaption4.Text = Language.getMessage(mstrModuleName_upd_percent, 202, "Current") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
    '        '        'S.SANDEEP |18-JAN-2019| -- END
    '        '    Case 2
    '        '        txtChangeBy.Enabled = True
    '        '        'S.SANDEEP |18-JAN-2019| -- START
    '        '        'objUpdateCaption1.Text = cboChangeBy.SelectedItem.Text
    '        '        'objUpdateCaption2.Text = iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
    '        '        objUpdateCaption1.Text = Language.getMessage(mstrModuleName, 202, "Current") & " " & cboChangeBy.SelectedItem.Text
    '        '        objUpdateCaption2.Text = Language.getMessage(mstrModuleName, 203, "Final") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
    '        '        objlblCaption3.Text = Language.getMessage(mstrModuleName, 201, "Last") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
    '        '        objlblCaption4.Text = Language.getMessage(mstrModuleName, 202, "Current") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
    '        '        'S.SANDEEP |18-JAN-2019| -- END
    '        'End Select
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("cboChangeBy_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub cboGoalType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim objCbo As DropDownList = Nothing
            Dim objTxt As TextBox = Nothing
            Select Case CType(sender, DropDownList).ID.ToString().ToUpper()
                Case cboEmpField1GoalType.ID.ToUpper()
                    objCbo = cboEmpField1GoalType
                    objTxt = txtEmpField1GoalValue
                Case cboEmpField2GoalType.ID.ToUpper()
                    objCbo = cboEmpField2GoalType
                    objTxt = txtEmpField2GoalValue
                Case cboEmpField3GoalType.ID.ToUpper()
                    objCbo = cboEmpField3GoalType
                    objTxt = txtEmpField3GoalValue
                Case cboEmpField4GoalType.ID.ToUpper()
                    objCbo = cboEmpField4GoalType
                    objTxt = txtEmpField4GoalValue
                Case cboEmpField5GoalType.ID.ToUpper()
                    objCbo = cboEmpField5GoalType
                    objTxt = txtEmpField5GoalValue
            End Select
            If CInt(IIf(objCbo.SelectedValue = "", enGoalType.GT_QUALITATIVE, objCbo.SelectedValue)) = enGoalType.GT_QUALITATIVE Then
                objTxt.Enabled = False : objTxt.Text = 0
            Else
                objTxt.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("cboGoalType_SelectedIndexChanged:- " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP [01-OCT-2018] -- END

#End Region

#Region "Textbox Event(S)"
    Protected Sub txtEmpField1Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField1Remark1.TextChanged
        Try
            Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
            mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark1.Value)) = txtEmpField1Remark1.Text
            Me.ViewState("mdicEmpField2FieldData") = mdicEmpField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1Remark1_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField1Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField1Remark2.TextChanged
        Try
            Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
            mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark2.Value)) = txtEmpField1Remark2.Text
            Me.ViewState("mdicEmpField2FieldData") = mdicEmpField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1Remark2_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField1Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField1Remark3.TextChanged
        Try
            Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
            mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark3.Value)) = txtEmpField1Remark3.Text
            Me.ViewState("mdicEmpField2FieldData") = mdicEmpField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1Remark3_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField2Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField2Remark1.TextChanged
        Try
            Dim mdicEmpField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
            mdicEmpField2FieldData(CInt(hdf_lblEmpField2Remark1.Value)) = txtEmpField2Remark1.Text
            Me.ViewState("mdicEmpField2FieldData") = mdicEmpField2FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField2Remark1_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField2Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField2Remark2.TextChanged
        Try
            Dim mdicEmpField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
            mdicEmpField2FieldData(CInt(hdf_lblEmpField2Remark2.Value)) = txtEmpField2Remark2.Text
            Me.ViewState("mdicEmpField2FieldData") = mdicEmpField2FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField2Remark2_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField2Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField2Remark3.TextChanged
        Try
            Dim mdicEmpField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
            mdicEmpField2FieldData(CInt(hdf_lblEmpField2Remark3.Value)) = txtEmpField2Remark3.Text
            Me.ViewState("mdicEmpField2FieldData") = mdicEmpField2FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField2Remark3_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField3Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField3Remark1.TextChanged
        Try
            Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
            mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark1.Value)) = txtEmpField3Remark1.Text
            Me.ViewState("mdicEmpField3FieldData") = mdicEmpField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField3Remark1_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField3Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField3Remark2.TextChanged
        Try
            Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
            mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark2.Value)) = txtEmpField3Remark2.Text
            Me.ViewState("mdicEmpField3FieldData") = mdicEmpField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField3Remark2_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField3Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField3Remark3.TextChanged
        Try
            Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
            mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark2.Value)) = txtEmpField3Remark2.Text
            Me.ViewState("mdicEmpField3FieldData") = mdicEmpField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField3Remark3_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField4Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField4Remark1.TextChanged
        Try
            Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
            mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark1.Value)) = txtEmpField4Remark1.Text
            Me.ViewState("mdicEmpField4FieldData") = mdicEmpField4FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField4Remark1_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField4Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField4Remark2.TextChanged
        Try
            Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
            mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark2.Value)) = txtEmpField4Remark2.Text
            Me.ViewState("mdicEmpField4FieldData") = mdicEmpField4FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField4Remark2_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField4Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField4Remark3.TextChanged
        Try
            Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
            mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark3.Value)) = txtEmpField4Remark3.Text
            Me.ViewState("mdicEmpField4FieldData") = mdicEmpField4FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField4Remark3_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField5Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField5Remark1.TextChanged
        Try
            Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
            mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark1.Value)) = txtEmpField5Remark1.Text
            Me.ViewState("mdicEmpField4FieldData") = mdicEmpField5FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField5Remark1_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField5Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField5Remark2.TextChanged
        Try
            Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
            mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark2.Value)) = txtEmpField5Remark2.Text
            Me.ViewState("mdicEmpField4FieldData") = mdicEmpField5FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField5Remark2_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField5Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField5Remark3.TextChanged
        Try
            Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
            mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark3.Value)) = txtEmpField5Remark3.Text
            Me.ViewState("mdicEmpField4FieldData") = mdicEmpField5FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField5Remark3_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField1SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField1SearchEmp.TextChanged
        Try
            Call Fill_Data_EmpField1()
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1SearchEmp_TextChanged" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField2SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField2SearchEmp.TextChanged
        Try
            Call Fill_Data_EmpField2()
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1SearchEmp_TextChanged" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField3SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField3SearchEmp.TextChanged
        Try
            Call Fill_Data_EmpField3()
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1SearchEmp_TextChanged" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField4SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField4SearchEmp.TextChanged
        Try
            Call Fill_Data_EmpField4()
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1SearchEmp_TextChanged" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtEmpField5SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEmpField5SearchEmp.TextChanged
        Try
            Call Fill_Data_EmpField5()
        Catch ex As Exception
            DisplayMessage.DisplayError("txtEmpField1SearchEmp_TextChanged" & ex.Message, Me)
        End Try
    End Sub


    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    'Protected Sub txtAccomplishmentPerComp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim txt As TextBox = CType(sender, TextBox)
    '        Dim gvRow As GridViewRow = CType(txt.NamingContainer, GridViewRow)
    '        If gvRow.Style("background") = "red" Then
    '            gvRow.Style.Remove("background")
    '        End If

    '        'S.SANDEEP [01-OCT-2018] -- START
    '        'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '        'If CDec(txt.Text) > 100 Then
    '        '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), Me)
    '        '    txt.Text = mdtAccomplishmentData.Rows(gvRow.RowIndex)("org_per_comp")
    '        'End If
    '        Dim dblMaxValue As Double = mdtAccomplishmentData.Rows(gvRow.RowIndex)("org_per_comp")
    '        If CDec(txt.Text) > dblMaxValue Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 28, "Sorry, % completed cannot be greater than " & dblMaxValue & "."), Me)
    '            txt.Text = dblMaxValue
    '        End If
    '        If CDec(txt.Text) <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            txt.Text = dblMaxValue
            'End If
    '        'S.SANDEEP [01-OCT-2018] -- END
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("txtEmpField1SearchEmp_TextChanged" & ex.Message, Me)
    '    End Try
    'End Sub
    'Shani(24-JAN-2017) -- End

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    'Protected Sub txtChangeBy_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        'S.SANDEEP |18-JAN-2019| -- START
    '        txtChangeBy.Text = Format(CDbl(txtChangeBy.Text), Session("fmtCurrency"))
    '        'S.SANDEEP |18-JAN-2019| -- END
    '        If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '            If CDbl(txtChangeBy.Text) > CDbl(txtChangeBy.Attributes(CInt(enGoalType.GT_QUALITATIVE).ToString)) Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRejGoals, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), Me)
    '                Exit Sub
    '            Else
    '                'S.SANDEEP |18-JAN-2019| -- START
    '                'txtFinalValue.Text = txtChangeBy.Text
    '                txtCurrentValue.Text = txtChangeBy.Text
    '                'S.SANDEEP |18-JAN-2019| -- END
    '            End If
    '        ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
    '            'S.SANDEEP |18-JAN-2019| -- START
    '            'Select Case cboChangeBy.SelectedIndex
    '            '    Case 1  'PERCENTAGE
    '            '        txtFinalValue.Text = CDbl((mdblGoalValue * txtChangeBy.Text) / 100)
    '            '    Case 2  'VALUE
    '            '        txtFinalValue.Text = CDbl((txtChangeBy.Text * 100) / mdblGoalValue)
    '            'End Select

                'Select Case cboChangeBy.SelectedIndex
                '    Case 1  'PERCENTAGE
    '                    txtCurrentValue.Text = Format(CDbl((mdblGoalValue * txtChangeBy.Text) / 100), Session("fmtCurrency"))
                '    Case 2  'VALUE
    '                    txtCurrentValue.Text = Format(CDbl((txtChangeBy.Text * 100) / mdblGoalValue), Session("fmtCurrency"))
                'End Select
    '            'S.SANDEEP |18-JAN-2019| -- END
    '        End If

    '        'S.SANDEEP |18-JAN-2019| -- START
    '        If cboChangeBy.SelectedIndex = 1 Then
    '            txtFinalValue.Text = Format(CDec(CDec(txtChangeBy.Text) + CDec(txtLastValue_Pct.Text)), Session("fmtCurrency"))
    '        ElseIf cboChangeBy.SelectedIndex = 2 Then
    '            txtFinalValue.Text = Format(CDec(CDec(txtLastValue_Pct.Text) + CDec(txtCurrentValue.Text)), Session("fmtCurrency"))
    '        End If
    '        'S.SANDEEP |18-JAN-2019| -- END

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("txtChangeBy_TextChanged : " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [01-OCT-2018] -- END

#End Region

#Region " CheckBox Event(S) "

    Protected Sub chkEmpField1AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField1All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField1Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField1select"), CheckBox)
                chk.Checked = chkOwrField1All.Checked
                Call GoalEmpOperation_Emp1(iRow.Cells(3).Text, CBool(chkOwrField1All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField1AllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField1select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField1 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField1.NamingContainer, DataGridItem)
            If chkOwrField1 IsNot Nothing Then
                Call GoalEmpOperation_Emp1(dgvEmpField1Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField1.Checked))
            End If
            Call Fill_Data_EmpField1()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField1select_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField2AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField2All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField2Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField2select"), CheckBox)
                chk.Checked = chkOwrField2All.Checked
                Call GoalEmpOperation_Emp2(iRow.Cells(3).Text, CBool(chkOwrField2All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField2AllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField2select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField2 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField2.NamingContainer, DataGridItem)
            If chkOwrField2 IsNot Nothing Then
                Call GoalEmpOperation_Emp2(dgvEmpField2Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField2.Checked))
            End If
            Call Fill_Data_EmpField2()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField1allSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField3AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField3All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField3Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField3select"), CheckBox)
                chk.Checked = chkOwrField3All.Checked
                Call GoalEmpOperation_Emp3(iRow.Cells(3).Text, CBool(chkOwrField3All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField3AllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField3select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField3 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField3.NamingContainer, DataGridItem)
            If chkOwrField3 IsNot Nothing Then
                Call GoalEmpOperation_Emp3(dgvEmpField3Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField3.Checked))
            End If
            Call Fill_Data_EmpField3()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField3select_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField4AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField4All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField4Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField4select"), CheckBox)
                chk.Checked = chkOwrField4All.Checked
                Call GoalEmpOperation_Emp4(iRow.Cells(3).Text, CBool(chkOwrField4All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField4AllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField4select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField4 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField4.NamingContainer, DataGridItem)
            If chkOwrField4 IsNot Nothing Then
                Call GoalEmpOperation_Emp4(dgvEmpField4Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField4.Checked))
            End If
            Call Fill_Data_EmpField4()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField1al4Select_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField5AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField5All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField5Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField5select"), CheckBox)
                chk.Checked = chkOwrField5All.Checked
                Call GoalEmpOperation_Emp5(iRow.Cells(3).Text, CBool(chkOwrField5All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField5AllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkEmpField5select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField5 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField5.NamingContainer, DataGridItem)
            If chkOwrField5 IsNot Nothing Then
                Call GoalEmpOperation_Emp5(dgvEmpField5Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField5.Checked))
            End If
            Call Fill_Data_EmpField5()
        Catch ex As Exception
            DisplayMessage.DisplayError("chkEmpField5select_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    'Protected Sub chkselect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chk As CheckBox = CType(sender, CheckBox)
    '        Dim row As GridViewRow = TryCast(chk.NamingContainer, GridViewRow)
    '        Dim dtRow As DataRow = mdtAccomplishmentData.Rows(row.RowIndex)

    '        If chk.Checked Then
    '            CType(row.Cells(5).FindControl("txtPerComp"), TextBox).ReadOnly = False
    '        Else
    '            CType(row.Cells(5).FindControl("txtPerComp"), TextBox).Text = dtRow.Item("org_per_comp")
    '            CType(row.Cells(5).FindControl("txtPerComp"), TextBox).ReadOnly = True
    '        End If

    '        For Each dRow As DataRow In mdtAccomplishmentData.Select("IsGrp = TRUE")
    '            CType(dgv_Accomplishment_data.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '        Next
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("chkEmpField1al4Select_CheckedChanged :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Shani(24-JAN-2017) -- End

#End Region

    'SHANI (18 JUN 2015) -- Start
    '#Region "Update Progress Popup"
    '    Private Sub UpdateProgressLoad(ByVal dgvRow As GridViewRow)
    '        Dim objFMapping As New clsAssess_Field_Mapping
    '        Dim iColName As String = ""
    '        Dim iExOrder As Integer = 0
    '        mintFieldUnkid = 0
    '        mintLinkedFld = 0
    '        mintFieldTypeId = 0
    '        mdtPeriodStartDate = Nothing
    '        mstrDefinedGoal = ""
    '        mintTableFieldTranUnkId = 0
    '        mintGoalTypeId = 0
    '        mdblGoalValue = 0
    '        mdblPercentage = 0
    '        mdtGoalAccomplishment = Nothing
    '        Try
    '            Call Update_Clear_Controls()
    '            mintFieldUnkid = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
    '            If mintFieldUnkid <= 0 Then Exit Sub
    '            Dim objFMaster As New clsAssess_Field_Master
    '            iExOrder = objFMaster.Get_Field_ExOrder(mintFieldUnkid)
    '            Select Case iExOrder
    '                Case 1
    '                    iColName = "empfield1unkid"
    '                Case 2
    '                    iColName = "empfield2unkid"
    '                Case 3
    '                    iColName = "empfield3unkid"
    '                Case 4
    '                    iColName = "empfield4unkid"
    '                Case 5
    '                    iColName = "empfield5unkid"
    '            End Select
    '            mintTableFieldTranUnkId = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)(iColName)
    '            mstrDefinedGoal = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)("Field" & mintFieldUnkid.ToString)

    '            'S.SANDEEP [01-OCT-2018] -- START
    '            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '            mintGoalTypeId = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)("goaltypeid")
    '            mdblGoalValue = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)("goalvalue")

    '            'S.SANDEEP |12-FEB-2019| -- START
    '            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '            'If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '            '    objlblGoalTypeInfo.Text = Language.getMessage(mstrModuleName_upd_percent, 202, "Goal Type : Qualitative | Value : 100")
    '            'ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
    '            '    objlblGoalTypeInfo.Text = Language.getMessage(mstrModuleName_upd_percent, 203, "Goal Type : Quantitative | Value : " & CDbl(mdblGoalValue))
    '            'End If
    '            Dim mstrUoMTypeName As String = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)(mstrUoMColName)

    '            'S.SANDEEP |08-JUL-2019| -- START
    '            'ISSUE/ENHANCEMENT : PA CHANGES
    '            'If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '            '    objlblGoalTypeInfo.Text = Language.getMessage(mstrModuleName_upd_percent, 202, "Goal Type : Qualitative | Value : 100") & " " & mstrUoMTypeName
    '            'ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
    '            '    objlblGoalTypeInfo.Text = Language.getMessage(mstrModuleName_upd_percent, 203, "Goal Type : Quantitative | Value : ") & Format(CDbl(mdblGoalValue), Session("fmtCurrency")) & " " & mstrUoMTypeName
    '            'End If
            'If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '                objlblGoalTypeInfo.Text = Language.getMessage("clsMasterData", 843, "Qualitative") & " | " & Language.getMessage(mstrModuleName_upd_percent, 102, "Value") & " : 100 " & mstrUoMTypeName
            'ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
    '                objlblGoalTypeInfo.Text = Language.getMessage("clsMasterData", 844, "Quantitative") & " | " & Language.getMessage(mstrModuleName_upd_percent, 102, "Value") & " : " & Format(CDbl(mdblGoalValue), Session("fmtCurrency")) & " " & mstrUoMTypeName
            'End If
    '            'S.SANDEEP |08-JUL-2019| -- END


    '            'S.SANDEEP |12-FEB-2019| -- END

    '            txtNewPercentage.Attributes.Add("lstval", 0) : txtNewValue.Attributes.Add("lstval", 0)
    '            txtTotalPercentage.Attributes.Add("lstval", 0) : txtTotalValue.Attributes.Add("lstval", 0)
           
    '            'S.SANDEEP |08-JUL-2019| -- START
    '            'ISSUE/ENHANCEMENT : {Extra Validation Removed}

    '            'If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '            '    cboChangeBy.SelectedIndex = 1
    '            '    Call cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '            '    'txtChangeBy.Attributes.Add(CInt(enGoalType.GT_QUALITATIVE).ToString(), "100")
    '            '    cboChangeBy.Enabled = False
    '            'Else
    '            '    cboChangeBy.Enabled = True
    '            '    Try
    '            '        'txtChangeBy.Attributes.Remove(CInt(enGoalType.GT_QUALITATIVE).ToString())
    '            '    Catch ex As Exception

    '            '    End Try
    '            'End If

            'If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
            '    cboChangeBy.SelectedIndex = 1
            '    cboChangeBy.Enabled = False
    '                lblCaption3.Visible = True : txtNewPercentage.Visible = True
    '                lblCaption4.Visible = True : txtTotalPercentage.Visible = True
    '                lblCaption1.Visible = False : txtNewValue.Visible = False
    '                lblCaption2.Visible = False : txtTotalValue.Visible = False
            'Else
    '                cboChangeBy.SelectedIndex = 2
    '                cboChangeBy.Enabled = False
    '                lblCaption3.Visible = False : txtNewPercentage.Visible = False
    '                lblCaption4.Visible = False : txtTotalPercentage.Visible = False
    '                lblCaption1.Visible = True : txtNewValue.Visible = True
    '                lblCaption2.Visible = True : txtTotalValue.Visible = True
            'End If
    '            Call cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '            'S.SANDEEP |08-JUL-2019| -- END

    '            'S.SANDEEP [01-OCT-2018] -- END

    '            txtUpdatePeriod.Text = cboPeriod.SelectedItem.Text
    '            txtUpdateGoals.Text = mstrDefinedGoal
    '            txtUpdateEmployeeName.Text = cboEmployee.SelectedItem.Text

    '            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(CInt(cboPeriod.SelectedValue))
    '            mintLinkedFld = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))

    '            Dim mintExOrder As Integer = objFMaster.Get_Field_ExOrder(mintLinkedFld)

    '            Select Case mintExOrder
    '                Case enWeight_Types.WEIGHT_FIELD1
    '                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD1
    '                Case enWeight_Types.WEIGHT_FIELD2
    '                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD2
    '                Case enWeight_Types.WEIGHT_FIELD3
    '                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD3
    '                Case enWeight_Types.WEIGHT_FIELD4
    '                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD4
    '                Case enWeight_Types.WEIGHT_FIELD5
    '                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD5
    '            End Select
    '            objFMapping = Nothing : objFMaster = Nothing

    '            Dim objEvaluation As New clsevaluation_analysis_master
    '            'S.SANDEEP |26-AUG-2019| -- START
    '            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '            'If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
    '            If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
    '                'S.SANDEEP |26-AUG-2019| -- END
    '                dgvHistory.Columns(0).Visible = False
    '                dgvHistory.Columns(1).Visible = False
    '                'S.SANDEEP [29-NOV-2017] -- START
    '                'ISSUE/ENHANCEMENT : REF-ID # 136
    '                dgvHistory.Columns(2).Visible = False
    '                'S.SANDEEP [29-NOV-2017] -- END
    '                btnUpdateSave.Enabled = False
    '            End If
    '            objEvaluation = Nothing

    '            Dim objPrd As New clscommom_period_Tran

    '            'Shani(20-Nov-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
    '            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
    '            'Shani(20-Nov-2015) -- End

    '            mdtPeriodStartDate = objPrd._Start_Date
    '            objPrd = Nothing

    '            Call UpdateFillCombo()
    '            Call Fill_History()

    '            'Shani (26-Sep-2016) -- Start
    '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '            If CBool(Session("GoalsAccomplishedRequiresApproval")) = False Then
    '                'S.SANDEEP [29-NOV-2017] -- START
    '                'ISSUE/ENHANCEMENT : REF-ID # 136
    '                'dgvHistory.Columns(7).Visible = False
    '                dgvHistory.Columns(8).Visible = False
    '                'S.SANDEEP [29-NOV-2017] -- END
    '            End If
    '            'Shani (26-Sep-2016) -- End


    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("UpdateProgressLoad:- " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub UpdateFillCombo()
    '        Dim objMData As New clsMasterData
    '        Dim dsList As New DataSet
    '        Try
    '            dsList = objMData.Get_CompanyGoal_Status("List", True)
    '            With cboUpdateStatus
    '                .DataValueField = "Id"
    '                .DataTextField = "Name"
    '                .DataSource = dsList.Tables("List")
    '                .DataBind()
    '                .SelectedValue = 0
    '            End With
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
    '        Finally
    '            dsList.Dispose() : objMData = Nothing
    '        End Try
    '    End Sub

    '    Private Sub Fill_History()
    '        'Dim dtViewTable As DataTable = Nothing
    '        Try

    '            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
    '            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    '            Call SetDateFormat()
    '            'Pinkal (16-Apr-2016) -- End


    '            mdtGoalAccomplishment = objEUpdateProgress.GetList("List", CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), mintTableFieldTranUnkId, Session("fmtCurrency")).Tables(0)

    '            mdtGoalAccomplishment.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()

    '            'S.SANDEEP |18-JAN-2019| -- START
    '            'Dim iMaxTranId As Integer = 0
    '            'If mdtGoalAccomplishment.Rows.Count > 0 Then
    '            '    'S.SANDEEP |30-JAN-2019| -- START
    '            '    'iMaxTranId = Convert.ToInt32(dtViewTable.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
    '            '    If IsDBNull(mdtGoalAccomplishment.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
    '            '        iMaxTranId = Convert.ToInt32(mdtGoalAccomplishment.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
    '            '    End If
    '            '    'S.SANDEEP |30-JAN-2019| -- END
    '            'End If
    '            'If iMaxTranId > 0 Then
    '            '    Dim dr() As DataRow = Nothing 'dtViewTable.Select("empupdatetranunkid = " & iMaxTranId)
    '            '    Dim iSecondLastTranId As Integer = 0
    '            '    Dim iList As List(Of Integer) = mdtGoalAccomplishment.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()
    '            '    If iList.Count > 0 Then
    '            '        If iList.Min() = iMaxTranId Then
    '            '            dr = mdtGoalAccomplishment.Select("empupdatetranunkid = " & iMaxTranId)
    '            '            If dr.Length > 0 Then
    '            '                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
    '            '                cboChangeBy.Enabled = False
    '            '                Call cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '            '                Select Case cboChangeBy.SelectedIndex
    '            '                    Case 1
    '            '                        txtNewPercentage.Attributes("lstval") = 0
    '            '                    Case 2
    '            '                        txtNewValue.Attributes("lstval") = 0
    '            '                End Select
    '            '            End If
    '            '        Else
    '            '            Try
    '            '                iSecondLastTranId = iList.Item(iList.IndexOf(iMaxTranId) + 1)
    '            '            Catch ex As Exception
    '            '                iSecondLastTranId = iMaxTranId
    '            '            End Try
    '            '            dr = mdtGoalAccomplishment.Select("empupdatetranunkid = " & iSecondLastTranId)
    '            '            If dr.Length > 0 Then
    '            '                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
    '            '                cboChangeBy.Enabled = False
    '            '                Call cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '            '                Select Case cboChangeBy.SelectedIndex
    '            '                    Case 1
    '            '                        txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
    '            '                    Case 2
    '            '                        txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
    '            '                End Select
    '            '            End If
    '            '        End If
    '            '    End If
    '            'Else
    '            '    txtNewPercentage.Attributes("lstval") = 0
    '            '    txtNewValue.Attributes("lstval") = 0
    '            'End If
    '            Call SetTagValue(mdtGoalAccomplishment, False)
    '            'S.SANDEEP |18-JAN-2019| -- END

    '            If mdtGoalAccomplishment IsNot Nothing Then
    '                dgvHistory.DataSource = mdtGoalAccomplishment
    '                dgvHistory.DataBind()
            '    End If
            '            Catch ex As Exception
    '            DisplayMessage.DisplayError("Fill_History : " & ex.Message, Me)
    '        Finally
            '            End Try
    '    End Sub

    '    Private Sub GetUpdateValue()
    '        Try
    '            'txtUpdatePercent.Text = objEUpdateProgress._Pct_Completed
    '            txtUpdateRemark.Text = objEUpdateProgress._Remark
    '            cboUpdateStatus.SelectedValue = objEUpdateProgress._Statusunkid
    '            dtpUpdateChangeDate.SetDate = objEUpdateProgress._Updatedate
    '            'S.SANDEEP [01-OCT-2018] -- START
    '            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '            cboChangeBy.SelectedIndex = objEUpdateProgress._ChangeById
    '            'Select Case objEUpdateProgress._ChangeById
    '            '    Case 1  'PERCENTAGE
    '            '        txtChangeBy.Text = objEUpdateProgress._Pct_Completed
    '            '        txtFinalValue.Text = objEUpdateProgress._FinalValue
    '            '    Case 2  'VALUE
    '            '        txtChangeBy.Text = objEUpdateProgress._FinalValue
    '            '        txtFinalValue.Text = objEUpdateProgress._Pct_Completed
    '            'End Select
    '            'If txtChangeBy.Enabled = False Then txtChangeBy.Enabled = True
    '            'If txtFinalValue.Enabled = False Then txtFinalValue.Enabled = True
    '            'S.SANDEEP [01-OCT-2018] -- END
            'Select Case objEUpdateProgress._ChangeById
            '    Case 1  'PERCENTAGE
    '                    txtTotalPercentage.Text = Format(objEUpdateProgress._FinalValue, GUI.fmtCurrency).ToString
    '                    Call txtTotalPercentage_TextChanged(New Object(), New EventArgs())
            '    Case 2  'VALUE
    '                    txtTotalValue.Text = Format(objEUpdateProgress._FinalValue, GUI.fmtCurrency).ToString
    '                    Call txtTotalValue_TextChanged(New Object(), New EventArgs())
            'End Select
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("GetUpdateValue:- " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SeUpdateValue()
    '        Try
    '            objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
    '            objEUpdateProgress._Empfieldtypeid = mintFieldTypeId
    '            objEUpdateProgress._Empfieldunkid = mintTableFieldTranUnkId
    '            objEUpdateProgress._Employeeunkid = CInt(cboEmployee.SelectedValue)
    '            objEUpdateProgress._Fieldunkid = mintFieldUnkid
    '            objEUpdateProgress._Isvoid = False
    '            'objEUpdateProgress._Pct_Completed = txtUpdatePercent.Text
    '            objEUpdateProgress._Periodunkid = CInt(cboPeriod.SelectedValue)
    '            objEUpdateProgress._Remark = txtUpdateRemark.Text
    '            objEUpdateProgress._Statusunkid = CInt(cboUpdateStatus.SelectedValue)
    '            objEUpdateProgress._Updatedate = dtpUpdateChangeDate.GetDate
    '            objEUpdateProgress._Userunkid = CInt(Session("UserId"))
    '            objEUpdateProgress._Voiddatetime = Nothing
    '            objEUpdateProgress._Voidreason = ""
    '            objEUpdateProgress._Voiduserunkid = -1
    '            'Shani (26-Sep-2016) -- Start
    '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '            If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
    '                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '            Else
    '                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
    '            End If
    '            objEUpdateProgress._GoalAccomplishmentRemark = ""
    '            'Shani (26-Sep-2016) -- End

    '            'S.SANDEEP [01-OCT-2018] -- START
    '            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '            objEUpdateProgress._ChangeById = cboChangeBy.SelectedIndex

    '            'S.SANDEEP |18-JAN-2019| -- START
    '            'If cboChangeBy.SelectedIndex = 1 Then
    '            '    objEUpdateProgress._Pct_Completed = txtChangeBy.Text
    '            '    objEUpdateProgress._FinalValue = txtFinalValue.Text
    '            'ElseIf cboChangeBy.SelectedIndex = 2 Then
    '            '    objEUpdateProgress._Pct_Completed = txtFinalValue.Text
    '            '    objEUpdateProgress._FinalValue = txtChangeBy.Text
    '            'End If
    '            'If cboChangeBy.SelectedIndex = 1 Then
    '            '    objEUpdateProgress._Pct_Completed = txtFinalValue.Text
    '            '    objEUpdateProgress._FinalValue = txtFinalValue.Text
    '            'ElseIf cboChangeBy.SelectedIndex = 2 Then
    '            '    objEUpdateProgress._Pct_Completed = txtFinalValue.Text
    '            '    objEUpdateProgress._FinalValue = txtChangeBy.Text
    '            'End If
    '            'S.SANDEEP |18-JAN-2019| -- END

    '            'S.SANDEEP [01-OCT-2018] -- END

            'If cboChangeBy.SelectedIndex = 1 Then
    '                objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
    '                objEUpdateProgress._FinalValue = txtTotalPercentage.Text
            'ElseIf cboChangeBy.SelectedIndex = 2 Then
    '                objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
    '                objEUpdateProgress._FinalValue = txtTotalValue.Text
            'End If

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("SeUpdateValue:- " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub Update_Clear_Controls()
    '        Try
    '            'S.SANDEEP |12-FEB-2019| -- START
    '            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '            'dtpUpdateChangeDate.SetDate = Nothing
    '            dtpUpdateChangeDate.SetDate = Now.Date
    '            'S.SANDEEP |12-FEB-2019| -- END

    '            mintEmpUpdateTranunkid = 0
    '            cboStatus.SelectedValue = 0
    '            'txtUpdatePercent.Text = "0.00"
    '            txtUpdateRemark.Text = ""
    '            'S.SANDEEP [01-OCT-2018] -- START
    '            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '            cboChangeBy.SelectedIndex = 0
    '            Call cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())
    '            txtNewPercentage.Text = 0
    '            txtNewValue.Text = 0
    '            txtTotalPercentage.Text = 0
    '            txtTotalValue.Text = 0
    '            'txtChangeBy.Text = ""
    '            ''S.SANDEEP |18-JAN-2019| -- START
    '            'txtCurrentValue.Text = ""
    '            ''S.SANDEEP |18-JAN-2019| -- END
    '            'txtFinalValue.Text = ""
    '            'S.SANDEEP [01-OCT-2018] -- END

    '            'S.SANDEEP |08-JUL-2019| -- START
    '            'ISSUE/ENHANCEMENT : PA CHANGES
    '            cboUpdateStatus.SelectedValue = 0
    '            'S.SANDEEP |08-JUL-2019| -- END

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("Clear_Controls:- " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Function UpdateIsValidInfo() As Boolean
    '        Try
    '            If dtpUpdateChangeDate.IsNull = True Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 1, "Sorry, Date is mandatory information. Please set date to continue."), Me)
    '                dtpUpdateChangeDate.Focus()
    '                Return False
    '            End If

    '            If dtpUpdateChangeDate.GetDate.Date < mdtPeriodStartDate.Date Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 2, "Sorry, Date cannot be less than period start date."), Me)
    '                dtpUpdateChangeDate.Focus()
    '                Return False
    '            End If

    '            If dtpUpdateChangeDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 102, "Sorry, Date cannot be greater than today date."), Me)
    '                dtpUpdateChangeDate.Focus()
    '                Return False
    '            End If

    '            If CInt(cboUpdateStatus.SelectedValue) <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 3, "Sorry, Change Status is mandatory information. Please select change status to continue."), Me)
    '                cboUpdateStatus.Focus()
    '                Return False
    '            End If

    '            'Shani (05-Sep-2016) -- Start
    '            'If CDec(txtUpdatePercent.Text) <= 0 Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            '    txtUpdatePercent.Focus()
    '            '    Return False
    '            'End If
    '            'Shani (05-Sep-2016) -- End

    '            'If CDec(txtUpdatePercent.Text) > 100 Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '            '    txtUpdatePercent.Focus()
    '            '    Return False
    '            'End If

    '            'S.SANDEEP [01-OCT-2018] -- START
    '            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '            'If cboChangeBy.Enabled = True Then
    '            '    If cboChangeBy.SelectedIndex = 0 Then
    '            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, change by is mandatory information. Please select change by to continue."), Me)
    '            '        cboChangeBy.Focus()
    '            '        Return False
    '            '    ElseIf cboChangeBy.SelectedIndex = 1 AndAlso txtChangeBy.Text <= 0 Then
    '            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            '        txtChangeBy.Focus()
    '            '        Return False
    '            '    ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtFinalValue.Text <= 0 Then
    '            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            '        txtChangeBy.Focus()
    '            '        Return False
    '            '    End If
    '            'End If
    '            'If txtChangeBy.Text <= 0 Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            '    txtChangeBy.Focus()
    '            '    Return False
    '            'End If
    '            'Dim dblMaxValue As Double = 0
    '            'If txtChangeBy.HasAttributes() Then
    '            '    Try
    '            '        dblMaxValue = txtChangeBy.Attributes(CInt(enGoalType.GT_QUALITATIVE).ToString)
    '            '        If dblMaxValue > 0 AndAlso (CDec(txtChangeBy.Text) > dblMaxValue) Then
    '            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '            '            txtChangeBy.Focus()
    '            '    Return False
    '            'End If
    '            '    Catch ex As Exception

    '            '    End Try
    '            'End If

    '            If CInt(cboChangeBy.SelectedIndex) = 1 Then
    '                Dim dblMaxValue As Double = 0
    '                dblMaxValue = 100
    '                If dblMaxValue > 0 AndAlso (CDec(txtNewPercentage.Text) > dblMaxValue) Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '                    txtNewPercentage.Focus()
            '    Return False
            'End If
    '                If dblMaxValue > 0 AndAlso (CDec(txtTotalPercentage.Text) > dblMaxValue) Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '                    txtNewPercentage.Focus()
            '    Return False
            'End If
    '            End If

            'If cboChangeBy.Enabled = True Then
            '    If cboChangeBy.SelectedIndex = 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, change by is mandatory information. Please select change by to continue."), Me)
            '        cboChangeBy.Focus()
            '        Return False
    '                ElseIf cboChangeBy.SelectedIndex = 1 AndAlso txtTotalPercentage.Text <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '                    txtNewValue.Focus()
            '        Return False
    '                ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtTotalValue.Text <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '                    txtNewValue.Focus()
            '        Return False
            '    End If
            'End If
    '            'S.SANDEEP [01-OCT-2018] -- END

    '            'Shani (26-Sep-2016) -- Start
    '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '            If mintEmpUpdateTranunkid <= 0 Then 'INSERT
    '                If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
    '                    'S.SANDEEP [29-NOV-2017] -- START
    '                    'ISSUE/ENHANCEMENT : REF-ID # 136
    '                    'Dim drow = dgvHistory.Items.Cast(Of DataGridItem).Where(Function(x) CInt(x.Cells(8).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '                    Dim drow = dgvHistory.Items.Cast(Of DataGridItem).Where(Function(x) CInt(x.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '                    'S.SANDEEP [29-NOV-2017] -- END
    '                    If drow.Count > 0 Then
    '                        'S.SANDEEP [15-Feb-2018] -- START
    '                        'ISSUE/ENHANCEMENT : {#32}
    '                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), Me)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), Me)
    '                        'S.SANDEEP [15-Feb-2018] -- END
            '    Return False
            'End If
            'End If
            'End If
    '            'Shani (26-Sep-2016) -- End


    '            'S.SANDEEP |30-JAN-2019| -- START
    '            ' REMOVED : mstrModuleName
    '            ' ADDED : mstrModuleName_upd_percent
    '            'S.SANDEEP |30-JAN-2019| -- END

    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("IsValidInfo:- " & ex.Message, Me)
    '        Finally
    '        End Try
    '        Return True
    '    End Function

    '    Private Sub Save_UpdateProgress()
    '        Dim blnFlag As Boolean = False
    '        Try
    '            Call SeUpdateValue()

    '            If mintEmpUpdateTranunkid <= 0 Then 'INSERT
    '                blnFlag = objEUpdateProgress.Insert()
    '            ElseIf mintEmpUpdateTranunkid > 0 Then  'UPDATE
    '                blnFlag = objEUpdateProgress.Update()
    '            End If


    '            If blnFlag = True Then
    '                'Shani (26-Sep-2016) -- Start
    '                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '                If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
    '                    If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                        'objEUpdateProgress._WebFrmName = mstrModuleName_AppRejGoals
    '                        Dim iLoginType As enLogin_Mode = enLogin_Mode.MGR_SELF_SERVICE
    '                        Dim intEmpUnkid As Integer = 0

    '                        If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                            iLoginType = enLogin_Mode.EMP_SELF_SERVICE
    '                            intEmpUnkid = CInt(Session("Employeeunkid"))
    '                        End If
    '                        'S.SANDEEP |05-MAR-2019| -- START
    '                        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    '                        'objEUpdateProgress.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
    '                        '                                              Session("Fin_year"), _
    '                        '                                              Session("FinancialYear_Name"), _
    '                        '                                              CStr(Session("Database_Name")), _
    '                        '                                              objEUpdateProgress._Empupdatetranunkid, _
    '                        '                                              CInt(Session("CompanyUnkId")), _
    '                        '                                              Session("ArutiSelfServiceURL"), _
    '                        '                                              iLoginType, intEmpUnkid, Session("UserId"))
    '                        'S.SANDEEP |05-MAR-2019| -- END
    '                    End If
    '                End If
    '                'Shani (26-Sep-2016) -- End
    '                Call Update_Clear_Controls()
    '                Call Fill_History()
    '            Else
    '                If objEUpdateProgress._Message <> "" Then
    '                    DisplayMessage.DisplayError(objEUpdateProgress._Message, Me)
    '                    Exit Sub
    '                End If
    '            End If
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub

    '    Private Sub txtTotalPercentage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalPercentage.TextChanged
    '        Try
    '            RemoveHandler txtNewPercentage.TextChanged, AddressOf txtNewPercentage_Pct_TextChanged
    '            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '                If CDbl(txtTotalPercentage.Text) > CDbl(100) Then
    '                    'Sohail (23 Mar 2019) -- Start
    '                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName_upd_percent, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_upd_percent, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), Me)
    '                    'Sohail (23 Mar 2019) -- End
    '                    Exit Sub
    '                End If
    '            End If
    '            txtNewPercentage.Text = txtTotalPercentage.Text - CDec(txtNewPercentage.Attributes("lstval"))
    '            mdblPercentage = txtTotalPercentage.Text
    '            AddHandler txtNewPercentage.TextChanged, AddressOf txtNewPercentage_Pct_TextChanged
    '        Catch ex As Exception
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'DisplayError.Show("-1", ex.Message, "txtTotalPercentage_TextChanged", mstrModuleName)
    '            DisplayMessage.DisplayError("txtTotalPercentage_TextChanged:- " & ex.Message, Me)
    '            'Sohail (23 Mar 2019) -- End
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub txtNewPercentage_Pct_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewPercentage.TextChanged
    '        Try
    '            RemoveHandler txtTotalPercentage.TextChanged, AddressOf txtTotalPercentage_TextChanged
    '            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '                If CDbl(txtNewPercentage.Text) > CDbl(100) Then
    '                    'Sohail (23 Mar 2019) -- Start
    '                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), Me)
    '                    'Sohail (23 Mar 2019) -- End
    '                    Exit Sub
    '                End If
    '            End If
    '            txtTotalPercentage.Text = txtNewPercentage.Text + CDec(txtNewPercentage.Attributes("lstval"))
    '            mdblPercentage = txtTotalPercentage.Text
    '            AddHandler txtTotalPercentage.TextChanged, AddressOf txtTotalPercentage_TextChanged
    '        Catch ex As Exception
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'DisplayError.Show("-1", ex.Message, "txtNewPercentage_Pct_TextChanged", mstrModuleName)
    '            DisplayMessage.DisplayError("txtNewPercentage_Pct_TextChanged:- " & ex.Message, Me)
    '            'Sohail (23 Mar 2019) -- End
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub txtTotalValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalValue.TextChanged
    '        Try
    '            RemoveHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
    '            txtNewValue.Text = txtTotalValue.Text - CDec(txtNewValue.Attributes("lstval"))
    '            mdblPercentage = CDbl((txtTotalValue.Text * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue)))
    '            AddHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
    '        Catch ex As Exception
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'DisplayError.Show("-1", ex.Message, "txtTotalValue_TextChanged", mstrModuleName)
    '            DisplayMessage.DisplayError("txtTotalValue_TextChanged:- " & ex.Message, Me)
    '            'Sohail (23 Mar 2019) -- End
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub txtNewValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewValue.TextChanged
    '        Try
    '            RemoveHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
    '            txtTotalValue.Text = txtNewValue.Text + CDec(txtNewValue.Attributes("lstval"))
    '            mdblPercentage = CDbl((txtTotalValue.Text * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue)))
    '            AddHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
    '        Catch ex As Exception
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'DisplayError.Show("-1", ex.Message, "txtNewValue_TextChanged", mstrModuleName)
    '            DisplayMessage.DisplayError("txtNewValue_TextChanged:- " & ex.Message, Me)
    '            'Sohail (23 Mar 2019) -- End
    '        Finally
    '        End Try
    '    End Sub

    '#End Region
    'SHANI (18 JUN 2015) -- End 


    'S.SANDEEP [06 Jan 2016] -- START
    Private Sub SetLanguage()
        Try
            '====== Employee Goal List (frmEmpFieldsList) Start =====
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblPerspective.Text = Language._Object.getCaption(Me.lblPerspective.ID, Me.lblPerspective.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnGlobalAssign.Text = Language._Object.getCaption("mnuGlobalAssign", Me.btnGlobalAssign.Text).Replace("&", "")
            Me.cmnuOperation.Text = Language._Object.getCaption("btnOperations", Me.cmnuOperation.Text)
            'Me.mnuAppRejGoalAccomplished.Text = Language._Object.getCaption(Me.mnuAppRejGoalAccomplished.ID, Me.mnuAppRejGoalAccomplished.Text)
            '====== Employee Goal List (frmEmpFieldsList) End =====

            '====== Add/Edit EmpFiled1 (objfrmAddEditEmpField1) Start =====
            Language.setLanguage(mstrModuleName_Emp1)
            Me.lblheaderEmpField1.Text = Language._Object.getCaption(mstrModuleName_Emp1, Me.lblheaderEmpField1.Text)
            Me.lblEmpField1Period.Text = Language._Object.getCaption("lblPeriod", Me.lblEmpField1Period.Text)
            Me.lblEmpField1Employee.Text = Language._Object.getCaption("lblEmployee", Me.lblEmpField1Employee.Text)
            Me.lblEmpField1Perspective.Text = Language._Object.getCaption("lblPerspective", Me.lblEmpField1Perspective.Text)
            Me.lblEmpField1StartDate.Text = Language._Object.getCaption("lblStartDate", Me.lblEmpField1StartDate.Text)
            Me.lblEmpField1EndDate.Text = Language._Object.getCaption("lblEndDate", Me.lblEmpField1EndDate.Text)
            Me.lblEmpField1Status.Text = Language._Object.getCaption("lblStatus", Me.lblEmpField1Status.Text)
            Me.lblEmpField1Weight.Text = Language._Object.getCaption("lblWeight", Me.lblEmpField1Weight.Text)
            Me.lblEmpField1Percentage.Text = Language._Object.getCaption("lblPercentage", Me.lblEmpField1Percentage.Text)
            Me.btnEmpField1Save.Text = Language._Object.getCaption("btnSave", Me.btnEmpField1Save.Text).Replace("&", "")
            Me.btnEmpField1Cancel.Text = Language._Object.getCaption("btnClose", Me.btnEmpField1Cancel.Text).Replace("&", "")
            Me.dgvEmpField1Owner.Columns(1).HeaderText = Language._Object.getCaption("dgcolhEcode", Me.dgvEmpField1Owner.Columns(1).HeaderText)
            Me.dgvEmpField1Owner.Columns(2).HeaderText = Language._Object.getCaption("dgcolhEName", Me.dgvEmpField1Owner.Columns(2).HeaderText)
            'S.SANDEEP |14-MAR-2019| -- START
            Me.lblEmpField1GoalType.Text = Language._Object.getCaption("lblGoalType", Me.lblEmpField1GoalType.Text)
            Me.lblEmpField1GoalValue.Text = Language._Object.getCaption("lblGoalValue", Me.lblEmpField1GoalValue.Text)
            'S.SANDEEP |14-MAR-2019| -- END

            '====== Add/Edit EmpFiled1 (objfrmAddEditEmpField1) End =====


            '====== Add/Edit EmpFiled2 (objfrmAddEditEmpField2) Start =====
            Language.setLanguage(mstrModuleName_Emp2)
            Me.lblHeaderEmpField2.Text = Language._Object.getCaption(mstrModuleName_Emp2, Me.lblHeaderEmpField2.Text)
            Me.lblEmpField2Period.Text = Language._Object.getCaption("lblPeriod", Me.lblEmpField2Period.Text)
            Me.lblEmpField2Employee.Text = Language._Object.getCaption("lblEmployee", Me.lblEmpField2Employee.Text)

            Me.lblEmpField2StartDate.Text = Language._Object.getCaption("lblStartDate", Me.lblEmpField2StartDate.Text)
            Me.lblEmpField2EndDate.Text = Language._Object.getCaption("lblEndDate", Me.lblEmpField2EndDate.Text)

            Me.lblEmpField2Status.Text = Language._Object.getCaption("lblStatus", Me.lblEmpField2Status.Text)
            Me.lblEmpField2Weight.Text = Language._Object.getCaption("lblWeight", Me.lblEmpField2Weight.Text)
            Me.lblEmpField2Percentage.Text = Language._Object.getCaption("lblPercentage", Me.lblEmpField2Percentage.Text)
            Me.btnEmpField2Save.Text = Language._Object.getCaption("btnSave", Me.btnEmpField2Save.Text).Replace("&", "")
            Me.btnEmpField2Cancel.Text = Language._Object.getCaption("btnClose", Me.btnEmpField2Cancel.Text).Replace("&", "")
            Me.dgvEmpField2Owner.Columns(1).HeaderText = Language._Object.getCaption("dgcolhEcode", Me.dgvEmpField2Owner.Columns(1).HeaderText)
            Me.dgvEmpField2Owner.Columns(2).HeaderText = Language._Object.getCaption("dgcolhEName", Me.dgvEmpField2Owner.Columns(2).HeaderText)
            'S.SANDEEP |14-MAR-2019| -- START
            Me.lblEmpField2GoalType.Text = Language._Object.getCaption("lblGoalType", Me.lblEmpField1GoalType.Text)
            Me.lblEmpField2GoalValue.Text = Language._Object.getCaption("lblGoalValue", Me.lblEmpField1GoalValue.Text)
            'S.SANDEEP |14-MAR-2019| -- END
            '====== Add/Edit EmpFiled2 (objfrmAddEditEmpField2) End =====

            '====== Add/Edit EmpFiled3 (objfrmAddEditEmpField3) Start =====
            Language.setLanguage(mstrModuleName_Emp3)
            Me.lblHeaderEmpField3.Text = Language._Object.getCaption(mstrModuleName_Emp3, Me.lblHeaderEmpField3.Text)
            Me.lblEmpField3Period.Text = Language._Object.getCaption("lblPeriod", Me.lblEmpField3Period.Text)
            Me.lblEmpField3Employee.Text = Language._Object.getCaption("lblEmployee", Me.lblEmpField3Employee.Text)

            Me.lblEmpField3StartDate.Text = Language._Object.getCaption("lblStartDate", Me.lblEmpField3StartDate.Text)
            Me.lblEmpField3EndDate.Text = Language._Object.getCaption("lblEndDate", Me.lblEmpField3EndDate.Text)

            Me.lblEmpField3Status.Text = Language._Object.getCaption("lblStatus", Me.lblEmpField3Status.Text)
            Me.lblEmpField3Weight.Text = Language._Object.getCaption("lblWeight", Me.lblEmpField3Weight.Text)
            Me.lblEmpField3Percentage.Text = Language._Object.getCaption("lblPercentage", Me.lblEmpField3Percentage.Text)
            Me.btnEmpField3Save.Text = Language._Object.getCaption("btnSave", Me.btnEmpField3Save.Text).Replace("&", "")
            Me.btnEmpField3Cancel.Text = Language._Object.getCaption("btnClose", Me.btnEmpField3Cancel.Text).Replace("&", "")
            Me.dgvEmpField3Owner.Columns(1).HeaderText = Language._Object.getCaption("dgcolhEcode", Me.dgvEmpField3Owner.Columns(1).HeaderText)
            Me.dgvEmpField3Owner.Columns(2).HeaderText = Language._Object.getCaption("dgcolhEName", Me.dgvEmpField3Owner.Columns(2).HeaderText)
            'S.SANDEEP |14-MAR-2019| -- START
            Me.lblEmpField3GoalType.Text = Language._Object.getCaption("lblGoalType", Me.lblEmpField1GoalType.Text)
            Me.lblEmpField3GoalValue.Text = Language._Object.getCaption("lblGoalValue", Me.lblEmpField1GoalValue.Text)
            'S.SANDEEP |14-MAR-2019| -- END
            '====== Add/Edit EmpFiled2 (objfrmAddEditEmpField2) End =====


            '====== Add/Edit EmpFiled4 (objfrmAddEditEmpField4) Start =====
            Language.setLanguage(mstrModuleName_Emp4)
            Me.lblHeaderEmpField4.Text = Language._Object.getCaption(mstrModuleName_Emp4, Me.lblHeaderEmpField4.Text)
            Me.lblEmpField4Period.Text = Language._Object.getCaption("lblPeriod", Me.lblEmpField4Period.Text)
            Me.lblEmpField4Employee.Text = Language._Object.getCaption("lblEmployee", Me.lblEmpField4Employee.Text)

            Me.lblEmpField4StartDate.Text = Language._Object.getCaption("lblStartDate", Me.lblEmpField4StartDate.Text)
            Me.lblEmpField4EndDate.Text = Language._Object.getCaption("lblEndDate", Me.lblEmpField4EndDate.Text)

            Me.lblEmpField4Status.Text = Language._Object.getCaption("lblStatus", Me.lblEmpField4Status.Text)
            Me.lblEmpField4Weight.Text = Language._Object.getCaption("lblWeight", Me.lblEmpField4Weight.Text)
            Me.lblEmpField4Percentage.Text = Language._Object.getCaption("lblPercentage", Me.lblEmpField4Percentage.Text)
            Me.btnEmpField4Save.Text = Language._Object.getCaption("btnSave", Me.btnEmpField4Save.Text).Replace("&", "")
            Me.btnEmpField4Cancel.Text = Language._Object.getCaption("btnClose", Me.btnEmpField4Cancel.Text).Replace("&", "")
            Me.dgvEmpField4Owner.Columns(1).HeaderText = Language._Object.getCaption("dgcolhEcode", Me.dgvEmpField4Owner.Columns(1).HeaderText)
            Me.dgvEmpField4Owner.Columns(2).HeaderText = Language._Object.getCaption("dgcolhEName", Me.dgvEmpField4Owner.Columns(2).HeaderText)
            'S.SANDEEP |14-MAR-2019| -- START
            Me.lblEmpField4GoalType.Text = Language._Object.getCaption("lblGoalType", Me.lblEmpField1GoalType.Text)
            Me.lblEmpField4GoalValue.Text = Language._Object.getCaption("lblGoalValue", Me.lblEmpField1GoalValue.Text)
            'S.SANDEEP |14-MAR-2019| -- END
            '====== Add/Edit EmpFiled4 (objfrmAddEditEmpField4) End =====

            '====== Add/Edit EmpFiled5 (objfrmAddEditEmpField5) Start =====
            Language.setLanguage(mstrModuleName_Emp5)
            Me.lblHeaderEmpField5.Text = Language._Object.getCaption(mstrModuleName_Emp5, Me.lblHeaderEmpField5.Text)
            Me.lblEmpField5Period.Text = Language._Object.getCaption("lblPeriod", Me.lblEmpField5Period.Text)
            Me.lblEmpField5Employee.Text = Language._Object.getCaption("lblEmployee", Me.lblEmpField5Employee.Text)

            Me.lblEmpField5StartDate.Text = Language._Object.getCaption("lblStartDate", Me.lblEmpField5StartDate.Text)
            Me.lblEmpField5EndDate.Text = Language._Object.getCaption("lblEndDate", Me.lblEmpField5EndDate.Text)

            Me.lblEmpField5Status.Text = Language._Object.getCaption("lblStatus", Me.lblEmpField5Status.Text)
            Me.lblEmpField5Weight.Text = Language._Object.getCaption("lblWeight", Me.lblEmpField5Weight.Text)
            Me.lblEmpField5Percentage.Text = Language._Object.getCaption("lblPercentage", Me.lblEmpField5Percentage.Text)
            Me.btnEmpField5Save.Text = Language._Object.getCaption("btnSave", Me.btnEmpField5Save.Text).Replace("&", "")
            Me.btnEmpField5Cancel.Text = Language._Object.getCaption("btnClose", Me.btnEmpField5Cancel.Text).Replace("&", "")
            Me.dgvEmpField5Owner.Columns(1).HeaderText = Language._Object.getCaption("dgcolhEcode", Me.dgvEmpField5Owner.Columns(1).HeaderText)
            Me.dgvEmpField5Owner.Columns(2).HeaderText = Language._Object.getCaption("dgcolhEName", Me.dgvEmpField5Owner.Columns(2).HeaderText)
            'S.SANDEEP |14-MAR-2019| -- START
            Me.lblEmpField5GoalType.Text = Language._Object.getCaption("lblGoalType", Me.lblEmpField1GoalType.Text)
            Me.lblEmpField5GoalValue.Text = Language._Object.getCaption("lblGoalValue", Me.lblEmpField1GoalValue.Text)
            'S.SANDEEP |14-MAR-2019| -- END
            '====== Add/Edit EmpFiled5 (objfrmAddEditEmpField5) End =====

            ''====== Update Progress (frmUpdateFieldValue) Start =====
            'Language.setLanguage("frmUpdateFieldValue")
            'Me.lblUpdateProgressPageTitle.Text = Language._Object.getCaption("frmUpdateFieldValue", Me.lblUpdateProgressPageTitle.Text)
            'Me.lblUpdateProgressPageHeader.Text = Language._Object.getCaption("frmUpdateFieldValue", Me.lblUpdateProgressPageTitle.Text)
            'Me.lblUpdatePeriod.Text = Language._Object.getCaption("lblPeriod", Me.lblUpdatePeriod.Text)
            'Me.lblUpdateGoals.Text = Language._Object.getCaption("lblGoals", Me.lblUpdateGoals.Text)
            'Me.lblUpdateEmployee.Text = Language._Object.getCaption("lblEmployee", Me.lblUpdateEmployee.Text)
            'Me.lblUpdateDate.Text = Language._Object.getCaption("lblDate", Me.lblUpdateDate.Text)
            ''Me.lblUpdatePercentage.Text = Language._Object.getCaption("lblPercentage", Me.lblUpdatePercentage.Text)
            'Me.lblUpdateStatus.Text = Language._Object.getCaption("lblStatus", Me.lblUpdateStatus.Text)
            'Me.lblUpdateRemark.Text = Language._Object.getCaption("lblRemark", Me.lblUpdateRemark.Text)
            'Me.btnUpdateSave.Text = Language._Object.getCaption("btnSave", Me.btnUpdateSave.Text).Replace("&", "")
            'Me.btnUpdateClose.Text = Language._Object.getCaption("btnClose", Me.btnUpdateClose.Text).Replace("&", "")
            'Me.dgvHistory.Columns(3).HeaderText = Language._Object.getCaption("dgcolhDate", Me.dgvHistory.Columns(3).HeaderText)
            'Me.dgvHistory.Columns(4).HeaderText = Language._Object.getCaption("dgcolhPercent", Me.dgvHistory.Columns(4).HeaderText)
            'Me.dgvHistory.Columns(5).HeaderText = Language._Object.getCaption("dgcolhStatus", Me.dgvHistory.Columns(5).HeaderText)
            'Me.dgvHistory.Columns(6).HeaderText = Language._Object.getCaption("dgcolhRemark", Me.dgvHistory.Columns(6).HeaderText)
            'Me.dgvHistory.Columns(8).HeaderText = Language._Object.getCaption("dgcolhAccomplishedStatus", Me.dgvHistory.Columns(8).HeaderText)

            ''S.SANDEEP |08-JUL-2019| -- START
            ''ISSUE/ENHANCEMENT : PA CHANGES
            'Me.dgvHistory.Columns(10).HeaderText = Language._Object.getCaption("dgcolhLastValue", Me.dgvHistory.Columns(10).HeaderText)
            'lblChangeBy.Text = Language._Object.getCaption("lblChangeBy", Me.lblChangeBy.Text)
            'lblCaption3.Text = Language._Object.getCaption("lblCaption3", Me.lblCaption3.Text)
            'lblCaption4.Text = Language._Object.getCaption("lblCaption4", Me.lblCaption4.Text)
            'lblCaption1.Text = Language._Object.getCaption("lblCaption1", Me.lblCaption1.Text)
            'lblCaption2.Text = Language._Object.getCaption("lblCaption2", Me.lblCaption2.Text)
            ''S.SANDEEP |08-JUL-2019| -- END

            ''====== Update Progress (frmUpdateFieldValue) End =====

            ''====== Approve/Reject Goals Accomplishment (frmAppRejGoalAccomplishmentList) Start =====
            'Language.setLanguage(mstrModuleName_AppRejGoals)
            'Me.lblAccomplishment_Header.Text = Language._Object.getCaption(mstrModuleName_AppRejGoals, Me.lblAccomplishment_Header.Text)
            'Me.lblAccomplishment_gbFilterCriteria.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblAccomplishment_gbFilterCriteria.Text)
            'Me.lblAccomplishmentStatus.Text = Language._Object.getCaption("lblStatus", Me.lblAccomplishmentStatus.Text)
            'Me.btnAccomplishmentClose.Text = Language._Object.getCaption("btnClose", Me.btnAccomplishmentClose.Text).Replace("&", "")
            ''====== Approve/Reject Goals Accomplishment (frmAppRejGoalAccomplishmentList) End =====

        Catch ex As Exception
            DisplayMessage.DisplayError("SetLanguage" & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP [06 Jan 2016] -- END

   
    'S.SANDEEP |08-JAN-2019| -- START
    '#Region " Unlock Employee Popup "

    '#Region " Combobox Event(s) "

    '    Protected Sub cboUnlockPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnlockPeriod.SelectedIndexChanged
    '        Try
    '            If CInt(cboUnlockPeriod.SelectedValue) > 0 Then
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboUnlockPeriod.SelectedValue)
    '                nudNextDays.Maximum = objPeriod._Constant_Days
    '                mdtUnlockEmployeePrdEndDate = objPeriod._End_Date.Date
    '                objlblPeriodDates.Text = objPeriod._Start_Date.Date.ToShortDateString() & " - " & objPeriod._End_Date.Date.ToShortDateString()
    '                objPeriod = Nothing
    '            Else
    '                objlblPeriodDates.Text = ""
    '                mdtUnlockEmployeePrdEndDate = Nothing
    '                nudNextDays.Maximum = 100
    '            End If
    '            mblnpopup_UnlockEmp = True
    '            popup_unlockemp.Show()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("cboUnlockPeriod_SelectedIndexChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub cboLockType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLockType.SelectedIndexChanged
    '        Try
    '            dgvUnlockEmp.DataSource = Nothing
    '            dgvUnlockEmp.DataBind()
    '            btnUProcess.Enabled = True
    '            mblnpopup_UnlockEmp = True
    '            popup_unlockemp.Show()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("cboLockType_SelectedIndexChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Button's Event(s) "

    '    Protected Sub btnUSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUSearch.Click
    '        Try
    '            If CInt(cboUnlockPeriod.SelectedValue) <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_unlockemployee, 1, "Period is compulsory information.Please select period."), Me)
    '                cboUnlockPeriod.Focus()
    '                Exit Sub
    '            End If

    '            If CInt(cboLockType.SelectedValue) <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_unlockemployee, 2, "Lock type is compulsory information.Please select Lock type."), Me)
    '                cboLockType.Focus()
    '                Exit Sub
    '            End If
    '            Call FillUnlockEmpGrid()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("btnUSearch_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub btnUReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUReset.Click
    '        Try
    '            cboUnlockPeriod.SelectedValue = 0
    '            cboLockType.SelectedIndex = 0
    '            dgvData.DataSource = Nothing
    '            dgvData.DataBind()
    '            objlblPeriodDates.Text = ""
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("btnUReset_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub btnUpdateEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateEmp.Click
    '        Try
    '            mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
    '            If Convert.ToInt32(txtNextDays.Text) <= 0 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_unlockemployee, 3, "Sorry, Next Lock days are mandatory information. Please set the next lock days."), Me)
    '                Exit Sub
    '            End If

    '            If radApplyToAll.Checked = False AndAlso radApplyTochecked.Checked = False Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_unlockemployee, 4, "Sorry, Please select atleast one setting to apply changes for the selected or all employee(s)."), Me)
    '                Exit Sub
    '            End If

    '            Dim drRow() As DataRow
    '            If mdtUnlockEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
    '                drRow = mdtUnlockEmployee.Select("ischeck = True ")
    '            Else
    '                drRow = mdtUnlockEmployee.Select("")
    '            End If
    '            drRow.ToList.ForEach(Function(x) UpdateRowValue(x, Convert.ToInt32(txtNextDays.Text)))

    '            Dim blnFlag As Boolean = False
    '            If mdtUnlockEmployee.Rows.Count <= 0 Then
    '                mdtUnlockEmployee.Rows.Add(mdtUnlockEmployee.NewRow())
    '                blnFlag = True
    '            End If
   
    '            dgvUnlockEmp.DataSource = mdtUnlockEmployee
    '            dgvUnlockEmp.DataBind()
    '            If blnFlag Then dgvUnlockEmp.Rows(0).Visible = False
    '            btnUProcess.Enabled = Not blnFlag
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("btnUpdateEmp_Click : " & ex.Message, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub btnUProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUProcess.Click
    '        Try
    '            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub

    '            mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
    '            If mdtUnlockEmployee.AsEnumerable.Where(Function(x) x.Field(Of Integer)("unlockdays") > 0).Count() <= 0 Then
    '                cnfUnlockDays.Message = Language.getMessage(mstrModuleName_unlockemployee, 6, "You have not set any next locking days for") & " " & _
    '                                        cboLockType.SelectedItem.Text & " " & Language.getMessage(mstrModuleName_unlockemployee, 7, ", due to this employee will not be locked for planning once it's unlocked for the selected assessment period.") & vbCrLf & _
    '                                        Language.getMessage(mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
    '                cnfUnlockDays.Show()
    '            Else
    '                If mdtUnlockEmployee.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of String)("message") = "").Count() <= 0 Then
    '                    cnfUnlockCheck.Message = Language.getMessage(mstrModuleName_unlockemployee, 9, "You have not checked any employee from below list") & " " & _
    '                                         Language.getMessage(mstrModuleName_unlockemployee, 10, ", due to this all employee will be unlocked for the selected assessment period.") & vbCrLf & _
    '                                         Language.getMessage(mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
    '                    cnfUnlockCheck.Show()
    '                Else
    '                    Call cnfUnlockCheck_buttonYes_Click(New Object(), New EventArgs())
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("btnUProcess_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub cnfUnlockDays_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfUnlockDays.buttonYes_Click
    '        Try
    '            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub
    '            mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
    '            If mdtUnlockEmployee.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of String)("message") = "").Count() <= 0 Then
    '                cnfUnlockCheck.Message = Language.getMessage(mstrModuleName_unlockemployee, 9, "You have not checked any employee from below list") & " " & _
    '                                         Language.getMessage(mstrModuleName_unlockemployee, 10, ", due to this all employee will be unlocked for the selected assessment period.") & vbCrLf & _
    '                                         Language.getMessage(mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
    '                cnfUnlockCheck.Show()
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("cnfUnlockDays_buttonYes_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub cnfUnlockCheck_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfUnlockCheck.buttonYes_Click
    '        Try
    '            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub
    '            mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
    '            Dim drRow() As DataRow = Nothing
    '            If mdtUnlockEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
    '                drRow = mdtUnlockEmployee.Select("ischeck = True And message = '' ")
    '            Else
    '                drRow = mdtUnlockEmployee.Select("message = '' ")
    '            End If

    '            Dim iMsgText As String = ""
    '            For Each dr As DataRow In drRow
    '                iMsgText = ""
    '                objUnlock._Lockunkid = dr("lockunkid")
    '                objUnlock._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
    '                objUnlock._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
    '                objUnlock._Audittype = enAuditType.EDIT
    '                objUnlock._Audituserunkid = Session("UserId")
    '                objUnlock._Form_Name = mstrModuleName_unlockemployee
    '                objUnlock._Hostname = Session("HOST_NAME")
    '                objUnlock._Ip = Session("IP_ADD")
    '                objUnlock._Islock = False
    '                objUnlock._Isunlocktenure = True
    '                objUnlock._Isweb = True
    '                If IsDBNull(dr("nextlockdatetime")) = False Then
    '                    objUnlock._Nextlockdatetime = CDate(dr("nextlockdatetime"))
    '                Else
    '                    objUnlock._Nextlockdatetime = Nothing
    '                End If
    '                objUnlock._Unlockdays = CInt(dr("unlockdays"))
    '                objUnlock._Unlockreason = txtUnlockEmpRemark.Text
    '                objUnlock._Unlockuserunkid = Session("UserId")
    '                If objUnlock.Update() = False Then
    '                    dr("Message") = objUnlock._Message
    '                    Continue For
    '                Else
    '                    objUnlock.SendUnlockNotification(dr("oName").ToString(), _
    '                                                     dr("emp_email").ToString(), _
    '                                                     cboUnlockPeriod.SelectedItem.Text, _
    '                                                     CType(cboLockType.SelectedValue, clsassess_plan_eval_lockunlock.enAssementLockType), _
    '                                                     Session("CompanyUnkId"), _
    '                                                     enLogin_Mode.MGR_SELF_SERVICE, _
    '                                                     IIf(Session("Firstname") & "" & Session("Surname") = "", Session("UserName"), Session("Firstname") & " " & Session("Surname")).ToString(), _
    '                                                     CInt(dr("unlockdays")), _
    '                                                     objUnlock._Nextlockdatetime, _
    '                                                     mstrModuleName_unlockemployee, _
    '                                                     Session("IP_ADD"), _
    '                                                     Session("UserId"), _
    '                                                     ConfigParameter._Object._CurrentDateAndTime, True, 0, Session("HOST_NAME"), _
    '                                                     Session("ArutiSelfServiceURL"))
    '                    dr("Message") = cboLockType.Text & " " & Language.getMessage(mstrModuleName_unlockemployee, 12, "Succesfully.")
    '                    Continue For
    '                End If
    '            Next
    '            Call FillUnlockEmpGrid()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("cnfUnlockCheck_buttonYes_Click : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Private Function(s) "

    '    Private Sub FillUnlockEmpGrid()
    '        Dim dsList As New DataSet
    '        Dim strFilter As String = String.Empty
    '        Try
    '            If CInt(cboUnlockPeriod.SelectedValue) > 0 Then
    '                strFilter &= "AND periodunkid = " & CInt(cboUnlockPeriod.SelectedValue)
    '            End If
    '            If CInt(cboLockType.SelectedValue) > 0 Then
    '                strFilter &= "AND locktypeid = " & CInt(cboLockType.SelectedValue)
    '            End If

    '            Dim csvIds As String = String.Empty
    '            Dim dsMapEmp As New DataSet
    '            Dim objEval As New clsevaluation_analysis_master
    '            dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
    '                                                    Session("UserId"), _
    '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                    True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

    '            If dsMapEmp.Tables("List").Rows.Count > 0 Then

    '                mintAssessorMasterId = CInt(dsMapEmp.Tables("List").Rows(0)("Id"))
    '                mintAssessorEmployeeId = CInt(dsMapEmp.Tables("List").Rows(0)("EmpId"))

    '                dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
    '                                                          Session("UserId"), _
    '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
    '                                                          Session("IsIncludeInactiveEmp"), _
    '                                                          CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


    '                strFilter = "AND hremployee_master.employeeunkid IN "
    '                csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
    '                If csvIds.Trim.Length > 0 Then
    '                    strFilter &= "(" & csvIds & ")"
    '                Else
    '                    strFilter &= "(0)"
    '                End If
    '            Else
    '                strFilter = "AND hremployee_master.employeeunkid IN (0) "
    '            End If

    '            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

    '            dsList = objUnlock.GetList(Session("Database_Name"), _
    '                                       Session("UserId"), _
    '                                       Session("Fin_year"), _
    '                                       Session("CompanyUnkId"), _
    '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                       Session("UserAccessModeSetting"), True, _
    '                                       Session("IsIncludeInactiveEmp"), "List", strFilter, False)

    '            Dim dCol As New DataColumn
    '            With dCol
    '                .DataType = GetType(Boolean)
    '                .ColumnName = "ischeck"
    '                .DefaultValue = False
    '            End With
    '            dsList.Tables(0).Columns.Add(dCol)
    '            dCol = New DataColumn
    '            With dCol
    '                .DataType = GetType(String)
    '                .ColumnName = "message"
    '                .DefaultValue = ""
    '            End With
    '            dsList.Tables(0).Columns.Add(dCol)
    '            Dim blnFlag As Boolean = False
    '            mdtUnlockEmployee = dsList.Tables(0).Copy()
    '            If mdtUnlockEmployee.Rows.Count <= 0 Then
    '                mdtUnlockEmployee.Rows.Add(mdtUnlockEmployee.NewRow())
    '                blnFlag = True
    '            End If
    '            dgvUnlockEmp.DataSource = mdtUnlockEmployee
    '            dgvUnlockEmp.DataBind()
    '            If blnFlag Then dgvUnlockEmp.Rows(0).Visible = False
    '            btnUProcess.Enabled = Not blnFlag
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("FillUnlockEmpGrid : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer) As Boolean
    '        Try
    '            Dim mdtdate As DateTime = CDate(dr("lockunlockdatetime")).AddDays(intValue)
    '            If mdtUnlockEmployeePrdEndDate <> Nothing Then
    '                If mdtdate.Date > mdtUnlockEmployeePrdEndDate.Date Then
    '                    dr("message") = Language.getMessage(mstrModuleName_unlockemployee, 5, "Sorry, Next Lock date exceed the period end date.")
    '                Else
    '                    dr("udays") = intValue
    '                    dr("nextlockdatetime") = mdtdate
    '                    dr("message") = ""
    '                    dr("unlockdays") = intValue
    '                End If
    '            Else
    '                dr("udays") = intValue
    '                dr("nextlockdatetime") = mdtdate
    '                dr("message") = ""
    '                dr("unlockdays") = intValue
    '            End If
    '            Return True
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("UpdateRowValue : " & ex.Message, Me)
    '        End Try
    '    End Function
   
    '#End Region

    '#Region " Grid Event(s) "

    '    Protected Sub dgvUnlockEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvUnlockEmp.RowDataBound
    '        Try
    '            If e.Row.RowType = DataControlRowType.DataRow Then
    '                Call SetDateFormat()
    '                Dim index As Integer = 0

    '                index = GetColumnIndex.getColumnID_Griview(dgvUnlockEmp, "dgcolhLockDate", False, True)
    '                If e.Row.Cells(index).Text.Length > 0 AndAlso e.Row.Cells(index).Text <> "&nbsp;" Then
    '                    e.Row.Cells(index).Text = CDate(e.Row.Cells(index).Text).Date.ToShortDateString()
    '                End If

    '                index = GetColumnIndex.getColumnID_Griview(dgvUnlockEmp, "dgcolhNextLockDate", False, True)
    '                If e.Row.Cells(index).Text.Length > 0 AndAlso e.Row.Cells(index).Text <> "&nbsp;" Then
    '                    e.Row.Cells(index).Text = CDate(e.Row.Cells(index).Text).Date.ToShortDateString()
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("dgvUnlockEmp_RowDataBound : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Checkbox Event(s) "

    '    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '        Try
    '            Dim cb As CheckBox = CType(sender, CheckBox)
    '            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub
    '            Dim j As Integer = 0
    '            Dim dvEmployee As DataTable = CType(Me.ViewState("mdtUnlockEmployee"), DataTable)
    '            Dim index As Integer = 0
    '            index = getColumnID_Griview(dgvUnlockEmp, "colhEmployee", False, True)
    '            For i As Integer = 0 To dgvUnlockEmp.Rows.Count - 1
    '                Dim gvRow As GridViewRow = dgvUnlockEmp.Rows(i)
    '                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '                Dim dRow() As DataRow = dvEmployee.Select("Employee = '" & gvRow.Cells(index).Text & "'")
    '                If dRow.Length > 0 Then
    '                    dRow(0).Item("ischeck") = cb.Checked
    '                End If
    '                mdtUnlockEmployee.AcceptChanges()
    '            Next
    '            Me.ViewState("mdtUnlockEmployee") = mdtUnlockEmployee
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("chkHeder1_CheckedChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '        Try
    '            Dim cb As CheckBox = CType(sender, CheckBox)
    '            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '            Dim index As Integer = 0
    '            index = getColumnID_Griview(dgvUnlockEmp, "colhEmployee", False, True)
    '            If gvr.Cells.Count > 0 Then
    '                Dim dRow() As DataRow = CType(Me.ViewState("mdtUnlockEmployee"), DataTable).Select("Employee = '" & gvr.Cells(index).Text & "'")
    '                If dRow.Length > 0 Then
    '                    dRow(0).Item("ischeck") = cb.Checked
    '                End If
    '            End If
    '            CType(Me.ViewState("mdtUnlockEmployee"), DataTable).AcceptChanges()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError("chkbox1_CheckedChanged : " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#End Region
    'S.SANDEEP |08-JAN-2019| -- END  

    
End Class
