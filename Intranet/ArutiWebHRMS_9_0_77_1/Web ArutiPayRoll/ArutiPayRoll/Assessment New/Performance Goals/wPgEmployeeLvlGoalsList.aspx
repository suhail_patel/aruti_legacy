﻿<%@ Page Title="Employee Goals List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmployeeLvlGoalsList.aspx.vb" Inherits="wPgEmployeeLvlGoalsList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <link href="../../App_Themes/PA_Style.css" type="text/css" />

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        $(".objAddBtn").live("mousedown", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
    </script>

    <script type="text/javascript">
        $(".popMenu").live("click", function(e) {
            var id = jQuery(this).attr('id').replace('_backgroundElement', '');
            $find(id).hide();
        });
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        $("input[id$=btnOperation]").live("click", function() {
            x = $("#<%= btnOperation.ClientID %>").offset();
            $("#<%= hdf_locationx.ClientID %>").val(x.top);
            $("#<%= hdf_locationy.ClientID %>").val(x.left);
        });
    </script>

    <script type="text/javascript">
        function setscrollPosition(sender) {
            sender.scrollLeft = document.getElementById("<%=hdf_leftposition.ClientID%>").value;
            sender.scrollTop = document.getElementById("<%=hdf_topposition.ClientID%>").value;
        }
        function getscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("<%=pnl_dgvData.ClientID%>").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("<%=pnl_dgvData.ClientID%>").scrollLeft;
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
        }
        $(document).ready(function() {
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
        });
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }

        $("[id*=chkHeder1]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    $(this).attr("checked", "checked");

                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkbox1]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");

            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
            }
        }    

        });    
    </script>

    <%--    <script>
//        function IsValidAttach() {
//            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
//                alert('Please Select Document Type.');
//                $('.cboScanDcoumentType').focus();
//                return false;
//            }
//        }    
    </script>--%>
    <%--<script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>--%>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server" EnableViewState="true">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Goals List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ibwm" style="width: 30%">
                                            <div style="margin-bottom: 15px;">
                                                <h2 style="margin: 0 !important; color: #666; font-weight: normal; font-size: 14px;
                                                    text-align: left">
                                                    Mandatory Filters</h2>
                                            </div>
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                                    <td style="width: 100%;" align="left">
                                                        <asp:Label ID="lblEmployee" runat="server" Width="100%" Text="Employee"></asp:Label>
                                                        <asp:DropDownList ID="cboEmployee" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                        </asp:DropDownList>
                                            </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 100%;" align="left">
                                                        <asp:Label ID="lblPeriod" runat="server" Width="100%" Text="Period"></asp:Label>
                                                        <asp:DropDownList ID="cboPeriod" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                        </asp:DropDownList>
                                            </td>
                                        </tr>
                                            </table>
                                        </div>
                                        <div class="ibwm" style="width: 65%; padding-left: 10px; border-left: 1px solid #ddd;">
                                            <div style="margin-bottom: 15px;">
                                                <h2 style="margin: 0 !important; color: #666; font-weight: normal; font-size: 14px;
                                                    text-align: left">
                                                    Optional Filters</h2>
                                            </div>
                                            <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                        <asp:Label ID="lblPerspective" runat="server" Width="100%" Text="Perspective"></asp:Label>
                                                <asp:DropDownList ID="cboPerspective" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                        <asp:Label ID="objlblField1" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                                <asp:DropDownList ID="cboFieldValue1" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="objlblField2" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                                        <asp:DropDownList ID="cboFieldValue2" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                        <asp:Label ID="objlblField3" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                                        <asp:DropDownList ID="cboFieldValue3" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                        <asp:Label ID="objlblField4" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                                        <asp:DropDownList ID="cboFieldValue4" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%;" align="left">
                                                        <asp:Label ID="objlblField5" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                                <asp:DropDownList ID="cboFieldValue5" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 33%;" align="left">
                                                <asp:Label ID="lblStatus" runat="server" Width="100%" Text="Status"></asp:Label>
                                                <asp:DropDownList ID="cboStatus" runat="server" Width="100%" Height="25" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <div style="float: left;">
                                            <asp:Label ID="objlblCurrentStatus" runat="server" Font-Bold="true" Font-Size="Small"
                                                ForeColor="Red"></asp:Label>
                                        </div>
                                        <asp:Label ID="objlblTotalWeight" runat="server" Text="" Font-Bold="true"></asp:Label>
                                        <asp:Button ID="btnOperation" runat="server" Text="Operation" CssClass="btndefault"
                                            Visible="False" />
                                        <asp:Button ID="btnGlobalAssign" runat="server" CssClass="btndefault" Text="Global Assign Goals" />
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                    </div>
                                </div>
                            </div>
                            <div class="panel-default">
                                <div class="panel-body-default">
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 99%;
                                        overflow: auto" class="gridscroll">
                                        <asp:Panel ID="pnl_dgvData" runat="server" Width="100%" Style="text-align: center">
                                            <asp:GridView ID="dgvData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                RowStyle-CssClass="griviewitem" Style="margin: 0px" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                DataKeyNames="isfinal,UoMType,empfield1unkid,empfield2unkid,empfield3unkid,empfield4unkid,empfield5unkid,goaltypeid,goalvalue,Field1,Field2,Field3,Field4,Field5,opstatusid">
                                                <Columns>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                    <div class="btn-default" style="height: 31px">
                                        <div style="float: left">
                                            <uc3:OperationButton ID="cmnuOperation" runat="server" Text="Operation" TragateControlId="data" />
                                            <div id="data" style="display: none">
                                                <%--<asp:LinkButton ID="mnuAppRejGoalAccomplished" Style="min-width: 200px" runat="server"
                                                    Text="App/Rej Goals Accomplishment" Visible="false"></asp:LinkButton>
                                                <asp:LinkButton ID="btnSubmitForGoalAccomplished" runat="server" Style="min-width: 200px"
                                                    Text="Submit For Goal Accomplished" Visible="false"></asp:LinkButton>--%>
                                                <%--'S.SANDEEP |08-JAN-2019| -- START--%>
                                                <asp:LinkButton ID="btnUnlockEmployee" runat="server" Style="min-width: 200px" Text="Unlock Employee(s)"></asp:LinkButton>
                                                <%--'S.SANDEEP |08-JAN-2019| -- END--%>
                                            </div>
                                        </div>
                                        <asp:Button ID="btnclose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                            <div id="div_PopupControl" style="width: 100%;">
                                <div id="trimageadd">
                                    <asp:HiddenField ID="hdfv" runat="server" />
                                    <cc1:ModalPopupExtender ID="popAddButton" BehaviorID="MPE" runat="server" TargetControlID="hdfv"
                                        CancelControlID="btnAddCancel" DropShadow="true" BackgroundCssClass="popMenu"
                                        PopupControlID="pnlImageAdd" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnlImageAdd" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 100%">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                                <asp:DataList ID="dlmnuAdd" runat="server">
                                                    <AlternatingItemStyle BackColor="Transparent" />
                                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 100%; height: 25px" class="mnuTools">
                                                                <asp:LinkButton ID='objadd' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                    OnClick="lnkAdd_Click" CssClass="lnkhover" />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:HiddenField ID="btnAddCancel" runat="server" />
                                                <asp:HiddenField ID="btndlmnuAdd" runat="server" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="trimageEdit">
                                    <cc1:ModalPopupExtender ID="popEditButton" runat="server" TargetControlID="btndlmnuEdit"
                                        CancelControlID="btnEditCancel" DropShadow="true" BackgroundCssClass="popMenu"
                                        PopupControlID="pnlImageEdit" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnlImageEdit" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 100%">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                                <asp:DataList ID="dlmnuEdit" runat="server">
                                                    <AlternatingItemStyle BackColor="Transparent" />
                                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 100%; height: 25px" class="mnuTools">
                                                                <asp:LinkButton ID='objEdit' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                    CssClass="lnkhover" OnClick="lnkEdit_Click" />
                                                                <asp:HiddenField ID="objEditOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:HiddenField ID="btnEditCancel" runat="server" />
                                                <asp:HiddenField ID="btndlmnuEdit" runat="server" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="trimageDelete">
                                    <cc1:ModalPopupExtender ID="popDeleteButton" runat="server" TargetControlID="btndlmnuDelete"
                                        CancelControlID="btnDeleteCancel" DropShadow="true" BackgroundCssClass="popMenu"
                                        PopupControlID="pnlImageDelete" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnlImageDelete" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 100%">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                                <asp:DataList ID="dlmnuDelete" runat="server">
                                                    <AlternatingItemStyle BackColor="Transparent" />
                                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="width: 100%; height: 25px" class="mnuTools">
                                                                <asp:LinkButton ID='objDelete' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                                                    CssClass="lnkhover" OnClick="lnkDelete_Click" />
                                                                <asp:HiddenField ID="objDeleteOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:HiddenField ID="btnDeleteCancel" runat="server" />
                                                <asp:HiddenField ID="btndlmnuDelete" runat="server" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="hiddenFiled">
                                    <asp:HiddenField ID="hdf_locationx" runat="server" />
                                    <asp:HiddenField ID="hdf_locationy" runat="server" />
                                </div>
                                <div id="objfrmAddEditEmpField1">
                                    <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField1" runat="server" TargetControlID="hdf_btnEmpField1Save"
                                        CancelControlID="hdf_btnEmpField1Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                        PopupControlID="pnl_objfrmAddEditEmpField1" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_objfrmAddEditEmpField1" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 750px">
                                        <div class="panel-primary" style="margin-bottom: 0px">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblheaderEmpField1" runat="server" Text="AddEditEmpField1"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div3" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField1Period" runat="server" Text="Period"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField1Period" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField1Employee" runat="server" Text="Employee"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField1EmployeeName" runat="server" ReadOnly="true"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_txtEmpField1EmployeeName" runat="server" />
                                                            </div>
                                                            <div class="ib no-padding" style="width: 32%">
                                                                <asp:Label ID="lblEmpField1Perspective" runat="server" Text="Perspective"></asp:Label>
                                                                <asp:DropDownList ID="cboEmpField1Perspective" runat="server" Height="20px" Width="225px">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <asp:Panel ID="pnlEmpField1lblField1" runat="server">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="objEmpField1lblField1" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:DropDownList ID="cboEmpField1FieldValue1" runat="server" Height="20px" Width="225px">
                                                                </asp:DropDownList>
                                                            </div>
                                                            </asp:Panel>
                                                            <div class="ibwm no-padding" style="width: 64%">
                                                                <asp:Label ID="objEmpField1lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField1EmpField1" runat="server" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_txtEmpField1EmpField1" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="ibwm" style="width: 100%">
                                                            <asp:Panel ID="pnl_RightEmpField1" runat="server">
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 31%">
                                                                        <asp:Label ID="lblEmpField1StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        <br />
                                                                        <uc2:DateCtrl ID="dtpEmpField1StartDate" runat="server" AutoPostBack="false" />
                                                                    </div>
                                                                    <div class="ibwm" style="width: 31%">
                                                                        <asp:Label ID="lblEmpField1EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        <br />
                                                                        <uc2:DateCtrl ID="dtpEmpField1EndDate" runat="server" AutoPostBack="false" />
                                                                    </div>
                                                                    <div class="ibwm" style="width: 32%">
                                                                        <asp:Label ID="lblEmpField1Status" runat="server" Text="Status"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField1Status" runat="server" Height="25px" Width="200px"
                                                                            Enabled="false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <cc1:TabContainer ID="objEmpField1tabcRemarks" runat="server" Height="50px">
                                                                            <cc1:TabPanel ID="objEmpField1tabpRemark1" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField1Remark1" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField1Remark1" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField1Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                            <cc1:TabPanel ID="objEmpField1tabpRemark2" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField1Remark2" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField1Remark2" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField1Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                            <cc1:TabPanel ID="objEmpField1tabpRemark3" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField1Remark3" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField1Remark3" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField1Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                        </cc1:TabContainer>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField1Weight" runat="server" Text="Weight"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField1Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                            Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField1Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField1Percent" runat="server" Style="text-align: right" Text="0.00"
                                                                            Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField1GoalType" runat="server" Text="Goal Type"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField1GoalType" runat="server" Height="20px" Width="125px"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField1UoMType" runat="server" Text="UoM Type"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField1UomType" runat="server" Height="20px" Width="125px">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField1GoalValue" runat="server" Text="Goal Value"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField1GoalValue" runat="server" Style="text-align: right"
                                                                            Text="0.00" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField1SearchEmp" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <asp:Panel ID="pnl_EmpField1dgvower" runat="server" Style="height: 100px; overflow: auto;">
                                                                            <asp:DataGrid ID="dgvEmpField1Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                Width="100%" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="chkEmpField1allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField1AllSelect_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkEmpField1select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField1select_CheckedChanged" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                        HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                        HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                    </asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                                    </asp:Panel>
                                                        </div>
                                                        <div class="btn-default">
                                                            <asp:HiddenField ID="hdf_btnEmpField1Save" runat="server" />
                                                            <asp:Button ID="btnEmpField1Save" runat="server" Text="Save" CssClass="btndefault" />
                                                            <asp:Button ID="btnEmpField1Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="objfrmAddEditEmpField2">
                                    <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField2" runat="server" TargetControlID="hdf_btnEmpField2Save"
                                        CancelControlID="hdf_btnEmpField2Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                        PopupControlID="pnl_objfrmAddEditEmpField2" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_objfrmAddEditEmpField2" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 750px">
                                        <div class="panel-primary" style="margin-bottom: 0px">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblHeaderEmpField2" runat="server" Text="AddEditEmpField2"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div4" class="panel-default">
                                                    <div id="Div5" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div6" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField2Period" runat="server" Text="Period"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField2Period" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField2Employee" runat="server" Text="Employee"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField2EmployeeName" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 32%">
                                                                <asp:Label ID="objEmpField2lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:DropDownList ID="cboEmpField2EmpFieldValue1" runat="server" Height="20px" Width="225px"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdf_cboEmpField2EmpFieldValue1" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 64%">
                                                                <asp:Label ID="objEmpField2lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField2EmpField2" runat="server" TextMode="MultiLine" Rows="2"
                                                                    Style="resize: none"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_txtEmpField2EmpField2" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="ibwm" style="width: 100%">
                                                            <asp:Panel ID="pnl_RightEmpField2" runat="server">
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 31%">
                                                                        <asp:Label ID="lblEmpField2StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        <br />
                                                                        <uc2:DateCtrl ID="dtpEmpField2StartDate" runat="server" />
                                                                    </div>
                                                                    <div class="ibwm" style="width: 31%">
                                                                        <asp:Label ID="lblEmpField2EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        <br />
                                                                        <uc2:DateCtrl ID="dtpEmpField2EndDate" runat="server" />
                                                                    </div>
                                                                    <div class="ibwm" style="width: 32%">
                                                                        <asp:Label ID="lblEmpField2Status" runat="server" Text="Status"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField2Status" runat="server" Height="25px" Width="225px"
                                                                            Enabled="false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <cc1:TabContainer ID="objEmpField2tabcRemarks" runat="server" Height="60px">
                                                                            <cc1:TabPanel ID="objEmpField2tabpRemark1" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField2Remark1" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField2Remark1" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField2Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                            <cc1:TabPanel ID="objEmpField2tabpRemark2" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField2Remark2" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField2Remark2" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField2Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                            <cc1:TabPanel ID="objEmpField2tabpRemark3" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField2Remark3" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField2Remark3" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField2Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                        </cc1:TabContainer>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField2Weight" runat="server" Text="Weight"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField2Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                            Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField2Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField2Percent" runat="server" Style="text-align: right" Text="0.00"
                                                                            Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField2GoalType" runat="server" Text="Goal Type"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField2GoalType" runat="server" Height="20px" Width="125px"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField2UoMType" runat="server" Text="UoM Type"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField2UomType" runat="server" Height="20px" Width="125px">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField2GoalValue" runat="server" Text="Goal Value"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField2GoalValue" runat="server" Style="text-align: right"
                                                                            Text="0.00" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField2SearchEmp" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <asp:Panel ID="pnl_EmpField2dgvower" runat="server" Style="height: 90px; overflow: auto;">
                                                                            <asp:DataGrid ID="dgvEmpField2Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                        HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="chkEmpField2allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField2AllSelect_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkEmpField2select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField2select_CheckedChanged" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                    </asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="btn-default">
                                                            <asp:HiddenField ID="hdf_btnEmpField2Save" runat="server" />
                                                            <asp:Button ID="btnEmpField2Save" runat="server" Text="Save" CssClass="btndefault" />
                                                            <asp:Button ID="btnEmpField2Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="objfrmAddEditEmpField3">
                                    <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField3" runat="server" TargetControlID="hdf_btnEmpField3Save"
                                        CancelControlID="hdf_btnEmpField3Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                        PopupControlID="pnl_objfrmAddEditEmpField3" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_objfrmAddEditEmpField3" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 760px">
                                        <div class="panel-primary" style="margin-bottom: 0px">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblHeaderEmpField3" runat="server" Text="AddEditEmpField3"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div7" class="panel-default">
                                                    <div id="Div8" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div9" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField3Period" runat="server" Text="Period"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField3Period" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField3Employee" runat="server" Text="Employee"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField3Employee" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 32%">
                                                                <asp:Label ID="objEmpField3lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField3txtEmpField1" runat="server" ReadOnly="true" Width="220px"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%;vertical-align: top;">
                                                                <asp:Label ID="objEmpField3lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:DropDownList ID="cboEmpField3EmpFieldValue2" runat="server" Height="20px" AutoPostBack="true"
                                                                    Width="220px">
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdf_cboEmpField3EmpFieldValue2" runat="server" />
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="objEmpField3lblOwrField3" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField3EmpField3" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_txtEmpField3EmpField3" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="ibwm" style="width: 100%">
                                                            <asp:Panel ID="pnl_RightEmpField3" runat="server">
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 31%">
                                                                        <asp:Label ID="lblEmpField3StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        <br />
                                                                        <uc2:DateCtrl ID="dtpEmpField3StartDate" runat="server" />
                                                                    </div>
                                                                    <div class="ibwm" style="width: 31%">
                                                                        <asp:Label ID="lblEmpField3EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        <br />
                                                                        <uc2:DateCtrl ID="dtpEmpField3EndDate" runat="server" />
                                                                    </div>
                                                                    <div class="ibwm" style="width: 32%">
                                                                        <asp:Label ID="lblEmpField3Status" runat="server" Text="Status"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField3Status" runat="server" Height="25px" Width="225px"
                                                                            Enabled="false">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <cc1:TabContainer ID="objEmpField3tabcRemarks" runat="server" Height="60px">
                                                                            <cc1:TabPanel ID="objEmpField3tabpRemark1" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField3Remark1" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField3Remark1" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField3Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                            <cc1:TabPanel ID="objEmpField3tabpRemark2" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField3Remark2" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField3Remark2" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField3Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                            <cc1:TabPanel ID="objEmpField3tabpRemark3" runat="server" HeaderText="">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="lblEmpField3Remark3" runat="server"></asp:Label>
                                                                                    <asp:HiddenField ID="hdf_lblEmpField3Remark3" runat="server" />
                                                                                </HeaderTemplate>
                                                                                <ContentTemplate>
                                                                                    <asp:TextBox ID="txtEmpField3Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                                        Style="resize: none;"></asp:TextBox>
                                                                                </ContentTemplate>
                                                                            </cc1:TabPanel>
                                                                        </cc1:TabContainer>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField3Weight" runat="server" Text="Weight"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField3Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                            Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField3Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField3Percent" runat="server" Style="text-align: right" Text="0.00"
                                                                            Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField3GoalType" runat="server" Text="Goal Type"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField3GoalType" runat="server" Height="20px" Width="125px"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField3UoMType" runat="server" Text="UoM Type"></asp:Label>
                                                                        <asp:DropDownList ID="cboEmpField3UomType" runat="server" Height="20px" Width="125px">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="ib no-padding" style="width: 18%">
                                                                        <asp:Label ID="lblEmpField3GoalValue" runat="server" Text="Goal Value"></asp:Label>
                                                                        <asp:TextBox ID="txtEmpField3GoalValue" runat="server" Style="text-align: right"
                                                                            Text="0.00" Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField3SearchEmp" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ibwm" style="width: 100%">
                                                                        <asp:Panel ID="pnl_EmpField3dgvOwner" runat="server" Style="height: 90px; overflow: auto;">
                                                                            <asp:DataGrid ID="dgvEmpField3Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="chkEmpField3allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField3AllSelect_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkEmpField3select" runat="server" Checked='<%# Eval("ischeck")  %>'
                                                                                                AutoPostBack="true" OnCheckedChanged="chkEmpField3select_CheckedChanged" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                    </asp:BoundColumn>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="btn-default">
                                                            <asp:HiddenField ID="hdf_btnEmpField3Save" runat="server" />
                                                            <asp:Button ID="btnEmpField3Save" runat="server" Text="Save" CssClass="btndefault" />
                                                            <asp:Button ID="btnEmpField3Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="objfrmAddEditEmpField4">
                                    <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField4" runat="server" TargetControlID="hdf_btnEmpField4Save"
                                        CancelControlID="hdf_btnEmpField4Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                        PopupControlID="pnl_objfrmAddEditEmpField4" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_objfrmAddEditEmpField4" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 760px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblHeaderEmpField4" runat="server" Text="AddEditOwrField3"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div10" class="panel-default">
                                                    <div id="Div11" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div12" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField4Period" runat="server" Text="Period"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField4Period" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField4Employee" runat="server" Text="Employee"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField4Employee" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 32%">
                                                                <asp:Label ID="objEmpField4lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField4txtEmpField1" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%;vertical-align:top">
                                                                <asp:Label ID="objEmpField4lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField4txtEmpField2" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%;vertical-align:top">
                                                                <asp:Label ID="objEmpField4lblEmpField3" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:DropDownList ID="cboEmpField4EmpFieldValue3" runat="server" Height="20px" AutoPostBack="true"
                                                                    Width="222px">
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdf_cboEmpField4EmpFieldValue3" runat="server" />
                                                            </div>
                                                            <div class="ib no-padding" style="width: 32%">
                                                                <asp:Label ID="objEmpField4lblEmpField4" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField4txtEmpField4" runat="server" TextMode="MultiLine" Style="resize: none"
                                                                    Rows="2"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_objEmpField4txtEmpField4" runat="server" />
                                                            </div>
                                                        </div>
                                                        <asp:Panel ID="pnl_RightEmpField4" runat="server">
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 31%">
                                                                    <asp:Label ID="lblEmpField4StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                    <br />
                                                                    <uc2:DateCtrl ID="dtpEmpField4StartDate" runat="server" />
                                                                </div>
                                                                <div class="ibwm" style="width: 31%">
                                                                    <asp:Label ID="lblEmpField4EndDate" runat="server" Text="End Date"></asp:Label>
                                                                    <br />
                                                                    <uc2:DateCtrl ID="dtpEmpField4EndDate" runat="server" />
                                                                </div>
                                                                <div class="ibwm" style="width: 32%">
                                                                    <asp:Label ID="lblEmpField4Status" runat="server" Text="Status"></asp:Label>
                                                                    <asp:DropDownList ID="cboEmpField4Status" runat="server" Height="25px" Width="225px"
                                                                        Enabled="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 100%">
                                                                    <cc1:TabContainer ID="objEmpField4tabcRemarks" runat="server" Height="60px">
                                                                        <cc1:TabPanel ID="objEmpField4tabpRemark1" runat="server" HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblEmpField4Remark1" runat="server"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtEmpField4Remark1" runat="server" TextMode="MultiLine" Rows="3"
                                                                                    Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdf_txtEmpField4Remark1" runat="server" />
                                                                            </ContentTemplate>
                                                                        </cc1:TabPanel>
                                                                        <cc1:TabPanel ID="objEmpField4tabpRemark2" runat="server" HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblEmpField4Remark2" runat="server"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtEmpField4Remark2" runat="server" TextMode="MultiLine" Rows="3"
                                                                                    Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdf_txtEmpField4Remark2" runat="server" />
                                                                            </ContentTemplate>
                                                                        </cc1:TabPanel>
                                                                        <cc1:TabPanel ID="objEmpField4tabpRemark3" runat="server" HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblEmpField4Remark3" runat="server"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtEmpField4Remark3" runat="server" TextMode="MultiLine" Rows="3"
                                                                                    Height="75px" Style="resize: none;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdf_txtEmpField4Remark3" runat="server" />
                                                                            </ContentTemplate>
                                                                        </cc1:TabPanel>
                                                                    </cc1:TabContainer>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField4Weight" runat="server" Text="Weight"></asp:Label>
                                                                    <asp:TextBox ID="txtEmpField4Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                        Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField4Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                    <asp:TextBox ID="txtEmpField4Percent" runat="server" Style="text-align: right" Text="0.00"
                                                                        Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField4GoalType" runat="server" Text="Goal Type"></asp:Label>
                                                                    <asp:DropDownList ID="cboEmpField4GoalType" runat="server" Height="20px" Width="125px"
                                                                        AutoPostBack="true" OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField4UoMType" runat="server" Text="UoM Type"></asp:Label>
                                                                    <asp:DropDownList ID="cboEmpField4UomType" runat="server" Height="20px" Width="125px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField4GoalValue" runat="server" Text="Goal Value"></asp:Label>
                                                                    <asp:TextBox ID="txtEmpField4GoalValue" runat="server" Style="text-align: right"
                                                                        Text="0.00" Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 100%">
                                                                    <asp:TextBox ID="txtEmpField4SearchEmp" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 100%">
                                                                    <asp:Panel ID="pnl_EmpField4dgvOwner" runat="server" Style="height: 90px; overflow: auto;">
                                                                        <asp:DataGrid ID="dgvEmpField4Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="chkEmpField4allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField4AllSelect_CheckedChanged" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkEmpField4select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                            AutoPostBack="true" OnCheckedChanged="chkEmpField4select_CheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                    HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                    HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <div class="btn-default">
                                                            <asp:HiddenField ID="hdf_btnEmpField4Save" runat="server" />
                                                            <asp:Button ID="btnEmpField4Save" runat="server" Text="Save" CssClass="btndefault" />
                                                            <asp:Button ID="btnEmpField4Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="objfrmAddEditEmpField5">
                                    <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField5" runat="server" TargetControlID="hdf_btnEmpField5Save"
                                        CancelControlID="hdf_btnEmpField5Save" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                        PopupControlID="pnl_objfrmAddEditEmpField5" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_objfrmAddEditEmpField5" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 760px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblHeaderEmpField5" runat="server" Text="AddEditEmpField5"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div13" class="panel-default">
                                                    <div id="Div14" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div15" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField5Period" runat="server" Text="Period"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField5Period" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="lblEmpField5Employee" runat="server" Text="Employee"></asp:Label>
                                                                <asp:TextBox ID="txtEmpField5Employee" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 32%">
                                                                <asp:Label ID="objEmpField5lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField5txtEmpField1" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="objEmpField5lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField5txtEmpField2" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="objEmpField5lblEmpField3" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField5txtEmpField3" runat="server" ReadOnly="true"></asp:TextBox>
                                                            </div>
                                                            <div class="ib no-padding" style="width: 3%">
                                                                <asp:Label ID="objEmpField5lblEmpField4" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:DropDownList ID="cboEmpField5EmpFieldValue4" runat="server" Height="20px" Width="235px"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdf_cboEmpField5EmpFieldValue4" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib no-padding" style="width: 31%">
                                                                <asp:Label ID="objEmpField5lblEmpField5" runat="server" Text="#Caption"></asp:Label>
                                                                <asp:TextBox ID="objEmpField5txtEmpField5" runat="server" TextMode="MultiLine" Style="resize: none"></asp:TextBox>
                                                                <asp:HiddenField ID="hdf_objEmpField5txtEmpField5" runat="server" />
                                                            </div>
                                                        </div>
                                                        <asp:Panel ID="pnl_RightEmpField5" runat="server">
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 31%">
                                                                    <asp:Label ID="lblEmpField5StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                    <br />
                                                                    <uc2:DateCtrl ID="dtpEmpField5StartDate" runat="server" />
                                                                </div>
                                                                <div class="ibwm" style="width: 31%">
                                                                    <asp:Label ID="lblEmpField5EndDate" runat="server" Text="End Date"></asp:Label>
                                                                    <br />
                                                                    <uc2:DateCtrl ID="dtpEmpField5EndDate" runat="server" />
                                                                </div>
                                                                <div class="ibwm" style="width: 32%">
                                                                    <asp:Label ID="lblEmpField5Status" runat="server" Text="Status"></asp:Label>
                                                                    <asp:DropDownList ID="cboEmpField5Status" runat="server" Height="25px" Width="225px"
                                                                        Enabled="false">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 100%">
                                                                    <cc1:TabContainer ID="objEmpField5tabcRemarks" runat="server" Height="55px">
                                                                        <cc1:TabPanel ID="objEmpField5tabpRemark1" runat="server" HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblEmpField5Remark1" runat="server"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtEmpField5Remark1" runat="server" TextMode="MultiLine" Rows="2"
                                                                                    Style="resize: none;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdf_txtEmpField5Remark1" runat="server" />
                                                                            </ContentTemplate>
                                                                        </cc1:TabPanel>
                                                                        <cc1:TabPanel ID="objEmpField5tabpRemark2" runat="server" HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblEmpField5Remark2" runat="server"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtEmpField5Remark2" runat="server" TextMode="MultiLine" Rows="2"
                                                                                    Style="resize: none;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdf_txtEmpField5Remark2" runat="server" />
                                                                            </ContentTemplate>
                                                                        </cc1:TabPanel>
                                                                        <cc1:TabPanel ID="objEmpField5tabpRemark3" runat="server" HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="lblEmpField5Remark3" runat="server"></asp:Label>
                                                                            </HeaderTemplate>
                                                                            <ContentTemplate>
                                                                                <asp:TextBox ID="txtEmpField5Remark3" runat="server" TextMode="MultiLine" Rows="2"
                                                                                    Style="resize: none;"></asp:TextBox>
                                                                                <asp:HiddenField ID="hdf_txtEmpField5Remark3" runat="server" />
                                                                            </ContentTemplate>
                                                                        </cc1:TabPanel>
                                                                    </cc1:TabContainer>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField5Weight" runat="server" Text="Weight"></asp:Label>
                                                                    <asp:TextBox ID="txtEmpField5Weight" runat="server" Style="text-align: right" Text="0.00"
                                                                        Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField5Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                    <asp:TextBox ID="txtEmpField5Percent" runat="server" Style="text-align: right" Text="0.00"
                                                                        Width="125px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField5GoalType" runat="server" Text="Goal Type"></asp:Label>
                                                                    <asp:DropDownList ID="cboEmpField5GoalType" runat="server" Height="20px" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="cboGoalType_SelectedIndexChanged" Width="125px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField5UoMType" runat="server" Text="UoM Type"></asp:Label>
                                                                    <asp:DropDownList ID="cboEmpField5UomType" runat="server" Height="20px" Width="125px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="ib no-padding" style="width: 18%">
                                                                    <asp:Label ID="lblEmpField5GoalValue" runat="server" Text="Goal Value"></asp:Label>
                                                                    <asp:TextBox ID="txtEmpField5GoalValue" runat="server" Style="text-align: right"
                                                                        Text="0.00" Width="125px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 100%">
                                                                    <asp:TextBox ID="txtEmpField5SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="row2">
                                                                <div class="ibwm" style="width: 100%">
                                                                    <asp:Panel ID="pnl_EmpField5dgvOwner" runat="server" Style="height: 80px; overflow: auto;">
                                                                        <asp:DataGrid ID="dgvEmpField5Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="chkEmpField5allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField5AllSelect_CheckedChanged" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkEmpField5select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                                            AutoPostBack="true" OnCheckedChanged="chkEmpField5select_CheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                    HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                    HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <div class="btn-default">
                                                            <asp:HiddenField ID="hdf_btnEmpField5Save" runat="server" />
                                                            <asp:Button ID="btnEmpField5Save" runat="server" Text="Save" CssClass="btndefault" />
                                                            <asp:Button ID="btnEmpField5Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="YesNoPopup">
                                    <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                                        CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_YesNo" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 450px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div16" class="panel-default">
                                                    <div id="Div17" class="panel-heading-default" style="height: 50px">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblMessage" runat="server" Text="Message :" />
                                                        </div>
                                                    </div>
                                                    <div id="Div18" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:TextBox ID="txtMessage" runat="server" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                            <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <%--<div id="OprationBtn">
                                    <asp:HiddenField ID="hdf_Operation" runat="server" />
                                    <cc1:ModalPopupExtender ID="popup_Opration" runat="server" TargetControlID="hdf_Operation"
                                        CancelControlID="hdf_Operation" DropShadow="true" BackgroundCssClass="popMenu"
                                        PopupControlID="pnl_Opration" Drag="True">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_Opration" runat="server" Style="display: none; padding: 0px; width: 229px;
                                        border-style: solid; border-width: 1px; box-shadow: 5px 5px 10px #000000;" BackColor="White">
                                        <table style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                                            margin-bottom: 10px;">
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSubmitApproval" runat="server" CssClass="btnsubmenu" Text="Submit For Approval"
                                                        Width="214px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnUpdatePercentage" runat="server" CssClass="btnsubmenu" Text="Update Percentage Completed"
                                                        Width="214px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnApproveSubmitted" runat="server" CssClass="btnsubmenu" Text="Approve/Reject BSC Plan"
                                                        Width="214px" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    
                                                    <asp:Button ID="btnUnlockFinalSave" runat="server" CssClass="btnsubmenu" Text="Unlock Final Approve"
                                                        Width="214px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <div id="submitApproval">
                                    <ucCfnYesno:Confirmation ID="popup_SubmitApproval" runat="server" Message="" Title="Confirmation" />
                                </div>
                                <div id="AprroverRejectYesNO">
                                    <ucCfnYesno:Confirmation ID="popup_ApproverRejYesNo" runat="server" Message="" Title="Confirmation" />
                                </div>
                                <div id="ApprovrReject">
                                    <cc1:ModalPopupExtender ID="popup_ApprovrReject" runat="server" TargetControlID="hdf_ApprovrReject"
                                        CancelControlID="hdf_ApprovrReject" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                        PopupControlID="pnl_ApprovrReject" Drag="True" BehaviorID="popupApprovr">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_ApprovrReject" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 850px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblApproveRejectPlanningHeader" runat="server" Text="Approve/Reject BSC Planning"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div19" class="panel-default">
                                                    <div id="Div20" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div21" class="panel-body-default">
                                                        <table style="vertical-align: middle;">
                                                            <tr style="width: 100%">
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblAppRejEmployee" runat="server" Text="Employee"></asp:Label>
                                                                </td>
                                                                <td style="width: 90%">
                                                                    <asp:Label ID="objlblCaption" runat="server" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Panel ID="pnlAppRejGrid" runat="server" Style="height: 430px; width: 790px;
                                                                        overflow: auto; float: none; background-color: White">
                                                                        <asp:GridView ID="dgvAppRejdata" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblComment" runat="server" Text="Comment"></asp:Label>
                                                                </td>
                                                                <td style="width: 90%">
                                                                    <asp:TextBox ID="txtComments" TextMode="MultiLine" runat="server" Style="resize: none"
                                                                        Width="98%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:HiddenField ID="hdf_ApprovrReject" runat="server" />
                                                            <asp:Button ID="btnApRejFinalSave" runat="server" Text="Approve" CssClass="btndefault" />
                                                            <asp:Button ID="btnOpen_Changes" runat="server" Text="Reject" CssClass="btndefault" />
                                                            <asp:Button ID="btnApRejClose" runat="server" Text="Close" CssClass="btndefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="ApprovrFinalYesNO">
                                    <cc1:ModalPopupExtender ID="popup_ApprovrFinalYesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                                        CancelControlID="btnFinalNo" PopupControlID="pnl_ApprovrFinalYesNo" TargetControlID="hdf_ApprvoerFinalYesNo">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_ApprovrFinalYesNo" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 450px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblFinalTitle" runat="server" Text="Title"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div22" class="panel-default">
                                                    <div id="Div23" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblFinalMessage" runat="server" Text="Message :"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div24" class="panel-body-default">
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnFinalYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                            <asp:Button ID="btnOpenYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                            <asp:Button ID="btnFinalNo" runat="server" Text="No" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_ApprvoerFinalYesNo" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="UnlockFinalSave">
                                    <cc1:ModalPopupExtender ID="popup_UnclokFinalSave" runat="server" BackgroundCssClass="ModalPopupBG"
                                        CancelControlID="btnUnlockNo" PopupControlID="pnl_UnlocakFinalSave" TargetControlID="hdf_btnUnlockYesNO">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_UnlocakFinalSave" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 100%" DefaultButton="btnUnlockYes">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblUnlockTitel" runat="server" Text="Title"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div25" class="panel-default">
                                                    <div id="Div26" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblUnlockMessage" runat="server" Text="Message :"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div27" class="panel-body-default">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txtUnlockReason" runat="server" TextMode="MultiLine" Width="270px"
                                                                        Height="50px" Style="resize: none"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnUnlockYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                            <asp:Button ID="btnUnlockNo" runat="server" Text="No" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_btnUnlockYesNO" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div> --%>
                                <%--<div id="UpdateProgress">
                                    <cc1:ModalPopupExtender ID="popup_UpdateProgress" runat="server" BackgroundCssClass="ModalPopupBG"
                                        CancelControlID="hdf_UpdateProgress" Drag="true" PopupDragHandleControlID="pnl_UpdateProgress"
                                        PopupControlID="pnl_UpdateProgress" TargetControlID="hdf_UpdateProgress">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_UpdateProgress" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 750px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblUpdateProgressPageTitle" runat="server" Text="Update Progress"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div28" class="panel-default">
                                                    <div id="Div29" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblUpdateProgressPageHeader" runat="server" Text="Update Progress" />
                                                        </div>
                                                    </div>
                                                    <div id="Div30" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblUpdatePeriod" runat="server" Text="Period"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:TextBox ID="txtUpdatePeriod" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblUpdateGoals" runat="server" Text="Goal"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblUpdateEmployee" runat="server" Text="Employee"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:TextBox ID="txtUpdateEmployeeName" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 40%; vertical-align: top" rowspan="3">
                                                                    <asp:TextBox ID="txtUpdateGoals" runat="server" Rows="6" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblUpdateDate" runat="server" Text="Change Date"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <uc2:DateCtrl ID="dtpUpdateChangeDate" runat="server" Width="100" AutoPostBack="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblChangeBy" runat="server" Text="Change by" Visible="false"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:DropDownList ID="cboChangeBy" runat="server" Width="310px" AutoPostBack="true"
                                                                        OnSelectedIndexChanged="cboChangeBy_SelectedIndexChanged" Visible="false">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblCaption3" runat="server" Text="New Percentage"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:TextBox ID="txtNewPercentage" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right; width: 42%" Text="0.00" AutoPostBack="true"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblUpdateRemark" runat="server" Text="Remark"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblCaption4" runat="server" Text="Total Percentage"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:TextBox ID="txtTotalPercentage" runat="server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right; width: 42%" Text="0.00"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblCaption1" runat="server" Text="New Value"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:TextBox ID="txtNewValue" runat="server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right; width: 42%" Text="0.00"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 40%; vertical-align: top" rowspan="3">
                                                                    <asp:TextBox ID="txtUpdateRemark" runat="server" Rows="6" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblCaption2" runat="server" Text="Total Value"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:TextBox ID="txtTotalValue" runat="server" AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"
                                                                        Style="text-align: right; width: 42%" Text="0.00"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 15%">
                                                                    <asp:Label ID="lblUpdateStatus" runat="server" Text="Status"></asp:Label>
                                                                </td>
                                                                <td style="width: 45%">
                                                                    <asp:DropDownList ID="cboUpdateStatus" runat="server" Width="310px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>                                                            
                                                        </table>
                                                        <div class="btn-default">
                                                            <div style="float: left">
                                                                <asp:Label ID="objlblGoalTypeInfo" runat="server" Text="" Font-Bold="true" Font-Size="Small"
                                                                    ForeColor="Red" Font-Italic="true"></asp:Label>
                                                            </div>
                                                            <asp:Button ID="btnUpdateSave" runat="server" Text="Save" CssClass="btnDefault" />
                                                            <asp:Button ID="btnUpdateClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_UpdateProgress" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div31" class="panel-default">
                                                    <div id="Div32" class="panel-body-default">
                                                        <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 99%;
                                                            overflow: auto; max-height: 250px;" class="gridscroll">
                                                            <asp:Panel ID="pnl_dgvHistory" runat="server" Width="100%" Style="text-align: center">
                                                                <asp:DataGrid ID="dgvHistory" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                    ItemStyle-CssClass="griviewitem" AutoGenerateColumns="false" AllowPaging="false"
                                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                                                    ToolTip="Edit" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                                                    ToolTip="Delete" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgAttach" runat="server" CommandName="objAttach" ImageUrl="~/images/add_16.png"
                                                                                    ToolTip="Add Attachment" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="ddate" HeaderText="Date" FooterText="dgcolhDate" />
                                                                        <asp:BoundColumn DataField="pct_completed" HeaderText="% Completed" FooterText="dgcolhPercent" />
                                                                        <asp:BoundColumn DataField="dstatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                                                        <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                                                        <asp:BoundColumn DataField="empupdatetranunkid" FooterText="objdgcolhUnkid" Visible="false" />
                                                                        <asp:BoundColumn DataField="GoalAccomplishmentStatus" HeaderText="Approval Status"
                                                                            FooterText="dgcolhAccomplishedStatus" />
                                                                        <asp:BoundColumn DataField="approvalstatusunkid" Visible="false" FooterText="objcolhAccomplishedStatusId" />
                                                                        <asp:BoundColumn DataField="dfinalvalue" FooterText="dgcolhLastValue" HeaderText="Last Value">
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="UpdateYesNO">
                                    <cc1:ModalPopupExtender ID="popup_UpdateYesNO" runat="server" BackgroundCssClass="ModalPopupBG"
                                        CancelControlID="hdf_UpdateYesNo" PopupControlID="pnl_UpdateYesNO" TargetControlID="hdf_UpdateYesNo">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_UpdateYesNO" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 450px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="Label1" runat="server" Text="Aruti"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div33" class="panel-default">
                                                    <div id="Div35" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Label ID="lblUpdateMessages" runat="server" Text="Message :" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnupdateYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                            <asp:Button ID="btnupdateNo" runat="server" Text="No" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_UpdateYesNo" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="GoalAccomplishment">
                                    <cc1:ModalPopupExtender ID="popup_GoalAccomplishment" runat="server" TargetControlID="hdf_GoalAccomplishment"
                                        CancelControlID="hdf_GoalAccomplishment" BackgroundCssClass="ModalPopupBG" PopupControlID="pnl_GoalAccomplishment">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_GoalAccomplishment" runat="server" CssClass="newpopup" Style="width: 750px;
                                        display: none">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblAccomplishment_Header" runat="server" Text="Approve/Reject Goals Accomplishment List"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div class="panel-default">
                                                    <div id="Div34" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblAccomplishment_gbFilterCriteria" runat="server" Text="Filter Criteria" />
                                                        </div>
                                                    </div>
                                                    <div class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 25%">
                                                                    <asp:Label ID="lblAccomplishmentStatus" runat="server" Text="Status" />
                                                                </td>
                                                                <td style="width: 75%">
                                                                    <asp:DropDownList ID="cboAccomplishmentStatus" runat="server" Width="200px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnAccomplishmentSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                                            <asp:Button ID="btnAccomplishmentReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_GoalAccomplishment" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-default">
                                                    <div class="panel-body-default">
                                                        <asp:Panel ID="pnlAccomplishmentData" runat="server" Style="width: 99%; overflow: auto;
                                                            max-height: 250px">
                                                            <asp:GridView ID="dgv_Accomplishment_data" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                RowStyle-CssClass="griviewitem" Style="margin: 0px" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnlApproved" runat="server" ToolTip="Approve" Font-Underline="false"
                                                                                CommandName="Approved">
                                                                                <i class="fa fa-check-circle" aria-hidden="true" style="font-size:20px;color:Green"></i>                                                                                              
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" Visible="false">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkRejected" runat="server" ToolTip="Reject" Font-Underline="false"
                                                                                CommandName="Rejected">
                                                                                <i class="fa fa-times-circle" aria-hidden="true" style="font-size:20px;color:Red"></i>                                                                                              
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" Visible="false" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkselect_CheckedChanged" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="field_data" HeaderText="" />
                                                                    <asp:BoundField DataField="Goal_status" HeaderText="Goals Status" />
                                                                    <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" HeaderText="% Complate">
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtPerComp" runat="server" CssClass="decimal" AutoPostBack="true"
                                                                                ReadOnly="true" Text='<%# Eval("per_comp") %>' OnTextChanged="txtAccomplishmentPerComp_TextChanged"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Goal_Remark" HeaderText="Remark" />
                                                                    <asp:BoundField DataField="accomplished_status" HeaderText="Approve/Disapprove Status" />
                                                                    <asp:BoundField DataField="org_per_comp" Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </asp:Panel>
                                                        <div style="width: 100%">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblAccomplishmentRemark" runat="server" Text="Remark"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:TextBox ID="txtAccomplishmentRemark" runat="server" Text="" TextMode="MultiLine"
                                                                            Rows="3"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnAccomplishmentApprove" runat="server" Text="Approve" CssClass="btnDefault" />
                                                            <asp:Button ID="btnAccomplishmentReject" runat="server" Text="Reject" CssClass="btnDefault" />
                                                            <asp:Button ID="btnAccomplishmentClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="Accomplishment_YesNo">
                                    <cc1:ModalPopupExtender ID="popup_Accomplishment_YesNo" runat="server" BackgroundCssClass="ModalPopupBG"
                                        CancelControlID="hdfAccomplishment" PopupControlID="pnl_Accomplishment_YesNo"
                                        TargetControlID="hdfAccomplishment">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_Accomplishment_YesNo" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 450px">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblAccompllishmentHeader" runat="server" Text="Aruti"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div class="panel-default">
                                                    <div class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Label ID="lblAccomplishment_Message" runat="server" Text="Message :" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnAccomplishment_yes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                            <asp:Button ID="btnAccomplishment_No" runat="server" Text="No" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdfAccomplishment" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div id="ScanAttachment">
                                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                                        CancelControlID="hdf_ScanAttchment">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                                        Style="display: none;">
                                        <div class="panel-primary" style="margin: 0px">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div36" class="panel-default">
                                                    <div id="Div37" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                                        Width="200px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="width: 30%">
                                                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                        <div id="fileuploader">
                                                                            <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse" />
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                                        Text="Browse" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td colspan="3" style="width: 100%">
                                                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                                        <Columns>
                                                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btnDefault" />
                                                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation" />
                                </div>
                                <div id="UnlockEmployee">
                                    <cc1:ModalPopupExtender ID="popup_unlockemp" runat="server" TargetControlID="hdf_UnlockEmp"
                                        PopupControlID="pnl_UnlockEmp" CancelControlID="btnUClose">
                                    </cc1:ModalPopupExtender>
                                    <asp:Panel ID="pnl_UnlockEmp" runat="server" CssClass="newpopup" Width="800px" Style="display: none;">
                                        <div class="panel-primary" style="margin: 0;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblUnlockHeader" runat="server" Text="Unlock Employee(s)"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div49" class="panel-default ib" style="width: 60%; vertical-align: top">
                                                    <div id="Div50" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblUnlockFC" runat="server" Text="Filter Criteria"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div51" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib" style="width: 22%">
                                                                <asp:Label ID="lblUnlockPeriod" runat="server" Width="100%" Text="Period"></asp:Label>
                                                            </div>
                                                            <div class="ib" style="width: 50%">
                                                                <asp:DropDownList ID="cboUnlockPeriod" runat="server" Width="200px" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib" style="width: 22%">
                                                                <asp:Label ID="lblLockType" runat="server" Width="100%" Text="Select Lock Type"></asp:Label>
                                                            </div>
                                                            <div class="ib" style="width: 50%">
                                                                <asp:DropDownList ID="cboLockType" runat="server" Width="200px" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="btn-default">
                                                            <asp:LinkButton ID="objlblPeriodDates" runat="server" Text="" Style="float: left"
                                                                Font-Underline="false"></asp:LinkButton>
                                                            <asp:Button ID="btnUSearch" runat="server" Text="Search" class="btndefault" />
                                                            <asp:Button ID="btnUReset" runat="server" Text="Reset" class="btndefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div55" class="panel-default ib" style="width: 33%">
                                                    <div id="Div56" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblUSetInfo" runat="server" Text="Set Information"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div57" class="panel-body-default">
                                                        <div class="row2">
                                                            <div class="ib" style="width: 58%">
                                                                <asp:Label ID="lblSetDays" runat="server" Width="100%" Text="Set Next Lock Days"></asp:Label>
                                                            </div>
                                                            <div class="ib" style="width: 30%">
                                                                <asp:TextBox ID="txtNextDays" runat="server" Enabled="false" Style="text-align: right;
                                                                    background-color: White"></asp:TextBox>
                                                                <cc1:NumericUpDownExtender ID="nudNextDays" Width="80" runat="server" Minimum="0"
                                                                    TargetControlID="txtNextDays">
                                                                </cc1:NumericUpDownExtender>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib" style="width: 80%">
                                                                <asp:RadioButton ID="radApplyTochecked" runat="server" GroupName="setinfo" Text="Apply To Checked" />
                                                            </div>
                                                            <div class="ib" style="width: 80%">
                                                                <asp:RadioButton ID="radApplyToAll" runat="server" GroupName="setinfo" Text="Apply To All" />
                                                            </div>
                                                        </div>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnUpdateEmp" runat="server" Text="Update" class="btndefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div42" class="panel-default">
                                                    <div id="Div46" class="panel-body-default" style="position: relative">
                                                        <div class="row2">
                                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="150px">
                                                                <asp:GridView ID="dgvUnlockEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                    HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                    HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="lockunkid,employeeunkid">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="25">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" />
                                                                                
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                                
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Employee" HeaderText="Emplyoee" FooterText="colhEmployee" />
                                                                        <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock Date" FooterText="dgcolhLockDate" />
                                                                        <asp:BoundField DataField="udays" HeaderText="Unlock Day(s)" FooterText="dgcolhGraceDays" />
                                                                        <asp:BoundField DataField="nextlockdatetime" HeaderText="Next Lock Date" FooterText="dgcolhNextLockDate" />
                                                                        <asp:BoundField DataField="message" HeaderText="Message" FooterText="dgcolhMessage" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div38" class="panel-default">
                                                    <div id="Div39" class="panel-body-default" style="position: relative">
                                                        <div class="row2">
                                                            <div class="ib" style="width: 20%">
                                                                <asp:Label ID="lblUnlockEmpRemark" runat="server" Width="100%" Text="Remark"></asp:Label>
                                                            </div>
                                                            <div class="ib" style="width: 80%">
                                                                <asp:TextBox ID="txtUnlockEmpRemark" runat="server" Width="99%" TextMode="MultiLine"
                                                                    Rows="3"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="btnfixedbottom" class="btn-default">
                                                        <asp:Button ID="btnUProcess" runat="server" Text="Process" CssClass="btnDefault" />
                                                        <asp:Button ID="btnUClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_UnlockEmp" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>--%>
                                <div id="HiddenFieldSetScroll">
                                    <asp:HiddenField ID="hdf_topposition" runat="server" />
                                    <asp:HiddenField ID="hdf_leftposition" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<ucDel:DeleteReason ID="popup_UpdateProgressDeleteReason" runat="server" Title="Aruti"
                        CancelControlName="hdf_UpdateProgress" />--%>
                    <%--<ucCfnYesno:Confirmation ID="cnfUnlockDays" runat="server" Title="Aruti" />--%>
                    <%--<ucCfnYesno:Confirmation ID="cnfUnlockCheck" runat="server" Title="Aruti" />--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <%-- <script>
        $('.decimal').live("focusout",function(){
            if (parseFloat($(this).val()) > 100){
                alert('Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue.');
                $(this).val(0.00);
            }
        });
    </script>--%>
    <%--<script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmployeeLvlGoalsList.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
        
    </script>--%>
</asp:Content>
