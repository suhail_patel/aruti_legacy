﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
#End Region

Partial Class Assessment_New_Performance_Goals_wPgOwrUpdatePercentage
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmOwrUpdatePercentage"
#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                If Session("iOwnerTypeId") IsNot Nothing Then
                    Dim mintOwrFldTypeId As Integer = -1
                    Dim dtOwner As New DataTable

                    Me.ViewState.Add("OwnerTypeId", Session("iOwnerTypeId"))
                    Me.ViewState.Add("mintSelectedPeriodId", 0)
                    FillCombo()
                    Me.ViewState.Add("dtFinalTab", dtOwner)
                    Me.ViewState.Add("mintExOrder", 0)
                    Me.ViewState.Add("AdvanceFilter", "")
                    Me.ViewState.Add("strLinkedFld", "")
                    Me.ViewState.Add("OwrFldTypeId", mintOwrFldTypeId)
                Else
                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
                End If
            End If
            If CStr(Me.ViewState("strLinkedFld")).Trim.Length > 0 Then
                dgvItems.Columns(1).HeaderText = CStr(Me.ViewState("strLinkedFld"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        If IsPostBack = False Then
            Session.Remove("iOwnerTypeId")
        End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet
        Try
            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = intCurrentPeriodId
            End With
            'Shani (09-May-2016) -- [.SelectedValue = intCurrentPeriodId]

            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(Me.ViewState("OwnerTypeId"))
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "stationunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0).Copy
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "deptgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "departmentunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "sectiongroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "sectionunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "unitgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "unitunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "teamunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "jobgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "jobunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "classgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "classesunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :-" & ex.Message, Me)
        End Try
    End Sub

    Private Sub Set_Caption()
        Dim mintLinkedFld As Integer
        Dim mintExOrder As Integer
        Dim mintOwrFldTypeId As Integer
        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(cboPeriod.SelectedValue)
            mintLinkedFld = objFMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            Dim objFMaster As New clsAssess_Field_Master
            mintExOrder = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMaster = Nothing : objFMapping = Nothing
            lblOwrItemHeader.Text = strLinkedFld & " " & Language.getMessage(mstrModuleName, 1, "Information")
            dgvItems.Columns(1).HeaderText = strLinkedFld
            Me.ViewState("mintExOrder") = mintExOrder
            Me.ViewState("strLinkedFld") = strLinkedFld
            Me.ViewState("OwrFldTypeId") = mintOwrFldTypeId
        Catch ex As Exception
            DisplayMessage.DisplayError("Set_Caption :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Items_Grid()
        Dim dtFinalTab As New DataTable
        Dim dtFinalTabView As DataView
        Try

            Dim objOwner As New clsassess_owrfield1_master
            dtFinalTab = objOwner.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinalTab.Columns.Add("fcheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinalTab.Columns.Add("fchange", System.Type.GetType("System.Boolean")).DefaultValue = False
            dgvItems.AutoGenerateColumns = False

            Dim xCol As New BoundColumn()
            xCol.HeaderText = dgvItems.Columns(1).HeaderText
            xCol.HeaderStyle.Width = Unit.Pixel(380)
            xCol.ItemStyle.Width = Unit.Pixel(380)
            xCol.FooterStyle.Width = Unit.Pixel(380)
            Select Case CInt(Me.ViewState("mintExOrder"))
                Case enWeight_Types.WEIGHT_FIELD1
                    xCol.DataField = "Field1"
                Case enWeight_Types.WEIGHT_FIELD2
                    xCol.DataField = "Field2"
                Case enWeight_Types.WEIGHT_FIELD3
                    xCol.DataField = "Field3"
                Case enWeight_Types.WEIGHT_FIELD4
                    xCol.DataField = "Field4"
                Case enWeight_Types.WEIGHT_FIELD5
                    xCol.DataField = "Field5"
            End Select
            dgvItems.Columns.RemoveAt(1)
            dgvItems.Columns.AddAt(1, xCol)
            Dim xRow = From iRow In dtFinalTab.AsEnumerable() Where iRow.IsNull("fcheck") = True Select iRow
            For Each mRow As DataRow In xRow
                mRow.Item("fcheck") = False
            Next
            dtFinalTabView = dtFinalTab.DefaultView
            Dim strSearch As String = ""
            dgvItems.DataSource = dtFinalTabView
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If Me.ViewState("strLinkedFld") IsNot Nothing Then
                dgvItems.Columns(1).HeaderText = CStr(Me.ViewState("strLinkedFld"))
            End If
            'SHANI [01 FEB 2015]--END
            dgvItems.DataBind()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Items_Grid :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidData(ByVal dtFinalTab As DataTable) As Boolean
        Try
            Dim mintPercent As Decimal = 0
            Decimal.TryParse(txtPercent.Text, mintPercent)
            If mintPercent > 100 Or mintPercent <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Percentage Completed should be between 1 and 100."), Me)
                txtPercent.Focus()
                Return False
            End If

            If radApplyTochecked.Checked = False AndAlso radApplyToAll.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Please select atleast one operation type to set percentage."), Me)
                Return False
            End If

            If radApplyTochecked.Checked = True Then
                dtFinalTab.AcceptChanges()
                Dim xRow() As DataRow = Nothing
                xRow = dtFinalTab.Select("fcheck = true")
                If xRow.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, please check atleast one of the") & " " & Me.ViewState("strLinkedFld") & " " & _
                                    Language.getMessage(mstrModuleName, 4, "goal(s) to assign Percentage."), Me)
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidOwner :-" & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub BindData(ByVal dtFinalTab As DataTable)
        Dim dtFinalTabView As DataView
        Try
            Dim xCol As New BoundColumn()
            xCol.HeaderText = dgvItems.Columns(1).HeaderText
            Select Case CInt(Me.ViewState("mintExOrder"))
                Case enWeight_Types.WEIGHT_FIELD1
                    xCol.DataField = "Field1"
                Case enWeight_Types.WEIGHT_FIELD2
                    xCol.DataField = "Field2"
                Case enWeight_Types.WEIGHT_FIELD3
                    xCol.DataField = "Field3"
                Case enWeight_Types.WEIGHT_FIELD4
                    xCol.DataField = "Field4"
                Case enWeight_Types.WEIGHT_FIELD5
                    xCol.DataField = "Field5"
            End Select
            dgvItems.Columns.RemoveAt(1)
            dgvItems.Columns.AddAt(1, xCol)
            dtFinalTabView = dtFinalTab.DefaultView
            Dim strSearch As String = ""
            dgvItems.DataSource = dtFinalTabView
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If Me.ViewState("strLinkedFld") IsNot Nothing Then
                dgvItems.Columns(1).HeaderText = CStr(Me.ViewState("strLinkedFld"))
            End If
            'SHANI [01 FEB 2015]--END
            dgvItems.DataBind()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError("BindData :-" & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            If IsValidData(dtFinalTab) = False Then Exit Sub
            Dim rows As IEnumerable(Of DataRow) = Nothing
            If radApplyToAll.Checked = True Then
                rows = dtFinalTab.Rows.Cast(Of DataRow)()
            ElseIf radApplyTochecked.Checked = True Then
                rows = dtFinalTab.Rows.Cast(Of DataRow)().Where(Function(r) r("fcheck").ToString() = "True")
            End If
            For Each row As DataRow In rows
                row("pct_complete") = txtPercent.Text
                row("fchange") = True
            Next
            BindData(dtFinalTab)
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkUpdate_LinkClicked : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSeach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSeach.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Please select Period and Owner to do global assign operation."), Me)
                Exit Sub
            End If
            Call Fill_Items_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSeach_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            If dtFinalTab IsNot Nothing Then
                Dim blnflag As Boolean = False
                dtFinalTab.AcceptChanges()
                Dim rows As IEnumerable(Of DataRow) = dtFinalTab.Rows.Cast(Of DataRow)().Where(Function(r) r("fchange").ToString() = "True")
                Dim mGoalIdx As Integer = 1
                For Each row As DataRow In rows
                    Select Case CInt(Me.ViewState("mintExOrder"))
                        Case enWeight_Types.WEIGHT_FIELD1
                            Dim objOField1 As New clsassess_owrfield1_master
                            objOField1._Owrfield1unkid = CInt(row("owrfield1unkid"))
                            objOField1._Pct_Completed = CDec(row("pct_complete"))
                            With objOField1
                                ._FormName = mstrModuleName
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            blnflag = objOField1.Update()
                            If blnflag = False AndAlso objOField1._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField1._Message, Me)
                                Exit For
                            End If
                            objOField1 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD2
                            Dim objOField2 As New clsassess_owrfield2_master
                            objOField2._Owrfield2unkid = CInt(row("owrfield2unkid"))
                            objOField2._Pct_Completed = CDec(row("pct_complete"))
                            With objOField2
                                ._FormName = mstrModuleName
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            blnflag = objOField2.Update()
                            If blnflag = False AndAlso objOField2._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField2._Message, Me)
                                Exit For
                            End If
                            objOField2 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD3
                            Dim objOField3 As New clsassess_owrfield3_master
                            objOField3._Owrfield3unkid = CInt(row("owrfield3unkid"))
                            objOField3._Pct_Completed = CDec(row("pct_complete"))
                            With objOField3
                                ._FormName = mstrModuleName
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            blnflag = objOField3.Update()
                            If blnflag = False AndAlso objOField3._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField3._Message, Me)
                                Exit For
                            End If
                            objOField3 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD4
                            Dim objOField4 As New clsassess_owrfield4_master
                            objOField4._Owrfield4unkid = CInt(row("owrfield4unkid"))
                            objOField4._Pct_Completed = CDec(row("pct_complete"))
                            With objOField4
                                ._FormName = mstrModuleName
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            blnflag = objOField4.Update()
                            If blnflag = False AndAlso objOField4._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField4._Message, Me)
                                Exit For
                            End If
                            objOField4 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD5
                            Dim objOField5 As New clsassess_owrfield5_master
                            objOField5._Owrfield5unkid = CInt(row("owrfield5unkid"))
                            objOField5._Pct_Completed = CDec(row("pct_complete"))
                            With objOField5
                                ._FormName = mstrModuleName
                                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            blnflag = objOField5.Update()
                            If blnflag = False AndAlso objOField5._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField5._Message, Me)
                                Exit For
                            End If
                            objOField5 = Nothing

                    End Select
                    mGoalIdx += 1
                Next
                Call Fill_Items_Grid()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSave_Click :- " & ex.Message, Me)
        Finally
        End Try
    End Sub
#End Region

#Region "Control Event"
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboOwner.SelectedIndexChanged
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                dgvItems.DataSource = Nothing : dgvItems.DataBind()
                If dtFinalTab IsNot Nothing Then dtFinalTab.Rows.Clear()
            ElseIf CInt(cboPeriod.SelectedValue) > 0 Then
                Call Set_Caption()
            End If
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkItemAllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvItems.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkItemSelect"), CheckBox)
                chk.Checked = chkAll.Checked
                dtFinalTab.Rows(iRow.ItemIndex)("fcheck") = chkAll.Checked
            Next
            dtFinalTab.AcceptChanges()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError("chkItemAllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkItemSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            Dim chkItem As CheckBox = CType(sender, CheckBox)
            Dim xRow As DataGridItem = CType(chkItem.NamingContainer, DataGridItem)
            If xRow.ItemIndex >= 0 Then
                dtFinalTab.Rows(xRow.ItemIndex)("fcheck") = chkItem.Checked
            End If
            dtFinalTab.AcceptChanges()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError("chkItemAllSelect_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Protected Sub dgvItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvItems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Call SetDateFormat()

                If e.Item.Cells(2).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(2).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToShortDateString
                End If

                If e.Item.Cells(3).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(3).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).Date.ToShortDateString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvItems_ItemDataBound:- " & ex.Message, Me)
        End Try
    End Sub
    'Pinkal (16-Apr-2016) -- End


#End Region

End Class
