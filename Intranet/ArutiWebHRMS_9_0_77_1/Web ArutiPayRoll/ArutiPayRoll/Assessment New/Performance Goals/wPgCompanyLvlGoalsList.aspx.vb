﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
#End Region

Partial Class wPgCompanyLvlGoalsList
    Inherits Basepage

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmCoyFieldsList"
    Private objCoyField1 As New clsassess_coyfield1_master
    Private objCoyField2 As New clsassess_coyfield2_master
    Private objCoyField3 As New clsassess_coyfield3_master
    Private objCoyField4 As New clsassess_coyfield4_master
    Private objCoyField5 As New clsassess_coyfield5_master
    Dim DisplayMessage As New CommonCodes
    Dim dic_ColIndex As Dictionary(Of String, Integer)
#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call SetFormViewSetting()
                Call FillCombo()

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Else
                dic_ColIndex = CType(Me.ViewState("dic_ColIndex"), Dictionary(Of String, Integer))
                'Pinkal (16-Apr-2016) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load1 :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dic_ColIndex") = dic_ColIndex
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        End Try
    End Sub
    'Pinkal (16-Apr-2016) -- End

#End Region

#Region "Private Methods"

    Private Sub SetFormViewSetting()
        Dim objFMaster = New clsAssess_Field_Master(True)
        Try

            objlblField1.Text = objFMaster._Field1_Caption
            If objlblField1.Text = "" Then
                cboFieldValue1.Enabled = False
            End If

            objlblField2.Text = objFMaster._Field2_Caption
            If objlblField2.Text = "" Then
                cboFieldValue2.Enabled = False
            End If

            objlblField3.Text = objFMaster._Field3_Caption
            If objlblField3.Text = "" Then
                cboFieldValue3.Enabled = False
            End If

            objlblField4.Text = objFMaster._Field4_Caption
            If objlblField4.Text = "" Then
                cboFieldValue4.Enabled = False
            End If

            objlblField5.Text = objFMaster._Field5_Caption
            If objlblField5.Text = "" Then
                cboFieldValue5.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("SetFormViewSetting :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            Dim objPerspective As New clsassess_perspective_master
            dsList = objPerspective.getComboList("List", True)
            With cboPerspective
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCoyField2.getComboList("List", True)
            With cboFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCoyField3.getComboList("List", True)
            With cboFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCoyField4.getComboList("List", True)
            With cboFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCoyField5.getComboList("List", True)
            With cboFieldValue5
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = Nothing
            dtTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            Dim iRow As DataRow = dtTable.NewRow
            iRow.Item("Id") = 0
            iRow.Item("Name") = Language.getMessage(mstrModuleName, 26, "Select")
            dtTable.Rows.InsertAt(iRow, 0)
            With cboAllocations
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = intCurrentPeriodId
            End With
            'Shani (09-May-2016) -- Add[.SelectedValue = intCurrentPeriodId]

            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            Call cboAllocations_SelectedIndexChanged(cboAllocations, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Dim mdtFinal As DataTable
        Dim objFMaster As New clsAssess_Field_Master
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No data defined for asseessment caption settings screen."), Me)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry no field is mapped with the selected period."), Me)
                Exit Sub
            End If
            objMap = Nothing

            dtFinal = objCoyField1.GetDisplayList(CInt(cboPeriod.SelectedValue), "List")

            If dtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = dtFinal.NewRow
                dtFinal.Rows.Add(iRow)
            End If

            If CInt(cboPerspective.SelectedValue) > 0 Then
                iSearch &= "AND perspectiveunkid = '" & CInt(cboPerspective.SelectedValue) & "' "
            End If

            If cboFieldValue1.Enabled = True AndAlso CInt(cboFieldValue1.SelectedValue) > 0 Then
                iSearch &= "AND coyfield1unkid = '" & CInt(cboFieldValue1.SelectedValue) & "' "
            End If

            If cboFieldValue2.Enabled = True AndAlso CInt(cboFieldValue2.SelectedValue) > 0 Then
                iSearch &= "AND coyfield2unkid = '" & CInt(cboFieldValue2.SelectedValue) & "' "
            End If

            If cboFieldValue3.Enabled = True AndAlso CInt(cboFieldValue3.SelectedValue) > 0 Then
                iSearch &= "AND coyfield3unkid = '" & CInt(cboFieldValue3.SelectedValue) & "' "
            End If

            If cboFieldValue4.Enabled = True AndAlso CInt(cboFieldValue4.SelectedValue) > 0 Then
                iSearch &= "AND coyfield4unkid = '" & CInt(cboFieldValue4.SelectedValue) & "' "
            End If

            If cboFieldValue5.Enabled = True AndAlso CInt(cboFieldValue5.SelectedValue) > 0 Then
                iSearch &= "AND coyfield5unkid = '" & CInt(cboFieldValue5.SelectedValue) & "' "
            End If

            If dtpStartDate.IsNull = False AndAlso dtpEndDate.IsNull = False Then
                iSearch &= "AND sdate >= '" & eZeeDate.convertDate(dtpStartDate.GetDate) & "' AND edate <= '" & eZeeDate.convertDate(dtpEndDate.GetDate) & "' "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                iSearch &= "AND CStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
            End If

            If CInt(cboAllocations.SelectedValue) > 0 Then
                iSearch &= "AND COwnerId = '" & CInt(cboAllocations.SelectedValue) & "' "
            End If
            If cboOwner.Enabled Then
                If CInt(cboOwner.SelectedValue) > 0 Then
                    Dim objOwr As New clsassess_coyowner_tran
                    Dim iString As String = ""
                    iString = objOwr.GetCSV_CoyFieldTableIds(CInt(cboOwner.SelectedValue), IIf(IsDBNull(dtFinal.Rows(0).Item("CFieldTypeId")) = True, 0, dtFinal.Rows(0).Item("CFieldTypeId")), True)
                    objOwr = Nothing
                    iSearch &= iString
                End If
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtFinal = New DataView(dtFinal, iSearch, "Field1Id DESC, perspectiveunkid ASC", DataViewRowState.CurrentRows).ToTable
            Else
                mdtFinal = New DataView(dtFinal, "", "Field1Id DESC, perspectiveunkid ASC", DataViewRowState.CurrentRows).ToTable
            End If

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Dim intTotalWidth As Integer = 0
            If iPlan IsNot Nothing Then
                For index As Integer = 0 To iPlan.Length - 1
                    intTotalWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, iPlan(index), Session("CompnayUnkId"))
                Next
            End If
            'SHANI [09 Mar 2015]--END 

            dgvData.AutoGenerateColumns = False
            If dgvData.Columns.Count > 0 Then dgvData.Columns.Clear()
            Dim iColName As String = String.Empty

            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            dic_ColIndex = New Dictionary(Of String, Integer)
            'Pinkal (16-Apr-2016) -- End

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                dgvCol.HtmlEncode = False
                'SHANI [01 FEB 2015]--END

                If dgvData.Columns.Contains(dgvCol) = True Then Continue For
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'dgvCol.HeaderStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), CInt(Session("Companyunkid"))))
                        'dgvCol.ItemStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), CInt(Session("Companyunkid"))))
                        'dgvCol.FooterStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), CInt(Session("Companyunkid"))))

                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        'Dim decColumnWidth As Decimal = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), CInt(Session("Companyunkid"))) * 100 / intTotalWidth
                        'dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        'dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'dgvCol.FooterStyle.Width = Unit.Percentage(decColumnWidth)

                        Dim decColumnWidth As Decimal = 0
                        If intTotalWidth > 0 Then
                            decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), CInt(Session("Companyunkid"))) * 100 / intTotalWidth
                        dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        dgvCol.FooterStyle.Width = Unit.Percentage(decColumnWidth)
                        Else
                            decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), CInt(Session("Companyunkid")))
                            dgvCol.HeaderStyle.Width = Unit.Pixel(decColumnWidth)
                            dgvCol.ItemStyle.Width = Unit.Pixel(decColumnWidth)
                            dgvCol.FooterStyle.Width = Unit.Pixel(decColumnWidth)
                        End If
                        'SHANI [21 Mar 2015]--END

                        'SHANI [09 Mar 2015]--END 
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    ElseIf dCol.ColumnName = "Owner" Then
                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'dgvCol.HeaderStyle.Width = Unit.Pixel(350)
                        'dgvCol.ItemStyle.Width = Unit.Pixel(350)
                        'dgvCol.FooterStyle.Width = Unit.Pixel(350)

                        'SHANI (16 APR 2015)-START
                        Dim decColumnWidth As Decimal
                        Try
                            decColumnWidth = 350 * 100 / intTotalWidth
                        Catch ex As Exception
                            decColumnWidth = 30
                        End Try

                        dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        dgvCol.FooterStyle.Width = Unit.Percentage(decColumnWidth)
                        'SHANI (16 APR 2015)--END 
                        
                        'SHANI [09 Mar 2015]--END
                    End If
                End If
                dgvData.Columns.Add(dgvCol)

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                dic_ColIndex.Add(dgvCol.FooterText, dgvData.Columns.IndexOf(dgvCol))
                'Pinkal (16-Apr-2016) -- End
            Next
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'pnl_dgvData.Height = 450
            'dgvData.Width = Unit.Pixel(idgvWidth)
            'Dim idgvWidth As Integer = 0
            'For i As Integer = 0 To dgvData.Columns.Count - 1
            '    idgvWidth = idgvWidth + CInt(dgvData.Columns(i).ItemStyle.Width.Value)
            'Next
            dgvData.Width = Unit.Percentage(99.5)
            'SHANI [09 Mar 2015]--END
            dgvData.DataSource = mdtFinal
            dgvData.DataBind()

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_COMPANY_LEVEL, CInt(cboPeriod.SelectedValue), 0, 0)
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Language.getMessage(mstrModuleName, 30, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Grid :- " & ex.Message, Me)
        Finally
        End Try
    End Sub
#End Region

#Region "Button Event"
    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Period is mandatory information. Please select Period to continue."), Me)
                Exit Sub
            End If
            Call Fill_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnSearch_Click :-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            cboPerspective.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboFieldValue1.SelectedValue = 0
            cboFieldValue2.SelectedValue = 0
            cboFieldValue3.SelectedValue = 0
            cboFieldValue4.SelectedValue = 0
            cboFieldValue5.SelectedValue = 0
            dtpStartDate.SetDate = Nothing : dtpEndDate.SetDate = Nothing
            dgvData.DataSource = Nothing
            cboPeriod.SelectedValue = 0
            cboAllocations.SelectedValue = 0
            dgvData.DataSource = Nothing
            dgvData.DataBind()
            pnl_dgvData.Height = 0
            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            Call cboAllocations_SelectedIndexChanged(cboAllocations, Nothing)
            objlblTotalWeight.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReset_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " ComboBox "

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField1.getComboList(CInt(cboPeriod.SelectedValue), "List", True)
            With cboFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField2.getComboList("List", True, CInt(cboFieldValue1.SelectedValue))
            With cboFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue1_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboFieldValue2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue2.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField3.getComboList("List", True, CInt(cboFieldValue2.SelectedValue))
            With cboFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue2_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField4.getComboList("List", True, CInt(cboFieldValue3.SelectedValue))
            With cboFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue3_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboFieldValue4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue4.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objCoyField5.getComboList("List", True, CInt(cboFieldValue4.SelectedValue))
            With cboFieldValue5
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboFieldValue4_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            If CInt(cboAllocations.SelectedValue) > 0 Then
                objlblCaption.Text = "" : cboOwner.Enabled = True
                Dim dList As New DataSet
                objlblCaption.Text = cboAllocations.SelectedItem.Text
                cboOwner.DataSource = Nothing
                Select Case CInt(cboAllocations.SelectedValue)
                    Case enAllocation.BRANCH
                        Dim objBranch As New clsStation
                        dList = objBranch.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "stationunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0).Copy
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.DEPARTMENT_GROUP
                        Dim objDeptGrp As New clsDepartmentGroup
                        dList = objDeptGrp.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "deptgroupunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.DEPARTMENT
                        Dim objDept As New clsDepartment
                        dList = objDept.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "departmentunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.SECTION_GROUP
                        Dim objSecGrp As New clsSectionGroup
                        dList = objSecGrp.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "sectiongroupunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.SECTION
                        Dim objSec As New clsSections
                        dList = objSec.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "sectionunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.UNIT_GROUP
                        Dim objUnitGrp As New clsUnitGroup
                        dList = objUnitGrp.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "unitgroupunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.UNIT
                        Dim objUnit As New clsUnits
                        dList = objUnit.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "unitunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.TEAM
                        Dim objTeam As New clsTeams
                        dList = objTeam.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "teamunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.JOB_GROUP
                        Dim objJobGrp As New clsJobGroup
                        dList = objJobGrp.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "jobgroupunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.JOBS
                        Dim objJob As New clsJobs
                        dList = objJob.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "jobunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.CLASS_GROUP
                        Dim objClsGrp As New clsClassGroup
                        dList = objClsGrp.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "classgroupunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                    Case enAllocation.CLASSES
                        Dim objCls As New clsClass
                        dList = objCls.getComboList("List", True)
                        With cboOwner
                            .DataValueField = "classesunkid"
                            .DataTextField = "name"
                            .DataSource = dList.Tables(0)
                            .DataBind()
                            .SelectedValue = 0
                        End With
                End Select
            Else
                objlblCaption.Text = "" : cboOwner.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("cboAllocations_SelectedIndexChanged :-" & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region
   
#Region " Control's Event(S) "

    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            Call SetDateFormat()
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(dic_ColIndex("objSt_Date")).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(dic_ColIndex("objSt_Date")).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(dic_ColIndex("objSt_Date")).Text = CDate(e.Row.Cells(dic_ColIndex("objSt_Date")).Text).Date.ToShortDateString
                End If

                If e.Row.Cells(dic_ColIndex("objEd_Date")).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(dic_ColIndex("objEd_Date")).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(dic_ColIndex("objEd_Date")).Text = CDate(e.Row.Cells(dic_ColIndex("objEd_Date")).Text).Date.ToShortDateString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvData_RowDataBound:- " & ex.Message, Me)
        End Try
    End Sub
    'Pinkal (16-Apr-2016) -- End
#End Region

    
End Class
