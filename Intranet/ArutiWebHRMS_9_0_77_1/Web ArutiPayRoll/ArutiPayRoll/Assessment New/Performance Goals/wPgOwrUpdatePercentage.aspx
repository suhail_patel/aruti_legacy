﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgOwrUpdatePercentage.aspx.vb" Inherits="Assessment_New_Performance_Goals_wPgOwrUpdatePercentage"
    Title="Goal Owner And Period Detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
       if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        return true;
    }    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);
        $(window).scroll(function () {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

    function beginRequestHandler(sender, args) {
        $("#endreq").val("0");
        $("#bodyy").val($(window).scrollTop());
    }
    
    function endRequestHandler(sender, args) {
        $("#endreq").val("1");
        SetGeidScrolls();
         if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
        }
    }
    </script>

    <script type="text/javascript">
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "none"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Owners Goals List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <table style="width: 100%;">
                                <tr style="width: 100%">
                                    <td style="width: 50%">
                                        <div id="Div4" class="panel-default">
                                            <div id="Div5" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Goal Owner And Period Detail"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div6" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblOwner" runat="server" Text="Owner"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:DropDownList ID="cboOwner" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 35%">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnSeach" runat="server" Text="Search" Width="125px" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="width: 50%">
                                        <div id="Div7" class="panel-default">
                                            <div id="Div8" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblOperationheader" runat="server" Text="Set Percentage Information"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div9" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblPercentage" runat="server" Text="% Completed"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtPercent" runat="server" Width="100%" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 60%; padding-left: 20px;">
                                                            <asp:RadioButton ID="radApplyTochecked" runat="server" Text="Apply To Checked" GroupName="Apply" />
                                                            <asp:RadioButton ID="radApplyToAll" runat="server" Text="Apply To All" GroupName="Apply" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnUpdate" runat="server" Text="Update Percent" Width="125px" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td colspan="2" style="width: 100%">
                                        <div id="Div1" class="panel-default">
                                            <div id="Div2" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblOwrItemHeader" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div3" class="panel-body-default">
                                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="height: 405px;
                                                    overflow: auto; border-bottom: solid 2px #DDD;" class="gridscroll">
                                                    <asp:Panel ID="pnl_dgvOwr" runat="server">
                                                    <asp:DataGrid ID="dgvItems" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        Width="99%" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="40px" ItemStyle-Width="40px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkItemAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkItemAllSelect_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkItemSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("fcheck") %>'
                                                                        OnCheckedChanged="chkItemSelect_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="" HeaderText="" FooterText="objdgcolhfname"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="dgcolhfstdate"
                                                                HeaderStyle-Width="90px" ItemStyle-Width="90px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="dgcolhfeddate"
                                                                HeaderStyle-Width="90px" ItemStyle-Width="90px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="dgcolhfstatus"
                                                                HeaderStyle-Width="90px" ItemStyle-Width="90px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhfweight"
                                                                HeaderStyle-Width="90px" ItemStyle-Width="90px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="dgcolhfpct_complete"
                                                                HeaderStyle-Width="90px" ItemStyle-Width="90px"></asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                                </div>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
