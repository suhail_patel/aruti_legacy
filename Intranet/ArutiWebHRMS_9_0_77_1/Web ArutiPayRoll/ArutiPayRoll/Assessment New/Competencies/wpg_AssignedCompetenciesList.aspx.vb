﻿#Region " Import "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing

#End Region

Partial Class wpg_AssignedCompetenciesList
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssignedCompetenciesList"
    Private objAssignedMaster As New clsassess_competence_assign_master
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Page Event "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call FillCombo()
                cboEmployee.Enabled = CBool(Session("Self_Assign_Competencies"))
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Competencies_Filter") IsNot Nothing Then
                    If CBool(Session("Self_Assign_Competencies")) = True Then
                        cboEmployee.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(0).Trim
                        cboPeriod.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(1).Trim
                        Session.Remove("Competencies_Filter")
                        Call btnSearch_Click(btnSearch, Nothing)
                    Else
                        cboAssessGroup.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(0).Trim
                        cboPeriod.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(1).Trim
                        Session.Remove("Competencies_Filter")
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END

                'Shani (31-Aug-2016) -- Start
                'Enhancement - Change Competencies List Design Given by Andrew
                ''S.SANDEEP [04 JUN 2015] -- START
                'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                '    Call btnSearch_Click(btnSearch, Nothing)
                '    'S.SANDEEP [31 DEC 2015] -- START
                'ElseIf Session("SelectedEmpId") IsNot Nothing Then
                '    cboEmployee.SelectedValue = Session("SelectedEmpId")
                '    Call btnSearch_Click(btnSearch, Nothing)
                '    Session("SelectedEmpId") = Nothing
                '    'S.SANDEEP [31 DEC 2015] -- END
                'End If
                ''S.SANDEEP [04 JUN 2015] -- END
                'Shani (31-Aug-2016) -- End


                'SHANI [27 Mar 2015]-START
                'ENHANCEMENT : 
                If CBool(Session("Self_Assign_Competencies")) Then

                    'Shani(06-Feb-2016) -- Start
                    'PA Changes Given By CCBRT
                    'btnNew.Visible = True
                    'lvData.Columns(0).Visible = True
                    'lvData.Columns(1).Visible = True

                    'S.SANDEEP [23 FEB 2016] -- START

                    'S.SANDEEP [23 FEB 2016] -- END
                    'btnNew.Visible = CBool(Session("AllowtoAddAssignedCompetencies"))
                    'lvData.Columns(0).Visible = CBool(Session("AllowtoEditAssignedCompetencies"))
                    'lvData.Columns(1).Visible = CBool(Session("AllowtoDeleteAssignedCompetencies"))
                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    btnNew.Visible = CBool(Session("AllowtoAddAssignedCompetencies"))
                    lvData.Columns(0).Visible = CBool(Session("AllowtoEditAssignedCompetencies"))
                    lvData.Columns(1).Visible = CBool(Session("AllowtoDeleteAssignedCompetencies"))
                    Else
                        btnNew.Visible = True
                        lvData.Columns(0).Visible = True
                        lvData.Columns(1).Visible = True
                    End If
                    
                    'Shani(06-Feb-2016) -- End
                Else
                    btnNew.Visible = False
                    lvData.Columns(0).Visible = False
                    lvData.Columns(1).Visible = False
                End If
                'SHANI [27 Mar 2015]--END 


                'Shani (31-Aug-2016) -- Start
                'Enhancement - Change Competencies List Design Given by Andrew
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    Call btnSearch_Click(btnSearch, Nothing)
                ElseIf Session("SelectedEmpId") IsNot Nothing Then
                    cboEmployee.SelectedValue = Session("SelectedEmpId")
                    Call btnSearch_Click(btnSearch, Nothing)
                    Session("SelectedEmpId") = Nothing
                End If
                'Shani (31-Aug-2016) -- End


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load1 : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAGroup As New clsassess_group_master
        Dim objPeriod As New clscommom_period_Tran
        Dim objCMaster As New clsCommon_Master
        Dim objEmployee As New clsEmployee_Master
        Try
            dsCombo = objAGroup.getListForCombo("List", True)
            With cboAssessGroup
                .DataValueField = "assessgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            'S.SANDEEP [14 APR 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), "List", True, 1)

            'S.SANDEEP [17 NOV 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END

            'S.SANDEEP [14 APR 2015] -- END

            'S.SANDEEP [04 JUN 2015] -- START

            'Shani (09-May-2016) -- Start
            'Dim mintCurrentPeriodId As Integer = 0
            'If dsCombo.Tables(0).Rows.Count > 1 Then
            '    mintCurrentPeriodId = dsCombo.Tables(0).Rows(dsCombo.Tables(0).Rows.Count - 1).Item("periodunkid")
            'End If
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                'S.SANDEEP [04 JUN 2015] -- START
                '.SelectedValue = 0
                .SelectedValue = mintCurrentPeriodId
                'S.SANDEEP [04 JUN 2015] -- END
                .DataBind()
            End With

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCompetenceCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Session("IsIncludeInactiveEmp") = False Then
                '    dsCombo = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
                'Else
                '    dsCombo = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If


                'S.SANDEEP [04 Jan 2016] -- START
                'dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
                '                            Session("UserId"), _
                '                            Session("Fin_year"), _
                '                            Session("CompanyUnkId"), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                            Session("UserAccessModeSetting"), True, _
                '                            Session("IsIncludeInactiveEmp"), "List", True)

                Dim strFilterQry As String = String.Empty
                Dim blnApplyFilter As Boolean = False 'Shani(14-APR-2016) --[True]
                'Shani(14-APR-2016) -- Start
                'If Session("SkipApprovalFlowInPlanning") = True Then
                'Shani(14-APR-2016) -- End
                    Dim csvIds As String = String.Empty
                    Dim dsMapEmp As New DataSet
                    Dim objEval As New clsevaluation_analysis_master

                    dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                            Session("UserId"), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                    If dsMapEmp.Tables("List").Rows.Count > 0 Then

                        dsCombo = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                                  Session("UserId"), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                  Session("IsIncludeInactiveEmp"), _
                                                                  CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                        strFilterQry = " hremployee_master.employeeunkid IN "
                        csvIds = String.Join(",", dsCombo.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                        If csvIds.Trim.Length > 0 Then
                            strFilterQry &= "(" & csvIds & ")"
                        Else
                        strFilterQry &= "(0)"
                    End If
                    'Shani(14-APR-2016) -- Start
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                    'Shani(14-APR-2016) -- End 
                End If
                'Shani(14-APR-2016) -- Start
                'End If
                'Shani(14-APR-2016) -- End 

                dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                                      Session("IsIncludeInactiveEmp"), "List", True, , , , , , , , , , , , , , , strFilterQry, , blnApplyFilter)
                'S.SANDEEP [04 Jan 2016] -- END

                
                'Shani(24-Aug-2015) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombo.Tables("List")
                    .SelectedValue = 0
                    .DataBind()
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsList As New DataSet
        Dim iSearch As String = String.Empty
        Dim dTable As DataTable = Nothing
        Try
            'S.SANDEEP [09 FEB 2015] -- START
            'If CInt(cboAssessGroup.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Group and Period are mandatory information. Please selecte Group and Period."), Me)
            '    Exit Sub
            'End If
            If CBool(Session("Self_Assign_Competencies")) = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Employee and Period are mandatory information. Please selecte Employee and Period."), Me)
                    Exit Sub
                End If
            Else

                'TODO <need to open it after analysing user login >
                'If CInt(cboAssessGroup.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Group and Period are mandatory information. Please selecte Group and Period."), Me)
                '    Exit Sub
                'End If
            End If
            'S.SANDEEP [09 FEB 2015] -- END


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objAssignedMaster.GetList("List", CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), CBool(Session("Self_Assign_Competencies")))
            dsList = objAssignedMaster.GetList("List", eZeeDate.convertDate(Session("EmployeeAsOnDate")), CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), CBool(Session("Self_Assign_Competencies")))
            'Shani(20-Nov-2015) -- End



            'S.SANDEEP [23 APR 2015] -- START
            If dsList Is Nothing Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, there is no list of competencies assigned to this employee or some defined assessment group does not fall in the allocation of the selected employee."), Me)
                Exit Sub
            End If
            'S.SANDEEP [23 APR 2015] -- END


            If CInt(cboCompetenceCategory.SelectedValue) > 0 Then
                iSearch &= "AND masterunkid = '" & CInt(cboCompetenceCategory.SelectedValue) & "' "
            End If

            If CInt(IIf(cboCompetencies.SelectedValue = "", 0, cboCompetencies.SelectedValue)) > 0 Then
                iSearch &= "AND competenciesunkid = '" & CInt(cboCompetencies.SelectedValue) & "' "
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                dTable = New DataView(dsList.Tables(0), iSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Shani (31-Aug-2016) -- Start
            'Enhancement - Change Competencies List Design Given by Andrew
            'lvData.DataSource = dTable
            Dim intcmpCategory As Integer = 0
            Dim dtComp As DataTable = dTable.Clone
            dtComp.Columns.Add("iscategory", Type.GetType("System.Boolean"))

            Dim dtRow As DataRow = Nothing
            For Each dRow As DataRow In dTable.Rows
                If intcmpCategory <> CInt(dRow.Item("masterunkid").ToString) Then
                    dtRow = dtComp.NewRow
                    dtRow("iscategory") = True
                    dtRow("competencies") = dRow("ccategory")
                    dtRow.Item("masterunkid") = dRow.Item("masterunkid")
                    intcmpCategory = dRow.Item("masterunkid")
                    dtComp.Rows.Add(dtRow)
                End If
                dtRow = dtComp.NewRow
                dtRow("iscategory") = False
                For Each dCol As DataColumn In dTable.Columns
                    dtRow(dCol.ColumnName) = dRow(dCol.ColumnName)
                Next
                dtComp.Rows.Add(dtRow)
            Next
            'Shani (31-Aug-2016) -- End

            lvData.DataSource = dtComp
            lvData.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Grid : " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call Fill_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("Action") = enAction.ADD_ONE
            Session("Unkid") = -1
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CBool(Session("Self_Assign_Competencies")) = True Then
                If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                    Session("Competencies_Filter") = cboEmployee.SelectedValue + "-" + cboPeriod.SelectedValue
                End If
            Else
                If CInt(cboAssessGroup.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                    Session("Competencies_Filter") = cboAssessGroup.SelectedValue + "-" + cboPeriod.SelectedValue
                End If
            End If
            'SHANI [09 Mar 2015]--END
            Response.Redirect(Session("servername") & "~/Assessment New/Competencies/wPg_AssignedCompetencies.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnNew_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboAssessGroup.SelectedValue = 0
            cboCompetenceCategory.SelectedValue = 0
            cboCompetencies.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            If CBool(Session("Self_Assign_Competencies")) = True Then
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    cboEmployee.SelectedValue = 0
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    cboEmployee.SelectedValue = Session("Employeeunkid")
                End If
            End If
            lvData.DataSource = Nothing
            lvData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Try
            If delReason.Reason.ToString.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Please enter valid reason.", Me)
                delReason.Show()
                Exit Sub
            Else
                Dim objAssignTran As New clsassess_competence_assign_tran

                With objAssignTran
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With

                'S.SANDEEP [22 Jan 2016] -- START
                If objAssignTran.Delete(Me.ViewState("DelTranId"), True, Session("UserId"), delReason.Reason, ConfigParameter._Object._CurrentDateAndTime, CBool(Session("Self_Assign_Competencies"))) = False Then
                    'If objAssignTran.Delete(Me.ViewState("DelTranId"), True, Session("UserId"), delReason.Reason, ConfigParameter._Object._CurrentDateAndTime) = False Then
                    'S.SANDEEP [22 Jan 2016] -- END
                    If objAssignTran._Message <> "" Then
                        DisplayMessage.DisplayMessage(objAssignTran._Message, Me)
                        Exit Sub
                    End If
                Else
                    Fill_Grid()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("delReason_buttonDelReasonYes_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboCCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompetenceCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objCompetencies As New clsassess_competencies_master
            dsList = objCompetencies.getComboList(CInt(IIf(cboCompetenceCategory.SelectedValue = "", 0, cboCompetenceCategory.SelectedValue)), _
                                                  CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)), True, "List")
            With cboCompetencies
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("cboCCategory_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Protected Sub lvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvData.ItemCommand
        Try
            If e.CommandName.ToUpper = "OBJEDIT" Then

                Session("Action") = enAction.EDIT_ONE
                Session("Unkid") = e.Item.Cells(5).Text
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If CBool(Session("Self_Assign_Competencies")) = True Then
                    If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                        Session("Competencies_Filter") = cboEmployee.SelectedValue + "-" + cboPeriod.SelectedValue
                    End If
                Else
                    If CInt(cboAssessGroup.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                        Session("Competencies_Filter") = cboAssessGroup.SelectedValue + "-" + cboPeriod.SelectedValue
                    End If
                End If
                'SHANI [09 Mar 2015]--END
                Response.Redirect(Session("servername") & "~/Assessment New/Competencies/wPg_AssignedCompetencies.aspx", False)
            ElseIf e.CommandName.ToUpper = "OBJDELETE" Then
                If Me.ViewState("DelTranId") IsNot Nothing Then
                    Me.ViewState("DelTranId") = e.Item.Cells(9).Text
                Else
                    Me.ViewState.Add("DelTranId", e.Item.Cells(9).Text)
                End If
                'S.SANDEEP [29 DEC 2015] -- START
                delReason.Title = "Please enter valid reason to void assigned competencies"
                'S.SANDEEP [29 DEC 2015] -- END
                delReason.Reason = ""
                delReason.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lvData_ItemCommand : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub lvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvData.ItemDataBound
        Try
            If e.Item.Cells.Count > 0 Then
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                    'Shani (31-Aug-2016) -- Start
                    'Enhancement - Change Competencies List Design Given by Andrew
                    'If e.Item.Cells(6).Text = 2 Then
                    '    e.Item.Cells(0).Controls.RemoveAt(1) : e.Item.Cells(1).Controls.RemoveAt(1)
                    '    e.Item.ForeColor = Color.Gray
                    'End If
                    'If CBool(e.Item.Cells(7).Text) = True Then
                    '    e.Item.Cells(0).Controls.RemoveAt(1) : e.Item.Cells(1).Controls.RemoveAt(1)
                    '    e.Item.ForeColor = Color.Blue
                    'End If
                    'If e.Item.Cells(8).Text = enObjective_Status.SUBMIT_APPROVAL Then
                    '    e.Item.Cells(0).Controls.RemoveAt(1) : e.Item.Cells(1).Controls.RemoveAt(1)
                    '    e.Item.ForeColor = Color.Green
                    'End If
                    If CBool(e.Item.Cells(11).Text) = True Then
                        'CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                        'e.Item.Cells(0).Text = e.Item.Cells(3).Text
                        Dim CellCout As Integer = 1
                        For i = 0 To lvData.Columns.Count - 2
                            If lvData.Columns(i).Visible Then
                                e.Item.Cells(i).Visible = False
                                CellCout += 1
                            End If
                        Next
                        e.Item.Cells(3).Visible = True
                        e.Item.Cells(3).ColumnSpan = CellCout - 2
                        e.Item.Cells(3).CssClass = "GroupHeaderStylecomp"
                        e.Item.Cells(10).Visible = True
                        e.Item.Cells(10).CssClass = "GroupHeaderStylecomp"
                    Else
                    If e.Item.Cells(6).Text = 2 Then
                        e.Item.Cells(0).Controls.RemoveAt(1) : e.Item.Cells(1).Controls.RemoveAt(1)
                        e.Item.ForeColor = Color.Gray
                    End If
                    If CBool(e.Item.Cells(7).Text) = True Then
                        e.Item.Cells(0).Controls.RemoveAt(1) : e.Item.Cells(1).Controls.RemoveAt(1)
                        e.Item.ForeColor = Color.Blue
                    End If
                    If e.Item.Cells(8).Text = enObjective_Status.SUBMIT_APPROVAL Then
                        e.Item.Cells(0).Controls.RemoveAt(1) : e.Item.Cells(1).Controls.RemoveAt(1)
                        e.Item.ForeColor = Color.Green
                    End If
                End If

                    'Shani (31-Aug-2016) -- End
                    
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lvData_ItemDataBound : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Shani (31-Aug-2016) -- Start
    'Enhancement - Change Competencies List Design Given by Andrew
    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtData.Text = ""
            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
            If CBool(xRow.Cells(11).Text) = True Then
                Dim objCOMaster As New clsCommon_Master
                objCOMaster._Masterunkid = CInt(xRow.Cells(12).Text)
                If objCOMaster._Description <> "" Then
                    txtData.Text = objCOMaster._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCOMaster = Nothing
            ElseIf CBool(xRow.Cells(11).Text) = False Then
                Dim objCPMsater As New clsassess_competencies_master
                objCPMsater._Competenciesunkid = CInt(xRow.Cells(13).Text)
                If objCPMsater._Description <> "" Then
                    txtData.Text = objCPMsater._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCPMsater = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("link_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'Shani (31-Aug-2016) -- End

#End Region

End Class
