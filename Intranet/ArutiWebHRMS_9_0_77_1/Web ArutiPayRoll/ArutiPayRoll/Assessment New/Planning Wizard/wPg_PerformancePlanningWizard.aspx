﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_PerformancePlanningWizard.aspx.vb" Inherits="wPg_PerformancePlanningWizard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
        
    </script>

    <script type="text/javascript">
        $(".objAddBtn").live("mousedown", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
    </script>

    <script type="text/javascript">
        function setscrollPosition(sender) {
            sender.scrollLeft = document.getElementById("<%=hdf_leftposition.ClientID%>").value;
            sender.scrollTop = document.getElementById("<%=hdf_topposition.ClientID%>").value;
        }
        function getscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("scrollable-container2").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("scrollable-container2").scrollLeft;
        }    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition3" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition4" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition5" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition6" runat="server" Value="0" />
    <%--<script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        var scroll3 = {
            Y: '#<%= hfScrollPosition3.ClientID %>'
        };
        var scroll4 = {
            Y: '#<%= hfScrollPosition4.ClientID %>'
        };
        var scroll5 = {
            Y: '#<%= hfScrollPosition5.ClientID %>'
        };
        var scroll6 = {
            Y: '#<%= hfScrollPosition6.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
                $("#scrollable-container3").scrollTop($(scroll3.Y).val());
                $("#scrollable-container4").scrollTop($(scroll4.Y).val());
                $("#scrollable-container5").scrollTop($(scroll5.Y).val());
                $("#scrollable-container6").scrollTop($(scroll5.Y).val());
            }
        }
    </script>--%>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Performance Planning Wizard"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <asp:MultiView ID="mvPerformancePlanningWz" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="vwPg1PeriodSelection" runat="server">
                                        <div class="panel-body">
                                            <div id="Div1" class="panel-default">
                                                <div id="Div2" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="lblPg1EmpSelection" runat="server" Style="font-weight: bold" Text="Employee Selection"></asp:Label>
                                                    </div>
                                                </div>
                                                <div id="Div3" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 22%">
                                                                <asp:Label ID="lblPlanningPeriod" runat="server" Text="Performance Planning For Period"></asp:Label>
                                                            </td>
                                                            <td style="width: 18%">
                                                                <asp:DropDownList ID="cboPlanningPeriod" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 5%">
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="lblNoOfDirects" runat="server" Text="Number of Directs"></asp:Label>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:TextBox ID="txtNoOfDirects" runat="server" ReadOnly="true" Style="text-align: center" Font-Bold="true"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 5%">
                                                            </td>
                                                            <td style="width: 15%">
                                                                <asp:Label ID="lblPlanningDone" runat="server" Text="Done with Planning"></asp:Label>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:TextBox ID="txtPlanningDone" runat="server" ReadOnly="true" Style="text-align: center" Font-Bold="true"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%" colspan="8">
                                                                <h3 style="border: 1px #DDD solid; margin-top: 5px; margin-bottom: 5px;">
                                                                </h3>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%" colspan="8">
                                                                <h3 style="text-align: center">
                                                                    Performance Planning For</h3>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <table style="width: 100%; margin-top: 10px">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <div id="scrollable-container" style="vertical-align: top; overflow: auto; max-height: 400px;" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                    <asp:DataGrid ID="dgvEmployee" runat="server" Style="margin: auto" AutoGenerateColumns="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                        <ItemStyle CssClass="griviewitem" />
                                                                        <Columns>
                                                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <asp:CheckBox ID="chkPg1SelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkPg1SelectAll_CheckedChanged" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:CheckBox ID="chkPg1Select" runat="server" AutoPostBack="True" Checked='<%# Bind("ischeck") %>' OnCheckedChanged="chkPg1Select_CheckedChanged" />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="Code" HeaderText="Employee Code" FooterText="" />
                                                                            <asp:BoundColumn DataField="NAME" HeaderText="Employee Name" FooterText="" />
                                                                            <asp:BoundColumn DataField="Department" HeaderText="Department" FooterText="" />
                                                                            <asp:BoundColumn DataField="Job" HeaderText="Job" FooterText="" />
                                                                            <asp:BoundColumn DataField="Id" HeaderText="" FooterText="" Visible="false" />
                                                                        </Columns>
                                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                    </asp:DataGrid>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwPg2SetGoals" runat="server">
                                        <div id="Div4" class="panel-default">
                                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblPg2SetGoals" runat="server" Style="font-weight: bold" Text="Set Goals/Objectives"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div5" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="lblPg2Period" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtGPeriodDisplay" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 10%">
                                                        </td>
                                                        <td style="width: 10%; vertical-align: top">
                                                            <asp:Label ID="lblPg2Employee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" rowspan="4" valign="top">
                                                            <asp:TextBox ID="txtGEmployeeDisplay" runat="server" ReadOnly="true" TextMode="MultiLine" Height="50px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 50%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblGoalSource" runat="server" Text="Source"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:DropDownList ID="cboGoalSource" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 10%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="5">
                                                            <h3 style="border: 1px #DDD solid; margin-top: 5px; margin-bottom: 5px;">
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="scrollable-container1" style="vertical-align: top; overflow: auto; max-height: 300px;" onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="dgvGoalList1" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="97%">
                                                                    <ItemStyle CssClass="griviewitem" />
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <%--<HeaderTemplate>
                                                                                <asp:CheckBox ID="chkPg2" runat="server" AutoPostBack="true" />
                                                                            </HeaderTemplate>--%>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chk1Pg2" runat="server" AutoPostBack="true" Checked='<%# Bind("assign")%>' OnCheckedChanged="chk1Pg2_CheckedChanged" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="Perspective" HeaderText="Perspective" FooterText="Perspective"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field1" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field2" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field3" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field4" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field5" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="St_Date"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="Ed_Date"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="CStatus"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="Weight"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="pct_complete"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield1unkid" HeaderText="empfield1unkid" FooterText="empfield1unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield2unkid" HeaderText="empfield2unkid" FooterText="empfield2unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield3unkid" HeaderText="empfield3unkid" FooterText="empfield3unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield4unkid" HeaderText="empfield4unkid" FooterText="empfield4unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield5unkid" HeaderText="empfield5unkid" FooterText="empfield5unkid" Visible="false"></asp:BoundColumn>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%; text-align: center">
                                                            <asp:Button ID="btnAddGoals" runat="server" Text="v" CssClass="btndefault" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="scrollable-container2" style="vertical-align: top; overflow: auto; max-height: 300px;" onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="dgvGoalList2" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="97%">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgAdd" runat="server" CssClass="objAddBtn" OnClientClick="getscrollPosition();" CommandName="objAdd" ToolTip="New" OnClick="imgAdd_Click" AutoPostBack="true" ImageUrl="~/images/add_16.png" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgEdit" runat="server" CssClass="objAddBtn" OnClientClick="getscrollPosition()" CommandName="objEdit" ToolTip="Edit" OnClick="imgEdit_Click" AutoPostBack="true" ImageUrl="~/images/edit.png" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgDelete" runat="server" CssClass="objAddBtn" OnClientClick="getscrollPosition()" CommandName="objDelete" ToolTip="Delete" OnClick="imgDelete_Click" AutoPostBack="true" ImageUrl="~/images/remove.png" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <%--<HeaderTemplate>
                                                                                <asp:CheckBox ID="chk1Pg2" runat="server" AutoPostBack="true" />
                                                                            </HeaderTemplate>--%>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chk2Pg2" runat="server" AutoPostBack="true" Checked='<%# Bind("assign")%>' OnCheckedChanged="chk2Pg2_CheckedChanged" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="owrfieldunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="perspectiveunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="periodunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="userunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="perspective" HeaderText="Perspective" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="linkfieldid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f1fieldunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2fieldunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3fieldunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4fieldunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5fieldunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f1field_data" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2field_data" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3field_data" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4field_data" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5field_data" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f1weight" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2weight" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3weight" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4weight" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5weight" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <asp:BoundColumn DataField="f1pct_completed" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2pct_completed" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3pct_completed" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4pct_completed" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5pct_completed" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <asp:BoundColumn DataField="f1startdate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2startdate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3startdate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4startdate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5startdate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <asp:BoundColumn DataField="f1enddate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2enddate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3enddate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4enddate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5enddate" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <asp:BoundColumn DataField="f1statusunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2statusunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3statusunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4statusunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5statusunkid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <asp:BoundColumn DataField="f1status" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2status" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3status" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4status" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5status" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <asp:BoundColumn DataField="f1fieldtypeid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f2fieldtypeid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f3fieldtypeid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f4fieldtypeid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="f5fieldtypeid" HeaderText="" FooterText="" Visible="false"></asp:BoundColumn>                                                                        
                                                                        <%--<asp:BoundColumn DataField="Perspective" HeaderText="Perspective" FooterText="Perspective"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field1" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field2" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field3" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field4" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field5" HeaderText="" FooterText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="St_Date"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="Ed_Date"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="CStatus"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="Weight"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="pct_complete"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield1unkid" HeaderText="empfield1unkid" FooterText="empfield1unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield2unkid" HeaderText="empfield2unkid" FooterText="empfield2unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield3unkid" HeaderText="empfield3unkid" FooterText="empfield3unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield4unkid" HeaderText="empfield4unkid" FooterText="empfield4unkid" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="empfield5unkid" HeaderText="empfield5unkid" FooterText="empfield5unkid" Visible="false"></asp:BoundColumn>--%>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnDeleteGoals" runat="server" Text="Delete" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwPg3ReviewGoals" runat="server">
                                        <div id="Div12" class="panel-default">
                                            <div id="Div13" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblPg3ReviewGoals" runat="server" Style="font-weight: bold" Text="Review Goals/Weight Confirmation"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div14" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="lblPg3Period" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtGRPeriodDisplay" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 10%">
                                                        </td>
                                                        <td style="width: 10%; vertical-align: top">
                                                            <asp:Label ID="lblPg3Employee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:TextBox ID="txtGREmployeeDisplay" runat="server" ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="5">
                                                            <h3 style="border: 1px #DDD solid; margin-top: 5px; margin-bottom: 5px;">
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%; text-align: right" colspan="2">
                                                            <asp:CheckBox ID="chkGoalEqweight" Style="margin-right: 10px" runat="server" Text="Equal Weight" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="2">
                                                            <div id="scrollable-container3" style="vertical-align: top; overflow: auto; max-height: 400px;" onscroll="$(scroll3.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="DataGrid1Pg3" runat="server" Style="margin: auto" AutoGenerateColumns="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <ItemStyle CssClass="griviewitem" />
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="Perspective" FooterText="Perspective" HeaderText="Perspective"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field1" FooterText="" HeaderText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field2" FooterText="" HeaderText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field3" FooterText="" HeaderText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field4" FooterText="" HeaderText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Field5" FooterText="" HeaderText=""></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="St_Date" FooterText="St_Date" HeaderText="Start Date"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Ed_Date" FooterText="Ed_Date" HeaderText="End Date"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="CStatus" FooterText="CStatus" HeaderText="Status"></asp:BoundColumn>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" HeaderText="Weight" ItemStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtGoalWeight" runat="server" OnTextChanged="txtGoalWeight_TextChanged" Style="text-align: right" Text='<%# Bind("Weight")  %>'></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                            <ItemStyle Width="100px" />
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 80%; text-align: right">
                                                            <asp:Label ID="lblPg3Total" runat="server" Style="margin-right: 10px" Text="Total"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtGoalTotal" runat="server" ReadOnly="True" Style="text-align: right;"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwPg4SetCompentencies" runat="server">
                                        <div id="Div7" class="panel-default">
                                            <div id="Div8" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblPg4SetCompetencies" runat="server" Style="font-weight: bold" Text="Set Competencies"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div9" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="lblPg4Period" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtCPeriodDisplay" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 10%">
                                                        </td>
                                                        <td style="width: 10%; vertical-align: top">
                                                            <asp:Label ID="lblPg4Employee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:TextBox ID="txtCEmployeeDisplay" runat="server" ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 50%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblCompetencySource" runat="server" Text="Source"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:DropDownList ID="cboCompetencySource" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 70%" colspan="3">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="5">
                                                            <h3 style="border: 1px #DDD solid; margin-top: 5px; margin-bottom: 5px;">
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblCompetenceGroup" runat="server" Text="Competency Group"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:DropDownList ID="cboCompetenceGroup" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblCompetencyCategory" runat="server" Text="Competency Category"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:DropDownList ID="cboCategory" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="4">
                                                            <div id="scrollable-container4" style="vertical-align: top; overflow: auto; max-height: 400px;" onscroll="$(scroll4.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="DataGrid2Pg4" runat="server" Style="margin: auto" AutoGenerateColumns="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkPg4" runat="server" AutoPostBack="true" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chk1Pg4" runat="server" AutoPostBack="true" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%; text-align: center" colspan="4">
                                                            <asp:Button ID="btnPg4Add" runat="server" Text="v" CssClass="btndefault" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="4">
                                                            <div id="scrollable-container5" style="vertical-align: top; overflow: auto; max-height: 400px;" onscroll="$(scroll5.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="DataGrid3Pg4" runat="server" Style="margin: auto" AutoGenerateColumns="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chk1Pg4All" runat="server" AutoPostBack="true" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chk2Pg4Select" runat="server" AutoPostBack="true" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnPg4Delete" runat="server" Text="Delete" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwPg5ReviewCompentencies" runat="server">
                                        <div id="Div16" class="panel-default">
                                            <div id="Div17" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblPg5ReviewCompt" runat="server" Style="font-weight: bold" Text="Review Compentencies/Set Weight"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div18" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%;">
                                                            <asp:Label ID="lblPg5Period" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtCRPeriodDisplay" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 10%">
                                                        </td>
                                                        <td style="width: 10%; vertical-align: top">
                                                            <asp:Label ID="lblPg5Employee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:TextBox ID="txtCREmployeeDisplay" runat="server" ReadOnly="true" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="5">
                                                            <h4 style="border: 1px #DDD solid">
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%; text-align: right" colspan="2">
                                                            <asp:CheckBox ID="chkPg5EqWeight" runat="server" Style="margin-right: 10px" Text="Equal Weight" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="2">
                                                            <div id="scrollable-container6" style="vertical-align: top; overflow: auto; max-height: 400px;" onscroll="$(scroll6.Y).val(this.scrollTop);">
                                                                <asp:DataGrid ID="DataGrid1Pg5" runat="server" Style="margin: auto" AutoGenerateColumns="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtPg3dgcolh" Style="text-align: right" runat="server" AutoPostBack="True" Text=""></asp:TextBox>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Right" />
                                                                        </asp:TemplateColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 80%; text-align: right">
                                                            <asp:Label ID="lblTotalPg5" runat="server" Style="margin-right: 10px" Text="Total"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtPg5Total" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwPg6FinalConfirmation" runat="server">
                                        <div id="Div6" class="panel-default">
                                            <div id="Div10" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblPg6FinalConf" runat="server" Style="font-weight: bold" Text="Final Confirmation"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div11" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="4">
                                                            <h3>
                                                                You are about to complete Performance Planning</h3>
                                                            <h4 style="border-bottom: 1px #DDD solid; margin-bottom: 10px">
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblPg6Period" runat="server" Text="For the Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="txtCnfPeriodDisplay" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35%">
                                                        </td>
                                                        <td style="width: 35%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 15%; vertical-align: top">
                                                            <asp:Label ID="lblPg6Employee" runat="server" Text="For the Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" colspan="2">
                                                            <asp:TextBox ID="txtCnfEmployeeDisplay" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="4">
                                                            <h4 style="border-top: 1px #DDD solid; margin-bottom: 10px">
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="4">
                                                            <h3>
                                                                Click the Confirmation button below to submit for Approval or Commit the plans.
                                                            </h3>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnPg6SaveCommit" runat="server" Text="Save & Commit" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:View>
                                    <asp:View ID="vwPg7SuccessfulPlanning" runat="server">
                                        <div id="Div15" class="panel-default">
                                            <div id="Div19" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblPg7SuccessFul" runat="server" Style="font-weight: bold" Text="Successful Planning"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div20" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="6">
                                                            <h3>
                                                                Congratulations! Performance Planning successfully Saved/Submitted for approval.</h3>
                                                            <h4 style="border-bottom: 1px #DDD solid; margin-bottom: 10px">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblPg7Directs" runat="server" Text="Number of Directs"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="txtPg7Directs" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblPg7Donewith" runat="server" Text="Done with Planning"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="txtPg7Donewith" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35%">
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblPg7Period" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="txtConPeriodDisplay" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 5%">
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblPg7PendingList" runat="server" Text="Pending List"></asp:Label>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:TextBox ID="txtPg7PendingList" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 35%">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnPg7Preview" runat="server" Text="Click to Preview Performance Template" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:View>
                                </asp:MultiView>
                                <div class="btn-default">
                                    <asp:Button ID="btnPrevious" runat="server" Text="Previous" CssClass="btndefault" />
                                    <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <table style="width: 100%;">
                    <tr id="trimageadd">
                        <td colspan="3">
                            <asp:HiddenField ID="hdfv" runat="server" />
                            <cc1:ModalPopupExtender ID="popAddButton" BehaviorID="MPE" runat="server" TargetControlID="hdfv" CancelControlID="btnAddCancel" DropShadow="true" BackgroundCssClass="popMenu" PopupControlID="pnlImageAdd" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlImageAdd" runat="server" CssClass="newpopup" Style="display: none; width: 100%">
                                <div class="panel-primary" style="margin-bottom: 0px;">
                                    <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                        <asp:DataList ID="dlmnuAdd" runat="server">
                                            <AlternatingItemStyle BackColor="Transparent" />
                                            <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                            <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 100%; height: 25px" class="mnuTools">
                                                        <asp:LinkButton ID='objadd' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>' OnClick="lnkAdd_Click" CssClass="lnkhover" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <asp:HiddenField ID="btnAddCancel" runat="server" />
                                        <asp:HiddenField ID="btndlmnuAdd" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="trimageEdit">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="popEditButton" runat="server" TargetControlID="btndlmnuEdit" CancelControlID="btnEditCancel" DropShadow="true" BackgroundCssClass="popMenu" PopupControlID="pnlImageEdit" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlImageEdit" runat="server" CssClass="newpopup" Style="display: none; width: 100%">
                                <div class="panel-primary" style="margin-bottom: 0px;">
                                    <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                        <asp:DataList ID="dlmnuEdit" runat="server">
                                            <AlternatingItemStyle BackColor="Transparent" />
                                            <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                            <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 100%; height: 25px" class="mnuTools">
                                                        <asp:LinkButton ID='objEdit' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>' CssClass="lnkhover" OnClick="lnkEdit_Click" />
                                                        <asp:HiddenField ID="objEditOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <asp:HiddenField ID="btnEditCancel" runat="server" />
                                        <asp:HiddenField ID="btndlmnuEdit" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="trimageDelete">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="popDeleteButton" runat="server" TargetControlID="btndlmnuDelete" CancelControlID="btnDeleteCancel" DropShadow="true" BackgroundCssClass="popMenu" PopupControlID="pnlImageDelete" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnlImageDelete" runat="server" CssClass="newpopup" Style="display: none; width: 100%">
                                <div class="panel-primary" style="margin-bottom: 0px;">
                                    <div class="panel-body" style="background-color: #EDF1F4; padding: 2px;">
                                        <asp:DataList ID="dlmnuDelete" runat="server">
                                            <AlternatingItemStyle BackColor="Transparent" />
                                            <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                            <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="width: 100%; height: 25px" class="mnuTools">
                                                        <asp:LinkButton ID='objDelete' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>' CssClass="lnkhover" OnClick="lnkDelete_Click" />
                                                        <asp:HiddenField ID="objDeleteOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:DataList>
                                        <asp:HiddenField ID="btnDeleteCancel" runat="server" />
                                        <asp:HiddenField ID="btndlmnuDelete" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="hiddenFiled">
                        <td colspan="3">
                            <asp:HiddenField ID="hdf_locationx" runat="server" />
                            <asp:HiddenField ID="hdf_locationy" runat="server" />
                        </td>
                    </tr>
                    <tr id="objfrmAddEditEmpField1">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField1" runat="server" TargetControlID="hdf_btnEmpField1Save" CancelControlID="hdf_btnEmpField1Save" DropShadow="true" BackgroundCssClass="ModalPopupBG" PopupControlID="pnl_objfrmAddEditEmpField1" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_objfrmAddEditEmpField1" runat="server" CssClass="newpopup" Style="display: none; width: 750px">
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblheaderEmpField1" runat="server" Text="AddEditEmpField1"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div21" class="panel-default">
                                            <div id="Div22" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div23" class="panel-body-default">
                                                <table style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px; margin-bottom: 10px;">
                                                    <tr style="width: 650px">
                                                        <td style="width: 220px; vertical-align: top">
                                                            <table style="width: 230px">
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField1Period" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField1Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField1Employee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField1EmployeeName" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtEmpField1EmployeeName" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="lblEmpField1Perspective" runat="server" Text="Perspective"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:DropDownList ID="cboEmpField1Perspective" runat="server" Width="230px" Height="20px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="objEmpField1lblField1" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:DropDownList ID="cboEmpField1FieldValue1" runat="server" Height="20px" Width="230px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="objEmpField1lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmpField1EmpField1" runat="server" TextMode="MultiLine" Height="125px" Width="230px" Style="resize: none;"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtEmpField1EmpField1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 430px; vertical-align: top">
                                                            <asp:Panel ID="pnl_RightEmpField1" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField1StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField1EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        </td>
                                                                        <td rowspan="5" style="width: 200px;">
                                                                            <cc1:TabContainer ID="objEmpField1tabcRemarks" runat="server" Width="200px" Height="90px">
                                                                                <cc1:TabPanel ID="objEmpField1tabpRemark1" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField1Remark1" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField1Remark1" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField1Remark1" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="True"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField1tabpRemark2" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField1Remark2" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField1Remark2" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField1Remark2" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField1tabpRemark3" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField1Remark3" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField1Remark3" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField1Remark3" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                            </cc1:TabContainer>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField1StartDate" runat="server" Width="75" AutoPostBack="false" />
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField1EndDate" runat="server" Width="75" AutoPostBack="false" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:Label ID="lblEmpField1Status" runat="server" Text="Status"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:DropDownList ID="cboEmpField1Status" runat="server" Height="25px" Width="200px" Enabled="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField1Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField1Percent" runat="server" Style="text-align: right" Text="0.00" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField1Weight" runat="server" Text="Weight"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField1Weight" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="width: 420px">
                                                                            <asp:TextBox ID="txtEmpField1SearchEmp" runat="server" Width="420px" AutoPostBack="true"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 420px">
                                                                        <td colspan="3" style="background-color: White">
                                                                            <asp:Panel ID="pnl_EmpField1dgvower" runat="server" Style="height: 160px; overflow: auto;">
                                                                                <asp:DataGrid ID="dgvEmpField1Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField1allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField1AllSelect_CheckedChanged" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField1select" runat="server" Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField1select_CheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_btnEmpField1Save" runat="server" />
                                                    <asp:Button ID="btnEmpField1Save" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnEmpField1Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="objfrmAddEditEmpField2">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField2" runat="server" TargetControlID="hdf_btnEmpField2Save" CancelControlID="hdf_btnEmpField2Save" DropShadow="true" BackgroundCssClass="ModalPopupBG" PopupControlID="pnl_objfrmAddEditEmpField2" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_objfrmAddEditEmpField2" runat="server" CssClass="newpopup" Style="display: none; width: 750px">
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblHeaderEmpField2" runat="server" Text="AddEditEmpField2"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div24" class="panel-default">
                                            <div id="Div25" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div26" class="panel-body-default">
                                                <table style="vertical-align: middle; overflow: auto; margin: 10px">
                                                    <tr style="width: 650px">
                                                        <td style="width: 230px; vertical-align: top">
                                                            <table style="width: 230px">
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField2Period" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField2Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField2Employee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField2EmployeeName" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField2lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:DropDownList ID="cboEmpField2EmpFieldValue1" runat="server" Width="230px" Height="20px" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hdf_cboEmpField2EmpFieldValue1" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="objEmpField2lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmpField2EmpField2" runat="server" TextMode="MultiLine" Width="230px" Height="175px" Style="resize: none"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtEmpField2EmpField2" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 420px; vertical-align: top">
                                                            <asp:Panel ID="pnl_RightEmpField2" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                                                        </td>
                                                                        <td rowspan="5" style="background-color: White; width: 200px">
                                                                            <cc1:TabContainer ID="objEmpField2tabcRemarks" runat="server" Width="200px" Height="90px" ActiveTabIndex="2">
                                                                                <cc1:TabPanel ID="objEmpField2tabpRemark1" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField2Remark1" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField2Remark1" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField2Remark1" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="True"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField2tabpRemark2" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField2Remark2" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField2Remark2" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField2Remark2" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField2tabpRemark3" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField2Remark3" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField2Remark3" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField2Remark3" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                            </cc1:TabContainer>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField2StartDate" runat="server" Width="75" />
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField2EndDate" runat="server" Width="75" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:Label ID="lblEmpField2Status" runat="server" Text="Status"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:DropDownList ID="cboEmpField2Status" runat="server" Height="25px" Width="200px" Enabled="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField2Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField2Percent" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField2Weight" runat="server" Text="Weight"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField2Weight" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="width: 420px">
                                                                            <asp:TextBox ID="txtEmpField2SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 420px">
                                                                        <td colspan="3" style="background-color: White">
                                                                            <asp:Panel ID="pnl_EmpField2dgvower" runat="server" Style="height: 160px; overflow: auto; border-bottom: 2px solid #DDD">
                                                                                <asp:DataGrid ID="dgvEmpField2Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField2allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField2AllSelect_CheckedChanged" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField2select" runat="server" Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField2select_CheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_btnEmpField2Save" runat="server" />
                                                    <asp:Button ID="btnEmpField2Save" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnEmpField2Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="objfrmAddEditEmpField3">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField3" runat="server" TargetControlID="hdf_btnEmpField3Save" CancelControlID="hdf_btnEmpField3Save" DropShadow="true" BackgroundCssClass="ModalPopupBG" PopupControlID="pnl_objfrmAddEditEmpField3" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_objfrmAddEditEmpField3" runat="server" CssClass="newpopup" Style="display: none; width: 760px">
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblHeaderEmpField3" runat="server" Text="AddEditEmpField3"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div27" class="panel-default">
                                            <div id="Div28" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div29" class="panel-body-default">
                                                <table style="vertical-align: middle;">
                                                    <tr style="width: 650px">
                                                        <td style="width: 230px; vertical-align: top">
                                                            <table style="width: 230px">
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField3Period" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField3Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField3Employee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField3Employee" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField3lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="objEmpField3txtEmpField1" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField3lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:DropDownList ID="cboEmpField3EmpFieldValue2" runat="server" Width="230px" Height="20px" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hdf_cboEmpField3EmpFieldValue2" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="objEmpField3lblOwrField3" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmpField3EmpField3" runat="server" TextMode="MultiLine" Width="230px" Height="125px"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtEmpField3EmpField3" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 420px; vertical-align: top">
                                                            <asp:Panel ID="pnl_RightEmpField3" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField3StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField3EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        </td>
                                                                        <td rowspan="5" style="background-color: White; width: 200px">
                                                                            <cc1:TabContainer ID="objEmpField3tabcRemarks" runat="server" Width="200px" Height="90px">
                                                                                <cc1:TabPanel ID="objEmpField3tabpRemark1" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField3Remark1" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField3Remark1" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField3Remark1" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField3tabpRemark2" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField3Remark2" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField3Remark2" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField3Remark2" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField3tabpRemark3" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField3Remark3" runat="server"></asp:Label>
                                                                                        <asp:HiddenField ID="hdf_lblEmpField3Remark3" runat="server" />
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField3Remark3" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                            </cc1:TabContainer>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField3StartDate" runat="server" Width="75" />
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField3EndDate" runat="server" Width="75" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:Label ID="lblEmpField3Status" runat="server" Text="Status"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:DropDownList ID="cboEmpField3Status" runat="server" Height="25px" Width="200px" Enabled="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField3Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField3Percent" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField3Weight" runat="server" Text="Weight"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField3Weight" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="width: 420px">
                                                                            <asp:TextBox ID="txtEmpField3SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 420px">
                                                                        <td colspan="3" style="background-color: White">
                                                                            <asp:Panel ID="pnl_EmpField3dgvOwner" runat="server" Style="height: 160px; overflow: auto; border-bottom: 2px solid #DDD">
                                                                                <asp:DataGrid ID="dgvEmpField3Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField3allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField3AllSelect_CheckedChanged" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField3select" runat="server" Checked='<%# Eval("ischeck")  %>' AutoPostBack="true" OnCheckedChanged="chkEmpField3select_CheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_btnEmpField3Save" runat="server" />
                                                    <asp:Button ID="btnEmpField3Save" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnEmpField3Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="objfrmAddEditEmpField4">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField4" runat="server" TargetControlID="hdf_btnEmpField4Save" CancelControlID="hdf_btnEmpField4Save" DropShadow="true" BackgroundCssClass="ModalPopupBG" PopupControlID="pnl_objfrmAddEditEmpField4" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_objfrmAddEditEmpField4" runat="server" CssClass="newpopup" Style="display: none; width: 760px">
                                <div class="panel-primary" style="margin-bottom: 0px;">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblHeaderEmpField4" runat="server" Text="AddEditOwrField3"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div30" class="panel-default">
                                            <div id="Div31" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div32" class="panel-body-default">
                                                <table style="vertical-align: middle; overflow: auto; margin: 10px">
                                                    <tr style="width: 650px">
                                                        <td style="width: 230px; vertical-align: top">
                                                            <table style="width: 230px">
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField4Period" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField4Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField4Employee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField4Employee" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField4lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="objEmpField4txtEmpField1" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField4lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="objEmpField4txtEmpField2" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField4lblEmpField3" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:DropDownList ID="cboEmpField4EmpFieldValue3" runat="server" Width="230px" Height="20px" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hdf_cboEmpField4EmpFieldValue3" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="objEmpField4lblEmpField4" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td>
                                                                        <asp:TextBox ID="objEmpField4txtEmpField4" runat="server" TextMode="MultiLine" Style="resize: none" Width="230px" Height="110px"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_objEmpField4txtEmpField4" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 420px; vertical-align: top">
                                                            <asp:Panel ID="pnl_RightEmpField4" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField4StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField4EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        </td>
                                                                        <td rowspan="5" style="background-color: White; width: 200px">
                                                                            <cc1:TabContainer ID="objEmpField4tabcRemarks" runat="server" Width="200px" Height="90px">
                                                                                <cc1:TabPanel ID="objEmpField4tabpRemark1" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField4Remark1" runat="server"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField4Remark1" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdf_txtEmpField4Remark1" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField4tabpRemark2" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField4Remark2" runat="server"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField4Remark2" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdf_txtEmpField4Remark2" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField4tabpRemark3" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField4Remark3" runat="server"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField4Remark3" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdf_txtEmpField4Remark3" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                            </cc1:TabContainer>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField4StartDate" runat="server" Width="75" />
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField4EndDate" runat="server" Width="75" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:Label ID="lblEmpField4Status" runat="server" Text="Status"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:DropDownList ID="cboEmpField4Status" runat="server" Height="25px" Width="200px" Enabled="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField4Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField4Percent" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField4Weight" runat="server" Text="Weight"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField4Weight" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="width: 420px">
                                                                            <asp:TextBox ID="txtEmpField4SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 420px">
                                                                        <td colspan="3" style="background-color: White">
                                                                            <asp:Panel ID="pnl_EmpField4dgvOwner" runat="server" Style="height: 195px; overflow: auto; border-bottom: 2px solid #DDD">
                                                                                <asp:DataGrid ID="dgvEmpField4Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField4allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField4AllSelect_CheckedChanged" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField4select" runat="server" Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField4select_CheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_btnEmpField4Save" runat="server" />
                                                    <asp:Button ID="btnEmpField4Save" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnEmpField4Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="objfrmAddEditEmpField5">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="pop_objfrmAddEditEmpField5" runat="server" TargetControlID="hdf_btnEmpField5Save" CancelControlID="hdf_btnEmpField5Save" DropShadow="true" BackgroundCssClass="ModalPopupBG" PopupControlID="pnl_objfrmAddEditEmpField5" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_objfrmAddEditEmpField5" runat="server" CssClass="newpopup" Style="display: none; width: 760px">
                                <div class="panel-primary" style="margin-bottom: 0px;">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblHeaderEmpField5" runat="server" Text="AddEditEmpField5"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div33" class="panel-default">
                                            <div id="Div34" class="panel-heading-default">
                                                <div style="float: left;">
                                                </div>
                                            </div>
                                            <div id="Div35" class="panel-body-default">
                                                <table style="vertical-align: middle; overflow: auto; margin: 10px">
                                                    <tr style="width: 650px">
                                                        <td style="width: 230px; vertical-align: top">
                                                            <table style="width: 230px">
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField5Period" runat="server" Text="Period"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField5Period" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="lblEmpField5Employee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px; display: none">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtEmpField5Employee" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField5lblEmpField1" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="objEmpField5txtEmpField1" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField5lblEmpField2" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="objEmpField5txtEmpField2" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField5lblEmpField3" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="objEmpField5txtEmpField3" runat="server" Width="230px" ReadOnly="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 300px">
                                                                    <td style="width: 300px">
                                                                        <asp:Label ID="objEmpField5lblEmpField4" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:DropDownList ID="cboEmpField5EmpFieldValue4" runat="server" Width="230px" Height="20px" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                        <asp:HiddenField ID="hdf_cboEmpField5EmpFieldValue4" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td style="width: 230px">
                                                                        <asp:Label ID="objEmpField5lblEmpField5" runat="server" Text="#Caption"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 230px">
                                                                    <td>
                                                                        <asp:TextBox ID="objEmpField5txtEmpField5" runat="server" TextMode="MultiLine" Width="230px" Height="100px" Style="resize: none"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_objEmpField5txtEmpField5" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 420px; vertical-align: top">
                                                            <asp:Panel ID="pnl_RightEmpField5" runat="server">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField5StartDate" runat="server" Text="Start Date"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField5EndDate" runat="server" Text="End Date"></asp:Label>
                                                                        </td>
                                                                        <td rowspan="5" style="background-color: White; width: 200px">
                                                                            <cc1:TabContainer ID="objEmpField5tabcRemarks" runat="server" Width="200px" Height="90px">
                                                                                <cc1:TabPanel ID="objEmpField5tabpRemark1" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField5Remark1" runat="server"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField5Remark1" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdf_txtEmpField5Remark1" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField5tabpRemark2" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField5Remark2" runat="server"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField5Remark2" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdf_txtEmpField5Remark2" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                                <cc1:TabPanel ID="objEmpField5tabpRemark3" runat="server" HeaderText="">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblEmpField5Remark3" runat="server"></asp:Label>
                                                                                    </HeaderTemplate>
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtEmpField5Remark3" runat="server" TextMode="MultiLine" Width="97%" Height="75px" Style="resize: none;" AutoPostBack="true"></asp:TextBox>
                                                                                        <asp:HiddenField ID="hdf_txtEmpField5Remark3" runat="server" />
                                                                                    </ContentTemplate>
                                                                                </cc1:TabPanel>
                                                                            </cc1:TabContainer>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField5StartDate" runat="server" Width="75" />
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <uc2:DateCtrl ID="dtpEmpField5EndDate" runat="server" Width="75" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:Label ID="lblEmpField5Status" runat="server" Text="Status"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="2" style="width: 220px">
                                                                            <asp:DropDownList ID="cboEmpField5Status" runat="server" Height="25px" Width="200px" Enabled="false">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField5Percentage" runat="server" Text="% Completed"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField5Percent" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);" Enabled="false"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display: none">
                                                                        <td style="width: 110px">
                                                                            <asp:Label ID="lblEmpField5Weight" runat="server" Text="Weight"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 110px">
                                                                            <asp:TextBox ID="txtEmpField5Weight" runat="server" Style="text-align: right" Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="3" style="width: 420px">
                                                                            <asp:TextBox ID="txtEmpField5SearchEmp" runat="server" AutoPostBack="true" Width="420px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 420px">
                                                                        <td colspan="3" style="background-color: White">
                                                                            <asp:Panel ID="pnl_EmpField5dgvOwner" runat="server" Style="height: 220px; overflow: auto; border-bottom: 2px solid #DDD">
                                                                                <asp:DataGrid ID="dgvEmpField5Owner" runat="server" AutoGenerateColumns="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                    <Columns>
                                                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField5allSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkEmpField5AllSelect_CheckedChanged" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkEmpField5select" runat="server" Checked='<%# Eval("ischeck") %>' AutoPostBack="true" OnCheckedChanged="chkEmpField5select_CheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateColumn>
                                                                                        <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                                                        <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false"></asp:BoundColumn>
                                                                                    </Columns>
                                                                                </asp:DataGrid>
                                                                            </asp:Panel>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_btnEmpField5Save" runat="server" />
                                                    <asp:Button ID="btnEmpField5Save" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnEmpField5Cancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="YesNoPopup">
                        <td colspan="3">
                            <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="ModalPopupBG" CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_YesNo" runat="server" CssClass="newpopup" Style="display: none; width: 450px">
                                <div class="panel-primary" style="margin-bottom: 0px;">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div36" class="panel-default">
                                            <div id="Div37" class="panel-heading-default" style="height: 50px">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblMessage" runat="server" Text="Message :" />
                                                </div>
                                            </div>
                                            <div id="Div38" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:TextBox ID="txtMessage" runat="server" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                    <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                                    <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="HiddenFieldSetScroll">
                        <td colspan="3">
                            <asp:HiddenField ID="hdf_topposition" runat="server" />
                            <asp:HiddenField ID="hdf_leftposition" runat="server" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
