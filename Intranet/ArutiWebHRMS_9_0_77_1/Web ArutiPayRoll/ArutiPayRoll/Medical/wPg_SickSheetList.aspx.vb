﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region


Partial Class Medical_wPg_SickSheetList
    Inherits Basepage


#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim strSearching As String = ""


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmSickSheetList"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try

            'Pinkal (19-Nov-2012) -- Start
            'Enhancement : TRA Changes

            ' Dim objprovider As New clsinstitute_master
            '  dsFill = objprovider.getListForCombo(True, "Provider", True, 1, True, CInt(Session("UserId")))

            Dim objUserMapping As New clsprovider_mapping
            dsFill = objUserMapping.GetUserProvider("Provider", , CInt(Session("UserId")), True)
            Dim drRow As DataRow = dsFill.Tables(0).NewRow
            drRow("providerunkid") = 0
            drRow("name") = "Select"
            dsFill.Tables(0).Rows.InsertAt(drRow, 0)

            'Pinkal (19-Nov-2012) -- End


            drpProvider.DataTextField = "name"
            drpProvider.DataValueField = "providerunkid"
            drpProvider.DataSource = dsFill.Tables("Provider")
            drpProvider.DataBind()

            dsFill = Nothing
            Dim objjob As New clsJobs
            dsFill = objjob.getComboList("Job", True)
            drpJob.DataTextField = "name"
            drpJob.DataValueField = "jobunkid"
            drpJob.DataSource = dsFill.Tables("Job")
            drpJob.DataBind()

            dsFill = Nothing
            Dim objMaster As New clsmedical_master
            dsFill = objMaster.getListForCombo(enMedicalMasterType.Cover, "Cover", True)
            drpMedicalCover.DataTextField = "name"
            drpMedicalCover.DataValueField = "mdmasterunkid"
            drpMedicalCover.DataSource = dsFill.Tables(0)
            drpMedicalCover.DataBind()

            dsFill = Nothing
            Dim objEmployee As New clsEmployee_Master


            'Pinkal (5-MAY-2012) -- Start
            'Enhancement : TRA Changes

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            Dim strUACF As String = "TRUE"
            If CBool(Session("RevokeUserAccessOnSicksheet")) = True Then
                strUACF = "FALSE"
            End If

            dsFill = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                 False, "Employee", True, , , , , , , , , , , , , , , , , CBool(strUACF))

            'Pinkal (06-Jan-2016) -- End

            'Shani(24-Aug-2015) -- End

            'Pinkal (5-MAY-2012) -- End

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'drpEmployee.DataTextField = "employeename"
            drpEmployee.DataTextField = "EmpCodeName"
            'Nilay (09-Aug-2016) -- End
            drpEmployee.DataValueField = "employeeunkid"
            drpEmployee.DataSource = dsFill.Tables(0)
            drpEmployee.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strSearching As String = ""
        Dim dtList As New DataTable
        Try
            Dim objSickSheet As New clsmedical_sicksheet


            'Pinkal (12-Feb-2015) -- Start
            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User AndAlso CBool(Session("AllowToViewEmpSickSheetList")) = False Then Exit Sub
            'Pinkal (12-Feb-2015) -- End



            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            objSickSheet._RevokeUserAccessOnSickSheet = CBool(Session("RevokeUserAccessOnSicksheet"))
            'Pinkal (12-Sep-2014) -- End



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            If txtSickSheet.Text.Trim.Length > 0 Then
                strSearching = "AND mdmedical_sicksheet.sicksheetno like '%" & txtSickSheet.Text.Trim & "%' "
            End If

            If CInt(drpEmployee.SelectedValue) > 0 Then
                strSearching &= "AND mdmedical_sicksheet.employeeunkid =" & CInt(drpEmployee.SelectedValue) & " "
            End If

            If CInt(drpProvider.SelectedValue) > 0 Then
                strSearching &= "AND mdmedical_sicksheet.instituteunkid =" & CInt(drpProvider.SelectedValue) & " "
            End If

            If CInt(drpJob.SelectedValue) > 0 Then
                strSearching &= "AND mdmedical_sicksheet.jobunkid =" & CInt(drpJob.SelectedValue) & " "
            End If

            If CInt(drpMedicalCover.SelectedValue) > 0 Then
                strSearching &= "AND mdmedical_sicksheet.medicalcoverunkid =" & CInt(drpMedicalCover.SelectedValue) & " "
            End If

            If dtpFromdate.IsNull = False Then
                strSearching &= "AND mdmedical_sicksheet.sicksheetdate >= '" & eZeeDate.convertDate(dtpFromdate.GetDate.Date) & "' "
            Else
                strSearching &= "AND mdmedical_sicksheet.sicksheetdate >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearching &= "AND mdmedical_sicksheet.sicksheetdate <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            Else
                strSearching &= "AND mdmedical_sicksheet.sicksheetdate <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' "
            End If


            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If


            'Pinkal (06-Jan-2016) -- End



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objSickSheet.GetList("SickSheet", , , , CBool(Session("IsIncludeInactiveEmp")), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("UserId"))
            dsList = objSickSheet.GetList(Session("Database_Name").ToString(), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                          Session("UserAccessModeSetting").ToString(), True, _
                                           False, "SickSheet", True, "", -1, strSearching)
            'Shani(20-Nov-2015) -- End



            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'If txtSickSheet.Text.Trim.Length > 0 Then
            '    strSearching = "AND sicksheetno like '%" & txtSickSheet.Text.Trim & "%' "
            'End If

            'If CInt(drpEmployee.SelectedValue) > 0 Then
            '    strSearching &= "AND employeeunkid =" & CInt(drpEmployee.SelectedValue) & " "
            'End If

            'If CInt(drpProvider.SelectedValue) > 0 Then
            '    strSearching &= "AND instituteunkid =" & CInt(drpProvider.SelectedValue) & " "
            'End If

            'If CInt(drpJob.SelectedValue) > 0 Then
            '    strSearching &= "AND jobunkid =" & CInt(drpJob.SelectedValue) & " "
            'End If

            'If CInt(drpMedicalCover.SelectedValue) > 0 Then
            '    strSearching &= "AND medicalcoverunkid =" & CInt(drpMedicalCover.SelectedValue) & " "
            'End If

            'If dtpFromdate.IsNull = False Then
            '    strSearching &= "AND sicksheetdate >= '" & eZeeDate.convertDate(dtpFromdate.GetDate.Date) & "' "
            'Else
            '    strSearching &= "AND sicksheetdate >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' "
            'End If

            'If dtpToDate.IsNull = False Then
            '    strSearching &= "AND sicksheetdate <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            'Else
            '    strSearching &= "AND sicksheetdate <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' "
            'End If


            'If strSearching.Length > 0 Then
            '    strSearching = strSearching.Substring(3)
            '    dtList = New DataView(dsList.Tables("SickSheet"), strSearching, "sicksheetdate desc", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtList = dsList.Tables("SickSheet")
            'End If

            dtList = New DataView(dsList.Tables("SickSheet"), "", "sicksheetdate desc", DataViewRowState.CurrentRows).ToTable

            'Pinkal (06-Jan-2016) -- End


            If (Not dtList Is Nothing) Then
                'Shani [ 17 SEP 2014 ] -- START
                'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'If dtList.Rows.Count > gvSickSheet.PageSize Then
                '    gvSickSheet.AllowPaging = True
                'Else
                '    gvSickSheet.AllowPaging = False
                'End If
                'Nilay (01-Feb-2015) -- End
                'Shani [ 17 SEP 2014 ] -- END
                gvSickSheet.DataSource = dtList
                gvSickSheet.DataKeyField = "sicksheetunkid"
                gvSickSheet.DataBind()
            Else
                DisplayMessage.DisplayMessage("GetData :-  ", Me)
                Exit Sub
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                gvSickSheet.CurrentPageIndex = 0
                gvSickSheet.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError("GetData :- " & ex.Message, Me)
            End If
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
            'Nilay (01-Feb-2015) -- End
            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmSickSheetList"
            'StrModuleName2 = "mnuMedical"
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            If Not IsPostBack Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'BtnDelete.Visible = Session("AllowToDeleteMedicalSickSheet")
                'Nilay (01-Feb-2015) -- End
                BtnNew.Visible = CBool(Session("AllowToAddMedicalSickSheet"))
                FillCombo()


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                dtpFromdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'Pinkal (18-Dec-2012) -- End

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("SickSheet_Filter") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(IIf(CStr(Session("SickSheet_Filter")).Split(CChar("-"))(0).Trim = "", 0, CStr(Session("SickSheet_Filter")).Split(CChar("-"))(0).Trim))
                    drpJob.SelectedValue = CStr(IIf(CStr(Session("SickSheet_Filter")).Split(CChar("-"))(1).Trim = "", 0, CStr(Session("SickSheet_Filter")).Split(CChar("-"))(1).Trim))
                    drpProvider.SelectedValue = CStr(IIf(CStr(Session("SickSheet_Filter")).Split(CChar("-"))(2).Trim = "", 0, CStr(Session("SickSheet_Filter")).Split(CChar("-"))(2).Trim))
                    Session.Remove("SickSheet_Filter")
                    If CInt(drpEmployee.SelectedValue) > 0 Or CInt(drpJob.SelectedValue) > 0 Or CInt(drpProvider.SelectedValue) > 0 Then
                        Call BtnSearch_Click(BtnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleImageSaprator1(False)
    '    ToolbarEntry1.VisibleImageSaprator2(False)
    '    ToolbarEntry1.VisibleImageSaprator3(False)
    '    ToolbarEntry1.VisibleImageSaprator4(False)
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            Session("SickSheetId") = Nothing

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(drpEmployee.SelectedValue) > 0 Or CInt(drpJob.SelectedValue) > 0 Or CInt(drpProvider.SelectedValue) > 0 Then
                Session("SickSheet_Filter") = drpEmployee.SelectedValue + "-" + drpJob.SelectedValue + "-" + drpProvider.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END
            Response.Redirect("~/Medical/wPgAddEdit_SickSheet.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnNew_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try

            If CInt(drpEmployee.SelectedValue) <= 0 AndAlso CInt(drpProvider.SelectedValue) <= 0 AndAlso CInt(drpJob.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Either Employee Or Provider Or Job to Continue.", Me)
                Exit Sub
            End If

            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnSearch_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            If drpEmployee.Items.Count > 0 Then drpEmployee.SelectedIndex = 0
            If drpProvider.Items.Count > 0 Then drpProvider.SelectedIndex = 0
            If drpJob.Items.Count > 0 Then drpJob.SelectedIndex = 0
            If drpMedicalCover.Items.Count > 0 Then drpMedicalCover.SelectedIndex = 0
            txtSickSheet.Text = ""
            gvSickSheet.DataSource = Nothing
            gvSickSheet.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReset_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Nilay (01-Feb-2015) -- End
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'Nilay (01-Feb-2015) -- txtreasondel.Text = ""
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If

            Dim objSickSheet As New clsmedical_sicksheet

            If CInt(Session("loginBy")) = Global.User.en_loginby.Employee Then
                objSickSheet._Userunkid = -1
            Else
                objSickSheet._Userunkid = CInt(Session("UserId"))
            End If


            With objSickSheet
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text

                If CInt(Session("loginBy")) = Global.User.en_loginby.User Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                End If

                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))

                .Delete(CInt(Me.ViewState("SickSheetId")))

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("SickSheetId") = Nothing
                End If
                popup_DeleteReason.Dispose() 'Nilay (01-Feb-2015) -- popup1.Dispose()
                popup_DeleteReason.Reason = "" 'Nilay (01-Feb-2015) -- 'txtreasondel.Text = ""
                FillList()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnDelete_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " GridView's Event(s) "

    Protected Sub gvSickSheet_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvSickSheet.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            If e.CommandName = "Select" Then
                Dim objSickSheet As New clsmedical_sicksheet
                If objSickSheet.isUsed(CInt(gvSickSheet.DataKeys(e.Item.ItemIndex))) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry,You cannot Edit/Delete this sicksheet.Reason:It is used in Medical claim."), Me)
                    Exit Sub
                End If

                Session.Add("SickSheetId", gvSickSheet.DataKeys(e.Item.ItemIndex))
                If CInt(drpEmployee.SelectedValue) > 0 Or CInt(drpJob.SelectedValue) > 0 Or CInt(drpProvider.SelectedValue) > 0 Then
                    Session("SickSheet_Filter") = drpEmployee.SelectedValue + "-" + drpJob.SelectedValue + "-" + drpProvider.SelectedValue
                End If
                Response.Redirect("~/Medical/wPgAddEdit_SickSheet.aspx", False)
            ElseIf e.CommandName = "Print" Then
                Dim objSickSheetRpt As New ArutiReports.clsSickSheetReport
                objSickSheetRpt._SickSheetunkid = gvSickSheet.DataKeys(e.Item.ItemIndex).ToString()
                objSickSheetRpt._Employeeunkid = gvSickSheet.Items(e.Item.ItemIndex).Cells(9).Text
                objSickSheetRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objSickSheetRpt._UserUnkId = CInt(Session("UserId"))
                Dim objSicksheet As New clsmedical_sicksheet
                objSicksheet._Sicksheetunkid = CInt(gvSickSheet.DataKeys(e.Item.ItemIndex))
                objSickSheetRpt._SicksheetDate = objSicksheet._Sicksheetdate.Date
                objSicksheet = Nothing
                objSickSheetRpt._SicksheetTemplate = CInt(Session("SicksheetTemplate"))

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'objSickSheetRpt.generateReport(enPrintAction.None, enExportAction.None)
                SetDateFormat()
                objSickSheetRpt.generateReport(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, enPrintAction.None, enExportAction.None)
                'Pinkal (16-Apr-2016) -- End

                Session("objRpt") = objSickSheetRpt._Rpt

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                'Shani(24-Aug-2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvSickSheet_ItemCommand :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvSickSheet_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvSickSheet.DeleteCommand
        Try

            'Pinkal (12-Sep-2014) -- Start
            'Medical Enhancement - TANAPA URGENT CHANGES FOR MEDICAL SICK SHEET GIVEN BY OLAIS
            Dim objSickSheet As New clsmedical_sicksheet
            If objSickSheet.isUsed(CInt(gvSickSheet.DataKeys(e.Item.ItemIndex))) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry,You cannot Edit/Delete this sicksheet.Reason:It is used in Medical claim."), Me)
                Exit Sub
            End If

            'Pinkal (12-Sep-2014) -- End

            Me.ViewState.Add("SickSheetId", CInt(gvSickSheet.DataKeys(e.Item.ItemIndex)))
            popup_DeleteReason.Show() 'Nilay (01-Feb-2015) --popup1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("gvSickSheet_DeleteCommand :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvSickSheet_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvSickSheet.PageIndexChanged
        Try
            gvSickSheet.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError("gvSickSheet_PageIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvSickSheet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvSickSheet.ItemDataBound
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then
                gvSickSheet.Columns(0).Visible = CBool(Session("AllowToEditMedicalSickSheet"))
                gvSickSheet.Columns(1).Visible = CBool(Session("AllowToDeleteMedicalSickSheet"))
                gvSickSheet.Columns(2).Visible = CBool(Session("AllowToPrintMedicalSickSheet"))


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If Session("DateFormat").ToString() <> Nothing Then
                '    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat").ToString())
                'Else
                '    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).ToShortDateString
                'End If
                    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).ToShortDateString
                'Pinkal (16-Apr-2016) -- End



            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvSickSheet_ItemDataBound : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "ToolBar Event(s)"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Medical/wPgAddEdit_SickSheet.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("ToolbarEntry1_FormMode_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End



        Me.lblSickSheetNo.Text = Language._Object.getCaption(Me.lblSickSheetNo.ID, Me.lblSickSheetNo.Text)
        Me.LblSickSheetDate.Text = Language._Object.getCaption(Me.LblSickSheetDate.ID, Me.LblSickSheetDate.Text)
        Me.LblToSickSheetDate.Text = Language._Object.getCaption(Me.LblToSickSheetDate.ID, Me.LblToSickSheetDate.Text)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblProvider.Text = Language._Object.getCaption(Me.lblProvider.ID, Me.lblProvider.Text)
        Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
        Me.lblMedicalCover.Text = Language._Object.getCaption(Me.lblMedicalCover.ID, Me.lblMedicalCover.Text)

        gvSickSheet.Columns(0).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(0).FooterText, gvSickSheet.Columns(0).HeaderText).Replace("&", "")
        gvSickSheet.Columns(1).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(1).FooterText, gvSickSheet.Columns(1).HeaderText).Replace("&", "")
        gvSickSheet.Columns(2).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(2).FooterText, gvSickSheet.Columns(2).HeaderText).Replace("&", "")
        gvSickSheet.Columns(3).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(3).FooterText, gvSickSheet.Columns(3).HeaderText)
        gvSickSheet.Columns(4).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(4).FooterText, gvSickSheet.Columns(4).HeaderText)
        gvSickSheet.Columns(5).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(5).FooterText, gvSickSheet.Columns(5).HeaderText)
        gvSickSheet.Columns(6).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(6).FooterText, gvSickSheet.Columns(6).HeaderText)
        gvSickSheet.Columns(7).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(7).FooterText, gvSickSheet.Columns(7).HeaderText)
        gvSickSheet.Columns(8).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(8).FooterText, gvSickSheet.Columns(8).HeaderText)
        gvSickSheet.Columns(9).HeaderText = Language._Object.getCaption(gvSickSheet.Columns(9).FooterText, gvSickSheet.Columns(9).HeaderText)

        Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.BtnDelete.Text = Language._Object.getCaption(Me.BtnDelete.ID, Me.BtnDelete.Text).Replace("&", "")
        'Nilay (01-Feb-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

End Class
