﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_MedicalClaimList.aspx.vb"
    Inherits="Medical_wPg_MedicalClaimList" MasterPageFile="~/home.master"
    Title="Medical Claim List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script type="text/javascript">
    function onlyNumbers(evt,isInvoiceFromamt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (isInvoiceFromamt == true) {
            var totInvoiceFromAmtval = document.getElementById('<%=txtTotInvoiceFromAmt.ClientID%>').value;
            if (totInvoiceFromAmtval.length > 0) {
                if (charCode == 46)
                    if (totInvoiceFromAmtval.indexOf(".") > 0)
                    return false;
            }
        }
        else if (isInvoiceFromamt == false) {
        var totInvoiceToAmt = document.getElementById('<%=txtTotInvoiceToAmt.ClientID%>').value;

        if (totInvoiceToAmt.length > 0) {
                if (charCode == 46)
                    if (totInvoiceToAmt.indexOf(".") > 0)
                    return false;
            }
        }

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }    
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Medical Claim List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblInvoiceNo" runat="server" Text="Invoice No"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpStatus" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblProvider" runat="server" Text="Service Provider"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpProvider" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblFromTotInvoiceAmt" runat="server" Text="Total Invoice From Amount"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtTotInvoiceFromAmt" runat="server" Style="text-align: right" Text="0"
                                                    onKeypress="return onlyNumbers(true);" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblPayPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblToTotInvoiceAmt" runat="server" Text="Total Invoice To Amount"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtTotInvoiceToAmt" runat="server"  Style="text-align: right;"
                                                    Text="0" onKeypress="return onlyNumbers(false);" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="4">
                                                <asp:CheckBox ID="chkMyInvoiceOnly" runat="server" Text="My Invoice Only" Checked="true" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" CssClass="btndefault" runat="server" Text="New" />
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                        <asp:Button ID="BtnReset" CssClass="btndefault" runat="server" Text="Reset" />
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top:5px">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_gvMedicalClaim" ScrollBars="Auto" style="margin-top:5px; margin-bottom:10px" Height="400px" runat="server">
                                                <asp:DataGrid ID="gvMedicalClaim" runat="server" Style="margin: auto" AutoGenerateColumns="false"
                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Edit" CommandName="Select"
                                                                        CssClass="gridedit"></asp:LinkButton>
                                                                </span>
                                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit"
                                                        CommandName="Select" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                        CssClass="griddelete"></asp:LinkButton>
                                                                </span>
                                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Export" HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center" FooterText="btnExportStatus">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc" style="padding:2px">
                                                                    <asp:ImageButton ID="ImgExport" runat="server" ImageUrl="~/images/common-export.png"
                                                                        ToolTip="Export" CommandName="Export" />
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Cancel" HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Center" FooterText="btnCancelExport">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc" style="padding:2px">
                                                                    <asp:ImageButton ID="ImgCancelExport" runat="server" ImageUrl="~/images/CancelExport.png"
                                                                        ToolTip="Cancel Export" CommandName="Cancel Export" />
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="invoiceno" HeaderText="Invoice No" ReadOnly="True" FooterText="colhInvoiceNo" />
                                                        <asp:BoundColumn DataField="period_name" HeaderText="Pay Period" ReadOnly="True"
                                                            FooterText="colhPeriod" />
                                                        <asp:BoundColumn DataField="institute_name" HeaderText="Provider" ReadOnly="True"
                                                            FooterText="colhInstitute" />
                                                        <asp:BoundColumn DataField="invoice_amt" HeaderText="Invoice Amount" ReadOnly="True"
                                                            ItemStyle-HorizontalAlign="Right" FooterText="colhInvoiceAmt" />
                                                        <asp:BoundColumn DataField="status_name" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                                        <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" ReadOnly="True"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="isfinal" HeaderText="isfinal" ReadOnly="True" Visible="false" />
                                                    </Columns>
                                                    <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To delete?:" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

