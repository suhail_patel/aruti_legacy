﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_SickSheetList.aspx.vb"
    Inherits="Medical_wPg_SickSheetList" MasterPageFile="~/home.master" Title="Sick Sheet List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Sick Sheet List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblSickSheetNo" runat="server" Text="Sick Sheet No"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtSickSheet" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpJob" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblSickSheetDate" runat="server" Text="Sick Sheet Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 45%">
                                                            <uc2:DateCtrl ID="dtpFromdate" runat="server" AutoPostBack="false" />
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="LblToSickSheetDate" runat="server" Text="To"></asp:Label>
                                                        </td>
                                                        <td style="width: 45%">
                                                            <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblMedicalCover" runat="server" Text="Medical Cover"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpMedicalCover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblProvider" runat="server" Text="Provider"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpProvider" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnNew" CssClass="btndefault" runat="server" Text="New" />
                                        <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                        <asp:Button ID="BtnReset" CssClass="btndefault" runat="server" Text="Reset" />
                                    </div>
                                </div>
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_gvSickSheet" ScrollBars="Auto" style="margin-top:5px; margin-bottom:10px" Height="300px" runat="server">
                                                <asp:DataGrid ID="gvSickSheet" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="120%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Edit" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgSelect" ToolTip="Edit" CommandName="Select" runat="server"
                                                                        CssClass="gridedit"></asp:LinkButton>
                                                                </span>
                                                                <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit"
                                                                    CommandName="Select" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" ToolTip="Delete" CommandName="Delete" runat="server"
                                                                        CssClass="griddelete"></asp:LinkButton>
                                                                </span>
                                                                <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                                    CommandName="Delete" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Preview" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="mnuPreviewSickSheet">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="ImgPrint" runat="server" ImageUrl="~/images/PrintSummary_16.png"
                                                                    ToolTip="Print" CommandName="Print" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="sicksheetno" HeaderText="Sick Sheet No" ReadOnly="True"
                                                            HeaderStyle-HorizontalAlign="Left" FooterText="colhSickSheetNo" />
                                                        <asp:BoundColumn DataField="sicksheetdate" HeaderText="Sick Sheet Date" ReadOnly="True"
                                                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" FooterText="colhSicksheetDate" />
                                                        <asp:BoundColumn DataField="institute_name" HeaderText="Provider" ReadOnly="True"
                                                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" FooterText="colhProvider" />
                                                        <asp:BoundColumn DataField="employeename" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundColumn DataField="job" HeaderText="Job" ReadOnly="True" FooterText="colhJob"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundColumn DataField="cover" HeaderText="Medical Cover" ReadOnly="True" FooterText="colhMedicalCover"
                                                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
                                                        <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="True"
                                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" Visible="false" />
                                                    </Columns>
                                                   <%-- <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />--%>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <%--<cc1:ModalPopupExtender ID="popup1" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="BtnCancel" DropShadow="true" PopupControlID="pnlpopup" TargetControlID="txtreasondel">
                    </cc1:ModalPopupExtender>--%>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
<%--<asp:Panel ID="" CssClass="pnlcontentmain" Width="100%" Height="100%" runat="server"
    Style="margin: 0 auto;">
    <div>
       
        <tr>
            <td colspan="2">
                <asp:Panel ID="pnlpopup" Width="100%" Height="100%" runat="server" Style="display: none"
                    CssClass="modalPopup">
                    <asp:Panel ID="Panel2" runat="server" Style="cursor: move; background-color: #DDDDDD;
                        border: solid 1px Gray; color: Black">
                        <div>
                            <p>
                                Are you Sure You Want To delete?:</p>
                        </div>
                    </asp:Panel>
                    <table style="width: 180px;">
                        <tr>
                            <td>
                                Reason:
                            </td>
                            <td>
                                <asp:TextBox ID="txtreasondel" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="right">
                                <asp:Button ID="BtnDelete" runat="server" Text="Delete" />
                                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        </table>
    </div>
</asp:Panel>--%>