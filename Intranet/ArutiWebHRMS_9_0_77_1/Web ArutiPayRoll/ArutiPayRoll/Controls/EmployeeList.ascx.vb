﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Controls_EmployeeList
    Inherits System.Web.UI.UserControl

    Dim clsEmployee As New Aruti.Data.clsEmployee_Master

    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)


    Public Enum enentrymode
        Entry = 1
        Filter = 2

    End Enum

    Public Sub BindEmployee(Optional ByVal EmployeeunkId As Integer = 0)
        Try
            'Dim objglobalaccess As GlobalAccess = CType(Session("objGlobalAccess"), GlobalAccess)
            'ddlEmpList.DataSource = objglobalaccess.ListOfEmployee
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then


                'Pinkal (22-Mar-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                Dim blnApplyFilter As Boolean = True
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    blnApplyFilter = False
                End If
                'Pinkal (22-Mar-2016) -- End

     
                ddlEmpList.DataSource = clsEmployee.GetEmployeeList(Session("Database_Name"), _
                                                                    Session("UserId"), _
                                                                    Session("Fin_year"), _
                                                                    Session("CompanyUnkId"), _
                                                                    eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                    Session("UserAccessModeSetting"), True, _
                                                                    Session("IsIncludeInactiveEmp"), "Employee", _
                                                                    False, CInt(Session("Employeeunkid")), , , , , , , , , , , , , , , , blnApplyFilter)

            Else
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If EmployeeunkId > 0 Then
                '    ddlEmpList.DataSource = clsEmployee.GetEmployeeList("Employee", False, True, EmployeeunkId)
                'Else
                '    ddlEmpList.DataSource = clsEmployee.GetEmployeeList("Employee", True, True)
                'End If
                If CBool(Session("IsIncludeInactiveEmp")) = False Then

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'ddlEmpList.DataSource = clsEmployee.GetEmployeeList("Emp", True, , EmployeeunkId, , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                    ddlEmpList.DataSource = clsEmployee.GetEmployeeList(Session("Database_Name"), _
                                                                        Session("UserId"), _
                                                                        Session("Fin_year"), _
                                                                        Session("CompanyUnkId"), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                        Session("UserAccessModeSetting"), True, _
                                                                        Session("IsIncludeInactiveEmp"), "Emp", False, EmployeeunkId)
                    'Shani(20-Nov-2015) -- End

                Else

                    'Pinkal (5-MAY-2012) -- Start
                    'Enhancement : TRA Changes

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'ddlEmpList.DataSource = clsEmployee.GetEmployeeList("Emp", True, , EmployeeunkId, , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                    ddlEmpList.DataSource = clsEmployee.GetEmployeeList(Session("Database_Name"), _
                                                                        Session("UserId"), _
                                                                        Session("Fin_year"), _
                                                                        Session("CompanyUnkId"), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                        Session("UserAccessModeSetting"), True, _
                                                                        Session("IsIncludeInactiveEmp"), "Emp", False, EmployeeunkId)
                    'Shani(20-Nov-2015) -- End

                    'Pinkal (5-MAY-2012) -- End


            End If

                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            End If

            ddlEmpList.DataValueField = "employeeunkid"
            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'ddlEmpList.DataTextField = "employeename"
            ddlEmpList.DataTextField = "EmpCodeName"
            'Nilay (09-Aug-2016) -- End

            ddlEmpList.DataBind()
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    Dim mItem As New ListItem
            '    mItem.Text = "All"
            '    mItem.Value = "-1"
            '    ddlEmpList.Items.Insert(0, mItem)
            'End If
            ddlEmpList.SelectedIndex = 0
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("EmployeeList-->BindEmployee Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        If Not IsPostBack Then
            BindEmployee()
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateTable :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    Private pWidth As Integer
    Public Property width() As Integer
        Get
            Return pWidth
            Setwidth()
        End Get
        Set(ByVal value As Integer)
            pWidth = value
            Setwidth()
        End Set
    End Property
    Private _mode As Integer
    Public Property Mode() As Integer
        Get
            Return _mode

        End Get
        Set(ByVal value As Integer)
            _mode = value

        End Set
    End Property

    Public Property Enabaled() As Boolean
        Get
            Return ddlEmpList.Enabled
        End Get
        Set(ByVal value As Boolean)
            ddlEmpList.Enabled = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlEmpList.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            ddlEmpList.AutoPostBack = value
        End Set
    End Property

    Public Sub Setwidth()
        ddlEmpList.Width = pWidth - 20

    End Sub
    Public Property SelectedValue() As String
        Get
            Return ddlEmpList.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlEmpList.SelectedValue = value
        End Set
    End Property
    Public Property SelectedText() As String
        Get
            Return ddlEmpList.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            ddlEmpList.SelectedItem.Text = value
        End Set
    End Property

    Public Property SelectedIndex() As Integer
        Get
            Return ddlEmpList.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            ddlEmpList.SelectedIndex = value
        End Set
    End Property


    'Sohail (22 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Protected Sub ddlEmpList_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpList.DataBound
        Try
        If ddlEmpList.Items.Count > 0 Then
            For Each lstItem As ListItem In ddlEmpList.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            ddlEmpList.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (22 Dec 2012) -- End

    Protected Sub ddlEmpList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmpList.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub
End Class
