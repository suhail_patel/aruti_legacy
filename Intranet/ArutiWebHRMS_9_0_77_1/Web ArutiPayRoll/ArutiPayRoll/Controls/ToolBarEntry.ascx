﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ToolBarEntry.ascx.vb" Inherits="Controls_ToolBarEntry" %>
<div id="menustrip"    style="width:98%;  vertical-align:  top; top :0px; left  :0px;"  >
    
        <div style="float:left; height:20px;">
            <asp:ImageButton ID="imgNew"  ToolTip="New" SkinID="btnadd" runat="server"  />
            <asp:ImageButton ID="imgSave"  SkinID="btnsave" runat="server" AccessKey="s" 
            ToolTip="alt + s for saving " />
            <asp:ImageButton ID="imgCancel" ToolTip="Cancel" SkinID="btncancel" 
            runat="server"  />
            <asp:ImageButton ID="imgSep1"  SkinID="btnsep" runat="server"  />
            <asp:ImageButton ID="imgDel" ToolTip="Delete" runat="server" SkinID="btndel" >
            </asp:ImageButton>
            <asp:ImageButton ID="imgSep2"  ToolTip="New" runat="server" SkinID="btnsep"  >
            </asp:ImageButton>
            <asp:ImageButton ID="imgFirst" ToolTip="First" runat="server" 
                SkinID="btnfirst"  >
            </asp:ImageButton>
            <asp:ImageButton ID="imgPrev" ToolTip="Previous" runat="server" 
            SkinID="btnprev" >
            </asp:ImageButton>
            <asp:ImageButton ID="imgNext" ToolTip="Next" runat="server" SkinID="btnnext"  >
            </asp:ImageButton>
            <asp:ImageButton ID="imgLast" ToolTip="Last" runat="server" SkinID="btnlast" >
            </asp:ImageButton>
            <asp:ImageButton ID="imgSep3"  runat="server" SkinID="btnsep">
            </asp:ImageButton>
        </div>
        <div style="width:20px;float:left;position:relative; height:20px;">
            &nbsp;</div>
        <div style="float:left;position:relative; height:20px;">
            <asp:ImageButton ID="imgModeForm"  ToolTip="Form Mode" runat="server" 
            SkinID="btnmodeform"  >
            </asp:ImageButton>
            <asp:ImageButton ID="imgModeGrid" ToolTip="Grid Mode" runat="server" 
            SkinID="btnmodegrid"  >
            </asp:ImageButton>
            <asp:ImageButton ID="imgSep4" runat="server" SkinID="btnsep"  >
            </asp:ImageButton>
        </div>
        <div style="width:20px;float:left;position:relative; height:20px;">
            &nbsp;</div>

    
</div>
