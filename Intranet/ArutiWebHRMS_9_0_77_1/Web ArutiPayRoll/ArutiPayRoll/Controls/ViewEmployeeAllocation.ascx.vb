﻿Imports System.Data
Imports Aruti.Data

Partial Class Controls_ViewEmployeeAllocation
    Inherits System.Web.UI.UserControl

#Region "Private Varible"
    Private mintEmployeeId As Integer
    Private DisplayMessage As New CommonCodes
    Dim objEmp As New clsEmployee_Master
    Private ReadOnly mstrModuleName As String = "ctrlViewEmployeeAllocation"

#End Region

#Region "Public Property(S)"
    Public Property _EmployeeId() As Integer
        Get
            Return mintEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeId = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            SetLanguage()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub

#Region "Private Method(S)"

    Private Sub SetInfo()
        Try

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintEmployeeId
            Dim dslist As DataSet = Nothing


            dslist = objEmp.GetList(Session("Database_Name").ToString(), _
                                    CInt(Session("UserId")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                    CStr(IIf(mintEmployeeId <= 0, Session("UserAccessModeSetting").ToString(), "0")), True, _
                                    True, "EmployeeDetail", CBool(Session("ShowFirstAppointmentDate")), mintEmployeeId, False, "", False, True)


            objlblEmployee.Text = objEmp._Employeecode + " - " + objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname


            If IsNothing(dslist) = False Then

                If dslist.Tables("EmployeeDetail").Rows(0)("station").ToString().Trim.ToString() <> "" Then
                    objlblBranch.Text = dslist.Tables("EmployeeDetail").Rows(0)("station").ToString().Trim.ToString()
                Else
                    objlblBranch.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("jobgroup").ToString().Trim.ToString() <> "" Then
                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'objlblJobGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("").ToString()
                    objlblJobGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("jobgroup").ToString()
                    'Pinkal (07-Feb-2020) -- End
                Else
                    objlblJobGroup.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("deptgroup").ToString().Trim.ToString() <> "" Then
                    objlblDepartmentGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("deptgroup").ToString()
                Else
                    objlblDepartmentGroup.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("job_name").ToString().Trim.ToString() <> "" Then
                    objlblJob.Text = dslist.Tables("EmployeeDetail").Rows(0)("job_name").ToString()
                Else
                    objlblJob.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("DeptName").ToString().Trim.ToString() <> "" Then
                    objlblDepartment.Text = dslist.Tables("EmployeeDetail").Rows(0)("DeptName").ToString()
                Else
                    objlblDepartment.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("scale").ToString().Trim.ToString() <> "" Then
                    objlblScale.Text = Format(CDec(dslist.Tables("EmployeeDetail").Rows(0)("scale").ToString()), Session("fmtCurrency"))
                Else
                    objlblScale.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("sectiongroup").ToString().Trim.ToString() <> "" Then
                    objlblSectionGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("sectiongroup").ToString()
                Else
                    objlblSectionGroup.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("classgroup").ToString().Trim.ToString() <> "" Then
                    objlblClassGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("classgroup").ToString()
                Else
                    objlblClassGroup.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("section").ToString().Trim.ToString() <> "" Then
                    objlblSection.Text = dslist.Tables("EmployeeDetail").Rows(0)("section").ToString()
                Else
                    objlblSection.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("class").ToString().Trim.ToString() <> "" Then
                    objlblClass.Text = dslist.Tables("EmployeeDetail").Rows(0)("class").ToString()
                Else
                    objlblClass.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("unitgroup").ToString().Trim.ToString() <> "" Then
                    objlblUnitGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("unitgroup").ToString()
                Else
                    objlblUnitGroup.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("gradegroup").ToString().Trim.ToString() <> "" Then
                    objlblGradeGroup.Text = dslist.Tables("EmployeeDetail").Rows(0)("gradegroup").ToString()
                Else
                    objlblGradeGroup.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("unit").ToString().Trim.ToString() <> "" Then
                    objlblUnits.Text = dslist.Tables("EmployeeDetail").Rows(0)("unit").ToString()
                Else
                    objlblUnits.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("grade").ToString().Trim.ToString() <> "" Then
                    objlblGrade.Text = dslist.Tables("EmployeeDetail").Rows(0)("grade").ToString()
                Else
                    objlblGrade.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("team").ToString().Trim.ToString() <> "" Then
                    objlblTeam.Text = dslist.Tables("EmployeeDetail").Rows(0)("team").ToString()
                Else
                    objlblTeam.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("gradelevel").ToString().Trim.ToString() <> "" Then
                    objlblGradeLevel.Text = dslist.Tables("EmployeeDetail").Rows(0)("gradelevel").ToString()
                Else
                    objlblGradeLevel.Text = "-"
                End If

                If dslist.Tables("EmployeeDetail").Rows(0)("costcenter").ToString().Trim.ToString() <> "" Then
                    objlblCostCenter.Text = dslist.Tables("EmployeeDetail").Rows(0)("costcenter").ToString()
                Else
                    objlblCostCenter.Text = "-"
                End If


                If CBool(Session("AllowViewEmployeeScale")) = False Then
                    lblScale.Visible = False
                    objlblScale.Visible = False
                End If

            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Public Sub Show()
        Try
            popup_ViewEmployeeAllocation.Show()
            SetInfo()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.lblTitle.Text = Language._Object.getCaption(lblTitle.ID, lblTitle.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(mstrModuleName, lblEmployee.Text)
            Me.lblBranch.Text = Language._Object.getMessage("clsMasterData", 430, lblBranch.Text)
            Me.lblJobGroup.Text = Language._Object.getMessage("clsMasterData", 422, lblJobGroup.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getMessage("clsMasterData", 429, lblDepartmentGroup.Text)
            Me.lblDepartment.Text = Language._Object.getMessage("clsMasterData", 428, lblDepartment.Text)
            Me.lblSectionGroup.Text = Language._Object.getMessage("clsMasterData", 427, lblSectionGroup.Text)
            Me.lblSection.Text = Language._Object.getMessage("clsMasterData", 426, lblSection.Text)
            Me.lblClassGroup.Text = Language._Object.getMessage("clsMasterData", 420, lblClassGroup.Text)
            Me.lblClass.Text = Language._Object.getMessage("clsMasterData", 419, lblClass.Text)
            Me.lblUnitGroup.Text = Language._Object.getMessage("clsMasterData", 425, lblUnitGroup.Text)
            Me.lblUnits.Text = Language._Object.getMessage("clsMasterData", 424, lblUnits.Text)
            Me.lblJob.Text = Language._Object.getMessage("clsMasterData", 421, lblJob.Text)
            Me.lblCostCenter.Text = Language._Object.getMessage("clsMasterData", 586, lblCostCenter.Text)
            Me.lblTeam.Text = Language._Object.getMessage("clsMasterData", 423, lblTeam.Text)

            Me.lblGradeGroup.Text = Language._Object.getCaption(lblGradeGroup.ID, lblGradeGroup.Text)
            Me.lblGrade.Text = Language._Object.getCaption(lblGrade.ID, lblGrade.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(lblGradeLevel.ID, lblGradeLevel.Text)
            Me.lblScale.Text = Language._Object.getCaption(lblScale.ID, lblScale.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch Ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(Ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub


End Class
