﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CashDenomination.ascx.vb"
    Inherits="Controls_CashDenomination" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<script type="text/jscript">
    //$(document).ready(function() {
    //prm = Sys.WebForms.PageRequestManager.getInstance();
    //prm.add_endRequest(NumberOnly);

    //function NumberOnly() {
    $(".CashGridCell").live("keypress", function(e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    //}
    //});
</script>

<script type="text/jscript">
    $(".CashGridCell").live("keyup", function() {
        var total = 0;
        var subtotal = 0;
        $(this).parent().parent().children("td").each(function(i) {
            if ($(this).children().length > 0) {
                var currval = $(this).children().attr("value");
                var subtotal = $(this).children().attr("denomevalue");
                if (currval.length > 0) {
                    total += (parseFloat(subtotal) * parseFloat(currval));
                }
            }
        });

        var netSal = parseFloat($(this).parent().parent().children("td:nth-child(2)").attr("NetAmount"));
//        total = total.toFixed(2);
        if (parseFloat(total)<= parseFloat(netSal)) {

            if (parseFloat(total.toFixed(2)) == parseFloat(netSal.toFixed(2))) {
                $(this).parent().parent().children("td:nth-child(1)").css("background-color", "green");
                $(this).parent().parent().children("td:nth-child(2)").css("background-color", "green");
                $(this).parent().parent().children("td:nth-child(1)").css("color", "white");
                $(this).parent().parent().children("td:nth-child(2)").css("color", "white");
            }
            else {
                $(this).parent().parent().children("td:nth-child(1)").css("background-color", "red");
                $(this).parent().parent().children("td:nth-child(2)").css("background-color", "red");
                $(this).parent().parent().children("td:nth-child(1)").css("color", "white");
                $(this).parent().parent().children("td:nth-child(2)").css("color", "white");
            }

            $(this).parent().parent().children("td:nth-child(3)").html(currencyFormat(total));
            $(this).parent().parent().children("td:nth-child(3)").attr("NetAmount", total);
        }
        else {

            $(this).val("0");
            total = 0;
            $(this).parent().parent().children("td").each(function(i) {
                if ($(this).children().length > 0) {
                    var currval = $(this).children().attr("value");
                    var subtotal = $(this).children().attr("denomevalue");
                    if (currval.length > 0) {
                        total += (parseFloat(subtotal) * parseFloat(currval));
                    }
                }
            });

            $(this).parent().parent().children("td:nth-child(3)").html(currencyFormat(total));
            $(this).parent().parent().children("td:nth-child(3)").attr("NetAmount", total.toFixed(2));

            var msg = "Denomination amount cannot be greater than Net Salary.";
            return alert(msg);
        }

    });

    function currencyFormat(num) {
        return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
    }

    $("#<%= btnSave.ClientID %>").live("click", function() {
        var blnValid = true;
        $("#<%= gvCashDenom.ClientID %>").children().children(".griviewitem").each(function(index) {
        var amount = parseFloat($(this).children("td:nth-child(2)").attr("NetAmount")).toFixed(2);
            var total = parseFloat($(this).children("td:nth-child(3)").attr("NetAmount")).toFixed(2);
                        
            if (amount == total) {
                $(this).children("td:nth-child(1)").css("background-color", "green");
                $(this).children("td:nth-child(2)").css("background-color", "green");
                $(this).children("td:nth-child(1)").css("color", "white");
                $(this).children("td:nth-child(2)").css("color", "white");
            } else {
                blnValid = false;
                $(this).children("td:nth-child(1)").css("background-color", "red");
                $(this).children("td:nth-child(2)").css("background-color", "red");
                $(this).children("td:nth-child(1)").css("color", "white");
                $(this).children("td:nth-child(2)").css("color", "white");
            }
        });

        if (blnValid == false) {
            alert("Cash Denomination cannot be less than Net Salary. Record(s) marked in Red.");
        }
        else {
            $("#<%= gvCashDenom.ClientID %>").children().children(".griviewitem").each(function(index) {
                $(this).children("td").children("input").each(function(colIndex) {
                    PageMethods.SetCashDenimination(index, $(this).attr("ColumnName"), $(this).attr("value"));
                });

                var totalamt = parseFloat($(this).children("td:nth-child(3)").attr("NetAmount"));
                PageMethods.SetCashDenimination(index, "total", totalamt);
            });

        }
        return blnValid;
    });
</script>

<%--<script type="text/javascript">
    function addCommas(nStr) {

        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;

    }
</script>--%>
<ajaxToolkit:ModalPopupExtender ID="popupCashDenomination" runat="server" BackgroundCssClass="ModalPopupBG"
    CancelControlID="btnCancel" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none;"
    Width="75%">
    <div class="panel-primary" style="margin: -1px">
        <div class="panel-heading">
            <asp:Label ID="lblpopupHeader" runat="server" Text="Cash Denomination"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaTitle" class="panel-heading-default">
                    <div style="float: left;">
                        <asp:Label ID="lblTitle" runat="server" Text="Employee Cash Denomination"></asp:Label>
                    </div>
                </div>
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <div id="divGrid" style="overflow: auto; max-height: 300px;">
                        <asp:GridView ID="gvCashDenom" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                            RowStyle-CssClass="griviewitem" AllowPaging="false" AutoGenerateColumns="False"
                            ShowFooter="False" Width="120%" DataKeyNames="employeeunkid">
                            <Columns>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="btn-default">
                        <div style="float: left">
                            <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                        </div>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" ValidationGroup="Reason" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btndefault" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
