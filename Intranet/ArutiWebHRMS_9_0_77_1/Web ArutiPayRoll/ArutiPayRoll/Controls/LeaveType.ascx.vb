﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Controls_LeaveType
    Inherits System.Web.UI.UserControl

    Dim clsleavetype As New Aruti.Data.clsleavetype_master

    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Enum enentrymode
        Entry = 1
        Filter = 2
    End Enum
    Public Sub BindLeaveType()
        Try
            Dim ds As New DataSet
            Dim clsuser As New User
            '  Dim objDataOperation As New eZeeCommonLib.clsDataOperation
            '  msql = "select leavetypeunkid,leavename from " & Session("mdbname") & "..lvleavetype_master"
            'ds = objDataOperation.WExecQuery(msql, "lvleavetype_master")

            'Sohail (01 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            'ds = clsleavetype.getListForCombo("LeaveType", True)
            ds = clsleavetype.getListForCombo("LeaveType", True, , Session("mdbname"))
            'Sohail (01 Jan 2013) -- End
            If ds.Tables(0).Rows.Count > 0 Then
                ddlLeave.DataSource = ds.Tables(0)
                ddlLeave.DataValueField = "leavetypeunkid"
                ddlLeave.DataTextField = "name"
                ddlLeave.DataBind()
                ddlLeave.SelectedIndex = 0

                'Dim mItem As New ListItem
                'If (Me.Mode = enentrymode.Filter) Then
                '    mItem.Text = "All"
                '    mItem.Value = "-1"
                'Else
                '    mItem.Text = "Select"
                '    mItem.Value = "-1"
                'End If

                'ddlLeave.Items.Insert(0, mItem)

                ddlLeave.SelectedIndex = -1
            End If
        Catch ex As Exception
            Throw New Exception("LeaveType-->BindLeaveType Method!!!" & ex.Message.ToString)
        Finally
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        'Sohail (01 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If
        'Sohail (01 Jan 2013) -- End

        If Not IsPostBack Then
            BindLeaveType()
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    Private pWidth As Integer
    Public Property width() As Integer
        Get
            Return pWidth
            Setwidth()
        End Get
        Set(ByVal value As Integer)
            pWidth = value
            Setwidth()
        End Set
    End Property
    Private _mode As Integer
    Public Property Mode() As Integer
        Get
            Return _mode

        End Get
        Set(ByVal value As Integer)
            _mode = value

        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlLeave.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            ddlLeave.AutoPostBack = value
        End Set
    End Property

    Public Sub Setwidth()
        ddlLeave.Width = pWidth - 20

    End Sub
    Public Property SelectedValue() As String
        Get
            Return ddlLeave.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlLeave.SelectedValue = value
        End Set
    End Property
    Public Property SelectedText() As String
        Get
            Return ddlLeave.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            ddlLeave.SelectedItem.Text = value
        End Set
    End Property

    Public Property SelectedIndex() As Integer
        Get
            Return ddlLeave.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            ddlLeave.SelectedIndex = value
        End Set
    End Property

    Protected Sub ddlLeave_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeave.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub

    'Sohail (22 Dec 2012) -- Start
    'TRA - ENHANCEMENT
    Protected Sub ddlLeave_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeave.DataBound
        Try
        If ddlLeave.Items.Count > 0 Then
            For Each lstItem As ListItem In ddlLeave.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            ddlLeave.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("ddlLeave_DataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (22 Dec 2012) -- End
    'Pinkal (18-Jun-2015) -- Start
    'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
    Public Property Enabaled() As Boolean
        Get
            Return ddlLeave.Enabled
        End Get
        Set(ByVal value As Boolean)
            ddlLeave.Enabled = value
        End Set
    End Property
    'Pinkal (18-Jun-2015) -- End
End Class

