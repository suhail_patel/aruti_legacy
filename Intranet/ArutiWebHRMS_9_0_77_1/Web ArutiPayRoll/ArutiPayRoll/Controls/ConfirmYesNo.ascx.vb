﻿
Partial Class Controls_ConfirmYesNo
    Inherits System.Web.UI.UserControl

    Public Event buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)


    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.

    Public Property Title() As String
        Get
            Try
            Return lblTitle.Text
            Catch ex As Exception
                Throw New Exception("Title Get : " & ex.Message)
            End Try
        End Get
        Set(ByVal value As String)
            Try
            lblTitle.Text = value
            Catch ex As Exception
                Throw New Exception("Title Set : " & ex.Message)
            End Try
        End Set
    End Property

    Public Property Message() As String
        Get
            Try
            Return lblMessage.Text
            Catch ex As Exception
                Throw New Exception("Message Get : " & ex.Message)
            End Try
        End Get
        Set(ByVal value As String)
            Try
            lblMessage.Text = value
            Catch ex As Exception
                Throw New Exception("Message Set : " & ex.Message)
            End Try
        End Set
    End Property

    'Sohail (19 Dec 2018) -- Start
    'NMB Enhancement - 76.1 - Security issues in self service.
    Public Property ValidationGroup() As String
        Get
            Try
            Return btnYes.ValidationGroup
            Catch ex As Exception
                Throw New Exception("ValidationGroup Get : " & ex.Message)
            End Try
        End Get
        Set(ByVal value As String)
            Try
            btnYes.ValidationGroup = value
            Catch ex As Exception
                Throw New Exception("ValidationGroup Set : " & ex.Message)
            End Try
        End Set
    End Property
    'Sohail (19 Dec 2018) -- End

    'Pinkal (20-Nov-2018) -- Start
    'Enhancement - Working on P2P Integration for NMB.
    Public WriteOnly Property IsFireButtonNoClick() As Boolean
        Set(ByVal value As Boolean)
            Try
            If value Then
                ModalPopupExtender1.CancelControlID = HiddenField1.ID
            End If
            Catch ex As Exception
                Throw New Exception("IsFireButtonNoClick Set : " & ex.Message)
            End Try
        End Set
    End Property
    'Pinkal (20-Nov-2018) -- End


    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
        RaiseEvent buttonYes_Click(sender, e)
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnYes_Click  : " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Try
        RaiseEvent buttonNo_Click(sender, e)
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnNo_Click  : " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Show()
        Try
        ModalPopupExtender1.Show()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show  : " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        ModalPopupExtender1.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide  : " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    'Pinkal (13-Aug-2020) -- End

End Class
