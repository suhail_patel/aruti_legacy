﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

Partial Class Controls_LeftMenus
    Inherits System.Web.UI.UserControl
    'Dim clsAuthorize As New HRMDAL.Authorize, 
    Dim mformtype As String
    'Dim clsEmp As New HRMDAL.empmaster
    'Dim i As Integer, j As Integer

    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Private DisplayMessage As New CommonCodes
    'S.SANDEEP [ 05 NOV 2014 ] -- END


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Sohail (01 Jan 2013) -- Start
        'TRA - ENHANCEMENT
        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If
        'Sohail (01 Jan 2013) -- End

        ' Aruti.Data.ArtLic._Object = New ArutiLic(False)
        If (Session("LoginBy") = Global.User.en_loginby.User) Then
            pnlAssessmentUserwise.Visible = True
            pnlAssessmentEmpWise.Visible = False
            pnlTraining.Visible = False
            pnlSalaryChange.Visible = True 'Sohail (24 Dec 2013)
            'pnlPayslip.Visible = False 'Sohail (22 May 2012)
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            'pnlPayrollReport.Visible = False
            'pnlEdDetailReport.Visible = False
            'Sohail (25 Mar 2013) -- End


            MyAccordion.Panes("AccordionPane8").Visible = False 'Asset Declaration
            MyAccordion.Panes("AccordionPane9").Visible = False


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            pnlLeaveApprover.Visible = True
            'Pinkal (18-Dec-2012) -- End



            'Pinkal (24-Jan-2013) -- Start
            'Enhancement : TRA Changes
            LinkProcesspendleave.Text = "Leave Approval"
            'Pinkal (24-Jan-2013) -- End

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            pnlPayrollMSSReport.Visible = True
            pnlPayrollTransactions.Visible = True
            If CBool(Session("SetPayslipPaymentApproval")) = True Then
                pnlPaymentApproval.Visible = True
            Else
                pnlPaymentApproval.Visible = False
            End If
            'Sohail (18 May 2013) -- End

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            pnlLoan_Advance_List.Visible = True
            'S.SANDEEP [ 29 May 2013 ] -- END

           'Pinkal (03-Jun-2013) -- Start
            'Enhancement : TRA Changes
            PnlEmployeeSavings.Visible = True
            'Pinkal (03-Jun-2013) -- End


            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            MyAccordion.Panes("AccordionPane10").Visible = CBool(Session("PolicyManagementTNA"))
            'Pinkal (01-Feb-2014) -- End

            'Sohail (15 Jul 2014) -- Start
            'Enhancement - Consider Max/Min Level as per Last Approver Mapped and NoOfPosition on Staff Requisition.
            'pnlStaffRequisition.Visible = True 'Sohail (28 May 2014)
            If CBool(Session("ApplyStaffRequisition")) = True Then
                pnlStaffRequisition.Visible = True
            Else
                pnlStaffRequisition.Visible = False
            End If
            'Sohail (15 Jul 2014) -- End

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            lnkMyprmcGoals.Text = "Employee Goals"
            lnkMyprmcGoals.ToolTip = lnkMyprmcGoals.Text
            pnlSelf.Visible = False
            pnlAssessUser.Visible = True
            lnkReviewEmployeeAssessment.Visible = CBool(Session("IsCompanyNeedReviewer"))
            'S.SANDEEP [ 05 NOV 2014 ] -- END

            'S.SANDEEP [29 JAN 2015] -- START
            lnkAssignCompetencies.Text = "Employee Competencies"
            'S.SANDEEP [29 JAN 2015] -- END

            'S.SANDEEP [12 FEB 2015] -- START
            lnkViewPlanning.Text = "Approve/Reject Goals"
            lnkViewPlanning.ToolTip = "Approve/Reject Goals"
            'S.SANDEEP [12 FEB 2015] -- END

            'Pinkal (16-Dec-2014) -- Start
            'Enhancement - Claim & Request For Web.
            pnlClaimApproverTran.Visible = True
            pnlClaimPosing.Visible = True
            'Pinkal (16-Dec-2014) -- End

            GetReportPriviliage()
        Else

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            lnkMyprmcGoals.Text = "My Goals"
            lnkMyprmcGoals.ToolTip = lnkMyprmcGoals.Text
            pnlSelf.Visible = True
            pnlAssessUser.Visible = False
            'S.SANDEEP [ 05 NOV 2014 ] -- END

            'S.SANDEEP [12 FEB 2015] -- START
            lnkViewPlanning.Text = "Goals Approval"
            lnkViewPlanning.ToolTip = "Goals Approval"
            'S.SANDEEP [12 FEB 2015] -- END

            MyAccordion.Panes("AccordionPane4").Visible = False     'Medical
            pnlAssessmentUserwise.Visible = False
            pnlAssessmentEmpWise.Visible = True
            pnlTraining.Visible = True
            pnlSalaryChange.Visible = False 'Sohail (24 Dec 2013)
            pnl_5.Visible = True 'Sohail (22 May 2012)'pnlPayslip.Visible = True 'Sohail (22 May 2012)
            'Sohail (25 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If CBool(Session("AllowToViewPersonalSalaryCalculationReport")) = True Then
                pnl_1.Visible = True '  pnlPayrollReport.Visible = True
            Else
                pnl_1.Visible = False 'pnlPayrollReport.Visible = False
            End If
            If CBool(Session("AllowToViewEDDetailReport")) = True Then
                pnl_60.Visible = True 'pnlEdDetailReport.Visible = True
            Else
                pnl_60.Visible = False 'pnlEdDetailReport.Visible = False
            End If
            'Sohail (25 Mar 2013) -- End

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            pnlPayrollMSSReport.Visible = False
            pnlPayrollTransactions.Visible = False
            'S.SANDEEP [ 28 NOV 2013 ] -- START
            'ReportAccordion.Panes("AccordionPane52").Visible = False
            ReportAccordion.Panes("AccordionPane52").Visible = True
            For Each iCtrl As Control In ReportAccordion.Panes("AccordionPane52").ContentContainer.Controls
                If TypeOf iCtrl Is Panel AndAlso iCtrl.ID = "pnl_67" Then
                    iCtrl.Visible = True
                Else
                    iCtrl.Visible = False
                End If
            Next
            'S.SANDEEP [ 28 NOV 2013 ] -- END
            ReportAccordion.Panes("AccordionPane53").Visible = False

            'Sohail (18 May 2013) -- End


            MyAccordion.Panes("AccordionPane9").Visible = False

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            pnlLeaveApprover.Visible = False
            'Pinkal (18-Dec-2012) -- End

            'Pinkal (24-Jan-2013) -- Start
            'Enhancement : TRA Changes
            LinkProcesspendleave.Text = "Leave Approval Status"
            'Pinkal (24-Jan-2013) -- End

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            pnlLoan_Advance_List.Visible = False
            'S.SANDEEP [ 29 May 2013 ] -- END

            'Pinkal (03-Jun-2013) -- Start
            'Enhancement : TRA Changes
            pnlEmployeeSavings.Visible = False
            'Pinkal (03-Jun-2013) -- End

            'S.SANDEEP [ 17 DEC 2013 ] -- START 
            MyAccordion.Panes("AccordionPane10").Visible = False
            'S.SANDEEP [ 17 DEC 2013 ] -- END


            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'Enhancement : Oman Changes
            MyAccordion.Panes("AccordionPane11").Visible = False
            'S.SANDEEP [ 31 DEC 2013 ] -- END

            pnlStaffRequisition.Visible = False 'Sohail (28 May 2014)


            'Shani [ 28 OCT 2014 ] -- START
            'Implementing "Leave Approver Report" under Web In Leave Report Secation
            pnl_105.Visible = False
            pnl_101.Visible = False
            pnl_12.Visible = False
            'Shani [ 28 OCT 2014 ] -- END



            'Pinkal (16-Dec-2014) -- Start
            'Enhancement - Claim & Request For Web.
            pnlClaimApproverTran.Visible = False
            pnlClaimPosing.Visible = False
            'Pinkal (16-Dec-2014) -- End

            'S.SANDEEP [29 JAN 2015] -- START
            lnkAssignCompetencies.Text = "My Competencies"
            'S.SANDEEP [29 JAN 2015] -- END
        End If


        'Pinkal (18-Dec-2012) -- Start
        'Enhancement : TRA Changes
        pnlLeaveIssueUser.Visible = CBool(Session("AllowToAssignIssueUsertoEmp"))
        pnlMigrateApprovers.Visible = CBool(Session("AllowtoMigrateLeaveApprovers"))
        'Pinkal (18-Dec-2012) -- End


        'Sohail (21 Mar 2012) -- Start
        'TRA - ENHANCEMENT
        If IsPostBack = True Then
            Session("ActivePane") = MyAccordion.SelectedIndex
            Session("ActivePaneReport") = ReportAccordion.SelectedIndex
            If MyAccordion.SelectedIndex = -1 AndAlso ReportAccordion.SelectedIndex = -1 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "load", "LoadImage('" & Session("imgAcc").ToString & "',true);", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "load", "LoadImage('" & Session("imgAcc").ToString & "',false);", True)
            End If
        Else

            If Not Page.ClientScript.IsStartupScriptRegistered("load") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "load", "LoadImage();", True)
            End If

            If Session("ActivePane") IsNot Nothing AndAlso Session("ActivePaneReport") IsNot Nothing Then
                If CInt(Session("menuView")) = 0 Then 'Main View
                    MyAccordion.SelectedIndex = Session("ActivePane")
                ElseIf CInt(Session("menuView")) = 1 Then 'Report View
                    ReportAccordion.SelectedIndex = Session("ActivePaneReport")
                End If
            End If

            'Pinkal (30-Apr-2013) -- Start
            'Enhancement : TRA Changes

            'Sohail (01 Jan 2013) -- Start
            'TRA - ENHANCEMENT

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ProcessPendingLeave.aspx?") = True AndAlso (Request.UrlReferrer Is Nothing OrElse Request.UrlReferrer.ToString().Length <= 0) Then
            'MyAccordion.Visible = False
            'End If
            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ProcessPendingLeave.aspx?") = True AndAlso (Request.UrlReferrer Is Nothing OrElse Request.UrlReferrer.ToString().Length <= 0) Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If


            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : TRA Changes Leave Enhancement
            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ApproveDisapproveStaffRequisition.aspx?") = True AndAlso (Request.UrlReferrer Is Nothing OrElse Request.UrlReferrer.ToString().Length <= 0) Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If
            'Pinkal (19-Jun-2014) -- End


            'S.SANDEEP [ 14 FEB 2014 ] -- START
            'If Session("iQueryString") <> "" Then
            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgEmployeeProfile.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If

            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgInterviewAnalysisList.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If

            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ApplicantFilterList.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If

            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_FinalApplicant.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If

            'S.SANDEEP [ 17 DEC 2014 ] -- START
            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgAssessorEvaluation.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If

            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgReviewerEvaluation.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If

            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ViewPerfEvaluation.aspx?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If
            'S.SANDEEP [ 17 DEC 2014 ] -- END

            'S.SANDEEP [29 JAN 2015] -- START
            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("frmViewPerformancePlanning?") = True Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If
            'S.SANDEEP [29 JAN 2015] -- END



            'S.SANDEEP [ 14 FEB 2014 ] -- END

            'S.SANDEEP [ 31 DEC 2013 ] -- END

            'Sohail (01 Jan 2013) -- End

            'Pinkal (30-Apr-2013) -- End

            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("Paymentapprovedisapprove.aspx?") = True AndAlso (Request.UrlReferrer Is Nothing OrElse Request.UrlReferrer.ToString().Length <= 0) Then
                MyAccordion.Visible = False
                ReportAccordion.Visible = False
            End If
            'Sohail (17 Dec 2014) -- End

        End If
        'Sohail (21 Mar 2012) -- End

        'S.SANDEEP [ 05 NOV 2014 ] -- START
        If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
            plnGoals.Visible = False
        End If
        Call Generate_Custom_Header_Menu()
        'S.SANDEEP [ 05 NOV 2014 ] -- END

        'S.SANDEEP [29 JAN 2015] -- START
        pnlSelfCompetencies.Visible = Session("Self_Assign_Competencies")
        'S.SANDEEP [29 JAN 2015] -- END

    End Sub

    Protected Sub LinkProcesspendleave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkProcesspendleave.Click
        ' Session("formname") = "Process Pending Leave"
        Response.Redirect(Session("servername") & "~/Leave/ProcessPendingLeave.aspx", False)
    End Sub

    'Protected Sub lnkbtnprof_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnprof.Click
    '    ' Session("formname") = "Home page"
    '    Response.Redirect(Session("servername") & "UserHome.aspx", False)
    'End Sub

    Protected Sub lnkbtnLeaveApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbtnLeaveApp.Click
        ' Session("formname") = "LeaveApplicationForm"
        Response.Redirect(Session("servername") & "~/Leave/LeaveApplication.aspx", False)
    End Sub

    'Protected Sub LinkbtJobApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkbtJobApp.Click
    '    '' Session("formname") = "JobApplicationForm"
    '    Response.Redirect(Session("servername") & "JobApplicationForm.aspx", False)
    'End Sub

    Protected Sub Linkbtplanner_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbtplanner.Click
        ' Session("formname") = "LeavePlanner"
        Response.Redirect(Session("servername") & "~/Leave/LeavePlanner.aspx", False)
    End Sub

    Protected Sub LinkleavePlannerViwer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkleavePlannerViwer.Click
        ' Session("formname") = "LeavePlannerViewer"
        Response.Redirect(Session("servername") & "~/Leave/LeavePlannerViewer.aspx", False)
    End Sub

    Protected Sub LinkLeaveViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkLeaveViewer.Click
        Response.Redirect(Session("servername") & "~/Leave/LeaveViewer.aspx", False)
    End Sub

    'Protected Sub LinkAddEditAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkAssess.Click
    '    Response.Redirect(Session("servername") & "~/Assessment/AddEditAssess.aspx", False)
    'End Sub

    Protected Sub LnkSickSheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LnkSickSheet.Click
        Response.Redirect(Session("servername") & "~/Medical/wPg_SickSheetList.aspx", False)
    End Sub

    Protected Sub LinkMedicalClaim_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkMedicalClaim.Click
        Response.Redirect(Session("servername") & "~/Medical/wPg_MedicalClaimList.aspx", False)
    End Sub

    Protected Sub lnkChangePassword_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChangePassword.Click
        Response.Redirect(Session("servername") & "~/Others_Forms/ChangePassword.aspx", False)
    End Sub

    Protected Sub lnkLoanAdvance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLoanAdvance_Application.Click
        Response.Redirect(Session("servername") & "~/Loan_Savings/wPg_LA_PendingList.aspx", False)
    End Sub

    Protected Sub lnkViewProfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewProfile.Click
        Response.Redirect(Session("servername") & "~/HR/wPgEmployeeProfile.aspx", False)
    End Sub

    Protected Sub lnkEmployeeSkill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmployeeSkill.Click
        Response.Redirect(Session("servername") & "~/HR/wPg_EmployeeSkillList.aspx", False)
    End Sub

    Protected Sub lnkQualification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkQualification.Click
        Response.Redirect(Session("servername") & "~/HR/wPgEmpQualificationList.aspx", False)
    End Sub
    Protected Sub lnkEmpExperience_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpExperience.Click
        Response.Redirect(Session("servername") & "~/HR/wPg_EmployeeExperienceList.aspx", False)
    End Sub

    Protected Sub lnkEmpReferee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpReferee.Click
        Response.Redirect(Session("servername") & "~/HR/wPgEmpRefereeList.aspx", False)
    End Sub

    Protected Sub lnkEnrollCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEnrollCancel.Click
        Response.Redirect(Session("servername") & "~/Training/wpgTraning_Enroll_Cancel.aspx", False)
    End Sub
    Protected Sub LinkInitiativesActionListClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkInitiatives_ActionList.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC Masters/wPg_Initiatives_ActionList.aspx", False)
    End Sub

    Protected Sub LinkKpiMeasureListsClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkKpiMeasuresList.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC Masters/wPg_KpiMeasuresList.aspx", False)
    End Sub

    'Protected Sub LinkEmployeeAssetDeclarationClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkEmployeeAssetDeclarationList.Click
    '    Response.Redirect(Session("servername") & "~/HR/wPg_EmployeeAssetDeclarationList.aspx", False)
    'End Sub

    Protected Sub LinkObjectiveListClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkObjectiveList.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC Masters/wPg_ObjectiveList.aspx", False)
    End Sub

    Protected Sub LinkTargetListClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkTargetList.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC Masters/wPg_TargetList.aspx", False)
    End Sub

    Protected Sub lnkViewIdentities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewIdentities.Click
        Response.Redirect(Session("servername") & "~/HR/wPgEmpIdentities_List.aspx", False)
    End Sub

    Protected Sub lnkViewMemberships_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewMemberships.Click
        Response.Redirect(Session("servername") & "~/HR/wPgEmpMembership_List.aspx", False)
    End Sub

    Protected Sub lnkViewDependants_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewDependants.Click
        Response.Redirect(Session("servername") & "~/HR/wPgDependantsList.aspx", False)
    End Sub

    Protected Sub lnkViewBenefits_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewBenefits.Click
        Response.Redirect(Session("servername") & "~/HR/wPgBenefitList.aspx", False)
    End Sub

    'Protected Sub lnkAssetDecleration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssetDecleration.Click
    '    Response.Redirect(Session("servername") & "~/Asset/wPg_EmployeeAssetDeclarationList.aspx", False)
    'End Sub

    Protected Sub lnkGeneralSelf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneralSelf.Click
        Response.Redirect(Session("servername") & "~/Assessment/General/wPgSelfAssessment.aspx", False)
    End Sub

    Protected Sub lnkGeneralAssessor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneralAssessor.Click
        Response.Redirect(Session("servername") & "~/Assessment/General/wPgAssessorAssessment.aspx", False)
    End Sub

    Protected Sub lnkGeneralReviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneralReviewer.Click
        Response.Redirect(Session("servername") & "~/Assessment/General/wPgReviewerAssessment.aspx", False)
    End Sub

    Protected Sub lnkBSC_Self_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBSC_Self.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC/wPgSelf_BSC.aspx", False)
    End Sub

    Protected Sub lnkBSC_Assessor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBSC_Assessor.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC/wPgAssessor_BSC.aspx", False)
    End Sub

    Protected Sub lnkBSC_Reviewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBSC_Reviewer.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC/wPgReviewer_BSC.aspx", False)
    End Sub

    Protected Sub lnkGeneralAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewGeneralAssessment.Click
        Response.Redirect(Session("servername") & "~/Assessment/General/wPgViewAssessment.aspx", False)
    End Sub

    Protected Sub lnkViewBSCAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewBSCAssessment.Click
        Response.Redirect(Session("servername") & "~/Assessment/General/wPgViewBSCAssessment.aspx", False)
    End Sub

    Protected Sub lnkLevel1Evaluation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLevel1Evaluation.Click
        Response.Redirect(Session("servername") & "~/Training/wPgEmployeeFeedbackList.aspx", False)
    End Sub

    Protected Sub lnkEmpAssetsClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpAssets.Click
        Response.Redirect(Session("servername") & "~/HR/wPg_AssetsRegisterList.aspx", False)
    End Sub


    Protected Sub lnkTrainingImpact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTrainingImpact.Click
        Response.Redirect(Session("servername") & "~/Training/wPg_TrainingImpactList.aspx", False)
    End Sub

    Protected Sub lnkEmpAssetDeclarationList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpAssetDeclarationList.Click
        Response.Redirect(Session("servername") & "~/Assets Declaration/wPg_EmployeeAssetDeclarationList.aspx", False)
    End Sub


    'Sohail (22 May 2012) -- Start
    'TRA - ENHANCEMENT
    Protected Sub lnkPayslip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayslip.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_PayslipReport.aspx", False)
    End Sub
    'Sohail (22 May 2012) -- End

    'Anjan (30 May 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Protected Sub LeaveBalance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LeaveBalance.Click
        Response.Redirect(Session("servername") & "~/HR/wPgLeaveDetails.aspx", False)
    End Sub
    'Anjan (30 May 2012)-End 

    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Protected Sub lnkEmployeeGeneralAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmployeeGeneralAssessment.Click
        Response.Redirect(Session("servername") & "~/Assessment/General/wPgSelfAssessment.aspx", False)
    End Sub

    Protected Sub lnkEmployeeBSCAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmployeeBSCAssessment.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC/wPgSelf_BSC.aspx", False)
    End Sub

    Protected Sub lnkViewFinalBSC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewFinalBSC.Click
        Response.Redirect(Session("servername") & "~/Assessment/BSC Masters/wPg_ObjectiveList.aspx", False)
    End Sub
    'S.SANDEEP [ 14 JUNE 2012 ] -- END



    'Pinkal (14-Aug-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkEmpAgeAnalysisReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpAgeAnalysisReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Analysis.aspx", False)
    End Sub

    Protected Sub lnkEmpAssestReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpAssestReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Assets_Register.aspx", False)
    End Sub

    Protected Sub lnkEmpBeneficiresReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpBeneficiresReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Beneficiary.aspx", False)
    End Sub

    Protected Sub lnkEmpWorkingStation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpWorkingStation.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Class.aspx", False)
    End Sub

    Protected Sub lnkEmpCV_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpCV.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_CV.aspx", False)
    End Sub

    Protected Sub lnkEmpDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpDetails.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Details.aspx", False)
    End Sub

    Protected Sub lnkEmpDistributionReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpDistributionReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Distribution.aspx", False)
    End Sub

    Protected Sub lnkEmpHeadCount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpHeadCount.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_head_Count.aspx", False)
    End Sub

    Protected Sub lnkEmpWithNoDOB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpWithNoDOB.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeWithNoDOB.aspx", False)
    End Sub

    Protected Sub lnkEmpListing_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpListing.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Listing.aspx", False)
    End Sub

    Protected Sub lnkEmpMovement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpMovement.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeMovement.aspx", False)
    End Sub


    Protected Sub lnkEmpWOEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpWOEmail.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Without_Email.aspx", False)
    End Sub

    Protected Sub lnkEmpWOMembership_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpWOMembership.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Without_Membership.aspx", False)
    End Sub

    Protected Sub lnkEmpEOC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpEOC.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EndofContract.aspx", False)
    End Sub

    Protected Sub lnkEmpJobHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpJobHistory.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Job_Employment_History.aspx", False)
    End Sub

    Protected Sub lnkManningLevel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkManningLevel.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Manning_Level.aspx", False)
    End Sub

    Protected Sub lnkEmpPhysicalCount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpPhysicalCount.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Monthly_physical_Count.aspx", False)
    End Sub

    Protected Sub lnkNewhiredEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNewhiredEmp.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Newly_Hired_Employee.aspx", False)
    End Sub

    Protected Sub lnkTerminatedEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTerminatedEmp.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Terminated_Employee.aspx", False)
    End Sub

    Protected Sub lnkEmpPesonalParticulars_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpPesonalParticulars.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_PersonalParticulars.aspx", False)
    End Sub

    Protected Sub lnkEmpQualificationrange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpQualificationrange.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Emp_QualificationRange.aspx", False)
    End Sub

    Protected Sub lnkEmpQualification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpQualification.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeQualification.aspx", False)
    End Sub

    Protected Sub lnkEmpRefereeRpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpRefereeRpt.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeRefereeReport.aspx", False)
    End Sub

    Protected Sub lnkEmpRelation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpRelation.click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeRelation.aspx", False)
    End Sub

    Protected Sub lnkEmpRetirement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpRetirement.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeRetirement.aspx", False)
    End Sub

    Protected Sub lnkEmpSalaryGrade_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpSalaryGrade.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmpCurrSalaryGrade.aspx", False)
    End Sub

    Protected Sub lnkEmpSkill_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpSkill.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeSkills.aspx", False)
    End Sub

    'S.SANDEEP [ 12 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Protected Sub lnkEmployeeListLocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmployeeListLocation.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_List_Location.aspx", False)
    End Sub
    'S.SANDEEP [ 12 NOV 2012 ] -- END

    'Pinkal (12-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkEmpdispoition_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpdispoition.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Disposition_Regionwise.aspx", False)
    End Sub

    'Pinkal (12-Nov-2012) -- End



    'Pinkal (19-Nov-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkMedicalClaim_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedicalClaim.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_MedicalClaim.aspx", False)
    End Sub

    'Pinkal (19-Nov-2012) -- End


    'Pinkal (14-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkMedicalBillSummary_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMedicalBillSummary.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_MedicalBillSummary.aspx", False)
    End Sub

    'Pinkal (14-Dec-2012) -- End




    'Pinkal (24-Aug-2012) -- Start
    'Enhancement : TRA Changes
    Private Sub GetReportPriviliage()
        Try
            Dim objReport As New clsArutiReportClass
            Dim dsData As DataSet = objReport.getReportList(CInt(Session("UserId")), Session("CompanyUnkId"), False)



            For i As Integer = 51 To 55

                Dim Ctl As Control = Me.FindControl("AccordionPane" & i.ToString)
               
                If Ctl Is Nothing Then Exit Sub

                Dim mblnIsVisible As Boolean = False

                If i = 51 Then

                    '*** For ESS report
                    For k As Integer = 1 To Ctl.Controls(1).Controls.Count - 1

                        If Ctl.Controls(1).Controls(k).GetType.FullName = "System.Web.UI.WebControls.Panel" Then

                            If Ctl.Controls(1).Controls(k).ID.ToString().Contains("_") Then

                                Dim strId As String = Ctl.Controls(1).Controls(k).ID.ToString().Substring(Ctl.Controls(1).Controls(k).ID.ToString().IndexOf("_") + 1, (Ctl.Controls(1).Controls(k).ID.ToString().Length - 1) - Ctl.Controls(1).Controls(k).ID.ToString().IndexOf("_"))

                                Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & strId)

                                If drRow.Length > 0 Then
                                    Ctl.Controls(1).Controls(k).Visible = True
                                    mblnIsVisible = True
                                Else
                                    Ctl.Controls(1).Controls(k).Visible = False
                                End If

                            End If

                        End If

                    Next


                    '*** For MSS report
                    For k As Integer = 1 To Ctl.Controls(1).Controls(7).Controls.Count - 1

                        If Ctl.Controls(1).Controls(7).Controls(k).GetType.FullName = "System.Web.UI.WebControls.Panel" Then

                            If Ctl.Controls(1).Controls(7).Controls(k).ID.ToString().Contains("_") Then

                                Dim strId As String = Ctl.Controls(1).Controls(7).Controls(k).ID.ToString().Substring(Ctl.Controls(1).Controls(7).Controls(k).ID.ToString().IndexOf("_") + 1, (Ctl.Controls(1).Controls(7).Controls(k).ID.ToString().Length - 1) - Ctl.Controls(1).Controls(7).Controls(k).ID.ToString().IndexOf("_"))

                                Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & strId)

                                If drRow.Length > 0 Then
                                    Ctl.Controls(1).Controls(7).Controls(k).Visible = True
                                    mblnIsVisible = True
                                Else
                                    Ctl.Controls(1).Controls(7).Controls(k).Visible = False
                                End If

                            End If

                        End If

                    Next
                Else
                    For k As Integer = 1 To Ctl.Controls(1).Controls.Count - 1

                        If Ctl.Controls(1).Controls(k).GetType.FullName = "System.Web.UI.WebControls.Panel" Then

                            If Ctl.Controls(1).Controls(k).ID.ToString().Contains("_") Then

                                Dim strId As String = Ctl.Controls(1).Controls(k).ID.ToString().Substring(Ctl.Controls(1).Controls(k).ID.ToString().IndexOf("_") + 1, (Ctl.Controls(1).Controls(k).ID.ToString().Length - 1) - Ctl.Controls(1).Controls(k).ID.ToString().IndexOf("_"))

                                Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & strId)

                                If drRow.Length > 0 Then
                                    Ctl.Controls(1).Controls(k).Visible = True
                                    mblnIsVisible = True
                                Else
                                    Ctl.Controls(1).Controls(k).Visible = False
                                End If

                            End If

                        End If

                    Next
                End If
               

                If mblnIsVisible = False Then
                    Ctl.Visible = False
                End If
               
            Next
            

            
            

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'MsgBox("GetReportPriviliage:- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
    'Pinkal (24-Aug-2012) -- End


 'S.SANDEEP [ 24 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Protected Sub lnkMigrateApprovers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMigrateApprovers.Click
        Response.Redirect(Session("servername") & "~/Leave/Leave_Approver_Migration.aspx", False)
    End Sub
    'S.SANDEEP [ 24 DEC 2012 ] -- END

    'Pinkal (18-Dec-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkLeaveApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveApprover.Click
        Response.Redirect(Session("servername") & "~/Leave/wPg_LeaveApproverList.aspx", False)
    End Sub

    Protected Sub lnkLeaveIssueUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveIssueUser.Click
        Response.Redirect(Session("servername") & "~/Leave/wPg_AssignIssueUserList.aspx", False)
    End Sub

    'Pinkal (18-Dec-2012) -- End


    'S.SANDEEP [ 01 FEB 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Protected Sub lnkEmpDuration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpDuration.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Duration.aspx", False)
    End Sub
    'S.SANDEEP [ 01 FEB 2013 ] -- END
    
    'Sohail (25 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Protected Sub lnkPayrollReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_PayrollReport.aspx", False)
    End Sub

    Protected Sub lnkEdDetailReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEdDetailReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_EDDetailReport.aspx", False)
    End Sub
    'Sohail (25 Mar 2013) -- End

  

    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkEmpWoImg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpWoImg.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Without_Image.aspx", False)
    End Sub

    Protected Sub lnkEmpWoGender_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpWoGender.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_Employee_Without_Gender.aspx", False)
    End Sub

    'Pinkal (24-Apr-2013) -- End

  
    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkbatchpostinlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkbatchpostinlist.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_BatchPostingList.aspx", False)
    End Sub

    Protected Sub lnkGlobalPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalPayment.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_PayslipGlobalpayment.aspx", False)
    End Sub

    Protected Sub lnkGlobalvoidPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalvoidPayment.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_GlobalVoidPayment.aspx", False)
    End Sub

    Protected Sub lnkAuthorizedPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAuthorizedPayment.Click
        Session("IsAuthorized") = True
        Response.Redirect(Session("servername") & "~/Payroll/wPg_AuthorizedPayment.aspx", False)
    End Sub

    Protected Sub lnkvoidAuthorizedPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkvoidAuthorizedPayment.Click
        Session("IsAuthorized") = False
        Response.Redirect(Session("servername") & "~/Payroll/wPg_AuthorizedPayment.aspx", False)
    End Sub

    Protected Sub lnkPaymentApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPaymentApproval.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_PaymentApproval.aspx", False)
    End Sub

    Protected Sub lnkPayrollSummaryReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollSummaryReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_PayrollSummary.aspx", False)
    End Sub

    Protected Sub lnkPayrollHeadCountReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollHeadCountReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Payroll_Head_Count.aspx", False)
    End Sub

    Protected Sub lnkPayrollVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollVarianceReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_PayrollVariance.aspx", False)
    End Sub

    Protected Sub lnkPayrollTotalVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollTotalVarianceReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Payroll_Total_Variance.aspx", False)
    End Sub

    Protected Sub lnkCashPaymentReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCashPaymentReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Cash_Payment_List.aspx", False)
    End Sub

    Protected Sub lnkBankPaymentReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBankPaymentReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Bank_Payment_List.aspx", False)
    End Sub

    Protected Sub lnkJournalVoucherReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkJournalVoucherReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Journal_Voucher_Ledger.aspx", False)
    End Sub

    Protected Sub lnkSalaryChangeReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalaryChangeReport.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Employee_Salary_Change.aspx", False)
    End Sub


    'Pinkal (12-May-2013) -- End

    'S.SANDEEP [ 29 May 2013 ] -- START
    'ENHANCEMENT : ATLAS COPCO WEB CHANGES
    Protected Sub lnkLoan_Advance_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLoan_Advance.Click
        Response.Redirect(Session("servername") & "~/Loan_Savings/wPg_LA_List.aspx", False)
    End Sub
    'S.SANDEEP [ 29 May 2013 ] -- END

    'Pinkal (03-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkEmployeeSavings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmployeeSavings.Click
        Response.Redirect(Session("servername") & "~/Loan_Savings/wPg_Employee_Saving_List.aspx", False)
    End Sub

    'Pinkal (03-Jun-2013) -- End

 'Sohail (07 Jun 2013) -- Start
    'TRA - ENHANCEMENT
    Protected Sub lnkLoanAdvanceSaving_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLoanAdvanceSaving.Click
        Response.Redirect(Session("servername") & "~/Payroll/Rpt_Loan_Advance_Saving.aspx", False)
    End Sub
    'Sohail (07 Jun 2013) -- End
  

    'Pinkal (18-Jun-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkEDList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEDList.Click
        Response.Redirect(Session("servername") & "~/Payroll/wPg_EarningDeductionList.aspx", False)
    End Sub

    'Pinkal (18-Jun-2013) -- End

  

    'Pinkal (3-Aug-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkEmpIdentity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpIdentity.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeIdentities.aspx", False)
    End Sub

    Protected Sub lnkEmpMembership_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpMembership.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmpMembership_Report.aspx", False)
    End Sub

    'Pinkal (3-Aug-2013) -- End

    'S.SANDEEP [ 28 OCT 2013 ] -- START
    'Protected Sub lnkMigration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMigration.Click
    '    Response.Redirect(Session("servername") & "~/Assessment/Common/wPgMigration.aspx", False)
    'End Sub
    'S.SANDEEP [ 28 OCT 2013 ] -- END
  
    'Sohail (24 Dec 2013) -- Start
    'Enhancement - Send link in salary change notification
    Protected Sub lnkSalaryChange_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSalaryChange.Click
        Response.Redirect(Session("servername") & "~/HR/wPg_SalaryIncrementList.aspx", False)
    End Sub
    'Sohail (24 Dec 2013) -- End

   'S.SANDEEP [ 17 DEC 2013 ] -- START
    Protected Sub lnkGlobalAEDTimeSheet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGlobalAEDTimeSheet.Click
        Response.Redirect(Session("servername") & "~/TnA/GlobalTimesheetAddEditDelete/wPgGlobalTimesheetOperation.aspx", False)
    End Sub
    'S.SANDEEP [ 17 DEC 2013 ] -- END

    'S.SANDEEP [ 31 DEC 2013 ] -- START
    Protected Sub lnkInterviewAnalysisList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInterviewAnalysisList.Click
        Response.Redirect(Session("servername") & "~/Recruitment/wPgInterviewAnalysisList.aspx", False)
    End Sub

    Protected Sub lnkApplicantFilterList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApplicantFilterList.Click
        Response.Redirect(Session("servername") & "~/Recruitment/wPg_ApplicantFilterList.aspx", False)
    End Sub

Protected Sub lnkFinalApplicant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFinalApplicant.Click
        Response.Redirect(Session("servername") & "~/Recruitment/wPg_FinalApplicant.aspx", False)
    End Sub
    'S.SANDEEP [ 31 DEC 2013 ] -- END



    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkLeaveStatementReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveStatementReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_LeaveStatementReport.aspx", False)
    End Sub

    Protected Sub lnkEmplopyeeLeaveFormReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmplopyeeLeaveFormReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeLeaveFormReport.aspx", False)
    End Sub

    Protected Sub lnkLeaveBalListReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveBalListReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeLeaveBalanceReport.aspx", False)
    End Sub

    Protected Sub lnkLeavePlanReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeavePlanReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_LeavePlannerReport.aspx", False)
    End Sub

    Protected Sub lnkEmpTimesheetReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpTimesheetReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeTimesheetReport.aspx", False)
    End Sub

    Protected Sub lnkViewShiftPolicy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewShiftPolicy.Click
        Response.Redirect(Session("servername") & "~/TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx", False)
    End Sub

    'Pinkal (01-Feb-2014) -- End


    'Pinkal (25-Apr-2014) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkLvFormApprovalStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLvFormApprovalStatus.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_LvForm_ApprovalStatus_Report.aspx", False)
    End Sub

    Protected Sub lnkEmpIssuedLeaveStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpIssuedLeaveStatus.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_IssuedLeave_Status_Report.aspx", False)
    End Sub

    'Pinkal (25-Apr-2014) -- End


    'Pinkal (11-Jun-2014) -- Start
    'Enhancement : TANAPA Changes [STAFF REQUISITION]

    Protected Sub lnkStaffRequisition_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStaffRequisition.Click
        Response.Redirect(Session("servername") & "~/Recruitment/Staff_Requisition/wPg_StaffRequisitionList.aspx", False)
    End Sub

    'Pinkal (11-Jun-2014) -- End

   
    'Pinkal (19-Jun-2014) -- Start
    'Enhancement : TRA Changes Leave Enhancement
    Protected Sub lnkStaffRequisition_Approval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkStaffRequisition_Approval.Click
        Response.Redirect(Session("servername") & "~/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisitionList.aspx", False)
    End Sub

    'Pinkal (19-Jun-2014) -- End

'Shani(30-Aug-2014) -- Start
   
    Protected Sub lnkcrExpenseApprovalList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcrExpenseApprovalList.Click
        Response.Redirect(Session("servername") & "~/Claims_And_Expenses/wPg_Approval_List.aspx", False)
    End Sub

    Protected Sub lnkcrExpenseApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcrExpenseApproval.Click
        Response.Redirect(Session("servername") & "~/Claims_And_Expenses/wPg_ExpenseApprovalList.aspx", False)
    End Sub


    Protected Sub lnkcrClaimRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcrClaimRequest.Click
        Response.Redirect(Session("servername") & "~/Claims_And_Expenses/wPg_ClaimAndRequestList.aspx", False)
    End Sub

    Protected Sub lnkcrApproverMigration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkcrApproverMigration.Click
        Response.Redirect(Session("servername") & "~/Claims_And_Expenses/wPg_ApprovalMigration.aspx", False)
    End Sub

    Protected Sub lnkPostToPayroll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPostToPayroll.Click
        Response.Redirect(Session("servername") & "~/Claims_And_Expenses/wPg_PostToPayroll.aspx", False)
    End Sub

    'Shani(30-Aug-2014) -- End


    'Shani [ 28 OCT 2014 ] -- START
    'Implementing "Leave Approver Report AND Employee List without Leave Approvers" under Web In Leave Report Secation
    Protected Sub lnkLeaveApproverRepor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveApproverReport.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_LeaveApproverReport.aspx", False)
    End Sub

    Protected Sub lnkEmpListWithoutLeaveApprvers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpListWithoutLeaveApprvers.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmpListWithoutLeaveApprover.aspx", False)
    End Sub
    Protected Sub lnkEmpLeaveAbsSummaryRpt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmpLeaveAbsSummaryRpt.Click
        Response.Redirect(Session("servername") & "~/Reports/Rpt_EmployeeLeaveAbsenceSummary.aspx", False)
    End Sub
    'Shani [ 28 OCT 2014 ] -- END

   'Shani [ 31 OCT 2014 ] -- START
    'Enhancement-Implementing New Perfromance Module in Web. (Allocate By SandeepSir)
    Protected Sub lnkCmpnyGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCmpnyGoals.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgCompanyLvlGoalsList.aspx", False)
    End Sub

    Protected Sub lnkAllocationGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocationGoals.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgAllocationLvlGoalsList.aspx", False)
    End Sub

    Protected Sub lnkMyprmcGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMyprmcGoals.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx", False)
    End Sub

    Protected Sub lnkselfAssessmnet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkselfAssessmnet.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
    End Sub

    'Protected Sub lnkEmployeeGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEmployeeGoals.Click
    '    Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx", False)
    'End Sub

    Protected Sub lnkAssessEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssessEmployee.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
    End Sub

    Protected Sub lnkReviewEmployeeAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReviewEmployeeAssessment.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_ReviewerEvaluationList.aspx", False)
    End Sub
   
    Protected Sub lnkAssessorReviewerMigration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssessorReviewerMigration.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Common/wPgMigration.aspx", False)
    End Sub

    Protected Sub lnkViewPerformanceEval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewPerformanceEval.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_ViewPerfEvaluation.aspx", False)
    End Sub
    'Shani [ 31 OCT 2014 ] -- END


    'S.SANDEEP [ 05 NOV 2014 ] -- START
    Protected Sub objlnkCustomHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ilnk As LinkButton = CType(sender, LinkButton)
        Session.Add("customheaderunkid", ilnk.CommandArgument)
        Response.Redirect(Session("servername") & "~/Assessment New/Common/wPg_CustomeItemView.aspx", False)
    End Sub

    Private Sub Generate_Custom_Header_Menu()
        Try
            Dim objCHeader As New clsassess_custom_header
            Dim dsList As New DataSet
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                dsList = objCHeader.Get_Headers_For_Menu(False, True)
            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                dsList = objCHeader.Get_Headers_For_Menu(True, False)
            End If
            dlCHeaderMenu.DataSource = dsList
            dlCHeaderMenu.DataBind()
        Catch ex As Exception
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 05 NOV 2014 ] -- END

    'S.SANDEEP [29 JAN 2015] -- START
    Protected Sub lnkAssignCompetencies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAssignCompetencies.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Competencies/wpg_AssignedCompetenciesList.aspx", False)
    End Sub

    Protected Sub lnkViewPlanning_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewPlanning.Click
        Response.Redirect(Session("servername") & "~/Assessment New/Common/wPgPerformance_Planning.aspx", False)
    End Sub
    'S.SANDEEP [29 JAN 2015] -- END
   
End Class
