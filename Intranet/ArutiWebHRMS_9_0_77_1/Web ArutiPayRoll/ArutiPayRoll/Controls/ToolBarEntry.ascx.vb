﻿Partial Class Controls_ToolBarEntry
    Inherits System.Web.UI.UserControl

    Enum E_DisplayMode
        Form
        Grid
    End Enum

    Enum enEntryMode
        Add
        Update
        Navigate
        Save
        Cancel
        Delete
        None
        form
        grid
    End Enum

    Enum EN_ButtonMode
        Add
        Update
    End Enum

    Private prEntryMode As enEntryMode
    Public Property EntryMode() As enEntryMode
        Get
            EntryMode = prEntryMode
        End Get
        Set(ByVal value As enEntryMode)
            prEntryMode = value
        End Set
    End Property
    Public Property Enabled() As Boolean
        Get

        End Get
        Set(ByVal value As Boolean)
            Me.Enabled = value
        End Set
    End Property


    Private pDisplayMode As E_DisplayMode
    Public Property DisplayMode() As E_DisplayMode
        Get
            Return pDisplayMode
        End Get
        Set(ByVal value As E_DisplayMode)
            value = pDisplayMode
        End Set
    End Property

#Region "UserEvents"
    Public Event New_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event First_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Find_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Last_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Next_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event Prev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean)
    Public Event GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    Public Event FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)

    Dim mSetButtons As Boolean = True

    Protected Sub imgNew_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgNew.Click
        EntryMode = EN_ButtonMode.Add
        RaiseEvent New_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub

    Protected Sub imgSave_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgSave.Click
        EntryMode = EN_ButtonMode.Add
        RaiseEvent Save_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub
    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgDel.Click
        EntryMode = EN_ButtonMode.Add
        RaiseEvent Delete_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub

    Protected Sub imgCancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCancel.Click
        EntryMode = EN_ButtonMode.Add
        RaiseEvent Cancel_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub

   
    Protected Sub imgFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgFirst.Click
        EntryMode = EN_ButtonMode.Update
        RaiseEvent First_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub
    Protected Sub imgLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgLast.Click
        EntryMode = EN_ButtonMode.Update
        RaiseEvent Last_Click(sender, e, mSetButtons)
        SetButtons()
    End Sub
    Protected Sub imgNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgNext.Click

        EntryMode = EN_ButtonMode.Update
        RaiseEvent Next_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub

    Protected Sub imgPrev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgPrev.Click
        EntryMode = EN_ButtonMode.Update
        RaiseEvent Prev_Click(sender, e, mSetButtons)
        If mSetButtons Then SetButtons()
    End Sub
    
    Protected Sub imgModeGrid_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgModeGrid.Click

        SetEntryMode(E_DisplayMode.Grid)
        RaiseEvent GridMode_Click(sender, e)
    End Sub
    Protected Sub imgModeForm_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgModeForm.Click
        SetEntryMode(E_DisplayMode.Form)
        RaiseEvent FormMode_Click(sender, e)
    End Sub

#End Region

#Region "Functions"

    Public Function VisibleNewButton(ByVal NewButton As Boolean) As Boolean
        imgNew.Visible = NewButton
    End Function

    Private Function EnableNewButton(ByVal ENewButton As Boolean) As Boolean
        imgNew.Enabled = ENewButton

        Return imgNew.Enabled
    End Function
    Public Function VisibleModeGridButton(ByVal GridButton As Boolean) As Boolean
        imgModeGrid.Visible = GridButton
    End Function

    Private Function EnableModeGridButton(ByVal EGridButton As Boolean) As Boolean
        imgModeGrid.Enabled = EGridButton
        Return imgModeGrid.Enabled
    End Function
    Public Function VisibleModeFormButton(ByVal FormButton As Boolean) As Boolean
        imgModeForm.Visible = FormButton
    End Function

    Private Function EnableModeFormButton(ByVal EFormButton As Boolean) As Boolean
        imgModeGrid.Enabled = EFormButton
        Return imgModeGrid.Enabled
    End Function

    Private Function EnableCancelButton(ByVal ECancelButton As Boolean) As Boolean
        imgCancel.Enabled = ECancelButton

    End Function

    Public Function VisibleCancelButton(ByVal CancelButton As Boolean) As Boolean
        imgCancel.Visible = CancelButton
    End Function


    Public Function VisibleDeleteButton(ByVal DeleteButton As Boolean) As Boolean
        imgDel.Visible = DeleteButton
    End Function

    Private Function EnableDeleteButton(ByVal EDeleteButton As Boolean) As Boolean
        imgDel.Enabled = EDeleteButton

    End Function

    Public Function VisibleSaveButton(ByVal SaveButton As Boolean) As Boolean
        imgSave.Visible = SaveButton

    End Function
    Private Function EnableSaveButton(ByVal ESaveButton As Boolean) As Boolean
        imgSave.Enabled = ESaveButton

    End Function
    Public Function VisibleFindButton(ByVal FindButton As Boolean) As Boolean
        'imgFind.Visible = FindButton

    End Function
    Private Function EnableFindButton(ByVal EFindButton As Boolean) As Boolean
        '  imgFind.Enabled = EFindButton

    End Function

    Public Function VisibleExitButton(ByVal ExitButton As Boolean) As Boolean
        ' imgExit.Visible = ExitButton

    End Function

    Private Function EnableExitButton(ByVal EExitButton As Boolean) As Boolean
        '  imgExit.Enabled = EExitButton
    End Function

    Public Function VisibleMoveFirstButton(ByVal MoveFirstButton As Boolean) As Boolean
        imgFirst.Visible = MoveFirstButton
    End Function

    Private Function EnableMoveFirstButton(ByVal EMoveFirstButton As Boolean) As Boolean
        imgFirst.Enabled = EMoveFirstButton


    End Function

    Public Function VisibleMoveLastButton(ByVal MoveLastButton As Boolean) As Boolean
        imgLast.Visible = MoveLastButton

    End Function

    Private Function EnableMoveLastButton(ByVal EMoveLastButton As Boolean) As Boolean
        imgLast.Enabled = EMoveLastButton

    End Function

    Public Function VisibleMovePreviousButton(ByVal MovePreviousButton As Boolean) As Boolean
        imgPrev.Visible = MovePreviousButton

    End Function

    Private Function EnableMovePreviousButton(ByVal EMovePreviousButton As Boolean) As Boolean
        imgPrev.Enabled = EMovePreviousButton

    End Function

    Public Function VisibleMoveNextButton(ByVal MoveNextButton As Boolean) As Boolean
        imgNext.Visible = MoveNextButton

    End Function

    Private Function EnableMoveNextButton(ByVal EMoveNextButton As Boolean) As Boolean
        imgNext.Enabled = EMoveNextButton

    End Function

    Public Function VisibleImageSaprator1(ByVal EGridButton As Boolean) As Boolean
        imgSep1.Visible = EGridButton
        Return imgSep1.Visible
    End Function
    Public Function VisibleImageSaprator2(ByVal EGridButton As Boolean) As Boolean
        imgSep2.Visible = EGridButton
        Return imgSep2.Visible
    End Function
    Public Function VisibleImageSaprator3(ByVal EGridButton As Boolean) As Boolean
        imgSep3.Visible = EGridButton
        Return imgSep3.Visible
    End Function
    Public Function VisibleImageSaprator4(ByVal EGridButton As Boolean) As Boolean
        imgSep4.Visible = EGridButton
        Return imgSep4.Visible
    End Function

#End Region

#Region "UserMethods"

    'Public Sub SetButtons()

    '    Me.VisibleSaveButton(True)
    '    Me.VisibleCancelButton(True)
    '    Me.VisibleNewButton(True)
    '    Me.VisibleDeleteButton(True)
    '    Me.VisibleFindButton(True)
    '    Me.VisibleMoveFirstButton(True)
    '    Me.VisibleMoveLastButton(True)
    '    Me.VisibleMovePreviousButton(True)
    '    Me.VisibleMoveNextButton(True)

    '    Select Case EntryMode
    '        Case EN_ButtonMode.Add

    '            Me.VisibleSaveButton(True)
    '            Me.VisibleCancelButton(True)
    '            Me.VisibleNewButton(False)
    '            Me.VisibleDeleteButton(False)
    '            Me.VisibleFindButton(True)
    '            Me.VisibleMoveFirstButton(True)
    '            Me.VisibleMoveLastButton(True)
    '            Me.VisibleMovePreviousButton(True)
    '            Me.VisibleMoveNextButton(True)

    '        Case Else

    '            Me.VisibleSaveButton(True)
    '            Me.VisibleCancelButton(True)
    '            Me.VisibleNewButton(True)
    '            Me.VisibleDeleteButton(True)
    '            Me.VisibleFindButton(True)
    '            Me.VisibleMoveFirstButton(True)
    '            Me.VisibleMoveLastButton(True)
    '            Me.VisibleMovePreviousButton(True)
    '            Me.VisibleMoveNextButton(True)




    '    End Select
    'End Sub
    Public Sub SetButtons()

        Me.VisibleSaveButton(True)
        Me.VisibleCancelButton(True)
        Me.VisibleNewButton(True)
        Me.VisibleDeleteButton(False)
        Me.VisibleFindButton(False)
        Me.VisibleMoveFirstButton(False)
        Me.VisibleMoveLastButton(False)
        Me.VisibleMovePreviousButton(False)
        Me.VisibleMoveNextButton(False)

        Select Case EntryMode
            Case EN_ButtonMode.Add

                Me.VisibleSaveButton(True)
                Me.VisibleCancelButton(True)
                Me.VisibleNewButton(False)
                Me.VisibleDeleteButton(False)
                Me.VisibleFindButton(False)
                Me.VisibleMoveFirstButton(False)
                Me.VisibleMoveLastButton(False)
                Me.VisibleMovePreviousButton(False)
                Me.VisibleMoveNextButton(False)

            Case Else

                Me.VisibleSaveButton(True)
                Me.VisibleCancelButton(True)
                Me.VisibleNewButton(True)
                Me.VisibleDeleteButton(False)
                Me.VisibleFindButton(False)
                Me.VisibleMoveFirstButton(False)
                Me.VisibleMoveLastButton(False)
                Me.VisibleMovePreviousButton(False)
                Me.VisibleMoveNextButton(False)

        End Select
    End Sub




#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Me.IsPostBack Then
                prEntryMode = CType(Me.ViewState("prentrymode"), enEntryMode)
                pDisplayMode = CType(Me.ViewState("pDisplayMode"), E_DisplayMode)
            End If

            If Not Me.IsPostBack Then
                EntryMode = EN_ButtonMode.Add
                SetButtons()
            End If
            SetEntryMode(Me.DisplayMode)
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("ToolBarEntry-->PageLoad Event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("prentrymode", prEntryMode)
        Me.ViewState.Add("pDisplayMode", pDisplayMode)
    End Sub

    Public Sub SetEntryMode(ByVal EntryMode As E_DisplayMode)
        Try
            Select Case EntryMode
                Case E_DisplayMode.Form
                    imgModeGrid.Visible = True
                    imgModeGrid.Enabled = True
                    imgModeForm.Visible = False
                    imgModeForm.Enabled = False
                    pDisplayMode = E_DisplayMode.Form

                Case E_DisplayMode.Grid
                    imgModeGrid.Visible = False
                    imgModeGrid.Enabled = False
                    imgModeForm.Visible = True
                    imgModeForm.Enabled = True
                    imgSave.Visible = False
                    imgCancel.Visible = False
                    imgFirst.Visible = False
                    imgNext.Visible = False
                    '  imgFind.Visible = False
                    imgDel.Visible = False
                    imgLast.Visible = False
                    imgPrev.Visible = False
                    imgNew.Visible = False
                    pDisplayMode = E_DisplayMode.Grid
            End Select
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("ToolBarEntry-->SetEntryMode Event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try

    End Sub

End Class
