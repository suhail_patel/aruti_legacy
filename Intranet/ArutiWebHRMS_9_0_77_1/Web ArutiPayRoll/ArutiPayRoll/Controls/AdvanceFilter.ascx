﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AdvanceFilter.ascx.vb"
    Inherits="Controls_AdvanceFilter" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    
</style>

<script type="text/javascript" language="javascript" >
    $.expr[":"].containsNoCase = function(el, i, m) {
        var search = m[3];
        if (!search) return false;
        return eval("/" + search + "/i").test($(el).text());
    };

    function $$(id, context) {
        var el = $("#" + id, context);
        if (el.length < 1)
            el = $("[id$=_" + id + "]", context);
        return el;
    }

    $(document).ready(function() {

    var txtSearch = $$('txtSearch');
      

        
    });

    //'S.SANDEEP |25-OCT-2019| -- START
    //'ISSUE/ENHANCEMENT : Calibration Issues
    function Search(ctr) {
        var txtSearch = $$('txtSearch');
        var gvDetails = $$('gvDetails');

        if ($(txtSearch).length > 1) {
            for (i = 0; i <= $(txtSearch).length - 1; i++) {
                if ($(txtSearch)[i].id === ctr.id) {
                    txtSearch = $(txtSearch)[i];
                    gvDetails = $(txtSearch).closest('table').find('.gridview')[0];
                }
            }
        }
        //'S.SANDEEP |25-OCT-2019| -- END

        $('.norecords').remove();

        if ($(txtSearch).val().length > 0) {
            $('#' + $(gvDetails).attr('id') + ' tr').hide();            
            $('#' + $(gvDetails).attr('id') + ' tr:first').show();
            $('#' + $(gvDetails).attr('id') + ' tr td:containsNoCase(\'' + $(txtSearch).val() + '\')').parent().show();
            $('#' + $(gvDetails).attr('id') + ' tr:visible').find($$('hdnfld')).val('')
            $('#' + $(gvDetails).attr('id') + ' tr:hidden').find($$('hdnfld')).val('hidden')
        }
        else if ($(txtSearch).val().length == 0) {
            resetSearchValue();
        }

        if ($('#' + $(gvDetails).attr('id') + ' tr:visible').length == 1) {
            $('.norecords').remove();
            $(gvDetails).append('<tr class="norecords"><td colspan="6" class="Normal" style="text-align: center">No records were found</td></tr>');
        }
    }

    function resetSearchValue() {
        var txtSearch = $$('txtSearch');
        var gvDetails = $$('gvDetails');
        
        $(txtSearch).val('');
        $('#' + $(gvDetails).attr('id') + ' tr').show();
        $('#' + $(gvDetails).attr('id') + ' tr:visible').find($$('hdnfld')).val('')
        $('#' + $(gvDetails).attr('id') + ' tr:hidden').find($$('hdnfld')).val('hidden')
        $('.norecords').remove();
        $(txtSearch).focus();
    }
</script>

<ajaxToolkit:ModalPopupExtender ID="popupAdvanceFilter" runat="server" BackgroundCssClass="ModalPopupBG"
    CancelControlID="HiddenField2" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none; width: 810px"
    DefaultButton="btnApply">
    <div id="divAdvanceFilter" class="panel-primary" style="margin-bottom: 0px">
        <div class="panel-heading">
            <asp:Label ID="lblTitle" runat="server" Text="Advance Search"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 30%">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Style="float: left; height: 400px;
                                    width: 220px; overflow: auto" AutoPostBack="true" CssClass="mycustomcss">
                                    <ajaxToolkit:TabPanel ID="tbpnlBranch" runat="server" Width="0px" Style="width: 0px;"
                                        HeaderText="Branch">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlBranch" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                                <div>
                                                </div>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlDeptGroup" runat="server" Style="width: 0px;" HeaderText="Department Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlDept" runat="server" Style="width: 0px;" HeaderText="Department">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel22" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlSectionGroup" runat="server" Style="width: 0px;" HeaderText="Section Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlSection" runat="server" Style="width: 0px;" HeaderText="Section">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel4" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlUnitGroup" runat="server" Style="width: 0px;" HeaderText="Unit Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlUnit" runat="server" Style="width: 0px;" HeaderText="Unit">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel6" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlTeam" runat="server" Style="width: 0px;" HeaderText="Team">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel7" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlJobGroup" runat="server" Style="width: 0px;" HeaderText="Job Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel8" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlJob" runat="server" Style="width: 0px;" HeaderText="Job">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel9" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlClassGroup" runat="server" Style="width: 0px;" HeaderText="Class Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel10" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlClass" runat="server" Style="width: 0px;" HeaderText="Class">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel11" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGradeGroup" runat="server" Style="width: 0px;" HeaderText="Grade Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel12" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGrade" runat="server" Style="width: 0px;" HeaderText="Grade">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel13" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGradeLevel" runat="server" Style="width: 0px;" HeaderText="Grade Level">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel14" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlCostCenterGroup" runat="server" Style="width: 0px;"
                                        HeaderText="Cost Center Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel15" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlCostCenter" runat="server" Style="width: 0px;" HeaderText="Cost Center">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel16" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlEmployementType" runat="server" Style="width: 0px;"
                                        HeaderText="Employement Type">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel17" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlReligion" runat="server" Style="width: 0px;" HeaderText="Religion">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel18" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGender" runat="server" Style="width: 0px;" HeaderText="Gender">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel19" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlNationality" runat="server" Style="width: 0px;" HeaderText="Nationality">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel20" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlPayType" runat="server" Style="width: 0px;" HeaderText="Pay Type">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel21" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlPayPoint" runat="server" Style="width: 0px;" HeaderText="Pay Point">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel23" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <%--'S.SANDEEP |04-JUN-2019| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]--%>
                                    <ajaxToolkit:TabPanel ID="tbpnlMaritalStatus" runat="server" Style="width: 0px;" HeaderText="Marital Status">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel24" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlRelationFromDpndt" runat="server" Style="width: 0px;" HeaderText="Relation from Dependents">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel25" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <%--'S.SANDEEP |04-JUN-2019| -- END--%>
                                    <ajaxToolkit:TabPanel ID="tbpnlSkill" runat="server" Style="width: 0px;" HeaderText="Skill">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel26" runat="server" ScrollBars="Auto" Width="0px" Height="380px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </td>
                            <td style="width: 35%">
                                <asp:Panel ID="pnl_gvdetails" runat="server" ScrollBars="Auto" Height="400px" Width="100%">
                                <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="false" placeholder="Type to search" style="width:98%; margin:5px;" onkeyup="Search();" />
                                    <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                        Width="98%" CellPadding="3" DataKeyNames="Id" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Eval("IsCheck") %>' OnCheckedChanged="chkSelect_CheckedChanged"
                                                        AutoPostBack="true" />
                                                        <asp:HiddenField ID="hdnfld" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="Name" HeaderText="Description" FooterText="colhDescription"
                                                ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                            <td style="width: 35%">
                                <asp:Panel ID="pnl_gvFilterInfo" runat="server" ScrollBars="Auto" Width="100%" Height="400px">
                                    <asp:GridView ID="gvFilterInfo" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                        Width="98%" CellPadding="3" DataKeyNames="" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="Name" HeaderText="Filter Value" FooterText="colhFilterValue"
                                                ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                        <asp:Label ID="lblMessage" runat="server" Text="" Style="max-width: 475px; display: inline-block;" />
                        <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btnDefault" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
