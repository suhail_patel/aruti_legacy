﻿Imports Aruti.Data
Imports System.Data

Partial Class Controls_ViewMovementApproval
    Inherits System.Web.UI.UserControl
    Public Event buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
#Region "Private Varible"
    Private blnShowPopup As Boolean = False
    Private DisplayMessage As New CommonCodes
    Private mintUserId As Integer
    Private mintPriority As Integer
    Private mintPrivilegeId As Integer
    Private meFillType As clsEmployeeMovmentApproval.enMovementType
    Private meDtType As enEmp_Dates_Transaction
    Private mblnIsResidentPermit As Boolean
    Private mstrFilterString As String = ""
    Private mblnFromApprovalScreen As Boolean = True
    Private objApprovalTran As New clsEmployeeMovmentApproval
    'S.SANDEEP |17-JAN-2019| -- START
    Private meOperation As clsEmployeeMovmentApproval.enOperationType
    'S.SANDEEP |17-JAN-2019| -- END
#End Region

#Region "Public Property(S)"

    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    Public Property _PrivilegeId() As Integer
        Get
            Return mintPrivilegeId
        End Get
        Set(ByVal value As Integer)
            mintPrivilegeId = value
        End Set
    End Property

    Public Property _FillType() As clsEmployeeMovmentApproval.enMovementType
        Get
            Return meFillType
        End Get
        Set(ByVal value As clsEmployeeMovmentApproval.enMovementType)
            meFillType = value
        End Set
    End Property

    Public Property _DtType() As enEmp_Dates_Transaction
        Get
            Return meDtType
        End Get
        Set(ByVal value As enEmp_Dates_Transaction)
            meDtType = value
        End Set
    End Property

    Public Property _IsResidentPermit() As Boolean
        Get
            Return mblnIsResidentPermit
        End Get
        Set(ByVal value As Boolean)
            mblnIsResidentPermit = value
        End Set
    End Property

    Public Property _FilterString() As String
        Get
            Return mstrFilterString
        End Get
        Set(ByVal value As String)
            mstrFilterString = value
        End Set
    End Property

    Public Property _FromApprovalScreen() As Boolean
        Get
            Return mblnFromApprovalScreen
        End Get
        Set(ByVal value As Boolean)
            mblnFromApprovalScreen = value
        End Set
    End Property
    'S.SANDEEP |17-JAN-2019| -- START
    Public Property _OprationType() As clsEmployeeMovmentApproval.enOperationType
        Get
            Return meOperation
        End Get
        Set(ByVal value As clsEmployeeMovmentApproval.enOperationType)
            meOperation = value
        End Set
    End Property
'S.SANDEEP |17-JAN-2019| -- END
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            blnShowPopup = CBool(Me.ViewState("blnShowPopup"))
            If blnShowPopup Then
                popup_ViewMovementApproval.Show()
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("blnShowPopup") = blnShowPopup
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub


#Region "Private Method(S)"
    Public Sub Show()

        Try
            blnShowPopup = True
            popup_ViewMovementApproval.Show()

            If mblnFromApprovalScreen = False Then
                lblTitle.Text = "Approval Status"
            Else
                lblTitle.Text = "My Report"
            End If
            Call FillGrid()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
            lblTitle.Text = ""
            blnShowPopup = False
            popup_ViewMovementApproval.Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub


    Private Sub FillGrid()
        Try
            'S.SANDEEP |17-JAN-2019| -- ADD OPRATION TYPE
            Dim dt As DataTable = objApprovalTran.GetNextEmployeeApprovers(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), mintPrivilegeId, meFillType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), meDtType, mblnIsResidentPermit, meOperation, Nothing, mintPriority, True, mstrFilterString)
            If mblnFromApprovalScreen Then
                dt = New DataView(dt, "priority <= " & mintPriority, "employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()
            End If

            SetGridColums()
            gvApproveRejectMovement.DataSource = dt
            gvApproveRejectMovement.DataBind()

        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub

    Private Sub SetGridColums()
        Try

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate1", False, True)).Visible = False _
                  : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhDispValue", False, True)).Visible = False _
              : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJobGroup", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJob", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhwork_permit_no", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhissue_place", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhIDate", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhExDate", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhCountry", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhbranch", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdeptgroup", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdept", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsecgroup", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsection", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunitgrp", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunit", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhteam", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclassgrp", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclass", False, True)).Visible = False


            Select Case meFillType
                Case clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate1", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = False

                Case clsEmployeeMovmentApproval.enMovementType.PROBATION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate1", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = False

                Case clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.TERMINATION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhDispValue", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJobGroup", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJob", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhwork_permit_no", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhissue_place", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhIDate", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhExDate", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhCountry", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhbranch", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdeptgroup", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdept", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsecgroup", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsection", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunitgrp", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunit", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhteam", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclassgrp", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclass", False, True)).Visible = True
            End Select
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("SetGridColums :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub


#End Region

#Region "Gridview Event"

    Protected Sub gvApproveRejectMovement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproveRejectMovement.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub
#End Region

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Hide()
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'DisplayMessage.DisplayError(ex.Message, Me.Page)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try

    End Sub
End Class
