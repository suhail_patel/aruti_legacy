﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LeftMenus.ascx.vb" Inherits="Controls_LeftMenus" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<link href="../App_Themes/blue/blue.css" rel="stylesheet" type="text/css" />

<link href="../App_Themes/blue/blue.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.js"></script>


<script language="javascript" type="text/javascript">

   
    
    function ChangeImage(imgID) {
        var pathname = document.location.href;
        var arr = pathname.split('/');


        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';

        if (imgURL[imgURL.length - 1] == 'plus.png') {
            document.getElementById(imgID).src = URL + "images/minus.png";
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') {
            document.getElementById(imgID).src = URL + "images/plus.png";
        }


        var cnt = 1;
        while (cnt <= 13) {
            var imgIdRec = "imgAcc" + cnt;

            if (document.getElementById(imgIdRec) != null && imgIdRec != imgID) {
                document.getElementById(imgIdRec).src = URL + "images/plus.png";
            }
            cnt = cnt + 1;
        }

        PageMethods.SectionClicked(0, imgID);   
        
    }

    function ChangeImageReport(imgID) {
        var pathname = document.location.href;
        var arr = pathname.split('/');


        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';

        if (imgURL[imgURL.length - 1] == 'plus.png') {
            document.getElementById(imgID).src = URL + "images/minus.png";
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') {
            document.getElementById(imgID).src = URL + "images/plus.png";
        }
       

        var cnt = 51;
        while (cnt <= 55) {
            var imgIdRec = "imgAcc" + cnt;
            if (document.getElementById(imgIdRec) != null && imgIdRec != imgID) {
                document.getElementById(imgIdRec).src = URL + "images/plus.png";
            }
            cnt = cnt + 1;
        }

        PageMethods.SectionClicked(1, imgID);       
    }


    function LoadImage(imgAcc, collapsed) {
        var pathname = document.location.href;
        var arr = pathname.split('/');

        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';
        var cnt = 1;
        while (cnt <= 13) {

            var imgIdRec = "imgAcc" + cnt;

            if (document.getElementById(imgIdRec) != null) {
                if (imgIdRec != imgAcc) {
                    document.getElementById(imgIdRec).src = URL + "images/plus.png";
                }
                else {
                    if (collapsed == true) {
                        document.getElementById(imgIdRec).src = URL + "images/plus.png";
                    }
                    else {
                    document.getElementById(imgIdRec).src = URL + "images/minus.png";
                }
                    /*document.getElementById('mview').className = 'active';
                    document.getElementById('rview').className = '';

                    document.getElementById('tab1').style.display = 'block';
                    document.getElementById('tab2').style.display = 'none';*/
                    document.getElementById('<%= MyAccordion.ClientID %>').style.height = '520px';
            }
            }
            cnt = cnt + 1;
        }

        var cnt = 51;
        while (cnt <= 55) {

            var imgIdRec = "imgAcc" + cnt;
            if (document.getElementById(imgIdRec) != null) {
                if (imgIdRec != imgAcc) {
                    document.getElementById(imgIdRec).src = URL + "images/plus.png";
    }
                else {
                    if (collapsed == true) {
                        document.getElementById(imgIdRec).src = URL + "images/plus.png";
                    }
                    else {
                        document.getElementById(imgIdRec).src = URL + "images/minus.png";
                    }
                    /*document.getElementById('mview').className = '';
                    document.getElementById('rview').className = 'active';
   
                    document.getElementById('tab1').style.display = 'none';
                    document.getElementById('tab2').style.display = 'block';*/
                    document.getElementById('<%= ReportAccordion.ClientID %>').style.height = '520px';
                    $("#mview").removeClass("active");
                    $("#rview").addClass("active");
                    $(".tab_content:first").hide();
                    $(".tab_content:nth-child(2)").show();
        }
            }
            cnt = cnt + 1;
    }
    
    }

    function GetSynchronousJSONResponse(url, postData) {
        var xmlhttp = null;
        if (window.XMLHttpRequest)
            xmlhttp = new XMLHttpRequest();
        else if (window.ActiveXObject) {
            if (new ActiveXObject("Microsoft.XMLHTTP"))
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            else
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        }
        // to be ensure non-cached version of response
        url = url + "?rnd=" + Math.random();

        xmlhttp.open("POST", url, false); //false means synchronous
        xmlhttp.setRequestHeader("Content-Type", "application/json; charset=utf-8");
        xmlhttp.send(postData);
        var responseText = xmlhttp.responseText;
        return responseText;

    }

    function CheckLic(strModuleName) {
        var result = GetSynchronousJSONResponse('<%= Page.ResolveUrl("~/Index.aspx/CheckLicence") %>', '{"strModuleName":"' + strModuleName + '"}');
        //PageMethods.CheckLicence(strModuleName, GetResult);
        //return IsValidLic;
        result = eval('(' + result + ')');
        if (result.d == false)
            alert('Sorry, you are not registered to use this module. Please contact Aruti support team to register for this moudule.');
        return result.d;
    }
    
</script>

<div id="tabcontainer">
    <ul class="tabs">
        <li id="mview" class="active" rel="tab1" style="width: 86px;" onclick="if (document.getElementById('<%= MyAccordion.ClientID %>') != null) document.getElementById('<%= MyAccordion.ClientID %>').style.height='516px';">
            Main View</li>
        <li id="rview" rel="tab2" style="width: 86px;" onclick="if (document.getElementById('<%= ReportAccordion.ClientID %>') != null)document.getElementById('<%= ReportAccordion.ClientID %>').style.height='516px';">
            Report View</li>
    </ul>
</div>
<div class="tab_container">
    <div id="tab1" class="tab_content" style="display: block; height: 516px;">
        <ajaxToolkit:Accordion ID="MyAccordion" SelectedIndex="-1" SkinID="myaccordion" runat="server"
            HeaderCssClass="accordionHeadernew" HeaderSelectedCssClass="accordionHeaderSelectednew"
            ContentCssClass="accordionContentnew" FadeTransitions="true" FramesPerSecond="50"
            TransitionDuration="250" AutoSize="Limit" RequireOpenedPane="false" SuppressHeaderPostbacks="false"
            Width="250px" Height="516px">
            <Panes>
                <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                    <Header>
                        <%--<asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/images/plus.png" />
                       &nbsp;General--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc1.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc1" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;General</div>--%>
                        <div onclick="ChangeImage('imgAcc1');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img onprerender="LoadImage('<%= Session("imgAcc").ToString() %>');" id="imgAcc1" src="<%=  Session("imgAcc1").tostring %>" alt="" />&nbsp;&nbsp;General</div>--%>
                            <img id="imgAcc1" src="../images/plus.png" alt="" />&nbsp;&nbsp;General</div>
                    </Header>
                    <Content>
                        <asp:LinkButton ID="lnkChangePassword" ToolTip="Change Password" runat="server" Text="Change Password"
                            CssClass="homemenu"></asp:LinkButton><br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                    <Header>
                        <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Human Resource Module" />--%>
                        <%--<asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Employee Profile--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc2.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc2" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;Employee Profile</div>--%>
                        <div onclick="ChangeImage('imgAcc2');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc2" src="<%=  Session("imgAcc2").tostring %>" alt="" />&nbsp;&nbsp;Employee Profile</div>--%>
                            <img id="imgAcc2" src="../images/plus.png" alt="" />&nbsp;&nbsp;Employee Profile</div>
                    </Header>
                    <Content>
                        <asp:LinkButton ID="lnkViewProfile" ToolTip="Bio – Data" runat="server" Text="Bio – Data"
                            CssClass="homemenu"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkViewIdentities" ToolTip="Identities" runat="server" Text="Identities"
                            CssClass="homemenu"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkViewMemberships" ToolTip="Memberships" runat="server" Text="Memberships"
                            CssClass="homemenu"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkViewDependants" ToolTip="Dependants" runat="server" Text="Dependants"
                            CssClass="homemenu"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkViewBenefits" ToolTip="Benefits" runat="server" Text="Benefits"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_BIODATA_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkEmpAssets" ToolTip="Assets" runat="server" Text="Company Assets"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_BIODATA_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkEmployeeSkill" ToolTip="Skills" runat="server" Text="Skills"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_BIODATA_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkQualification" ToolTip="Qualifications" runat="server" Text="Qualifications"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_BIODATA_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkEmpReferee" ToolTip="References" runat="server" Text="References"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_BIODATA_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkEmpExperience" ToolTip="Experience" runat="server" Text="Experiences"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_BIODATA_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:Panel ID="pnlSalaryChange" runat="server">
                            <asp:LinkButton ID="lnkSalaryChange" ToolTip="Salary Change" runat="server" Text="Salary Change Approval"
                                CssClass="homemenu"></asp:LinkButton><br />
                        </asp:Panel>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane3" runat="server">
                    <Header>
                        <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Assessment Module" />--%>
                        <%--<asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Training--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc3.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc3" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;Training</div>--%>
                        <div onclick="ChangeImage('imgAcc3');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc3" src="<%=  Session("imgAcc3").tostring %>" alt="" />&nbsp;&nbsp;Training</div>--%>
                            <img id="imgAcc3" src="../images/plus.png" alt="" />&nbsp;&nbsp;Training</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnlTraining" runat="server" Width="100%">
                            <asp:LinkButton ID="lnkEnrollCancel" ToolTip="Enrollments" runat="server" Text="Enrollments"
                                CssClass="homemenu" OnClientClick="return CheckLic('ON_JOB_TRAINING_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkLevel1Evaluation" ToolTip="Level I Evaluation" runat="server"
                                Text="Level I Evaluation" CssClass="homemenu" OnClientClick="return CheckLic('ON_JOB_TRAINING_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:LinkButton ID="lnkTrainingImpact" ToolTip="Level III Evaluation" runat="server"
                            Text="Level III Evaluation" CssClass="homemenu" OnClientClick="return CheckLic('ON_JOB_TRAINING_MANAGEMENT');"></asp:LinkButton><br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
                    <Header>
                        <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Medical Module" />--%>
                        <%--<asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Medical--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc4.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc4" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;Medical</div>--%>
                        <div onclick="ChangeImage('imgAcc4');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc4" src="<%=  Session("imgAcc4").tostring %>" alt="" />&nbsp;&nbsp;Medical</div>--%>
                            <img id="imgAcc4" src="../images/plus.png" alt="" />&nbsp;&nbsp;Medical</div>
                    </Header>
                    <Content>
                        <asp:LinkButton ID="LnkSickSheet" ToolTip="Sick Sheet" runat="server" Text="Sick Sheet"
                            CssClass="homemenu" OnClientClick="return CheckLic('MEDICAL_BILLS_AND_CLAIMS_MANAGEMENT');"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="LinkMedicalClaim" ToolTip="Medical Claim" runat="server" Text="Medical Claim"
                            CssClass="homemenu" OnClientClick="return CheckLic('MEDICAL_BILLS_AND_CLAIMS_MANAGEMENT');"></asp:LinkButton>
                        <br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane5" runat="server" Visible="false">
                    <Header>
                        <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Assessment Module" />--%>
                        <%--<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Performance Evaluation--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc5.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc5" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;Performance Evaluation</div>--%>
                        <div onclick="ChangeImage('imgAcc5');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc5" src="<%=  Session("imgAcc5").tostring %>" alt="" />&nbsp;&nbsp;Performance Evaluation</div>--%>
                            <img id="imgAcc5" src="../images/plus.png" alt="" />&nbsp;&nbsp;Performance Evaluation</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnlAssessmentEmpWise" runat="server" Width="100%">
                            <asp:LinkButton ID="LinkObjectiveList" ToolTip="Objectives" runat="server" Text="Objectives"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="LinkKpiMeasuresList" ToolTip="KPI/Measures" runat="server" Text="KPI/Measures"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="LinkTargetList" ToolTip="Targets" runat="server" Text="Targets"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="LinkInitiatives_ActionList" ToolTip="Initiatives/Actions" runat="server"
                                Text="Initiatives/Actions" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkBSC_Self" ToolTip="Self Balanced Score Card" runat="server"
                                Text="BSC Self Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkGeneralSelf" ToolTip="Self Assessment" runat="server" Text="General Self Assessment"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnlAssessmentUserwise" runat="server" Width="100%">
                            <asp:LinkButton ID="lnkViewFinalBSC" ToolTip="Approve/Reject/Unlock BSC" runat="server"
                                Text="Approve/Reject/Unlock BSC" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkEmployeeGeneralAssessment" ToolTip="Employee General Assessment"
                                runat="server" Text="Employee General Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkEmployeeBSCAssessment" ToolTip="Employee BSC Assessment" runat="server"
                                Text="Employee BSC Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkGeneralAssessor" ToolTip="Assessor Assessment" runat="server"
                                Text="Assessor Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkGeneralReviewer" ToolTip="Reviewer Assessment" runat="server"
                                Text="Reviewer Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkBSC_Assessor" ToolTip="Assessor Balanced Score Card" runat="server"
                                Text="Assessor Balanced Score Card" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkBSC_Reviewer" ToolTip="Reviewer Balanced Score Card" runat="server"
                                Text="Reviewer Balanced Score Card" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkMigration" ToolTip="Assessor/Reviewer Migration" runat="server"
                                Text="Assessor/Reviewer Migration" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:LinkButton ID="lnkViewGeneralAssessment" ToolTip="View Assessment" runat="server"
                            Text="View Supervisor Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="lnkViewBSCAssessment" ToolTip="View Balanced Score Card" runat="server"
                            Text="View Balanced Score Card" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton><br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane12" runat="server">
                    <Header>
                        <div onclick="ChangeImage('imgAcc12');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px; overflow: auto"
                            class="accordionHeadernew">
                            <img id="imgAcc12" src="../images/plus.png" alt="" />&nbsp;&nbsp;Performance Evaluation</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="plnGoals" runat="server">
                            <asp:LinkButton ID="lnkCmpnyGoals" ToolTip="Company Goals" runat="server" Text="Company Goals"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="lnkAllocationGoals" ToolTip="Allocation Goals" runat="server"
                                Text="Allocation Goals" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:LinkButton ID="lnkMyprmcGoals" ToolTip="My Perfromance Goals" runat="server"
                            Text="My Perfromance Goals" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                        <br />
                        <asp:Panel ID="pnlSelfCompetencies" runat="server">
                            <asp:LinkButton ID="lnkAssignCompetencies" ToolTip="Assign Competencies" runat="server"
                                Text="Assign Competencies" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnlPrefPlanning" runat="server">
                            <asp:LinkButton ID="lnkViewPlanning" ToolTip="Performance Planning" runat="server" Text="Performance Planning"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnlSelf" runat="server">
                            <asp:LinkButton ID="lnkselfAssessmnet" ToolTip="Self-Assessment" runat="server" Text="Self-Assessment"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <%--<asp:LinkButton ID="lnkEmployeeGoals" ToolTip="Employee Goals" runat="server" Text="Employee Goals"
                            CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                        <br />--%>
                        <asp:Panel ID="pnlAssessUser" runat="server">
                            <asp:LinkButton ID="lnkAssessEmployee" ToolTip="Assess Employee" runat="server" Text="Assess Employee"
                                CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="lnkReviewEmployeeAssessment" ToolTip="Review Employee Assessment"
                                runat="server" Text="Review Employee Assessment" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="lnkAssessorReviewerMigration" ToolTip="Assessor/Reviewer Migration"
                                runat="server" Text="Assessor/Reviewer Migration" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnlCustomHeaders" Width="100%" runat="server" Visible="true">
                            <asp:DataList ID="dlCHeaderMenu" runat="server">
                                <ItemTemplate>
                                    <asp:LinkButton ID="objlnkCustomHeader" runat="server" Text='<%# Eval("name") %>'
                                        CommandArgument='<%# Eval("customheaderunkid") %>' OnClick="objlnkCustomHeader_Click"
                                        CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');" />
                                </ItemTemplate>
                            </asp:DataList>
                        </asp:Panel>
                        <asp:LinkButton ID="lnkViewPerformanceEval" ToolTip="View Performance Evaluation"
                            runat="server" Text="View Performance Evaluation" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT');"></asp:LinkButton>
                        <br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane6" runat="server">
                    <Header>
                        <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Assessment Module" />--%>
                        <%-- <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Loan/Advance--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc6.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc6" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;Loan/Advance</div>--%>
                        <div onclick="ChangeImage('imgAcc6');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc6" src="<%=  Session("imgAcc6").tostring %>" alt="" />&nbsp;&nbsp;Loan/Advance</div>--%>
                            <img id="imgAcc6" src="../images/plus.png" alt="" />&nbsp;&nbsp;Payroll</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnlPayrollTransactions" runat="server">
                            <asp:LinkButton ID="lnkEDList" ToolTip="Earning And Deduction" runat="server" Text="Earning And Deduction"
                                CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkbatchpostinlist" ToolTip="Batch Posting List" runat="server"
                                Text="Batch Posting List" CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkGlobalPayment" ToolTip="Payslip Global Payment" runat="server"
                                Text="Payslip Global Payment" CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkGlobalvoidPayment" ToolTip="Global Void Payment" runat="server"
                                Text="Global Void Payment" CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:Panel ID="pnlPaymentApproval" runat="server">
                                <asp:LinkButton ID="lnkPaymentApproval" ToolTip="Payment Approval" runat="server"
                                    Text="Payment Approval" CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:LinkButton ID="lnkAuthorizedPayment" ToolTip="Authorized Payment" runat="server"
                                Text="Authorized Payment" CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                            <asp:LinkButton ID="lnkvoidAuthorizedPayment" ToolTip="Void Authorized Payment" runat="server"
                                Text="Void Authorized Payment" CssClass="homemenu" OnClientClick="return CheckLic('PAYROLL_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:LinkButton ID="lnkLoanAdvance_Application" ToolTip="Loan/Advance Application"
                            runat="server" Text="Loan/Advance Application" CssClass="homemenu" OnClientClick="return CheckLic('LOAN_AND_SAVINGS_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:Panel ID="pnlLoan_Advance_List" runat="server">
                            <asp:LinkButton ID="lnkLoan_Advance" ToolTip="Loan/Advance" runat="server" Text="Loan/Advance List"
                                CssClass="homemenu" OnClientClick="return CheckLic('LOAN_AND_SAVINGS_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnlEmployeeSavings" runat="server">
                            <asp:LinkButton ID="lnkEmployeeSavings" ToolTip="Employee Savings" runat="server"
                                Text="Employee Savings" CssClass="homemenu" OnClientClick="return CheckLic('LOAN_AND_SAVINGS_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <!--<a href="<%=session("servername")%>mforms/Profile.aspx" class="homemenu">Profile Master</a><br />
                        <a href="<%=session("servername")%>mforms/Grade.aspx" class="homemenu">Grade Master</a><br />
                        <a href="<%=session("servername")%>mforms/DeptMst.aspx" class="homemenu">Department Master</a><br />
                        <a href="<%=session("servername")%>mforms/empmaster.aspx" class="homemenu">Employee Master</a><br />
                        <a href="<%=session("servername")%>mforms/taskmaster.aspx" class="homemenu">Task Master</a>-->
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane7" runat="server">
                    <Header>
                        <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Leave Module" />--%>
                        <%-- <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Leave--%>
                        <%--<div onclick="ChangeImage('<%= imgAcc7.ClientID %>');" style="width:242px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">
                        <asp:ImageButton ID="imgAcc7" runat="server" ImageUrl="~/images/plus.png" />&nbsp;&nbsp;Leave</div>--%>
                        <div onclick="ChangeImage('imgAcc7');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc7" src="<%=  Session("imgAcc7").tostring %>" alt="" />&nbsp;&nbsp;Leave</div>--%>
                            <img id="imgAcc7" src="../images/plus.png" alt="" />&nbsp;&nbsp;Leave</div>
                    </Header>
                    <Content>
                        <asp:LinkButton ID="LeaveBalance" ToolTip="Leave Balance" runat="server" Text="Leave Balance"
                            CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton>
                        <br />
                        <asp:Panel ID="pnlLeaveApprover" runat="server">
                            <asp:LinkButton ID="lnkLeaveApprover" ToolTip="Leave Approver" runat="server" Text="Leave Approver"
                                CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnlMigrateApprovers" runat="server">
                            <asp:LinkButton ID="lnkMigrateApprovers" ToolTip="Leave Approver Migration" runat="server"
                                Text="Leave Approver Migration" CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnlLeaveIssueUser" runat="server">
                            <asp:LinkButton ID="lnkLeaveIssueUser" ToolTip="Assign Issue User to Employee" runat="server"
                                Text="Assign Issue User With Employee" CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:LinkButton ID="lnkbtnLeaveApp" ToolTip="Leave Application" runat="server" Text="Leave Application"
                            CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="LinkProcesspendleave" ToolTip="Leave Status" runat="server" Text="Leave Status"
                            CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="LinkLeaveViewer" ToolTip="Leave Viewer" runat="server" Text="Leave Viewer"
                            CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="Linkbtplanner" ToolTip="Leave Planner" runat="server" Text="Leave Planner"
                            CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        <asp:LinkButton ID="LinkleavePlannerViwer" ToolTip="Leave Planner Viewer" runat="server"
                            Text="Leave Planner Viewer" CssClass="homemenu" OnClientClick="return CheckLic('LEAVE_MANAGEMENT');"></asp:LinkButton><br />
                        <!--<a href="<%=session("servername")%>mforms/Profile.aspx" class="homemenu">Profile Master</a><br />
                        <a href="<%=session("servername")%>mforms/Grade.aspx" class="homemenu">Grade Master</a><br />
                        <a href="<%=session("servername")%>mforms/DeptMst.aspx" class="homemenu">Department Master</a><br />
                        <a href="<%=session("servername")%>mforms/empmaster.aspx" class="homemenu">Employee Master</a><br />
                        <a href="<%=session("servername")%>mforms/taskmaster.aspx" class="homemenu">Task Master</a>-->
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane8" runat="server">
                    <Header>
                        <%-- <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Asset Declaration--%>
                        <div onclick="ChangeImage('imgAcc8');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <%--<img id="imgAcc8" src="<%=  Session("imgAcc8").tostring %>" alt="" />&nbsp;&nbsp;Asset Declaration</div>--%>
                            <img id="imgAcc8" src="../images/plus.png" alt="" />&nbsp;&nbsp;Asset Declaration</div>
                    </Header>
                    <Content>
                        <asp:LinkButton ID="lnkEmpAssetDeclarationList" ToolTip="Asset Declaration" runat="server"
                            Text="Asset Declaration" CssClass="homemenu" OnClientClick="return CheckLic('EMPLOYEE_ASSETS_DECLARATIONS');"></asp:LinkButton>
                        <br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane9" runat="server" Height="156px" Visible="false">
                    <Header>
                        <div onclick="ChangeImage('imgAcc9');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <img id="imgAcc9" src="../images/plus.png" alt="" />&nbsp;&nbsp;Reports</div>
                    </Header>
                    <Content>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane10" runat="server">
                    <Header>
                        <div onclick="ChangeImage('imgAcc10');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <img id="imgAcc10" src="../images/plus.png" alt="" />&nbsp;&nbsp;Time & Attendance</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnlViewShiftPolicy" runat="server">
                            <asp:LinkButton ID="lnkViewShiftPolicy" ToolTip="View Assigned Shift/Policy" runat="server"
                                Text="View Assigned Shift/Policy" CssClass="homemenu" OnClientClick="return CheckLic('TIME_AND_ATTENDANCE_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnlGlobalTimesheet" runat="server">
                            <asp:LinkButton ID="lnkGlobalAEDTimeSheet" ToolTip="Global Timesheet" runat="server"
                                Text="Global Timesheet" CssClass="homemenu" OnClientClick="return CheckLic('TIME_AND_ATTENDANCE_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <%-- <ajaxToolkit:AccordionPane ID="AccordionPane4" runat="server">
                <Header>
                <%--<img src="<%=session("servername")%>images/icon_trans.gif" alt="Assessment Module" />
                 <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/images/plus.png" />
                &nbsp;Reports
                </Header>
                <Content>
                
                        <asp:LinkButton ID="LinkButton1" ToolTip="Age Analysis Report" runat="server" Text="Age Analysis Report" CssClass="homemenu"></asp:LinkButton><br />
                                           
                        <!--<a href="<%=session("servername")%>mforms/Profile.aspx" class="homemenu">Profile Master</a><br />
                        <a href="<%=session("servername")%>mforms/Grade.aspx" class="homemenu">Grade Master</a><br />
                        <a href="<%=session("servername")%>mforms/DeptMst.aspx" class="homemenu">Department Master</a><br />
                        <a href="<%=session("servername")%>mforms/empmaster.aspx" class="homemenu">Employee Master</a><br />
                        <a href="<%=session("servername")%>mforms/taskmaster.aspx" class="homemenu">Task Master</a>-->
                </Content>
            </ajaxToolkit:AccordionPane>--%>
                <ajaxToolkit:AccordionPane ID="AccordionPane11" runat="server">
                    <Header>
                        <div onclick="ChangeImage('imgAcc11');" style="width: 238px; height: 20px; padding: 5px;
                            margin-top: 0px; background-color: Transparent; border-width: 0px;" class="accordionHeadernew">
                            <img id="imgAcc11" src="../images/plus.png" alt="" />&nbsp;&nbsp;Recruitment</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnlStaffRequisition" runat="server">
                            <asp:LinkButton ID="lnkStaffRequisition" ToolTip="Staff Requisition" runat="server"
                                Text="Staff Requisition" CssClass="homemenu" OnClientClick="return CheckLic('RECRUITMENT_MANAGEMENT');"></asp:LinkButton>
                            <br />
                            <asp:LinkButton ID="lnkStaffRequisition_Approval" ToolTip="Staff Requisition Approval"
                                runat="server" Text="Staff Requisition Approval" CssClass="homemenu" OnClientClick="return CheckLic('RECRUITMENT_MANAGEMENT');"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:LinkButton ID="lnkApplicantFilterList" ToolTip="Approve Shortlisting Criteria"
                            runat="server" Text="Approve Shortlisting Criteria" CssClass="homemenu" OnClientClick="return CheckLic('RECRUITMENT_MANAGEMENT');"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="lnkFinalApplicant" ToolTip="Approve Final Shortlisted Applicants"
                            runat="server" Text="Approve Final Shortlisted Applicants" CssClass="homemenu"
                            OnClientClick="return CheckLic('RECRUITMENT_MANAGEMENT');"></asp:LinkButton>
                        <br />
                        <asp:LinkButton ID="lnkInterviewAnalysisList" ToolTip="Approve Eligible Applicants"
                            runat="server" Text="Approve Eligible Applicants" CssClass="homemenu" OnClientClick="return CheckLic('RECRUITMENT_MANAGEMENT');"></asp:LinkButton>
                        <br />
                    </Content>
                </ajaxToolkit:AccordionPane>
                
                <ajaxToolkit:AccordionPane ID="AccordionPane13" runat="server">
            <Header>
            <div onclick="ChangeImage('imgAcc13');" style="width:238px;height:20px;padding: 5px; margin-top:0px; background-color:Transparent;border-width:0px;" class="accordionHeadernew">                    
                    <img id="imgAcc13" src="../images/plus.png" alt="" />&nbsp;&nbsp;Claim And Request</div>
            </Header>
            <Content>
                    <asp:Panel ID="pnlClaimApproverTran" runat="server">
                            <asp:LinkButton ID="lnkcrExpenseApprovalList" ToolTip="Expense Approval List" runat="server" Text="Expense Approval List"  CssClass="homemenu" ></asp:LinkButton> <br />                        
                            <asp:LinkButton ID="lnkcrApproverMigration" ToolTip="Approver Migration" runat="server" Text="Approver Migration"  CssClass="homemenu" ></asp:LinkButton> <br /> 
                    </asp:Panel>
                    <asp:LinkButton ID="lnkcrClaimRequest" ToolTip="Claim & Request" runat="server" Text="Claim & Request"  CssClass="homemenu"></asp:LinkButton> <br />
                    <asp:LinkButton ID="lnkcrExpenseApproval" ToolTip="Expense Approval" runat="server" Text="Expense Approval"  CssClass="homemenu"></asp:LinkButton> <br />
                     <asp:Panel ID="pnlClaimPosing" runat="server">
                            <asp:LinkButton ID="lnkPostToPayroll" ToolTip="Post To Payroll" runat="server" Text="Post To Payrol"  CssClass="homemenu" ></asp:LinkButton> <br /> 
                    </asp:Panel>
             </Content>
            </ajaxToolkit:AccordionPane>
            
            </Panes>
        </ajaxToolkit:Accordion>
    </div>
    <div id="tab2" class="tab_content" style="display: none; height: 516px;">
        <ajaxToolkit:Accordion ID="ReportAccordion" SelectedIndex="-1" SkinID="myaccordion"
            runat="server" HeaderCssClass="accordionHeadernew" HeaderSelectedCssClass="accordionHeaderSelectednew"
            ContentCssClass="accordionContentnew" FadeTransitions="true" FramesPerSecond="50"
            TransitionDuration="250" AutoSize="Limit" RequireOpenedPane="false" SuppressHeaderPostbacks="false"
            Width="250px" Height="516px" Style="height: 250px;">
            <Panes>
                <ajaxToolkit:AccordionPane ID="AccordionPane51" runat="server">
                    <Header>
                        <div onclick="ChangeImageReport('imgAcc51');" style="width: 238px; height: 20px;
                            padding: 5px; margin-top: 0px; background-color: Transparent; border-width: 0px;"
                            class="accordionHeadernew">
                            <img id="imgAcc51" src="../images/plus.png" alt="../images/plus.png" />&nbsp;&nbsp;Payroll</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnl_5" runat="server" Width="100%">
                            <asp:LinkButton ID="lnkPayslip" ToolTip="Payslip" runat="server" Text="Payslip" CssClass="homemenu"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_1" runat="server" Width="100%">
                            <asp:LinkButton ID="lnkPayrollReport" ToolTip="Payroll Report" runat="server" Text="Payroll Report"
                                CssClass="homemenu"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_60" runat="server" Width="100%">
                            <asp:LinkButton ID="lnkEdDetailReport" ToolTip="E&D Detail Report" runat="server"
                                Text="E&D Detail Report" CssClass="homemenu"></asp:LinkButton><br />
                        </asp:Panel>
                        <asp:Panel ID="pnlPayrollMSSReport" runat="server" Width="100%">
                            <asp:Panel ID="pnl_2" runat="server">
                                <asp:LinkButton ID="lnkPayrollSummaryReport" ToolTip="Payroll Summary Report" runat="server"
                                    Text="Payroll Summary Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_102" runat="server">
                                <asp:LinkButton ID="lnkPayrollHeadCountReport" ToolTip="Payroll Head Count Report"
                                    runat="server" Text="Payroll Head Count Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_10" runat="server">
                                <asp:LinkButton ID="lnkPayrollVarianceReport" ToolTip="Payroll Variance Report" runat="server"
                                    Text="Payroll Variance Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_11" runat="server">
                                <asp:LinkButton ID="lnkPayrollTotalVarianceReport" ToolTip="Payroll Total Variance Report"
                                    runat="server" Text="Payroll Total Variance Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_48" runat="server">
                                <asp:LinkButton ID="lnkCashPaymentReport" ToolTip="Cash Payment List Report" runat="server"
                                    Text="Cash Payment List Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_6" runat="server">
                                <asp:LinkButton ID="lnkBankPaymentReport" ToolTip="Bank Payment List Report" runat="server"
                                    Text="Bank Payment List Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_22" runat="server">
                                <asp:LinkButton ID="lnkJournalVoucherReport" ToolTip="Journal Voucher Ledger Report"
                                    runat="server" Text="Journal Voucher Ledger Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_97" runat="server">
                                <asp:LinkButton ID="lnkSalaryChangeReport" ToolTip="Employee Salary Change Report"
                                    runat="server" Text="Employee Salary Change Report" CssClass="homemenu"></asp:LinkButton><br />
                            </asp:Panel>
                            <%-- <asp:Panel ID = "pnl_17" runat="server">
                            <asp:LinkButton ID="lnkLoanAdvanceSaving" ToolTip="Loan Advance Saving Report" runat="server" Text="Loan Advance Saving Report" CssClass="homemenu"></asp:LinkButton><br />
                              </asp:Panel>--%>
                        </asp:Panel>
                        <asp:Panel ID="pnl_17" runat="server">
                            <asp:LinkButton ID="lnkLoanAdvanceSaving" ToolTip="Loan Advance Saving Report" runat="server"
                                Text="Loan Advance Saving Report" CssClass="homemenu"></asp:LinkButton><br />
                        </asp:Panel>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane52" runat="server">
                    <Header>
                        <div onclick="ChangeImageReport('imgAcc52');" style="width: 238px; height: 20px;
                            padding: 5px; margin-top: 0px; background-color: Transparent; border-width: 0px;"
                            class="accordionHeadernew">
                            <img id="imgAcc52" src="../images/plus.png" alt="" />&nbsp;&nbsp;Human Resource</div>
                    </Header>
                    <Content>
                        <div style="overflow: visible; height: 139px;">
                            <asp:Panel ID="pnl_28" runat="server">
                                <asp:LinkButton ID="lnkEmpAgeAnalysisReport" ToolTip="Employee Age Analysis Report"
                                    runat="server" Text="Employee Age Analysis Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_51" runat="server">
                                <asp:LinkButton ID="lnkEmpAssestReport" ToolTip="Employee Assets Register Report"
                                    runat="server" Text="Employee Assets Register Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_19" runat="server">
                                <asp:LinkButton ID="lnkEmpBeneficiresReport" ToolTip="Employee Beneficiary Report"
                                    runat="server" Text="Employee Beneficiary Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_75" runat="server">
                                <asp:LinkButton ID="lnkEmpWorkingStation" ToolTip="Employee Class Report" runat="server"
                                    Text="Employee Class Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_67" runat="server">
                                <asp:LinkButton ID="lnkEmpCV" ToolTip="Employee CV Report" runat="server" Text="Employee CV Report"
                                    CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_33" runat="server">
                                <asp:LinkButton ID="lnkEmpDetails" ToolTip="Employee Detail Report" runat="server"
                                    Text="Employee Detail Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_29" runat="server">
                                <asp:LinkButton ID="lnkEmpDistributionReport" ToolTip="Employee Distribution Report"
                                    runat="server" Text="Employee Distribution Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_3" runat="server">
                                <asp:LinkButton ID="lnkEmpHeadCount" ToolTip="Employee Head Count Report" runat="server"
                                    Text="Employee Head Count Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_65" runat="server">
                                <asp:LinkButton ID="lnkEmpWithNoDOB" ToolTip="Employee List With No Date of Birth Report"
                                    runat="server" Text="Employee List With No Date of Birth Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_58" runat="server">
                                <asp:LinkButton ID="lnkEmpListing" ToolTip="Employee Listing Report" runat="server"
                                    Text="Employee Listing Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_56" runat="server">
                                <asp:LinkButton ID="lnkEmpMovement" ToolTip="Employee Movement Report" runat="server"
                                    Text="Employee Movement Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_95" runat="server">
                                <asp:LinkButton ID="lnkEmpWOEmail" ToolTip="Employee Without Email Report" runat="server"
                                    Text="Employee Without Email Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_81" runat="server">
                                <asp:LinkButton ID="lnkEmpWOMembership" ToolTip="Employee Without Membership Report"
                                    runat="server" Text="Employee Without Membership Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_93" runat="server">
                                <asp:LinkButton ID="lnkEmpEOC" ToolTip="End of Contract Report" runat="server" Text="End of Contract Report"
                                    CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_47" runat="server">
                                <asp:LinkButton ID="lnkEmpJobHistory" ToolTip="Job Employeement History Report" runat="server"
                                    Text="Job Employment History Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_78" runat="server">
                                <asp:LinkButton ID="lnkManningLevel" ToolTip="Manning Level Report" runat="server"
                                    Text="Manning Level Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_30" runat="server">
                                <asp:LinkButton ID="lnkEmpPhysicalCount" ToolTip="Employee Monthly Physical Report"
                                    runat="server" Text="Employee Monthly Physical Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_68" runat="server">
                                <asp:LinkButton ID="lnkNewhiredEmp" ToolTip="Newly Hired Employees Report" runat="server"
                                    Text="Newly Hired Employees Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_69" runat="server">
                                <asp:LinkButton ID="lnkTerminatedEmp" ToolTip="Terminated Employees Report" runat="server"
                                    Text="Terminated Employees Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_66" runat="server">
                                <asp:LinkButton ID="lnkEmpPesonalParticulars" ToolTip="Employee Personal Particulars Report"
                                    runat="server" Text="Employee Personal Particulars Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_70" runat="server">
                                <asp:LinkButton ID="lnkEmpQualificationrange" ToolTip="Employee Qualification Range Report"
                                    runat="server" Text="Employee Qualification Range Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_20" runat="server">
                                <asp:LinkButton ID="lnkEmpQualification" ToolTip="Employee Qualification Report"
                                    runat="server" Text="Employee Qualification Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_21" runat="server">
                                <asp:LinkButton ID="lnkEmpRefereeRpt" ToolTip="Employee Referee Report" runat="server"
                                    Text="Employee Referee Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_34" runat="server">
                                <asp:LinkButton ID="lnkEmpRelation" ToolTip="Employee Relation Report" runat="server"
                                    Text="Employee Relation Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_64" runat="server">
                                <asp:LinkButton ID="lnkEmpRetirement" ToolTip="Employee Retirement Report" runat="server"
                                    Text="Employee Retirement Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_76" runat="server">
                                <asp:LinkButton ID="lnkEmpSalaryGrade" ToolTip="Employee Salary Grade Report" runat="server"
                                    Text="Employee Salary Grade Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_18" runat="server">
                                <asp:LinkButton ID="lnkEmpSkill" ToolTip="Employee Skills Report" runat="server"
                                    Text="Employee Skills Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <%-- SANDEEP 12 NOV 2012 --%>
                            <asp:Panel ID="pnl_83" runat="server">
                                <asp:LinkButton ID="lnkEmployeeListLocation" ToolTip="Employee List Location" runat="server"
                                    Text="Employee List Location" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_86" runat="server">
                                <asp:LinkButton ID="lnkEmpdispoition" ToolTip="Employee Disposition Report" runat="server"
                                    Text="Employee Disposition Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_110" runat="server">
                                <asp:LinkButton ID="lnkEmpDuration" ToolTip="Employee Duration" runat="server" Text="Employee Duration"
                                    CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_115" runat="server">
                                <asp:LinkButton ID="lnkEmpWoImg" ToolTip="Employee Without Image Report" runat="server"
                                    Text="Employee Without Image Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_116" runat="server">
                                <asp:LinkButton ID="lnkEmpWoGender" ToolTip="Employee Without Gender Report" runat="server"
                                    Text="Employee Without Gender Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_121" runat="server">
                                <asp:LinkButton ID="lnkEmpIdentity" ToolTip="Employee Identities Report" runat="server"
                                    Text="Employee Identities Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                            <asp:Panel ID="pnl_122" runat="server">
                                <asp:LinkButton ID="lnkEmpMembership" ToolTip="Employee Membership Report" runat="server"
                                    Text="Employee Membership Report" CssClass="homemenu"></asp:LinkButton>
                                <br />
                            </asp:Panel>
                        </div>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane53" runat="server">
                    <Header>
                        <div onclick="ChangeImageReport('imgAcc53');" style="width: 238px; height: 20px;
                            padding: 5px; margin-top: 0px; background-color: Transparent; border-width: 0px;"
                            class="accordionHeadernew">
                            <img id="imgAcc53" src="../images/plus.png" alt="" />&nbsp;&nbsp;Medical</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnl_59" runat="server">
                            <asp:LinkButton ID="lnkMedicalClaim" ToolTip="Medical Claim Report" runat="server"
                                Text="Medical Claim Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_103" runat="server">
                            <asp:LinkButton ID="lnkMedicalBillSummary" ToolTip="Medical Bill Summary" runat="server"
                                Text="Medical Bill Summary" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane54" runat="server">
                    <Header>
                        <div onclick="ChangeImageReport('imgAcc54');" style="width: 238px; height: 20px;
                            padding: 5px; margin-top: 0px; background-color: Transparent; border-width: 0px;"
                            class="accordionHeadernew">
                            <img id="imgAcc54" src="../images/plus.png" alt="" />&nbsp;&nbsp;Leave</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnl_108" runat="server">
                            <asp:LinkButton ID="lnkLeaveStatementReport" ToolTip="Leave Statement Report" runat="server"
                                Text="Leave Statement Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_92" runat="server">
                            <asp:LinkButton ID="lnkEmplopyeeLeaveFormReport" ToolTip="Employee Leave Form Report"
                                runat="server" Text="Employee Leave Form Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_13" runat="server">
                            <asp:LinkButton ID="lnkLeaveBalListReport" ToolTip="Employee Leave Balance List Report"
                                runat="server" Text="Employee Leave Balance List Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_137" runat="server">
                            <asp:LinkButton ID="lnkLeavePlanReport" ToolTip="Leave Planner Report" runat="server"
                                Text="Leave Planner Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_150" runat="server">
                            <asp:LinkButton ID="lnkLvFormApprovalStatus" ToolTip="Leave Form Approval Status Report"
                                runat="server" Text="Leave Form Approval Status Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_151" runat="server">
                            <asp:LinkButton ID="lnkEmpIssuedLeaveStatus" ToolTip="Issued Leave Status Report"
                                runat="server" Text="Issued Leave Status Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_105" runat="server">
                            <asp:LinkButton ID="lnkLeaveApproverReport" ToolTip="Leave Approver Report" runat="server"
                                Text="Leave Approver Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_101" runat="server">
                            <asp:LinkButton ID="lnkEmpListWithoutLeaveApprvers" ToolTip="Employee List without Leave Approvers"
                                runat="server" Text="Employee List without Leave Approvers" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                        <asp:Panel ID="pnl_12" runat="server">
                            <asp:LinkButton ID="lnkEmpLeaveAbsSummaryRpt" ToolTip="Employee Leave Absence Summary"
                                runat="server" Text="Employee Leave Absence Summary" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                    </Content>
                </ajaxToolkit:AccordionPane>
                <ajaxToolkit:AccordionPane ID="AccordionPane55" runat="server">
                    <Header>
                        <div onclick="ChangeImageReport('imgAcc55');" style="width: 238px; height: 20px;
                            padding: 5px; margin-top: 0px; background-color: Transparent; border-width: 0px;"
                            class="accordionHeadernew">
                            <img id="imgAcc55" src="../images/plus.png" alt="" />&nbsp;&nbsp; Time & Attendance</div>
                    </Header>
                    <Content>
                        <asp:Panel ID="pnl_38" runat="server">
                            <asp:LinkButton ID="lnkEmpTimesheetReport" ToolTip="Employee Timesheet Report" runat="server"
                                Text="Employee Timesheet Report" CssClass="homemenu"></asp:LinkButton>
                            <br />
                        </asp:Panel>
                    </Content>
                </ajaxToolkit:AccordionPane>
            </Panes>
        </ajaxToolkit:Accordion>
    </div>
</div>
