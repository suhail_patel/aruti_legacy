﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ZoomImage.ascx.vb" Inherits="Controls_ZoomImage" %>
<script type="text/javascript">

    var tempX = 0;
    var tempY = 0;

    // Detect if the browser is IE or not.
    // If it is not IE, we assume that the browser is NS.
    var IE = document.all ? true : false;

    // If NS -- that is, !IE -- then set up for mouse capture
    if (!IE) document.captureEvents(Event.MOUSEMOVE);

    
//    function pageLoad(sender, args) {
//        document.onmousemove = getMouseXY;
//    }

    function getMouseXY(e) {
        if (IE) { // grab the x-y pos.s if browser is IE
            tempX = event.clientX + document.body.scrollLeft;
            tempY = event.clientY + document.body.scrollTop;
        } else {  // grab the x-y pos.s if browser is NS
            tempX = e.pageX;
            tempY = e.pageY;
        }
        // catch possible negative values in NS4
        if (tempX < 0) { tempX = 0 }
        if (tempY < 0) { tempY = 0 }
        return true
    }

    function ShowToolTip(img) {
        var posx = 0;
        var posy = 0;

        if (!e) var e = window.event;

        if ((img.mimeType != '' && img.mimeType != 'undefined') && img.complete && (img.src.indexOf('.ashx?id=') >= 0 || img.src.toLowerCase().lastIndexOf('.bmp') >= 0 || img.src.toLowerCase().lastIndexOf('.jpeg') >= 0 || img.src.toLowerCase().lastIndexOf('.jpg') >= 0 || img.src.toLowerCase().lastIndexOf('.gif') >= 0 || img.src.toLowerCase().lastIndexOf('.png') >= 0)) {

            document.getElementById('img_tool').style.width = '<%= ZoomPercentage %>'+'%';
            document.getElementById('img_tool').style.height = '<%= ZoomPercentage %>' + '%';
            
            document.getElementById('div_img').style.visibility = 'visible';
            document.getElementById('img_tool').src = img.src;

            document.getElementById('div_img').style.position = 'fixed';

            if (screen.width < (tempX + document.getElementById('img_tool').clientWidth))
                document.getElementById('div_img').style.left = (tempX - document.getElementById('img_tool').clientWidth) + 'px';            
            else
                document.getElementById('div_img').style.left = tempX + 'px';
                
            document.getElementById('div_img').style.top = tempY + 'px';

            document.getElementById('div_img').style.zIndex = "100";
        }
    }

    function HideToolTip() {
        document.getElementById("div_img").style.visibility = "hidden";
    }
</script>

<asp:Image ID="imgZoom" runat="server" ImageAlign="Middle" style="width: 115px; height:110px;z-index:999;" Visible="true" />
<div id="div_img" style="height:100px;width:100px;border:solid 0px black;position:absolute;visibility:hidden;top:0px;left:0px;z-index:1;">

   <img id="img_tool" src="" alt="" height="250%" width="250%" />

  </div>