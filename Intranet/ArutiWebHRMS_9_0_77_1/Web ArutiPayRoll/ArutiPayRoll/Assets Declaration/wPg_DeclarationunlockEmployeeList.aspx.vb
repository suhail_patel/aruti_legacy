﻿Option Strict On

Imports Aruti.Data
Imports System.Data

Partial Class Assets_Declaration_wPg_AssetDecunlockEmployee
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmAssetDecunlockEmployee"
    Private mdtUnlockEmpList As DataTable = Nothing
    Private objlockunlockEmp As New clsassetdecemp_lockunlock
    Private mblnBlank As Boolean = False
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                FillList()
            Else
                mdtUnlockEmpList = CType(Me.ViewState("dtUnlockEmpList"), DataTable)
                mblnBlank = CBool(Me.ViewState("mblnBlank"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreInit : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("dtUnlockEmpList") = mdtUnlockEmpList
            Me.ViewState("mblnBlank") = mblnBlank
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim strfilter As String = ""
        Try

            dsList = objlockunlockEmp.GetList()

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dc As New DataColumn("ischeck", Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                dsList.Tables(0).Columns.Add(dc)
            End If
            mdtUnlockEmpList = dsList.Tables(0).Copy()
            dsList.Clear()
            dsList = Nothing

            If mdtUnlockEmpList.Rows.Count <= 0 Then
                Dim dr As DataRow = mdtUnlockEmpList.NewRow()
                dr("ischeck") = False
                mdtUnlockEmpList.Rows.Add(dr)
                mblnBlank = True
            End If

            GvLockEmployeeList.DataSource = mdtUnlockEmpList
            GvLockEmployeeList.DataBind()

            If mblnBlank Then
                GvLockEmployeeList.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("FillList :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            If mdtUnlockEmpList Is Nothing Then Exit Sub
            Dim xCount As Integer = mdtUnlockEmpList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count
            If xCount <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select atleast one employee to do further operation on it"), Me)
                Exit Sub
            End If

            popup_YesNo.Title = Language.getMessage(mstrModuleName, 2, "Confirmation")
            popup_YesNo.Message = Language.getMessage(mstrModuleName, 3, "Are you sure you want to unlock checked employee(s)?")
            popup_YesNo.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnUnlock_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError("btnUnlock_Click :" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False

            objlockunlockEmp._Islock = False
            objlockunlockEmp._Isunlocktenure = True
            objlockunlockEmp._Unlockuserunkid = CInt(Session("UserId"))
            objlockunlockEmp._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime

            objlockunlockEmp._AuditUserunkid = CInt(Session("UserId"))
            objlockunlockEmp._AuditDateTime = ConfigParameter._Object._CurrentDateAndTime
            objlockunlockEmp._ClientIp = Session("IP_ADD").ToString()
            objlockunlockEmp._HostName = Session("HOST_NAME").ToString()
            objlockunlockEmp._FormName = mstrModuleName
            objlockunlockEmp._IsFromWeb = True

            Dim dtTable As DataTable = New DataView(mdtUnlockEmpList, "ischeck = 1", "", DataViewRowState.CurrentRows).ToTable()

            If objlockunlockEmp.GlobalUnlockEmployee(dtTable) = False Then
                DisplayMessage.DisplayMessage(objlockunlockEmp._Message, Me)
                Exit Sub
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Checked employee(s) unlocked successfully."), Me)
            End If
            mdtUnlockEmpList.Clear()
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonYes_Click:" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click " + ex.Message, Me)
        End Try

    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub GvLockEmployeeList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvLockEmployeeList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvLockEmployeeList, "colhLockDateTime", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvLockEmployeeList, "colhLockDateTime", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvLockEmployeeList, "colhLockDateTime", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvLockEmployeeList, "colhLockDateTime", False, True)).Text).ToShortDateString() & " " & CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvLockEmployeeList, "colhLockDateTime", False, True)).Text).ToString("HH:mm")
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("GvLockEmployeeList_RowDataBound :" + ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            If mdtUnlockEmpList.Rows.Count <= 1 AndAlso mblnBlank Then Exit Sub '1 STANDS HIDDEN BLANK ROW.

            For Each item As GridViewRow In GvLockEmployeeList.Rows
                If item.RowType = DataControlRowType.DataRow Then
                    CType(item.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                    mdtUnlockEmpList.Rows(item.RowIndex)("ischeck") = chkSelectAll.Checked
                End If
            Next
            mdtUnlockEmpList.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If GvLockEmployeeList.Rows.Count <= 0 Then Exit Sub

            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)

            Dim dsRow As DataRow() = mdtUnlockEmpList.Select("adlockunkid = " & CInt(GvLockEmployeeList.DataKeys(item.RowIndex).Values("adlockunkid")))
            If dsRow.Length > 0 Then
                For Each dR In dsRow
                    dR.Item("ischeck") = chkSelect.Checked
                Next
            End If
            mdtUnlockEmpList.AcceptChanges()

            Dim xCount As DataRow() = mdtUnlockEmpList.Select("ischeck=True")
            If xCount.Length = GvLockEmployeeList.Rows.Count Then
                CType(GvLockEmployeeList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(GvLockEmployeeList.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelect_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            Language._Object.setCaption(Me.btnUnlock.ID, Me.btnUnlock.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            Language._Object.setCaption(GvLockEmployeeList.Columns(1).FooterText, GvLockEmployeeList.Columns(1).HeaderText)
            Language._Object.setCaption(GvLockEmployeeList.Columns(2).FooterText, GvLockEmployeeList.Columns(2).HeaderText)
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage " + Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader2.Text = Language._Object.getCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)

            Me.btnUnlock.Text = Language._Object.getCaption(Me.btnUnlock.ID, Me.btnUnlock.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            GvLockEmployeeList.Columns(1).HeaderText = Language._Object.getCaption(GvLockEmployeeList.Columns(1).FooterText, GvLockEmployeeList.Columns(1).HeaderText)
            GvLockEmployeeList.Columns(2).HeaderText = Language._Object.getCaption(GvLockEmployeeList.Columns(2).FooterText, GvLockEmployeeList.Columns(2).HeaderText)
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage " + Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please Select atleast one employee to do further operation on it")
			Language.setMessage(mstrModuleName, 2, "Confirmation")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to unlock checked employee(s)?")

		Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage " + Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
