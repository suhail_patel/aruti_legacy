﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class HR_wPg_EmployeeAssetDeclarationList
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Dim mintAssetDeclarationUnkid As Integer = -1
    Dim strSearching As String = ""

    Dim dsAssetList As DataSet

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Dim dtBank As DataTable
    'Dim dtShare As DataTable
    'Dim dtHouse As DataTable
    'Dim dtPark As DataTable
    'Dim dtVehicles As DataTable
    'Dim dtMachinery As DataTable
    'Dim dtOtherBusiness As DataTable
    'Hemant (24 Dec 2019) -- End

    'Pinkal (30-Jan-2013) -- Start
    'Enhancement : TRA Changes
    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Dim dtResources As DataTable
    'Dim dtDebt As DataTable
    'Hemant (24 Dec 2019) -- End
    'Pinkal (30-Jan-2013) -- End


    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Dim dsCurrBank As DataSet
    'Dim dsCurrShare As DataSet
    'Dim dsCurrHouse As DataSet
    'Dim dsCurrPark As DataSet
    'Dim dsCurrVehicles As DataSet
    'Dim dsCurrMachinery As DataSet
    'Dim dsCurrOtherBusiness As DataSet
    'Hemant (24 Dec 2019) -- End
    'Sohail (06 Apr 2012) -- End


    'Pinkal (30-Jan-2013) -- Start
    'Enhancement : TRA Changes
    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Dim dsCurrResources As DataSet
    'Dim dsCurrDebt As DataSet
    'Hemant (24 Dec 2019) -- End
    'Pinkal (30-Jan-2013) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Shared ReadOnly mstrModuleName As String = "frmEmpAssetDeclarationList"
    Private Shared ReadOnly mstrModuleName1 As String = "frmEmpAssetDeclaration"
    'Anjan [04 June 2014 ] -- End


#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        Try


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes

            Dim objEmployee As New clsEmployee_Master

            If (Session("loginBy") = Global.User.en_loginby.Employee) Then

                'drpEmployee.BindEmployee(CInt(Session("Employeeunkid")))
                'drpEmpListAddEdit.BindEmployee(CInt(Session("Employeeunkid")))

                Dim objglobalassess = New GlobalAccess

                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()

                drpEmpListAddEdit.DataSource = objglobalassess.ListOfEmployee.Copy
                drpEmpListAddEdit.DataTextField = "loginname"
                drpEmpListAddEdit.DataValueField = "employeeunkid"
                drpEmpListAddEdit.DataBind()

            Else
                'drpEmployee.BindEmployee()
                'drpEmpListAddEdit.BindEmployee()
                Dim dsList As DataSet = Nothing

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If
                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Employee", True)
                'Shani(24-Aug-2015) -- End
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = 0
                End With

                With drpEmpListAddEdit
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("Employee").Copy
                    .DataBind()
                    .SelectedValue = 0
                End With


            End If

            Dim objExchange As New clsExchangeRate
            Dim dsCombos As DataSet = objExchange.getComboList("BaseCurr", False, True)
            If dsCombos.Tables("BaseCurr").Rows.Count > 0 Then
                Me.ViewState.Add("Exchangerateunkid", CInt(dsCombos.Tables("BaseCurr").Rows(0).Item("exchangerateunkid")))
                Me.ViewState.Add("currencysign", dsCombos.Tables("BaseCurr").Rows(0).Item("currency_sign").ToString)
            End If

            'Pinkal (30-Jan-2013) -- End

            If CInt(drpEmpListAddEdit.SelectedValue) > 0 Then
                'Dim objEmployee As New clsEmployee_Master


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(drpEmpListAddEdit.SelectedValue)
                'Shani(20-Nov-2015) -- End


                txtEmployeeCode.Text = objEmployee._Employeecode

                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmployee._Departmentunkid
                txtDepartment.Text = objDept._Name
                objDept = Nothing

                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                txtOfficePosition.Text = objJob._Job_Name
                objJob = Nothing

                'Sohail (02 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                txtWorkStation.Text = objClass._Name
                objClass = Nothing
                'Sohail (02 Apr 2012) -- End

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                If objEmployee._Appointeddate.Date > Session("fin_startdate") Then
                    'If objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
                    'Sohail (23 Apr 2012) -- End
                    Session("FinStartDate") = objEmployee._Appointeddate.Date
                Else
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Session("FinStartDate") = FinancialYear._Object._Database_Start_Date.Date
                    Session("FinStartDate") = Session("fin_startdate")
                    'Sohail (23 Apr 2012) -- End
                End If

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Dim EndDate As Date = FinancialYear._Object._Database_End_Date.Date
                Dim EndDate As Date = Session("fin_enddate")
                'Sohail (23 Apr 2012) -- End
                If objEmployee._Termination_From_Date.Date <> Nothing Then
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < FinancialYear._Object._Database_End_Date.Date, objEmployee._Termination_From_Date.Date, FinancialYear._Object._Database_End_Date.Date))
                    EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < Session("fin_enddate"), objEmployee._Termination_From_Date.Date, Session("fin_enddate")))
                    'Sohail (23 Apr 2012) -- End
                End If
                If objEmployee._Termination_To_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate))
                End If

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                If EndDate <> Session("fin_enddate") Then
                    'If EndDate <> FinancialYear._Object._Database_End_Date.Date Then
                    'Sohail (23 Apr 2012) -- End
                    Session("FinEndDate") = EndDate
                Else
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Session("FinEndDate") = FinancialYear._Object._Database_End_Date.Date
                    Session("FinEndDate") = Session("fin_enddate")
                    'Sohail (23 Apr 2012) -- End
                End If
            Else
                txtEmployeeCode.Text = ""
                txtDepartment.Text = ""
                txtOfficePosition.Text = ""
                txtWorkStation.Text = ""
                dtpDate.SetDate = DateAndTime.Today.Date 'Sohail (06 Apr 2012)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData 'SHANI (16 Apr 2015)
        'Sohail (06 Oct 2017) -- Start
        'TRA Issue - 68.1 / 70.1 - Asset Declaration is not coming on ESS.
        Dim blnApplyAccessFilter As Boolean = True
        'Sohail (06 Oct 2017) -- End

        Try

            'Anjan [24 February 2015] -- Start
            'ENHANCEMENT : Implementing session privileges for MSS
            If Session("LoginBy") = Global.User.en_loginby.User Then
                If CBool(Session("ViewAssetsDeclarationList")) = False Then Exit Sub
                'Sohail (06 Oct 2017) -- Start
                'TRA Issue - 68.1 / 70.1 - Asset Declaration is not coming on ESS.
            Else
                blnApplyAccessFilter = False
                'Sohail (06 Oct 2017) -- End
            End If
            'Anjan [24 February 2015] -- End

            'Sohail (27 Feb 2015) -- Start
            'Enhancement - Unlock Final Save in Self Service.
            If CInt(drpEmployee.SelectedValue) <= 0 Then Exit Try
            'Sohail (27 Feb 2015) -- End

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'dsAssetList = clsAssetdeclaration_master.GetList("List", , CInt(drpEmployee.SelectedValue))
            'SHANI (16 APR 2015)-START
            'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
            'dsAssetList = clsAssetdeclaration_master.GetList("List", , CInt(drpEmployee.SelectedValue), , Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
            Dim dicDB As New Dictionary(Of String, String)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim dicDBDate As New Dictionary(Of String, String)
            Dim dicDBYearId As New Dictionary(Of String, Integer)
            'Shani(20-Nov-2015) -- End

            If chkIncludeClosedYearTrans.Checked = True Then
                Dim ds As DataSet = objMaster.Get_Database_Year_List("List", True, Session("CompanyUnkId"))
                dicDB = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearName = p.Item("financialyear_name").ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.YearName)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                dicDBDate = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .Date = (p.Item("start_date").ToString & "|" & p.Item("end_date").ToString).ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.Date)
                dicDBYearId = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearId = CInt(p.Item("yearunkid"))}).ToDictionary(Function(x) x.DBName, Function(y) y.YearId)
                'Shani(20-Nov-2015) -- End

            Else
                dicDB.Add(Session("Database_Name"), Session("FinancialYear_Name"))
                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                dicDBDate.Add(Session("Database_Name"), (eZeeDate.convertDate(Session("fin_startdate")) & "|" & eZeeDate.convertDate(Session("fin_enddate"))).ToString)
                dicDBYearId.Add(Session("Database_Name"), Session("Fin_year"))
                'Shani(20-Nov-2015) -- End
            End If

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsAssetList = clsAssetdeclaration_master.GetList("List", dicDB, , CInt(drpEmployee.SelectedValue), , Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), , , "A.employeename")
            'Sohail (06 Oct 2017) -- Start
            'TRA Issue - 68.1 / 70.1 - Asset Declaration is not coming on ESS.
            'dsAssetList = clsAssetdeclaration_master.GetList(Session("UserId"), _
            '                                                 Session("CompanyUnkId"), _
            '                                                 Session("UserAccessModeSetting"), True, _
            '                                                 Session("IsIncludeInactiveEmp"), "List", _
            '                                                 dicDB, dicDBDate, dicDBYearId, , _
            '                                                 CInt(drpEmployee.SelectedValue), , , , , , "A.employeename")
            dsAssetList = clsAssetdeclaration_master.GetList(Session("UserId"), _
                                                             Session("CompanyUnkId"), _
                                                             Session("UserAccessModeSetting"), True, _
                                                             Session("IsIncludeInactiveEmp"), "List", _
                                                             dicDB, dicDBDate, dicDBYearId, , _
                                                             CInt(drpEmployee.SelectedValue), , , , , , "A.employeename", blnApplyAccessFilter)
            'Sohail (06 Oct 2017) -- End
            'Shani(20-Nov-2015) -- End

            'SHANI (16 APR 2015)--END 
            'Sohail (23 Apr 2012) -- End

            'Sohail (27 Feb 2015) -- Start
            'Enhancement - Unlock Final Save in Self Service.
            'If (Not dsAssetList Is Nothing) Then

            '    If dsAssetList.Tables(0).Rows.Count = 0 Then
            '        Dim r As DataRow = dsAssetList.Tables(0).NewRow

            '        r.Item(3) = "None" ' To Hide the row and display only Row Header
            '        r.Item(4) = ""
            '        dsAssetList.Tables(0).Rows.Add(r)
            '    End If
            '    gvList.DataSource = dsAssetList
            '    gvList.DataBind()

            'Else
            '    DisplayMessage.DisplayMessage("FillList :-  ", Me)
            '    Exit Sub
            'End If
            'Sohail (27 Feb 2015) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            If (Not dsAssetList Is Nothing) Then

                If dsAssetList.Tables(0).Rows.Count = 0 Then
                    Dim r As DataRow = dsAssetList.Tables(0).NewRow

                    r.Item(3) = "None" ' To Hide the row and display only Row Header
                    r.Item(4) = ""
                    dsAssetList.Tables(0).Rows.Add(r)
                End If
                gvList.DataSource = dsAssetList
                gvList.DataBind()

            Else
                DisplayMessage.DisplayMessage("FillList :-  ", Me)
            End If
        End Try
    End Sub


    'Pinkal (30-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub SetLabelRate()
        Try
            Select Case mvwAssetDeclaration.ActiveViewIndex

                Case 0
                    lblExchangeRate.Text = ""
                Case 1
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyBank, New EventArgs())
                Case 2
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyShare, New EventArgs())
                Case 3
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyHouse, New EventArgs())
                Case 4
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyPark, New EventArgs())
                Case 5
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyVehicle, New EventArgs())
                Case 6
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyMachine, New EventArgs())
                Case 7
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyBusiness, New EventArgs())
                Case 8
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyResources, New EventArgs())
                Case 9
                    drpCurrencyBank_SelectedIndexChanged(drpCurrencyDebt, New EventArgs())

            End Select
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLabelRate:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetLabelRate:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    'Private Sub Update_DataGridview_DataTable_Extra_Columns(ByVal objdgview As GridView, ByRef drRow As DataRow, ByVal intCountryId As Integer)
    '    Try

    '        drRow.Item("userunkid") = -1

    '        If IsDBNull(drRow.Item("transactiondate")) Or drRow.Item("transactiondate") Is Nothing Then
    '            drRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
    '        End If

    '        Dim objExRate As New clsExchangeRate
    '        Dim dsList As DataSet
    '        Dim decBaseRate As Decimal = 0
    '        Dim decPaidRate As Decimal = 0
    '        Dim decPaidAmt As Decimal = 0
    '        Dim decPaidDividendAmt As Decimal = 0

    '        dsList = objExRate.GetList("ExRate", True, , , intCountryId, True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
    '        If dsList.Tables("ExRate").Rows.Count > 0 Then
    '            drRow.Item("currencyunkid") = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
    '            drRow.Item("basecurrencyid") = Me.ViewState("Exchangerateunkid")
    '            decBaseRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
    '            decPaidRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))

    '            If objdgview.ID = gvBank.ID Then
    '                decPaidAmt = drRow.Item("amount")
    '            ElseIf objdgview.ID = gvShareDividend.ID Then

    '                decPaidAmt = drRow.Item("sharevalue")
    '                decPaidDividendAmt = drRow.Item("dividendamount")

    '            Else
    '                decPaidAmt = drRow.Item("value")
    '            End If

    '            drRow.Item("baseexchangerate") = decBaseRate
    '            drRow.Item("expaidrate") = decPaidRate

    '            If decPaidRate <> 0 Then
    '                drRow.Item("baseamount") = decPaidAmt * decBaseRate / decPaidRate
    '                If objdgview.ID = gvShareDividend.ID Then
    '                    drRow.Item("basedividendamount") = decPaidDividendAmt * decBaseRate / decPaidRate
    '                End If
    '            Else
    '                drRow.Item("baseamount") = decPaidAmt * decBaseRate
    '                If objdgview.ID = gvShareDividend.ID Then
    '                    drRow.Item("basedividendamount") = decPaidDividendAmt * decBaseRate
    '                End If
    '            End If

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Update_DataGridview_DataTable_Extra_Columns :- " & ex.Message, Me)
    '    End Try
    'End Sub

    Private Function GetCurrencyRate(ByVal intCountryUnkID As Integer, ByRef decBaseExRate As Decimal, ByRef decPaidExRate As Decimal, ByRef intPaidCurrencyunkid As Integer) As Boolean
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objExRate.GetList("ExRate", True, , , intCountryUnkID, True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            decBaseExRate = 0
            decPaidExRate = 0
            intPaidCurrencyunkid = 0
            If dtTable.Rows.Count > 0 Then
                decBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                decPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                intPaidCurrencyunkid = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError("GetCurrencyRate:- " & ex.Message, Me)
        End Try
    End Function
    'Hemant (24 Dec 2019) -- End


    'Pinkal (30-Jan-2013) -- End

    'Sohail (13 May 2013) -- Start
    'TRA - ENHANCEMENT
    Private Function IsValidData() As Boolean
        Try

            lblExchangeRate.Text = ""
            lblError.Text = ""

            If CInt(drpEmpListAddEdit.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError("Emplolyee is compulsory information.Please Select Emplolyee.", Me)
                DisplayMessage.DisplayMessage("Emplolyee is compulsory information.Please Select Emplolyee.", Me)
                'Sohail (23 Mar 2019) -- End
                drpEmpListAddEdit.Focus()
                Return False
            End If

            If mintAssetDeclarationUnkid <= 0 Then
                Dim objAssetDeclare As New clsAssetdeclaration_master

                If objAssetDeclare.isExist(CInt(drpEmpListAddEdit.SelectedValue)) = True Then
                    lblError.Text = "Sorry! Asset declaration for this employee is already exist."
                    drpEmpListAddEdit.Focus()
                    popupAddEdit.Show()
                    Return False
                End If

                'Hemant (01 Mar 2022) -- Start            
                If CheckUnLockEmployee(CInt(drpEmpListAddEdit.SelectedValue)) = False Then Return False
                'Hemant (01 Mar 2022) -- End
            End If

            If dtpDate.IsNull = True Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError("Please enter Transaction Date.", Me)
                DisplayMessage.DisplayMessage("Please enter Transaction Date.", Me)
                'Sohail (23 Mar 2019) -- End
                popupAddEdit.Show()
                dtpDate.Focus()
                Return False
            ElseIf dtpDate.GetDate < Session("fin_startdate") OrElse dtpDate.GetDate > Session("fin_enddate") Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError("Sorry! Transaction Date should be in between " & Session("fin_startdate") & " AND " & Session("fin_enddate") & ".", Me)
                DisplayMessage.DisplayMessage("Sorry! Transaction Date should be in between " & Session("fin_startdate") & " AND " & Session("fin_enddate") & ".", Me)
                'Sohail (23 Mar 2019) -- End
                popupAddEdit.Show()
                dtpDate.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidData :- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidData :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
    'Sohail (13 May 2013) -- End

    'Hemant (01 Mar 2022) -- Start            
    Private Function CheckUnLockEmployee(ByVal xEmployeeId As Integer) As Boolean
        Try
            Dim mdtDeclarationStartDate As Date = Nothing
            Dim mdtDeclarationEndDate As Date = Nothing

            If (Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").ToString().Length > 0) Then
                mdtDeclarationStartDate = eZeeDate.convertDate(Session("AssetDeclarationFromDate").ToString()).Date
            End If
            If Session("AssetDeclarationToDate") IsNot Nothing AndAlso Session("AssetDeclarationToDate").ToString().Length > 0 Then
                mdtDeclarationEndDate = eZeeDate.convertDate(Session("AssetDeclarationToDate").ToString()).Date
            End If

            If mdtDeclarationStartDate <> Nothing OrElse mdtDeclarationEndDate <> Nothing Then
                Dim mintDontAllowAssetDeclarationAfterDays As Integer = CInt(Session("DontAllowAssetDeclarationAfterDays"))
                Dim mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate As Integer = CInt(Session("NewEmpUnLockDaysforAssetDecWithinAppointmentDate"))

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = xEmployeeId

                Dim mdtDate As Date = objEmployee._Appointeddate.Date

                If objEmployee._Reinstatementdate <> Nothing AndAlso objEmployee._Appointeddate.Date < objEmployee._Reinstatementdate.Date Then
                    mdtDate = objEmployee._Reinstatementdate.Date
                End If


                If mdtDate.Date < mdtDeclarationStartDate.Date Then  'CONSIDER AS OLD EMPLOYEE
                    If DateDiff(DateInterval.Day, mdtDeclarationEndDate, ConfigParameter._Object._CurrentDateAndTime.Date) > 0 AndAlso DateDiff(DateInterval.Day, mdtDeclarationEndDate, ConfigParameter._Object._CurrentDateAndTime.Date) > mintDontAllowAssetDeclarationAfterDays Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 91, "Sorry, you cannot do your declaration as last date of declaration has been passed.") & Language.getMessage(mstrModuleName1, 92, "Please contact your administrator/manager to do futher operation on it."), Me)
                        Return False
                    End If

                ElseIf mdtDate.Date > mdtDeclarationStartDate.Date Then    'CONSIDER AS NEW EMPLOYEE
                    If mdtDate.AddDays(mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate).Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 91, "Sorry, you cannot do your declaration as last date of declaration has been passed.") & Language.getMessage(mstrModuleName1, 92, "Please contact your administrator/manager to do futher operation on it."), Me)
                        Return False
                    End If
                End If
                objEmployee = Nothing
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
    'Hemant (01 Mar 2022) -- End


    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Private Function SetValueAssetDeclaratationMaster() As clsAssetdeclaration_master
        Dim objAssetDeclare As New clsAssetdeclaration_master
        Try
            objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid

            objAssetDeclare._Employeeunkid = CInt(drpEmpListAddEdit.SelectedValue)

            objAssetDeclare._Finyear_Start = Session("FinStartDate")
            objAssetDeclare._Finyear_End = Session("FinEndDate")

            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._Loginemployeeunkid = -1
            End If
            objAssetDeclare._Isvoid = False
            objAssetDeclare._Voiddatetime = Nothing
            objAssetDeclare._Voidreason = ""
            objAssetDeclare._Voiduserunkid = -1
            objAssetDeclare._TransactionDate = dtpDate.GetDate

            objAssetDeclare._Savedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = Nothing

            objAssetDeclare._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
           
        Catch ex As Exception
            DisplayMessage.DisplayError("SetValueAssetDeclaratationMaster :- " & ex.Message, Me)
        End Try
        Return objAssetDeclare
    End Function
    'Hemant (24 Dec 2019) -- End


#End Region

#Region " Page's Event "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintAssetDeclarationUnkid", mintAssetDeclarationUnkid)
            Me.ViewState.Add("dsAssetList", dsAssetList)
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Me.ViewState.Add("dtBank", dtBank)
            'Me.ViewState.Add("dtShare", dtShare)
            'Me.ViewState.Add("dtHouse", dtHouse)
            'Me.ViewState.Add("dtPark", dtPark)
            'Me.ViewState.Add("dtVehicles", dtVehicles)
            'Me.ViewState.Add("dtMachinery", dtMachinery)
            'Me.ViewState.Add("dtOtherBusiness", dtOtherBusiness)
            'Hemant (24 Dec 2019) -- End

            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Me.ViewState.Add("dtResources", dtResources)
            'Me.ViewState.Add("dtDebt", dtDebt)
            'Hemant (24 Dec 2019) -- End
            'Pinkal (30-Jan-2013) -- End


            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Me.ViewState.Add("dtCurrBank", dsCurrBank)
            'Me.ViewState.Add("dtCurrShare", dsCurrShare)
            'Me.ViewState.Add("dtCurrHouse", dsCurrHouse)
            'Me.ViewState.Add("dtCurrPark", dsCurrPark)
            'Me.ViewState.Add("dtCurrVehicles", dsCurrVehicles)
            'Me.ViewState.Add("dtCurrMachinery", dsCurrMachinery)
            'Me.ViewState.Add("dtCurrOtherBusiness", dsCurrOtherBusiness)
            'Hemant (24 Dec 2019) -- End            
            'Sohail (06 Apr 2012) -- End


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Me.ViewState.Add("dsCurrResources", dsCurrResources)
            'Me.ViewState.Add("dsCurrDebt", dsCurrDebt)
            'Hemant (24 Dec 2019) -- End
            'Pinkal (30-Jan-2013) -- End


            Me.ViewState.Add("SrNo", Me.ViewState("SrNo"))
            Me.ViewState.Add("BankSrNo", Me.ViewState("BankSrNo"))
            Me.ViewState.Add("ShareSrNo", Me.ViewState("ShareSrNo"))
            Me.ViewState.Add("HouseSrNo", Me.ViewState("HouseSrNo"))
            Me.ViewState.Add("ParkSrNo", Me.ViewState("ParkSrNo"))
            Me.ViewState.Add("VehiclesSrNo", Me.ViewState("VehiclesSrNo"))
            Me.ViewState.Add("MachinerySrNo", Me.ViewState("MachinerySrNo"))
            Me.ViewState.Add("OtherBusinessSrNo", Me.ViewState("OtherBusinessSrNo"))


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            Me.ViewState.Add("ResourcesNo", Me.ViewState("ResourcesNo"))
            Me.ViewState.Add("DebtNo", Me.ViewState("DebtNo"))
            'Pinkal (30-Jan-2013) -- End


            Me.ViewState.Add("DelUnkId", Me.ViewState("DelUnkId"))
            Me.ViewState.Add("FinalSaveUnkId", Me.ViewState("FinalSaveUnkId"))
            Me.ViewState.Add("UnlockFinalSaveUnkId", Me.ViewState("UnlockFinalSaveUnkId")) 'Sohail (27 Feb 2015)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            Call SetLanguage()

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            'Anjan [24 February 2015] -- Start
            'ENHANCEMENT : Implementing session privileges for MSS
            If Not IsPostBack AndAlso Session("LoginBy") = Global.User.en_loginby.User Then
                BtnNew.Visible = Session("AddAssetDeclaration")
            End If
            'Anjan [24 February 2015] -- End


            If Not IsPostBack AndAlso mintAssetDeclarationUnkid <= 0 Then

                Call FillCombo()
                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                Call FillComboBank()
                Call FillComboShare()
                Call FillComboHouse()
                Call FillComboPark()
                Call FillComboVehicles()
                Call FillComboMachine()
                Call FillComboBusiness()
                'Sohail (06 Apr 2012) -- End


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                FillComboResources()
                FillComboDebt()
                'Pinkal (30-Jan-2013) -- End



                Call FillBankGrid()
                Call FillShareDividendGrid()
                Call FillHouseBuildingGrid()
                Call FillParmFarmMineGrid()
                Call FillVehiclesGrid()
                Call FillMachineryGrid()
                Call FillOtherBusinessGrid()


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                FillResourcesGrid()
                FillDebtGrid()
                'Pinkal (30-Jan-2013) -- End


                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                If Not Page.ClientScript.IsStartupScriptRegistered("load") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "load", "GetInstruct('');", True)
                End If
                'Sohail (06 Apr 2012) -- End


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes

                dtpAcquistionDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

                'Pinkal (30-Jan-2013) -- End



                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    gvList.Columns(0).Visible = CBool(Session("EditAssetDeclaration"))
                    gvList.Columns(1).Visible = CBool(Session("DeleteAssetDeclaration"))
                    gvList.Columns(2).Visible = CBool(Session("FinalSaveAssetDeclaration"))
                    gvList.Columns(3).Visible = CBool(Session("UnlockFinalSaveAssetDeclaration")) 'Sohail (27 Feb 2015)
                    'btnSaveClose.Visible = CBool(Session("FinalSaveAssetDeclaration"))
                End If
                'Pinkal (12-Feb-2015) -- End



            Else

                dsAssetList = Me.ViewState("dsAssetList")
                mintAssetDeclarationUnkid = Me.ViewState("mintAssetDeclarationUnkid")

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtBank = Me.ViewState("dtBank")
                'dtShare = Me.ViewState("dtShare")
                'dtHouse = Me.ViewState("dtHouse")
                'dtPark = Me.ViewState("dtPark")
                'dtVehicles = Me.ViewState("dtVehicles")
                'dtMachinery = Me.ViewState("dtMachinery")
                'dtOtherBusiness = Me.ViewState("dtOtherBusiness")
                'Hemant (24 Dec 2019) -- End

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtResources = Me.ViewState("dtResources")
                'dtDebt = Me.ViewState("dtDebt")
                'Hemant (24 Dec 2019) -- End
                'Pinkal (30-Jan-2013) -- End


                'Sohail (06 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Dim str As String = ConfigParameter._Object._AssetDeclarationInstruction.ToString.Replace(vbCrLf, "<br />")
                Dim str As String = Session("AssetDeclarationInstruction").ToString.Replace(vbCrLf, "<br />")
                'Sohail (23 Apr 2012) -- End
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "load", "GetInstruct('" & str & "');", True)

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dsCurrBank = Me.ViewState("dsCurrBank")
                'dsCurrShare = Me.ViewState("dsCurrShare")
                'dsCurrHouse = Me.ViewState("dsCurrHouse")
                'dsCurrPark = Me.ViewState("dsCurrPark")
                'dsCurrVehicles = Me.ViewState("dsCurrVehicles")
                'dsCurrMachinery = Me.ViewState("dsCurrMachinery")
                'dsCurrOtherBusiness = Me.ViewState("dsCurrOtherBusiness")
                'Hemant (24 Dec 2019) -- End
                ''Sohail (06 Apr 2012) -- End


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dsCurrResources = Me.ViewState("dsCurrResources")
                'dsCurrDebt = Me.ViewState("dsCurrDebt")
                'Hemant (24 Dec 2019) -- End

                'START FOR CHECK WHETHER PopupAddEdit Open or NOT

                Dim c As Control = Nothing
                Dim ctrlStr As String = String.Empty
                For Each ctl As String In Page.Request.Form

                    If ctl Is Nothing Or ctl = "" Then Continue For

                    If ctl.EndsWith(".x") OrElse ctl.EndsWith(".y") Then
                        ctrlStr = ctl.Substring(0, ctl.Length - 2)
                        c = Page.FindControl(ctrlStr)
                    Else
                        c = Page.FindControl(ctl)
                    End If
                    'SHANI [17 JAN 2015]-START
                    '
                    'If (c IsNot Nothing AndAlso TypeOf c Is System.Web.UI.WebControls.Button Or TypeOf c Is System.Web.UI.WebControls.ImageButton) AndAlso _
                    '    (c.ID = BtnSearch.ID Or c.ID = BtnReset.ID Or c.ID = btnSaveYes.ID Or c.ID = "btnGvRemove" Or c.ID = "btnGvFinalSave") Then
                    'Sohail (27 Feb 2015) -- Start
                    'Enhancement - Unlock Final Save in Self Service.
                    'If (c IsNot Nothing AndAlso TypeOf c Is System.Web.UI.WebControls.Button Or TypeOf c Is System.Web.UI.WebControls.ImageButton) AndAlso _
                    '    (c.ID = BtnSearch.ID Or c.ID = BtnReset.ID Or c.ID = popupYesNo.ID Or c.ID = popupFinalYesNo.ID Or c.ID = "btnGvRemove" Or c.ID = "btnGvFinalSave") Then
                    If (c IsNot Nothing AndAlso TypeOf c Is System.Web.UI.WebControls.Button OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton) AndAlso _
                        (c.ID = BtnSearch.ID OrElse c.ID = BtnReset.ID OrElse c.ID = popupYesNo.ID OrElse c.ID = popupFinalYesNo.ID OrElse c.ID = popupUnlockFinalYesNo.ID OrElse c.ID = "btnGvRemove" OrElse c.ID = "btnGvFinalSave" OrElse c.ID = "btnGvUnlockFinalSave") Then
                        'Sohail (27 Feb 2015) -- End
                        'SHANI [17 JAN 2015]--END 
                        Me.ViewState("OpenPopupAddEdit") = False
                    End If

                    'END FOR CHECK WHETHER PopupAddEdit Open or NOT

                Next

                If Me.ViewState("OpenPopupAddEdit") IsNot Nothing AndAlso CBool(Me.ViewState("OpenPopupAddEdit")) Then
                    popupAddEdit.Show()
                End If

                If dtpAcquistionDate.IsNull Then
                    dtpAcquistionDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                End If

                'Pinkal (30-Jan-2013) -- End


            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Button's Event(s) "

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click, Closebutton1.CloseButton_click
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        'SHANI [01 FEB 2015]--END
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnClose_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            'Hemant (01 Mar 2022) -- Start            
            If (Session("LoginBy") = Global.User.en_loginby.Employee) AndAlso CInt(drpEmployee.SelectedValue) > 0 Then
                If CheckUnLockEmployee(CInt(drpEmployee.SelectedValue)) = False Then Exit Sub
            End If
            'Hemant (01 Mar 2022) -- End
            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpEmployee.SelectedIndex = 0
            lblExchangeRate.Text = ""
            Reset_Bank()
            Reset_HouseBuilding()
            Reset_Machinery()
            Reset_OtherBusiness()
            Reset_ParkFarmMine()
            Reset_ShareDividend()
            Reset_Vehicles()
            Reset_Resources()
            Reset_Debts()
            'Pinkal (30-Jan-2013) -- End

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            txtCash.Text = ""
            txtCashExistingBank.Text = ""
            Call FillBankGrid()
            Call FillShareDividendGrid()
            Call FillHouseBuildingGrid()
            Call FillParmFarmMineGrid()
            Call FillVehiclesGrid()
            Call FillMachineryGrid()
            Call FillOtherBusinessGrid()
            Call FillResourcesGrid()
            Call FillDebtGrid()
            'Sohail (03 Dec 2013) -- End

            mintAssetDeclarationUnkid = -1
            lblError.Text = ""
            mvwAssetDeclaration.ActiveViewIndex = 0
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnNew_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnSearch_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpEmployee.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnReset_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try

            'Sohail (13 May 2013) -- Start
            'TRA - ENHANCEMENT
            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'lblExchangeRate.Text = ""
            ''Pinkal (30-Jan-2013) -- End

            'lblError.Text = ""
            'If CInt(drpEmpListAddEdit.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayError("Emplolyee is compulsory information.Please Select Emplolyee.", Me)
            '    drpEmpListAddEdit.Focus()
            '    Exit Sub
            'End If

            'If mintAssetDeclarationUnkid <= 0 Then
            '    Dim objAssetDeclare As New clsAssetdeclaration_master

            '    If objAssetDeclare.isExist(CInt(drpEmpListAddEdit.SelectedValue)) = True Then
            '        lblError.Text = "Sorry! Asset declaration for this employee is already exist."
            '        drpEmpListAddEdit.Focus()
            '        popupAddEdit.Show()
            '        Exit Sub
            '    End If
            'End If

            ''Sohail (06 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'If dtpDate.IsNull = True Then
            '    DisplayMessage.DisplayError("Please enter Transaction Date.", Me)
            '    popupAddEdit.Show()
            '    dtpDate.Focus()
            '    Exit Sub
            '    'Sohail (23 Apr 2012) -- Start
            '    'TRA - ENHANCEMENT
            'ElseIf dtpDate.GetDate < Session("fin_startdate") OrElse dtpDate.GetDate > Session("fin_enddate") Then
            '    'ElseIf dtpDate.GetDate < FinancialYear._Object._Database_Start_Date OrElse dtpDate.GetDate > FinancialYear._Object._Database_End_Date Then
            '    'DisplayMessage.DisplayError("Sorry! Transaction Date should be in between " & FinancialYear._Object._Database_Start_Date.ToShortDateString & " AND " & FinancialYear._Object._Database_End_Date.ToShortDateString & ".", Me)
            '    DisplayMessage.DisplayError("Sorry! Transaction Date should be in between " & Session("fin_startdate") & " AND " & Session("fin_enddate") & ".", Me)
            '    'Sohail (23 Apr 2012) -- End
            '    popupAddEdit.Show()
            '    dtpDate.Focus()
            '    Exit Sub
            'End If
            ''Sohail (06 Apr 2012) -- End
            If IsValidData() = False Then
                Exit Sub
            End If
            'Sohail (13 May 2013) -- End

            If mvwAssetDeclaration.ActiveViewIndex < mvwAssetDeclaration.Views.Count - 1 Then
                mvwAssetDeclaration.ActiveViewIndex += 1

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                SetLabelRate()
                'Pinkal (30-Jan-2013) -- End

                popupAddEdit.Show()
            Else

                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                'Sohail (27 Feb 2015) -- Start
                'Enhancement - Unlock Final Save in Self Service.
                'If CBool(Session("FinalSaveAssetDeclaration")) Then
                '    popupYesNo.Show()
                'End If
                popupYesNo.Show()
                'Sohail (27 Feb 2015) -- End
                'Pinkal (12-Feb-2015) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("btnNext_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If mvwAssetDeclaration.ActiveViewIndex > 0 Then
                mvwAssetDeclaration.ActiveViewIndex -= 1

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                SetLabelRate()
                'Pinkal (30-Jan-2013) -- End

            End If
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnBack_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'SHANI [17 JAN 2015]-START
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup1.buttonDelReasonYes_Click
        'SHANI [17 JAN 2015]--END 
        Try

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmpAssetDeclarationList"
            'StrModuleName2 = "mnuAssetDeclaration"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            Dim objAssetDeclare As New clsAssetdeclaration_master
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._VoidLoginEmployeeunkid = Session("Employeeunkid")
            Else
                objAssetDeclare._VoidLoginEmployeeunkid = -1
            End If

            With objAssetDeclare
                ._FormName = mstrModuleName
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If                
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare.Void(CInt(Me.ViewState("DelUnkId")), -1, ConfigParameter._Object._CurrentDateAndTime, popup1.Reason) ' txtreasondel.Text SHANI [17 JAN 2015]
            Else
                objAssetDeclare.Void(CInt(Me.ViewState("DelUnkId")), Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popup1.Reason) ' txtreasondel.Text SHANI [17 JAN 2015]
            End If
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnDelete_Click :- " & ex.Message, Me)
        End Try
    End Sub


    'SHANI [17 JAN 2015]-START
    '
    'Protected Sub btnSaveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveYes.Click
    Protected Sub btnSaveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonYes_Click
        'SHANI [17 JAN 2015]--END 
        Dim objAssetDeclare As New clsAssetdeclaration_master
        Try

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            'StrModuleName2 = "mnuAssetDeclaration"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = mintAssetDeclarationUnkid
            objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            'Shani(20-Nov-2015) -- End

            objAssetDeclare._Employeeunkid = CInt(drpEmpListAddEdit.SelectedValue)
            If txtCash.Text.Trim = "" Then
                objAssetDeclare._Cash = 0
            Else
                objAssetDeclare._Cash = txtCash.Text.Trim
            End If
            If txtCashExistingBank.Text.Trim = "" Then
                objAssetDeclare._CashExistingBank = 0
            Else
                objAssetDeclare._CashExistingBank = txtCashExistingBank.Text.Trim
            End If
            objAssetDeclare._Finyear_Start = Session("FinStartDate")
            objAssetDeclare._Finyear_End = Session("FinEndDate")
            objAssetDeclare._Resources = txtResources.Text.Trim
            ' objAssetDeclare._Debt = txtDebt.Text.Trim
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._LoginEmployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._LoginEmployeeunkid = -1
            End If
            objAssetDeclare._Isvoid = False
            objAssetDeclare._Voiddatetime = Nothing
            objAssetDeclare._Voidreason = ""
            objAssetDeclare._Voiduserunkid = -1
            objAssetDeclare._TransactionDate = dtpDate.GetDate 'Sohail (06 Apr 2012)

            Dim drRow() As DataRow

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'drRow = dtBank.Select("bank = 'None' AND account = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtBank.AcceptChanges()
            'End If

            'drRow = dtShare.Select("location = 'None' AND dividendamount = -1 ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtShare.AcceptChanges()
            'End If

            'drRow = dtHouse.Select("homebuilding = 'None' AND location = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtHouse.AcceptChanges()
            'End If

            'drRow = dtPark.Select("parkfarmmines = 'None' AND placeclad = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtPark.AcceptChanges()
            'End If

            'drRow = dtVehicles.Select("vehicletype = 'None' AND value = -1 ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtVehicles.AcceptChanges()
            'End If

            'drRow = dtMachinery.Select("machines = 'None' AND location = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtMachinery.AcceptChanges()
            'End If

            'drRow = dtOtherBusiness.Select("businesstype = 'None' AND placeclad = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtOtherBusiness.AcceptChanges()
            'End If
            'Hemant (24 Dec 2019) -- End

            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'drRow = dtResources.Select("otherresources = 'None' AND location = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtResources.AcceptChanges()
            'End If

            'drRow = dtDebt.Select("debts = 'None' AND location = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtDebt.AcceptChanges()
            'End If
            'Hemant (24 Dec 2019) -- End
            'Pinkal (30-Jan-2013) -- End


            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetDeclare._Datasource_Bank = dtBank
            'objAssetDeclare._Datasource_ShareDividend = dtShare
            'objAssetDeclare._Datasource_HouseBuilding = dtHouse
            'objAssetDeclare._Datasource_ParkFarmMines = dtPark
            'objAssetDeclare._Datasource_Vehicles = dtVehicles
            'objAssetDeclare._Datasource_Machinery = dtMachinery
            'objAssetDeclare._Datasource_OtherBusiness = dtOtherBusiness
            'Hemant (24 Dec 2019) -- End           


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetDeclare._Datasource_Resources = dtResources
            'objAssetDeclare._Datasource_Debts = dtDebt
            'Hemant (24 Dec 2019) -- End
            objAssetDeclare._Savedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = Nothing

            'Pinkal (30-Jan-2013) -- End

            With objAssetDeclare
                ._FormName = mstrModuleName
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If                
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objAssetDeclare.InsertData() = False Then
            If objAssetDeclare.InsertData(Session("Database_Name"), ConfigParameter._Object._CurrentDateAndTime) = False Then
                'Shani(20-Nov-2015) -- End

                DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
            Else
                Call FillList()
                DisplayMessage.DisplayMessage("Entry Saved Successfuly!", Me)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSaveYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSaveYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'SHANI [17 JAN 2015]-START
    'Protected Sub btnSaveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNo.Click
    Protected Sub btnSaveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonNo_Click
        'SHANI [17 JAN 2015]--END 
        Try
            popupAddEdit.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSaveNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSaveNo_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnScanAttachDocuments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanAttachDocuments.Click
        Try
            Session("ScanAttchLableCaption") = "Select Employee"
            Session("ScanAttchRefModuleID") = enImg_Email_RefId.Employee_Module
            Session("ScanAttchenAction") = enAction.ADD_ONE
            Session("ScanAttchToEditIDs") = ""
            Response.Redirect(Session("servername") & "~/Others_Forms/wPg_ScanOrAttachmentInfo.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnScanAttachDocuments_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnScanAttachDocuments_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'SHANI [17 JAN 2015]-START
    '
    'Protected Sub btnFinalSaveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalSaveYes.Click
    Protected Sub btnFinalSaveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalYesNo.buttonYes_Click
        'SHANI [17 JAN 2015]--END 
        Try

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            'StrModuleName2 = "mnuAssetDeclaration"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            Dim objAssetDeclare As New clsAssetdeclaration_master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(Me.ViewState("FinalSaveUnkId"))
            objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(Me.ViewState("FinalSaveUnkId"))
            'Shani(20-Nov-2015) -- End

            objAssetDeclare._Isfinalsaved = True
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._LoginEmployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._LoginEmployeeunkid = -1
            End If


            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes
            objAssetDeclare._Finalsavedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Unlockfinalsavedate = Nothing

            'If objAssetDeclare.Update() = False AndAlso objAssetDeclare._Message <> "" Then
            '    DisplayMessage.DisplayError(objAssetDeclare._Message, Me)
            'Else
            '    DisplayMessage.DisplayError("This Asset Declaration Final Saved Successfully.", Me)
            '    Call FillList()
            'End If

            With objAssetDeclare
                ._FormName = mstrModuleName
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If objAssetDeclare.Update(True) = False AndAlso objAssetDeclare._Message <> "" Then
                DisplayMessage.DisplayError(objAssetDeclare._Message, Me)
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 9, "This Asset Declaration Final Saved Successfully."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "This Asset Declaration Final Saved Successfully."), Me)
                'Sohail (23 Mar 2019) -- End
                Call FillList()
            End If

            'Pinkal (06-Feb-2013) -- End




        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnFinalSaveYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnFinalSaveYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'SHANI [17 JAN 2015]-START
    '
    'Protected Sub btnFinalSaveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalSaveNo.Click
    Protected Sub btnFinalSaveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalYesNo.buttonNo_Click
        'SHANI [17 JAN 2015]--END 
        Try

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnFinalSaveNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnFinalSaveNo_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (13 May 2013) -- Start
    'TRA - ENHANCEMENT
    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If IsValidData() = False Then
                Exit Sub
            End If

            popupYesNo.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSaveClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSaveClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (13 May 2013) -- End

    'Sohail (27 Feb 2015) -- Start
    'Enhancement - Unlock Final Save in Self Service.
    Protected Sub popupUnlockFinalYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupUnlockFinalYesNo.buttonNo_Click
        Try

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupUnlockFinalYesNo_buttonNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupUnlockFinalYesNo_buttonNo_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupUnlockFinalYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupUnlockFinalYesNo.buttonYes_Click
        Try
            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            'StrModuleName2 = "mnuAssetDeclaration"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            Dim objAssetDeclare As New clsAssetdeclaration_master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDeclare._Assetdeclarationunkid = CInt(Me.ViewState("UnlockFinalSaveUnkId"))
            objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(Me.ViewState("UnlockFinalSaveUnkId"))
            'Shani(20-Nov-2015) -- End

            objAssetDeclare._Isfinalsaved = False
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._LoginEmployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._LoginEmployeeunkid = -1
            End If


            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = ConfigParameter._Object._CurrentDateAndTime

            With objAssetDeclare
                ._FormName = mstrModuleName
                If Session("LoginBy") = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If objAssetDeclare.Update(True) = False AndAlso objAssetDeclare._Message <> "" Then
                DisplayMessage.DisplayError(objAssetDeclare._Message, Me)
            Else
                Dim objDocument As New clsScan_Attach_Documents
                Dim strIds As String = ""
                Dim dtTable As DataTable


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dtTable = New DataView(objDocument.GetList("Attachment").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(Me.ViewState("UnlockFinalSaveUnkId")) & " AND UNKID = " & objAssetDeclare._Employeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable

                'S.SANDEEP |04-SEP-2021| -- START
                'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                'dtTable = New DataView(objDocument.GetList(Session("Document_Path"), "Attachment", "").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(Me.ViewState("UnlockFinalSaveUnkId")) & " AND UNKID = " & objAssetDeclare._Employeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(objDocument.GetList(Session("Document_Path"), "Attachment", "", objAssetDeclare._Employeeunkid, , , , , CBool(IIf(objAssetDeclare._Employeeunkid <= 0, True, False))).Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(Me.ViewState("UnlockFinalSaveUnkId")) & " AND UNKID = " & objAssetDeclare._Employeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP |04-SEP-2021| -- END

                'Shani(20-Nov-2015) -- End


                If dtTable.Rows.Count > 0 Then
                    For Each dtRow As DataRow In dtTable.Rows
                        strIds &= "," & dtRow.Item("scanattachtranunkid").ToString

                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        If dtRow("filepath").ToString <> "" AndAlso dtRow("fileuniquename").ToString <> "" Then
                            Dim strFilepath As String = dtRow("filepath")
                            Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                            If strFilepath.Contains(strArutiSelfService) Then
                                strFilepath = strFilepath.Replace(strArutiSelfService, "")
                                If Strings.Left(strFilepath, 1) <> "/" Then
                                    strFilepath = "~/" & strFilepath
                                Else
                                    strFilepath = "~" & strFilepath
                                End If

                                If File.Exists(Server.MapPath(strFilepath)) Then
                                    File.Delete(Server.MapPath(strFilepath))
                                Else
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError("File connot exitst localpath", Me)
                                    DisplayMessage.DisplayMessage("File dows not exist on localpath", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    Exit Sub
                                End If
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError("File connot exitst localpath", Me)
                                DisplayMessage.DisplayMessage("File dows not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If

                        End If
                        'SHANI (16 JUL 2015) -- End 

                    Next
                    If strIds.Trim.Length > 0 Then
                        strIds = Mid(strIds, 2)
                        With objDocument
                            ._FormName = mstrModuleName
                            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                                ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                            Else
                                ._AuditUserId = Convert.ToInt32(Session("UserId"))
                            End If
                            ._ClientIP = Session("IP_ADD").ToString()
                            ._HostName = Session("HOST_NAME").ToString()
                            ._FromWeb = True
                            ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                            ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                        End With
                        If objDocument.Delete(strIds) = False Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Problem in deleting information."), Me)
                            Exit Try
                        End If
                    End If
                End If

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully."), Me)
                'Sohail (23 Mar 2019) -- End
                Call FillList()
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnFinalSaveYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnFinalSaveYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (27 Feb 2015) -- End

    'SHANI (16 APR 2015)-START
    Protected Sub btnCloseAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseAddEdit.Click
        Try
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            mintAssetDeclarationUnkid = -1
            'Hemant (24 Dec 2019) -- End
            Me.ViewState("OpenPopupAddEdit") = False
            popupAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnCloseAddEdit_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'SHANI (16 APR 2015)--END
#End Region

#Region " GridView Events "

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        Try
            gvList.PageIndex = e.NewPageIndex
            gvList.DataSource = dsAssetList
            gvList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvList_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvList.RowCommand
        Try
            If e.CommandName <> "Print" Then
                'SHANI (16 Apr 2015) -- Start
                'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                If dsAssetList.Tables(0).Rows(CInt(e.CommandArgument))("yearname").ToString.Trim <> Session("FinancialYear_Name") Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), Me)
                    Exit Sub
                End If
                'SHANI (16 Apr 2015) -- End
            End If
            If e.CommandName = "Change" Then
                lblError.Text = ""
                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                lblExchangeRate.Text = ""
                Reset_Bank()
                Reset_HouseBuilding()
                Reset_Machinery()
                Reset_OtherBusiness()
                Reset_ParkFarmMine()
                Reset_ShareDividend()
                Reset_Vehicles()
                Reset_Resources()
                Reset_Debts()
                'Pinkal (30-Jan-2013) -- End



                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("SrNo") = SrNo

                Dim objAssetDeclare As New clsAssetdeclaration_master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDeclare._Assetdeclarationunkid = CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationunkid"))
                objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationunkid"))
                'Shani(20-Nov-2015) -- End

                'Hemant (01 Mar 2022) -- Start            
                If (Session("LoginBy") = Global.User.en_loginby.Employee) AndAlso CInt(objAssetDeclare._Employeeunkid) > 0 Then
                    If CheckUnLockEmployee(CInt(objAssetDeclare._Employeeunkid)) = False Then Exit Sub
                End If
                'Hemant (01 Mar 2022) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If

                drpEmpListAddEdit.SelectedValue = dsAssetList.Tables(0).Rows(SrNo)("employeeunkid")
                Call drpEmpListAddEdit_SelectedIndexChanged(sender, New EventArgs)

                txtEmployeeCode.Text = dsAssetList.Tables(0).Rows(SrNo).Item("noofemployment").ToString
                txtOfficePosition.Text = dsAssetList.Tables(0).Rows(SrNo).Item("position").ToString
                txtDepartment.Text = dsAssetList.Tables(0).Rows(SrNo).Item("centerforwork").ToString
                txtWorkStation.Text = dsAssetList.Tables(0).Rows(SrNo).Item("workstation").ToString
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'txtCash.Text = Format(dsAssetList.Tables(0).Rows(SrNo).Item("cash"), GUI.fmtCurrency)
                'txtCashExistingBank.Text = Format(dsAssetList.Tables(0).Rows(SrNo).Item("cashexistingbank"), GUI.fmtCurrency)
                txtCash.Text = Format(dsAssetList.Tables(0).Rows(SrNo).Item("cash"), Session("fmtCurrency"))
                txtCashExistingBank.Text = Format(dsAssetList.Tables(0).Rows(SrNo).Item("cashexistingbank"), Session("fmtCurrency"))
                'Sohail (23 Apr 2012) -- End
                txtResources.Text = dsAssetList.Tables(0).Rows(SrNo).Item("resources").ToString
                'txtDebt.Text = dsAssetList.Tables(0).Rows(SrNo).Item("debt").ToString
                dtpDate.SetDate = CDate(dsAssetList.Tables(0).Rows(SrNo).Item("transactiondate").ToString).Date 'Sohail (06 Apr 2012)

                mintAssetDeclarationUnkid = CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationunkid"))

                Call FillBankGrid()
                Call FillShareDividendGrid()
                Call FillHouseBuildingGrid()
                Call FillParmFarmMineGrid()
                Call FillVehiclesGrid()
                Call FillMachineryGrid()
                Call FillOtherBusinessGrid()


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                FillResourcesGrid()
                FillDebtGrid()
                'Pinkal (30-Jan-2013) -- End


                mvwAssetDeclaration.ActiveViewIndex = 0
                popupAddEdit.Show()


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                Me.ViewState.Add("OpenPopupAddEdit", True)
                'Pinkal (30-Jan-2013) -- End


            ElseIf e.CommandName = "Remove" Then
                Me.ViewState("DelUnkId") = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)

                Dim objAssetDeclare As New clsAssetdeclaration_master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDeclare._Assetdeclarationunkid = CInt(Me.ViewState("DelUnkId"))
                objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(Me.ViewState("DelUnkId"))
                'Shani(20-Nov-2015) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If
                'SHANI (16 APR 2015)-START
                popupAddEdit.Hide()
                'SHANI (16 APR 2015)--END 
                popup1.Show()

            ElseIf e.CommandName = "FinalSave" Then
                Me.ViewState("FinalSaveUnkId") = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                Dim SrNo As Integer = CInt(e.CommandArgument)

                Dim objAssetDeclare As New clsAssetdeclaration_master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDeclare._Assetdeclarationunkid = CInt(Me.ViewState("FinalSaveUnkId"))
                objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(Me.ViewState("FinalSaveUnkId"))
                'Shani(20-Nov-2015) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If

                Dim objDocument As New clsScan_Attach_Documents


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                Dim objAssetBank As New clsAsset_bank_tran
                Dim decBankTotal As Decimal = 0
                'Pinkal (30-Jan-2013) -- End


                Dim dtTable As DataTable


                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetBank._Assetdeclarationunkid = CInt(Me.ViewState("FinalSaveUnkId"))
                objAssetBank._Assetdeclarationunkid(Session("Database_Name")) = CInt(Me.ViewState("FinalSaveUnkId"))
                'Shani(20-Nov-2015) -- End

                dtTable = objAssetBank._Datasource

                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'For Each dtRow As DataRow In dtTable.Rows
                '    decBankTotal += CDec(dtRow.Item("baseamount"))
                'Next
                decBankTotal = (From p In dtTable Select CDec(p.Item("baseamount"))).Sum()
                'Sohail (22 Feb 2020) -- End
                If decBankTotal.ToString(Session("fmtCurrency")) <> objAssetDeclare._CashExistingBank.ToString(Session("fmtCurrency")) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry! Total amount of respective banks should match with cash in existing bank."), Me)
                    Exit Sub
                End If
                'Pinkal (30-Jan-2013) -- End



                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'dtTable = New DataView(objDocument.GetList("Attachment").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationunkid")) & " AND UNKID = " & CInt(dsAssetList.Tables(0).Rows(SrNo).Item("employeeunkid")) & " ", "", DataViewRowState.CurrentRows).ToTable
                'dtTable = New DataView(objDocument.GetList("Attachment", , Session("Document_Path")).Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationunkid")) & " AND UNKID = " & CInt(dsAssetList.Tables(0).Rows(SrNo).Item("employeeunkid")) & " ", "", DataViewRowState.CurrentRows).ToTable
                'Sohail (23 Apr 2012) -- End

                'If dtTable.Rows.Count <= 0 Then
                '    DisplayMessage.DisplayMessage("Sorry! First attach document before Final Save.", Me)
                '    Exit Sub
                'End If

                'SHANI [17 JAN 2015]-START
                'lblYesNoHeader.Text = "Are you sure you want to Final Save this Asset Declaration?"
                'lblYesNoMessage.Text = "After Final Save You can not Edit / Delete Asset Declaration. Are you sure you want to Final Save this Asset Declaration?"
                'pnlSaveYesNo.Visible = False
                'pnlFinalSaveYesNo.Visible = True
                Call SetLanguage() 'Sohail (27 Feb 2015)
                popupFinalYesNo.Show()
                'SHANI [17 JAN 2015]--END 


                'Sohail (27 Feb 2015) -- Start
                'Enhancement - Unlock Final Save in Self Service.
            ElseIf e.CommandName = "UnlockFinalSave" Then
                Me.ViewState("UnlockFinalSaveUnkId") = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                Dim SrNo As Integer = CInt(e.CommandArgument)

                Dim objAssetDeclare As New clsAssetdeclaration_master

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDeclare._Assetdeclarationunkid = CInt(Me.ViewState("UnlockFinalSaveUnkId"))
                objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(Me.ViewState("UnlockFinalSaveUnkId"))
                'Shani(20-Nov-2015) -- End

                If objAssetDeclare._Isfinalsaved = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Unlock Final Save. Reason: This transaction is not Final Saved."), Me)
                    Exit Sub
                End If

                Call SetLanguage()
                popupUnlockFinalYesNo.Show()
                'Sohail (27 Feb 2015) -- End



            ElseIf e.CommandName = "Print" Then

                Dim objAssetDeclare As New clsAssetdeclaration_master 'SHANI (16 APR 2015)-START

                Dim objRptAssetDeclare As New ArutiReports.clsAssetDeclaration
                objRptAssetDeclare._AssetDeclarationUnkid = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                objRptAssetDeclare._EmployeeUnkid = CInt(dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("employeeunkid"))
                objRptAssetDeclare._EmployeeCode = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("noofemployment").ToString
                objRptAssetDeclare._EmployeeName = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("employeename").ToString
                objRptAssetDeclare._WorkStation = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("workstation").ToString
                objRptAssetDeclare._Title = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("position").ToString
                objRptAssetDeclare._CompanyUnkId = Session("CompanyUnkId") 'Sohail (23 Apr 2012)

                'SHANI (16 APR 2015)-START
                '
                objRptAssetDeclare._DatabaseName = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("databasename")
                objAssetDeclare._DatabaseName = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("databasename")

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objAssetDeclare._Assetdeclarationunkid = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                objAssetDeclare._Assetdeclarationunkid(Session("Database_Name")) = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                'Shani(20-Nov-2015) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    objRptAssetDeclare._NonOfficialDeclaration = ""
                Else
                    objRptAssetDeclare._NonOfficialDeclaration = Language.getMessage(mstrModuleName, 14, "Non Official Declaration")
                End If
                'SHANI (16 APR 2015)--END


                ''Anjan [ 22 Nov 2013 ] -- Start
                ''ENHANCEMENT : Requested by Rutta
                'objRptAssetDeclare._LanguageId = CInt(Session("LangId"))
                ''Anjan [22 Nov 2013 ] -- End


                'Sohail (11 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'objRptAssetDeclare.generateReport(0, enPrintAction.Print)
                'Sohail (18 Jul 2017) -- Start
                'Enhancement - 69.1 - Day wise total days worked and hours worked functions.
                'objRptAssetDeclare.generateReport(0, enPrintAction.None, enExportAction.None)
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    'Sohail (04 Jul 2019) -- Start
                    'TRA Issue - Support Issue Id # 0003897 - 70.1 - System allow us to select form of required year / period but exported report displays form of current year.
                    'objRptAssetDeclare.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
                    objRptAssetDeclare.generateReportNew(dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("databasename"), CInt(Session("UserId")), CInt(dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("yearunkid")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
                    'Sohail (04 Jul 2019) -- End
                Else
                    'Sohail (04 Jul 2019) -- Start
                    'TRA Issue - Support Issue Id # 0003897 - 70.1 - System allow us to select form of required year / period but exported report displays form of current year.
                    'objRptAssetDeclare.generateReportNew(Session("Database_Name").ToString(), 1, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
                    objRptAssetDeclare.generateReportNew(dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("databasename"), 1, CInt(dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("yearunkid")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
                    'Sohail (04 Jul 2019) -- End
                End If
                'Sohail (18 Jul 2017) -- End
                Session("objRpt") = objRptAssetDeclare._Rpt

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                'Shani(24-Aug-2015) -- End

                'Sohail (11 Apr 2012) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvList_RowCommand :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Anjan [24 February 2015] -- Start
                'ENHANCEMENT : Implementing session privileges for MSS

                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    e.Row.Cells(0).Visible = Session("EditAssetDeclaration")
                    e.Row.Cells(1).Visible = Session("DeleteAssetDeclaration")
                    e.Row.Cells(2).Visible = Session("FinalSaveAssetDeclaration")
                    e.Row.Cells(3).Visible = Session("UnlockFinalSaveAssetDeclaration")
                End If
                'Anjan [24 February 2015] -- End

                If e.Row.Cells(5).Text = "None" AndAlso e.Row.Cells(6).Text = "&nbsp;" Then
                    e.Row.Visible = False
                End If

                'SHANI (16 APR 2015)-START
                'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.
                If e.Row.Cells(9).Text.ToString.Trim <> Session("FinancialYear_Name") Then
                    e.Row.Style.Add("color", "green")
                End If
                'SHANI (16 APR 2015)--END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvList_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " DropDown List Events "
    Protected Sub drpEmpListAddEdit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmpListAddEdit.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Try
            If CInt(drpEmpListAddEdit.SelectedValue) > 0 Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = CInt(drpEmpListAddEdit.SelectedValue)
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(drpEmpListAddEdit.SelectedValue)
                'Shani(20-Nov-2015) -- End


                txtEmployeeCode.Text = objEmployee._Employeecode

                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmployee._Departmentunkid
                txtDepartment.Text = objDept._Name
                objDept = Nothing

                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                txtOfficePosition.Text = objJob._Job_Name
                objJob = Nothing

                'Sohail (02 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                txtWorkStation.Text = objClass._Name
                objClass = Nothing
                'Sohail (02 Apr 2012) -- End

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                If objEmployee._Appointeddate.Date > Session("fin_startdate") Then
                    'If objEmployee._Appointeddate.Date > FinancialYear._Object._Database_Start_Date.Date Then
                    'Sohail (23 Apr 2012) -- End
                    Session("FinStartDate") = objEmployee._Appointeddate.Date
                Else
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Session("FinStartDate") = FinancialYear._Object._Database_Start_Date.Date
                    Session("FinStartDate") = Session("fin_startdate")
                    'Sohail (23 Apr 2012) -- End
                End If

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Dim EndDate As Date = FinancialYear._Object._Database_End_Date.Date
                Dim EndDate As Date = Session("fin_enddate")
                'Sohail (23 Apr 2012) -- End
                If objEmployee._Termination_From_Date.Date <> Nothing Then
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < FinancialYear._Object._Database_End_Date.Date, objEmployee._Termination_From_Date.Date, FinancialYear._Object._Database_End_Date.Date))
                    EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < Session("fin_enddate"), objEmployee._Termination_From_Date.Date, Session("fin_enddate")))
                    'Sohail (23 Apr 2012) -- End
                End If
                If objEmployee._Termination_To_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate))
                End If

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                If EndDate <> Session("fin_enddate") Then
                    'If EndDate <> FinancialYear._Object._Database_End_Date.Date Then
                    'Sohail (23 Apr 2012) -- End
                    Session("FinEndDate") = EndDate
                Else
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Session("FinEndDate") = FinancialYear._Object._Database_End_Date.Date
                    Session("FinEndDate") = Session("fin_enddate")
                    'Sohail (23 Apr 2012) -- End
                End If
            Else
                txtEmployeeCode.Text = ""
                txtDepartment.Text = ""
                txtOfficePosition.Text = ""
                txtWorkStation.Text = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("drpEmpListAddEdit_SelectedIndexChanged :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
#Region "Date's Events "
    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged
        Try
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("dtpDate_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub
#End Region
    'Sohail (06 Apr 2012) -- End

#Region " Bank "

#Region " Private Method Functions "

    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboBank()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrBank As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrBank = objExchange.getComboList("Currency", True)
            With drpCurrencyBank

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes

                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"

                'Pinkal (30-Jan-2013) -- End
                .DataTextField = "currency_sign"
                .DataSource = dsCurrBank.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboBank :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillBankGrid()
        Dim objAssetBank As New clsAsset_bank_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtBank As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetBank._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetBank._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtBank = objAssetBank._Datasource

            'If dtBank.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtBank.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(3) = ""

            '    'Pinkal (30-Jan-2013) -- Start
            '    'Enhancement : TRA Changes
            '    r.Item(4) = 0
            '    'Pinkal (30-Jan-2013) -- End

            '    dtBank.Rows.Add(r)
            'End If
            Dim dsBank As DataSet = objAssetBank.GetList("List", mintAssetDeclarationUnkid)
            dtBank = dsBank.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtBank.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtBank.NewRow
                dtBank.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvBank.DataSource = dtBank
            gvBank.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvBank.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End

            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            SetBankTotal()
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("FillBankGrid :- " & ex.Message, Me)
        End Try

    End Sub

    Private Function IsValid_Bank() As Boolean
        Try
            lblErrorBank.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCBank.Text = ""
            If CInt(drpCurrencyBank.SelectedItem.Value) <= 0 Then
                lblErrorCBank.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End

            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantBank.Text, decServant)
            Decimal.TryParse(txtWifeHusbandBank.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenBank.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorBank.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_Bank :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_Bank()
        Try
            txtBank.Text = ""
            txtAccountNumber.Text = ""
            txtAmountBank.Text = ""
            txtServantBank.Text = ""
            txtWifeHusbandBank.Text = ""
            txtChildrenBank.Text = ""
            lblErrorBank.Text = ""
            lblErrorCBank.Text = "" 'Sohail (06 Apr 2012)

            BtnAddBank.Enabled = True
            BtnUpdateBank.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyBank.SelectedIndex = 0
            'Pinkal (30-Jan-2013) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_Bank :- " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (30-Jan-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub SetBankTotal()
        Try

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'If dtBank IsNot Nothing Then
            '    Dim drRow() As DataRow = dtBank.Select("AUD <> 'D'")
            '    If drRow.Length > 0 Then
            '        txtCashExistingBank.Text = Format(CDec(dtBank.Compute("sum(baseamount)", "AUD <> 'D'")), Session("fmtCurrency"))
            '    Else
            '        txtCashExistingBank.Text = Format(0, Session("fmtCurrency"))
            '    End If
            'End If
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("SetBankTotal :- " & ex.Message, Me)
        End Try
    End Sub

    'Pinkal (30-Jan-2013) -- End

#End Region

#Region " Button's Events "
    Protected Sub BtnAddBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddBank.Click
        Try
            If IsValid_Bank() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtBank.NewRow

            'drRow.Item("assetbanktranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("bank") = txtBank.Text.Trim
            'drRow.Item("account") = txtAccountNumber.Text.Trim
            'drRow.Item("amount") = txtAmountBank.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyBank.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyBank.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantBank.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantBank.Text.Trim
            'End If
            'If txtWifeHusbandBank.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbandBank.Text.Trim
            'End If
            'If txtChildrenBank.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenBank.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes

            'Update_DataGridview_DataTable_Extra_Columns(gvBank, drRow, CInt(drpCurrencyBank.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtBank.Rows.Add(drRow)

            'gvBank.DataSource = dtBank
            'gvBank.DataBind()
            Dim objAssetBank As New clsAsset_bank_tran

            objAssetBank._Assetbanktranunkid = -1
            objAssetBank._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetBank._Bank = txtBank.Text.Trim
            objAssetBank._Account = txtAccountNumber.Text.Trim
            objAssetBank._Amount = txtAmountBank.Text.Trim
            objAssetBank._Countryunkid = CInt(drpCurrencyBank.SelectedItem.Value)
            If txtServantBank.Text.Trim = "" Then
                objAssetBank._Servant = 0
            Else
                objAssetBank._Servant = txtServantBank.Text.Trim
            End If
            If txtWifeHusbandBank.Text.Trim = "" Then
                objAssetBank._Wifehusband = 0
            Else
                objAssetBank._Wifehusband = txtWifeHusbandBank.Text.Trim
            End If
            If txtChildrenBank.Text.Trim = "" Then
                objAssetBank._Children = 0
            Else
                objAssetBank._Children = txtChildrenBank.Text.Trim
            End If
            objAssetBank._Isfinalsaved = 0
            objAssetBank._Userunkid = CInt(Session("UserId"))
            If objAssetBank._Transactiondate = Nothing Then
                objAssetBank._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyBank.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetBank._CurrencyUnkId = intPaidCurrencyunkid
            objAssetBank._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetBank._Baseexchangerate = decBaseExRate
            objAssetBank._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetBank._Baseamount = objAssetBank._Amount * decBaseExRate / decPaidExRate
            Else
                objAssetBank._Baseamount = objAssetBank._Amount
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetBank._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetBank._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetBank._ClientIp = Session("IP_ADD").ToString()
            objAssetBank._HostName = Session("HOST_NAME").ToString()
            objAssetBank._FormName = mstrModuleName1
            objAssetBank._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsBank As DataSet = objAssetBank.GetList("List", mintAssetDeclarationUnkid)
            Dim dtBank As DataTable = dsBank.Tables(0)
            If dtBank IsNot Nothing Then
                Dim drRow() As DataRow = dtBank.Select()
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'If drRow.Length > 0 Then
                '    objAssetDeclare._Banktotal = objAssetBank._Baseamount + Format(CDec(dtBank.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                'Else
                '    objAssetDeclare._Banktotal = Format(objAssetBank._Baseamount, Session("fmtCurrency"))
                'End If
                If drRow.Length > 0 Then
                    Dim decBankTotal As Decimal = (From p In dtBank Select CDec(p.Item("baseamount"))).Sum()
                    objAssetDeclare._Banktotal = objAssetBank._Baseamount + decBankTotal
                Else
                    objAssetDeclare._Banktotal = objAssetBank._Baseamount
                End If
                'Sohail (22 Feb 2020) -- End
            End If
            If txtCash.Text.Trim = "" Then
                objAssetDeclare._Cash = 0
            Else
                objAssetDeclare._Cash = txtCash.Text.Trim
            End If
            If CDec(objAssetDeclare._Banktotal) > 0 Then
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'objAssetDeclare._CashExistingBank = Format(CDec(objAssetDeclare._Banktotal), Session("fmtCurrency"))
                objAssetDeclare._CashExistingBank = objAssetDeclare._Banktotal
                'Sohail (22 Feb 2020) -- End
            Else
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'objAssetDeclare._CashExistingBank = Format(0, Session("fmtCurrency"))
                objAssetDeclare._CashExistingBank = 0
                'Sohail (22 Feb 2020) -- End
            End If
            If objAssetBank.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetBank._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetBank._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetBank._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillBankGrid()
            'Hemant (24 Dec 2019) -- End

            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'SetBankTotal()
            txtCashExistingBank.Text = Format(CDec(objAssetDeclare._Banktotal), Session("fmtCurrency"))
            'Hemant (24 Dec 2019) -- End
            'Pinkal (30-Jan-2013) -- End


            Call Reset_Bank()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnAddBank_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub BtnUpdateBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUpdateBank.Click
        Try
            If IsValid_Bank() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("BankSrNo")
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtBank.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtBank.Rows(SrNo)

            ''drRow.Item("assetbanktranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("bank") = txtBank.Text.Trim
            'drRow.Item("account") = txtAccountNumber.Text.Trim
            'drRow.Item("amount") = txtAmountBank.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyBank.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyBank.SelectedItem.Value) 'Sohail (06 Apr 2012)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantBank.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantBank.Text.Trim
            'End If
            'If txtWifeHusbandBank.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbandBank.Text.Trim
            'End If
            'If txtChildrenBank.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenBank.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvBank, drRow, CInt(drpCurrencyBank.SelectedValue))

            ''Pinkal (30-Jan-2013) -- End



            'dtBank.AcceptChanges()

            'gvBank.DataSource = dtBank
            'gvBank.DataBind()
            Dim objAssetBank As New clsAsset_bank_tran

            objAssetBank.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvBank.DataKeys(SrNo).Item("assetbanktranunkid")), Nothing)
            objAssetBank._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetBank._Bank = txtBank.Text.Trim
            objAssetBank._Account = txtAccountNumber.Text.Trim
            objAssetBank._Amount = txtAmountBank.Text.Trim
            objAssetBank._Countryunkid = CInt(drpCurrencyBank.SelectedItem.Value)
            If txtServantBank.Text.Trim = "" Then
                objAssetBank._Servant = 0
            Else
                objAssetBank._Servant = txtServantBank.Text.Trim
            End If
            If txtWifeHusbandBank.Text.Trim = "" Then
                objAssetBank._Wifehusband = 0
            Else
                objAssetBank._Wifehusband = txtWifeHusbandBank.Text.Trim
            End If
            If txtChildrenBank.Text.Trim = "" Then
                objAssetBank._Children = 0
            Else
                objAssetBank._Children = txtChildrenBank.Text.Trim
            End If
            objAssetBank._Isfinalsaved = 0

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyBank.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetBank._CurrencyUnkId = intPaidCurrencyunkid
            objAssetBank._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetBank._Baseexchangerate = decBaseExRate
            objAssetBank._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetBank._Baseamount = objAssetBank._Amount * decBaseExRate / decPaidExRate
            Else
                objAssetBank._Baseamount = objAssetBank._Amount
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetBank._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetBank._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetBank._ClientIp = Session("IP_ADD").ToString()
            objAssetBank._HostName = Session("HOST_NAME").ToString()
            objAssetBank._FormName = mstrModuleName1
            objAssetBank._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1


            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsBank As DataSet = objAssetBank.GetList("List", mintAssetDeclarationUnkid)
            Dim dtBank As DataTable = dsBank.Tables(0)
            If dtBank IsNot Nothing Then
                Dim drRow() As DataRow = dtBank.Select()
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'If drRow.Length > 1 Then
                '    objAssetDeclare._Banktotal = objAssetBank._Baseamount + Format(CDec(dtBank.Compute("sum(baseamount)", " assetbanktranunkid <> " & objAssetBank._Assetbanktranunkid & " ")), Session("fmtCurrency"))
                'Else
                '    objAssetDeclare._Banktotal = Format(objAssetBank._Baseamount, Session("fmtCurrency"))
                'End If
                If drRow.Length > 1 Then
                    Dim decBankTotal As Decimal = (From p In dtBank Where (CInt(p.Item("assetbanktranunkid")) <> objAssetBank._Assetbanktranunkid) Select CDec(p.Item("baseamount"))).Sum()
                    objAssetDeclare._Banktotal = objAssetBank._Baseamount + decBankTotal
                Else
                    objAssetDeclare._Banktotal = objAssetBank._Baseamount
                End If
                'Sohail (22 Feb 2020) -- End
            End If
            If txtCash.Text.Trim = "" Then
                objAssetDeclare._Cash = 0
            Else
                objAssetDeclare._Cash = txtCash.Text.Trim
            End If
            If CDec(objAssetDeclare._Banktotal) > 0 Then
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'objAssetDeclare._CashExistingBank = Format(CDec(objAssetDeclare._Banktotal), Session("fmtCurrency"))
                objAssetDeclare._CashExistingBank = objAssetDeclare._Banktotal
                'Sohail (22 Feb 2020) -- End
            Else
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'objAssetDeclare._CashExistingBank = Format(0, Session("fmtCurrency"))
                objAssetDeclare._CashExistingBank = 0
                'Sohail (22 Feb 2020) -- End
            End If
            If objAssetBank.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetBank._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetBank._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetBank._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillBankGrid()
            'Hemant (24 Dec 2019) -- End

            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'SetBankTotal()
            txtCashExistingBank.Text = Format(CDec(objAssetDeclare._Banktotal), Session("fmtCurrency"))
            'Hemant (24 Dec 2019) -- End
            'Pinkal (30-Jan-2013) -- End


            Call Reset_Bank()
        Catch ex As Exception
            DisplayMessage.DisplayError("BtnUpdateBank_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBank.Click
        Try
            Call Reset_Bank()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetBank_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteBank_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteBank.buttonDelReasonYes_Click
        Dim objAssetBank As New clsAsset_bank_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("BankSrNo"))
            objAssetBank.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvBank.DataKeys(SrNo).Item("assetbanktranunkid")), Nothing)
            objAssetBank._Isvoid = True
            objAssetBank._Voiduserunkid = CInt(Session("UserId"))
            objAssetBank._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetBank._Voidreason = popupDeleteBank.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetBank._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetBank._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetBank._ClientIp = Session("IP_ADD").ToString()
            objAssetBank._HostName = Session("HOST_NAME").ToString()
            objAssetBank._FormName = mstrModuleName1
            objAssetBank._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsBank As DataSet = objAssetBank.GetList("List", mintAssetDeclarationUnkid)
            Dim dtBank As DataTable = dsBank.Tables(0)
            If dtBank IsNot Nothing Then
                Dim drRow() As DataRow = dtBank.Select()
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'If drRow.Length > 1 Then
                '    objAssetDeclare._Banktotal = Format(CDec(dtBank.Compute("sum(baseamount)", " assetbanktranunkid <> " & objAssetBank._Assetbanktranunkid & " ")), Session("fmtCurrency"))
                'Else
                '    objAssetDeclare._Banktotal = Format(0, Session("fmtCurrency"))
                'End If
                If drRow.Length > 1 Then
                    Dim decBankTotal As Decimal = (From p In dtBank Where (CInt(p.Item("assetbanktranunkid")) <> objAssetBank._Assetbanktranunkid) Select CDec(p.Item("baseamount"))).Sum()
                    objAssetDeclare._Banktotal = decBankTotal
                Else
                    objAssetDeclare._Banktotal = 0
                End If
                'Sohail (22 Feb 2020) -- End
            End If
            If txtCash.Text.Trim = "" Then
                objAssetDeclare._Cash = 0
            Else
                objAssetDeclare._Cash = txtCash.Text.Trim
            End If
            If CDec(objAssetDeclare._Banktotal) > 0 Then
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'objAssetDeclare._CashExistingBank = Format(CDec(objAssetDeclare._Banktotal), Session("fmtCurrency"))
                objAssetDeclare._CashExistingBank = objAssetDeclare._Banktotal
                'Sohail (22 Feb 2020) -- End
            Else
                'Sohail (22 Feb 2020) -- Start
                'TRA issue # 0004456: Getting message on final save asset declaration "sorry Total amount of respective banks should match with cash in existing bank".
                'objAssetDeclare._CashExistingBank = Format(0, Session("fmtCurrency"))
                objAssetDeclare._CashExistingBank = 0
                'Sohail (22 Feb 2020) -- End
            End If
            txtCashExistingBank.Text = Format(CDec(objAssetDeclare._Banktotal), Session("fmtCurrency"))
            If objAssetBank.Void(CInt(gvBank.DataKeys(SrNo).Item("assetbanktranunkid")), CInt(Session("UserId")), objAssetBank._Voiddatetime, objAssetBank._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillBankGrid()
            ElseIf objAssetBank._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetBank._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteBank_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End

#End Region

#Region " GridView Events "

    Protected Sub gvBank_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBank.PageIndexChanging
        Try
            gvBank.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvBank.DataSource = dtBank
            'gvBank.DataBind()
            Call FillBankGrid()
            'Hemant (24 Dec 2019) -- End            
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvBank_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvBank_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBank.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorBank.Text = ""
                lblErrorCBank.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("BankSrNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtBank.Text = dtBank.Rows(SrNo).Item("bank").ToString
                'txtAccountNumber.Text = dtBank.Rows(SrNo).Item("account").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtAmountBank.Text = Format(dtBank.Rows(SrNo).Item("amount"), GUI.fmtCurrency)
                'txtAmountBank.Text = Format(dtBank.Rows(SrNo).Item("amount"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End
                'txtServantBank.Text = dtBank.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbandBank.Text = dtBank.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenBank.Text = dtBank.Rows(SrNo).Item("children").ToString

                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                ''drpCurrencyBank.SelectedValue = dtBank.Rows(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyBank.SelectedValue = dtBank.Rows(SrNo).Item("countryunkid").ToString 'Sohail (06 Apr 2012)
                ''Pinkal (30-Jan-2013) -- End
                txtBank.Text = gvBank.Rows(SrNo).Cells(2).Text
                txtAccountNumber.Text = gvBank.Rows(SrNo).Cells(3).Text
                txtAmountBank.Text = Format(CDec(gvBank.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                txtServantBank.Text = gvBank.Rows(SrNo).Cells(6).Text
                txtWifeHusbandBank.Text = gvBank.Rows(SrNo).Cells(7).Text
                txtChildrenBank.Text = gvBank.Rows(SrNo).Cells(8).Text
                drpCurrencyBank.SelectedValue = CInt(gvBank.DataKeys(SrNo).Item("countryunkid").ToString)
                'Hemant (24 Dec 2019) -- End

                BtnAddBank.Enabled = False
                BtnUpdateBank.Enabled = True
            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtBank.Rows(CInt(e.CommandArgument)).Delete()

                'If dtBank.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtBank.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = ""

                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes
                '    r.Item(4) = 0
                '    'Pinkal (30-Jan-2013) -- End

                '    dtBank.Rows.Add(r)
                'End If
                'dtBank.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvBank.DataSource = dtBank
                'gvBank.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("BankSrNo") = SrNo
                popupDeleteBank.Show()
                'Hemant (24 Dec 2019) -- End

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                SetBankTotal()
                'Pinkal (30-Jan-2013) -- End


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvBank_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvBank_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBank.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else

                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), GUI.fmtCurrency)
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes


                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(5).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(5).Text = objExRate._Currency_Sign


                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If

                '    'Pinkal (30-Jan-2013) -- End

                '    'Sohail (06 Apr 2012) -- End
                'End If
                If IsDBNull(gvBank.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvBank_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

    'Pinkal (30-Jan-2013) -- Start
    'Enhancement : TRA Changes

#Region "Dropdown Events"

    Protected Sub drpCurrencyBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpCurrencyBank.SelectedIndexChanged, drpCurrencyShare.SelectedIndexChanged _
                                                                                                                                                                                , drpCurrencyHouse.SelectedIndexChanged, drpCurrencyPark.SelectedIndexChanged _
                                                                                                                                                                                , drpCurrencyVehicle.SelectedIndexChanged, drpCurrencyMachine.SelectedIndexChanged _
                                                                                                                                                                                , drpCurrencyBusiness.SelectedIndexChanged, drpCurrencyResources.SelectedIndexChanged _
                                                                                                                                                                                , drpCurrencyDebt.SelectedIndexChanged
        Try

            Dim cbo As Object = Nothing

            'If TypeOf sender Is DropDownList Then
            '    cbo = CType(sender, DropDownList)
            'ElseIf TypeOf sender Is ASP.controls_getcombolist_ascx Then
            '    cbo = CType(sender, ASP.controls_getcombolist_ascx)
            'End If
            cbo = sender

            Dim mstrClientID As String = cbo.ClientID.ToString().Replace("_drpCombo", "")

            If mstrClientID = drpCurrencyBank.ClientID Then
                If txtAmountBank.Text.Trim.Length <= 0 Then
                    txtAmountBank.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtAmountBank.Text = Format(CDec(txtAmountBank.Text.Trim), Session("fmtcurrency"))
                End If

                If txtCash.Text.Trim.Length <= 0 Then
                    txtCash.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtCash.Text = Format(CDec(txtCash.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyShare.ClientID Then
                If txtShareValue.Text.Trim.Length <= 0 Then
                    txtShareValue.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtShareValue.Text = Format(CDec(txtShareValue.Text.Trim), Session("fmtcurrency"))
                End If

                If txtDividendAmount.Text.Trim.Length <= 0 Then
                    txtDividendAmount.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtDividendAmount.Text = Format(CDec(txtDividendAmount.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyHouse.ClientID Then

                If txtValueHomeBuilding.Text.Trim.Length <= 0 Then
                    txtValueHomeBuilding.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtValueHomeBuilding.Text = Format(CDec(txtValueHomeBuilding.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyPark.ClientID Then

                If txtValueParkFarmMine.Text.Trim.Length <= 0 Then
                    txtValueParkFarmMine.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtValueParkFarmMine.Text = Format(CDec(txtValueParkFarmMine.Text.Trim), Session("fmtcurrency"))
                End If


            ElseIf mstrClientID = drpCurrencyVehicle.ClientID Then

                If txtValueVehicle.Text.Trim.Length <= 0 Then
                    txtValueVehicle.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtValueVehicle.Text = Format(CDec(txtValueVehicle.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyMachine.ClientID Then

                If txtValueMachinery.Text.Trim.Length <= 0 Then
                    txtValueMachinery.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtValueMachinery.Text = Format(CDec(txtValueMachinery.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyBusiness.ClientID Then

                If txtValueOtherBusiness.Text.Trim.Length <= 0 Then
                    txtValueOtherBusiness.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtValueOtherBusiness.Text = Format(CDec(txtValueOtherBusiness.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyResources.ClientID Then

                If txtResourceValue.Text.Trim.Length <= 0 Then
                    txtResourceValue.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtResourceValue.Text = Format(CDec(txtResourceValue.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpCurrencyDebt.ClientID Then

                If txtDebtValue.Text.Trim.Length <= 0 Then
                    txtDebtValue.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtDebtValue.Text = Format(CDec(txtDebtValue.Text.Trim), Session("fmtcurrency"))
                End If

            End If


            If cbo.SelectedIndex > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim dsList As DataSet
                Dim dtTable As DataTable
                Dim mdecBaseExRate As Decimal = 0
                Dim mdecPaidExRate As Decimal = 0

                dsList = objExRate.GetList("ExRate", True, , , CInt(cbo.SelectedValue), True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
                dtTable = New DataView(dsList.Tables("ExRate")).ToTable
                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    lblExchangeRate.Text = Format(mdecBaseExRate, Session("fmtCurrency")) & " " & Me.ViewState("currencysign") & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                End If

            Else
                lblExchangeRate.Text = ""
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpCurrencyBank_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("drpCurrencyBank_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

#End Region

    'Pinkal (30-Jan-2013) -- End


#End Region

#Region " Share Dividends "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboShare()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrShare As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrShare = objExchange.getComboList("Currency", True)
            With drpCurrencyShare

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"
                'Pinkal (30-Jan-2013) -- End


                .DataTextField = "currency_sign"
                .DataSource = dsCurrShare.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboShare :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillShareDividendGrid()
        Dim objAssetShareDividend As New clsAsset_sharedividend_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtShare As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetShareDividend._Assetdeclarationunkid = mintAssetDeclarationUnkid

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetShareDividend._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtShare = objAssetShareDividend._Datasource

            'If dtShare.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtShare.NewRow

            '    r.Item(3) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = -1
            '    dtShare.Rows.Add(r)
            'End If
            'Hemant (24 Dec 2019) -- End
            Dim dsShare As DataSet = objAssetShareDividend.GetList("List", mintAssetDeclarationUnkid)
            dtShare = dsShare.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtShare.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtShare.NewRow
                dtShare.Rows.Add(r)
            End If
            gvShareDividend.DataSource = dtShare
            gvShareDividend.DataBind()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvShareDividend.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillShareDividendGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_ShareDividend() As Boolean
        Try
            lblErrorShareDividend.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCShareDividend.Text = ""
            If CInt(drpCurrencyShare.SelectedItem.Value) <= 0 Then
                lblErrorCShareDividend.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantShareDividend.Text, decServant)
            Decimal.TryParse(txtWifeHusbandShareDividend.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenShareDividend.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorShareDividend.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_ShareDividend :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_ShareDividend()
        Try
            txtShareValue.Text = ""
            txtLocationShareDividend.Text = ""
            txtDividendAmount.Text = ""
            txtServantShareDividend.Text = ""
            txtWifeHusbandShareDividend.Text = ""
            txtChildrenShareDividend.Text = ""
            lblErrorShareDividend.Text = ""
            lblErrorCShareDividend.Text = "" 'Sohail (06 Apr 2012)

            btnAddShareDividend.Enabled = True
            btnUpdateShareDividend.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyShare.SelectedIndex = 0
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_ShareDividend :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddShareDividend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddShareDividend.Click
        Try
            If IsValid_ShareDividend() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtShare.NewRow

            'drRow.Item("assetsharedividendtranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("sharevalue") = txtShareValue.Text.Trim
            'drRow.Item("location") = txtLocationShareDividend.Text.Trim
            'drRow.Item("dividendamount") = txtDividendAmount.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyShare.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyShare.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantShareDividend.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantShareDividend.Text.Trim
            'End If
            'If txtWifeHusbandShareDividend.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbandShareDividend.Text.Trim
            'End If
            'If txtChildrenShareDividend.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenShareDividend.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'Update_DataGridview_DataTable_Extra_Columns(gvShareDividend, drRow, CInt(drpCurrencyShare.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtShare.Rows.Add(drRow)

            'gvShareDividend.DataSource = dtShare
            'gvShareDividend.DataBind()
            Dim objAssetShare As New clsAsset_sharedividend_tran
            objAssetShare._Assetsharedividendtranunkid = -1
            objAssetShare._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetShare._Sharevalue = txtShareValue.Text.Trim
            objAssetShare._Location = txtLocationShareDividend.Text.Trim
            objAssetShare._Dividendamount = txtDividendAmount.Text.Trim
            objAssetShare._Countryunkid = CInt(drpCurrencyShare.SelectedItem.Value)
            If txtServantShareDividend.Text.Trim = "" Then
                objAssetShare._Servant = 0
            Else
                objAssetShare._Servant = txtServantShareDividend.Text.Trim
            End If
            If txtWifeHusbandShareDividend.Text.Trim = "" Then
                objAssetShare._Wifehusband = 0
            Else
                objAssetShare._Wifehusband = txtWifeHusbandShareDividend.Text.Trim
            End If
            If txtChildrenShareDividend.Text.Trim = "" Then
                objAssetShare._Children = 0
            Else
                objAssetShare._Children = txtChildrenShareDividend.Text.Trim
            End If
            objAssetShare._Isfinalsaved = 0
            objAssetShare._Userunkid = CInt(Session("UserId"))
            If objAssetShare._Transactiondate = Nothing Then
                objAssetShare._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyShare.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetShare._CurrencyUnkId = intPaidCurrencyunkid
            objAssetShare._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetShare._Baseexchangerate = decBaseExRate
            objAssetShare._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetShare._Baseamount = objAssetShare._Sharevalue * decBaseExRate / decPaidExRate
            Else
                objAssetShare._Baseamount = objAssetShare._Sharevalue
            End If

            If decPaidExRate <> 0 Then
                objAssetShare._BaseDividendamount = objAssetShare._Dividendamount * decBaseExRate / decPaidExRate
            Else
                objAssetShare._BaseDividendamount = objAssetShare._Dividendamount
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetShare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetShare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetShare._ClientIp = Session("IP_ADD").ToString()
            objAssetShare._HostName = Session("HOST_NAME").ToString()
            objAssetShare._FormName = mstrModuleName1
            objAssetShare._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsShare As DataSet = objAssetShare.GetList("List", mintAssetDeclarationUnkid)
            Dim dtShare As DataTable = dsShare.Tables(0)
            If dtShare IsNot Nothing Then
                Dim drRow() As DataRow = dtShare.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Sharedividendtotal = objAssetShare._Baseamount + Format(CDec(dtShare.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Sharedividendtotal = Format(objAssetShare._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetShare.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetShare._Message <> "" Then
                    DisplayMessage.DisplayError(objAssetShare._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetShare._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillShareDividendGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_ShareDividend()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddShareDividend_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateShareDividend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateShareDividend.Click
        Try
            If IsValid_ShareDividend() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("ShareSrNo")
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtShare.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtShare.Rows(SrNo)

            ''drRow.Item("assetsharedividendtranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("sharevalue") = txtShareValue.Text.Trim
            'drRow.Item("location") = txtLocationShareDividend.Text.Trim
            'drRow.Item("dividendamount") = txtDividendAmount.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyShare.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyShare.SelectedItem.Value) 'Sohail (06 Apr 2012)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantShareDividend.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantShareDividend.Text.Trim
            'End If
            'If txtWifeHusbandShareDividend.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbandShareDividend.Text.Trim
            'End If
            'If txtChildrenShareDividend.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenShareDividend.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvShareDividend, drRow, CInt(drpCurrencyShare.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End

            'dtShare.AcceptChanges()

            'gvShareDividend.DataSource = dtShare
            'gvShareDividend.DataBind()
            Dim objAssetShare As New clsAsset_sharedividend_tran
            objAssetShare.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvShareDividend.DataKeys(SrNo).Item("assetsharedividendtranunkid")), Nothing)
            objAssetShare._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetShare._Sharevalue = txtShareValue.Text.Trim
            objAssetShare._Location = txtLocationShareDividend.Text.Trim
            objAssetShare._Dividendamount = txtDividendAmount.Text.Trim
            objAssetShare._Countryunkid = CInt(drpCurrencyShare.SelectedItem.Value)
            If txtServantShareDividend.Text.Trim = "" Then
                objAssetShare._Servant = 0
            Else
                objAssetShare._Servant = txtServantShareDividend.Text.Trim
            End If
            If txtWifeHusbandShareDividend.Text.Trim = "" Then
                objAssetShare._Wifehusband = 0
            Else
                objAssetShare._Wifehusband = txtWifeHusbandShareDividend.Text.Trim
            End If
            If txtChildrenShareDividend.Text.Trim = "" Then
                objAssetShare._Children = 0
            Else
                objAssetShare._Children = txtChildrenShareDividend.Text.Trim
            End If
            objAssetShare._Isfinalsaved = 0
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyShare.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetShare._CurrencyUnkId = intPaidCurrencyunkid
            objAssetShare._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetShare._Baseexchangerate = decBaseExRate
            objAssetShare._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetShare._Baseamount = objAssetShare._Sharevalue * decBaseExRate / decPaidExRate
            Else
                objAssetShare._Baseamount = objAssetShare._Sharevalue
            End If

            If decPaidExRate <> 0 Then
                objAssetShare._BaseDividendamount = objAssetShare._Dividendamount * decBaseExRate / decPaidExRate
            Else
                objAssetShare._BaseDividendamount = objAssetShare._Dividendamount
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetShare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetShare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetShare._ClientIp = Session("IP_ADD").ToString()
            objAssetShare._HostName = Session("HOST_NAME").ToString()
            objAssetShare._FormName = mstrModuleName1
            objAssetShare._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsShare As DataSet = objAssetShare.GetList("List", mintAssetDeclarationUnkid)
            Dim dtShare As DataTable = dsShare.Tables(0)
            If dtShare IsNot Nothing Then
                Dim drRow() As DataRow = dtShare.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Sharedividendtotal = objAssetShare._Baseamount + Format(CDec(dtShare.Compute("sum(baseamount)", " assetsharedividendtranunkid <> " & objAssetShare._Assetsharedividendtranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Sharedividendtotal = Format(objAssetShare._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetShare.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetShare._Message <> "" Then
                    DisplayMessage.DisplayError(objAssetShare._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetShare._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillShareDividendGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_ShareDividend()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateShareDividend_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetShareDividend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetShareDividend.Click
        Try
            Call Reset_ShareDividend()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetShareDividend_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteShareDividend_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteShareDividend.buttonDelReasonYes_Click
        Dim objAssetShare As New clsAsset_sharedividend_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("ShareSrNo"))
            objAssetShare.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvShareDividend.DataKeys(SrNo).Item("assetsharedividendtranunkid")), Nothing)
            objAssetShare._Isvoid = True
            objAssetShare._Voiduserunkid = CInt(Session("UserId"))
            objAssetShare._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetShare._Voidreason = popupDeleteShareDividend.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetShare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetShare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetShare._ClientIp = Session("IP_ADD").ToString()
            objAssetShare._HostName = Session("HOST_NAME").ToString()
            objAssetShare._FormName = mstrModuleName1
            objAssetShare._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsShare As DataSet = objAssetShare.GetList("List", mintAssetDeclarationUnkid)
            Dim dtShare As DataTable = dsShare.Tables(0)
            If dtShare IsNot Nothing Then
                Dim drRow() As DataRow = dtShare.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Sharedividendtotal = Format(CDec(dtShare.Compute("sum(baseamount)", " assetsharedividendtranunkid <> " & objAssetShare._Assetsharedividendtranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Sharedividendtotal = Format(0, Session("fmtCurrency"))
                End If
            End If
            If objAssetShare.Void(CInt(gvShareDividend.DataKeys(SrNo).Item("assetsharedividendtranunkid")), CInt(Session("UserId")), objAssetShare._Voiddatetime, objAssetShare._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillShareDividendGrid()
            ElseIf objAssetShare._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetShare._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteShareDividend_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvShareDividend_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvShareDividend.PageIndexChanging
        Try
            gvShareDividend.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvShareDividend.DataSource = dtShare
            gvShareDividend.DataBind()
            Call FillShareDividendGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvShareDividend_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvShareDividend_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvShareDividend.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorShareDividend.Text = ""
                lblErrorCShareDividend.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ShareSrNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtShareValue.Text = Format(dtShare.Rows(SrNo).Item("sharevalue"), GUI.fmtCurrency)
                'txtShareValue.Text = Format(dtShare.Rows(SrNo).Item("sharevalue"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End
                'txtLocationShareDividend.Text = dtShare.Rows(SrNo).Item("location").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtDividendAmount.Text = Format(dtShare.Rows(SrNo).Item("dividendamount"), GUI.fmtCurrency)
                'txtDividendAmount.Text = Format(dtShare.Rows(SrNo).Item("dividendamount"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End


                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                ''drpCurrencyShare.SelectedValue = dtShare.Rows(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyShare.SelectedValue = dtShare.Rows(SrNo).Item("countryunkid").ToString
                ''Pinkal (30-Jan-2013) -- End


                'txtServantShareDividend.Text = dtShare.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbandShareDividend.Text = dtShare.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenShareDividend.Text = dtShare.Rows(SrNo).Item("children").ToString
                txtShareValue.Text = Format(CDec(gvShareDividend.Rows(SrNo).Cells(2).Text), Session("fmtCurrency"))
                txtLocationShareDividend.Text = gvShareDividend.Rows(SrNo).Cells(3).Text.ToString
                txtDividendAmount.Text = Format(CDec(gvShareDividend.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpCurrencyShare.SelectedValue = CInt(gvShareDividend.DataKeys(SrNo).Item("countryunkid").ToString)
                txtServantShareDividend.Text = gvShareDividend.Rows(SrNo).Cells(6).Text
                txtWifeHusbandShareDividend.Text = gvShareDividend.Rows(SrNo).Cells(7).Text
                txtChildrenShareDividend.Text = gvShareDividend.Rows(SrNo).Cells(8).Text
                'Hemant (24 Dec 2019) -- End
                btnAddShareDividend.Enabled = False
                btnUpdateShareDividend.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtShare.Rows(CInt(e.CommandArgument)).Delete()

                'If dtShare.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtShare.NewRow

                '    r.Item(3) = "None" ' To Hide the row and display only Row Header
                '    r.Item(4) = -1
                '    dtShare.Rows.Add(r)
                'End If
                'dtShare.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvShareDividend.DataSource = dtShare
                'gvShareDividend.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ShareSrNo") = SrNo
                popupDeleteShareDividend.Show()
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvSahreDividend_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvShareDividend_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvShareDividend.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(3).Text = "None" AndAlso e.Row.Cells(4).Text = "-1" Then
                '    e.Row.Visible = False
                'Else
                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(2).Text = Format(CDec(e.Row.Cells(2).Text), GUI.fmtCurrency)
                '    'e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), GUI.fmtCurrency)
                '    e.Row.Cells(2).Text = Format(CDec(e.Row.Cells(2).Text), Session("fmtCurrency"))
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes

                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(5).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(5).Text = objExRate._Currency_Sign


                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If

                '    'Pinkal (30-Jan-2013) -- End

                '    'Sohail (06 Apr 2012) -- End
                'End If
                If IsDBNull(gvShareDividend.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(2).Text = Format(CDec(e.Row.Cells(2).Text), Session("fmtCurrency"))
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvSahreDividend_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " House Building "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboHouse()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrHouse As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrHouse = objExchange.getComboList("Currency", True)
            With drpCurrencyHouse

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"
                'Pinkal (30-Jan-2013) -- End

                .DataTextField = "currency_sign"
                .DataSource = dsCurrHouse.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboHouse :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillHouseBuildingGrid()
        Dim objAssetHouseBuilding As New clsAsset_housebuilding_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtHouse As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetHouseBuilding._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetHouseBuilding._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtHouse = objAssetHouseBuilding._Datasource

            'If dtHouse.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtHouse.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(3) = ""
            '    dtHouse.Rows.Add(r)
            'End If
            Dim dsHouse As DataSet = objAssetHouseBuilding.GetList("List", mintAssetDeclarationUnkid)
            dtHouse = dsHouse.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtHouse.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtHouse.NewRow
                dtHouse.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvHouseBuilding.DataSource = dtHouse
            gvHouseBuilding.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvHouseBuilding.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("FillHouseBuildingGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_HouseBuilding() As Boolean
        Try
            lblErrorHouseBuilding.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCHouseBuilding.Text = ""
            If CInt(drpCurrencyHouse.SelectedItem.Value) <= 0 Then
                lblErrorCHouseBuilding.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantHomeBuilding.Text, decServant)
            Decimal.TryParse(txtWifeHusbundHomeBuilding.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenHomeBuilding.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorHouseBuilding.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_HouseBuilding :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_HouseBuilding()
        Try
            txtHomeBuilding.Text = ""
            txtLocationHomeBuilding.Text = ""
            txtValueHomeBuilding.Text = ""
            txtServantHomeBuilding.Text = ""
            txtWifeHusbundHomeBuilding.Text = ""
            txtChildrenHomeBuilding.Text = ""
            lblErrorHouseBuilding.Text = ""
            lblErrorCHouseBuilding.Text = "" 'Sohail (06 Apr 2012)

            btnAddHomeBuilding.Enabled = True
            btnUpdateHomeBuilding.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyHouse.SelectedIndex = 0
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_HouseBuilding :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddHomeBuilding_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddHomeBuilding.Click
        Try
            If IsValid_HouseBuilding() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtHouse.NewRow

            'drRow.Item("assethousebuildingtranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("homebuilding") = txtHomeBuilding.Text.Trim
            'drRow.Item("location") = txtLocationHomeBuilding.Text.Trim
            'drRow.Item("value") = txtValueHomeBuilding.Text.Trim


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyHouse.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyHouse.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantHomeBuilding.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantHomeBuilding.Text.Trim
            'End If
            'If txtWifeHusbundHomeBuilding.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundHomeBuilding.Text.Trim
            'End If
            'If txtChildrenHomeBuilding.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenHomeBuilding.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'Update_DataGridview_DataTable_Extra_Columns(gvHouseBuilding, drRow, CInt(drpCurrencyHouse.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtHouse.Rows.Add(drRow)

            'gvHouseBuilding.DataSource = dtHouse
            'gvHouseBuilding.DataBind()
            Dim objAssetHouse As New clsAsset_housebuilding_tran
            objAssetHouse._Assethousebuildingtranunkid = -1
            objAssetHouse._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetHouse._Homebuilding = txtHomeBuilding.Text.Trim
            objAssetHouse._Location = txtLocationHomeBuilding.Text.Trim
            objAssetHouse._Value = txtValueHomeBuilding.Text.Trim
            objAssetHouse._Countryunkid = CInt(drpCurrencyHouse.SelectedItem.Value)
            If txtServantHomeBuilding.Text.Trim = "" Then
                objAssetHouse._Servant = 0
            Else
                objAssetHouse._Servant = txtServantHomeBuilding.Text.Trim
            End If
            If txtWifeHusbundHomeBuilding.Text.Trim = "" Then
                objAssetHouse._Wifehusband = 0
            Else
                objAssetHouse._Wifehusband = txtWifeHusbundHomeBuilding.Text.Trim
            End If
            If txtChildrenHomeBuilding.Text.Trim = "" Then
                objAssetHouse._Children = 0
            Else
                objAssetHouse._Children = txtChildrenHomeBuilding.Text.Trim
            End If
            objAssetHouse._Isfinalsaved = 0
            objAssetHouse._Userunkid = CInt(Session("UserId"))
            If objAssetHouse._Transactiondate = Nothing Then
                objAssetHouse._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyHouse.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetHouse._CurrencyUnkId = intPaidCurrencyunkid
            objAssetHouse._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetHouse._Baseexchangerate = decBaseExRate
            objAssetHouse._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetHouse._Baseamount = objAssetHouse._Value * decBaseExRate / decPaidExRate
            Else
                objAssetHouse._Baseamount = objAssetHouse._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetHouse._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetHouse._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetHouse._ClientIp = Session("IP_ADD").ToString()
            objAssetHouse._HostName = Session("HOST_NAME").ToString()
            objAssetHouse._FormName = mstrModuleName1
            objAssetHouse._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsHouse As DataSet = objAssetHouse.GetList("List", mintAssetDeclarationUnkid)
            Dim dtHouse As DataTable = dsHouse.Tables(0)
            If dtHouse IsNot Nothing Then
                Dim drRow() As DataRow = dtHouse.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Housetotal = objAssetHouse._Baseamount + Format(CDec(dtHouse.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Housetotal = Format(objAssetHouse._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetHouse.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetHouse._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetHouse._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetHouse._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillHouseBuildingGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_HouseBuilding()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddHomeBuilding_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateHomeBuilding_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateHomeBuilding.Click
        Try
            If IsValid_HouseBuilding() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("HouseSrNo")

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtHouse.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtHouse.Rows(SrNo)

            ''drRow.Item("assethousebuildingtranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("homebuilding") = txtHomeBuilding.Text.Trim
            'drRow.Item("location") = txtLocationHomeBuilding.Text.Trim
            'drRow.Item("value") = txtValueHomeBuilding.Text.Trim


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyHouse.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyHouse.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantHomeBuilding.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantHomeBuilding.Text.Trim
            'End If
            'If txtWifeHusbundHomeBuilding.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundHomeBuilding.Text.Trim
            'End If
            'If txtChildrenHomeBuilding.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenHomeBuilding.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvHouseBuilding, drRow, CInt(drpCurrencyHouse.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtHouse.AcceptChanges()

            'gvHouseBuilding.DataSource = dtHouse
            'gvHouseBuilding.DataBind()
            Dim objAssetHouse As New clsAsset_housebuilding_tran
            objAssetHouse.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvHouseBuilding.DataKeys(SrNo).Item("assethousebuildingtranunkid")), Nothing)
            objAssetHouse._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetHouse._Homebuilding = txtHomeBuilding.Text.Trim
            objAssetHouse._Location = txtLocationHomeBuilding.Text.Trim
            objAssetHouse._Value = txtValueHomeBuilding.Text.Trim
            objAssetHouse._Countryunkid = CInt(drpCurrencyHouse.SelectedItem.Value)
            If txtServantHomeBuilding.Text.Trim = "" Then
                objAssetHouse._Servant = 0
            Else
                objAssetHouse._Servant = txtServantHomeBuilding.Text.Trim
            End If
            If txtWifeHusbundHomeBuilding.Text.Trim = "" Then
                objAssetHouse._Wifehusband = 0
            Else
                objAssetHouse._Wifehusband = txtWifeHusbundHomeBuilding.Text.Trim
            End If
            If txtChildrenHomeBuilding.Text.Trim = "" Then
                objAssetHouse._Children = 0
            Else
                objAssetHouse._Children = txtChildrenHomeBuilding.Text.Trim
            End If
            objAssetHouse._Isfinalsaved = 0
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyHouse.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetHouse._CurrencyUnkId = intPaidCurrencyunkid
            objAssetHouse._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetHouse._Baseexchangerate = decBaseExRate
            objAssetHouse._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetHouse._Baseamount = objAssetHouse._Value * decBaseExRate / decPaidExRate
            Else
                objAssetHouse._Baseamount = objAssetHouse._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetHouse._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetHouse._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetHouse._ClientIp = Session("IP_ADD").ToString()
            objAssetHouse._HostName = Session("HOST_NAME").ToString()
            objAssetHouse._FormName = mstrModuleName1
            objAssetHouse._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsHouse As DataSet = objAssetHouse.GetList("List", mintAssetDeclarationUnkid)
            Dim dtHouse As DataTable = dsHouse.Tables(0)
            If dtHouse IsNot Nothing Then
                Dim drRow() As DataRow = dtHouse.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Housetotal = objAssetHouse._Baseamount + Format(CDec(dtHouse.Compute("sum(baseamount)", " assethousebuildingtranunkid <> " & objAssetHouse._Assethousebuildingtranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Housetotal = Format(objAssetHouse._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetHouse.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetHouse._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetHouse._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetHouse._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillHouseBuildingGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_HouseBuilding()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateHomeBuilding_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetHomeBuilding_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetHomeBuilding.Click
        Try
            Call Reset_HouseBuilding()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetHomeBuilding_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteHouseBuilding_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteHouseBuilding.buttonDelReasonYes_Click
        Dim objAssetHouse As New clsAsset_housebuilding_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("HouseSrNo"))
            objAssetHouse.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvHouseBuilding.DataKeys(SrNo).Item("assethousebuildingtranunkid")), Nothing)
            objAssetHouse._Isvoid = True
            objAssetHouse._Voiduserunkid = CInt(Session("UserId"))
            objAssetHouse._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetHouse._Voidreason = popupDeleteHouseBuilding.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetHouse._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetHouse._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetHouse._ClientIp = Session("IP_ADD").ToString()
            objAssetHouse._HostName = Session("HOST_NAME").ToString()
            objAssetHouse._FormName = mstrModuleName1
            objAssetHouse._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsHouse As DataSet = objAssetHouse.GetList("List", mintAssetDeclarationUnkid)
            Dim dtHouse As DataTable = dsHouse.Tables(0)
            If dtHouse IsNot Nothing Then
                Dim drRow() As DataRow = dtHouse.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Housetotal = Format(CDec(dtHouse.Compute("sum(baseamount)", " assethousebuildingtranunkid <> " & objAssetHouse._Assethousebuildingtranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Housetotal = Format(0, Session("fmtCurrency"))
                End If
            End If
            If objAssetHouse.Void(CInt(gvHouseBuilding.DataKeys(SrNo).Item("assethousebuildingtranunkid")), CInt(Session("UserId")), objAssetHouse._Voiddatetime, objAssetHouse._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillHouseBuildingGrid()
            ElseIf objAssetHouse._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetHouse._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteHouseBuilding_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvHouseBuilding_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvHouseBuilding.PageIndexChanging
        Try
            gvHouseBuilding.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvHouseBuilding.DataSource = dtHouse
            'gvHouseBuilding.DataBind()
            Call FillHouseBuildingGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvHouseBuilding_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvHouseBuilding_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvHouseBuilding.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorHouseBuilding.Text = ""
                lblErrorCHouseBuilding.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("HouseSrNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtHomeBuilding.Text = dtHouse.Rows(SrNo).Item("homebuilding").ToString
                'txtLocationHomeBuilding.Text = dtHouse.Rows(SrNo).Item("location").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtValueHomeBuilding.Text = Format(dtHouse.Rows(SrNo).Item("value"), GUI.fmtCurrency)
                'txtValueHomeBuilding.Text = Format(dtHouse.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End


                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                ''drpCurrencyHouse.SelectedValue = dtHouse(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyHouse.SelectedValue = dtHouse(SrNo).Item("countryunkid").ToString 'Sohail (06 Apr 2012)
                ''Pinkal (30-Jan-2013) -- End


                'txtServantHomeBuilding.Text = dtHouse.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbundHomeBuilding.Text = dtHouse.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenHomeBuilding.Text = dtHouse.Rows(SrNo).Item("children").ToString
                txtHomeBuilding.Text = gvHouseBuilding.Rows(SrNo).Cells(2).Text
                txtLocationHomeBuilding.Text = gvHouseBuilding.Rows(SrNo).Cells(3).Text
                txtValueHomeBuilding.Text = Format(CDec(gvHouseBuilding.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpCurrencyHouse.SelectedValue = CInt(gvHouseBuilding.DataKeys(SrNo).Item("countryunkid").ToString)
                txtServantHomeBuilding.Text = gvHouseBuilding.Rows(SrNo).Cells(6).Text
                txtWifeHusbundHomeBuilding.Text = gvHouseBuilding.Rows(SrNo).Cells(7).Text
                txtChildrenHomeBuilding.Text = gvHouseBuilding.Rows(SrNo).Cells(8).Text
                'Hemant (24 Dec 2019) -- End
                btnAddHomeBuilding.Enabled = False
                btnUpdateHomeBuilding.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtHouse.Rows(CInt(e.CommandArgument)).Delete()

                'If dtHouse.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtHouse.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = ""
                '    dtHouse.Rows.Add(r)
                'End If
                'dtHouse.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvHouseBuilding.DataSource = dtHouse
                'gvHouseBuilding.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("HouseSrNo") = SrNo
                popupDeleteHouseBuilding.Show()
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvHouseBuilding_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvHouseBuilding_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvHouseBuilding.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), GUI.fmtCurrency)
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes

                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(5).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(5).Text = objExRate._Currency_Sign

                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If


                '    'Pinkal (30-Jan-2013) -- End


                '    'Sohail (06 Apr 2012) -- End
                'End If
                If IsDBNull(gvHouseBuilding.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvHouseBuilding_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Park Farm Mines "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboPark()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrPark As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrPark = objExchange.getComboList("Currency", True)
            With drpCurrencyPark

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"
                'Pinkal (30-Jan-2013) -- End


                .DataTextField = "currency_sign"
                .DataSource = dsCurrPark.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboPark :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillParmFarmMineGrid()
        Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtParkFarm As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetParkFarm._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetParkFarm._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtPark = objAssetParkFarm._Datasource

            'If dtPark.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtPark.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = ""
            '    dtPark.Rows.Add(r)
            'End If
            Dim dsParkFarm As DataSet = objAssetParkFarm.GetList("List", mintAssetDeclarationUnkid)
            dtParkFarm = dsParkFarm.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtParkFarm.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtParkFarm.NewRow
                dtParkFarm.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvParkFarm.DataSource = dtParkFarm
            gvParkFarm.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvParkFarm.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillParmFarmMineGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_ParkFarmMine() As Boolean
        Try
            lblErrorParkFarmMine.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCParkFarmMine.Text = ""
            If CInt(drpCurrencyPark.SelectedItem.Value) <= 0 Then
                lblErrorCParkFarmMine.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantParkFarmMine.Text, decServant)
            Decimal.TryParse(txtWifeHusbundParkFarmMine.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenParkFarmMine.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorParkFarmMine.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_ParkFarmMine :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_ParkFarmMine()
        Try
            txtParkFarmsMines.Text = ""
            txtSizeofAreaParkFarmMine.Text = ""
            txtLocationParkFarmMine.Text = ""
            txtValueParkFarmMine.Text = ""
            txtServantParkFarmMine.Text = ""
            txtWifeHusbundParkFarmMine.Text = ""
            txtChildrenParkFarmMine.Text = ""
            lblErrorParkFarmMine.Text = ""
            lblErrorCParkFarmMine.Text = "" 'Sohail (06 Apr 2012)

            btnAddParkFarmMine.Enabled = True
            btnUpdateParkFarmMine.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyPark.SelectedIndex = 0
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_ParkFarmMine :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddParkFarmMine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddParkFarmMine.Click
        Try
            If IsValid_ParkFarmMine() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtPark.NewRow

            'drRow.Item("assetparkfarmminestranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("parkfarmmines") = txtParkFarmsMines.Text.Trim
            'drRow.Item("sizeofarea") = txtSizeofAreaParkFarmMine.Text.Trim
            'drRow.Item("placeclad") = txtLocationParkFarmMine.Text.Trim
            'drRow.Item("value") = txtValueParkFarmMine.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyPark.SelectedItem.Value) 'Sohail (06 Apr 2012)
            ''Sohail (24 Feb 2015) -- Start
            ''Issue - wrong currency was coming in grid view list.
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyPark.SelectedItem.Value)
            'drRow.Item("countryunkid") = CInt(drpCurrencyPark.SelectedItem.Value)
            ''Sohail (24 Feb 2015) -- End
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantParkFarmMine.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantParkFarmMine.Text.Trim
            'End If
            'If txtWifeHusbundParkFarmMine.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundParkFarmMine.Text.Trim
            'End If
            'If txtChildrenParkFarmMine.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenParkFarmMine.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'Update_DataGridview_DataTable_Extra_Columns(gvParkFarm, drRow, CInt(drpCurrencyPark.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtPark.Rows.Add(drRow)

            'gvParkFarm.DataSource = dtPark
            'gvParkFarm.DataBind()
            Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran

            objAssetParkFarm._Assetparkfarmminestranunkid = -1
            objAssetParkFarm._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetParkFarm._Parkfarmmines = txtParkFarmsMines.Text.Trim
            objAssetParkFarm._Sizeofarea = txtSizeofAreaParkFarmMine.Text.Trim
            objAssetParkFarm._Placeclad = txtLocationParkFarmMine.Text.Trim
            objAssetParkFarm._Value = txtValueParkFarmMine.Text.Trim
            objAssetParkFarm._Countryunkid = CInt(drpCurrencyPark.SelectedItem.Value)
            If txtServantParkFarmMine.Text.Trim = "" Then
                objAssetParkFarm._Servant = 0
            Else
                objAssetParkFarm._Servant = txtServantParkFarmMine.Text.Trim
            End If
            If txtWifeHusbundParkFarmMine.Text.Trim = "" Then
                objAssetParkFarm._Wifehusband = 0
            Else
                objAssetParkFarm._Wifehusband = txtWifeHusbundParkFarmMine.Text.Trim
            End If
            If txtChildrenParkFarmMine.Text.Trim = "" Then
                objAssetParkFarm._Children = 0
            Else
                objAssetParkFarm._Children = txtChildrenParkFarmMine.Text.Trim
            End If
            objAssetParkFarm._Isfinalsaved = 0
            objAssetParkFarm._Userunkid = CInt(Session("UserId"))
            If objAssetParkFarm._Transactiondate = Nothing Then
                objAssetParkFarm._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyPark.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetParkFarm._CurrencyUnkId = intPaidCurrencyunkid
            objAssetParkFarm._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetParkFarm._Baseexchangerate = decBaseExRate
            objAssetParkFarm._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetParkFarm._Baseamount = objAssetParkFarm._Value * decBaseExRate / decPaidExRate
            Else
                objAssetParkFarm._Baseamount = objAssetParkFarm._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetParkFarm._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetParkFarm._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetParkFarm._ClientIp = Session("IP_ADD").ToString()
            objAssetParkFarm._HostName = Session("HOST_NAME").ToString()
            objAssetParkFarm._FormName = mstrModuleName1
            objAssetParkFarm._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsParkFarm As DataSet = objAssetParkFarm.GetList("List", mintAssetDeclarationUnkid)
            Dim dtParkFarm As DataTable = dsParkFarm.Tables(0)
            If dtParkFarm IsNot Nothing Then
                Dim drRow() As DataRow = dtParkFarm.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Parkfarmtotal = objAssetParkFarm._Baseamount + Format(CDec(dtParkFarm.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Parkfarmtotal = Format(objAssetParkFarm._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetParkFarm.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetParkFarm._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetParkFarm._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetParkFarm._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillParmFarmMineGrid()
            'Hemant (24 Dec 2019) -- End
            Call Reset_ParkFarmMine()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddParkFarmMine_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateParkFarmMine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateParkFarmMine.Click
        Try
            If IsValid_ParkFarmMine() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("ParkSrNo")

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtPark.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtPark.Rows(SrNo)

            ''drRow.Item("assetparkfarmminestranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("parkfarmmines") = txtParkFarmsMines.Text.Trim
            'drRow.Item("sizeofarea") = txtSizeofAreaParkFarmMine.Text.Trim
            'drRow.Item("placeclad") = txtLocationParkFarmMine.Text.Trim
            'drRow.Item("value") = txtValueParkFarmMine.Text.Trim


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyPark.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyPark.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantParkFarmMine.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantParkFarmMine.Text.Trim
            'End If
            'If txtWifeHusbundParkFarmMine.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundParkFarmMine.Text.Trim
            'End If
            'If txtChildrenParkFarmMine.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenParkFarmMine.Text.Trim
            'End If

            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvParkFarm, drRow, CInt(drpCurrencyPark.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtPark.AcceptChanges()

            'gvParkFarm.DataSource = dtPark
            'gvParkFarm.DataBind()
            Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran

            objAssetParkFarm.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvParkFarm.DataKeys(SrNo).Item("assetparkfarmminestranunkid")), Nothing)
            objAssetParkFarm._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetParkFarm._Parkfarmmines = txtParkFarmsMines.Text.Trim
            objAssetParkFarm._Sizeofarea = txtSizeofAreaParkFarmMine.Text.Trim
            objAssetParkFarm._Placeclad = txtLocationParkFarmMine.Text.Trim
            objAssetParkFarm._Value = txtValueParkFarmMine.Text.Trim
            objAssetParkFarm._Countryunkid = CInt(drpCurrencyPark.SelectedItem.Value)
            If txtServantParkFarmMine.Text.Trim = "" Then
                objAssetParkFarm._Servant = 0
            Else
                objAssetParkFarm._Servant = txtServantParkFarmMine.Text.Trim
            End If
            If txtWifeHusbundParkFarmMine.Text.Trim = "" Then
                objAssetParkFarm._Wifehusband = 0
            Else
                objAssetParkFarm._Wifehusband = txtWifeHusbundParkFarmMine.Text.Trim
            End If
            If txtChildrenParkFarmMine.Text.Trim = "" Then
                objAssetParkFarm._Children = 0
            Else
                objAssetParkFarm._Children = txtChildrenParkFarmMine.Text.Trim
            End If

            objAssetParkFarm._Isfinalsaved = 0

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyPark.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetParkFarm._CurrencyUnkId = intPaidCurrencyunkid
            objAssetParkFarm._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetParkFarm._Baseexchangerate = decBaseExRate
            objAssetParkFarm._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetParkFarm._Baseamount = objAssetParkFarm._Value * decBaseExRate / decPaidExRate
            Else
                objAssetParkFarm._Baseamount = objAssetParkFarm._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetParkFarm._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetParkFarm._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetParkFarm._ClientIp = Session("IP_ADD").ToString()
            objAssetParkFarm._HostName = Session("HOST_NAME").ToString()
            objAssetParkFarm._FormName = mstrModuleName1
            objAssetParkFarm._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsParkFarm As DataSet = objAssetParkFarm.GetList("List", mintAssetDeclarationUnkid)
            Dim dtParkFarm As DataTable = dsParkFarm.Tables(0)
            If dtParkFarm IsNot Nothing Then
                Dim drRow() As DataRow = dtParkFarm.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Parkfarmtotal = objAssetParkFarm._Baseamount + Format(CDec(dtParkFarm.Compute("sum(baseamount)", " assetparkfarmminestranunkid <> " & objAssetParkFarm._Assetparkfarmminestranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Parkfarmtotal = Format(objAssetParkFarm._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetParkFarm.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetParkFarm._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetParkFarm._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetParkFarm._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillParmFarmMineGrid()

            'Hemant (24 Dec 2019) -- End

            Call Reset_ParkFarmMine()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateParkFarmMine_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetParkFarmMine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetParkFarmMine.Click
        Try
            Call Reset_ParkFarmMine()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'popupAddEdit.Show()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetParkFarmMine_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteParkFarm_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteParkFarm.buttonDelReasonYes_Click
        Dim objAssetParkFarm As New clsAsset_parkfarmmines_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("ParkSrNo"))
            objAssetParkFarm.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvParkFarm.DataKeys(SrNo).Item("assetparkfarmminestranunkid")), Nothing)
            objAssetParkFarm._Isvoid = True
            objAssetParkFarm._Voiduserunkid = CInt(Session("UserId"))
            objAssetParkFarm._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetParkFarm._Voidreason = popupDeleteParkFarm.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetParkFarm._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetParkFarm._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetParkFarm._ClientIp = Session("IP_ADD").ToString()
            objAssetParkFarm._HostName = Session("HOST_NAME").ToString()
            objAssetParkFarm._FormName = mstrModuleName1
            objAssetParkFarm._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1
            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsParkFarm As DataSet = objAssetParkFarm.GetList("List", mintAssetDeclarationUnkid)
            Dim dtParkFarm As DataTable = dsParkFarm.Tables(0)
            If dtParkFarm IsNot Nothing Then
                Dim drRow() As DataRow = dtParkFarm.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Parkfarmtotal = Format(CDec(dtParkFarm.Compute("sum(baseamount)", " assetparkfarmminestranunkid <> " & objAssetParkFarm._Assetparkfarmminestranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Parkfarmtotal = Format(0, Session("fmtCurrency"))
                End If
            End If

            If objAssetParkFarm.Void(CInt(gvParkFarm.DataKeys(SrNo).Item("assetparkfarmminestranunkid")), CInt(Session("UserId")), objAssetParkFarm._Voiddatetime, objAssetParkFarm._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillParmFarmMineGrid()
            ElseIf objAssetParkFarm._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetParkFarm._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteParkFarm_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvParkFarm_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvParkFarm.PageIndexChanging
        Try
            gvParkFarm.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvParkFarm.DataSource = dtPark
            'gvParkFarm.DataBind()
            Call FillParmFarmMineGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvParkFarm_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvParkFarm_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvParkFarm.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorParkFarmMine.Text = ""
                lblErrorCParkFarmMine.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ParkSrNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtParkFarmsMines.Text = dtPark.Rows(SrNo).Item("parkfarmmines").ToString
                'txtSizeofAreaParkFarmMine.Text = dtPark.Rows(SrNo).Item("sizeofarea").ToString
                'txtLocationParkFarmMine.Text = dtPark.Rows(SrNo).Item("placeclad").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtValueParkFarmMine.Text = Format(dtPark.Rows(SrNo).Item("value"), GUI.fmtCurrency)
                'txtValueParkFarmMine.Text = Format(dtPark.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End


                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                ''drpCurrencyPark.SelectedValue = dtPark.Rows(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyPark.SelectedValue = dtPark.Rows(SrNo).Item("countryunkid").ToString
                ''Pinkal (30-Jan-2013) -- End


                'txtServantParkFarmMine.Text = dtPark.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbundParkFarmMine.Text = dtPark.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenParkFarmMine.Text = dtPark.Rows(SrNo).Item("children").ToString
                txtParkFarmsMines.Text = gvParkFarm.Rows(SrNo).Cells(2).Text
                txtSizeofAreaParkFarmMine.Text = gvParkFarm.Rows(SrNo).Cells(3).Text
                txtLocationParkFarmMine.Text = gvParkFarm.Rows(SrNo).Cells(4).Text
                txtValueParkFarmMine.Text = Format(CDec(gvParkFarm.Rows(SrNo).Cells(5).Text), Session("fmtCurrency"))
                txtServantParkFarmMine.Text = gvParkFarm.Rows(SrNo).Cells(7).Text
                txtWifeHusbundParkFarmMine.Text = gvParkFarm.Rows(SrNo).Cells(8).Text
                txtChildrenParkFarmMine.Text = gvParkFarm.Rows(SrNo).Cells(9).Text
                drpCurrencyPark.SelectedValue = CInt(gvParkFarm.DataKeys(SrNo).Item("countryunkid").ToString)
                'Hemant (24 Dec 2019) -- End
                btnAddParkFarmMine.Enabled = False
                btnUpdateParkFarmMine.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtPark.Rows(CInt(e.CommandArgument)).Delete()

                'If dtPark.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtPark.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(4) = ""
                '    dtPark.Rows.Add(r)
                'End If
                'dtPark.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvParkFarm.DataSource = dtPark
                'gvParkFarm.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ParkSrNo") = SrNo
                popupDeleteParkFarm.Show()
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvParkFarm_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvParkFarm_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvParkFarm.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(4).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                '    'e.Row.Cells(3).Text = Format(CDec(e.Row.Cells(3).Text), GUI.fmtCurrency) 'Sohail (06 Apr 2012)
                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), GUI.fmtCurrency)
                '    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes

                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(6).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(6).Text = objExRate._Currency_Sign

                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(6).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(6).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(6).Text = ""
                '    End If


                '    'Pinkal (30-Jan-2013) -- End

                '    'Sohail (06 Apr 2012) -- End
                'End If
                If IsDBNull(gvParkFarm.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Sohail (28 Jan 2020) -- Start
                    'TRA Issue # 0004456 : Performance and asset declaration issues. (gvParkFarm_RowDataBound :- Conversion from string "14***33" to type Decimal is not valid)
                    'e.Row.Cells(3).Text = Format(CDec(e.Row.Cells(3).Text), Session("fmtCurrency"))
                    'Sohail (28 Jan 2020) -- End
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                    e.Row.Cells(9).Text = Format(CDec(e.Row.Cells(9).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvParkFarm_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Vehicles "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboVehicles()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrVehicles As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrVehicles = objExchange.getComboList("Currency", True)
            With drpCurrencyVehicle

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"
                'Pinkal (30-Jan-2013) -- End


                .DataTextField = "currency_sign"
                .DataSource = dsCurrVehicles.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboVehicles :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillVehiclesGrid()
        Dim objAssetVehicles As New clsAsset_vehicles_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtVehicles As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetVehicles._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetVehicles._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtVehicles = objAssetVehicles._Datasource

            'If dtVehicles.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtVehicles.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(3) = -1
            '    dtVehicles.Rows.Add(r)
            'End If
            Dim dsVehicles As DataSet = objAssetVehicles.GetList("List", mintAssetDeclarationUnkid)
            dtVehicles = dsVehicles.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtVehicles.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtVehicles.NewRow
                dtVehicles.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvVehicles.DataSource = dtVehicles
            gvVehicles.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvVehicles.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillVehiclesGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_Vehicles() As Boolean
        Try
            lblErrorVehicle.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCVehicle.Text = ""
            If CInt(drpCurrencyVehicle.SelectedItem.Value) <= 0 Then
                lblErrorCVehicle.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantVehicle.Text, decServant)
            Decimal.TryParse(txtWifeHusbundVehicle.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenVehicle.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorVehicle.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes

            If dtpAcquistionDate.IsNull Then
                lblErrorVehicle.Text = "Acquisition date is mandatory information.Please Select Acquisition date."
                Return False
            End If

            If dtpAcquistionDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                lblErrorVehicle.Text = "Sorry! Acquisition date should not be greater than current date."
                Return False
            End If

            'Pinkal (30-Jan-2013) -- End


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_Vehicles :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_Vehicles()
        Try
            txtVehicleType.Text = ""
            txtValueVehicle.Text = ""
            txtServantVehicle.Text = ""
            txtWifeHusbundVehicle.Text = ""
            txtChildrenVehicle.Text = ""
            lblErrorVehicle.Text = ""
            lblErrorCVehicle.Text = "" 'Sohail (06 Apr 2012)

            btnAddVehicle.Enabled = True
            btnUpdateVehicle.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyVehicle.SelectedIndex = 0
            txtModel.Text = ""
            txtColour.Text = ""
            txtRegNo.Text = ""
            txtVehicleUse.Text = ""
            dtpAcquistionDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_Vehicles :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddVehicle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddVehicle.Click
        Try
            If IsValid_Vehicles() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.           
            'Dim drRow As DataRow = dtVehicles.NewRow

            'drRow.Item("assetvehiclestranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("vehicletype") = txtVehicleType.Text.Trim
            'drRow.Item("value") = txtValueVehicle.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyVehicle.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyVehicle.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantVehicle.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantVehicle.Text.Trim
            'End If
            'If txtWifeHusbundVehicle.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundVehicle.Text.Trim
            'End If
            'If txtChildrenVehicle.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenVehicle.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes

            'drRow.Item("model") = txtModel.Text.Trim
            'drRow.Item("colour") = txtColour.Text.Trim
            'drRow.Item("regno") = txtRegNo.Text.Trim
            'drRow.Item("vehicleuse") = txtVehicleUse.Text.Trim
            'drRow.Item("acquisitiondate") = dtpAcquistionDate.GetDate

            'Update_DataGridview_DataTable_Extra_Columns(gvVehicles, drRow, CInt(drpCurrencyVehicle.SelectedValue))

            ''Pinkal (30-Jan-2013) -- End


            'dtVehicles.Rows.Add(drRow)

            'gvVehicles.DataSource = dtVehicles
            'gvVehicles.DataBind()
            Dim objAssetVehicles As New clsAsset_vehicles_tran

            objAssetVehicles._Assetvehiclestranunkid = -1
            objAssetVehicles._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetVehicles._Vehicletype = txtVehicleType.Text.Trim
            objAssetVehicles._Value = txtValueVehicle.Text.Trim
            objAssetVehicles._Countryunkid = CInt(drpCurrencyVehicle.SelectedItem.Value)
            If txtServantVehicle.Text.Trim = "" Then
                objAssetVehicles._Servant = 0
            Else
                objAssetVehicles._Servant = txtServantVehicle.Text.Trim
            End If
            If txtWifeHusbundVehicle.Text.Trim = "" Then
                objAssetVehicles._Wifehusband = 0
            Else
                objAssetVehicles._Wifehusband = txtWifeHusbundVehicle.Text.Trim
            End If
            If txtChildrenVehicle.Text.Trim = "" Then
                objAssetVehicles._Children = 0
            Else
                objAssetVehicles._Children = txtChildrenVehicle.Text.Trim
            End If
            objAssetVehicles._Isfinalsaved = 0
            objAssetVehicles._Model = txtModel.Text.Trim
            objAssetVehicles._Colour = txtColour.Text.Trim
            objAssetVehicles._Regno = txtRegNo.Text.Trim
            objAssetVehicles._Vehicleuse = txtVehicleUse.Text.Trim
            objAssetVehicles._Acquisitiondate = dtpAcquistionDate.GetDate
            objAssetVehicles._Userunkid = CInt(Session("UserId"))
            If objAssetVehicles._Transactiondate = Nothing Then
                objAssetVehicles._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyVehicle.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetVehicles._CurrencyUnkId = intPaidCurrencyunkid
            objAssetVehicles._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetVehicles._Baseexchangerate = decBaseExRate
            objAssetVehicles._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetVehicles._Baseamount = objAssetVehicles._Value * decBaseExRate / decPaidExRate
            Else
                objAssetVehicles._Baseamount = objAssetVehicles._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetVehicles._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetVehicles._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetVehicles._ClientIp = Session("IP_ADD").ToString()
            objAssetVehicles._HostName = Session("HOST_NAME").ToString()
            objAssetVehicles._FormName = mstrModuleName1
            objAssetVehicles._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsVehicles As DataSet = objAssetVehicles.GetList("List", mintAssetDeclarationUnkid)
            Dim dtVehicles As DataTable = dsVehicles.Tables(0)
            If dtVehicles IsNot Nothing Then
                Dim drRow() As DataRow = dtVehicles.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Vehicletotal = objAssetVehicles._Baseamount + Format(CDec(dtVehicles.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Vehicletotal = Format(objAssetVehicles._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetVehicles.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetVehicles._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetVehicles._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetVehicles._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillVehiclesGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_Vehicles()

        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddVehicle_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateVehicle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateVehicle.Click
        Try
            If IsValid_Vehicles() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("VehiclesSrNo")
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtVehicles.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtVehicles.Rows(SrNo)

            ''drRow.Item("assetvehiclestranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("vehicletype") = txtVehicleType.Text.Trim
            'drRow.Item("value") = txtValueVehicle.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyVehicle.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyVehicle.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantVehicle.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantVehicle.Text.Trim
            'End If
            'If txtWifeHusbundVehicle.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundVehicle.Text.Trim
            'End If
            'If txtChildrenVehicle.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenVehicle.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("acquisitiondate") = dtpAcquistionDate.GetDate
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvVehicles, drRow, CInt(drpCurrencyVehicle.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtVehicles.AcceptChanges()

            'gvVehicles.DataSource = dtVehicles
            'gvVehicles.DataBind()
            Dim objAssetVehicles As New clsAsset_vehicles_tran

            objAssetVehicles.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvVehicles.DataKeys(SrNo).Item("assetvehiclestranunkid")), Nothing)
            objAssetVehicles._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetVehicles._Vehicletype = txtVehicleType.Text.Trim
            objAssetVehicles._Value = txtValueVehicle.Text.Trim
            objAssetVehicles._Countryunkid = CInt(drpCurrencyVehicle.SelectedItem.Value)
            If txtServantVehicle.Text.Trim = "" Then
                objAssetVehicles._Servant = 0
            Else
                objAssetVehicles._Servant = txtServantVehicle.Text.Trim
            End If
            If txtWifeHusbundVehicle.Text.Trim = "" Then
                objAssetVehicles._Wifehusband = 0
            Else
                objAssetVehicles._Wifehusband = txtWifeHusbundVehicle.Text.Trim
            End If
            If txtChildrenVehicle.Text.Trim = "" Then
                objAssetVehicles._Children = 0
            Else
                objAssetVehicles._Children = txtChildrenVehicle.Text.Trim
            End If
            objAssetVehicles._Isfinalsaved = 0
            objAssetVehicles._Acquisitiondate = dtpAcquistionDate.GetDate

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyVehicle.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetVehicles._CurrencyUnkId = intPaidCurrencyunkid
            objAssetVehicles._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetVehicles._Baseexchangerate = decBaseExRate
            objAssetVehicles._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetVehicles._Baseamount = objAssetVehicles._Value * decBaseExRate / decPaidExRate
            Else
                objAssetVehicles._Baseamount = objAssetVehicles._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetVehicles._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetVehicles._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetVehicles._ClientIp = Session("IP_ADD").ToString()
            objAssetVehicles._HostName = Session("HOST_NAME").ToString()
            objAssetVehicles._FormName = mstrModuleName1
            objAssetVehicles._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsVehicles As DataSet = objAssetVehicles.GetList("List", mintAssetDeclarationUnkid)
            Dim dtVehicles As DataTable = dsVehicles.Tables(0)
            If dtVehicles IsNot Nothing Then
                Dim drRow() As DataRow = dtVehicles.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Vehicletotal = objAssetVehicles._Baseamount + Format(CDec(dtVehicles.Compute("sum(baseamount)", " assetvehiclestranunkid <> " & objAssetVehicles._Assetvehiclestranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Vehicletotal = Format(objAssetVehicles._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetVehicles.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetVehicles._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetVehicles._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetVehicles._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillVehiclesGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_Vehicles()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateVehicle_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetVehicle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetVehicle.Click
        Try
            Call Reset_Vehicles()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetVehicle_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteVehicles_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteVehicles.buttonDelReasonYes_Click
        Dim objAssetVehicles As New clsAsset_vehicles_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("VehiclesSrNo"))
            objAssetVehicles.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvVehicles.DataKeys(SrNo).Item("assetvehiclestranunkid")), Nothing)
            objAssetVehicles._Isvoid = True
            objAssetVehicles._Voiduserunkid = CInt(Session("UserId"))
            objAssetVehicles._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetVehicles._Voidreason = popupDeleteVehicles.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetVehicles._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetVehicles._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetVehicles._ClientIp = Session("IP_ADD").ToString()
            objAssetVehicles._HostName = Session("HOST_NAME").ToString()
            objAssetVehicles._FormName = mstrModuleName1
            objAssetVehicles._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1
            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsVehicles As DataSet = objAssetVehicles.GetList("List", mintAssetDeclarationUnkid)
            Dim dtVehicles As DataTable = dsVehicles.Tables(0)
            If dtVehicles IsNot Nothing Then
                Dim drRow() As DataRow = dtVehicles.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Vehicletotal = Format(CDec(dtVehicles.Compute("sum(baseamount)", " assetvehiclestranunkid <> " & objAssetVehicles._Assetvehiclestranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Vehicletotal = Format(0, Session("fmtCurrency"))
                End If
            End If

            If objAssetVehicles.Void(CInt(gvVehicles.DataKeys(SrNo).Item("assetvehiclestranunkid")), CInt(Session("UserId")), objAssetVehicles._Voiddatetime, objAssetVehicles._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillVehiclesGrid()
            ElseIf objAssetVehicles._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetVehicles._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteVehicles_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvVehicles_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvVehicles.PageIndexChanging
        Try
            gvVehicles.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvVehicles.DataSource = dtVehicles
            'gvVehicles.DataBind()
            Call FillVehiclesGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvVehicles_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvVehicles_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvVehicles.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorVehicle.Text = ""
                lblErrorCVehicle.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("VehiclesSrNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtVehicleType.Text = dtVehicles.Rows(SrNo).Item("vehicletype").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtValueVehicle.Text = Format(dtVehicles.Rows(SrNo).Item("value"), GUI.fmtCurrency)
                'txtValueVehicle.Text = Format(dtVehicles.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End


                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                '' drpCurrencyVehicle.SelectedValue = dtVehicles.Rows(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyVehicle.SelectedValue = dtVehicles.Rows(SrNo).Item("countryunkid").ToString
                'txtModel.Text = dtVehicles.Rows(SrNo).Item("model").ToString
                'txtColour.Text = dtVehicles.Rows(SrNo).Item("colour").ToString
                'txtRegNo.Text = dtVehicles.Rows(SrNo).Item("regno").ToString
                'txtVehicleUse.Text = dtVehicles.Rows(SrNo).Item("vehicleuse").ToString
                ''Pinkal (30-Jan-2013) -- End


                'txtServantVehicle.Text = dtVehicles.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbundVehicle.Text = dtVehicles.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenVehicle.Text = dtVehicles.Rows(SrNo).Item("children").ToString
                txtVehicleType.Text = gvVehicles.Rows(SrNo).Cells(2).Text
                txtValueVehicle.Text = Format(CDec(gvVehicles.Rows(SrNo).Cells(7).Text), Session("fmtCurrency"))
                drpCurrencyVehicle.SelectedValue = CInt(gvVehicles.DataKeys(SrNo).Item("countryunkid").ToString)
                txtModel.Text = gvVehicles.Rows(SrNo).Cells(3).Text
                txtColour.Text = gvVehicles.Rows(SrNo).Cells(4).Text
                txtRegNo.Text = gvVehicles.Rows(SrNo).Cells(5).Text
                txtVehicleUse.Text = gvVehicles.Rows(SrNo).Cells(6).Text
                txtServantVehicle.Text = gvVehicles.Rows(SrNo).Cells(9).Text
                txtWifeHusbundVehicle.Text = gvVehicles.Rows(SrNo).Cells(10).Text
                txtChildrenVehicle.Text = gvVehicles.Rows(SrNo).Cells(11).Text
                'Hemant (24 Dec 2019) -- End

                btnAddVehicle.Enabled = False
                btnUpdateVehicle.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtVehicles.Rows(CInt(e.CommandArgument)).Delete()

                'If dtVehicles.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtVehicles.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = -1
                '    dtVehicles.Rows.Add(r)
                'End If
                'dtVehicles.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvVehicles.DataSource = dtVehicles
                'gvVehicles.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("VehiclesSrNo") = SrNo
                popupDeleteVehicles.Show()
                'Hemant (24 Dec 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvVehicles_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvVehicles_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvVehicles.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes

                ''If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "-1" Then

                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(7).Text = "-1" Then
                '    e.Row.Visible = False

                '    'Pinkal (30-Jan-2013) -- End

                'Else
                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(3).Text = Format(CDec(e.Row.Cells(3).Text), GUI.fmtCurrency)

                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes
                '    'e.Row.Cells(3).Text = Format(CDec(e.Row.Cells(3).Text), Session("fmtCurrency"))
                '    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                '    'Pinkal (30-Jan-2013) -- End


                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes

                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(4).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(4).Text = objExRate._Currency_Sign

                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(8).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(8).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(8).Text = ""
                '    End If

                '    'Pinkal (30-Jan-2013) -- End

                '    'Sohail (06 Apr 2012) -- End
                'End If
                If IsDBNull(gvVehicles.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(9).Text = Format(CDec(e.Row.Cells(9).Text), Session("fmtCurrency"))
                    e.Row.Cells(10).Text = Format(CDec(e.Row.Cells(10).Text), Session("fmtCurrency"))
                    e.Row.Cells(11).Text = Format(CDec(e.Row.Cells(11).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvVehicles_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Machinery "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboMachine()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrMachinery As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrMachinery = objExchange.getComboList("Currency", True)
            With drpCurrencyMachine

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"
                'Pinkal (30-Jan-2013) -- End

                .DataTextField = "currency_sign"
                .DataSource = dsCurrMachinery.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboMachine :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillMachineryGrid()
        Dim objAssetMachinery As New clsAsset_machinery_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtMachinery As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetMachinery._Assetdeclarationunkid = mintAssetDeclarationUnkid

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetMachinery._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtMachinery = objAssetMachinery._Datasource

            'If dtMachinery.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtMachinery.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(3) = ""
            '    dtMachinery.Rows.Add(r)
            'End If
            Dim dsMachinery As DataSet = objAssetMachinery.GetList("List", mintAssetDeclarationUnkid)
            dtMachinery = dsMachinery.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtMachinery.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtMachinery.NewRow
                dtMachinery.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvMachinery.DataSource = dtMachinery
            gvMachinery.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvMachinery.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("FillMachineryGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_Machinery() As Boolean
        Try
            lblErrorMachinery.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCMachinery.Text = ""
            If CInt(drpCurrencyMachine.SelectedItem.Value) <= 0 Then
                lblErrorCMachinery.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantMachinery.Text, decServant)
            Decimal.TryParse(txtWifeHusbundMachinery.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenMachinery.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorMachinery.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_Machinery :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_Machinery()
        Try
            txtMachines.Text = ""
            txtLocationMachinery.Text = ""
            txtValueMachinery.Text = ""
            txtServantMachinery.Text = ""
            txtWifeHusbundMachinery.Text = ""
            txtChildrenMachinery.Text = ""
            lblErrorMachinery.Text = ""
            lblErrorCMachinery.Text = "" 'Sohail (06 Apr 2012)

            btnAddMachinery.Enabled = True
            btnUpdateMachinery.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyMachine.SelectedIndex = 0
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_Machinery :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddMachinery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMachinery.Click
        Try
            If IsValid_Machinery() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtMachinery.NewRow

            'drRow.Item("assetmachinerytranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("machines") = txtMachines.Text.Trim
            'drRow.Item("location") = txtLocationMachinery.Text.Trim
            'drRow.Item("value") = txtValueMachinery.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyMachine.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyMachine.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantMachinery.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantMachinery.Text.Trim
            'End If
            'If txtWifeHusbundMachinery.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundMachinery.Text.Trim
            'End If
            'If txtChildrenMachinery.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenMachinery.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'Update_DataGridview_DataTable_Extra_Columns(gvMachinery, drRow, CInt(drpCurrencyMachine.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtMachinery.Rows.Add(drRow)

            'gvMachinery.DataSource = dtMachinery
            'gvMachinery.DataBind()
            Dim objAssetMachinery As New clsAsset_machinery_tran

            objAssetMachinery._Assetmachinerytranunkid = -1
            objAssetMachinery._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetMachinery._Machines = txtMachines.Text.Trim
            objAssetMachinery._Location = txtLocationMachinery.Text.Trim
            objAssetMachinery._Value = txtValueMachinery.Text.Trim
            objAssetMachinery._Countryunkid = CInt(drpCurrencyMachine.SelectedItem.Value)
            If txtServantMachinery.Text.Trim = "" Then
                objAssetMachinery._Servant = 0
            Else
                objAssetMachinery._Servant = txtServantMachinery.Text.Trim
            End If
            If txtWifeHusbundMachinery.Text.Trim = "" Then
                objAssetMachinery._Wifehusband = 0
            Else
                objAssetMachinery._Wifehusband = txtWifeHusbundMachinery.Text.Trim
            End If
            If txtChildrenMachinery.Text.Trim = "" Then
                objAssetMachinery._Children = 0
            Else
                objAssetMachinery._Children = txtChildrenMachinery.Text.Trim
            End If
            objAssetMachinery._Isfinalsaved = 0

            objAssetMachinery._Userunkid = CInt(Session("UserId"))
            If objAssetMachinery._Transactiondate = Nothing Then
                objAssetMachinery._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyMachine.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetMachinery._CurrencyUnkId = intPaidCurrencyunkid
            objAssetMachinery._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetMachinery._Baseexchangerate = decBaseExRate
            objAssetMachinery._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetMachinery._Baseamount = objAssetMachinery._Value * decBaseExRate / decPaidExRate
            Else
                objAssetMachinery._Baseamount = objAssetMachinery._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetMachinery._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetMachinery._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetMachinery._ClientIp = Session("IP_ADD").ToString()
            objAssetMachinery._HostName = Session("HOST_NAME").ToString()
            objAssetMachinery._FormName = mstrModuleName1
            objAssetMachinery._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsMachinery As DataSet = objAssetMachinery.GetList("List", mintAssetDeclarationUnkid)
            Dim dtMachinery As DataTable = dsMachinery.Tables(0)
            If dtMachinery IsNot Nothing Then
                Dim drRow() As DataRow = dtMachinery.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Machinerytotal = objAssetMachinery._Baseamount + Format(CDec(dtMachinery.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Machinerytotal = Format(objAssetMachinery._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetMachinery.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetMachinery._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetMachinery._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetMachinery._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillMachineryGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_Machinery()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddMachinery_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateMachinery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateMachinery.Click
        Try
            If IsValid_Machinery() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("MachinerySrNo")

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtMachinery.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtMachinery.Rows(SrNo)

            ''drRow.Item("assetmachinerytranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("machines") = txtMachines.Text.Trim
            'drRow.Item("location") = txtLocationMachinery.Text.Trim
            'drRow.Item("value") = txtValueMachinery.Text.Trim


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes

            ''drRow.Item("currencyunkid") = CInt(drpCurrencyMachine.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyMachine.SelectedItem.Value)

            ''Pinkal (30-Jan-2013) -- End


            'If txtServantMachinery.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantMachinery.Text.Trim
            'End If
            'If txtWifeHusbundMachinery.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundMachinery.Text.Trim
            'End If
            'If txtChildrenMachinery.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenMachinery.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvMachinery, drRow, CInt(drpCurrencyMachine.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End

            'dtMachinery.AcceptChanges()

            'gvMachinery.DataSource = dtMachinery
            'gvMachinery.DataBind()
            Dim objAssetMachinery As New clsAsset_machinery_tran

            objAssetMachinery.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvMachinery.DataKeys(SrNo).Item("assetmachinerytranunkid")), Nothing)
            objAssetMachinery._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetMachinery._Machines = txtMachines.Text.Trim
            objAssetMachinery._Location = txtLocationMachinery.Text.Trim
            objAssetMachinery._Value = txtValueMachinery.Text.Trim
            objAssetMachinery._Countryunkid = CInt(drpCurrencyMachine.SelectedItem.Value)
            If txtServantMachinery.Text.Trim = "" Then
                objAssetMachinery._Servant = 0
            Else
                objAssetMachinery._Servant = txtServantMachinery.Text.Trim
            End If
            If txtWifeHusbundMachinery.Text.Trim = "" Then
                objAssetMachinery._Wifehusband = 0
            Else
                objAssetMachinery._Wifehusband = txtWifeHusbundMachinery.Text.Trim
            End If
            If txtChildrenMachinery.Text.Trim = "" Then
                objAssetMachinery._Children = 0
            Else
                objAssetMachinery._Children = txtChildrenMachinery.Text.Trim
            End If
            objAssetMachinery._Isfinalsaved = 0

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyMachine.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetMachinery._CurrencyUnkId = intPaidCurrencyunkid
            objAssetMachinery._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetMachinery._Baseexchangerate = decBaseExRate
            objAssetMachinery._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetMachinery._Baseamount = objAssetMachinery._Value * decBaseExRate / decPaidExRate
            Else
                objAssetMachinery._Baseamount = objAssetMachinery._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetMachinery._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetMachinery._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetMachinery._ClientIp = Session("IP_ADD").ToString()
            objAssetMachinery._HostName = Session("HOST_NAME").ToString()
            objAssetMachinery._FormName = mstrModuleName1
            objAssetMachinery._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsMachinery As DataSet = objAssetMachinery.GetList("List", mintAssetDeclarationUnkid)
            Dim dtMachinery As DataTable = dsMachinery.Tables(0)
            If dtMachinery IsNot Nothing Then
                Dim drRow() As DataRow = dtMachinery.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Machinerytotal = objAssetMachinery._Baseamount + Format(CDec(dtMachinery.Compute("sum(baseamount)", " assetmachinerytranunkid <> " & objAssetMachinery._Assetmachinerytranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Machinerytotal = Format(objAssetMachinery._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetMachinery.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetMachinery._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetMachinery._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetMachinery._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillMachineryGrid()
            'Hemant (24 Dec 2019) -- End
            Call Reset_Machinery()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateMachinery_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetMachinery_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetMachinery.Click
        Try
            Call Reset_Machinery()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetMachinery_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteMachinery_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteMachinery.buttonDelReasonYes_Click
        Dim objAssetMachinery As New clsAsset_machinery_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("MachinerySrNo"))
            objAssetMachinery.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvMachinery.DataKeys(SrNo).Item("assetmachinerytranunkid")), Nothing)
            objAssetMachinery._Isvoid = True
            objAssetMachinery._Voiduserunkid = CInt(Session("UserId"))
            objAssetMachinery._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetMachinery._Voidreason = popupDeleteMachinery.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetMachinery._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetMachinery._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetMachinery._ClientIp = Session("IP_ADD").ToString()
            objAssetMachinery._HostName = Session("HOST_NAME").ToString()
            objAssetMachinery._FormName = mstrModuleName1
            objAssetMachinery._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsMachinery As DataSet = objAssetMachinery.GetList("List", mintAssetDeclarationUnkid)
            Dim dtMachinery As DataTable = dsMachinery.Tables(0)
            If dtMachinery IsNot Nothing Then
                Dim drRow() As DataRow = dtMachinery.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Machinerytotal = Format(CDec(dtMachinery.Compute("sum(baseamount)", " assetmachinerytranunkid <> " & objAssetMachinery._Assetmachinerytranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Machinerytotal = Format(0, Session("fmtCurrency"))
                End If
            End If

            If objAssetMachinery.Void(CInt(gvMachinery.DataKeys(SrNo).Item("assetmachinerytranunkid")), CInt(Session("UserId")), objAssetMachinery._Voiddatetime, objAssetMachinery._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillMachineryGrid()
            ElseIf objAssetMachinery._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetMachinery._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteMachinery_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvMachinery_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMachinery.PageIndexChanging
        Try
            gvMachinery.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvMachinery.DataSource = dtMachinery
            'gvMachinery.DataBind()
            Call FillMachineryGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvMachinery_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvMachinery_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvMachinery.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorMachinery.Text = ""
                lblErrorCMachinery.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("MachinerySrNo") = SrNo


                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtMachines.Text = dtMachinery.Rows(SrNo).Item("machines").ToString
                'txtLocationMachinery.Text = dtMachinery.Rows(SrNo).Item("location").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtValueMachinery.Text = Format(dtMachinery.Rows(SrNo).Item("value"), GUI.fmtCurrency)
                'txtValueMachinery.Text = Format(dtMachinery.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End


                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                ''drpCurrencyMachine.SelectedValue = dtMachinery.Rows(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyMachine.SelectedValue = dtMachinery.Rows(SrNo).Item("countryunkid").ToString
                ''Pinkal (30-Jan-2013) -- End


                'txtServantMachinery.Text = dtMachinery.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbundMachinery.Text = dtMachinery.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenMachinery.Text = dtMachinery.Rows(SrNo).Item("children").ToString
                txtMachines.Text = gvMachinery.Rows(SrNo).Cells(2).Text
                txtLocationMachinery.Text = gvMachinery.Rows(SrNo).Cells(3).Text
                txtValueMachinery.Text = Format(CDec(gvMachinery.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpCurrencyMachine.SelectedValue = CInt(gvMachinery.DataKeys(SrNo).Item("countryunkid").ToString)
                txtServantMachinery.Text = gvMachinery.Rows(SrNo).Cells(6).Text
                txtWifeHusbundMachinery.Text = gvMachinery.Rows(SrNo).Cells(7).Text
                txtChildrenMachinery.Text = gvMachinery.Rows(SrNo).Cells(8).Text
                'Hemant (24 Dec 2019) -- End
                btnAddMachinery.Enabled = False
                btnUpdateMachinery.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtMachinery.Rows(CInt(e.CommandArgument)).Delete()

                'If dtMachinery.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtMachinery.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = ""
                '    dtMachinery.Rows.Add(r)
                'End If
                'dtMachinery.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvMachinery.DataSource = dtMachinery
                'gvMachinery.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("MachinerySrNo") = SrNo
                popupDeleteMachinery.Show()
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvMachinery_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvMachinery_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMachinery.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), GUI.fmtCurrency)
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes


                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(5).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(5).Text = objExRate._Currency_Sign


                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(5).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(5).Text = objExRate._Currency_Sign

                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If

                '    'Pinkal (30-Jan-2013) -- End

                '    'Sohail (06 Apr 2012) -- End
                'End If
                If IsDBNull(gvMachinery.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvMachinery_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Other Business "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboBusiness()
        Dim objExchange As New clsExchangeRate
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dsCurrOtherBusiness As DataSet
        'Hemant (24 Dec 2019) -- End
        Try
            dsCurrOtherBusiness = objExchange.getComboList("Currency", True)
            With drpCurrencyBusiness

                'Pinkal (30-Jan-2013) -- Start
                'Enhancement : TRA Changes
                '.DataValueField = "exchangerateunkid"
                .DataValueField = "countryunkid"
                'Pinkal (30-Jan-2013) -- End

                .DataTextField = "currency_sign"
                .DataSource = dsCurrOtherBusiness.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboBusiness :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillOtherBusinessGrid()
        Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtOtherBusiness As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetOtherBusiness._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetOtherBusiness._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtOtherBusiness = objAssetOtherBusiness._Datasource

            'If dtOtherBusiness.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtOtherBusiness.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(3) = ""
            '    dtOtherBusiness.Rows.Add(r)
            'End If
            Dim dsOtherBusiness As DataSet = objAssetOtherBusiness.GetList("List", mintAssetDeclarationUnkid)
            dtOtherBusiness = dsOtherBusiness.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtOtherBusiness.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtOtherBusiness.NewRow
                dtOtherBusiness.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvOtherBusiness.DataSource = dtOtherBusiness
            gvOtherBusiness.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvOtherBusiness.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillOtherBusinessGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_OtherBusiness() As Boolean
        Try
            lblErrorOtherBusiness.Text = ""
            'Sohail (06 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            lblErrorCOtherBusiness.Text = ""
            If CInt(drpCurrencyBusiness.SelectedItem.Value) <= 0 Then
                lblErrorCOtherBusiness.Text = "Please select Currency."
                Return False
            End If
            'Sohail (06 Apr 2012) -- End
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtServantOtherBusiness.Text, decServant)
            Decimal.TryParse(txtWifeHusbundOtherBusiness.Text, decWifeHusband)
            Decimal.TryParse(txtChildrenOtherBusiness.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                lblErrorOtherBusiness.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_OtherBusiness :- " & ex.Message, Me)
        End Try
    End Function

    Private Sub Reset_OtherBusiness()
        Try
            txtBusinessType.Text = ""
            txtLocationOtherBusiness.Text = ""
            txtValueOtherBusiness.Text = ""
            txtServantOtherBusiness.Text = ""
            txtWifeHusbundOtherBusiness.Text = ""
            txtChildrenOtherBusiness.Text = ""
            lblErrorOtherBusiness.Text = ""
            lblErrorCOtherBusiness.Text = "" 'Sohail (06 Apr 2012)

            btnAddOtherBusiness.Enabled = True
            btnUpdateOtherBusiness.Enabled = False


            'Pinkal (30-Jan-2013) -- Start
            'Enhancement : TRA Changes
            drpCurrencyBusiness.SelectedIndex = 0
            'Pinkal (30-Jan-2013) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_OtherBusiness :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddOtherBusiness_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOtherBusiness.Click
        Try
            If IsValid_OtherBusiness() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtOtherBusiness.NewRow

            'drRow.Item("assetotherbusinesstranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("businesstype") = txtBusinessType.Text.Trim
            'drRow.Item("placeclad") = txtLocationOtherBusiness.Text.Trim
            'drRow.Item("value") = txtValueOtherBusiness.Text.Trim

            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyBusiness.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyBusiness.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantOtherBusiness.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantOtherBusiness.Text.Trim
            'End If
            'If txtWifeHusbundOtherBusiness.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundOtherBusiness.Text.Trim
            'End If
            'If txtChildrenOtherBusiness.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenOtherBusiness.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'Update_DataGridview_DataTable_Extra_Columns(gvOtherBusiness, drRow, CInt(drpCurrencyBusiness.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtOtherBusiness.Rows.Add(drRow)

            'gvOtherBusiness.DataSource = dtOtherBusiness
            'gvOtherBusiness.DataBind()
            Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran

            objAssetOtherBusiness._Assetotherbusinesstranunkid = -1
            objAssetOtherBusiness._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetOtherBusiness._Businesstype = txtBusinessType.Text.Trim
            objAssetOtherBusiness._Placeclad = txtLocationOtherBusiness.Text.Trim
            objAssetOtherBusiness._Value = txtValueOtherBusiness.Text.Trim
            objAssetOtherBusiness._Countryunkid = CInt(drpCurrencyBusiness.SelectedItem.Value)
            If txtServantOtherBusiness.Text.Trim = "" Then
                objAssetOtherBusiness._Servant = 0
            Else
                objAssetOtherBusiness._Servant = txtServantOtherBusiness.Text.Trim
            End If
            If txtWifeHusbundOtherBusiness.Text.Trim = "" Then
                objAssetOtherBusiness._Wifehusband = 0
            Else
                objAssetOtherBusiness._Wifehusband = txtWifeHusbundOtherBusiness.Text.Trim
            End If
            If txtChildrenOtherBusiness.Text.Trim = "" Then
                objAssetOtherBusiness._Children = 0
            Else
                objAssetOtherBusiness._Children = txtChildrenOtherBusiness.Text.Trim
            End If
            objAssetOtherBusiness._Isfinalsaved = 0

            objAssetOtherBusiness._Userunkid = CInt(Session("UserId"))
            If objAssetOtherBusiness._Transactiondate = Nothing Then
                objAssetOtherBusiness._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyBusiness.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetOtherBusiness._CurrencyUnkId = intPaidCurrencyunkid
            objAssetOtherBusiness._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetOtherBusiness._Baseexchangerate = decBaseExRate
            objAssetOtherBusiness._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetOtherBusiness._Baseamount = objAssetOtherBusiness._Value * decBaseExRate / decPaidExRate
            Else
                objAssetOtherBusiness._Baseamount = objAssetOtherBusiness._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetOtherBusiness._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetOtherBusiness._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetOtherBusiness._ClientIp = Session("IP_ADD").ToString()
            objAssetOtherBusiness._HostName = Session("HOST_NAME").ToString()
            objAssetOtherBusiness._FormName = mstrModuleName1
            objAssetOtherBusiness._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsOtherBusiness As DataSet = objAssetOtherBusiness.GetList("List", mintAssetDeclarationUnkid)
            Dim dtOtherBusiness As DataTable = dsOtherBusiness.Tables(0)
            If dtOtherBusiness IsNot Nothing Then
                Dim drRow() As DataRow = dtOtherBusiness.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Otherbusinesstotal = objAssetOtherBusiness._Baseamount + Format(CDec(dtOtherBusiness.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Otherbusinesstotal = Format(objAssetOtherBusiness._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetOtherBusiness.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetOtherBusiness._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetOtherBusiness._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetOtherBusiness._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillOtherBusinessGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_OtherBusiness()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddOtherBusiness_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateOtherBusiness_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateOtherBusiness.Click
        Try
            If IsValid_OtherBusiness() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("OtherBusinessSrNo")

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtOtherBusiness.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtOtherBusiness.Rows(SrNo)

            ''drRow.Item("assetotherbusinesstranunkid") = -1
            ''drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("businesstype") = txtBusinessType.Text.Trim
            'drRow.Item("placeclad") = txtLocationOtherBusiness.Text.Trim
            'drRow.Item("value") = txtValueOtherBusiness.Text.Trim


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            ''drRow.Item("currencyunkid") = CInt(drpCurrencyBusiness.SelectedItem.Value) 'Sohail (06 Apr 2012)
            'drRow.Item("countryunkid") = CInt(drpCurrencyBusiness.SelectedItem.Value)
            ''Pinkal (30-Jan-2013) -- End


            'If txtServantOtherBusiness.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtServantOtherBusiness.Text.Trim
            'End If
            'If txtWifeHusbundOtherBusiness.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtWifeHusbundOtherBusiness.Text.Trim
            'End If
            'If txtChildrenOtherBusiness.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtChildrenOtherBusiness.Text.Trim
            'End If
            'drRow.Item("isfinalsaved") = 0


            ''Pinkal (30-Jan-2013) -- Start
            ''Enhancement : TRA Changes
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvOtherBusiness, drRow, CInt(drpCurrencyBusiness.SelectedValue))
            ''Pinkal (30-Jan-2013) -- End


            'dtOtherBusiness.AcceptChanges()

            'gvOtherBusiness.DataSource = dtOtherBusiness
            'gvOtherBusiness.DataBind()
            Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran

            objAssetOtherBusiness.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvOtherBusiness.DataKeys(SrNo).Item("assetotherbusinesstranunkid")), Nothing)
            objAssetOtherBusiness._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetOtherBusiness._Businesstype = txtBusinessType.Text.Trim
            objAssetOtherBusiness._Placeclad = txtLocationOtherBusiness.Text.Trim
            objAssetOtherBusiness._Value = txtValueOtherBusiness.Text.Trim
            objAssetOtherBusiness._Countryunkid = CInt(drpCurrencyBusiness.SelectedItem.Value)
            If txtServantOtherBusiness.Text.Trim = "" Then
                objAssetOtherBusiness._Servant = 0
            Else
                objAssetOtherBusiness._Servant = txtServantOtherBusiness.Text.Trim
            End If
            If txtWifeHusbundOtherBusiness.Text.Trim = "" Then
                objAssetOtherBusiness._Wifehusband = 0
            Else
                objAssetOtherBusiness._Wifehusband = txtWifeHusbundOtherBusiness.Text.Trim
            End If
            If txtChildrenOtherBusiness.Text.Trim = "" Then
                objAssetOtherBusiness._Children = 0
            Else
                objAssetOtherBusiness._Children = txtChildrenOtherBusiness.Text.Trim
            End If
            objAssetOtherBusiness._Isfinalsaved = 0

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyBusiness.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetOtherBusiness._CurrencyUnkId = intPaidCurrencyunkid
            objAssetOtherBusiness._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetOtherBusiness._Baseexchangerate = decBaseExRate
            objAssetOtherBusiness._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetOtherBusiness._Baseamount = objAssetOtherBusiness._Value * decBaseExRate / decPaidExRate
            Else
                objAssetOtherBusiness._Baseamount = objAssetOtherBusiness._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetOtherBusiness._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetOtherBusiness._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetOtherBusiness._ClientIp = Session("IP_ADD").ToString()
            objAssetOtherBusiness._HostName = Session("HOST_NAME").ToString()
            objAssetOtherBusiness._FormName = mstrModuleName1
            objAssetOtherBusiness._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsOtherBusiness As DataSet = objAssetOtherBusiness.GetList("List", mintAssetDeclarationUnkid)
            Dim dtOtherBusiness As DataTable = dsOtherBusiness.Tables(0)
            If dtOtherBusiness IsNot Nothing Then
                Dim drRow() As DataRow = dtOtherBusiness.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Otherbusinesstotal = objAssetOtherBusiness._Baseamount + Format(CDec(dtOtherBusiness.Compute("sum(baseamount)", " assetotherbusinesstranunkid <> " & objAssetOtherBusiness._Assetotherbusinesstranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Otherbusinesstotal = Format(objAssetOtherBusiness._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetOtherBusiness.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetOtherBusiness._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetOtherBusiness._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetOtherBusiness._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillOtherBusinessGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_OtherBusiness()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateOtherBusiness_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetOtherBusiness_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetOtherBusiness.Click
        Try
            Call Reset_OtherBusiness()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetOtherBusiness_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteOtherBusiness_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteOtherBusiness.buttonDelReasonYes_Click
        Dim objAssetOtherBusiness As New clsAsset_otherbusiness_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("OtherBusinessSrNo"))
            objAssetOtherBusiness.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvOtherBusiness.DataKeys(SrNo).Item("assetotherbusinesstranunkid")), Nothing)
            objAssetOtherBusiness._Isvoid = True
            objAssetOtherBusiness._Voiduserunkid = CInt(Session("UserId"))
            objAssetOtherBusiness._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetOtherBusiness._Voidreason = popupDeleteOtherBusiness.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetOtherBusiness._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetOtherBusiness._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetOtherBusiness._ClientIp = Session("IP_ADD").ToString()
            objAssetOtherBusiness._HostName = Session("HOST_NAME").ToString()
            objAssetOtherBusiness._FormName = mstrModuleName1
            objAssetOtherBusiness._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsOtherBusiness As DataSet = objAssetOtherBusiness.GetList("List", mintAssetDeclarationUnkid)
            Dim dtOtherBusiness As DataTable = dsOtherBusiness.Tables(0)
            If dtOtherBusiness IsNot Nothing Then
                Dim drRow() As DataRow = dtOtherBusiness.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Otherbusinesstotal = Format(CDec(dtOtherBusiness.Compute("sum(baseamount)", " assetotherbusinesstranunkid <> " & objAssetOtherBusiness._Assetotherbusinesstranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Otherbusinesstotal = Format(0, Session("fmtCurrency"))
                End If
            End If

            If objAssetOtherBusiness.Void(CInt(gvOtherBusiness.DataKeys(SrNo).Item("assetotherbusinesstranunkid")), CInt(Session("UserId")), objAssetOtherBusiness._Voiddatetime, objAssetOtherBusiness._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillOtherBusinessGrid()
            ElseIf objAssetOtherBusiness._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetOtherBusiness._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteOtherBusiness_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvOtherBusiness_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOtherBusiness.PageIndexChanging
        Try
            gvOtherBusiness.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvOtherBusiness.DataSource = dtOtherBusiness
            'gvOtherBusiness.DataBind()
            Call FillOtherBusinessGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvOtherBusiness_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvOtherBusiness_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvOtherBusiness.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblErrorOtherBusiness.Text = ""
                lblErrorCOtherBusiness.Text = "" 'Sohail (06 Apr 2012)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("OtherBusinessSrNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtBusinessType.Text = dtOtherBusiness.Rows(SrNo).Item("businesstype").ToString
                'txtLocationOtherBusiness.Text = dtOtherBusiness.Rows(SrNo).Item("placeclad").ToString
                ''Sohail (23 Apr 2012) -- Start
                ''TRA - ENHANCEMENT
                ''txtValueOtherBusiness.Text = Format(dtOtherBusiness.Rows(SrNo).Item("value"), GUI.fmtCurrency)
                'txtValueOtherBusiness.Text = Format(dtOtherBusiness.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                ''Sohail (23 Apr 2012) -- End


                ''Pinkal (30-Jan-2013) -- Start
                ''Enhancement : TRA Changes
                ''drpCurrencyBusiness.SelectedValue = dtOtherBusiness.Rows(SrNo).Item("currencyunkid").ToString 'Sohail (06 Apr 2012)
                'drpCurrencyBusiness.SelectedValue = dtOtherBusiness.Rows(SrNo).Item("countryunkid").ToString 'Sohail (06 Apr 2012)
                ''Pinkal (30-Jan-2013) -- End


                'txtServantOtherBusiness.Text = dtOtherBusiness.Rows(SrNo).Item("servant").ToString
                'txtWifeHusbundOtherBusiness.Text = dtOtherBusiness.Rows(SrNo).Item("wifehusband").ToString
                'txtChildrenOtherBusiness.Text = dtOtherBusiness.Rows(SrNo).Item("children").ToString
                txtBusinessType.Text = gvOtherBusiness.Rows(SrNo).Cells(2).Text
                txtLocationOtherBusiness.Text = gvOtherBusiness.Rows(SrNo).Cells(3).Text
                txtValueOtherBusiness.Text = Format(CDec(gvOtherBusiness.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpCurrencyBusiness.SelectedValue = CInt(gvOtherBusiness.DataKeys(SrNo).Item("countryunkid").ToString)
                txtServantOtherBusiness.Text = gvOtherBusiness.Rows(SrNo).Cells(6).Text
                txtWifeHusbundOtherBusiness.Text = gvOtherBusiness.Rows(SrNo).Cells(7).Text
                txtChildrenOtherBusiness.Text = gvOtherBusiness.Rows(SrNo).Cells(8).Text
                'Hemant (24 Dec 2019) -- End

                btnAddOtherBusiness.Enabled = False
                btnUpdateOtherBusiness.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtOtherBusiness.Rows(CInt(e.CommandArgument)).Delete()

                'If dtOtherBusiness.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtOtherBusiness.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = ""
                '    dtOtherBusiness.Rows.Add(r)
                'End If
                'dtOtherBusiness.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvOtherBusiness.DataSource = dtOtherBusiness
                'gvOtherBusiness.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("OtherBusinessSrNo") = SrNo
                popupDeleteOtherBusiness.Show()
                'Hemant (24 Dec 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvOtherBusiness_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvOtherBusiness_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOtherBusiness.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                '    'Sohail (23 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    'e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), GUI.fmtCurrency)
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    'Sohail (23 Apr 2012) -- End
                '    'Sohail (06 Apr 2012) -- Start
                '    'TRA - ENHANCEMENT


                '    'Pinkal (30-Jan-2013) -- Start
                '    'Enhancement : TRA Changes

                '    'Dim intCurrencyId As Integer
                '    'Integer.TryParse(e.Row.Cells(5).Text, intCurrencyId)
                '    'Dim objExRate As New clsExchangeRate
                '    'objExRate._ExchangeRateunkid = intCurrencyId
                '    'e.Row.Cells(5).Text = objExRate._Currency_Sign

                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If


                '    'Pinkal (30-Jan-2013) -- End

                '    'Sohail (06 Apr 2012) -- End
                'End If
                'Hemant (24 Dec 2019) -- End
                If IsDBNull(gvOtherBusiness.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvOtherBusiness_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

    'Pinkal (30-Jan-2013) -- Start
    'Enhancement : TRA Changes

#Region " Resources "

#Region " Private Method Functions "

    Private Sub FillComboResources()
        Dim objExchange As New clsExchangeRate
        Try
            Dim dsCurrResources As DataSet = objExchange.getComboList("Currency", True)
            With drpCurrencyResources
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCurrResources.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboResources :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub FillResourcesGrid()
        Dim objAssetResources As New clsAsset_otherresources_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtResources As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetResources._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetResources._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtResources = objAssetResources._Datasource

            'If dtResources.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtResources.NewRow

            '    r.Item(2) = "None"
            '    r.Item(3) = ""
            '    dtResources.Rows.Add(r)
            'End If
            Dim dsResources As DataSet = objAssetResources.GetList("List", mintAssetDeclarationUnkid)
            dtResources = dsResources.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtResources.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtResources.NewRow
                dtResources.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvResources.DataSource = dtResources
            gvResources.DataBind()
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvResources.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("FillResourcesGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub Reset_Resources()
        Try
            txtResources.Text = ""
            txtResourcesLocation.Text = ""
            txtResourceValue.Text = ""
            txtResourcesEmp.Text = ""
            txtResourcesHusbandwife.Text = ""
            txtResourceChildren.Text = ""
            LblErrorResources.Text = ""
            lblErrorCResources.Text = ""
            btnAddResource.Enabled = True
            btnUpdateResource.Enabled = False
            drpCurrencyResources.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_Resources :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_Resources() As Boolean
        Try
            LblErrorResources.Text = ""
            lblErrorCResources.Text = ""
            If CInt(drpCurrencyResources.SelectedItem.Value) <= 0 Then
                lblErrorCResources.Text = "Please select Currency."
                Return False
            End If
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtResourcesEmp.Text, decServant)
            Decimal.TryParse(txtResourcesHusbandwife.Text, decWifeHusband)
            Decimal.TryParse(txtResourceChildren.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                LblErrorResources.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_Resources :- " & ex.Message, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnAddResource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddResource.Click
        Try
            If IsValid_Resources() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtResources.NewRow

            'drRow.Item("assetotherresourcestranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("otherresources") = txtResources.Text.Trim
            'drRow.Item("location") = txtResourcesLocation.Text.Trim
            'drRow.Item("value") = txtResourceValue.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpCurrencyResources.SelectedItem.Value)

            'If txtResourcesEmp.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtResourcesEmp.Text.Trim
            'End If

            'If txtResourcesHusbandwife.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtResourcesHusbandwife.Text.Trim
            'End If

            'If txtResourceChildren.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtResourceChildren.Text.Trim
            'End If

            'drRow.Item("isfinalsaved") = 0

            'Update_DataGridview_DataTable_Extra_Columns(gvResources, drRow, CInt(drpCurrencyResources.SelectedValue))
            'dtResources.Rows.Add(drRow)

            'gvResources.DataSource = dtResources
            'gvResources.DataBind()
            Dim objAssetResources As New clsAsset_otherresources_tran

            objAssetResources._Assetotherresourcestranunkid = -1
            objAssetResources._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetResources._Otherresources = txtResources.Text.Trim
            objAssetResources._Location = txtResourcesLocation.Text.Trim
            objAssetResources._Value = txtResourceValue.Text.Trim
            objAssetResources._Countryunkid = CInt(drpCurrencyResources.SelectedItem.Value)

            If txtResourcesEmp.Text.Trim = "" Then
                objAssetResources._Servant = 0
            Else
                objAssetResources._Servant = txtResourcesEmp.Text.Trim
            End If

            If txtResourcesHusbandwife.Text.Trim = "" Then
                objAssetResources._Wifehusband = 0
            Else
                objAssetResources._Wifehusband = txtResourcesHusbandwife.Text.Trim
            End If

            If txtResourceChildren.Text.Trim = "" Then
                objAssetResources._Children = 0
            Else
                objAssetResources._Children = txtResourceChildren.Text.Trim
            End If

            objAssetResources._Isfinalsaved = 0

            objAssetResources._Userunkid = CInt(Session("UserId"))
            If objAssetResources._Transactiondate = Nothing Then
                objAssetResources._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyResources.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetResources._CurrencyUnkId = intPaidCurrencyunkid
            objAssetResources._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetResources._Baseexchangerate = decBaseExRate
            objAssetResources._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetResources._Baseamount = objAssetResources._Value * decBaseExRate / decPaidExRate
            Else
                objAssetResources._Baseamount = objAssetResources._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetResources._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetResources._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetResources._ClientIp = Session("IP_ADD").ToString()
            objAssetResources._HostName = Session("HOST_NAME").ToString()
            objAssetResources._FormName = mstrModuleName1
            objAssetResources._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsResources As DataSet = objAssetResources.GetList("List", mintAssetDeclarationUnkid)
            Dim dtResources As DataTable = dsResources.Tables(0)
            If dtResources IsNot Nothing Then
                Dim drRow() As DataRow = dtResources.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Otherresourcetotal = objAssetResources._Baseamount + Format(CDec(dtResources.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Otherresourcetotal = Format(objAssetResources._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetResources.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetResources._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetResources._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetResources._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillResourcesGrid()
            'Hemant (24 Dec 2019) -- End
            Call Reset_Resources()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddResource_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateResource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateResource.Click
        Try
            If IsValid_Resources() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("ResourcesNo")

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtResources.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtResources.Rows(SrNo)
            'drRow.Item("otherresources") = txtResources.Text.Trim
            'drRow.Item("location") = txtResourcesLocation.Text.Trim
            'drRow.Item("value") = txtResourceValue.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpCurrencyResources.SelectedItem.Value)

            'If txtResourcesEmp.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtResourcesEmp.Text.Trim
            'End If

            'If txtResourcesHusbandwife.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtResourcesHusbandwife.Text.Trim
            'End If

            'If txtResourceChildren.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtResourceChildren.Text.Trim
            'End If

            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("AUD") = "U"

            'Update_DataGridview_DataTable_Extra_Columns(gvResources, drRow, CInt(drpCurrencyResources.SelectedValue))

            'dtResources.AcceptChanges()

            'gvResources.DataSource = dtResources
            'gvResources.DataBind()
            Dim objAssetResources As New clsAsset_otherresources_tran

            objAssetResources.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvResources.DataKeys(SrNo).Item("assetotherresourcestranunkid")), Nothing)
            objAssetResources._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetResources._Otherresources = txtResources.Text.Trim
            objAssetResources._Location = txtResourcesLocation.Text.Trim
            objAssetResources._Value = txtResourceValue.Text.Trim
            objAssetResources._Countryunkid = CInt(drpCurrencyResources.SelectedItem.Value)

            If txtResourcesEmp.Text.Trim = "" Then
                objAssetResources._Servant = 0
            Else
                objAssetResources._Servant = txtResourcesEmp.Text.Trim
            End If

            If txtResourcesHusbandwife.Text.Trim = "" Then
                objAssetResources._Wifehusband = 0
            Else
                objAssetResources._Wifehusband = txtResourcesHusbandwife.Text.Trim
            End If

            If txtResourceChildren.Text.Trim = "" Then
                objAssetResources._Children = 0
            Else
                objAssetResources._Children = txtResourceChildren.Text.Trim
            End If

            objAssetResources._Isfinalsaved = 0

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyResources.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetResources._CurrencyUnkId = intPaidCurrencyunkid
            objAssetResources._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetResources._Baseexchangerate = decBaseExRate
            objAssetResources._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetResources._Baseamount = objAssetResources._Value * decBaseExRate / decPaidExRate
            Else
                objAssetResources._Baseamount = objAssetResources._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetResources._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetResources._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetResources._ClientIp = Session("IP_ADD").ToString()
            objAssetResources._HostName = Session("HOST_NAME").ToString()
            objAssetResources._FormName = mstrModuleName1
            objAssetResources._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsResources As DataSet = objAssetResources.GetList("List", mintAssetDeclarationUnkid)
            Dim dtResources As DataTable = dsResources.Tables(0)
            If dtResources IsNot Nothing Then
                Dim drRow() As DataRow = dtResources.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Otherresourcetotal = objAssetResources._Baseamount + Format(CDec(dtResources.Compute("sum(baseamount)", " assetotherresourcestranunkid <> " & objAssetResources._Assetotherresourcestranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Otherresourcetotal = Format(objAssetResources._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetResources.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetResources._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetResources._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetResources._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillResourcesGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_Resources()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateResource_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetResource_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetResource.Click
        Try
            Call Reset_Resources()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetResource_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteResources_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteResources.buttonDelReasonYes_Click
        Dim objAssetResources As New clsAsset_otherresources_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("ResourcesNo"))
            objAssetResources.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvResources.DataKeys(SrNo).Item("assetotherresourcestranunkid")), Nothing)
            objAssetResources._Isvoid = True
            objAssetResources._Voiduserunkid = CInt(Session("UserId"))
            objAssetResources._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetResources._Voidreason = popupDeleteResources.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetResources._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetResources._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetResources._ClientIp = Session("IP_ADD").ToString()
            objAssetResources._HostName = Session("HOST_NAME").ToString()
            objAssetResources._FormName = mstrModuleName1
            objAssetResources._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1
            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsResources As DataSet = objAssetResources.GetList("List", mintAssetDeclarationUnkid)
            Dim dtResources As DataTable = dsResources.Tables(0)
            If dtResources IsNot Nothing Then
                Dim drRow() As DataRow = dtResources.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Otherresourcetotal = Format(CDec(dtResources.Compute("sum(baseamount)", " assetotherresourcestranunkid <> " & objAssetResources._Assetotherresourcestranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Otherresourcetotal = Format(0, Session("fmtCurrency"))
                End If
            End If

            If objAssetResources.Void(CInt(gvResources.DataKeys(SrNo).Item("assetotherresourcestranunkid")), CInt(Session("UserId")), objAssetResources._Voiddatetime, objAssetResources._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillResourcesGrid()
            ElseIf objAssetResources._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetResources._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteResources_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvResources_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvResources.PageIndexChanging
        Try
            gvResources.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvResources.DataSource = dtResources
            'gvResources.DataBind()
            Call FillResourcesGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvResources_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvResources_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvResources.RowCommand
        Try

            If e.CommandName = "Change" Then

                LblErrorResources.Text = ""
                lblErrorCResources.Text = ""
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ResourcesNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtResources.Text = dtResources.Rows(SrNo).Item("otherresources").ToString
                'txtResourcesLocation.Text = dtResources.Rows(SrNo).Item("location").ToString
                'txtResourceValue.Text = Format(dtResources.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                'drpCurrencyResources.SelectedValue = dtResources.Rows(SrNo).Item("countryunkid").ToString
                'txtResourcesEmp.Text = dtResources.Rows(SrNo).Item("servant").ToString
                'txtResourcesHusbandwife.Text = dtResources.Rows(SrNo).Item("wifehusband").ToString
                'txtResourceChildren.Text = dtResources.Rows(SrNo).Item("children").ToString
                txtResources.Text = gvResources.Rows(SrNo).Cells(2).Text
                txtResourcesLocation.Text = gvResources.Rows(SrNo).Cells(3).Text
                txtResourceValue.Text = Format(CDec(gvResources.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpCurrencyResources.SelectedValue = CInt(gvResources.DataKeys(SrNo).Item("countryunkid").ToString)
                txtResourcesEmp.Text = gvResources.Rows(SrNo).Cells(6).Text
                txtResourcesHusbandwife.Text = gvResources.Rows(SrNo).Cells(7).Text
                txtResourceChildren.Text = gvResources.Rows(SrNo).Cells(8).Text
                'Hemant (24 Dec 2019) -- End
                btnAddResource.Enabled = False
                btnUpdateResource.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtResources.Rows(CInt(e.CommandArgument)).Delete()

                'If dtResources.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtResources.NewRow

                '    r.Item(2) = "None"
                '    r.Item(3) = ""
                '    dtResources.Rows.Add(r)
                'End If
                'dtResources.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvResources.DataSource = dtResources
                'gvResources.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ResourcesNo") = SrNo
                popupDeleteResources.Show()
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvResources_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvResources_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvResources.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If
                'End If
                If IsDBNull(gvResources.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvResources_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Debt "

#Region " Private Method Functions "

    Private Sub FillComboDebt()
        Dim objExchange As New clsExchangeRate
        Try
            Dim dsCurrDebt As DataSet = objExchange.getComboList("Currency", True)
            With drpCurrencyDebt
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCurrDebt.Tables("Currency")
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboDebt :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub FillDebtGrid()
        Dim objAssetDebt As New clsAsset_debts_tran
        'Hemant (24 Dec 2019) -- Start
        'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
        Dim dtDebt As DataTable
        'Hemant (24 Dec 2019) -- End
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAssetDebt._Assetdeclarationunkid = mintAssetDeclarationUnkid
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'objAssetDebt._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            ''Shani(20-Nov-2015) -- End

            'dtDebt = objAssetDebt._Datasource

            'If dtDebt.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtDebt.NewRow

            '    r.Item(2) = "None"
            '    r.Item(3) = ""
            '    dtDebt.Rows.Add(r)
            'End If
            Dim dsDebt As DataSet = objAssetDebt.GetList("List", mintAssetDeclarationUnkid)
            dtDebt = dsDebt.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtDebt.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtDebt.NewRow
                dtDebt.Rows.Add(r)
            End If
            'Hemant (24 Dec 2019) -- End
            gvDebt.DataSource = dtDebt
            gvDebt.DataBind()

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            If blnRecordExist = False Then
                gvDebt.Rows(0).Visible = False
            End If
            'Hemant (24 Dec 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError("FillDebtGrid :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub Reset_Debts()
        Try
            txtDebt.Text = ""
            txtDebtLocation.Text = ""
            txtDebtValue.Text = ""
            txtDebtEmployee.Text = ""
            txtDebtWifeHusband.Text = ""
            txtDebtChildren.Text = ""
            LblErrorDebt.Text = ""
            LblCErrorDebt.Text = ""
            btnAddDebt.Enabled = True
            btnUpdateDebt.Enabled = False
            drpCurrencyDebt.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError("Reset_Debts :- " & ex.Message, Me)
        End Try
    End Sub

    Private Function IsValid_Debts() As Boolean
        Try
            LblErrorDebt.Text = ""
            LblCErrorDebt.Text = ""
            If CInt(drpCurrencyDebt.SelectedItem.Value) <= 0 Then
                lblErrorCResources.Text = "Please select Currency."
                Return False
            End If
            Dim decServant As Decimal
            Dim decWifeHusband As Decimal
            Dim decChildren As Decimal

            Decimal.TryParse(txtDebtEmployee.Text, decServant)
            Decimal.TryParse(txtDebtWifeHusband.Text, decWifeHusband)
            Decimal.TryParse(txtDebtChildren.Text, decChildren)

            Dim decTotalPercentage As Decimal = decServant + decWifeHusband + decChildren

            If decTotalPercentage <> 100 Then
                LblErrorResources.Text = "Sorry! Total for Employee, Wife/Husband and Children must be 100."
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValid_Debts :- " & ex.Message, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnAddDebt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDebt.Click
        Try
            If IsValid_Debts() = False Then
                Exit Try
            End If

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'Dim drRow As DataRow = dtDebt.NewRow

            'drRow.Item("assetdebtstranunkid") = -1
            'drRow.Item("assetdeclarationunkid") = mintAssetDeclarationUnkid
            'drRow.Item("debts") = txtDebt.Text.Trim
            'drRow.Item("location") = txtDebtLocation.Text.Trim
            'drRow.Item("value") = txtDebtValue.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpCurrencyDebt.SelectedItem.Value)

            'If txtDebtEmployee.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtDebtEmployee.Text.Trim
            'End If

            'If txtDebtWifeHusband.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtDebtWifeHusband.Text.Trim
            'End If

            'If txtDebtChildren.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtDebtChildren.Text.Trim
            'End If

            'drRow.Item("isfinalsaved") = 0

            'Update_DataGridview_DataTable_Extra_Columns(gvDebt, drRow, CInt(drpCurrencyDebt.SelectedValue))
            'dtDebt.Rows.Add(drRow)

            'gvDebt.DataSource = dtDebt
            'gvDebt.DataBind()
            Dim objAssetDebt As New clsAsset_debts_tran

            objAssetDebt._Assetdebtstranunkid = -1
            objAssetDebt._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetDebt._Debts = txtDebt.Text.Trim
            objAssetDebt._Location = txtDebtLocation.Text.Trim
            objAssetDebt._Value = txtDebtValue.Text.Trim
            objAssetDebt._Countryunkid = CInt(drpCurrencyDebt.SelectedItem.Value)

            If txtDebtEmployee.Text.Trim = "" Then
                objAssetDebt._Servant = 0
            Else
                objAssetDebt._Servant = txtDebtEmployee.Text.Trim
            End If

            If txtDebtWifeHusband.Text.Trim = "" Then
                objAssetDebt._Wifehusband = 0
            Else
                objAssetDebt._Wifehusband = txtDebtWifeHusband.Text.Trim
            End If

            If txtDebtChildren.Text.Trim = "" Then
                objAssetDebt._Children = 0
            Else
                objAssetDebt._Children = txtDebtChildren.Text.Trim
            End If

            objAssetDebt._Isfinalsaved = 0
            objAssetDebt._Userunkid = CInt(Session("UserId"))
            If objAssetDebt._Transactiondate = Nothing Then
                objAssetDebt._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyDebt.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetDebt._CurrencyUnkId = intPaidCurrencyunkid
            objAssetDebt._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetDebt._Baseexchangerate = decBaseExRate
            objAssetDebt._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetDebt._Baseamount = objAssetDebt._Value * decBaseExRate / decPaidExRate
            Else
                objAssetDebt._Baseamount = objAssetDebt._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDebt._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDebt._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDebt._ClientIp = Session("IP_ADD").ToString()
            objAssetDebt._HostName = Session("HOST_NAME").ToString()
            objAssetDebt._FormName = mstrModuleName1
            objAssetDebt._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsDebt As DataSet = objAssetDebt.GetList("List", mintAssetDeclarationUnkid)
            Dim dtDebt As DataTable = dsDebt.Tables(0)
            If dtDebt IsNot Nothing Then
                Dim drRow() As DataRow = dtDebt.Select()
                If drRow.Length > 0 Then
                    objAssetDeclare._Debttotal = objAssetDebt._Baseamount + Format(CDec(dtDebt.Compute("sum(baseamount)", "")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Debttotal = Format(objAssetDebt._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetDebt.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetDebt._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetDebt._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetDebt._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillDebtGrid()
            'Hemant (24 Dec 2019) -- End

            Call Reset_Debts()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnAddDebt_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateDebt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDebt.Click
        Try
            If IsValid_Debts() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("DebtNo")
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'dtDebt.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtDebt.Rows(SrNo)
            'drRow.Item("debts") = txtDebt.Text.Trim
            'drRow.Item("location") = txtDebtLocation.Text.Trim
            'drRow.Item("value") = txtDebtValue.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpCurrencyDebt.SelectedItem.Value)

            'If txtDebtEmployee.Text.Trim = "" Then
            '    drRow.Item("servant") = 0
            'Else
            '    drRow.Item("servant") = txtDebtEmployee.Text.Trim
            'End If

            'If txtDebtWifeHusband.Text.Trim = "" Then
            '    drRow.Item("wifehusband") = 0
            'Else
            '    drRow.Item("wifehusband") = txtDebtWifeHusband.Text.Trim
            'End If

            'If txtDebtChildren.Text.Trim = "" Then
            '    drRow.Item("children") = 0
            'Else
            '    drRow.Item("children") = txtDebtChildren.Text.Trim
            'End If

            'drRow.Item("isfinalsaved") = 0
            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvDebt, drRow, CInt(drpCurrencyDebt.SelectedValue))

            'dtDebt.AcceptChanges()

            'gvDebt.DataSource = dtDebt
            'gvDebt.DataBind()
            Dim objAssetDebt As New clsAsset_debts_tran

            objAssetDebt.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDebt.DataKeys(SrNo).Item("assetdebtstranunkid")), Nothing)
            objAssetDebt._Assetdeclarationunkid(Session("Database_Name")) = mintAssetDeclarationUnkid
            objAssetDebt._Debts = txtDebt.Text.Trim
            objAssetDebt._Location = txtDebtLocation.Text.Trim
            objAssetDebt._Value = txtDebtValue.Text.Trim
            objAssetDebt._Countryunkid = CInt(drpCurrencyDebt.SelectedItem.Value)

            If txtDebtEmployee.Text.Trim = "" Then
                objAssetDebt._Servant = 0
            Else
                objAssetDebt._Servant = txtDebtEmployee.Text.Trim
            End If

            If txtDebtWifeHusband.Text.Trim = "" Then
                objAssetDebt._Wifehusband = 0
            Else
                objAssetDebt._Wifehusband = txtDebtWifeHusband.Text.Trim
            End If

            If txtDebtChildren.Text.Trim = "" Then
                objAssetDebt._Children = 0
            Else
                objAssetDebt._Children = txtDebtChildren.Text.Trim
            End If

            objAssetDebt._Isfinalsaved = 0

            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpCurrencyDebt.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetDebt._CurrencyUnkId = intPaidCurrencyunkid
            objAssetDebt._Basecurrencyid = Me.ViewState("Exchangerateunkid")
            objAssetDebt._Baseexchangerate = decBaseExRate
            objAssetDebt._Expaidrate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetDebt._Baseamount = objAssetDebt._Value * decBaseExRate / decPaidExRate
            Else
                objAssetDebt._Baseamount = objAssetDebt._Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDebt._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDebt._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDebt._ClientIp = Session("IP_ADD").ToString()
            objAssetDebt._HostName = Session("HOST_NAME").ToString()
            objAssetDebt._FormName = mstrModuleName1
            objAssetDebt._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsDebt As DataSet = objAssetDebt.GetList("List", mintAssetDeclarationUnkid)
            Dim dtDebt As DataTable = dsDebt.Tables(0)
            If dtDebt IsNot Nothing Then
                Dim drRow() As DataRow = dtDebt.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Debttotal = objAssetDebt._Baseamount + Format(CDec(dtDebt.Compute("sum(baseamount)", " assetdebtstranunkid <> " & objAssetDebt._Assetdebtstranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Debttotal = Format(objAssetDebt._Baseamount, Session("fmtCurrency"))
                End If
            End If
            If objAssetDebt.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetDebt._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetDebt._Message, Me)
                End If
            Else
                mintAssetDeclarationUnkid = objAssetDebt._Assetdeclarationunkid(Session("Database_Name"))
            End If

            Call FillDebtGrid()
            'Hemant (24 Dec 2019) -- End
            Call Reset_Debts()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnUpdateDebt_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetDebt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetDebt.Click
        Try
            Call Reset_Debts()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnResetDebt_Click :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (24 Dec 2019) -- Start
    'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
    Protected Sub popupDeleteDebts_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteDebts.buttonDelReasonYes_Click
        Dim objAssetDebt As New clsAsset_debts_tran

        Try
            Dim SrNo As Integer = CInt(Me.ViewState("DebtNo"))
            objAssetDebt.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDebt.DataKeys(SrNo).Item("assetdebtstranunkid")), Nothing)
            objAssetDebt._Isvoid = True
            objAssetDebt._Voiduserunkid = CInt(Session("UserId"))
            objAssetDebt._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetDebt._Voidreason = popupDeleteDebts.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDebt._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDebt._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDebt._ClientIp = Session("IP_ADD").ToString()
            objAssetDebt._HostName = Session("HOST_NAME").ToString()
            objAssetDebt._FormName = mstrModuleName1
            objAssetDebt._FromWeb = True
            'clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            'clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'clsCommonATLog._WebFormName = mstrModuleName1

            Dim objAssetDeclare As clsAssetdeclaration_master = SetValueAssetDeclaratationMaster()
            Dim dsDebt As DataSet = objAssetDebt.GetList("List", mintAssetDeclarationUnkid)
            Dim dtDebt As DataTable = dsDebt.Tables(0)
            If dtDebt IsNot Nothing Then
                Dim drRow() As DataRow = dtDebt.Select()
                If drRow.Length > 1 Then
                    objAssetDeclare._Debttotal = Format(CDec(dtDebt.Compute("sum(baseamount)", " assetdebtstranunkid <> " & objAssetDebt._Assetdebtstranunkid & " ")), Session("fmtCurrency"))
                Else
                    objAssetDeclare._Debttotal = Format(0, Session("fmtCurrency"))
                End If
            End If
            If objAssetDebt.Void(CInt(gvDebt.DataKeys(SrNo).Item("assetdebtstranunkid")), CInt(Session("UserId")), objAssetDebt._Voiddatetime, objAssetDebt._Voidreason, Nothing, objAssetDeclare) = True Then
                'Hemant (24 Dec 2019) --[objAssetDeclare]
                Call FillDebtGrid()
            ElseIf objAssetDebt._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetDebt._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popupDeleteDebts_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (24 Dec 2019) -- End

#End Region

#Region " GridView Events "

    Protected Sub gvDebt_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDebt.PageIndexChanging
        Try
            gvDebt.PageIndex = e.NewPageIndex
            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            'gvDebt.DataSource = dtDebt
            'gvDebt.DataBind()
            Call FillDebtGrid()
            'Hemant (24 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Error in gvDebt_PageIndexChanging " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvdebt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDebt.RowCommand
        Try

            If e.CommandName = "Change" Then

                LblErrorDebt.Text = ""
                LblCErrorDebt.Text = ""
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DebtNo") = SrNo

                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'txtDebt.Text = dtDebt.Rows(SrNo).Item("debts").ToString
                'txtDebtLocation.Text = dtDebt.Rows(SrNo).Item("location").ToString
                'txtDebtValue.Text = Format(dtDebt.Rows(SrNo).Item("value"), Session("fmtCurrency"))
                'drpCurrencyDebt.SelectedValue = dtDebt.Rows(SrNo).Item("countryunkid").ToString
                'txtDebtEmployee.Text = dtDebt.Rows(SrNo).Item("servant").ToString
                'txtDebtWifeHusband.Text = dtDebt.Rows(SrNo).Item("wifehusband").ToString
                'txtDebtChildren.Text = dtDebt.Rows(SrNo).Item("children").ToString
                txtDebt.Text = gvDebt.Rows(SrNo).Cells(2).Text
                txtDebtLocation.Text = gvDebt.Rows(SrNo).Cells(3).Text
                txtDebtValue.Text = Format(CDec(gvDebt.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpCurrencyDebt.SelectedValue = CInt(gvDebt.DataKeys(SrNo).Item("countryunkid").ToString)
                txtDebtEmployee.Text = gvDebt.Rows(SrNo).Cells(6).Text
                txtDebtWifeHusband.Text = gvDebt.Rows(SrNo).Cells(7).Text
                txtDebtChildren.Text = gvDebt.Rows(SrNo).Cells(8).Text
                'Hemant (24 Dec 2019) -- End

                btnAddDebt.Enabled = False
                btnUpdateDebt.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'dtDebt.Rows(CInt(e.CommandArgument)).Delete()

                'If dtResources.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtDebt.NewRow

                '    r.Item(2) = "None"
                '    r.Item(3) = ""
                '    dtDebt.Rows.Add(r)
                'End If
                'dtResources.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvDebt.DataSource = dtDebt
                'gvDebt.DataBind()
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DebtNo") = SrNo
                popupDeleteDebts.Show()
                'Hemant (24 Dec 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvdebt_RowCommand :- " & ex.Message, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvDebt_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDebt.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (24 Dec 2019) -- Start
                'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                '    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                '    Dim intCountryId As Integer
                '    Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                '    Dim objExRate As New clsExchangeRate
                '    Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                '    Else
                '        e.Row.Cells(5).Text = ""
                '    End If
                'End If
                If IsDBNull(gvDebt.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                End If
                'Hemant (24 Dec 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvDebt_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub
#End Region


#End Region

    'Pinkal (30-Jan-2013) -- End


    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]--END


            'Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text)
            Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            gvList.Columns(0).HeaderText = Language._Object.getCaption("btnEdit", gvList.Columns(0).HeaderText).Replace("&", "")
            gvList.Columns(1).HeaderText = Language._Object.getCaption("btnDelete", gvList.Columns(1).HeaderText).Replace("&", "")
            gvList.Columns(2).HeaderText = Language._Object.getCaption("mnuFinalSave", gvList.Columns(2).HeaderText)
            gvList.Columns(3).HeaderText = Language._Object.getCaption("mnuUnlockFinalSave", gvList.Columns(3).HeaderText)
            gvList.Columns(4).HeaderText = Language._Object.getCaption("mnuPrintDeclaration", gvList.Columns(4).HeaderText)


            gvList.Columns(5).HeaderText = Language._Object.getCaption(gvList.Columns(5).FooterText, gvList.Columns(5).HeaderText)
            gvList.Columns(6).HeaderText = Language._Object.getCaption(gvList.Columns(6).FooterText, gvList.Columns(6).HeaderText)
            gvList.Columns(7).HeaderText = Language._Object.getCaption(gvList.Columns(7).FooterText, gvList.Columns(7).HeaderText)
            gvList.Columns(8).HeaderText = Language._Object.getCaption(gvList.Columns(8).FooterText, gvList.Columns(8).HeaderText)

            'Sohail (27 Feb 2015) -- Start
            'Enhancement - Unlock Final Save in Self Service.
            popupFinalYesNo.Message = Language.getMessage(mstrModuleName, 6, "After Final Save You can not Edit / Delete Asset Declaration." & vbCrLf & vbCrLf & "Are you sure you want to Final Save this Asset Declaration?")
            popupUnlockFinalYesNo.Message = Language.getMessage(mstrModuleName, 8, "Are you sure you want to Unlock Final Save this Asset Declaration?")
            popup1.Title = Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?")
            'Sohail (27 Feb 2015) -- End

            Language.setLanguage(mstrModuleName1)
            Me.Title = Language._Object.getCaption(mstrModuleName1, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName1, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName1, Me.Title)
            'SHANI [01 FEB 2015]--END

            'step1 Instruction
            Me.lblInstruction.Text = Language._Object.getCaption(lblInstruction.ID, Me.lblInstruction.Text)
            Me.lblStep1_Employee.Text = Language._Object.getCaption(lblStep1_Employee.ID, Me.lblStep1_Employee.Text)
            Me.lblDate.Text = Language._Object.getCaption(lblDate.ID, Me.lblDate.Text)
            Me.lblStep1_NoOfEmployment.Text = Language._Object.getCaption(lblStep1_NoOfEmployment.ID, Me.lblStep1_NoOfEmployment.Text)
            Me.lblOfficePosition.Text = Language._Object.getCaption(lblOfficePosition.ID, Me.lblOfficePosition.Text)
            Me.lblCenterforWork.Text = Language._Object.getCaption(lblCenterforWork.ID, Me.lblCenterforWork.Text)
            Me.lblWorkStation.Text = Language._Object.getCaption(Me.lblWorkStation.ID, Me.lblWorkStation.Text)

            'step2 
            Me.lblFinanceToDate_Step2.Text = Language._Object.getCaption(lblFinanceToDate_Step2.ID, Me.lblFinanceToDate_Step2.Text)
            Me.colhBank_step3.Text = Language._Object.getCaption("colhBank_step3", Me.colhBank_step3.Text)
            Me.colhAccount_step3.Text = Language._Object.getCaption("colhAccount_step3", Me.colhAccount_step3.Text)
            Me.colhAmount_step3.Text = Language._Object.getCaption("colhAmount_step3", Me.colhAmount_step3.Text)
            Me.lblCashExistingBank_Step2.Text = Language._Object.getCaption(Me.lblCashExistingBank_Step2.ID, Me.lblCashExistingBank_Step2.Text)
            Me.colhServant_step3.Text = Language._Object.getCaption("colhServant_step3", Me.colhServant_step3.Text)
            Me.colhWifeHusband_step3.Text = Language._Object.getCaption("colhWifeHusband_step3", Me.colhWifeHusband_step3.Text)
            Me.colhChildren_step3.Text = Language._Object.getCaption("colhChildren_step3", Me.colhChildren_step3.Text)
            Me.lblCash_Step2.Text = Language._Object.getCaption(Me.lblCash_Step2.ID, Me.lblCash_Step2.Text)

            gvBank.Columns(2).HeaderText = Language._Object.getCaption(gvBank.Columns(2).FooterText, gvBank.Columns(2).HeaderText)
            gvBank.Columns(3).HeaderText = Language._Object.getCaption(gvBank.Columns(3).FooterText, gvBank.Columns(3).HeaderText)
            gvBank.Columns(4).HeaderText = Language._Object.getCaption(gvBank.Columns(4).FooterText, gvBank.Columns(4).HeaderText)
            gvBank.Columns(5).HeaderText = Language._Object.getCaption(gvBank.Columns(5).FooterText, gvBank.Columns(5).HeaderText)
            gvBank.Columns(6).HeaderText = Language._Object.getCaption(gvBank.Columns(6).FooterText, gvBank.Columns(6).HeaderText)
            gvBank.Columns(7).HeaderText = Language._Object.getCaption(gvBank.Columns(7).FooterText, gvBank.Columns(7).HeaderText)
            gvBank.Columns(8).HeaderText = Language._Object.getCaption(gvBank.Columns(8).FooterText, gvBank.Columns(8).HeaderText)

            'Hemant (24 Dec 2019) -- Start
            'ENHANCEMENT : Changes for Bind Transactions & Perfomance Enhacement.
            popupDeleteBank.Title = Language.getMessage(mstrModuleName1, 16, "Are you sure you want to delete selected Current Assets?")
            'Hemant (24 Dec 2019) -- End


            'Step 3
            Me.lblShareDividend_step2.Text = Language._Object.getCaption(Me.lblShareDividend_step2.ID, Me.lblShareDividend_step2.Text)
            Me.colhShareValue_step3.Text = Language._Object.getCaption("colhShareValue_step3", Me.colhShareValue_step3.Text)
            Me.colhDividendAmount_step3.Text = Language._Object.getCaption("colhDividendAmount_step3", Me.colhDividendAmount_step3.Text)
            Me.colhLocation_step3.Text = Language._Object.getCaption("colhLocation_step3", Me.colhLocation_step3.Text)
            Me.colhServant_step3Shares.Text = Language._Object.getCaption("colhServant_step3Shares", Me.colhServant_step3Shares.Text)
            Me.colhWifeHusband_step3Shares.Text = Language._Object.getCaption("colhWifeHusband_step3Shares", Me.colhWifeHusband_step3Shares.Text)
            Me.colhChildren_step3Shares.Text = Language._Object.getCaption("colhChildren_step3Shares", Me.colhChildren_step3Shares.Text)

            gvShareDividend.Columns(2).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(2).FooterText, gvShareDividend.Columns(2).HeaderText)
            gvShareDividend.Columns(3).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(3).FooterText, gvShareDividend.Columns(3).HeaderText)
            gvShareDividend.Columns(4).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(4).FooterText, gvShareDividend.Columns(4).HeaderText)
            gvShareDividend.Columns(5).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(5).FooterText, gvShareDividend.Columns(5).HeaderText)
            gvShareDividend.Columns(6).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(6).FooterText, gvShareDividend.Columns(6).HeaderText)
            gvShareDividend.Columns(7).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(7).FooterText, gvShareDividend.Columns(7).HeaderText)
            gvShareDividend.Columns(8).HeaderText = Language._Object.getCaption(gvShareDividend.Columns(8).FooterText, gvShareDividend.Columns(8).HeaderText)

            'Step4
            Me.lblHouseBuilding_step3.Text = Language._Object.getCaption(Me.lblHouseBuilding_step3.ID, Me.lblHouseBuilding_step3.Text)
            Me.colhHomesBuilding_step4.Text = Language._Object.getCaption("colhHomesBuilding_step4", Me.colhHomesBuilding_step4.Text)
            Me.colhLocation_step4.Text = Language._Object.getCaption("colhLocation_step4", Me.colhLocation_step4.Text)
            Me.colhValue_step4.Text = Language._Object.getCaption("colhValue_step4", Me.colhValue_step4.Text)
            Me.colhServant_step4House.Text = Language._Object.getCaption("colhServant_step4House", Me.colhServant_step4House.Text)
            Me.colhWifeHusband_step4House.Text = Language._Object.getCaption("colhWifeHusband_step4House", Me.colhWifeHusband_step4House.Text)
            Me.colhChildren_step4House.Text = Language._Object.getCaption("colhChildren_step4House", Me.colhChildren_step4House.Text)

            gvHouseBuilding.Columns(2).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(2).FooterText, gvHouseBuilding.Columns(2).HeaderText)
            gvHouseBuilding.Columns(3).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(3).FooterText, gvHouseBuilding.Columns(3).HeaderText)
            gvHouseBuilding.Columns(4).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(4).FooterText, gvHouseBuilding.Columns(4).HeaderText)
            gvHouseBuilding.Columns(5).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(5).FooterText, gvHouseBuilding.Columns(5).HeaderText)
            gvHouseBuilding.Columns(6).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(6).FooterText, gvHouseBuilding.Columns(6).HeaderText)
            gvHouseBuilding.Columns(7).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(7).FooterText, gvHouseBuilding.Columns(7).HeaderText)
            gvHouseBuilding.Columns(8).HeaderText = Language._Object.getCaption(gvHouseBuilding.Columns(8).FooterText, gvHouseBuilding.Columns(8).HeaderText)

            'Step5
            Me.lblParkFarm_step3.Text = Language._Object.getCaption(Me.lblParkFarm_step3.ID, Me.lblParkFarm_step3.Text)
            Me.colhParkFarmMines_step4Parks.Text = Language._Object.getCaption("colhParkFarmMines_step4Parks", Me.colhParkFarmMines_step4Parks.Text)
            Me.colhAreaSize_step4Parks.Text = Language._Object.getCaption("colhAreaSize_step4Parks", Me.colhAreaSize_step4Parks.Text)
            Me.colhPlaceClad_step4Parks.Text = Language._Object.getCaption("colhPlaceClad_step4Parks", Me.colhPlaceClad_step4Parks.Text)
            Me.colhValue_step4Parks.Text = Language._Object.getCaption("colhValue_step4Parks", Me.colhValue_step4Parks.Text)
            Me.colhServant_step4Parks.Text = Language._Object.getCaption("colhServant_step4Parks", Me.colhServant_step4Parks.Text)
            Me.colhWifeHusband_step4Parks.Text = Language._Object.getCaption("colhWifeHusband_step4Parks", Me.colhWifeHusband_step4Parks.Text)
            Me.colhChildren_step4Parks.Text = Language._Object.getCaption("colhChildren_step4Parks", Me.colhChildren_step4Parks.Text)

            gvParkFarm.Columns(2).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(2).FooterText, gvParkFarm.Columns(2).HeaderText)
            gvParkFarm.Columns(3).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(3).FooterText, gvParkFarm.Columns(3).HeaderText)
            gvParkFarm.Columns(4).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(4).FooterText, gvParkFarm.Columns(4).HeaderText)
            gvParkFarm.Columns(5).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(5).FooterText, gvParkFarm.Columns(5).HeaderText)
            gvParkFarm.Columns(6).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(6).FooterText, gvParkFarm.Columns(6).HeaderText)
            gvParkFarm.Columns(7).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(7).FooterText, gvParkFarm.Columns(7).HeaderText)
            gvParkFarm.Columns(8).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(8).FooterText, gvParkFarm.Columns(8).HeaderText)
            gvParkFarm.Columns(9).HeaderText = Language._Object.getCaption(gvParkFarm.Columns(9).FooterText, gvParkFarm.Columns(9).HeaderText)


            'Step 6
            Me.lblVehicles_step4.Text = Language._Object.getCaption(Me.lblVehicles_step4.ID, Me.lblVehicles_step4.Text)
            Me.colhCarMotorcycle_step5Vehicle.Text = Language._Object.getCaption("colhCarMotorcycle_step5Vehicle", Me.colhCarMotorcycle_step5Vehicle.Text)
            Me.colhCarModel_step5Vehicle.Text = Language._Object.getCaption("colhCarModel_step5Vehicle", Me.colhCarModel_step5Vehicle.Text)
            Me.colhCarColour_step5Vehicle.Text = Language._Object.getCaption("colhCarColour_step5Vehicle", Me.colhCarColour_step5Vehicle.Text)
            Me.colhCarRegNo_step5Vehicle.Text = Language._Object.getCaption("colhCarRegNo_step5Vehicle", Me.colhCarRegNo_step5Vehicle.Text)
            Me.colhCarUse_step5Vehicle.Text = Language._Object.getCaption("colhCarUse_step5Vehicle", Me.colhCarUse_step5Vehicle.Text)
            Me.colhCarAcqDate_step5Vehicle.Text = Language._Object.getCaption("colhCarAcqDate_step5Vehicle", Me.colhCarAcqDate_step5Vehicle.Text)
            Me.colhValue_step5Vehicle.Text = Language._Object.getCaption("colhValue_step5Vehicle", Me.colhValue_step5Vehicle.Text)
            Me.colhServant_step5Vehicle.Text = Language._Object.getCaption("colhServant_step5Vehicle", Me.colhServant_step5Vehicle.Text)
            Me.colhWifeHusband_step5Vehicle.Text = Language._Object.getCaption("colhWifeHusband_step5Vehicle", Me.colhWifeHusband_step5Vehicle.Text)
            Me.colhChildren_step5Vehicle.Text = Language._Object.getCaption("colhChildren_step5Vehicle", Me.colhChildren_step5Vehicle.Text)

            gvVehicles.Columns(2).HeaderText = Language._Object.getCaption(gvVehicles.Columns(2).FooterText, gvVehicles.Columns(2).HeaderText)
            gvVehicles.Columns(3).HeaderText = Language._Object.getCaption(gvVehicles.Columns(3).FooterText, gvVehicles.Columns(3).HeaderText)
            gvVehicles.Columns(4).HeaderText = Language._Object.getCaption(gvVehicles.Columns(4).FooterText, gvVehicles.Columns(4).HeaderText)
            gvVehicles.Columns(5).HeaderText = Language._Object.getCaption(gvVehicles.Columns(5).FooterText, gvVehicles.Columns(5).HeaderText)
            gvVehicles.Columns(6).HeaderText = Language._Object.getCaption(gvVehicles.Columns(6).FooterText, gvVehicles.Columns(6).HeaderText)
            gvVehicles.Columns(7).HeaderText = Language._Object.getCaption(gvVehicles.Columns(7).FooterText, gvVehicles.Columns(7).HeaderText)
            gvVehicles.Columns(8).HeaderText = Language._Object.getCaption(gvVehicles.Columns(8).FooterText, gvVehicles.Columns(8).HeaderText)
            gvVehicles.Columns(9).HeaderText = Language._Object.getCaption(gvVehicles.Columns(9).FooterText, gvVehicles.Columns(9).HeaderText)
            gvVehicles.Columns(10).HeaderText = Language._Object.getCaption(gvVehicles.Columns(10).FooterText, gvVehicles.Columns(10).HeaderText)
            gvVehicles.Columns(11).HeaderText = Language._Object.getCaption(gvVehicles.Columns(11).FooterText, gvVehicles.Columns(11).HeaderText)

            'Step 7
            Me.lblGrindingMachine.Text = Language._Object.getCaption(Me.lblGrindingMachine.ID, Me.lblGrindingMachine.Text)
            Me.colhMachine_step5Machinery.Text = Language._Object.getCaption("colhMachine_step5Machinery", Me.colhMachine_step5Machinery.Text)
            Me.colhLocation_step5Machinery.Text = Language._Object.getCaption("colhLocation_step5Machinery", Me.colhLocation_step5Machinery.Text)
            Me.colhValue_step5Machinery.Text = Language._Object.getCaption("colhValue_step5Machinery", Me.colhValue_step5Machinery.Text)
            Me.colhServant_step5Machinery.Text = Language._Object.getCaption("colhServant_step5Machinery", Me.colhServant_step5Machinery.Text)
            Me.colhWifeHusband_step5Machinery.Text = Language._Object.getCaption("colhWifeHusband_step5Machinery", Me.colhWifeHusband_step5Machinery.Text)
            Me.colhChildren_step5Machinery.Text = Language._Object.getCaption("colhChildren_step5Machinery", Me.colhChildren_step5Machinery.Text)

            gvMachinery.Columns(2).HeaderText = Language._Object.getCaption(gvMachinery.Columns(2).FooterText, gvMachinery.Columns(2).HeaderText)
            gvMachinery.Columns(3).HeaderText = Language._Object.getCaption(gvMachinery.Columns(3).FooterText, gvMachinery.Columns(3).HeaderText)
            gvMachinery.Columns(4).HeaderText = Language._Object.getCaption(gvMachinery.Columns(4).FooterText, gvMachinery.Columns(4).HeaderText)
            gvMachinery.Columns(5).HeaderText = Language._Object.getCaption(gvMachinery.Columns(5).FooterText, gvMachinery.Columns(5).HeaderText)
            gvMachinery.Columns(6).HeaderText = Language._Object.getCaption(gvMachinery.Columns(6).FooterText, gvMachinery.Columns(6).HeaderText)
            gvMachinery.Columns(7).HeaderText = Language._Object.getCaption(gvMachinery.Columns(7).FooterText, gvMachinery.Columns(7).HeaderText)
            gvMachinery.Columns(8).HeaderText = Language._Object.getCaption(gvMachinery.Columns(8).FooterText, gvMachinery.Columns(8).HeaderText)

            'Step 8
            Me.lblOtherBusiness_step5.Text = Language._Object.getCaption(Me.lblOtherBusiness_step5.ID, Me.lblOtherBusiness_step5.Text)
            Me.colhBusinessType_step6OtherBusiness.Text = Language._Object.getCaption("colhBusinessType_step6OtherBusiness", Me.colhBusinessType_step6OtherBusiness.Text)
            Me.colhPlaceClad_step6OtherBusiness.Text = Language._Object.getCaption("colhPlaceClad_step6OtherBusiness", Me.colhPlaceClad_step6OtherBusiness.Text)
            Me.colhServant_step6OtherBusiness.Text = Language._Object.getCaption("colhServant_step6OtherBusiness", Me.colhServant_step6OtherBusiness.Text)
            Me.colhWifeHusband_step6OtherBusiness.Text = Language._Object.getCaption("colhWifeHusband_step6OtherBusiness", Me.colhWifeHusband_step6OtherBusiness.Text)
            Me.colhChildren_step6OtherBusiness.Text = Language._Object.getCaption("colhChildren_step6OtherBusiness", Me.colhChildren_step6OtherBusiness.Text)
            Me.colhValue_step6OtherBusiness.Text = Language._Object.getCaption("colhValue_step6OtherBusiness", Me.colhValue_step6OtherBusiness.Text)

            gvOtherBusiness.Columns(2).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(2).FooterText, gvOtherBusiness.Columns(2).HeaderText)
            gvOtherBusiness.Columns(3).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(3).FooterText, gvOtherBusiness.Columns(3).HeaderText)
            gvOtherBusiness.Columns(4).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(4).FooterText, gvOtherBusiness.Columns(4).HeaderText)
            gvOtherBusiness.Columns(5).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(5).FooterText, gvOtherBusiness.Columns(5).HeaderText)
            gvOtherBusiness.Columns(6).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(6).FooterText, gvOtherBusiness.Columns(6).HeaderText)
            gvOtherBusiness.Columns(7).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(7).FooterText, gvOtherBusiness.Columns(7).HeaderText)
            gvOtherBusiness.Columns(8).HeaderText = Language._Object.getCaption(gvOtherBusiness.Columns(8).FooterText, gvOtherBusiness.Columns(8).HeaderText)

            'Step 9
            Me.lblResources_step5.Text = Language._Object.getCaption(Me.lblResources_step5.ID, Me.lblResources_step5.Text)
            Me.colhResources_step6Resources.Text = Language._Object.getCaption("colhResources_step6Resources", Me.colhResources_step6Resources.Text)
            Me.colhLocation_step6Resources.Text = Language._Object.getCaption("colhLocation_step6Resources", Me.colhLocation_step6Resources.Text)
            Me.colhValue_step6Resources.Text = Language._Object.getCaption("colhValue_step6Resources", Me.colhValue_step6Resources.Text)
            Me.colhEmployee_step6Resources.Text = Language._Object.getCaption("colhEmployee_step6Resources", Me.colhEmployee_step6Resources.Text)
            Me.colhWifeHusband_step6Resources.Text = Language._Object.getCaption("colhWifeHusband_step6Resources", Me.colhWifeHusband_step6Resources.Text)
            Me.colhChildren_step6Resources.Text = Language._Object.getCaption("colhChildren_step6Resources", Me.colhChildren_step6Resources.Text)

            gvResources.Columns(2).HeaderText = Language._Object.getCaption(gvResources.Columns(2).FooterText, gvResources.Columns(2).HeaderText)
            gvResources.Columns(3).HeaderText = Language._Object.getCaption(gvResources.Columns(3).FooterText, gvResources.Columns(3).HeaderText)
            gvResources.Columns(4).HeaderText = Language._Object.getCaption(gvResources.Columns(4).FooterText, gvResources.Columns(4).HeaderText)
            gvResources.Columns(5).HeaderText = Language._Object.getCaption(gvResources.Columns(5).FooterText, gvResources.Columns(5).HeaderText)
            gvResources.Columns(6).HeaderText = Language._Object.getCaption(gvResources.Columns(6).FooterText, gvResources.Columns(6).HeaderText)
            gvResources.Columns(7).HeaderText = Language._Object.getCaption(gvResources.Columns(7).FooterText, gvResources.Columns(7).HeaderText)
            gvResources.Columns(8).HeaderText = Language._Object.getCaption(gvResources.Columns(8).FooterText, gvResources.Columns(8).HeaderText)

            'Step 10
            Me.lblDebt_step6.Text = Language._Object.getCaption(Me.lblDebt_step6.ID, Me.lblDebt_step6.Text)
            Me.colhDebts_step6Debts.Text = Language._Object.getCaption("colhDebts_step6Debts", Me.colhDebts_step6Debts.Text)
            Me.colhLocation_step6Debts.Text = Language._Object.getCaption("colhLocation_step6Debts", Me.colhLocation_step6Debts.Text)
            Me.colhValue_step6Debts.Text = Language._Object.getCaption("colhValue_step6Debts", Me.colhValue_step6Debts.Text)
            Me.colhEmployee_step6Debts.Text = Language._Object.getCaption("colhEmployee_step6Debts", Me.colhEmployee_step6Debts.Text)
            Me.colhWifeHusband_step6Debts.Text = Language._Object.getCaption("colhWifeHusband_step6Debts", Me.colhWifeHusband_step6Debts.Text)
            Me.colhChildren_step6Debts.Text = Language._Object.getCaption("colhChildren_step6Debts", Me.colhChildren_step6Debts.Text)

            gvDebt.Columns(2).HeaderText = Language._Object.getCaption(gvDebt.Columns(2).FooterText, gvDebt.Columns(2).HeaderText)
            gvDebt.Columns(3).HeaderText = Language._Object.getCaption(gvDebt.Columns(3).FooterText, gvDebt.Columns(3).HeaderText)
            gvDebt.Columns(4).HeaderText = Language._Object.getCaption(gvDebt.Columns(4).FooterText, gvDebt.Columns(4).HeaderText)
            gvDebt.Columns(5).HeaderText = Language._Object.getCaption(gvDebt.Columns(5).FooterText, gvDebt.Columns(5).HeaderText)
            gvDebt.Columns(6).HeaderText = Language._Object.getCaption(gvDebt.Columns(6).FooterText, gvDebt.Columns(6).HeaderText)
            gvDebt.Columns(7).HeaderText = Language._Object.getCaption(gvDebt.Columns(7).FooterText, gvDebt.Columns(7).HeaderText)
            gvDebt.Columns(8).HeaderText = Language._Object.getCaption(gvDebt.Columns(8).FooterText, gvDebt.Columns(8).HeaderText)

            Me.lblExchangeRate.Text = Language._Object.getCaption(Me.lblExchangeRate.ID, Me.lblExchangeRate.Text)



        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage " + Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this transaction?")
			Language.setMessage(mstrModuleName, 6, "After Final Save You can not Edit / Delete Asset Declaration." & vbCrLf & vbCrLf & "Are you sure you want to Final Save this Asset Declaration?")
			Language.setMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved.")
			Language.setMessage(mstrModuleName, 8, "Are you sure you want to Unlock Final Save this Asset Declaration?")
			Language.setMessage(mstrModuleName, 9, "This Asset Declaration Final Saved Successfully.")
			Language.setMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully.")
			Language.setMessage(mstrModuleName, 11, "Sorry! You can not Unlock Final Save. Reason: This transaction is not Final Saved.")
			Language.setMessage(mstrModuleName, 12, "Problem in deleting information.")
			Language.setMessage(mstrModuleName, 13, "Sorry! Total amount of respective banks should match with cash in existing bank.")
			Language.setMessage(mstrModuleName, 14, "Non Official Declaration")
			Language.setMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction.")
                        Language.setMessage(mstrModuleName1, 91, "Sorry, you cannot do your declaration as last date of declaration has been passed.")
                        Language.setMessage(mstrModuleName1, 92, "Please contact your administrator/manager to do futher operation on it.")
            Language.setMessage(mstrModuleName1, 16, "Are you sure you want to delete selected Current Assets?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
