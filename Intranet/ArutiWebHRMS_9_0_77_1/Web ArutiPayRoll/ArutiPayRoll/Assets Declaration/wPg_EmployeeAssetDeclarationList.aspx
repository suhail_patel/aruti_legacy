﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EmployeeAssetDeclarationList.aspx.vb" Inherits="HR_wPg_EmployeeAssetDeclarationList"
    Title="Employee Asset Declaration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

//function pageLoad(sender, args) {
//    $("select").searchable();

//    $('#imgclose').click(function(evt) {
//        $find('<%= popupAddEdit.ClientID %>').hide();
//    });
    //}
    function closePopup() {
    $('#imgclose').click(function(evt) {
        $find('<%= popupAddEdit.ClientID %>').hide();
    });
}
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
                if (cval.indexOf(".") > -1)
                    return false;
        
        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }

//    function onlyNumbers($char, $mozChar,txtBox) {
//        //var e = event || evt; // for trans-browser compatibility
//        //var charCode = e.which || e.keyCode;
//        var charCode
//        if ($mozChar != null) {
//            charCode = $mozChar;
//        }
//        else {
//            charCode = $char;
//        }
//        
//        //var cval = document.getElementById(txtBoxId).value;
//        var cval = txtBox.value;

//        if (cval.length > 0)
//            if (charCode == 46)
//                if (cval.indexOf(".") > -1)
//                    return false;

//        if (charCode > 31 && (charCode < 46 || charCode > 57))
//            return false;

//        return true;
//    }    

    function GetInstruct(str) {
        var dv = document.getElementById("divInstruction");
        if (dv != null && str != null)
            dv.innerHTML = str.toString();
    }
    </script>

    <%--
    <div>
        <asp:Panel ID="MainPan" runat="server">
            <uc1:Closebutton ID="Closebutton1" runat="server" PageHeading="" />
            <div class="container_outer">
                <div class="container">
                    <ul class="tablecontainer">
                        <li class="col" style="width: 70%;"></li>
                        <li class="col" style="margin-right: 0; width: 23%; text-align: right;">
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </li>
                        <li class="col" style="width: 95%; margin-left: 5px;"></li>
                        <li class="btnbar"></li>
                    </ul>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="" Width="800px" Height="550px" runat="server" Style="display: none;
            background-color: #FFFFFF; border-style: solid; border-width: thin; margin-left: auto;
            margin-right: auto;">
            <img id="imgclose" alt="" src="../images/icon_close.gif" class="rightcorner" />
            <div id="divAddEdit" style="height: 500px; margin-left: 25px; margin-right: 25px;
                margin-top: 25px; border-width: 1px; border-style: solid;">
                <asp:Panel ID="pnlInnerAddEdit" Height="500px" runat="server" Style="background-color: #FFFFFF;">
                    <div style="height: 457px;">
                        <asp:Panel ID="pnlMove" runat="server" Style="background-color: #FFFFFF; margin: 0px;
                            padding: 0px;">
                        </asp:Panel>
                    </div>
                </asp:Panel>
            </div>
        </asp:Panel>
    </div>--%>
    <center>
        <asp:Panel ID="Panel2" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Asset Declaration List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 75%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="False">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:30%">
                                                <asp:CheckBox ID="chkIncludeClosedYearTrans" runat="server" Text="Include Closed Year Transactions" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%">
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Panel ID="pbl_gvList" runat="server" ScrollBars="Auto" Width="100%" Height="350px">
                                                    <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetdeclarationunkid">
                                                        <RowStyle BorderStyle="Solid" BorderWidth="1px" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                    </span>
                                                                    <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName="Change"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="btnGvRemove" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                    </span>
                                                                    <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName="Remove"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnGvFinalSave" runat="server" CausesValidation="False" CommandName="FinalSave"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/Save.png" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Unlock Final Save">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <%--<span class="gridiconbc">
                                                                        <asp:LinkButton ID="btnGvUnlockFinalSave" runat="server" CssClass="gridiconunlock" CausesValidation="false"
                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="UnlockFinalSave"></asp:LinkButton>
                                                                    </span>--%>
                                                                    <asp:ImageButton ID="btnGvUnlockFinalSave" runat="server" CausesValidation="False"
                                                                        CommandName="UnlockFinalSave" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        ImageUrl="~/Images/unlock.png" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Preview">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="btnView3" runat="server" CausesValidation="False" CommandName="Print"
                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/PrintSummary_16.png" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="noofemployment" HeaderText="Employee Code" ReadOnly="True"
                                                                FooterText="colhNoOfEmployment">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee Name" ReadOnly="True"
                                                                FooterText="colhEmployee">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="position" HeaderText="Office / Position" ReadOnly="True"
                                                                FooterText="colhPosition">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="centerforwork" HeaderText="Department" ReadOnly="True"
                                                                FooterText="colhCenterForWork">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="yearname" HeaderText="Year" ReadOnly="True"
                                                                FooterText="colhYearName">
                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                <ItemStyle HorizontalAlign="Left" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <%--<asp:Button ID="btnScanAttachDocuments" runat="server" Text="Scan / Attach Document"
                                            CssClass="btnDefault" />--%>
                                        <asp:Button ID="btnScanAttachDocuments" runat="server" Text="Browse"
                                            CssClass="btnDefault" />
                                        <asp:Button ID="BtnNew" runat="server" Text="New" CssClass="btnDefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                        <asp:HiddenField ID="btnHidden1" runat="Server" />
                                        <asp:HiddenField ID="btnHidden2" runat="Server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupAddEdit" runat="server" TargetControlID="btnHidden"
                        PopupControlID="pnlAddEdit" BackgroundCssClass="ModalPopupBG" DropShadow="false"
                        CancelControlID="hdf_popupAddEdit" PopupDragHandleControlID="divBottom" Enabled="True">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="newpopup" Style="display: none;
                        width: 800px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-body">
                                <div id="Div2" class="panel-default" style="margin: 0px">
                                    <div id="Div5" class="panel-body-default">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:MultiView ID="mvwAssetDeclaration" runat="server" ActiveViewIndex="0">
                                                    <asp:View ID="vwStep1" runat="server">
                                                        <div class="panel-heading">
                                                            <asp:Label ID="lblInstruction" runat="server" Text="Instructions"> </asp:Label>
                                                        </div>
                                                        <div class="panel-body">
                                                            <div id="divInstruction" style="width: 100%; height: 220px; overflow: auto;">
                                                            </div>
                                                            <div id="Div4" class="panel-default" style="margin: 0px">
                                                                <div id="Div6" class="panel-heading-default">
                                                                    <div style="float: left;">
                                                                        <asp:Label ID="gbEmployee" runat="server" Text="Employee Selection" />
                                                                    </div>
                                                                </div>
                                                                <div id="Div7" class="panel-body-default">
                                                                    <table style="width: 100%">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="lblStep1_Employee" runat="server" Text="Employee"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80%">
                                                                                <asp:DropDownList ID="drpEmpListAddEdit" runat="server" AutoPostBack="False" Width="250px">
                                                                                </asp:DropDownList>
                                                                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="lblDate" runat="server" Text="Transaction Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80%">
                                                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="lblStep1_NoOfEmployment" runat="server" Text="Employee Code"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80%">
                                                                                <asp:Label ID="txtEmployeeCode" runat="server" Font-Bold="True" Text="1"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="lblOfficePosition" runat="server" Text="Office/Position"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80%">
                                                                                <asp:Label ID="txtOfficePosition" runat="server" Font-Bold="True" Text="General Manager"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="lblCenterforWork" runat="server" Text="Department"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80%">
                                                                                <asp:Label ID="txtDepartment" runat="server" Font-Bold="True" Text="Finance and Administration"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="lblWorkStation" runat="server" Text="Work Station"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 80%">
                                                                                <asp:Label ID="txtWorkStation" runat="server" Font-Bold="True" Text="Class1"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep2" runat="server">
                                                        <%--<div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 218px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel19" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="btnAddBank" EventName="Click" />
                                                                        <asp:AsyncPostBackTrigger ControlID="btnUpdateBank" EventName="Click" />
                                                                        <asp:AsyncPostBackTrigger ControlID="btnResetBank" EventName="Click" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblFinanceToDate_Step2" runat="server" Text="Current Assets"></asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div8" class="panel-default">
                                                                    <div id="Div10" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%; margin-bottom: 10px;">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="lblCash_Step2" runat="server" Text="Cash"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtCash" runat="server" Width="153px" Style="margin-bottom: 2px;"
                                                                                        CssClass="RightTextAlign" onkeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="lblCashExistingBank_Step2" runat="server" Text="Cash Balances in Existing Banks or Financial Institutions"></asp:Label></label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtCashExistingBank" runat="server" Width="153px" Style="margin-bottom: 2px;"
                                                                                        CssClass="RightTextAlign" onkeypress="return onlyNumbers(this, event);" ReadOnly="true"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%; margin-bottom: 10px;">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhBank_step3" runat="server" Text="Bank / Financial Institution"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtBank" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtBank" ErrorMessage="Bank Information can not be Blank. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Bank"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhServant_step3" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtServantBank" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtServantBank"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper percentage. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Bank"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%; margin-bottom: 10px;">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhAccount_step3" runat="server" Text="Account Number"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtAccountNumber" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtAccountNumber" ErrorMessage="Account Number can not be Blank. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Bank"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step3" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtWifeHusbandBank" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtWifeHusbandBank"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper percentage. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Bank"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%; margin-bottom: 10px;">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhAmount_step3" runat="server" Text="Amount"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtAmountBank" runat="server" Style="margin-right: 5px;" Width="153px"
                                                                                            CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtAmountBank" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                            ForeColor="White" Style="z-index: 1000" ValidationGroup="Bank" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyBank" runat="server" Width="50px" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblErrorCBank" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step3" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtChildrenBank" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorBank" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtChildrenBank"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper percentage. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Bank"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="BtnAddBank" runat="server" Text="Add" CssClass="btnDefault" ValidationGroup="Bank" />
                                                                            <asp:Button ID="BtnUpdateBank" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="Bank" Enabled="false" />
                                                                            <asp:Button ID="btnResetBank" runat="server" Text="Reset" CssClass="btnDefault" ValidationGroup="Bank"
                                                                                CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="pnl_" runat="server" Width="100%" Height="218px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvBank" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetbanktranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName=""
                                                                                            CommandArgument="" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="" runat="server" CausesValidation="False" CommandName=""
                                                                                            CommandArgument="" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="bank" HeaderText="Bank / Financial Institution" ReadOnly="True"
                                                                                FooterText="colhBank_step3">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="account" HeaderText="Account" ReadOnly="True" FooterText="colhAccount_step3">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" FooterText="colhAmount_step3">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyBank">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step3">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step3">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step3">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep3" runat="server">
                                                        <%--<div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddShareDividend" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateShareDividend" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetShareDividend" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 255px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel20" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblShareDividend_step2" runat="server" Text="Shares and Dividends from Shares" />
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div9" class="panel-default">
                                                                    <div id="Div12" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhShareValue_step3" runat="server" Text="Value of Shares"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%;">
                                                                                    <asp:TextBox ID="txtShareValue" runat="server" Width="153px" CssClass="RightTextAlign"
                                                                                        onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtShareValue" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="ShareDividend" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%;">
                                                                                    <asp:Label ID="colhServant_step3Shares" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%;">
                                                                                    <asp:TextBox ID="txtServantShareDividend" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtServantShareDividend"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ShareDividend"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%;">
                                                                                    <asp:Label ID="colhLocation_step3" runat="server" Text="Source"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%;">
                                                                                    <asp:TextBox ID="txtLocationShareDividend" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtLocationShareDividend" ErrorMessage="Please enter source. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="ShareDividend"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%;">
                                                                                    <asp:Label ID="colhWifeHusband_step3Shares" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%;">
                                                                                    <asp:TextBox ID="txtWifeHusbandShareDividend" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtWifeHusbandShareDividend"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ShareDividend"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%;">
                                                                                    <asp:Label ID="colhDividendAmount_step3" runat="server" Text="Dividend Amount"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%;">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtDividendAmount" runat="server" Style="margin-right: 5px;" Width="153px"
                                                                                            CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtDividendAmount" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                            ForeColor="White" Style="z-index: 1000" ValidationGroup="ShareDividend" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyShare" AutoPostBack="true" runat="server" Width="50px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblErrorCShareDividend" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%;">
                                                                                    <asp:Label ID="colhChildren_step3Shares" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%;">
                                                                                    <asp:TextBox ID="txtChildrenShareDividend" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorShareDividend" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="txtChildrenShareDividend"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ShareDividend"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddShareDividend" runat="server" Text="Add" CssClass="btnDefault"
                                                                                ValidationGroup="ShareDividend" />
                                                                            <asp:Button ID="btnUpdateShareDividend" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="ShareDividend" Enabled="false" />
                                                                            <asp:Button ID="btnResetShareDividend" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="ShareDividend" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                    <asp:Panel ID="Panel3" runat="server" Width="100%" Height="255px" ScrollBars="Auto">
                                                                        <asp:GridView ID="gvShareDividend" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetsharedividendtranunkid,countryunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="Edit">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                                CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                        </span>
                                                                                        <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Delete">
                                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                                CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                        </span>
                                                                                        <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="sharevalue" HeaderText="Share Value" ReadOnly="True" FooterText="colhShareValue_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="location" HeaderText="Source" ReadOnly="True" FooterText="colhLocation_step3">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="dividendamount" HeaderText="Dividend Amount" ReadOnly="True"
                                                                                    FooterText="colhDividendAmount_step3">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyShare">
                                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step3Shares">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                    FooterText="colhWifeHusband_step3Shares">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step3Shares">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep4" runat="server">
                                                        <%-- <div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddHomeBuilding" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateHomeBuilding" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetHomeBuilding" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 255px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel21" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblHouseBuilding_step3" runat="server" Text="Houses and Other Buildings"> </asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div11" class="panel-default">
                                                                    <div id="Div14" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhHomesBuilding_step4" runat="server" Text="Home/Buildings/ Others"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtHomeBuilding" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtHomeBuilding" ErrorMessage="Please enter detail. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="HomeBuilding" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhServant_step4House" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtServantHomeBuilding" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="txtServantHomeBuilding"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="HomeBuilding"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhLocation_step4" runat="server" Text="Location"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtLocationHomeBuilding" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtLocationHomeBuilding" ErrorMessage="Please enter location. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="HomeBuilding"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step4House" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtWifeHusbundHomeBuilding" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator11" runat="server" ControlToValidate="txtWifeHusbundHomeBuilding"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="HomeBuilding"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhValue_step4" runat="server" Text="Value"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtValueHomeBuilding" runat="server" Style="margin-right: 5px;"
                                                                                            Width="153px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtValueHomeBuilding" ErrorMessage="Please enter amount. "
                                                                                            CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="HomeBuilding"
                                                                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyHouse" AutoPostBack="true" runat="server" Width="50px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblErrorCHouseBuilding" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step4House" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtChildrenHomeBuilding" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorHouseBuilding" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="txtChildrenHomeBuilding"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="HomeBuilding"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddHomeBuilding" runat="server" Text="Add" CssClass="btnDefault"
                                                                                ValidationGroup="HomeBuilding" />
                                                                            <asp:Button ID="btnUpdateHomeBuilding" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="HomeBuilding" Enabled="false" />
                                                                            <asp:Button ID="btnResetHomeBuilding" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="HomeBuilding" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel5" runat="server" Width="100%" Height="218px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvHouseBuilding" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assethousebuildingtranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="homebuilding" HeaderText="Home/ Buildings/ Others" ReadOnly="True"
                                                                                FooterText="colhHomesBuilding_step4">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step4">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step4">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyHouse">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step4House">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step4House">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step4House">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep5" runat="server">
                                                        <%--<div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel14" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddParkFarmMine" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateParkFarmMine" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetParkFarmMine" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 230px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel22" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblParkFarm_step3" runat="server" Text="Parks, Farms and Mines"></asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div13" class="panel-default">
                                                                    <div id="Div16" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhParkFarmMines_step4Parks" runat="server" Text="Plots, Farms and Mines"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtParkFarmsMines" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtParkFarmsMines" ErrorMessage="Please enter detail. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="ParkFarmMine" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhServant_step4Parks" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtServantParkFarmMine" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator15" runat="server" ControlToValidate="txtServantParkFarmMine"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ParkFarmMine"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhAreaSize_step4Parks" runat="server" Text="Size of Area"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtSizeofAreaParkFarmMine" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtSizeofAreaParkFarmMine" ErrorMessage="Please enter Size of Area. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="ParkFarmMine"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step4Parks" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtWifeHusbundParkFarmMine" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator16" runat="server" ControlToValidate="txtWifeHusbundParkFarmMine"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ParkFarmMine"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhPlaceClad_step4Parks" runat="server" Text="Location"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtLocationParkFarmMine" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtLocationParkFarmMine" ErrorMessage="Please enter location. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="ParkFarmMine"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step4Parks" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtChildrenParkFarmMine" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorParkFarmMine" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator17" runat="server" ControlToValidate="txtChildrenParkFarmMine"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="ParkFarmMine"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhValue_step4Parks" runat="server" Text="Value"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtValueParkFarmMine" runat="server" Style="float: left; margin-right: 5px;"
                                                                                            Width="153px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtValueParkFarmMine" ErrorMessage="Please enter amount. "
                                                                                            CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="ParkFarmMine"
                                                                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyPark" AutoPostBack="true" runat="server" Width="50px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblErrorCParkFarmMine" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddParkFarmMine" runat="server" Text="Add" CssClass="btnDefault"
                                                                                ValidationGroup="ParkFarmMine" />
                                                                            <asp:Button ID="btnUpdateParkFarmMine" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="ParkFarmMine" Enabled="false" />
                                                                            <asp:Button ID="btnResetParkFarmMine" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="ParkFarmMine" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel6" runat="server" Width="100%" Height="230px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvParkFarm" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetparkfarmminestranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%-- <asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="parkfarmmines" HeaderText="Plots, Farms and Mines" ReadOnly="True"
                                                                                FooterText="colhParkFarmMines_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="sizeofarea" HeaderText="Size of area" ReadOnly="True"
                                                                                FooterText="colhAreaSize_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="placeclad" HeaderText="Location" ReadOnly="True" FooterText="colhPlaceClad_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyPark">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step4Parks">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep6" runat="server">
                                                        <%--<div style="height: 457px;">
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddVehicle" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateVehicle" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetVehicle" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 200px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel23" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblVehicles_step4" runat="server" Text="Vehicle and Other Transport Equipment"></asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div15" class="panel-default">
                                                                    <div id="Div18" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhCarMotorcycle_step5Vehicle" runat="server" Text="Type and Name of Car / Motorcycle"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtVehicleType" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtVehicleType" ErrorMessage="Please enter detail. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhCarAcqDate_step5Vehicle" runat="server" Text="Acquistion Date"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <uc2:DateCtrl ID="dtpAcquistionDate" runat="server" AutoPostBack="False" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhCarModel_step5Vehicle" runat="server" Text="Model"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtModel" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtModel" ErrorMessage="Please enter model. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhValue_step5Vehicle" runat="server" Text="Value"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtValueVehicle" runat="server" Style="float: left; margin-right: 5px;"
                                                                                            Width="110px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtValueVehicle" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                            ForeColor="White" Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyVehicle" AutoPostBack="true" runat="server" Width="50px" />
                                                                                        <asp:Label ID="lblErrorCVehicle" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhCarColour_step5Vehicle" runat="server" Text="Colour"></asp:Label></label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtColour" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtColour" ErrorMessage="Please enter colour. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhServant_step5Vehicle" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtServantVehicle" runat="server" Width="110px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator19" runat="server" ControlToValidate="txtServantVehicle"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Vehicle"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhCarRegNo_step5Vehicle" runat="server" Text="Reg.No"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtRegNo" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtRegNo" ErrorMessage="Please enter Reg.No. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step5Vehicle" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtWifeHusbundVehicle" runat="server" Width="110px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator20" runat="server" ControlToValidate="txtWifeHusbundVehicle"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Vehicle"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhCarUse_step5Vehicle" runat="server" Text="Vehicle Use"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtVehicleUse" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtVehicleUse" ErrorMessage="Please enter Vehical use. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Vehicle" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step5Vehicle" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtChildrenVehicle" runat="server" Width="110px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorVehicle" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator21" runat="server" ControlToValidate="txtChildrenVehicle"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Vehicle"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddVehicle" runat="server" Text="Add" CssClass="btnDefault" ValidationGroup="Vehicle" />
                                                                            <asp:Button ID="btnUpdateVehicle" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="Vehicle" Enabled="false" />
                                                                            <asp:Button ID="btnResetVehicle" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="Vehicle" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel7" runat="server" Width="100%" Height="200px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvVehicles" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetvehiclestranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="vehicletype" HeaderText="Type and Name of Car / Motorcycle"
                                                                                ReadOnly="True" FooterText="colhCarMotorcycle_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="model" HeaderText="Model" ReadOnly="True" FooterText="colhCarModel_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="colour" HeaderText="Colour" ReadOnly="True" FooterText="colhCarColour_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="regno" HeaderText="Reg.No" ReadOnly="True" FooterText="colhCarRegNo_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="vehicleuse" HeaderText="Vehicle Use" ReadOnly="True" FooterText="colhCarUse_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyVehicle">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step5Vehicle">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep7" runat="server">
                                                        <%-- <div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                        </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddMachinery" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateMachinery" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetMachinery" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 220px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel24" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblGrindingMachine" runat="server" Text="Machinery for Grinding Grain, Industries/Factories and Other"> </asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div17" class="panel-default">
                                                                    <div id="Div20" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhMachine_step5Machinery" runat="server" Text="Grinding Machine, Industries/Factories, Other Machines"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtMachines" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtMachines" ErrorMessage="Please enter detail. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Machinery" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhServant_step5Machinery" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtServantMachinery" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator23" runat="server" ControlToValidate="txtServantMachinery"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Machinery"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhLocation_step5Machinery" runat="server" Text="Location"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtLocationMachinery" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtLocationMachinery" ErrorMessage="Please enter location. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Machinery"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step5Machinery" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtWifeHusbundMachinery" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator24" runat="server" ControlToValidate="txtWifeHusbundMachinery"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Machinery"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhValue_step5Machinery" runat="server" Text="Value"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtValueMachinery" runat="server" Style="margin-right: 5px;" Width="153px"
                                                                                            CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtValueMachinery" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                            ForeColor="White" Style="z-index: 1000" ValidationGroup="Machinery" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyMachine" AutoPostBack="true" runat="server" Width="50px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblErrorCMachinery" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step5Machinery" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtChildrenMachinery" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorMachinery" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator25" runat="server" ControlToValidate="txtChildrenMachinery"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Machinery"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddMachinery" runat="server" Text="Add" CssClass="btnDefault"
                                                                                ValidationGroup="Machinery" />
                                                                            <asp:Button ID="btnUpdateMachinery" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="Machinery" Enabled="false" />
                                                                            <asp:Button ID="btnResetMachinery" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="Machinery" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel8" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvMachinery" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetmachinerytranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="machines" HeaderText="Grinding Machine, Industries/Factories, Other Machines"
                                                                                ReadOnly="True" FooterText="colhMachine_step5Machinery">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step5Machinery">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step5Machinery">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyMachine">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step5Machinery">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step5Machinery">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step5Machinery">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep8" runat="server">
                                                        <%--<div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddOtherBusiness" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateOtherBusiness" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetOtherBusiness" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 260px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel25" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblOtherBusiness_step5" runat="server" Text="Other Businesses (Shops, Bars, Lodges etc.)"></asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div19" class="panel-default">
                                                                    <div id="Div22" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhBusinessType_step6OtherBusiness" runat="server" Text="Type of Business"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtBusinessType" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtBusinessType" ErrorMessage="Please enter Type of Business. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="OtherBusiness"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhServant_step6OtherBusiness" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtServantOtherBusiness" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator27" runat="server" ControlToValidate="txtServantOtherBusiness"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="OtherBusiness"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhPlaceClad_step6OtherBusiness" runat="server" Text="Location"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtLocationOtherBusiness" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtLocationOtherBusiness" ErrorMessage="Please enter Location. "
                                                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="OtherBusiness"
                                                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step6OtherBusiness" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtWifeHusbundOtherBusiness" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator28" runat="server" ControlToValidate="txtWifeHusbundOtherBusiness"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="OtherBusiness"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhValue_step6OtherBusiness" runat="server" Text="Value"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtValueOtherBusiness" runat="server" Style="margin-right: 5px;"
                                                                                            Width="153px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtValueOtherBusiness" ErrorMessage="Please enter amount. "
                                                                                            CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="OtherBusiness"
                                                                                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyBusiness" AutoPostBack="true" runat="server" Width="50px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="lblErrorCOtherBusiness" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step6OtherBusiness" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtChildrenOtherBusiness" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="lblErrorOtherBusiness" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator29" runat="server" ControlToValidate="txtChildrenOtherBusiness"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="OtherBusiness"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddOtherBusiness" runat="server" Text="Add" CssClass="btnDefault"
                                                                                ValidationGroup="OtherBusiness" />
                                                                            <asp:Button ID="btnUpdateOtherBusiness" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="OtherBusiness" Enabled="false" />
                                                                            <asp:Button ID="btnResetOtherBusiness" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="OtherBusiness" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel9" runat="server" Width="100%" Height="260px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvOtherBusiness" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetotherbusinesstranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="businesstype" HeaderText="Type of Business" ReadOnly="True"
                                                                                FooterText="colhBusinessType_step6OtherBusiness">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="placeclad" HeaderText="Place clad" ReadOnly="True" FooterText="colhPlaceClad_step6OtherBusiness">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step6OtherBusiness">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyBusiness">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhServant_step6OtherBusiness">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step6OtherBusiness">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step6OtherBusiness">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep9" runat="server">
                                                        <%--<div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                    </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel18" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddResource" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateResource" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetResource" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 229px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblResources_step5" runat="server" Text="Resources or other Commercial Interests including livestock"></asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div21" class="panel-default">
                                                                    <table style="width: 100%" cellpadding="3">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="colhResources_step6Resources" runat="server" Text="Resources / Commercial Interests"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:TextBox ID="txtResources" runat="server" Width="153px"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" Display="Dynamic"
                                                                                    ControlToValidate="txtResources" ErrorMessage="Please enter Resources / Commercial Interests. "
                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Resources"
                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="colhEmployee_step6Resources" runat="server" Text="Employee (%)"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                                <asp:TextBox ID="txtResourcesEmp" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtResourcesEmp"
                                                                                    CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                    ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Resources"
                                                                                    SetFocusOnError="True"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="colhLocation_step6Resources" runat="server" Text="Location"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:TextBox ID="txtResourcesLocation" runat="server" Width="153px"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" Display="Dynamic"
                                                                                    ControlToValidate="txtResourcesLocation" ErrorMessage="Please enter Location."
                                                                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" ValidationGroup="Resources"
                                                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="colhWifeHusband_step6Resources" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                                <asp:TextBox ID="txtResourcesHusbandwife" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtResourcesHusbandwife"
                                                                                    CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                    ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Resources"
                                                                                    SetFocusOnError="True"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="colhValue_step6Resources" runat="server" Text="Value"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <div style="float: left">
                                                                                    <asp:TextBox ID="txtResourceValue" runat="server" Style="float: left; margin-right: 5px;"
                                                                                        Width="153px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtResourceValue" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Resources" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </div>
                                                                                <div style="float: left">
                                                                                    <asp:DropDownList ID="drpCurrencyResources" AutoPostBack="true" runat="server" Width="50px">
                                                                                    </asp:DropDownList>
                                                                                    <asp:Label ID="lblErrorCResources" runat="server" Text="" CssClass="ErrorControl" />
                                                                                </div>
                                                                            </td>
                                                                            <td style="width: 20%">
                                                                                <asp:Label ID="colhChildren_step6Resources" runat="server" Text="Children (%)"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 25%">
                                                                                <asp:TextBox ID="txtResourceChildren" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                <asp:Label ID="LblErrorResources" runat="server" Text="" CssClass="ErrorControl" />
                                                                                <asp:RangeValidator ID="RangeValidator9" runat="server" ControlToValidate="txtResourceChildren"
                                                                                    CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                    ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Resources"
                                                                                    SetFocusOnError="True"></asp:RangeValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <div id="Div24" class="panel-body-default">
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddResource" runat="server" Text="Add" CssClass="btnDefault" ValidationGroup="Resources" />
                                                                            <asp:Button ID="btnUpdateResource" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="Resources" Enabled="false" />
                                                                            <asp:Button ID="btnResetResource" runat="server" Text="Reset" CssClass="btnDefault"
                                                                                ValidationGroup="Resources" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel10" runat="server" Width="100%" Height="229px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvResources" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetotherresourcestranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="otherresources" HeaderText="Resources / Commercial Interests"
                                                                                ReadOnly="True" FooterText="colhResources_step6Resources">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step6Resources">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step6Resources">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrencyResources">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhEmployee_step6Resources">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step6Resources">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step6Resources">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                    <asp:View ID="vwStep10" runat="server">
                                                        <%--<div>
                                                            <ul class="tablecontainer">
                                                                <li class="col" style="width: 98%; margin-right: 0;">
                                                                    <h3 class="heading1">
                                                                        </h3>
                                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                                        <ContentTemplate>
                                                                        </ContentTemplate>
                                                                        <Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnAddDebt" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnUpdateDebt" EventName="Click" />
                                                                            <asp:AsyncPostBackTrigger ControlID="btnResetDebt" EventName="Click" />
                                                                        </Triggers>
                                                                    </asp:UpdatePanel>
                                                                    <div class="btnbar">
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <div style="width: 100%; height: 245px; float: left; overflow: auto;">
                                                                <asp:UpdatePanel ID="UpdatePanel26" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>--%>
                                                        <div class="panel-primary" style="margin-bottom: 0px">
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblDebt_step6" runat="server" Text="Debt"></asp:Label>
                                                            </div>
                                                            <div class="panel-body">
                                                                <div id="Div23" class="panel-default">
                                                                    <div id="Div26" class="panel-body-default">
                                                                        <table style="width: 100%" cellpadding="3">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhDebts_step6Debts" runat="server" Text="Debt"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtDebt" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtDebt" ErrorMessage="Please enter debt. " CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Debt" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhEmployee_step6Debts" runat="server" Text="Employee (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtDebtEmployee" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator13" runat="server" ControlToValidate="txtDebtEmployee"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Debt"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhLocation_step6Debts" runat="server" Text="Location"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <asp:TextBox ID="txtDebtLocation" runat="server" Width="153px"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" Display="Dynamic"
                                                                                        ControlToValidate="txtDebtLocation" ErrorMessage="Please enter Location." CssClass="ErrorControl"
                                                                                        ForeColor="White" Style="z-index: 1000" ValidationGroup="Debt" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhWifeHusband_step6Debts" runat="server" Text="Wife / Husband (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtDebtWifeHusband" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:RangeValidator ID="RangeValidator14" runat="server" ControlToValidate="txtDebtWifeHusband"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Debt"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhValue_step6Debts" runat="server" Text="Value"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 35%">
                                                                                    <div style="float: left">
                                                                                        <asp:TextBox ID="txtDebtValue" runat="server" Style="float: left; margin-right: 5px;"
                                                                                            Width="153px" CssClass="RightTextAlign" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" Display="Dynamic"
                                                                                            ControlToValidate="txtDebtValue" ErrorMessage="Please enter amount. " CssClass="ErrorControl"
                                                                                            ForeColor="White" Style="z-index: 1000" ValidationGroup="Debt" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                    <div style="float: left">
                                                                                        <asp:DropDownList ID="drpCurrencyDebt" AutoPostBack="true" runat="server" Width="50px">
                                                                                        </asp:DropDownList>
                                                                                        <asp:Label ID="LblCErrorDebt" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    </div>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:Label ID="colhChildren_step6Debts" runat="server" Text="Children (%)"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 25%">
                                                                                    <asp:TextBox ID="txtDebtChildren" runat="server" Width="153px" CssClass="RightTextAlign"></asp:TextBox>
                                                                                    <asp:Label ID="LblErrorDebt" runat="server" Text="" CssClass="ErrorControl" />
                                                                                    <asp:RangeValidator ID="RangeValidator18" runat="server" ControlToValidate="txtDebtChildren"
                                                                                        CssClass="ErrorControl" ErrorMessage="Please enter proper amount. " Display="Dynamic"
                                                                                        ForeColor="White" MaximumValue="100" MinimumValue="0" Type="Double" ValidationGroup="Debt"
                                                                                        SetFocusOnError="True"></asp:RangeValidator>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <div class="btn-default">
                                                                            <asp:Button ID="btnAddDebt" runat="server" Text="Add" CssClass="btnDefault" ValidationGroup="Debt" />
                                                                            <asp:Button ID="btnUpdateDebt" runat="server" Text="Update" CssClass="btnDefault"
                                                                                ValidationGroup="Debt" Enabled="false" />
                                                                            <asp:Button ID="btnResetDebt" runat="server" Text="Reset" CssClass="btnDefault" ValidationGroup="Debt"
                                                                                CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <asp:Panel ID="Panel11" runat="server" Width="100%" Height="245px" ScrollBars="Auto">
                                                                    <asp:GridView ID="gvDebt" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="assetdebtstranunkid,countryunkid">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Edit">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView" runat="server" CssClass="gridedit" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Change"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView" runat="server" CausesValidation="False" CommandName="Change"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/edit.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                                <ItemStyle HorizontalAlign="Center" />
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:LinkButton ID="btnView1" runat="server" CssClass="griddelete" CausesValidation="false"
                                                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"></asp:LinkButton>
                                                                                    </span>
                                                                                    <%--<asp:ImageButton ID="btnView1" runat="server" CausesValidation="False" CommandName="Remove"
                                                                                        CommandArgument="<%# Container.DataItemIndex %>" ImageUrl="~/Images/remove.png" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="debts" HeaderText="Debt" ReadOnly="True" FooterText="colhDebts_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="location" HeaderText="Location" ReadOnly="True" FooterText="colhLocation_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="value" HeaderText="Value" ReadOnly="True" FooterText="colhValue_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrency_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="servant" HeaderText="Employee (%)" ReadOnly="True" FooterText="colhEmployee_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="wifehusband" HeaderText="Wife / Husband (%)" ReadOnly="True"
                                                                                FooterText="colhWifeHusband_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="children" HeaderText="Children (%)" ReadOnly="True" FooterText="colhChildren_step6Debts">
                                                                                <HeaderStyle HorizontalAlign="Right" />
                                                                                <ItemStyle HorizontalAlign="Right" />
                                                                            </asp:BoundField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </asp:View>
                                                </asp:MultiView>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnClose" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnNew" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="popup1" EventName="buttonDelReasonYes_Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnSearch" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="BtnReset" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnNext" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnSaveClose" EventName="Click" />
                                                <asp:AsyncPostBackTrigger ControlID="btnBack" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <div class="btn-default">
                                            <div style="float: left">
                                                <b>
                                                    <asp:Label ID="lblExchangeRate" runat="server" Text="" Style="text-align: center;"></asp:Label>
                                                </b>
                                            </div>
                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btnDefault" />
                                            <asp:Button ID="btnNext" runat="server" Text="Next" CssClass="btnDefault" ValidationGroup="Step" />
                                            <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="btnDefault"
                                                ValidationGroup="Step" />
                                            <asp:Button ID="btnCloseAddEdit" runat="server" Text="Close" CssClass="btnDefault"
                                                CausesValidation="false" />
                                                <asp:HiddenField ID="hdf_popupAddEdit" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc8:ConfirmYesNo ID="popupYesNo" runat="server" Title="Are you sure you want to Save this Asset Declaration?"
                        Message="Are you sure you want to Save this Asset Declaration?" />
                    <uc8:ConfirmYesNo ID="popupFinalYesNo" runat="server" Title="Are you sure you want to Final Save this Asset Declaration?"
                        Message="After Final Save You can not Edit / Delete Asset Declaration. Are you sure you want to Final Save this Asset Declaration?" />
                    <uc9:DeleteReason ID="popup1" runat="server" Title="Are ou Sure You Want To delete?:" />
                    <uc8:ConfirmYesNo ID="popupUnlockFinalYesNo" runat="server" Title="Unlock Final Save"
                        Message="Are you sure you want to Unlock Final Save this Asset Declaration?" />
                    <uc9:DeleteReason ID="popupDeleteBank" runat="server" Title="Are you sure you want to delete selected Current Assets?" ValidationGroup="DeleteBank" />
                    <uc9:DeleteReason ID="popupDeleteShareDividend" runat="server" Title="Are you sure you want to delete selected Shares and Dividends?" ValidationGroup="DeleteShareDividend" />
                    <uc9:DeleteReason ID="popupDeleteHouseBuilding" runat="server" Title="Are you sure you want to delete selected House and Other Buildings?" ValidationGroup="DeleteHouseBuilding" />
                    <uc9:DeleteReason ID="popupDeleteParkFarm" runat="server" Title="Are you sure you want to delete selected Parks, Farms and Mines?" ValidationGroup="DeleteParkFarm" />
                    <uc9:DeleteReason ID="popupDeleteVehicles" runat="server" Title="Are you sure you want to delete selected Vehicle and Other Transport Equipment?" ValidationGroup="DeleteVehicles" />
                    <uc9:DeleteReason ID="popupDeleteMachinery" runat="server" Title="Are you sure you want to delete selected Machinery?" ValidationGroup="DeleteMachinery" />
                    <uc9:DeleteReason ID="popupDeleteOtherBusiness" runat="server" Title="Are you sure you want to delete selected Other Business?" ValidationGroup="DeleteOtherBusiness" />
                    <uc9:DeleteReason ID="popupDeleteResources" runat="server" Title="Are you sure you want to delete selected Resources?" ValidationGroup="DeleteResources" />
                    <uc9:DeleteReason ID="popupDeleteDebts" runat="server" Title="Are you sure you want to delete selected Debts?" ValidationGroup="DeleteDebts" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
