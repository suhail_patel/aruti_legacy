﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_DetailedSalaryBreakdownReport.aspx.vb"
    Inherits="Payroll_wPg_DetailedSalaryBreakdownReport" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll2 = {
                Y: '#<%= hfScrollPosition2.ClientID %>'
            }; 
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container2").scrollTop($(scroll2.Y).val());
    }
    }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Detailed Salary Breakdown Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div2" class="panel-body-default" style="position: relative">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%; vertical-align: top;">
                                            <div id="FilterCriteria" class="panel-default">
                                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                                    </div>
                                                    <div style="text-align: right">
                                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblCCenter" runat="server" Text="Cost Center"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="cboCCenter" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblDept" runat="server" Text="Department"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="cboDept" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                                        </td>
                                                        <td style="width: 30%">
                                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 45%">
                                                                <asp:Label ID="lblExRate" runat="server" Text="#Value"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                        </td>
                                                        <td style="width: 75%" colspan="2">
                                                            <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                                Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td style="width: 50%; vertical-align: top;">
                                            <asp:UpdatePanel ID="uppnlCustomReport" runat="server">
                                                <ContentTemplate>
                                                    <div id="Div3" class="panel-default">
                                                        <div id="Div4" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblCustomSettings" runat="server" Text="Custom Settings"></asp:Label>
                                                            </div>
                                                            <div style="text-align: right">
                                                                <asp:LinkButton ID="lnkSave" runat="server" Text="Save Settings" CssClass="lnkhover"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="width: 100%">
                                                                    <table style="width: 100%">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 30%">
                                                                                <asp:Label ID="lblTrnHeadType" runat="server" Text="Transaction Head Type"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 70%">
                                                                                <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%; vertical-align: top">
                                                                <td style="width: 100%">
                                                                    <div id="scrollable-container2" style="max-height: 250px; overflow: auto" onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                        <asp:GridView ID="lvCustomTranHead" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                                            DataKeyNames="tranheadunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                            OnCheckedChanged="chkHeadSelect_OnCheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                    FooterText="colhAllocation" />
                                                                                <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                                    FooterText="colhAllocation" />
                                                                                <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                                            Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                                                        <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                                            Text="&#x25BC;" OnClick="ChangeTranHeadLocation" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                                <div id="btnfixedbottom" class="btn-default">
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btnDefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc6:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
