﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_AuthorizedPayment.aspx.vb" Inherits="Payroll_wPg_AuthorizedPayment"
    Title="Authorized Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
                }
            }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Authorized Payment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top; overflow: auto">
                            <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                                        height: 385px; overflow: auto; vertical-align: top;">
                                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                    <asp:GridView ID="GvAuthorizedPayment" runat="server" AutoGenerateColumns="False"
                                                                        AllowPaging="False" ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                        RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                                HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                                <span class="gridiconbc">
                                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                                                                </span>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                                <span class="gridiconbc">
                                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                            OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                                                                </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" FooterText="colhCode" />
                                                                            <asp:BoundField DataField="EmpName" HeaderText="Name" ReadOnly="True" FooterText="colhName" />
                                                                            <asp:BoundField DataField="expaidamt" HeaderText="Paid Amount" ReadOnly="True" ItemStyle-HorizontalAlign="Right"
                                                                                HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                                                            <asp:BoundField DataField="paidcurrency" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrency" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                                <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                </div>
                                                                   
                                                            </td>
                                                        </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <h3 style="border-bottom: 1px solid #DDD; color: #333; margin-bottom: 10px">
                                                            <asp:Label ID="gbAmountInfo" runat="server" Text="Total Amount"></asp:Label>
                                                                    </h3>
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 30%">
                                                                <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount"></asp:Label>
                                                            </td>
                                                            <td style="width: 70%">
                                                                                <asp:TextBox ID="txtTotalAmount" Style="text-align: right; padding-right: 10px" runat="server"
                                                                                    ReadOnly="true" Text="0"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 50%; vertical-align: top; overflow: auto">
                                                <div class="panel-body">
                                                    <div id="Div3" class="panel-default">
                                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbPaymentInfo" runat="server" Text="Payment Information"></asp:Label>
                                                            </div>
                                                            <div style="text-align: right;">
                                                                <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                                                    Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                                </div>
                                                        </div>
                                                        <div id="Div4" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                            </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblPmtVoucher" runat="server" Text="Voucher #"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboPmtVoucher" runat="server" Width="100%">
                                                                        </asp:DropDownList>
                                            </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="objbtnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                                                <asp:Button ID="objbtnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div11" class="panel-default">
                                                        <div id="Div6" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbPaymentModeInfo" runat="server" Text="Payment Mode Summary"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div14" class="panel-body-default" style="position: relative">
                                                            <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                <asp:Label ID="lblbankPaid" runat="server" Text="Bank Payment"></asp:Label>
                                                            </td>
                                                                    <td style="width: 75%">
                                                                <asp:Label ID="objBankpaidVal" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                <asp:Label ID="lblCashPaid" runat="server" Text="Cash Payment"></asp:Label>
                                                            </td>
                                                                    <td style="width: 75%">
                                                                <asp:Label ID="objCashpaidVal" runat="server" Text="0"></asp:Label>
                                                            </td>
                                                        </tr>
                                                                <tr style="width: 100%; margin-top: 10px">
                                                                    <td style="width: 100%" colspan="2">
                                                                        <div style="border-bottom: 1px solid #DDD; margin-top: 10px; margin-bottom: 5px">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%; margin-top: 5px">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblTotalPaid" runat="server" Text="Total Payment"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:Label ID="objTotalpaidVal" runat="server" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div style="border-bottom: 1px solid #DDD; margin-top: 10px">
                                                        </div>
                                                            <table style="width: 100%; margin-top: 10px">
                                                        <tr style="width: 100%">
                                                                    <td style="width: 30%; vertical-align: top">
                                                                <asp:Label ID="lblRemarks" runat="server" Text="Remark"></asp:Label>
                                                            </td>
                                                            <td style="width: 70%">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                            <div id="btnfixedbottom" class="btn-default">
                                                            <%--<div style="text-align: left;">--%>
                                                                <table style="width: 100%" >
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 37%; vertical-align: top">
                                                                            <table style="">
                                                                                <tr style="width: 100%">
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkPayrollVarianceReport" runat="server" Text="Show Payroll Variance Report"
                                                                                            CssClass="lnkhover" Style="color: Blue; vertical-align: top; float: left; font-size: 10px;"></asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:LinkButton ID="lnkPayrollTotalVarianceReport" runat="server" Text="Show Payroll Total Variance Report"
                                                                                            CssClass="lnkhover" Style="color: Blue; vertical-align: top; float: left; font-size: 10px;"></asp:LinkButton>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td style="width: 60%; vertical-align: top">
                                                                            <table>
                                                                                <tr style="width: 100%">
                                                                                    <td>
                                                                <asp:Button ID="btnProcess" runat="server" Text="Authorize Payment" CssClass="btnDefault" />
                                                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <%--</div>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
