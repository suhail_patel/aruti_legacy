﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Payroll_wPg_EDDetailReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEDDetailReport"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim objEmp As New clsEmployee_Master 'Sohail (18 May 2013)
        Dim dsCombo As DataSet
        Try

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsCombo = objEmp.GetEmployeeList("Emp", True, False, , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'Else
            '    dsCombo = objEmp.GetEmployeeList("Emp", False, False, CInt(Session("Employeeunkid")), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyAccess As Boolean = True 'Sohail (13 Feb 2016) 
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyAccess = False 'Sohail (13 Feb 2016) 
            End If

            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                Session("UserAccessModeSetting").ToString, True, _
            '                                chkInactiveemp.Checked, "Emp", blnSelect, intEmpId)
            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            'dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                Session("UserAccessModeSetting").ToString, True, _
            '                                True, "Emp", blnSelect, intEmpId)
            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, True, _
                                          True, "Emp", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyAccess)
            'Sohail ((13 Feb 2016) -- End
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables("Emp")
                .DataBind()
            End With
            'Sohail (18 May 2013) -- End

            dsCombo = objMaster.getComboListForHeadType("List")
            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'Dim dr As DataRow = dsCombo.Tables(0).NewRow
            'dr.Item("Id") = 99999
            'dr.Item("Name") = Language.getMessage(mstrModuleName, 7, "PAY PER ACTIVITY")
            'dsCombo.Tables(0).Rows.Add(dr)
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            Dim dr As DataRow = dsCombo.Tables(0).NewRow
            dr.Item("Id") = 99998
            dr.Item("Name") = Language.getMessage(mstrModuleName, 8, "Claim Request Expense")
            dsCombo.Tables(0).Rows.Add(dr)
            With ddlMode
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
            Call ddlMode_SelectedIndexChanged(ddlMode, New System.EventArgs)

            Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC - Inclusion of Closed FY periods
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, 2, , , Session("Database_Name").ToString)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True, 2, , , Session("Database_Name").ToString)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "Period", True, 2)
            'Shani(20-Nov-2015) -- End

            'Sohail (03 Dec 2013) -- End
            If intFirstOpenPeriod > 0 Then
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("periodunkid = " & intFirstOpenPeriod & " ")
                If drRow.Length <= 0 Then
                    Dim dRow As DataRow = dsCombo.Tables(0).NewRow

                    objPeriod._Periodunkid(Session("Database_Name").ToString) = intFirstOpenPeriod

                    dRow.Item("periodunkid") = intFirstOpenPeriod
                    dRow.Item("name") = objPeriod._Period_Name
                    dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                    dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                    dRow.Item("yearunkid") = objPeriod._Yearunkid 'Sohail (03 Dec 2013)

                    dsCombo.Tables(0).Rows.Add(dRow)
                End If
            End If
            With drpPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = intFirstOpenPeriod.ToString
            End With

            With drpPeriodTo
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = intFirstOpenPeriod.ToString
            End With

            If ViewState("dsPeriod") Is Nothing Then
                ViewState("dsPeriod") = dsCombo.Tables(0).Copy
            End If




            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes.

            Dim objMember As New clsmembership_master
            dsCombo = objMember.getListForCombo("Membership", True)
            With drpMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (08-Jun-2013) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            objPeriod = Nothing
            objMaster = Nothing
            objEmp = Nothing
            'Sohail (18 May 2013) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objPeriod As New clscommom_period_Tran
        Dim objPayment As New clsPayment_tran
        Try
            If ddlMode.SelectedValue = "" OrElse CInt(ddlMode.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Mode.", Me)
                Return False
            ElseIf ddlHeads.SelectedValue = "" OrElse CInt(ddlHeads.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Head.", Me)
                Return False
            ElseIf drpPeriod.SelectedValue = "" OrElse CInt(drpPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please select From Period.", Me)
                Return False
            ElseIf drpPeriodTo.SelectedValue = "" OrElse CInt(drpPeriodTo.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please select To Period.", Me)
                Return False
            ElseIf CInt(drpPeriodTo.SelectedIndex) < CInt(drpPeriod.SelectedIndex) Then
                DisplayMessage.DisplayMessage(" To Period cannot be less than From Period.", Me)
                Return False
            End If


            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedItem.Value)
            If objPeriod._Statusid <> 2 Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim ds As DataSet = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(drpPeriod.SelectedItem.Value), clsPayment_tran.enPayTypeId.PAYMENT, CInt(Session("Employeeunkid")))
                'Nilay (03-Feb-2016) -- Start
                'Accessfilter parameter problem
                'Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name"), _
                '                                               Session("UserId"), _
                '                                               Session("Fin_year"), _
                '                                               Session("CompanyUnkId"), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               Session("UserAccessModeSetting"), True, _
                '                                               Session("IsIncludeInactiveEmp"), _
                '                                               "Payment", _
                '                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                               CInt(drpPeriod.SelectedItem.Value), _
                '                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                               CInt(Session("Employeeunkid")))

                Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               chkInactiveemp.Checked, _
                                                               "Payment", _
                                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                               CInt(drpPeriod.SelectedItem.Value), _
                                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                                                               Session("Employeeunkid").ToString, False)
                'Nilay (03-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If ds.Tables("Payment").Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry! Payment is not done for selected From Period.", Me)
                    Return False
                End If
            End If

            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriodTo.SelectedItem.Value)
            If objPeriod._Statusid <> 2 Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim ds As DataSet = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(drpPeriodTo.SelectedItem.Value), clsPayment_tran.enPayTypeId.PAYMENT, CInt(Session("Employeeunkid")))
                
                'Nilay (03-Feb-2016) -- Start
                'Accessfilter parameter problem
                'Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name"), _
                '                                               Session("UserId"), _
                '                                               Session("Fin_year"), _
                '                                               Session("CompanyUnkId"), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               Session("UserAccessModeSetting"), True, _
                '                                               Session("IsIncludeInactiveEmp"), _
                '                                               "Payment", _
                '                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                               CInt(drpPeriodTo.SelectedItem.Value), _
                '                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                               CInt(Session("Employeeunkid")))

                Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               chkInactiveemp.Checked, _
                                                               "Payment", _
                                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                               CInt(drpPeriodTo.SelectedItem.Value), _
                                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                                                               Session("Employeeunkid").ToString, False)
                'Nilay (03-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If ds.Tables("Payment").Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry! Payment is not done for selected To Period.", Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidate:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            SetLanguage()
            'Anjan [04 June 2014] -- End

            If Not IsPostBack Then
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Combobox's Events "
    Protected Sub ddlMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMode.SelectedIndexChanged
        'Dim objTransactionhead As New clsTransactionHead 'Sohail (10 Sep 2014)
        Dim dsCombo As DataSet
        Try
            'Sohail (10 Sep 2014) -- Start
            'Enhancement - Pay Per Activity on E&D Detail Report.
            'dsCombo = objTransactionhead.getComboList("List", True, CInt(ddlMode.SelectedValue))
            'dsCombo = objTransactionhead.getComboList("List", True, CInt(ddlMode.SelectedValue), , , , , , , , True)
            'If CInt(ddlMode.SelectedValue) = 99999 Then
            '    Dim objActivity As New clsActivity_Master
            '    dsCombo = objActivity.getComboList("List", True)
            'Else
            '    Dim objTransactionhead As New clsTransactionHead
            '    dsCombo = objTransactionhead.getComboList("List", True, CInt(ddlMode.SelectedValue))
            'End If
            'ddlHeads.DataSource = Nothing
            'Sohail (10 Sep 2014) -- End
            'Sohail (23 Jun 2015) -- Start
            'Enhancement - Claim Request Expense on E&D Detail Reports.
            If CInt(ddlMode.SelectedValue) = 99998 Then
                Dim objExpense As New clsExpense_Master
                dsCombo = objExpense.getComboList(0, True, "List")
            Else
                Dim objTransactionhead As New clsTransactionHead
                dsCombo = objTransactionhead.getComboList(Session("Database_Name").ToString, "List", True, CInt(ddlMode.SelectedValue), , , , , , , True)
            End If
            'ddlHeads.DataSource = Nothing
            'Sohail (23 Jun 2015) -- End
            With ddlHeads
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                '.DataValueField = "tranheadunkid"
                'If CInt(ddlMode.SelectedValue) = 99999 Then
                '    .DataValueField = "activityunkid"
                'Else
                '.DataValueField = "tranheadunkid"
                'End If
                'Sohail (10 Sep 2014) -- End
                'Sohail (23 Jun 2015) -- Start
                'Enhancement - Claim Request Expense on E&D Detail Reports.
                If CInt(ddlMode.SelectedValue) = 99998 Then
                    .DataValueField = "Id"
                Else
                    .DataValueField = "tranheadunkid"
                End If
                'Sohail (23 Jun 2015) -- End
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            'If ViewState("dsHeads") Is Nothing Then 'Sohail (10 Sep 2014)
                ViewState("dsHeads") = dsCombo.Tables("List").Copy
            'End If 'Sohail (10 Sep 2014)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ddlMode_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ddlMode_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "
    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebutton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'Sohail (03 Dec 2013) -- Start
        'Enhancement - TBC
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        'Sohail (03 Dec 2013) -- End
        Dim mdicYearDBName As New Dictionary(Of Integer, String) 'Sohail (21 Aug 2015)

        Try
            If IsValidate() = False Then
                Exit Sub
            End If


            Dim objEDDetail As New clsEDDetailReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            objEDDetail.SetDefaultValue()

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'objEDDetail._EmployeeId = CInt(Session("Employeeunkid"))
            'objEDDetail._EmployeeName = Session("UserName")
            objEDDetail._EmployeeId = CInt(cboEmployee.SelectedValue)
            objEDDetail._EmployeeName = cboEmployee.SelectedItem.Text
            objEDDetail.setDefaultOrderBy(0)
            'Sohail (18 May 2013) -- End

            objEDDetail._ModeId = CInt(ddlMode.SelectedValue)
            objEDDetail._ModeName = ddlMode.SelectedItem.Text

            objEDDetail._HeadId = CInt(ddlHeads.SelectedValue)
            objEDDetail._HeadName = ddlHeads.SelectedItem.Text
            If ViewState("dsHeads") IsNot Nothing Then
                'Sohail (10 Sep 2014) -- Start
                'Enhancement - Pay Per Activity on E&D Detail Report.
                'Dim dtRow() As DataRow = CType(ViewState("dsHeads"), DataTable).Select("tranheadunkid = '" & CInt(ddlHeads.SelectedValue) & "'")
                'Dim dtRow() As DataRow
                'If CInt(ddlMode.SelectedValue) = 99999 Then
                '    dtRow = CType(ViewState("dsHeads"), DataTable).Select("activityunkid = '" & CInt(ddlHeads.SelectedValue) & "'")
                'Else
                '    dtRow = CType(ViewState("dsHeads"), DataTable).Select("tranheadunkid = '" & CInt(ddlHeads.SelectedValue) & "'")
                'End If
                'Sohail (10 Sep 2014) -- End
                'Sohail (23 Jun 2015) -- Start
                'Enhancement - Claim Request Expense on E&D Detail Reports.
                Dim dtRow() As DataRow
                If CInt(ddlMode.SelectedValue) = 99998 Then
                    dtRow = CType(ViewState("dsHeads"), DataTable).Select("Id = '" & CInt(ddlHeads.SelectedValue) & "'")
                Else
                    dtRow = CType(ViewState("dsHeads"), DataTable).Select("tranheadunkid = '" & CInt(ddlHeads.SelectedValue) & "'")
                End If
                'Sohail (23 Jun 2015) -- End
                If dtRow.Length > 0 Then
                    objEDDetail._HeadCode = dtRow(0)("code").ToString
                End If
            End If

            objEDDetail._PeriodId = CInt(drpPeriod.SelectedValue)
            objEDDetail._PeriodName = drpPeriod.SelectedItem.Text

            objEDDetail._ViewByIds = ""
            objEDDetail._ViewIndex = 0
            objEDDetail._ViewByName = ""
            objEDDetail._Analysis_Fields = ""
            objEDDetail._Analysis_Join = ""
            objEDDetail._Analysis_OrderBy = ""
            objEDDetail._Report_GroupName = ""

            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
            'objEDDetail._IsActive = True
            'objEDDetail._IncludeNegativeHeads = True
            objEDDetail._IsActive = chkInactiveemp.Checked
            objEDDetail._IncludeNegativeHeads = chkIncludeNegativeHeads.Checked
            'Sohail (18 May 2013) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)
            objEDDetail._PeriodStartDate = objPeriod._Start_Date

            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriodTo.SelectedValue)
            objEDDetail._PeriodEndDate = objPeriod._End_Date

            objEDDetail._ToPeriodId = CInt(drpPeriodTo.SelectedValue)
            objEDDetail._ToPeriodName = drpPeriodTo.SelectedItem.Text

            'Sohail (03 Dec 2013) -- Start
            'Enhancement - TBC
            'Dim i As Integer = 0
            'Dim strList As String = drpPeriod.SelectedValue.ToString
            'For Each dsRow As DataRow In CType(ViewState("dsPeriod"), DataTable).Rows
            '    If i > drpPeriod.SelectedIndex AndAlso i <= drpPeriodTo.SelectedIndex Then
            '        strList &= ", " & dsRow.Item("periodunkid").ToString
            '    End If
            '    i += 1
            'Next
            'objEDDetail._mstrPeriodID = strList
            For Each dsRow As DataRow In CType(ViewState("dsPeriod"), DataTable).Rows
                If i >= drpPeriod.SelectedIndex AndAlso i <= drpPeriodTo.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                            End If
                            'Sohail (21 Aug 2015) -- End
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Exit Sub

            objEDDetail._mstrPeriodID = strPeriodIDs
            objEDDetail._Arr_DatabaseName = arrDatabaseName
            'Sohail (03 Dec 2013) -- End
           
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            objEDDetail._mdicYearDBName = mdicYearDBName
            'Sohail (21 Aug 2015) -- End

            'objPeriod = Nothing 'Sohail (21 Jan 2016)

            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            Select Case CInt(Session("PayslipTemplate"))
                Case 2 'TRA Format
                    objEDDetail._IsLogoCompanyInfo = False
                    objEDDetail._IsOnlyCompanyInfo = False
                    objEDDetail._IsOnlyLogo = True
                Case Else
                    Select Case CInt(Session("LogoOnPayslip"))
                        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                            objEDDetail._IsLogoCompanyInfo = True
                            objEDDetail._IsOnlyCompanyInfo = False
                            objEDDetail._IsOnlyLogo = False
                        Case enLogoOnPayslip.COMPANY_DETAILS
                            objEDDetail._IsLogoCompanyInfo = False
                            objEDDetail._IsOnlyCompanyInfo = True
                            objEDDetail._IsOnlyLogo = False
                        Case enLogoOnPayslip.LOGO
                            objEDDetail._IsLogoCompanyInfo = False
                            objEDDetail._IsOnlyCompanyInfo = False
                            objEDDetail._IsOnlyLogo = True
                    End Select
            End Select
            'Sohail (09 Feb 2016) -- End


            objEDDetail._Advance_Filter = ""

            GUI.fmtCurrency = Session("fmtCurrency").ToString


            'Pinkal (08-Jun-2013) -- Start
            'Enhancement : TRA Changes
            objEDDetail._MembershipId = CInt(drpMembership.SelectedValue)
            objEDDetail._MemberShipName = drpMembership.SelectedItem.Text
            'Pinkal (08-Jun-2013) -- End

            objEDDetail._TranDatabaseName = Session("Database_Name").ToString
            objEDDetail._Base_CurrencyId = CInt(Session("Base_CurrencyId"))
            objEDDetail._fmtCurrency = Session("fmtCurrency").ToString
            objEDDetail._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objEDDetail._UserUnkId = CInt(Session("UserId"))
            objEDDetail._UserAccessFilter = Session("AccessLevelFilterString").ToString 'Sohail (31 Jan 2014)

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            objEDDetail._ApplyUserAccessFilter = blnApplyAccess
            'Sohail ((13 Feb 2016) -- End
			'Sohail (04 Aug 2016) -- Start
            'Enhancement - 63.1 - Consolidated Employee name on E&D Detail Report for ASP.
            objEDDetail._FirstNamethenSurname = CBool(Session("FirstNamethenSurname"))
            'Sohail (04 Aug 2016) -- End

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 58.1 changes in Web.
            'objEDDetail.generateReport(0, enPrintAction.None, enExportAction.None)
            objEDDetail.generateReportNew(Session("Database_Name").ToString, _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          objPeriod._Start_Date, _
                                          objPeriod._End_Date, _
                                          Session("UserAccessModeSetting").ToString, True, _
                                          Session("ExportReportPath").ToString, _
                                          CBool(Session("OpenAfterExport")), _
                                          0, enPrintAction.None, enExportAction.None, _
                                          CInt(Session("Base_CurrencyId")))
            'Sohail (21 Jan 2016) -- End
            Session("objRpt") = objEDDetail._Rpt

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            If Session("objRpt") IsNot Nothing Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Sohail (09 Feb 2016) -- End
            'Shani(24-Aug-2015) -- End

            objPeriod = Nothing 'Sohail (21 Jan 2016)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnPreview_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnPreview_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblHeads.Text = Language._Object.getCaption(Me.lblHeads.ID, Me.lblHeads.Text)
            Me.lblSelectMode.Text = Language._Object.getCaption(Me.lblSelectMode.ID, Me.lblSelectMode.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.LblTo.Text = Language._Object.getCaption(Me.LblTo.ID, Me.LblTo.Text)
            Me.chkIncludeNegativeHeads.Text = Language._Object.getCaption(Me.chkIncludeNegativeHeads.ID, Me.chkIncludeNegativeHeads.Text)
            Me.LblMembership.Text = Language._Object.getCaption(Me.LblMembership.ID, Me.LblMembership.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class
