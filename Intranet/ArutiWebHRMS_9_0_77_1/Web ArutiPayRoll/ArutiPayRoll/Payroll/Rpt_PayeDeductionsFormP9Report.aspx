﻿<%@ Page Title="Paye Deductions FormP9 Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_PayeDeductionsFormP9Report.aspx.vb" Inherits="Payroll_Rpt_PayeDeductionsFormP9Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 65%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Paye Deductions FormP9 Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <asp:UpdatePanel ID="updt_pnl" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table id="tblP9" runat="server" style="width: 100%">
                                            <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblFYear" runat="server" Text="Financial Year"></asp:Label>
                                                    </td>
                                                    <td style="width: 81%" colspan="4">
                                                        <asp:DropDownList ID="cboFYear" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>   
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                    </td>
                                                    <td style="width: 81%" colspan="4">
                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>   
                                                                                            
                                            </table>
                                            
                                            <div id="scrollable-container1" style="width: 100%; overflow: auto; vertical-align: top"
                                                    onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                    <asp:GridView ID="gvPeriod" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        AllowPaging="False" Width="99%" CssClass="gridview" HeaderStyle-Font-Bold="false"
                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem">
                                                <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAllPeriod" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAllPeriod_OnCheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelectPeriod" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                OnCheckedChanged="chkSelectPeriod_OnCheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="periodunkid" HeaderText="periodunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="period_name" HeaderText="Pay Period" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                        FooterText="lblPeriods" />
                                                    <asp:BoundField DataField="start_date" HeaderText="start_date" ReadOnly="true" Visible="false"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="end_date" HeaderText="end_date" ReadOnly="true" Visible="false"
                                                        HeaderStyle-HorizontalAlign="Left" />
                                                </Columns>
                                            </asp:GridView>
                                                </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <%--<asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />--%>
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
</asp:Content>

