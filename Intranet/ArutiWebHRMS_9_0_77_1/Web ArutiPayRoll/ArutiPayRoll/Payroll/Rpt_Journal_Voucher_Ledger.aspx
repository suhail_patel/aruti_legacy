﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Journal_Voucher_Ledger.aspx.vb" Inherits="Payroll_Rpt_Journal_Voucher_Ledger"
    Title="Journal Voucher Ledger" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ExportReport.ascx" TagName="Export" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script type="text/javascript">

    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

        if (charCode == 13)
            return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 60%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Journal Voucher Ledger"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Employee Allocation Filter..." CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 40%" colspan="2">
                                                <asp:Label ID="lblExRate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblDebitAmountFrom" runat="server" Text="Debit Amount From"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtDebitAmountFrom" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                            </td>
                                            <td style="width: 5%; text-align: center">
                                                <asp:Label ID="lblDebitAmountTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtDebitAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblCreditAMountFrom" runat="server" Text="Credit Amount From"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtCreditAmountFrom" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                            </td>
                                            <td style="width: 5%; text-align: center">
                                                <asp:Label ID="lblCreditAMountTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtCreditAMountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboBranch" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblCCGroup" runat="server" Text="C.Center Group"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboCCGroup" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 40%" colspan="2">
                                                <asp:CheckBox ID="chkShowGroupByCCGroup" runat="server" Text="Show Group By Cost Center Group"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblCustomCCenter" runat="server" Text="Custom C.Center"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboCustomCCenter" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblInvoiceRef" runat="server" Text="Invoice Reference"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtInvoiceRef" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkIncludeEmployerContribution" runat="server" Text="Include Employer Contribution">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkIgnoreZero" runat="server" Text="Ignore Zero"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkShowSummaryBottom" runat="server" Text="Show Summary Report at the Bottom"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkShowSummaryNewPage" runat="server" Text="Show Summary Report on New Page"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkShowColumnHeader" runat="server" Text="Show Column Header" AutoPostBack="true">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:CheckBox ID="chkIncludeEmpCodeNameOnDebit" runat="server" Text="Include Emp. code and Name on Debit" AutoPostBack="false">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>                                        
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblTranHead" runat="server" Text="Transaction Head"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboTranHead" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>                                        
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblPostingDate" runat="server" Text="Posting Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <uc2:DateCtrl ID="dtpPostingDate" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblCostCenter" runat="server" Text="Default C. Center"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboCostCenter" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkPayrollJournalReport" runat="server" Text="Payroll Journal Report..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkPayrollJournalExport" runat="server" Text="Payroll Journal Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkiScalaJVExport" runat="server" Text="iScala JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSunJV5Export" runat="server" Text="Sun JV 5 Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSunJVExport" runat="server" Text="Sun JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkTBCJVExport" runat="server" Text="SAP JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkiScala2JVExport" runat="server" Text="iScala2 JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkXEROJVExport" runat="server" Text="XERO JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkNetSuiteERPJVExport" runat="server" Text="NetSuite ERP JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkFlexCubeRetailJVExport" runat="server" Text="Flex Cube Retail JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSunAccountProjectJVExport" runat="server" Text="Sun Account (Project JV) Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkDynamicsNavJVExport" runat="server" Text="Dynamics Nav JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkFlexCubeUPLDJVExport" runat="server" Text="Flex Cube UPLD JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAPJVBSOneExport" runat="server" Text="SAP JV BS One Export..."></asp:LinkButton>
                                            </td>
                                        </tr>                                        
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkFlexCubeJVExport" runat="server" Text="Flex Cube JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkFlexCubeJVPostOracle" runat="server" Text="Flex Cube JV Post to Oracle..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkFlexCubeLoanBatchExport" runat="server" Text="Flex Cube Loan Batch Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <!--'Sohail (02 Mar 2019) -- Start -->
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkBRJVExport" runat="server" Text="BR-JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkBRJVPostSQL" runat="server" Text="BR-JV Post to SQL..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <!--'Sohail (02 Mar 2019) -- End -->
                                        <!--'Hemant (15 Mar 2019) -- Start -->
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAPECC6_0JVExport" runat="server" Text="SAP ECC6.0-JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <!--'Hemant (15 Mar 2019) -- End -->
                                        <!--'Hemant (18 Apr 2019) -- Start -->
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAGE300JVExport" runat="server" Text="SAGE 300-JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <!--'Hemant (18 Apr 2019) -- End -->
                                        <!--'Hemant (29 May 2019) -- Start -->
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkPASTELV2JVExport" runat="server" Text="PASTEL V2-JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <!--'Hemant (29 May 2019) -- End -->
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAGE300ERPJVExport" runat="server" Text="SAGE 300 ERP JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAGE300ERPImport" runat="server" Text="SAGE 300 ERP Importation Temp..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkCoralSunJVExport" runat="server" Text="Coral Sun JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkCargoWiseJVExport" runat="server" Text="Cargo Wise JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAPStandardJVExport" runat="server" Text="SAP Standard JV Export......"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkSAGEEvolutionJVExport" runat="server" Text="SAGE Evolution JV Export......"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:LinkButton ID="lnkHakikaBankJVExport" runat="server" Text="Hakika Bank JV Export..."></asp:LinkButton>
                                            </td>
                                        </tr>
                                        
                                    </table>
                                    <asp:Panel ID="pnlLoanSchemeList" runat="server">
                                        <table style="width: 100%; margin-top: 10px">
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblSearchScheme" runat="server" Text="Search Scheme"></asp:Label>
                                                </td>
                                                <td style="width: 80%">
                                                    <asp:TextBox ID="txtSearchScheme" runat="server" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                                <td style="width: 100%; vertical-align: top" colspan="2">
                                                    <Div ID="divScheme" runat="server" ScrollBars="Auto">
                                                        <asp:GridView ID="gvScheme" runat="server" Style="margin: auto;" AutoGenerateColumns="False"
                                                            AllowPaging="False" Width="100%" CssClass="gridview" HeaderStyle-Font-Bold="false"
                                                            HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem">
                                                        <Columns>
                                                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-Width="10%" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="headerstyle" ItemStyle-CssClass="itemstyle">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="objchkSelectAllLoanScheme" runat="server" AutoPostBack="true" OnCheckedChanged="objchkSelectAllLoanScheme_OnCheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="objdgcolhCheck" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                        OnCheckedChanged="objdgcolhCheck_OnCheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="id" HeaderText="ID" ReadOnly="true" Visible="false" />
                                                            <asp:BoundField DataField="code" HeaderText="Scheme Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                    FooterText="colhCode" HeaderStyle-Width="30%" ItemStyle-Width="30%" />
                                                            <asp:BoundField DataField="name" HeaderText="Scheme Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                    FooterText="colhName" HeaderStyle-Width="60%" ItemStyle-Width="60%" />
                                                        </Columns>
                                                    </asp:GridView>
                                                    </Div>
                                            </td>
                                        </tr>
                                    </table>
                                    </asp:Panel>
                                    
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%; vertical-align: top">
                                                <asp:Panel ID="pnlTranHead" runat="server" Width="100%" ScrollBars="Auto" Height="150px">
                                                    <asp:GridView ID="gbFilterTBCJV" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        AllowPaging="False" ShowFooter="False" Width="100%" CellPadding="3" CssClass="gridview"
                                                        HeaderStyle-Font-Bold="false" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="code" HeaderText="Code" FooterText="colhEContribHeadCode" />
                                                            <asp:BoundField DataField="name" HeaderText="Employer Contribution Head" FooterText="colhEContribHead" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popup_PayrollJournal" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnPayrollJournalClose" PopupControlID="pnl_PayrollJournal" TargetControlID="HiddenField1">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_PayrollJournal" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="Payroll Journal Columns Export" />
                            </div>
                            <div class="panel-body">
                                <div id="Div10" class="panel-default">
                                    <div id="Div11" class="panel-heading-default">
                                        <div style="float: left;">
                                        </div>
                                    </div>
                                    <div id="Div12" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td colspan="2" style="width: 100%">
                                                    <div style="max-height: 400px; overflow: auto">
                                                        <asp:GridView ID="lvPayrollJournalColumns" runat="server" AutoGenerateColumns="False"
                                                            AllowPaging="False" Width="99%" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            CssClass="gridview" DataKeyNames="ID">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                    FooterText="objcolhNCheck">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" ToolTip="All"
                                                                            OnCheckedChanged="lvPayrollJournalColumns_ItemChecked" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                            AutoPostBack="true" ToolTip="Checked" OnCheckedChanged="lvPayrollJournalColumns_ItemChecked" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Name" HeaderText="Payroll Journal Columns" ReadOnly="true"
                                                                    FooterText="colhPayrollJournalColumns" />
                                                                <asp:BoundField DataField="ID" ReadOnly="true" Visible="false" />
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                            Text="&#x25B2;" OnClick="ChangeLocation" />
                                                                        <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                            Text="&#x25BC;" OnClick="ChangeLocation" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="2" style="width: 100%">
                                                    <asp:CheckBox ID="chkShowColumnHeaderPayrollJournal" runat="server" Text="Show Column Header on Report" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblExportMode" runat="server" Text="Export Mode"></asp:Label>
                                                </td>
                                                <td style="width: 64%">
                                                    <asp:DropDownList ID="cboExportMode" runat="server" AutoPostBack="true" Width="250px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                             <%--'Hemant (13 Jul 2020) -- Start--%>
                                            <tr style="width: 100%">
                                                <td style="width: 35%">
                                                    <asp:Label ID="lblDateFormat" runat="server" Text="Date Format"></asp:Label>
                                                </td>
                                                <td style="width: 64%">
                                                    <asp:TextBox ID="txtDateFormat" runat="server" >
                                                    </asp:TextBox>
                                                </td>
                                            </tr>             
                                           <%--'Hemant (13 Jul 2020) -- End--%>
                                           <tr style="width: 100%">
                                                <td  style="width: 35%">
                                                    <asp:CheckBox ID="chkSaveAsTXT" runat="server" Text="Save As TXT" />
                                                </td>
                                                <td style="width: 64%">
                                                    <asp:CheckBox ID="chkTABDelimiter" runat="server" Text="TAB Delimiter" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnPayrollJournalSaveSelection" runat="server" Text="Save Selection" CssClass="btnDefault" />
                                            <asp:Button ID="btnPayrollJournalOK" runat="server" Text="OK" CssClass="btnDefault" />
                                            <asp:Button ID="btnPayrollJournalClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc3:AnalysisBy ID="popupAnalysisBy" runat="server" />
                    <uc9:Export runat="server" ID="Export" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                    <uc9:ConfirmYesNo ID="popupSure" runat="server" Title="Are you sure?" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Export" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
