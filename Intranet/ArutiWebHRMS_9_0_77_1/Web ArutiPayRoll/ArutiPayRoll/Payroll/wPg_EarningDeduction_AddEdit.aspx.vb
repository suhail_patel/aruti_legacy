﻿Option Strict On 'Nilay (10-Feb-2016)

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Data

Partial Class Payroll_wPg_EarningDeduction_AddEdit
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Private objED As clsEarningDeduction
    'Hemant (04 Sep 2020) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEarningDeduction_AddEdit"
    'Anjan [04 June 2014 ] -- End

    'Nilay (10-Feb-2016) -- Start
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End

#End Region

#Region " Private Functions "

    Private Function IsValidate() As Boolean
        Dim strMsg As String = ""
        Dim strHeadName As String = ""
        Dim dsList As DataSet = Nothing
        Dim objTrnFormula As New clsTranheadFormulaTran
        Dim objED As New clsEarningDeduction
        Dim objTranHead As New clsTransactionHead
        Dim objTnA As New clsTnALeaveTran

        Try

            If CInt(cboEmployee.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                strMsg = Language.getMessage(mstrModuleName, 1, "Please Select Employee. Employee is mandatory information.")
                'Anjan [04 June 2014] -- End


                DisplayMessage.DisplayMessage(strMsg, Me)
                cboEmployee.Focus()
                Exit Function

            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                strMsg = Language.getMessage(mstrModuleName, 9, "Please Select Period. Period is mandatory information.")
                'Anjan [04 June 2014] -- End
                DisplayMessage.DisplayMessage(strMsg, Me)
                cboPeriod.Focus()
                Exit Function

            ElseIf CInt(cboTrnHead.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                strMsg = Language.getMessage(mstrModuleName, 2, "Please Select Transaction Head. Transaction Head is mandatory information.")
                'Anjan [04 June 2014] -- End
                DisplayMessage.DisplayMessage(strMsg, Me)
                cboTrnHead.Focus()
                Exit Function
            End If

            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT

            'Nilay (10-Feb-2016) -- Start
            'If dtpCumulativeStartDate.IsNull = False AndAlso dtpCumulativeStartDate.GetDate.Date > CType(Me.ViewState("EndDate"), Date).Date Then
            If dtpCumulativeStartDate.IsNull = False AndAlso dtpCumulativeStartDate.GetDate.Date > mdtPeriodEndDate Then
                'Nilay (10-Feb-2016) -- End

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Nilay (10-Feb-2016) -- Start
                'strMsg = Language.getMessage(mstrModuleName, 15, "Sorry, Cumulative Start Date should not be greater than.") & " " & CType(Me.ViewState("EndDate"), Date).ToShortDateString
                strMsg = Language.getMessage(mstrModuleName, 15, "Sorry, Cumulative Start Date should not be greater than.") & " " & mdtPeriodEndDate
                'Nilay (10-Feb-2016) -- End

                'Anjan [04 June 2014] -- End
                DisplayMessage.DisplayMessage(strMsg, Me)
                Exit Function
            End If

            'Nilay (10-Feb-2016) -- Start
            'If dtpStopDate.IsNull = False AndAlso dtpStopDate.GetDate.Date < CType(Me.ViewState("StartDate"), Date).Date Then
            If dtpStopDate.IsNull = False AndAlso dtpStopDate.GetDate.Date < mdtPeriodStartDate Then
                'Nilay (10-Feb-2016) -- End

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Nilay (10-Feb-2016) -- Start
                'strMsg = Language.getMessage(mstrModuleName, 16, "Sorry, Stop Date should not be less than.") & " " & CType(Me.ViewState("StartDate"), Date).ToShortDateString
                strMsg = Language.getMessage(mstrModuleName, 16, "Sorry, Stop Date should not be less than.") & " " & mdtPeriodStartDate
                'Nilay (10-Feb-2016) -- End

                'Anjan [04 June 2014] -- End
                DisplayMessage.DisplayMessage(strMsg, Me)
                Exit Function
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If dtpEDStartDate.IsNull = False AndAlso (dtpEDStartDate.GetDate.Date < mdtPeriodStartDate OrElse dtpEDStartDate.GetDate.Date > mdtPeriodEndDate) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, E. D. Start Date should be in between selected period start date and end date."), Me)
                Exit Try
            End If
            If dtpStopDate.IsNull = False AndAlso dtpEDStartDate.IsNull = False AndAlso eZeeDate.convertDate(dtpStopDate.GetDate.Date) < eZeeDate.convertDate(dtpEDStartDate.GetDate.Date) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, E. D. Start Date should be greater than stop date."), Me)
                Exit Try
            End If
            'Sohail (24 Aug 2019) -- End
           
            'Nilay (10-Feb-2016) -- Start
            'If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, CDate(Me.ViewState("EndDate"))) = True Then
            If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmployee.SelectedValue.ToString, mdtPeriodEndDate) = True Then
                'Nilay (10-Feb-2016) -- End

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, Process Payroll is already done for last date of selected Period. Please Void Process Payroll first for selected employee to assign earning deduction."), Me)
                'Anjan [04 June 2014] -- End
                Exit Try
            End If
            
            dsList = objTrnFormula.getFlateRateHeadFromFormula(CInt(cboTrnHead.SelectedValue), "TranHead")
            For Each dsRow As DataRow In dsList.Tables("TranHead").Rows
                With dsRow
                    'Do not prompt message if default value has been set on transaction head formula (other tham Computed Value)
                    If CInt(dsRow.Item("defaultvalue_id")) <> enTranHeadFormula_DefaultValue.COMPUTED_VALUE Then Continue For

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objED.isExist(CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                    If objED.isExist(CStr(Session("Database_Name")), CInt(cboEmployee.SelectedValue), CInt(.Item("tranheadunkid").ToString), , CInt(cboPeriod.SelectedValue)) = False Then
                        'Shani(20-Nov-2015) -- End

                        objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(.Item("tranheadunkid").ToString)
                        strHeadName = objTranHead._Trnheadname
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        Language.setLanguage(mstrModuleName)
                        strMsg = Language.getMessage(mstrModuleName, 3, "The Formula of this Transaction Head contains ") & strHeadName & Language.getMessage(mstrModuleName, 4, " Tranasaction Head (type of Flate Rate).") & vbCrLf & Language.getMessage(mstrModuleName, 5, "If you do not assign ") & strHeadName & " Transaction Head, its value will be cosidered as Zero." & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 7, "Do you want to proceed?")
                        'Anjan [04 June 2014] -- End
                        popupFormulaAssign.Message = strMsg
                        popupFormulaAssign.Show()
                        Exit Function
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate :- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidate :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objTrnFormula = Nothing
            objED = Nothing
            objTranHead = Nothing
            dsList = Nothing
        End Try
    End Function

    Private Sub GetValue(ByVal objED As clsEarningDeduction)
        'Hemant (04 Sep 2020) -- [objED]
        Dim objCC As New clsemployee_costcenter_Tran
        Dim dsList As DataSet = Nothing
        Try
            With objED
                cboPeriod.SelectedValue = CStr(._Periodunkid)
                cboPeriod_SelectedIndexChanged(New Object(), New EventArgs())

                cboEmployee.SelectedValue = CStr(._Employeeunkid(Session("Database_Name").ToString))
                cboEmployee_SelectedIndexChanged(New Object(), New EventArgs())

                cboTrnHeadType.SelectedValue = CStr(._Trnheadtype_Id)
                cboTrnHeadType_SelectedIndexChanged(New Object(), New EventArgs())

                cboTypeOf.SelectedValue = CStr(._Typeof_Id)
                cboTypeOf_SelectedIndexChanged(New Object(), New EventArgs())

                cboTrnHead.SelectedValue = CStr(._Tranheadunkid)
                cboTrnHead_SelectedIndexChanged(New Object(), New EventArgs())

                txtAmount.Text = Format(._Amount, CStr(Session("fmtCurrency")))

                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If ._Cumulative_Startdate <> Nothing Then
                    dtpCumulativeStartDate.SetDate = ._Cumulative_Startdate
                Else
                    dtpCumulativeStartDate.SetDate = Nothing
                End If
                If ._Stop_Date <> Nothing Then
                    dtpStopDate.SetDate = ._Stop_Date
                Else
                    dtpStopDate.SetDate = Nothing
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If ._EDStart_Date <> Nothing Then
                    dtpEDStartDate.SetDate = ._EDStart_Date
                Else
                    dtpEDStartDate.SetDate = Nothing
                End If
                'Sohail (24 Aug 2019) -- End

                If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsList = objCC.GetList("EmpCC", True, CInt(cboEmployee.SelectedValue), CInt(cboTrnHead.SelectedValue))

                    'Nilay (10-Feb-2016) -- Start
                    'dsList = objCC.GetList(Session("Database_Name"), _
                    '                       Session("UserId"), _
                    '                       Session("Fin_year"), _
                    '                       Session("CompanyUnkId"), _
                    '                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                       Session("UserAccessModeSetting"), True, _
                    '                       Session("IsIncludeInactiveEmp"), "EmpCC", True, _
                    '                       CInt(cboEmployee.SelectedValue), _
                    '                       CInt(cboTrnHead.SelectedValue))
                    'Sohail (29 Mar 2017) -- Start
                    'CCBRT Enhancement - 66.1 - Employee cost center distribution for non-salary heads, Effective period option on Employee Cost Center.
                    'dsList = objCC.GetList(CStr(Session("Database_Name")), _
                    '                       CInt(Session("UserId")), _
                    '                       CInt(Session("Fin_year")), _
                    '                       CInt(Session("CompanyUnkId")), _
                    '                       mdtPeriodStartDate, _
                    '                       mdtPeriodEndDate, _
                    '                       CStr(Session("UserAccessModeSetting")), True, _
                    '                       True, "EmpCC", True, _
                    '                       CInt(cboEmployee.SelectedValue), _
                    '                       CInt(cboTrnHead.SelectedValue))
                    dsList = objCC.GetList(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           mdtPeriodStartDate, _
                                           mdtPeriodEndDate, _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           True, "EmpCC", True, _
                                           CInt(cboEmployee.SelectedValue), _
                                           CInt(cboTrnHead.SelectedValue), _
                                           , mdtPeriodEndDate)
                    'Sohail (29 Mar 2017) -- End
                    'Nilay (10-Feb-2016) -- End

                    'Shani(20-Nov-2015) -- End

                    'Sohail (19 Dec 2018) -- Start
                    'Internal Issue - 76.1 - Employee Cost center get removed ob Editing Earning Deduction when distributed (multiple) cost center has been set for any head.
                    'If dsList.Tables("EmpCC").Rows.Count > 0 Then
                    '    cboCostCenter.SelectedValue = CStr(dsList.Tables("EmpCC").Rows(0).Item("costcenterunkid"))
                    'End If
                    cboCostCenter.Enabled = True
                    If dsList.Tables("EmpCC").Rows.Count > 1 Then 'More than one cost center
                        cboCostCenter.SelectedValue = "0"
                        cboCostCenter.Enabled = False
                    ElseIf dsList.Tables("EmpCC").Rows.Count = 1 Then
                        cboCostCenter.SelectedValue = CStr(dsList.Tables("EmpCC").Rows(0).Item("costcenterunkid"))
                    Else
                        cboCostCenter.SelectedValue = "0"
                    End If
                    'Sohail (19 Dec 2018) -- End
                Else
                    cboCostCenter.SelectedValue = "0"
                End If

                If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                    Dim mdtTable As DataTable = CType(Me.ViewState("ED"), DataTable)

                    'Nilay (10-Feb-2016) -- Start
                    'Dim dRow As DataRow() = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date > '" & eZeeDate.convertDate(Me.ViewState("StartDate")) & "' ")
                    Dim dRow As DataRow() = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date > '" & eZeeDate.convertDate(mdtPeriodStartDate) & "' ")
                    'Nilay (10-Feb-2016) -- End

                    If dRow.Length > 0 Then
                        dRow(0).Item("AUD") = "U"
                        mdtTable.AcceptChanges()
                        Me.ViewState("ED") = mdtTable
                    End If
                End If
                txtMedicalRefNo.Text = objED._MedicalRefNo
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue :-" & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objCC = Nothing
        End Try
    End Sub

    Private Sub SetValue(ByVal objED As clsEarningDeduction)
        'Hemant (04 Sep 2020) -- [objED]
        Dim objTranHead As New clsTransactionHead
        Try
            With objED
                ._Employeeunkid(CStr(Session("Database_Name"))) = CInt(cboEmployee.SelectedValue)
                objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(cboTrnHead.SelectedValue)
                ._Tranheadunkid = CInt(cboTrnHead.SelectedValue)

                If txtAmount.Text.Length > 0 Then
                    ._Amount = CDec(txtAmount.Text)
                Else
                    ._Amount = Nothing
                End If
                ._Currencyunkid = 0
                
                If objTranHead._Trnheadtype_Id = enTranHeadType.EarningForEmployees Then
                    ._Isdeduct = False
                ElseIf objTranHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee Or objTranHead._Trnheadtype_Id = enTranHeadType.EmployeesStatutoryDeductions Then
                    ._Isdeduct = True
                Else
                    ._Isdeduct = Nothing
                End If
                ._Trnheadtype_Id = objTranHead._Trnheadtype_Id
                ._Typeof_Id = objTranHead._Typeof_id
                ._Calctype_Id = objTranHead._Calctype_Id
                ._Computeon_Id = objTranHead._Computeon_Id
                ._Formula = objTranHead._Formula
                ._FormulaId = objTranHead._Formulaid
                ._Userunkid = CInt(Session("UserId"))
                ._CostCenterUnkID = CInt(cboCostCenter.SelectedValue)
                ._Periodunkid = CInt(cboPeriod.SelectedValue)
                ._MedicalRefNo = txtMedicalRefNo.Text.Trim
                'Sohail (09 Nov 2013) -- Start
                'TRA - ENHANCEMENT
                If dtpCumulativeStartDate.IsNull = False Then
                    ._Cumulative_Startdate = dtpCumulativeStartDate.GetDate.Date
                Else
                    ._Cumulative_Startdate = Nothing
                End If
                If dtpStopDate.IsNull = False Then
                    ._Stop_Date = dtpStopDate.GetDate.Date
                Else
                    ._Stop_Date = Nothing
                End If
                'Sohail (09 Nov 2013) -- End
                'Sohail (24 Aug 2019) -- Start
                'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
                If dtpEDStartDate.IsNull = False Then
                    ._EDStart_Date = dtpEDStartDate.GetDate.Date
                Else
                    ._EDStart_Date = Nothing
                End If
                'Sohail (24 Aug 2019) -- End

                If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                    If CBool(Session("AllowToApproveEarningDeduction")) = True Then
                        ._Isapproved = True
                        ._Approveruserunkid = CInt(Session("UserId"))
                    Else
                        ._Isapproved = False
                    End If
                End If

            End With

            'TRA - ENHANCEMENT [Now when assigning single head, copy all previous slab heads]

            Dim drED As DataRow = Nothing
            Dim mdtTable As DataTable = CType(Me.ViewState("ED"), DataTable)
            Dim mdtSingleED As DataTable = New DataView(mdtTable, "1 = 2 ", "", DataViewRowState.CurrentRows).ToTable

            drED = mdtSingleED.NewRow
            drED.Item("edunkid") = -1
            drED.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            drED.Item("employeecode") = cboEmployee.SelectedItem.Text.Split(CChar("-"))(0).Trim
            drED.Item("employeename") = cboEmployee.SelectedItem.Text
            'Sohail (31 Oct 2019) -- End
            drED.Item("tranheadunkid") = CInt(cboTrnHead.SelectedValue)
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            'drED.Item("trnheadname") = ""
            drED.Item("trnheadname") = cboTrnHead.SelectedItem.Text
            drED.Item("calctype_id") = objTranHead._Calctype_Id
            'Sohail (31 Oct 2019) -- End
            drED.Item("batchtransactionunkid") = -1
            If txtAmount.Text.Length > 0 Then
                drED.Item("amount") = txtAmount.Text
            Else
                drED.Item("amount") = 0
            End If
            drED.Item("currencyid") = 0
            drED.Item("vendorid") = 0
            drED.Item("userunkid") = Session("UserId")
            drED.Item("isvoid") = False
            drED.Item("voiduserunkid") = -1
            drED.Item("voiddatetime") = DBNull.Value
            drED.Item("voidreason") = ""
            drED.Item("membership_categoryunkid") = 0
            If CBool(Session("AllowToApproveEarningDeduction")) = True Then
                drED.Item("isapproved") = True
                drED.Item("approveruserunkid") = Session("UserId")
            Else
                drED.Item("isapproved") = False
                drED.Item("approveruserunkid") = -1
            End If
            drED.Item("periodunkid") = CInt(cboPeriod.SelectedValue)

            'Nilay (10-Feb-2016) -- Start
            'drED.Item("start_date") = eZeeDate.convertDate(Me.ViewState("StartDate"))
            'drED.Item("end_date") = eZeeDate.convertDate(Me.ViewState("EndDate"))
            drED.Item("start_date") = eZeeDate.convertDate(mdtPeriodStartDate)
            drED.Item("end_date") = eZeeDate.convertDate(mdtPeriodEndDate)
            'Nilay (10-Feb-2016) -- End

            drED.Item("costcenterunkid") = CInt(cboCostCenter.SelectedValue)
            drED.Item("medicalrefno") = txtMedicalRefNo.Text.Trim()
            'Sohail (09 Nov 2013) -- Start
            'TRA - ENHANCEMENT
            drED.Item("membershiptranunkid") = 0
            drED.Item("disciplinefileunkid") = 0
            If dtpCumulativeStartDate.IsNull = False Then
                drED.Item("cumulative_startdate") = dtpCumulativeStartDate.GetDate.Date
            Else
                drED.Item("cumulative_startdate") = DBNull.Value
            End If
            If dtpStopDate.IsNull = False Then
                drED.Item("stop_date") = dtpStopDate.GetDate.Date
            Else
                drED.Item("stop_date") = DBNull.Value
            End If
            'Sohail (09 Nov 2013) -- End
            'Sohail (24 Aug 2019) -- Start
            'NMB Payroll UAT # TC016 - 76.1 - System should be able to accept future amendments (Future period) and prorate. It should be possible to set start date of the specific transaction and prorate based on the start date. It should also be possible to set start date of the future period.
            If dtpEDStartDate.IsNull = False Then
                drED.Item("edstart_date") = dtpEDStartDate.GetDate.Date
            Else
                drED.Item("edstart_date") = DBNull.Value
            End If
            'Sohail (24 Aug 2019) -- End

            drED.Item("AUD") = "A"
            mdtSingleED.Rows.Add(drED)
            Me.ViewState.Add("SingleED", mdtSingleED)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objMaster As New clsMasterData
        Dim objTranHead As New clsTransactionHead
        Dim objExRate As New clsExchangeRate
        Dim objCostCenter As New clscostcenter_master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try

            Call FillCombo_Employee()



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True, 1, , , Session("Database_Name"))
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True, 1)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()
            End With

            dsCombo = objMaster.getComboListForHeadType("HeadType")
            With cboTrnHeadType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("HeadType")
                .DataBind()
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
            cboTrnHeadType_SelectedIndexChanged(New Object(), New EventArgs())

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dsCombo = objTranHead.getComboList("TranHead", True, 0, 0, -1, , True)

            'S.SANDEEP [12-JUL-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            'dsCombo = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, 0, 0, -1, , True)
            If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                dsCombo = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, 0, 0, -1, , True, "", False, False, False, 0)
            Else
            dsCombo = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, 0, 0, -1, , True)
            End If
            'S.SANDEEP [12-JUL-2018] -- END
            

            'Sohail (03 Feb 2016) -- End
            dtTable = New DataView(dsCombo.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            With cboTrnHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                If .Items.Count > 0 Then .SelectedValue = "0"
            End With
            cboTrnHead_SelectedIndexChanged(New Object(), New EventArgs())
           
            dsCombo = objCostCenter.getComboList("CostCenter", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dsCombo.Tables("CostCenter")
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
        Finally
            objEmployee = Nothing
            objMaster = Nothing
            objExRate = Nothing
            objCostCenter = Nothing
            objPeriod = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            If IsNothing(dtTable) = False Then
                dtTable.Clear()
                dtTable = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub FillCombo_Employee()
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , Me.ViewState("StartDate"), Me.ViewState("EndDate"))

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                   Session("UserId"), _
            '                                   Session("Fin_year"), _
            '                                   Session("CompanyUnkId"), _
            '                                   Me.ViewState("StartDate"), _
            '                                   Me.ViewState("EndDate"), _
            '                                   Session("UserAccessModeSetting"), True, _
            '                                   Session("IsIncludeInactiveEmp"), "EmployeeList", True)

            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 mdtPeriodStartDate, _
                                                 mdtPeriodEndDate, _
                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                 False, "EmployeeList", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("EmployeeList")
                .DataBind()
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError("FillComboEmployee :- " & ex.Message, Me)
        Finally
            objEmployee = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub Save()
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            If IsValidate() = False Then Exit Sub
            Call SetValue(objED)

            Dim mdtSingleED As DataTable = CType(Me.ViewState("SingleED"), DataTable)
            Dim blnFlag As Boolean = False

            Blank_ModuleName()
            objED._WebFormName = "frmEarningDeduction_AddEdit"
            'StrModuleName2 = "mnuPayroll"
            'objED._WebClientIP = CStr(Session("IP_ADD"))
            'objED._WebHostName = CStr(Session("HOST_NAME"))
            'objED._DatabaseName = CStr(Session("Database_Name")) 'Sohail (03 Dec 2013)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
            '    blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, True, False, chkCopyPreviousEDSlab.Checked, Session("UserId"), Session("AllowToApproveEarningDeduction"), chkOverwritePrevEDSlabHeads.Checked)
            'Else
            '    blnFlag = objED.InsertAllByDataTable(cboEmployee.SelectedValue.ToString, mdtSingleED, False, True, chkCopyPreviousEDSlab.Checked, Session("UserId"), Session("AllowToApproveEarningDeduction"), chkOverwritePrevEDSlabHeads.Checked)
            'End If

            'Nilay (10-Feb-2016) -- Start
            'If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
            '    blnFlag = objED.InsertAllByDataTable(Session("Database_Name"), _
            '                                         Session("Fin_year"), _
            '                                         Session("CompanyUnkId"), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         Session("UserAccessModeSetting"), True, _
            '                                         Session("IsIncludeInactiveEmp"), _
            '                                         cboEmployee.SelectedValue.ToString, mdtSingleED, _
            '                                         chkOverwritePrevEDSlabHeads.Checked, Session("UserId"), _
            '                                         Session("AllowToApproveEarningDeduction"), _
            '                                         ConfigParameter._Object._CurrentDateAndTime, False, _
            '                                         chkCopyPreviousEDSlab.Checked, , True, "")
            'Else
            '    blnFlag = objED.InsertAllByDataTable(Session("Database_Name"), _
            '                                         Session("Fin_year"), _
            '                                         Session("CompanyUnkId"), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         Session("UserAccessModeSetting"), True, _
            '                                         Session("IsIncludeInactiveEmp"), _
            '                                         cboEmployee.SelectedValue.ToString, mdtSingleED, _
            '                                         chkOverwritePrevEDSlabHeads.Checked, Session("UserId"), _
            '                                         Session("AllowToApproveEarningDeduction"), _
            '                                         ConfigParameter._Object._CurrentDateAndTime, , chkCopyPreviousEDSlab.Checked, , True, "")
            'End If

            With objED
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Gloab Assign ED screen in 75.1.
                'blnFlag = objED.InsertAllByDataTable(Session("Database_Name").ToString, _
                '                                     CInt(Session("Fin_year")), _
                '                                     CInt(Session("CompanyUnkId")), _
                '                                     mdtPeriodStartDate, _
                '                                     mdtPeriodEndDate, _
                '                                     Session("UserAccessModeSetting").ToString, True, _
                '                                     CBool(Session("IsIncludeInactiveEmp")), _
                '                                     cboEmployee.SelectedValue.ToString, mdtSingleED, _
                '                                     True, CInt(Session("UserId")), _
                '                                     CBool(Session("AllowToApproveEarningDeduction")), _
                '                                     ConfigParameter._Object._CurrentDateAndTime, False, _
                '                                     chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                blnFlag = objED.InsertAllByDataTable(Session("Database_Name").ToString, _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     mdtPeriodStartDate, _
                                                     mdtPeriodEndDate, _
                                                     Session("UserAccessModeSetting").ToString, True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                     Nothing, _
                                                     cboEmployee.SelectedValue.ToString, mdtSingleED, _
                                                     True, CInt(Session("UserId")), _
                                                     CBool(Session("AllowToApproveEarningDeduction")), _
                                                     ConfigParameter._Object._CurrentDateAndTime, False, _
                                                     chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (15 Dec 2018) -- End
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                If blnFlag = True Then
                    If CBool(Session("AllowToApproveEarningDeduction")) = False AndAlso mdtSingleED.Select("calctype_id = " & enCalcType.FlatRate_Others & " ").Length > 0 Then
                        If mdtSingleED.Columns.Contains("trnheadname") = True Then
                            mdtSingleED.Columns("trnheadname").ColumnName = "tranheadname"
                        End If
                        Call objED.SendMailToApprover(Session("Database_Name").ToString, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), CInt(Session("UserId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, False, CBool(Session("AllowToApproveEarningDeduction")), mdtSingleED, CInt(cboPeriod.SelectedValue), Session("fmtcurrency").ToString, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                    End If
                End If
                'Sohail (31 Oct 2019) -- End
            Else
                'Sohail (15 Dec 2018) -- Start
                'HJFMRI Enhancement - Copy Selected Period ED Slab option on Gloab Assign ED screen in 75.1.
                blnFlag = objED.InsertAllByDataTable(Session("Database_Name").ToString, _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     mdtPeriodStartDate, _
                                                     mdtPeriodEndDate, _
                                                     Session("UserAccessModeSetting").ToString, True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                     Nothing, _
                                                     cboEmployee.SelectedValue.ToString, mdtSingleED, _
                                                     False, CInt(Session("UserId")), _
                                                     CBool(Session("AllowToApproveEarningDeduction")), _
                                                     ConfigParameter._Object._CurrentDateAndTime, True, chkCopyPreviousEDSlab.Checked, chkOverwritePrevEDSlabHeads.Checked, True, "")
                'Sohail (15 Dec 2018) -- End
                'Sohail (31 Oct 2019) -- Start
                'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                If blnFlag = True Then
                    If CBool(Session("AllowToApproveEarningDeduction")) = False AndAlso mdtSingleED.Select("calctype_id = " & enCalcType.FlatRate_Others & " ").Length > 0 Then
                        If mdtSingleED.Columns.Contains("trnheadname") = True Then
                            mdtSingleED.Columns("trnheadname").ColumnName = "tranheadname"
                        End If
                        Call objED.SendMailToApprover(Session("Database_Name").ToString, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), CInt(Session("UserId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, False, CBool(Session("AllowToApproveEarningDeduction")), mdtSingleED, CInt(cboPeriod.SelectedValue), Session("fmtcurrency").ToString, enLogin_Mode.DESKTOP, mstrModuleName, 0)
                    End If
                End If
                'Sohail (31 Oct 2019) -- End
            End If
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            If blnFlag AndAlso (Session("EDId") Is Nothing AndAlso CInt(Session("EDId")) <= 0) Then
                Dim mintPeriodID As Integer = CInt(cboPeriod.SelectedValue)
                Dim mintEmpId As Integer = CInt(cboEmployee.SelectedValue)
                objED = Nothing
                objED = New clsEarningDeduction
                Me.ViewState("SingleED") = Nothing
                Me.ViewState("ED") = Nothing
                Me.ViewState("StartDate") = Nothing
                Me.ViewState("EndDate") = Nothing
                Call GetValue(objED)
                cboPeriod.SelectedValue = CStr(mintPeriodID)
                cboEmployee.SelectedValue = CStr(mintEmpId)
            ElseIf Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                Session("EDId") = Nothing
                'Nilay (10-Feb-2016) -- Start
                'Response.Redirect(Session("servername").ToString & "~/Payroll/wPg_EarningDeductionList.aspx", False)
                Response.Redirect(Session("rootpath").ToString & "Payroll/wPg_EarningDeductionList.aspx", False)
                'Nilay (10-Feb-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError("Save :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            objED = New clsEarningDeduction()
            If Me.IsPostBack = False Then
                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call FillCombo()
                chkCopyPreviousEDSlab_CheckedChanged(New Object(), New EventArgs())

                If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                    objED._Edunkid(Nothing, CStr(Session("Database_Name"))) = CInt(Session("EDId"))
                    cboEmployee.Enabled = False
                    cboTrnHeadType.Enabled = False
                    cboTypeOf.Enabled = False
                    cboTrnHead.Enabled = False
                    cboPeriod.Enabled = False 
                    chkCopyPreviousEDSlab.Checked = False
                    chkCopyPreviousEDSlab.Visible = False
                End If
                GetValue(objED)

                'Nilay (10-Feb-2016) -- Start
            Else
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                'Nilay (10-Feb-2016) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Nilay (10-Feb-2016) -- Start
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
        End Try
    End Sub
    'Nilay (10-Feb-2016) -- End

#End Region

#Region "Button Event"

    Protected Sub popupFormulaAssign_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFormulaAssign.buttonYes_Click
        Try
            Dim objTranHead As New clsTransactionHead
            objTranHead = New clsTransactionHead
            objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(cboTrnHead.SelectedValue)
            If objTranHead._Formulaid.Trim = "" AndAlso (objTranHead._Calctype_Id = enCalcType.AsComputedValue OrElse objTranHead._Calctype_Id = enCalcType.AsComputedOnWithINEXCESSOFTaxSlab) Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 8, "Formula is not set on this Trnasction Head. Please set formula from Transaction Head Form."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Formula is not set on this Trnasction Head. Please set formula from Transaction Head Form."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            Dim dRow As DataRow()
            Dim mdtTable As DataTable = CType(Me.ViewState("ED"), DataTable)
            If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then

                'Nilay (10-Feb-2016) -- Start
                'dRow = mdtTable.Select("edunkid <> " & CInt(Session("EDId")) & " AND AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date = '" & eZeeDate.convertDate(Me.ViewState("EndDate")) & "' ")
                dRow = mdtTable.Select("edunkid <> " & CInt(Session("EDId")) & " AND AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date = '" & mdtPeriodEndDate & "' ")
                'Nilay (10-Feb-2016) -- End
            Else
                'Nilay (10-Feb-2016) -- Start
                'dRow = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date = '" & eZeeDate.convertDate(Me.ViewState("EndDate")) & "' ")
                dRow = mdtTable.Select("AUD <> 'D' AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " AND tranheadunkid = " & CInt(cboTrnHead.SelectedValue) & " AND end_date = '" & mdtPeriodEndDate & "' ")
                'Nilay (10-Feb-2016) -- End
            End If
            If dRow.Length > 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(Language.getMessage(mstrModuleName, 10, "Sorry! This Transaction Head for this Period is already defined.."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry! This Transaction Head for this Period is already defined.."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            If (Session("EDId") Is Nothing AndAlso CInt(Session("EDId")) <= 0) AndAlso chkCopyPreviousEDSlab.Checked = True Then
                'If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle))) = Windows.Forms.DialogResult.No Then
                '    Return False
                'End If
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                popupPreviousED.Message = Language.getMessage(mstrModuleName, 12, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
                'Anjan [04 June 2014] -- End
                popupPreviousED.Show()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupFormulaAssign_buttonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupFormulaAssign_buttonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupPreviousED_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupPreviousED.buttonYes_Click
        Try
            Save()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupPreviousED_buttonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupPreviousED_buttonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveEDHistory.Click
        Try
            Save()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("EDId") = Nothing

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx")

            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect("~\Payroll\wPg_EarningDeductionList.aspx")
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Response.Redirect(Session("rootpath").ToString & "Payroll\wPg_EarningDeductionList.aspx")
            Response.Redirect(Session("rootpath").ToString & "Payroll\wPg_EarningDeductionList.aspx", False)
            'Sohail (31 Dec 2019) -- End
            'Nilay (10-Feb-2016) -- End

            'SHANI [09 Mar 2015]--END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                'Nilay (10-Feb-2016) -- Start
                'Me.ViewState.Add("StartDate", objPeriod._Start_Date)
                'Me.ViewState.Add("EndDate", objPeriod._End_Date)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
                'Nilay (10-Feb-2016) -- End
                Call FillCombo_Employee()
                'Nilay (10-Feb-2016) -- Start
            Else
                mdtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (10-Feb-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPeriod_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Dim objCC As New clscostcenter_master
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            'lblDefCC.Text = ""
            If CInt(cboEmployee.SelectedValue) > 0 Then
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
                objCC._Costcenterunkid = objEmployee._Costcenterunkid

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'lblDefCC.Text = "Default Cost Center : " & objCC._Costcentername
                lblDefCC.Text = Language.getMessage(mstrModuleName, 17, "Default Cost Center :") & objCC._Costcentername
                'Anjan [04 June 2014] -- End

                objED._Employeeunkid(CStr(Session("Database_Name"))) = CInt(cboEmployee.SelectedValue)
                Me.ViewState.Add("ED", objED._DataSource)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboEmployee_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objEmployee = Nothing
            objCC = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objMaster.getComboListTypeOf("TypeOf", CInt(cboTrnHeadType.SelectedValue))
            dtTable = New DataView(dsList.Tables("TypeOf"), "Id <>  " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads

            With cboTypeOf
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                If .Items.Count > 0 Then .SelectedValue = "0"
            End With
            
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboTrnHeadType_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError("cboTrnHeadType_SelectedIndexChanged :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objMaster = Nothing
            dsList = Nothing
            dtTable = Nothing
        End Try
    End Sub

    Private Sub cboTypeOf_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTypeOf.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)

            'S.SANDEEP [12-JUL-2018] -- START
            'dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            If Session("EDId") IsNot Nothing AndAlso CInt(Session("EDId")) > 0 Then
                dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True, "", False, False, False, 0)
            Else
            dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, CInt(cboTrnHeadType.SelectedValue), , CInt(IIf(CInt(cboTypeOf.SelectedValue) > 0, CInt(cboTypeOf.SelectedValue), -1)), , True)
            End If
            'S.SANDEEP [12-JUL-2018] -- END


            'Sohail (03 Feb 2016) -- End
            dtTable = New DataView(dsList.Tables("TranHead"), "typeof_id <> " & enTypeOf.Salary & "", "", DataViewRowState.CurrentRows).ToTable 'Except Salary Heads
            With cboTrnHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                If .Items.Count > 0 Then .SelectedValue = "0"
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboTypeOf_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboTypeOf_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objTranHead = Nothing
            dsList = Nothing
        End Try
    End Sub

    Private Sub cboTrnHead_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHead.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Try
            objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(cboTrnHead.SelectedValue)
            If objTranHead._Calctype_Id = enCalcType.FlatRate_Others Then
                txtAmount.Enabled = True
            Else
                txtAmount.Enabled = False
                txtAmount.Text = "0.0"
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboTrnHead_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError("cboTrnHead_SelectedIndexChanged :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objTranHead = Nothing
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkCopyPreviousEDSlab_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCopyPreviousEDSlab.CheckedChanged
        Try
            If chkCopyPreviousEDSlab.Checked = False Then chkOverwritePrevEDSlabHeads.Checked = False
            chkOverwritePrevEDSlabHeads.Enabled = chkCopyPreviousEDSlab.Checked
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkCopyPreviousEDSlab_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkCopyPreviousEDSlab_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Session("EDId") = Nothing
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region
    
  
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.btnSaveEDHistory.Text = Language._Object.getCaption(Me.btnSaveEDHistory.ID, Me.btnSaveEDHistory.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblTrnHead.Text = Language._Object.getCaption(Me.lblTrnHead.ID, Me.lblTrnHead.Text)
            Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.ID, Me.lblTrnHeadType.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblTypeOf.Text = Language._Object.getCaption(Me.lblTypeOf.ID, Me.lblTypeOf.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Me.lblDefCC.Text = Language._Object.getCaption(Me.lblDefCC.ID, Me.lblDefCC.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.chkCopyPreviousEDSlab.Text = Language._Object.getCaption(Me.chkCopyPreviousEDSlab.ID, Me.chkCopyPreviousEDSlab.Text)

            Me.lblMedicalRefNo.Text = Language._Object.getCaption(Me.lblMedicalRefNo.ID, Me.lblMedicalRefNo.Text)
            Me.chkOverwritePrevEDSlabHeads.Text = Language._Object.getCaption(Me.chkOverwritePrevEDSlabHeads.ID, Me.chkOverwritePrevEDSlabHeads.Text)
            Me.lblCumulativeStartDate.Text = Language._Object.getCaption(Me.lblCumulativeStartDate.ID, Me.lblCumulativeStartDate.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.ID, Me.lblStopDate.Text)
            Me.lblEDStartDate.Text = Language._Object.getCaption(Me.lblEDStartDate.ID, Me.lblEDStartDate.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
