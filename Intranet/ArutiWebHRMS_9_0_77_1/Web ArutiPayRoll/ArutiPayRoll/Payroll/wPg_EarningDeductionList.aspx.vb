﻿Option Strict On 'Nilay (10-Feb-2016)

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO
Imports System.Data
Imports System.Drawing

Partial Class Payroll_wPg_EarningDeductionList
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Private objED As clsEarningDeduction
    'Hemant (04 Sep 2020) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEarningDeductionList"
    'Anjan [04 June 2014 ] -- End

    'Pinkal (16-Apr-2016) -- Start
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Private ReadOnly mstrModuleName1 As String = "frmEarningDeduction_AddEdit"
    'Pinkal (16-Apr-2016) -- End

    Private mstrAdvanceFilter As String = ""
    'Nilay (10-Feb-2016) -- Start
    Private mdtPeriodStartDate As Date
    Private mdtPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            objED = New clsEarningDeduction()
            If Me.IsPostBack = False Then
                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call FillComBo()
                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'FillList()
                'Sohail (02 Sep 2019) -- End


                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    dgED.Columns(1).Visible = CBool(Session("EditEarningDeduction"))
                    dgED.Columns(2).Visible = CBool(Session("DeleteEarningDeduction"))
                    btnApprove.Visible = CBool(Session("AllowToApproveEarningDeduction"))
                    btnDisApprove.Visible = CBool(Session("AllowToApproveEarningDeduction"))
                End If
                'Pinkal (12-Feb-2015) -- End

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("EarningDedc_EmpUnkID") IsNot Nothing Then
                    cboEmployee.SelectedValue = CStr(Session("EarningDedc_EmpUnkID"))
                    Session.Remove("EarningDedc_EmpUnkID")
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END 
            Else
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter"))
                'Nilay (10-Feb-2016) -- Start
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                'Nilay (10-Feb-2016) -- End
                'Sohail (13 Feb 2020) -- Start
                'POWERSOFT Enhancement # 0004519 : To see total amount for checked employees before approving flat rate earning deductions.
                objlblTotalAmt.Text = Request.Form(hfobjlblTotalAmt.UniqueID)
                'Sohail (13 Feb 2020) -- End
            End If

            If CInt(cboEDApprovalStatus.SelectedValue) <= 0 Then
                btnApprove.Visible = False
                btnDisApprove.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'Nilay (10-Feb-2016) -- Start
        'Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
        Try
            Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
        End Try
        'Nilay (10-Feb-2016) -- End
    End Sub
#End Region

#Region " Private Methods "

    Private Sub FillComBo()
        Dim objCommonMaster As New clsMasterData
        'Dim objDept As New clsDepartment
        'Dim objGrade As New clsGrade
        'Dim objSection As New clsSections
        'Dim objClass As New clsClass
        'Dim objCostCenter As New clscostcenter_master
        'Dim objJob As New clsJobs
        'Dim objPayPoint As New clspaypoint_master
        'Dim objUnit As New clsUnits
        Dim dsList As DataSet
        Dim objEmployee As New clsEmployee_Master
        'Dim objTranHead As New clsTransactionHead 'Sohail (30 Oct 2019)
        Dim objPeriod As New clscommom_period_Tran

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("EmployeeList", True)
            'End If

            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 CStr(Session("UserAccessModeSetting")), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True)

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("EmployeeList")
                .DataBind()
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            'dsList = objDept.getComboList("DepartmentList", True)
            'With cboDepartment
            '    .DataValueField = "departmentunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("DepartmentList")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'dsList = objGrade.getComboList("GradeList", True)
            'With cboGrade
            '    .DataValueField = "gradeunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("GradeList")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'dsList = objSection.getComboList("SectionList", True)
            'With cboSections
            '    .DataValueField = "sectionunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("SectionList")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'dsList = objCostCenter.getComboList("CostCenterList", True)
            'With cboCostCenter
            '    .DataValueField = "CostCenterunkid"
            '    .DataTextField = "CostCentername"
            '    .DataSource = dsList.Tables("CostCenterList")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'dsList = objJob.getComboList("JobList", True)
            'With cboJob
            '    .DataValueField = "jobunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("JobList")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'dsList = objPayPoint.getListForCombo("PayPointList", True)
            'With cboPayPoint
            '    .DataValueField = "paypointunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables("PayPointList")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedIndex = 0
            'End With

            'Changes : New filter Transaction Head added. New field 'IsApproved' added.
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dsList = objTranHead.getComboList("TranHead", True)

            'Sohail (30 Oct 2019) -- Start
            'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
            ''S.SANDEEP [12-JUL-2018] -- START
            ''ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
            ''dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True)
            'dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, 0, 0, -1, False, False, "", False, False, False, 0)
            ''S.SANDEEP [12-JUL-2018] -- END


            ''Sohail (03 Feb 2016) -- End
            'With cboTranHead
            '    .DataValueField = "tranheadunkid"
            '    .DataTextField = "Name"
            '    .DataSource = dsList.Tables("TranHead")
            '    .DataBind()
            '    If .Items.Count > 0 Then .SelectedValue = "0"
            'End With
            dsList = objCommonMaster.getComboListCalcType("List", , True)
            With cboCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
                Call cboCalcType_SelectedIndexChanged(cboCalcType, New System.EventArgs)
            End With
            'Sohail (30 Oct 2019) -- End

            dsList = objCommonMaster.getComboListForEDHeadApprovalStatus("EdApproval", True)
            With cboEDApprovalStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = New DataView(dsList.Tables("EdApproval"), "Id <> " & enEDHeadApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                .DataBind()
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True, , , , Session("Database_Name"))
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
            End With
            'Nilay (10-Feb-2016) -- Start
            'cboPeriod.SelectedValue = objCommonMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date, 0, Session("Fin_year"))
            cboPeriod.SelectedValue = CStr(objCommonMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1))
            'Nilay (10-Feb-2016) -- End
            cboPeriod_SelectedIndexChanged(New Object(), New EventArgs())

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillComBo :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillComBo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objCommonMaster = Nothing
            objEmployee = Nothing
            'objDept = Nothing
            'objGrade = Nothing
            'objSection = Nothing
            'objClass = Nothing
            'objCostCenter = Nothing
            'objJob = Nothing
            'objPayPoint = Nothing
            'objUnit = Nothing
            'objTranHead = Nothing 'Sohail (30 Oct 2019)
            objPeriod = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet = Nothing
        Dim objTranHead As New clsTransactionHead
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try

            'Sohail (11 Mar 2020) -- Start
            'Eko Supreme Enhancement # 0004596 : Allow to approve / reject all flat rate heads at a time on earning and deduction list screen in self service.
            objlblTotalAmt.Text = Format(0, CStr(Session("fmtcurrency")))
            'Sohail (11 Mar 2020) -- End

            If CBool(Session("AllowToViewEmpEDList")) = True Then

                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'Sohail (11 Mar 2020) -- Start
                'Eko Supreme Enhancement # 0004596 : Allow to approve / reject all flat rate heads at a time on earning and deduction list screen in self service.
                'If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboTranHead.SelectedValue) <= 0 AndAlso CInt(cboEDApprovalStatus.SelectedValue) <> enEDHeadApprovalStatus.Pending Then
                If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboTranHead.SelectedValue) <= 0 AndAlso CInt(cboEDApprovalStatus.SelectedValue) <> enEDHeadApprovalStatus.Pending AndAlso CInt(cboCalcType.SelectedValue) <= 0 Then
                    'Sohail (11 Mar 2020) -- End
                    Exit Try
                End If
                'Sohail (31 Dec 2019) -- End

                'S.SANDEEP [12-JUL-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#223|ARUTI-239}
                'Dim dsTranHead As DataSet = objTranHead.GetList("TranHead", , , True)
                Dim dsTranHead As DataSet = objTranHead.GetList("TranHead", , , True, False, -1, False, "", False, False, False, 0)
                'S.SANDEEP [12-JUL-2018] -- END

                Me.ViewState.Add("TranHead", dsTranHead)

                'Sohail (30 Oct 2019) -- Start
                'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
                Dim strFilter As String = ""
                If CInt(cboCalcType.SelectedValue) > 0 Then
                    strFilter = " AND prtranhead_master.calctype_id = " & CInt(cboCalcType.SelectedValue) & " "
                End If
                'Sohail (30 Oct 2019) -- End

                'If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then 'Sohail (02 Sep 2019)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'dsList = objED.GetList("ED", cboEmployee.SelectedValue.ToString, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    0, _
                '                    "employeename, employeeunkid, end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC", _
                '                     CInt(cboPeriod.SelectedValue), _
                '                     CInt(cboEDApprovalStatus.SelectedValue), _
                '                    CInt(cboTranHead.SelectedValue), _
                '                    chkIncludeInactiveEmployee.Checked _
                '                    , mstrAdvanceFilter _
                '                    , Me.ViewState("EndDate"))

                'Nilay (10-Feb-2016) -- Start
                'dsList = objED.GetList(Session("Database_Name"), _
                '                       Session("UserId"), _
                '                       Session("Fin_year"), _
                '                       Session("CompanyUnkId"), _
                '                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                       Session("UserAccessModeSetting"), True, _
                '                       chkIncludeInactiveEmployee.Checked, "ED", True, _
                '                       cboEmployee.SelectedValue.ToString, _
                '                       "employeename, employeeunkid, end_date DESC, trnheadtype_id, typeof_id, calctype_id DESC", _
                '                       CInt(cboPeriod.SelectedValue), _
                '                       CInt(cboEDApprovalStatus.SelectedValue), _
                '                       CInt(cboTranHead.SelectedValue), _
                '                       mstrAdvanceFilter, _
                '                       Me.ViewState("EndDate"))

                dsList = objED.GetList(CStr(Session("Database_Name")), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       mdtPeriodStartDate, _
                                       mdtPeriodEndDate, _
                                       CStr(Session("UserAccessModeSetting")), True, _
                                       chkIncludeInactiveEmployee.Checked, "ED", True, _
                                       cboEmployee.SelectedValue.ToString, _
                                       "", _
                                       CInt(cboPeriod.SelectedValue), _
                                       CInt(cboEDApprovalStatus.SelectedValue), _
                                       CInt(cboTranHead.SelectedValue), _
                                       mstrAdvanceFilter, _
                               mdtPeriodEndDate, blnAddGrouping:=True, strFilter:=strFilter)
                'Sohail (12 Oct 2021) - [Removed strSortField:employeename, prearningdeduction_master.employeeunkid, cfcommon_period_tran.end_date DESC, prearningdeduction_master.trnheadtype_id, prearningdeduction_master.typeof_id, prearningdeduction_master.calctype_id DESC]
                'Sohail (30 Oct 2019) - [strFilter]
                'Sohail (02 Sep 2019) - blnAddGrouping:=True)
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

                'If CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Approved Then
                '    btnDisApprove.Visible = True
                '    btnApprove.Visible = False
                'ElseIf CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Pending Then
                '    btnApprove.Visible = True
                '    btnDisApprove.Visible = False
                'End If

                If CInt(Session("LoginBy")) = Global.User.en_loginby.User AndAlso CBool(Session("AllowToApproveEarningDeduction")) Then
                    If CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Approved Then
                        'Sohail (06 Sep 2019) -- Start
                        'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                        'btnDisApprove.Visible = True
                        btnDisApprove.Visible = False
                        'Sohail (06 Sep 2019) -- End
                        btnApprove.Visible = False
                    ElseIf CInt(cboEDApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Pending Then
                        btnApprove.Visible = True
                        'Sohail (06 Sep 2019) -- Start
                        'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                        'btnDisApprove.Visible = False
                        btnDisApprove.Visible = True
                        'Sohail (06 Sep 2019) -- End
                    End If
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.User AndAlso CBool(Session("AllowToApproveEarningDeduction")) = False Then
                    btnApprove.Visible = False
                    btnDisApprove.Visible = False
                End If

                'Pinkal (12-Feb-2015) -- End

                'End If'Sohail (02 Sep 2019)

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then
                dsList = New DataSet()
                dsList.Tables.Add("ED")
                dsList.Tables(0).Columns.Add("trnheadcode", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("trnheadname", Type.GetType("System.String"))
                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'dsList.Tables(0).Columns.Add("trnheadtype_id", Type.GetType("System.String"))
                'dsList.Tables(0).Columns.Add("typeof_id", Type.GetType("System.String"))
                'dsList.Tables(0).Columns.Add("calctype_id", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("trnheadtype_id", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("typeof_id", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("calctype_id", Type.GetType("System.Int32"))
                'Sohail (02 Sep 2019) -- End
                dsList.Tables(0).Columns.Add("period_name", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("amount", Type.GetType("System.Decimal"))
                dsList.Tables(0).Columns.Add("isapproved", Type.GetType("System.Boolean"))
                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'dsList.Tables(0).Columns.Add("trnheadtype", Type.GetType("System.Int32"))
                'dsList.Tables(0).Columns.Add("typeof", Type.GetType("System.Int32"))
                'dsList.Tables(0).Columns.Add("calctype", Type.GetType("System.Int32"))
                dsList.Tables(0).Columns.Add("trnheadtype_name", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("typeof_name", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("calctype_name", Type.GetType("System.String"))
                dsList.Tables(0).Columns.Add("edunkid", Type.GetType("System.Int32")).DefaultValue = -1
                dsList.Tables(0).Columns.Add("IsGrp", Type.GetType("System.Boolean")).DefaultValue = False
                'Sohail (02 Sep 2019) -- End
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                dsList.Tables(0).Columns.Add("employeeunkid", Type.GetType("System.Int32")).DefaultValue = -1
                dsList.Tables(0).Columns.Add("tranheadunkid", Type.GetType("System.Int32")).DefaultValue = -1
                dsList.Tables(0).Columns.Add("periodunkid", Type.GetType("System.Int32")).DefaultValue = -1
                dsList.Tables(0).Columns.Add("membershiptranunkid", Type.GetType("System.Int32")).DefaultValue = -1
                dsList.Tables(0).Columns.Add("userunkid", Type.GetType("System.Int32")).DefaultValue = -1
                dsList.Tables(0).Columns.Add("employeename", Type.GetType("System.String")).DefaultValue = ""
                'Sohail (31 Dec 2019) -- End

                Dim drRow As DataRow = dsList.Tables(0).NewRow
                drRow("trnheadcode") = "None"
                drRow("trnheadname") = "None"
                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'drRow("trnheadtype_id") = "None"
                'drRow("typeof_id") = "None"
                'drRow("calctype_id") = "None"
                drRow("trnheadtype_name") = "None"
                drRow("typeof_name") = "None"
                drRow("calctype_name") = "None"
                'Sohail (02 Sep 2019) -- End
                drRow("period_name") = "None"
                drRow("amount") = "0"
                drRow("isapproved") = "False"
                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'drRow("trnheadtype") = -1
                'drRow("typeof") = -1
                'drRow("calctype") = -1
                drRow("trnheadtype_id") = -1
                drRow("typeof_id") = -1
                drRow("calctype_id") = -1
                'Sohail (02 Sep 2019) -- End
                dsList.Tables(0).Rows.Add(drRow)

            Else
                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'Dim dc As DataColumn
                'dc = New DataColumn
                'dc.ColumnName = "trnheadtype"
                'dc.DataType = Type.GetType("System.Int32")
                'dc.DefaultValue = 0
                'dsList.Tables(0).Columns.Add(dc)

                'dc = New DataColumn
                'dc.ColumnName = "typeof"
                'dc.DataType = Type.GetType("System.Int32")
                'dc.DefaultValue = 0
                'dsList.Tables(0).Columns.Add(dc)

                'dc = New DataColumn
                'dc.ColumnName = "calctype"
                'dc.DataType = Type.GetType("System.Int32")
                'dc.DefaultValue = 0
                'dsList.Tables(0).Columns.Add(dc)
                'Sohail (02 Sep 2019) -- End
            End If

            If dsList.Tables(0).Columns.Contains("IsCheck") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "IsCheck"
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                dsList.Tables(0).Columns.Add(dc)
            End If

            dgED.DataSource = dsList.Tables("ED")
            dgED.DataBind()
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Me.ViewState.Add("ED", dsList.Tables("ED"))
            'Sohail (31 Dec 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'btnNew.Visible = Session("AddEarningDeduction")
            btnApprove.Visible = CBool(Session("AllowToApproveEarningDeduction"))
            btnDisApprove.Visible = CBool(Session("AllowToApproveEarningDeduction"))
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility :- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetVisibility :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            'If CInt(cboEmployee.SelectedValue) <= 0 Then

            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    Language.setLanguage(mstrModuleName)
            '    'Pinkal (16-Apr-2016) -- Start
            '    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            '    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select employee in order to do the futher operation on it."), Me)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Please select employee in order to do the futher operation on it."), Me)
            '    'Pinkal (16-Apr-2016) -- End
            '    'Anjan [04 June 2014] -- End
            '    Exit Sub
            'End If
            'Sohail (02 Sep 2019) -- End
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Sohail (11 Mar 2020) -- Start
            'Eko Supreme Enhancement # 0004596 : Allow to approve / reject all flat rate heads at a time on earning and deduction list screen in self service.
            'If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboTranHead.SelectedValue) <= 0 AndAlso CInt(cboEDApprovalStatus.SelectedValue) <> enEDHeadApprovalStatus.Pending Then
            '   DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Please select Employee / Transaction head in order to do the futher operation on it."), Me)
            If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboTranHead.SelectedValue) <= 0 AndAlso CInt(cboEDApprovalStatus.SelectedValue) <> enEDHeadApprovalStatus.Pending AndAlso CInt(cboCalcType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Please select Employee / Transaction head / Calculation type in order to do the futher operation on it."), Me)
                'Sohail (11 Mar 2020) -- End
                Exit Sub
            End If
            'Sohail (31 Dec 2019) -- End
            FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSearch_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = "0"
            'If cboDepartment.Items.Count > 0 Then cboDepartment.SelectedValue = 0
            'If cboGrade.Items.Count > 0 Then cboGrade.SelectedValue = 0
            'If cboSections.Items.Count > 0 Then cboSections.SelectedValue = 0
            'If cboCostCenter.Items.Count > 0 Then cboCostCenter.SelectedValue = 0
            'If cboJob.Items.Count > 0 Then cboJob.SelectedValue = 0
            'If cboPayPoint.Items.Count > 0 Then cboPayPoint.SelectedValue = 0
            If cboTranHead.Items.Count > 0 Then cboTranHead.SelectedValue = "0"
            If cboEDApprovalStatus.Items.Count > 0 Then cboEDApprovalStatus.SelectedIndex = 0
            btnApprove.Visible = False
            btnDisApprove.Visible = False
            mstrAdvanceFilter = ""
            FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnNew_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- Start
            'Manspring Support Issue Id # 0004197 : error occurs when the top box is ticked. i.e select all. (SqlException: Incorrect syntax near ,. // SqlErrorNumber:- 102; Procedure Name: isExistEDApproval)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True")
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim drRow() As DataRow = dtTab.Select("IsGrp = 0 AND Ischeck = True")
            Dim lst As List(Of GridViewRow) = (From r As GridViewRow In dgED.Rows.Cast(Of GridViewRow)() Where (DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True AndAlso CBool(dgED.DataKeys(r.RowIndex).Item("IsGrp")) = False) Select (r)).ToList
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- End

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'If drRow.Length <= 0 Then
            If lst.Count <= 0 Then
                'Sohail (31 Dec 2019) -- End
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please Check atleast one ED Heads to Approve/Disapprove."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If
            'popupApprove.Message = "Are you sure you want to Approve selected ED Heads?"

            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim intSalaryHeads As Integer = dtTab.Select("IsGrp = 0 AND IsCheck = 1 AND typeof_id = " & enTypeOf.Salary & " ").Length
            Dim intSalaryHeads As Integer = (From r As GridViewRow In lst Where (CInt(dgED.DataKeys(r.RowIndex).Item("typeof_id")) = enTypeOf.Salary) Select (r)).Count
            'Sohail (31 Dec 2019) -- End
            If intSalaryHeads > 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove Salary Head(s)."), Me)
                Exit Sub
            End If
            'Sohail (02 Sep 2019) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            'popupDisapprove.Message = Language.getMessage(mstrModuleName, 9, "Are you sure you want to Approve selected ED Heads?")
            popupApprove.Message = Language.getMessage(mstrModuleName, 9, "Are you sure you want to Approve selected ED Heads?")
            'Sohail (02 Sep 2019) -- End
            'Anjan [04 June 2014] -- End

            popupApprove.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprove_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnApprove_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupApprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupApprove.buttonYes_Click
        Try
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True")
            'Dim objED As New clsEarningDeduction
            'For i As Integer = 0 To drRow.Length - 1
            '    With objED

            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        '._Edunkid = drRow(i)("edunkid")
            '        ._Edunkid(Nothing, CStr(Session("Database_Name"))) = CInt(drRow(i)("edunkid"))
            '        'Shani(20-Nov-2015) -- End

            '        ._Isapproved = True
            '        ._Approveruserunkid = CInt(Session("UserId"))

            '        Blank_ModuleName()
            '        ._WebFormName = "frmEarningDeductionList"
            '        StrModuleName2 = "mnuPayroll"
            '        ._WebClientIP = CStr(Session("IP_ADD"))
            '        ._WebHostName = CStr(Session("HOST_NAME"))

            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'Dim blnResult As Boolean = .Update(False)
            '        Dim blnResult As Boolean = .Update(CStr(Session("Database_Name")), False, Nothing, ConfigParameter._Object._CurrentDateAndTime)
            '        'Shani(20-Nov-2015) -- End

            '        If blnResult = False AndAlso objED._Message <> "" Then
            '            DisplayMessage.DisplayMessage("popupApprove_buttonYes_Click :- " & ._Message, Me)
            '            Exit Try
            '        End If
            '    End With
            'Next
            'FillList()
            Language.setLanguage(mstrModuleName)
            popApproveReason.Reason = ""
            popApproveReason.Title = Language.getMessage(mstrModuleName, 26, "Comments")
            popApproveReason.Show()
            'Sohail (06 Sep 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupApprove_buttonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupApprove_buttonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnDisapprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisApprove.Click
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- Start
            'Manspring Support Issue Id # 0004197 : error occurs when the top box is ticked. i.e select all. (SqlException: Incorrect syntax near ,. // SqlErrorNumber:- 102; Procedure Name: isExistEDApproval)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True")
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim drRow() As DataRow = dtTab.Select("IsGrp = 0 AND Ischeck = True")
            Dim lst As List(Of GridViewRow) = (From r As GridViewRow In dgED.Rows.Cast(Of GridViewRow)() Where (DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True AndAlso CBool(dgED.DataKeys(r.RowIndex).Item("IsGrp")) = False) Select (r)).ToList
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- End

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'If drRow.Length <= 0 Then
            If lst.Count <= 0 Then
                'Sohail (31 Dec 2019) -- End
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please Check atleast one ED Heads to Approve/Disapprove."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            'Sohail (02 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
            '1. Set ED in pending status if user has not priviledge of ED approval
            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
            '3. Send notifications to ED approvers for all employees ED in one email
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim intSalaryHeads As Integer = dtTab.Select("IsGrp = 0 AND IsCheck = 1 AND typeof_id = " & enTypeOf.Salary & " ").Length
            Dim intSalaryHeads As Integer = (From r As GridViewRow In lst Where (CInt(dgED.DataKeys(r.RowIndex).Item("typeof_id")) = enTypeOf.Salary) Select (r)).Count
            'Sohail (31 Dec 2019) -- End
            If intSalaryHeads > 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove Salary Head(s)."), Me)
                Exit Sub
            End If
            'Sohail (02 Sep 2019) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            popupDisapprove.Message = Language.getMessage(mstrModuleName, 10, "Are you sure you want to Dispprove selected ED Heads?")
            'Anjan [04 June 2014] -- End


            popupDisapprove.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDisapprove_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnDisapprove_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDisapprove.buttonYes_Click
        Try
            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True")
            'Dim objED As New clsEarningDeduction
            'For i As Integer = 0 To drRow.Length - 1
            '    With objED

            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        '._Edunkid = drRow(i)("edunkid")
            '        ._Edunkid(Nothing, CStr(Session("Database_Name"))) = CInt(drRow(i)("edunkid"))
            '        'Shani(20-Nov-2015) -- End

            '        ._Isapproved = False
            '        ._Approveruserunkid = CInt(Session("UserId"))

            '        Blank_ModuleName()
            '        ._WebFormName = "frmEarningDeductionList"
            '        StrModuleName2 = "mnuPayroll"
            '        ._WebClientIP = CStr(Session("IP_ADD"))
            '        ._WebHostName = CStr(Session("HOST_NAME"))


            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'Dim blnResult As Boolean = .Update(False)
            '        Dim blnResult As Boolean = .Update(CStr(Session("Database_Name")), False, Nothing, ConfigParameter._Object._CurrentDateAndTime)
            '        'Shani(20-Nov-2015) -- End

            '        If blnResult = False AndAlso objED._Message <> "" Then
            '            DisplayMessage.DisplayMessage("popupDisapprove_buttonYes_Click :- " & ._Message, Me)
            '            Exit Try
            '        End If
            '    End With
            'Next
            'FillList()
            Language.setLanguage(mstrModuleName)
            popRejectReason.Reason = ""
            popRejectReason.Title = Language.getMessage(mstrModuleName, 26, "Comments")
            popRejectReason.Show()
            'Sohail (06 Sep 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDisapprove_buttonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDisapprove_buttonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            'Hemant (20 June 2018) -- Start
            'popupDelete.Reason = ""
            'Hemant (20 June 2018) -- End
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim dtED As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Sohail (31 Dec 2019) -- End
            Dim intRowIndex As Integer = CInt(Me.ViewState("RIndex"))

            'Blank_ModuleName()
            'objED._WebFormName = "frmEarningDeductionList"
            'StrModuleName2 = "mnuPayroll"
            'objED._WebClientIP = CStr(Session("IP_ADD"))
            'objED._WebHostName = CStr(Session("HOST_NAME"))

            'Sohail (06 Sep 2019) -- Start
            'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
            'If CInt(dtED.Rows(intRowIndex)("membershiptranunkid")) > 0 Then
            '    Dim iCurrPeriodUnkid As Integer = -1 : Dim objMData As New clsMasterData
            '    iCurrPeriodUnkid = objMData.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            '    Dim iEDUnkid As Integer = -1 : Dim objMTran As New clsMembershipTran
            '    iEDUnkid = objMTran.Get_Mem_EDUnkid(CInt(dtED.Rows(intRowIndex)("edunkid")), CInt(dtED.Rows(intRowIndex)("membershiptranunkid")), _
            '                                                                      CInt(dtED.Rows(intRowIndex)("periodunkid")), _
            '                                                                      CInt(dtED.Rows(intRowIndex)("employeeunkid")))

            '    If iCurrPeriodUnkid = CInt(dtED.Rows(intRowIndex)("periodunkid")) Then

            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'If objED.Void(CInt(dtED.Rows(intRowIndex)("edunkid")), Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason) = True Then
            '        'If objED.Void(iEDUnkid, Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason) = True Then
            '        If objED.Void(CInt(dtED.Rows(intRowIndex)("edunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason, CStr(Session("Database_Name"))) = True Then

            '            If objED.Void(iEDUnkid, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason, CStr(Session("Database_Name"))) = True Then
            '                'Shani(20-Nov-2015) -- End

            '                objMTran.Void_Membership(CInt(dtED.Rows(intRowIndex)("membershiptranunkid")), CInt(dtED.Rows(intRowIndex)("employeeunkid")))
            '            End If

            '        End If
            '    Else

            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'objED.Void(CInt(dtED.Rows(intRowIndex)("edunkid")), Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason)
            '        'objED.Void(iEDUnkid, Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason)
            '        objED.Void(CInt(dtED.Rows(intRowIndex)("edunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason, CStr(Session("Database_Name")))
            '        objED.Void(iEDUnkid, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason, CStr(Session("Database_Name")))
            '        'Shani(20-Nov-2015) -- End
            '    End If
            'Else

            '    'Shani(20-Nov-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'objED.Void(CInt(dtED.Rows(intRowIndex)("edunkid")), Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason)
            '    objED.Void(CInt(dtED.Rows(intRowIndex)("edunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelete.Reason, CStr(Session("Database_Name")))
            '    'Shani(20-Nov-2015) -- End

            'End If
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim drRow As DataRow = dtED.Rows(intRowIndex)
            'Dim dtTable As DataTable = dtED.Clone
            'If drRow IsNot Nothing Then
            '    dtTable.ImportRow(drRow)
            Dim dtTable As DataTable = Nothing
            Dim lst As New List(Of GridViewRow)
            lst.Add(dgED.Rows(intRowIndex))

            If lst IsNot Nothing Then
                dtTable = GridViewRowToDataTable(lst)
                'Sohail (31 Dec 2019) -- End

                If objED.VoidAll(Nothing, CInt(Session("Fin_year")), dtTable, popupDelete.Reason, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, mstrModuleName, -1, False) = False Then
                    If objED._Message <> "" Then
                        DisplayMessage.DisplayError(objED._Message, Me)
                        Exit Try
                    End If
                End If
            End If
            'Sohail (06 Sep 2019) -- End

            FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDelete_buttonDelReasonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDelete_buttonDelReasonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    'Sohail (06 Sep 2019) -- Start
    'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
    Protected Sub popApproveReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popApproveReason.buttonDelReasonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- Start
            'Manspring Support Issue Id # 0004197 : error occurs when the top box is ticked. i.e select all. (SqlException: Incorrect syntax near ,. // SqlErrorNumber:- 102; Procedure Name: isExistEDApproval)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True")
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim drRow() As DataRow = dtTab.Select("IsGrp = 0 AND Ischeck = True")
            Dim lst As List(Of GridViewRow) = (From r As GridViewRow In dgED.Rows.Cast(Of GridViewRow)() Where (DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True AndAlso CBool(dgED.DataKeys(r.RowIndex).Item("IsGrp")) = False) Select (r)).ToList
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- End
            Dim dtTable As DataTable = Nothing
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'If drRow.Length > 0 Then
            '    dtTable = drRow.CopyToDataTable
            If lst.Count > 0 Then
                dtTable = GridViewRowToDataTable(lst)
                'Sohail (31 Dec 2019) -- End

                If objED.ApproveAll(Nothing, dtTable, True, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, mstrModuleName, -1, False) = False Then
                    If objED._Message <> "" Then
                        DisplayMessage.DisplayMessage(objED._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList()

                    Call objED.SendMailToInitiator(Session("Database_Name").ToString, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), CInt(Session("UserId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, False, dtTable, True, popApproveReason.Reason, Session("fmtcurrency").ToString, enLogin_Mode.MGR_SELF_SERVICE, mstrModuleName, 0)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub popRejectReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popRejectReason.buttonDelReasonYes_Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- Start
            'Manspring Support Issue Id # 0004197 : error occurs when the top box is ticked. i.e select all. (SqlException: Incorrect syntax near ,. // SqlErrorNumber:- 102; Procedure Name: isExistEDApproval)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True")
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim drRow() As DataRow = dtTab.Select("IsGrp = 0 AND Ischeck = True")
            Dim lst As List(Of GridViewRow) = (From r As GridViewRow In dgED.Rows.Cast(Of GridViewRow)() Where (DirectCast(r.FindControl("chkSelect"), CheckBox).Checked = True AndAlso CBool(dgED.DataKeys(r.RowIndex).Item("IsGrp")) = False) Select (r)).ToList
            'Sohail (31 Dec 2019) -- End
            'Sohail (30 Sep 2019) -- End
            Dim dtTable As DataTable = Nothing
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'If drRow.Length > 0 Then
            '    dtTable = drRow.CopyToDataTable
            If lst.Count > 0 Then
                dtTable = GridViewRowToDataTable(lst)
                'Sohail (31 Dec 2019) -- End

                If objED.VoidAll(Nothing, CInt(Session("Fin_year")), dtTable, popRejectReason.Reason, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, mstrModuleName, -1, False) = False Then
                    If objED._Message <> "" Then
                        DisplayMessage.DisplayMessage(objED._Message, Me)
                        Exit Try
                    End If
                Else
                    Call FillList()

                    Call objED.SendMailToInitiator(Session("Database_Name").ToString, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), CInt(Session("UserId")), mdtPeriodStartDate, mdtPeriodEndDate, Session("UserAccessModeSetting").ToString, True, False, dtTable, False, popRejectReason.Reason, Session("fmtcurrency").ToString, enLogin_Mode.MGR_SELF_SERVICE, mstrModuleName, 0)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub
    'Sohail (06 Sep 2019) -- End

    'Sohail (31 Dec 2019) -- Start
    'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
    Private Function GridViewRowToDataTable(ByVal lst As List(Of GridViewRow)) As DataTable
        Dim dtTable As New DataTable
        Try
            dtTable.Columns.Add("edunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("membershiptranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("employeename", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("trnheadname", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = -1

            Dim dr As DataRow

            For Each r As GridViewRow In lst
                dr = dtTable.NewRow

                dr.Item("edunkid") = CInt(dgED.DataKeys(r.RowIndex).Item("edunkid"))
                dr.Item("employeeunkid") = CInt(dgED.DataKeys(r.RowIndex).Item("employeeunkid"))
                dr.Item("userunkid") = CInt(dgED.DataKeys(r.RowIndex).Item("userunkid"))
                dr.Item("periodunkid") = CInt(dgED.DataKeys(r.RowIndex).Item("periodunkid"))
                dr.Item("membershiptranunkid") = CInt(dgED.DataKeys(r.RowIndex).Item("membershiptranunkid"))
                dr.Item("employeename") = dgED.DataKeys(r.RowIndex).Item("employeename").ToString
                dr.Item("trnheadname") = dgED.DataKeys(r.RowIndex).Item("trnheadname").ToString
                dr.Item("amount") = CDec(dgED.DataKeys(r.RowIndex).Item("amount"))

                dtTable.Rows.Add(dr)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError("GridViewRowToDataTable :- " & ex.Message, Me)
        End Try
        Return dtTable
    End Function
    'Sohail (31 Dec 2019) -- End

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("EDId") = Nothing
            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect("~\UserHome.aspx")
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx")
            'Nilay (10-Feb-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

#End Region

#Region "ComboBox Event"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                'Nilay (10-Feb-2016) -- Start
                'Me.ViewState.Add("StartDate", objPeriod._Start_Date)
                'Me.ViewState.Add("EndDate", objPeriod._End_Date)
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
                'Nilay (10-Feb-2016) -- End
            Else
                'Nilay (10-Feb-2016) -- Start
                'Me.ViewState("StartDate") = Nothing
                'Me.ViewState("EndDate") = Nothing
                mdtPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (10-Feb-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPeriod_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPeriod_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (30 Oct 2019) -- Start
    'Mainspring Resourcing Ghana - Enhancement # 4212 : Add a filter "Calculation Type" in Employee Earning & Deduction List screen..
    Protected Sub cboCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCalcType.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, 0, CInt(cboCalcType.SelectedValue), -1, False, False, "", False, False, False, 0)
            With cboTranHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("TranHead")
                .DataBind()
                If .Items.Count > 0 Then .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("cboCalcType_SelectedIndexChanged :- " & ex.Message, Me)
        Finally
            objTranHead = Nothing
        End Try
    End Sub
    'Sohail (30 Oct 2019) -- End

#End Region

#Region "CheckBox Event"

    'Sohail (31 Dec 2019) -- Start
    'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
    'Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If cb Is Nothing Then Exit Try
    '        Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)

    '        For i As Integer = 0 To dtTab.Rows.Count - 1
    '            'Sohail (02 Sep 2019) -- Start
    '            'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
    '            '1. Set ED in pending status if user has not priviledge of ED approval
    '            '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
    '            '3. Send notifications to ED approvers for all employees ED in one email
    '            'If CInt(dtTab.Rows(i)("typeof")) <> -1 AndAlso CInt(dtTab.Rows(i)("calctype")) <> -1 AndAlso CInt(dtTab.Rows(i)("typeof_id")) <> enTypeOf.Salary Then
    '            If CBool(dtTab.Rows(i)("IsGrp")) = True OrElse (CInt(dtTab.Rows(i)("typeof_id")) <> -1 AndAlso CInt(dtTab.Rows(i)("calctype_id")) <> -1) Then
    '                'Sohail (02 Sep 2019) -- End
    '                Dim gvRow As GridViewRow = dgED.Rows(i)
    '                CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
    '                dtTab.Rows(i)("IsCheck") = cb.Checked
    '            End If
    '        Next
    '        dtTab.AcceptChanges()
    '        Me.ViewState("ED") = dtTab
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If cb Is Nothing Then Exit Try

    '        Dim dtTab As DataTable = CType(Me.ViewState("ED"), DataTable)
    '        Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
    '        dtTab.Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
    '        dtTab.AcceptChanges()

    '        'Sohail (02 Sep 2019) -- Start
    '        'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
    '        '1. Set ED in pending status if user has not priviledge of ED approval
    '        '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
    '        '3. Send notifications to ED approvers for all employees ED in one email
    '        If CBool(dtTab.Rows(gvRow.RowIndex).Item("IsGrp")) = True Then
    '            Dim drGrp() As DataRow = dtTab.Select("employeeunkid = " & CInt(dtTab.Rows(gvRow.RowIndex).Item("employeeunkid")) & " ")
    '            For Each dr As DataRow In drGrp
    '                CType(dgED.Rows(dtTab.Rows.IndexOf(dr)).FindControl("chkSelect"), CheckBox).Checked = cb.Checked
    '                dr("IsCheck") = cb.Checked
    '            Next
    '            dtTab.AcceptChanges()
    '        End If
    '        'Sohail (02 Sep 2019) -- End

    '        Me.ViewState("ED") = dtTab

    '        Dim drcheckedRow() As DataRow = dtTab.Select("Ischeck = True AND typeof_id <> " & enTypeOf.Salary)
    '        Dim drRow() As DataRow = dtTab.Select("typeof_id <> " & enTypeOf.Salary)

    '        If drcheckedRow.Length = drRow.Length Then
    '            CType(dgED.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
    '        Else
    '            CType(dgED.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError("chkSelect_CheckedChanged :- " & ex.Message, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    'Sohail (31 Dec 2019) -- End
#End Region

#Region "GridView Event"

    Protected Sub dgED_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgED.RowCommand
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objED As New clsEarningDeduction
        'Hemant (04 Sep 2020) -- End
        Try

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
            'Dim dtED As DataTable = CType(Me.ViewState("ED"), DataTable)
            'Sohail (31 Dec 2019) -- End
            If e.CommandName = "Change" Then

                Dim objPeriod As New clscommom_period_Tran
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtED.Rows(CInt(e.CommandArgument))("periodunkid"))
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dgED.DataKeys(CInt(e.CommandArgument)).Item("periodunkid"))
                'Sohail (31 Dec 2019) -- End
                If objPeriod._Statusid = 2 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed."), Me)
                    Exit Sub
                End If

                Dim objTranHead As New clsTransactionHead
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(dtED.Rows(CInt(e.CommandArgument))("tranheadunkid"))
                objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(dgED.DataKeys(CInt(e.CommandArgument)).Item("tranheadunkid"))
                'Sohail (31 Dec 2019) -- End
                If objTranHead._Typeof_id = enTypeOf.Salary Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), Me)
                    Exit Try
                End If

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                Session("EarningDedc_EmpUnkID") = cboEmployee.SelectedValue
                'SHANI [09 Mar 2015]--END 

                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'Session.Add("EDId", CInt(dtED.Rows(CInt(e.CommandArgument))("edunkid")))
                Session.Add("EDId", CInt(dgED.DataKeys(CInt(e.CommandArgument)).Item("edunkid")))
                'Sohail (31 Dec 2019) -- End
                'Nilay (10-Feb-2016) -- Start
                'Response.Redirect(Session("servername").ToString & "~/Payroll/wPg_EarningDeduction_AddEdit.aspx", False)
                Response.Redirect(Session("rootpath").ToString & "Payroll/wPg_EarningDeduction_AddEdit.aspx", False)
                'Nilay (10-Feb-2016) -- End

            ElseIf e.CommandName = "Remove" Then


                Dim dtPeriodStart As Date
                Dim dtPeriodEnd As Date
                Dim objPeriod As New clscommom_period_Tran
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtED.Rows(CInt(e.CommandArgument))("periodunkid"))
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dgED.DataKeys(CInt(e.CommandArgument)).Item("periodunkid"))
                'Sohail (31 Dec 2019) -- End
                If objPeriod._Statusid = 2 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Edit/Delete this transaction. Reason: Period is closed."), Me)
                    Exit Sub
                Else
                    dtPeriodStart = objPeriod._Start_Date
                    dtPeriodEnd = objPeriod._End_Date
                End If

                Dim objTranHead As New clsTransactionHead
                Dim objMaster As New clsMasterData
                Dim objPayment As New clsPayment_tran
                Dim objTnALeaveTran As New clsTnALeaveTran
                Dim intOpenPeriod As Integer = 0
                Dim dtOpenPeriodEnd As DateTime = Nothing
                Dim dsList As DataSet = Nothing

                intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intOpenPeriod
                dtOpenPeriodEnd = objPeriod._End_Date

                'Sohail (06 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd REQ # 0004099: Email Notification to approve / reject Flat Rate Amounts Uploaded/edited/updated		
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'If CBool(dtED.Rows(CInt(e.CommandArgument))("IsApproved").ToString()) = False Then
                If CBool(dgED.DataKeys(CInt(e.CommandArgument)).Item("IsApproved")) = False Then
                    'Sohail (31 Dec 2019) -- End
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, You can not Delete ticked Transaction Head(s). Reason : Some transaction heads are in pending status."), Me)
                    Exit Try
                End If
                'Sohail (06 Sep 2019) -- End

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, intOpenPeriod, clsPayment_tran.enPayTypeId.PAYMENT, dtED.Rows(e.CommandArgument)("employeeunkid").ToString())
                'Nilay (03-Feb-2016) -- Start
                'Accessfilter parameter problem
                'dsList = objPayment.GetListByPeriod(Session("Database_Name"), _
                '                                    Session("UserId"), _
                '                                    Session("Fin_year"), _
                '                                    Session("CompanyUnkId"), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                    Session("UserAccessModeSetting"), True, _
                '                                    Session("IsIncludeInactiveEmp"), "Payment", _
                '                                    clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                    intOpenPeriod, _
                '                                    clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                    dtED.Rows(e.CommandArgument)("employeeunkid").ToString())
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'dsList = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    objPeriod._Start_Date, _
                '                                    objPeriod._End_Date, _
                '                                    Session("UserAccessModeSetting").ToString, True, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "Payment", _
                '                                    clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                    intOpenPeriod, _
                '                                    clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                    dtED.Rows(CInt(e.CommandArgument))("employeeunkid").ToString(), False)
                dsList = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    objPeriod._Start_Date, _
                                                    objPeriod._End_Date, _
                                                    Session("UserAccessModeSetting").ToString, True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "Payment", _
                                                    clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                    intOpenPeriod, _
                                                    clsPayment_tran.enPayTypeId.PAYMENT, _
                                                    dgED.DataKeys(CInt(e.CommandArgument)).Item("employeeunkid").ToString(), False)
                'Sohail (31 Dec 2019) -- End
                'Nilay (03-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If dsList.Tables("Payment").Rows.Count > 0 Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, You can not Delete ticked Transaction Head(s). Reason : Payment is already done for current open Period. Please Delete payment first to Delete ticked transaction head."), Me)
                    'Anjan [04 June 2014] -- End
                    Exit Try
                End If

                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, dtED.Rows(CInt(e.CommandArgument))("employeeunkid").ToString(), dtOpenPeriodEnd) = True Then
                If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, dgED.DataKeys(CInt(e.CommandArgument)).Item("employeeunkid").ToString(), dtOpenPeriodEnd) = True Then
                    'Sohail (31 Dec 2019) -- End
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, You can not Delete ticked Transaction Head. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete ticked transaction head."), Me)
                    'Anjan [04 June 2014] -- End
                    Exit Try
                End If

                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                'objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(dtED.Rows(CInt(e.CommandArgument))("tranheadunkid").ToString())
                objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(dgED.DataKeys(CInt(e.CommandArgument)).Item("tranheadunkid"))
                'Sohail (31 Dec 2019) -- End
                If objTranHead._Typeof_id = enTypeOf.Salary Then
                    Dim ds As DataSet

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'ds = objED.GetList("EDSalary", dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), , , , , , , , , , , , , , , , , dtPeriodStart)

                    'Nilay (10-Feb-2016) -- Start
                    'ds = objED.GetList(Session("Database_Name"), _
                    '                  Session("UserId"), _
                    '                  Session("Fin_year"), _
                    '                  Session("CompanyUnkId"), _
                    '                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                  Session("UserAccessModeSetting"), True, _
                    '                  Session("IsIncludeInactiveEmp"), _
                    '                  "EDSalary", True, _
                    '                  dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), , , , , , _
                    '                  dtPeriodStart)

                    'Sohail (31 Dec 2019) -- Start
                    'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                    'ds = objED.GetList(CStr(Session("Database_Name")), _
                    '                  CInt(Session("UserId")), _
                    '                  CInt(Session("Fin_year")), _
                    '                  CInt(Session("CompanyUnkId")), _
                    '                  dtPeriodStart, _
                    '                  dtPeriodEnd, _
                    '                  CStr(Session("UserAccessModeSetting")), True, _
                    '                  chkIncludeInactiveEmployee.Checked, _
                    '                  "EDSalary", False, _
                    '                  dtED.Rows(CInt(e.CommandArgument))("employeeunkid").ToString(), , , , , , _
                    '                   dtPeriodStart)
                    ds = objED.GetList(CStr(Session("Database_Name")), _
                                      CInt(Session("UserId")), _
                                      CInt(Session("Fin_year")), _
                                      CInt(Session("CompanyUnkId")), _
                                      dtPeriodStart, _
                                      dtPeriodEnd, _
                                      CStr(Session("UserAccessModeSetting")), True, _
                                      chkIncludeInactiveEmployee.Checked, _
                                      "EDSalary", False, _
                                      dgED.DataKeys(CInt(e.CommandArgument)).Item("employeeunkid").ToString(), , , , , , _
                                       dtPeriodStart)
                    'Sohail (31 Dec 2019) -- End
                    'Nilay (10-Feb-2016) -- End

                    'Shani(20-Nov-2015) -- End

                    If ds.Tables("EDSalary").Rows.Count <= 0 Then 'Only One period ED is aasigned so after deleting this salary head no salary head will remain on ED.
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee."), Me)
                        'Anjan [04 June 2014] -- End
                        Exit Try
                    End If


                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'ds = objED.GetList("EDSalary", dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), , , , , , , , , , , , , , , , , dtPeriodEnd)

                    'Nilay (10-Feb-2016) -- Start
                    'ds = objED.GetList(Session("Database_Name"), _
                    '                  Session("UserId"), _
                    '                  Session("Fin_year"), _
                    '                  Session("CompanyUnkId"), _
                    '                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                  Session("UserAccessModeSetting"), True, _
                    '                  Session("IsIncludeInactiveEmp"), "EDSalary", True, _
                    '                  dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), , , , , , _
                    '                  dtPeriodEnd)

                    'Sohail (31 Dec 2019) -- Start
                    'NMB Enhancement # : Performance issue on Earning Deduction list in self service.
                    'ds = objED.GetList(CStr(Session("Database_Name")), _
                    '                  CInt(Session("UserId")), _
                    '                  CInt(Session("Fin_year")), _
                    '                  CInt(Session("CompanyUnkId")), _
                    '                  dtPeriodStart, _
                    '                  dtPeriodEnd, _
                    '                  CStr(Session("UserAccessModeSetting")), True, _
                    '                  chkIncludeInactiveEmployee.Checked, "EDSalary", False, _
                    '                  dtED.Rows(CInt(e.CommandArgument))("employeeunkid").ToString(), , , , , , _
                    '                   dtPeriodEnd)
                    ds = objED.GetList(CStr(Session("Database_Name")), _
                                      CInt(Session("UserId")), _
                                      CInt(Session("Fin_year")), _
                                      CInt(Session("CompanyUnkId")), _
                                      dtPeriodStart, _
                                      dtPeriodEnd, _
                                      CStr(Session("UserAccessModeSetting")), True, _
                                      chkIncludeInactiveEmployee.Checked, "EDSalary", False, _
                                      dgED.DataKeys(CInt(e.CommandArgument)).Item("employeeunkid").ToString(), , , , , , _
                                       dtPeriodEnd)
                    'Sohail (31 Dec 2019) -- End
                    'Nilay (10-Feb-2016) -- End

                    'Shani(20-Nov-2015) -- End

                    If ds.Tables("EDSalary").Rows.Count > 1 Then 'Other Heads are asssigned besides Salary Heads. First delete other heads then finally Salary Heads.
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, You can not Delete some of the ticked Salary Head(s) when other Heads are assigned. Please delete other Heads First."), Me)
                        'Anjan [04 June 2014] -- End
                        Exit Try
                    End If

                End If
                Me.ViewState.Add("RIndex", e.CommandArgument)
                popupDelete.Show()

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgED_RowCommand :- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgED_RowCommand :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objED = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub dgED_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgED.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If CBool(dgED.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "employeename").ToString & " - [" & DataBinder.Eval(e.Row.DataItem, "employeecode").ToString & "]"
                e.Row.Cells(3).ColumnSpan = e.Row.Cells.Count - 3
                e.Row.BackColor = Color.Silver
                e.Row.ForeColor = Color.Black
                e.Row.Font.Bold = True

                For i As Integer = 4 To e.Row.Cells.Count - 1
                    e.Row.Cells(i).Visible = False
                Next


                Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                lnkEdit.Visible = False

                Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                lnkDelete.Visible = False


            ElseIf e.Row.Cells(3).Text = "None" AndAlso e.Row.Cells(4).Text = "None" AndAlso e.Row.Cells(5).Text = "None" AndAlso e.Row.Cells(6).Text = "None" Then
                e.Row.Visible = False
            Else


                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                'dgED.Columns(1).Visible = Session("EditEarningDeduction")
                'dgED.Columns(2).Visible = Session("AllowToApproveEarningDeduction")
                'Pinkal (12-Feb-2015) -- End

                'Sohail (02 Sep 2019) -- Start
                'Mainspring Resourcing Ghana Ltd Enhancement - 76.1 - # 0004098: Approval Flow for Flat Rate Heads Amount Uploaded.
                '1. Set ED in pending status if user has not priviledge of ED approval
                '2. Perform Global approver with option to filter for allocation and approved any posted (new /edited) E & D values in MSS
                '3. Send notifications to ED approvers for all employees ED in one email
                'Dim dsTranHead As DataSet = CType(Me.ViewState("TranHead"), DataSet)
                'Dim drRow() As DataRow = dsTranHead.Tables(0).Select("tranheadunkid = " & CInt(DataBinder.Eval(e.Row.DataItem, "tranheadunkid")))
                'If drRow.Length > 0 Then

                '    If CInt(e.Row.Cells(6).Text) = enTypeOf.Salary Then
                '        e.Row.Cells(0).Enabled = False
                '    End If

                '    e.Row.Cells(5).Text = drRow(0)("HeadType").ToString()
                '    e.Row.Cells(6).Text = drRow(0)("TypeOf").ToString()
                '    e.Row.Cells(7).Text = drRow(0)("calctype").ToString()
                'End If               
                'Sohail (02 Sep 2019) -- End

                e.Row.Cells(9).Text = Format(CDec(e.Row.Cells(9).Text), CStr(Session("fmtcurrency")))

                If CBool(e.Row.Cells(10).Text) Then
                    e.Row.Cells(10).Text = "A"
                Else
                    e.Row.Cells(10).Text = "P"
                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgED_RowDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgED_RowDataBound :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Session("EDId") = Nothing
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
#Region " Link button's Events "
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkAllocation_Click :- " & ex.Message, Me)
        End Try
    End Sub
#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            'Hemant (17 Oct 2019) -- Start
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Hemant (17 Oct 2019) -- End
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.ID, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblApprovalStatus.Text = Language._Object.getCaption(Me.lblApprovalStatus.ID, Me.lblApprovalStatus.Text)
            Me.btnDisApprove.Text = Language._Object.getCaption(Me.btnDisApprove.ID, Me.btnDisApprove.Text).Replace("&", "")
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.ID, Me.lblTranHead.Text)
            Me.chkIncludeInactiveEmployee.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmployee.ID, Me.chkIncludeInactiveEmployee.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblperiod.Text = Language._Object.getCaption(Me.lblperiod.ID, Me.lblperiod.Text)
            Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.ID, Me.lblCalcType.Text)

            dgED.Columns(1).HeaderText = Language._Object.getCaption("btnEdit", dgED.Columns(1).HeaderText).Replace("&", "")
            dgED.Columns(2).HeaderText = Language._Object.getCaption("btnDelete", dgED.Columns(2).HeaderText).Replace("&", "")

            dgED.Columns(3).HeaderText = Language._Object.getCaption(dgED.Columns(3).FooterText, dgED.Columns(3).HeaderText)
            dgED.Columns(4).HeaderText = Language._Object.getCaption(dgED.Columns(4).FooterText, dgED.Columns(4).HeaderText)
            dgED.Columns(5).HeaderText = Language._Object.getCaption(dgED.Columns(5).FooterText, dgED.Columns(5).HeaderText)
            dgED.Columns(6).HeaderText = Language._Object.getCaption(dgED.Columns(6).FooterText, dgED.Columns(6).HeaderText)
            dgED.Columns(7).HeaderText = Language._Object.getCaption(dgED.Columns(7).FooterText, dgED.Columns(7).HeaderText)
            dgED.Columns(8).HeaderText = Language._Object.getCaption(dgED.Columns(8).FooterText, dgED.Columns(8).HeaderText)
            dgED.Columns(9).HeaderText = Language._Object.getCaption(dgED.Columns(9).FooterText, dgED.Columns(9).HeaderText)
            dgED.Columns(10).HeaderText = Language._Object.getCaption(dgED.Columns(10).FooterText, dgED.Columns(10).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class
