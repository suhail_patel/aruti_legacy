﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Data.SqlClient

#End Region

Partial Class Payroll_wPg_Paymentapprovedisapprove
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer

    Private mdicCurrency As New Dictionary(Of Integer, String)

    Private mdtApproval As DataTable

    Private mintCurrLevelPriority As Integer = -1
    Private mintLowerLevelPriority As Integer = -99 'Keep -99
    Private mintMinPriority As Integer = -1
    Private mintMaxPriority As Integer = -1
    Private mintLowerPriorityForFirstLevel As Integer = -1
    Private mintCurrLevelID As Integer = -1

    Private mstrAdvanceFilter As String = "" 'Sohail (18 Feb 2014)

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmApproveDisapprovePayment"
    'Anjan [04 June 2014] -- End

    Private objCONN As SqlConnection 'Sohail (17 Dec 2014)

    'Nilay (10-Feb-2016) -- Start
    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End


#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim objExRate As New clsExchangeRate
        Dim objPeriod As New clscommom_period_Tran
        Dim objApproverMap As New clspayment_approver_mapping
        Dim objApproverLevel As New clsPaymentApproverlevel_master
        Dim dsCombo As DataSet

        Try

            'Sohail (21 Jan 2014) -- Start
            'Enhancement - Oman
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "PayPeriod", True, 1, True, , Session("Database_Name"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "PayPeriod", True, 1, False, , Session("Database_Name"))

            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name"), Session("fin_startdate"), "PayPeriod", True, 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "PayPeriod", True, 1, False)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            'Sohail (21 Jan 2014) -- End
            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                .DataBind()
                If .Items.Count > 0 Then
                    Call cboPayPeriod_SelectedIndexChanged(cboPayPeriod, New System.EventArgs())
                End If
            End With

            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            Dim intBaseCountryId As Integer = 0
            'Sohail (16 Oct 2019) -- End
            dsCombo = objExRate.getComboList("ExRate", False)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    intBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                    'Sohail (16 Oct 2019) -- End
                End If

                With cboCurrency
                    .DataValueField = "countryunkid"
                    .DataTextField = "currency_sign"
                    .DataSource = dsCombo.Tables("ExRate")
                    .DataBind()
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    If intBaseCountryId > 0 Then
                        .SelectedValue = intBaseCountryId.ToString
                    Else
                        .SelectedIndex = 0
                    End If
                    'Sohail (16 Oct 2019) -- End
                End With

                For Each dsRow As DataRow In dsCombo.Tables(0).Rows
                    If mdicCurrency.ContainsKey(CInt(dsRow.Item("exchangerateunkid"))) = False Then
                        mdicCurrency.Add(CInt(dsRow.Item("exchangerateunkid")), dsRow.Item("currency_sign").ToString)
                    End If
                Next
            End If

            'Sohail (16 Apr 2015) -- Start
            'Aghakhan Issue -  inactive Level users also can approve payments.
            'dsCombo = objApproverMap.GetList("ApproverLevel", CInt(Session("UserId")))
            dsCombo = objApproverMap.GetList("ApproverLevel", CInt(Session("UserId")), " prpaymentapproverlevel_master.isinactive = 0 ")
            'Sohail (16 Apr 2015) -- End
            If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
                mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
                mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority)
                mintMinPriority = objApproverLevel.GetMinPriority()
                mintMaxPriority = objApproverLevel.GetMaxPriority()
                mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority)

                If mintCurrLevelPriority >= 0 Then
                    mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
                    objApproverName.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver").ToString
                    objApproverLevelVal.Text = dsCombo.Tables("ApproverLevel").Rows(0).Item("approver_level").ToString
                    objLevelPriorityVal.Text = mintCurrLevelPriority.ToString
                End If


            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPeriod = Nothing
            objExRate = Nothing
            objApproverMap = Nothing
            objApproverLevel = Nothing
        End Try
    End Sub

    Private Sub CreateListTable()
        Try
            mdtApproval = New DataTable
            With mdtApproval
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False 'Sohail (10 Mar 2014)
                .Columns.Add("paymentapprovaltranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("paymenttranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("EmpName", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("expaidamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("expaidamt_tag", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("paidcurrency", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("paymentmode", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("CreateListTable :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet
        Dim decTotAmt As Decimal = 0
        Dim mintBankPayment As Integer = 0
        Dim mintCashPayment As Integer = 0
        Dim intIsApproved As Integer
        Dim strEmpFilter As String = "" 'Sohail (18 Feb 2014)

        Dim objPaymentApproval As New clsPayment_approval_tran

        Try
            mdtApproval.Rows.Clear()
            txtTotalAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))

            If CInt(cboPayPeriod.SelectedValue) <= 0 OrElse CInt(cboCurrency.SelectedValue) <= 0 OrElse mintCurrLevelPriority < 0 Then
                objBankpaidVal.Text = mintBankPayment.ToString
                objCashpaidVal.Text = mintCashPayment.ToString
                objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString
                Exit Try
            End If

            If CBool(Session("mblnFromApprove")) = True Then
                intIsApproved = 0
            Else
                intIsApproved = -1
            End If

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            strEmpFilter = mstrAdvanceFilter
            If cboPmtVoucher.SelectedIndex > 0 Then
                If strEmpFilter.Trim = "" Then
                    strEmpFilter &= " (prpayment_tran.voucherno = '" & cboPmtVoucher.Text & "' OR prglobalvoc_master.globalvocno = '" & cboPmtVoucher.Text & "') "
                Else
                    strEmpFilter &= " AND (prpayment_tran.voucherno = '" & cboPmtVoucher.Text & "' OR prglobalvoc_master.globalvocno = '" & cboPmtVoucher.Text & "') "
                End If
            End If
            'Sohail (18 Feb 2014) -- End

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            'dsList = objPaymentApproval.GetListForApproval("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CBool(Session("mblnFromApprove")), mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved)

            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            'dsList = objPaymentApproval.GetListForApproval("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CBool(Session("mblnFromApprove")), mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved, strEmpFilter)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objPaymentApproval.GetListForApproval("List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CBool(Session("mblnFromApprove")), mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved, strEmpFilter, Session("AccessLevelFilterString"))
            dsList = objPaymentApproval.GetListForApproval(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                           mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, _
                                                           "List", clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, _
                                                           CBool(Session("mblnFromApprove")), mintCurrLevelPriority, mintLowerLevelPriority, mintMaxPriority, _
                                                           CInt(cboPayPeriod.SelectedValue), CInt(cboCurrency.SelectedValue), intIsApproved, strEmpFilter)
            'Nilay (10-Feb-2016) -- End

            'Sohail (17 Dec 2014) -- End
            'Sohail (18 Feb 2014) -- End

            Dim intCount As Integer = dsList.Tables("List").Rows.Count
            Dim dtRow As DataRow
            Dim dRow As DataRow
            For i As Integer = 0 To intCount - 1
                dtRow = dsList.Tables("List").Rows(i)

                dRow = mdtApproval.NewRow()

                dRow.Item("paymentapprovaltranunkid") = CInt(dtRow.Item("paymentapprovaltranunkid"))
                dRow.Item("paymenttranunkid") = CInt(dtRow.Item("paymenttranunkid"))
                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("EmpName") = dtRow.Item("EmpName").ToString
                dRow.Item("expaidamt") = Format(CDec(dtRow.Item("expaidamt")), CStr(Session("fmtCurrency")))
                dRow.Item("expaidamt_tag") = CDec(dtRow.Item("expaidamt"))
                
                decTotAmt += CDec(dtRow.Item("expaidamt"))

                If mdicCurrency.ContainsKey(CInt(dtRow.Item("paidcurrencyid"))) = True Then
                    dRow.Item("paidcurrency") = mdicCurrency.Item(CInt(dtRow.Item("paidcurrencyid")))
                Else
                    dRow.Item("paidcurrency") = mstrBaseCurrSign
                End If
                dRow.Item("paymentmode") = dtRow.Item("paymentmode").ToString


                If CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH OrElse CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE Then
                    mintCashPayment += 1
                ElseIf CInt(dtRow.Item("paymentmode")) = enPaymentMode.CHEQUE OrElse CInt(dtRow.Item("paymentmode")) = enPaymentMode.TRANSFER Then
                    mintBankPayment += 1
                End If

                mdtApproval.Rows.Add(dRow)

            Next

            'txtTotalAmount.Text = Format(decTotAmt, Session("fmtCurrency")) 'Sohail (10 Mar 2014)

            objBankpaidVal.Text = mintBankPayment.ToString
            objCashpaidVal.Text = mintCashPayment.ToString
            objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPaymentApproval = Nothing

            If mdtApproval.Rows.Count <= 0 Then
                Dim r As DataRow = mdtApproval.NewRow

                r.Item(4) = "None" ' To Hide the row and display only Row Header
                r.Item(5) = ""

                mdtApproval.Rows.Add(r)
            End If

            GvApprovePayment.DataSource = mdtApproval
            GvApprovePayment.DataBind()
        End Try
    End Sub

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Send Email Notification to lower levels when payment disapproved.
    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In GvApprovePayment.Rows
                If CInt(mdtApproval.Rows(gvr.RowIndex).Item("employeeunkid")) > 0 AndAlso mdtApproval.Rows(gvr.RowIndex).Item("employeecode").ToString <> "" Then
                    cb = CType(GvApprovePayment.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                    cb.Checked = blnCheckAll

                    mdtApproval.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError("CheckAllEmployee :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim decTotAmt As Decimal = 0
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

            decTotAmt = (From p In mdtApproval Where CBool(p.Item("IsChecked")) = True Select CDec(p.Item("expaidamt"))).Sum()

            txtTotalAmount.Text = Format(decTotAmt, Session("fmtCurrency").ToString)
        Catch ex As Exception
            DisplayMessage.DisplayError("chkSelectAll_OnCheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim decTotAmt As Decimal
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtApproval.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

            decTotAmt = (From p In mdtApproval Where CBool(p.Item("IsChecked")) = True Select CDec(p.Item("expaidamt"))).Sum()
            txtTotalAmount.Text = Format(decTotAmt, Session("fmtCurrency").ToString)
        Catch ex As Exception
            DisplayMessage.DisplayError("chkSelect_OnCheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (10 Mar 2014) -- End

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please select Pay Period. Pay Period is mandatory information.", Me)
                Return False
            ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please select Currency. Currency is mandatory information.", Me)
                Return False
                'ElseIf lvEmployeeList.CheckedItems.Count <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please select atleast one employee to Approve / Void Approved payment."), enMsgBoxStyle.Information)
                '    Return False
            ElseIf mdtApproval.Select("employeeunkid > 0 AND employeecode <> '' AND IsChecked = 1 ").Length <= 0 Then
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'DisplayMessage.DisplayMessage("Sorry! There is no payment available to Approve / Void Approved.", Me)
                DisplayMessage.DisplayMessage("Please select atleast one employee to Approve / Void Approved payment.", Me)
                'Sohail (10 Mar 2014) -- End
                Return False
            ElseIf mintCurrLevelPriority < 0 Then
                DisplayMessage.DisplayMessage("Sorry! You can not not Approve / Disapprove Payment. Please contact Administrator.", Me)
                Return False
            End If

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            If gobjEmailList.Count > 0 Then
                DisplayMessage.DisplayMessage("Sending Email(s) process is in progress from other module. Please wait for some time.", Me)
                Return False
            End If
            'Sohail (10 Mar 2014) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidData:- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidData:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub SaveData(Optional ByVal mstrDisApprovalReason As String = "")
        Dim objPaymentApproval As New clsPayment_approval_tran
        Dim blnResult As Boolean
        Dim dsRow As DataRow
        Dim intIsApproved As Integer = 0

        Dim strEmpIDs As String = ""
        Dim intBankPaid As Integer = 0
        Dim intCashPaid As Integer = 0
        Dim decBankPaid As Decimal = 0
        Dim decCashPaid As Decimal = 0

        Dim mdtTable As DataTable

        Try
            mdtTable = objPaymentApproval._DataTable
            mdtTable.Clear()

            For Each dtRow As DataRow In mdtApproval.Select("IsChecked")
                dsRow = mdtTable.NewRow

                dsRow.Item("paymentapprovaltranunkid") = CInt(dtRow.Item("paymentapprovaltranunkid"))
                dsRow.Item("paymenttranunkid") = CInt(dtRow.Item("paymenttranunkid")) 'paymenttranunkid

                dsRow.Item("levelunkid") = mintCurrLevelID
                dsRow.Item("priority") = mintCurrLevelPriority
                dsRow.Item("approval_date") = ConfigParameter._Object._CurrentDateAndTime

                If CBool(Session("mblnFromApprove")) = True Then
                    dsRow.Item("statusunkid") = 1 'Approve
                    dsRow.Item("isvoid") = False
                Else
                    dsRow.Item("statusunkid") = 2 'DisApproved
                    dsRow.Item("isvoid") = True
                End If
                dsRow.Item("userunkid") = Session("UserId")
                dsRow.Item("disapprovalreason") = mstrDisApprovalReason
                dsRow.Item("remarks") = txtRemarks.Text.Trim

                dsRow.Item("voiduserunkid") = -1
                dsRow.Item("voiddatetime") = DBNull.Value
                dsRow.Item("voidreason") = ""

                If strEmpIDs.Trim = "" Then
                    strEmpIDs = dtRow.Item("employeeunkid").ToString
                Else
                    strEmpIDs &= "," & dtRow.Item("employeeunkid").ToString
                End If

                If CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH OrElse CInt(dtRow.Item("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE Then
                    intCashPaid += 1
                    decCashPaid += CDec(dtRow.Item("expaidamt"))
                ElseIf CInt(dtRow.Item("paymentmode")) = enPaymentMode.CHEQUE OrElse CInt(dtRow.Item("paymentmode")) = enPaymentMode.TRANSFER Then
                    intBankPaid += 1
                    decBankPaid += CDec(dtRow.Item("expaidamt"))
                End If
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
                dsRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dsRow.Item("Paymentmodeid") = CInt(dtRow.Item("paymentmode"))
                dsRow.Item("expaidamt") = CDec(dtRow.Item("expaidamt"))
                'Sohail (18 Feb 2014) -- End

                mdtTable.Rows.Add(dsRow)
            Next

            'Sohail (29 Mar 2016) -- Start
            'Issue - 58.1 - Payment did not get final approved when there is only one level with 0 priority.
            'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CBool(Session("mblnFromApprove")) = True Then
            If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CBool(Session("mblnFromApprove")) = True Then
                'Sohail (29 Mar 2016) -- End
                intIsApproved = 1
                'Sohail (29 Mar 2016) -- Start
                'Issue - 58.1 - Payment did not get final approved when there is only one level with 0 priority.
                'ElseIf mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CBool(Session("mblnFromApprove")) = False Then
            ElseIf mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority AndAlso CBool(Session("mblnFromApprove")) = False Then
                'Sohail (29 Mar 2016) -- End
                intIsApproved = 2
            End If

            'Blank_ModuleName()
            'objPaymentApproval._WebFormName = "frmApproveDisapprovePayment"
            'StrModuleName2 = "mnuPayroll"
            'objPaymentApproval._WebHostName = CStr(Session("HOST_NAME"))
            'objPaymentApproval._WebIP = CStr(Session("IP_ADD"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'blnResult = objPaymentApproval.UpdatePaymentApproval(mdtTable, intIsApproved)

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'blnResult = objPaymentApproval.UpdatePaymentApproval(mdtTable, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)

            'Nilay (25-Mar-2016) -- Start
            'blnResult = objPaymentApproval.UpdatePaymentApproval(CStr(Session("Database_Name")), CInt(Session("Fin_year")), mdtTable, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)

            With objPaymentApproval
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            blnResult = objPaymentApproval.UpdatePaymentApproval(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                 CStr(Session("UserAccessModeSetting")), True, mdtTable, ConfigParameter._Object._CurrentDateAndTime, intIsApproved)
            'Nilay (25-Mar-2016) -- End

            'Nilay (15-Dec-2015) -- End

            'Shani(20-Nov-2015) -- End

            If blnResult = False And objPaymentApproval._Message <> "" Then
                DisplayMessage.DisplayMessage(objPaymentApproval._Message, Me)
            Else
                Dim objPaymentTran As New clsPayment_tran
                'Sohail (17 Dec 2014) -- Start
                'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                'Blank_ModuleName()
                'objPaymentTran._WebFrmName = "frmApproveDisapprovePayment"
                'StrModuleName2 = "mnuPayroll"
                'objPaymentTran._WebHostName = CStr(Session("HOST_NAME"))
                'objPaymentTran._WebIP = CStr(Session("IP_ADD"))
                'Sohail (17 Dec 2014) -- End

                With objPaymentTran
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._Companyunkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With

                If CBool(Session("mblnFromApprove")) = True Then
                    'Sohail (18 Feb 2014) -- Start
                    'Enhancement - AGKN
                    'objPaymentTran.SendMailToApprover(False, Session("UserId"), strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency"))
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objPaymentTran.SendMailToApprover(False, Session("UserId"), strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency"), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), mdtTable, enLogin_Mode.MGR_SELF_SERVICE, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.MGR_SELF_SERVICE, 0} -- END
                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'objPaymentTran.SendMailToApprover(False, CInt(Session("UserId")), strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")), mdtTable, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(cboCurrency.SelectedValue), CStr(Session("ArutiSelfServiceURL")))
                    objPaymentTran.SendMailToApprover(False, CInt(Session("UserId")), strEmpIDs, CInt(cboPayPeriod.SelectedValue), False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, mdtTable, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(cboCurrency.SelectedValue), CStr(Session("ArutiSelfServiceURL")))
                    'Sohail (16 Nov 2016) -- End
                    'Sohail (17 Dec 2014) -- End
                    'Sohail (18 Feb 2014) -- End
                    DisplayMessage.DisplayMessage("Payment Approval process completed successfully.", Me)
                Else
                    'objPaymentTran.SendMailToApprover(False, User._Object._Userunkid, strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, mintBankPaid, mintCashPaid, (mintBankPaid + mintCashPaid))
                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'objPaymentTran.SendMailToApprover(False, Session("UserId"), strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency"), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), mdtTable, enLogin_Mode.MGR_SELF_SERVICE, 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {enLogin_Mode.MGR_SELF_SERVICE, 0} -- END
                    'Sohail (16 Nov 2016) -- Start
                    'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                    'objPaymentTran.SendMailToApprover(False, CInt(Session("UserId")), strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")), mdtTable, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(cboCurrency.SelectedValue), CStr(Session("ArutiSelfServiceURL")))
                    objPaymentTran.SendMailToApprover(False, CInt(Session("UserId")), strEmpIDs, CInt(cboPayPeriod.SelectedValue), True, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim, Session("fmtCurrency").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False, mdtTable, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(cboCurrency.SelectedValue), CStr(Session("ArutiSelfServiceURL")))
                    'Sohail (16 Nov 2016) -- End
                    'Sohail (17 Dec 2014) -- End
                    DisplayMessage.DisplayMessage("Void Approved Payment process completed successfully.", Me)
                End If
                cboPayPeriod.SelectedValue = "0"
                Call cboPayPeriod_SelectedIndexChanged(cboPayPeriod, New System.EventArgs())
                txtRemarks.Text = ""
                'mblnCancel = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SaveData:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SaveData:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPaymentApproval = Nothing
        End Try
    End Sub
#End Region

#Region " Page's Event "

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mstrBaseCurrSign", mstrBaseCurrSign)
        Me.ViewState.Add("mintBaseCurrId", mintBaseCurrId)

        Me.ViewState.Add("mdicCurrency", mdicCurrency)

        Me.ViewState.Add("mdtApproval", mdtApproval)

        Me.ViewState.Add("mintCurrLevelPriority", mintCurrLevelPriority)
        Me.ViewState.Add("mintLowerLevelPriority", mintLowerLevelPriority)
        Me.ViewState.Add("mintMinPriority", mintMinPriority)
        Me.ViewState.Add("mintMaxPriority", mintMaxPriority)
        Me.ViewState.Add("mintLowerPriorityForFirstLevel", mintLowerPriorityForFirstLevel)
        Me.ViewState.Add("mintCurrLevelID", mintCurrLevelID)

        Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter) 'Sohail (18 Feb 2014)

        'Nilay (10-Feb-2016) -- Start
        Me.ViewState("PayPeriodStartDate") = mdtPayPeriodStartDate
        Me.ViewState("PayPeriodEndDate") = mdtPayPeriodEndDate
        'Nilay (10-Feb-2016) -- End

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            'If Session("clsuser") Is Nothing Or Session("mblnFromApprove") Is Nothing Then
            '    Exit Sub
            'End If

            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                Me.Title = ""

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'Me.Closebotton1.PageHeading = ""
                Me.lblPageHeader.Text = ""
                'Nilay (01-Feb-2015) -- End
                If Request.QueryString.Count > 0 Then
                    If Request.UrlReferrer IsNot Nothing AndAlso Request.UrlReferrer.OriginalString.ToString().Contains("PaymentApproval") Then
                        GoTo FromPaymentApproval
                    End If
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 5 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))

                        Session("mblnFromApprove") = CBool(arr(2))
                        Me.ViewState.Add("mintPeriodUnkId", CInt(arr(3)))
                        Me.ViewState.Add("mintCurrencyUnkId", CInt(arr(4)))


                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objCommon As New CommonCodes
                        'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False) Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If

                        Dim clsConfig As New clsConfigOptions
                        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        Session("fmtCurrency") = clsConfig._CurrencyFormat
                        Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim
                        Session("SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval

                        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Session("ArutiSelfServiceURL") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                        Else
                            Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
                        End If


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try


                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        'Sohail (21 Mar 2015) -- Start
                        'Enhancement - New UI Notification Link Changes.
                        'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                        'Sohail (21 Mar 2015) -- End
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        'gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        'gobjLocalization._LangId = HttpContext.Current.Session("LangId")
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objUserPrivilege As New clsUserPrivilege
                        'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                        'Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance
                        'Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
                        'Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
                        'Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense

                        'Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave
                        'Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave

                        'Dim objMaster As New clsMasterData
                        'Session("UserAccessLevel") = objMaster.GetUserAccessLevel(CInt(Session("UserId")), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("UserAccessModeSetting"))
                        'If Session("LoginBy") = Global.User.en_loginby.Employee Then
                        '    Session("UserAccessLevel") = ""
                        'End If

                        'If Session("UserAccessLevel") = "" Then
                        '    Session("AccessLevelFilterString") = " AND 1 = 1 "

                        '    Session("AccessLevelBranchFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelDepartmentGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelDepartmentFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelSectionGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelSectionFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelUnitGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelUnitFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelTeamFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelJobGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelJobFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelClassGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelClassFilterString") = " AND 1 = 1 "
                        'Else
                        '    Session("AccessLevelFilterString") = UserAccessLevel._AccessLevelFilterString

                        '    Session("AccessLevelBranchFilterString") = UserAccessLevel._AccessLevelBranchFilterString
                        '    Session("AccessLevelDepartmentGroupFilterString") = UserAccessLevel._AccessLevelDepartmentGroupFilterString
                        '    Session("AccessLevelDepartmentFilterString") = UserAccessLevel._AccessLevelDepartmentFilterString
                        '    Session("AccessLevelSectionGroupFilterString") = UserAccessLevel._AccessLevelSectionGroupFilterString
                        '    Session("AccessLevelSectionFilterString") = UserAccessLevel._AccessLevelSectionFilterString
                        '    Session("AccessLevelUnitGroupFilterString") = UserAccessLevel._AccessLevelUnitGroupFilterString
                        '    Session("AccessLevelUnitFilterString") = UserAccessLevel._AccessLevelUnitFilterString
                        '    Session("AccessLevelTeamFilterString") = UserAccessLevel._AccessLevelTeamFilterString
                        '    Session("AccessLevelJobGroupFilterString") = UserAccessLevel._AccessLevelJobGroupFilterString
                        '    Session("AccessLevelJobFilterString") = UserAccessLevel._AccessLevelJobFilterString
                        '    Session("AccessLevelClassGroupFilterString") = UserAccessLevel._AccessLevelClassGroupFilterString
                        '    Session("AccessLevelClassFilterString") = UserAccessLevel._AccessLevelClassFilterString
                        'End If
                        'Sohail (30 Mar 2015) -- End

                        Call SetLanguage()
                        If CBool(Session("mblnFromApprove")) = True Then
                            btnProcess.Text = Language.getMessage(mstrModuleName, 12, "Approve Payment")
                            lblRemarks.Text = Language.getMessage(mstrModuleName, 13, "Approval Remarks")
                            'Nilay (01-Feb-2015) -- Start
                            'Enhancement - REDESIGN SELF SERVICE.
                            'Closebotton1.PageHeading = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                            lblPageHeader.Text = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                            'Nilay (01-Feb-2015) -- End


                        ElseIf CBool(Session("mblnFromApprove")) = False Then
                            btnProcess.Text = Language.getMessage(mstrModuleName, 15, "Void Approved Payment")
                            lblRemarks.Text = Language.getMessage(mstrModuleName, 16, "Disapproval Remarks")
                            'Nilay (01-Feb-2015) -- Start
                            'Enhancement - REDESIGN SELF SERVICE.
                            'Closebotton1.PageHeading = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                            lblPageHeader.Text = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                            'Nilay (01-Feb-2015) -- End


                        End If

                        Call CreateListTable()
                        Call FillCombo()
                        cboPayPeriod.SelectedValue = CStr(arr(3))
                        Call cboPayPeriod_SelectedIndexChanged(sender, e)
                        cboCurrency.SelectedValue = CStr(arr(4))
                        Call cboCurrency_SelectedIndexChanged(sender, e)

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                        HttpContext.Current.Session("Login") = True
                        'Sohail (30 Mar 2015) -- End
                    Else
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
            End If

FromPaymentApproval:

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False) Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False) Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            'Sohail (17 Dec 2014) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            If Me.IsPostBack = False Then

                If CBool(Session("mblnFromApprove")) = True Then
                    btnProcess.Text = Language.getMessage(mstrModuleName, 12, "Approve Payment")
                    lblRemarks.Text = Language.getMessage(mstrModuleName, 13, "Approval Remarks")
                    'Nilay (01-Feb-2015) -- Start
                    'Enhancement - REDESIGN SELF SERVICE.
                    'Closebotton1.PageHeading = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                    'Nilay (01-Feb-2015) -- End

                ElseIf CBool(Session("mblnFromApprove")) = False Then
                    btnProcess.Text = Language.getMessage(mstrModuleName, 15, "Void Approved Payment")
                    lblRemarks.Text = Language.getMessage(mstrModuleName, 16, "Disapproval Remarks")
                    'Nilay (01-Feb-2015) -- Start
                    'Enhancement - REDESIGN SELF SERVICE.
                    'Closebotton1.PageHeading = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                    lblPageHeader.Text = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                    'Nilay (01-Feb-2015) -- End
                End If

                If ViewState("mintPeriodUnkId") Is Nothing Then 'Sohail (17 Dec 2014)
                Call CreateListTable()
                Call FillCombo()
                End If 'Sohail (17 Dec 2014)
                'Hemant (17 Oct 2019) -- Start
                lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
                lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
                'Hemant (17 Oct 2019) -- End
            Else
                mstrBaseCurrSign = CStr(ViewState("mstrBaseCurrSign"))
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))

                mdicCurrency = CType(ViewState("mdicCurrency"), Global.System.Collections.Generic.Dictionary(Of Integer, String))

                mdtApproval = CType(ViewState("mdtApproval"), DataTable)

                mintCurrLevelPriority = CInt(ViewState("mintCurrLevelPriority"))
                mintLowerLevelPriority = CInt(ViewState("mintLowerLevelPriority"))
                mintMinPriority = CInt(ViewState("mintMinPriority"))
                mintMaxPriority = CInt(ViewState("mintMaxPriority"))
                mintLowerPriorityForFirstLevel = CInt(ViewState("mintLowerPriorityForFirstLevel"))
                mintCurrLevelID = CInt(ViewState("mintCurrLevelID"))
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter")) 'Sohail (18 Feb 2014)
                'Nilay (10-Feb-2016) -- Start
                mdtPayPeriodStartDate = CDate(Me.ViewState("PayPeriodStartDate"))
                mdtPayPeriodEndDate = CDate(Me.ViewState("PayPeriodEndDate"))
                'Nilay (10-Feb-2016) -- End
            End If
            'Hemant (17 Oct 2019) -- Start
            lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
            lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
            'Hemant (17 Oct 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Sohail (17 Dec 2014) -- Start
        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
        'Me.IsLoginRequired = True
        If Request.QueryString.Count <= 0 Then
        Me.IsLoginRequired = True
        End If
        'Sohail (17 Dec 2014) -- End
    End Sub

#End Region

#Region " Button's Event "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("../Payroll/wPg_PaymentApproval.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Payroll/wPg_PaymentApproval.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        Dim strMsg As String

        Try
            popupConfirm.Message = ""
            If IsValidData() = False Then Exit Sub

            If CBool(Session("mblnFromApprove")) = True Then
                'Sohail (29 Mar 2016) -- Start
                'Issue - 58.1 - Payment did not get final approved when there is only one level with 0 priority.
                'If mintMaxPriority > 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                If mintMaxPriority >= 0 AndAlso mintMaxPriority = mintCurrLevelPriority Then
                    'Sohail (29 Mar 2016) -- End
                    strMsg = "Are you sure you want to Final Approve selected Payments?"
                Else
                    strMsg = "Are you sure you want to Approve selected Payments?"
                End If

            Else
                strMsg = "Are you sure you want to Disapprove selected Payments?"
            End If

            popupConfirm.Message = strMsg
            popupConfirm.Show()


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnProcess_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnProcess_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirm.buttonYes_Click
        Try
            Dim mstrDisApprovalReason As String = String.Empty
            If CBool(Session("mblnFromApprove")) = False Then
                popupConfirm.Hide()
                popupDisApprove.Show()
            Else
                Call SaveData()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupConfirm_buttonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupConfirm_buttonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDisApprove_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDisApprove.buttonDelReasonYes_Click
        Try
            Call SaveData(popupDisApprove.Reason)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDisApprove_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDisApprove_buttonDelReasonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Combobox's Events "

    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim objPayment As New clsPayment_tran
                Dim dsCombo As DataSet
                'Sohail (17 Oct 2016) -- Start
                'Issue - 63.1 - period id passed instead of database name.
                'dsCombo = objPayment.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CStr(cboPayPeriod.SelectedValue))
                dsCombo = objPayment.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, Session("Database_Name").ToString, CInt(cboPayPeriod.SelectedValue))
                'Sohail (17 Oct 2016) -- End

                With cboPmtVoucher
                    .DataValueField = "voucherno"
                    .DataTextField = "voucherno"
                    .DataSource = dsCombo.Tables("Voucher")
                    .DataBind()
                End With
                objPayment = Nothing
                'Nilay (10-Feb-2016) -- Start
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
                mdtPayPeriodStartDate = objPeriod._Start_Date
                mdtPayPeriodEndDate = objPeriod._End_Date
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPayPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (10-Feb-2016) -- End
            End If
            'Sohail (18 Feb 2014) -- End
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPayPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPayPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Try
            lblTotalAmount.Text = "Total Amount (" & cboCurrency.SelectedItem.Text & ")"
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboCurrency_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboCurrency_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " GridView Events "

    Protected Sub GvApprovePayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprovePayment.RowDataBound
        Try
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            If e.Row.RowType = DataControlRowType.Header OrElse e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(6).Visible = CBool(Session("AllowToViewPaidAmount"))
            End If
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).Enabled = CBool(Session("AllowToViewPaidAmount"))
            End If
            'Sohail (16 Oct 2019) -- End
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(4).Text = "None" AndAlso e.Row.Cells(5).Text = "&nbsp;" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvApprovePayment_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GvApprovePayment_RowDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Sohail (18 Feb 2014) -- Start
    'Enhancement - AGKN
#Region " Other Controls Events "
    Protected Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("objbtnSearch_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboPmtVoucher.Items.Count > 0 Then cboPmtVoucher.SelectedIndex = 0
            mstrAdvanceFilter = ""
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("objbtnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAdvanceFilter_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkAdvanceFilter_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Hemant (17 Oct 2019) -- Start
    Protected Sub lnkPayrollVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollVarianceReport.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect("Rpt_PayrollVariance.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayMessage("lnkPayrollVarianceReport_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lnkPayrollTotalVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollTotalVarianceReport.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect("Rpt_Payroll_Total_Variance.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayMessage("lnkPayrollTotalVarianceReport_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (17 Oct 2019) -- End

#End Region
    'Sohail (18 Feb 2014) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try

            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.gbAmountInfo.Text = Language._Object.getCaption(Me.gbAmountInfo.ID, Me.gbAmountInfo.Text)
            Me.lblTotalAmount.Text = Language._Object.getCaption(Me.lblTotalAmount.ID, Me.lblTotalAmount.Text)
            Me.gbPaymentInfo.Text = Language._Object.getCaption(Me.gbPaymentInfo.ID, Me.gbPaymentInfo.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.ID, Me.btnProcess.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.gbPaymentApprovalnfo.Text = Language._Object.getCaption(Me.gbPaymentApprovalnfo.ID, Me.gbPaymentApprovalnfo.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.ID, Me.lblApproverLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLevelPriority.Text = Language._Object.getCaption(Me.lblLevelPriority.ID, Me.lblLevelPriority.Text)
            Me.gbPaymentModeInfo.Text = Language._Object.getCaption(Me.gbPaymentModeInfo.ID, Me.gbPaymentModeInfo.Text)
            Me.lblTotalPaid.Text = Language._Object.getCaption(Me.lblTotalPaid.ID, Me.lblTotalPaid.Text)
            Me.lblCashPaid.Text = Language._Object.getCaption(Me.lblCashPaid.ID, Me.lblCashPaid.Text)
            Me.lblbankPaid.Text = Language._Object.getCaption(Me.lblbankPaid.ID, Me.lblbankPaid.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Me.lblPmtVoucher.Text = Language._Object.getCaption(Me.lblPmtVoucher.ID, Me.lblPmtVoucher.Text)

            Me.GvApprovePayment.Columns(4).HeaderText = Language._Object.getCaption(Me.GvApprovePayment.Columns(4).FooterText, Me.GvApprovePayment.Columns(4).HeaderText)
            Me.GvApprovePayment.Columns(5).HeaderText = Language._Object.getCaption(Me.GvApprovePayment.Columns(5).FooterText, Me.GvApprovePayment.Columns(5).HeaderText)
            Me.GvApprovePayment.Columns(6).HeaderText = Language._Object.getCaption(Me.GvApprovePayment.Columns(6).FooterText, Me.GvApprovePayment.Columns(6).HeaderText)
            Me.GvApprovePayment.Columns(8).HeaderText = Language._Object.getCaption(Me.GvApprovePayment.Columns(8).FooterText, Me.GvApprovePayment.Columns(8).HeaderText)



        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :-" & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
