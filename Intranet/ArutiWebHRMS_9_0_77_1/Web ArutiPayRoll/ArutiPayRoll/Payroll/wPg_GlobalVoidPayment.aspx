﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_GlobalVoidPayment.aspx.vb" Inherits="Payroll_wPg_GlobalVoidPayment"
    Title="Global Void Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
            var scroll = {
                    Y: '#<%= hfScrollPosition.ClientID %>'
                };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                  if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());
                }
            }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Void Payment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Employee Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                            Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPaymentDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <uc2:DateCtrl ID="dtpPaymentDate" runat="server"></uc2:DateCtrl>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblVoucherno" runat="server" Text="Voucher No"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="cboPmtVoucher" runat="server">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblPaidAmount" runat="server" Text="Amount From"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtPaidAmount" Style="text-align: right;width:93%;padding-right:10px;" runat="server" Text="0"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblTo" runat="server" Text="To" Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtPaidAmountTo" Style="text-align: right;width:93%;padding-right:10px;" runat="server" 
                                                            Text="0"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                    height: 410px; overflow: auto; vertical-align: top">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="GvVoidPayment" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" Width="99%" CellPadding="3" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" >
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                    OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="paymenttranunkid" HeaderText="paymenttranunkid" ReadOnly="true"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" ReadOnly="true"
                                                        HeaderStyle-HorizontalAlign="Left" FooterText="colhDate" />
                                                    <asp:BoundField DataField="employeecode" HeaderText="Emp. Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                        FooterText="colhEmpCode" />
                                                    <asp:BoundField DataField="EmpName" HeaderText="Employee" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                        FooterText="colhEmployee" />
                                                    <asp:BoundField DataField="PeriodName" HeaderText="Pay Period" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                        FooterText="colhPayPeriod" />
                                                    <asp:BoundField DataField="expaidamt" HeaderText="Paid Amount" ReadOnly="true" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" FooterText="colhPaidAmount" />
                                                    <asp:BoundField DataField="paidcurrency" HeaderText="Currency" ReadOnly="true" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" FooterText="colhCurrency" />
                                                    <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" ReadOnly="true"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="popupDelPayment" EventName="buttonDelReasonYes_Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <div id="Div3" class="panel-body-default">
                                    <div class="btn-default">
                                        <asp:Button ID="btnVoid" runat="server" Text="Void" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:DeleteReason ID="popupDelPayment" runat="server" Title="Are you sure you want to delete selected Payment(s)?" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
