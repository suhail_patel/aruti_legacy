﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ApproverSwapping.aspx.vb" Inherits="Swap_Approver_wPg_ApproverSwapping"
    Title="Approver Swapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
    }
    }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Swap Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblCategory" runat="server"  Text="Expense Category"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboExpenseCat" runat="server"  AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 45%" colspan="2">
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFromApprover" runat="server" Text="From Approver"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboFromApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblToApprover" runat="server" Text="To Approver"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboToApprover" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFromLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboFromLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblToLevel" runat="server" Text="Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboToLevel" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; margin-top: 10px; vertical-align: top">
                                        <tr style="width: 100%">
                                            <td style="width: 45%; vertical-align:top">
                                                <table style="width: 100%; vertical-align: top">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                                width: 100%; max-height: 350px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                <asp:GridView ID="dgFromApprover" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" HeaderStyle-Font-Bold="false"
                                                                    RowStyle-CssClass="griviewitem" Style="margin: auto; max-height:350px" Width="99%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="employeecode" FooterText="dgcolhFromEmpcode" HeaderStyle-Width="30%"
                                                                            HeaderText="Employee Code" ReadOnly="true" />
                                                                        <asp:BoundField DataField="employeename" FooterText="dgcolhFromEmployee" HeaderStyle-Width="70%"
                                                                            HeaderText="Employee" ReadOnly="true" />
                                                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeID" ReadOnly="true"
                                                                            Visible="false" />
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                    <RowStyle CssClass="griviewitem" />
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 10%; text-align: center">
                                                <asp:Button ID="btnSwap" runat="server"  Style="text-align: center" Text="Swap" CssClass="btndefault">
                                                </asp:Button>
                                            </td>
                                            <td style="width: 45%; vertical-align:top">
                                                <table style="width: 100%; vertical-align: top">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="scrollable-container1" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                                width: 100%; max-height: 350px" onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                <asp:GridView ID="dgToApprover" runat="server" Style="margin: auto; max-height:350px" AutoGenerateColumns="false"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="employeecode" HeaderText="Employee Code" HeaderStyle-Width="30%"
                                                                            ReadOnly="true" FooterText="dgcolhToEmpCode" />
                                                                        <asp:BoundField DataField="employeename" HeaderText="Employee" HeaderStyle-Width="70%"
                                                                            ReadOnly="true" FooterText="dgcolhToEmployee" />
                                                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeID" ReadOnly="true"
                                                                            Visible="false" />
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                    <RowStyle CssClass="griviewitem" />
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
