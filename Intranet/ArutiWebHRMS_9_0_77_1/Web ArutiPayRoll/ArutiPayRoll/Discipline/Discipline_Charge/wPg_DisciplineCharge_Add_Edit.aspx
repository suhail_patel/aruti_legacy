﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_DisciplineCharge_Add_Edit.aspx.vb" Inherits="Discipline_Discipline_Charge_wPg_DisciplineCharge_Add_Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="RConfirmation" TagPrefix="ucyesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
            var dtpResponseDt = $('#<%= dtpReponseDate.ClientID %>');
            var cboResponsetype = $('#<%= cboReponseType.ClientID %>');

            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            //            if (parseInt(cboResponsetype.val()) <= 0) {
            //                alert('Response type is compulsory information. Please select Response type to continue.');
            //                cboResponsetype.focus();
            //                return false;
            //            }
            if (dtpResponseDt.val().trim() == "") {
                alert('Sorry, response date is mandatory information. Please give proper response date.');
                dtpResponseDt.focus();
                return false;
            }
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Discipline Charge"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Discipline Details"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 22%">
                                                            <asp:Label ID="lblPersonalInvolved" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 22%">
                                                            <asp:Label ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%">
                                                            <asp:TextBox ID="txtJobTitle" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 22%">
                                                            <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%">
                                                            <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%; vertical-align: top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 24%">
                                                            <asp:Label ID="lblRefNo" runat="server" Style="margin-left: 5px" Text="Reference No."></asp:Label>
                                                        </td>
                                                        <td style="width: 76%" colspan="2">
                                                            <asp:DropDownList ID="cboReferenceNo" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 24%">
                                                            <asp:Label ID="lblDate" runat="server" Style="margin-left: 5px" Text="Charge Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtChargeDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 26%">
                                                            <asp:Label ID="lblInterdictionDate" runat="server" Style="margin-left: 10px" Text="Interdiction Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtInterdictionDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 24%; vertical-align: top">
                                                            <asp:Label ID="lblGeneralChargeDescr" runat="server" Style="margin-left: 5px; margin-top: "
                                                                Text="Charge Description"></asp:Label>
                                                        </td>
                                                        <td style="width: 76%" colspan="2">
                                                            <asp:TextBox ID="txtChargeDescription" runat="server" TextMode="MultiLine" Rows="3"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="2">
                                                <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblChargeCount" runat="server" Text="Charge Count(s)"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div3" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server" Style="max-height: 350px">
                                                                        <asp:DataGrid ID="dgvChargeCountList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                                            <ItemStyle CssClass="griviewitem" />
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="charge_count" FooterText="dgcolhCount" HeaderText="Count"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--0--%>
                                                                                <asp:BoundColumn DataField="incident_description" FooterText="dgcolhIncident" HeaderText="Incident"
                                                                                    ReadOnly="true" />
                                                                                <%--1--%>
                                                                                <asp:BoundColumn DataField="charge_category" FooterText="dgcolhOffCategory" HeaderText="Offence Category"
                                                                                    ReadOnly="true" />
                                                                                <%--2--%>
                                                                                <asp:BoundColumn DataField="charge_descr" FooterText="dgcolhOffence" HeaderText="Offence Discription"
                                                                                    ReadOnly="true" />
                                                                                <%--3--%>
                                                                                <asp:BoundColumn DataField="charge_severity" FooterText="dgcolhSeverity" HeaderText="Severity"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--4--%>
                                                                                <asp:BoundColumn DataField="disciplinefileunkid" FooterText="objcolhfileunkid" ReadOnly="true"
                                                                                    Visible="false" />
                                                                                <%--5--%>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderText="Add Response" ItemStyle-Width="90px" FooterText="dglnkCol">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                                            Font-Underline="false"><i class="fa fa-comments" aria-hidden="true" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <%--6--%>
                                                                                <asp:BoundColumn DataField="disciplinefiletranunkid" Visible="false" />
                                                                                <%--7--%>
                                                                                <asp:BoundColumn DataField="response_date" FooterText="dgcolhResponseDate" HeaderText="Response Date"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--8--%>
                                                                                <asp:BoundColumn DataField="response_type" FooterText="dgcolhResponseType" HeaderText="Response Type"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--9--%>
                                                                                <asp:BoundColumn DataField="response_remark" FooterText="dgcolhResponseText" HeaderText="Response Remark"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true" Visible="false">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--10--%>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderText="Remove Response" ItemStyle-Width="90px" FooterText="dglnkResponsevoid">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkResponsevoid" runat="server" OnClick="delete_Click" CommandName="remove_response"
                                                                                            Font-Underline="false"><i class="fa fa-trash-o" aria-hidden="true" style="font-size:20px;color:Red"></i></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <%--11--%>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderText="Download Attachment" ItemStyle-Width="90px" FooterText="dglnkDownloadAttachment">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkDownloadAttachment" runat="server" OnClick="linkDownload_Click"
                                                                                            CommandName="download_attachment" Font-Underline="false"><i class="fa fa-download" aria-hidden="true" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <%--12--%>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="text-align: left; position: absolute">
                                            <asp:Button ID="btnViewScheduling" runat="server" Text="View Scheduling" CssClass="btndefault" />
                                        </div>
                                        <asp:Button ID="btnFinalSave" runat="server" Text="Final Save" CssClass="btndefault" />
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalPopupBG2"
                                        CancelControlID="btnSClose" PopupControlID="Panel2" TargetControlID="hdnfieldDelReason">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Panel ID="Panel2" runat="server" CssClass="newpopup" Style="display: none; width: 600px;
                                        z-index: 100002!important;" DefaultButton="btnSOk">
                                        <div class="panel-primary" style="margin-bottom: 0px;">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblHearingSchduling" runat="server" Text="Hearing Scheduling Details"></asp:Label>
                                            </div>
                                            <div class="panel-body">
                                                <div id="Div6" class="panel-default">
                                                    <div id="Div7" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblHearingDate" runat="server" Text="Hearing Date"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%">
                                                                    <asp:TextBox ID="txtHearingDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblHearingTime" runat="server" Text="Hearing Time"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%">
                                                                    <asp:TextBox ID="txtHearingTime" runat="server" ReadOnly="true"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblVenue" runat="server" Text="Venue"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%">
                                                                    <asp:TextBox ID="txtVenue" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblHeadingRemark" runat="server" Text="Remark"></asp:Label>
                                                                </td>
                                                                <td colspan="5">
                                                                    <asp:TextBox ID="txtHearingRemark" runat="server" ReadOnly="true" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnSOk" runat="server" Text="Ok" Width="70px" Style="margin-right: 10px"
                                                                CssClass="btnDefault" Visible="false" />
                                                            <asp:Button ID="btnSClose" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                                                CssClass="btnDefault" />
                                                            <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <ajaxToolkit:ModalPopupExtender ID="popup_DisciplineCharge" runat="server" BackgroundCssClass="ModalPopupBG2"
                                        CancelControlID="hdf_DiscplineCharge" PopupControlID="pnl_DisciplineCharge" TargetControlID="hdf_DiscplineCharge">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <asp:Panel ID="pnl_DisciplineCharge" runat="server" CssClass="newpopup" Style="display: none;
                                        width: 800px; z-index: 100002!important; height: 450px;" DefaultButton="btnSOk">
                                        <div class="panel-primary">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblDiscplinechargeHeader" runat="server" Text="Add/Edit Charge Count Response"></asp:Label>
                                            </div>
                                            <div id="Div4" class="panel-body-default" style="height: 390px;">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblOffenceCategory" runat="server" Text="Offence Category"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtOffenceCategory" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblOffenceDescription" runat="server" Text="Offence Description"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtOffenceDescription" ReadOnly="true" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblIncident" runat="server" Text="Incident"></asp:Label>
                                                        </td>
                                                        <td colspan="2">
                                                            <asp:TextBox ID="txtIncident" runat="server" Rows="3" TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblResponseDate" runat="server" Text="Response Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <uc1:DateCtrl ID="dtpReponseDate" runat="server" Enabled="false" />
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblResponseType" runat="server" Text="Response Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:DropDownList ID="cboReponseType" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblResponse" runat="server" Text="Response"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%" colspan="2">
                                                            <asp:TextBox ID="txtResponseDescription" runat="server" Rows="3" TextMode="MultiLine"
                                                                Width="99%"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblDocType" runat="server" Text="Document Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server" Width="99%">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 45%">
                                                            <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                <div id="fileuploader">
                                                                    <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                                        value="Save Attachment" />
                                                                </div>
                                                            </asp:Panel>
                                                            <%--'Hemant (30 Nov 2018) -- Start
                                                            'Enhancement : Including Language Settings For Scan/Attachment Button--%>
                                                            <%--<asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click" Text="Browse" />--%>
                                                            <asp:Button ID="btnScanAttachDoc" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                                Text="Browse" />
                                                            <%--'Hemant (30 Nov 2018) -- End--%>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%;">
                                                        <td colspan="3">
                                                            <asp:Panel ID="pnl_dgvResponse" runat="server" ScrollBars="Auto" Style="max-height: 390px"
                                                                Height="100px">
                                                                <asp:DataGrid ID="dgvResponse" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                    HeaderStyle-Font-Bold="false">
                                                                    <Columns>
                                                                        <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                        ToolTip="Delete"></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--0--%>
                                                                        <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                        <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                            <ItemTemplate>
                                                                                <span class="gridiconbc">
                                                                                    <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                </span>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--1--%>
                                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                        <%--2--%>
                                                                        <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                            Visible="false" />
                                                                        <%--3--%>
                                                                        <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                            Visible="false" />
                                                                        <%--4--%>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                    <div style="float: left">
                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                    </div>
                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                    <asp:Button ID="btnFilesave" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnFileClose" runat="server" Text="Close" CssClass="btndefault" />
                                                    <asp:HiddenField ID="hdf_DiscplineCharge" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                    <ucyesno:RConfirmation ID="popup_response" runat="server" Message="" Title="Aruti" />
                </ContentTemplate>
                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="dgvResponse" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                    <asp:PostBackTrigger ControlID="dgvChargeCountList" />                    
                </Triggers>
                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");

        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_DisciplineCharge_Add_Edit.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                    $("#<%= btnScanAttachDoc.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>
