﻿Option Strict On

#Region " Imports "
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.eZeeDataType
Imports Aruti.Data.clsEmployee_Master
Imports eZeeCommonLib.clsDataOperation
Imports System.IO
Imports System.Configuration
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading

#End Region

Partial Class Discipline_Discipline_Charge_wPg_DisciplineCharge_Add_Edit
    Inherits Basepage

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "frmDisciplineFiling"
    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private ReadOnly mstrModuleName1 As String = "frmAddEditResponse"
    'S.SANDEEP |01-OCT-2019| -- END
    Dim DisplayMessage As New CommonCodes
    Private objDisciplineFileMaster As clsDiscipline_file_master
    Private mdtCharge As New DataTable
    Private objCONN As SqlConnection
    Private mintEmployeeId As Integer = 0
    Private mintDisciplineFileUnkid As Integer = 0

    Private mdtAttchment As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    Private mintDeleteIndex As Integer = 0
    Private mintDisiciplineFileTranUnkId As Integer = 0
    Private mblnIsVisibleChargesPopup As Boolean = False

#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            SetControlCaptions()
            Call SetLanguage()
            'S.SANDEEP |11-NOV-2019| -- END
            If Session("clsuser") Is Nothing OrElse (Request.QueryString.Count > 0 AndAlso Request.QueryString("uploadimage") Is Nothing) Then
                Call DirectNotificationLink()
                If Session("isUrlPost") Is Nothing Then
                    Session("isUrlPost") = True
                End If
                'Exit Sub
            Else
                If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If
                If IsPostBack = False Then
                    Call FillCombo()
                End If
            End If


            If IsPostBack Then
                    mdtCharge = CType(Me.ViewState("ChargeTable"), DataTable)
                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                mintDisiciplineFileTranUnkId = CInt(Me.ViewState("mintDisiciplineFileTranUnkId"))
                mblnIsVisibleChargesPopup = CBool(Me.ViewState("mblnIsVisibleChargesPopup"))
                End If

            If mblnIsVisibleChargesPopup = True Then
                popup_DisciplineCharge.Show()
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            lblInterdictionDate.Visible = CBool(Session("ShowInterdictionDate"))
            txtInterdictionDate.Visible = CBool(Session("ShowInterdictionDate"))

            lblResponseType.Visible = CBool(Session("ShowReponseTypeOnPosting"))
            cboReponseType.Visible = CBool(Session("ShowReponseTypeOnPosting"))
            'S.SANDEEP |11-NOV-2019| -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1 :-" & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1 :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ChargeTable") = mdtCharge
            Session("mdtAttchment") = mdtAttchment
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            Me.ViewState("mintDisiciplineFileTranUnkId") = mintDisiciplineFileTranUnkId
            Me.ViewState("mblnIsVisibleChargesPopup") = mblnIsVisibleChargesPopup
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender :-" & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub DirectNotificationLink()
        Try
            If IsPostBack = False Then
                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim objPwdOpt As New clsPassowdOptions
                    Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length > 0 Then
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        mintDisciplineFileUnkid = CInt(arr(1))
                        mintEmployeeId = CInt(arr(2))

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        'ArtLic._Object = New ArutiLic(False)
                        'If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        '    Dim objGroupMaster As New clsGroup_Master
                        '    objGroupMaster._Groupunkid = 1
                        '    ArtLic._Object.HotelName = objGroupMaster._Groupname
                        'End If

                        'If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False) Then
                        '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        '    Exit Sub
                        'End If

                        'If ConfigParameter._Object._IsArutiDemo Then
                        '    If ConfigParameter._Object._IsExpire Then
                        '        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                        '        Exit Try
                        '    Else
                        '        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                        '            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                        '            Exit Try
                        '        End If
                        '    End If
                        'End If

                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try
                        Dim objUser As New clsUserAddEdit
                        'objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()

                        If HttpContext.Current.Session("EmployeeAsOnDate") Is Nothing Then
                            HttpContext.Current.Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                        End If

                        Dim objEmp As New clsEmployee_Master
                        objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = mintEmployeeId

                        'S.SANDEEP |01-OCT-2019| -- START
                        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                        'Dim clsuser As New User(objEmp._Displayname, objEmp._Password, Session("mdbname").ToString, mintEmployeeId)

                        'HttpContext.Current.Session("clsuser") = clsuser
                        'HttpContext.Current.Session("UserName") = clsuser.UserName
                        'HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        'HttpContext.Current.Session("Surname") = clsuser.Surname
                        'HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        'HttpContext.Current.Session("UserId") = clsuser.UserID
                        'HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        ''S.SANDEEP [25 JUL 2016] -- START
                        ''ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                        'HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        ''S.SANDEEP [25 JUL 2016] -- START
                        'HttpContext.Current.Session("Password") = clsuser.password
                        'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        'HttpContext.Current.Session("DisplayName") = clsuser.DisplayName
                        Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""
                        xUName = objEmp._Displayname : xPasswd = objEmp._Password
                        HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                        HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                        HttpContext.Current.Session("Employeeunkid") = mintEmployeeId
                        objEmp = Nothing

                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                        HttpContext.Current.Session("DisplayName") = xUName

                        HttpContext.Current.Session("UserName") = xUName
                        HttpContext.Current.Session("MemberName") = xUName
                        'Gajanan [27-June-2019] -- Start      
                        'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                        HttpContext.Current.Session("LangId") = 1
                        'S.SANDEEP |01-OCT-2019| -- END

                        strError = ""

                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If
                        'S.SANDEEP |01-OCT-2019| -- START
                        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                        'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        'S.SANDEEP |01-OCT-2019| -- END

                        objDisciplineFileMaster = New clsDiscipline_file_master
                        objDisciplineFileMaster._Disciplinefileunkid = mintDisciplineFileUnkid
                        'If CInt(objDisciplineFileMaster._Responsetypeunkid) > 0 AndAlso objDisciplineFileMaster._Response_Date <> Nothing Then
                        '    DisplayMessage.DisplayMessage("Sorry, Response is already given for posted Charge.", Me, Session("rootpath").ToString() & "Index.aspx")
                        'End If

                        Call FillCombo()
                        cboReferenceNo.SelectedValue = CStr(mintDisciplineFileUnkid)
                        Call cboReferenceNo_SelectedIndexChanged(cboReferenceNo, New EventArgs())
                        'dtpResponseDate.SetDate = CDate(DateAndTime.Now.ToShortDateString).Date

                        'S.SANDEEP |11-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                        lblInterdictionDate.Visible = CBool(Session("ShowInterdictionDate"))
                        txtInterdictionDate.Visible = CBool(Session("ShowInterdictionDate"))

                        lblResponseType.Visible = CBool(Session("ShowReponseTypeOnPosting"))
                        cboReponseType.Visible = CBool(Session("ShowReponseTypeOnPosting"))
                        'S.SANDEEP |11-NOV-2019| -- END

                    Else
                        Response.Redirect("../Index.aspx", False)
                        Exit Sub
                    End If
                Else
                    Response.Redirect("../Index.aspx", False)
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("DirectNotificationLink :-" & ex.Message, Me)
            DisplayMessage.DisplayError("DirectNotificationLink :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objCMaster As New clsCommon_Master

            Dim objglobalassess = New GlobalAccess
            objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
            cboEmployee.DataSource = objglobalassess.ListOfEmployee
            cboEmployee.DataTextField = "loginname"
            cboEmployee.DataValueField = "employeeunkid"
            cboEmployee.DataBind()

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.DISC_REPONSE_TYPE, True, "List")
            With cboReponseType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            Call cboEmployee_SelectedIndexChanged(cboEmployee, New EventArgs())

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :-" & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillChargeList()
        Try
            If mdtCharge IsNot Nothing Then
                dgvChargeCountList.AutoGenerateColumns = False
                dgvChargeCountList.DataSource = mdtCharge
                dgvChargeCountList.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillChargeList :-" & ex.Message, Me)
            DisplayMessage.DisplayError("FillChargeList :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValueByPersonInvolved()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim objJob As New clsJobs
            Dim objDepartment As New clsDepartment

            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmployee.SelectedValue)

            objJob._Jobunkid = objEmployee._Jobunkid
            objDepartment._Departmentunkid = objEmployee._Departmentunkid

            txtJobTitle.Text = objJob._Job_Name
            txtDepartment.Text = objDepartment._Name

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValueByPersonInvolved :-" & ex.Message, Me)
            DisplayMessage.DisplayError("GetValueByPersonInvolved :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValueByReferenceNo()
        Try
            objDisciplineFileMaster = New clsDiscipline_file_master
            Dim objDisciplineFileTran As New clsDiscipline_file_tran

            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)

            If objDisciplineFileMaster._Chargedate <> Nothing Then
                txtChargeDate.Text = CStr(CDate(objDisciplineFileMaster._Chargedate).Date)
            Else
                txtChargeDate.Text = ""
            End If

            If objDisciplineFileMaster._Interdictiondate <> Nothing Then
                txtInterdictionDate.Text = CStr(CDate(objDisciplineFileMaster._Interdictiondate).Date)
            Else
                txtInterdictionDate.Text = ""
            End If

            txtChargeDescription.Text = objDisciplineFileMaster._Charge_Description

            '=====================  Fill Charges Counts ====================================
            objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            mdtCharge = objDisciplineFileTran._ChargesTable

            Call FillChargeList()

            If objDisciplineFileMaster._IsFinal = True Then
                btnSave.Visible = False : btnFinalSave.Visible = False
                dgvChargeCountList.Columns(6).Visible = False
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                dgvChargeCountList.Columns(12).Visible = True
            Else
                dgvChargeCountList.Columns(12).Visible = False
                'S.SANDEEP |01-OCT-2019| -- END
            End If

            'If objDisciplineFileMaster._Response_Date = Nothing Then
            '    dtpResponseDate.SetDate = CDate(DateAndTime.Now.ToShortDateString).Date
            'Else
            '    dtpResponseDate.SetDate = CDate(objDisciplineFileMaster._Response_Date).Date
            'End If

            'If CInt(objDisciplineFileMaster._Responsetypeunkid) > 0 Then
            '    cboResponseType.SelectedValue = objDisciplineFileMaster._Responsetypeunkid.ToString
            'End If

            'If objDisciplineFileMaster._Response_Remark <> "" Then
            '    txtEmployeeResponse.Text = objDisciplineFileMaster._Response_Remark
            'Else
            '    txtEmployeeResponse.Text = ""
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValueByReferenceNo :-" & ex.Message, Me)
            DisplayMessage.DisplayError("GetValueByReferenceNo :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False

            ElseIf CInt(cboReferenceNo.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Reference No. is compulsory information. Please select Employee to continue."), Me)
                cboReferenceNo.Focus()
                Return False

                'ElseIf CInt(cboResponseType.SelectedValue) <= 0 Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, Response Type is compulsory information. Please select Response Type to continue."), Me)
                '    cboReferenceNo.Focus()
                '    Return False

                'ElseIf txtEmployeeResponse.Text.Trim = "" Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Employee Response is compulsory information. Please give Employee Response to continue."), Me)
                '    cboReferenceNo.Focus()
                '    Return False
            End If

            objDisciplineFileMaster = New clsDiscipline_file_master
            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)

            'If dtpResponseDate.GetDate.Date < CDate(objDisciplineFileMaster._Chargedate).Date Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Response Date cannot be less than Charge Date."), Me)
            '    cboReferenceNo.Focus()
            '    Return False
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate :-" & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidate :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub SetValue()
        Try
            objDisciplineFileMaster = New clsDiscipline_file_master

            objDisciplineFileMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            'objDisciplineFileMaster._Response_Date = dtpResponseDate.GetDate.Date
            'objDisciplineFileMaster._Responsetypeunkid = CInt(cboResponseType.SelectedValue)
            'objDisciplineFileMaster._Response_Remark = txtEmployeeResponse.Text.Trim
            objDisciplineFileMaster._LoginemployeeId = CInt(Session("Employeeunkid"))
            objDisciplineFileMaster._LoginTypeId = enLogin_Mode.EMP_SELF_SERVICE

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue :-" & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            cboReferenceNo.SelectedValue = CStr(0)
            Call cboReferenceNo_SelectedIndexChanged(cboReferenceNo, New EventArgs())
            Call GetValueByReferenceNo()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearControls :-" & ex.Message, Me)
            DisplayMessage.DisplayError("ClearControls :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttchment Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttchment, "AUD <> 'D' AND form_name = 'frmChargeCountResponse' ", "", DataViewRowState.CurrentRows)
            dgvResponse.AutoGenerateColumns = False
            dgvResponse.DataSource = dtView
            dgvResponse.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillAttachment", mstrModuleName)
            DisplayMessage.DisplayError("FillAttachment :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtAttchment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtAttchment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Discipline_Module
                dRow("scanattachrefid") = enScanAttactRefId.DISCIPLINES
                dRow("transactionunkid") = mintDisiciplineFileTranUnkId
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length 'objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = Today.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
                dRow("form_name") = "frmChargeCountResponse"
                dRow("userunkid") = CInt(Session("userid"))
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtAttchment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
            DisplayMessage.DisplayError("AddDocumentAttachment :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Void_Response()
        Try
            Dim dRow() As DataRow = mdtCharge.Select("disciplinefiletranunkid = '" & mintDisiciplineFileTranUnkId & "'")
            If dRow.Count > 0 Then
                dRow(0).Item("response_date") = DBNull.Value
                dRow(0).Item("responsetypeunkid") = 0
                dRow(0).Item("response_remark") = ""
                dRow(0).Item("AUD") = "U"
            End If
            mdtCharge.AcceptChanges()
            mdtAttchment = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, mintDisiciplineFileTranUnkId, CStr(Session("Document_Path"))).Copy
            Dim dRows As DataRow() = mdtAttchment.Select("AUD <> 'D' AND form_name = 'frmChargeCountResponse'")
            dRows.ToList.ForEach(Function(x) Update_DataRow(x))
            mdtAttchment.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError("Void_Response:- " & ex.Message, Me)
        End Try
    End Sub

    Private Function Update_DataRow(ByVal dr As DataRow) As Boolean
        Try
            dr.Item("AUD") = "D"
            dr.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError("Void_Response:- " & ex.Message, Me)
        Finally
        End Try
    End Function

    Private Sub Charges_GetData()
        Try
            Dim dRow() As DataRow = mdtCharge.Select("disciplinefiletranunkid = '" & mintDisiciplineFileTranUnkId & "'")
            If dRow.Count > 0 Then
                txtOffenceCategory.Text = dRow(0).Item("charge_category").ToString
                txtOffenceDescription.Text = dRow(0).Item("charge_descr").ToString
                txtIncident.Text = dRow(0).Item("incident_description").ToString
                If IsDBNull(dRow(0).Item("response_date")) = False Then
                    dtpReponseDate.SetDate = CDate(dRow(0).Item("response_date"))
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                Else
                    dtpReponseDate.SetDate = Now.Date
                    'S.SANDEEP |01-OCT-2019| -- END
                End If
                cboReponseType.SelectedValue = dRow(0).Item("responsetypeunkid").ToString
                txtResponseDescription.Text = dRow(0).Item("response_remark").ToString
            End If
            mdtAttchment = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DISCIPLINES, mintDisiciplineFileTranUnkId, CStr(Session("Document_Path"))).Copy
            Call FillAttachment()

            'objDisciplineMaster._Disciplinefileunkid = mintDisiciplineFileUnkId
            'mintEmployeeUnkId = objDisciplineMaster._Involved_Employeeunkid
            'If objDisciplineMaster._IsFinal = True Then
            '    btnSave.Visible = False
            '    dgvResponse.Columns(0).Visible = False
            '    pnl_ImageAdd.Visible = False
            '    lblDocType.Visible = False
            '    cboDocumentType.Visible = False
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError("GetData:- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub Charges_SetData()
        Try
            Dim dRow() As DataRow = mdtCharge.Select("disciplinefiletranunkid = '" & mintDisiciplineFileTranUnkId & "'")
            If dRow.Count > 0 Then
                dRow(0).Item("AUD") = "U"
                dRow(0).Item("response_date") = dtpReponseDate.GetDate.Date
                dRow(0).Item("response_remark") = txtResponseDescription.Text
                dRow(0).Item("response_type") = cboReponseType.SelectedItem.Text
                dRow(0).Item("responsetypeunkid") = cboReponseType.SelectedValue
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("SetData:- " & ex.Message, Me)
        End Try
    End Sub

    Private Function Charges_IsValidate() As Boolean
        Try
            If dtpReponseDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date."), Me)
                Return False
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'If CInt(cboReponseType.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue."), Me)
            '    Return False
            'End If
            If cboReponseType.Visible = True AndAlso CInt(cboReponseType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Response type is mandatory information. Please select Response type to continue."), Me)
                Return False
            End If
            'S.SANDEEP |11-NOV-2019| -- END


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("IsValidate:- " & ex.Message, Me)
        End Try
    End Function

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Sub Fill_Hearing()
        Dim objSchedular As New clshearing_schedule_master
        Dim dsList As New DataSet
        Try
            dsList = objSchedular.GetList(Session("Database_Name").ToString(), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                          Session("UserAccessModeSetting").ToString(), _
                                          True, CBool(Session("IsIncludeInactiveEmp")), _
                                          "List", "hrdiscipline_file_master.disciplinefileunkid = '" & CInt(cboReferenceNo.SelectedValue) & "'", False)

            If dsList.Tables("List").Rows.Count > 0 Then
                txtHearingDate.Text = CDate(dsList.Tables(0).Rows(0)("hearing_date")).ToShortDateString()
                txtHearingTime.Text = Format(dsList.Tables(0).Rows(0)("hearning_time"), "HH:mm")
                txtVenue.Text = dsList.Tables(0).Rows(0)("hearning_venue").ToString()
                txtHearingRemark.Text = dsList.Tables(0).Rows(0)("remark").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Fill_Hearing : " & ex.Message, Me)
        Finally
            objSchedular = Nothing
        End Try
    End Sub
    'S.SANDEEP |01-OCT-2019| -- END

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(Me.btnViewScheduling.ID, Me.btnViewScheduling.Text)
            Language._Object.setCaption(dgvChargeCountList.Columns(6).FooterText, dgvChargeCountList.Columns(6).HeaderText)
            Language._Object.setCaption(dgvChargeCountList.Columns(10).FooterText, dgvChargeCountList.Columns(10).HeaderText)
            Language._Object.setCaption(dgvChargeCountList.Columns(11).FooterText, dgvChargeCountList.Columns(11).HeaderText)

            Language._Object.setCaption(mstrModuleName, Me.lblPageHeader.Text)
            Language._Object.setCaption("gbDiscipilneFiling", Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.lblPersonalInvolved.ID, Me.lblPersonalInvolved.Text)
            Language._Object.setCaption(Me.lblJobTitle.ID, Me.lblJobTitle.Text)
            Language._Object.setCaption(Me.lblDepartment.ID, Me.lblDepartment.Text)
            Language._Object.setCaption(Me.lblRefNo.ID, Me.lblRefNo.Text)
            Language._Object.setCaption(Me.lblDate.ID, Me.lblDate.Text)
            Language._Object.setCaption(Me.lblInterdictionDate.ID, Me.lblInterdictionDate.Text)
            Language._Object.setCaption(Me.lblGeneralChargeDescr.ID, Me.lblGeneralChargeDescr.Text)
            Language._Object.setCaption(Me.lblResponseDate.ID, Me.lblResponseDate.Text)
            Language._Object.setCaption(Me.lblResponseType.ID, Me.lblResponseType.Text)
            Language._Object.setCaption("elLine1", Me.lblChargeCount.Text)

            Language._Object.setCaption(Me.btnFinalSave.ID, Me.btnFinalSave.Text)
            Language._Object.setCaption(Me.btnSave.ID, Me.btnSave.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(Me.btnScanAttachDoc.ID, Me.btnScanAttachDoc.Text)
            Language._Object.setCaption(Me.btnScanAttachDoc.ID, Me.btnScanAttachDoc.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError("SetControlCaptions : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |11-NOV-2019| -- END


    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbDiscipilneFiling", Me.lblDetialHeader.Text)
            Me.lblPersonalInvolved.Text = Language._Object.getCaption(Me.lblPersonalInvolved.ID, Me.lblPersonalInvolved.Text)
            Me.lblJobTitle.Text = Language._Object.getCaption(Me.lblJobTitle.ID, Me.lblJobTitle.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(Me.lblRefNo.ID, Me.lblRefNo.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblInterdictionDate.Text = Language._Object.getCaption(Me.lblInterdictionDate.ID, Me.lblInterdictionDate.Text)
            Me.lblGeneralChargeDescr.Text = Language._Object.getCaption(Me.lblGeneralChargeDescr.ID, Me.lblGeneralChargeDescr.Text)
            Me.lblResponseDate.Text = Language._Object.getCaption(Me.lblResponseDate.ID, Me.lblResponseDate.Text)
            Me.lblResponseType.Text = Language._Object.getCaption(Me.lblResponseType.ID, Me.lblResponseType.Text)
            Me.lblChargeCount.Text = Language._Object.getCaption("elLine1", Me.lblChargeCount.Text)

            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.ID, Me.btnFinalSave.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvChargeCountList.Columns(0).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(0).FooterText, Me.dgvChargeCountList.Columns(0).HeaderText)
            Me.dgvChargeCountList.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(1).FooterText, Me.dgvChargeCountList.Columns(1).HeaderText)
            Me.dgvChargeCountList.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(2).FooterText, Me.dgvChargeCountList.Columns(2).HeaderText)
            Me.dgvChargeCountList.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(3).FooterText, Me.dgvChargeCountList.Columns(3).HeaderText)
            Me.dgvChargeCountList.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(4).FooterText, Me.dgvChargeCountList.Columns(4).HeaderText)
            Me.dgvChargeCountList.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(8).FooterText, Me.dgvChargeCountList.Columns(8).HeaderText)
            Me.dgvChargeCountList.Columns(9).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(9).FooterText, Me.dgvChargeCountList.Columns(9).HeaderText)
            Me.dgvChargeCountList.Columns(10).HeaderText = Language._Object.getCaption(Me.dgvChargeCountList.Columns(10).FooterText, Me.dgvChargeCountList.Columns(10).HeaderText)

            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            btnScanAttachDoc.Text = Language._Object.getCaption(Me.btnScanAttachDoc.ID, Me.btnScanAttachDoc.Text).Replace("&", "")
            btnAddFile.Value = Language._Object.getCaption(Me.btnScanAttachDoc.ID, Me.btnScanAttachDoc.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- End

            Me.btnViewScheduling.Text = Language._Object.getCaption(Me.btnViewScheduling.ID, Me.btnViewScheduling.Text)
            dgvChargeCountList.Columns(6).HeaderText = Language._Object.getCaption(dgvChargeCountList.Columns(6).FooterText, dgvChargeCountList.Columns(6).HeaderText)
            dgvChargeCountList.Columns(11).HeaderText = Language._Object.getCaption(dgvChargeCountList.Columns(10).FooterText, dgvChargeCountList.Columns(11).HeaderText)
            dgvChargeCountList.Columns(12).HeaderText = Language._Object.getCaption(dgvChargeCountList.Columns(11).FooterText, dgvChargeCountList.Columns(12).HeaderText)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objDisciplineFileMaster As New clsDiscipline_file_master

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Call GetValueByPersonInvolved()

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'dsList = objDisciplineFileMaster.GetComboList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                              CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, , _
                '                                              CInt(cboEmployee.SelectedValue), , , , False)
                dsList = objDisciplineFileMaster.GetComboList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                              CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, , _
                                                              CInt(cboEmployee.SelectedValue), , , , False, False)
                'S.SANDEEP |11-NOV-2019| -- END

                With cboReferenceNo
                    .DataValueField = "disciplinefileunkid"
                    .DataTextField = "reference_no"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    If dsList.Tables("List").Rows.Count = 2 Then
                        .SelectedValue = CStr(dsList.Tables("List").Rows(1).Item("disciplinefileunkid"))
                        Call cboReferenceNo_SelectedIndexChanged(cboReferenceNo, New EventArgs())
                    Else
                        .SelectedValue = CStr(0)
                    End If
                End With
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError("cboEmployee_SelectedIndexChanged :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged
        Try
            If CInt(cboReferenceNo.SelectedValue) > 0 Then
                Call GetValueByReferenceNo()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboReferenceNo_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError("cboReferenceNo_SelectedIndexChanged :-" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("ChargeTable") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click" & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnFilesave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilesave.Click
        Dim objDisciplineTran As New clsDiscipline_file_tran
        Dim mdsDoc As DataSet
        Dim mstrFolderName As String = ""
        Dim strFileName As String = ""
        Try
            If Charges_IsValidate() = False Then Exit Sub
            Call Charges_SetData()
            If mdtCharge IsNot Nothing Then
                mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                For Each dRow As DataRow In mdtAttchment.Rows

                    mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                    If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                        'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                        strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                        'Pinkal (20-Nov-2018) -- End
                        If File.Exists(CStr(dRow("localpath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            File.Move(CStr(dRow("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError("File connot exitst localpath", Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError("File connot exitst localpath", Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError("File connot exitst localpath", Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    End If

                Next

                objDisciplineTran._ChargesTable = mdtCharge
                With objDisciplineTran
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With
                If objDisciplineTran.InsertUpdateDelete_DisciplineCharges(CInt(Session("userid")), ConfigParameter._Object._CurrentDateAndTime, mdtAttchment, "frmChargeCountResponse") Then
                    'DisplayMessage.DisplayMessage("Information Sucssfully save", Me, Session("rootpath").ToString & "Discipline/Discipline_Charge/wPg_DisciplineCharge_Add_Edit.aspx")
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'DisplayMessage.DisplayMessage("Information Sucssfully save", Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 500, "Response added successfully"), Me)
                    'S.SANDEEP |01-OCT-2019| -- END
                    mdtAttchment = Nothing : mintDisiciplineFileTranUnkId = 0 : mblnIsVisibleChargesPopup = False : popup_DisciplineCharge.Hide()
                    Dim objDisciplineFileTran As New clsDiscipline_file_tran
                    objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
                    mdtCharge = objDisciplineFileTran._ChargesTable
                    objDisciplineFileTran = Nothing
                    Call FillChargeList()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("btnFilesave_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnFileClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileClose.Click
        mdtAttchment = Nothing
        mblnIsVisibleChargesPopup = False
        mintDisiciplineFileTranUnkId = 0
        popup_DisciplineCharge.Hide()
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If IsValidate() = False Then Exit Sub
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtAttchment = CType(Session("mdtAttchment"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSaveAttachment_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteIndex > 0 Then
                mdtAttchment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtAttchment.AcceptChanges()
                mintDeleteIndex = 0
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonYes_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then
                mintDeleteIndex = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonNo_Click:- " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim blnFlag As Boolean

            objDisciplineFileMaster = New clsDiscipline_file_master

            If IsValidate() = False Then Exit Sub

            Call SetValue()

            Dim lstName As New List(Of String)(New String() {"frmChargeCountResponse"})
            blnFlag = objDisciplineFileMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtAttchment, "frmChargeCountResponse", _
                                                     CInt(Session("CompanyUnkId")), Session("Document_Path").ToString, lstName, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Database_Name").ToString, CInt(Session("Fin_year")), Nothing, True, Session("ArutiSelfServiceURL").ToString)

            If blnFlag = False AndAlso objDisciplineFileMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objDisciplineFileMaster._Message, Me)
                Exit Sub
            Else
                Call ClearControls()
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'DisplayMessage.DisplayMessage("Discipline Charge Information Saved Successfully.", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 501, "Response Saved Successfully."), Me)
                'S.SANDEEP |01-OCT-2019| -- END
                If Request.QueryString.Count > 0 Then
                    Response.Redirect("~/Index.aspx", False)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("btnSave_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |01-OCT-2019| -- END

    Protected Sub btnFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Try
            Dim blnFlag As Boolean

            Dim strmessage As String = String.Empty
            Dim objDisciplineTran As New clsDiscipline_file_tran
            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'strmessage = objDisciplineTran.IsRespondedAll_Counts(CInt(cboReferenceNo.SelectedValue), mdtCharge)
            If cboReponseType.Visible Then
                strmessage = objDisciplineTran.IsRespondedAll_Counts(CInt(cboReferenceNo.SelectedValue), mdtCharge, True)
            Else
                strmessage = objDisciplineTran.IsRespondedAll_Counts(CInt(cboReferenceNo.SelectedValue), mdtCharge, False)
            End If
            'S.SANDEEP |11-NOV-2019| -- END
            If strmessage.Trim.Length > 0 Then
                objDisciplineTran = Nothing
                DisplayMessage.DisplayMessage(strmessage, Me)
                Exit Sub
            End If
            objDisciplineTran = Nothing

            objDisciplineFileMaster = New clsDiscipline_file_master

            If IsValidate() = False Then Exit Sub

            Call SetValue()
            objDisciplineFileMaster._IsFinal = True

            Dim lstName As New List(Of String)(New String() {"frmChargeCountResponse"})
            With objDisciplineFileMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            blnFlag = objDisciplineFileMaster.Update(ConfigParameter._Object._CurrentDateAndTime, mdtAttchment, "frmChargeCountResponse", _
                                                     CInt(Session("CompanyUnkId")), Session("Document_Path").ToString, lstName, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("Database_Name").ToString, CInt(Session("Fin_year")), Nothing, True, Session("ArutiSelfServiceURL").ToString)

            If blnFlag = False AndAlso objDisciplineFileMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objDisciplineFileMaster._Message, Me)
                Exit Sub
            Else
                Call ClearControls()
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'DisplayMessage.DisplayMessage("Discipline Charge Information Saved Successfully.", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 502, "Response submitted Successfully."), Me)
                'S.SANDEEP |01-OCT-2019| -- END
                If Request.QueryString.Count > 0 Then
                    Response.Redirect("~/Index.aspx", False)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnFinalSave_Click" & ex.Message, Me)
            DisplayMessage.DisplayError("btnFinalSave_Click" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvResponse.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboReferenceNo.SelectedItem.Text.Replace(" ", "") + ".zip", mdtAttchment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnDownloadAll_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

    Protected Sub btnViewScheduling_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewScheduling.Click
        Try         'Hemant (13 Aug 2020)
        'S.SANDEEP |01-OCT-2019| -- START
        'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
        Call Fill_Hearing()
        'S.SANDEEP |01-OCT-2019| -- END
        ModalPopupExtender1.Show()
        Exit Sub

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub popup_response_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_response.buttonYes_Click
        Try
            Dim objDisciplineTran As New clsDiscipline_file_tran
            'Dim strmessage As String = String.Empty
            'strmessage = objDisciplineTran.AllowVoidingResponse(mintDisiciplineFileTranUnkId)
            'If strmessage.Trim.Length > 0 Then
            '    DisplayMessage.DisplayMessage(strmessage, Me)
            '    Exit Sub
            'End If
            Call Void_Response()
            objDisciplineTran._ChargesTable = mdtCharge
            With objDisciplineTran
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            If objDisciplineTran.InsertUpdateDelete_DisciplineCharges(CInt(Session("userid")), ConfigParameter._Object._CurrentDateAndTime, mdtAttchment, "frmChargeCountResponse") Then
                'S.SANDEEP |01-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'DisplayMessage.DisplayMessage("Given response voided successfully.", Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 503, "Response voided Successfully."), Me)
                'S.SANDEEP |01-OCT-2019| -- END
                mdtAttchment = Nothing : mintDisiciplineFileTranUnkId = 0
                Dim objDisciplineFileTran As New clsDiscipline_file_tran
                objDisciplineFileTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
                mdtCharge = objDisciplineFileTran._ChargesTable
                objDisciplineFileTran = Nothing
                Call FillChargeList()
            End If
            objDisciplineTran = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_response_buttonYes_Click : - " & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
    '    Try
    '        If mintDeleteIndex > 0 Then
    '            mdtAttchment.Rows(mintDeleteIndex)("AUD") = "D"
    '            mdtAttchment.AcceptChanges()
    '            mintDeleteIndex = 0
    '            Call FillAttachment()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("popup_YesNo_buttonYes_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
    '    Try
    '        If mintDeleteIndex > 0 Then
    '            mintDeleteIndex = 0
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("popup_YesNo_buttonNo_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub
    'Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        'If IsDataValid() = False Then Exit Sub
    '        If Session("Imagepath") Is Nothing Then Exit Sub
    '        If Session("Imagepath").ToString.Trim <> "" Then
    '            Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
    '            mdtAttchment = CType(Session("mdtAttchment"), DataTable)
    '            AddDocumentAttachment(f, f.FullName)
    '            Call FillAttachment()
    '        End If
    '        Session.Remove("Imagepath")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnSaveAttachment_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub
#End Region

#Region " DataGrid Event(s) "
    'Protected Sub dgvAttchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvAttchment.ItemCommand
    '    Try
    '        If e.Item.ItemIndex >= 0 Then
    '            Dim xrow() As DataRow
    '            If CInt(e.Item.Cells(5).Text) > 0 Then
    '                xrow = mdtAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(5).Text) & "")
    '            Else
    '                xrow = mdtAttchment.Select("GUID = '" & e.Item.Cells(4).Text.Trim & "'")
    '            End If

    '            If e.CommandName = "Delete" Then
    '                If xrow.Length > 0 Then
    '                    popup_YesNo.Message = Language.getMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
    '                    mintDeleteIndex = mdtAttchment.Rows.IndexOf(xrow(0))
    '                    popup_YesNo.Show()
    '                End If
    '            ElseIf e.CommandName = "imgdownload" Then
    '                Dim xPath As String = ""

    '                If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
    '                    xPath = xrow(0).Item("filepath").ToString
    '                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
    '                    If Strings.Left(xPath, 1) <> "/" Then
    '                        xPath = "~/" & xPath
    '                    Else
    '                        xPath = "~" & xPath
    '                    End If
    '                    xPath = Server.MapPath(xPath)
    '                ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
    '                    xPath = xrow(0).Item("localpath").ToString
    '                End If
    '                If xPath.Trim <> "" Then
    '                    Dim fileInfo As New IO.FileInfo(xPath)
    '                    If fileInfo.Exists = False Then
    '                        DisplayMessage.DisplayError("File does not Exit...", Me)
    '                        Exit Sub
    '                    End If
    '                    fileInfo = Nothing
    '                    Dim strFile As String = xPath
    '                    Response.ContentType = "image/jpg/pdf"
    '                    Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(1).Text & """")
    '                    Response.Clear()
    '                    Response.TransmitFile(strFile)
    '                    HttpContext.Current.ApplicationInstance.CompleteRequest()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvAttchment_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub dgvAttchment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvAttchment.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
    '            DirectCast(Master.FindControl("script1"), ScriptManager).RegisterPostBackControl(e.Item.Cells(3).FindControl("DownloadLink"))
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvAttchment_ItemDataBound:- " & ex.Message, Me)
    '    End Try
    'End Sub
#End Region

#Region " Link Event "

    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim gvItem As DataGridItem = CType(lnk.NamingContainer, DataGridItem)
            mintDisiciplineFileTranUnkId = CInt(gvItem.Cells(7).Text)
            Call Charges_GetData()
            mblnIsVisibleChargesPopup = True
            popup_DisciplineCharge.Show()
            'Session("disciplinefileunkid") = gvItem.Cells(5).Text
            'Session("disciplinefiletranunkid") = gvItem.Cells(7).Text
            'If Request.QueryString.Count > 0 Then
            '    Session("IsdirectLink") = True
            'Else
            '    Session("IsdirectLink") = False
            'End If
            'Response.Redirect(Session("rootpath").ToString & "Discipline/Discipline_Charge/wPg_ChargesResponse.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("link_Click" & ex.Message, Me)
            DisplayMessage.DisplayError("link_Click" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'S.SANDEEP |01-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Protected Sub linkDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim gvItem As DataGridItem = CType(lnk.NamingContainer, DataGridItem)
            Dim objScanAttachment As New clsScan_Attach_Documents

            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'objScanAttachment.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboEmployee.SelectedValue))
            objScanAttachment.GetList(Session("Document_Path").ToString(), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "", CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END
            Dim xRows As DataRow() = objScanAttachment._Datatable.Select("scanattachrefid = '" & enScanAttactRefId.DISCIPLINES & "' AND transactionunkid = '" & CInt(gvItem.Cells(7).Text) & "' AND form_name = 'frmChargeCountResponse'")
            If xRows IsNot Nothing AndAlso xRows.Length > 0 Then
                Dim xTable As DataTable = xRows.CopyToDataTable()
                Dim strMsg As String = String.Empty
                strMsg = DownloadAllDocument("file" & cboReferenceNo.SelectedItem.Text.Replace(" ", "") + ".zip", xTable, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
            Else
                DisplayMessage.DisplayMessage(Language.getMessage("frmChargeCountResponse", 14, "Sorry, No file attached."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("linkDownload_Click : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |01-OCT-2019| -- END

    Protected Sub delete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = CType(sender, LinkButton)
            Dim gvItem As DataGridItem = CType(lnk.NamingContainer, DataGridItem)
            Dim objDisciplineTran As New clsDiscipline_file_tran
            Dim strmessage As String = String.Empty
            strmessage = objDisciplineTran.AllowVoidingResponse(CInt(gvItem.Cells(7).Text))
            If strmessage.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(strmessage, Me)
                Exit Sub
            End If
            objDisciplineTran = Nothing
            If gvItem.Cells(9).Text.Trim.Length > 0 AndAlso gvItem.Cells(9).Text.Trim <> "&nbsp;" Then
                mintDisiciplineFileTranUnkId = CInt(gvItem.Cells(7).Text)
                popup_response.Message = Language.getMessage(mstrModuleName, 40, "Are you sure you want to delete the response provided and attachment?")
                popup_response.Show()
            Else
                DisplayMessage.DisplayMessage("Sorry, no response given for the selected count to delete.", Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("delete_Click : " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Control Event(s) "

    Protected Sub dgvResponse_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvResponse.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtAttchment.Select("GUID = '" & e.Item.Cells(2).Text.Trim & "'")
                'End If

                'If e.CommandName = "Delete" Then
                '    If xrow.Length > 0 Then
                '        popup_YesNo.Message = Language.getMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                '        mintDeleteIndex = mdtAttchment.Rows.IndexOf(xrow(0))
                '        popup_YesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvResponse, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvResponse, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtAttchment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvResponse, "objcolhGUID", False, True)).Text.Trim & "'")
                End If

                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Language.getMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                        mintDeleteIndex = mdtAttchment.Rows.IndexOf(xrow(0))
                        popup_YesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("localpath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
                'S.SANDEEP |16-MAY-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvResponse_ItemCommand:- " & ex.Message, Me)
        End Try
    End Sub

    'Protected Sub dgvChargeCountList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvChargeCountList.ItemCommand
    '    Try
    '        If e.Item.ItemIndex >= 0 Then
    '            Dim xrow() As DataRow
    '            xrow = mdtCharge.Select("disciplinefiletranunkid = " & CInt(e.Item.Cells(7).Text) & "")
    '            If e.CommandName = "remove_response" Then
    '                If xrow.Length > 0 Then
    '                    popup_response.Message = Language.getMessage(mstrModuleName, 40, "Are you sure you want to delete the response provided and attachment?")
    '                    mintDisiciplineFileTranUnkId = CInt(e.Item.Cells(7).Text)
    '                    popup_response.Show()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("dgvChargeCountList_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub dgvChargeCountList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvChargeCountList.ItemDataBound
        Try
            If e.Item.ItemType <> ListItemType.Header Then
                If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                    'S.SANDEEP |01-OCT-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                    'If e.Item.Cells(9).Text.Trim.Length > 0 AndAlso e.Item.Cells(9).Text <> "&nbsp;" Then
                    If e.Item.Cells(8).Text.Trim.Length > 0 AndAlso e.Item.Cells(8).Text <> "&nbsp;" Then
                        'S.SANDEEP |01-OCT-2019| -- END
                        e.Item.ForeColor = Drawing.Color.Blue
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvChargeCountList_ItemDataBound :- " & ex.Message, Me)
        End Try
    End Sub


#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "



    Public Sub SetMessages()
        Try
            Language.setMessage("frmChargeCountResponse", 14, "Sorry, No file attached.")
            Language.setMessage(mstrModuleName, 1, "Sorry, Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Reference No. is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Response date is mandatory information. Please provide Response Date.")
            Language.setMessage(mstrModuleName, 6, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
            Language.setMessage(mstrModuleName, 40, "Are you sure you want to delete the response provided and attachment?")
            Language.setMessage(mstrModuleName1, 500, "Response added successfully")
            Language.setMessage(mstrModuleName1, 501, "Response Saved Successfully.")
            Language.setMessage(mstrModuleName1, 502, "Response submitted Successfully.")
            Language.setMessage(mstrModuleName1, 503, "Response voided Successfully.")
            Language.setMessage(mstrModuleName1, 1, "Sorry, Response type is mandatory information. Please select Response type to continue.")

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages:- " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
