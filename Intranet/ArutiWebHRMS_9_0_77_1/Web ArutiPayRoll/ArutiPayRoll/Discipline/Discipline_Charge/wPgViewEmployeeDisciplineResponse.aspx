﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgViewEmployeeDisciplineResponse.aspx.vb" Inherits="Discipline_Discipline_Charge_wPgViewEmployeeDisciplineResponse" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Discipline Charge Response"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Charges Details"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 22%">
                                                            <asp:Label ID="lblPersonalInvolved" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 22%">
                                                            <asp:Label ID="lblJobTitle" runat="server" Text="Job Title"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%">
                                                            <asp:TextBox ID="txtJobTitle" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 22%">
                                                            <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                                        </td>
                                                        <td style="width: 78%">
                                                            <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%; vertical-align: top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 24%">
                                                            <asp:Label ID="lblRefNo" runat="server" Style="margin-left: 5px" Text="Reference No."></asp:Label>
                                                        </td>
                                                        <td style="width: 76%" colspan="3">
                                                            <asp:DropDownList ID="cboReferenceNo" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 24%">
                                                            <asp:Label ID="lblDate" runat="server" Style="margin-left: 5px" Text="Charge Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtChargeDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 26%">
                                                            <asp:Label ID="lblInterdictionDate" runat="server" Style="margin-left: 10px" Text="Interdiction Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtInterdictionDate" runat="server" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 24%; vertical-align: top">
                                                            <asp:Label ID="lblGeneralChargeDescr" runat="server" Style="margin-left: 5px; margin-top: "
                                                                Text="Charge Description"></asp:Label>
                                                        </td>
                                                        <td style="width: 76%" colspan="3">
                                                            <asp:TextBox ID="txtChargeDescription" runat="server" TextMode="MultiLine" Rows="3"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="2">
                                                <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblChargeCount" runat="server" Text="Charge Count(s)"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div3" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server" Style="max-height: 350px">
                                                                        <asp:DataGrid ID="dgvChargeCountList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                                            <ItemStyle CssClass="griviewitem" />
                                                                            <Columns>
                                                                                <asp:BoundColumn DataField="charge_count" FooterText="dgcolhCount" HeaderText="Count"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--0--%>
                                                                                <asp:BoundColumn DataField="incident_description" FooterText="dgcolhIncident" HeaderText="Incident"
                                                                                    ReadOnly="true" />
                                                                                <%--1--%>
                                                                                <asp:BoundColumn DataField="charge_category" FooterText="dgcolhOffCategory" HeaderText="Offence Category"
                                                                                    ReadOnly="true" />
                                                                                <%--2--%>
                                                                                <asp:BoundColumn DataField="charge_descr" FooterText="dgcolhOffence" HeaderText="Offence Discription"
                                                                                    ReadOnly="true" />
                                                                                <%--3--%>
                                                                                <asp:BoundColumn DataField="charge_severity" FooterText="dgcolhSeverity" HeaderText="Severity"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--4--%>
                                                                                <asp:BoundColumn DataField="response_date" FooterText="dgcolhResponseDate" HeaderText="Response Date"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--5--%>
                                                                                <asp:BoundColumn DataField="response_type" FooterText="dgcolhResponseType" HeaderText="Response Type"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--6--%>
                                                                                <asp:BoundColumn DataField="response_remark" FooterText="dgcolhResponseText" HeaderText="Response Remark"
                                                                                    ItemStyle-HorizontalAlign="Center" ReadOnly="true">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                </asp:BoundColumn>
                                                                                <%--7--%>
                                                                                <asp:BoundColumn DataField="disciplinefileunkid" FooterText="objcolhfileunkid" ReadOnly="true"
                                                                                    Visible="false" />
                                                                                <%--8--%>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderText="View Attachment" ItemStyle-Width="90px" FooterText="dgvalnkCol">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewattachment"
                                                                                            Font-Underline="false"><i class="fa fa-eye" aria-hidden="true" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <%--9--%>
                                                                                <asp:BoundColumn DataField="disciplinefiletranunkid" Visible="false" />                                                                                
                                                                                <%--10--%>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                            CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ajaxToolkit:ModalPopupExtender ID="popup_DisciplineCharge" runat="server" BackgroundCssClass="ModalPopupBG2"
                            CancelControlID="btnSOk" PopupControlID="pnl_ResponseCharge" TargetControlID="hdf_DiscplineCharge">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnl_ResponseCharge" runat="server" CssClass="newpopup" Style="display: none;
                            width: 600px; z-index: 100002!important;" DefaultButton="btnSOk">
                            <div class="panel-primary" style="margin-bottom: 0px;">
                                <div class="panel-heading">
                                    <asp:Label ID="lblDocumentHeader" runat="server" Text="View Attached Document(s)"></asp:Label>
                                </div>
                                <div id="Div4" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvResponse" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                    <asp:DataGrid ID="dgvResponse" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                            <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Font-Size="22px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete">
                                                                        <i class="fa fa-download"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                Visible="false" />
                                                            <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:HiddenField ID="hdf_DiscplineCharge" runat="server" />
                                        <asp:Button ID="btnSOk" runat="server" Text="Ok" Width="70px" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
