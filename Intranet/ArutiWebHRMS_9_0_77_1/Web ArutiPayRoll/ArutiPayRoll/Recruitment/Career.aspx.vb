﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Data

#End Region

Partial Class Recruitment_Career
    Inherits System.Web.UI.Page

#Region " Private Variable(s) "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmSearchJob"
#End Region

#Region " Method Functions "

    Private Sub FillVacancyList(ByVal strDatabaseName As String _
                                , ByVal strExtInt As String _
                                )
        Try

            'odsVacancy.SelectParameters.Item("strCompCode").DefaultValue = strCompCode
            'odsVacancy.SelectParameters.Item("intComUnkID").DefaultValue = intCompanyunkid
            odsVacancy.SelectParameters.Item("strDatabaseName").DefaultValue = strDatabaseName
            odsVacancy.SelectParameters.Item("intMasterTypeId").DefaultValue = clsCommon_Master.enCommonMaster.VACANCY_MASTER
            odsVacancy.SelectParameters.Item("intEType").DefaultValue = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE
            If strExtInt = "9" Then
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = False
            Else
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = True
            End If
            odsVacancy.SelectParameters.Item("blnAllVacancy").DefaultValue = False
            odsVacancy.SelectParameters.Item("intDateZoneDifference").DefaultValue = 0
            odsVacancy.SelectParameters.Item("strVacancyUnkIdLIs").DefaultValue = ""
            odsVacancy.SelectParameters.Item("intApplicantUnkId").DefaultValue = 0
            odsVacancy.SelectParameters.Item("blnOnlyCurrent").DefaultValue = False
            odsVacancy.SelectParameters.Item("blnOnlyExportToWeb").DefaultValue = False 'Sohail (14 Oct 2020)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Session("LangId") = 1

                Dim strComp As String = ""
                Dim intCompID As Integer = 0
                Dim ExtInt As String = "7"

                If Request.QueryString("code") Is Nothing OrElse Request.QueryString("code") = "" Then
                    Response.Redirect("~/Index.aspx", False)
                    Exit Try
                Else

                    strComp = Request.QueryString("code").ToString

                    If Request.QueryString("ext") IsNot Nothing Then
                        ExtInt = Request.QueryString("ext").ToString
                    End If

                    Dim objMaster As New clsMasterData
                    Dim objBase As New Basepage
                    If Request.QueryString("code") IsNot Nothing Then
                        Dim objCompany As New clsCompany_Master
                        Dim intCompanyID As Integer = objCompany.GetIdByCode(Request.QueryString("code").ToString)

                        If intCompanyID > 0 Then
                            Dim strError As String = ""
                            objBase.GetCompanyYearInfo(strError, intCompanyID)

                            'Hemant (08 Jul 2021) -- Start
                            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                            If Request.UrlReferrer Is Nothing Then
                                gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("Database_Name").ToString)
                                gobjLocalization._LangId = HttpContext.Current.Session("LangId")
                            End If
                            'Hemant (08 Jul 2021) -- End
                        Else
                            Response.Redirect("~/Index.aspx", False)
                            Exit Try
                        End If
                    Else
                        Response.Redirect("~/Index.aspx", False)
                        Exit Try
                    End If

                End If

                Call FillVacancyList(Session("Database_Name").ToString, ExtInt)

                'Hemant (08 Jul 2021) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                Call SetControlCaptions()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                'Hemant (08 Jul 2021) -- End
            Else

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SearchJob_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " DataList Events "

    Private Sub dlVaanciesList_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlVaanciesList.ItemCommand
        Try
            If e.CommandName.ToUpper = "APPLY" Then
                If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Dim Idx As Integer = e.Item.ItemIndex

                Dim ExtInt As String = "7"
                If Request.QueryString("ext") IsNot Nothing Then
                    ExtInt = Request.QueryString("ext").ToString
                End If
                'Response.Redirect(Request.Url.GetLeftPart(UriPartial.Authority) & "" & Request.ApplicationPath & "/Login.aspx?ext=" & ExtInt & "&cc=" & CType(dlVaanciesList.Items(Idx).FindControl("objlblAuthCode"), Label).Text & "", False)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub dlVaanciesList_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlVaanciesList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lbl As Label = CType(e.Item.FindControl("objlblIsApplyed"), Label)
                CType(e.Item.FindControl("objlblAuthCode"), Label).Visible = False

                If CBool(lbl.Text) = True Then
                    CType(e.Item.FindControl("lblApplyText"), Label).Text = "Applied"
                    CType(e.Item.FindControl("btnApply"), LinkButton).Enabled = False
                    CType(e.Item.FindControl("divApplied"), Control).Visible = True
                Else

                End If
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                If drv.Item("experience").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divExp"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isexpbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isexpitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                End If

                If drv.Item("noposition").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divNoPosition"), Control).Visible = False
                End If

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim arrJobLocation As New ArrayList
                If Session("CompCode").ToString.ToUpper = "NMB" Then
                    If drv.Item("classgroupname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classgroupname").ToString)
                    End If
                    If drv.Item("classname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classname").ToString)
                    End If
                End If
                CType(e.Item.FindControl("objlblJobLocation"), Label).Text = String.Join(", ", TryCast(arrJobLocation.ToArray(GetType(String)), String()))
                If arrJobLocation.Count <= 0 Then
                    CType(e.Item.FindControl("divJobLocation"), Control).Visible = False
                End If
                'Hemant (01 Nov 2021) -- End

                If drv.Item("skill").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divSkill"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isskillbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isskillitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Hemant (08 Jul 2021) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI
                    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'Hemant (08 Jul 2021) -- End
                End If

                If drv.Item("Lang").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divLang"), Control).Visible = False
                End If

                If CDec(drv.Item("pay_from").ToString) <= 0 AndAlso CDec(drv.Item("pay_to").ToString) <= 0 Then
                    CType(e.Item.FindControl("divScale"), Control).Visible = False
                Else
                    Dim str As String = Format(CDec(drv.Item("pay_from").ToString), "##,##,##,##,##0.00") & " - " & Format(CDec(drv.Item("pay_to").ToString), "##,##,##,##,##0.00")
                    CType(e.Item.FindControl("objlblScale"), Label).Text = str
                End If

                If drv.Item("openingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divOpenningDate"), Control).Visible = False
                End If
                If drv.Item("closingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divClosuingDate"), Control).Visible = False
                End If

                If drv.Item("remark").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divRemark"), Control).Visible = False
                Else
                    Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblJobDiscription"), Label).Text = strResult
                End If
                If drv.Item("duties").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divResponsblity"), Control).Visible = False
                Else
                    Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblResponsblity"), Label).Text = strResult
                End If
                If drv.Item("Qualification").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divQualification"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isqualibold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isqualiitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub


    Private Sub odsVacancy_Selected(ByVal sender As Object, ByVal e As ObjectDataSourceStatusEventArgs) Handles odsVacancy.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is DataTable Then
                intC = CType(e.ReturnValue, DataTable).Rows.Count
            End If

            objlblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub odsVacancy_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsVacancy.Selecting
        Try
            If e.InputParameters("strDatabaseName") Is Nothing Then
                e.Cancel = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region

    'Hemant (08 Jul 2021) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            
            For Each dli As DataListItem In dlVaanciesList.Items
                Dim lblJobDiscription As Label = CType(dli.FindControl("lblJobDiscription"), Label)
                Language._Object.setCaption(lblJobDiscription.ID, lblJobDiscription.Text)
                Dim lblResponsblity As Label = CType(dli.FindControl("lblResponsblity"), Label)
                Language._Object.setCaption(lblResponsblity.ID, lblResponsblity.Text)
                Dim lblSkill As Label = CType(dli.FindControl("lblSkill"), Label)
                Language._Object.setCaption(lblSkill.ID, lblSkill.Text)
                Dim lblQualification As Label = CType(dli.FindControl("lblQualification"), Label)
                Language._Object.setCaption(lblQualification.ID, lblQualification.Text)
                Dim lblExp As Label = CType(dli.FindControl("lblExp"), Label)
                Language._Object.setCaption(lblExp.ID, lblExp.Text)
                Dim lblNoPosition As Label = CType(dli.FindControl("lblNoPosition"), Label)
                Language._Object.setCaption(lblNoPosition.ID, lblNoPosition.Text)
                Dim lblLang As Label = CType(dli.FindControl("lblLang"), Label)
                Language._Object.setCaption(lblLang.ID, lblLang.Text)
                Dim lblScale As Label = CType(dli.FindControl("lblScale"), Label)
                Language._Object.setCaption(lblScale.ID, lblScale.Text)
                Dim lblOpeningDate As Label = CType(dli.FindControl("lblOpeningDate"), Label)
                Language._Object.setCaption(lblOpeningDate.ID, lblOpeningDate.Text)
                Dim lblClosingDate As Label = CType(dli.FindControl("lblClosingDate"), Label)
                Language._Object.setCaption(lblClosingDate.ID, lblClosingDate.Text)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim lblJobLocation As Label = CType(dli.FindControl("lblJobLocation"), Label)
                Language._Object.setCaption(lblJobLocation.ID, lblJobLocation.Text)
                'Hemant (01 Nov 2021) -- End
            Next
            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            For Each dli As DataListItem In dlVaanciesList.Items
                Dim lblJobDiscription As Label = CType(dli.FindControl("lblJobDiscription"), Label)
                lblJobDiscription.Text = Language._Object.getCaption(lblJobDiscription.ID, lblJobDiscription.Text)
                Dim lblResponsblity As Label = CType(dli.FindControl("lblResponsblity"), Label)
                lblResponsblity.Text = Language._Object.getCaption(lblResponsblity.ID, lblResponsblity.Text)
                Dim lblSkill As Label = CType(dli.FindControl("lblSkill"), Label)
                lblSkill.Text = Language._Object.getCaption(lblSkill.ID, lblSkill.Text)
                Dim lblQualification As Label = CType(dli.FindControl("lblQualification"), Label)
                lblQualification.Text = Language._Object.getCaption(lblQualification.ID, lblQualification.Text)
                Dim lblExp As Label = CType(dli.FindControl("lblExp"), Label)
                lblExp.Text = Language._Object.getCaption(lblExp.ID, lblExp.Text)
                Dim lblNoPosition As Label = CType(dli.FindControl("lblNoPosition"), Label)
                lblNoPosition.Text = Language._Object.getCaption(lblNoPosition.ID, lblNoPosition.Text)
                Dim lblLang As Label = CType(dli.FindControl("lblLang"), Label)
                lblLang.Text = Language._Object.getCaption(lblLang.ID, lblLang.Text)
                Dim lblScale As Label = CType(dli.FindControl("lblScale"), Label)
                lblScale.Text = Language._Object.getCaption(lblScale.ID, lblScale.Text)
                Dim lblOpeningDate As Label = CType(dli.FindControl("lblOpeningDate"), Label)
                lblOpeningDate.Text = Language._Object.getCaption(lblOpeningDate.ID, lblOpeningDate.Text)
                Dim lblClosingDate As Label = CType(dli.FindControl("lblClosingDate"), Label)
                lblClosingDate.Text = Language._Object.getCaption(lblClosingDate.ID, lblClosingDate.Text)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim lblJobLocation As Label = CType(dli.FindControl("lblJobLocation"), Label)
                lblJobLocation.Text = Language._Object.getCaption(lblJobLocation.ID, lblJobLocation.Text)
                'Hemant (01 Nov 2021) -- End
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Hemant (08 Jul 2021) -- End
    
End Class
