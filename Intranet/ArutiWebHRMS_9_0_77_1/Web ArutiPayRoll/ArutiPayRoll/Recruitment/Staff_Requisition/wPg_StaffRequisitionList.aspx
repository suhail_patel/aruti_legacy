﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_StaffRequisitionList.aspx.vb"
    Inherits="wPg_StaffRequisitionList" MasterPageFile="~/home.master" Title="Staff Requisition List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

  function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }
       
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblAllocation" runat="server" Text="Requisition By"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboAllocation" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboName" AutoPostBack="false" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" AutoPostBack="false" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 20%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 13%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 20%">
                                                <asp:CheckBox ID="chkMyApprovals" runat="server" Text="My Approvals" />
                                            </td>
                                            <td style="width: 13%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 20%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btnDefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                                <div id="Grid-view" style="height: 235px; overflow: auto">
                                    <asp:GridView ID="dgStaffReqList" runat="server" AutoGenerateColumns="False" CellPadding="3"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%" DataKeyNames="staffrequisitiontranunkid">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="btnView" runat="server" CausesValidation="False" CommandArgument="<%# Container.DataItemIndex %>"
                                                            CssClass="gridedit" CommandName="Change" ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="btnGvRemove" runat="server" CausesValidation="false" CssClass="griddelete"
                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                FooterText="btnApprovedForm">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="lnkViewStaffRequisitionFormReport" CssClass="gridicon" runat="server"
                                                            ToolTip="View Staff Requisition Form Report" CommandArgument="<%# Container.DataItemIndex %>"
                                                            CommandName="ViewStaffRequisitionFormReport"><i class="fa fa-list-alt"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="staffrequisitionby" HeaderText="Staff Req. By" FooterText="colhAllocation" />
                                            <asp:BoundField DataField="formno" HeaderText="Form No" FooterText="colhFormNo" />
                                            <asp:BoundField DataField="name" HeaderText="Allocation" FooterText="colhName" />
                                            <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="colhJob" />
                                            <asp:BoundField DataField="jobdescrription" HeaderText="Job Description" FooterText="colhJobDesc" />
                                            <asp:BoundField DataField="workstartdate" HeaderText="Work Start Date" FooterText="colhWorkStart" />
                                            <asp:BoundField DataField="form_status" HeaderText="Status" FooterText="colhStatus" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc4:Confirmation ID="popupYesNo" runat="server" Title="Confirmation" Message="Are you sure you want to delete this Staff Requisition?" />
                    <uc5:DeleteReason ID="popupDelReason" runat="server" Title="Void Reason" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
