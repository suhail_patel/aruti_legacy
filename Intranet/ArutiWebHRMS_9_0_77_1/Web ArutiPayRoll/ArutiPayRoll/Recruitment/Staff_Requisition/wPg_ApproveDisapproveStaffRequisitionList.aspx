﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_ApproveDisapproveStaffRequisitionList.aspx.vb"
    Inherits="wPg_ApproveDisapproveStaffRequisitionList" MasterPageFile="~/home.master"
    Title="Staff Requisition Approval List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

  function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }
       
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 13%;">
                                                        <asp:Label ID="lblAllocation" runat="server" Text="Requisition By"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:DropDownList ID="cboAllocation" AutoPostBack="true" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 13%">
                                                        <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:DropDownList ID="cboName" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 13%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" AutoPostBack="false" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 13%">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 20%">
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 13%">
                                                        &nbsp;
                                                    </td>
                                            <td style="width: 20%">
                                                        <asp:CheckBox ID="chkMyApprovals" runat="server" Text="My Approvals" Visible="True" />
                                                    </td>
                                                    <td style="width: 13%">
                                                        &nbsp;
                                                    </td>
                                            <td style="width: 20%">
                                                        &nbsp;
                                            </td>
                                        </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" Width="75px" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" Width="75px" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" Width="90px" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                                <div style="height: 235px; overflow: auto">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="lvStaffReqApprovalList" runat="server" AutoGenerateColumns="false"
                                                CellPadding="3" Width="99%" DataKeyNames="staffrequisitionapprovaltranunkid"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkChangeStatus" runat="server" Text="Approve /Reject" Font-Underline="false"
                                                                CommandName="Select" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkViewStaffRequisitionFormReport" CssClass="gridicon" runat="server"
                                                                ToolTip="View Staff Requisition Form Report" CommandArgument="<%# Container.DataItemIndex %>"
                                                                CommandName="ViewStaffRequisitionFormReport"><i class="fa fa-list-alt"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="staffrequisitionbyid" HeaderText="Staff Req. By" FooterText="colhStaffReqBy" />
                                                    <asp:BoundField DataField="allocationunkid" HeaderText="Name" FooterText="colhName" />
                                                    <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="colhJobTitle" />
                                                    <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="colhLevel" />
                                                    <asp:BoundField DataField="username" HeaderText="Approver" FooterText="colhApprover" />
                                                    <asp:BoundField DataField="priority" HeaderText="Priority" FooterText="colhPriority" />
                                                    <asp:BoundField DataField="approval_date" HeaderText="Approval Date" FooterText="colhApprovalDate" />
                                                    <asp:BoundField DataField="statusunkid" HeaderText="Status" FooterText="colhStatus" />
                                                    <asp:BoundField DataField="remarks" HeaderText="Remarks" FooterText="colhRemarks" />
                                                    <asp:BoundField DataField="form_statusunkid" HeaderText="Form Status" FooterText="objcolhFormStatus"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="staffrequisitiontranunkid" HeaderText="objcolhStaffReqTranUnkId"
                                                        FooterText="objcolhStaffReqTranUnkId" Visible="false" />
                                                    <asp:BoundField DataField="IsGroup" HeaderText="IsGroup" Visible="false" />
                                                    <asp:BoundField DataField="userunkid" HeaderText="userunkid" Visible="false" />
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
