﻿<%@ Page Title="Final Applicant" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_FinalApplicant.aspx.vb" Inherits="wPg_FinalApplicant" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Final Applicant"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="LblVacancyType" runat="server" Text="Vacancy Type"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboVacancyType" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblRefNo" runat="server" Text="Reference No" Width="90px"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboRefNo" AutoPostBack="true" runat="server" />
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEnddate" runat="server" Text="Vacancy End date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtClosingDate" runat="server" ReadOnly="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblVacancy" runat="server" Text="Vacancy"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboVacancy" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblStartdate" runat="server" Text="Vacancy Start date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtOpeningDate" runat="server" ReadOnly="true" />
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="LblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btnDefault" Visible="false"
                                            ValidationGroup="FinalApplicant" />
                                        <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btnDefault"
                                            Visible="false" ValidationGroup="FinalApplicant" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" ValidationGroup="FinalApplicant" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" ValidationGroup="FinalApplicant" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" ValidationGroup="FinalApplicant"
                                            CausesValidation="false" />
                                    </div>
                                </div>
                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%" Height="235px">
                                    <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-Width="50px">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Enabled="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" CommandArgument="<%# Container.DataItemIndex %>"
                                                        Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="refno" HeaderText="Applicant" FooterText="colhFinalApplicantName" />
                                            <asp:BoundField DataField="Gender" HeaderText="Gender" FooterText="colhFinalGender" />
                                            <asp:BoundField DataField="BDate" HeaderText="BirthDate" FooterText="colhFinalBirthdate" />
                                            <asp:BoundField DataField="Phone" HeaderText="Phone" FooterText="colhFinalPhone" />
                                            <asp:BoundField DataField="Email" HeaderText="Email" FooterText="colhFinalEmail" />
                                            <asp:BoundField DataField="Status" HeaderText="Status" FooterText="colhdgStatus" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <uc8:Confirmation ID="popupConfirm" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
