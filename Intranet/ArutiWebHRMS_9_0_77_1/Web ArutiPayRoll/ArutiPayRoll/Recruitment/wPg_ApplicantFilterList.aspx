﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_ApplicantFilterList.aspx.vb"
    Inherits="wPg_ApplicantFilterList" MasterPageFile="~/home.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

    function closePopup() {
        $('#imgclose').click(function(evt) 
        {
            $find('<%= popupApproveDisapprove.ClientID %>').hide();        
        });
    }

function ChangeApplicantFilterImage(imgID, divID)
 {
        var pathname = document.location.href;
        var arr = pathname.split('/');        

        var imgURL = document.getElementById(imgID).src.split('/');
        var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';        
        
        if (imgURL[imgURL.length - 1] == 'plus.png')
         {
            document.getElementById(imgID).src = URL + "images/minus.png";
            document.getElementById(divID).style.display = 'block';             
        }

        if (imgURL[imgURL.length - 1] == 'minus.png') 
        {
            document.getElementById(imgID).src = URL + "images/plus.png";
            document.getElementById(divID).style.display = 'none';
        }


//        var cnt = 1;
//        while (cnt <= 3) 
//        {
//            var imgIdRec = "imgAcc" + cnt;

//            if (document.getElementById(imgIdRec) != null && imgIdRec != imgID) 
//            {
//                document.getElementById(imgIdRec).src = URL + "images/plus.png";
//            }
//            cnt = cnt + 1;
//        }        
        
    }

    
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
	 if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
          
    }
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Applicant Filter List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="LblVacancyType" runat="server" Text="Vacancy Type"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboVacancyType" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblRefNo" runat="server" Text="Reference No" Width="90px"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboRefNo" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEnddate" runat="server" Text="Vac. End Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateControl ID="dtEndDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblVacancy" runat="server" Text="Vacancy"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboVacancy" AutoPostBack="true" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblStartdate" runat="server" Text="Vac. Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateControl ID="dtpStartdate" runat="server" />
                                            </td>
                                            <td style="width: 13%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        <asp:Button ID="btnOperation" runat="server" CssClass="btnDefault" Text="Operation" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="height: 235px;
                                    overflow: auto">
                                    <asp:GridView ID="dgvApplicant" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-Width="50px">
                                                <HeaderTemplate>
                                                        <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Enabled="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                        <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" CommandArgument="<%# Container.DataItemIndex %>"
                                                            Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="refno" HeaderText="Reference No" FooterText="dgcolhRefNo" />
                                            <asp:BoundField DataField="vacancytitle" HeaderText="Vacancy" FooterText="dgcolhVacancy" />
                                            <asp:BoundField DataField="Status" HeaderText="Status" FooterText="dgcolhStatus" />
                                            <asp:BoundField DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupApproveDisapprove" runat="server" TargetControlID="HiddenField1"
                        PopupControlID="pnlApproveDisapprove" BackgroundCssClass="ModalPopupBG" DropShadow="false"
                        CancelControlID="btnApproveDisapproveClose" Drag="true">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlApproveDisapprove" runat="server" CssClass="newpopup" Style="display: none;
                        width: 800px" Height="550px">
                        <div class="panel-primary">
                            <div class="panel-heading">
                                <asp:Label ID="frmApplicantFilter_Approval" runat="server" Text="Approve/Disapprove Applicant Filter"></asp:Label>
                            </div>
                            <div class="panel-body" style="height: 500px; overflow: auto">
                                <div id="Div10" class="panel-default">
                                    <div id="Div12" class="panel-body-default">
                                        <div id="Div1" class="panel-default">
                                            <div id="Div2" class="panel-heading-default" onclick="ChangeApplicantFilterImage('img1','dvApplicantFilter');">
                                                <div style="float: left;">
                                                    <img id="img1" src="../images/plus.png" alt="" />
                                                    <asp:Label ID="gbCriteria" runat="server" Text="Shortlisting Criteria"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div3" class="panel-body-default">
                                                <div id="dvApplicantFilter" style="height: 200px; overflow: auto; display: none;
                                                    margin-left: 2px; margin-right: 2px">
                                                    <asp:GridView ID="dgvShortListCriteria" runat="server" AutoGenerateColumns="False"
                                                        ShowFooter="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="4500px">
                                                        <Columns>
                                                            <asp:BoundField DataField="refno" HeaderText="Reference No" FooterText="colhRefNo" />
                                                            <asp:BoundField DataField="vacancytitle" HeaderText="Vacancy" FooterText="dgcolhSCVacancy" />
                                                            <asp:BoundField DataField="qlevel" HeaderText="Ql.Grp Level" FooterText="colhQlGrpLevel" />
                                                            <asp:BoundField DataField="qualificationgroup" HeaderText="Qul. Group" FooterText="colhQlGrp" />
                                                            <asp:BoundField DataField="qualification" HeaderText="Qualification" FooterText="colhdgQualification" />
                                                            <asp:BoundField DataField="resultname" HeaderText="Result Code" FooterText="colhResultCode" />
                                                            <asp:BoundField DataField="resultlevel" HeaderText="Result Level" FooterText="colhRLevel" />
                                                            <asp:BoundField DataField="ResultLvl_Condition" HeaderText="Result Level Condition"
                                                                FooterText="colhRCondition" />
                                                            <asp:BoundField DataField="gpacode" HeaderText="GPA" FooterText="colhGPA" />
                                                            <asp:BoundField DataField="gpacode_condition" HeaderText="GPA Condition" FooterText="colhGPACondition" />
                                                            <asp:BoundField DataField="age" HeaderText="Age" FooterText="colhAge" HeaderStyle-Width="50px"
                                                                ItemStyle-Width="50px" />
                                                            <asp:BoundField DataField="agecondition" HeaderText="Age Condition" FooterText="colhAgeCondition" />
                                                            <asp:BoundField DataField="award_year" HeaderText="Awarded Year" FooterText="colhAwardYear" />
                                                            <asp:BoundField DataField="year_condition" HeaderText="Year Condition" FooterText="colhYearCondition" />
                                                            <asp:BoundField DataField="gender_name" HeaderText="Gender" FooterText="colhGender" />
                                                            <asp:BoundField DataField="skill_category" HeaderText="SKill Category" FooterText="colhFilterSkillCategory" />
                                                            <asp:BoundField DataField="skill" HeaderText="Skill" FooterText="colhFilterSkill" />
                                                            <asp:BoundField DataField="other_qualificationgrp" HeaderText="Other Qul. Group"
                                                                FooterText="colhFilterOtherQuliGrp" />
                                                            <asp:BoundField DataField="other_qualification" HeaderText="Other Qualification"
                                                                FooterText="colhFilterOtherQualification" />
                                                            <asp:BoundField DataField="other_resultcode" HeaderText="Other Result Code" FooterText="colhFilterOtherResultCode" />
                                                            <asp:BoundField DataField="other_skillcategory" HeaderText="Other Skill Category"
                                                                FooterText="colhFilterOtherSkillCategory" />
                                                            <asp:BoundField DataField="other_skill" HeaderText="Other Skill" FooterText="colhFilterOtherSkill" />
                                                            <asp:BoundField DataField="nationality" HeaderText="Nationality" FooterText="colhNationality" />
                                                            <asp:BoundField DataField="experience_days" HeaderText="Experience (Days)" FooterText="colhExperience" />
                                                            <asp:BoundField DataField="experiencecondition" HeaderText="Experience Condition" FooterText="colhExperienceCond" />
                                                            <asp:BoundField DataField="logicalcondition" HeaderText="Logical Condition" FooterText="colhLogicalCondition" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Div4" class="panel-default">
                                            <div id="Div5" class="panel-heading-default" onclick="ChangeApplicantFilterImage('img2','dvData');">
                                                <div style="float: left;">
                                                    <img id="img2" src="../images/plus.png" alt="" />
                                                    <asp:Label ID="gbApplicantInfo" runat="server" Text="Applicant Info"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div6" class="panel-body-default">
                                                <div id="dvData" style="height: 285px; overflow: auto; display: none; margin-left: 2px;
                                                    margin-right: 2px">
                                                    <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="false"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="refno" HeaderText="Reference No" FooterText="dgcolhRefNo" />
                                                            <asp:BoundField DataField="vacancytitle" HeaderText="Vacancy" FooterText="dgcolhVacancy" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="Div7" class="panel-default">
                                            <div id="Div8" class="panel-heading-default" onclick="ChangeApplicantFilterImage('img3','dvRemark');">
                                                <div style="float: left;">
                                                    <img id="img3" src="../images/plus.png" alt="" />
                                                    <asp:Label ID="gbRemark" runat="server" Text="Remark"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="Div9" class="panel-body-default">
                                                <div id="dvRemark" style="overflow: auto; display: none; margin-left: 2px; margin-right: 2px">
                                                    <asp:TextBox ID="txtRemark" runat="server" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnApprove" runat="server" Text="Approve" Width="90px" CssClass="btnDefault" />
                                            <asp:Button ID="btnReject" runat="server" Text="Disapprove" Width="94px" CssClass="btnDefault" />
                                            <asp:Button ID="btnApproveDisapproveClose" runat="server" Text="Close" Width="90px"
                                                CssClass="btnDefault" />
                                            <asp:HiddenField ID="HiddenField1" runat="Server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc4:Confirmation ID="popConfirm" runat="server" />
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
