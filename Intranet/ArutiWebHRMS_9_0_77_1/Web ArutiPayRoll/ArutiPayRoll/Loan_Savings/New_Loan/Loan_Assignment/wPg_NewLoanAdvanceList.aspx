<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_NewLoanAdvanceList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Assignment_wPg_NewLoanAdvanceList" Title="Loan/Advance List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <link href="../../../App_Themes/PA_Style.css" rel="Stylesheet" type="text/css" />

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan/Advance List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%" colspan="4">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblPayPeriod" runat="server" Style="margin-left: 10px" Text="Assigned Period"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboAssignPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblVocNo" runat="server" Style="margin-left: 10px" Text="Voucher No."></asp:Label>
                                            </td>
                                            <td style="width: 17%" colspan="2">
                                                <asp:TextBox ID="txtVocNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 30%" colspan="4">
                                                <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblStatus" runat="server" Style="margin-left: 10px" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblAmount" runat="server" Style="margin-left: 10px" Text="Amount"></asp:Label>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:TextBox ID="txtAmount" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right"></asp:TextBox>
                                            </td>
                                            <td style="width: 5%">
                                                <asp:DropDownList ID="cboCondition" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLoanAdvance" runat="server" Text="Loan/Advance"></asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 1%">
                                            <td style="width: 7%">
                                                <asp:Label ID="lblCalcType" runat="server" Text="Calc. Type"></asp:Label>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:DropDownList ID="cboCalcType" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblOtherOpStatus" runat="server" Style="margin-left: 10px" Text="Other Opr. Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboOtherOpStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblCurrency" runat="server" Style="margin-left: 10px" Text="Currency"></asp:Label>
                                            </td>
                                            <td style="width: 17%" colspan="2">
                                                <asp:DropDownList ID="cboCurrency" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:Label ID="lblCurrOpenPeriod" runat="server" Style="font-size: 12px; color: Red;
                                            margin-left: 10px" Text=""></asp:Label>
                                        </div>
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                        max-height: 400px; width: auto" onscroll="$(scroll.Y).val(this.scrollTop);">
                                        <asp:DataGrid ID="dgvLoanAdvanceList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="225%">
                                            <Columns>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="btnEdit">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="btnDelete">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="btnPayment">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgPayment" Style="text-align: center;" runat="server" ToolTip="Payment"
                                                                CommandName="Payment" CssClass="gridpayment"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="btnRecieved">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgRecieved" Style="text-align: center;" runat="server" ToolTip="Recieved"
                                                                CommandName="Recieved" CssClass="gridrecieved"></asp:LinkButton></span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="btnApprovedForm">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgApprovedForm" Style="text-align: center;" runat="server" ToolTip="View Approved Form"
                                                                CommandName="ViewForm" CssClass="gridapprovedform"></asp:LinkButton></span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                    FooterText="btnChangeStatus">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgChangeStatus" Style="text-align: center;" runat="server" ToolTip="Change Status"
                                                                CommandName="ChangeStatus" CssClass="gridchangestatus"></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="ImgLoanOperation" CssClass="gridicon" runat="server" ToolTip="Other Loan Operation"
                                                                CommandName="LoanOperation"><i class="fa fa-list-alt"></i></asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="VocNo" HeaderText="Voc #" FooterText="dgcolhVocNo"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="PeriodName" HeaderText="Period" FooterText="dgcolhPeriod">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="DeductionPeriodName" HeaderText="Deduction Period" FooterText="dgcolhDeductionPeriod">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEmployee">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" FooterText="dgcolhLoanScheme">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Loan_Advance" HeaderText="Loan/Advance" FooterText="dgcolhLoanAdvance">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="LoanCalcType" HeaderStyle-Width="200px" ItemStyle-Width="200px"
                                                    HeaderText="Loan Calculation Type" FooterText="dgcolhLoanCalcType"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="InterestCalcType" HeaderText="Interest Type" FooterText="dgcolhInterestCalcType">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Amount" HeaderText="Loan Amount" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="dgcolhAmount"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="cSign" HeaderText="Currency" FooterText="dgcolhCurrency">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="installments" HeaderText="No. Of INSTL" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="dgcolhInstallments"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="loanbalance" HeaderText="Curr. Principal Balance" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="dgcolhCurrPrincipalBal"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="paidlaon" HeaderText="Paid Loan" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="dgcolhPaidLoan"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="loan_status" HeaderText="Status" FooterText="dgcolhStatus">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="isloan" HeaderText="Is Loan" Visible="false" FooterText="objdgcolhIsLoan">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="loan_statusunkid" HeaderText="Statusunkid" Visible="false"
                                                    FooterText="objdgcolhstatusunkid"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" Visible="false"
                                                    FooterText="objdgcolhemployeeunkid"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="loanadvancetranunkid" HeaderText="objdgcolhLoanAdvanceunkid"
                                                    Visible="false" FooterText="objdgcolhLoanAdvanceunkid"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="isbrought_forward" HeaderText="objdgcolhhisbrought_forward"
                                                    Visible="false" FooterText="objdgcolhhisbrought_forward"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="effective_date" HeaderText="EffectiveDate" Visible="false"
                                                    FooterText="dgcolhEffectiveDate"></asp:BoundColumn>
                                                <asp:BoundColumn DataField="otherop_approval" HeaderText="Other Opr. Approval" FooterText="dgcolhOtherOpApproval">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="opstatus_id" Visible="false" FooterText="objdgcolhOpStatusId">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="processpendingloanunkid" Visible="false" FooterText="objdgcolhProcessPendingUnkid">
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="loanschemeunkid" Visible="false" FooterText="objdgcolhLoanSchemeUnkid">
                                                </asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <div style="float: left">
                                            <asp:Button ID="btnGlobalChangeStatus" runat="server" Text="Global Change Status"
                                                CssClass="btndefault" />
                                            <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btndefault" />
                                        </div>
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnl_ViewApproveForm" runat="server" CssClass="newpopup" Style="display: none;"
                        Width="40%">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="frmLoanApproveForm" runat="server" Text="Loan Approve Form"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div2" class="panel-default">
                                    <div id="Div4" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Loan Approve Form"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div5" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblEmployeeName" runat="server" Text="Employee Name"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:TextBox ID="txtEName" runat="server" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblAFLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboAFLoanScheme" runat="server" Width="200px" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblAFLoanApplicationNo" runat="server" Text="Loan Application No"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboAFLoanApplicationNo" runat="server" Width="200px" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblAFLoanVoucherNo" runat="server" Text="Loan Voucher No"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboAFLoanVoucherNo" runat="server" Width="200px" Enabled="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include inactive employee">
                                                    </asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%" colspan="2">
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%;">
                                                    <b>
                                                        <asp:Label ID="gbMonthlySal" runat="server" Text="Monthly Salary Display Setting"></asp:Label></b>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:RadioButtonList ID="radView" runat="server" RepeatDirection="Vertical">
                                                        <asp:ListItem Selected="True" Text="Display Monthly Basic Salary" Value="1"></asp:ListItem>
                                                        <asp:ListItem Selected="False" Text="Display Monthly Gross Amount" Value="2"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnViewReport" runat="server" Text="View Report" CssClass="btndefault" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupViewApproveForm" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btnCancel" PopupControlID="pnl_ViewApproveForm" TargetControlID="txtEName">
                    </cc1:ModalPopupExtender>
                    <uc1:DeleteReason ID="popupDeleteReason" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
