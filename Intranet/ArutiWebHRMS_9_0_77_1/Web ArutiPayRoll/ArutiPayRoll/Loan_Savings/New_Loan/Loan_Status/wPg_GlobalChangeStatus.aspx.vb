﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Diagnostics

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Status_wPg_GlobalChangeStatus
    Inherits Basepage

#Region " Private Variables "
    Public Shared ReadOnly mstrModuleName As String = "frmGlobalChangeStatus"
    Private DisplayMessage As New CommonCodes
    Private objLoanAdvance As clsLoan_Advance
    Private objStatusTran As clsLoan_Status_tran
    Private objPaymentTran As clsPayment_tran
    Private mstrBaseCurrSign As String = String.Empty
    Private mstrAdvanceFilter As String = String.Empty
    'Private mblnIsFromApplyButton As Boolean = False
    Private mblnIsWrittenOff As Boolean = False
    Private mintBaseCurrId As Integer = -1
    Private mintBaseCountryId As Integer = -1
    Private mintPaidCurrId As Integer = -1
    Private mdecBaseExRate As Decimal = 0
    Private mdecPaidExRate As Decimal = 0
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    Private mdtList As DataTable = Nothing
    Private mdtExcel As DataTable = Nothing
#End Region

#Region " Page's Events "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call FillCombo()

                If dgvLoanList.Items.Count <= 0 Then
                    dgvLoanList.DataSource = New List(Of String)
                    dgvLoanList.DataBind()
                End If

                If CInt(Session("PaymentVocNoType")) = 1 Then
                    txtVoucherNo.Enabled = False
                Else
                    txtVoucherNo.Enabled = True
                End If

            Else
                mstrBaseCurrSign = CStr(Me.ViewState("BaseCurrSign"))
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
                mintBaseCurrId = CInt(Me.ViewState("BaseCurrId"))
                mintBaseCountryId = CInt(Me.ViewState("BaseCountryId"))
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                mintPaidCurrId = CInt(Me.ViewState("PaidCurrId"))
                mdecBaseExRate = CDec(Me.ViewState("BaseExRate"))
                mdecPaidExRate = CDec(Me.ViewState("PaidExRate"))
                'mblnIsFromApplyButton = CBool(Me.ViewState("IsFromApplyButton"))
                mblnIsWrittenOff = CBool(Me.ViewState("IsWrittenOff"))
                mdtList = CType(Me.ViewState("DataTable"), DataTable)
                mdtExcel = CType(Me.ViewState("ExcelDataTable"), DataTable)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("BaseCurrSign") = mstrBaseCurrSign
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("BaseCurrId") = mintBaseCurrId
            Me.ViewState("BaseCountryId") = mintBaseCountryId
            Me.ViewState("PaidCurrId") = mintPaidCurrId
            Me.ViewState("BaseExRate") = mdecBaseExRate
            Me.ViewState("PaidExRate") = mdecPaidExRate
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            'Me.ViewState("IsFromApplyButton") = mblnIsFromApplyButton
            Me.ViewState("IsWrittenOff") = mblnIsWrittenOff
            Me.ViewState("DataTable") = mdtList
            Me.ViewState("ExcelDataTable") = mdtExcel

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim dtTable As New DataTable
            Dim objEmployee As New clsEmployee_Master
            Dim objLoanScheme As New clsLoan_Scheme
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim objExRate As New clsExchangeRate
            Dim objCompanyBank As New clsCompany_Bank_tran
            objLoanAdvance = New clsLoan_Advance

            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 CStr(Session("UserAccessModeSetting")), _
                                                 True, False, "Employee", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objMaster.GetLoan_Saving_Status("Status", False)
            dtTable = dsList.Tables("Status").Select("Id IN(" & enLoanStatus.IN_PROGRESS & "," & enLoanStatus.ON_HOLD & ")").CopyToDataTable
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = CStr(enLoanStatus.IN_PROGRESS)
            End With
            dtTable = Nothing

            dsList = objLoanAdvance.GetLoanCalculationTypeList("List", True)
            With cboCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoanAdvance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .DataValueField = "Id"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                              CDate(Session("fin_startdate")), "Period", True, enStatusType.OPEN)

            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN, )

            If intFirstPeriodId > 0 Then
                dtTable = New DataView(dsList.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = CStr(intFirstPeriodId)
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, "Period", True)

            With cboAssignedPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboDeductionPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period").Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objMaster.GetLoanAdvance("List")
            With cboMode
                .DataValueField = "ID"
                .DataTextField = "NAME"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With

            dsList = objMaster.GetPaymentMode("PayMode")
            With cboPaymentMode
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("PayMode")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objExRate.getComboList("ExRate", False)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                dtTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                Else
                    mintBaseCountryId = 0
                End If

                With cboCurrency
                    .DataValueField = "countryunkid"
                    .DataTextField = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    .DataBind()
                    If mintBaseCountryId > 0 Then
                        .SelectedValue = CStr(mintBaseCountryId)
                    Else
                        .SelectedIndex = 0
                    End If
                End With
            End If

            dsList = objCompanyBank.GetComboList(CInt(Session("CompanyUnkId")), 1, "BankGrp")
            With cboBankGroup
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("BankGrp")
                .DataBind()
                .SelectedValue = "0"
            End With

            Call cboBankGroup_SelectedIndexChanged(cboBankGroup, New EventArgs())
            Call cboStatus_SelectedIndexChanged(cboStatus, New EventArgs())
            Call cboCurrency_SelectedIndexChanged(cboCurrency, New EventArgs())
            Call cboPayPeriod_SelectedIndexChanged(cboPayPeriod, New EventArgs())

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim StrSearching As String = String.Empty
            Dim dsList As New DataSet
            objLoanAdvance = New clsLoan_Advance

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            objlblCount.Text = "( 0 / 0 )"
            'Sohail (31 Dec 2019) -- End

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                If CInt(cboCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 45, "Interest calculation type is mandatory information."), Me)
                    cboInterestCalcType.Focus()
                    Exit Sub
                ElseIf CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    StrSearching &= "AND (lnloan_advance_tran.interest_calctype_id = " & CInt(cboInterestCalcType.SelectedValue) & " " & _
                                    " OR lnloan_advance_tran.interest_calctype_id = 0) "
                End If
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue)
            End If

            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_scheme_master.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue)
            End If

            If CInt(cboAssignedPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.periodunkid = " & CInt(cboAssignedPeriod.SelectedValue)
            End If

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.deductionperiodunkid = " & CInt(cboDeductionPeriod.SelectedValue)
            End If

            If CInt(cboCalcType.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_advance_tran.calctype_id = " & CInt(cboCalcType.SelectedValue)
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearching &= "AND ST.statusunkid = " & CInt(cboStatus.SelectedValue)
            End If

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                StrSearching &= "AND lnloan_advance_tran.isloan=1 "
            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                StrSearching &= "AND lnloan_advance_tran.isloan=0 "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            Dim intFirstOpenPeriodID As Integer = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), _
                                                                                       enStatusType.OPEN, , True)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intFirstOpenPeriodID

            dsList = objLoanAdvance.GetListForGlobalChangeStatus(CStr(Session("Database_Name")), _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                                 objPeriod._Start_Date, _
                                                                 objPeriod._End_Date, _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 "Loan", StrSearching)
            mdtList = dsList.Tables("Loan")

            dgvLoanList.AutoGenerateColumns = False
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'dgvLoanList.DataSource = mdtList
            objlblCount.Text = "( 0 / " & mdtList.Rows.Count.ToString & " )"
            mdtList.DefaultView.Sort = "IsChecked DESC"
            dgvLoanList.DataSource = mdtList.DefaultView
            'Sohail (31 Dec 2019) -- End
            dgvLoanList.DataBind()

            Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))

            If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                dgvLoanList.Columns(dgv("dgcolhLoanAdvanceAmount")).HeaderText = Language.getMessage(mstrModuleName, 2, "Loan Amount")
                dgvLoanList.Columns(dgv("dgcolhCalcType")).Visible = True
                dgvLoanList.Columns(dgv("dgcolhInterestCalcType")).Visible = True

            ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                dgvLoanList.Columns(dgv("dgcolhLoanAdvanceAmount")).HeaderText = Language.getMessage(mstrModuleName, 3, "Advance Amount")
                dgvLoanList.Columns(dgv("dgcolhCalcType")).Visible = False
                dgvLoanList.Columns(dgv("dgcolhInterestCalcType")).Visible = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Calculate_LoanBalanceInfo()
        Dim strLoanAdvanceTranIDs As String = String.Empty
        Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))
        Dim decBaseCurrBalanceAmt As Decimal = 0
        Dim decPaidCurrBalanceAmt As Decimal = 0
        Try
            If mdtList.Rows.Count > 0 Then

                strLoanAdvanceTranIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())

                'If mblnIsFromApplyButton = True Then
                '    strLoanAdvanceTranIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())
                'Else
                '    strLoanAdvanceTranIDs = String.Join(",", (From p In mdtList Select (p.Item("loanadvancetranunkid").ToString)).ToArray())
                'End If


                objLoanAdvance = New clsLoan_Advance
                Dim dsLoan As DataSet = objLoanAdvance.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), _
                                                                                 CInt(Session("UserId")), _
                                                                                 CInt(Session("Fin_year")), _
                                                                                 CInt(Session("CompanyUnkId")), _
                                                                                 mdtPeriodStartDate, _
                                                                                 mdtPeriodEndDate, _
                                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                                 True, "List", dtpPaymentDate.GetDate, "", , , , , , , , , , , , , , , , _
                                                                                 strLoanAdvanceTranIDs)

                Dim dTemp As DataRow()
                dTemp = mdtList.Select("IsChecked=True")

                'If mblnIsFromApplyButton = True Then
                '    dTemp = mdtList.Select("IsChecked=True")
                '    If dTemp.Length <= 0 Then
                '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), Me)
                '        dgvLoanList.Focus()
                '        Exit Sub
                '    End If
                'Else
                '    dTemp = mdtList.Select("1=1")
                'End If

                If dTemp.Length > 0 Then

                    For Each dtRow As DataRow In dTemp

                        'Sohail (11 Feb 2020) -- Start
                        'NMB Enhancement # : index out of bound error.
                        'If radValue.Checked = True Then
                        '    CType(dgvLoanList.Items(mdtList.Rows.IndexOf(dtRow)).Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = True
                        'ElseIf radPercentage.Checked = True Then
                        '    CType(dgvLoanList.Items(mdtList.Rows.IndexOf(dtRow)).Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                        'Else
                        '    CType(dgvLoanList.Items(mdtList.Rows.IndexOf(dtRow)).Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                        'End If
                        'Sohail (11 Feb 2020) -- End

                        Dim dR As DataRow() = dsLoan.Tables("List").Select("loanadvancetranunkid=" & CInt(dtRow.Item("loanadvancetranunkid")))
                        If dR.Length > 0 Then
                            If CInt(dtRow("modeid")) = enLoanAdvance.LOAN Then
                                decBaseCurrBalanceAmt = CDec(dR(0).Item("LastProjectedBalance"))
                            ElseIf CInt(dtRow("modeid")) = enLoanAdvance.ADVANCE Then
                                decBaseCurrBalanceAmt = CDec(dR(0).Item("balance_amount"))
                            End If

                            If mdecBaseExRate <> 0 Then
                                decPaidCurrBalanceAmt = decBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
                                dtRow.Item("balance_amount") = decPaidCurrBalanceAmt
                            Else
                                decPaidCurrBalanceAmt = decBaseCurrBalanceAmt * mdecPaidExRate
                                dtRow.Item("balance_amount") = decPaidCurrBalanceAmt
                            End If

                            If radPercentage.Checked = True Then
                                dtRow.Item("settelment_amount") = decPaidCurrBalanceAmt * CDec(txtPercentage.Text) / 100
                                'If mblnIsFromApplyButton = True Then
                                '    dtRow.Item("settelment_amount") = decPaidCurrBalanceAmt * CDec(txtPercentage.Text) / 100
                                'Else
                                '    dtRow.Item("settelment_amount") = decPaidCurrBalanceAmt
                                'End If
                            Else
                                dtRow.Item("settelment_amount") = decPaidCurrBalanceAmt
                            End If

                            dtRow.Item("settelment_amount") = CDec(Format(dtRow.Item("settelment_amount"), GUI.fmtCurrency))
                            dtRow.Item("changestatusunkid") = IIf(CBool(dtRow.Item("IsChecked")) = True, CInt(cboChangeStatus.SelectedValue), 0)
                            Select Case CInt(dtRow.Item("changestatusunkid"))
                                Case 0 'Select
                                    dtRow.Item("ChangedStatus") = ""
                                Case enLoanStatus.IN_PROGRESS
                                    dtRow.Item("ChangedStatus") = Language.getMessage(mstrModuleName, 32, "In Progress")
                                Case enLoanStatus.ON_HOLD
                                    dtRow.Item("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "On Hold")
                                Case enLoanStatus.WRITTEN_OFF
                                    dtRow.Item("ChangedStatus") = Language.getMessage(mstrModuleName, 34, "Written Off")
                            End Select
                        End If
                        dtRow.AcceptChanges()
                    Next
                End If
            End If
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'dgvLoanList.DataSource = mdtList
            If mdtList IsNot Nothing AndAlso mdtList.Columns.Count > 0 Then mdtList.DefaultView.Sort = "IsChecked DESC"
            dgvLoanList.DataSource = mdtList.DefaultView
            'Sohail (31 Dec 2019) -- End
            dgvLoanList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Calculate_LoanBalanceInfo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Calculate_LoanBalanceInfo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            Dim blnFlag As Boolean
            Dim dsList As New DataSet
            Dim strEmployeeIDs As String = String.Empty
            Dim strLoanAdvanceUnkIDs As String = String.Empty
            Dim objEmpBank As New clsEmployeeBanks
            Dim objExRate As New clsExchangeRate

            If mdtList.Rows.Count > 0 Then
                strEmployeeIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("employeeunkid").ToString)).ToArray())
                strLoanAdvanceUnkIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())
            End If

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please select Pay Period."), Me)
                cboPayPeriod.Focus()
                Return False
            End If

            If CInt(Session("PaymentVocNoType")) = 0 Then
                If txtVoucherNo.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Voucher No. cannot be blank. Voucher No. is compulsory information."), Me)
                    txtVoucherNo.Focus()
                    Return False
                End If
            End If

            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.GetDate, True)
            If dsList Is Nothing OrElse dsList.Tables("ExRate").Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 44, "There is no exchange rate defined for current period."), Me)
                Return False
            End If

            If mblnIsWrittenOff = True Then

                If radValue.Checked = False AndAlso radPercentage.Checked = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), Me)
                    Return False
                End If

                If CDate(dtpPaymentDate.GetDate).Date < mdtPeriodStartDate OrElse CDate(dtpPaymentDate.GetDate).Date > mdtPeriodEndDate Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Payment Date should be in between selected period start date and end date."), Me)
                    dtpPaymentDate.Focus()
                    Return False
                End If

                If CInt(cboPaymentMode.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Payment Mode is compulsory information. Please select Payment Mode to continue."), Me)
                    cboPaymentMode.Focus()
                    Return False
                End If

                Select Case CInt(cboPaymentMode.SelectedValue)
                    Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER
                        If CInt(cboBankGroup.SelectedValue) <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), Me)
                            cboBankGroup.Focus()
                            Return False
                        End If
                        If CInt(cboBranch.SelectedValue) <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), Me)
                            cboBranch.Focus()
                            Return False
                        ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please select Bank Account. Bank Account is mandatory information."), Me)
                            cboAccountNo.Focus()
                            Return False
                        End If
                        If txtChequeNo.Text = "" Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Cheque No. cannot be blank. Please enter the cheque information."), Me)
                            txtChequeNo.Focus()
                            Return False
                        End If
                End Select

                If radPercentage.Checked = True Then
                    If CDec(txtPercentage.Text) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Percent cannot be 0 or blank. Percent is compulsory information."), Me)
                        txtPercentage.Focus()
                        Return False

                    ElseIf CDec(txtPercentage.Text) <= 0.0 AndAlso CDec(txtPercentage.Text) > 100.0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100."), Me)
                        txtPercentage.Focus()
                        Return False
                    End If
                End If

                If txtRemarks.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Remark cannot be blank. Remark is compulsory information."), Me)
                    txtRemarks.Focus()
                    Return False
                End If
            End If

            Dim objLoan_Advance As New clsLoan_Advance
            Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
            Dim objTnA As New clsTnALeaveTran
            Dim dREx As DataRow
            mdtExcel = New DataTable

            If mdtExcel.Columns.Contains("LoanVocNo") = False Then mdtExcel.Columns.Add("LoanVocNo", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("Mode") = False Then mdtExcel.Columns.Add("Mode", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("Employee") = False Then mdtExcel.Columns.Add("Employee", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("LoanScheme") = False Then mdtExcel.Columns.Add("LoanScheme", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("ReferScreen") = False Then mdtExcel.Columns.Add("ReferScreen", GetType(System.String)).DefaultValue = ""
            If mdtExcel.Columns.Contains("Remark") = False Then mdtExcel.Columns.Add("Remark", GetType(System.String)).DefaultValue = ""

            For Each dRow As DataRow In mdtList.Select("IsChecked=True")

                blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPayPeriod.SelectedValue), dRow("employeeunkid").ToString, _
                                                                                  dtpPaymentDate.GetDate)
                If blnFlag = False Then
                    dREx = mdtExcel.NewRow
                    dREx("LoanVocNo") = dRow("VoucherNo")
                    dREx("Mode") = dRow("Mode")
                    dREx("Employee") = dRow("Employee")
                    dREx("LoanScheme") = dRow("LoanScheme")
                    dREx("ReferScreen") = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                    dREx("Remark") = Language.getMessage(mstrModuleName, 19, "Some of Operation(s) either Interest Rate/EMI Tenure/Topup amount are still pending for approval process in current period. In order to change status, Please either Approve or Reject pending operations for current period.")
                    mdtExcel.Rows.Add(dREx)
                    dRow("IsError") = True
                End If

                If objTnA.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), dRow("employeeunkid").ToString, mdtPeriodEndDate, _
                                               enModuleReference.Payroll) = True Then
                    dREx = mdtExcel.NewRow
                    dREx("LoanVocNo") = dRow("VoucherNo")
                    dREx("Mode") = dRow("Mode")
                    dREx("Employee") = dRow("Employee")
                    dREx("LoanScheme") = dRow("LoanScheme")
                    dREx("ReferScreen") = Language.getMessage(mstrModuleName, 37, "Process Payroll")
                    dREx("Remark") = Language.getMessage(mstrModuleName, 20, "You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for this employee.")
                    mdtExcel.Rows.Add(dREx)
                    dRow("IsError") = True
                End If

                dsList = objLoan_Advance.GetLastLoanBalance("List", CInt(dRow("loanadvancetranunkid")))
                Dim strMsg As String = ""
                Dim strScreen As String = ""
                If dsList.Tables("List").Rows.Count > 0 Then
                    If dsList.Tables("List").Rows.Count > 0 AndAlso _
                       dsList.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(mdtPeriodStartDate) Then
                        If CInt(dsList.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 21, " Process Payroll.")
                            strScreen = Language.getMessage(mstrModuleName, 38, "Payroll Transaction => Process Payroll")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(dsList.Tables("List").Rows(0).Item("isreceipt")) = False Then
                            strMsg = Language.getMessage(mstrModuleName, 22, " Loan Payment List.")
                            strScreen = Language.getMessage(mstrModuleName, 39, "Loan/Advance & Savings => Loan/Advance => Operation => Payment")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(dsList.Tables("List").Rows(0).Item("isreceipt")) = True Then
                            strMsg = Language.getMessage(mstrModuleName, 23, " Receipt Payment List.")
                            strScreen = Language.getMessage(mstrModuleName, 40, "Loan/Advance & Savings => Loan/Advance => Operation => Receipt")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 24, " Loan Installment Change.")
                            strScreen = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 25, " Loan Topup Added.")
                            strScreen = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                        ElseIf CInt(dsList.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                            strMsg = Language.getMessage(mstrModuleName, 26, " Loan Rate Change.")
                            strScreen = Language.getMessage(mstrModuleName, 36, "Loan/Advance & Savings => Loan/Advance => Operation => Other Loan Operation")
                        End If

                        dREx = mdtExcel.NewRow
                        dREx("LoanVocNo") = dRow("VoucherNo")
                        dREx("Mode") = dRow("Mode")
                        dREx("Employee") = dRow("Employee")
                        dREx("LoanScheme") = dRow("LoanScheme")
                        dREx("ReferScreen") = strScreen
                        dREx("Remark") = Language.getMessage(mstrModuleName, 27, "You cannot change loan status. Please delete last transaction from the screen of") & strMsg
                        mdtExcel.Rows.Add(dREx)
                        dRow("IsError") = True
                    End If
                End If

                If mblnIsWrittenOff = True Then

                    If IsDBNull(dRow("settelment_amount")) = True OrElse CDec(dRow("settelment_amount")) <= 0 Then
                        dREx = mdtExcel.NewRow
                        dREx("LoanVocNo") = dRow("VoucherNo")
                        dREx("Mode") = dRow("Mode")
                        dREx("Employee") = dRow("Employee")
                        dREx("LoanScheme") = dRow("LoanScheme")
                        dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                        dREx("Remark") = Language.getMessage(mstrModuleName, 48, "Settlement amount cannot be blank. Settlement amount is compulsory information.")
                        mdtExcel.Rows.Add(dREx)
                        dRow("IsError") = True
                    Else
                        If CDec(dRow("settelment_amount")) > CDec(dRow("balance_amount")) Then
                            dREx = mdtExcel.NewRow
                            dREx("LoanVocNo") = dRow("VoucherNo")
                            dREx("Mode") = dRow("Mode")
                            dREx("Employee") = dRow("Employee")
                            dREx("LoanScheme") = dRow("LoanScheme")
                            dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                            dREx("Remark") = Language.getMessage(mstrModuleName, 49, "Settlement amount cannot be greater than Balance amount.")
                            mdtExcel.Rows.Add(dREx)
                            dRow("IsError") = True
                        End If
                    End If

                    If CStr(dRow("ChangedStatus")) = "" OrElse CInt(dRow("changestatusunkid")) <= 0 Then
                        dREx = mdtExcel.NewRow
                        dREx("LoanVocNo") = dRow("VoucherNo")
                        dREx("Mode") = dRow("Mode")
                        dREx("Employee") = dRow("Employee")
                        dREx("LoanScheme") = dRow("LoanScheme")
                        dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                        dREx("Remark") = Language.getMessage(mstrModuleName, 50, "Change Status cannot be balnk. Change Status is mandatory information.")
                        mdtExcel.Rows.Add(dREx)
                        dRow("IsError") = True
                    End If

                    Select Case CInt(cboPaymentMode.SelectedValue)
                        Case enPaymentMode.CHEQUE, enPaymentMode.TRANSFER

                            If objEmpBank.isEmployeeBankExist(CInt(dRow("employeeunkid"))) = False Then
                                dREx = mdtExcel.NewRow
                                dREx("LoanVocNo") = dRow("VoucherNo")
                                dREx("Mode") = dRow("Mode")
                                dREx("Employee") = dRow("Employee")
                                dREx("LoanScheme") = dRow("LoanScheme")
                                dREx("ReferScreen") = Language.getMessage(mstrModuleName, 41, "Payroll Transaction => Employee Banks")
                                dREx("Remark") = Language.getMessage(mstrModuleName, 28, "This employees do not have any bank account. Please add bank account from Employee Bank.")
                                mdtExcel.Rows.Add(dREx)
                                dRow("IsError") = True
                            End If

                            dsList = objEmpBank.GetEmployeeWithOverDistribution(CStr(Session("Database_Name")), CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                CStr(Session("UserAccessModeSetting")), True, True, False, _
                                                                                "", "List", dRow("employeeunkid").ToString)
                            If dsList.Tables("List").Rows.Count > 0 Then
                                dREx = mdtExcel.NewRow
                                dREx("LoanVocNo") = dRow("VoucherNo")
                                dREx("Mode") = dRow("Mode")
                                dREx("Employee") = dRow("Employee")
                                dREx("LoanScheme") = dRow("LoanScheme")
                                dREx("ReferScreen") = Language.getMessage(mstrModuleName, 43, "Payroll Transaction => Employee Banks")
                                dREx("Remark") = Language.getMessage(mstrModuleName, 30, "This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank.")
                                mdtExcel.Rows.Add(dREx)
                                dRow("IsError") = True
                            End If
                    End Select

                    dsList = objLoan_Advance.GetLastLoanBalance("List", CInt(dRow("employeeunkid")))
                    If dsList.Tables("List").Rows.Count > 0 Then
                        If dsList.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtpPaymentDate.GetDate) Then
                            dREx = mdtExcel.NewRow
                            dREx("LoanVocNo") = dRow("VoucherNo")
                            dREx("Mode") = dRow("Mode")
                            dREx("Employee") = dRow("Employee")
                            dREx("LoanScheme") = dRow("LoanScheme")
                            dREx("ReferScreen") = Language.getMessage(mstrModuleName, 42, "Loan/Advance & Savings => Loan/Advance => Operation => Global Change Status")
                            dREx("Remark") = Language.getMessage(mstrModuleName, 29, "Payment date should not less than last transactoin date") & _
                                                  " " & Format(eZeeDate.convertDate(dsList.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & "."
                            mdtExcel.Rows.Add(dREx)
                            dRow("IsError") = True
                        End If
                    End If

                End If
            Next
            mdtList.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidate:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub SetVisibility(ByVal IsEnable As Boolean)
        Try
            Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))

            'If CInt(cboChangeStatus.SelectedValue) <> 0 Then mblnIsFromApplyButton = IsEnable
            radValue.Enabled = IsEnable
            'radValue.Checked = False
            radPercentage.Enabled = IsEnable
            'radPercentage.Checked = False
            cboPaymentMode.Enabled = IsEnable
            If CInt(cboPaymentMode.SelectedValue) <= 0 Then
                cboBankGroup.Enabled = IsEnable
                cboBranch.Enabled = IsEnable
                cboAccountNo.Enabled = IsEnable
                txtChequeNo.Enabled = IsEnable
            End If
            txtChequeNo.Text = ""
            txtPercentage.Text = "0"
            If radValue.Checked = True Then
                txtPercentage.Enabled = False
            Else
                txtPercentage.Enabled = IsEnable
            End If
            txtRemarks.Text = ""
            dtpPaymentDate.Enabled = IsEnable
            'btnApply.Enabled = IsEnable
            chkCalculateInterest.Checked = False

            dgvLoanList.Columns(dgv("dgcolhSettelmentAmount")).Visible = IsEnable

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : SetVisibility : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrAdvanceFilter = ""
            cboEmployee.SelectedValue = "0"
            cboLoanScheme.SelectedValue = "0"
            cboAssignedPeriod.SelectedValue = "0"
            cboDeductionPeriod.SelectedValue = "0"
            cboStatus.SelectedIndex = 0
            cboCalcType.Enabled = True
            cboCalcType.SelectedValue = "0"
            cboInterestCalcType.Enabled = True
            cboInterestCalcType.SelectedValue = "0"
            cboMode.SelectedValue = CStr(enLoanAdvance.LOAN)
            radValue.Enabled = True : radPercentage.Enabled = True
            radValue.Checked = False : radPercentage.Checked = False
            cboPaymentMode.Enabled = True
            cboPaymentMode.SelectedValue = "0"
            cboCurrency.SelectedValue = CStr(mintBaseCountryId)
            cboBankGroup.Enabled = True
            cboBankGroup.SelectedValue = "0"
            cboBranch.Enabled = True
            cboBranch.SelectedValue = "0"
            cboAccountNo.Enabled = True
            cboAccountNo.SelectedValue = "0"
            txtChequeNo.Enabled = True
            txtChequeNo.Text = ""
            cboChangeStatus.SelectedValue = "0"
            txtRemarks.Text = ""
            txtPercentage.Enabled = True
            txtPercentage.Text = "0"
            txtVoucherNo.Text = ""
            chkCalculateInterest.Enabled = True

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            dtpPaymentDate.Enabled = True
            dtpPaymentDate.SetDate = CDate(objPeriod._Start_Date).Date

            dgvLoanList.DataSource = New List(Of String)
            dgvLoanList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ResetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue(ByVal dRow As DataRow)
        Dim strStatusData As String = String.Empty
        Try
            objStatusTran._Isvoid = False
            objStatusTran._Loanadvancetranunkid = CInt(dRow("loanadvancetranunkid"))
            objStatusTran._Remark = txtRemarks.Text
            objStatusTran._Settle_Amount = 0
            objStatusTran._Userunkid = CInt(Session("UserId"))
            objStatusTran._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objStatusTran._Staus_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboChangeStatus.SelectedValue)
            If CInt(dRow("calctype_id")) <> enLoanCalcId.No_Interest Then
                objStatusTran._IsCalculateInterest = chkCalculateInterest.Checked
            Else
                objStatusTran._IsCalculateInterest = False
            End If

            If mblnIsWrittenOff = True Then

                strStatusData = objStatusTran._Isvoid & "|" & _
                                 objStatusTran._Loanadvancetranunkid & "|" & _
                                 objStatusTran._Remark & "|" & _
                                 objStatusTran._Settle_Amount & "|" & _
                                 objStatusTran._Staus_Date & "|" & _
                                 objStatusTran._Voiddatetime & "|" & _
                                 objStatusTran._Voiduserunkid & "|" & _
                                 objStatusTran._Statusunkid & "|" & _
                                 objStatusTran._Periodunkid & "|"

                objPaymentTran._strData = strStatusData
                objPaymentTran._IsFromStatus = True

                objPaymentTran._Voucherno = txtVoucherNo.Text
                objPaymentTran._Periodunkid = CInt(dRow("periodunkid"))
                objPaymentTran._Paymentdate = dtpPaymentDate.GetDate
                objPaymentTran._PaymentDate_Periodunkid = CInt(cboPayPeriod.SelectedValue)
                objPaymentTran._Employeeunkid = CInt(dRow("employeeunkid"))
                If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                    objPaymentTran._Paymentrefid = enLoanAdvance.LOAN
                ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                    objPaymentTran._Paymentrefid = enLoanAdvance.ADVANCE
                End If
                objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
                objPaymentTran._Branchunkid = CInt(cboBranch.SelectedValue)
                objPaymentTran._Chequeno = txtChequeNo.Text
                If radValue.Checked = True Then
                    objPaymentTran._Paymentbyid = enPaymentBy.Value
                ElseIf radPercentage.Checked = True Then
                    objPaymentTran._Paymentbyid = enPaymentBy.Percentage
                End If
                objPaymentTran._Percentage = CDec(IIf(radPercentage.Checked, txtPercentage.Text, 0))
                objPaymentTran._Voucherref = CStr(dRow("VoucherNo"))
                objPaymentTran._Payreftranunkid = CInt(dRow("loanadvancetranunkid"))
                objPaymentTran._PaymentTypeId = clsPayment_tran.enPayTypeId.RECEIVED
                objPaymentTran._Accountno = CStr(IIf(CInt(cboAccountNo.SelectedValue) > 0, cboAccountNo.Text, "0"))
                objPaymentTran._Is_Receipt = True
                objPaymentTran._Isglobalpayment = True
                objPaymentTran._Isapproved = False
                objPaymentTran._Userunkid = CInt(Session("UserId"))
                objPaymentTran._Isvoid = False
                objPaymentTran._Voiddatetime = Nothing
                objPaymentTran._Voiduserunkid = -1
                objPaymentTran._Voidreason = ""

                objPaymentTran._Basecurrencyid = mintBaseCurrId
                objPaymentTran._Baseexchangerate = mdecBaseExRate
                objPaymentTran._Paidcurrencyid = mintPaidCurrId
                objPaymentTran._Expaidrate = mdecPaidExRate
                objPaymentTran._Countryunkid = CInt(cboCurrency.SelectedValue)
                objPaymentTran._Roundingtypeid = 0
                objPaymentTran._Roundingmultipleid = 0
                objPaymentTran._RoundingAdjustment = CDec(0.0)
                objPaymentTran._Remarks = txtRemarks.Text.Trim

                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = CDec(dRow("settelment_amount")) * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = CDec(dRow("settelment_amount")) * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = CDec(dRow("settelment_amount"))
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " ComboBox's Events "

    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Try
            Dim objCompanyBank As New clsCompany_Bank_tran
            Dim dsList As New DataSet

            dsList = objCompanyBank.GetComboList(CInt(Session("CompanyUnkId")), 2, "Branch", _
                                                 " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
            With cboBranch
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Branch")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboBranch_SelectedIndexChanged(cboBranch, New EventArgs())

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboBankGroup_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboBankGroup_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsList As New DataSet
        Try
            dsList = objCompanyBank.GetComboListBankAccount("Account", CInt(Session("CompanyUnkId")), True, _
                                                            CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
            With cboAccountNo
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("Account")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboBranch_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboBranch_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub

    Private Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged
        Try
            If CInt(cboPaymentMode.SelectedValue) > 0 AndAlso CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH Then
                cboBankGroup.Enabled = False
                cboBankGroup.SelectedValue = "0"
                cboBranch.Enabled = False
                cboBranch.SelectedValue = "0"
                cboAccountNo.Enabled = False
                cboAccountNo.SelectedValue = "0"
                txtChequeNo.Text = ""
                txtChequeNo.Enabled = False
            Else
                cboBankGroup.Enabled = True
                cboBankGroup.SelectedValue = "0"
                cboBranch.Enabled = True
                cboBranch.SelectedValue = "0"
                cboAccountNo.Enabled = True
                cboAccountNo.SelectedValue = "0"
                txtChequeNo.Text = ""
                txtChequeNo.Enabled = True
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPaymentMode_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPaymentMode_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objMaster As New clsMasterData

            If CInt(cboStatus.SelectedValue) > 0 Then
                dsList = objMaster.GetLoan_Saving_Status("Status", False)
                Dim dtTable As DataTable = dsList.Tables("Status").Select("Id NOT IN(" & CInt(cboStatus.SelectedValue) & "," & enLoanStatus.COMPLETED & ")").CopyToDataTable
                With cboChangeStatus
                    .DataValueField = "Id"
                    .DataTextField = "NAME"
                    .DataSource = dtTable
                    .DataBind()
                    .SelectedValue = "0"
                End With
                Call cboChangeStatus_SelectedIndexChanged(cboChangeStatus, New EventArgs())
                If dgvLoanList.Items.Count > 0 Then Call FillList()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStatus_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboStatus_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), enStatusType.OPEN)
                If intFirstOpenPeriod > 0 Then
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriod
                    dtpPaymentDate.SetDate = CDate(objPeriod._Start_Date).Date
                    mdtPeriodStartDate = CDate(objPeriod._Start_Date).Date
                    mdtPeriodEndDate = CDate(objPeriod._End_Date).Date
                End If
            Else
                dtpPaymentDate.SetDate = CDate(ConfigParameter._Object._CurrentDateAndTime).Date
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPayPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPayPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExchangeRate.Text = ""

            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.GetDate, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable

            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExchangeRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString) & _
                                          " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & _
                                          dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If

            If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then
                Call Calculate_LoanBalanceInfo()
            End If

            'Dim dRow As DataRow() = mdtList.Select("IsChecked=True")
            'If dRow.Length > 0 Then
            '    Call Calculate_LoanBalanceInfo()
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError("Procedure : cboCurrency_SelectedIndexChanged : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboChangeStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboChangeStatus.SelectedIndexChanged
        Try
            Select Case CInt(cboChangeStatus.SelectedValue)
                Case 0 'Select
                    chkCalculateInterest.Enabled = True
                    cboPaymentMode.SelectedValue = "0"
                    cboBankGroup.SelectedValue = "0"
                    Call SetVisibility(True)

                Case enLoanStatus.IN_PROGRESS
                    chkCalculateInterest.Enabled = False
                    cboPaymentMode.SelectedValue = "0"
                    cboBankGroup.SelectedValue = "0"
                    Call SetVisibility(False)

                Case enLoanStatus.ON_HOLD
                    cboPaymentMode.SelectedValue = "0"
                    cboBankGroup.SelectedValue = "0"
                    chkCalculateInterest.Enabled = True
                    Call SetVisibility(False)

                Case enLoanStatus.WRITTEN_OFF
                    chkCalculateInterest.Enabled = False
                    Call SetVisibility(True)
            End Select

            If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then

                'Sohail (11 Feb 2020) -- Start
                'NMB Enhancement # : index out of bound error.
                'Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))

                'If CInt(cboChangeStatus.SelectedValue) > 0 Then
                '    For Each dR As DataRow In mdtList.Rows
                '        If CBool(dR.Item("IsChecked")) = True Then
                '            dR("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
                '            dR("ChangedStatus") = IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.SelectedItem.Text, "").ToString
                '            dgvLoanList.Items(mdtList.Rows.IndexOf(dR)).Cells(dgv("dgcolhChangedStatus")).Text = cboChangeStatus.SelectedItem.Text
                '            If CInt(cboChangeStatus.SelectedValue) = enLoanStatus.WRITTEN_OFF Then
                '                CType(dgvLoanList.Items(mdtList.Rows.IndexOf(dR)).Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = True
                '            Else
                '                CType(dgvLoanList.Items(mdtList.Rows.IndexOf(dR)).Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                '            End If
                '        End If
                '    Next
                'Else
                '    For Each row As DataRow In mdtList.Rows
                '        row("changestatusunkid") = 0
                '        row("ChangedStatus") = ""
                '        'dgvLoanList.Items(mdtList.Rows.IndexOf(row)).Cells(dgv("dgcolhChangedStatus")).Text = ""
                '    Next
                'End If
                'mdtList.AcceptChanges()
                'Sohail (11 Feb 2020) -- End

                Call Calculate_LoanBalanceInfo()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboChangeStatus_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboChangeStatus_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboInterestCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboInterestCalcType.SelectedIndexChanged
        Try
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                'chkSelectAll.Checked = False
                If CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.DAILY Then
                    dtpPaymentDate.SetDate = mdtPeriodStartDate
                    dtpPaymentDate.Enabled = True
                    If dgvLoanList.Items.Count > 0 Then Call FillList()
                    'Sohail (14 Mar 2017) -- Start
                    'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                    'ElseIf CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.MONTHLY Then
                    'Sohail (23 May 2017) -- Start
                    'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                    'ElseIf CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.MONTHLY OrElse CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.BY_TENURE Then
                ElseIf CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.MONTHLY OrElse CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.BY_TENURE OrElse CInt(cboInterestCalcType.SelectedValue) = enLoanInterestCalcType.SIMPLE_INTEREST Then
                    'Sohail (23 May 2017) -- End
                    'Sohail (14 Mar 2017) -- End
                    dtpPaymentDate.SetDate = mdtPeriodStartDate
                    dtpPaymentDate.Enabled = False
                    If dgvLoanList.Items.Count > 0 Then Call FillList()
                End If
                'Call FillList()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboInterestCalcType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboInterestCalcType_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMode.SelectedIndexChanged
        Try
            If CInt(cboMode.SelectedValue) > 0 Then
                If CInt(cboMode.SelectedValue) = enLoanAdvance.LOAN Then
                    cboCalcType.SelectedValue = "0"
                    cboCalcType.Enabled = True
                    cboInterestCalcType.SelectedValue = "0"
                    cboInterestCalcType.Enabled = True
                    chkCalculateInterest.Checked = False
                    chkCalculateInterest.Enabled = True

                ElseIf CInt(cboMode.SelectedValue) = enLoanAdvance.ADVANCE Then
                    cboCalcType.SelectedValue = "0"
                    cboCalcType.Enabled = False
                    cboInterestCalcType.SelectedValue = "0"
                    cboInterestCalcType.Enabled = False
                    chkCalculateInterest.Checked = False
                    chkCalculateInterest.Enabled = False
                End If
                dgvLoanList.DataSource = Nothing
                dgvLoanList.DataBind()

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboMode_SelectedIndexChanged", mstrModuleName)1
            DisplayMessage.DisplayError("Procedure : cboMode_SelectedIndexChanged : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvLoanList.Items.Count <= 0 Then Exit Sub
            Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            For Each item As DataGridItem In dgvLoanList.Items
                CType(item.Cells(dgv("objdgcolhSelectAll")).FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                Dim dRow() As DataRow = mdtList.Select("loanadvancetranunkid=" & CInt(item.Cells(dgv("objdgcolhLoanAdvanceUnkid")).Text))
                If dRow.Length > 0 Then
                    dRow(0).Item("IsChecked") = chkSelectAll.Checked
                End If
                If chkSelectAll.Checked Then
                    dRow(0).Item("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
                    dRow(0).Item("ChangedStatus") = CStr(IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.SelectedItem.Text, ""))
                    item.Cells(dgv("dgcolhChangedStatus")).Text = CStr(IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.SelectedItem.Text, ""))
                Else
                    dRow(0).Item("changestatusunkid") = 0
                    dRow(0).Item("ChangedStatus") = ""
                    item.Cells(dgv("dgcolhChangedStatus")).Text = ""
                End If

                If radValue.Checked Then
                    'Sohail (11 Feb 2020) -- Start
                    'NMB Enhancement # : index out of bound error.
                    'CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = chkSelectAll.Checked
                    CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = True
                    'Sohail (11 Feb 2020) -- End
                ElseIf radPercentage.Checked Then
                    CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                Else
                    CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                End If
            Next
            mdtList.AcceptChanges()

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            objlblCount.Text = "( " & mdtList.Select("IsChecked = 1 ").Length.ToString & " / " & mdtList.Rows.Count.ToString & " )"
            'Sohail (31 Dec 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If dgvLoanList.Items.Count <= 0 Then Exit Sub
            Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))
            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

            Dim dRow() As DataRow = mdtList.Select("loanadvancetranunkid=" & CInt(item.Cells(dgv("objdgcolhLoanAdvanceUnkid")).Text))
            If dRow.Length > 0 Then
                dRow(0).Item("IsChecked") = chkSelect.Checked

                If chkSelect.Checked Then
                    dRow(0).Item("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
                    dRow(0).Item("ChangedStatus") = CStr(IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.SelectedItem.Text, ""))
                    item.Cells(dgv("dgcolhChangedStatus")).Text = CStr(IIf(CInt(cboChangeStatus.SelectedValue) > 0, cboChangeStatus.SelectedItem.Text, ""))
                Else
                    dRow(0).Item("changestatusunkid") = 0
                    dRow(0).Item("ChangedStatus") = ""
                    item.Cells(dgv("dgcolhChangedStatus")).Text = ""
                End If
            End If

            mdtList.AcceptChanges()

            'Sohail (11 Feb 2020) -- Start
            'NMB Enhancement # : index out of bound error.
            'If radValue.Checked = True Then
            '    CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = chkSelect.Checked
            'ElseIf radPercentage.Checked Then
            '    CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
            'Else
            '    CType(item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
            'End If
            'Sohail (11 Feb 2020) -- End

            Dim xCount As DataRow() = mdtList.Select("IsChecked=True")
            If xCount.Length = dgvLoanList.Items.Count Then
                CType(dgvLoanList.Controls(dgv("objdgcolhSelectAll")).Controls(dgv("objdgcolhSelectAll")).FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvLoanList.Controls(dgv("objdgcolhSelectAll")).Controls(dgv("objdgcolhSelectAll")).FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

            mdtList.AcceptChanges()

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            objlblCount.Text = "( " & xCount.Length.ToString & " / " & mdtList.Rows.Count.ToString & " )"
            'Sohail (31 Dec 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvLoanList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLoanList.ItemDataBound
        Try
            Call SetDateFormat()

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                Dim dgv = dgvLoanList.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvLoanList.Columns.IndexOf(x))
                'Dim dRow As DataRowView = CType(e.Item.DataItem, DataRowView)

                e.Item.Cells(dgv("dgcolhLoanAdvanceAmount")).Text = Format(CDec(e.Item.Cells(dgv("dgcolhLoanAdvanceAmount")).Text), Session("fmtCurrency").ToString)
                e.Item.Cells(dgv("dgcolhBalanceAmount")).Text = Format(CDec(e.Item.Cells(dgv("dgcolhBalanceAmount")).Text), Session("fmtCurrency").ToString)

                Dim txtSettlementAmt As TextBox = CType(e.Item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox)
                If txtSettlementAmt.Text <> "" AndAlso txtSettlementAmt.Text <> "&nbsp;" Then
                    txtSettlementAmt.Text = Format(CDec(txtSettlementAmt.Text), Session("fmtCurrency").ToString)
                End If

                Dim chkSelect As CheckBox = CType(e.Item.Cells(dgv("objdgcolhSelectAll")).FindControl("chkSelect"), CheckBox)

                'Sohail (11 Feb 2020) -- Start
                'NMB Enhancement # : index out of bound error.
                'If radValue.Checked Then
                '    CType(e.Item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = chkSelect.Checked
                If radValue.Checked AndAlso CInt(cboChangeStatus.SelectedValue) = enLoanStatus.WRITTEN_OFF Then
                    CType(e.Item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = True
                    'Sohail (11 Feb 2020) -- End
                ElseIf radPercentage.Checked Then
                    CType(e.Item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                Else
                    CType(e.Item.Cells(dgv("dgcolhSettelmentAmount")).FindControl("txtdgcolhSettelmentAmount"), TextBox).Enabled = False
                End If

                If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then
                    Dim dRow() As DataRow = mdtList.Select("IsChecked=True")
                    If dRow.Length > 0 AndAlso dRow.Length = mdtList.Rows.Count Then
                        CType(dgvLoanList.Controls(dgv("objdgcolhSelectAll")).Controls(dgv("objdgcolhSelectAll")).FindControl("chkSelectAll"), CheckBox).Checked = True
                    End If
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanList_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvLoanList_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Date Control's Events "
    Protected Sub dtpPaymentDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpPaymentDate.TextChanged
        Try
            If radValue.Checked = False AndAlso radPercentage.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), Me)
                dtpPaymentDate.SetDate = mdtPeriodStartDate
                lblPaymentBy.Focus()
                Exit Sub
            End If

            Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
            If dTemp.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), Me)
                dgvLoanList.Focus()
                Exit Sub
            End If

            'mblnIsFromApplyButton = False
            Call Calculate_LoanBalanceInfo()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpPaymentDate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dtpPaymentDate_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " TextBox's Events "

    Protected Sub txtPercentage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPercentage.TextChanged
        Try
            If txtPercentage.Text <> "" Then
                'mblnIsFromApplyButton = True
                If radValue.Checked = False AndAlso radPercentage.Checked = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), Me)
                    lblPaymentBy.Focus()
                    Exit Sub
                End If

                If radPercentage.Checked = True Then
                    If CDec(txtPercentage.Text) < 0.0 AndAlso CDec(txtPercentage.Text) > 100.0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100."), Me)
                        txtPercentage.Focus()
                        Exit Sub
                    End If
                End If

                If CInt(cboChangeStatus.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please select Change status. Change status is mandatory information."), Me)
                    cboChangeStatus.Focus()
                    Exit Sub
                End If

                Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
                If dTemp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), Me)
                    dgvLoanList.Focus()
                    Exit Sub
                End If

                Call Calculate_LoanBalanceInfo()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtPercentage_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtPercentage_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtdgcolhSettelmentAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtSettlementAmt As TextBox = CType(sender, TextBox)
            Dim item As DataGridItem = CType(txtSettlementAmt.NamingContainer, DataGridItem)

            If txtSettlementAmt.Text = "" Then
                txtSettlementAmt.Text = Format(CDec(mdtList.Rows(item.ItemIndex).Item("balance_amount")), CStr(Session("fmtCurrency")))

            ElseIf txtSettlementAmt.Text <> "" Then
                If CDec(txtSettlementAmt.Text) > CDec(mdtList.Rows(item.ItemIndex).Item("balance_amount")) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Settelment amount cannot be greater than Balance amout."), Me)
                    txtSettlementAmt.Text = Format(CDec(mdtList.Rows(item.ItemIndex).Item("balance_amount")), CStr(Session("fmtCurrency")))
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtdgcolhSettelmentAmount_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtdgcolhSettelmentAmount_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (31 Dec 2019) -- Start
    'NMB Enhancement # : Search option on Global Loan Change Status screen.
    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then
                'Sohail (29 Jan 2020) -- Start
                'NMB Enhancement # : Comma separated employee code search option on global loan change status screen.
                'mdtList.DefaultView.RowFilter = "employee LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'  OR loanscheme LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR loancalctype LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                If txtSearch.Text.Contains(",") = True Then
					'Sohail (11 Feb 2020) -- Start
					'NMB Enhancement # : allow to search with space after comma.
                    'Dim arr As String = "'" & txtSearch.Text.Replace("'", "''").Replace(",", "','") & "'"
					Dim arr As String = "'" & txtSearch.Text.Replace("  ", "").Replace(" ", "").Replace("'", "''").Replace(",", "','") & "'"
					'Sohail (11 Feb 2020) -- End
                    mdtList.DefaultView.RowFilter = "employeecode IN (" & arr & ") "
                Else
                    mdtList.DefaultView.RowFilter = "employee LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'  OR loanscheme LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR loancalctype LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR VoucherNo LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                End If
                'Sohail (29 Jan 2020) -- End
                mdtList.DefaultView.Sort = "IsChecked DESC "

                With dgvLoanList
                    .DataSource = mdtList.DefaultView
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("txtSearch_TextChanged :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (31 Dec 2019) -- End

#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSearch_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objEmpBank As New clsEmployeeBanks
        Dim objLoan_Advance As New clsLoan_Advance
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        objPaymentTran = New clsPayment_tran
        objStatusTran = New clsLoan_Status_tran
        Try

            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'If dgvLoanList.Items.Count <= 0 Then
            If mdtList Is Nothing OrElse mdtList.Rows.Count <= 0 Then
                'Sohail (31 Dec 2019) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), Me)
                Exit Sub
            End If

            Dim dTemp As DataRow() = mdtList.Select("IsChecked=True")
            If dTemp.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), Me)
                dgvLoanList.Focus()
                Exit Sub
            End If

            If CInt(cboChangeStatus.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please select change status. Change status is mandatory information."), Me)
                cboChangeStatus.Focus()
                Exit Sub
            End If

            Select Case CInt(cboChangeStatus.SelectedValue)

                Case enLoanStatus.IN_PROGRESS, enLoanStatus.ON_HOLD

                    mblnIsWrittenOff = False
                    If IsValidate() = False Then Exit Sub

                    For i As Integer = 0 To dTemp.Length - 1

                        If CBool(dTemp(i)("IsError")) = False Then
                            Call SetValue(dTemp(i))

                            With objStatusTran
                                ._FormName = mstrModuleName
                                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With

                            blnFlag = objStatusTran.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                           mdtPeriodStartDate, _
                                                           mdtPeriodEndDate, _
                                                           CStr(Session("UserAccessModeSetting")), _
                                                           True, , True)

                            If blnFlag = False And objPaymentTran._Message <> "" Then
                                DisplayMessage.DisplayMessage(objStatusTran._Message, Me)
                                Exit Sub
                            ElseIf blnFlag = True Then
                                objLoan_Advance._Loanadvancetranunkid = CInt(dTemp(i)("loanadvancetranunkid"))
                                objLoan_Advance._LoanStatus = CInt(cboChangeStatus.SelectedValue)
                                With objLoan_Advance
                                    ._FormName = mstrModuleName
                                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                        ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                                    Else
                                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                    End If
                                    ._ClientIP = Session("IP_ADD").ToString()
                                    ._HostName = Session("HOST_NAME").ToString()
                                    ._FromWeb = True
                                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                                End With
                                If objLoan_Advance.Update() = False Then
                                    DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next

                Case enLoanStatus.WRITTEN_OFF

                    mblnIsWrittenOff = True
                    If IsValidate() = False Then Exit Sub

                    For i As Integer = 0 To dTemp.Length - 1

                        If CBool(dTemp(i)("IsError")) = False Then
                            Call SetValue(dTemp(i))
                            'Sohail (23 May 2017) -- Start
                            'Enhancement - 67.1 - Link budget with Payroll.
                            'blnFlag = objPaymentTran.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), _
                            '                                CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                            '                                mdtPeriodStartDate, mdtPeriodEndDate, _
                            '                                CStr(Session("UserAccessModeSetting")), True, _
                            '                                CBool(Session("IsIncludeInactiveEmp")), _
                            '                                CInt(Session("PaymentVocNoType")), CStr(Session("PaymentVocPrefix")), _
                            '                                ConfigParameter._Object._CurrentDateAndTime, True, "")

                            With objPaymentTran
                                ._FormName = mstrModuleName
                                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._Companyunkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            blnFlag = objPaymentTran.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                            mdtPeriodStartDate, mdtPeriodEndDate, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            CBool(Session("IsIncludeInactiveEmp")), _
                                                            CInt(Session("PaymentVocNoType")), CStr(Session("PaymentVocPrefix")), _
                                                            ConfigParameter._Object._CurrentDateAndTime, True, "", Nothing)
                            'Sohail (23 May 2017) -- End

                            If blnFlag = False And objPaymentTran._Message <> "" Then
                                DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
                                Exit Sub
                            End If
                        End If
                    Next
            End Select

            If mdtExcel.Rows.Count > 0 Then
                DisplayMessage.DisplayMessage("There are some operations that need to be done before change status. Please click on Save button and refer the excel for more detailed log.", Me)
                'Dim dsExcel As New DataSet
                'dsExcel.Tables.Add(mdtExcel)

                'S.SANDEEP [12-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 0001843
                'Dim IExcel As New ExcelData
                'Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Language._Object.getCaption(mstrModuleName, lblPageHeader.Text).Replace(" ", "_") & "_" & Now.ToString("yyyyMMddhhmmss") & ".xls"
                'IExcel.Export(strFilePath, mdtExcel)

                Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Language._Object.getCaption(mstrModuleName, lblPageHeader.Text).Replace(" ", "_") & "_" & Now.ToString("yyyyMMddhhmmss") & ".xlsx"
                'Sohail (11 Feb 2020) -- Start
                'NMB Enhancement # : not able to generate data in exported file.
                'Aruti.Data.modGlobal.OpenXML_Export(strFilePath, mdtExcel)
                Dim ds As New DataSet
                ds.Tables.Clear()
                ds.Tables.Add(mdtExcel.Copy)
                Aruti.Data.modGlobal.OpenXML_Export(strFilePath, ds)
                'Sohail (11 Feb 2020) -- End
                'S.SANDEEP [12-Jan-2018] -- END

                Session("ExFileName") = strFilePath
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
            Else
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Process completed successfully."), Me)
                End If
            End If

            txtRemarks.Text = ""
            FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            If mdtExcel IsNot Nothing AndAlso mdtExcel.Rows.Count > 0 Then mdtExcel.Rows.Clear()
            objPaymentTran = Nothing
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString.Replace("ADF", "hremployee_master")
            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
    '    Dim objEmpBank As New clsEmployeeBanks
    '    Dim objLoan_Advance As New clsLoan_Advance
    '    Dim dsList As New DataSet
    '    Dim strLoanAdvanceTranIDs As String = String.Empty
    '    Try
    '        mblnIsFromApplyButton = True

    '        If dgvLoanList.Items.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), Me)
    '            Exit Sub
    '        End If

    '        If CInt(cboChangeStatus.SelectedValue) <= 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please select change status. Change status is mandatory information."), Me)
    '            cboChangeStatus.Focus()
    '            Exit Sub
    '        End If

    '        Select Case CInt(cboChangeStatus.SelectedValue)

    '            Case enLoanStatus.IN_PROGRESS, enLoanStatus.ON_HOLD

    '                If mdtList.Rows.Count > 0 Then
    '                    strLoanAdvanceTranIDs = String.Join(",", (From p In mdtList Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("loanadvancetranunkid").ToString)).ToArray())
    '                End If

    '                Dim dTemp As DataRow()
    '                dTemp = mdtList.Select("IsChecked=True")
    '                If dTemp.Length <= 0 Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please tick atleast one record for further process."), Me)
    '                    dgvLoanList.Focus()
    '                    Exit Sub
    '                End If

    '                'For Each dRow As DataRow In dTemp
    '                '    mintLoanAdvanceUnkid = CInt(dRow("loanadvancetranunkid"))
    '                '    dRow("changestatusunkid") = CInt(cboChangeStatus.SelectedValue)
    '                '    Select Case CInt(dRow("changestatusunkid"))
    '                '        Case 0 'Select
    '                '            dRow("ChangedStatus") = ""
    '                '        Case enLoanStatus.IN_PROGRESS
    '                '            dRow("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "In Progress")
    '                '        Case enLoanStatus.ON_HOLD
    '                '            dRow("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "On Hold")
    '                '        Case enLoanStatus.WRITTEN_OFF
    '                '            dRow("ChangedStatus") = Language.getMessage(mstrModuleName, 33, "Written Off")
    '                '    End Select

    '                '    dRow.AcceptChanges()
    '                'Next
    '                'dgvLoanList.DataSource = mdtList

    '            Case enLoanStatus.WRITTEN_OFF

    '                If radValue.Checked = False AndAlso radPercentage.Checked = False Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Payment By is compulsory information. Please select Payment By to continue."), Me)
    '                    lblPaymentBy.Focus()
    '                    Exit Sub
    '                End If

    '                If radPercentage.Checked = True Then
    '                    If CDec(txtPercentage.Text) < 0.0 AndAlso CDec(txtPercentage.Text) > 100.0 Then
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Invalid Percentage. Must be in between 0 To 100."), Me)
    '                        txtPercentage.Focus()
    '                        Exit Sub
    '                    End If
    '                End If

    '                Call Calculate_LoanBalanceInfo()
    '        End Select

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("btnApply_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region " Radio Button's Events "

    Protected Sub radValue_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radValue.CheckedChanged
        Try
            If radValue.Checked = True Then
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Search option on Global Loan Change Status screen.
                'If dgvLoanList.Items.Count <= 0 Then
                If mdtList Is Nothing OrElse mdtList.Rows.Count <= 0 Then
                    'Sohail (31 Dec 2019) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), Me)
                    radValue.Checked = False
                    Exit Sub
                End If
                dtpPaymentDate.SetDate = mdtPeriodStartDate
                txtPercentage.Text = "0"
                txtPercentage.Enabled = False
                'mblnIsFromApplyButton = False
                'chkSelectAll.Checked = False
                Call Calculate_LoanBalanceInfo()
            Else
                txtPercentage.Enabled = True
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radValue_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("radValue_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub radPercentage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPercentage.CheckedChanged
        Try
            If radPercentage.Checked = True Then
                'Sohail (31 Dec 2019) -- Start
                'NMB Enhancement # : Search option on Global Loan Change Status screen.
                'If dgvLoanList.Items.Count <= 0 Then
                If mdtList Is Nothing OrElse mdtList.Rows.Count <= 0 Then
                    'Sohail (31 Dec 2019) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "There is no record found for further process."), Me)
                    radPercentage.Checked = False
                    Exit Sub
                End If
                dtpPaymentDate.SetDate = mdtPeriodStartDate
                txtPercentage.Text = "0"
                txtPercentage.Enabled = True
                'mblnIsFromApplyButton = False
                'chkSelectAll.Checked = False
                Call Calculate_LoanBalanceInfo()

            Else
                txtPercentage.Enabled = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radPercentage_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("radPercentage_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Link Button's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkAllocation_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblAssignedPeriod.Text = Language._Object.getCaption(Me.lblAssignedPeriod.ID, Me.lblAssignedPeriod.Text)
            Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.ID, Me.lblDeductionPeriod.Text)
            Me.lblCalcType.Text = Language._Object.getCaption(Me.lblCalcType.ID, Me.lblCalcType.Text)
            Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.ID, Me.lblInterestCalcType.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.ID, Me.lblMode.Text)
            Me.lblPaymentBy.Text = Language._Object.getCaption(Me.lblPaymentBy.ID, Me.lblPaymentBy.Text)
            Me.radValue.Text = Language._Object.getCaption(Me.radValue.ID, Me.radValue.Text)
            Me.radPercentage.Text = Language._Object.getCaption(Me.radPercentage.ID, Me.radPercentage.Text)

            Me.lblPaymentMode.Text = Language._Object.getCaption(Me.lblPaymentMode.ID, Me.lblPaymentMode.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.ID, Me.lblBankGroup.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.ID, Me.lblAccountNo.Text)
            Me.lblChequeNo.Text = Language._Object.getCaption(Me.lblChequeNo.ID, Me.lblChequeNo.Text)
            Me.lblChangeStatus.Text = Language._Object.getCaption(Me.lblChangeStatus.ID, Me.lblChangeStatus.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.ID, Me.lblVoucherNo.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.ID, Me.lblPaymentDate.Text)
            Me.lblPercentage.Text = Language._Object.getCaption(Me.lblPercentage.ID, Me.lblPercentage.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)

            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvLoanList.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(2).FooterText, Me.dgvLoanList.Columns(2).HeaderText)
            Me.dgvLoanList.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(3).FooterText, Me.dgvLoanList.Columns(3).HeaderText)
            Me.dgvLoanList.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(4).FooterText, Me.dgvLoanList.Columns(4).HeaderText)
            Me.dgvLoanList.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(5).FooterText, Me.dgvLoanList.Columns(5).HeaderText)
            Me.dgvLoanList.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(6).FooterText, Me.dgvLoanList.Columns(6).HeaderText)
            Me.dgvLoanList.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(7).FooterText, Me.dgvLoanList.Columns(7).HeaderText)
            Me.dgvLoanList.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(8).FooterText, Me.dgvLoanList.Columns(8).HeaderText)
            Me.dgvLoanList.Columns(9).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(9).FooterText, Me.dgvLoanList.Columns(9).HeaderText)
            Me.dgvLoanList.Columns(10).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(10).FooterText, Me.dgvLoanList.Columns(10).HeaderText)
            Me.dgvLoanList.Columns(11).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(11).FooterText, Me.dgvLoanList.Columns(11).HeaderText)
            Me.dgvLoanList.Columns(12).HeaderText = Language._Object.getCaption(Me.dgvLoanList.Columns(12).FooterText, Me.dgvLoanList.Columns(12).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
