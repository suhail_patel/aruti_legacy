﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanStatus_AddEdit.aspx.vb"
    Inherits="Loan_Savings_New_Loan_wPg_LoanStatus_AddEdit" Title="Change Loan Status" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 70%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Change Loan Status"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Loan Status Information"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 18%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 22%">
                                                <asp:CheckBox ID="chkCalInterest" runat="server" Style="margin-left: -4px" Text="Calculate Interest" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 18%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:TextBox ID="txtLoanScheme" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblSettlementAmt" runat="server" Style="margin-left: 10px" Text="Settlement Amount"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtSettlementAmt" runat="server" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0" Style="text-align: right" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 18%">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:TextBox ID="txtEmployeeName" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBalance" runat="server" Style="margin-left: 10px" Text="Balance"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtBalance" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 18%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Loan Status"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 42%" colspan="2">
                                                <asp:LinkButton ID="lnkAddSettlement" runat="server" Enabled="false" Text="Add Settlement"
                                                    Style="margin-left: 10px; font-size: 12px;"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 18%" valign="top">
                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                            </td>
                                            <td style="width: 82%" colspan="3">
                                                <asp:TextBox ID="txtRemarks" runat="server" AutoPostBack="true" TextMode="MultiLine"
                                                    Rows="3"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; margin-top: 10px; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div style="vertical-align: top; overflow: auto; max-height: 400px">
                                                    <asp:DataGrid ID="dgvLoanAdvanceStatushistory" runat="server" Style="margin: auto"
                                                        AutoGenerateColumns="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                        Width="100%">
                                                        <ItemStyle CssClass="griviewitem" />
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderStyle HorizontalAlign="Center" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" 
                                                                        CssClass="griddelete" ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="username" FooterText="dgcolhUser" 
                                                                HeaderStyle-Width="40%" HeaderText="User" ItemStyle-Width="40%">
                                                                <HeaderStyle Width="40%" />
                                                                <ItemStyle Width="40%" />
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="status_date" FooterText="dgcolhTransactionDate" 
                                                                HeaderText="Transaction Date"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="status" FooterText="dgcolhStatus" 
                                                                HeaderText="Status"></asp:BoundColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="20%" HeaderText="Calculate Interest" 
                                                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkdgcolhCalculateInterest" runat="server" 
                                                                        Checked='<%# Eval("iscalculateinterest") %>' Enabled="False" />
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="20%" />
                                                                <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="grp" FooterText="objdgcolhIsGroup" 
                                                                HeaderText="Is Group" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="loanadvancetranunkid" 
                                                                FooterText="objLoanAdvancestatustranunkid" Visible="false" />
                                                            <asp:BoundColumn DataField="statusid" FooterText="objdgcolhPeriodStatusId" 
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="periodunkid" FooterText="objcolhPeriodunkid" 
                                                                Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="end_date" FooterText="objcolhEndDate" 
                                                                Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="start_date" FooterText="objcolhStartDate" 
                                                                Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="loanstatustranunkid" 
                                                                FooterText="objcolhLoanStatusTranunkid" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="iscalculateinterest" 
                                                                FooterText="dgcolhCalculateInterest" Visible="false"></asp:BoundColumn>
                                                        </Columns>
                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:ConfirmYesNo ID="popupConfirmYesNo" runat="server" />
                    <ucDel:DeleteReason ID="popupDeleteReason" runat="server" />
                    <asp:HiddenField ID="hd1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
