﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Loan_Savings_New_Loan_wPg_LoanStatus_AddEdit
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmLoanStatus_AddEdit"
    Private DisplayMessage As New CommonCodes
    Private objLoan_Advance As New clsLoan_Advance
    Private objEmployee As New clsEmployee_Master
    Private objLoanScheme As New clsLoan_Scheme
    Private objStatusTran As New clsLoan_Status_tran
    Private mintLoanAdvanceTranunkid As Integer = -1
    Private mstrStatusData As String = String.Empty
    Private mintStatus As Integer = -1
    Private mblnIsFromLoan As String = ""
    Private mintItemIndex As Integer = -1
    'Nilay (25-Mar-2016) -- Start
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (25-Mar-2016) -- End

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetLanguage()
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If Request.QueryString.Count > 0 Then
                '    Dim array() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                '    If array.Length > 0 Then
                '        mintLoanAdvanceTranunkid = CInt(array(0))
                '        mblnIsFromLoan = CStr(CBool(array(1)))
                '        mintStatus = CInt(array(2))
                '    End If
                'End If
                If Session("LoanAdvanceTranunkid") IsNot Nothing Then
                    mintLoanAdvanceTranunkid = CInt(Session("LoanAdvanceTranunkid"))
                    Session("LoanAdvanceTranunkid") = Nothing
                End If
                If Session("IsFromLoan") IsNot Nothing Then
                    mblnIsFromLoan = CStr(Session("IsFromLoan"))
                    Session("IsFromLoan") = Nothing
                    End If
                If Session("StatusId") IsNot Nothing Then
                    mintStatus = CInt(Session("StatusId"))
                    Session("StatusId") = Nothing
                End If
                'Nilay (06-Aug-2016) -- END

               

                lnkAddSettlement.Enabled = False
                objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLoan_Advance._Employeeunkid
                objLoanScheme._Loanschemeunkid = objLoan_Advance._Loanschemeunkid

                Call FillCombo()

                Call Fill_Info()

                Call FillStatusHistory()

                cboPeriod.Focus()

                Call cboStatus_SelectedIndexChanged(Nothing, Nothing)

                If dgvLoanAdvanceStatushistory.Items.Count <= 0 Then
                    dgvLoanAdvanceStatushistory.DataSource = New List(Of String)
                    dgvLoanAdvanceStatushistory.DataBind()
                End If

                If Session("LoanStatusData") IsNot Nothing Then
                    Session("LoanStatusData") = Nothing
                End If

            Else
                mintLoanAdvanceTranunkid = CInt(Me.ViewState("LoanAdvanceunkid"))
                mintStatus = CInt(Me.ViewState("Status"))
                mblnIsFromLoan = CStr(Me.ViewState("IsFromLoan"))
                mstrStatusData = CStr(Me.ViewState("StatusData"))
                mintItemIndex = CInt(Me.ViewState("ItemIndex"))
                'Nilay (25-Mar-2016) -- Start
                mdtStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtEndDate = CDate(Me.ViewState("PeriodEndDate"))
                'Nilay (25-Mar-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("LoanAdvanceunkid") = mintLoanAdvanceTranunkid
            Me.ViewState("Status") = mintStatus
            Me.ViewState("IsFromLoan") = mblnIsFromLoan
            Me.ViewState("StatusData") = mstrStatusData
            Me.ViewState("ItemIndex") = mintItemIndex
            'Nilay (25-Mar-2016) -- Start
            Me.ViewState("PeriodStartDate") = mdtStartDate
            Me.ViewState("PeriodEndDate") = mdtEndDate
            'Nilay (25-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreInit:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Try
            Dim dsList As New DataSet
            Dim objMaster As New clsMasterData

            'If CBool(mblnIsFromLoan) = True Then
            dsList = objMaster.GetLoan_Saving_Status("Status")
            If objLoan_Advance._Balance_Amount > 0 Then
                dsList.Tables(0).Rows.RemoveAt(4)
            End If
            'End If
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
                .SelectedValue = "0"
            End With

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                               CInt(Session("Fin_year")), _
                                               CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")), "Period", True, 1)

            Dim dtTable As New DataTable
            Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            If intFirstPeriodId > 0 Then
                dtTable = New DataView(dsList.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
            End If

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = intFirstPeriodId.ToString
            End With
            objPeriod = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_Info()

        Try
            txtEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtLoanScheme.Text = objLoanScheme._Name
            cboStatus.SelectedValue = CStr(objLoan_Advance._LoanStatus)
            'txtBalance.Text = Format(objLoan_Advance._Balance_Amount, Session("fmtCurrency"))

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Nilay (25-Mar-2016) -- Start
            'Dim dsTable As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo("List", objPeriod._Start_Date.Date, objLoan_Advance._Employeeunkid.ToString, mintLoanAdvanceTranunkid, objPeriod._Start_Date, False, True)
            Dim dsTable As DataSet = objLoan_Advance.Calculate_LoanBalanceInfo(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                                               CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, _
                                                                               CStr(Session("UserAccessModeSetting")), True, "List", _
                                                                               objPeriod._Start_Date, objLoan_Advance._Employeeunkid.ToString, _
                                                                               mintLoanAdvanceTranunkid, objPeriod._Start_Date, False, True)
            'Nilay (25-Mar-2016) -- End

            If dsTable.Tables("List").Rows.Count > 0 Then
                txtBalance.Text = Format(dsTable.Tables(0).Rows(0)("LastProjectedBalance"), Session("fmtCurrency").ToString)
            End If
            objPeriod = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Info:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Info:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()

        Try
            'Nilay (25-Mar-2016) -- Start
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            mdtStartDate = objPeriod._Start_Date
            mdtEndDate = objPeriod._End_Date
            'Nilay (25-Mar-2016) -- End
            objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
            objStatusTran._Loanadvancetranunkid = objLoan_Advance._Loanadvancetranunkid
            objStatusTran._Isvoid = False
            objStatusTran._Remark = txtRemarks.Text
            If txtSettlementAmt.Text.Trim <> "" Then
                objStatusTran._Settle_Amount = CDec(txtSettlementAmt.Text)
            Else
                objStatusTran._Settle_Amount = 0
            End If

            objStatusTran._Userunkid = CInt(Session("UserId"))
            objStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
            'Nilay (05-May-2016) -- Start
            'objStatusTran._Staus_Date = DateAndTime.Now.Date
            objStatusTran._Staus_Date = ConfigParameter._Object._CurrentDateAndTime
            'Nilay (05-May-2016) -- End
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voiduserunkid = -1
            objStatusTran._Statusunkid = CInt(cboStatus.SelectedValue)
            objStatusTran._IsCalculateInterest = chkCalInterest.Checked

            mstrStatusData = objStatusTran._Isvoid & "|" & _
                             objStatusTran._Loanadvancetranunkid & "|" & _
                             objStatusTran._Remark & "|" & _
                             objStatusTran._Settle_Amount & "|" & _
                             objStatusTran._Staus_Date & "|" & _
                             objStatusTran._Voiddatetime & "|" & _
                             objStatusTran._Voiduserunkid & "|" & _
                             objStatusTran._Statusunkid & "|" & _
                             objStatusTran._Periodunkid & "|"

            'Nilay (05-May-2016) -- Start
            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objStatusTran._WebClientIP = Session("IP_ADD").ToString
            'objStatusTran._WebFormName = "frmLoanStatus_AddEdit"
            'objStatusTran._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Period is compulsory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Status is compulsory information. Please select Status to continue."), Me)
                cboStatus.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) = mintStatus Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, This status is already set. Please select different status."), Me)
                cboStatus.Focus()
                Return False
            End If

            'Dim dsList As New DataSet
            'dsList = objStatusTran.GetPeriodWiseLoanStatusHistory("List", True, "loanadvancetranunkid = " & mintLoanAdvanceTranunkid)

            Dim objPeriod As New clscommom_period_Tran
            Dim dsListPeriod As DataSet = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True, 1)

            Dim dtTable As New DataTable
            dtTable = New DataView(dsListPeriod.Tables("Period"), "periodunkid = " & cboPeriod.SelectedValue & " ", "", DataViewRowState.CurrentRows).ToTable

            If dgvLoanAdvanceStatushistory IsNot Nothing AndAlso dgvLoanAdvanceStatushistory.Items.Count > 0 Then
                If eZeeDate.convertDate(dtTable.Rows(0)("start_date").ToString).Date < CDate(dgvLoanAdvanceStatushistory.Items(0).Cells(10).Text).Date Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Loan/Advance status period should be greater than or equal to the Last Loan/Advance status period. Please set Loan/Advance status period greater than or equal to the the Last Loan/Advance status period."), Me)
                    cboPeriod.Focus()
                    Exit Function
                End If
            End If

            Select Case CInt(cboStatus.SelectedValue)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'Case 3, 4
                Case enLoanStatus.WRITTEN_OFF, enLoanStatus.COMPLETED
                    'Nilay (20-Sept-2016) -- End
                    If txtRemarks.Text.Trim = "" Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Remark cannot be blank. Remark is compulsory information."), Me)
                        txtRemarks.Focus()
                        Return False
                    End If
                    lnkAddSettlement.Enabled = False
            End Select

            Dim objTnaLeaveTran As New clsTnALeaveTran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), objLoan_Advance._Employeeunkid.ToString, objPeriod._End_Date) Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot change loan status for this employee loan. Reason:Process Payroll is already done for this employee for last date of selected period."), Me)
                objTnaLeaveTran = Nothing
                objPeriod = Nothing
                Return False
            End If

            objTnaLeaveTran = Nothing

            Dim ds As DataSet = objLoan_Advance.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
            Dim strMsg As String = ""
            If ds.Tables("List").Rows.Count > 0 Then
                If ds.Tables("List").Rows.Count > 0 AndAlso ds.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(objPeriod._Start_Date.Date) Then
                    If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 13, " Process Payroll.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                        strMsg = Language.getMessage(mstrModuleName, 14, " Loan Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                        strMsg = Language.getMessage(mstrModuleName, 15, " Receipt Payment List.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 16, " Loan Installment Change.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 17, " Loan Topup Added.")
                    ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                        strMsg = Language.getMessage(mstrModuleName, 18, " Loan Rate Change.")
                    End If
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, You cannot change loan status. Please delete last transaction from the screen of") & strMsg, Me)
                    objPeriod = Nothing
                    Return False
                End If
            End If
            objPeriod = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError("IsValidate:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub FillStatusHistory()
        Try
            Dim dsList As New DataSet
            dsList = objStatusTran.GetPeriodWiseLoanStatusHistory("List", True, "loanadvancetranunkid = " & mintLoanAdvanceTranunkid)
            If dsList.Tables(0).Rows.Count > 1 Then
                mintStatus = CInt(dsList.Tables(0).Rows(1).Item("statusunkid"))
            End If
            dgvLoanAdvanceStatushistory.AutoGenerateColumns = False
            dgvLoanAdvanceStatushistory.DataSource = dsList.Tables("List")
            dgvLoanAdvanceStatushistory.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillStatusHistory:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillStatusHistory:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If IsValidate() = False Then Exit Sub

            Select Case CInt(cboStatus.SelectedValue)
                'Nilay (20-Sept-2016) -- Start
                'Enhancement : Cancel feature for approved but not assigned loan application
                'Case 3, 4
                Case enLoanStatus.WRITTEN_OFF, enLoanStatus.COMPLETED
                    'Nilay (20-Sept-2016) -- End
                    Dim objPayment As New clsPayment_tran

                    Dim ePRef As clsPayment_tran.enPaymentRefId
                    If CBool(mblnIsFromLoan) = True Then
                        ePRef = clsPayment_tran.enPaymentRefId.LOAN
                    Else
                        ePRef = clsPayment_tran.enPaymentRefId.ADVANCE
                    End If

                    If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, mintLoanAdvanceTranunkid, ePRef) Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot set the selected status for this transaction. Reason : Payment is not made to employee for this transaction."), Me)
                        Exit Sub
                    End If
                    objPayment = Nothing

                    If CDec(txtSettlementAmt.Text) = 0 Then
                        Language.setLanguage(mstrModuleName)
                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'If CInt(cboStatus.SelectedValue) = 3 Then
                        If CInt(cboStatus.SelectedValue) = enLoanStatus.WRITTEN_OFF Then
                            'Nilay (20-Sept-2016) -- End
                            popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 2, "You are about to ") & cboStatus.SelectedItem.Text & _
                                                        Language.getMessage(mstrModuleName, 3, " selected transaction. Do you want to settle it?")
                            popupConfirmYesNo.Show()
                            Exit Sub
                        ElseIf CInt(cboStatus.SelectedValue) = 4 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot set the selected status for this transaction. Reason : Loan/Advance is not settled."), Me)
                            Exit Sub
                        End If
                    End If
            End Select

            Call popupConfirmYesNo_buttonNo_Click(Nothing, Nothing)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonNo_Click
        Try
            Dim blnFlag As Boolean = False
            lnkAddSettlement.Enabled = False

            Call SetValue()

            'Nilay (15-Dec-2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            'blnFlag = objStatusTran.Insert(Session("Database_Name"), Session("Fin_year"))

            'Nilay (25-Mar-2016) -- Start
            'blnFlag = objStatusTran.Insert(Session("Database_Name").ToString, CInt(Session("Fin_year")), , True)

            With objStatusTran
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With
            blnFlag = objStatusTran.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                           mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, , True)
            'Nilay (25-Mar-2016) -- End

            'Nilay (15-Dec-2015) -- End

            If blnFlag = True Then
                objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                objLoan_Advance._LoanStatus = CInt(cboStatus.SelectedValue)
                'Nilay (05-May-2016) -- Start

                'Blank_ModuleName()
                'StrModuleName2 = "mnuLoan_Advance_Savings"
                'objLoan_Advance._WebClientIP = Session("IP_ADD").ToString
                'objLoan_Advance._WebFormName = "frmLoanStatus_AddEdit"
                'objLoan_Advance._WebHostName = Session("HOST_NAME").ToString
                'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                '    objLoan_Advance._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                'End If
                'Nilay (05-May-2016) -- End

                With objLoan_Advance
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With

                objLoan_Advance.Update()

                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupConfirmYesNo_buttonNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupConfirmYesNo_buttonNo_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Try
            lnkAddSettlement.Enabled = True
            lnkAddSettlement.Focus()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupConfirmYesNo_buttonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupConfirmYesNo_buttonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Try
            Dim ds As DataSet = objLoan_Advance.GetLastLoanBalance("List", mintLoanAdvanceTranunkid)
            Dim strMsg As String = String.Empty
            If ds.Tables("List").Rows.Count > 0 AndAlso ds.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(CDate(dgvLoanAdvanceStatushistory.Items(mintItemIndex).Cells(9).Text).Date) Then
                If CInt(ds.Tables("List").Rows(0).Item("payrollprocesstranunkid")) > 0 Then
                    strMsg = Language.getMessage(mstrModuleName, 13, " Process Payroll.")
                ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = False Then
                    strMsg = Language.getMessage(mstrModuleName, 14, " Loan Payment List.")
                ElseIf CInt(ds.Tables("List").Rows(0).Item("paymenttranunkid")) > 0 AndAlso CBool(ds.Tables("List").Rows(0).Item("isreceipt")) = True Then
                    strMsg = Language.getMessage(mstrModuleName, 15, " Receipt Payment List.")
                ElseIf CInt(ds.Tables("List").Rows(0).Item("lnemitranunkid")) > 0 Then
                    strMsg = Language.getMessage(mstrModuleName, 16, " Loan Installment Change.")
                ElseIf CInt(ds.Tables("List").Rows(0).Item("lntopuptranunkid")) > 0 Then
                    strMsg = Language.getMessage(mstrModuleName, 17, " Loan Topup Added.")
                ElseIf CInt(ds.Tables("List").Rows(0).Item("lninteresttranunkid")) > 0 Then
                    strMsg = Language.getMessage(mstrModuleName, 18, " Loan Rate Change.")
                End If
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot delete this transaction. Please delete last transaction from the screen of") & strMsg, Me)
                Exit Sub
            End If

            objStatusTran._Isvoid = True
            objStatusTran._Voidreason = popupDeleteReason.Reason
            objStatusTran._Voiduserunkid = CInt(Session("UserId"))
            'Nilay (05-May-2016) -- Start
            'objStatusTran._Voiddatetime = DateAndTime.Now.Date
            objStatusTran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objStatusTran._WebClientIP = Session("IP_ADD").ToString
            'objStatusTran._WebFormName = "frmLoanStatus_AddEdit"
            'objStatusTran._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End

            With objStatusTran
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            objStatusTran.DeleteByLoanStatusUnkId(CInt(dgvLoanAdvanceStatushistory.Items(mintItemIndex).Cells(11).Text))

            If objStatusTran._Message <> "" Then
                DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
            Else
                Call FillStatusHistory()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgvLoanAdvanceStatushistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvLoanAdvanceStatushistory.ItemCommand
        Try
            If e.CommandName.ToUpper = "DELETE" Then

                mintItemIndex = e.Item.ItemIndex

                If CInt(e.Item.Cells(7).Text) = 0 Then Exit Sub
                If CInt(e.Item.Cells(7).Text) = 1 AndAlso e.Item.ItemIndex > 1 Then Exit Sub

                objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceTranunkid
                If CDate(objLoan_Advance._Effective_Date) = CDate(e.Item.Cells(2).Text) Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot delete this loan status. Reason : This is system generated  loan status."), Me)
                    Exit Sub
                End If

                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(e.Item.Cells(8).Text), objLoan_Advance._Employeeunkid.ToString, CDate(e.Item.Cells(9).Text)) Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot delete this loan status for this employee loan. Reason:Process payroll is already done for this employee for last date of selected period."), Me)
                    objTnaLeaveTran = Nothing
                    Exit Sub
                End If
                objTnaLeaveTran = Nothing
                popupDeleteReason.Reason = ""
                popupDeleteReason.Title = Language.getMessage(mstrModuleName, 20, "Are you sure you want to delete this Loan Status?")

                popupDeleteReason.Show()

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanAdvanceStatushistory_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvLoanAdvanceStatushistory_ItemCommand:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvLoanAdvanceStatushistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLoanAdvanceStatushistory.ItemDataBound
        Dim objLnAdv As New clsLoan_Advance 'Nilay (01-Apr-2016)
        Try
            If e.Item.ItemIndex >= 0 Then
                If CBool(e.Item.Cells(5).Text) = True Then
                    'e.Item.Cells(0).Visible = False
                    CType(e.Item.Cells(0).FindControl("ImgDelete"), LinkButton).Visible = False
                    e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1

                    For i = 2 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(0).Style.Add("text-align", "left")
                    e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(1).Style.Add("text-align", "left")
                End If

                objLnAdv._Loanadvancetranunkid = CInt(e.Item.Cells(6).Text) 'Nilay (01-Apr-2016)

                If e.Item.Cells(2).Text IsNot Nothing AndAlso e.Item.Cells(2).Text <> "&nbsp;" Then
                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    'e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text.ToString).ToString
                    If CDate(e.Item.Cells(2).Text).Date = CDate(objLnAdv._Effective_Date).Date Then
                        e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                    Else
                        e.Item.Cells(2).Text = e.Item.Cells(2).Text
                    End If
                    'Nilay (01-Apr-2016) -- End
                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanAdvanceStatushistory_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvLoanAdvanceStatushistory_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Events"

    Protected Sub txtRemarks_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRemarks.TextChanged
        Try
            If txtRemarks.Text.Trim = "" Then
                lnkAddSettlement.Enabled = False
            Else
                Select Case (cboStatus.SelectedValue)
                    Case CStr(0), CStr(1), CStr(2)
                        lnkAddSettlement.Enabled = False
                    Case CStr(3), CStr(4)
                        lnkAddSettlement.Enabled = True
                End Select
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtRemarks_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtRemarks_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkAddSettlement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAddSettlement.Click

        Dim strURL As String = String.Empty
        Dim mintReferenceId As Integer = -1
        Try
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'If CBool(mblnIsFromLoan) = True Then
            '    mintReferenceId = clsPayment_tran.enPaymentRefId.LOAN
            'Else
            '    mintReferenceId = clsPayment_tran.enPaymentRefId.ADVANCE
            'End If
            If CBool(mblnIsFromLoan) = True Then
                Session("ReferenceId") = clsPayment_tran.enPaymentRefId.LOAN
            Else
                Session("ReferenceId") = clsPayment_tran.enPaymentRefId.ADVANCE
            End If
            'Nilay (06-Aug-2016) -- END

            Call SetValue()

            Session("LoanStatusData") = mstrStatusData

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'strURL = "-1" & "|" & mintReferenceId & "|" & mintLoanAdvanceTranunkid & "|" & clsPayment_tran.enPayTypeId.RECEIVED & "|" & _
            '           objPeriod._Start_Date.ToString & "|" & "0" & "|" & "True"
            'Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Add_Edit_Payment.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(strURL)), False)

            Session("TransactionId") = mintLoanAdvanceTranunkid
            Session("PaymentTypeId") = clsPayment_tran.enPayTypeId.RECEIVED
            Session("LoanSavingEffectiveDate") = objPeriod._Start_Date.ToString
            Session("IsFromStatus") = True

            Response.Redirect(Session("rootpath").ToString & "Payment/wPg_Add_Edit_Payment.aspx", False)
            'Nilay (06-Aug-2016) -- END



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAddSettlement_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkAddSettlement_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Events"
    Protected Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = enLoanStatus.ON_HOLD Then
                chkCalInterest.Enabled = True
                lnkAddSettlement.Enabled = False
                chkCalInterest.Checked = False
            ElseIf CInt(cboStatus.SelectedValue) = enLoanStatus.IN_PROGRESS Then
                chkCalInterest.Enabled = False
                lnkAddSettlement.Enabled = False
                chkCalInterest.Checked = False
            Else
                If txtRemarks.Text.Trim.Length > 0 Then lnkAddSettlement.Enabled = True
                chkCalInterest.Enabled = False
                chkCalInterest.Checked = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStatus_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboStatus_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbLoanStatusInfo", Me.lblDetialHeader.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lblSettlementAmt.Text = Language._Object.getCaption(Me.lblSettlementAmt.ID, Me.lblSettlementAmt.Text)
            Me.lblBalance.Text = Language._Object.getCaption(Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lnkAddSettlement.Text = Language._Object.getCaption(Me.lnkAddSettlement.ID, Me.lnkAddSettlement.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)

            Me.dgvLoanAdvanceStatushistory.Columns(0).HeaderText = Language._Object.getCaption(Me.dgvLoanAdvanceStatushistory.Columns(0).FooterText, Me.dgvLoanAdvanceStatushistory.Columns(0).HeaderText)
            Me.dgvLoanAdvanceStatushistory.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvLoanAdvanceStatushistory.Columns(1).FooterText, Me.dgvLoanAdvanceStatushistory.Columns(1).HeaderText)
            Me.dgvLoanAdvanceStatushistory.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvLoanAdvanceStatushistory.Columns(2).FooterText, Me.dgvLoanAdvanceStatushistory.Columns(2).HeaderText)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>

End Class
