﻿<%@ Page Title="Global Change Status" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPg_GlobalChangeStatus.aspx.vb" Inherits="Loan_Savings_New_Loan_Loan_Status_wPg_GlobalChangeStatus" %>

        
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-ui.js"></script>
    
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {

            $(".percentage").keyup(function(e) {
                if (($(this).val() < 0 || $(this).val() > 100)) {
                    $(this).val(0);
                    return false;
                }
            });
            $(".percentage").keypress(function(e) {
                if (($(this).val().indexOf('.') != -1) && $(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2) {
                    return false;
                }
                if (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Global Change Status"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <%--<div id="FilterCriteriaTitle" class="panel-heading-default">
									<div style="float: left;">
										<asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
									</div>
								</div>--%>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div id="divFilter" class="panel-default" style="margin: 0px">
                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                        </div>
                                        <div style="float: right">
                                            <asp:LinkButton ID="lnkAllocation" Style="font-weight: bold; font-size: 12px; margin-right: 5px"
                                                Font-Underline="false" runat="server" Text="Allocation"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div id="Div2" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblAssignedPeriod" runat="server" Text="Assig. Period"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboAssignedPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblCalcType" runat="server" Text="Loan Calc. Type"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboCalcType" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 6%">
                                                    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                </td>
                                                <td style="width: 11%">
                                                    <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblDeductionPeriod" runat="server" Text="Deduct. Period"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboDeductionPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblInterestCalcType" runat="server" Text="Intr Calc. Type"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboInterestCalcType" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 6%">
                                                    <asp:Label ID="lblMode" runat="server" Text="Mode"></asp:Label>
                                                </td>
                                                <td style="width: 11%">
                                                    <asp:DropDownList ID="cboMode" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                                <div id="divBasicInfo" class="panel-default" style="margin: 0px; margin-top: 10px;
                                    margin-bottom: 10px">
                                    <div id="Div3" class="panel-heading-default" style="height: 20px">
                                        <div style="float: left;">
                                            <asp:Label ID="lblBasicInfo" runat="server" Text="Basic Information"></asp:Label>
                                        </div>
                                        <div id="divCurrency" style="float: left; padding: 3px; margin-left: 145px; font-size: 13px">
                                            <asp:Label ID="objlblExchangeRate" runat="server" Text="" Style="color: Red"></asp:Label>
                                        </div>
                                        <div style="float: right">
                                            <asp:Label ID="lblPaymentBy" runat="server" Text="Payment By" Style="margin-right: 10px;
                                                font-weight: bold"></asp:Label>
                                            <asp:RadioButton ID="radValue" runat="server" Text="Value" GroupName="Payment" AutoPostBack="true"
                                                Style="margin-right: 10px" />
                                            <asp:RadioButton ID="radPercentage" runat="server" Text="Percentage" GroupName="Payment"
                                                AutoPostBack="true" Style="margin-right: 25px" />
                                            <asp:CheckBox ID="chkCalculateInterest" runat="server" Text="Calculate Interest"
                                                Style="margin-right: 50px" />
                                        </div>
                                    </div>
                                    <div id="Div4" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode"></asp:Label>
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:DropDownList ID="cboPaymentMode" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 6%">
                                                    <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblAccountNo" runat="server" Text="Account No."></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboAccountNo" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblPayPeriod" runat="server" Text="Period"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 6%">
                                                    <asp:Label ID="lblPaymentDate" runat="server" Text="Pmt Date"></asp:Label>
                                                </td>
                                                <td style="width: 11%">
                                                    <uc1:DateCtrl ID="dtpPaymentDate" runat="server" Width="80" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblBankGroup" runat="server" Text="Bank Group"></asp:Label>
                                                </td>
                                                <td style="width: 20%" colspan="3">
                                                    <asp:DropDownList ID="cboBankGroup" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblChequeNo" runat="server" Text="Cheque No."></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:TextBox ID="txtChequeNo" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblVoucherNo" runat="server" Text="Voucher#"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:TextBox ID="txtVoucherNo" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 6%">
                                                    <asp:Label ID="lblPercentage" runat="server" Text="Percent(%)"></asp:Label>
                                                </td>
                                                <td style="width: 11%">
                                                    <asp:TextBox ID="txtPercentage" CssClass="percentage" runat="server" AutoPostBack="true" Style="text-align: right"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%; vertical-align: baseline">
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                                </td>
                                                <td style="width: 20%" colspan="3">
                                                    <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblChangeStatus" runat="server" Text="Change Status"></asp:Label>
                                                </td>
                                                <td style="width: 16%">
                                                    <asp:DropDownList ID="cboChangeStatus" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 1%">
                                                </td>
                                                <td style="width: 9%">
                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                </td>
                                                <td style="width: 35%; vertical-align: top" colspan="4" rowspan="2">
                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--<div class="btn-default">
                                            <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btndefault" />
                                        </div>--%>
                                    </div>
                                </div>
                                <div id="divDataGrid" class="panel-default" style="margin: 0px">
                                    <div id="Div6" class="panel-body-default">
                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Type to Search" AutoPostBack="true" style="width:70%;" ></asp:TextBox>
                                        <asp:Label ID="objlblCount" runat="server" style="" Text = "( 0 / 0 )"  ></asp:Label>
                                        <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="max-height: 400px;
                                            overflow: auto">
                                            <asp:DataGrid ID="dgvLoanList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                HeaderStyle-Font-Bold="false" Width="135%">
                                                <ItemStyle CssClass="griviewitem" />
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objdgcolhSelectAll" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("IsChecked") %>'
                                                                OnCheckedChanged="chkSelect_CheckedChanged" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="IsChecked" FooterText="objdgcolhSelect" Visible="false" />
                                                    <asp:BoundColumn DataField="VoucherNo" FooterText="dgcolhVocNo" HeaderText="Voucher No." />
                                                    <asp:BoundColumn DataField="Employee" FooterText="dgcolhEmployee" HeaderText="Employee" />
                                                    <asp:BoundColumn DataField="LoanScheme" FooterText="dgcolhLoanScheme" HeaderText="Loan Scheme" />
                                                    <asp:BoundColumn DataField="Mode" FooterText="dgcolhLoanAdvance" HeaderText="Mode" />
                                                    <asp:BoundColumn DataField="LoanCalcType" FooterText="dgcolhCalcType" HeaderText="Calculation Type" />
                                                    <asp:BoundColumn DataField="LoanInterestCalcType" FooterText="dgcolhInterestCalcType"
                                                        HeaderText="Interest Type" />
                                                    <asp:BoundColumn DataField="LoanAdvanceAmount" FooterText="dgcolhLoanAdvanceAmount"
                                                        HeaderText="Ln/Adv Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="balance_amount" FooterText="dgcolhBalanceAmount" HeaderText="Balance Amount"
                                                        HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" />
                                                    </asp:BoundColumn>
                                                    <%--<asp:BoundColumn DataField="settelment_amount" FooterText="dgcolhSettelmentAmount"
                                                        HeaderText="Settlement Amount" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />--%>
                                                    <asp:TemplateColumn HeaderText="Settlement Amount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderStyle-Width="160px" ItemStyle-Width="160px" FooterText="dgcolhSettelmentAmount">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtdgcolhSettelmentAmount" onkeypress="return onlyNumbers(this, event);"
                                                                Style="text-align: right" runat="server" AutoPostBack="True" 
                                                                Text='<%# Eval("settelment_amount") %>' 
                                                                ontextchanged="txtdgcolhSettelmentAmount_TextChanged"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle Width="160px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="loan_status" FooterText="dgcolhStatus" HeaderText="Status" />
                                                    <asp:BoundColumn DataField="ChangedStatus" FooterText="dgcolhChangedStatus" HeaderText="Change Status" />
                                                    <asp:BoundColumn DataField="loanadvancetranunkid" FooterText="objdgcolhLoanAdvanceUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeUnkid" Visible="false" />
                                                    <asp:BoundColumn DataField="loanschemeunkid" FooterText="objdgcolhLoanSchemeUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objdgcolhAssignedPeriodUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="deductionperiodunkid" FooterText="objdgcolhDeductionPeriodUnkid"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="calctype_id" FooterText="objdgcolhCalcTypeId" Visible="false" />
                                                    <asp:BoundColumn DataField="interest_calctype_id" FooterText="objdgcolhInterestCalcTypeId"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="statusunkid" FooterText="objdgcolhStatusUnkid" Visible="false" />
                                                    <asp:BoundColumn DataField="changestatusunkid" FooterText="objdgcolhChangeStatusUnkid"
                                                        Visible="false" />
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
