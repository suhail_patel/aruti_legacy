﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" Title="Global Cancel Approved Application"
    CodeFile="wPg_GlobalCancelApprovedApplication.aspx.vb" Inherits="Loan_Savings_New_Loan_Loan_Approval_Process_wPg_GlobalCancelApprovedApplication" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").not(".combo").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollLeft($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Cancel Approved Application"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <div id="Div7" class="panel-default">
                                    <div id="Div9" class="panel-body-default">
                                        <table style="width: 100%; margin-top: -10px; margin-bottom: -10px">
                                            <tr style="width: 100%">
                                                <td style="width: 65%" valign="top">
                                                    <div id="FilterCriteria" class="panel-default" style="margin-left: -2px; margin-right: -2px">
                                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                                            </div>
                                                            <div style="float: right">
                                                                <asp:LinkButton ID="lnkAllocation" Style="font-weight: bold; font-size: 12px; margin-right: 5px"
                                                                    Font-Underline="false" runat="server" Text="Allocation"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div id="FilterCriteriaBody" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 17%">
                                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 40%" colspan="3">
                                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="lblApplicationNo" Style="margin-left: 15px" runat="server" Text="Application No."></asp:Label>
                                                                    </td>
                                                                    <td style="width: 25%" colspan="2">
                                                                        <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 17%">
                                                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 40%" colspan="3">
                                                                        <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="lblApprovedAmount" runat="server" Style="margin-left: 15px" Text="Approved Amt"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 17%">
                                                                        <asp:TextBox ID="txtApprovedAmount" Text="0" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                                            runat="server"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 8%">
                                                                        <asp:DropDownList ID="cboAmountCondition" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 17%">
                                                                        <asp:Label ID="lblApplicationDate" runat="server" Text="Application Date"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        <uc2:DateCtrl ID="dtpFromDate" runat="server" Width="85" AutoPostBack="false" />
                                                                    </td>
                                                                    <td style="width: 4%">
                                                                        <asp:Label ID="lblTo" runat="server" Text="To" Style="text-align: left"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        <uc2:DateCtrl ID="dtpToDate" runat="server" Width="85" AutoPostBack="false" />
                                                                    </td>
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="lblLoanAdvance" runat="server" Style="margin-left: 15px" Text="Loan/advance"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 25%" colspan="2">
                                                                        <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 35%" valign="top">
                                                    <div id="Div1" class="panel-default" style="height: 140px; margin-right: -2px">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbInfo" runat="server" Text="Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboStatus" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%; vertical-align: top" rowspan="2">
                                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%" rowspan="2">
                                                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Div4" class="panel-default" style="margin-top: 15px">
                                <div id="Div5" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbApproved" runat="server" Text="Approved Loan/Advance Application(s)."></asp:Label>
                                    </div>
                                </div>
                                <div id="Div6" class="panel-body-default" style="position: relative">
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollLeft);$(scroll1.Y).val(this.scrollTop);"
                                        style="max-height: 400px; overflow: auto; margin-bottom: 5px">
                                        <asp:DataGrid ID="dgvLoanApproved" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                            ShowFooter="False" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            HeaderStyle-Font-Bold="false" Width="99%">
                                            <ItemStyle CssClass="griviewitem" />
                                            <Columns>
                                                <asp:TemplateColumn FooterText="objdgcolhchkSelect" 
                                                    HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" 
                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" 
                                                            oncheckedchanged="chkSelectAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" 
                                                            oncheckedchanged="chkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="Application_No" FooterText="dgcolhApplicationNo" 
                                                    HeaderText="Application No." />
                                                <asp:BoundColumn DataField="applicationdate" FooterText="dgcolhAppDate" 
                                                    HeaderText="Application Date" />
                                                <asp:BoundColumn DataField="EmpName" FooterText="dgcolhEmployee" 
                                                    HeaderText="Employee" />
                                                <asp:BoundColumn DataField="LoanScheme" FooterText="dgcolhLoanScheme" 
                                                    HeaderText="Loan Scheme" />
                                                <asp:BoundColumn DataField="Loan_Advance" FooterText="dgcolhLoan_Advance" 
                                                    HeaderText="Loan/Advance" />
                                                <asp:BoundColumn DataField="Amount" FooterText="dgcolhAmount" 
                                                    HeaderStyle-HorizontalAlign="Right" HeaderText="Amount" 
                                                    ItemStyle-HorizontalAlign="Right">
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="Approved_Amount" FooterText="dgcolhApp_Amount" 
                                                    HeaderStyle-HorizontalAlign="Right" HeaderText="Approved Amount" 
                                                    ItemStyle-HorizontalAlign="Right">
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                    <ItemStyle HorizontalAlign="Right" />
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="LoanStatus" FooterText="dgcolhStatus" 
                                                    HeaderText="Loan Status" />
                                                <asp:BoundColumn DataField="isloan" FooterText="objdgcolhIsLoan" 
                                                    Visible="false" />
                                                <asp:BoundColumn DataField="loan_statusunkid" FooterText="objdgcolhStatusunkid" 
                                                    Visible="false" />
                                                <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmployeeunkid" 
                                                    Visible="false" />
                                                <asp:BoundColumn DataField="loanschemeunkid" FooterText="objdgcolhSchemeID" 
                                                    Visible="false" />
                                                <asp:BoundColumn DataField="processpendingloanunkid" 
                                                    FooterText="objdgcolhprocesspendingloanunkid" Visible="false" />
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                    <div class="btn-default" id="btnfixedbottom">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
