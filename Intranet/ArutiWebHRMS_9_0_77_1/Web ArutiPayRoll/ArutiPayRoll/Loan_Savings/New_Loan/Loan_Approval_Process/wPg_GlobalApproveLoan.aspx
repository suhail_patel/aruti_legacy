﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_GlobalApproveLoan.aspx.vb"
    Inherits="Loan_Savings_wPg_GlobalApproveLoan" Title="Global Approve Loan" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").not(".combo").searchable();
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };

        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollLeft($(scroll.Y).val());
                $("#scrollable-container").scrollTop($(scroll1.Y).val());
            }
            setIcon()
        }
    </script>

    <script type="text/javascript">
        function setIcon() {
            $(".isgroupclass").each(function() {
                if ($(this).attr("title") == "True") {
                    $(this).parent().parent().children("td:first").children("a").attr("title", "Collapse")
                    $(this).parent().parent().children("td:first").children().children("div").children("i").removeClass("fa-chevron-circle-down")
                }
                else {
                    $(this).parent().parent().children("td:first").children("a").attr("title", "Expand")
                    $(this).parent().parent().children("td:first").children().children("div").children("i").addClass("fa-chevron-circle-down")
                }
            });
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Approve Loan"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <div id="Div7" class="panel-default">
                                    <div id="Div9" class="panel-body-default">
                                        <table style="width: 100%; margin-top: -10px; margin-bottom: -10px">
                                            <tr style="width: 100%">
                                                <td style="width: 65%" valign="top">
                                                    <div id="FilterCriteria" class="panel-default" style="margin-left: -2px; margin-right: -2px">
                                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbFilter" runat="server" Text="Filter Criteria"></asp:Label>
                                                            </div>
                                                            <div style="float: right">
                                                                <asp:LinkButton ID="lnkAllocation" Style="font-weight: bold; font-size: 12px; margin-right: 5px"
                                                                    Font-Underline="false" runat="server" Text="Allocation"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div id="FilterCriteriaBody" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 82%" colspan="4">
                                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 82%" colspan="4">
                                                                        <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 18%">
                                                                        <asp:Label ID="LblDeductionPeriod" runat="server" Text="Deduction Period"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 35%">
                                                                        <asp:DropDownList ID="cboDeductionPeriod" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 17%">
                                                                        <asp:Label ID="lblLoanAmount" runat="server" Style="margin-left: 10px" Text="Loan Amount"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <asp:TextBox ID="txtLoanAmount" runat="server" Text="0" onKeypress="return onlyNumbers(this, event);"
                                                                            Style="text-align: right"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 10%">
                                                                        <asp:DropDownList ID="cboAmountCondition" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="width: 35%" valign="top">
                                                    <div id="Div1" class="panel-default" style="height: 140px; margin-right: -2px">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="gbInfo" runat="server" Text="Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboApprover" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%">
                                                                        <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 75%">
                                                                        <asp:DropDownList ID="cboStatus" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 25%; vertical-align: top">
                                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="Div4" class="panel-default" style="margin-top: 15px">
                                <div id="Div5" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbPending" runat="server" Text="Pending Loan/Advance Application(s)."></asp:Label>
                                    </div>
                                    <div style="text-align: right; vertical-align: top;">
                                        <asp:RadioButton ID="radLoan" runat="server" Text="Loan" GroupName="GlobalApprove"
                                            Style="margin-right: 20px" AutoPostBack="true"></asp:RadioButton>
                                        <asp:RadioButton ID="radAdvance" runat="server" Text="Advance" GroupName="GlobalApprove"
                                            Style="margin-right: 20px" AutoPostBack="true"></asp:RadioButton>
                                    </div>
                                </div>
                                <div id="Div6" class="panel-body-default" style="position: relative">
                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollLeft);$(scroll1.Y).val(this.scrollTop);"
                                        style="max-height: 400px; overflow: auto; margin-bottom: 5px">
                                        <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                            ShowFooter="False" Width="125%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            ItemStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" FooterText="objdgcolhCollapse">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgCollapse" runat="server" ToolTip="Collapse">
                                                            <div style="font-size: 20px; color:#004474"><i id="expcol" class="fa fa-chevron-circle-up"></i></div>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="30px" HorizontalAlign="Center" />
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                </asp:TemplateColumn>
                                                <%--1--%>
                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" FooterText="objdgcolhCheck">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged1" />
                                                        <asp:Label ID="lblgroupid" runat="server" Text="" Style="display: none"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--2--%>
                                                <asp:BoundColumn DataField="EName" HeaderText="Employee" FooterText="dgcolhEName"
                                                    HeaderStyle-Width="300px" ItemStyle-Width="300px" />
                                                <%--3--%>
                                                <asp:BoundColumn DataField="BasicSal" HeaderText="Basic Salary" FooterText="dgcolhBasicSal"
                                                    HeaderStyle-Width="150px" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right"
                                                    HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                <%--4--%>
                                                <asp:BoundColumn DataField="AppNo" HeaderText="Application No." FooterText="dgcolhAppNo" HeaderStyle-Width="110px" ItemStyle-Width="110px" />
                                                <%--5--%>
                                                <asp:BoundColumn DataField="Amount" HeaderText="Applied Amount" HeaderStyle-Width="150px"
                                                    ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"
                                                    FooterText="dgcolhRAmount">
                                                </asp:BoundColumn>
                                                <%--6--%>
                                                <asp:BoundColumn DataField="DeductionPeriodID" HeaderText="DeductionPeriodID" Visible="false"
                                                    FooterText="dgcolhDeductionPeriod"></asp:BoundColumn>
                                                <%--7--%>
                                                <asp:TemplateColumn HeaderText="Deduction Period" HeaderStyle-Width="150px" ItemStyle-Width="150px"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cbodgcolhDeductionPeriod" runat="server" AutoPostBack="true"
                                                            OnSelectedIndexChanged="cbodgcolhDeductionPeriod_SelectedIndexChanged" CssClass="combo">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--8--%>
                                                <asp:TemplateColumn HeaderText="Duration" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"
                                                    FooterText="dgcolhDuration" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhDuration" CssClass="numberonly" Text='<%# Eval("duration") %>'
                                                            Style="text-align: right; padding-right: 2px" runat="server" OnTextChanged="txtdgcolhDuration_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Right" />
                                                </asp:TemplateColumn>
                                                <%--9--%>
                                                <asp:TemplateColumn HeaderText="Installment Amount" HeaderStyle-HorizontalAlign="Right"
                                                    FooterText="dgcolhInstallmentAmt" HeaderStyle-Width="150px" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhInstallmentAmt" Text='<%# Eval("installmentamt") %>' Style="text-align: right;
                                                            padding-right: 2px" runat="server" AutoPostBack="true" OnTextChanged="txtdgcolhInstallmentAmt_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--10--%>
                                                <asp:TemplateColumn HeaderText="No of Installment" HeaderStyle-HorizontalAlign="Right"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="dgcolhNoOfInstallments" HeaderStyle-Width="150px" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhNoOfInstallments" CssClass="numberonly" Text='<%# Eval("noofinstallment") %>'
                                                            Style="text-align: right; padding-right: 2px" runat="server" AutoPostBack="true"
                                                            OnTextChanged="txtdgcolhNoOfInstallments_TextChanged"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--11--%>
                                                <asp:TemplateColumn HeaderText="Approved Amount" HeaderStyle-HorizontalAlign="Right"
                                                    FooterText="dgcolhAAmount" HeaderStyle-Width="150px" ItemStyle-Width="150px">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdgcolhAAmount" Text='<%# Eval("AppAmount") %>' Style="text-align: right;
                                                            padding-right: 2px" runat="server" OnTextChanged="txtdgcolhAAmount_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>
                                                    </ItemTemplate>
                                                    
                                                </asp:TemplateColumn>
                                                <%--12--%>
                                                <asp:BoundColumn DataField="PId" HeaderText="objcolhPendingUnkid" Visible="false"
                                                    ReadOnly="true" FooterText="objcolhPendingUnkid" />
                                                <%--13--%>
                                                <asp:BoundColumn DataField="IsGrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                <%--14--%>
                                                <asp:BoundColumn DataField="GrpId" HeaderText="objdgcolhGrpId" Visible="false" FooterText="objdgcolhGrpId" />
                                                <%--15--%>
                                                <asp:BoundColumn DataField="IsVisible" Visible="false" />
                                                <%--16--%>
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:DataGrid>
                                    </div>
                                    <div class="btn-default" id="btnfixedbottom">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
