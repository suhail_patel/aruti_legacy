﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanApprovalList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Approval_Process_wPg_LoanApprovalList" Title="Loan Approval List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right; font-weight: bold; text-align: right; font-size: 12px">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblApplicationNo" Style="margin-left: 10px" runat="server" Text="Application No.">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblFromDate" runat="server" Style="margin-left: 10px" Text="From Date">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Style="margin-left: 10px" Text="Status">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboLoanScheme" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblLoanAdvance" Style="margin-left: 10px" runat="server" Text="Loan/Advance">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:DropDownList ID="cboLoanAdvance" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblToDate" runat="server" Style="margin-left: 10px" Text="To Date">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 8%">
                                            </td>
                                            <td style="width: 12%">
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtApprover" runat="server"></asp:TextBox>
                                                <%--<asp:DropDownList ID="cboApprover" runat="server">
                                                </asp:DropDownList>--%>
                                            </td>
                                            <td style="width: 25%" colspan="2">
                                                <asp:CheckBox ID="chkMyApprovals" runat="server" AutoPostBack="true" Text="My Approvals" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLoanAmount" Style="margin-left: 10px" runat="server" Text="Loan Amount">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:TextBox ID="txtLoanAmount" runat="server" Style="text-align: right" onkeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:DropDownList ID="cboAmountCondition" Width="70%" runat="server" AutoPostBack="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgLoanApproval" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="150%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="110px" Visible="true" ItemStyle-Width="110px"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="lnkChangeStatus" Font-Underline="false" Style="font-weight: bold;
                                                                            font-size: 12px" CommandName="Status" Text="Change Status" runat="server"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="application_no" HeaderText="Application No" Visible="false"
                                                                FooterText="dgcolhApplicationNo" />
                                                            <asp:BoundColumn DataField="application_date" HeaderText="Application Date" FooterText="dgcolhApplicationDate" />
                                                            <asp:BoundColumn DataField="Employee" HeaderText="Employee" FooterText="dgcolhEmployee" />
                                                            <asp:BoundColumn DataField="ApproverName" HeaderText="Approver" FooterText="dgcolhApprover" />
                                                            <asp:BoundColumn DataField="Scheme" HeaderText="Loan Scheme" FooterText="dgcolhLoanScheme" />
                                                            <asp:BoundColumn DataField="loan_advance" HeaderText="Loan/Advance" FooterText="dgcolhloan_advance">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="loan_amount" HeaderText="Applied Amount" HeaderStyle-HorizontalAlign="Right"
                                                                ItemStyle-HorizontalAlign="Right" FooterText="dgcolhLnAdvAmount" />
                                                            <asp:BoundColumn DataField="approved_amount" HeaderText="Approved Amount" FooterText="dgcolhApprovedAmount"
                                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="status" HeaderText="Status" FooterText="dgcolhStatus" />
                                                            <asp:BoundColumn DataField="remark" HeaderText="Remarks" FooterText="dgcolhStatus" />
                                                            <asp:BoundColumn DataField="priority" HeaderText="objdgcolhPriority" Visible="false"
                                                                FooterText="objdgcolhPriority" />
                                                            <asp:BoundColumn DataField="Isgrp" HeaderText="objdgcolhIsGrp" Visible="false" FooterText="objdgcolhIsGrp" />
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="objdgcolhEmployeeID" Visible="false"
                                                                FooterText="objdgcolhEmployeeID" />
                                                            <asp:BoundColumn DataField="MappedUserId" HeaderText="objdgcolhUserID" Visible="false"
                                                                FooterText="objdgcolhUserID" />
                                                            <asp:BoundColumn DataField="approverunkid" HeaderText="objdgcolhApproverunkid" Visible="false"
                                                                FooterText="objdgcolhApproverunkid" />
                                                            <asp:BoundColumn DataField="approverempunkid" HeaderText="objdgcolhApproverempunkid"
                                                                Visible="false" FooterText="objdgcolhApproverempunkid" />
                                                            <asp:BoundColumn DataField="statusunkid" HeaderText="objdgcolhStatusunkid" Visible="false"
                                                                FooterText="objdgcolhStatusunkid" />
                                                            <asp:BoundColumn DataField="loan_statusunkid" HeaderText="objdgcolhLoanStatusID"
                                                                Visible="false" FooterText="objdgcolhLoanStatusID" />
                                                            <asp:BoundColumn DataField="processpendingloanunkid" HeaderText="objdgcolhprocesspendingloanunkid"
                                                                Visible="false" FooterText="objdgcolhprocesspendingloanunkid" />
                                                            <asp:BoundColumn DataField="pendingloantranunkid" HeaderText="objdgcolhpendingloantranunkid"
                                                                Visible="false" FooterText="objdgcolhpendingloantranunkid" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
