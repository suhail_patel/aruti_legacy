﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_AddEditLoanApprover.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Approver_wPg_AddEditLoanApprover" Title="Add/Edit Loan Approver" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll1 = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
                
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll1.Y).val());
            $("#scrollable-container1").scrollTop($(scroll2.Y).val());
    }
}
    </script>
    
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Loan Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top">
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblInfo" runat="server" Text="Loan Approver Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 20%"></td>
                                                                    <td style="width: 80%">
                                                                        <asp:CheckBox ID="chkExternalApprover" runat="server" Text="Make External Approver" AutoPostBack="true" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblLoanApproverName" runat="server" Text="Loan Approver"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboApprover" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblApproveLevel" runat="server" Text="Level"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboLoanApproveLevel" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblUser" runat="server" Text="User"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboUser" runat="server" Style="margin-bottom: 5px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%" colspan="2">
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 40%; text-align: left; font-weight: bold;">
                                                                                    <asp:LinkButton ID="lnkMapLoanScheme" runat="server" Text="Map Loan Scheme" CssClass="lnkhover"></asp:LinkButton>
                                                                                </td>
                                                                                <td style="width: 60%; text-align: right; padding-right: 15px; font-weight: bold;">
                                                                                    <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"
                                                                                        Style="margin-right: 10px;"></asp:LinkButton>
                                                                                    <asp:LinkButton ID="lnkReset" runat="server" Style="text-align: right" Text="Reset"
                                                                                        CssClass="lnkhover"></asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%;" colspan="2">
                                                                        <asp:TextBox ID="txtSearchEmp" runat="server" Style="margin-top: 5px; margin-bottom: 5px"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="border-bottom: 1px solid #DDD; margin-bottom: 10px; margin-top: 10px"
                                                                        colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%" colspan="2">
                                                                        <div id="scrollable-container" style="overflow: auto; vertical-align: top; max-height:300px"
                                                                            onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                            <asp:DataGrid ID="dgvAEmployee" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <ItemStyle CssClass="griviewitem" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAll_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged" />
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                                        <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="colhEcode" />
                                                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="colhEName" />
                                                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                        FooterText="colhEmpId" Visible="false" />
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                            </asp:DataGrid>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 50%; vertical-align: top">
                                                <div class="panel-body">
                                                    <div id="Div4" class="panel-default">
                                                        <div id="Div5" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblAssignedEmployee" runat="server" Text="Assigned Employee"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div6" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtAssignedEmpSearch" Style="margin-bottom: 5px" runat="server"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="border-bottom: 1px solid #DDD; margin-bottom: 10px; margin-top: 10px">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <div id="scrollable-container1" style="overflow: auto; vertical-align: top;max-height:450px"
                                                                            onscroll="$(scroll2.Y).val(this.scrollTop);">
                                                                            <asp:DataGrid ID="dgvAssignedEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="150%">
                                                                                <ItemStyle CssClass="griviewitem" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkAsgAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAsgAll_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkAsgSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAsgSelect_CheckedChanged" />
                                                                                        </ItemTemplate>
                                                                                        <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                                        <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="ecode" HeaderText="Code" ReadOnly="true" FooterText="colhaCode" />
                                                                                    <asp:BoundColumn DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhaEmp" />
                                                                                    <asp:BoundColumn DataField="edept" HeaderText="Department" ReadOnly="true" FooterText="colhaDepartment" />
                                                                                    <asp:BoundColumn DataField="ejob" HeaderText="Job" ReadOnly="true" FooterText="colhaJob" />
                                                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                        FooterText="colhaEmpId" Visible="false" />
                                                                                    <asp:BoundColumn DataField="lnloanapproverunkid" HeaderText="AMasterId" ReadOnly="true"
                                                                                        FooterText="colhAMasterId" Visible="false" />
                                                                                    <asp:BoundColumn DataField="lnapprovertranunkid" HeaderText="ATranId" ReadOnly="true"
                                                                                        FooterText="colhATranId" Visible="false" />
                                                                                </Columns>
                                                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                            </asp:DataGrid>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnDeleteA" runat="server" Text="Delete" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupLoanSchemeMapping" BackgroundCssClass="modalBackground"
                        TargetControlID="lblApprover" runat="server" PopupControlID="pnlLoanSchemeMapping"
                        CancelControlID="btnCancel" />
                    <asp:Panel ID="pnlLoanSchemeMapping" runat="server" CssClass="newpopup" Style="display: none;"
                        DefaultButton="btnOK" Width="35%">
                        <div class="panel-primary" style="margin: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Loan Scheme Mapping"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div7" class="panel-default">
                                    <div id="Div8" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Approver Detail"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div9" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:Label ID="objlblApprover" style="margin-left:10px" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblApproverLevel" runat="server" Text="Approver Level"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:Label ID="objlblApproverLevel" style="margin-left:10px" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <div style="border: 1px solid #DDD; margin-top: 10px; margin-bottom: 10px">
                                        </div>
                                        <table style="width: 100%; vertical-align: top">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtSearchScheme" OnTextChanged="txtSearchScheme_TextChanged" runat="server" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <div class="gridscroll" style="vertical-align: top; overflow: auto; max-height: 300px">
                                                        <asp:DataGrid ID="dgLoanScheme" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                            AutoGenerateColumns="false" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                            <ItemStyle CssClass="griviewitem" />
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                    ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="ChkAllLoanScheme" runat="server" OnCheckedChanged="ChkAllLoanScheme_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="ChkdgSelectLoanScheme" runat="server" OnCheckedChanged="ChkdgSelectLoanScheme_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                                    <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="code" HeaderStyle-Width="40%" HeaderText="Loan Scheme Code"
                                                                    ReadOnly="true" FooterText="dgcolhSchemeCode"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="name" HeaderStyle-Width="60%" HeaderText="Loan Scheme Name"
                                                                    ReadOnly="true" FooterText="dgcolhLoanScheme"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="Loanschememappingunkid" Visible="false" ReadOnly="true"
                                                                    FooterText="objdgcolhLoanSchemeMappingunkid"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="loanschemeunkid" Visible="false" ReadOnly="true"
                                                                    ></asp:BoundColumn>
                                                            </Columns>
                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                        </asp:DataGrid>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btnDefault" />
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc1:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc2:ConfirmYesNo ID="popYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

</asp:Content>
