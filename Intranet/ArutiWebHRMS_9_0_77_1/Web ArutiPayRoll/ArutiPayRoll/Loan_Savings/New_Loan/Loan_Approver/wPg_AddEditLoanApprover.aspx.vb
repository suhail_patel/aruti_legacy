﻿Option Strict On

#Region "Import"

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Approver_wPg_AddEditLoanApprover
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmLoanAprroverAddEdit"
    Private Shared ReadOnly mstrModuleName1 As String = "frmApprover_LoanSchemeMapping"
    Dim DisplayMessage As New CommonCodes
    'Private mintlnApproverunkid As Integer = -1
    Private mstrAdvanceFilter As String = ""
    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Private objlnApproverLevelMaster As New clslnapproverlevel_master
    'Private objlnApproverMaster As New clsLoanApprover_master
    ''Private objlnApproverTran As New clsLoanApprover_tran
    'Private objLoanMapping As New clsapprover_scheme_mapping
    'Hemant (04 Sep 2020) -- End
    Private mdtEmployee As DataTable = Nothing
    Private mdtAssignedEmp As DataTable = Nothing
    Private dtAssignEmpView As DataView = Nothing
    Private mintapproverunkid As Integer = -1
    Private dtEmployee As DataTable = Nothing
    Private mdtLoanSchemeMapping As DataTable = Nothing
    Private dtLoanScheme As New DataTable
    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Private mstrEmployeeIDs As String = String.Empty
    Private mintApproverEmpunkid As Integer = -1
    'Nilay (01-Mar-2016) -- End
#End Region

#Region "Page's event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objlnApproverMaster As New clsLoanApprover_master
        Dim objlnApproverTran As New clsLoanApprover_tran
        'Hemant (04 Sep 2020) -- End
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Hemant (04 Sep 2020) -- Start
                'Issue : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                Call SetLanguage()
                'Dim dtAEmp As DataTable = Nothing 'Nilay (01-Mar-2016)

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                dgvAEmployee.DataSource = New List(Of String)
                dgvAEmployee.DataBind()
                dgvAssignedEmp.DataSource = New List(Of String)
                dgvAssignedEmp.DataBind()
                'Nilay (01-Mar-2016) -- End

                FillCombo()

                If Session("lnapproverunkid") IsNot Nothing Then
                    cboApprover.Enabled = False
                    mintapproverunkid = CInt(Session("lnapproverunkid"))
                    objlnApproverMaster._lnApproverunkid = mintapproverunkid
                    objlnApproverTran._LoanApproverunkid = mintapproverunkid
                    objlnApproverTran.GetData()
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    chkExternalApprover.Enabled = False
                    'Nilay (01-Mar-2016) -- End
                Else
                    Session("lnapproverunkid") = -1
                End If

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dtAEmp = objlnApproverTran._TranDataTable
                'Me.ViewState.Add("dtAssignEmpView", dtAEmp)
                mdtAssignedEmp = objlnApproverTran._TranDataTable
                'Nilay (01-Mar-2016) -- End

                Call GetValue(objlnApproverMaster)
                'Hemant (04 Sep 2020) -- [objlnApproverMaster]
            Else
                mdtAssignedEmp = CType(Me.ViewState("mdtAssignedEmp"), DataTable)
                mdtEmployee = CType(Me.ViewState("mdtEmployee"), DataTable)
                mintapproverunkid = CInt(Me.ViewState("mintapproverunkid"))
                dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
                dtLoanScheme = CType(Me.ViewState("dtLoanScheme"), DataTable)
                mdtLoanSchemeMapping = CType(Me.ViewState("mdtLoanSchemeMapping"), DataTable)
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                mstrEmployeeIDs = CStr(Me.ViewState("EmployeeIDs"))
                mintApproverEmpunkid = CInt(Me.ViewState("ApproverEmpunkid"))
                'Nilay (01-Mar-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load1:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApproverMaster = Nothing
            objlnApproverTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtAssignedEmp") = mdtAssignedEmp
            Me.ViewState("mdtEmployee") = mdtEmployee
            Me.ViewState("mintapproverunkid") = mintapproverunkid
            Me.ViewState("dtEmployee") = dtEmployee
            Me.ViewState("dtLoanScheme") = dtLoanScheme
            Me.ViewState("mdtLoanSchemeMapping") = mdtLoanSchemeMapping
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            Me.ViewState("ApproverEmpunkid") = mintApproverEmpunkid
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session.Remove("lnapproverunkid")
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objlnApproverLevelMaster As New clslnapproverlevel_master
            Dim dsList As DataSet = Nothing
            Dim dsCombo As DataSet = Nothing
            Dim objEmp As New clsEmployee_Master
            Dim objUser As New clsUserAddEdit
        'Hemant (04 Sep 2020) -- End
        Try
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            'Dim dsList As DataSet = Nothing
            'Dim dsCombo As DataSet = Nothing
            'Dim objEmp As New clsEmployee_Master
            'Dim objUser As New clsUserAddEdit
            'Hemant (04 Sep 2020) -- End

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'dsCombo = objEmp.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                             CStr(Session("UserAccessModeSetting")), True, False, "List", _
            '                             , , , "isapproved = 1")

            'mdtEmployee = dsCombo.Tables("List").Copy
            'mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
            'For i As Integer = 0 To mdtEmployee.Rows.Count - 1
            '    mdtEmployee.Rows(i).Item("ischeck") = False
            'Next

            'Dim drRow As DataRow = dsCombo.Tables("List").NewRow
            'drRow("name") = "Select"
            'drRow("employeeunkid") = 0
            'dsCombo.Tables("List").Rows.InsertAt(drRow, 0)
            'With cboApprover
            '    .DataTextField = "name"
            '    .DataValueField = "employeeunkid"
            '    .DataSource = dsCombo.Tables("List")
            '    .DataBind()
            'End With

            If chkExternalApprover.Checked = False Then
               
            dsCombo = objEmp.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                     CStr(Session("UserAccessModeSetting")), True, False, "List", _
                                     , , , "isapproved = 1")

            mdtEmployee = dsCombo.Tables("List").Copy
            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False

            For i As Integer = 0 To mdtEmployee.Rows.Count - 1
                mdtEmployee.Rows(i).Item("ischeck") = False
            Next

            Dim drRow As DataRow = dsCombo.Tables("List").NewRow
            drRow("name") = "Select"
            drRow("employeeunkid") = 0
            dsCombo.Tables("List").Rows.InsertAt(drRow, 0)
            With cboApprover
                .DataTextField = "name"
                .DataValueField = "employeeunkid"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With
            Else
                dsList = objUser.GetExternalApproverList("List", CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         "345", True)

                Dim drRow As DataRow = dsList.Tables("List").NewRow
                drRow("name") = "Select"
                drRow("userunkid") = 0
                dsList.Tables("List").Rows.InsertAt(drRow, 0)
                With cboApprover
                    .DataTextField = "name"
                    .DataValueField = "userunkid"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
            'Nilay (01-Mar-2016) -- End

            dsList = objlnApproverLevelMaster.getListForCombo("List", True)
            With cboLoanApproveLevel
                .DataTextField = "name"
                .DataValueField = "lnlevelunkid"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CBool(Session("IsEmployeeAsUser")) Then
            '    dsCombo = objUser.getComboList("User", True, False, True, CInt(Session("CompanyUnkId")), , CInt(Session("Fin_year")))
            'Else
            '    dsCombo = objUser.getComboList("User", True, False, False)
            'End If
            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(702), CInt(Session("Fin_year")), True)
            'Nilay (01-Mar-2016) -- End

            With cboUser
                .DataValueField = "userunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("User")
                .DataBind()
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApproverLevelMaster = Nothing
            objlnApproverLevelMaster = Nothing
            objEmp = Nothing
            objUser = Nothing
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub Fill_Employee()
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objlnApproverTran As New clsLoanApprover_tran
        'Hemant (04 Sep 2020) -- End
        Try
            'Dim dtEmployee As DataTable = Nothing
            Dim strSearch As String = ""

            If mdtEmployee Is Nothing Then Exit Sub

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter.Replace("ADF.", "")
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboApprover.SelectedValue) > 0 Then
            '    strSearch &= "AND employeeunkid <> " & CInt(cboApprover.SelectedValue) & " "
            'End If
            If chkExternalApprover.Checked = False Then
            If CInt(cboApprover.SelectedValue) > 0 Then
                strSearch &= "AND employeeunkid <> " & CInt(cboApprover.SelectedValue) & " "
            End If
            End If

            If mstrEmployeeIDs.Trim.Length > 0 Then
                strSearch &= "AND employeeunkid NOT IN(" & mstrEmployeeIDs & ") "
            End If
            'Nilay (01-Mar-2016) -- End

            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch &= "AND (employeecode like '%" & txtSearchEmp.Text.Trim & "%' OR name like '%" & txtSearchEmp.Text.Trim & "%') "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtEmployee = New DataView(mdtEmployee, strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = New DataView(mdtEmployee, "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dtApprover As DataTable = Nothing
            'dtApprover = New DataView(mdtEmployee, "employeeunkid=" & CInt(cboApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            'If CInt(cboApprover.SelectedValue) > 0 Then
            '    If dtApprover IsNot Nothing Then
            '        Me.ViewState("Approver") = dtApprover.Rows(0)("employeecode").ToString & " - " & dtApprover.Rows(0)("name").ToString
            '    End If
            'End If
            'Nilay (01-Mar-2016) -- End

            dgvAEmployee.DataSource = dtEmployee
            dgvAEmployee.DataBind()

            If CInt(cboApprover.SelectedValue) > 0 Then
                objlnApproverTran._LoanApproverunkid = mintapproverunkid
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'objlnApproverTran.GetData()
                'Nilay (01-Mar-2016) -- End
                'mdtAssignedEmp = objlnApproverTran._TranDataTable
                Call Fill_Assigned_Employee()
            End If

            'Me.ViewState("dtEmployee") = dtEmployee

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Employee:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Employee:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApproverTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub Fill_Assigned_Employee(Optional ByVal blnIsFillter As Boolean = False)
        Try
            dtAssignEmpView = mdtAssignedEmp.DefaultView
            dtAssignEmpView.RowFilter = " AUD <> 'D' "
            dgvAssignedEmp.AutoGenerateColumns = False

            If blnIsFillter = True Then
                If txtAssignedEmpSearch.Text.Trim.Length > 0 Then
                    dtAssignEmpView.RowFilter = "ecode like '%" & txtAssignedEmpSearch.Text.Trim & "%' OR ename like '%" & txtAssignedEmpSearch.Text.Trim & "%' "
                End If
            End If

            dgvAssignedEmp.DataSource = dtAssignEmpView
            dgvAssignedEmp.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Assigned_Employee:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Fill_Assigned_Employee:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboApprover.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Loan Approver is mandatory information. Please provide Loan Approver to continue."), Me)
                cboApprover.Focus()
                Return False
            End If

            If CInt(cboLoanApproveLevel.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Loan Approver Level is mandatory information. Please provide Loan Approver Level to continue"), Me)
                cboLoanApproveLevel.Focus()
                Return False
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboUser.SelectedValue) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "User is mandatory information. Please provide User to continue"), Me)
            '    cboUser.Focus()
            '    Return False
            'End If
            If chkExternalApprover.Checked = False Then
            If CInt(cboUser.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "User is mandatory information. Please provide User to continue"), Me)
                cboUser.Focus()
                Return False
            End If
            End If
            'Nilay (01-Mar-2016) -- End

            

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Validation:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objlnApproverMaster As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End
        Try
            If mdtAssignedEmp Is Nothing Then Exit Sub
            Dim mdtRow As DataRow = Nothing
            Dim dtAssignEmp() As DataRow = Nothing
            dtAssignEmp = mdtAssignedEmp.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtAssignEmp.Length <= 0 Then
                mdtRow = mdtAssignedEmp.NewRow
                mdtRow.Item("ischeck") = False
                mdtRow.Item("lnapprovertranunkid") = -1
                mdtRow.Item("lnloanapproverunkid") = objlnApproverMaster._lnApproverunkid
                mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                mdtRow.Item("userunkid") = CInt(Session("UserId"))
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtRow.Item("ecode") = dRow.Item("employeecode")
                mdtRow.Item("ename") = dRow.Item("name")
                mdtRow.Item("edept") = dRow.Item("DeptName")
                mdtRow.Item("ejob") = dRow.Item("job_name")

                mdtAssignedEmp.Rows.Add(mdtRow)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Add_DataRow:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Add_DataRow:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApproverMaster = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub SetValue(ByVal objlnApproverMaster As clsLoanApprover_master)
        'Hemant (04 Sep 2020) -- [objlnApproverMaster]
        Try
            objlnApproverMaster._ApproverEmpunkid = CInt(cboApprover.SelectedValue)
            objlnApproverMaster._lnLevelunkid = CInt(cboLoanApproveLevel.SelectedValue)
            
            objlnApproverMaster._Isactive = True
            objlnApproverMaster._Userunkid = CInt(Session("UserId"))
            objlnApproverMaster._dtLoanSchemeMapping = dtLoanScheme

            If mintapproverunkid > 0 Then
                objlnApproverMaster._Isvoid = objlnApproverMaster._Isvoid
                objlnApproverMaster._VoidDateTime = objlnApproverMaster._VoidDateTime
                objlnApproverMaster._VoidReason = objlnApproverMaster._VoidReason
                objlnApproverMaster._VoidUserunkid = objlnApproverMaster._VoidUserunkid
                objlnApproverMaster._lnApproverunkid = mintapproverunkid
            Else
                objlnApproverMaster._Isvoid = False
                objlnApproverMaster._VoidDateTime = Nothing
                objlnApproverMaster._VoidReason = ""
                objlnApproverMaster._VoidUserunkid = -1
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'objlnApproverMaster._MappedUserunkid = CInt(cboUser.SelectedValue)
            If chkExternalApprover.Checked = False Then
                objlnApproverMaster._MappedUserunkid = CInt(cboUser.SelectedValue)
            Else
                'objlnApproverMaster._MappedUserunkid = mintApproverEmpunkid
                objlnApproverMaster._MappedUserunkid = CInt(cboApprover.SelectedValue)
            End If
            objlnApproverMaster._IsExternalApprover = chkExternalApprover.Checked
            'Nilay (01-Mar-2016) -- End

            'Nilay (05-May-2016) -- Start
            'Blank_ModuleName()
            'StrModuleName2 = "mnuLoan_Advance_Savings"
            'objlnApproverMaster._WebClientIP = Session("IP_ADD").ToString
            'objlnApproverMaster._WebFormName = "frmLoanApproverAddEdit"
            'objlnApproverMaster._WebHostName = Session("HOST_NAME").ToString
            'Nilay (05-May-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue(ByVal objlnApproverMaster As clsLoanApprover_master)
        'Hemant (04 Sep 2020) -- [objlnApproverMaster]
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objLoanMapping As New clsapprover_scheme_mapping
        'Hemant (04 Sep 2020) -- End
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'cboApprover.SelectedValue = CStr(objlnApproverMaster._ApproverEmpunkid)
            'cboLoanApproveLevel.SelectedValue = CStr(objlnApproverMaster._lnLevelunkid)
            'cboUser.SelectedValue = CStr(objlnApproverMaster._Userunkid)

            chkExternalApprover.Checked = objlnApproverMaster._IsExternalApprover
            Call chkExternalApprover_CheckedChanged(Nothing, Nothing)

            Dim objEmployee As New clsEmployee_Master

            If objlnApproverMaster._IsExternalApprover = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = objlnApproverMaster._ApproverEmpunkid
                Dim strAppName As String = objUser._Firstname + " " + objUser._Lastname
                If strAppName.Trim.Length > 0 Then
                    cboApprover.SelectedItem.Text = strAppName
                Else
                    cboApprover.SelectedItem.Text = objUser._Username
                End If
                mintApproverEmpunkid = objUser._Userunkid
                cboApprover.SelectedValue = mintApproverEmpunkid.ToString
                objUser = Nothing
            Else
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objlnApproverMaster._ApproverEmpunkid
                mintApproverEmpunkid = objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                cboApprover.SelectedValue = mintApproverEmpunkid.ToString
            End If

            cboLoanApproveLevel.SelectedValue = CStr(objlnApproverMaster._lnLevelunkid)
            cboUser.SelectedValue = CStr(objlnApproverMaster._MappedUserunkid)
            'Nilay (01-Mar-2016) -- End

            If mintapproverunkid > 0 Then
                dtLoanScheme = objLoanMapping.GetLoanSchemeForMapping(mintapproverunkid)
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                mstrEmployeeIDs = objlnApproverMaster.GetApproverEmployeeId(mintApproverEmpunkid, CBool(objlnApproverMaster._IsExternalApprover))
                'Nilay (01-Mar-2016) -- End
            End If

            If CInt(cboApprover.SelectedValue) > 0 Then
                Call Fill_Employee()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetValue:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objLoanMapping = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub GetApproverInfo()
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If Me.ViewState("Approver") IsNot Nothing Then
            '    objlblApprover.Text = CStr(Me.ViewState("Approver"))
            'End If
            'Nilay (01-Mar-2016) -- End

            objlblApprover.Text = cboApprover.SelectedItem.Text

            If CInt(cboLoanApproveLevel.SelectedValue) > 0 Then
                objlblApproverLevel.Text = cboLoanApproveLevel.SelectedItem.ToString
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetApproverInfo:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetApproverInfo:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillLoanScheme()
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objLoanMapping As New clsapprover_scheme_mapping
        'Hemant (04 Sep 2020) -- End
            Dim objLoanScheme As New clsLoan_Scheme
            Dim dsFill As DataSet = Nothing

        Try
          
            If dtLoanScheme.Rows.Count <= 0 Then
                dsFill = objLoanScheme.getComboList(False, "List")
            Else
                Dim dsLoanScheme As New DataSet
                dsLoanScheme.Tables.Add(dtLoanScheme)
                dsFill = dsLoanScheme
            End If
            'If mintapproverunkid > 0 Then
            '    dtLoanScheme = objLoanMapping._dtLoanScheme
            'Else
            '    If dtLoanScheme.Rows.Count <= 0 Then
            '        dsFill = objLoanScheme.getComboList(False, "List")
            '    Else
            '        Dim dsLoanScheme As New DataSet
            '        dsLoanScheme.Tables.Add(dtLoanScheme)
            '        dsFill = dsLoanScheme
            '    End If
            'End If

            For Each dr As DataRow In dsFill.Tables(0).Rows

                Dim drRow As DataRow = objLoanMapping._dtLoanScheme.NewRow()

                drRow("Loanschememappingunkid") = objLoanMapping.GetLoanSchemeMappingUnkId(mintapproverunkid, CInt(dr("loanschemeunkid")))
                If mintapproverunkid > 0 AndAlso dtLoanScheme Is Nothing Then
                    drRow("ischecked") = objLoanMapping.isExist(mintapproverunkid, CInt(dr("loanschemeunkid")))
                Else
                    If dsFill.Tables(0).Columns.Contains("ischecked") Then
                        drRow("ischecked") = dr("ischecked")
                    Else
                        drRow("ischecked") = False
                    End If
                End If

                drRow("loanschemeunkid") = dr("loanschemeunkid").ToString()
                drRow("code") = dr("Code").ToString()
                drRow("name") = dr("name").ToString()
                objLoanMapping._dtLoanScheme.Rows.Add(drRow)
            Next
            objLoanMapping._dtLoanScheme.AcceptChanges()

            dtLoanScheme = objLoanMapping._dtLoanScheme

            dgLoanScheme.AutoGenerateColumns = False
            dgLoanScheme.DataSource = dtLoanScheme
            dgLoanScheme.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillLoanScheme:- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillLoanScheme:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objLoanMapping = Nothing
            objLoanScheme = Nothing
            If IsNothing(dsFill) = False Then
                dsFill.Clear()
                dsFill = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub


#End Region

#Region "Button's Event"

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("lnapproverunkid") = Nothing
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            dtLoanScheme = Nothing
            dtEmployee = Nothing
            dtAssignEmpView = Nothing
            mdtEmployee = Nothing
            mdtAssignedEmp = Nothing
            'Nilay (01-Mar-2016) -- End
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approver/wPg_LoanApproversList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() = False Then Exit Sub

            'Dim dtEmployee As DataTable = CType(Me.ViewState("dtEmployee"), DataTable)
            Dim drCheck() As DataRow = dtEmployee.Select("ischeck = true")

            For i As Integer = 0 To drCheck.Length - 1
                Call Add_DataRow(drCheck(i))
            Next
            Call Fill_Assigned_Employee()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAdd_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnAdd_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnDeleteA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteA.Click
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If ViewState("mdtAssignedEmp") Is Nothing Then Exit Sub
            If mdtAssignedEmp Is Nothing Then Exit Sub
            'Nilay (01-Mar-2016) -- End
            popYesNo.Title = "Aruti"
            Language.setLanguage(mstrModuleName)
            popYesNo.Message = Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete Selected Employee?")
            popYesNo.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDeleteA_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnDeleteA_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objlnApproverMaster As New clsLoanApprover_master
        'Hemant (04 Sep 2020) -- End
        Try
            If Validation() = False Then Exit Sub
            Call SetValue(objlnApproverMaster)
            'Hemant (04 Sep 2020) -- [objlnApproverMaster]

            Dim count As Integer = CInt(mdtAssignedEmp.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtAssignedEmp.Rows.Count = 0 OrElse count = 0 OrElse dgvAssignedEmp.Items.Count = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Employee is compulsory information.Please Check atleast One Employee."), Me)
                dgvAEmployee.Focus()
                Exit Sub
            End If

            If CBool(Session("LoanApprover_ForLoanScheme")) = True Then
                If dtLoanScheme Is Nothing Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Loan Scheme Mapping is compulsory information.Please map loan scheme for this approver."), Me)
                    Exit Sub
                End If
            End If

            With objlnApproverMaster
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            If mintapproverunkid > 0 Then
                If objlnApproverMaster.Update(mdtAssignedEmp) Then
                    mintapproverunkid = -1
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Loan Approver updated successfully."), Me)
                Else
                    DisplayMessage.DisplayMessage(objlnApproverMaster._Message, Me)
                    Exit Sub
                End If
            Else
                If objlnApproverMaster.Insert(mdtAssignedEmp) Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Loan Approver saved successfully."), Me)
                Else
                    DisplayMessage.DisplayMessage(objlnApproverMaster._Message, Me)
                    Exit Sub
                End If
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'dtLoanScheme = Nothing
            'dtEmployee = Nothing
            'dtAssignEmpView = Nothing
            'mdtEmployee = Nothing
            'mdtAssignedEmp = Nothing
            'Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Approver/wPg_LoanApproversList.aspx")
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSave_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApproverMaster = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub popYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popYesNo.buttonYes_Click
        Try
            dtAssignEmpView = mdtAssignedEmp.DefaultView
            Dim drRow() As DataRow = dtAssignEmpView.Table.Select("ischeck=True AND AUD <> 'D' ")
            If drRow.Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please check atleast one of the employee to unassigned."), Me)
                Exit Sub
            End If

            Dim blnFlag As Boolean = False

            'Pinkal (26-Sep-2017) -- Start
            'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
            Dim objLoan As New clsProcess_pending_loan
            Dim mblnFlag As Boolean = True

            For i As Integer = 0 To drRow.Length - 1
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    If objLoan.GetApproverPendingLoanFormCount(mintapproverunkid, drRow(i)("employeeunkid").ToString()) <= 0 Then
                    drRow(i).Item("AUD") = "D"
                    drRow(i).Item("isvoid") = True
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'drRow(i).Item("voiddatetime") = DateAndTime.Now.Date
                    drRow(i).Item("voiddatetime") = DateAndTime.Now
                    'Nilay (01-Mar-2016) -- End
                    drRow(i).Item("voiduserunkid") = Session("UserId")
                    drRow(i).Item("voidreason") = ""
                    Else
                        mblnFlag = False
                    End If
                    drRow(i).AcceptChanges()
                End If
            Next
            dtAssignEmpView.Table.AcceptChanges()

            If mblnFlag = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "This Employee(s) has pending loan application(s).You cannot delete this employee(s)."), Me)
            End If
            objLoan = Nothing

            'Pinkal (26-Sep-2017) -- End

            dtAssignEmpView.Table.AcceptChanges()
            txtAssignedEmpSearch.Text = ""
            Call Fill_Assigned_Employee()



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popYesNo_buttonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("popYesNo_buttonYes_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call Fill_Employee()
        Catch ex As Exception
            DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            popupLoanSchemeMapping.Show()
            If dtLoanScheme Is Nothing Then Exit Sub

            Dim drRow() As DataRow = dtLoanScheme.Select("ischecked=true")
            If drRow.Length <= 0 Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Loan Scheme is compulsory information.Please Select one Loan Scheme."), Me)
                dgLoanScheme.Focus()
                Exit Sub
            Else
                Me.ViewState("dtLoanScheme") = dtLoanScheme
            End If

            popupLoanSchemeMapping.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnOK_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnOK_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ComboBox Events"

    Protected Sub cboApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprover.SelectedIndexChanged
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objlnApproverMaster As New clsLoanApprover_master
        Dim dsCombo As DataSet
        Dim objUser As New clsUserAddEdit
        'Hemant (04 Sep 2020) -- End
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'If CInt(cboApprover.SelectedValue) > 0 Then
            '    cboLoanApproveLevel.SelectedIndex = 0
            '    Call Fill_Employee()
            'End If

            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            'Dim dsCombo As DataSet
            'Dim objUser As New clsUserAddEdit
            'Hemant (04 Sep 2020) -- End         

            dsCombo = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(702), CInt(Session("Fin_year")), True)

            Dim objoption As New clsPassowdOptions

            If objoption._IsEmployeeAsUser Then

                Dim mintuserid As Integer = objUser.Return_UserId(CInt(cboApprover.SelectedValue), CInt(Session("CompanyUnkId")))
                Dim drrow() As DataRow = dsCombo.Tables("User").Select("userunkid = " & mintuserid)
                If drrow.Length > 0 Then
                    cboUser.SelectedValue = mintuserid.ToString
                Else
                    cboUser.SelectedValue = "0"
                End If
            End If

            mstrEmployeeIDs = objlnApproverMaster.GetApproverEmployeeId(CInt(cboApprover.SelectedValue), chkExternalApprover.Checked)

            If CInt(cboApprover.SelectedValue) > 0 Then
                cboLoanApproveLevel.SelectedIndex = 0
                Call Fill_Employee()
            End If
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboApprover_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboApprover_SelectedIndexChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objlnApproverMaster = Nothing
            objUser = Nothing
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Events"
    Protected Sub txtSearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            If dtEmployee IsNot Nothing Then
                Dim dvEmployee As DataView = dtEmployee.DefaultView
                If dvEmployee.Table.Rows.Count > 0 Then
                    dvEmployee.RowFilter = "employeecode like '%" & txtSearchEmp.Text.Trim & "%' OR name like '%" & txtSearchEmp.Text.Trim & "%' "
                    dgvAEmployee.DataSource = dvEmployee.ToTable
                    dgvAEmployee.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearchEmp_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtSearchEmp_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtAssignedEmpSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAssignedEmpSearch.TextChanged
        Try
            Call Fill_Assigned_Employee(True)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtAssignedEmpSearch_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtAssignedEmpSearch_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtSearchScheme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchScheme.TextChanged
        Try
            If dtLoanScheme IsNot Nothing Then
                Dim dvLoanScheme As DataView = dtLoanScheme.DefaultView
                If dvLoanScheme.Table.Rows.Count > 0 Then
                    dvLoanScheme.RowFilter = "code like '%" & txtSearchScheme.Text.Trim & "%' OR name like '%" & txtSearchScheme.Text.Trim & "%' "
                    dgLoanScheme.DataSource = dvLoanScheme.ToTable
                    dgLoanScheme.DataBind()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearchScheme_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("txtSearchScheme_TextChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            popupLoanSchemeMapping.Show()
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Protected Sub ChkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dtEmployee As DataTable = CType(Me.ViewState("dtEmployee"), DataTable)
            'Nilay (01-Mar-2016) -- End

            If dgvAEmployee.Items.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgvAEmployee.Items
                CType(item.Cells(0).FindControl("ChkgvSelect"), CheckBox).Checked = chkBox.Checked
                Dim xrow() As DataRow = dtEmployee.Select("employeeunkid=" & item.Cells(3).Text)
                If xrow.Length > 0 Then
                    xrow(0).Item("ischeck") = chkBox.Checked
                End If
            Next
            dtEmployee.AcceptChanges()
            'Me.ViewState("dtEmployee") = dtEmployee
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            'Dim dtEmployee As DataTable = Me.ViewState("dtEmployee")
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim xrow() As DataRow = dtEmployee.Select("employeeunkid=" & item.Cells(3).Text)
            If xrow.Length > 0 Then
                xrow(0).Item("ischeck") = chkBox.Checked
            End If
            dtEmployee.AcceptChanges()
            'Me.ViewState("dtEmployee") = dtEmployee
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkAsgAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            If dgvAssignedEmp.Items.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgvAssignedEmp.Items
                CType(item.Cells(0).FindControl("ChkAsgSelect"), CheckBox).Checked = chkAll.Checked
                Dim aRow() As DataRow = mdtAssignedEmp.Select("employeeunkid=" & item.Cells(5).Text)
                If aRow.Length > 0 Then
                    aRow(0).Item("ischeck") = chkAll.Checked
                End If
            Next

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkAsgAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkAsgAll_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkAsgSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            'Dim aRow() As DataRow = dtAssignEmp.Select("employeeunkid=" & item.Cells(5).Text)
            Dim aRow() As DataRow = mdtAssignedEmp.Select("employeeunkid=" & item.Cells(5).Text)

            If aRow.Length > 0 Then
                aRow(0).Item("ischeck") = chkBox.Checked
            End If

            mdtAssignedEmp.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkAsgSelect_CheckedChanged" & ex.Message, Me)
            DisplayMessage.DisplayError("ChkAsgSelect_CheckedChanged" & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkAllLoanScheme_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dtLoanScheme As DataTable = CType(Me.ViewState("dtLoanScheme"), DataTable)
            'Nilay (01-Mar-2016) -- End

            If dgLoanScheme.Items.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgLoanScheme.Items
                CType(item.Cells(0).FindControl("ChkdgSelectLoanScheme"), CheckBox).Checked = chkBox.Checked
                Dim dRow() As DataRow = dtLoanScheme.Select("loanschemeunkid=" & item.Cells(4).Text)
                If dRow.Length > 0 Then
                    dRow(0).Item("ischecked") = chkBox.Checked
                End If
            Next
            dtLoanScheme.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkAllLoanScheme_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkAllLoanScheme_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            popupLoanSchemeMapping.Show()
        End Try
    End Sub

    Protected Sub ChkdgSelectLoanScheme_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Dim dtLoanScheme As DataTable = CType(Me.ViewState("dtLoanScheme"), DataTable)
            'Nilay (01-Mar-2016) -- End

            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim dRow() As DataRow = dtLoanScheme.Select("loanschemeunkid=" & item.Cells(4).Text)
            If dRow.Length > 0 Then
                dRow(0).Item("ischecked") = chkBox.Checked
            End If
            dtLoanScheme.AcceptChanges()
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'Me.ViewState("dtLoanScheme") = dtLoanScheme
            'Nilay (01-Mar-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ChkdgSelectLoanScheme_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("ChkdgSelectLoanScheme_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            popupLoanSchemeMapping.Show()
        End Try
    End Sub

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Protected Sub chkExternalApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkExternalApprover.CheckedChanged
        Try
            If chkExternalApprover.Checked = True Then
                lblUser.Visible = False
                cboUser.Visible = False
            Else
                lblUser.Visible = True
                cboUser.Visible = True
                cboUser.SelectedValue = "0"
            End If
            Call FillCombo()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkExternalApprover_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkExternalApprover_CheckedChanged:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Mar-2016) -- End

#End Region

#Region "Link Button Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkAllocation_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            mstrAdvanceFilter = ""
            txtSearchEmp.Text = ""

            Call Fill_Employee()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkReset_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkMapLoanScheme_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMapLoanScheme.Click
        Try
            If CInt(cboApprover.SelectedValue) > 0 AndAlso CInt(cboLoanApproveLevel.SelectedValue) > 0 Then
                txtSearchScheme.Text = ""
                Call GetApproverInfo()
                If dtLoanScheme IsNot Nothing OrElse dtLoanScheme.Rows.Count > 0 Then
                    Call FillLoanScheme()
                End If

                popupLoanSchemeMapping.Show()

            Else
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please Select Name or Level to do futher operation on it."), Me)
                'dgLoanScheme.DataSource = dtLoanScheme
                'dgLoanScheme.DataBind()
                Exit Sub
            End If
           
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkMapLoanScheme_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError("lnkMapLoanScheme_Click:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Grid's Events"

    Protected Sub dgvAEmployee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvAEmployee.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                If dtEmployee IsNot Nothing AndAlso dtEmployee.Rows.Count > 0 Then
                    Dim dRow() As DataRow = CType(dtEmployee, DataTable).Select("employeecode = '" & e.Item.Cells(1).Text & "'")
                    If dRow.Length > 0 Then
                        If CBool(dRow(0).Item("ischeck")) = True Then
                            CType(e.Item.Cells(0).FindControl("ChkgvSelect"), CheckBox).Checked = CBool(dRow(0).Item("ischeck"))
                        End If
                    End If
                End If
            Else
                Exit Sub
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvAEmployee_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvAEmployee_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvAssignedEmp_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvAssignedEmp.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                If mdtAssignedEmp IsNot Nothing AndAlso mdtAssignedEmp.Rows.Count > 0 Then
                    Dim dRow() As DataRow = CType(mdtAssignedEmp, DataTable).Select("ecode= '" & e.Item.Cells(1).Text & "'")
                    If dRow.Length > 0 Then
                        If CBool(dRow(0).Item("ischeck")) = True Then
                            CType(e.Item.Cells(0).FindControl("ChkAsgSelect"), CheckBox).Checked = CBool(dRow(0).Item("ischeck"))
                        End If
                    End If
                End If
            Else
                Exit Sub
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvAssignedEmp_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgvAssignedEmp_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgLoanScheme_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgLoanScheme.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                If dtLoanScheme IsNot Nothing AndAlso dtLoanScheme.Rows.Count > 0 Then
                    Dim drRow() As DataRow = CType(dtLoanScheme, DataTable).Select("loanschemeunkid= '" & e.Item.Cells(4).Text & "'")
                    If drRow.Length > 0 Then
                        If CBool(drRow(0).Item("ischecked")) = True Then
                            CType(e.Item.Cells(0).FindControl("ChkdgSelectLoanScheme"), CheckBox).Checked = CBool(drRow(0).Item("ischecked"))
                        End If
                        'Dim chk As CheckBox = CType(e.Item.Cells(0).FindControl("ChkdgSelectLoanScheme"), CheckBox)
                        'chk.Checked = CBool(dtLoanScheme.Rows(e.Item.ItemIndex)("ischecked"))
                    End If
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgLoanScheme_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgLoanScheme_ItemDataBound:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, lblPageHeader.Text)
            Me.lblpopupHeader.Text = Language._Object.getCaption(mstrModuleName1, lblpopupHeader.Text)
            
            Me.lblInfo.Text = Language._Object.getCaption("gbInfo", Me.lblInfo.Text)
            Me.lblAssignedEmployee.Text = Language._Object.getCaption("gbAssignedEmployee", Me.lblAssignedEmployee.Text)
            Me.lblTitle.Text = Language._Object.getCaption("gbLeavedetail", Me.lblTitle.Text)
            Me.lblApproveLevel.Text = Language._Object.getCaption(Me.lblApproveLevel.ID, Me.lblApproveLevel.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblLoanApproverName.Text = Language._Object.getCaption(Me.lblLoanApproverName.ID, Me.lblLoanApproverName.Text)
            Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.ID, Me.lblUser.Text)
            Me.lnkMapLoanScheme.Text = Language._Object.getCaption(Me.lnkMapLoanScheme.ID, Me.lnkMapLoanScheme.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.ID, Me.lblApproverLevel.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
            Me.btnDeleteA.Text = Language._Object.getCaption(Me.btnDeleteA.ID, Me.btnDeleteA.Text).Replace("&", "")
            Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.ID, Me.btnOK.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvAEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvAEmployee.Columns(1).FooterText, Me.dgvAEmployee.Columns(1).HeaderText)
            Me.dgvAEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvAEmployee.Columns(2).FooterText, Me.dgvAEmployee.Columns(2).HeaderText)

            Me.dgvAssignedEmp.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(1).FooterText, Me.dgvAssignedEmp.Columns(1).HeaderText)
            Me.dgvAssignedEmp.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(2).FooterText, Me.dgvAssignedEmp.Columns(2).HeaderText)
            Me.dgvAssignedEmp.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(3).FooterText, Me.dgvAssignedEmp.Columns(3).HeaderText)
            Me.dgvAssignedEmp.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvAssignedEmp.Columns(4).FooterText, Me.dgvAssignedEmp.Columns(4).HeaderText)

            Me.dgLoanScheme.Columns(1).HeaderText = Language._Object.getCaption(Me.dgLoanScheme.Columns(1).FooterText, Me.dgLoanScheme.Columns(1).HeaderText)
            Me.dgLoanScheme.Columns(2).HeaderText = Language._Object.getCaption(Me.dgLoanScheme.Columns(2).FooterText, Me.dgLoanScheme.Columns(2).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError("SetLanguage:- " & Ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
