﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeExperience.aspx.vb"
    Inherits="HR_wPg_EmployeeExperience" MasterPageFile="~/home.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }    
    
     function IsValidAttach() {
         return true;
     }
    
    </script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, event) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 80%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Job History And Experience Form"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Job History And Experience Form Add/Edit"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 50%" colspan="2" rowspan="3">
                                                <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1">
                                                    <cc1:TabPanel ID="tabpBenefit" runat="server" HeaderText="Benefits">
                                                        <ContentTemplate>
                                                            <asp:Panel ID="PnlBenefits" BorderWidth="0px" runat="server" ScrollBars="Auto" Height="62px">
                                                                <asp:CheckBoxList ID="ChkBenefits" runat="server" Style="text-align: left;" />
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </cc1:TabPanel>
                                                    <cc1:TabPanel ID="tabpOtherBenefit" runat="server" HeaderText="Other Benefits">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtOtherBenefits" runat="server" Height="56px" TextMode="MultiLine"></asp:TextBox>
                                                        </ContentTemplate>
                                                    </cc1:TabPanel>
                                                </cc1:TabContainer>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblInstitution" runat="server" Text="Company"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtCompany" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtAdress" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtpStartdate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Currency Sign"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtSign" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtpEnddate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLastGrossPay" runat="server" Text="Last Gross Pay"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtGrossPay" runat="server" MaxLength="28" Style="text-align: right;"
                                                    onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtOldJob" runat="server" ValidationGroup="Save"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOldJob"
                                                    CssClass="ErrorControl" Display="Dynamic" ValidationGroup="Save" ErrorMessage="Job is compulsory information. Please give Job to continue. "
                                                    ForeColor="White" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </td>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 30%">
                                                <asp:CheckBox ID="chkCanContatEmployee" runat="server" Text="Can Contact Previous Employer" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblSupervisor" runat="server" Text="Supervisors"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtSuperVisors" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblContacPerson" runat="server" Text="Contact Person"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtContactPerson" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtRemark" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblContactNo" runat="server" Text="Contact Address"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtContactAddress" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblMemo" runat="server" Text="Memo"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtMemo" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="LblLeaveReason" runat="server" Text="Leave Reason"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="TxtLeaveReason" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="Label1" runat="server" Text="Remark :"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%">
                                                        <asp:TextBox ID="TextBox1" runat="server" Height="45px" TextMode="MultiLine"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" width="100%">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 11%; padding: 0">
                                                                    <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:DropDownList ID="cboDocumentType" runat="server" Width="310px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="width: 1%">
                                                                    <asp:Panel ID="pnl_ImageAdd" runat="server" Width="100%">
                                                                        <div id="fileuploader">
                                                                            <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                                                value="Browse" />
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse"
                                                                        OnClick="btnSaveAttachment_Click" />
                                                                </td>
                                                                <td style="width: 15%">
                                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table style="width: 100%;">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:Panel ID="pnl_dgvExperience" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                                        <asp:DataGrid ID="dgvExperienceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateColumn FooterText="objcohDelete">
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                                    ItemStyle-Font-Size="22px">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Download">
                                                                                                            <i class="fa fa-download"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                                    Visible="false" />
                                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                                    Visible="false" />
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" ValidationGroup="Save" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
			});
			function ImageLoad(){
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPg_EmployeeExperience.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAddAttachment.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			$('input[type=file]').live("click",function(){
			    return IsValidAttach();
			});
    </script>

</asp:Content>
