﻿<%@ Page Title="Personal Info" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmployeePersonal.aspx.vb" Inherits="wPgEmployeePersonal" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%--S.SANDEEP |26-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%--S.SANDEEP |26-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        //        var e = event || evt; // for trans-browser compatibility
        //        var charCode = e.which || e.keyCode;
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
                    return false;
        
        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;
        
        return true;
    }
    
   function onlyNum(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        var cWeight = document.getElementById('<%=txtWeight.ClientID%>').value;
        if (cWeight.length > 0)
            if (charCode == 46)
                if (cWeight.indexOf(".") > 0)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }    
    </script>
    <%--S.SANDEEP |26-APR-2019| -- START    --%>

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Personal Info"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div7" class="panel-default">
                                <div id="Div8" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblpnl_Header" runat="server" Text="Employee Personal Detail"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div9" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEmployee" Style="margin-left: 10px" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 36%">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td style="width: 51%">
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%">
                                                <%--Gajanan [17-April-2019] -- Start--%>
                                                <%--<div id="FilterCriteria" class="panel-default">--%>
                                                <asp:Panel ID="pnlBirthInfo" runat="server" class="panel-default">
                                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="gbBirthInfo" runat="server" Text="Birth Info."></asp:Label>
                                                        </div>
                                                        <%--S.SANDEEP |26-APR-2019| -- START--%>
                                                        <asp:LinkButton ID="lnkScanAttachBirthInfo" runat="server" Text="Attach Document"
                                                            Style="padding: 2px; font-weight: bold; margin-top: -3px; float: right;" Font-Underline="false"></asp:LinkButton>
                                                        <%--'S.SANDEEP |26-APR-2019| -- END--%>
                                                        <asp:Label ID="lblbirthinfoapproval" Visible="false" runat="server" Text="Original Details"
                                                            BackColor="PowderBlue" Style="padding: 2px; font-weight: bold; margin-top: -3px;
                                                            float: right"></asp:Label>
                                                    </div>
                                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                                        <table>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboBirthCounty" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblState" runat="server" Text="State"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboBirthState" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblCity" runat="server" Text="City"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboBirthCity" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblWard" runat="server" Text="Ward"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:TextBox ID="txtBirthWard" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblCertificateNo" runat="server" Text="Cert. No."></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:TextBox ID="txtBirthCertNo" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblVillage" runat="server" Text="Village"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:TextBox ID="txtBirthVillage" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <%--Gajanan 17.9.2019 Start--%>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblBirthTown" runat="server" Text="Town1"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboBrithTown" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblBirthChiefdom" runat="server" Text="Chiefdom"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboBrithChiefdom" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblBirthVillage" runat="server" Text="Village1"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboBrithVillage" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <%--Gajanan 17.9.2019 End--%>
                                                        </table>
                                                    </div>
                                                    <%--</div>--%>
                                                </asp:Panel>
                                                <%--Gajanan [17-April-2019] -- End--%>
                                            </td>
                                            <td style="width: 50%">
                                                <cc1:TabContainer ID="tbcPermit" runat="server">
                                                    <cc1:TabPanel ID="tabpWP" runat="server" TabIndex="0" HeaderText="Work Pemit">
                                                        <ContentTemplate>
                                                <div id="Div2" class="panel-default">
                                                    <div id="Div3" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="gbWorkPermit" runat="server" Text="Work Permit"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div4" class="panel-body-default">
                                                        <table>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblWorkPermitNo" runat="server" Text="Permit No"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:TextBox ID="txtWorkPermitNo" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblIssueDate" runat="server" Text="Issue Date"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <uc2:DateCtrl ID="dtIssueDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblIssueCountry" runat="server" Text="Country"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:DropDownList ID="cboIssueCountry" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblExpiryDate" runat="server" Text="Expiry Date"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <uc2:DateCtrl ID="dtExpiryDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 25%">
                                                                    <asp:Label ID="lblPlaceOfIssue" runat="server" Text="Issue Place"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                    <asp:TextBox ID="txtIssuePlace" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                </td>
                                                                <td align="left" style="width: 25%">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                                        </ContentTemplate>
                                                    </cc1:TabPanel>
                                                    <cc1:TabPanel ID="tabpRP" runat="server" TabIndex="1" HeaderText="Resident Pemit">
                                                        <ContentTemplate>
                                                            <div id="Div10" class="panel-default">
                                                                <div id="Div11" class="panel-heading-default">
                                                                    <div style="float: left;">
                                                                        <asp:Label ID="lblResidentPermitNo" runat="server" Text="Resident Permit"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div id="Div12" class="panel-body-default">
                                                                    <table>
                                                                        <tr style="width: 100%">
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:Label ID="Label2" runat="server" Text="Permit No"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:TextBox ID="txtResidentPermitNo" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:Label ID="lblResidentIssueDate" runat="server" Text="Issue Date"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <uc2:DateCtrl ID="dtpResidentIssueDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:Label ID="lblResidentIssueCountry" runat="server" Text="Country"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:DropDownList ID="cboResidentIssueCountry" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:Label ID="lblResidentExpiryDate" runat="server" Text="Expiry Date"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <uc2:DateCtrl ID="dtpResidentExpiryDate" runat="server" AutoPostBack="false" ReadonlyDate="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:Label ID="lblResidentIssuePlace" runat="server" Text="Issue Place"></asp:Label>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                                <asp:TextBox ID="txtResidentIssuePlace" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                            </td>
                                                                            <td align="left" style="width: 25%">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </ContentTemplate>
                                                    </cc1:TabPanel>
                                                </cc1:TabContainer>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="2">
                                                <%--Gajanan [17-April-2019] -- Start--%>
                                                <%--<div id="Div1" class="panel-default">--%>
                                                <asp:Panel ID="pnlOtherinfo" runat="server" class="panel-default">
                                                    <div id="Div5" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="gbOtherInfo" runat="server" Text="Other Info."></asp:Label>
                                                        </div>
                                                        <%--S.SANDEEP |26-APR-2019| -- START--%>
                                                        <asp:LinkButton ID="lnkAttachOtherInfo" runat="server" Text="Attach Document" Style="padding: 2px;
                                                            font-weight: bold; margin-top: -3px; float: right;" Font-Underline="false"></asp:LinkButton>
                                                        <%--S.SANDEEP |26-APR-2019| -- END--%>
                                                        <asp:Label ID="lblotherinfoapproval" Visible="false" runat="server" Text="Pending Approval"
                                                            BackColor="PowderBlue" Style="padding: 2px; font-weight: bold; margin-top: -3px;
                                                            float: right"></asp:Label>
                                                    </div>
                                                    <div id="Div6" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblComplexion" runat="server" Text="Complexion"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboComplexion" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblExtraTel" runat="server" Text="Tel. Ext."></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:TextBox ID="txtExTel" runat="server" ></asp:TextBox>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblBloodGroup" runat="server" Text="Blood Group"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboBloodGrp" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblEyeColor" runat="server" Text="Eye Color"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboEyeColor" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblLanguage1" runat="server" Text="Language1"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboLang1" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblLanguage2" runat="server" Text="Language2"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboLang2" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblLanguage3" runat="server" Text="Language3"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboLang3" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblLanguage4" runat="server" Text="Language4"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboLang4" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblEthinCity" runat="server" Text="Ethnicity"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboEthnicity" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboNationality" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblReligion" runat="server" Text="Religion"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboReligion" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblHair" runat="server" Text="Hair"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboHair" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblHeight" runat="server" Text="Height"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:TextBox ID="txtHeight" runat="server" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblWeight" runat="server" Text="Weight"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:TextBox ID="txtWeight" runat="server" onKeypress="return onlyNum();"></asp:TextBox>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblMaritalStatus" runat="server" Text="Marital Status"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:DropDownList ID="cboMaritalStatus" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <asp:Label ID="lblMaritalDate" runat="server" Text="Marriage Date"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                    <uc2:DateCtrl ID="dtMarriedDate" runat="server" ReadonlyDate="False" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" valign="top" style="width: 12%">
                                                                    <asp:Label ID="lblAllergies" runat="server" Text="Allergies"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%" rowspan="4">
                                                                    <asp:Panel ID="pnlallergies" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                                        Height="111px" ScrollBars="Auto">
                                                                        <asp:CheckBoxList ID="chkLstAllergies" runat="server" Height="111px" RepeatLayout="Flow"
                                                                            BorderStyle="None">
                                                                        </asp:CheckBoxList>
                                                                    </asp:Panel>
                                                                </td>
                                                                <td align="left" valign="top" style="width: 12%">
                                                                    <asp:Label ID="lblDisabilities" runat="server" Text="Disabilities"></asp:Label>
                                                                </td>
                                                                <td align="left" style="width: 12%" rowspan="4">
                                                                    <asp:Panel ID="pnlDisabilities" runat="server" BorderStyle="Solid" BorderWidth="1px"
                                                                        Height="111px" ScrollBars="Auto">
                                                                        <asp:CheckBoxList ID="chkLstDisabilities" runat="server" Height="111px" RepeatLayout="Flow"
                                                                            BorderStyle="None">
                                                                        </asp:CheckBoxList>
                                                                    </asp:Panel>
                                                                </td>
                                                                <td align="left" valign="top" style="width: 12%">
                                                                    <asp:Label ID="lblSportsHobbies" runat="server" Text="Sport/Hobbies"></asp:Label>
                                                                </td>
                                                                <td align="left" colspan="3" rowspan="4">
                                                                    <asp:TextBox ID="txtSpHob" runat="server" Height="111px" TextMode="MultiLine"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                                <td align="left" style="width: 12%">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <%--</div>--%>
                                                    <%--S.SANDEEP |26-APR-2019| -- START--%>
                                                    <div id="ScanAttachment">
                                                        <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                                                            TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                                                            CancelControlID="hdf_ScanAttchment">
                                                        </cc1:ModalPopupExtender>
                                                        <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                                                            Style="display: none;">
                                                            <div class="panel-primary" style="margin: 0px">
                                                                <div class="panel-heading">
                                                                    <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                                                    <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"></asp:Label>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <div id="Div36" class="panel-default">
                                                                        <div id="Div37" class="panel-body-default">
                                                                            <table style="width: 100%">
                                                                                <tr style="width: 100%">
                                                                                    <td style="width: 30%">
                                                                                        <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                                    </td>
                                                                                    <td style="width: 40%">
                                                                                        <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                                                            Width="200px">
                                                                                        </asp:DropDownList>
                                                                                    </td>
                                                                                    <td style="width: 30%">
                                                                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                                            <div id="fileuploader">
                                                                                                <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse"
                                                                                                    onclick="return IsValidAttach();" />
                                                </div>
                                                                                        </asp:Panel>
                                                                                        <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                                                            Text="Browse" />
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="width: 100%">
                                                                                    <td colspan="3" style="width: 100%">
                                                                                        <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%--0--%>
                                                                                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                                                <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <%--1--%>
                                                                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                                <%--2--%>
                                                                                                <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                                                                <%--3--%>
                                                                                                <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                                                                <%--4--%>
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <div class="btn-default">
                                                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                                <div style="float: left">
                                                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                                </div>
                                                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                                <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btnDefault" />
                                                                                <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                                                <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                                                            </div>
                                                                        </div>
                                                </div>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                        <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                                                            IsFireButtonNoClick="false" />
                                                    </div>
                                                    <%--S.SANDEEP |26-APR-2019| -- END--%>
                                                </asp:Panel>
                                                <%--Gajanan [17-April-2019] -- End--%>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnsave" runat="server" CssClass="btndefault" Text="Update" Width="77px" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" Width="77px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="dgv_Attchment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmployeePersonal.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            debugger;
            return IsValidAttach();
        });
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
</asp:Content>
