﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Threading

#End Region

Partial Class wPgEmployeeAllocation
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    'Private objEmployee As New clsEmployee_Master


    'S.SANDEEP [ 04 JULY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Dim objEmp As New clsEmployee_Master

    'S.SANDEEP [ 04 JULY 2012 ] -- END


    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicAllocNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objGradeGrp As New clsGradeGroup
        Dim objClassGrp As New clsClassGroup
        'Dim objClass As New clsClass
        Dim objCostCenter As New clscostcenter_master
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Try
            dsCombos = objStation.getComboList("Station", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Station")
                .DataBind()
            End With
            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            With cboDeptGrp
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("DeptGrp")
                .DataBind()
            End With
            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Department")
                .DataBind()
            End With

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSecGroup
                .DataValueField = "sectiongroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objSection.getComboList("Section", True)
            With cboSection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Section")
                .DataBind()
            End With

            dsCombos = objUnitGroup.getComboList("List", True)
            With cboUnitGrp
                .DataValueField = "unitgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnit
                .DataValueField = "unitunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Unit")
                .DataBind()
            End With

            dsCombos = objTeam.getComboList("List", True)
            With cboTeam
                .DataValueField = "teamunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .DataValueField = "jobgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("JobGrp")
                .DataBind()
            End With
            dsCombos = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Job")
                .DataBind()
            End With

            dsCombos = objGradeGrp.getComboList("GradeGrp", True)
            With cboGradeGrp
                .DataValueField = "gradegroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("GradeGrp")
                .DataBind()
            End With

            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            With cboClassGrp
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("ClassGrp")
                .DataBind()
            End With

            'dsCombos = objClass.getComboList("Class", True)
            'With cboClass
            '    .DataValueField = "classesunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("Class")
            '    .DataBind()
            'End With

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostcenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
                .DataBind()
            End With

        Catch ex As Exception
            msg.DisplayError("Procedure FillCombo : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetInfo()
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = Session("Employeeunkid")
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End

            txtEmployee.Text = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname

            cboBranch.SelectedValue = CStr(objEmp._Stationunkid)
            Call cboBranch_SelectedIndexChanged(Nothing, Nothing)

            cboDeptGrp.SelectedValue = objEmp._Deptgroupunkid.ToString
            Call cboDeptGrp_SelectedIndexChanged(Nothing, Nothing)

            cboDepartment.SelectedValue = objEmp._Departmentunkid.ToString
            Call cboDepartment_SelectedIndexChanged(Nothing, Nothing)

            cboSecGroup.SelectedValue = objEmp._Sectiongroupunkid.ToString
            Call cboSectionGroup_SelectedIndexChanged(Nothing, Nothing)

            cboSection.SelectedValue = objEmp._Sectionunkid.ToString
            Call cboSection_SelectedIndexChanged(Nothing, Nothing)

            cboUnitGrp.SelectedValue = objEmp._Unitgroupunkid.ToString
            Call cboUnitGroup_SelectedIndexChanged(Nothing, Nothing)

            cboUnit.SelectedValue = objEmp._Unitunkid.ToString
            Call cboUnit_SelectedIndexChanged(Nothing, Nothing)

            cboTeam.SelectedValue = objEmp._Teamunkid.ToString
            cboJobGroup.SelectedValue = objEmp._Jobgroupunkid.ToString
            cboJob.SelectedValue = objEmp._Jobunkid.ToString
            cboGradeGrp.SelectedValue = objEmp._Gradegroupunkid.ToString
            Call cboGradeGrp_SelectedIndexChanged(Nothing, Nothing)
            cboGrade.SelectedValue = objEmp._Gradeunkid.ToString
            Call cboGrade_SelectedIndexChanged(Nothing, Nothing)
            cboGradeLevel.SelectedValue = objEmp._Gradelevelunkid.ToString
            Call cboGradeLevel_SelectedIndexChanged(Nothing, Nothing)
            cboClassGrp.SelectedValue = objEmp._Classgroupunkid.ToString
            Call cboClassGrp_SelectedIndexChanged(Nothing, Nothing)
            cboClass.SelectedValue = objEmp._Classunkid.ToString
            cboCostcenter.SelectedValue = objEmp._Costcenterunkid.ToString


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            If objEmp._Isapproved Then
                cboBranch.Enabled = False
                cboDeptGrp.Enabled = False
                cboDepartment.Enabled = False
                cboSecGroup.Enabled = False
                cboSection.Enabled = False
                cboUnitGrp.Enabled = False
                cboUnit.Enabled = False
                cboTeam.Enabled = False
                cboClassGrp.Enabled = False
                cboClass.Enabled = False
                cboJobGroup.Enabled = False
                cboJob.Enabled = False
                cboCostcenter.Enabled = False
                cboGradeGrp.Enabled = False
                cboGrade.Enabled = False
                cboGradeLevel.Enabled = False
                btnsave.Visible = False

                If CInt(cboBranch.SelectedValue) <= 0 Then cboBranch.SelectedItem.Text = ""
                If CInt(cboDeptGrp.SelectedValue) <= 0 Then cboDeptGrp.SelectedItem.Text = ""
                If CInt(cboDepartment.SelectedValue) <= 0 Then cboDepartment.SelectedItem.Text = ""
                If CInt(cboSecGroup.SelectedValue) <= 0 Then cboSecGroup.SelectedItem.Text = ""
                If CInt(cboSection.SelectedValue) <= 0 Then cboSection.SelectedItem.Text = ""
                If CInt(cboUnitGrp.SelectedValue) <= 0 Then cboUnitGrp.SelectedItem.Text = ""
                If CInt(cboUnit.SelectedValue) <= 0 Then cboUnit.SelectedItem.Text = ""
                If CInt(cboTeam.SelectedValue) <= 0 Then cboTeam.SelectedItem.Text = ""
                If CInt(cboClassGrp.SelectedValue) <= 0 Then cboClassGrp.SelectedItem.Text = ""
                If CInt(cboClass.SelectedValue) <= 0 Then cboClass.SelectedItem.Text = ""
            End If

            'Pinkal (28-Dec-2015) -- End

            'Gajanan (10-Nov-2018) -- Start
            'Enhancement For NMB.
            If Session("CompCode").ToString.ToUpper = "NMB" Then
                pnlgrade.Visible = False
                pnlgradegrp.Visible = False
            Else
                pnlgrade.Visible = True
                pnlgradegrp.Visible = True
            End If
            'Gajanan (10-Nov-2018) -- End


        Catch ex As Exception
            msg.DisplayError("Procedure SetInfo : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Dim objEmp As New clsEmployee_Master
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = Session("Employeeunkid")
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End

            objEmp._Stationunkid = CInt(cboBranch.SelectedValue)
            objEmp._Deptgroupunkid = CInt(cboDeptGrp.SelectedValue)
            objEmp._Departmentunkid = CInt(cboDepartment.SelectedValue)
            objEmp._Sectiongroupunkid = CInt(cboSecGroup.SelectedValue)
            objEmp._Sectionunkid = CInt(cboSection.SelectedValue)
            objEmp._Unitgroupunkid = CInt(cboUnitGrp.SelectedValue)
            objEmp._Unitunkid = CInt(cboUnit.SelectedValue)
            objEmp._Teamunkid = CInt(cboTeam.SelectedValue)
            objEmp._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
            objEmp._Jobunkid = CInt(cboJob.SelectedValue)
            objEmp._Gradegroupunkid = CInt(cboGradeGrp.SelectedValue)
            objEmp._Gradeunkid = CInt(cboGrade.SelectedValue)
            objEmp._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            objEmp._Classgroupunkid = CInt(cboClassGrp.SelectedValue)
            objEmp._Classunkid = CInt(cboClass.SelectedValue)
            objEmp._Costcenterunkid = CInt(cboCostcenter.SelectedValue)

            'S.SANDEEP [ 19 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmp._Companyunkid = CInt(Session("CompanyUnkId"))
            'S.SANDEEP [ 19 MAR 2013 ] -- END

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'objEmp._WebFormName = "frmEmployeeMaster"
            'StrModuleName2 = "mnuCoreSetups"
            'objEmp._WebClientIP = CStr(Session("IP_ADD"))
            'objEmp._WebHostName = CStr(Session("HOST_NAME"))
            'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            '    objEmp._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''Sohail (23 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'If objEmp.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then
            '    'If objEmp.Update = False Then
            '    'Sohail (23 Apr 2012) -- End
            '    msg.DisplayMessage(objEmp._Message, Me)
            'End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'Dim blnFlag As Boolean = objEmp.Update(CStr(Session("Database_Name")), _
            '                                        CInt(Session("Fin_year")), _
            '                                        CInt(Session("CompanyUnkId")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        CStr(Session("IsArutiDemo")), _
            '                                        CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                        CInt(Session("NoOfEmployees")), _
            '                                        CInt(Session("UserId")), False, _
            '                                        CStr(Session("UserAccessModeSetting")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                        CBool(Session("IsIncludeInactiveEmp")), _
            '                                        CBool(Session("AllowToApproveEarningDeduction")), _
            '                            ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
            '                                        CStr(Session("EmployeeAsOnDate")), , _
            '                                        CBool(Session("IsImgInDataBase")))

With objEmp
                ._FormName = mstrModuleName
                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                Else
                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                End If
                ._ClientIP = Session("IP_ADD").ToString()
                ._HostName = Session("HOST_NAME").ToString()
                ._FromWeb = True
                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                ._Companyunkid = Convert.ToInt32(Session("CompanyUnkId"))
            End With

            Dim blnFlag As Boolean = objEmp.Update(CStr(Session("Database_Name")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    CStr(Session("IsArutiDemo")), _
                                                    CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                                    CInt(Session("NoOfEmployees")), _
                                                    CInt(Session("UserId")), False, _
                                                    CStr(Session("UserAccessModeSetting")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                    CBool(Session("IsIncludeInactiveEmp")), _
                                                    CBool(Session("AllowToApproveEarningDeduction")), _
                                                  ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                                  CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                    CStr(Session("EmployeeAsOnDate")), , _
                                                    CBool(Session("IsImgInDataBase")))

            'Pinkal (18-Aug-2018) -- End

            If blnFlag = False Then
                msg.DisplayMessage(objEmp._Message, Me)
            End If
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If objEmp._Message.Trim.Length <= 0 Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'S.SANDEEP [ 18 SEP 2012 ] -- END

        Catch ex As Exception
            msg.DisplayError("Procedure SetValue : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Function Set_Notification_Allocation(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = Session("Employeeunkid")
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If Session("Notify_Allocation").ToString.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname & "</b></span></p>")
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                If CInt(Session("UserId")) > 0 Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmp._Employeecode & "</b>. Following information has been changed by user : <b>" & objUser._Firstname.Trim & " " & objUser._Lastname & "</b></span></p>")
                ElseIf CInt(Session("Employeeunkid")) > 0 Then
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmp._Employeecode & "</b>. Following information has been changed by user : <b>" & objEmp._Firstname.Trim & " " & objEmp._Othername & " " & objEmp._Surname & "</b></span></p>")
                End If

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & Session("HOST_NAME").ToString & "</b> and IPAddress : <b>" & Session("IP_ADD").ToString & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TABLE border = '1' WIDTH = '60%'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '60%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Allocations" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Old Allocation" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:35%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "New Allocation" & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In Session("Notify_Allocation").ToString.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enAllocation.BRANCH
                            Dim ObjStation As New clsStation
                            If objEmp._Stationunkid <> CInt(IIf(cboBranch.SelectedValue Is Nothing, objEmp._Stationunkid, cboBranch.SelectedValue)) Then
                                ObjStation._Stationunkid = objEmp._Stationunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Branch" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjStation._Name.Trim = "", "&nbsp;", ObjStation._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboBranch.SelectedValue) <= 0, "&nbsp;", cboBranch.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.DEPARTMENT_GROUP
                            Dim ObjDG As New clsDepartmentGroup
                            If objEmp._Deptgroupunkid <> CInt(IIf(cboDeptGrp.SelectedValue Is Nothing, objEmp._Deptgroupunkid, cboDeptGrp.SelectedValue)) Then
                                ObjDG._Deptgroupunkid = objEmp._Deptgroupunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Department Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjDG._Name.Trim = "", "&nbsp;", ObjDG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboDeptGrp.SelectedValue) <= 0, "&nbsp;", cboDeptGrp.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.DEPARTMENT
                            Dim ObjDept As New clsDepartment
                            If objEmp._Departmentunkid <> CInt(IIf(cboDepartment.SelectedValue Is Nothing, objEmp._Departmentunkid, cboDepartment.SelectedValue)) Then
                                ObjDept._Departmentunkid = objEmp._Departmentunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Department" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjDept._Name.Trim = "", "&nbsp;", ObjDept._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboDepartment.SelectedValue) <= 0, "&nbsp;", cboDepartment.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.SECTION_GROUP
                            Dim ObjSecGrp As New clsSectionGroup
                            If objEmp._Sectiongroupunkid <> CInt(IIf(cboSecGroup.SelectedValue Is Nothing, objEmp._Sectiongroupunkid, cboSecGroup.SelectedValue)) Then
                                ObjSecGrp._Sectiongroupunkid = objEmp._Sectiongroupunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Section Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjSecGrp._Name.Trim = "", "&nbsp;", ObjSecGrp._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboSecGroup.SelectedValue) <= 0, "&nbsp;", cboSecGroup.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.SECTION
                            Dim ObjSec As New clsSections
                            If objEmp._Sectionunkid <> CInt(IIf(cboSection.SelectedValue Is Nothing, objEmp._Sectionunkid, cboSection.SelectedValue)) Then
                                ObjSec._Sectionunkid = objEmp._Sectionunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Section" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjSec._Name.Trim = "", "&nbsp;", ObjSec._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboSection.SelectedValue) <= 0, "&nbsp;", cboSection.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.UNIT_GROUP
                            Dim ObjUG As New clsUnitGroup
                            If objEmp._Unitgroupunkid <> CInt(IIf(cboUnitGrp.SelectedValue Is Nothing, objEmp._Unitgroupunkid, cboUnitGrp.SelectedValue)) Then
                                ObjUG._Unitgroupunkid = objEmp._Unitgroupunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Unit Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjUG._Name.Trim = "", "&nbsp;", ObjUG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboUnitGrp.SelectedValue) <= 0, "&nbsp;", cboUnitGrp.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.UNIT
                            Dim ObjUnit As New clsUnits
                            If objEmp._Unitunkid <> CInt(IIf(cboUnit.SelectedValue Is Nothing, objEmp._Unitunkid, cboUnit.SelectedValue)) Then
                                ObjUnit._Unitunkid = objEmp._Unitunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Unit" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjUnit._Name.Trim = "", "&nbsp;", ObjUnit._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboUnit.SelectedValue) <= 0, "&nbsp;", cboUnit.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.TEAM
                            Dim ObjTeam As New clsTeams
                            If objEmp._Teamunkid <> CInt(IIf(cboTeam.SelectedValue Is Nothing, objEmp._Teamunkid, cboTeam.SelectedValue)) Then
                                ObjTeam._Teamunkid = objEmp._Teamunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Team" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjTeam._Name.Trim = "", "&nbsp;", ObjTeam._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboTeam.SelectedValue) <= 0, "&nbsp;", cboTeam.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.JOB_GROUP
                            Dim ObjJG As New clsJobGroup
                            If objEmp._Jobgroupunkid <> CInt(IIf(cboJobGroup.SelectedValue Is Nothing, objEmp._Jobgroupunkid, cboJobGroup.SelectedValue)) Then
                                ObjJG._Jobgroupunkid = objEmp._Jobgroupunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Job Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjJG._Name.Trim = "", "&nbsp;", ObjJG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboJobGroup.SelectedValue) <= 0, "&nbsp;", cboJobGroup.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.JOBS
                            Dim ObjJob As New clsJobs
                            If objEmp._Jobunkid <> CInt(IIf(cboJob.SelectedValue Is Nothing, objEmp._Jobunkid, cboJob.SelectedValue)) Then
                                ObjJob._Jobunkid = objEmp._Jobunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Jobs" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjJob._Job_Name.Trim = "", "&nbsp;", ObjJob._Job_Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboJob.SelectedValue) <= 0, "&nbsp;", cboJob.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.CLASS_GROUP
                            Dim ObjCG As New clsClassGroup
                            If objEmp._Classgroupunkid <> CInt(IIf(cboClassGrp.SelectedValue Is Nothing, objEmp._Classgroupunkid, cboClassGrp.SelectedValue)) Then
                                ObjCG._Classgroupunkid = objEmp._Classgroupunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Class Group" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjCG._Name.Trim = "", "&nbsp;", ObjCG._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboClassGrp.SelectedValue) <= 0, "&nbsp;", cboClassGrp.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                        Case enAllocation.CLASSES
                            Dim ObjClass As New clsClass
                            If objEmp._Classunkid <> CInt(IIf(cboClass.SelectedValue Is Nothing, objEmp._Classunkid, cboClass.SelectedValue)) Then
                                ObjClass._Classesunkid = objEmp._Classunkid
                                StrMessage.Append("<TR WIDTH = '60%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '10%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Class" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(ObjClass._Name.Trim = "", "&nbsp;", ObjClass._Name).ToString & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '25%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & IIf(CInt(cboClass.SelectedValue) <= 0, "&nbsp;", cboClass.SelectedItem.Text).ToString & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If
                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString

        Catch ex As Exception
            msg.DisplayError("Set_Notification_Allocation : " & ex.Message, Me)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If CInt(Session("Employeeunkid")) > 0 Then

                If dicAllocNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicAllocNotification.Keys
                        If dicAllocNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = "Notification of Changes in Employee Master file"
                            objSendMail._Message = dicAllocNotification(sKey)
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            With objSendMail
                                ._FormName = mstrModuleName
                                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With
                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If

            End If
        Catch ex As Exception
            msg.DisplayError("Send_Notification : " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    'S.SANDEEP [ 20 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Sub Set_Language()
    '    Try
    '        Dim dTab As DataTable = clsuser.Get_Languages(Session("mdbname"), "frmEmployeeMaster")
    '        Dim dRow() As DataRow = Nothing
    '        If dTab.Rows.Count > 0 Then
    '            dRow = dTab.Select("controlname = '" & lblBranch.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblBranch.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblBranch.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblBranch.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblBranch.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblDepartmentGroup.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblDepartmentGroup.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblDepartmentGroup.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblDepartmentGroup.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblDepartmentGroup.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblDepartment.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblDepartment.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblDepartment.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblDepartment.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblDepartment.Text = dRow(0).Item("language")
    '                End Select
    '            End If


    '            dRow = dTab.Select("controlname = '" & lblSectionGroup.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblSectionGroup.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblSectionGroup.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblSectionGroup.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblSectionGroup.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblSection.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblSection.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblSection.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblSection.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblSection.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblUnitGroup.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblUnitGroup.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblUnitGroup.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblUnitGroup.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblUnitGroup.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblUnits.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblUnits.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblUnits.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblUnits.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblUnits.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblTeam.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblTeam.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblTeam.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblTeam.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblTeam.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblCostCenter.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblCostCenter.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblCostCenter.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblCostCenter.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblCostCenter.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblJobGroup.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblJobGroup.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblJobGroup.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblJobGroup.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblJobGroup.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblJob.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblJob.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblJob.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblJob.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblJob.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblGradeGroup.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblGradeGroup.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblGradeGroup.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblGradeGroup.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblGradeGroup.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblGrade.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblGrade.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblGrade.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblGrade.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblGrade.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblGradeLevel.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblGradeLevel.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblGradeLevel.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblGradeLevel.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblGradeLevel.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblScale.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblScale.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblScale.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblScale.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblScale.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblClassGroup.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblClassGroup.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblClassGroup.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblClassGroup.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblClassGroup.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '            dRow = dTab.Select("controlname = '" & lblClass.ID.ToString & "'")
    '            If dRow.Length > 0 Then
    '                Select Case CInt(Session("LangId"))
    '                    Case 0  'DEFAULT
    '                        lblClass.Text = dRow(0).Item("language")
    '                    Case 1  'LANGUAGE1
    '                        lblClass.Text = dRow(0).Item("language1")
    '                    Case 2  'LANGUAGE2
    '                        lblClass.Text = dRow(0).Item("language2")
    '                    Case Else
    '                        lblClass.Text = dRow(0).Item("language")
    '                End Select
    '            End If

    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError("Set_Language : " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [ 20 NOV 2012 ] -- END

    'Pinkal (06-May-2014) -- End

#End Region

#Region " Controls Events "

    Protected Sub cboGradeGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeGrp.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGrade As New clsGrade
        Try
            If CInt(cboGradeGrp.SelectedValue) > 0 Then
                dsCombos = objGrade.getComboList("Grade", True, CInt(cboGradeGrp.SelectedValue))
                With cboGrade
                    .DataValueField = "gradeunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Grade")
                    .DataBind()                    
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboGradeGrp_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGradeLevel As New clsGradeLevel
        Try
            If CInt(cboGrade.SelectedValue) > 0 Then
                dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(cboGrade.SelectedValue))
                With cboGradeLevel
                    .DataValueField = "gradelevelunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("GradeLevel")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboGrade_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Dim objWagesTran As New clsWagesTran
        Dim decScale As Decimal = 0 'Sohail (11 May 2011)
        Dim objMaster As New clsMasterData
        Dim objWages As New clsWagesTran
        Dim dsList As DataSet
        Try
            objWagesTran.GetSalary(CInt(cboGradeGrp.SelectedValue), _
                               CInt(cboGrade.SelectedValue), _
                               CInt(cboGradeLevel.SelectedValue), _
                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                               decScale)
            dsList = objMaster.Get_Current_Scale("Salary", CInt(Session("Employeeunkid")), ConfigParameter._Object._CurrentDateAndTime.Date)

            If dsList.Tables("Salary").Rows.Count > 0 Then
                decScale = CDec(dsList.Tables("Salary").Rows(0).Item("newscale").ToString)
            End If

            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'txtScale.Text = CStr(Format(decScale, GUI.fmtCurrency))
            txtScale.Text = CStr(Format(decScale, CStr(Session("fmtCurrency"))))
            'Sohail (02 May 2012) -- End

        Catch ex As Exception
            msg.DisplayError("Procedure cboGradeLevel_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    'Anjan (25 Oct 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboBranch.SelectedValue), "DeptGrp", True)
                With cboDeptGrp
                    .DataValueField = "deptgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboStation_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboDeptGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeptGrp.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDeptGrp.SelectedValue), "Department", True)
                With cboDepartment
                    .DataValueField = "departmentunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Department")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboDeptGrp_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSecGroup
                    .DataValueField = "sectiongroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboDepartment_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSecGroup.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSecGroup.SelectedValue), "Section", True)
                With cboSection
                    .DataValueField = "sectionunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Section")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboSectionGroup_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSection.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet

                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSection.SelectedValue))
                With cboUnitGrp
                    .DataValueField = "unitgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboSection_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnitGrp.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.UNIT)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGrp.SelectedValue))
                With cboUnit
                    .DataValueField = "unitunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Unit")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboUnitGroup_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnit.SelectedValue))
                With cboTeam
                    .DataValueField = "teamunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure cboUnit_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub cboClassGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClassGrp.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objclass As New clsClass
            dsCombos = objclass.getComboList("Classgrp", True, CInt(cboClassGrp.SelectedValue))
            With cboClass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Classgrp")
                .DataBind()
            End With
        Catch ex As Exception
            msg.DisplayError("Procedure cboClassGrp_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub
    'Anjan (25 Oct 2012)-End 

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If
        SetLanguage()

        If (Page.IsPostBack = False) Then
            clsuser = CType(Session("clsuser"), User)
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                btnsave.Visible = CBool(Session("EditEmployee"))
                lblScale.Visible = CBool(Session("ViewScale"))
                txtScale.Visible = CBool(Session("ViewScale"))
                cboBranch.Enabled = CBool(Session("AllowtoChangeBranch"))
                cboDeptGrp.Enabled = CBool(Session("AllowtoChangeDepartmentGroup"))
                cboDepartment.Enabled = CBool(Session("AllowtoChangeDepartment"))
                cboSecGroup.Enabled = CBool(Session("AllowtoChangeSectionGroup"))
                cboSection.Enabled = CBool(Session("AllowtoChangeSection"))
                cboUnitGrp.Enabled = CBool(Session("AllowtoChangeUnitGroup"))
                cboUnit.Enabled = CBool(Session("AllowtoChangeUnit"))
                cboTeam.Enabled = CBool(Session("AllowtoChangeTeam"))
                cboJobGroup.Enabled = CBool(Session("AllowtoChangeJobGroup"))
                cboJob.Enabled = CBool(Session("AllowtoChangeJob"))
                cboGradeGrp.Enabled = CBool(Session("AllowtoChangeGradeGroup"))
                cboGrade.Enabled = CBool(Session("AllowtoChangeGrade"))
                cboGradeLevel.Enabled = CBool(Session("AllowtoChangeGradeLevel"))
                cboClassGrp.Enabled = CBool(Session("AllowtoChangeClassGroup"))
                cboClass.Enabled = CBool(Session("AllowtoChangeClass"))
                cboCostcenter.Enabled = CBool(Session("AllowtoChangeCostCenter"))
                If Session("ShowPending") IsNot Nothing Then

                    'S.SANDEEP [19-JAN-2017] -- START
                    'ISSUE/ENHANCEMENT :
                    ''Pinkal (28-Dec-2015) -- Start
                    ''Enhancement - Working on Changes in SS for Employee Master.
                    ''btnsave.Visible = Not Session("ShowPending")
                    'btnsave.Visible = CBool(Session("ShowPending"))
                    ''Pinkal (28-Dec-2015) -- End
                    If Session("ShowPending") IsNot Nothing Then
                        btnsave.Visible = Not CBool(Session("ShowPending"))
                    End If
                    'S.SANDEEP [19-JAN-2017] -- END


                    
                End If
            Else
                lblEmployee.Visible = False : txtEmployee.Visible = False
                btnsave.Visible = False

                If CBool(Session("AllowViewEmployeeScale")) = False Then
                    lblScale.Visible = False
                    txtScale.Visible = False
                End If
            End If
            Call FillCombo()
            Call SetInfo()

        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            dicAllocNotification = New Dictionary(Of String, String)
            If CInt(Session("Employeeunkid")) > 0 Then
                If Session("Notify_Allocation").ToString.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In Session("Notify_Allocation").ToString.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification_Allocation(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicAllocNotification.ContainsKey(objUsr._Email) = False Then
                            dicAllocNotification.Add(objUsr._Email, StrMessage)
                        End If
                    Next
                    objUsr = Nothing
                End If
            End If

            Call SetValue()
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
            Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            msg.DisplayError("btnsave_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
        Catch ex As Exception
            msg.DisplayError("btnClose_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    '
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton1_CloseButton_click Event : " & ex.Message, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 

#End Region

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption("tabpMainInfo", Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption("tabpMainInfo", Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption("tabpMainInfo", Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblClass.Text = Language._Object.getCaption(Me.lblClass.ID, Me.lblClass.Text)
            Me.lblDepartmentGroup.Text = Language._Object.getCaption(Me.lblDepartmentGroup.ID, Me.lblDepartmentGroup.Text)
            Me.lblJobGroup.Text = Language._Object.getCaption(Me.lblJobGroup.ID, Me.lblJobGroup.Text)
            Me.lblUnits.Text = Language._Object.getCaption(Me.lblUnits.ID, Me.lblUnits.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.ID, Me.lblSection.Text)
            Me.lblClassGroup.Text = Language._Object.getCaption(Me.lblClassGroup.ID, Me.lblClassGroup.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
            Me.lblScale.Text = Language._Object.getCaption(Me.lblScale.ID, Me.lblScale.Text)
            Me.lblGrade.Text = Language._Object.getCaption(Me.lblGrade.ID, Me.lblGrade.Text)
            Me.lblGradeGroup.Text = Language._Object.getCaption(Me.lblGradeGroup.ID, Me.lblGradeGroup.Text)
            Me.lblTeam.Text = Language._Object.getCaption(Me.lblTeam.ID, Me.lblTeam.Text)
            Me.lblUnitGroup.Text = Language._Object.getCaption(Me.lblUnitGroup.ID, Me.lblUnitGroup.Text)
            Me.lblSectionGroup.Text = Language._Object.getCaption(Me.lblSectionGroup.ID, Me.lblSectionGroup.Text)
            Me.lblGradeLevel.Text = Language._Object.getCaption(Me.lblGradeLevel.ID, Me.lblGradeLevel.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")




            Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

End Class
