<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_AssetsRegisterList.aspx.vb" Inherits="wPg_AssetsRegisterList" Title="Company Assets List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Company Assets List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <%--<tr style="width: 100%">
                                            <td style="width: 100%" colspan="6">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>--%>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="LblAsset" runat="server" Text="Asset"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="drpAsset" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="LblDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc2:DateCtrl ID="dtpdate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblCodition" runat="server" Text="Condition"></asp:Label></label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="drpCondition" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="LblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="drpStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblAssetNo" runat="server" Text="Asset No."></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:TextBox ID="txtAssetno" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnAssign" runat="server" Text="Assign" CssClass="btndefault" />
                                        <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                                <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server">
                                    <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" Width="99%">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Returned" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="mnuReturned">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkReturned" runat="server" CommandName="Returned" Text="Returned"
                                                        Font-Underline="false" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Written Off" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="mnuWrittenOff">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkWritten" runat="server" CommandName="Writtenoff" Text="Written Off"
                                                        Font-Underline="false" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Lost" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="mnuLost">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkLost" runat="server" CommandName="Lost" Text="Lost" Font-Underline="false" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Delete" HeaderStyle-Width="30px" ItemStyle-Width="30px"
                                                HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                            CssClass="griddelete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployeeName" />
                                            <asp:BoundColumn DataField="Asset" HeaderText="Asset Name" ReadOnly="True" FooterText="colhAssetName" />
                                            <asp:BoundColumn DataField="AssetNo" HeaderText="Asset No." ReadOnly="True" FooterText="colhAssestNo" />
                                            <asp:BoundColumn DataField="assetserial_no" HeaderText="Serial No." ReadOnly="True"
                                                FooterText="" />
                                            <asp:BoundColumn DataField="Date" HeaderText="Date" ReadOnly="True" FooterText="colhDate" />
                                            <asp:BoundColumn DataField="Status" HeaderText="Status" ReadOnly="True" FooterText="colhStatus" />
                                            <asp:BoundColumn DataField="Condtion" HeaderText="Condition" ReadOnly="True" FooterText="colhCondition" />
                                            <asp:BoundColumn DataField="Remark" HeaderText="Remark" ReadOnly="True" FooterText="colhRemark" />
                                            <asp:BoundColumn DataField="asset_statusunkid" HeaderText="Statusunkid" ReadOnly="True"
                                                Visible="false" />
                                        </Columns>
                                        <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete ?" />
                    <cc1:ModalPopupExtender ID="popupStatus" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btnStatusClose" PopupControlID="pnlStatus" TargetControlID="txtStatusAsset" />
                    <asp:Panel ID="pnlStatus" runat="server" CssClass="newpopup" Style="width: 450px;
                        display: none">
                        <div class="panel-primary" style="margin-bottom:0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Employee Asset Status"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left">
                                            <asp:Label ID="lblChangeStatus" runat="server" Text="Employee Asset Status"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 18%">
                                                    <asp:Label ID="lblStatusAsset" runat="server" Text="Asset" />
                                                </td>
                                                <td style="width: 82%">
                                                    <asp:TextBox ID="txtStatusAsset" runat="server" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td />
                                            </tr>
                                            <tr>
                                                <td style="width: 18%">
                                                    <asp:Label ID="lblStatusEmp" runat="server" Text="Employee" />
                                                </td>
                                                <td style="width: 82%">
                                                    <asp:TextBox ID="txtStatusEmp" runat="server" ReadOnly="true" />
                                                </td>
                                                <td />
                                            </tr>
                                            <tr>
                                                <td style="width: 18%">
                                                    <asp:Label ID="lblAssetStatus" runat="server" Text="Status" />
                                                </td>
                                                <td style="width: 82%">
                                                    <asp:TextBox ID="txtAssetStatus" runat="server" ReadOnly="true" />
                                                </td>
                                                <td />
                                            </tr>
                                            <tr>
                                                <td style="width: 18%" valign="top">
                                                    <asp:Label ID="lblStatusRemarks" runat="server" Text="Remark" />
                                                </td>
                                                <td style="width: 82%">
                                                    <asp:TextBox ID="txtStatusRemark" runat="server" Rows="3" TextMode="MultiLine" />
                                                </td>
                                                <td />
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnStatusSave" runat="server" Text="Save" CssClass="btndefault" />
                                            <asp:Button ID="btnStatusClose" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
