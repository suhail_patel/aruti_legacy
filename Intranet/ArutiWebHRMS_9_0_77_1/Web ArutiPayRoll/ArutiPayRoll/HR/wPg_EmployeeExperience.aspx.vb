﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class HR_wPg_EmployeeExperience
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim objJobExperience As clsJobExperience_tran
    Dim mintExperienceTranUnkid As Integer = -1

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeExperience"
    'Pinkal (06-May-2014) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintExperienceUnkid As Integer = -1

    Private mintTransactionId As Integer = 0
    Private objAExperienceTran As New clsJobExperience_approval_tran

    Dim Arr() As String
    Dim ExperienceApprovalFlowVal As String
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Dim isEmployeeApprove As Boolean = False

    'Gajanan [3-April-2019] -- Start
    Dim mblnisEmployeeApprove As Boolean = False
    'Gajanan [3-April-2019] -- End
    'Gajanan [22-Feb-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [21-June-2019] -- Start      
    Private OldData As New clsJobExperience_tran
    'Gajanan [21-June-2019] -- End


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private mdtExperienceDocument As DataTable
    Private objDocument As New clsScan_Attach_Documents
    'Gajanan [5-Dec-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Try

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            Dim dsList As New DataSet
            'Gajanan [5-Dec-2019] -- End

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                Dim objEmployee As New clsEmployee_Master



                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim dsList As DataSet = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Gajanan [5-Dec-2019] -- Start   
                'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                'Dim dsList As New DataSet
                'Gajanan [5-Dec-2019] -- End

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsList = objEmployee.GetEmployeeList("Emp", True, )
                '    dsList = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .



                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmJobHistory_ExperienceList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                'dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                            CInt(Session("UserId")), _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            CStr(Session("UserAccessModeSetting")), True, _
                '                            CBool(Session("IsIncludeInactiveEmp")), "Emp", True)


                dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                           CStr(Session("UserAccessModeSetting")), _
                                      mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                With drpEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With

                'Shani(24-Aug-2015) -- End
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                'S.SANDEEP [20-JUN-2018] -- End




                'S.SANDEEP [ 16 JAN 2014 ] -- START
                'With drpEmployee
                '    .DataValueField = "employeeunkid"
                '    .DataTextField = "employeename"
                '    .DataSource = dsList.Tables(0)
                '    .DataBind()
                'End With

               
                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .





                'If Session("ShowPending") IsNot Nothing Then

                '    'Shani(24-Aug-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"), , , False)
                '    dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                            CInt(Session("UserId")), _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            CStr(Session("UserAccessModeSetting")), False, _
                '                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                '    'Shani(24-Aug-2015) -- End
                '    Dim dtTab As DataTable = New DataView(dsList.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                '    With drpEmployee
                '        .DataValueField = "employeeunkid"
                '        'Nilay (09-Aug-2016) -- Start
                '        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '        '.DataTextField = "employeename"
                '        .DataTextField = "EmpCodeName"
                '        'Nilay (09-Aug-2016) -- End
                '        .DataSource = dtTab
                '        .DataBind()
                '    End With
                'Else
                '    With drpEmployee
                '        .DataValueField = "employeeunkid"
                '        'Nilay (09-Aug-2016) -- Start
                '        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '        '.DataTextField = "employeename"
                '        .DataTextField = "EmpCodeName"
                '        'Nilay (09-Aug-2016) -- End
                '        .DataSource = dsList.Tables(0)
                '        .DataBind()
                '    End With
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End

            Else

                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If
            'Dim objJob As New clsJobs
            'drpJobs.DataSource = objJob.getComboList("Job", True)
            'drpJobs.DataValueField = "jobunkid"
            'drpJobs.DataTextField = "name"
            'drpJobs.DataBind()


            Dim objBenefit As New clsbenefitplan_master
            ChkBenefits.DataSource = objBenefit.getComboList("Benefit")
            ChkBenefits.DataValueField = "benefitplanunkid"
            ChkBenefits.DataTextField = "name"
            ChkBenefits.DataBind()



            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            dsList = Nothing
            Dim objCMaster As New clsCommon_Master
            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            'Gajanan [5-Dec-2019] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    'Private Function Validation() As Boolean
    Private Function Validation(Optional ByVal IsFromAttachment As Boolean = False) As Boolean
        'Gajanan [5-Dec-2019] -- End
        Try

            'Gajanan [21-June-2019] -- Start 

            If (mintExperienceUnkid > 0) Then
                objJobExperience._Experiencetranunkid = mintExperienceUnkid
                OldData = objJobExperience
            End If

            Dim blnIsChange As Boolean = True

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            'If IsNothing(OldData) = False Then
            If IsNothing(OldData) = False AndAlso IsFromAttachment = False Then
                'Gajanan [5-Dec-2019] -- End
                If OldData._Company = TxtCompany.Text.Trim AndAlso _
                   OldData._Address = TxtAdress.Text.Trim AndAlso _
                   OldData._Start_Date = dtpStartdate.GetDate AndAlso _
                   OldData._End_Date = dtpEnddate.GetDate AndAlso _
                   OldData._Job = txtOldJob.Text.Trim AndAlso _
                   OldData._Supervisor = TxtSuperVisors.Text.Trim AndAlso _
                   OldData._Remark = TxtRemark.Text.Trim AndAlso _
                   OldData._OtherBenefit = txtOtherBenefits.Text.Trim AndAlso _
                   OldData._Last_Pay = CDec(Format(CDec(TxtGrossPay.Text), CStr(Session("fmtCurrency")))) AndAlso _
                   OldData._CurrencySign = txtSign.Text.Trim AndAlso _
                   OldData._Iscontact_Previous = chkCanContatEmployee.Checked AndAlso _
                   OldData._Contact_Person = TxtContactPerson.Text.Trim AndAlso _
                   OldData._Contact_No = TxtContactAddress.Text.Trim AndAlso _
                   OldData._Memo = TxtMemo.Text.Trim AndAlso _
                   OldData._Leave_Reason = TxtLeaveReason.Text.Trim Then
                    blnIsChange = False
                Else
                    OldData = Nothing
                End If
            End If

            If blnIsChange = False Then
                If ChkBenefits.Items.Count > 0 Then
                    Dim xPreviousBenefit As List(Of String)
                    xPreviousBenefit = OldData._PreviousBenefit.Split(CChar(",")).ToList()
                    For i As Integer = 0 To ChkBenefits.Items.Count - 1
                        If ChkBenefits.Items(i).Selected AndAlso xPreviousBenefit.Contains(CStr(ChkBenefits.Items(i).Value)) Then
                            blnIsChange = True
                            Exit For
                        End If
                    Next
                End If
            End If

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            If CBool(Session("JobExperienceDocsAttachmentMandatory")) AndAlso IsFromAttachment = False Then
                If mdtExperienceDocument Is Nothing OrElse mdtExperienceDocument.Select("AUD <> 'D'").Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
                    Return False
                End If
            End If

            If IsFromAttachment = False AndAlso blnIsChange = False AndAlso IsNothing(mdtExperienceDocument) = False AndAlso CInt(mdtExperienceDocument.Select("AUD <> '' ").Count) > 0 Then
                blnIsChange = True
            End If
            'Gajanan [5-Dec-2019] -- End

            If blnIsChange = False AndAlso mintExperienceTranUnkid > 0 Then
                objJobExperience = Nothing
                OldData = Nothing
                Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeExperienceList.aspx", False)
                Return False
            End If
            'Gajanan [21-June-2019] -- End


            If CInt(drpEmployee.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpEmployee.Focus()
                Return False
            End If

            If TxtCompany.Text.Trim = "" Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Company Name cannot be blank. Company Name is compulsory information."), Me)
                'Pinkal (06-May-2014) -- End
                TxtCompany.Focus()
                Return False
            End If



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If dtpStartdate.GetDate = Nothing Then
            '    DisplayMessage.DisplayMessage("Start Date is compulsory information. Please select Start Date to continue.", Me)
            '    dtpStartdate.Focus()
            '    Return False
            'End If

            'If dtpEnddate.GetDate = Nothing Then
            '    DisplayMessage.DisplayMessage("End Date is compulsory information. Please select End Date to continue.", Me)
            '    dtpEnddate.Focus()
            '    Return False
            'End If

            'If dtpEnddate.GetDate <> Nothing And dtpEnddate.GetDate <> Nothing Then
            '    If dtpEnddate.GetDate.Date <= dtpStartdate.GetDate.Date Then
            '        DisplayMessage.DisplayMessage("End date cannot be less then or equal to start date.", Me)
            '        dtpEnddate.Focus()
            '        Return False
            '    End If
            'End If


            If dtpStartdate.IsNull = True Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Start Date is compulsory information. Please select Start Date to continue."), Me)
                'Pinkal (06-May-2014) -- End
                dtpStartdate.Focus()
                Return False
            End If

            'Sandeep (30 Sep 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Rutta's Request
            'If dtpEnddate.IsNull = True Then
            '    DisplayMessage.DisplayMessage("End Date is compulsory information. Please select End Date to continue.", Me)
            '    dtpEnddate.Focus()
            '    Return False
            'End If
            'Sandeep (30 Sep 2012)-End 

            If dtpEnddate.IsNull = False And dtpEnddate.IsNull <> False Then
                If dtpEnddate.GetDate.Date <= dtpStartdate.GetDate.Date Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "End date cannot be less then or equal to start date."), Me)
                    'Pinkal (06-May-2014) -- End
                    dtpEnddate.Focus()
                    Return False
                End If
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END



            'If CInt(drpJobs.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Job is compulsory information. Please select Job to continue.", Me)
            '    drpJobs.Focus()
            '    Return False
            'End If


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'If TxtLeaveReason.Text.Trim = "" Then
            '    DisplayMessage.DisplayMessage("Leaving Reason cannot be blank. Leaving Reason is compulsory information.", Me)
            '    TxtLeaveReason.Focus()
            '    Return False
            'End If

            'Pinkal (22-Nov-2012) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If ExperienceApprovalFlowVal Is Nothing Then
            If ExperienceApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmJobHistory_ExperienceList, CStr(Session("Database_Name")), _
                                                    CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                    CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                    CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(drpEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objApprovalData._Message, Me)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End

                If objJobExperience.isExist(CInt(drpEmployee.SelectedValue), TxtCompany.Text.Trim, CDate(IIf(dtpStartdate.IsNull = False, dtpStartdate.GetDate(), Nothing)), CDate(IIf(dtpEnddate.IsNull = False, dtpEnddate.GetDate(), Nothing)), txtOldJob.Text.Trim, mintExperienceUnkid) = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage("clsJobExperience_tran", 1, "Particular Job History is already entered for selected employee. Please enter new information."), Me)
                    Return False
                End If
                Dim intOperationType As Integer = 0 : Dim strMsg As String = ""
                If objAExperienceTran.isExist(CInt(drpEmployee.SelectedValue), TxtCompany.Text.Trim, CDate(IIf(dtpStartdate.IsNull = False, dtpStartdate.GetDate(), Nothing)), CDate(IIf(dtpEnddate.IsNull = False, dtpEnddate.GetDate(), Nothing)), txtOldJob.Text, -1, "", Nothing, False, intOperationType) = True Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If mintExperienceUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 15, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 16, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If mintExperienceUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 17, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 18, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If mintExperienceUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 19, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 20, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Error in Validation function " & ex.Message, Me)
            DisplayMessage.DisplayError("Error in Validation function " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub SetValue()
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Gajanan [3-April-2019] -- Start
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [3-April-2019] -- End

            'If ExperienceApprovalFlowVal Is Nothing Then
            'Gajanan [3-April-2019] -- Start
            'If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
            If ExperienceApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                'Gajanan [22-Feb-2019] -- End
                'Gajanan [3-April-2019] -- End

                objAExperienceTran._Audittype = enAuditType.ADD
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objAExperienceTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                Else
                    objAExperienceTran._Audituserunkid = CInt(Session("UserId"))
                End If

                objAExperienceTran._Address = TxtAdress.Text
                objAExperienceTran._Company = TxtCompany.Text
                objAExperienceTran._Contact_No = TxtContactAddress.Text
                objAExperienceTran._Contact_Person = TxtContactPerson.Text
                objAExperienceTran._Employeeunkid = CInt(drpEmployee.SelectedValue)

                If dtpEnddate.IsNull = True Then
                    objAExperienceTran._End_Date = Nothing
                Else
                    objAExperienceTran._End_Date = dtpEnddate.GetDate.Date
                End If


                objAExperienceTran._Iscontact_Previous = chkCanContatEmployee.Checked

                objAExperienceTran._Old_Job = txtOldJob.Text
                objAExperienceTran._Otherbenefit = txtOtherBenefits.Text
                objAExperienceTran._Currencysign = txtSign.Text

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.


                'objAExperienceTran._Last_Pay = CDec(Format(CDec(TxtGrossPay.Text), CStr(Session("fmtCurrency"))))
                If TxtGrossPay.Text = String.Empty Then
                    objAExperienceTran._Last_Pay = 0
                Else
                objAExperienceTran._Last_Pay = CDec(Format(CDec(TxtGrossPay.Text), CStr(Session("fmtCurrency"))))
                End If
                'Gajanan [17-DEC-2018] -- End


                objAExperienceTran._Leave_Reason = TxtLeaveReason.Text
                objAExperienceTran._Memo = TxtMemo.Text
                objAExperienceTran._Remark = TxtRemark.Text
                If dtpStartdate.IsNull = True Then
                    objAExperienceTran._Start_Date = Nothing
                Else
                    objAExperienceTran._Start_Date = dtpStartdate.GetDate.Date
                End If

                objAExperienceTran._Supervisor = TxtSuperVisors.Text

                objAExperienceTran._Isvoid = False
                objAExperienceTran._Tranguid = Guid.NewGuid.ToString()
                objAExperienceTran._Transactiondate = Now
                objAExperienceTran._Approvalremark = ""
                objAExperienceTran._Isweb = True
                objAExperienceTran._Isfinal = False
                objAExperienceTran._Ip = Session("IP_ADD").ToString()
                objAExperienceTran._Host = Session("HOST_NAME").ToString()
                objAExperienceTran._Form_Name = mstrModuleName
                objAExperienceTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

                If mintExperienceUnkid <= 0 Then
                    objAExperienceTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objAExperienceTran._Experiencetranunkid = -1
                Else
                    objAExperienceTran._Experiencetranunkid = mintExperienceUnkid
                    objAExperienceTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If

                If ChkBenefits.Items.Count > 0 Then
                    objAExperienceTran._PreviousBenefitPlan = ""
                    For i As Integer = 0 To ChkBenefits.Items.Count - 1

                        If ChkBenefits.Items(i).Selected Then
                            If objAExperienceTran._PreviousBenefitPlan.Length <= 0 Then
                                objAExperienceTran._PreviousBenefitPlan = CStr(ChkBenefits.Items(i).Value)
                            Else
                                objAExperienceTran._PreviousBenefitPlan &= "," & CStr(ChkBenefits.Items(i).Value)
                            End If
                        End If
                    Next
                Else
                    objAExperienceTran._PreviousBenefitPlan = ""
                End If

            Else
            objJobExperience._Address = TxtAdress.Text
            objJobExperience._Company = TxtCompany.Text
            objJobExperience._Contact_No = TxtContactAddress.Text
            objJobExperience._Contact_Person = TxtContactPerson.Text
            objJobExperience._Employeeunkid = CInt(drpEmployee.SelectedValue)


            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If dtpEnddate.IsNull = True Then
                objJobExperience._End_Date = Nothing
            Else
            objJobExperience._End_Date = dtpEnddate.GetDate.Date
            End If

            'Pinkal (12-Nov-2012) -- End


            objJobExperience._Iscontact_Previous = chkCanContatEmployee.Checked

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objJobExperience._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
            Else
                objJobExperience._Userunkid = CInt(Session("UserId"))
            End If
            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'objJobExperience._Jobunkid = CInt(drpJobs.SelectedValue)
            objJobExperience._Job = txtOldJob.Text
            'Sohail (21 Mar 2012) -- End

            If TxtGrossPay.Text.Trim = "" Then
                TxtGrossPay.Text = "0"
            End If

            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'objJobExperience._Last_Pay = Format(CDec(TxtGrossPay.Text), GUI.fmtCurrency)
            objJobExperience._Last_Pay = CDec(Format(CDec(TxtGrossPay.Text), CStr(Session("fmtCurrency"))))
            'Sohail (02 May 2012) -- End

            objJobExperience._Leave_Reason = TxtLeaveReason.Text
            objJobExperience._Memo = TxtMemo.Text
            objJobExperience._Remark = TxtRemark.Text
            objJobExperience._Start_Date = dtpStartdate.GetDate.Date
            objJobExperience._Supervisor = TxtSuperVisors.Text

            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            objJobExperience._OtherBenefit = txtOtherBenefits.Text
            objJobExperience._CurrencySign = txtSign.Text
            'Sohail (21 Mar 2012) -- End

            If ChkBenefits.Items.Count > 0 Then
                objJobExperience._PreviousBenefit = ""
                For i As Integer = 0 To ChkBenefits.Items.Count - 1

                    If ChkBenefits.Items(i).Selected Then
                        If objJobExperience._PreviousBenefit.Length <= 0 Then
                            objJobExperience._PreviousBenefit = CStr(ChkBenefits.Items(i).Value)
                        Else
                            objJobExperience._PreviousBenefit &= "," & CStr(ChkBenefits.Items(i).Value)
                        End If
                    End If
                Next
            Else
                objJobExperience._PreviousBenefit = ""
            End If
            End If

            'Gajanan [17-DEC-2018] -- End










          

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue :-  " & ex.Message, Me)
            DisplayMessage.DisplayError("SetValue :-  " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub BlankObjects()
        Try


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'drpEmployee.SelectedIndex = 0

            'Pinkal (22-Nov-2012) -- End


            TxtCompany.Text = ""
            TxtAdress.Text = ""
            dtpStartdate.SetDate = Nothing
            dtpEnddate.SetDate = Nothing
            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'If drpJobs.Items.Count > 0 Then drpJobs.SelectedIndex = 0
            txtOldJob.Text = ""
            'Sohail (21 Mar 2012) -- End
            TxtSuperVisors.Text = ""
            TxtRemark.Text = ""
            TxtMemo.Text = ""
            TxtGrossPay.Text = "0"
            chkCanContatEmployee.Checked = False
            TxtContactPerson.Text = ""
            TxtContactAddress.Text = ""
            TxtLeaveReason.Text = ""

            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            txtOtherBenefits.Text = ""
            txtSign.Text = ""
            'Sohail (21 Mar 2012) -- End

            If ChkBenefits.Items.Count > 0 Then

                For i As Integer = 0 To ChkBenefits.Items.Count - 1
                    ChkBenefits.Items(i).Selected = False
                Next

            End If


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            mdtExperienceDocument.Rows.Clear()
            mintExperienceUnkid = -1
            FillExperienceAttachment()
            'Gajanan [5-Dec-2019] -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BlankObjects :- " & ex.Message, Me)
            DisplayMessage.DisplayError("BlankObjects :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetData()
        Try
            drpEmployee.SelectedValue = CStr(objJobExperience._Employeeunkid)
            TxtCompany.Text = objJobExperience._Company
            TxtAdress.Text = objJobExperience._Address
            dtpStartdate.SetDate = objJobExperience._Start_Date.Date
            dtpEnddate.SetDate = objJobExperience._End_Date.Date
            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            'drpJobs.SelectedValue = objJobExperience._Jobunkid
            txtOldJob.Text = objJobExperience._Job
            'Sohail (21 Mar 2012) -- End
            TxtSuperVisors.Text = objJobExperience._Supervisor
            TxtRemark.Text = objJobExperience._Remark
            TxtLeaveReason.Text = objJobExperience._Leave_Reason
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'TxtGrossPay.Text = Format(objJobExperience._Last_Pay, GUI.fmtCurrency)
            TxtGrossPay.Text = Format(objJobExperience._Last_Pay, CStr(Session("fmtCurrency")))
            'Sohail (02 May 2012) -- End
            TxtContactPerson.Text = objJobExperience._Contact_Person
            chkCanContatEmployee.Checked = objJobExperience._Iscontact_Previous
            TxtContactAddress.Text = objJobExperience._Contact_No
            TxtMemo.Text = objJobExperience._Memo

            'Sohail (21 Mar 2012) -- Start
            'TRA - ENHANCEMENT
            txtOtherBenefits.Text = objJobExperience._OtherBenefit
            txtSign.Text = objJobExperience._CurrencySign
            'Sohail (21 Mar 2012) -- End

            If objJobExperience._PreviousBenefit.Trim.Length > 0 Then

                Dim arBenefit() As String = objJobExperience._PreviousBenefit.Split(CChar(","))

                If arBenefit.Length > 0 Then

                    For i As Integer = 0 To ChkBenefits.Items.Count - 1

                        For j As Integer = 0 To arBenefit.Length - 1

                            If CInt(ChkBenefits.Items(i).Value) = CInt(arBenefit(j)) Then
                                ChkBenefits.Items(i).Selected = True
                                Exit For
                            End If

                        Next
                    Next

                End If

            End If


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetData:- " & ex.Message, Me)
            DisplayMessage.DisplayError("GetData:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'objJobExperience._Experiencetranunkid = CInt(Session("ExpId"))
            objJobExperience._Experiencetranunkid = mintExperienceUnkid

            'Gajanan [17-DEC-2018] -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(drpEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(drpEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                If CInt(Session("UserId")) > 0 Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & objUser._Firstname.Trim & " " & objUser._Lastname & "</b></span></p>")
                ElseIf CInt(Session("Employeeunkid")) > 0 Then
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & objEmployee._Firstname.Trim & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                End If

                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & Session("HOST_NAME").ToString & "</b> and IPAddress : <b>" & Session("IP_ADD").ToString & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Field" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Old Value" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "New Value" & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.EXPERIENCES

                            If objJobExperience._Company.Trim <> TxtCompany.Text.Trim Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Company" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objJobExperience._Company.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & TxtCompany.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objJobExperience._Job.Trim <> txtOldJob.Text.Trim Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Job" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objJobExperience._Job.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtOldJob.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objJobExperience._Start_Date.Date <> dtpStartdate.GetDate.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Start Date" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objJobExperience._Start_Date <> Nothing, objJobExperience._Start_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(Not dtpStartdate.IsNull, dtpStartdate.GetDate.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objJobExperience._End_Date.Date <> dtpEnddate.GetDate.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "End Date" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objJobExperience._End_Date <> Nothing, objJobExperience._End_Date.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(Not dtpEnddate.IsNull, dtpEnddate.GetDate.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True

                            End If

                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            DisplayMessage.DisplayError("Set_Notification : " & ex.Message, Me)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If CInt(Session("ExpId")) > 0 Then
            If mintExperienceUnkid > 0 Then
                'Gajanan [17-DEC-2018] -- End

                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey

                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            Language.setLanguage(mstrModuleName)
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 6, "Notification of Changes in Employee Experience(s).")
                            'Pinkal (06-May-2014) -- End


                            objSendMail._Message = dicNotification(sKey)
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()

                            With objSendMail
                                ._FormName = mstrModuleName
                                If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                    ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                                Else
                                    ._AuditUserId = Convert.ToInt32(Session("UserId"))
                                End If
                                ._ClientIP = Session("IP_ADD").ToString()
                                ._HostName = Session("HOST_NAME").ToString()
                                ._FromWeb = True
                                ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                                ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                            End With

                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'ElseIf CInt(Session("ExpId")) <= 0 Then
            ElseIf mintExperienceUnkid <= 0 Then
                'Gajanan [17-DEC-2018] -- End

                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey

                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            Language.setLanguage(mstrModuleName)
                            objSendMail._Subject = Language.getMessage(mstrModuleName, 7, "Notifications to newly added Employee Experience(s).")
                            'Pinkal (06-May-2014) -- End


                            Dim sMsg As String = dicNotification(sKey)

                            If CInt(Session("UserId")) > 0 Then
                                Dim objUser As New clsUserAddEdit
                                objUser._Userunkid = CInt(Session("UserId"))
                                Set_Notification(objUser._Firstname & " " & objUser._Lastname)
                            ElseIf CInt(Session("Employeeunkid")) > 0 Then
                                Dim objEmp As New clsEmployee_Master

                                'Shani(20-Nov-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objEmp._Employeeunkid = CInt(Session("Employeeunkid"))
                                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                                'Shani(20-Nov-2015) -- End

                                Set_Notification(objEmp._Firstname & "  " & objEmp._Surname)
                            End If

                            objSendMail._Message = sMsg
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Send_Notification : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END



    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtExperienceDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtExperienceDocument.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(drpEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.JOBHISTORYS
                If mintExperienceUnkid <= 0 Then
                    dRow("transactionunkid") = -1
                Else
                    dRow("transactionunkid") = mintExperienceUnkid
                End If
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = Date.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtExperienceDocument.Rows.Add(dRow)
            Call FillExperienceAttachment()

        Catch ex As Exception
            DisplayMessage.DisplayError("AddDocumentAttachment :- " & ex.Message, Me)
        End Try
    End Sub

    Private Sub FillExperienceAttachment()
        Dim dtView As DataView
        Try
            If mdtExperienceDocument Is Nothing Then Exit Sub
            dtView = New DataView(mdtExperienceDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvExperienceAttachment.AutoGenerateColumns = False
            dgvExperienceAttachment.DataSource = dtView
            dgvExperienceAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError("Procedure FillQualificationAttachment : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub DeleteTempFile()
        Try
            If mdtExperienceDocument Is Nothing Then Exit Sub
            Dim xRow() As DataRow = mdtExperienceDocument.Select("localpath <> '' ")
            If xRow.Length > 0 Then
                For Each row As DataRow In xRow
                    If System.IO.File.Exists(row("localpath").ToString) Then
                        System.IO.File.Delete(row("localpath").ToString)
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("DeleteTempFile:- " & ex.Message, Me)
        End Try
    End Sub
    'Gajanan [5-Dec-2019] -- End

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            objJobExperience = New clsJobExperience_tran

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmployeeExperience"
            'StrModuleName2 = "mnuPersonnel"
            'StrModuleName3 = "mnuEmployeeData"
            'clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            'clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            ExperienceApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmJobHistory_ExperienceList)))

            If Session("ExpId") IsNot Nothing Then
                mintExperienceUnkid = CInt(Session("ExpId"))
            End If
            'Gajanan [17-DEC-2018] -- End

            Dim objCommonATLog As New clsCommonATLog

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                objCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'eZeeCommonLib.eZeeDatabase.change_database((Session("mdbname")))
            'Sohail (23 Apr 2012) -- End

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Aruti.Data.User._Object._Userunkid = -1
                'Anjan (28 Feb 2012)-Start
                'ENHANCEMENT : SSRA COMMENTS on Andrew sir's Request
                'BtnSave.Visible = ConfigParameter._Object._AllowAddExperience
                'Anjan (28 Feb 2012)-End 

                'Aruti.Data.User._Object._Userunkid = -1
                btnSave.Visible = CBool(Session("AllowAddExperience"))
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            Else
                objJobExperience._Userunkid = CInt(Session("UserId"))

                'Anjan (30 May 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'BtnSave.Visible = False
                btnSave.Visible = CBool(Session("EditEmployeeExperience"))
                'Anjan (30 May 2012)-End 


            End If

            'Hemant (31 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Call SetMessages()
            Call Language._Object.SaveValue()
            'Hemant (31 May 2019) -- End
            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                FillCombo()

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If Session("ExpId") IsNot Nothing Then
                If mintExperienceUnkid > 0 Then
                    'Gajanan [17-DEC-2018] -- End

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'mintExperienceUnkid = CInt(Session("ExpId"))
                    'objJobExperience._Experiencetranunkid = CInt(Session("ExpId"))
                    objJobExperience._Experiencetranunkid = mintExperienceUnkid


                    GetData()
                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                Else
                    If Session("Experience_EmpUnkID") IsNot Nothing Then
                        drpEmployee.SelectedValue = CStr(Session("Experience_EmpUnkID"))
                    End If
                    'SHANI [09 Mar 2015]--END 


                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If Session("ExpId") IsNot Nothing Then
                        mintExperienceUnkid = CInt(Session("ExpId"))
                    End If

                    'Gajanan [17-DEC-2018] -- End

                    'Gajanan [3-April-2019] -- Start
                    If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                        mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                    End If
                    'Gajanan [3-April-2019] -- End
                End If



                'Gajanan [5-Dec-2019] -- Start   
                'Enhancement:Worked On ADD Attachment In Bio Data Experience    


                If Request.QueryString.Count > 0 Then
                    If Request.QueryString("uploadimage") Is Nothing Then
                        mdtExperienceDocument = objDocument.GetQulificationAttachment(CInt(drpEmployee.SelectedValue), enScanAttactRefId.JOBHISTORYS, mintExperienceUnkid, CStr(Session("Document_Path")))
                        mdtExperienceDocument.Rows.Clear()
                    End If
                End If


                If mintExperienceUnkid > -1 Then
                    mdtExperienceDocument = objDocument.GetQulificationAttachment(CInt(drpEmployee.SelectedValue), enScanAttactRefId.JOBHISTORYS, mintExperienceUnkid, CStr(Session("Document_Path")))

                    If mintExperienceUnkid <= 0 Then
                        mdtExperienceDocument.Rows.Clear()
                    End If
                Else
                    mdtExperienceDocument = objDocument.GetQulificationAttachment(CInt(drpEmployee.SelectedValue), enScanAttactRefId.JOBHISTORYS, -1, CStr(Session("Document_Path")))
                    mdtExperienceDocument.Rows.Clear()
                End If
                Call FillExperienceAttachment()
                'Gajanan [5-Dec-2019] -- End
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPg_EmployeeExperienceList.aspx"
                End If
                'Sohail (09 Nov 2020) -- End
            Else
                'Gajanan [5-Dec-2019] -- Start   
                'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                mdtExperienceDocument = CType(Session("mdtExperienceDocument"), DataTable)
                'Gajanan [5-Dec-2019] -- End
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If Session("ExpId") Is Nothing Or CInt(Session("ExpId")) <= 0 Then
                If mintExperienceUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End
                    btnSave.Visible = CBool(Session("AddEmployeeExperience"))
                Else
                    btnSave.Visible = CBool(Session("EditEmployeeExperience"))
                End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    btnSave.Visible = False
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End


            Else

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If Session("ExpId") Is Nothing Or CInt(Session("ExpId")) <= 0 Then
                If mintExperienceUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End
                    btnSave.Visible = CBool(Session("AllowAddExperience"))
                Else
                    btnSave.Visible = CBool(Session("AllowEditExperience"))
                End If
            End If

            'Pinkal (22-Nov-2012) -- End


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'Gajanan [5-Dec-2019] -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleImageSaprator1(False)
        'ToolbarEntry1.VisibleImageSaprator2(False)
        'ToolbarEntry1.VisibleImageSaprator3(False)
        'ToolbarEntry1.VisibleImageSaprator4(False)
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleNewButton(False)
        'SHANI [01 FEB 2015]--END

        Session("ExpId") = mintExperienceUnkid


        'Gajanan [3-April-2019] -- Start
        ViewState("mblnisEmployeeApprove") = mblnisEmployeeApprove
        'Gajanan [3-April-2019] -- End

        'Gajanan [5-Dec-2019] -- Start   
        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
        If Request.QueryString.Count > 0 Then
            If Request.QueryString("uploadimage") Is Nothing Then
                If Session("mdtExperienceDocument") Is Nothing Then
                    Session.Add("mdtExperienceDocument", mdtExperienceDocument)
                Else
                    Session("mdtExperienceDocument") = mdtExperienceDocument
                End If
            End If
        Else
            If Session("mdtExperienceDocument") Is Nothing Then
                Session.Add("mdtExperienceDocument", mdtExperienceDocument)
            Else
                Session("mdtExperienceDocument") = mdtExperienceDocument
            End If
        End If
        'Gajanan [5-Dec-2019] -- End
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End

            If Validation() Then

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objJobExperience._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    objJobExperience._Userunkid = CInt(Session("UserId"))
                End If

                With objJobExperience
                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeunkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))
                End With

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim ExtraFilter As String = "employeeunkid= " & drpEmployee.SelectedValue.ToString() & " and company = '" & TxtCompany.Text & "' and old_job = '" & txtOldJob.Text & "' "
                Dim ExtraFilterForQuery As String = "employeeunkid= " & drpEmployee.SelectedValue.ToString() & " and company = '" & TxtCompany.Text & "' and old_job = '" & txtOldJob.Text & "' "
                If dtpEnddate.IsNull = False Then
                    ExtraFilter &= " and end_date_fc = '" + eZeeDate.convertDate(dtpEnddate.GetDate).ToString() + "' "
                    ExtraFilterForQuery &= " and convert(varchar(8), cast(end_date as datetime), 112)   =  '" + eZeeDate.convertDate(dtpEnddate.GetDate).ToString() + "' "
                End If

                Dim eLMode As Integer
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    eLMode = enLogin_Mode.MGR_SELF_SERVICE
                Else
                    eLMode = enLogin_Mode.EMP_SELF_SERVICE
                End If
                'Gajanan [17-April-2019] -- End



                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.





                'If Session("ExpId") Is Nothing Then
                If mintExperienceUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End


                    'S.SANDEEP [ 18 SEP 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dicNotification = New Dictionary(Of String, String)
                    If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                        Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                        For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(2).Split(CChar(","))
                            objUsr._Userunkid = CInt(sId)
                            StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                            If dicNotification.ContainsKey(objUsr._Email) = False Then
                                dicNotification.Add(objUsr._Email, StrMessage)
                            End If
                        Next
                        objUsr = Nothing
                    End If

                    'S.SANDEEP [ 18 SEP 2012 ] -- END

                    SetValue()

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'objJobExperience.Insert()

                    'If objJobExperience._Message.Length > 0 Then
                    '    DisplayMessage.DisplayMessage("Entry Not Saved Due To " & objJobExperience._Message, Me)
                    'Else
                    '    DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                    '    'S.SANDEEP [ 18 SEP 2012 ] -- START
                    '    'ENHANCEMENT : TRA CHANGES
                    '    If objJobExperience._Message.Trim.Length <= 0 Then
                    '        trd = New Thread(AddressOf Send_Notification)
                    '        trd.IsBackground = True
                    '        trd.Start()
                    '    End If
                    '    'S.SANDEEP [ 18 SEP 2012 ] -- END
                    '    BlankObjects()
                    'End If


                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.



                    'If ExperienceApprovalFlowVal Is Nothing Then

                    'Gajanan [3-April-2019] -- Start
                    'If ExperienceApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                    If ExperienceApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                        'Gajanan [3-April-2019] -- End
                        'Gajanan [22-Feb-2019] -- End


                        'Gajanan [5-Dec-2019] -- Start   
                        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                        'blnFlag = objAExperienceTran.Insert(CInt(Session("Companyunkid")))
                        blnFlag = objAExperienceTran.Insert(CInt(Session("Companyunkid")), Nothing, False, mdtExperienceDocument)
                        'Gajanan [5-Dec-2019] -- End

                        If blnFlag = False AndAlso objAExperienceTran._Message <> "" Then
                            DisplayMessage.DisplayMessage("Entry Not Saved Due To " & objAExperienceTran._Message, Me)
                            Exit Sub

                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Else
                            objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                         enScreenName.frmJobHistory_ExperienceList, CStr(Session("EmployeeAsOnDate")), _
                                                         CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , drpEmployee.SelectedValue.ToString(), , , ExtraFilter, Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)
                            'Gajanan [17-April-2019] -- End
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2001, "Entry Saved Successfully !!!!!"), Me)
                            'Hemant (31 May 2019) -- End
                            BlankObjects()
                        End If
                    Else

                        'Gajanan [5-Dec-2019] -- Start   
                        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                        'blnFlag = objJobExperience.Insert()
                        blnFlag = objJobExperience.Insert(Nothing, mdtExperienceDocument)
                        'Gajanan [5-Dec-2019] -- End

                        If blnFlag = False Then
                            DisplayMessage.DisplayMessage("Entry Not Saved Due To " & objJobExperience._Message, Me)
                            Exit Sub
                        Else
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2001, "Entry Saved Successfully !!!!!"), Me)
                            'Hemant (31 May 2019) -- End
                            If objJobExperience._Message.Trim.Length <= 0 Then
                                trd = New Thread(AddressOf Send_Notification)
                                trd.IsBackground = True
                                trd.Start()
                            End If
                            BlankObjects()
                        End If
                    End If


                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    'ElseIf Session("ExpId") IsNot Nothing Then
                ElseIf mintExperienceUnkid > 0 Then
                    'objJobExperience._Experiencetranunkid = CInt(Session("ExpId"))
                    objJobExperience._Experiencetranunkid = mintExperienceUnkid
                    'Gajanan [17-DEC-2018] -- End


                    'S.SANDEEP [ 18 SEP 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    dicNotification = New Dictionary(Of String, String)
                    If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                        Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                        For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(2).Split(CChar(","))
                            objUsr._Userunkid = CInt(sId)
                            StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)

                            If dicNotification.ContainsKey(objUsr._Email) = False Then
                                dicNotification.Add(objUsr._Email, StrMessage)
                            End If
                        Next
                        objUsr = Nothing
                    End If


                    'S.SANDEEP [ 18 SEP 2012 ] -- END

                    SetValue()


                    'Gajanan [5-Dec-2019] -- Start   
                    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                    Dim mdsDoc As DataSet
                    Dim mstrFolderName As String = ""
                    mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                    Dim strFileName As String = ""
                    For Each dRow As DataRow In mdtExperienceDocument.Rows
                        mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                        If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                            strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                            If File.Exists(CStr(dRow("localpath"))) Then
                                Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                                If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                                End If

                                File.Move(CStr(dRow("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                                dRow("fileuniquename") = strFileName
                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            Else
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "File does not exist on localpath"), Me)
                                Exit Sub
                            End If
                        ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                            Dim strFilepath As String = dRow("filepath").ToString
                            Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                            If strFilepath.Contains(strArutiSelfService) Then
                                strFilepath = strFilepath.Replace(strArutiSelfService, "")
                                If Strings.Left(strFilepath, 1) <> "/" Then
                                    strFilepath = "~/" & strFilepath
                                Else
                                    strFilepath = "~" & strFilepath
                                End If

                                If File.Exists(Server.MapPath(strFilepath)) Then
                                    File.Delete(Server.MapPath(strFilepath))
                                Else
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "File does not exist on localpath"), Me)
                                    Exit Sub
                                End If
                            Else
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "File does not exist on localpath"), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                    'Gajanan [5-Dec-2019] -- End

                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'objJobExperience.Update()

                    'Gajanan [3-April-2019] -- Start

                    'If ExperienceApprovalFlowVal Is Nothing Then
                    If ExperienceApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                        'Gajanan [3-April-2019] -- End


                        'Gajanan [5-Dec-2019] -- Start   
                        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                        'blnFlag = objAExperienceTran.Insert(CInt(Session("Companyunkid")))
                        blnFlag = objAExperienceTran.Insert(CInt(Session("Companyunkid")), Nothing, False, mdtExperienceDocument)
                        'Gajanan [5-Dec-2019] -- End

                        If blnFlag = False AndAlso objAExperienceTran._Message <> "" Then
                            DisplayMessage.DisplayMessage(objAExperienceTran._Message, Me)
                            Exit Sub

                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        Else
                            objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeJobExperiences), _
                                                         enScreenName.frmJobHistory_ExperienceList, CStr(Session("EmployeeAsOnDate")), _
                                                         CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.EDITED, , drpEmployee.SelectedValue.ToString(), , , ExtraFilter, Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, ExtraFilterForQuery)
                            'Gajanan [17-April-2019] -- End
                        End If
                    Else

                        'Gajanan [5-Dec-2019] -- Start   
                        'Enhancement:Worked On ADD Attachment In Bio Data Experience    
                        'blnFlag = objJobExperience.Update()
                        blnFlag = objJobExperience.Update(Nothing, mdtExperienceDocument)
                        'Gajanan [5-Dec-2019] -- End
                    End If
                    'Gajanan [17-DEC-2018] -- End


                    If objJobExperience._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage("Entry Not Updated Due To " & objJobExperience._Message, Me)
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'DisplayMessage.DisplayMessage("Entry Updated Successfully !!!!!", Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2002, "Entry Updated Successfully !!!!!"), Me)
                        'Hemant (31 May 2019) -- End

                        'S.SANDEEP [ 18 SEP 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        If objJobExperience._Message.Trim.Length <= 0 Then
                            trd = New Thread(AddressOf Send_Notification)
                            trd.IsBackground = True
                            trd.Start()
                        End If
                        'S.SANDEEP [ 18 SEP 2012 ] -- END

                        BlankObjects()
                    End If
                    objJobExperience._Experiencetranunkid = -1
                    'Session("ExpId") = Nothing

                    mintExperienceUnkid = -1
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("BtnSave_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'SHANI [01 FEB 2015]--END 

        'Shani [ 24 DEC 2014 ] -- END
        Try
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Session("ExpId") = Nothing
            mintExperienceUnkid = -1
            'Gajanan [17-DEC-2018] -- End

            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeExperienceList.aspx", False)
            Response.Redirect(mstrURLReferer, False)
            'Sohail (09 Nov 2020) -- End
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Btnclose_Click :- " & ex.Message, Me)
        End Try
    End Sub

    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If Validation(True) = False Then Exit Sub
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtExperienceDocument = CType(Session("mdtExperienceDocument"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillExperienceAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSaveAttachment_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvExperienceAttachment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2007, "No Files to download."), Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & drpEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtExperienceDocument, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnDownloadAll_Click : " & ex.Message, Me)
        End Try
    End Sub
    'Gajanan [5-Dec-2019] -- End


#End Region

#Region " Control's Event(s) "


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        Session("ExpId") = Nothing
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END
    '        Response.Redirect(Session("servername") & "~/HR/wPg_EmployeeExperienceList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("ToolbarEntry1_GridMode_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Protected Sub dgvExperienceAttachment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvExperienceAttachment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(5).Text) > 0 Then
                    xrow = mdtExperienceDocument.Select("scanattachtranunkid = " & CInt(e.Item.Cells(5).Text) & "")
                Else
                    xrow = mdtExperienceDocument.Select("GUID = '" & e.Item.Cells(4).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Language.getMessage(mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                        popup_YesNo.Show()
                        Me.ViewState("DeleteAttRowIndex") = mdtExperienceDocument.Rows.IndexOf(xrow(0))
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""
                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2008, "File does not Exist..."), Me)
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(1).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvExperienceAttachment_ItemCommand:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub dgvExperienceAttachment_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvExperienceAttachment.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                DirectCast(Master.FindControl("script1"), ScriptManager).RegisterPostBackControl(e.Item.Cells(3).FindControl("DownloadLink"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("dgvQualification_ItemDataBound:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonNo_Click:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtExperienceDocument.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtExperienceDocument.AcceptChanges()
                Call FillExperienceAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popup_YesNo_buttonYes_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Gajanan [5-Dec-2019] -- End
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END


            Me.lblInstitution.Text = Language._Object.getCaption(Me.lblInstitution.ID, Me.lblInstitution.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
            Me.lblSupervisor.Text = Language._Object.getCaption(Me.lblSupervisor.ID, Me.lblSupervisor.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.ID, Me.lblEndDate.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.ID, Me.lblAddress.Text)
            Me.lblLastGrossPay.Text = Language._Object.getCaption(Me.lblLastGrossPay.ID, Me.lblLastGrossPay.Text)
            Me.chkCanContatEmployee.Text = Language._Object.getCaption(Me.chkCanContatEmployee.ID, Me.chkCanContatEmployee.Text)
            Me.lblContactNo.Text = Language._Object.getCaption(Me.lblContactNo.ID, Me.lblContactNo.Text)
            Me.lblContacPerson.Text = Language._Object.getCaption(Me.lblContacPerson.ID, Me.lblContacPerson.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.LblMemo.Text = Language._Object.getCaption("gbOtherDetails", Me.LblMemo.Text)
            Me.LblLeaveReason.Text = Language._Object.getCaption("gbLeavingReason", Me.LblLeaveReason.Text)
            Me.tabpBenefit.HeaderText = Language._Object.getCaption(Me.tabpBenefit.ID, Me.tabpBenefit.HeaderText)
            Me.tabpOtherBenefit.HeaderText = Language._Object.getCaption(Me.tabpOtherBenefit.ID, Me.tabpOtherBenefit.HeaderText)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Company Name cannot be blank. Company Name is compulsory information.")
            Language.setMessage(mstrModuleName, 4, "Start Date is compulsory information. Please select Start Date to continue.")
            Language.setMessage(mstrModuleName, 5, "End date cannot be less then or equal to start date.")
            Language.setMessage(mstrModuleName, 6, "Notification of Changes in Employee Experience(s).")
            Language.setMessage(mstrModuleName, 7, "Notifications to newly added Employee Experience(s).")
            Language.setMessage(mstrModuleName, 15, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
            Language.setMessage(mstrModuleName, 16, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
            Language.setMessage(mstrModuleName, 17, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 18, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 19, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
            Language.setMessage(mstrModuleName, 20, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
            Language.setMessage(mstrModuleName, 2001, "Entry Saved Successfully !!!!!")
            Language.setMessage(mstrModuleName, 2002, "Entry Updated Successfully !!!!!")
            Language.setMessage("clsJobExperience_tran", 1, "Particular Job History is already entered for selected employee. Please enter new information.")

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages : " & Ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
