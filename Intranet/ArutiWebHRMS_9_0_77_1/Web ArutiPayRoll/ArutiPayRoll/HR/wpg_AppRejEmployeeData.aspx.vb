﻿Imports Aruti.Data
Imports System.Data
Imports System.Drawing
Imports System.IO
Imports System.IO.Packaging

Partial Class HR_wpg_AppRejEmployeeData
    Inherits Basepage

#Region "Private Variable"

    'Gajanan [9-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
    'Private ReadOnly mstrModuleName As String = "frmAppRejEmployeeData"
    Private ReadOnly mstrModuleName As String = "frmAppRejEmpData"
    'Gajanan [9-July-2019] -- End

    Dim DisplayMessage As New CommonCodes

    Private objApprovalData As New clsEmployeeDataApproval
    Private mdvData As DataView
    Private intPrivilegeId As Integer = 0
    Private eScreenType As enScreenName
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtEmployeeData As DataTable
    Private mdtData As DataTable
    Private objDocument As New clsScan_Attach_Documents


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Private mdtQualificationDocument As DataTable
    Private mdtDocuments As DataTable
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintStartIndex As Integer = 0
    Private mintEndIndex As Integer = 0
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [3-April-2019] -- Start
    Private mblnQualificationNote As Boolean = False

    'Gajanan [3-April-2019] -- End

#End Region

#Region "Private Function"
    Private Sub SetApproverInfo()
        Dim objUMapping As New clsemp_appUsermapping
        Dim dsInfo As New DataSet
        Try
            dsInfo = objUMapping.GetList(enEmpApproverType.APPR_EMPLOYEE, "List", True, " AND hremp_appusermapping.isactive = 1 ", CInt(Session("UserId")), Nothing)
            If dsInfo.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = dsInfo.Tables("List").Rows(0)("User").ToString()
                hfApprover.Value = CInt(dsInfo.Tables("List").Rows(0)("mappingunkid"))

                txtLevel.Text = dsInfo.Tables("List").Rows(0)("Level").ToString()
                hfLevel.Value = CInt(dsInfo.Tables("List").Rows(0)("priority"))
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetApproverInfo", mstrModuleName)
            DisplayMessage.DisplayError("SetApproverInfo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True, , , , , , , , , , , , , , , , True)
            With drpEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("EmployeeList")
                .SelectedValue = CStr("0")
                .DataBind()
            End With

            dsList = objApprovalData.EmployeeDataApprovalList(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("LangId")), "List", True)
            With drpEmployeeData
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = CStr("0")
                .DataBind()
            End With
            mdtEmployeeData = CType(drpEmployeeData.DataSource, DataTable)

            dsList = objApprovalData.getOperationTypeList("List", True)
            With drpOprationType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With

            'S.SANDEEP |15-APR-2019| -- START
            Dim objPeriod As New clscommom_period_Tran
            Dim dsCombo As New DataSet
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.OPEN)
            With cboEffectivePeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            objPeriod = Nothing
            'S.SANDEEP |15-APR-2019| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            DisplayMessage.DisplayError("FillCombo :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objEmp = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Sub SetGridColums()
        Try
            For i As Integer = 1 To gvApproveRejectEmployeeData.Columns.Count - 1
                gvApproveRejectEmployeeData.Columns(i).Visible = False
            Next
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetGridColums", mstrModuleName)
            DisplayMessage.DisplayError("SetGridColums :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    'Private Sub FillGrid()
    '    Try
    '        Dim mstrFilterstring As String = String.Empty

    '        If txtApprover.Text.Trim.Length > 0 Then
    '            If CInt(drpEmployee.SelectedValue) > 0 Then
    '                mstrFilterstring = " EM.employeeunkid = '" & CInt(drpEmployee.SelectedValue) & "'"
    '            End If

    '            If mstrAdvanceFilter.Length > 0 Then
    '                mstrFilterstring &= mstrAdvanceFilter
    '            End If
    '        End If

    '        Dim dt As DataTable = objApprovalData.GetEmployeeApprovalData(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), _
    '                                                                      CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), _
    '                                                                      intPrivilegeId, CInt(Session("UserId")), CInt(hfLevel.Value), False, eScreenType, Session("EmployeeAsOnDate").ToString, mstrFilterstring, CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, False)

    '        mdvData = dt.DefaultView
    '        gvApproveRejectEmployeeData.DataSource = mdvData

    '        mdtData = dt

    '        'If eScreenType = enScreenName.frmQualificationsList Then
    '        '    For Each xdgvr As gridvie DataGridViewRow In gvApproveRejectEmployeeData.Rows
    '        '        If (CStr(xdgvr.Cells(objcolhqualificationgroupunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) And _
    '        '            (CStr(xdgvr.Cells(objcolhqualificationunkid.Index).Value).Trim.Length > 0 AndAlso CInt(xdgvr.Cells(objcolhqualificationunkid.Index).Value) <= 0) Then
    '        '            xdgvr.DefaultCellStyle.ForeColor = Color.Blue
    '        '            lblotherqualificationnote.Visible = True
    '        '        End If
    '        '    Next
    '        'Else
    '        '    lblotherqualificationnote.Visible = true
    '        'End If

    '        If eScreenType <> enScreenName.frmQualificationsList Then
    '            mdtData.Columns.Add("qualificationgroupunkid")
    '            mdtData.Columns("qualificationgroupunkid").DefaultValue = 0
    '            mdtData.Columns.Add("qualificationunkid")
    '            mdtData.Columns("qualificationunkid").DefaultValue = 0

    '            'Gajanan [22-Feb-2019] -- Start
    '            'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '            'mdtData.Columns("linkedmasterid").DefaultValue = 0
    '            'Gajanan [22-Feb-2019] -- End

    '            'Gajanan [22-Feb-2019] -- Start
    '            'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '            mdtData.Columns.Add("qualificationtranunkid")
    '            mdtData.Columns("qualificationtranunkid").DefaultValue = 0
    '            'Gajanan [22-Feb-2019] -- End


    '            'Gajanan [3-April-2019] -- Start
    '            mblnQualificationNote = False
    '            lblotherqualificationnote.Visible = False
    '            'Gajanan [3-April-2019] -- End

    '            'Gajanan [3-April-2019] -- Start
    '        Else
    '            mblnQualificationNote = True
    '            lblotherqualificationnote.Visible = True
    '            'Gajanan [3-April-2019] -- End
    '        End If


    '        'Gajanan [22-Feb-2019] -- Start
    '        'Enhancement - Implementing Employee Approver Flow On Employee Data.


    '        'Gajanan [22-Feb-2019] -- End

    '        If eScreenType <> enScreenName.frmDependantsAndBeneficiariesList Then
    '            mdtData.Columns.Add("dpndtbeneficetranunkid")
    '            mdtData.Columns("dpndtbeneficetranunkid").DefaultValue = 0
    '        End If

    '        Select Case eScreenType
    '            'S.SANDEEP |15-APR-2019| -- START
    '            'Case enScreenName.frmQualificationsList, enScreenName.frmDependantsAndBeneficiariesList
    '            Case enScreenName.frmQualificationsList, enScreenName.frmDependantsAndBeneficiariesList, enScreenName.frmMembershipInfoList
    '                'S.SANDEEP |15-APR-2019| -- END
    '            Case Else
    '                mdtData.Columns.Add("newattachdocumnetid")
    '                mdtData.Columns("newattachdocumnetid").DefaultValue = ""
    '                mdtData.Columns.Add("deleteattachdocumnetid")
    '                mdtData.Columns("deleteattachdocumnetid").DefaultValue = ""
    '        End Select

    '        'S.SANDEEP |26-APR-2019| -- START
    '        Dim dCol As DataColumn
    '        If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "addresstypeid").Count() <= 0 Then
    '            dCol = New DataColumn
    '            With dCol
    '                .DataType = GetType(System.Int32)
    '                .ColumnName = "addresstypeid"
    '                .DefaultValue = 0
    '            End With
    '            mdtData.Columns.Add(dCol)
    '        End If

    '        If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "isbirthinfo").Count() <= 0 Then
    '            dCol = New DataColumn
    '            With dCol
    '                .DataType = GetType(System.Boolean)
    '                .ColumnName = "isbirthinfo"
    '                .DefaultValue = False
    '            End With
    '            mdtData.Columns.Add(dCol)
    '        End If
    '        'S.SANDEEP |26-APR-2019| -- END



    '        Select Case eScreenType
    '            Case enScreenName.frmQualificationsList
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)
    '                'Gajanan [22-Feb-2019] -- End

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualification", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhInstitute", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(1).Visible = True

    '            Case enScreenName.frmJobHistory_ExperienceList
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRemark", False, True)
    '                'Gajanan [22-Feb-2019] -- End
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhJob", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStartDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEndDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSupervisor", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRemark", False, True)).Visible = True


    '            Case enScreenName.frmEmployeeRefereeList
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMobile", False, True)
    '                'Gajanan [22-Feb-2019] -- End
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCountry", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEmail", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhTelNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMobile", False, True)).Visible = True

    '            Case enScreenName.frmEmployee_Skill_List
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDescription", False, True)
    '                'Gajanan [22-Feb-2019] -- End
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkill", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDescription", False, True)).Visible = True
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '            Case enScreenName.frmIdentityInfoList
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdDLClass", False, True)
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSrNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdentityNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdCountry", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssuePlace", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssueDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdExpiryDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdDLClass", False, True)).Visible = True


    '            Case enScreenName.frmDependantsAndBeneficiariesList
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBGender", False, True)

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBRelation", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBbirthdate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBIdentifyNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBGender", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(1).Visible = True
    '                'Gajanan [22-Feb-2019] -- End



    '                'Gajanan [18-Mar-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '            Case enScreenName.frmAddressList
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddress1", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddress2", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCountry", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressState", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCity", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressZipCode", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEstate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince1", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad1", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressChiefdom", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressVillage", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTown", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressMobile", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTel_no", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressPlotNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressAltNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEmail", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)).Visible = True

    '            Case enScreenName.frmEmergencyAddressList
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhFirstname", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLastname", False, True)).Visible = True

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCountry", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressState", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCity", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressZipCode", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEstate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressPlotNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressMobile", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressAltNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTel_no", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEmail", False, True)).Visible = True
    '                'Gajanan [18-Mar-2019] -- End
    '                'S.SANDEEP |15-APR-2019| -- START
    '            Case enScreenName.frmMembershipInfoList
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMemRemark", False, True)

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhmembershipname", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhmembershipno", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhperiod_name", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhissue_date", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhstart_date", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhexpiry_date", False, True)).Visible = True
    '                'S.SANDEEP |15-APR-2019| -- END

    '            Case enScreenName.frmBirthinfo

    '                'Gajanan [17-April-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthChiefdom", False, True)

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthState", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCity", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCertificateNo", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthTown1", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthVillage1", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthWard", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthVillage", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthChiefdom", False, True)).Visible = True

    '                'Gajanan [17-April-2019] -- End

    '            Case enScreenName.frmOtherinfo

    '                mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True)
    '                mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSportsHobbies", False, True)

    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBloodGroup", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEyeColor", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhNationality", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEthinCity", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhReligion", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhHair", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMaritalStatus", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhExtraTel", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage1", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage2", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage3", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage4", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhHeight", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhWeight", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMaritalDate", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAllergies", False, True)).Visible = True
    '                gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSportsHobbies", False, True)).Visible = True
    '        End Select



    '        gvApproveRejectEmployeeData.DataBind()
    '        'SetGridStyle()

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
    '        DisplayMessage.DisplayError("FillGrid :- " & ex.Message, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '    End Try
    'End Sub

    Private Sub FillGrid()
        Try
            Dim mstrFilterstring As String = String.Empty

            If txtApprover.Text.Trim.Length > 0 Then
                If CInt(drpEmployee.SelectedValue) > 0 Then
                    mstrFilterstring = " EM.employeeunkid = '" & CInt(drpEmployee.SelectedValue) & "'"
                End If

                If mstrAdvanceFilter.Length > 0 Then
                    mstrFilterstring &= mstrAdvanceFilter
                End If
            End If

            Dim dt As DataTable = objApprovalData.GetEmployeeApprovalData(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), _
                                                                          CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), _
                                                                          intPrivilegeId, CInt(Session("UserId")), If(hfLevel.Value.ToString = "", 0, CInt(hfLevel.Value)), False, eScreenType, Session("EmployeeAsOnDate").ToString, mstrFilterstring, CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, False)
            'Sohail (13 Aug 2020) - Error: Conversion from "" to Interger; [CInt(hfLevel.Value), 0, CInt(hfLevel.Value))]
            mdvData = dt.DefaultView
            gvApproveRejectEmployeeData.DataSource = mdvData

            mdtData = dt


            Dim dCol As DataColumn
            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "addresstypeid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "addresstypeid"
                    .DefaultValue = 0
                End With
                mdtData.Columns.Add(dCol)
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "isbirthinfo").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Boolean)
                    .ColumnName = "isbirthinfo"
                    .DefaultValue = False
                End With
                mdtData.Columns.Add(dCol)
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "qualificationgroupunkid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "qualificationgroupunkid"
                    .DefaultValue = 0
                End With
                mdtData.Columns.Add(dCol)
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "qualificationunkid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "qualificationunkid"
                    .DefaultValue = 0
                End With
                mdtData.Columns.Add(dCol)
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "qualificationtranunkid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "qualificationtranunkid"
                    .DefaultValue = 0
                End With
                mdtData.Columns.Add(dCol)
            End If


            If eScreenType <> enScreenName.frmQualificationsList Then
                mblnQualificationNote = False
                lblotherqualificationnote.Visible = False
            Else
                mblnQualificationNote = True
                lblotherqualificationnote.Visible = True
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "dpndtbeneficetranunkid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "dpndtbeneficetranunkid"
                    .DefaultValue = 0
                End With
                mdtData.Columns.Add(dCol)
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "newattachdocumentid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.String)
                    .ColumnName = "newattachdocumentid"
                    .DefaultValue = ""
                End With
                mdtData.Columns.Add(dCol)
            End If

            If mdtData.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = "deleteattachdocumentid").Count() <= 0 Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.String)
                    .ColumnName = "deleteattachdocumentid"
                    .DefaultValue = ""
                End With
                mdtData.Columns.Add(dCol)
            End If


            If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
                Dim xCol As New Dictionary(Of String, Type)
                xCol.Add("newattachdocumentid", mdtData.Columns("newattachdocumentid").DataType)
                xCol.Add("deleteattachdocumentid", mdtData.Columns("newattachdocumentid").DataType)
                mdtData.AsEnumerable().ToList().ForEach(Function(x) SetNullToBlank(x, xCol))
                If mdtData.AsEnumerable().Where(Function(x) x.Field(Of String)("newattachdocumentid").Trim.Length > 0 Or x.Field(Of String)("deleteattachdocumentid").Trim.Length > 0).Count > 0 Then
                    gvApproveRejectEmployeeData.Columns(1).Visible = True
                Else
                    gvApproveRejectEmployeeData.Columns(1).Visible = False
                End If
            End If

            Select Case eScreenType
                Case enScreenName.frmQualificationsList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True)


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)
                    If CInt(drpEmployeeData.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhVoidReason", False, True)
                    Else
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)
                    End If
                    'Gajanan [17-April-2019] -- End


                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualifyGroup", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhQualification", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhInstitute", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)).Visible = True


                Case enScreenName.frmJobHistory_ExperienceList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True)

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefNo", False, True)
                    If CInt(drpEmployeeData.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhVoidReason", False, True)
                    Else
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRemark", False, True)
                    End If
                    'Gajanan [17-April-2019] -- End

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCompany", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhJob", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStartDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEndDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSupervisor", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRemark", False, True)).Visible = True


                Case enScreenName.frmEmployeeRefereeList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True)

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMobile", False, True)

                    If CInt(drpEmployeeData.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhVoidReason", False, True)
                    Else
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMobile", False, True)
                    End If
                    'Gajanan [17-April-2019] -- End

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhRefreeName", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCountry", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEmail", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhTelNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMobile", False, True)).Visible = True

                Case enScreenName.frmEmployee_Skill_List
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True)

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDescription", False, True)

                    If CInt(drpEmployeeData.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhVoidReason", False, True)
                    Else
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDescription", False, True)
                    End If
                    'Gajanan [17-April-2019] -- End

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkillCategory", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSkill", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDescription", False, True)).Visible = True
                    
                Case enScreenName.frmIdentityInfoList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True)
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdDLClass", False, True)

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdType", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSrNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdentityNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdCountry", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssuePlace", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssueDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdExpiryDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdDLClass", False, True)).Visible = True


                Case enScreenName.frmDependantsAndBeneficiariesList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True)

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBGender", False, True)

                    If CInt(drpEmployeeData.SelectedValue) = clsEmployeeDataApproval.enOperationType.DELETED Then
                        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhVoidReason", False, True)
                    Else
                        'Sohail (03 Jul 2019) -- Start
                        'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                        'mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBGender", False, True)
                        mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhInactiveReason", False, True)
                        'Sohail (03 Jul 2019) -- End
                    End If


                    'Gajanan [17-April-2019] -- End

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBName", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBRelation", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBbirthdate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBIdentifyNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBGender", False, True)).Visible = True
                    'Sohail (03 Jul 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhInactiveReason", False, True)).Visible = True
                    'Sohail (03 Jul 2019) -- End


                Case enScreenName.frmAddressList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddress1", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddress2", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCountry", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressState", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCity", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressZipCode", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEstate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince1", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad1", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressChiefdom", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressVillage", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTown", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressMobile", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTel_no", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressPlotNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressAltNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEmail", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)).Visible = True

                Case enScreenName.frmEmergencyAddressList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhFirstname", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLastname", False, True)).Visible = True

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressType", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCountry", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressState", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressCity", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressZipCode", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressProvince", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressRoad", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEstate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressPlotNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressMobile", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressAltNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressTel_no", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressFax", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAddressEmail", False, True)).Visible = True

                    
                Case enScreenName.frmMembershipInfoList
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True)
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMemRemark", False, True)

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhCategory", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhmembershipname", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhmembershipno", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhperiod_name", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhissue_date", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhstart_date", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhexpiry_date", False, True)).Visible = True

                Case enScreenName.frmBirthinfo
                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True)
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthChiefdom", False, True)

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCountry", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthState", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCity", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthCertificateNo", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthTown1", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthVillage1", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthWard", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthVillage", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBirthChiefdom", False, True)).Visible = True
                   
                Case enScreenName.frmOtherinfo

                    mintStartIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True)
                    mintEndIndex = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSportsHobbies", False, True)

                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhComplexion", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhBloodGroup", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEyeColor", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhNationality", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEthinCity", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhReligion", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhHair", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMaritalStatus", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhExtraTel", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage1", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage2", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage3", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhLanguage4", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhHeight", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhWeight", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhMaritalDate", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAllergies", False, True)).Visible = True
                    gvApproveRejectEmployeeData.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhSportsHobbies", False, True)).Visible = True
            End Select

            gvApproveRejectEmployeeData.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
            DisplayMessage.DisplayError("FillGrid :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Private Sub ApproveRejectMovement(ByVal status As Integer, ByVal dtTable As DataTable)
    Private Sub ApproveRejectData(ByVal status As Integer, ByVal dtTable As DataTable)
        'Gajanan [22-Feb-2019] -- End

        Try
            Dim enLoginMode As New enLogin_Mode
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
            Else
                enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
            End If
            Dim mstrFilterstring As String = String.Empty

            If CInt(drpEmployee.SelectedValue) > 0 Then
                mstrFilterstring = " EM.employeeunkid = '" & CInt(drpEmployee.SelectedValue) & "'"
            End If

            If mstrAdvanceFilter.Length > 0 Then
                mstrFilterstring &= mstrAdvanceFilter
            End If


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Dim dtAppr As DataTable : Dim drtemp() As DataRow = Nothing : Dim strCheckedEmpIds As String = ""
            Dim dtAppr As DataTable : Dim drtemp() As DataRow = Nothing : Dim strCheckedEmpIds As String = "" : Dim strFinalCheckedEmpIds As String = ""

            Dim mblnIsRejection As Boolean = False
            Dim iStatusunkid As clsEmployee_Master.EmpApprovalStatus
            Select Case status
                Case 1
                    iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                Case 2
                    iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                    mblnIsRejection = True
            End Select


            'dtAppr = objApprovalData.GetNextEmployeeApprovers(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, CInt(hfLevel.Value), False, mstrFilterstring, False)
            If mblnIsRejection Then
                dtAppr = objApprovalData.GetNextEmployeeApprovers(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, 0, False, mstrFilterstring, False)
            Else
            dtAppr = objApprovalData.GetNextEmployeeApprovers(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), Nothing, CInt(hfLevel.Value), False, mstrFilterstring, False)
            End If

            'Gajanan [17-April-2019] -- End


            


            If dtAppr.Rows.Count > 0 Then
                strCheckedEmpIds = String.Join(",", dtAppr.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
            Else
                strCheckedEmpIds = ""
            End If

            If strCheckedEmpIds.Trim.Length > 0 Then
                drtemp = dtTable.Select("employeeunkid not in (" & strCheckedEmpIds & ") AND icheck = true ")
            Else
                drtemp = dtTable.Select("icheck = true ")
            End If

            If drtemp.Length > 0 Then
                For index As Integer = 0 To drtemp.Length - 1
                    drtemp(index)("isfinal") = True
                Next
            End If

            dtTable.AcceptChanges()

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'dtTable = New DataView(dtTable, "icheck = true ", "", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dtTable, "icheck = true and isgrp = 0", "", DataViewRowState.CurrentRows).ToTable
            'Gajanan [17-DEC-2018] -- End


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            drtemp = Nothing
            Dim strCheckedData As String = String.Join("','", dtTable.AsEnumerable().Select(Function(y) y.Field(Of String)("tranguid").ToString).Distinct().ToArray())

            If strCheckedData.Trim.Length > 0 Then
                drtemp = dtAppr.Select("tranguid in ('" & strCheckedData & "')")
            End If

            If drtemp.Length > 0 Then
                For index As Integer = 0 To drtemp.Length - 1
                    drtemp(index)("icheck") = True
                Next
            End If

            dtAppr.AcceptChanges()
            'Gajanan [17-April-2019] -- End



            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmIdentityInfoList) Then
                drtemp = dtTable.Select("")
                drtemp.ToList().ForEach(Function(x) SetColumnData(x))
                dtTable.AcceptChanges()
            End If
            'Gajanan [22-Feb-2019] -- En


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.





            'Dim mblnIsRejection As Boolean = False
            'Dim iStatusunkid As clsEmployee_Master.EmpApprovalStatus
            'Select Case status
            '    Case 1
            '        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
            '    Case 2
            '        iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
            '        mblnIsRejection = True
            'End Select


            strCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())

            strFinalCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Where(Function(s) s.Field(Of Integer)("isfinal") = 1).Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())

            'Gajanan [17-April-2019] -- End


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If objApprovalData.InsertAll(eScreenType, CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), dtTable, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, True, iStatusunkid, txtRemark.Text, CInt(hfApprover.Value), CInt(Session("CompanyUnkId")), getHostName(), getIP(), CStr(Session("UserName")), enLoginMode) Then
            If objApprovalData.InsertAll(eScreenType, CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), dtTable, _
                                         CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, True, iStatusunkid, _
                                         txtRemark.Text, CInt(hfApprover.Value), CInt(Session("CompanyUnkId")), CStr(Session("Database_Name")), CInt(Session("Fin_year")), _
                                         ConfigParameter._Object._CurrentDateAndTime.Date, Session("IsArutiDemo"), _
                                        Session("Total_Active_Employee_ForAllCompany"), Session("NoOfEmployees"), _
                                        Session("UserId"), CBool(Session("DonotAttendanceinSeconds")), _
                                        Session("UserAccessModeSetting"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, Session("IsIncludeInactiveEmp"), _
                                        CBool(Session("AllowToApproveEarningDeduction")), CBool(Session("CreateADUserFromEmpMst")), Session("UserMustchangePwdOnNextLogon"), , _
                                        Session("HOST_NAME").ToString, Session("IP_ADD").ToString, Session("UserName").ToString(), enLoginMode) Then
                'Gajanan [18-Mar-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.




                'If mblnIsRejection = False Then
                '    objApprovalData.SendNotification(2, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text)
                'Else
                '    objApprovalData.SendNotification(3, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text, CType(Session("EmployeeDataRejectedUserIds"), Dictionary(Of Integer, String)))
                'End If


                If strCheckedEmpIds.Length > 0 Then
                If mblnIsRejection = False Then
                        objApprovalData.SendNotification(2, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text, Nothing, "", dtAppr, True)

                Else
                        objApprovalData.SendNotification(3, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text, CType(Session("EmployeeDataRejectedUserIds"), Dictionary(Of Integer, String)), "", dtAppr, True)
                    End If
                End If


                If strFinalCheckedEmpIds.Length > 0 Then
                    If mblnIsRejection Then
                        objApprovalData.SendNotification(3, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text, Nothing, "", dtTable, True)
                    Else
                        objApprovalData.SendNotification(4, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eScreenType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType), CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text, Nothing, "", dtTable, True)
                    End If
                End If
                'Gajanan [17-April-2019] -- End


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Else
                If objApprovalData._Message <> "" Then
                    DisplayMessage.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-DEC-2018] -- End

            End If

            'Gajanan [3-April-2019] -- Start
            txtRemark.Text = ""
            'Gajanan [3-April-2019] -- End
            Call FillGrid()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try
            Using ms As New MemoryStream()
                Using pkg As Package = Package.Open(ms, FileMode.Create)
                    For Each item As String In fileToAdd
                        Try
                        Dim destFilename As String = "/" & Path.GetFileName(item)
                        Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                        Dim byt As Byte() = File.ReadAllBytes(item)

                        Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                        Catch ex As System.ArgumentException
                            Continue For
                        Catch ex As System.UriFormatException
                            Continue For
                        End Try
                    Next
                    pkg.Close()
                End Using
                Response.Clear()
                Response.ContentType = "application/zip"
                Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
                Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
                Response.End()
            End Using
        Catch ex As Exception
            DisplayMessage.DisplayError("AddFileToZip" + ex.Message, Me)
        End Try
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try         'Hemant (13 Aug 2020)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private Function SetColumnData(ByVal dr As DataRow) As Boolean
        Try         'Hemant (13 Aug 2020)
            dr("loginemployeeunkid") = 0
            dr("isvoid") = False
            dr("voidreason") = ""
            dr("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
            dr("audittype") = enAuditType.ADD
            dr("audituserunkid") = Session("UserId")
            dr("ip") = CStr(Session("IP_ADD"))
            dr("host") = CStr(Session("HOST_NAME"))
            dr("form_name") = mstrModuleName
            dr("isweb") = False
            dr("mappingunkid") = CInt(hfApprover.Value)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Function


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    'Private Function DownloadDocumnet(ByVal dtdoc As DataTable, Optional ByVal empid As String = "Document") As Boolean


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    

    'Private Function DownloadDocumnet(ByVal dtdoc As DataTable, ByVal mstrFoldername As String, Optional ByVal empid As String = "Document") As Boolean
    '    'Gajanan [17-April-2019] -- End

    '    Dim filelist As New List(Of String)
    '    Dim xPath As String = ""

    '    'Gajanan [17-April-2019] -- Start
    '    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    '    'Dim mstrFoldername As String = String.Empty


    '    'If CInt(drpEmployeeData.SelectedValue) = enScreenName.frmQualificationsList Then
    '    '    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString

    '    'ElseIf CInt(drpEmployeeData.SelectedValue) = enScreenName.frmDependantsAndBeneficiariesList Then
    '    '    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString
    '    'End If
    '    'Gajanan [17-April-2019] -- End


    '    Dim strError As String = ""
    '    'S.SANDEEP |31-MAY-2019| -- START
    '    'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
    '    'For Each xRow As DataRow In dtdoc.Rows
    '    '    xPath = xRow("filepath")

    '    '    Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, Session("ArutiSelfServiceURL").ToString())
    '    '    If imagebyte IsNot Nothing Then
    '    '        xPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
    '    '        Dim ms As New MemoryStream(imagebyte)
    '    '        Dim fs As New FileStream(xPath, FileMode.Create)
    '    '        ms.WriteTo(fs)
    '    '        ms.Close()
    '    '        fs.Close()
    '    '        fs.Dispose()
    '    '    End If

    '    '    If xPath.Trim <> "" Then
    '    '        Try
    '    '            Dim fileInfo As New IO.FileInfo(xPath)
    '    '            If fileInfo.Exists = False Then
    '    '                'Sohail (23 Mar 2019) -- Start
    '    '                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '    '                'DisplayMessage.DisplayError("File does not Exit...", Me)
    '    '                DisplayMessage.DisplayMessage("File does not Exist...", Me)
    '    '                'Sohail (23 Mar 2019) -- End
    '    '                Exit Function
    '    '            End If
    '    '            fileInfo = Nothing
    '    '        Catch ex As Exception

    '    '        End Try
    '    '    End If
    '    '    filelist.Add(xPath)
    '    'Next

    '    For Each xRow As DataRow In dtdoc.Rows
    '        If IsDBNull(xRow("file_data")) = False Then
    '            xPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
    '            Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
    '            Dim fs As New FileStream(xPath, FileMode.Create)
    '            ms.WriteTo(fs)
    '            ms.Close()
    '            fs.Close()
    '            fs.Dispose()
    '            If xPath <> "" Then
    '                filelist.Add(xPath)
    '            End If
    '        ElseIf IsDBNull(xRow("file_data")) = True Then
    '    Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, Session("ArutiSelfServiceURL").ToString())
    '    If imagebyte IsNot Nothing Then
    '        xPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
    '        Dim ms As New MemoryStream(imagebyte)
    '        Dim fs As New FileStream(xPath, FileMode.Create)
    '        ms.WriteTo(fs)
    '        ms.Close()
    '        fs.Close()
    '        fs.Dispose()
    '    End If
    '            filelist.Add(xPath)
    '        End If
    '    Next
    '    'S.SANDEEP |31-MAY-2019| -- END

    '    AddFileToZip("Qualification_" & empid & "_" & DateTime.Now.ToString("MMyyyyddsshhmm") & ".zip", filelist)

    'End Function

    Private Function DownloadDocument(ByVal dtdoc As DataTable, ByVal mstrFoldername As String, Optional ByVal empid As String = "Document") As Boolean
        Try
            Dim xPath As String = ""
            Dim strError As String = ""
            Dim strLocalPath As String = String.Empty
            Dim fileToAdd As New List(Of String)

            For Each xRow As DataRow In dtdoc.Rows
                If IsDBNull(xRow("file_data")) = False Then
                    strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                    Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                    Dim fs As New FileStream(strLocalPath, FileMode.Create)
                    ms.WriteTo(fs)
                    ms.Close()
                    fs.Close()
                    fs.Dispose()
                    If strLocalPath <> "" Then
                        fileToAdd.Add(strLocalPath)
                    End If
                ElseIf IsDBNull(xRow("file_data")) = True Then
                    If IsSelfServiceExist() Then
                        Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, Session("ArutiSelfServiceURL"))
                        If imagebyte IsNot Nothing Then
                            strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
                            Dim ms As New MemoryStream(imagebyte)
                            Dim fs As New FileStream(strLocalPath, FileMode.Create)
                            ms.WriteTo(fs)
                            ms.Close()
                            fs.Close()
                            fs.Dispose()
                        End If
                        fileToAdd.Add(strLocalPath)
                    Else
                        strLocalPath = xRow("filepath").ToString()
                        If IO.File.Exists(strLocalPath) = True Then
                            fileToAdd.Add(strLocalPath)
                        End If
                    End If
                End If
            Next

            Dim DocumentName As String = String.Empty
            Select Case CInt(drpEmployeeData.SelectedValue)
                Case enScreenName.frmJobHistory_ExperienceList
                    DocumentName = "JobHistory_"
                Case enScreenName.frmQualificationsList
                    DocumentName = "Qualification_"
                Case enScreenName.frmDependantsAndBeneficiariesList
                    DocumentName = "Dependant_"
                Case enScreenName.frmIdentityInfoList
                    DocumentName = "Identity_"
                Case enScreenName.frmAddressList
                    DocumentName = "Address_"
                Case enScreenName.frmEmergencyAddressList
                    DocumentName = "EmergencyAddress_"
                Case enScreenName.frmBirthinfo
                    DocumentName = "Birthinfo_"
                Case enScreenName.frmOtherinfo
                    DocumentName = "Otherinfo_"
            End Select

            AddFileToZip(If(DocumentName = "", "AttachDocument_", DocumentName) & empid & "_" & DateTime.Now.ToString("MMyyyyddsshhmm") & ".zip", fileToAdd)

        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "DownloadDocument", mstrModuleName)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (13 Aug 2020) -- End
        Finally
        End Try

    End Function
    'Gajanan [5-Dec-2019] -- End


    Private Function SetNullToBlank(ByVal dr As DataRow, ByVal xCol As Dictionary(Of String, Type)) As Boolean
        Try         'Hemant (13 Aug 2020)
            If xCol.Keys.Count > 0 Then
                For Each iKey As String In xCol.Keys
                    Select Case xCol(iKey).Name.ToUpper()
                        Case "STRING"
                            If IsDBNull(dr(iKey)) Then dr(iKey) = ""
                        Case "INT32", "INTEGER", "DECIMAL", "DOUBLE"
                            If IsDBNull(dr(iKey)) Then dr(iKey) = 0
                        Case "BOOLEAN"
                            If IsDBNull(dr(iKey)) Then dr(iKey) = False
                        Case Else
                            If IsDBNull(dr(iKey)) Then dr(iKey) = ""
                    End Select
                Next
            End If
            Return True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Function
    'Gajanan [22-Feb-2019] -- End

#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
                Call SetGridColums()
                Call SetApproverInfo()
                Call FillCombo()

                'Gajanan [3-April-2019] -- Start
                lblotherqualificationnote.Visible = False
                'Gajanan [3-April-2019] -- End

            Else
                mdtEmployeeData = CType(ViewState("mdtEmployeeData"), DataTable)
                mdtData = CType(ViewState("mdtData"), DataTable)
                intPrivilegeId = CInt(ViewState("PrivilegeId"))
                eScreenType = CType(ViewState("eScreenType"), enScreenName)
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                mintStartIndex = Me.ViewState("mintStartIndex")
                mintEndIndex = Me.ViewState("mintEndIndex")
                'Gajanan [22-Feb-2019] -- End


                'Gajanan [3-April-2019] -- Start
                If IsNothing(ViewState("mblnQualificationNote")) = False Then
                    mblnQualificationNote = Me.ViewState("mblnQualificationNote")
                End If

                lblotherqualificationnote.Visible = mblnQualificationNote

                'Gajanan [3-April-2019] -- End

            End If

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            Me.ViewState.Add("mdtEmployeeData", mdtEmployeeData)
            Me.ViewState.Add("mdtData", mdtData)
            Me.ViewState.Add("PrivilegeId", intPrivilegeId)
            Me.ViewState.Add("eScreenType", eScreenType)
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState("mintStartIndex") = mintStartIndex
            Me.ViewState("mintEndIndex") = mintEndIndex
            'Gajanan [22-Feb-2019] -- End


            'Gajanan [3-April-2019] -- Start
            Me.ViewState("mblnQualificationNote") = mblnQualificationNote
            'Gajanan [3-April-2019] -- End

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub
#End Region

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(drpEmployeeData.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee Data is mandatory information. Please select employee data to continue"), Me)
                Exit Sub
            End If

            'Gajanan [17-DEC-2018] -- Start
            If CInt(drpOprationType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Opration Type is mandatory information. Please select opration type to continue"), Me)
                Exit Sub
            End If

            'Gajanan [17-DEC-2018] -- End
            Call SetGridColums()
            Call FillGrid()

        Catch ex As Exception
            DisplayMessage.DisplayError("objbtnSearch_Click" + ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub drpEmployeeData_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployeeData.SelectedIndexChanged
        Try         'Hemant (13 Aug 2020)
            If CInt(drpEmployeeData.SelectedValue) > 0 Then
                Dim dataView As DataView = mdtEmployeeData.DefaultView
                dataView.RowFilter = "Id = '" & drpEmployeeData.SelectedValue & "'"
                intPrivilegeId = CInt(dataView.ToTable().Rows(0)("PrivilegeId"))
                eScreenType = CType(dataView.ToTable().Rows(0)("Id"), enScreenName)
                btnShowMyReport.Enabled = True
            Else
                intPrivilegeId = 0
                btnShowMyReport.Enabled = False
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub ChkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)




            If mdtData.Rows(gvr.RowIndex)("isgrp") = 1 Then
                mdtData.Rows(gvr.RowIndex)("icheck") = cb.Checked
                If gvr.Cells.Count > 0 Then
                    Dim dRow() As DataRow = mdtData.Select("isgrp = 0 and employeeunkid = '" & gvApproveRejectEmployeeData.DataKeys(gvr.RowIndex)("employeeunkid").ToString & "'")
                    If dRow.Length > 0 Then
                        'Gajanan [17-DEC-2018] -- Start
                        'dRow(0).Item("icheck") = cb.Checked
                        dRow.ToList.ForEach(Function(x) UpdateRowValue(x, cb.Checked))
                        'Gajanan [17-DEC-2018] -- End
                    End If

                    Dim drRow() As DataRow = mdtData.Select("icheck = 1")

                    If mdtData.Rows.Count = drRow.Length Then
                        CType(gvApproveRejectEmployeeData.HeaderRow.FindControl("ChkAll"), CheckBox).Checked = True
                    Else
                        CType(gvApproveRejectEmployeeData.HeaderRow.FindControl("ChkAll"), CheckBox).Checked = False
                    End If


                End If
            Else
                mdtData.Rows(gvr.RowIndex)("icheck") = cb.Checked
            End If
            mdtData.AcceptChanges()




        Catch ex As Exception
            DisplayMessage.DisplayError("ChkSelect_CheckedChanged" + ex.Message, Me)
        End Try
    End Sub
    'Gajanan [17-DEC-2018] -- Start
    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal blnValue As Boolean) As Boolean
        Try
            If mdtData IsNot Nothing Then
                CType(gvApproveRejectEmployeeData.Rows(mdtData.Rows.IndexOf(dr)).FindControl("ChkSelect"), CheckBox).Checked = blnValue
                dr.Item("icheck") = blnValue
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError("UpdateRowValue : " & ex.Message, Me)
        End Try
    End Function
    'Gajanan [17-DEC-2018] -- End
    Protected Sub ChkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim chkstatus As Boolean = cb.Checked
            For i As Integer = 0 To mdtData.Rows.Count - 1
                mdtData.Rows(i)("icheck") = cb.Checked
                CType(gvApproveRejectEmployeeData.Rows(i).FindControl("ChkSelect"), CheckBox).Checked = cb.Checked
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("ChkAll_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Try
            If mdtData IsNot Nothing Then
                Dim dtTable As DataTable = mdtData

                'S.SANDEEP |15-APR-2019| -- START
                'If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count() <= 0 Then
                If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True And CBool(x.Field(Of Integer)("isgrp")) = False).Count() <= 0 Then
                    'S.SANDEEP |15-APR-2019| -- START
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Please check atleast one employee in order to perform further operation."), Me)
                    Exit Sub
                End If

                'S.SANDEEP |15-APR-2019| -- START
                If CType(sender, Button).Text = btnApprove.Text Then
                    If eScreenType = enScreenName.frmMembershipInfoList Then
                        Dim row = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True And CBool(x.Field(Of Integer)("isgrp")) = False)
                        If row IsNot Nothing AndAlso row.Count > 0 Then
                            Dim blnIsProcessed As Boolean = False
                            For Each r In row
                                Dim objPdata As New clscommom_period_Tran
                                objPdata._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(r("periodunkid"))
                                Dim objProcessed As New clsTnALeaveTran
                                If objProcessed.IsPayrollProcessDone(CInt(r("periodunkid")), CStr(r("employeeunkid")), objPdata._End_Date.Date) = True Then
                                    blnIsProcessed = True
                                    Dim intIndex As Integer = dtTable.Rows.IndexOf(r)
                                    dtTable.Rows(dtTable.Rows.IndexOf(r))("iCheck") = False
                                    dtTable.AcceptChanges()
                                    If intIndex <> -1 Then
                                        gvApproveRejectEmployeeData.Rows(intIndex).ForeColor = Color.Red
                                        CType(gvApproveRejectEmployeeData.Rows(intIndex).Cells(0).FindControl("ChkSelect"), CheckBox).Checked = False
                                    End If
                                End If
                                objProcessed = Nothing
                            Next
                            If blnIsProcessed Then
                                pnlextopr.Visible = True
                                DisplayMessage.DisplayMessage(Language.getMessage("frmEmployeeMaster", 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                End If
                'S.SANDEEP |15-APR-2019| -- END

                Select Case CType(sender, Button).Text
                    Case btnApprove.Text
                        If txtRemark.Text.Trim.Length <= 0 Then
                            Cnf_WithoutRemark.Message = Language.getMessage(mstrModuleName, 3, "Are you sure you want to approve employee(s) without remark?")
                            Cnf_WithoutRemark.Show()
                        Else

                            'Gajanan [22-Feb-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            'Call ApproveRejectMovement(1, dtTable)
                            Call ApproveRejectData(1, dtTable)
                            'Gajanan [22-Feb-2019] -- End


                        End If
                    Case btnReject.Text
                        If txtRemark.Text.Trim.Length <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue."), Me)
                            txtRemark.Focus()
                            Exit Sub
                        End If

                        'Gajanan [22-Feb-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'Call ApproveRejectMovement(2, dtTable)
                        Call ApproveRejectData(2, dtTable)
                        'Gajanan [22-Feb-2019] -- End
                End Select


            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
            DisplayMessage.DisplayError("btnApprove_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub Cnf_WithoutRemark_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cnf_WithoutRemark.buttonYes_Click
        Try
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Call ApproveRejectMovement(1, mdtData)
            Call ApproveRejectData(1, mdtData)
            'Gajanan [22-Feb-2019] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Cnf_WithoutRemark_buttonYes_Click" + ex.Message, Me)
        End Try

    End Sub

    'S.SANDEEP |26-APR-2019| -- START


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    



    'Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim lnk As LinkButton = CType(sender, LinkButton)
    '        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloadall").NamingContainer, GridViewRow)
    '        Dim mdtTran As DataTable = Nothing

    '        Dim strScanAttachRefTranId As String = String.Empty

    '        Dim hfnewattachdocumentid As HiddenField = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfnewattachdocumentid"), HiddenField)
    '        Dim hfdeleteattachdocumentid As HiddenField = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfdeleteattachdocumentid"), HiddenField)

    '        If hfnewattachdocumentid.Value <> "" AndAlso hfnewattachdocumentid.Value <> "&nbsp;" Then
    '            strScanAttachRefTranId &= "," & hfnewattachdocumentid.Value.ToString()
    '        End If
    '        If hfdeleteattachdocumentid.Value <> "" AndAlso hfdeleteattachdocumentid.Value <> "&nbsp;" Then
    '            strScanAttachRefTranId &= "," & hfdeleteattachdocumentid.Value.ToString()
    '        End If
    '        If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)

    '        Dim eAttachmentType As enScanAttactRefId = Nothing

    '        Select Case CInt(drpEmployeeData.SelectedValue)

    '            'Gajanan [5-Dec-2019] -- Start   
    '            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    '            Case enScreenName.frmJobHistory_ExperienceList
    '                eAttachmentType = enScanAttactRefId.JOBHISTORYS
    '                'Gajanan [5-Dec-2019] -- End

    '            Case enScreenName.frmQualificationsList
    '                eAttachmentType = enScanAttactRefId.QUALIFICATIONS
    '            Case enScreenName.frmDependantsAndBeneficiariesList
    '                eAttachmentType = enScanAttactRefId.DEPENDANTS
    '            Case enScreenName.frmIdentityInfoList
    '                eAttachmentType = enScanAttactRefId.IDENTITYS
    '            Case enScreenName.frmAddressList
    '                Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
    '                        eAttachmentType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
    '                        eAttachmentType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
    '                        eAttachmentType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
    '                End Select
    '            Case enScreenName.frmEmergencyAddressList
    '                Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT1
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT2
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT3
    '                End Select
    '            Case enScreenName.frmBirthinfo
    '                If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = True Then
    '                    eAttachmentType = enScanAttactRefId.EMPLOYEE_BIRTHINFO
    '                End If
    '            Case enScreenName.frmOtherinfo
    '                If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = False Then
    '                    eAttachmentType = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS
    '                End If
    '        End Select

    '        If eAttachmentType <> Nothing AndAlso strScanAttachRefTranId.Trim.Length > 0 Then

    '            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), eAttachmentType, 0, _
    '                                                        Session("Document_Path"), 0, strScanAttachRefTranId, True)

    '        End If

    '        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    '            DownloadDocumnet(mdtTran, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnDownloadAll_Click" + ex.Message, Me)
    '    End Try

    'End Sub

    'Protected Sub btnDownloadNewAddedDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try
    '        Dim lnk As LinkButton = CType(sender, LinkButton)
    '        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloadnew").NamingContainer, GridViewRow)
    '        Dim mdtTran As DataTable = Nothing


    '        'Gajanan [17-April-2019] -- Start
    '        'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '        Dim mstrFoldername As String = String.Empty
    '        'Gajanan [17-April-2019] -- End


    '        Dim strScanAttachRefTranId As String = String.Empty

    '        Dim hfnewattachdocumentid As HiddenField = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfnewattachdocumentid"), HiddenField)

    '        If hfnewattachdocumentid.Value <> "" AndAlso hfnewattachdocumentid.Value <> "&nbsp;" Then
    '            strScanAttachRefTranId &= "," & hfnewattachdocumentid.Value.ToString()
    '        End If

    '        If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)

    '        Dim eAttachmentType As enScanAttactRefId = Nothing

    '        Select Case CInt(drpEmployeeData.SelectedValue)


    '            'Gajanan [5-Dec-2019] -- Start   
    '            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    '            Case enScreenName.frmJobHistory_ExperienceList
    '                eAttachmentType = enScanAttactRefId.JOBHISTORYS
    '                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.JOBHISTORYS).Tables(0).Rows(0)("Name").ToString
    '                'Gajanan [5-Dec-2019] -- End

    '            Case enScreenName.frmQualificationsList
    '                eAttachmentType = enScanAttactRefId.QUALIFICATIONS
    '                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString

    '            Case enScreenName.frmDependantsAndBeneficiariesList
    '                eAttachmentType = enScanAttactRefId.DEPENDANTS
    '                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString

    '            Case enScreenName.frmIdentityInfoList
    '                eAttachmentType = enScanAttactRefId.IDENTITYS
    '                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.IDENTITYS).Tables(0).Rows(0)("Name").ToString

    '            Case enScreenName.frmAddressList
    '                Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
    '                        eAttachmentType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
    '                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.PERSONAL_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString

    '                    Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
    '                        eAttachmentType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
    '                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DOMICILE_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString

    '                    Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
    '                        eAttachmentType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
    '                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString

    '                End Select
    '            Case enScreenName.frmEmergencyAddressList
    '                Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT1
    '                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT1).Tables(0).Rows(0)("Name").ToString

    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT2
    '                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT2).Tables(0).Rows(0)("Name").ToString

    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT3
    '                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT3).Tables(0).Rows(0)("Name").ToString
    '                End Select
    '            Case enScreenName.frmBirthinfo
    '                If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = True Then
    '                    eAttachmentType = enScanAttactRefId.EMPLOYEE_BIRTHINFO
    '                    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMPLOYEE_BIRTHINFO).Tables(0).Rows(0)("Name").ToString
    '                End If
    '            Case enScreenName.frmOtherinfo
    '                If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = False Then
    '                    eAttachmentType = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS
    '                    mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMPLYOEE_OTHERLDETAILS).Tables(0).Rows(0)("Name").ToString

    '                End If
    '        End Select

    '        If eAttachmentType <> Nothing AndAlso strScanAttachRefTranId.Trim.Length > 0 Then

    '            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), eAttachmentType, 0, _
    '                                                        Session("Document_Path"), 0, strScanAttachRefTranId, True)

    '        End If


    '        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    '            DownloadDocumnet(mdtTran, mstrFoldername, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    '        Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "There is no newly added attached document(s) available."), Me)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnDownloadNewAddedDocument_Click" + ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnDownloadDeletedDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try
    '        Dim lnk As LinkButton = CType(sender, LinkButton)
    '        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloaddeleted").NamingContainer, GridViewRow)
    '        Dim mdtTran As DataTable = Nothing


    '        Dim strScanAttachRefTranId As String = String.Empty

    '        Dim hfdeleteattachdocumentid As HiddenField = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfdeleteattachdocumentid"), HiddenField)


    '        If hfdeleteattachdocumentid.Value <> "" AndAlso hfdeleteattachdocumentid.Value <> "&nbsp;" Then
    '            strScanAttachRefTranId &= "," & hfdeleteattachdocumentid.Value.ToString()
    '        End If
    '        If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)

    '        Dim eAttachmentType As enScanAttactRefId = Nothing

    '        Select Case CInt(drpEmployeeData.SelectedValue)

    '            'Gajanan [5-Dec-2019] -- Start   
    '            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    '            Case enScreenName.frmJobHistory_ExperienceList
    '                eAttachmentType = enScanAttactRefId.JOBHISTORYS
    '                'Gajanan [5-Dec-2019] -- End

    '            Case enScreenName.frmQualificationsList
    '                eAttachmentType = enScanAttactRefId.QUALIFICATIONS
    '            Case enScreenName.frmDependantsAndBeneficiariesList
    '                eAttachmentType = enScanAttactRefId.DEPENDANTS
    '            Case enScreenName.frmIdentityInfoList
    '                eAttachmentType = enScanAttactRefId.IDENTITYS
    '            Case enScreenName.frmAddressList
    '                Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
    '                        eAttachmentType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
    '                        eAttachmentType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
    '                    Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
    '                        eAttachmentType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
    '                End Select
    '            Case enScreenName.frmEmergencyAddressList
    '                Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT1
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT2
    '                    Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
    '                        eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT3
    '                End Select
    '            Case enScreenName.frmBirthinfo
    '                If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = True Then
    '                    eAttachmentType = enScanAttactRefId.EMPLOYEE_BIRTHINFO
    '                End If
    '            Case enScreenName.frmOtherinfo
    '                If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = False Then
    '                    eAttachmentType = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS
    '                End If
    '        End Select

    '        If eAttachmentType <> Nothing AndAlso strScanAttachRefTranId.Trim.Length > 0 Then

    '            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), eAttachmentType, 0, _
    '                                                        Session("Document_Path"), 0, strScanAttachRefTranId, True)

    '        End If

    '        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    '            DownloadDocumnet(mdtTran, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    '        Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "There is no deleted attached document(s) available."), Me)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnDownloadAll_Click" + ex.Message, Me)
    '    End Try

    'End Sub

    Protected Sub btndownloadAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim mdtTran As DataTable = Nothing
            Dim objDocument As New clsScan_Attach_Documents
            Dim strScanAttachRefTranId As String = String.Empty
            Dim mstrFoldername As String

            Dim lnk As LinkButton = CType(sender, LinkButton)

            Dim hfnewattachdocumentid As HiddenField
            Dim hfdeleteattachdocumentid As HiddenField
            Dim row As GridViewRow = Nothing
            Select Case lnk.ID
                Case "lnkdownloadall"
                    row = CType(lnk.FindControl("lnkdownloadall").NamingContainer, GridViewRow)
                Case "lnkdownloadnew"
                    row = CType(lnk.FindControl("lnkdownloadnew").NamingContainer, GridViewRow)
                Case "lnkdownloaddeleted"
                    row = CType(lnk.FindControl("lnkdownloaddeleted").NamingContainer, GridViewRow)
            End Select


            If IsNothing(row) = False Then
                hfnewattachdocumentid = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfnewattachdocumentid"), HiddenField)
                hfdeleteattachdocumentid = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfdeleteattachdocumentid"), HiddenField)

                If hfnewattachdocumentid.Value <> "" AndAlso hfnewattachdocumentid.Value <> "&nbsp;" Then
                    strScanAttachRefTranId &= "," & hfnewattachdocumentid.Value.ToString()
                End If
                If hfdeleteattachdocumentid.Value <> "" AndAlso hfdeleteattachdocumentid.Value <> "&nbsp;" Then
                    strScanAttachRefTranId &= "," & hfdeleteattachdocumentid.Value.ToString()
                End If
                If strScanAttachRefTranId.Trim.Length > 0 Then strScanAttachRefTranId = Mid(strScanAttachRefTranId, 2)
                Dim eAttachmentType As enScanAttactRefId = Nothing

                Select Case CInt(drpEmployeeData.SelectedValue)
                    Case enScreenName.frmJobHistory_ExperienceList
                        eAttachmentType = enScanAttactRefId.JOBHISTORYS
                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.JOBHISTORYS).Tables(0).Rows(0)("Name").ToString

                    Case enScreenName.frmQualificationsList
                        eAttachmentType = enScanAttactRefId.QUALIFICATIONS
                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
                    Case enScreenName.frmDependantsAndBeneficiariesList
                        eAttachmentType = enScanAttactRefId.DEPENDANTS
                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DEPENDANTS).Tables(0).Rows(0)("Name").ToString
                    Case enScreenName.frmIdentityInfoList
                        eAttachmentType = enScanAttactRefId.IDENTITYS
                        mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.IDENTITYS).Tables(0).Rows(0)("Name").ToString
                    Case enScreenName.frmAddressList
                        Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
                            Case clsEmployeeAddress_approval_tran.enAddressType.PRESENT
                                eAttachmentType = enScanAttactRefId.PERSONAL_ADDRESS_TYPE
                                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.PERSONAL_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString

                            Case clsEmployeeAddress_approval_tran.enAddressType.DOMICILE
                                eAttachmentType = enScanAttactRefId.DOMICILE_ADDRESS_TYPE
                                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.DOMICILE_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString
                            Case clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT
                                eAttachmentType = enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE
                                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE).Tables(0).Rows(0)("Name").ToString
                        End Select
                    Case enScreenName.frmEmergencyAddressList
                        Select Case CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("addresstypeid"))
                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1
                                eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT1
                                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT1).Tables(0).Rows(0)("Name").ToString
                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2
                                eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT2
                                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT2).Tables(0).Rows(0)("Name").ToString
                            Case clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3
                                eAttachmentType = enScanAttactRefId.EMERGENCY_CONTACT3
                                mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMERGENCY_CONTACT3).Tables(0).Rows(0)("Name").ToString
                        End Select
                    Case enScreenName.frmBirthinfo

                        If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = False Then
                            eAttachmentType = enScanAttactRefId.EMPLOYEE_BIRTHINFO
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMPLOYEE_BIRTHINFO).Tables(0).Rows(0)("Name").ToString
                        End If


                    Case enScreenName.frmOtherinfo
                        If CBool(gvApproveRejectEmployeeData.DataKeys(row.RowIndex).Values("isbirthinfo")) = False Then
                            eAttachmentType = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS
                            mstrFoldername = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.EMPLYOEE_OTHERLDETAILS).Tables(0).Rows(0)("Name").ToString
                        End If

                End Select


                If eAttachmentType <> Nothing AndAlso strScanAttachRefTranId.Trim.Length > 0 Then

                    mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), eAttachmentType, 0, _
                                                                Session("Document_Path"), 0, strScanAttachRefTranId, True)

                End If

                If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
                    DownloadDocument(mdtTran, mstrFoldername, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "There is no deleted attached document(s) available."), Me)
                End If
            End If


        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "btndownloadallattachment_Click", mstrModuleName)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (13 Aug 2020) -- End
        Finally
        End Try
    End Sub

    'Gajanan [5-Dec-2019] -- End

    'S.SANDEEP |26-APR-2019| -- END

    Protected Sub btnShowMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowMyReport.Click
        Try
            Pop_report._UserId = CInt(Session("UserId"))
            Pop_report._Priority = CInt(hfLevel.Value)
            Pop_report._PrivilegeId = CInt(intPrivilegeId)
            Pop_report._FillType = eScreenType
            Pop_report._FromApprovalScreen = True
                        'Gajanan [17-DEC-2018] -- Start
            Pop_report._OprationType = CType(CInt(drpOprationType.SelectedValue), clsEmployeeDataApproval.enOperationType)
                        'Gajanan [17-DEC-2018] -- End
            Pop_report.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnShowMyReport_Click" + ex.Message, Me)
        End Try


    End Sub

    'S.SANDEEP |26-APR-2019| -- START

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    'Protected Sub gvApproveRejectEmployeeData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvApproveRejectEmployeeData.RowCommand
    '    Try
    '        If e.CommandName.ToUpper() = "DOWNLOAD" Then
    '            Dim mdtTran As DataTable = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(CInt(e.CommandArgument))("employeeunkid")), enScanAttactRefId.QUALIFICATIONS, CInt(gvApproveRejectEmployeeData.DataKeys(CInt(e.CommandArgument))("linkedmasterid")), Session("Document_Path"))
    '            Dim filelist As New List(Of String)
    '            Dim xPath As String = ""
    '            Dim mstrFoldername As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
    '            Dim strError As String = ""

    '            For Each xRow As DataRow In mdtTran.Rows
    '                xPath = xRow("filepath")

    '                Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xRow("filepath").ToString, xRow("fileuniquename").ToString, mstrFoldername, strError, Session("ArutiSelfServiceURL").ToString())
    '                If imagebyte IsNot Nothing Then
    '                    xPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xRow("fileuniquename").ToString
    '                    Dim ms As New MemoryStream(imagebyte)
    '                    Dim fs As New FileStream(xPath, FileMode.Create)
    '                    ms.WriteTo(fs)
    '                    ms.Close()
    '                    fs.Close()
    '                    fs.Dispose()
    '                End If

    '                If xPath.Trim <> "" Then
    '                    Dim fileInfo As New IO.FileInfo(xPath)
    '                    If fileInfo.Exists = False Then
    '                        DisplayMessage.DisplayError("File does not Exit...", Me)
    '                        Exit Sub
    '                    End If
    '                    fileInfo = Nothing
    '                End If
    '                filelist.Add(xPath)
    '            Next
    '            AddFileToZip("Qualification_" & CInt(gvApproveRejectEmployeeData.DataKeys(CInt(e.CommandArgument))("employeeunkid")) & "_" & DateTime.Now.ToString("MMyyyyddsshhmm") & ".zip", filelist)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("gvApproveRejectEmployeeData_RowCommand" + ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim lnk As LinkButton = CType(sender, LinkButton)
    '        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloadall").NamingContainer, GridViewRow)
    '        Dim mdtTran As DataTable = Nothing

    '        If CInt(drpOprationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.ADDED Then

    '            If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '                                                                enScanAttactRefId.QUALIFICATIONS, _
    '                                                                CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("qualificationtranunkid").ToString), _
    '                                                                Session("Document_Path"), 0, _
    '                                                                CInt(gvApproveRejectEmployeeData.Rows(row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text))

    '            ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '                                                                enScanAttactRefId.DEPENDANTS, _
    '                                                                CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("dpndtbeneficetranunkid").ToString), _
    '                                                                Session("Document_Path"), 0, _
    '                                                                gvApproveRejectEmployeeData.Rows(row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text)

    '            End If

    '        Else
    '            If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '                                                                enScanAttactRefId.QUALIFICATIONS, _
    '                                                                CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("qualificationtranunkid").ToString), _
    '                                                                Session("Document_Path"), 0, "")


    '            ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '                mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '                                                                 enScanAttactRefId.DEPENDANTS, _
    '                                                                 CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("dpndtbeneficetranunkid").ToString), _
    '                                                                 Session("Document_Path"), 0, "")
    '            End If
    '        End If

    '        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    '            DownloadDocumnet(mdtTran, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnDownloadAll_Click" + ex.Message, Me)
    '    End Try

    'End Sub


    ''Gajanan [8-April-2019] -- Start

    ''Protected Sub btnDownloadNewAddedDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    ''    Try
    ''        Dim lnk As LinkButton = CType(sender, LinkButton)
    ''        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloadnew").NamingContainer, GridViewRow)
    ''        Dim mdtTran As DataTable = Nothing


    ''        If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    ''            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    ''                                                            enScanAttactRefId.QUALIFICATIONS, 0, _
    ''                                                            Session("Document_Path"), 1, _
    ''                                                            gvApproveRejectEmployeeData.Rows(row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text)

    ''        ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    ''            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    ''                                                            enScanAttactRefId.DEPENDANTS, 0, _
    ''                                                            Session("Document_Path"), 1, _
    ''                                                            gvApproveRejectEmployeeData.Rows(row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text)
    ''        End If


    ''        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    ''            DownloadDocumnet(mdtTran, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    ''        Else
    ''            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "There is no newly added attached document(s) available."), Me)
    ''        End If

    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError("btnDownloadAll_Click" + ex.Message, Me)
    ''    End Try
    ''End Sub

    'Protected Sub btnDownloadNewAddedDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try
    '        Dim lnk As LinkButton = CType(sender, LinkButton)
    '        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloadnew").NamingContainer, GridViewRow)


    '        Dim hfnewattachdocumentid As HiddenField = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfnewattachdocumnetid"), HiddenField)

    '        Dim mdtTran As DataTable = Nothing


    '        If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '                                                            enScanAttactRefId.QUALIFICATIONS, 0, _
    '                                                            Session("Document_Path"), 1, _
    '                                                            hfnewattachdocumentid.Value)

    '        ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '            mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '                                                            enScanAttactRefId.DEPENDANTS, 0, _
    '                                                            Session("Document_Path"), 1, _
    '                                                            hfnewattachdocumentid.Value)
    '        End If


    '        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    '            DownloadDocumnet(mdtTran, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    '        Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "There is no newly added attached document(s) available."), Me)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnDownloadAll_Click" + ex.Message, Me)
    '    End Try
    'End Sub
    ''Gajanan [8-April-2019] -- End

    'Protected Sub btnDownloadDeletedDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    '    Try
    '        Dim lnk As LinkButton = CType(sender, LinkButton)
    '        Dim row As GridViewRow = CType(lnk.FindControl("lnkdownloaddeleted").NamingContainer, GridViewRow)
    '        Dim hfdeleteattachdocumnetid As HiddenField = CType(gvApproveRejectEmployeeData.Rows(row.RowIndex).FindControl("hfdeleteattachdocumnetid"), HiddenField)

    '        Dim mdtTran As DataTable = Nothing


    '        'Gajanan [8-April-2019] -- Start



    '        'If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '        '    mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '        '                                                    enScanAttactRefId.QUALIFICATIONS, 0, _
    '        '                                                    Session("Document_Path"), 1, _
    '        '                                                    gvApproveRejectEmployeeData.Rows(row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "deleteattachdocumnetid", False, True)).Text)

    '        'ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '        '    mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
    '        '                                                    enScanAttactRefId.DEPENDANTS, 0, _
    '        '                                                    Session("Document_Path"), 1, _
    '        '                                                    gvApproveRejectEmployeeData.Rows(row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "deleteattachdocumnetid", False, True)).Text)
    '        'End If

            'If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
            '    mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
            '                                                    enScanAttactRefId.QUALIFICATIONS, 0, _
            '                                                    Session("Document_Path"), 1, _
    '                                                            hfdeleteattachdocumnetid.Value)

            'ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
            '    mdtTran = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString), _
            '                                                    enScanAttactRefId.DEPENDANTS, 0, _
            '                                                    Session("Document_Path"), 1, _
    '                                                            hfdeleteattachdocumnetid.Value)
            'End If
    '        'Gajanan [8-April-2019] -- End

    '        If IsNothing(mdtTran) = False AndAlso mdtTran.Rows.Count > 0 Then
    '            DownloadDocumnet(mdtTran, gvApproveRejectEmployeeData.DataKeys(row.RowIndex)("employeeunkid").ToString)
    '        Else
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "There is no deleted attached document(s) available."), Me)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("btnDownloadAll_Click" + ex.Message, Me)
    '    End Try

    'End Sub
    'S.SANDEEP |26-APR-2019| -- END

    'Gajanan [22-Feb-2019] -- End


    'Gajanan [8-April-2019] -- Start



    'Protected Sub gvApproveRejectEmployeeData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproveRejectEmployeeData.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp") = 1 Then
    '                'Gajanan [22-Feb-2019] -- Start
    '                'Enhancement - Implementing Employee Approver Flow On Employee Data.
    '                'e.Row.BackColor = Color.Silver
    '                Dim intCount As Integer = 1
    '                For i = mintStartIndex + 1 To mintEndIndex
    '                    If gvApproveRejectEmployeeData.Columns(i).Visible Then
    '                        e.Row.Cells(i).Visible = False
    '                        intCount += 1
    '                    End If
    '                Next
    '                e.Row.Cells(mintStartIndex).ColumnSpan = intCount
    '                e.Row.Cells(0).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                e.Row.Cells(mintStartIndex).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                If gvApproveRejectEmployeeData.Columns(1).Visible = True Then
    '                    e.Row.Cells(1).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    '                    'e.Row.Cells(1).Text = e.Row.Cells(mintStartIndex).Text
    '                    'e.Row.Cells(mintStartIndex).Text = ""
    '                End If
    '            ElseIf CBool(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) = False Then

    '                Select Case CInt(drpEmployeeData.SelectedValue)
    '                    Case enScreenName.frmJobHistory_ExperienceList
    '                        Dim intStartDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStartDate", False, True)
    '                        Dim intEndDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEndDate", False, True)
    '                        If e.Row.Cells(intStartDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intStartDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intStartDtIdx).Text = CDate(e.Row.Cells(intStartDtIdx).Text).Date.ToShortDateString
    '                        End If
    '                        If e.Row.Cells(intEndDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intEndDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intEndDtIdx).Text = CDate(e.Row.Cells(intEndDtIdx).Text).Date.ToShortDateString
    '                        End If
    '                    Case enScreenName.frmQualificationsList
    '                        Dim intAwdStDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)
    '                        Dim intAwdEdDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)
    '                        If e.Row.Cells(intAwdStDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intAwdStDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intAwdStDtIdx).Text = CDate(e.Row.Cells(intAwdStDtIdx).Text).Date.ToShortDateString
    '                        End If
    '                        If e.Row.Cells(intAwdEdDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intAwdEdDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intAwdEdDtIdx).Text = CDate(e.Row.Cells(intAwdEdDtIdx).Text).Date.ToShortDateString
    '                        End If
    '                    Case enScreenName.frmIdentityInfoList
    '                        Dim intIssueDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssueDate", False, True)
    '                        Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdExpiryDate", False, True)
    '                        If e.Row.Cells(intIssueDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intIssueDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intIssueDtIdx).Text = CDate(e.Row.Cells(intIssueDtIdx).Text).Date.ToShortDateString
    '                        End If
    '                        If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
    '                        End If

    '                    Case enScreenName.frmDependantsAndBeneficiariesList
    '                        Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBbirthdate", False, True)
    '                        If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
    '                            e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
    '                        End If
    '                End Select


    '                'Gajanan [22-Feb-2019] -- End
    '            End If

    '            Dim lnkdownload As LinkButton = CType(e.Row.FindControl("lnkdownload"), LinkButton)



    '            If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) AndAlso CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) <= 0 _
    '               Or CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) AndAlso CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) < 0 Then


    '                If CInt(drpOprationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.ADDED AndAlso _
    '                gvApproveRejectEmployeeData.Rows(e.Row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text.Length > 0 Then

    '                    If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
    '                        mdtDocuments = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("employeeunkid").ToString), _
    '                                                                             CInt(enScanAttactRefId.QUALIFICATIONS), _
    '                                                                             CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("qualificationtranunkid").ToString), _
    '                                                                             Session("Document_Path"), 0, _
    '                                                                             CInt(gvApproveRejectEmployeeData.Rows(e.Row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text))

    '                    ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
    '                        mdtDocuments = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("employeeunkid").ToString), _
    '                                                                             enScanAttactRefId.DEPENDANTS, _
    '                                                                             CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("dpndtbeneficetranunkid").ToString), _
    '                                                                             Session("Document_Path"), 0, _
    '                                                                             CInt(gvApproveRejectEmployeeData.Rows(e.Row.RowIndex).Cells(getColumnID_Griview(gvApproveRejectEmployeeData, "objcolhnewattachdocumentid", False, True)).Text))
    '                    End If
    '                End If

    '                If mdtDocuments.Rows.Count > 0 Then
    '                    lnkdownload.Visible = True
    '                    lnkdownload.ToolTip = Language.getMessage(mstrModuleName, 1, "Download")
    '                Else
    '                    lnkdownload.Visible = False
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError("gvApproveRejectEmployeeData_RowDataBound" + ex.Message, Me)
    '    End Try
    'End Sub
    Protected Sub gvApproveRejectEmployeeData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproveRejectEmployeeData.RowDataBound
        Try
        If e.Row.RowType = DataControlRowType.DataRow Then
            If gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp") = 1 Then
                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'e.Row.BackColor = Color.Silver
                    Dim intCount As Integer = 1
                    For i = mintStartIndex + 1 To mintEndIndex
                        If gvApproveRejectEmployeeData.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(mintStartIndex).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
                    e.Row.Cells(mintStartIndex).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
                    If gvApproveRejectEmployeeData.Columns(1).Visible = True Then
                        e.Row.Cells(1).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
                        'e.Row.Cells(1).Text = e.Row.Cells(mintStartIndex).Text
                        'e.Row.Cells(mintStartIndex).Text = ""
                    End If
                ElseIf CBool(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) = False Then

                    Select Case CInt(drpEmployeeData.SelectedValue)
                        Case enScreenName.frmJobHistory_ExperienceList
                            Dim intStartDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhStartDate", False, True)
                            Dim intEndDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhEndDate", False, True)
                            If e.Row.Cells(intStartDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intStartDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intStartDtIdx).Text = CDate(e.Row.Cells(intStartDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intEndDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intEndDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intEndDtIdx).Text = CDate(e.Row.Cells(intEndDtIdx).Text).Date.ToShortDateString
                            End If
                        Case enScreenName.frmQualificationsList
                            Dim intAwdStDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardDate", False, True)
                            Dim intAwdEdDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhAwardToDate", False, True)
                            If e.Row.Cells(intAwdStDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intAwdStDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intAwdStDtIdx).Text = CDate(e.Row.Cells(intAwdStDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intAwdEdDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intAwdEdDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intAwdEdDtIdx).Text = CDate(e.Row.Cells(intAwdEdDtIdx).Text).Date.ToShortDateString
                            End If
                        Case enScreenName.frmIdentityInfoList
                            Dim intIssueDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdIssueDate", False, True)
                            Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhIdExpiryDate", False, True)
                            If e.Row.Cells(intIssueDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intIssueDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intIssueDtIdx).Text = CDate(e.Row.Cells(intIssueDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
            End If

                        Case enScreenName.frmDependantsAndBeneficiariesList
                            Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhDBbirthdate", False, True)
                            If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
                            End If
                            'S.SANDEEP |15-APR-2019| -- START
                        Case enScreenName.frmMembershipInfoList

                            Dim intIssueDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhissue_date", False, True)
                            Dim intStartDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhstart_date", False, True)
                            Dim intExpirDtIdx As Integer = GetColumnIndex.getColumnID_Griview(gvApproveRejectEmployeeData, "dgcolhexpiry_date", False, True)
                            If e.Row.Cells(intIssueDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intIssueDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intIssueDtIdx).Text = CDate(e.Row.Cells(intIssueDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intStartDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intStartDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intStartDtIdx).Text = CDate(e.Row.Cells(intStartDtIdx).Text).Date.ToShortDateString
                            End If
                            If e.Row.Cells(intExpirDtIdx).Text <> "&nbsp;" AndAlso e.Row.Cells(intExpirDtIdx).Text.ToString.Trim.Length > 0 Then
                                e.Row.Cells(intExpirDtIdx).Text = CDate(e.Row.Cells(intExpirDtIdx).Text).Date.ToShortDateString
                            End If
                            'S.SANDEEP |15-APR-2019| -- END

                    End Select


                    'Gajanan [22-Feb-2019] -- End
                End If

                Dim lnkdownload As LinkButton = CType(e.Row.FindControl("lnkdownload"), LinkButton)

                'S.SANDEEP |26-APR-2019| -- START
                'mdtDocuments = Nothing
                'If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) AndAlso CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) <= 0 _
                '   Or CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) AndAlso CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) <= 0 Then


                '    Dim hfnewattachdocumentid As HiddenField = CType(e.Row.FindControl("hfnewattachdocumnetid"), HiddenField)
                '    Dim hfdeleteattachdocumnetid As HiddenField = CType(e.Row.FindControl("deleteattachdocumnetid"), HiddenField)



                '    If CInt(drpOprationType.SelectedValue) = clsEmployeeDataApproval.enOperationType.ADDED AndAlso _
                '    hfnewattachdocumentid.Value.Length > 0 Then


                '        If hfnewattachdocumentid.Value <> "" AndAlso _
                '           hfnewattachdocumentid.Value <> "&nbsp;" Then


                '            If CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmQualificationsList) Then
                '                mdtDocuments = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("employeeunkid").ToString), _
                '                                                                     CInt(enScanAttactRefId.QUALIFICATIONS), _
                '                                                                     CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("qualificationtranunkid").ToString), _
                '                                                                     Session("Document_Path"), 0, _
                '                                                                         hfnewattachdocumentid.Value)

                '            ElseIf CInt(drpEmployeeData.SelectedValue) = CInt(enScreenName.frmDependantsAndBeneficiariesList) Then
                '                mdtDocuments = objDocument.GetQulificationAttachment(CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("employeeunkid").ToString), _
                '                                                                     enScanAttactRefId.DEPENDANTS, _
                '                                                                     CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("dpndtbeneficetranunkid").ToString), _
                '                                                                     Session("Document_Path"), 0, _
                '                                                                         CInt(hfnewattachdocumentid.Value))
                '            End If
                '        End If
                '    End If
                'End If
                'If mdtDocuments IsNot Nothing AndAlso mdtDocuments.Rows.Count > 0 Then
                '    lnkdownload.Visible = True
                '    lnkdownload.ToolTip = Language.getMessage(mstrModuleName, 1, "Download")
                'Else
                '    lnkdownload.Visible = False
                'End If

                Dim hfnewattachdocumentid As HiddenField = CType(e.Row.FindControl("hfnewattachdocumentid"), HiddenField)
                Dim hfdeleteattachdocumnetid As HiddenField = CType(e.Row.FindControl("hfdeleteattachdocumentid"), HiddenField)

                If hfnewattachdocumentid IsNot Nothing Or hfdeleteattachdocumnetid IsNot Nothing Then
                    If CInt(gvApproveRejectEmployeeData.DataKeys(e.Row.RowIndex)("isgrp")) <= 0 Then
                        If hfnewattachdocumentid Is Nothing Or hfdeleteattachdocumnetid Is Nothing Then
                            lnkdownload.Visible = False
                            End If

                        If (hfnewattachdocumentid.Value <> "" AndAlso hfnewattachdocumentid.Value <> "&nbsp;") Or (hfdeleteattachdocumnetid.Value <> "" AndAlso hfdeleteattachdocumnetid.Value <> "&nbsp;") Then
                        lnkdownload.Visible = True
                        lnkdownload.ToolTip = Language.getMessage(mstrModuleName, 1, "Download")
                    Else
                        lnkdownload.Visible = False
                    End If
                    Else
                        lnkdownload.Visible = False
                    End If
                End If
                'S.SANDEEP |26-APR-2019| -- END

                End If

        Catch ex As Exception
            DisplayMessage.DisplayError("gvApproveRejectEmployeeData_RowDataBound" + ex.Message, Me)
        End Try
    End Sub
    'Gajanan [8-April-2019] -- End

    'S.SANDEEP |15-APR-2019| -- START
    Protected Sub lnkSet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSet.Click
        Try
            If mdtData IsNot Nothing Then
                If CInt(IIf(cboEffectivePeriod.SelectedValue = "", 0, cboEffectivePeriod.SelectedValue)) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Effective period is mandatory information. Please select effective period to continue."), Me)
                    Exit Sub
                End If
                If radAll.Checked = False AndAlso radChecked.Checked = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Please tick atleast one operation type whether you want to apply this period to all or checked transaction."), Me)
                    Exit Sub
                End If

                Dim iRow As IEnumerable(Of DataRow) = Nothing
                If radAll.Checked = True Then
                    iRow = mdtData.AsEnumerable().Where(Function(x) CBool(x.Field(Of Integer)("isgrp")) = False)
                ElseIf radChecked.Checked = True Then
                    iRow = mdtData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True And CBool(x.Field(Of Integer)("isgrp")) = False)
                End If
                If iRow IsNot Nothing AndAlso iRow.Count > 0 Then
                    For Each ro In iRow
                        Dim intIndex As Integer = mdtData.Rows.IndexOf(ro)
                        mdtData.Rows(mdtData.Rows.IndexOf(ro))("period_name") = cboEffectivePeriod.SelectedItem.Text
                        mdtData.Rows(mdtData.Rows.IndexOf(ro))("periodunkid") = cboEffectivePeriod.SelectedValue
                        mdtData.Rows(mdtData.Rows.IndexOf(ro))("effetiveperiodid") = cboEffectivePeriod.SelectedValue
                        mdtData.AcceptChanges()
                    Next
                    mdvData = mdtData.DefaultView
                    gvApproveRejectEmployeeData.DataSource = mdvData
                    gvApproveRejectEmployeeData.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkSet_Click : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub drpEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.SelectedIndexChanged
        Try
            If CInt(IIf(drpEmployeeData.SelectedValue = "", 0, drpEmployeeData.SelectedValue)) <> CInt(enScreenName.frmMembershipInfoList) Then
                If pnlextopr.Visible = True Then pnlextopr.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("drpEmployee_SelectedIndexChanged : " & ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |15-APR-2019| -- END

End Class
