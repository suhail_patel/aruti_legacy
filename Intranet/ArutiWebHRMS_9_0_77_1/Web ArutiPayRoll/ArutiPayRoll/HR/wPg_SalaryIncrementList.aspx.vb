﻿Option Strict On 'Shani(19-MAR-2016)

Imports Aruti.Data
Imports eZeeCommonLib
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net.Dns
Imports System.Globalization
Imports System.Drawing

Partial Class HR_wPg_SalaryIncrementList
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objSalaryInc As clsSalaryIncrement
    Private mstrAdvanceFilter As String = ""
    Private mstrAppraisalFilter As String = ""
    Private mdtSalaryIncr As DataTable
    Private objCONN As SqlConnection


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmSalaryIncrementList"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Page's Event "
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'If Session("clsuser") Is Nothing Then
            '    Exit Sub
            'End If



            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 5 Then
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))

                        Me.ViewState.Add("employeeunkid", CInt(arr(2)))
                        Me.ViewState.Add("periodunkid", CInt(arr(3)))
                        Me.ViewState.Add("salaryincrementtranunkid", CInt(arr(4)))

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objCommon As New CommonCodes
                        'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If

                        'Sohail (30 Dec 2013) -- Start
                        'Enhancement - Licence
                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If
                        'Sohail (30 Dec 2013) -- End

                        Dim clsConfig As New clsConfigOptions
                        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        Session("fmtCurrency") = clsConfig._CurrencyFormat

                        'Pinkal (16-Apr-2016) -- Start
                        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                        'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages(0))
                        'Session("DateFormat") = culture.DateTimeFormat.ShortDatePattern.ToString
                        'Pinkal (16-Apr-2016) -- End

                        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                        Else
                            Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                        End If

                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try


                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End

                        Dim objUserPrivilege As New clsUserPrivilege
                        objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                        Session("AllowToViewSalaryChangeList") = objUserPrivilege._AllowToViewSalaryChangeList
                        Session("AllowToApproveSalaryChange") = objUserPrivilege._AllowToApproveSalaryChange

                        'objSalaryInc = New clsSalaryIncrement
                        'objSalaryInc._Salaryincrementtranunkid = CInt(ViewState("salaryincrementtranunkid"))
                        'If objSalaryInc._Isapproved = True Then
                        '    Session.Abandon()
                        '    DisplayMessage.DisplayMessage("You cannot approve this salary change. Reason: This salary change was already approved.", Me, "../Index.aspx")
                        '    Exit Sub
                        'End If


                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("Login") = True
                    Else
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
            End If

            'If ConfigParameter._Object._IsArutiDemo = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            objSalaryInc = New clsSalaryIncrement()
            If Me.IsPostBack = False Then
                Call FillCombo()
                Call CreateTable()
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                Call SetVisibility()
                'Sohail (21 Jan 2020) -- End
                If Request.QueryString.Count > 0 Then
                    Call FillList()
                End If
            Else
                mdtSalaryIncr = CType(ViewState("mdtSalaryIncr"), DataTable)
            End If

            If CInt(cboSalaryChangeApprovalStatus.SelectedValue) <= 0 Then
                btnApprove.Visible = False
                btnDisapprove.Visible = False
            End If

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            dgSalaryIncrement.Columns(1).Visible = False 'Session("EditEarningDeduction")
            dgSalaryIncrement.Columns(2).Visible = False 'Session("AllowToApproveEarningDeduction")
            'SHANI [01 FEB 2015]--END 
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            Me.ViewState.Add("mdtSalaryIncr", mdtSalaryIncr)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsList = objEmployee.GetEmployeeList("Employee", True)
            'End If
            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                If Request.QueryString.Count > 0 Then
                    .SelectedValue = CStr(ViewState("employeeunkid"))
                End If
            End With

            dsList = objMaster.getComboListForSalaryChangeApprovalStatus("SalaryApproval", True)
            With cboSalaryChangeApprovalStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = New DataView(dsList.Tables("SalaryApproval"), "Id <> " & enSalaryChangeApprovalStatus.All & " ", "", DataViewRowState.CurrentRows).ToTable
                .DataBind()
                If Request.QueryString.Count > 0 Then
                    .SelectedValue = CStr(enSalaryChangeApprovalStatus.Pending)
                End If
            End With


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True, , , , Session("Database_Name"))
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            'Shani(20-Nov-2015) -- End

            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Period")
                .DataBind()
            End With
            If Request.QueryString.Count > 0 Then
                cboPayPeriod.SelectedValue = CStr(ViewState("periodunkid"))
            Else
                cboPayPeriod.SelectedValue = CStr(objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1))
            End If
            cboPayPeriod_SelectedIndexChanged(New Object(), New EventArgs())

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillComBo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objMaster = Nothing
            objEmployee = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Private Sub CreateTable()
        Try
            mdtSalaryIncr = New DataTable
            mdtSalaryIncr.Columns.Add("IsCheck", Type.GetType("System.Boolean")).DefaultValue = False
            mdtSalaryIncr.Columns.Add("salaryincrementtranunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtSalaryIncr.Columns.Add("periodunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtSalaryIncr.Columns.Add("period_name", Type.GetType("System.String")).DefaultValue = ""
            mdtSalaryIncr.Columns.Add("employeeunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtSalaryIncr.Columns.Add("employeename", Type.GetType("System.String")).DefaultValue = ""
            mdtSalaryIncr.Columns.Add("incrementdate", Type.GetType("System.String")).DefaultValue = ""
            mdtSalaryIncr.Columns.Add("currentscale", Type.GetType("System.Decimal")).DefaultValue = 0
            mdtSalaryIncr.Columns.Add("increment", Type.GetType("System.Decimal")).DefaultValue = 0
            mdtSalaryIncr.Columns.Add("newscale", Type.GetType("System.Decimal")).DefaultValue = 0
            mdtSalaryIncr.Columns.Add("isapproved", Type.GetType("System.Boolean")).DefaultValue = False
            mdtSalaryIncr.Columns.Add("isfromemployee", Type.GetType("System.Boolean")).DefaultValue = False
            mdtSalaryIncr.Columns.Add("IsGrp", Type.GetType("System.Boolean")).DefaultValue = False

        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'Throw New Exception("CreateTable :- " & ex.Message)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Hemant (13 Aug 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Dim strSearch As String = ""

        Try
            mdtSalaryIncr.Rows.Clear()
            If CBool(Session("AllowToViewSalaryChangeList")) = True Then

                If Request.QueryString.Count > 0 AndAlso CInt(ViewState("salaryincrementtranunkid")) > 0 Then
                    If mstrAdvanceFilter.Trim <> "" Then
                        mstrAdvanceFilter &= " AND prsalaryincrement_tran.salaryincrementtranunkid = " & CInt(ViewState("salaryincrementtranunkid")) & " "
                    Else
                        mstrAdvanceFilter = " prsalaryincrement_tran.salaryincrementtranunkid = " & CInt(ViewState("salaryincrementtranunkid")) & " "
                    End If
                End If

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objSalaryInc.GetList("SalaryInc", cboEmployee.SelectedValue.ToString, , _
                '                             CInt(cboPayPeriod.SelectedValue), "prsalaryincrement_tran.employeeunkid, prsalaryincrement_tran.incrementdate ", mstrAdvanceFilter, , , Session("AccessLevelFilterString"), , CInt(cboSalaryChangeApprovalStatus.SelectedValue))
                dsList = objSalaryInc.GetList(CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              CStr(Session("UserAccessModeSetting")), True, _
                                              CBool(Session("IsIncludeInactiveEmp")), True, _
                                              "SalaryInc", Nothing, cboEmployee.SelectedValue.ToString, _
                                              CInt(cboPayPeriod.SelectedValue), CInt(Session("Fin_year")), _
                                              "prsalaryincrement_tran.employeeunkid, prsalaryincrement_tran.incrementdate ", _
                                              mstrAdvanceFilter, _
                                              CInt(cboSalaryChangeApprovalStatus.SelectedValue), blnAddGrouping:=True)
                'Sohail (21 Jan 2020) - [blnAddGrouping]
                'Shani(20-Nov-2015) -- End


                If mstrAppraisalFilter.Trim.Length > 0 Then
                    strSearch = mstrAppraisalFilter
                End If
                If strSearch.Trim.Length > 0 Then
                    dtTable = New DataView(dsList.Tables("SalaryInc"), strSearch.Substring(4), "employeename", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsList.Tables("SalaryInc"), "", "employeename", DataViewRowState.CurrentRows).ToTable
                End If

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'Dim strPrevKey As String = ""
                'Dim dRow As DataRow
                'For Each dtRow As DataRow In dtTable.Rows

                '    If strPrevKey <> dtRow.Item("employeeunkid").ToString Then
                '        dRow = mdtSalaryIncr.NewRow

                '        dRow.Item("IsGrp") = True
                '        dRow.Item("periodunkid") = CInt(dtRow.Item("employeeunkid"))
                '        dRow.Item("period_name") = dtRow.Item("employeename").ToString

                '        'dRow("salaryincrementtranunkid") = 0
                '        'dRow("period_name") = "None"
                '        'dRow("employeeunkid") =0
                '        'dRow("employeename") = "None"
                '        'dRow("incrementdate") = "None"
                '        'dRow("currentscale") = 0
                '        'dRow("increment") = 0
                '        'dRow("newscale") = 0
                '        'dRow("isapproved") = "False"
                '        'dRow("isfromemployee") = "False"

                '        mdtSalaryIncr.Rows.Add(dRow)
                '    End If

                '    dRow = mdtSalaryIncr.NewRow

                '    dRow("salaryincrementtranunkid") = CInt(dtRow.Item("salaryincrementtranunkid"))
                '    dRow("periodunkid") = CInt(dtRow.Item("periodunkid"))
                '    dRow.Item("period_name") = dtRow.Item("period_name").ToString
                '    dRow("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                '    dRow("employeename") = dtRow.Item("employeename").ToString
                '    dRow("incrementdate") = dtRow.Item("incrementdate").ToString
                '    dRow("currentscale") = CDec(dtRow.Item("currentscale"))
                '    dRow("increment") = CDec(dtRow.Item("increment"))
                '    dRow("newscale") = CDec(dtRow.Item("newscale"))
                '    dRow("isapproved") = CBool(dtRow.Item("isapproved"))
                '    dRow("isfromemployee") = CBool(dtRow.Item("isfromemployee"))
                '    dRow("IsGrp") = False

                '    mdtSalaryIncr.Rows.Add(dRow)

                '    strPrevKey = dtRow.Item("employeeunkid").ToString
                'Next
                mdtSalaryIncr = dtTable
                mdtSalaryIncr.Columns("IsChecked").ColumnName = "IsCheck"
                'Sohail (21 Jan 2020) -- End

                If CInt(cboSalaryChangeApprovalStatus.SelectedValue) = enSalaryChangeApprovalStatus.Approved AndAlso btnDisapprove.Enabled = True Then
                    btnApprove.Visible = False
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'btnDisapprove.Visible = True
                    btnDisapprove.Visible = False
                    'Sohail (21 Jan 2020) -- End
                ElseIf CInt(cboSalaryChangeApprovalStatus.SelectedValue) = enEDHeadApprovalStatus.Pending AndAlso btnApprove.Enabled = True Then
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    'btnDisapprove.Visible = False
                    btnDisapprove.Visible = True
                    'Sohail (21 Jan 2020) -- End
                    btnApprove.Visible = True
                End If

            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList :- " & ex.Message, Me)
            DisplayMessage.DisplayError("FillList :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            If mdtSalaryIncr.Rows.Count <= 0 Then

                Dim drRow As DataRow = mdtSalaryIncr.NewRow
                drRow("salaryincrementtranunkid") = 0
                drRow("periodunkid") = 0
                drRow("period_name") = "None"
                drRow("employeeunkid") = 0
                drRow("employeename") = "None"
                drRow("incrementdate") = "None"
                drRow("currentscale") = 0
                drRow("increment") = 0
                drRow("newscale") = 0
                drRow("isapproved") = False
                drRow("isfromemployee") = False
                drRow("IsGrp") = False

                mdtSalaryIncr.Rows.Add(drRow)
            End If

            dgSalaryIncrement.DataSource = mdtSalaryIncr
            dgSalaryIncrement.DataBind()

        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'btnNew.Visible = Session("AddEarningDeduction")
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'btnApprove.Visible = CBool(Session("AllowToApproveSalaryChange"))
            'btnDisapprove.Visible = CBool(Session("AllowToApproveSalaryChange"))
            btnApprove.Enabled = CBool(Session("AllowToApproveSalaryChange"))
            btnDisapprove.Enabled = CBool(Session("AllowToApproveSalaryChange"))
            'Sohail (21 Jan 2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility :- " & ex.Message, Me)
            DisplayMessage.DisplayError("SetVisibility :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Please select employee in order to do the futher operation on it.", Me)
            '    Exit Sub
            'End If
            FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnSearch_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If cboEmployee.Items.Count > 0 Then cboEmployee.SelectedValue = CStr(0)
            If cboSalaryChangeApprovalStatus.Items.Count > 0 Then cboSalaryChangeApprovalStatus.SelectedIndex = 0
            btnApprove.Visible = False
            btnDisapprove.Visible = False
            FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnReset_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnNew_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim objTnA As New clsTnALeaveTran
        Dim dsList As DataSet
        Try
            Dim dtTab As DataTable = CType(Me.ViewState("mdtSalaryIncr"), DataTable)
            Dim drRow() As DataRow = dtTab.Select("Ischeck = True AND IsGrp = 0 ")

            If drRow.Length <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Please Check atleast one Salary Change to Approve/Disapprove."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub
            End If
            For i As Integer = 0 To drRow.Length - 1
                If CBool(drRow(i)("isfromemployee")) = True Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Some of the checked entries are Auto Generated."), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                Else
                    objPeriod = New clscommom_period_Tran

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objPeriod._Periodunkid = CInt(drRow(i)("periodunkid"))
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(drRow(i)("periodunkid"))
                    'Shani(20-Nov-2015) -- End

                    If objPeriod._Statusid = StatusType.Close Then
                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Language Changes 
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Period is closed for some of the checked entries."), Me)
                        'Pinkal (06-May-2014) -- End
                        Exit Sub
                    ElseIf objTnA.IsPayrollProcessDone(CInt(drRow(i)("periodunkid")), drRow(i)("employeeunkid").ToString, objPeriod._End_Date) = True Then
                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Language Changes 
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Process Payroll is already done for last date of selected Period for some of the ticked employees. Please Void Process Payroll first for some of the ticked employees to Approve/Disapprove salary change."), Me)
                        'Pinkal (06-May-2014) -- End
                        Exit Sub
                    End If

                    dsList = objSalaryInc.getLastIncrement("LastIncr", CInt(drRow(i)("employeeunkid")), True)
                    If dsList.Tables("LastIncr").Rows.Count > 0 Then
                        If CInt(dsList.Tables("LastIncr").Rows(0).Item("salaryincrementtranunkid")) <> CInt(drRow(i)("salaryincrementtranunkid")) Then
                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, You can Approve/Disapprove each employee's Last Salary Change only."), Me)
                            'Pinkal (06-May-2014) -- End
                            Exit Sub
                        End If
                    End If
                End If
            Next
            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            Language.setLanguage(mstrModuleName)
            popupApprove.Message = Language.getMessage(mstrModuleName, 11, "Are you sure you want to Approve selected Salary Change?")
            'Pinkal (06-May-2014) -- End
            popupApprove.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprove_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnApprove_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupApprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupApprove.buttonYes_Click
        Dim blnRefresh As Boolean = False
        Dim objPeriod As clscommom_period_Tran 'Sohail (21 Jan 2020)
        Try
            Dim dtTab As DataTable = CType(Me.ViewState("mdtSalaryIncr"), DataTable)
            Dim drRow() As DataRow = dtTab.Select("Ischeck = True AND IsGrp = 0 ")
            For i As Integer = 0 To drRow.Length - 1
                objSalaryInc = New clsSalaryIncrement

                With objSalaryInc
                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    objPeriod = New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(drRow(i)("periodunkid"))
                    'Sohail (21 Jan 2020) -- End

                    ._Salaryincrementtranunkid = CInt(drRow(i)("salaryincrementtranunkid"))
                    ._Isapproved = True
                    ._Approveruserunkid = CInt(Session("UserId"))

                    'Blank_ModuleName()
                    '._WebFormName = "frmSalaryIncrementList"
                    'StrModuleName2 = "mnuPayroll"
                    '._WebIP = CStr(Session("IP_ADD"))
                    '._WebHostName = CStr(Session("HOST_NAME"))


                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))

                    'Anjan (05 Dec 2012)-Start
                    'ISSUE : grades were not getting updated to employee master from salary increment.
                    'Dim blnResult As Boolean = .Update(False)

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim blnResult As Boolean = .Update(True)
                    Dim blnResult As Boolean = .Update(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                       ConfigParameter._Object._CurrentDateAndTime)
                    'Sohail (21 Jan 2020) - [eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date] = [objPeriod._Start_Date, objPeriod._End_Date]
                    'Shani(20-Nov-2015) -- End

                    'Anjan (05 Dec 2012)-End 
                    If blnResult = False AndAlso objSalaryInc._Message <> "" Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayMessage("popupApprove_buttonYes_Click :- " & ._Message, Me)
                        DisplayMessage.DisplayError("popupApprove_buttonYes_Click :- " & ._Message, Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Try
                    End If
                    blnRefresh = True
                End With
            Next
            If blnRefresh = True Then
                FillList()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupApprove_buttonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupApprove_buttonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnDisapprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisapprove.Click
        Dim objPeriod As New clscommom_period_Tran
        Dim objTnA As New clsTnALeaveTran
        Dim dsList As DataSet
        Try
            Dim dtTab As DataTable = CType(Me.ViewState("mdtSalaryIncr"), DataTable)
            Dim drRow() As DataRow = dtTab.Select("Ischeck = True AND IsGrp = 0 ")

            If drRow.Length <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Please Check atleast one Salary Change to Approve/Disapprove."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub
            End If
            For i As Integer = 0 To drRow.Length - 1
                If CBool(drRow(i)("isfromemployee")) = True Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Some of the checked entries are Auto Generated."), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                Else
                    objPeriod = New clscommom_period_Tran

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objPeriod._Periodunkid = CInt(drRow(i)("periodunkid"))
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(drRow(i)("periodunkid"))
                    'Shani(20-Nov-2015) -- End

                    If objPeriod._Statusid = StatusType.Close Then
                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Language Changes 
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot Approve/Disapprove checked entries. Reason: Period is closed for some of the checked entries."), Me)
                        'Pinkal (06-May-2014) -- End
                        Exit Sub
                    ElseIf objTnA.IsPayrollProcessDone(CInt(drRow(i)("periodunkid")), drRow(i)("employeeunkid").ToString, objPeriod._End_Date) = True Then
                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Language Changes 
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Process Payroll is already done for last date of selected Period for some of the ticked employees. Please Void Process Payroll first for some of the ticked employees to Approve/Disapprove salary change."), Me)
                        'Pinkal (06-May-2014) -- End
                        Exit Sub
                    End If

                    dsList = objSalaryInc.getLastIncrement("LastIncr", CInt(drRow(i)("employeeunkid")), True)
                    If dsList.Tables("LastIncr").Rows.Count > 0 Then
                        If CInt(dsList.Tables("LastIncr").Rows(0).Item("salaryincrementtranunkid")) <> CInt(drRow(i)("salaryincrementtranunkid")) Then
                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, You can Approve/Disapprove each employee's Last Salary Change only."), Me)
                            'Pinkal (06-May-2014) -- End
                            Exit Sub
                        End If
                    End If

                    'Sohail (21 Jan 2020) -- Start
                    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                    Dim intTnAID As Integer = 0
                    intTnAID = objTnA.Get_TnALeaveTranUnkID(CInt(drRow(i).Item("employeeunkid")), CInt(drRow(i).Item("periodunkid")))
                    If intTnAID > 0 Then
                        Dim objPayment As New clsPayment_tran
                        If objPayment.IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, intTnAID, clsPayment_tran.enPaymentRefId.PAYSLIP) = True Then
                            Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, You cannot Disapprove some of the salary change. Reason: Payment of Salary is already done for this period for those employee."), Me)
                            Exit Sub
                        End If
                    End If
                    'Sohail (21 Jan 2020) -- End

                End If
            Next

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            Language.setLanguage(mstrModuleName)
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'popupDisapprove.Message = Language.getMessage(mstrModuleName, 12, "Are you sure you want to Disapprove selected Salary Change?")
            popupDisapprove.Message = Language.getMessage(mstrModuleName, 28, "By disapproving salary change, Selected salary change transactions will be voided.") & "<BR><BR>" & Language.getMessage(mstrModuleName, 12, "Are you sure you want to Disapprove selected Salary Change?")
            'Sohail (21 Jan 2020) -- End
            'Pinkal (06-May-2014) -- End
            popupDisapprove.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDisapprove_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnDisapprove_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDisapprove.buttonYes_Click
        'Dim blnRefresh As Boolean = False 'Sohail (21 Jan 2020)
        Try
            'Sohail (21 Jan 2020) -- Start
            'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
            'Dim dtTab As DataTable = CType(Me.ViewState("mdtSalaryIncr"), DataTable)
            'Dim drRow() As DataRow = dtTab.Select("Ischeck = True AND IsGrp = 0 ")

            'For i As Integer = 0 To drRow.Length - 1
            '    objSalaryInc = New clsSalaryIncrement

            '    With objSalaryInc
            '        ._Salaryincrementtranunkid = CInt(drRow(i)("salaryincrementtranunkid"))
            '        ._Isapproved = False
            '        ._Approveruserunkid = CInt(Session("UserId"))

            '        Blank_ModuleName()
            '        ._WebFormName = "frmSalaryIncrementList"
            '        StrModuleName2 = "mnuPayroll"
            '        ._WebIP = CStr(Session("IP_ADD"))
            '        ._WebHostName = CStr(Session("HOST_NAME"))


            '        'Shani(20-Nov-2015) -- Start
            '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '        'Dim blnResult As Boolean = .Update(False)
            '        Dim blnResult As Boolean = .Update(CStr(Session("Database_Name")), _
            '                                           CInt(Session("UserId")), _
            '                                           CInt(Session("Fin_year")), _
            '                                           CInt(Session("CompanyUnkId")), _
            '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                           CStr(Session("UserAccessModeSetting")), True, _
            '                                           CBool(Session("IsIncludeInactiveEmp")), False, ConfigParameter._Object._CurrentDateAndTime)
            '        'Shani(20-Nov-2015) -- End

            '        If blnResult = False AndAlso objSalaryInc._Message <> "" Then
            '            'Sohail (23 Mar 2019) -- Start
            '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '            'DisplayMessage.DisplayMessage("popupDisapprove_buttonYes_Click :- " & ._Message, Me)
            '            DisplayMessage.DisplayError("popupDisapprove_buttonYes_Click :- " & ._Message, Me)
            '            'Sohail (23 Mar 2019) -- End
            '            Exit Try
            '        End If
            '        blnRefresh = True
            '    End With
            'Next
            'If blnRefresh = True Then
            '    FillList()
            'End If
            Language.setLanguage(mstrModuleName)
            popRejectReason.Reason = ""
            popRejectReason.Title = Language.getMessage(mstrModuleName, 27, "Comments")
            popRejectReason.Show()
            'Sohail (21 Jan 2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDisapprove_buttonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("popupDisapprove_buttonYes_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (21 Jan 2020) -- Start
    'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
    Protected Sub popRejectReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popRejectReason.buttonDelReasonYes_Click
        Dim blnRefresh As Boolean = False
        Dim objPeriod As clscommom_period_Tran
        Try
            Dim dtTab As DataTable = CType(Me.ViewState("mdtSalaryIncr"), DataTable)
            Dim drRow() As DataRow = dtTab.Select("Ischeck = True AND IsGrp = 0 ")

            For i As Integer = 0 To drRow.Length - 1
                objSalaryInc = New clsSalaryIncrement

                With objSalaryInc
                    objPeriod = New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(drRow(i)("periodunkid"))

                    ._Salaryincrementtranunkid = CInt(drRow(i)("salaryincrementtranunkid"))
                    ._Isapproved = False
                    ._Approveruserunkid = CInt(Session("UserId"))

                    'Blank_ModuleName()
                    '._WebFormName = "frmSalaryIncrementList"
                    'StrModuleName2 = "mnuPayroll"
                    '._WebIP = CStr(Session("IP_ADD"))
                    '._WebHostName = CStr(Session("HOST_NAME"))

                    ._FormName = mstrModuleName
                    If Convert.ToInt32(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        ._LoginEmployeeUnkid = Convert.ToInt32(Session("Employeeunkid"))
                    Else
                        ._AuditUserId = Convert.ToInt32(Session("UserId"))
                    End If
                    ._ClientIP = Session("IP_ADD").ToString()
                    ._HostName = Session("HOST_NAME").ToString()
                    ._FromWeb = True
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._CompanyUnkid = Convert.ToInt32(Session("CompanyUnkId"))

                    Dim blnResult As Boolean = .Void(CStr(Session("Database_Name")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), CInt(drRow(i)("salaryincrementtranunkid")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popRejectReason.Reason, True, ConfigParameter._Object._CurrentDateAndTime, True, "")
                    'Sohail (24 Feb 2022) - [CBool(Session("AllowToApproveEarningDeduction"))]=[True]

                    If blnResult = False AndAlso objSalaryInc._Message <> "" Then
                        DisplayMessage.DisplayError("popupDisapprove_buttonYes_Click :- " & ._Message, Me)
                        Exit Try
                    End If
                    blnRefresh = True
                End With
            Next
            If blnRefresh = True Then
                FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("popRejectReason_buttonDelReasonYes_Click :- " & ex.Message, Me)
        End Try
    End Sub
    'Sohail (21 Jan 2020) -- End

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    'Shani [01 FEB 2015] -- END
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Shani [ 24 DEC 2014 ] -- END
        Try
            Session("EDId") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError("btnClose_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


#End Region

#Region "ComboBox Event"

    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Try
            If CInt(cboPayPeriod.SelectedValue) > 0 Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPayPeriod.SelectedValue)
                'Shani(20-Nov-2015) -- End

                Me.ViewState.Add("StartDate", objPeriod._Start_Date)
                Me.ViewState.Add("EndDate", objPeriod._End_Date)
            Else
                Me.ViewState("StartDate") = Nothing
                Me.ViewState("EndDate") = Nothing
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboPayPeriod_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("cboPayPeriod_SelectedIndexChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            For i As Integer = 0 To mdtSalaryIncr.Rows.Count - 1
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                If CBool(mdtSalaryIncr.Rows(i)("IsGrp")) = True Then Continue For
                'Sohail (21 Jan 2020) -- End
                If CBool(mdtSalaryIncr.Rows(i)("isfromemployee")) = False AndAlso mdtSalaryIncr.Rows(i)("incrementdate").ToString.Trim <> "None" Then
                    Dim gvRow As GridViewRow = dgSalaryIncrement.Rows(i)
                    CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                    mdtSalaryIncr.Rows(i)("IsCheck") = cb.Checked
                End If
            Next
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelectAll_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
            mdtSalaryIncr.Rows(gvRow.RowIndex)("IsCheck") = cb.Checked

            If CBool(mdtSalaryIncr.Rows(gvRow.RowIndex).Item("IsGrp")) = True Then
                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'Dim dRow() As DataRow = mdtSalaryIncr.Select("employeeunkid = '" & mdtSalaryIncr.Rows(gvRow.RowIndex).Item("periodunkid").ToString & "' AND IsGrp = 0 AND isfromemployee = 0 AND incrementdate <> 'None' ")
                Dim dRow() As DataRow = mdtSalaryIncr.Select("employeeunkid = '" & mdtSalaryIncr.Rows(gvRow.RowIndex).Item("employeeunkid").ToString & "' AND IsGrp = 0 AND isfromemployee = 0 AND incrementdate <> 'None' ")
                'Sohail (21 Jan 2020) -- End

                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("IsCheck") = cb.Checked
                        Dim gRow As GridViewRow = dgSalaryIncrement.Rows(mdtSalaryIncr.Rows.IndexOf(dRow(i)))
                        CType(gRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                    Next
                End If
            End If

            Dim drcheckedRow() As DataRow = mdtSalaryIncr.Select("Ischeck = True AND isfromemployee = 0 ")
            Dim drRow() As DataRow = mdtSalaryIncr.Select("isfromemployee = 0 ")

            If drcheckedRow.Length = drRow.Length Then
                CType(dgSalaryIncrement.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(dgSalaryIncrement.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError("chkSelect_CheckedChanged :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub dgSalaryIncrement_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgSalaryIncrement.RowCommand
        Try

            Dim dtSalary As DataTable = CType(Me.ViewState("mdtSalaryIncr"), DataTable)
            If e.CommandName = "Change" Then

                Dim objPeriod As New clscommom_period_Tran

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = CInt(dtSalary.Rows(e.CommandArgument)("periodunkid"))
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dtSalary.Rows(CInt(e.CommandArgument))("periodunkid"))
                'Shani(20-Nov-2015) -- End

                If objPeriod._Statusid = 2 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'DisplayMessage.DisplayMessage("Sorry! You can not Edit/Delete this transaction. Reason: Period is closed.", Me)
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, You cannot Edit/Delete this entry. Reason: Period is closed."), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                Dim objTranHead As New clsTransactionHead

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTranHead._Tranheadunkid = CInt(dtSalary.Rows(e.CommandArgument)("tranheadunkid"))
                objTranHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(dtSalary.Rows(CInt(e.CommandArgument))("tranheadunkid"))
                'Shani(20-Nov-2015) -- End

                If objTranHead._Typeof_id = enTypeOf.Salary Then
                    DisplayMessage.DisplayMessage("Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee.", Me)
                    Exit Try
                End If

                Session.Add("EDId", CInt(dtSalary.Rows(CInt(e.CommandArgument))("edunkid")))
                Response.Redirect(CStr(Session("servername")) & "~/Payroll/wPg_EarningDeduction_AddEdit.aspx", False)

            ElseIf e.CommandName = "Remove" Then


                'Dim dtPeriodStart As Date
                'Dim dtPeriodEnd As Date
                'Dim objPeriod As New clscommom_period_Tran
                'objPeriod._Periodunkid = CInt(dtED.Rows(e.CommandArgument)("periodunkid"))
                'If objPeriod._Statusid = 2 Then
                '    DisplayMessage.DisplayMessage("Sorry! You can not Edit/Delete this transaction. Reason: Period is closed.", Me)
                '    Exit Sub
                'Else
                '    dtPeriodStart = objPeriod._Start_Date
                '    dtPeriodEnd = objPeriod._End_Date
                'End If

                'Dim objTranHead As New clsTransactionHead
                'Dim objMaster As New clsMasterData
                'Dim objPayment As New clsPayment_tran
                'Dim objTnALeaveTran As New clsTnALeaveTran
                'Dim intOpenPeriod As Integer = 0
                'Dim dtOpenPeriodEnd As DateTime = Nothing
                'Dim dsList As DataSet = Nothing

                'intOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, Session("Fin_year"))
                'objPeriod._Periodunkid = intOpenPeriod
                'dtOpenPeriodEnd = objPeriod._End_Date

                'dsList = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, intOpenPeriod, clsPayment_tran.enPayTypeId.PAYMENT, dtED.Rows(e.CommandArgument)("employeeunkid").ToString())
                'If dsList.Tables("Payment").Rows.Count > 0 Then
                '    DisplayMessage.DisplayMessage("Sorry, You can not Delete this Transaction Head. Reason : Payment is already done for current open Period. Please Delete payment first to Delete this transaction head.", Me)
                '    Exit Try
                'End If

                'If objTnALeaveTran.IsPayrollProcessDone(intOpenPeriod, dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), dtOpenPeriodEnd) = True Then
                '    DisplayMessage.DisplayMessage("Sorry, You can not Delete this Transaction Head. Reason : Process Payroll is already done for last date of current open Period. Please Void Process Payroll first to Delete this transaction head.", Me)
                '    Exit Try
                'End If

                'objTranHead._Tranheadunkid = CInt(dtED.Rows(e.CommandArgument)("tranheadunkid").ToString())
                'If objTranHead._Typeof_id = enTypeOf.Salary Then
                '    Dim ds As DataSet
                '    ds = objED.GetList("EDSalary", dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), , , , , , , , , , , , , , , , , dtPeriodStart)

                '    If ds.Tables("EDSalary").Rows.Count <= 0 Then 'Only One period ED is aasigned so after deleting this salary head no salary head will remain on ED.
                '        DisplayMessage.DisplayMessage("Sorry, You can not Edit/Delete Salary Head. Please go to Employee Master to Edit/Delete this Head for this employee.", Me)
                '        Exit Try
                '    End If

                '    ds = objED.GetList("EDSalary", dtED.Rows(e.CommandArgument)("employeeunkid").ToString(), , , , , , , , , , , , , , , , , dtPeriodEnd)
                '    If ds.Tables("EDSalary").Rows.Count > 1 Then 'Other Heads are asssigned besides Salary Heads. First delete other heads then finally Salary Heads.
                '        DisplayMessage.DisplayMessage("Sorry, You can not Delete Salary Head when other Heads are assigned. Please delete other Heads First.", Me)
                '        Exit Try
                '    End If

                'End If
                'Me.ViewState.Add("RIndex", e.CommandArgument)
                'popupDelete.Show()

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgED_RowCommand :- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgED_RowCommand :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgSalaryIncrement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgSalaryIncrement.RowDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.Cells(3).Text = "None" AndAlso e.Row.Cells(4).Text = "None" Then
                e.Row.Visible = False
            Else

                dgSalaryIncrement.Columns(1).Visible = False 'Session("EditEarningDeduction")
                dgSalaryIncrement.Columns(2).Visible = False 'Session("AllowToApproveEarningDeduction")

                'Sohail (21 Jan 2020) -- Start
                'NMB Enhancement # : Actual increment date on salary change screen for informational purpose.
                'If CBool(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                'For i = 4 To dgSalaryIncrement.Columns.Count - 1
                '    e.Row.Cells(i).Visible = False
                'Next

                'e.Row.Cells(2).Text = "Employee : " & e.Row.Cells(2).Text

                'e.Row.Cells(4).ColumnSpan = dgSalaryIncrement.Columns.Count - 2
                ''SHANI [01 FEB 2015]-START
                ''Enhancement - REDESIGN SELF SERVICE.
                ''e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                ''e.Row.Cells(3).CssClass = "GroupHeaderStyleBorderRight"
                'e.Row.CssClass = "GroupHeaderStyle"
                ''SHANI [01 FEB 2015]--END 

                'e.Row.CssClass = "GroupHeaderStyle"
                If CBool(dgSalaryIncrement.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    'Sohail (13 Feb 2020) -- Start
                    'NMB Enhancement # : show employee code on salary change list.
                    'e.Row.Cells(3).Text = "Employee : " & DataBinder.Eval(e.Row.DataItem, "employeename").ToString
                    e.Row.Cells(3).Text = "Employee : " & DataBinder.Eval(e.Row.DataItem, "employeename").ToString & " - [" & DataBinder.Eval(e.Row.DataItem, "employeecode").ToString & "]"
                    'Sohail (13 Feb 2020) -- End
                    e.Row.Cells(3).ColumnSpan = e.Row.Cells.Count - 4
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i = 5 To dgSalaryIncrement.Columns.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                    'Sohail (21 Jan 2020) -- End

                Else
                    If CBool(DataBinder.Eval(e.Row.DataItem, "isfromemployee")) = True Then
                        e.Row.Cells(0).Enabled = False
                    End If
                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'e.Row.Cells(4).Text = eZeeDate.convertDate(e.Row.Cells(4).Text).ToString(Session("DateFormat").ToString)
                    e.Row.Cells(4).Text = eZeeDate.convertDate(e.Row.Cells(4).Text).ToShortDateString
                    'Pinkal (16-Apr-2016) -- End
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), CStr(Session("fmtcurrency")))
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), CStr(Session("fmtcurrency")))
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), CStr(Session("fmtcurrency")))

                    If CBool(e.Row.Cells(8).Text) Then
                        e.Row.Cells(8).Text = "A"
                    Else
                        e.Row.Cells(8).Text = "P"
                    End If
                End If



            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgED_RowDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError("dgED_RowDataBound :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    ''Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    ''    Try
    ''        Session("EDId") = Nothing
    ''        Response.Redirect("~\UserHome.aspx")
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError("Closebotton_CloseButton_click : -  " & ex.Message, Me)
    ''    End Try

    ''End Sub

#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            ''Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblAccess.Text = Language._Object.getCaption(Me.lblAccess.ID, Me.lblAccess.Text)
            Me.lblSalaryChangeApprovalStatus.Text = Language._Object.getCaption(Me.lblSalaryChangeApprovalStatus.ID, Me.lblSalaryChangeApprovalStatus.Text)

            Me.dgSalaryIncrement.Columns(1).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(1).FooterText, Me.dgSalaryIncrement.Columns(1).HeaderText).Replace("&", "")
            Me.dgSalaryIncrement.Columns(2).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(2).FooterText, Me.dgSalaryIncrement.Columns(2).HeaderText).Replace("&", "")
            Me.dgSalaryIncrement.Columns(3).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(3).FooterText, Me.dgSalaryIncrement.Columns(3).HeaderText)
            Me.dgSalaryIncrement.Columns(4).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(4).FooterText, Me.dgSalaryIncrement.Columns(4).HeaderText)
            Me.dgSalaryIncrement.Columns(5).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(5).FooterText, Me.dgSalaryIncrement.Columns(5).HeaderText)
            Me.dgSalaryIncrement.Columns(6).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(6).FooterText, Me.dgSalaryIncrement.Columns(6).HeaderText)
            Me.dgSalaryIncrement.Columns(7).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(7).FooterText, Me.dgSalaryIncrement.Columns(7).HeaderText)
            Me.dgSalaryIncrement.Columns(8).HeaderText = Language._Object.getCaption(Me.dgSalaryIncrement.Columns(8).FooterText, Me.dgSalaryIncrement.Columns(8).HeaderText)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnApprove.Text = Language._Object.getCaption(Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnDisapprove.Text = Language._Object.getCaption(Me.btnDisapprove.ID, Me.btnDisapprove.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End


End Class
