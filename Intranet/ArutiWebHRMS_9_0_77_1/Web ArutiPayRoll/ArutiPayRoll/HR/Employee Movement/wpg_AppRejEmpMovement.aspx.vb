﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class HR_Employee_Movement_wpg_AppRejEmpMovement
    Inherits Basepage


#Region "Private Variable"
    'Private objApprovalTran As New clsEmployeeMovmentApproval
    Private ReadOnly mstrModuleName As String = "frmAppRejEmpMovement"
    Dim DisplayMessage As New CommonCodes
    Private mdvData As DataView
    Private mstrDefaultSearchParameter As String = ""
    Private eFillType As clsEmployeeMovmentApproval.enMovementType
    Private eDtType As enEmp_Dates_Transaction
    Private intPrivilegeId As Integer = 0
    Private blnIsResidentPermit As Boolean = False
    Private mdtmovement As DataTable
    Private mdtData As DataTable
    Private mblnConfirm As Boolean = False

#End Region

#Region "Private Funcation"
    Private Sub FillCombo()

        Dim dsComboList As New DataSet
        Dim objEmployee As New clsEmployee_Master


        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objApprovalTran As New clsEmployeeMovmentApproval
        'Gajanan [4-Sep-2020] -- End

        Try
            dsComboList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True, , , , , , , , , , , , , , , , True) '
            With drpEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsComboList.Tables("EmployeeList")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            dsComboList = Nothing

            dsComboList = objApprovalTran.getMovementList(CInt(Session("UserId")), "List", True)
            With drpMovement
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsComboList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            mdtmovement = CType(drpMovement.DataSource, DataTable)

            'S.SANDEEP |17-JAN-2019| -- START
            dsComboList = objApprovalTran.getOperationTypeList("List", True)
            With cboOperationType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsComboList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            'S.SANDEEP |17-JAN-2019| -- END


        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
        Finally
            objEmployee = Nothing : dsComboList.Dispose()
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objApprovalTran = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Private Sub SetApproverInfo()
        Dim objUMapping As New clsemp_appUsermapping
        Dim dsInfo As New DataSet
        Try
            dsInfo = objUMapping.GetList(enEmpApproverType.APPR_MOVEMENT, "List", True, " AND hremp_appusermapping.isactive = 1 ", CInt(Session("UserId")), Nothing)
            If dsInfo.Tables("List").Rows.Count > 0 Then
                txtApprover.Text = dsInfo.Tables("List").Rows(0)("User").ToString()
                hfApprover.Value = dsInfo.Tables("List").Rows(0)("mappingunkid").ToString()

                txtLevel.Text = dsInfo.Tables("List").Rows(0)("Level").ToString()
                hfLevel.Value = CStr(dsInfo.Tables("List").Rows(0)("priority"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo:- " & ex.Message, Me)
        Finally
        End Try

    End Sub

    Private Sub FillGrid()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objApprovalTran As New clsEmployeeMovmentApproval
        'Gajanan [4-Sep-2020] -- End

        Try   'Hemant (13 Aug 2020)
        If txtApprover.Text.Trim.Length > 0 Then
            If CInt(drpMovement.SelectedValue) <= 0 Then Exit Sub
            Dim dt As DataTable = objApprovalTran.GetEmployeeApprovalData(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), intPrivilegeId, CInt(Session("UserId")), CInt(hfLevel.Value), False, eFillType, Session("EmployeeAsOnDate").ToString, eDtType, blnIsResidentPermit, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), Nothing)

            gvApproveRejectMovement.AutoGenerateColumns = False
            mstrDefaultSearchParameter = ""
            If CInt(Session("Employeeunkid")) > 0 Then
                mstrDefaultSearchParameter = "employeeunkid <> " & CInt(Session("Employeeunkid"))
            End If
            mdvData = dt.DefaultView
            mdvData.RowFilter = mstrDefaultSearchParameter
            gvApproveRejectMovement.DataSource = mdvData.ToTable
            mdtData = mdvData.ToTable

            SetGridColums()

            gvApproveRejectMovement.DataBind()

        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objApprovalTran = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Private Sub SetGridColums()
        Try   'Hemant (13 Aug 2020)
            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate1", False, True)).Visible = False _
                  : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhDispValue", False, True)).Visible = False _
              : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJobGroup", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJob", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhwork_permit_no", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhissue_place", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhIDate", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhExDate", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhCountry", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhbranch", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdeptgroup", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdept", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsecgroup", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsection", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunitgrp", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunit", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhteam", False, True)).Visible = False

            gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclassgrp", False, True)).Visible = False _
            : gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclass", False, True)).Visible = False


            Select Case eFillType
                Case clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate1", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = False

                Case clsEmployeeMovmentApproval.enMovementType.PROBATION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate1", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = False

                Case clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.TERMINATION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhDispValue", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJobGroup", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhJob", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhwork_permit_no", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhissue_place", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhIDate", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhExDate", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhCountry", False, True)).Visible = True

                Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhbranch", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdeptgroup", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdept", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsecgroup", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhsection", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunitgrp", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhunit", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhteam", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclassgrp", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhclass", False, True)).Visible = True
                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    gvApproveRejectMovement.Columns(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhdDate2", False, True)).Visible = True
                    'Sohail (21 Oct 2019) -- End
            End Select
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Private Sub ApproveRejectMovement(ByVal status As Integer, ByVal dtTable As DataTable)
        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objApprovalTran As New clsEmployeeMovmentApproval
        'Gajanan [4-Sep-2020] -- End

        Try
            Dim enLoginMode As New enLogin_Mode
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                enLoginMode = enLogin_Mode.EMP_SELF_SERVICE
            Else
                enLoginMode = enLogin_Mode.MGR_SELF_SERVICE
            End If

            Dim dtAppr As DataTable : Dim drtemp() As DataRow = Nothing : Dim strCheckedEmpIds As String = ""

            dtAppr = objApprovalTran.GetNextEmployeeApprovers(CStr(Session("Database_Name")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")), intPrivilegeId, eFillType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), eDtType, blnIsResidentPermit, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), Nothing, CInt(hfLevel.Value), False)

            If dtAppr.Rows.Count > 0 Then
                strCheckedEmpIds = String.Join(",", dtAppr.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
            Else
                strCheckedEmpIds = ""
            End If

            If strCheckedEmpIds.Trim.Length > 0 Then
                drtemp = dtTable.Select("employeeunkid not in (" & strCheckedEmpIds & ") AND icheck = true ")
            Else
                drtemp = dtTable.Select("icheck = true ")
            End If
            If drtemp.Length > 0 Then
                For index As Integer = 0 To drtemp.Length - 1
                    drtemp(index)("isfinal") = True
                Next
            End If
            dtTable.AcceptChanges()
            dtTable = New DataView(dtTable, "icheck = true", "", DataViewRowState.CurrentRows).ToTable

            Dim mblnIsRejection As Boolean = False
            Dim iStatusunkid As clsEmployee_Master.EmpApprovalStatus
            Select Case (status)
                Case 1
                    iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved
                Case 2
                    iStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected
                    mblnIsRejection = True
            End Select

            strCheckedEmpIds = String.Join(",", dtTable.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
            'S.SANDEEP |17-JAN-2019| -- ADD OPRATION TYPE

            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            'If objApprovalTran.InsertAll(eFillType, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), dtTable, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, False, iStatusunkid, txtRemark.Text, CInt(hfApprover.Value), CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), getHostName(), getIP(), CStr(Session("UserName")), enLoginMode) Then

            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'If objApprovalTran.InsertAll(eFillType, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), dtTable, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, True, iStatusunkid, txtRemark.Text, CInt(hfApprover.Value), CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), getHostName(), getIP(), CStr(Session("UserName")), enLoginMode) Then
            If objApprovalTran.InsertAll(eFillType, CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), dtTable, CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, mstrModuleName, True, iStatusunkid, txtRemark.Text, CInt(hfApprover.Value), CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")), getHostName(), getIP(), CStr(Session("UserName")), enLoginMode) Then
                'Pinkal (12-Oct-2020) -- End

                'Gajanan [9-July-2019] -- End

                If mblnIsRejection = False Then
                    objApprovalTran.SendNotification(2, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eFillType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), blnIsResidentPermit, eDtType, CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text)
                Else
                    objApprovalTran.SendNotification(3, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eFillType, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLoginMode, CStr(Session("UserName")), CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType), blnIsResidentPermit, eDtType, CInt(hfLevel.Value), strCheckedEmpIds, txtRemark.Text, Session("RejectTransferUserNotification").ToString, Session("RejectReCategorizationUserNotification").ToString, Session("RejectProbationUserNotification").ToString, Session("RejectConfirmationUserNotification").ToString, Session("RejectSuspensionUserNotification").ToString, Session("RejectTerminationUserNotification").ToString, Session("RejectRetirementUserNotification").ToString, Session("RejectWorkPermitUserNotification").ToString, Session("RejectResidentPermitUserNotification").ToString, Session("RejectCostCenterPermitUserNotification").ToString, Session("RejectExemptionUserNotification").ToString)
                    'Sohail (21 Oct 2019) - [RejectExemptionUserNotification]
                End If
                'S.SANDEEP |17-JAN-2019| -- START
            Else
                If objApprovalTran._Message.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(objApprovalTran._Message, Me)
                    Exit Sub
                End If
                'S.SANDEEP |17-JAN-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("ApproveRejectMovement :" + ex.Message, Me)
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objApprovalTran = Nothing
            'Gajanan [4-Sep-2020] -- End
            Call FillGrid()
            txtRemark.Text = ""
        End Try
    End Sub

    Private Function EscapeLikeValue(ByVal value As String) As String
        Try   'Hemant (13 Aug 2020)
            Dim sb As New StringBuilder(value.Length)
            For i As Integer = 0 To value.Length - 1
                Dim c As Char = value(i)
                Select Case c
                    Case "]"c, "["c, "%"c, "*"c
                        sb.Append("[").Append(c).Append("]")
                        Exit Select
                    Case "'"c
                        sb.Append("''")
                        Exit Select
                    Case Else
                        sb.Append(c)
                        Exit Select
                End Select
            Next
            Return sb.ToString()
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Function

    Private Function GetFilterString(ByVal strCriteria As String) As String
        Dim strFilter As String = String.Empty
        Try
            If gvApproveRejectMovement.Rows.Count > 0 Then
                If CInt(drpMovement.SelectedValue) > 0 Then
                    Dim dataView As DataView = mdtmovement.DefaultView
                    dataView.RowFilter = "Id = '" & drpMovement.SelectedValue & "'"


                    Dim eFillType As clsEmployeeMovmentApproval.enMovementType
                    eFillType = CType(dataView.ToTable().Rows(0)("Id"), clsEmployeeMovmentApproval.enMovementType)


                    strFilter = "ecode" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                "ename" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                "EffDate" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                "CReason" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                    Select Case eFillType
                        Case clsEmployeeMovmentApproval.enMovementType.CONFIRMATION, clsEmployeeMovmentApproval.enMovementType.PROBATION, clsEmployeeMovmentApproval.enMovementType.RETIREMENTS, clsEmployeeMovmentApproval.enMovementType.SUSPENSION, clsEmployeeMovmentApproval.enMovementType.TERMINATION, clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) - [EXEMPTION]
                            strFilter &= "dDate1" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "dDate2" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.COSTCENTER

                            strFilter &= "DispValue" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE

                            strFilter &= "JobGroup" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "Job" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.RESIDENTPERMIT, clsEmployeeMovmentApproval.enMovementType.WORKPERMIT
                            strFilter &= "work_permit_no" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "issue_place" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "IDate" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "EDate" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "Country" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "

                        Case clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                            strFilter &= "branch" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "deptgroup" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "dept" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "secgroup" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "section" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "unitgrp" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "Unit" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "team" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "classgrp" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR " & _
                                         "class" & " LIKE '%" & EscapeLikeValue(strCriteria) & "%' OR "
                    End Select
                End If
            End If
            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(0, strFilter.Length - 4)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "GetFilterString", mstrModuleName)
            DisplayMessage.DisplayError("GetFilterString :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
        Return strFilter
    End Function
#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                GC.Collect()
                'Gajanan [4-Sep-2020] -- End
                Call SetApproverInfo()
                Call FillCombo()
            Else
                mdtmovement = CType(Me.ViewState("mdtmovement"), DataTable)
                mdtData = CType(Me.ViewState("mdtData"), DataTable)

                intPrivilegeId = CInt(Me.ViewState("PrivilegeId"))
                eFillType = CType(Me.ViewState("Id"), clsEmployeeMovmentApproval.enMovementType)
                eDtType = CType(Me.ViewState("DatesId"), enEmp_Dates_Transaction)
                blnIsResidentPermit = CBool(Me.ViewState("IsResident"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mdtmovement", mdtmovement)
            Me.ViewState.Add("mdtData", mdtData)
            Me.ViewState.Add("PrivilegeId", intPrivilegeId)
            Me.ViewState.Add("Id", eFillType)
            Me.ViewState.Add("DatesId", eDtType)
            Me.ViewState.Add("IsResident", blnIsResidentPermit)

        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender:- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(drpMovement.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Movement Type is mandatory information. Please select movement type to continue"), Me)
                Exit Sub
            End If
            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            If CInt(cboOperationType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Opration Type is mandatory information. Please select Opration type to continue"), Me)
                Exit Sub
            End If
            'Gajanan [9-July-2019] -- End 
            Call FillGrid()

        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click " + ex.Message, Me)
        End Try

    End Sub

    Protected Sub drpMovement_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpMovement.SelectedIndexChanged
        Try
            If CInt(drpMovement.SelectedValue) > 0 Then
                Dim dataView As DataView = mdtmovement.DefaultView
                dataView.RowFilter = "Id = '" & drpMovement.SelectedValue & "'"

                intPrivilegeId = CInt(dataView.ToTable().Rows(0)("PrivilegeId"))
                eFillType = CType(dataView.ToTable().Rows(0)("Id"), clsEmployeeMovmentApproval.enMovementType)
                eDtType = CType(dataView.ToTable().Rows(0)("DatesId"), enEmp_Dates_Transaction)
                blnIsResidentPermit = CBool(dataView.ToTable().Rows(0)("IsResident"))
                btnShowMyReport.Enabled = True
            Else
                intPrivilegeId = 0
                btnShowMyReport.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("drpMovement_SelectedIndexChanged" + ex.Message, Me)
        End Try

    End Sub


    Protected Sub gvApproveRejectMovement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproveRejectMovement.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then

            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim chkSelect As CheckBox = CType(e.Row.FindControl("ChkSelect"), CheckBox)
                chkSelect.Checked = CBool(gvApproveRejectMovement.DataKeys(e.Row.RowIndex).Values("icheck"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("gvApproveRejectMovement_RowDataBound" + ex.Message, Me)
        End Try

    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click
        Try
            If mdtData IsNot Nothing Then
                Dim dtTable As DataTable = mdtData

                If dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("icheck") = True).Count() <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Please check atleast one employee in order to perform further operation."), Me)
                    Exit Sub
                End If

                Select Case CType(sender, Button).ID
                    Case btnApprove.ID
                        If txtRemark.Text.Trim.Length <= 0 Then
                            mblnConfirm = True
                            Cnf_WithoutRemark.Message = "Are you sure you want to approve employee(s) without remark?"
                            Cnf_WithoutRemark.Show()
                            gvApproveRejectMovement.DataSource = mdtData
                            gvApproveRejectMovement.DataBind()
                        End If
                        Call ApproveRejectMovement(1, dtTable)
                    Case btnReject.ID
                        If txtRemark.Text.Trim.Length <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Rejection remark is mandatory information. Please enter enter remark to continue."), Me)
                            txtRemark.Focus()
                            Exit Sub
                        End If

                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        If CInt(cboOperationType.SelectedValue) = clsEmployeeMovmentApproval.enOperationType.ADDED Then
                            If dtTable.Select("icheck = 1 AND leaveissueunkid > 0 ").Length > 0 Then
                                Cnf_LeaveIssued.Message = Language.getMessage(mstrModuleName, 5, "Leave is arleady Issued, Are you sure you want to reject Employee exemotion for this") & " " & dtTable.Select("icheck = 1 AND leaveissueunkid > 0 ")(0).Item("CReason").ToString & "?" & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 6, "If YES employee will be considered in Payroll pocess for those unpaid leave days.")
                                Cnf_LeaveIssued.Show()
                                Exit Sub
                            End If
                        End If
                        'Sohail (21 Oct 2019) -- End
                        Call ApproveRejectMovement(2, dtTable)
                End Select
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "btnApprove_Click", mstrModuleName)
            DisplayMessage.DisplayError("btnApprove_Click :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

        End Try
    End Sub


    Protected Sub ChkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                Dim dRow() As DataRow = mdtData.Select("ecode = '" & gvr.Cells(GetColumnIndex.getColumnID_Griview(gvApproveRejectMovement, "dgcolhecode", False, True)).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("icheck") = cb.Checked
                End If
                CType(mdtData, DataTable).AcceptChanges()

                Dim drRow() As DataRow = mdtData.Select("icheck = 1")

                If mdtData.Rows.Count = drRow.Length Then
                    CType(gvApproveRejectMovement.HeaderRow.FindControl("ChkAll"), CheckBox).Checked = True
                Else
                    CType(gvApproveRejectMovement.HeaderRow.FindControl("ChkAll"), CheckBox).Checked = False
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("ChkSelect_CheckedChanged" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub ChkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim chkstatus As Boolean = cb.Checked
            For i As Integer = 0 To mdtData.Rows.Count - 1
                mdtData.Rows(i)("icheck") = cb.Checked
                CType(gvApproveRejectMovement.Rows(i).FindControl("ChkSelect"), CheckBox).Checked = cb.Checked
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError("ChkAll_CheckedChanged :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Cnf_WithoutRemark_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cnf_WithoutRemark.buttonYes_Click
        Try
            'gvApproveRejectMovement.DataSource = mdtData
            'gvApproveRejectMovement.DataBind()
            Call ApproveRejectMovement(1, mdtData)
        Catch ex As Exception
            DisplayMessage.DisplayError("Cnf_WithoutRemark_buttonYes_Click" + ex.Message, Me)
        End Try

    End Sub

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Protected Sub Cnf_LeaveIssued_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cnf_LeaveIssued.buttonYes_Click
        Try
            Call ApproveRejectMovement(2, mdtData)
        Catch ex As Exception
            DisplayMessage.DisplayError("Cnf_LeaveIssued_buttonYes_Click" + ex.Message, Me)
        End Try
    End Sub
    'Sohail (21 Oct 2019) -- End

    Protected Sub btnShowMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowMyReport.Click
        Try
            'frm.displayDialog(User._Object._Userunkid, CInt(txtLevel.Tag), intPrivilegeId, eFillType, eDtType, blnIsResidentPermit) 'S.SANDEEP [09-AUG-2018] -- START -- END
            Pop_report._UserId = CInt(Session("UserId"))
            Pop_report._Priority = CInt(hfLevel.Value)
            Pop_report._PrivilegeId = CInt(intPrivilegeId)
            Pop_report._FillType = eFillType
            Pop_report._DtType = eDtType
            Pop_report._IsResidentPermit = blnIsResidentPermit
 'S.SANDEEP |17-JAN-2019| -- START            
Pop_report._OprationType = CType(CInt(cboOperationType.SelectedValue), clsEmployeeMovmentApproval.enOperationType)
 'S.SANDEEP |17-JAN-2019| -- END
            Pop_report.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError("btnShowMyReport_Click" + ex.Message, Me)
        End Try
    End Sub

    Protected Sub txtsearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtsearch.TextChanged
        Dim str As String = ""
        Try
            If mdtData Is Nothing Then Exit Sub

            Dim mdvData As DataView = mdtData.DefaultView

            If (txtsearch.Text.Length > 0) Then
                str = GetFilterString(txtsearch.Text)
            End If

            If str.Trim.Length > 0 Then

                If mstrDefaultSearchParameter.Trim.Length > 0 Then
                    mdvData.RowFilter = mstrDefaultSearchParameter & " AND (" & str & ") "
                Else
                    mdvData.RowFilter = str
                End If
            Else
                mdvData.RowFilter = mstrDefaultSearchParameter
            End If
            gvApproveRejectMovement.DataSource = mdvData
            SetGridColums()
            gvApproveRejectMovement.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError("SetLanguage :- " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpMovement.SelectedIndex = 0
            drpEmployee.SelectedIndex = 0


        Catch ex As Exception
            DisplayMessage.DisplayError("btnReset_Click" + ex.Message, Me)
        End Try

    End Sub
End Class
