﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EmpMovementDates.aspx.vb" Inherits="HR_wPg_EmpMovementDates" Title="Employee Dates" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <%--    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.min.js"></script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            removeclasscss();
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
        function removeclasscss() {
            $('.click-nav > ul').toggleClass('no-js js');
            $('.click-nav .js ul').hide();
            $('.click-nav .js').click(function(e) {
                $('.click-nav .js ul').slideToggle(200);
                $('.clicker').addClass('active');
                e.stopPropagation();
            });
            if ($('.click-nav .js ul').is(':visible')) {
                alert('hii');
                $('.click-nav .js ul', this).slideUp();
                $('.clicker').removeClass('active');
            }
        }
                
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dates"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Dates"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
          Style="color: Blue; vertical-align: top"></asp:LinkButton>
</div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpEffectiveDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 60%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="objlblStartFrom" runat="server" Text="Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 70%" colspan="2">
                                                <asp:CheckBox ID="chkExclude" runat="server" Text="Exclude From Payroll Process For EOC Date/Leaving Date">
                                                </asp:CheckBox>
                                                <asp:LinkButton ID="lnkSetSuspension" runat="server" Text="Set Suspension For Descipline"></asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="objlblEndTo" runat="server" Text="End Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmplReason" runat="server" Text="EOC Reason" Visible="false"></asp:Label>
                                                <asp:Label ID="lblPaymentDate" runat="server" Text="Payment Date"></asp:Label>
                                            </td>
                                            <td style="width: 60%">
                                                <asp:DropDownList ID="cboEndEmplReason" Enabled="false" runat="server" Visible="false">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtPaymentDate" runat="server" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                         <tr style="width: 100%">
                                          <td style="width: 15%">
                                                <asp:Label ID="LblActualDate" runat="server" Text="Actual Date" Visible="false"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpActualDate" runat="server" AutoPostBack="false" Visible="false" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblReason" runat="server" Text="Change Reason"></asp:Label>
                                            </td>
                                            <td style="width: 35%;">
                                                <asp:DropDownList ID="cboChangeReason" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 50%" align="right">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblAppointmentdate" runat="server" Style="margin-left: 10px" Visible="False"
                                                                Text="Appoint Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 20%" align="right">
                                                            <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save Changes" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 350px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvHistory" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <ItemStyle CssClass="griviewitem" />
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                                        <i class="fa fa-exclamation-circle" style="font-size: 18px;"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn> 
                                                            <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                HeaderText="" ItemStyle-Width="30px">
                                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" CssClass="gridedit"
                                                                            ToolTip="Edit"></asp:LinkButton>
                                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                                            Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                HeaderText="" ItemStyle-Width="30px">
                                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="EffDate" FooterText="dgcolhChangeDate" HeaderText="Effective Date"
                                                                ReadOnly="true"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="dDate1" FooterText="objdgcolhStartDate" HeaderText=""
                                                                ReadOnly="true"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="dDate2" FooterText="objdgcolhEndDate" HeaderText="" ReadOnly="true">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CReason" FooterText="dgcolhReason" HeaderText="Reason"
                                                                ReadOnly="true"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="datestranunkid" FooterText="objdgcolhdatetranunkid" HeaderText="Reason"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="isfromemployee" FooterText="objdgcolhFromEmp" HeaderText="Reason"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="actionreasonunkid" FooterText="objdgcolhActionReasonId"
                                                                HeaderText="Reason" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="isexclude_payroll" FooterText="objdgcolhExclude" HeaderText="Reason"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="adate" FooterText="objdgcolhAppointdate" HeaderText="Reason"
                                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                                            <%--15--%>
                                                        </Columns>
                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Panel ID="pnl_OprationMenu" runat="server" Style="text-align: left; width: 60%;">
                                        <div class="click-nav" style="float: left">
                                            <%--<ul class="no-js">
                                                    <li><a href="#" class="clicker" style="position: fixed; float: left">
                                                        <div style="float: left; padding-left: 7px; margin-right: 5px" >
                                                            <asp:Label ID="btnOperations" runat="server" Text="Opration"></asp:Label>
                                                        </div>
                                                        <div style="float: right; font-size: 21px; margin-right: 9px">
                                                            <span class="fa fa-caret-down"></span>
                                                        </div>
                                                    </a>
                                                        <ul>
                                                            <li>
                                                                <asp:LinkButton ID="mnuSalaryChange" runat="server" Text="Salary Change" Visible="false" CssClass = "aa"></asp:LinkButton>
                                                            </li>
                                                            <li>
                                                                <asp:LinkButton ID="mnuBenefits" runat="server" Text="Benefits" Visible="false" ></asp:LinkButton>
                                                            </li>
                                                            <li>
                                                                <asp:LinkButton ID="mnuTransfers" runat="server" Text="Transfers" CssClass = "aa"></asp:LinkButton>
                                                            </li>
                                                            <li>
                                                                <asp:LinkButton ID="mnuRecategorization" runat="server" Text="Re-Categorize" ></asp:LinkButton>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>--%>
                                            <uc5:OperationButton ID="btnOperations" runat="server" Text="Operation" TragateControlId="data" />
                                            <div id="data" style="display: none;">
                                                    <asp:LinkButton ID="mnuSalaryChange" runat="server" Text="Salary Change" Visible="false"></asp:LinkButton>
                                                    <asp:LinkButton ID="mnuBenefits" runat="server" Text="Benefits" Visible="false"></asp:LinkButton>
                                                    <asp:LinkButton ID="mnuTransfers" runat="server" Text="Transfers"></asp:LinkButton>
                                                    <asp:LinkButton ID="mnuRecategorization" runat="server" Text="Re-Categorize"></asp:LinkButton>
                                            </div>
                                            </div>
                                        </asp:Panel>
                                        <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" BackColor="Orange"
                                            Style="padding: 2px; font-weight: bold"></asp:Label>
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                            Style="padding: 2px; font-weight: bold"></asp:Label>
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                    <uc3:Pop_report ID="Popup_report" runat="server" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc7:Confirmation ID="Confirmation" runat="server" Title="Confirmation" Message="Exclude from payroll process is not set. Are you sure you do not want to Exclude Payroll Process for this Employee?" />

                    <script>
                        $(function() {
                            // Clickable Dropdown
                            $('.click-nav > ul').toggleClass('no-js js');
                            $('.click-nav .js ul').hide();
                            $('.click-nav .js').live("click", function(e) {
                                if ($('.click-nav .js ul').not(':visible')) {
                                    $('.click-nav .js ul').slideToggle(200);
                                    $('.clicker').toggleClass('active');
                                    e.stopPropagation();
                                }
                            });
                            $(document).click(function() {
                                if ($('.click-nav .js ul').is(':visible')) {
                                    $('.click-nav .js ul', this).slideUp();
                                    $('.clicker').removeClass('active');
                                }
                            });
                        });
                    </script>

                    <cc1:ModalPopupExtender ID="popupViewDiscipline" BackgroundCssClass="modalBackground"
                        TargetControlID="lblPersonalInvolved" runat="server" PopupControlID="pnlGreApprover"
                        DropShadow="false" CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlGreApprover" runat="server" CssClass="newpopup" Style="display: none;
                        width: 850px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblCancelText1" Text="View Discipline Charges" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <div class="row2">
                                        <div style="width: 48%;" class="ibwm">
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblPersonalInvolved" runat="server" Text="Person Involved" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <asp:DropDownList ID="cboDisciplineEmployee" runat="server" Width="220px" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="row2">
                                                    <div style="width: 30%;" class="ib">
                                                        <asp:Label ID="lblReferenceNo" runat="server" Text="Referance No" Width="100%"></asp:Label>
                                                    </div>
                                                    <div style="width: 60%;" class="ib">
                                                        <asp:DropDownList ID="cboReferenceNo" runat="server" Width="220px" AutoPostBack=true>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="row2">
                                                    <div style="width: 30%;" class="ib">
                                                        <asp:Label ID="lblJobTitle" runat="server" Text="Job Title" Width="100%"></asp:Label>
                                                    </div>
                                                    <div style="width: 60%;" class="ib">
                                                        <asp:TextBox ID="txtJobTitle" runat="server" ReadOnly=true>
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="row2">
                                                    <div style="width: 30%;" class="ib">
                                                        <asp:Label ID="lblDepartment" runat="server" Text="Department" Width="100%"></asp:Label>
                                                    </div>
                                                    <div style="width: 60%;" class="ib">
                                                        <asp:TextBox ID="txtDepartment" runat="server" ReadOnly=true>
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="width: 48%;vertical-align: top" class="ibwm">
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblDate" runat="server" Text="Charge Date" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <asp:TextBox ID="txtchargeDate" runat="server" ReadOnly=true>
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblInterdictionDate" runat="server" Text="Interdiction Date" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <asp:TextBox ID="txtinterdictionDate" runat="server" ReadOnly=true>
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblGeneralChargeDescr" runat="server" Text="General Charge Description" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <asp:TextBox ID="txtChargeDescription" runat="server" TextMode="MultiLine" Rows=4 ReadOnly=true> 
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div id="Div1" class="gridscroll" style="vertical-align: top; overflow: auto; max-height: 200px">
                                            <asp:GridView ID="dgvData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Count" FooterText="dgcolhCount">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcount" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="charge_category" FooterText="dgcolhOffCategory" HeaderText="Off. Category"
                                                        ReadOnly="true"></asp:BoundField>
                                                    <asp:BoundField DataField="charge_descr" FooterText="dgcolhOffence" HeaderText="Offence"
                                                        ReadOnly="true"></asp:BoundField>
                                                    <asp:BoundField DataField="incident_description" FooterText="dgcolhIncident" HeaderText="Incident"
                                                        ReadOnly="true"></asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnOk" runat="server" CssClass="btndefault" Text="Ok" />
                                        <asp:Button ID="btnClosePopup" runat="server" CssClass="btndefault" Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
