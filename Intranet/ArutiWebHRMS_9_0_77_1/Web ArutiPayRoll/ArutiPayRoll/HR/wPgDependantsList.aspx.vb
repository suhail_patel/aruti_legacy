﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing

#End Region

Partial Class wPgDependantsList
    Inherits Basepage

#Region " Private Variable(s) "

    Private objDependant As New clsDependants_Beneficiary_tran
    Dim msg As New CommonCodes


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmDependantsAndBeneficiariesList"
    'Pinkal (06-May-2014) -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objADependantTran As New clsDependant_beneficiaries_approval_tran

    Dim Arr() As String
    Dim DependantApprovalFlowVal As String

    Dim blnPendingEmployee As Boolean = False
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As New clsEmployeeDataApproval
    Private mblnisEmployeeApprove As Boolean = False
    'Gajanan [17-April-2019] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, )
                'End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmp.GetEmployeeList("Emp", True, )
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                'dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                '                            CInt(Session("UserId")), _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            CStr(Session("UserAccessModeSetting")), True, _
                '                            CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), _
                                          mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)
                'S.SANDEEP [20-JUN-2018] -- End

                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Emp")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            With cboRelation
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relation")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objMaster.getGenderList("List", True)
            With cboGender
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError("Procedure FillCombo : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            cboGender.SelectedValue = CStr(0)
            cboRelation.SelectedValue = CStr(0)
            txtFirstName.Text = ""
            txtIdNo.Text = ""
            txtLastName.Text = ""
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            cboEmployee.SelectedIndex = 0
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            msg.DisplayError("Procedure ClearObject : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim dTable As DataTable
        Dim StrSearching As String = ""

        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim StrApproverSearching As String = String.Empty
        'Gajanan [22-Feb-2019] -- End
        Try

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewDependant")) = False Then Exit Sub
            End If

            'Pinkal (22-Nov-2012) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''S.SANDEEP [ 27 APRIL 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''dsList = objDependant.GetList("List")
            'dsList = objDependant.GetList("List", Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
            ''S.SANDEEP [ 27 APRIL 2012 ] -- END

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
            'End If

            'If CInt(cboRelation.SelectedValue) > 0 Then
            '    StrSearching &= "AND RelationId = " & CInt(cboRelation.SelectedValue) & " "
            'End If

            'If txtFirstName.Text.Trim <> "" Then
            '    StrSearching &= "AND FName LIKE '%" & txtFirstName.Text & "%'" & " "
            'End If

            'If txtLastName.Text.Trim <> "" Then
            '    StrSearching &= "AND LName LIKE '%" & txtLastName.Text & "%'" & " "
            'End If

            'If txtIdNo.Text.Trim <> "" Then
            '    StrSearching &= "AND IdNo LIKE '%" & txtIdNo.Text & "%'" & " "
            'End If

            'If CInt(cboGender.SelectedValue) > 0 Then
            '    StrSearching &= "AND GenderId = '" & CInt(cboGender.SelectedValue) & "' "
            'End If

            'If dtpBirthdate.IsNull = False Then
            '    StrSearching &= "AND birthdate = '" & eZeeDate.convertDate(dtpBirthdate.GetDate) & "' "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dTable = New DataView(dsList.Tables(0), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            'End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboRelation.SelectedValue) > 0 Then
                StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboRelation.SelectedValue) & " "
            End If

            If txtFirstName.Text.Trim <> "" Then
                StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.first_name,'') LIKE '%" & txtFirstName.Text & "%'" & " "
            End If

            If txtLastName.Text.Trim <> "" Then
                StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.last_name,'') LIKE '%" & txtLastName.Text & "%'" & " "
            End If

            If txtIdNo.Text.Trim <> "" Then
                StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.identify_no,'') LIKE '%" & txtIdNo.Text & "%'" & " "
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                StrSearching &= "AND hrdependants_beneficiaries_tran.gender = '" & CInt(cboGender.SelectedValue) & "' "
            End If

            If dtpBirthdate.IsNull = False Then
                StrSearching &= "AND ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') = '" & eZeeDate.convertDate(dtpBirthdate.GetDate) & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If



            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If DependantApprovalFlowVal Is Nothing Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrApproverSearching &= "AND hrdependant_beneficiaries_approval_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboRelation.SelectedValue) > 0 Then
                    StrApproverSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboRelation.SelectedValue) & " "
                End If

                If txtFirstName.Text.Trim <> "" Then
                    StrApproverSearching &= "AND ISNULL(hrdependant_beneficiaries_approval_tran.first_name,'') LIKE '%" & txtFirstName.Text & "%'" & " "
                End If

                If txtLastName.Text.Trim <> "" Then
                    StrApproverSearching &= "AND ISNULL(hrdependant_beneficiaries_approval_tran.last_name,'') LIKE '%" & txtLastName.Text & "%'" & " "
                End If

                If txtIdNo.Text.Trim <> "" Then
                    StrApproverSearching &= "AND ISNULL(hrdependant_beneficiaries_approval_tran.identify_no,'') LIKE '%" & txtIdNo.Text & "%'" & " "
                End If

                If CInt(cboGender.SelectedValue) > 0 Then
                    StrApproverSearching &= "AND hrdependant_beneficiaries_approval_tran.gender = '" & CInt(cboGender.SelectedValue) & "' "
                End If

                If dtpBirthdate.IsNull = False Then
                    StrApproverSearching &= "AND ISNULL(CONVERT(CHAR(8),hrdependant_beneficiaries_approval_tran.birthdate,112),'') = '" & eZeeDate.convertDate(dtpBirthdate.GetDate()) & "' "
                End If

                If StrSearching.Length > 0 Then
                    StrApproverSearching = StrApproverSearching.Substring(3)
                End If
            End If
            'Gajanan [22-Feb-2019] -- End

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            'dsList = objDependant.GetList(Session("Database_Name"), _
            '                                        Session("UserId"), _
            '                                        Session("Fin_year"), _
            '                                        Session("CompanyUnkId"), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                        Session("UserAccessModeSetting"), True, _
            '                                        Session("IsIncludeInactiveEmp"), "List", , , StrSearching)

            
           
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If




            'dsList = objDependant.GetList(CStr(Session("Database_Name")), _
            '                              CInt(Session("UserId")), _
            '                              CInt(Session("Fin_year")), _
            '                              CInt(Session("CompanyUnkId")), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                              CStr(Session("UserAccessModeSetting")), True, _
            '                              CBool(Session("IsIncludeInactiveEmp")), "List", , , StrSearching, _
            '                              CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

            dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                        CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "List", , CInt(cboEmployee.SelectedValue), StrSearching, _
                                        CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)), mblnAddApprovalCondition, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
            'Sohail (18 May 2019) - [CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate"))]

            'Pinkal (28-Dec-2015) -- End
            'Gajanan [22-Feb-2019] -- End

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dcol)

            If DependantApprovalFlowVal Is Nothing AndAlso mintTransactionId <= 0 Then
                Dim dsPending As New DataSet
                dsPending = objADependantTran.GetList(FinancialYear._Object._DatabaseName, _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", , , StrApproverSearching, , mblnAddApprovalCondition)

                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsList.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'Gajanan [22-Feb-2019] -- End

            dTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'Shani(24-Aug-2015) -- Start

            If dTable IsNot Nothing Then
                dgvDependantList.DataSource = dTable
                dgvDependantList.DataKeyField = "DpndtTranId"
                dgvDependantList.DataBind()
            End If

        Catch ex As Exception
            msg.DisplayError("Procedure FillGrid : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub delete()
        Try
            popup_DeleteReason.Show() ' Shani [01 FEB 2015] ---popup1.Show()
        Catch ex As Exception
            msg.DisplayError("delete : " & ex.Message, Me)
        End Try
    End Sub

    'Private Sub SetPrevious_State()
    '    Try
    '        Session("Prev_State") = cboEmployee.SelectedValue & "|" & _
    '                                cboGender.SelectedValue & "|" & _
    '                                cboRelation.SelectedValue & "|" & _
    '                                txtFirstName.Text & "|" & _
    '                                txtIdNo.Text & "|" & _
    '                                txtLastName.Text & "|" & _
    '                                dgvDependantList.CurrentPageIndex
    '    Catch ex As Exception
    '        msg.DisplayError("Procedure SetPrevious_State : " & ex.Message, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GetPrevious_State()
    '    Try
    '        Dim StrState() As String = Nothing
    '        StrState = CType(Session("Prev_State"), String).Split("|")

    '        If StrState.Length > 0 Then
    '            cboEmployee.SelectedValue = StrState(0)
    '            cboGender.SelectedValue = StrState(1)
    '            cboRelation.SelectedValue = StrState(2)
    '            txtFirstName.Text = StrState(3)
    '            txtIdNo.Text = StrState(4)
    '            txtLastName.Text = StrState(5)
    '            dgvDependantList.CurrentPageIndex = StrState(6)
    '        End If

    '        Session("IsFrom_AddEdit") = False

    '        Call FillGrid()

    '    Catch ex As Exception
    '        msg.DisplayError("Procedure GetPrevious_State : " & ex.Message, Me)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If


            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmDependantsAndBeneficiariesList"
            'StrModuleName2 = "mnuCoreSetups"
            'clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            'clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End
            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            DependantApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))


            Dim clsuser As New User
            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END



                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = True
                    'BtnNew.Visible = False
                    BtnNew.Visible = CBool(Session("AddDependant"))
                    'Anjan (02 Mar 2012)-End 


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    'If Session("ShowPending") IsNot Nothing Then
                    '    chkShowPending.Checked = True
                    'End If
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END
                    'S.SANDEEP [20-JUN-2018] -- End

                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BntNew.Visible = False


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnNew.Visible = ConfigParameter._Object._AllowAddDependants
                    BtnNew.Visible = CBool(Session("AllowAddDependants"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .
                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = False
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END
                    'S.SANDEEP [20-JUN-2018] -- End

                    'Anjan (02 Mar 2012)-End 


                End If
                Call FillCombo()

                If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 Then
                    Call FillGrid()
                End If


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'SHANI [01 FEB 2015]--END 
                'Pinkal (22-Mar-2012) -- End


                'If Session("IsFrom_AddEdit") = True Then
                ' Call GetPrevious_State()
                'Session("IsFrom_AddEdit") = False
                'End If



                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End


                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("dependantsEmpunkid") IsNot Nothing Then
                    If CInt(Session("dependantsEmpunkid")) > 0 Then
                        cboEmployee.SelectedValue = CStr(CInt(Session("dependantsEmpunkid")))
                        Call btnSearch_Click(BtnSearch, Nothing)
                        Session.Remove("dependantsEmpunkid")
                    End If
                End If
                'Nilay (02-Mar-2015) -- End



                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Else
                blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))
                'Gajanan [22-Feb-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                    mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                End If
                'Gajanan [17-April-2019] -- End
            End If


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            objtblPanel.Visible = False

            If blnPendingEmployee Then
                objtblPanel.Visible = blnPendingEmployee
            End If


            'Gajanan [22-Feb-2019] -- End

        Catch ex As Exception
            msg.DisplayError("Procedure Page_Load : " & ex.Message, Me)
        Finally
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END 
        End Try
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END 



        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.



        Try
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState.Add("mblnisEmployeeApprove", mblnisEmployeeApprove)
            'Gajanan [17-April-2019] -- End
        Catch ex As Exception
            msg.DisplayError("Page_PreRender" + ex.Message, Me)
        End Try
        'Gajanan [22-Feb-2019] -- End
    End Sub

    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            msg.DisplayError("BtnClose_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError("btnSearch_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Session("dependantsEmpunkid") = cboEmployee.SelectedValue
            End If
            'Nilay (02-Mar-2015) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            'Response.Redirect("~\HR\wPgEmpDepentants.aspx")
            Response.Redirect("~\HR\wPgEmpDepentants.aspx", False)
            'Sohail (18 May 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnNew_Click : " & ex.Message, Me)
            msg.DisplayError("btnNew_Click : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Call ClearObject()
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'Call FillGrid()
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                Call FillGrid()
            Else
                Dim dTable As New DataTable
                dgvDependantList.DataSource = dTable
                dgvDependantList.DataKeyField = "DpndtTranId"
                dgvDependantList.DataBind()
            End If
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError("btnReset_Click Event : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'SHANI [01 FEB 2015]--If (txtreason.Text = "") 
                msg.DisplayMessage(" Please enter delete reason.", Me)
                Exit Sub
            End If
            Dim objDependants_Benefice As New clsDependants_Beneficiary_tran


            objDependants_Benefice._Dpndtbeneficetranunkid = CInt(Me.ViewState("Unkid"))

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            objDependants_Benefice._CompanyId = CInt(Session("CompanyUnkId"))
            objDependants_Benefice._blnImgInDb = CBool(Session("IsImgInDataBase"))

            'Pinkal (01-Apr-2013) -- End


            objDependants_Benefice._Isvoid = True
            objDependants_Benefice._Voidreason = popup_DeleteReason.Reason.Trim 'SHANI [01 FEB 2015]--txtreason.Text 
            objDependants_Benefice._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDependants_Benefice._Voiduserunkid = Aruti.Data.User._Object._Userunkid
            objDependants_Benefice._Voiduserunkid = CInt(Session("UserId"))
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objDependants_Benefice._Employeeunkid = CInt(Me.ViewState("EmployeeId"))
            'Blank_ModuleName()
            'objDependants_Benefice._WebFormName = "frmDependantsAndBeneficiariesList"
            'StrModuleName2 = "mnuCoreSetups"
            'objDependants_Benefice._WebClientIP = CStr(Session("IP_ADD"))
            'objDependants_Benefice._WebHostName = CStr(Session("HOST_NAME"))
            'Pinkal (01-Apr-2013) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            Dim objDPStatus As New clsDependant_Benefice_Status_tran
            Dim dsList As DataSet = objDPStatus.GetList("List", CInt(Me.ViewState("Unkid")))
            Dim strDpStatusUnkIDs As String = String.Join(",", (From p In dsList.Tables(0) Select (p.Item("dpndtbeneficestatustranunkid").ToString)).ToArray)
            objDependants_Benefice._IsFromStatus = False
            objDependants_Benefice._DeletedpndtbeneficestatustranIDs = strDpStatusUnkIDs
            'Sohail (18 May 2019) -- End


            'Gajanan [19-June-2019] -- Start      


            ''SHANI (20 JUN 2015) -- Start
            ''Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            ''objDependants_Benefice.Delete(Me.ViewState("Unkid"))
            'objDependants_Benefice.Delete(CInt(Me.ViewState("Unkid")), CInt(Me.ViewState("EmployeeId")))
            ''SHANI (20 JUN 2015) -- End 


            If DependantApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then

                Dim eLMode As Integer
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    eLMode = enLogin_Mode.MGR_SELF_SERVICE
                Else
                    eLMode = enLogin_Mode.EMP_SELF_SERVICE
                End If

                objADependantTran._Isvoid = True
                objADependantTran._Audituserunkid = CInt(Session("UserId"))
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'objADependantTran._Isweb = False
                'objADependantTran._Ip = getIP()
                'objADependantTran._Host = getHostName()
                objADependantTran._Isweb = True
                objADependantTran._Ip = Session("IP_ADD").ToString
                objADependantTran._Host = Session("HOST_NAME").ToString
                'Sohail (18 May 2019) -- End
                objADependantTran._Form_Name = mstrModuleName
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objADependantTran._DeletedpndtbeneficestatustranIDs = strDpStatusUnkIDs
                objADependantTran._IsFromStatus = False
                'Sohail (18 May 2019) -- End
                If objADependantTran.Delete(CInt(Me.ViewState("Unkid")), popup_DeleteReason.Reason.Trim, CInt(Session("CompanyUnkId")), Nothing) = False Then
                    If objADependantTran._Message <> "" Then
                        msg.DisplayMessage("You Cannot Delete this Entry." & objADependantTran._Message, Me)
                        Exit Sub
                    End If
                Else
                    objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                     CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                     enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("EmployeeAsOnDate")), _
                                                     CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                     Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.DELETED, 0, _
                                                     objADependantTran._Employeeunkid.ToString(), "", Nothing, _
                                                     "dpndtbeneficetranunkid= " & CInt(Me.ViewState("Unkid")) & " ", Nothing, False, , _
                                                     "dpndtbeneficetranunkid= " & CInt(Me.ViewState("Unkid")) & " ", Nothing)
                End If

            Else
            objDependants_Benefice.Delete(CInt(Me.ViewState("Unkid")), CInt(Me.ViewState("EmployeeId")))
            End If
            'Gajanan [19-June-2019] -- End


            popup_DeleteReason.Dispose() ' Shani [01 FEB 2015] ---popup1.Dispose()
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError("ButDel_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeSkills
            Popup_Viewreport._FillType = enScreenName.frmDependantsAndBeneficiariesList
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE

            Popup_Viewreport.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnApprovalinfo_Click Event : " & ex.Message, Me)
            msg.DisplayError("btnApprovalinfo_Click Event : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (18 May 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
    Protected Sub mnuSetActiveInactive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetActiveInactive.Click
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Session("dependantsEmpunkid") = cboEmployee.SelectedValue
            Else
                Session("dependantsEmpunkid") = "0"
            End If
            Response.Redirect("~\HR\wPgDependantStatus.aspx", False)
        Catch ex As Exception
            msg.DisplayError("mnuSetActiveInactive_Click : " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'Sohail (18 May 2019) -- End

#End Region

#Region " Control's Event(s) "

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvDependantList.DeleteCommand
        Try

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes
            Me.ViewState.Add("EmployeeId", dgvDependantList.Items(e.Item.ItemIndex).Cells(7).Text)
            'Pinkal (01-Apr-2013) -- End


            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreason.Visible = True
            'SHANI [01 FEB 2015]--END
            


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(e.Item.Cells(7).Text)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            ' Me.ViewState.Add("Unkid", dgvDependantList.DataKeys(e.Item.ItemIndex))

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If DependantApprovalFlowVal Is Nothing Then
            If DependantApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(e.Item.Cells(7).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End


                Dim item = dgvDependantList.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(10).Text = e.Item.Cells(10).Text And x.Cells(8).Text.Trim <> "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), Me)
                    Exit Sub
                Else
                    Me.ViewState.Add("Unkid", CInt(dgvDependantList.DataKeys(e.Item.ItemIndex)))
                    delete()
                    FillGrid()
                End If
            Else
                Me.ViewState.Add("Unkid", CInt(dgvDependantList.DataKeys(e.Item.ItemIndex)))
            delete()
            FillGrid()
            End If
            'Gajanan [22-Feb-2019] -- End

        Catch ex As Exception
            msg.DisplayError("dgView_DeleteCommand Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvDependantList.ItemCommand
        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Session("IsFrom_AddEdit") = True
            ''Call SetPrevious_State()

            ''Nilay (02-Mar-2015) -- Start
            ''Enhancement - REDESIGN SELF SERVICE.
            'Session("dependantsEmpunkid") = CInt(cboEmployee.SelectedValue)
            ''Nilay (02-Mar-2015) -- End

            'If e.CommandName = "Select" Then
            '    Response.Redirect("~\HR\wPgEmpDepentants.aspx?ProcessId=" & b64encode(CStr(Val(dgvDependantList.DataKeys(e.Item.ItemIndex)))), False)
            'End If

            Session("IsFrom_AddEdit") = True
            If dgvDependantList.Items.Count > 0 Then
                If dgvDependantList.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).Count() > 0 Then
                    Dim item = dgvDependantList.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Color.White
                End If
            End If

            If e.CommandName = "Select" Then
                If DependantApprovalFlowVal Is Nothing Then
                    Dim item = dgvDependantList.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(10).Text = e.Item.Cells(10).Text And x.Cells(8).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), Me)
                        Exit Sub
                    Else
                        Session.Add("DependantId", dgvDependantList.DataKeys(e.Item.ItemIndex))
                        If CInt(cboEmployee.SelectedValue) > 0 Then
                            Session("dependantsEmpunkid") = cboEmployee.SelectedValue
                        End If
                        Response.Redirect("~\HR\wPgEmpDepentants.aspx?ProcessId=" & b64encode(CStr(Val(dgvDependantList.DataKeys(e.Item.ItemIndex)))), False)
                    End If
                Else
                    Session.Add("DependantId", dgvDependantList.DataKeys(e.Item.ItemIndex))
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Session("dependantsEmpunkid") = cboEmployee.SelectedValue
                    End If
                Response.Redirect("~\HR\wPgEmpDepentants.aspx?ProcessId=" & b64encode(CStr(Val(dgvDependantList.DataKeys(e.Item.ItemIndex)))), False)

            End If
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                Dim item = dgvDependantList.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(10).Text = e.Item.Cells(10).Text And x.Cells(8).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Color.LightCoral
                    item.ForeColor = Color.Black
                End If
            End If

            'Gajanan [22-Feb-2019] -- End

        Catch ex As Exception
            msg.DisplayError("dgView_ItemCommand Events : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgvDependantList.PageIndexChanged
        Try
            dgvDependantList.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError("dgView_PageIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvDependantList.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'If (e.Item.ItemIndex >= 0) Then
            '    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

            '        'Anjan (30 May 2012)-Start
            '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            '        'dgvDependantList.Columns(0).Visible = True : dgvDependantList.Columns(1).Visible = False 'Anjan (02 Mar 2012)


            '        'Pinkal (22-Nov-2012) -- Start
            '        'Enhancement : TRA Changes
            '        'dgvDependantList.Columns(0).Visible = True : dgvDependantList.Columns(1).Visible = Session("DeleteDependant")
            '        dgvDependantList.Columns(0).Visible = CBool(Session("EditDependant"))
            '        dgvDependantList.Columns(1).Visible = CBool(Session("DeleteDependant"))
            '        'Pinkal (22-Nov-2012) -- End
            '        'Anjan (30 May 2012)-End 

            '        'S.SANDEEP [ 16 JAN 2014 ] -- START
            '        If Session("ShowPending") IsNot Nothing Then
                    '    dgvDependantList.Columns(1).Visible = False
            '        End If
            '        'S.SANDEEP [ 16 JAN 2014 ] -- END

            '    Else
            '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
            '        'ENHANCEMENT : TRA CHANGES
            '        'dgvDependantList.Columns(0).Visible = True : dgvDependantList.Columns(1).Visible = ConfigParameter._Object._AllowDeleteDependants 'Anjan (02 Mar 2012)

            '        'Pinkal (22-Nov-2012) -- Start
            '        'Enhancement : TRA Changes
            '        'dgvDependantList.Columns(0).Visible = True : dgvDependantList.Columns(1).Visible = Session("AllowDeleteDependants")
            '        dgvDependantList.Columns(0).Visible = CBool(Session("AllowEditDependants"))
            '        dgvDependantList.Columns(1).Visible = CBool(Session("AllowDeleteDependants"))
            '        'Pinkal (22-Nov-2012) -- End


            '        'S.SANDEEP [ 27 APRIL 2012 ] -- END
            '    End If

            '    'Pinkal (16-Apr-2016) -- Start
            '    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            '    'If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.ToString.Trim.Length > 0 Then
            '    '    'S.SANDEEP [ 31 DEC 2013 ] -- START
            '    '    If Session("DateFormat").ToString <> Nothing Then
            '    '        e.Item.Cells(5).Text = CDate(eZeeDate.convertDate(e.Item.Cells(5).Text).ToString).Date.ToString(Session("DateFormat").ToString)
            '    '    Else
            '    '        e.Item.Cells(5).Text = CStr(CDate(eZeeDate.convertDate(e.Item.Cells(5).Text).ToString).Date)
            '    '    End If
            '    '    'S.SANDEEP [ 31 DEC 2013 ] -- END
            '    'End If

            '    If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.ToString.Trim.Length > 0 Then
            '        e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
            '    End If
            '    'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then

                If DependantApprovalFlowVal Is Nothing Then
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = CBool(Session("EditDependant"))
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = CBool(Session("DeleteDependant"))
                Else
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = CBool(Session("AllowEditDependants"))
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = CBool(Session("AllowDeleteDependants"))
                    End If

                    If e.Item.Cells(8).Text <> "&nbsp;" AndAlso e.Item.Cells(8).Text.Length > 0 Then
                        e.Item.BackColor = Color.PowderBlue
                        e.Item.ForeColor = Color.Black

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'lblPendingData.Visible = True
                        'btnApprovalinfo.Visible = True
                        objtblPanel.Visible = True
                        blnPendingEmployee = True
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(0).FindControl("ImgSelect"), LinkButton).Visible = False

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                End If

                Else
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        dgvDependantList.Columns(0).Visible = CBool(Session("EditDependant"))
                        dgvDependantList.Columns(1).Visible = CBool(Session("DeleteDependant"))
                    Else
                        dgvDependantList.Columns(0).Visible = CBool(Session("AllowEditDependants"))
                        dgvDependantList.Columns(1).Visible = CBool(Session("AllowDeleteDependants"))
                    End If
                End If

                If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
                End If

              

                'Gajanan [22-Feb-2019] -- End
            End If


        Catch ex As Exception
            msg.DisplayError("Procedure dgView_ItemDataBound : " & ex.Message, Me)
        End Try
    End Sub

    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    'S.SANDEEP [ 16 JAN 2014 ] -- START
    'Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
    '    Try
    '        Session("ShowPending") = chkShowPending.Checked
    '        Dim dsCombos As New DataSet
    '        Dim objEmp As New clsEmployee_Master
    '        If chkShowPending.Checked = True Then

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
    '            dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        CStr(Session("UserAccessModeSetting")), False, _
    '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
    '            'Shani(24-Aug-2015) -- End
    '            Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
    '            With cboEmployee
    '                .DataValueField = "employeeunkid"
    '                'Nilay (09-Aug-2016) -- Start
    '                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                '.DataTextField = "employeename"
    '                .DataTextField = "EmpCodeName"
    '                'Nilay (09-Aug-2016) -- End
    '                .DataSource = dtTab
    '                .DataBind()
    '                Try
    '                    .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                Catch ex As Exception
    '                    .SelectedValue = CStr(0)
    '                End Try
    '            End With
    '            BtnNew.Visible = False
    '        Else
    '            Session("ShowPending") = Nothing
    '            BtnNew.Visible = True
    '            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
    '                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        CStr(Session("UserAccessModeSetting")), True, _
    '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
    '                'Shani(24-Aug-2015) -- End
    '                With cboEmployee
    '                    .DataValueField = "employeeunkid"
    '                    'Nilay (09-Aug-2016) -- Start
    '                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                    '.DataTextField = "employeename"
    '                    .DataTextField = "EmpCodeName"
    '                    'Nilay (09-Aug-2016) -- End
    '                    .DataSource = dsCombos.Tables("Employee")
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            Else
    '                Dim objglobalassess = New GlobalAccess
    '                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '                With cboEmployee
    '                    .DataSource = objglobalassess.ListOfEmployee
    '                    .DataTextField = "loginname"
    '                    .DataValueField = "employeeunkid"
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            End If
    '        End If
    '        dgvDependantList.DataSource = Nothing : dgvDependantList.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError("chkShowPending_CheckedChanged : " & ex.Message, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [ 16 JAN 2014 ] -- END

    'S.SANDEEP [20-JUN-2018] -- End

#End Region

#Region "ToolBar Event"
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddDependant")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddDependants")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '        Response.Redirect("~\HR\wPgEmpDepentants.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("ToolbarEntry1_FormMode_Click :- " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 
#End Region

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--EN


        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text)
        Me.lblDBLastName.Text = Language._Object.getCaption(Me.lblDBLastName.ID, Me.lblDBLastName.Text)
        Me.lblDBFirstName.Text = Language._Object.getCaption(Me.lblDBFirstName.ID, Me.lblDBFirstName.Text)
        Me.lblDBRelation.Text = Language._Object.getCaption(Me.lblDBRelation.ID, Me.lblDBRelation.Text)
        Me.lblDBIdNo.Text = Language._Object.getCaption(Me.lblDBIdNo.ID, Me.lblDBIdNo.Text)
        Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.ID, Me.lblGender.Text)
        Me.lblBirthdate.Text = Language._Object.getCaption(Me.lblBirthdate.ID, Me.lblBirthdate.Text)

        dgvDependantList.Columns(2).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(2).FooterText, dgvDependantList.Columns(2).HeaderText)
        dgvDependantList.Columns(3).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(3).FooterText, dgvDependantList.Columns(3).HeaderText)
        dgvDependantList.Columns(4).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(4).FooterText, dgvDependantList.Columns(4).HeaderText)
        dgvDependantList.Columns(5).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(5).FooterText, dgvDependantList.Columns(5).HeaderText)
        dgvDependantList.Columns(6).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(6).FooterText, dgvDependantList.Columns(6).HeaderText)
        dgvDependantList.Columns(11).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(11).FooterText, dgvDependantList.Columns(11).HeaderText)

        Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")

        Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.ID, Me.lblPendingData.Text)
        Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.ID, Me.lblParentData.Text)
        Me.mnuSetActiveInactive.Text = Language._Object.getCaption(Me.mnuSetActiveInactive.ID, Me.mnuSetActiveInactive.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End

End Class