﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class wPgBenefitList
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpBenefit As New clsBenefitCoverage_tran
    Dim msg As New CommonCodes

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmBenefitCoverage_List"
    'Pinkal (06-May-2014) -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objTranHeads As New clsTransactionHead
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, )
                'End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmp.GetEmployeeList("Emp", True, )
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Emp", True)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Emp")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With drpEmployee
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataSource = objglobalassess.ListOfEmployee
                    .DataBind()
                End With
            End If

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "List")
            With drpBenefitGroup
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            Call drpBenefitGroup_SelectedIndexChanged(Nothing, Nothing)
            'Sohail (02 May 2012) -- End


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'dsCombos = objTranHeads.getComboList("List", True, enTranHeadType.EarningForEmployees, , enTypeOf.BENEFIT)
            dsCombos = objTranHeads.getComboList(CStr(Session("Database_Name")), "List", True, enTranHeadType.EarningForEmployees, , enTypeOf.BENEFIT)
            'Pinkal (28-Dec-2015) -- End


            With DrpTranHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError("Procedure FillCombo : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            drpBenefitGroup.SelectedValue = CStr(0)
            drpPlan.SelectedValue = CStr(0)
            DrpTranHead.SelectedValue = CStr(0)
            drpEmployee.SelectedIndex = 0
        Catch ex As Exception
            msg.DisplayError("Procedure ClearObject : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try


            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewBenefitList")) = False Then Exit Sub
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'dsList = objEmpBenefit.GetList("BCoverage", , , , Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))

            If CInt(drpBenefitGroup.SelectedValue) > 0 Then
                StrSearching &= "AND hremp_benefit_coverage.benefitgroupunkid = " & CInt(drpBenefitGroup.SelectedValue) & " "
            End If

            If CInt(IIf(drpPlan.SelectedValue = "", 0, drpPlan.SelectedValue)) > 0 Then
                StrSearching &= "AND hremp_benefit_coverage.benefitplanunkid = " & CInt(drpPlan.SelectedValue) & " "
            End If

            If CInt(drpEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hremp_benefit_coverage.employeeunkid = " & CInt(drpEmployee.SelectedValue) & " "
            End If

            If CInt(DrpTranHead.SelectedValue) > 0 Then
                StrSearching &= "AND hremp_benefit_coverage.tranheadunkid = " & CInt(DrpTranHead.SelectedValue) & " "
            End If

            If CDbl(radiolist1.SelectedValue) >= 0 Then
                If radiolist1.Items.Item(0).Selected = True Then
                    StrSearching &= "AND hremp_benefit_coverage.isvoid = 1 "
                ElseIf radiolist1.Items.Item(1).Selected = True Then
                    StrSearching &= "AND hremp_benefit_coverage.isvoid = 0 "
                End If
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            dsList = objEmpBenefit.GetList(CStr(Session("Database_Name")), _
                                           CInt(Session("UserId")), _
                                           CInt(Session("Fin_year")), _
                                           CInt(Session("CompanyUnkId")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                           CStr(Session("UserAccessModeSetting")), True, _
                                           CBool(Session("IsIncludeInactiveEmp")), "BCoverage", True, 0, 0, 0, StrSearching, _
                                           CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))


                dtTable = New DataView(dsList.Tables("BCoverage"), "", "", DataViewRowState.CurrentRows).ToTable

            If dtTable IsNot Nothing Then
                dgvBenefit.DataSource = dtTable
                dgvBenefit.DataKeyField = "BCoverageId"
                dgvBenefit.DataBind()
            End If


        Catch ex As Exception
            msg.DisplayError("Procedure FillGrid : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            SetLanguage()

            Dim clsuser As New User
            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End

                    chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    If Session("ShowPending") IsNot Nothing Then
                        chkShowPending.Checked = True
                    End If

                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End
                    chkShowPending.Visible = False
                End If
                Call FillCombo()

                If CInt(IIf(drpEmployee.SelectedValue = "", 0, drpEmployee.SelectedValue)) > 0 Then
                    Call FillGrid()
                End If

                If Session("ShowPending") IsNot Nothing Then
                    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                End If
            End If
        Catch ex As Exception
            msg.DisplayError("Procedure Page_Load : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Events "

    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
        Catch ex As Exception
            msg.DisplayError("BtnClose_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Try
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError("btnSearch_Click Event : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnreset.Click
        Try
            Call ClearObject()
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            Call FillGrid()
            Else
                Dim dtTable As New DataTable
                dgvBenefit.DataSource = dtTable
                dgvBenefit.DataKeyField = "BCoverageId"
                dgvBenefit.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError("btnReset_Click Event : " & ex.Message, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Close_botton.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError("Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region " Control's Event(s) "

    Protected Sub drpBenefitGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpBenefitGroup.SelectedIndexChanged
        Try
                Dim dsList As New DataSet
                Dim objBenefit As New clsbenefitplan_master
                dsList = objBenefit.getComboList("List", True, CInt(drpBenefitGroup.SelectedValue))
                With drpPlan
                    .DataValueField = "benefitplanunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
        Catch ex As Exception
            msg.DisplayError("Procedure drpBenefitGroup_SelectedIndexChanged : " & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
        Try
            Session("ShowPending") = chkShowPending.Checked
            Dim dsCombos As New DataSet
            Dim objEmp As New clsEmployee_Master
            If chkShowPending.Checked = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), False, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End
                Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dtTab
                    .DataBind()
                    Try
                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                    Catch ex As Exception
                        .SelectedValue = CStr(0)
                    End Try
                End With
            Else
                Session("ShowPending") = Nothing
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                    dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    With drpEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    With drpEmployee
                        .DataSource = objglobalassess.ListOfEmployee
                        .DataTextField = "loginname"
                        .DataValueField = "employeeunkid"
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                End If
            End If
            dgvBenefit.DataSource = Nothing : dgvBenefit.DataBind()
        Catch ex As Exception
            msg.DisplayError("chkShowPending_CheckedChanged : " & ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Close_botton.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Close_botton.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End

        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblBenefitType.Text = Language._Object.getCaption(Me.lblBenefitType.ID, Me.lblBenefitType.Text)
        Me.lblBenefitPlan.Text = Language._Object.getCaption(Me.lblBenefitPlan.ID, Me.lblBenefitPlan.Text)
        Me.lblTransactionHead.Text = Language._Object.getCaption(Me.lblTransactionHead.ID, Me.lblTransactionHead.Text)

        Me.dgvBenefit.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvBenefit.Columns(2).FooterText, Me.dgvBenefit.Columns(2).HeaderText)
        Me.dgvBenefit.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvBenefit.Columns(3).FooterText, Me.dgvBenefit.Columns(3).HeaderText)
        Me.dgvBenefit.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvBenefit.Columns(4).FooterText, Me.dgvBenefit.Columns(4).HeaderText)

        radiolist1.Items(0).Text = Language._Object.getCaption("radShowVoid", radiolist1.Items(0).Text)
        radiolist1.Items(1).Text = Language._Object.getCaption("radShowActive", radiolist1.Items(1).Text)
        radiolist1.Items(2).Text = Language._Object.getCaption("radShowAll", radiolist1.Items(2).Text)

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub


End Class
