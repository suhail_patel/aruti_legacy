﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Chart.aspx.vb" Inherits="OrganizationChart_Chart" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Organization Chart Plugin</title>
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery.orgchart.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/ProfileCard.css?version=1">
    <style type="text/css">
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="chart-container">
    </div>
    <div class="modal_background hidden">
        <div class="modal-body">
            <button type="button" class="close zija_modal_close" id="zija_modal_close_x">
                <span aria-hidden="true">×</span>
            </button>
            <div class="card">
                <header>
    <div>
      <h1 class="name"> Freddy González</h1>
      <h4 class="position">Frontend developer and designer</h4>
    </div><img src="https://s.cdpn.io/profiles/user/216787/80.jpg?156576273" alt=""/>
  </header>
                <nav>
    <ul>
      <li class="Department"><strong>Department:</strong><span>fredogonzalezr@gmail.com</span></li>
      <li class="Email"><strong>Email-id:</strong><span>codepen.io/fredddie</span></li>
      <li class="Mobile"><strong>Mobile No:</strong><span>dribbble.com/fredddie</span></li>
    </ul>
  </nav>
            </div>
        </div>
    </div>
    </form>

    <script type="text/javascript" src="js/jquery.min.js"></script>

    <script type="text/javascript" src="js/jquery.orgchart.js"></script>

    <script type="text/javascript" src="js/html2canvas.min.js"></script>

    <script type="text/javascript" src="js/jspdf.debug.js"></script>

    <script type="text/javascript">
    $(function() {
    var data = <%= Session("orgdata") %>;   
    var ChartType =<%= Session("status") %>;
    var Filterdata = ''
    var nodeTitle;
    
    debugger;
    if(ChartType == 1 )
    {
        nodeTitle = "Employeename";
    }
     else if(ChartType == 2)
    {
        nodeTitle="Jobname";
    }
    
    else if(ChartType == 3)
    {
        nodeTitle="JobPosition";
    }

    else if(ChartType == 4)
    {
        nodeTitle="Jobname";
    }
    
    
    
    if(<%= Session("isFilterdata") %> == true)
    {
        Filterdata = <%= Session("FilterData") %>;
    }
    else
    {
        Filterdata='';
    }
    
    var visibleLevel = 2;
    debugger;
    if (<%= Session("expandall") %> === true)
    {
        visibleLevel = 999;
    }
    
     $('#chart-container').orgchart({
          'data' : data,
          'nodeContent': 'title',
          'nodeTitle': nodeTitle,
          'verticalLevel': 3,
          'visibleLevel': visibleLevel,
	      'exportButton': true,
          'exportFilename': 'MyOrgChart',
          'viewJobdetail' : <%= Session("viewJobdetail") %>,
          'isFilterdata' : <%= Session("isFilterdata") %>,
	      'exportFileextension': 'pdf',
	      'ShowFilter' : <%= Session("ShowFilter") %>,
	      'Filterdata' : Filterdata,	      
	      'chartType' : ChartType,
	      onClickNode: function (node) {
                            log(node);
                        }
        });
  }); 
  
        debugger;
  function log(childdata) {
		$('.modal_background').removeClass('hidden');
	     $('.modal-body .name').html(childdata.Employeename);
	    $('.modal-body img').attr('src',"data:image/png;base64,"+  childdata.Photo + "");
	     $('.modal-body .position').html(childdata.JobPosition);
	     
	     $('.modal-body .Department span').html(childdata.Department);
	     $('.modal-body .Email span').html(childdata.Email);
	     if(childdata.Mobile != '')
	     {
	        $('.modal-body .Mobile span').html(childdata.Mobile);
         }
         else
         {
	        $('.modal-body .Mobile span').html("Data Not Available");         
         }
  }
  
	$('.zija_modal_close').click(function() {
		$('.modal_background').addClass('hidden');
	});


    </script>

</body>
</html>
