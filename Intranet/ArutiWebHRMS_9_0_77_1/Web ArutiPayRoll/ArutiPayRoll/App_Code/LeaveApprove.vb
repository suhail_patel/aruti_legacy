﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.String


Public Class LeaveApprove
    Dim dsPedingList As New DataSet
    Dim strSearching As String = ""
    Dim clsuser As New User



    Public Sub leaveApprove()
        Try

            Dim objpending As New Aruti.Data.clspendingleave_Tran
            '  Dim objApprover As New Aruti.Data.clsapprover_Usermapping
            Dim objApprover As New Aruti.Data.clsleaveapprover_master
            Dim objApprove As New Aruti.Data.clsapproverlevel_master
            Dim USER As New Aruti.Data.User



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsPedingList = objpending.GetList("PendingProcess")
            dsPedingList = objpending.GetList("PendingProcess", _
                                              HttpContext.Current.Session("Database_Name"), _
                                              HttpContext.Current.Session("UserId"), _
                                              HttpContext.Current.Session("Fin_year"), _
                                              HttpContext.Current.Session("CompanyUnkId"), _
                                              eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")), _
                                              True, HttpContext.Current.Session("IsIncludeInactiveEmp"))
            'Shani(20-Nov-2015) -- End

            'START FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            If dsPedingList.Tables("PendingProcess").Rows.Count > 0 Then
                Dim drRow As DataRow() = dsPedingList.Tables("PendingProcess").Select("mapuserunkid = " & CInt(USER._Object._Userunkid), "priority desc")
                If drRow.Length > 0 Then
                    Dim objmapuser As New Aruti.Data.clsapprover_Usermapping
                    objmapuser.GetData(Aruti.Data.enUserType.Approver, CInt(drRow(0)("leaveapproverunkid")), , )
                    objApprover._Approverunkid = objmapuser._Approverunkid

                End If
            End If
            'This to check the level of approver higer or lower.
            Dim objApproverLevel As New Aruti.Data.clsapproverlevel_master
            objApproverLevel._Levelunkid = objApprove._Levelunkid

            'END FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER
            'this to see the priority of approver to show data or not.


            dtLeaveForm = New DataView(dsPedingList.Tables("PendingProcess"), "priority <= " & objApproverLevel._Priority, "", DataViewRowState.CurrentRows).ToTable
        Catch ex As Exception
            Throw New Exception("leaveApprove-->leaveApprove Method!!!" & ex.Message.ToString)
        Finally
        End Try

    End Sub

    Private pdtLeaveForm As DataTable
    Public Property dtLeaveForm() As DataTable
        Get
            Return pdtLeaveForm
        End Get
        Set(ByVal value As DataTable)
            value = pdtLeaveForm
        End Set
    End Property
End Class
