﻿Imports Microsoft.VisualBasic
Imports System.Exception
Imports System.Web.Services 'Sohail (04 Jun 2012)
Imports Aruti.Data
Imports System.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.DirectoryServices
Imports System.Web.Script.Serialization
Imports System.IO.Packaging
Imports System.IO

Public Class Basepage
    Inherits System.Web.UI.Page

    Public Const m_NoLicenceMsg As String = "Sorry, you are not registered to use this module. Please contact Aruti support team to register this module." 'Sohail (27 Apr 2013)

    'Sohail (23 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    Public Const m_CustomErrorMsg As String = "Something went wrong, Please contact administrator!"
    'Sohail (23 Mar 2019) -- End

    Dim DisplayMessage As New CommonCodes 'Sohail (07 Jun 2013)

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If CType(Session("sessionexpired"), Boolean) = True Then
                Response.Write("<script>alert('Session Expired');</script>")
                Session("sessionexpired") = Nothing
            End If
            Session("mainfullurl") = Request.Url.OriginalString
            Session("issitelocal") = InStr(Session("mainfullurl"), "localhost") > 0
            If Session("LoginBy") IsNot Nothing Then
                Call SetDateFormat()
            End If
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try
    End Sub

    Protected Overloads Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        Try
            MyBase.OnPreInit(e)
            If Session("cur_theme") Is Nothing Then
                Session.Add("cur_theme", System.Configuration.ConfigurationManager.AppSettings("DefaultTheme"))
                Page.Theme = DirectCast(Session("cur_theme"), String)
            Else
                Page.Theme = DirectCast(Session("cur_theme"), String)
            End If
            If Request.ServerVariables("http_user_agent").IndexOf("Safari", System.StringComparison.CurrentCultureIgnoreCase) <> -1 Then
                Page.ClientTarget = "uplevel"
            End If
            Page.Theme = Session("Theme")
            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1 'Sohail (17 Dec 2018) - Commented to prevent duplicate header
            'Response.AppendHeader("X-Frame-Options", "DENY")
            Response.AppendHeader("X-XSS-Protection", "1; mode=block")
            'Sohail (13 Dec 2018) -- End
        Catch ex As Exception
            'Throw New Exception("Basepage->OnPreInit event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try

        
    End Sub
    Protected Overloads Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try
            If Me.IsLoginRequired Then
                'Sohail (21 Mar 2015) -- Start
                'Enhancement - New UI Notification Link Changes.
                'If Session("Login") = Nothing Then
                If Session("clsuser") Is Nothing Then
                    'Sohail (21 Mar 2015) -- End
                    'Sohail (07 Jun 2013) -- Start
                    'TRA - ENHANCEMENT
                    'Me.Response.Redirect("~/index.aspx", False)
                    DisplayMessage.DisplayMessage("Sorry, Session is Expired. Please Re-Login again.", Me, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                    'Sohail (07 Jun 2013) -- End
                Else
                    'Sohail (21 Mar 2015) -- Start
                    'Enhancement - New UI Notification Link Changes.
                    'If CType(Session("Login"), Boolean) = False Then
                    If Session("clsuser") Is Nothing Then
                        'Sohail (21 Mar 2015) -- End
                        Me.Response.Redirect("~/index.aspx", False)
                    End If
                End If
            End If

            If Session("LoginBy") IsNot Nothing Then
                Call SetDateFormat()
            End If

            If ArtLic._Object Is Nothing Then ArtLic._Object = New ArutiLic(False) 'Sohail (27 Apr 2013)
            MyBase.OnLoad(e)
        Catch ex As Exception
            'Throw New Exception("Basepage->OnLoad event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try

       
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error
        Try
            'Sohail (31 Dec 2019) -- Start
            'NMB Enhancement # : Search option on Global Loan Change Status screen.
            'If Not System.Web.HttpContext.Current Is Nothing Then
               
            'End If
            Dim strError As String = Server.GetLastError.Message & "; " & Server.GetLastError.StackTrace
            DisplayMessage.DisplayMessage(strError, Me.Page)
            'Sohail (31 Dec 2019) -- End
        Catch ex As Exception
            'Throw New Exception("Basepage->Page Error Event!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
        Finally
        End Try

    End Sub

    

    Private pIsLoginRequired As Boolean
    Public Property IsLoginRequired() As Boolean
        Get
            Return pIsLoginRequired
        End Get
        Set(ByVal value As Boolean)
            pIsLoginRequired = value
        End Set
    End Property

    'Sohail (20 Feb 2015) -- Start
    Public Sub GetDatabaseVersion()
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim ds As System.Data.DataSet
        Dim strQ As String
        Try
            strQ = "EXEC hrmsConfiguration..version "

            ds = clsDataOpr.WExecQuery(strQ, "List")

            If ds.Tables(0).Rows.Count > 0 Then
                Session("DatabaseVersion") = ds.Tables(0).Rows(0).Item(0).ToString
            Else
                Session("DatabaseVersion") = ""
            End If

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            DisplayMessage.DisplayError(ex.Message, Me)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (20 Feb 2015) -- End

    'Sohail (04 Jun 2012) -- Start
    'TRA - ENHANCEMENT - View Online Memberes
    <WebMethod()> Public Shared Function UpdateUsers() As String
        Dim res As String = ""
        Try

        
        'lstMembers.Items.Clear()

        'For Each s As String In clsActiveUserEngine.GetUserName()
        '    lstMembers.Items.Add(New ListItem(s, s))
        'Next

        Dim users As IEnumerable(Of String) = clsActiveUserEngine.GetUserName()
        SyncLock users
            For Each s In users
                res += s.ToString.Replace(vbCrLf, " ") & ","
            Next
        End SyncLock

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("UpdateUsers", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
        Return res
    End Function

    <WebMethod()> Public Shared Function Joined(ByVal strUserId As String, ByVal strUserName As String, ByVal enUserType As clsActiveUsers.enUserType) As String
        Try
        clsActiveUserEngine.Joined(strUserId, strUserName, enUserType)

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("Joined", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
        Return ""
    End Function

    <WebMethod()> Public Shared Function Leaved(ByVal strUserId As String) As String
        Try
        clsActiveUserEngine.Leaved(strUserId)

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("Leaved", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
        Return ""
    End Function
    'Sohail (04 Jun 2012) -- End

    'Sohail (06 Jun 2012) -- Start
    'TRA - ENHANCEMENT
    <WebMethod()> Public Shared Sub LogOut()

        'Pinkal (03-Sep-2012) -- Start
        'Enhancement : TRA Changes

        'If HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee Then
        '    Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")))
        'ElseIf HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then
        '    Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")))
        'End If
        Try
        If HttpContext.Current.Session("LoginBy") Is Nothing Then
        ElseIf HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee Then
            Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
        ElseIf HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then
            Aruti.Data.SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
        End If

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("LogOut", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End

        'HttpContext.Current.Session("LoginBy") = Nothing 'Sohail (08 May 2014)
        'Pinkal (03-Sep-2012) -- End

    End Sub
    'Sohail (06 Jun 2012) -- End

    'Sohail (18 May 2013) -- Start
    'TRA - ENHANCEMENT
    <WebMethod()> Public Shared Sub SectionClicked(ByVal intMenuView As Integer, ByVal strImgAcc As String)
        Try
        HttpContext.Current.Session("menuView") = intMenuView
        HttpContext.Current.Session("imgAcc") = strImgAcc
        'Sohail (24 Nov 2016) -- Start
        'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
        UpdateLastViewID(intMenuView)
        'Sohail (24 Nov 2016) -- End

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("LogOut", ex)
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub

    <WebMethod()> Public Shared Sub SetIsAuthorized(ByVal blnIsAuthorized As Boolean)
        HttpContext.Current.Session("IsAuthorized") = blnIsAuthorized
    End Sub
    'Sohail (18 May 2013) -- End
    'Hemant (29 Dec 2018) -- Start
    'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
    <WebMethod()> Public Shared Sub SetReturnURLFalse()
        HttpContext.Current.Session("ReturnURL") = Nothing
    End Sub
    'Hemant (29 Dec 2018) -- End

    'Sohail (12 Mar 2015) -- Start
    'ENHANCEMENT
    <WebMethod()> Public Shared Sub SetFavourites(ByVal blnFavourite As Boolean, ByVal strMenuID As String)
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim ds As System.Data.DataSet
        Dim strQ As String
        Dim strMenuFilter As String = ""
        Dim intMenuID As Integer = 0
        Dim intReportID As Integer = 0

        Try

            Dim intFavourite As Integer = CInt(Int(blnFavourite))

            If strMenuID.Substring(0, 4).ToLower = "menu" Then
                strMenuFilter &= " AND menuunkid = " & CInt(strMenuID.Substring(5)) & " "
                intMenuID = CInt(strMenuID.Substring(5))
            Else
                strMenuFilter &= " AND reportunkid = " & CInt(strMenuID.Substring(5)) & " "
                intReportID = CInt(strMenuID.Substring(5))
            End If


            If HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then

                strQ = "IF NOT EXISTS ( SELECT  usermenuunkid " & _
                                    "FROM    hrmsConfiguration..cfuser_menu " & _
                                    "WHERE   isactive = 1 " & _
                                            "AND userunkid = " & CInt(HttpContext.Current.Session("UserId")) & " " & _
                                            " " & strMenuFilter & " ) " & _
                        "BEGIN " & _
                            "INSERT  INTO hrmsConfiguration..cfuser_menu " & _
                                    "( userunkid  " & _
                                    ", employeeunkid " & _
                                    ", menuunkid " & _
                                    ", reportunkid " & _
                                    ", isfavourite " & _
                                    ", isactive " & _
                                    ") " & _
                            "VALUES  ( " & CInt(HttpContext.Current.Session("UserId")) & "  " & _
                                    ", 0 " & _
                                    ", " & intMenuID & " " & _
                                    ", " & intReportID & " " & _
                                    ", " & intFavourite & " " & _
                                    ", 1 " & _
                                    ") " & _
                        "END " & _
                    "ELSE " & _
                        "BEGIN " & _
                            "UPDATE  hrmsConfiguration..cfuser_menu " & _
                            "SET     isfavourite = " & intFavourite & " " & _
                            "WHERE   isactive = 1 " & _
                                    "AND userunkid = " & CInt(HttpContext.Current.Session("UserId")) & " " & _
                                    " " & strMenuFilter & " " & _
                        "END "
            Else

                strQ = "IF NOT EXISTS ( SELECT  usermenuunkid " & _
                                    "FROM    hrmsConfiguration..cfuser_menu " & _
                                    "WHERE   isactive = 1 " & _
                                            "AND employeeunkid = " & CInt(HttpContext.Current.Session("Employeeunkid")) & " " & _
                                            " " & strMenuFilter & " ) " & _
                        "BEGIN " & _
                            "INSERT  INTO hrmsConfiguration..cfuser_menu " & _
                                    "( userunkid  " & _
                                    ", employeeunkid " & _
                                    ", menuunkid " & _
                                    ", reportunkid " & _
                                    ", isfavourite " & _
                                    ", isactive " & _
                                    ") " & _
                            "VALUES  ( 0  " & _
                                    ", " & CInt(HttpContext.Current.Session("Employeeunkid")) & " " & _
                                    ", " & intMenuID & " " & _
                                    ", " & intReportID & " " & _
                                    ", " & intFavourite & " " & _
                                    ", 1 " & _
                                    ") " & _
                        "END " & _
                    "ELSE " & _
                        "BEGIN " & _
                            "UPDATE  hrmsConfiguration..cfuser_menu " & _
                            "SET     isfavourite = " & intFavourite & " " & _
                            "WHERE   isactive = 1 " & _
                                    "AND employeeunkid = " & CInt(HttpContext.Current.Session("Employeeunkid")) & " " & _
                                    " " & strMenuFilter & " " & _
                        "END "

            End If

            ds = clsDataOpr.WExecQuery(strQ, "List")

            If clsDataOpr.ErrorMessage <> "" Then
                Throw New Exception(clsDataOpr.ErrorMessage)
            End If

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            InsertErrorLog("SetFavourites", ex)
            'Pinkal (13-Aug-2020) -- End
        End Try

    End Sub
    'Sohail (12 Mar 2015) -- End

    <WebMethod()> Public Shared Function MSS() As String
        Dim base As New Basepage
        Dim strError As String = ""
        Try

            If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.User, CInt(HttpContext.Current.Session("U_UserID"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page)
                Throw New Exception(strError)
                Exit Try
            End If

            SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            clsActiveUserEngine.Leaved("EMP" & HttpContext.Current.Session("Employeeunkid").ToString)



            HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
            HttpContext.Current.Session("LangId") = HttpContext.Current.Session("U_LangId")
            HttpContext.Current.Session("UserId") = HttpContext.Current.Session("U_UserID")
            HttpContext.Current.Session("Employeeunkid") = HttpContext.Current.Session("U_Employeeunkid")
            HttpContext.Current.Session("UserName") = HttpContext.Current.Session("U_UserName")
            HttpContext.Current.Session("Password") = HttpContext.Current.Session("U_Password")
            HttpContext.Current.Session("LeaveBalances") = HttpContext.Current.Session("U_LeaveBalances")
            HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("U_MemberName")
            HttpContext.Current.Session("RoleID") = HttpContext.Current.Session("U_RoleID")
            HttpContext.Current.Session("Firstname") = HttpContext.Current.Session("U_Firstname")
            HttpContext.Current.Session("Surname") = HttpContext.Current.Session("U_Surname")
            HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("U_DisplayName")
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            HttpContext.Current.Session("Theme_id") = HttpContext.Current.Session("U_Theme_id")
            HttpContext.Current.Session("Lastview_id") = HttpContext.Current.Session("U_Lastview_id")
            'Sohail (24 Nov 2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            HttpContext.Current.Session("Email") = HttpContext.Current.Session("U_Email")
            'S.SANDEEP |08-JAN-2019| -- END

            strError = ""
            If base.SetUserSessions(strError) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If

            strError = ""
            If base.SetCompanySessions(strError, CInt(HttpContext.Current.Session("CompanyUnkId")), CInt(HttpContext.Current.Session("LangId"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If


            'HttpContext.Current.Response.Redirect(HttpContext.Current.Session("rootpath") & "UserHome.aspx")
            Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            InsertErrorLog("MSS", ex)
            Return ""
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function

    <WebMethod()> Public Shared Function ESS() As String
        Dim base As New Basepage
        Dim strError As String = ""
        Try

            If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(HttpContext.Current.Session("E_Employeeunkid"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page)
                Throw New Exception(strError)
                Exit Try
            End If

            SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            clsActiveUserEngine.Leaved("USER" & HttpContext.Current.Session("UserId").ToString)


            HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
            HttpContext.Current.Session("LangId") = 1 'Session("E_LangId")
            HttpContext.Current.Session("UserId") = HttpContext.Current.Session("E_UserID")
            HttpContext.Current.Session("Employeeunkid") = HttpContext.Current.Session("E_Employeeunkid")
            HttpContext.Current.Session("UserName") = HttpContext.Current.Session("E_UserName")
            HttpContext.Current.Session("Password") = HttpContext.Current.Session("E_Password")
            HttpContext.Current.Session("LeaveBalances") = HttpContext.Current.Session("E_LeaveBalances")
            HttpContext.Current.Session("MemberName") = HttpContext.Current.Session("E_MemberName")
            HttpContext.Current.Session("RoleID") = HttpContext.Current.Session("E_RoleID")
            HttpContext.Current.Session("Firstname") = HttpContext.Current.Session("E_Firstname")
            HttpContext.Current.Session("Surname") = HttpContext.Current.Session("E_Surname")
            HttpContext.Current.Session("DisplayName") = HttpContext.Current.Session("E_DisplayName")
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            HttpContext.Current.Session("Theme_id") = HttpContext.Current.Session("E_Theme_id")
            HttpContext.Current.Session("Lastview_id") = HttpContext.Current.Session("E_Lastview_id")
            'Sohail (24 Nov 2016) -- End

            strError = ""
            If base.SetUserSessions(strError) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If

            strError = ""
            If base.SetCompanySessions(strError, CInt(HttpContext.Current.Session("CompanyUnkId")), CInt(HttpContext.Current.Session("LangId"))) = False Then
                'DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Throw New Exception(strError)
                Exit Try
            End If


            'HttpContext.Current.Response.Redirect(HttpContext.Current.Session("rootpath") & "UserHome.aspx")
            Return HttpContext.Current.Session("rootpath") & "UserHome.aspx"

        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            InsertErrorLog("ESS", ex)
            Return ""
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function

    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    Public Function SetUserSessions(ByRef strErrorMsg As String, Optional ByRef blnPasswordExpired As Boolean = False) As Boolean
        Dim clsDataOpr As New clsDataOperation(True)
        Dim objGlobalAccess As New GlobalAccess
        Dim ds As DataSet
        Dim strQ As String = ""
        strErrorMsg = ""
        Try

            Session("sessionexpired") = False
            HttpContext.Current.Session("Login") = True
            Session("rootpath") = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath & "/"

            If Session("LoginBy") IsNot Nothing Then
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    clsActiveUserEngine.Leaved("USER" & Session("UserId").ToString)
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    clsActiveUserEngine.Leaved("EMP" & Session("Employeeunkid").ToString)
                End If
            End If


            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            Dim objConfigOption As New clsConfigOptions
            Dim mstrAllowEmpSystemAccessOnActualEOCRetirementDate As String = ""
            Dim mblnAllowEmpSystemAccessOnActualEOCRetirementDate As Boolean = False
            objConfigOption.IsValue_Changed("AllowEmpSystemAccessOnActualEOCRetirementDate", "-999", mstrAllowEmpSystemAccessOnActualEOCRetirementDate)
            mblnAllowEmpSystemAccessOnActualEOCRetirementDate = CBool(IIf(mstrAllowEmpSystemAccessOnActualEOCRetirementDate.Length <= 0, False, mstrAllowEmpSystemAccessOnActualEOCRetirementDate))
            objConfigOption = Nothing
            'Pinkal (27-Nov-2020) -- End

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'strQ = " select employeeunkid,(isnull(Firstname,'') + ' ' + isnull(othername,' ') + ' ' + isnull(surname ,' '))  as loginname from " & Session("mdbname") & "..hremployee_master "
            strQ = "SELECT " & _
                        "   employeeunkid " & _
                        ",  ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,' ') + ' ' + ISNULL(surname ,' ')  AS loginname " & _
                   "FROM " & Session("mdbname") & "..hremployee_master "
            'Nilay (09-Aug-2016) -- End

            If Session("LoginBy") = Global.User.en_loginby.Employee Then

                Dim sName As String = String.Empty
                sName = Session("DisplayName")
                If sName.Contains("'") Then
                    sName = sName.Replace("'", "''")
                End If
                strQ &= "   WHERE displayname = '" & sName & "' " & vbCrLf

            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.

                If CInt(Session("AuthenticationMode")) = enAuthenticationMode.BASIC_AUTHENTICATION Then

                Dim objUser As New clsUserAddEdit
                Dim mintUserUnkid As Integer
                If Session("UserName") IsNot Nothing AndAlso Session("Password") IsNot Nothing Then
                        'Pinkal (27-Nov-2020) -- Start
                        'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                        'mintUserUnkid = objUser.IsValidLogin(Session("UserName"), Session("Password"), enLoginMode.USER, "hrmsConfiguration")
                        mintUserUnkid = objUser.IsValidLogin(Session("UserName"), Session("Password"), enLoginMode.USER, "hrmsConfiguration", mblnAllowEmpSystemAccessOnActualEOCRetirementDate)
                        'Pinkal (27-Nov-2020) -- End
                End If

                If mintUserUnkid <= 0 AndAlso objUser._Message <> "" Then
                    If objUser._ShowChangePasswdfrm = True AndAlso objUser._RetUserId > 0 Then
                        Session("ExUId") = objUser._RetUserId
                        blnPasswordExpired = True
                        Exit Function
                    Else
                        strErrorMsg = objUser._Message
                        Exit Function
                    End If
                End If
                objUser = Nothing
                End If
                'Pinkal (03-Apr-2017) -- End

            End If

            strQ &= " order by loginname " & vbCrLf
            ds = clsDataOpr.WExecQuery(strQ, "cffinancial_year_tran")

            objGlobalAccess.ListOfEmployee = ds.Tables(0)

            If Session("LoginBy") = Global.User.en_loginby.User Then
                objGlobalAccess.userid = Session("UserId")
            Else
                objGlobalAccess.employeeid = Session("Employeeunkid")
            End If
            HttpContext.Current.Session("objGlobalAccess") = objGlobalAccess

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            If CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Black Then
                HttpContext.Current.Session("Theme") = "black"
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Green Then
                HttpContext.Current.Session("Theme") = "green"
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = enSelfServiceTheme.Yellow Then
                HttpContext.Current.Session("Theme") = "yellow"
                'Sohail (28 Mar 2018) -- Start
                'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            ElseIf CInt(HttpContext.Current.Session("Theme_id")) = CInt(enSelfServiceTheme.Brown) Then
                HttpContext.Current.Session("Theme") = "brown"
                'Sohail (28 Mar 2018) -- End
            Else
                HttpContext.Current.Session("Theme") = "blue"
            End If

            Session("menuView") = CInt(HttpContext.Current.Session("Lastview_id"))
            'Sohail (24 Nov 2016) -- End

            'gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
            If gobjLocalization Is Nothing Then
            gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
            Else
                gobjLocalization.Refresh(False, enArutiApplicatinType.Aruti_Payroll)
                'Pinkal (09-Aug-2021) -- End
            End If

            gobjLocalization._LangId = HttpContext.Current.Session("LangId")

            Dim objUserPrivilege As New clsUserPrivilege
            objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

            '*** Payroll
            Session("AllowGlobalPayment") = objUserPrivilege._AllowGlobalPayment
            Session("DeletePayment") = objUserPrivilege._DeletePayment
            Session("AllowToViewGlobalVoidPaymentList") = objUserPrivilege._AllowToViewGlobalVoidPaymentList
            Session("AllowToApprovePayment") = objUserPrivilege._AllowToApprovePayment
            Session("AllowToVoidApprovedPayment") = objUserPrivilege._AllowToVoidApprovedPayment
            Session("AllowToViewBatchPostingList") = objUserPrivilege._AllowToViewBatchPostingList
            Session("AllowToAddBatchPosting") = objUserPrivilege._AllowToAddBatchPosting
            Session("AllowToEditBatchPosting") = objUserPrivilege._AllowToEditBatchPosting
            Session("AllowToDeleteBatchPosting") = objUserPrivilege._AllowToDeleteBatchPosting
            Session("AllowToPostBatchPostingToED") = objUserPrivilege._AllowToPostBatchPostingToED
            Session("AddPayment") = objUserPrivilege._AddPayment
            Session("AllowToViewPaymentList") = objUserPrivilege._AllowToViewPaymentList
            Session("AddCashDenomination") = objUserPrivilege._AddCashDenomination
            Session("AllowToViewEmpEDList") = objUserPrivilege._AllowToViewEmpEDList
            Session("AddEarningDeduction") = objUserPrivilege._AddEarningDeduction
            Session("EditEarningDeduction") = objUserPrivilege._EditEarningDeduction
            Session("AllowToVoidBatchPostingToED") = objUserPrivilege._AllowToVoidBatchPostingToED 'Sohail (24 Feb 2016) 

            Session("DeleteEarningDeduction") = objUserPrivilege._DeleteEarningDeduction
            Session("AllowAuthorizePayslipPayment") = objUserPrivilege._AllowAuthorizePayslipPayment
            Session("AllowVoidAuthorizedPayslipPayment") = objUserPrivilege._AllowVoidAuthorizedPayslipPayment

            Session("AllowToApproveEarningDeduction") = objUserPrivilege._AllowToApproveEarningDeduction
            Session("AllowToViewSalaryChangeList") = objUserPrivilege._AllowToViewSalaryChangeList
            Session("AllowToApproveSalaryChange") = objUserPrivilege._AllowToApproveSalaryChange

            'SHANI (17 APR 2015)-START
            'Payroll Master
            Session("AllowToViewPayrollGroupList") = objUserPrivilege._AllowToViewPayrollGroupList
            Session("AddPayrollGroup") = objUserPrivilege._AddPayrollGroup
            Session("EditPayrollGroup") = objUserPrivilege._EditPayrollGroup
            Session("DeletePayrollGroup") = objUserPrivilege._DeletePayrollGroup

            Session("AllowToViewPayPointList") = objUserPrivilege._AllowToViewPayPointList
            Session("AddPayPoint") = objUserPrivilege._AddPayPoint
            Session("EditPayPoint") = objUserPrivilege._EditPayPoint
            Session("DeletePayPoint") = objUserPrivilege._DeletePayPoint

            Session("AllowToViewBankBranchList") = objUserPrivilege._AllowToViewBankBranchList
            Session("AddBankBranch") = objUserPrivilege._AddBankBranch
            Session("EditBankBranch") = objUserPrivilege._EditBankBranch
            Session("DeleteBankBranch") = objUserPrivilege._DeleteBankBranch

            'SHANI (17 APR 2015)--END 
            'Sohail (29 Aug 2019) -- Start
            'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
            Session("AllowToPostJVOnHolidays") = objUserPrivilege._AllowToPostJVOnHolidays
            'Sohail (29 Aug 2019) -- End

            '*** Loan & Advance
            Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
            Session("AllowtoApproveAdvance") = objUserPrivilege._AllowtoApproveAdvance
            Session("AddPendingLoan") = objUserPrivilege._AddPendingLoan
            Session("EditPendingLoan") = objUserPrivilege._EditPendingLoan
            Session("AllowToViewProcessLoanAdvanceList") = objUserPrivilege._AllowToViewProcessLoanAdvanceList
            Session("AllowToViewProceedingApprovalList") = objUserPrivilege._AllowToViewProceedingApprovalList
            Session("AllowToViewLoanAdvanceList") = objUserPrivilege._AllowToViewLoanAdvanceList
            Session("DeleteLoanAdvance") = objUserPrivilege._DeleteLoanAdvance
            Session("EditLoanAdvance") = objUserPrivilege._EditLoanAdvance
            Session("AllowChangeLoanAvanceStatus") = objUserPrivilege._AllowChangeLoanAvanceStatus
            Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
            Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
            Session("Is_Report_Assigned") = clsArutiReportClass.Is_Report_Assigned(enArutiReport.BBL_Loan_Report, Session("UserId"), Session("CompanyUnkId"))
            Session("DeletePendingLoan") = objUserPrivilege._DeletePendingLoan
            Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
            Session("AllowChangePendingLoanAdvanceStatus") = objUserPrivilege._AllowChangePendingLoanAdvanceStatus
            Session("AddLoanAdvancePayment") = objUserPrivilege._AddLoanAdvancePayment
            Session("EditLoanAdvancePayment") = objUserPrivilege._EditLoanAdvancePayment
            Session("DeleteLoanAdvancePayment") = objUserPrivilege._DeleteLoanAdvancePayment
            Session("AddLoanAdvanceReceived") = objUserPrivilege._AddLoanAdvanceReceived
            Session("EditLoanAdvanceReceived") = objUserPrivilege._EditLoanAdvanceReceived
            Session("DeleteLoanAdvanceReceived") = objUserPrivilege._DeleteLoanAdvanceReceived
            Session("AllowtoApproveLoan") = objUserPrivilege._AllowtoApproveLoan
            Session("AllowAssignPendingLoan") = objUserPrivilege._AllowAssignPendingLoan
            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            Session("AllowToCancelApprovedLoanApp") = objUserPrivilege._AllowToCancelApprovedLoanApp
            'Nilay (20-Sept-2016) -- End

            '*** Savings
            Session("AddSavingsPayment") = objUserPrivilege._AddSavingsPayment
            Session("EditSavingsPayment") = objUserPrivilege._EditSavingsPayment
            Session("DeleteSavingsPayment") = objUserPrivilege._DeleteSavingsPayment
            Session("AllowToViewEmployeeSavingsList") = objUserPrivilege._AllowToViewEmployeeSavingsList
            Session("AddEmployeeSavings") = objUserPrivilege._AddEmployeeSavings
            Session("EditEmployeeSavings") = objUserPrivilege._EditEmployeeSavings
            Session("DeleteEmployeeSavings") = objUserPrivilege._DeleteEmployeeSavings
            Session("AllowChangeSavingStatus") = objUserPrivilege._AllowChangeSavingStatus

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            Session("AddSavingsDeposit") = objUserPrivilege._AddSavingsDeposit
            Session("EditSavingsDeposit") = objUserPrivilege._EditSavingsDeposit
            Session("DeleteSavingsDeposit") = objUserPrivilege._DeleteSavingsDeposit
            'Shani(18-Dec-2015) -- End

            '*** HR
            Session("AddDependant") = objUserPrivilege._AddDependant
            Session("EditDependant") = objUserPrivilege._EditDependant
            Session("DeleteDependant") = objUserPrivilege._DeleteDependant
            Session("ViewDependant") = objUserPrivilege._AllowToViewEmpDependantsList
            Session("AddEmployeeAssets") = objUserPrivilege._AddEmployeeAssets
            Session("EditEmployeeAssets") = objUserPrivilege._EditEmployeeAssets
            Session("DeleteEmployeeAssets") = objUserPrivilege._DeleteEmployeeAssets
            Session("ViewCompanyAssetList") = objUserPrivilege._AllowToViewCompanyAssetList
            Session("AddEmployeeSkill") = objUserPrivilege._AddEmployeeSkill
            Session("EditEmployeeSkill") = objUserPrivilege._EditEmployeeSkill
            Session("DeleteEmployeeSkill") = objUserPrivilege._DeleteEmployeeSkill
            Session("ViewEmpSkillList") = objUserPrivilege._AllowToViewEmpSkillList
            Session("AddEmployeeQualification") = objUserPrivilege._AddEmployeeQualification
            Session("EditEmployeeQualification") = objUserPrivilege._EditEmployeeQualification
            Session("DeleteEmployeeQualification") = objUserPrivilege._DeleteEmployeeQualification
            Session("ViewEmpQualificationList") = objUserPrivilege._AllowToViewEmpQualificationList
            Session("AddEmployeeReferee") = objUserPrivilege._AddEmployeeReferee
            Session("EditEmployeeReferee") = objUserPrivilege._EditEmployeeReferee
            Session("DeleteEmployeeReferee") = objUserPrivilege._DeleteEmployeeReferee
            Session("ViewEmpReferenceList") = objUserPrivilege._AllowToViewEmpReferenceList
            Session("AddEmployeeExperience") = objUserPrivilege._AddEmployeeExperience
            Session("EditEmployeeExperience") = objUserPrivilege._EditEmployeeExperience
            Session("DeleteEmployeeExperience") = objUserPrivilege._DeleteEmployeeExperience
            Session("ViewEmpExperienceList") = objUserPrivilege._AllowToViewEmpExperienceList
            Session("AddEmployee") = objUserPrivilege._AddEmployee
            Session("EditEmployee") = objUserPrivilege._EditEmployee
            Session("DeleteEmployee") = objUserPrivilege._DeleteEmployee
            Session("ViewEmployee") = objUserPrivilege._AllowToViewEmpList
            Session("SetReinstatementdate") = objUserPrivilege._AllowtoSetEmpReinstatementDate
            Session("ChangeConfirmationDate") = objUserPrivilege._AllowtoChangeConfirmationDate
            Session("ChangeAppointmentDate") = objUserPrivilege._AllowtoChangeAppointmentDate
            Session("SetEmployeeBirthDate") = objUserPrivilege._AllowtoSetEmployeeBirthDate
            Session("SetEmpSuspensionDate") = objUserPrivilege._AllowtoSetEmpSuspensionDate
            Session("SetEmpProbationDate") = objUserPrivilege._AllowtoSetEmpProbationDate
            Session("SetEmploymentEndDate") = objUserPrivilege._AllowtoSetEmploymentEndDate
            Session("SetLeavingDate") = objUserPrivilege._AllowtoSetLeavingDate
            Session("ChangeRetirementDate") = objUserPrivilege._AllowtoChangeRetirementDate
            Session("ViewScale") = objUserPrivilege._AllowTo_View_Scale
            Session("ViewBenefitList") = objUserPrivilege._AllowToViewEmpBenefitList
            Session("AllowtoChangeBranch") = objUserPrivilege._AllowtoChangeBranch
            Session("AllowtoChangeDepartmentGroup") = objUserPrivilege._AllowtoChangeDepartmentGroup
            Session("AllowtoChangeDepartment") = objUserPrivilege._AllowtoChangeDepartment
            Session("AllowtoChangeSectionGroup") = objUserPrivilege._AllowtoChangeSectionGroup
            Session("AllowtoChangeSection") = objUserPrivilege._AllowtoChangeSection
            Session("AllowtoChangeUnitGroup") = objUserPrivilege._AllowtoChangeUnitGroup
            Session("AllowtoChangeUnit") = objUserPrivilege._AllowtoChangeUnit
            Session("AllowtoChangeTeam") = objUserPrivilege._AllowtoChangeTeam
            Session("AllowtoChangeJobGroup") = objUserPrivilege._AllowtoChangeJobGroup
            Session("AllowtoChangeJob") = objUserPrivilege._AllowtoChangeJob
            Session("AllowtoChangeGradeGroup") = objUserPrivilege._AllowtoChangeGradeGroup
            Session("AllowtoChangeGrade") = objUserPrivilege._AllowtoChangeGrade
            Session("AllowtoChangeGradeLevel") = objUserPrivilege._AllowtoChangeGradeLevel
            Session("AllowtoChangeClassGroup") = objUserPrivilege._AllowtoChangeClassGroup
            Session("AllowtoChangeClass") = objUserPrivilege._AllowtoChangeClass
            Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
            Session("AllowtoChangeCompanyEmail") = objUserPrivilege._AllowtoChangeCompanyEmail
            Session("AllowToAddEditPhoto") = objUserPrivilege._AllowToAddEditPhoto
            Session("AllowToDeletePhoto") = objUserPrivilege._AllowToDeletePhoto
            Session("AllowToViewScale") = objUserPrivilege._AllowTo_View_Scale
            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : show employee end date or eoc / leaving date which is less on Payslips.
            Session("AllowToChangeEOCLeavingDateOnClosedPeriod") = objUserPrivilege._AllowToChangeEOCLeavingDateOnClosedPeriod
            'Sohail (09 Oct 2019) -- End
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
            Session("AllowToPostFlexcubeJVToOracle") = objUserPrivilege._AllowToPostFlexcubeJVToOracle
            'Sohail (16 Oct 2019) -- End
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            Session("AllowToViewPaidAmount") = objUserPrivilege._AllowToViewPaidAmount
            'Sohail (16 Oct 2019) -- End
            'Sohail (18 Oct 2019) -- Start
            'NMB Enhancement # : For Add/New/Edit employee system should allow to set appointment date to last closed period for It should be possible to set appointment date of an employee to past closed period by use of the first appointment date but during payroll process, consider 1st of the following month
            Session("AllowToChangeAppointmentDateOnClosedPeriod") = objUserPrivilege._AllowToChangeAppointmentDateOnClosedPeriod
            'Sohail (18 Oct 2019) -- End

            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            Session("AllowToChangeEmpTransfers") = objUserPrivilege._AllowToChangeEmpTransfers
            Session("AllowToChangeEmpRecategorize") = objUserPrivilege._AllowToChangeEmpRecategorize
            Session("AllowToChangeEmpWorkPermit") = objUserPrivilege._AllowToChangeEmpWorkPermit
            'Shani (08-Dec-2016) -- End


            'Nilay (07-Feb-2016) -- Start
            'Session("FirstNamethenSurname") = ConfigParameter._Object._FirstNamethenSurname
            'Nilay (07-Feb-2016) -- End

            '*** Leave
            Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
            Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
            Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense
            Session("AddLeaveForm") = objUserPrivilege._AddLeaveForm
            Session("EditLeaveForm") = objUserPrivilege._EditLeaveForm
            Session("DeleteLeaveForm") = objUserPrivilege._DeleteLeaveForm
            Session("ViewLeaveFormList") = objUserPrivilege._AllowToViewLeaveFormList
            Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
            Session("ViewLeaveProcessList") = objUserPrivilege._AllowToViewLeaveProcessList
            Session("ChangeLeaveFormStatus") = objUserPrivilege._AllowChangeLeaveFormStatus
            Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance
            Session("AddPlanLeave") = objUserPrivilege._AddPlanLeave
            Session("EditPlanLeave") = objUserPrivilege._EditPlanLeave
            Session("DeletePlanLeave") = objUserPrivilege._DeletePlanLeave
            Session("ViewLeavePlannerList") = objUserPrivilege._AllowToViewLeavePlannerList
            Session("ViewPlannedLeaveViewer") = objUserPrivilege._AllowtoViewPlannedLeaveViewer
            Session("ViewLeaveViewer") = objUserPrivilege._AllowToViewLeaveViewer
            Session("AllowToCancelLeave") = objUserPrivilege._AllowToCancelLeave
            Session("AllowToCancelPreviousDateLeave") = objUserPrivilege._AllowToCancelPreviousDateLeave
            Session("AllowtoMigrateLeaveApprovers") = objUserPrivilege._AllowtoMigrateLeaveApprover
            Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave
            Session("AllowToViewLeaveApproverList") = objUserPrivilege._AllowToViewLeaveApproverList
            Session("AddLeaveApprover") = objUserPrivilege._AddLeaveApprover
            Session("EditLeaveApprover") = objUserPrivilege._EditLeaveApprover
            Session("DeleteLeaveApprover") = objUserPrivilege._DeleteLeaveApprover
            Session("AllowToAssignIssueUsertoEmp") = objUserPrivilege._AllowToAssignIssueUsertoEmp
            Session("AllowToMigrateIssueUser") = objUserPrivilege._AllowToMigrateIssueUser
            Session("AllowtoEndELC") = objUserPrivilege._AllowToEndLeaveCycle
            Session("AllowMapApproverWithUser") = objUserPrivilege._AllowMapApproverWithUser
            Session("AllowtoMapLeaveType") = objUserPrivilege._AllowtoMapLeaveType
            Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave
            Session("AllowPrintLeaveForm") = objUserPrivilege._AllowPrintLeaveForm
            Session("AllowPreviewLeaveForm") = objUserPrivilege._AllowPreviewLeaveForm

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            Session("AllowSetleaveActiveApprover") = objUserPrivilege._AllowtoSetLeaveActiveApprover
            Session("AllowSetleaveInactiveApprover") = objUserPrivilege._AllowtoSetLeaveInactiveApprover
            'Shani(17-Aug-2015) -- End


            '*** Time & Attendance
            Session("AllowToAddEditDeleteGlobalTimesheet") = objUserPrivilege._AllowToAddEditDeleteGlobalTimesheet

            '*** Medical
            Session("AddMedicalClaim") = objUserPrivilege._AddMedicalClaim
            Session("EditMedicalClaim") = objUserPrivilege._EditMedicalClaim
            Session("DeleteMedicalClaim") = objUserPrivilege._DeleteMedicalClaim
            Session("AllowToExportMedicalClaim") = objUserPrivilege._AllowToExportMedicalClaim
            Session("AllowToCancelExportedMedicalClaim") = objUserPrivilege._AllowToCancelExportedMedicalClaim
            Session("AllowToAddMedicalSickSheet") = objUserPrivilege._AllowToAddMedicalSickSheet
            Session("AllowToEditMedicalSickSheet") = objUserPrivilege._AllowToEditMedicalSickSheet
            Session("AllowToDeleteMedicalSickSheet") = objUserPrivilege._AllowToDeleteMedicalSickSheet
            Session("AllowToPrintMedicalSickSheet") = objUserPrivilege._AllowToPrintMedicalSickSheet
            Session("RevokeUserAccessOnSicksheet") = objUserPrivilege._RevokeUserAccessOnSicksheet
            Session("AllowToViewEmpSickSheetList") = objUserPrivilege._AllowToViewEmpSickSheetList
            Session("AllowToViewMedicalClaimList") = objUserPrivilege._AllowToViewMedicalClaimList
            Session("AllowToSaveMedicalClaim") = objUserPrivilege._AllowToSaveMedicalClaim
            Session("AllowToFinalSaveMedicalClaim") = objUserPrivilege._AllowToFinalSaveMedicalClaim

            '*** Assessment

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._Allow_UnlockFinalSaveBSCPlanning
            'Session("EditEmployeeAssessment") = objUserPrivilege._EditEmployeeAssessment
            'Session("DeleteEmployeeAssessment") = objUserPrivilege._DeleteEmployeeAssessment
            'Session("Allow_UnlockCommittedGeneralAssessment") = objUserPrivilege._Allow_UnlockCommittedGeneralAssessment
            'Session("AllowToViewSelfAssessmentList") = objUserPrivilege._AllowToViewSelfAssessmentList
            'Session("AddAssessmentAnalysis") = objUserPrivilege._AddAssessmentAnalysis
            'Session("EditAssessmentAnalysis") = objUserPrivilege._EditAssessmentAnalysis
            'Session("DeleteAssessmentAnalysis") = objUserPrivilege._DeleteAssessmentAnalysis
            'Session("AllowToViewAssessorAssessmentList") = objUserPrivilege._AllowToViewAssessorAssessmentList
            'Session("AllowToViewReviewerAssessmentList") = objUserPrivilege._AllowToViewReviewerAssessmentList
            'Session("AllowToAddReviewerGeneralAssessment") = objUserPrivilege._AllowToAddReviewerGeneralAssessment
            'Session("AllowToEditReviewerGeneralAssessment") = objUserPrivilege._AllowToEditReviewerGeneralAssessment
            'Session("AllowToDeleteReviewerGeneralAssessment") = objUserPrivilege._AllowToDeleteReviewerGeneralAssessment
            'Session("Allow_UnlockCommittedBSCAssessment") = objUserPrivilege._Allow_UnlockCommittedBSCAssessment
            'Session("AllowToEditSelfBSCAssessment") = objUserPrivilege._AllowToEditSelfBSCAssessment
            'Session("AllowToDeleteSelfBSCAssessment") = objUserPrivilege._AllowToDeleteSelfBSCAssessment
            'Session("AllowToViewSelfAssessedBSCList") = objUserPrivilege._AllowToViewSelfAssessedBSCList
            'Session("AllowToAddAssessorBSCAssessment") = objUserPrivilege._AllowToAddAssessorBSCAssessment
            'Session("AllowToEditAssessorBSCAssessment") = objUserPrivilege._AllowToEditAssessorBSCAssessment
            'Session("AllowToDeleteAssessorBSCAssessment") = objUserPrivilege._AllowToDeleteAssessorBSCAssessment
            'Session("AllowToViewAssessorAssessedBSCList") = objUserPrivilege._AllowToViewAssessorAssessedBSCList
            'Session("AllowToAddReviewerBSCAssessment") = objUserPrivilege._AllowToAddReviewerBSCAssessment
            'Session("AllowToEditReviewerBSCAssessment") = objUserPrivilege._AllowToEditReviewerBSCAssessment
            'Session("AllowToDeleteReviewerBSCAssessment") = objUserPrivilege._AllowToDeleteReviewerBSCAssessment
            'Session("AllowToViewReviewerAssessedBSCList") = objUserPrivilege._AllowToViewReviewerAssessedBSCList        
            'Session("Allow_FinalSaveBSCPlanning") = objUserPrivilege._Allow_FinalSaveBSCPlanning
            'Session("AllowToMigrateAssessor_Reviewer") = objUserPrivilege._AllowToMigrateAssessor_Reviewer

            Session("AllowtoSubmitGoalsforApproval") = objUserPrivilege._AllowtoSubmitGoalsforApproval
            Session("AllowtoApproveRejectGoalsPlanning") = objUserPrivilege._AllowtoApproveRejectGoalsPlanning
            Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._AllowtoUnlockFinalSavedGoals
            Session("AllowtoPerformAssessorReviewerMigration") = objUserPrivilege._AllowtoPerformAssessorReviewerMigration
            Session("AllowtoAddSelfEvaluation") = objUserPrivilege._AllowtoAddSelfEvaluation
            Session("AllowtoEditSelfEvaluation") = objUserPrivilege._AllowtoEditSelfEvaluation
            Session("AllowtoDeleteSelfEvaluation") = objUserPrivilege._AllowtoDeleteSelfEvaluation
            Session("AllowtoUnlockcommittedSelfEvaluation") = objUserPrivilege._AllowtoUnlockcommittedSelfEvaluation
            Session("AllowtoViewSelfEvaluationList") = objUserPrivilege._AllowtoViewSelfEvaluationList
            Session("AllowtoAddAssessorEvaluation") = objUserPrivilege._AllowtoAddAssessorEvaluation
            Session("AllowtoEditAssessorEvaluation") = objUserPrivilege._AllowtoEditAssessorEvaluation
            Session("AllowtoDeleteAssessorEvaluation") = objUserPrivilege._AllowtoDeleteAssessorEvaluation
            Session("AllowtoUnlockcommittedAssessorEvaluation") = objUserPrivilege._AllowtoUnlockcommittedAssessorEvaluation
            Session("AllowtoViewAssessorEvaluationList") = objUserPrivilege._AllowtoViewAssessorEvaluationList
            Session("AllowtoAddReviewerEvaluation") = objUserPrivilege._AllowtoAddReviewerEvaluation
            Session("AllowtoEditReviewerEvaluation") = objUserPrivilege._AllowtoEditReviewerEvaluation
            Session("AllowtoDeleteReviewerEvaluation") = objUserPrivilege._AllowtoDeleteReviewerEvaluation
            Session("AllowtoUnlockcommittedReviewerEvaluation") = objUserPrivilege._AllowtoUnlockcommittedReviewerEvaluation
            Session("AllowtoViewReviewerEvaluationList") = objUserPrivilege._AllowtoViewReviewerEvaluationList
            Session("AllowtoAddAllocationGoals") = objUserPrivilege._AllowtoAddAllocationGoals
            Session("AllowtoEditAllocationGoals") = objUserPrivilege._AllowtoEditAllocationGoals
            Session("AllowtoDeleteAllocationGoals") = objUserPrivilege._AllowtoDeleteAllocationGoals
            Session("AllowtoCommitAllocationGoals") = objUserPrivilege._AllowtoCommitAllocationGoals
            Session("AllowtoUnlockcommittedAllocationGoals") = objUserPrivilege._AllowtoUnlockcommittedAllocationGoals
            Session("AllowtoPerformGlobalAssignAllocationGoals") = objUserPrivilege._AllowtoPerformGlobalAssignAllocationGoals
            Session("AllowtoUpdatePercentCompletedAllocationGoals") = objUserPrivilege._AllowtoUpdatePercentCompletedAllocationGoals
            Session("AllowtoViewAllocationGoalsList") = objUserPrivilege._AllowtoViewAllocationGoalsList
            Session("AllowtoAddEmployeeGoals") = objUserPrivilege._AllowtoAddEmployeeGoals
            Session("AllowtoEditEmployeeGoals") = objUserPrivilege._AllowtoEditEmployeeGoals
            Session("AllowtoDeleteEmployeeGoals") = objUserPrivilege._AllowtoDeleteEmployeeGoals
            Session("AllowtoPerformGlobalAssignEmployeeGoals") = objUserPrivilege._AllowtoPerformGlobalAssignEmployeeGoals
            Session("AllowtoUpdatePercentCompletedEmployeeGoals") = objUserPrivilege._AllowtoUpdatePercentCompletedEmployeeGoals
            Session("AllowtoViewEmployeeGoalsList") = objUserPrivilege._AllowtoViewEmployeeGoalsList
            'S.SANDEEP [28 MAY 2015] -- END

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By CCBRT
            Session("AllowtoViewPerformanceEvaluation") = objUserPrivilege._AllowtoViewPerformanceEvaluation
            Session("AllowtoViewCompanyGoalsList") = objUserPrivilege._AllowtoViewCompanyGoalsList
            Session("AllowtoAddAssignedCompetencies") = objUserPrivilege._AllowtoAddAssignedCompetencies
            Session("AllowtoEditAssignedCompetencies") = objUserPrivilege._AllowtoEditAssignedCompetencies
            Session("AllowtoDeleteAssignedCompetencies") = objUserPrivilege._AllowtoDeleteAssignedCompetencies
            Session("AllowtoAddCustomItems") = objUserPrivilege._AllowtoAddCustomItems
            Session("AllowtoEditCustomItems") = objUserPrivilege._AllowtoEditCustomItems
            Session("AllowtoDeleteCustomItems") = objUserPrivilege._AllowtoDeleteCustomItems
            'Shani(06-Feb-2016) -- End

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            Session("AllowToApproveGoalsAccomplishment") = objUserPrivilege._AllowToApproveGoalsAccomplishment
            'Shani (26-Sep-2016) -- End


            

            '*** Recruitment
            Session("AllowToApproveEmployee") = objUserPrivilege._AllowToApproveEmployee
            Session("AllowtoApproveApplicantEligibility") = objUserPrivilege._AllowtoApproveApplicantEligibility
            Session("AllowtoDisapproveApplicantEligibility") = objUserPrivilege._AllowtoDisapproveApplicantEligibility
            Session("AllowtoViewInterviewAnalysisList") = objUserPrivilege._AllowtoViewInterviewAnalysisList
            Session("AllowToViewShortListApplicants") = objUserPrivilege._AllowToViewShortListApplicants
            Session("AllowToApproveApplicantFilter") = objUserPrivilege._AllowToApproveApplicantFilter
            Session("AllowToRejectApplicantFilter") = objUserPrivilege._AllowToRejectApplicantFilter
            Session("AllowToApproveFinalShortListedApplicant") = objUserPrivilege._AllowToApproveFinalShortListedApplicant
            Session("AllowToDisapproveFinalShortListedApplicant") = objUserPrivilege._AllowToDisapproveFinalShortListedApplicant
            Session("AllowToViewStaffRequisitionList") = objUserPrivilege._AllowToViewStaffRequisitionList
            Session("AllowToAddStaffRequisition") = objUserPrivilege._AllowToAddStaffRequisition
            Session("AllowToEditStaffRequisition") = objUserPrivilege._AllowToEditStaffRequisition
            Session("AllowToDeleteStaffRequisition") = objUserPrivilege._AllowToDeleteStaffRequisition
            Session("AllowToViewStaffRequisitionApprovals") = objUserPrivilege._AllowToViewStaffRequisitionApprovals
            Session("AllowToCancelStaffRequisition") = objUserPrivilege._AllowToCancelStaffRequisition
            Session("AllowToApproveStaffRequisition") = objUserPrivilege._AllowToApproveStaffRequisition


            '*** TRANING

            Session("AllowToViewLevel1EvaluationList") = objUserPrivilege._AllowToViewLevel1EvaluationList
            Session("AllowToAddLevelIEvaluation") = objUserPrivilege._AllowToAddLevelIEvaluation
            Session("AllowToEditLevelIEvaluation") = objUserPrivilege._AllowToEditLevelIEvaluation
            Session("AllowToDeleteLevelIEvaluation") = objUserPrivilege._AllowToDeleteLevelIEvaluation
            Session("AllowToSave_CompleteLevelIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIEvaluation
            Session("AllowToPrintLevelIEvaluation") = objUserPrivilege._AllowToPrintLevelIEvaluation
            Session("AllowToPreviewLevelIEvaluation") = objUserPrivilege._AllowToPreviewLevelIEvaluation
            Session("AllowToViewTrainingEnrollmentList") = objUserPrivilege._AllowToViewTrainingEnrollmentList
            Session("AddTrainingEnrollment") = objUserPrivilege._AddTrainingEnrollment
            Session("EditTrainingEnrollment") = objUserPrivilege._EditTrainingEnrollment
            Session("DeleteTrainingEnrollment") = objUserPrivilege._DeleteTrainingEnrollment
            Session("AllowToAddLevelIIIEvaluation") = objUserPrivilege._AllowToAddLevelIIIEvaluation
            Session("AllowToEditLevelIIIEvaluation") = objUserPrivilege._AllowToEditLevelIIIEvaluation
            Session("AllowToDeleteLevelIIIEvaluation") = objUserPrivilege._AllowToDeleteLevelIIIEvaluation
            Session("AllowToSave_CompleteLevelIIIEvaluation") = objUserPrivilege._AllowToSave_CompleteLevelIIIEvaluation
            Session("AllowToViewLevel3EvaluationList") = objUserPrivilege._AllowToViewLevel3EvaluationList

            '*** ASSET DECLARATION

            Session("AddAssetDeclaration") = objUserPrivilege._Allow_AddAssetDeclaration
            Session("EditAssetDeclaration") = objUserPrivilege._Allow_EditAssetDeclaration
            Session("DeleteAssetDeclaration") = objUserPrivilege._Allow_DeleteAssetDeclaration
            Session("FinalSaveAssetDeclaration") = objUserPrivilege._Allow_FinalSaveAssetDeclaration
            Session("UnlockFinalSaveAssetDeclaration") = objUserPrivilege._Allow_UnlockFinalSaveAssetDeclaration
            Session("ViewAssetsDeclarationList") = objUserPrivilege._AllowToViewAssetsDeclarationList

            'Pinkal (03-Dec-2018) -- Start
            'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
            Session("AllowtoUnlockEmployeeForAssetDeclaration") = objUserPrivilege._AllowtoUnlockEmployeeForAssetDeclaration
            'Pinkal (03-Dec-2018) -- end
            'Sohail (04 Feb 2020) -- Start
            'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
            Session("AllowtoUnlockEmployeeForNonDisclosureDeclaration") = objUserPrivilege._AllowtoUnlockEmployeeForNonDisclosureDeclaration
            'Sohail (04 Feb 2020) -- End

            '*** CLAIM &  REQUEST
            Session("AllowtoAddExpenseApprover") = objUserPrivilege._AllowtoAddExpenseApprover
            Session("AllowtoEditExpenseApprover") = objUserPrivilege._AllowtoEditExpenseApprover
            Session("AllowtoDeleteExpenseApprover") = objUserPrivilege._AllowtoDeleteExpenseApprover
            Session("AllowtoViewExpenseApproverList") = objUserPrivilege._AllowtoViewExpenseApproverList
            Session("AllowtoMigrateExpenseApprover") = objUserPrivilege._AllowtoMigrateExpenseApprover
            Session("AllowtoSwapExpenseApprover") = objUserPrivilege._AllowtoSwapExpenseApprover
            Session("AllowtoAddClaimExpenseForm") = objUserPrivilege._AllowtoAddClaimExpenseForm
            Session("AllowtoEditClaimExpenseForm") = objUserPrivilege._AllowtoEditClaimExpenseForm
            Session("AllowtoDeleteClaimExpenseForm") = objUserPrivilege._AllowtoDeleteClaimExpenseForm
            Session("AllowtoViewClaimExpenseFormList ") = objUserPrivilege._AllowtoViewClaimExpenseFormList
            Session("AllowtoCancelClaimExpenseForm") = objUserPrivilege._AllowtoCancelClaimExpenseForm
            Session("AllowtoProcessClaimExpenseForm") = objUserPrivilege._AllowtoProcessClaimExpenseForm
            Session("AllowtoViewProcessClaimExpenseFormList") = objUserPrivilege._AllowtoViewProcessClaimExpenseFormList
            Session("AllowtoPostClaimExpenseToPayroll") = objUserPrivilege._AllowtoPostClaimExpenseToPayroll
            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            Session("AllowSetClaimActiveApprover") = objUserPrivilege._AllowtoSetClaimActiveApprover
            Session("AllowSetClaimInactiveApprover") = objUserPrivilege._AllowtoSetClaimInactiveApprover
            'Shani(17-Aug-2015) -- End

            'S.SANDEEP [23 FEB 2016] -- START
            Session("AllowtoChangePreassignedCompetencies") = objUserPrivilege._AllowtoChangePreassignedCompetencies
            'S.SANDEEP [23 FEB 2016] -- END


            'Pinkal (21-Dec-2016) -- Start
            'Enhancement - Adding Swap Approver Privilage for Leave Module.
            Session("AllowToSwapLeaveApprover") = objUserPrivilege._AllowToSwapLeaveApprover
            'Pinkal (21-Dec-2016) -- End



            'Pinkal (03-May-2017) -- Start
            'Enhancement - Working On Implementing privileges in Budget Timesheet Module.
            Session("AllowToMigrateBudgetTimesheetApprover") = objUserPrivilege._AllowToMigrateBudgetTimesheetApprover
            Session("AllowToMapBudgetTimesheetApprover") = objUserPrivilege._AllowToMapBudgetTimesheetApprover
            Session("AllowToViewBudgetTimesheetApproverList") = objUserPrivilege._AllowToViewBudgetTimesheetApproverList
            Session("AllowToAddBudgetTimesheetApprover") = objUserPrivilege._AllowToAddBudgetTimesheetApprover
            Session("AllowToEditBudgetTimesheetApprover") = objUserPrivilege._AllowToEditBudgetTimesheetApprover
            Session("AllowToDeleteBudgetTimesheetApprover") = objUserPrivilege._AllowToDeleteBudgetTimesheetApprover
            Session("AllowToSetBudgetTimesheetApproverAsActive") = objUserPrivilege._AllowToSetBudgetTimesheetApproverAsActive
            Session("AllowToSetBudgetTimesheetApproverAsInActive") = objUserPrivilege._AllowToSetBudgetTimesheetApproverAsInActive
            Session("AllowToViewEmployeeBudgetTimesheetList") = objUserPrivilege._AllowToViewEmployeeBudgetTimesheetList
            Session("AllowToAddEmployeeBudgetTimesheet") = objUserPrivilege._AllowToAddEmployeeBudgetTimesheet
            Session("AllowToEditEmployeeBudgetTimesheet") = objUserPrivilege._AllowToEditEmployeeBudgetTimesheet
            Session("AllowToDeleteEmployeeBudgetTimesheet") = objUserPrivilege._AllowToDeleteEmployeeBudgetTimesheet
            Session("AllowToCancelEmployeeBudgetTimesheet") = objUserPrivilege._AllowToCancelEmployeeBudgetTimesheet
            Session("AllowToViewPendingEmpBudgetTimesheetSubmitForApproval") = objUserPrivilege._AllowToViewPendingEmpBudgetTimesheetSubmitForApproval
            Session("AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval") = objUserPrivilege._AllowToViewCompletedEmpBudgetTimesheetSubmitForApproval
            Session("AllowToSubmitForApprovalForPendingEmpBudgetTimesheet") = objUserPrivilege._AllowToSubmitForApprovalForPendingEmpBudgetTimesheet
            Session("AllowToViewEmployeeBudgetTimesheetApprovalList") = objUserPrivilege._AllowToViewEmployeeBudgetTimesheetApprovalList
            Session("AllowToChangeEmployeeBudgetTimesheetStatus") = objUserPrivilege._AllowToChangeEmployeeBudgetTimesheetStatus
            'Pinkal (03-May-2017) -- End




            'Nilay (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            'Dim culture As CultureInfo = CultureInfo.CreateSpecificCulture(HttpContext.Current.Request.UserLanguages(0))
            'System.Threading.Thread.CurrentThread.CurrentUICulture = culture
            'System.Threading.Thread.CurrentThread.CurrentCulture = culture
            'Session("DateFormat") = culture.DateTimeFormat.ShortDatePattern.ToString
            'Nilay (16-Apr-2016) -- End

            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Session("AllowToAddLoanApprover") = objUserPrivilege._AllowToAddLoanApprover
            Session("AllowToEditLoanApprover") = objUserPrivilege._AllowToEditLoanApprover
            Session("AllowToDeleteLoanApprover") = objUserPrivilege._AllowToDeleteLoanApprover
            Session("AllowToSetActiveInactiveLoanApprover") = objUserPrivilege._AllowToSetActiveInactiveLoanApprover
            Session("AllowToViewLoanApproverLevelList") = objUserPrivilege._AllowToViewLoanApproverLevelList
            Session("AllowToViewLoanApproverList") = objUserPrivilege._AllowToViewLoanApproverList
            Session("AllowToProcessGlobalLoanApprove") = objUserPrivilege._AllowToProcessGlobalLoanApprove
            Session("AllowToProcessGlobalLoanAssign") = objUserPrivilege._AllowToProcessGlobalLoanAssign
            Session("AllowToViewLoanApprovalList") = objUserPrivilege._AllowToViewLoanApprovalList
            Session("AllowToChangeLoanStatus") = objUserPrivilege._AllowToChangeLoanStatus
            Session("AllowToTransferLoanApprover") = objUserPrivilege._AllowToTransferLoanApprover
            Session("AllowToSwapLoanApprover") = objUserPrivilege._AllowToSwapLoanApprover

            

            Session("AllowToEditTransferEmployeeDetails") = objUserPrivilege._AllowToEditTransferEmployeeDetails
            Session("AllowToDeleteTransferEmployeeDetails") = objUserPrivilege._AllowToDeleteTransferEmployeeDetails

            Session("AllowToEditRecategorizeEmployeeDetails") = objUserPrivilege._AllowToEditRecategorizeEmployeeDetails
            Session("AllowToDeleteRecategorizeEmployeeDetails") = objUserPrivilege._AllowToDeleteRecategorizeEmployeeDetails

            Session("AllowToEditProbationEmployeeDetails") = objUserPrivilege._AllowToEditProbationEmployeeDetails
            Session("AllowToDeleteProbationEmployeeDetails") = objUserPrivilege._AllowToDeleteProbationEmployeeDetails

            Session("AllowToEditConfirmationEmployeeDetails") = objUserPrivilege._AllowToEditConfirmationEmployeeDetails
            Session("AllowToDeleteConfirmationEmployeeDetails") = objUserPrivilege._AllowToDeleteConfirmationEmployeeDetails

            Session("AllowToEditSuspensionEmployeeDetails") = objUserPrivilege._AllowToEditSuspensionEmployeeDetails
            Session("AllowToDeleteSuspensionEmployeeDetails") = objUserPrivilege._AllowToDeleteSuspensionEmployeeDetails

            Session("AllowToEditTerminationEmployeeDetails") = objUserPrivilege._AllowToEditTerminationEmployeeDetails
            Session("AllowToDeleteTerminationEmployeeDetails") = objUserPrivilege._AllowToDeleteTerminationEmployeeDetails

            Session("AllowToEditRetiredEmployeeDetails") = objUserPrivilege._AllowToEditRetiredEmployeeDetails
            Session("AllowToDeleteRetiredEmployeeDetails") = objUserPrivilege._AllowToDeleteRetiredEmployeeDetails

            Session("AllowToRehireEmployee") = objUserPrivilege._AllowToRehireEmployee

            Session("AllowToEditWorkPermitEmployeeDetails") = objUserPrivilege._AllowToEditWorkPermitEmployeeDetails
            Session("AllowToDeleteWorkPermitEmployeeDetails") = objUserPrivilege._AllowToDeleteWorkPermitEmployeeDetails

            Session("AllowToEditRehireEmployeeDetails") = objUserPrivilege._AllowToEditRehireEmployeeDetails
            Session("AllowToDeleteRehireEmployeeDetails") = objUserPrivilege._AllowToDeleteRehireEmployeeDetails

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Session("AllowToSetEmployeeExemptionDate") = objUserPrivilege._AllowToSetEmployeeExemptionDate
            Session("AllowToEditEmployeeExemptionDate") = objUserPrivilege._AllowToEditEmployeeExemptionDate
            Session("AllowToDeleteEmployeeExemptionDate") = objUserPrivilege._AllowToDeleteEmployeeExemptionDate
            'Sohail (21 Oct 2019) -- End

            Session("AddSalaryIncrement") = objUserPrivilege._AddSalaryIncrement
            Session("AllowToAssignBenefitGroup") = objUserPrivilege._AllowToAssignBenefitGroup
            Session("AllowToChangeEmpTransfers") = objUserPrivilege._AllowToChangeEmpTransfers
            Session("AllowToChangeEmpRecategorize") = objUserPrivilege._AllowToChangeEmpRecategorize

            Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
            Session("EditEmployeeCostCenter") = objUserPrivilege._EditEmployeeCostCenter
            Session("DeleteEmployeeCostCenter") = objUserPrivilege._DeleteEmployeeCostCenter

            Session("AllowToSubmitForGoalAccomplished") = objUserPrivilege._AllowToSubmitForGoalAccomplished

            Session("AllowToViewAssignedShiftList") = objUserPrivilege._AllowToViewAssignedShiftList
            Session("AllowToViewAssignedPolicyList") = objUserPrivilege._AllowToViewAssignedPolicyList
            Session("AllowToAddShiftPolicyAssignment") = objUserPrivilege._AllowToAddShiftPolicyAssignment
            Session("AllowToDeleteAssignedPolicy") = objUserPrivilege._AllowToDeleteAssignedPolicy
            Session("AllowToDeleteAssignedShift") = objUserPrivilege._AllowToDeleteAssignedShift

            Session("AllowToPerformEligibleOperation") = objUserPrivilege._AllowToPerformEligibleOperation
            Session("AllowToPerformNotEligibleOperation") = objUserPrivilege._AllowToPerformNotEligibleOperation

            Session("AllowToViewFinalApplicantList") = objUserPrivilege._AllowToViewFinalApplicantList

            'Varsha Rana (17-Oct-2017) -- End

            'S.SANDEEP [16-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 118
            Session("AllowtoAddEmployeeSignature") = objUserPrivilege._AllowtoAddEmployeeSignature
            Session("AllowtoDeleteEmployeeSignature") = objUserPrivilege._AllowtoDeleteEmployeeSignature
            Session("AllowtoSeeEmployeeSignature") = objUserPrivilege._AllowtoSeeEmployeeSignature
            'S.SANDEEP [16-Jan-2018] -- END

            'S.SANDEEP [06-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#274}
            Session("AllowToViewResolutionStepList") = objUserPrivilege._AllowToViewGrievanceResolutionStepList
            Session("AllowToAddGrievanceResolutionStep") = objUserPrivilege._AllowToAddGrievanceResolutionStep
            Session("AllowToEditGrievanceResolutionStep") = objUserPrivilege._AllowToEditGrievanceResolutionStep
            Session("AllowToDeleteGrievanceResolutionStep") = objUserPrivilege._AllowToDeleteGrievanceResolutionStep
            'S.SANDEEP [06-SEP-2018] -- END




            'Gajanan [13-AUG-2018] -- Start
            'Enhancement - Implementing Grievance Module.
            Session("AllowToAddGrievanceApproverLevel") = objUserPrivilege._AllowToAddGrievanceApproverLevel
            Session("AllowToEditGrievanceApproverLevel") = objUserPrivilege._AllowToEditGrievanceApproverLevel
            Session("AllowToDeleteGrievanceApproverLevel") = objUserPrivilege._AllowToDeleteGrievanceApproverLevel
            Session("AllowToViewGrievanceApproverLevel") = objUserPrivilege._AllowToViewGrievanceApproverLevel

            Session("AllowToAddGrievanceApprover") = objUserPrivilege._AllowToAddGrievanceApprover
            Session("AllowToEditGrievanceApprover") = objUserPrivilege._AllowToEditGrievanceApprover
            Session("AllowToDeleteGrievanceApprover") = objUserPrivilege._AllowToDeleteGrievanceApprover
            Session("AllowToViewGrievanceApprover") = objUserPrivilege._AllowToViewGrievanceApprover
            Session("AllowToActivateGrievanceApprover") = objUserPrivilege._AllowToActivateGrievanceApprover
            Session("AllowToInActivateGrievanceApprover") = objUserPrivilege._AllowToInActivateGrievanceApprover

            Session("AllowToViewGrievanceResolutionStepList") = objUserPrivilege._AllowToViewGrievanceResolutionStepList
            Session("AllowToAddGrievanceResolutionStep") = objUserPrivilege._AllowToAddGrievanceResolutionStep
            Session("AllowToEditGrievanceResolutionStep") = objUserPrivilege._AllowToEditGrievanceResolutionStep
            Session("AllowToDeleteGrievanceResolutionStep") = objUserPrivilege._AllowToDeleteGrievanceResolutionStep
            'Gajanan(13-AUG-2018) -- End    

            'S.SANDEEP [09-OCT-2018] -- START
            Session("AllowToAddTrainingApproverLevel") = objUserPrivilege._AllowToAddTrainingApproverLevel
            Session("AllowToEditTrainingApproverLevel") = objUserPrivilege._AllowToEditTrainingApproverLevel
            Session("AllowToDeleteTrainingApproverLevel") = objUserPrivilege._AllowToDeleteTrainingApproverLevel
            Session("AllowToViewTrainingApproverLevel") = objUserPrivilege._AllowToViewTrainingApproverLevel
            Session("AllowToAddTrainingApprover") = objUserPrivilege._AllowToAddTrainingApprover
            Session("AllowToDeleteTrainingApprover") = objUserPrivilege._AllowToDeleteTrainingApprover
            Session("AllowToViewTrainingApprover") = objUserPrivilege._AllowToViewTrainingApprover
            Session("AllowToApproveTrainingRequisition") = objUserPrivilege._AllowToApproveTrainingRequisition
            Session("AllowToActivateTrainingApprover") = objUserPrivilege._AllowToActivateTrainingApprover
            Session("AllowToDeactivateTrainingApprover") = objUserPrivilege._AllowToDeactivateTrainingApprover
            'S.SANDEEP [09-OCT-2018] -- END


            '*** START  TNA OT REQUISITION

            'Hemant (22 Oct 2018) -- Start
            'Enhancement : Implementing New Module of OT Requisition Approver Level & Approver Master
            Session("AllowToViewOTRequisitionApproverLevel") = objUserPrivilege._AllowToViewOTRequisitionApproverLevel
            Session("AllowToAddOTRequisitionApproverLevel") = objUserPrivilege._AllowToAddOTRequisitionApproverLevel
            Session("AllowToEditOTRequisitionApproverLevel") = objUserPrivilege._AllowToEditOTRequisitionApproverLevel
            Session("AllowToDeleteOTRequisitionApproverLevel") = objUserPrivilege._AllowToDeleteOTRequisitionApproverLevel
            Session("AllowToAddOTRequisitionApprover") = objUserPrivilege._AllowToAddOTRequisitionApprover
            Session("AllowToEditOTRequisitionApprover") = objUserPrivilege._AllowToEditOTRequisitionApprover
            Session("AllowToDeleteOTRequisitionApprover") = objUserPrivilege._AllowToDeleteOTRequisitionApprover
            Session("AllowToViewOTRequisitionApprover") = objUserPrivilege._AllowToViewOTRequisitionApprover
            'Hemant (22 Oct 2018) -- End


            'Pinkal (27-Jun-2019) -- Start
            'Enhancement - IMPLEMENTING OT MODULE.
            Session("AllowtoApproveOTRequisition") = objUserPrivilege._AllowtoApproveOTRequisition
            Session("AllowtoViewOTRequisitionApprovalList") = objUserPrivilege._AllowtoViewOTRequisitionApprovalList
            'Pinkal (27-Jun-2019) -- End


            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            Session("AllowToViewEmpOTAssignment") = objUserPrivilege._AllowToViewEmpOTAssignment
            Session("AllowToAddEmpOTAssignment") = objUserPrivilege._AllowToAddEmpOTAssignment
            Session("AllowToDeleteEmpOTAssignment") = objUserPrivilege._AllowToDeleteEmpOTAssignment
            Session("AllowToCancelEmpOTRequisition") = objUserPrivilege._AllowToCancelEmpOTRequisition
            'Pinkal (08-Jan-2020) -- End

            'Pinkal (29-Jan-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Session("AllowToPostOTRequisitionToPayroll") = objUserPrivilege._AllowToPostOTRequisitionToPayroll
            'Pinkal (29-Jan-2020) -- End


            'Pinkal (03-Mar-2020) -- Start
            'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
            Session("AllowToMigrateOTRequisitionApprover") = objUserPrivilege._AllowToMigrateOTRequisitionApprover
            'Pinkal (03-Mar-2020) -- End


            '*** END  TNA OT REQUISITION


            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            Session("AllowToDeletePercentCompletedEmployeeGoals") = objUserPrivilege._AllowToDeletePercentCompletedEmployeeGoals
            Session("AllowToViewPercentCompletedEmployeeGoals") = objUserPrivilege._AllowToViewPercentCompletedEmployeeGoals
            'S.SANDEEP |12-FEB-2019| -- END

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Session("AllowToApproveRejectEmployeeReferences") = objUserPrivilege._AllowToApproveRejectEmployeeReferences
            'Gajanan [22-Feb-2019] -- End

            'S.SANDEEP |15-APR-2019| -- START
            Session("AllowToApproveRejectEmployeeMembership") = objUserPrivilege._AllowToApproveRejectEmployeeMembership
            'S.SANDEEP |15-APR-2019| -- END

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            Session("AllowToSetDependentActive") = objUserPrivilege._AllowToSetDependentActive
            Session("AllowToSetDependentInactive") = objUserPrivilege._AllowToSetDependentInactive
            Session("AllowToDeleteDependentStatus") = objUserPrivilege._AllowToDeleteDependentStatus
            Session("AllowToViewDependentStatus") = objUserPrivilege._AllowToViewDependentStatus
            'Sohail (18 May 2019) -- End


'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Session("AllowToViewCalibrationApproverLevel") = objUserPrivilege._AllowToViewCalibrationApproverLevel
            Session("AllowToAddCalibrationApproverLevel") = objUserPrivilege._AllowToAddCalibrationApproverLevel
            Session("AllowToEditCalibrationApproverLevel") = objUserPrivilege._AllowToEditCalibrationApproverLevel
            Session("AllowToDeleteCalibrationApproverLevel") = objUserPrivilege._AllowToDeleteCalibrationApproverLevel

            Session("AllowToViewCalibrationApproverMaster") = objUserPrivilege._AllowToViewCalibrationApproverMaster
            Session("AllowToAddCalibrationApproverMaster") = objUserPrivilege._AllowToAddCalibrationApproverMaster
            Session("AllowToActivateCalibrationApproverMaster") = objUserPrivilege._AllowToActivateCalibrationApproverMaster
            Session("AllowToDeleteCalibrationApproverMaster") = objUserPrivilege._AllowToDeleteCalibrationApproverMaster
            Session("AllowToDeactivateCalibrationApproverMaster") = objUserPrivilege._AllowToDeactivateCalibrationApproverMaster
            Session("AllowToApproveRejectCalibratedScore") = objUserPrivilege._AllowToApproveRejectCalibratedScore
            'S.SANDEEP |27-MAY-2019| -- END



            'Pinkal (27-Jun-2019) -- Start
            'Enhancement - IMPLEMENTING OT MODULE.
            Session("AllowToEditOTRequisitionApprover") = objUserPrivilege._AllowToEditOTRequisitionApprover
            Session("AllowToDeleteOTRequisitionApprover") = objUserPrivilege._AllowToDeleteOTRequisitionApprover
            Session("AllowToActivateOTRequisitionApprover") = objUserPrivilege._AllowToActivateOTRequisitionApprover
            Session("AllowToInActivateOTRequisitionApprover") = objUserPrivilege._AllowToInActivateOTRequisitionApprover
            'Pinkal (27-Jun-2019) -- End

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            Session("AllowToEditCalibratedScore") = objUserPrivilege._AllowToEditCalibratedScore
            'S.SANDEEP |27-JUL-2019| -- END


            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            Session("AllowToViewBudgetTimesheetExemptEmployeeList") = objUserPrivilege._AllowToViewBudgetTimesheetExemptEmployeeList
            Session("AllowToAddBudgetTimesheetExemptEmployee") = objUserPrivilege._AllowToAddBudgetTimesheetExemptEmployee
            Session("AllowToDeleteBudgetTimesheetExemptEmployee") = objUserPrivilege._AllowToDeleteBudgetTimesheetExemptEmployee
            'Pinkal (13-Aug-2019) -- End

'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            Session("AllowtoCalibrateProvisionalScore") = objUserPrivilege._AllowtoCalibrateProvisionalScore
            'S.SANDEEP |16-AUG-2019| -- END

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : New Screen Training Need Form.
            Session("AllowToViewTrainingNeedForm") = objUserPrivilege._AllowToViewTrainingNeedForm
            Session("AllowToAddTrainingNeedForm") = objUserPrivilege._AllowToAddTrainingNeedForm
            Session("AllowToEditTrainingNeedForm") = objUserPrivilege._AllowToEditTrainingNeedForm
            Session("AllowToDeleteTrainingNeedForm") = objUserPrivilege._AllowToDeleteTrainingNeedForm
            'Sohail (14 Nov 2019) -- End

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            Session("AllowToUnlockEmployeeInPlanning") = objUserPrivilege._AllowToUnlockEmployeeInPlanning
            Session("AllowToUnlockEmployeeInAssessment") = objUserPrivilege._AllowToUnlockEmployeeInAssessment
            'S.SANDEEP |18-JAN-2020| -- END

            'S.SANDEEP |01-MAY-2020| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
            Session("AllowToViewCalibratorList") = objUserPrivilege._AllowToViewCalibratorList
            Session("AllowToAddCalibrator") = objUserPrivilege._AllowToAddCalibrator
            Session("AllowToMakeCalibratorActive") = objUserPrivilege._AllowToMakeCalibratorActive
            Session("AllowToDeleteCalibrator") = objUserPrivilege._AllowToDeleteCalibrator
            Session("AllowToMakeCalibratorInactive") = objUserPrivilege._AllowToMakeCalibratorInactive
            Session("AllowToEditCalibrator") = objUserPrivilege._AllowToEditCalibrator
            Session("AllowToEditCalibrationApproverMaster") = objUserPrivilege._AllowToEditCalibrationApproverMaster
            'S.SANDEEP |01-MAY-2020| -- END


            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
            Session("AllowToAdjustExpenseBalance") = objUserPrivilege._AllowToAdjustExpenseBalance
            'Pinkal (03-Mar-2021) -- End


            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Session("AllowToViewRetireClaimApplication") = objUserPrivilege._AllowToViewRetireClaimApplication
            Session("AllowToRetireClaimApplication") = objUserPrivilege._AllowToRetireClaimApplication
            Session("AllowToEditRetiredClaimApplication") = objUserPrivilege._AllowToEditRetiredClaimApplication
            Session("AllowToViewRetiredApplicationApproval") = objUserPrivilege._AllowToViewRetiredApplicationApproval
            Session("AllowToApproveRetiredApplication") = objUserPrivilege._AllowToApproveRetiredApplication
            Session("AllowToPostRetiredApplicationtoPayroll") = objUserPrivilege._AllowToPostRetiredApplicationtoPayroll
            'Pinkal (10-Mar-2021) -- End



            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objUserPrivilege = Nothing
            'Pinkal (11-Sep-2020) -- End


            Return True

        Catch ex As Exception
            strErrorMsg = "Error in SetUserSessions : " & ex.Message
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objGlobalAccess = Nothing
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Function

    'Nilay (16-Apr-2016) -- Start
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Public Sub SetDateFormat()
        Try

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If Session("DateFormat") IsNot Nothing AndAlso Session("DateSeparator") IsNot Nothing Then
            Dim NewCulture As CultureInfo = CType(System.Threading.Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
            NewCulture.DateTimeFormat.ShortDatePattern = Session("DateFormat")
            NewCulture.DateTimeFormat.DateSeparator = Session("DateSeparator")
            System.Threading.Thread.CurrentThread.CurrentCulture = NewCulture
            End If
            'Pinkal (20-Aug-2020) -- End
        Catch ex As Exception
            Throw New Exception("SetDateFormat:- " & ex.Message)
        End Try
    End Sub
    'Nilay (16-Apr-2016) -- End

    Public Function SetCompanySessions(ByRef strErrorMsg As String, ByVal intCompanyID As Integer, ByVal intLanguageID As Integer) As Boolean
        strErrorMsg = ""
        Try
            If Session("mdbname") IsNot Nothing Then
                Dim objMasterData As New clsMasterData
                objMasterData.Update_IDM_Active_Status(intCompanyID, Session("mdbname"))
                objMasterData = Nothing
            End If


            'TODO Change all global objects


            'Pinkal (11-Dec-2018) -- Start
            'Issue - Login page is taking time to login in NMB.

            'gobjConfigOptions = New clsConfigOptions
            'gobjConfigOptions._Companyunkid = intCompanyID
            'ConfigParameter._Object._Companyunkid = intCompanyID
            'Company._Object._Companyunkid = intCompanyID

            'If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Company > ConfigParameter._Object._NoOfCompany Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If

            'Company._Object._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso Company._Object._Total_Active_Employee_ForAllCompany > ConfigParameter._Object._NoOfEmployees Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If


            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- START
            'S.SANDEEP |24-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : TEMP WORK FOR NMB
            'Dim objCompany As New clsCompany_Master
            'objCompany._Companyunkid = intCompanyID

            'Dim clsConfig As New clsConfigOptions
            'clsConfig._Companyunkid = intCompanyID

            'If clsConfig._IsArutiDemo = False AndAlso objCompany._Total_Active_Company > clsConfig._NoOfCompany Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If

            ''Sohail (13 Sep 2019) -- Start
            ''Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
            ''objCompany._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
            'objCompany._Total_Active_Employee_AsOnDate = clsConfig._CurrentDateAndTime.Date
            ''Sohail (13 Sep 2019) -- End
            'If clsConfig._IsArutiDemo = False AndAlso objCompany._Total_Active_Employee_ForAllCompany > clsConfig._NoOfEmployees Then
            '    strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."
            '    Exit Try
            'End If

            ''Pinkal (11-Dec-2018) -- End


            ''Sohail (08 Dec 2018) -- Start
            ''NMB Enhancement - Hiding menu in 75.1.
            'Dim objGroupMaster As New clsGroup_Master
            'objGroupMaster._Groupunkid = 1
            'HttpContext.Current.Session("CompanyGroupName") = objGroupMaster._Groupname.ToUpper 'Company Group Name in UPPER CASE            
            'objGroupMaster = Nothing
            ''Sohail (08 Dec 2018) -- End



            'Sohail (08 Dec 2018) -- Start
            'NMB Enhancement - Hiding menu in 75.1.
            Dim objGroupMaster As New clsGroup_Master
            objGroupMaster._Groupunkid = 1
            HttpContext.Current.Session("CompanyGroupName") = objGroupMaster._Groupname.ToUpper 'Company Group Name in UPPER CASE            

            Dim clsConfig As New clsConfigOptions
            Dim objCompany As New clsCompany_Master

            If objGroupMaster._Groupname.ToString().ToUpper() <> "NMB PLC" Then

            objCompany._Companyunkid = intCompanyID


            clsConfig._Companyunkid = intCompanyID

            If clsConfig._IsArutiDemo = False AndAlso objCompany._Total_Active_Company > clsConfig._NoOfCompany Then
                strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of company license limit. Please contact Aruti Support team for assistance."
                Exit Try
            End If

            'Sohail (13 Sep 2019) -- Start
            'Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
            'objCompany._Total_Active_Employee_AsOnDate = ConfigParameter._Object._CurrentDateAndTime.Date
            objCompany._Total_Active_Employee_AsOnDate = clsConfig._CurrentDateAndTime.Date
            'Sohail (13 Sep 2019) -- End
            If clsConfig._IsArutiDemo = False AndAlso objCompany._Total_Active_Employee_ForAllCompany > clsConfig._NoOfEmployees Then
                strErrorMsg = "Sorry, you cannot login to Aruti. Reason : You have exceeded number of Employee license limit. Please contact Aruti Support team for assistance."
                Exit Try
            End If

            'Pinkal (11-Dec-2018) -- End
            End If
            objGroupMaster = Nothing

            'S.SANDEEP |24-NOV-2020| -- END
            '<TODO> -- TO BE REMOVED ONCE WORK IS DONE FOR NMB -- END





            '================================ START TO SET CONFIGURATION RELATED SESSIONS=====================================

            'Pinkal (11-Dec-2018) -- Start
            'Issue - Login page is taking time to login in NMB.
            'Dim clsConfig As New clsConfigOptions
            'clsConfig._Companyunkid = intCompanyID
            'Pinkal (11-Dec-2018) -- End

            HttpContext.Current.Session("CompanyUnkId") = intCompanyID

            'Nilay (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Session("DateFormat") = clsConfig._CompanyDateFormat
            Session("DateSeparator") = clsConfig._CompanyDateSeparator
            'Nilay (16-Apr-2016) -- End 

            'Nilay (07-Feb-2016) -- Start
            Session("FirstNamethenSurname") = clsConfig._FirstNamethenSurname
            'Nilay (07-Feb-2016) -- End

            Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
            Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting
            Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
            Session("SickSheetNotype") = clsConfig._SickSheetNotype
            Session("fmtCurrency") = clsConfig._CurrencyFormat
            Session("AllowEditAddress") = clsConfig._AllowEditAddress
            Session("AllowEditPersonalInfo") = clsConfig._AllowEditPersonalInfo
            Session("AllowEditEmergencyAddress") = clsConfig._AllowEditEmergencyAddress
            Session("LoanApplicationNoType") = clsConfig._LoanApplicationNoType
            Session("LoanApplicationPrifix") = clsConfig._LoanApplicationPrifix
            Session("NextLoanApplicationNo") = clsConfig._NextLoanApplicationNo
            Session("BatchPostingNotype") = clsConfig._BatchPostingNotype
            Session("BatchPostingPrifix") = clsConfig._BatchPostingPrifix
            Session("DoNotAllowOverDeductionForPayment") = clsConfig._DoNotAllowOverDeductionForPayment
            Session("PaymentVocNoType") = clsConfig._PaymentVocNoType

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Session("PaymentVocPrefix") = clsConfig._PaymentVocPrefix
            'Shani(20-Nov-2015) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Session("LeaveCancelFormNotype") = clsConfig._LeaveCancelFormNotype
            Session("LeaveCancelFormNoPrifix") = clsConfig._LeaveCancelFormNoPrifix
            'Pinkal (03-May-2019) -- End


            Session("IsDenominationCompulsory") = clsConfig._IsDenominationCompulsory
            Session("SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
            Session("AssetDeclarationInstruction") = clsConfig._AssetDeclarationInstruction
            'Sohail (04 Feb 2020) -- Start
            'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
            Session("NonDisclosureDeclaration") = clsConfig._NonDisclosureDeclaration
            Session("NonDisclosureWitnesser1") = clsConfig._NonDisclosureWitnesser1
            Session("NonDisclosureWitnesser2") = clsConfig._NonDisclosureWitnesser2
            Session("NonDisclosureWitnesser2") = clsConfig._NonDisclosureWitnesser2
            Session("NonDisclosureDeclarationFromDate") = clsConfig._NonDisclosureDeclarationFromDate
            Session("NonDisclosureDeclarationToDate") = clsConfig._NonDisclosureDeclarationToDate
            Session("LockDaysAfterNonDisclosureDeclarationToDate") = CInt(clsConfig._LockDaysAfterNonDisclosureDeclarationToDate)
            Session("NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate") = CInt(clsConfig._NewEmpUnLockDaysforNonDisclosureDecWithinAppointmentDate)
            'Sohail (04 Feb 2020) -- End
            Session("Document_Path") = clsConfig._Document_Path
            Session("IgnoreZeroValueHeadsOnPayslip") = clsConfig._IgnoreZeroValueHeadsOnPayslip
            Session("ShowLoanBalanceOnPayslip") = clsConfig._ShowLoanBalanceOnPayslip
            Session("ShowSavingBalanceOnPayslip") = clsConfig._ShowSavingBalanceOnPayslip
            Session("ShowEmployerContributionOnPayslip") = clsConfig._ShowEmployerContributionOnPayslip
            Session("ShowAllHeadsOnPayslip") = clsConfig._ShowAllHeadsOnPayslip
            Session("LogoOnPayslip") = clsConfig._LogoOnPayslip
            Session("PayslipTemplate") = clsConfig._PayslipTemplate
            Session("LeaveTypeOnPayslip") = clsConfig._LeaveTypeOnPayslip
            Session("ShowCumulativeAccrual") = clsConfig._ShowCumulativeAccrualOnPayslip
            Session("Base_CurrencyId") = clsConfig._Base_CurrencyId
            Session("ShowCategoryOnPayslip") = clsConfig._ShowCategoryOnPayslip
            Session("ShowInformationalHeadsOnPayslip") = clsConfig._ShowInformationalHeadsOnPayslip
            Session("PaymentRoundingType") = clsConfig._PaymentRoundingType
            Session("PaymentRoundingMultiple") = clsConfig._PaymentRoundingMultiple
            Session("CFRoundingAbove") = clsConfig._CFRoundingAbove
            Session("ShowBirthDateOnPayslip") = clsConfig._ShowBirthDateOnPayslip
            Session("ShowAgeOnPayslip") = clsConfig._ShowAgeOnPayslip
            Session("ShowNoOfDependantsOnPayslip") = clsConfig._ShowNoOfDependantsOnPayslip
            Session("ShowMonthlySalaryOnPayslip") = clsConfig._ShowMonthlySalaryOnPayslip
            Session("OT1HourHeadID") = clsConfig._OT1HourHeadId
            Session("OT2HourHeadID") = clsConfig._OT2HourHeadId
            Session("OT3HourHeadID") = clsConfig._OT3HourHeadId
            Session("OT4HourHeadID") = clsConfig._OT4HourHeadId
            Session("OT1HourHeadName") = clsConfig._OT1HourHeadName
            Session("OT2HourHeadName") = clsConfig._OT2HourHeadName
            Session("OT3HourHeadName") = clsConfig._OT3HourHeadName
            Session("OT4HourHeadName") = clsConfig._OT4HourHeadName
            Session("OT1AmountHeadID") = clsConfig._OT1AmountHeadId
            Session("OT2AmountHeadID") = clsConfig._OT2AmountHeadId
            Session("OT3AmountHeadID") = clsConfig._OT3AmountHeadId
            Session("OT4AmountHeadID") = clsConfig._OT4AmountHeadId
            Session("OT1AmountHeadName") = clsConfig._OT1AmountHeadName
            Session("OT2AmountHeadName") = clsConfig._OT2AmountHeadName
            Session("OT3AmountHeadName") = clsConfig._OT3AmountHeadName
            Session("OT4AmountHeadName") = clsConfig._OT4AmountHeadName
            Session("ShowSalaryOnHoldOnPayslip") = clsConfig._ShowSalaryOnHoldOnPayslip
            If clsConfig._ArutiSelfServiceURL = "http://" & HttpContext.Current.Request.ApplicationPath Then
                Session("ArutiSelfServiceURL") = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath
            Else
                Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
            End If

            Session("AllowEditExperience") = clsConfig._AllowEditExperience
            Session("AllowDeleteExperience") = clsConfig._AllowDeleteExperience
            Session("AllowAddSkills") = clsConfig._AllowAddSkills
            Session("AllowEditSkills") = clsConfig._AllowEditSkills
            Session("AllowDeleteSkills") = clsConfig._AllowDeleteSkills
            Session("AllowAddDependants") = clsConfig._AllowAddDependants
            Session("AllowDeleteDependants") = clsConfig._AllowDeleteDependants
            Session("IsDependant_AgeLimit_Set") = clsConfig._IsDependant_AgeLimit_Set
            Session("AllowEditDependants") = clsConfig._AllowEditDependants
            Session("AllowAddIdentity") = clsConfig._AllowAddIdentity
            Session("AllowDeleteIdentity") = clsConfig._AllowDeleteIdentity
            Session("AllowEditIdentity") = clsConfig._AllowEditIdentity
            Session("AllowAddMembership") = clsConfig._AllowAddMembership
            Session("AllowDeleteMembership") = clsConfig._AllowDeleteMembership
            Session("AllowEditMembership") = clsConfig._AllowEditMembership
            Session("AllowAddQualifications") = clsConfig._AllowAddQualifications
            Session("AllowDeleteQualifications") = clsConfig._AllowDeleteQualifications
            Session("AllowEditQualifications") = clsConfig._AllowEditQualifications
            Session("AllowAddReference") = clsConfig._AllowAddReference
            Session("AllowDeleteReference") = clsConfig._AllowDeleteReference
            Session("AllowEditReference") = clsConfig._AllowEditReference
            Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
            Session("IsBSCObjectiveSaved") = clsConfig._IsBSCObjectiveSaved
            Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
            Session("AllowAddExperience") = clsConfig._AllowAddExperience

            Session("IsAllocation_Hierarchy_Set") = clsConfig._IsAllocation_Hierarchy_Set
            Session("Allocation_Hierarchy") = clsConfig._Allocation_Hierarchy

            Session("AllowChangeCompanyEmail") = clsConfig._AllowChangeCompanyEmail
            Session("AllowToViewPersonalSalaryCalculationReport") = clsConfig._AllowToViewPersonalSalaryCalculationReport
            Session("AllowToViewEDDetailReport") = clsConfig._AllowToViewEDDetailReport

            Session("SickSheetNotype") = clsConfig._SickSheetNotype
            Session("LeaveApproverForLeaveType") = clsConfig._IsLeaveApprover_ForLeaveType
            Session("LeaveFormNoType") = clsConfig._LeaveFormNoType

            'Nilay (18-Apr-2015) -- Start
            'Add Loan Setting on configuration
            Session("LoanApprover_ForLoanScheme") = clsConfig._IsLoanApprover_ForLoanScheme
            'Nilay (18-Apr-2015) -- End

            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Session("SendLoanEmailFromDesktopMSS") = clsConfig._IsSendLoanEmailFromDesktopMSS
            Session("SendLoanEmailFromESS") = clsConfig._IsSendLoanEmailFromESS
            'Nilay (10-Dec-2016) -- End

            'Sohail (16 May 2018) -- Start
            'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
            Session("Advance_NetPayPercentage") = clsConfig._Advance_NetPayPercentage
            Session("Advance_NetPayTranheadUnkid") = clsConfig._Advance_NetPayTranheadUnkid
            'Sohail (16 May 2018) -- End

            'Gajanan (23-May-2018) -- Start
            'CCK Enhancement - Ref # 180 - On Advances Application, provide configuration where we can be able to prevent Advance application after configured days (set on configuration) in each period.
            Session("Advance_DontAllowAfterDays") = clsConfig._Advance_DontAllowAfterDays
            'Gajanan (23-May-2018) -- End

            Session("CompanyDomain") = clsConfig._CompanyDomain.ToString.Trim

            'Pinkal (01-Apr-2015) -- Start
            'Enhancement - IMPLEMENT EMPLOYEE RE-CATEGORIZATION FOR TANAPA
            Session("Notify_Dates") = clsConfig._Notify_Dates
            'Pinkal (01-Apr-2015) -- End

            Session("Notify_EmplData") = clsConfig._Notify_EmplData
            Session("Notify_Allocation") = clsConfig._Notify_Allocation
            Session("DatafileExportPath") = clsConfig._DatafileExportPath
            Session("DatafileName") = clsConfig._DatafileName
            Session("Accounting_TransactionReference") = clsConfig._Accounting_TransactionReference
            Session("Accounting_JournalType") = clsConfig._Accounting_JournalType
            Session("Accounting_JVGroupCode") = clsConfig._Accounting_JVGroupCode

            If clsConfig._LeaveBalanceSetting <= 0 Then
                Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
            Else
                Session("LeaveBalanceSetting") = clsConfig._LeaveBalanceSetting
            End If

            Session("AllowToviewPaysliponEss") = clsConfig._AllowToviewPaysliponEss
            Session("ViewPayslipDaysBefore") = clsConfig._ViewPayslipDaysBefore
            'Sohail (29 Mar 2017) -- Start
            'CCK Enhancement - 65.1 - Payslip to be accessible on ess only after payment authorisation. Provide setting on configuration as earlier agreed. 
            Session("DontShowPayslipOnESSIfPaymentNotAuthorized") = clsConfig._DontShowPayslipOnESSIfPaymentNotAuthorized
            'Sohail (29 Mar 2017) -- End
            'Sohail (15 May 2020) -- Start
            'NMB Enhancement # : Advance Filter report on Loan Advance Saving Report in self service.
            Session("DontShowPayslipOnESSIfPaymentNotDone") = clsConfig._DontShowPayslipOnESSIfPaymentNotDone
            'Sohail (15 May 2020) -- End

            'Nilay (03-Nov-2016) -- Start
            'Enhancement : Option to Show / hide payslip on ESS
            Session("ShowPayslipOnESS") = clsConfig._ShowHidePayslipOnESS
            'Nilay (03-Nov-2016) -- End

            Session("IsImgInDataBase") = clsConfig._IsImgInDataBase

            Session("AllowToAddEditImageForESS") = clsConfig._AllowToAddEditImageForESS

            Session("ShowFirstAppointmentDate") = clsConfig._ShowFirstAppointmentDate

            Session("BatchPostingNotype") = clsConfig._BatchPostingNotype
            Session("NotifyPayroll_Users") = clsConfig._Notify_Payroll_Users
            Session("EFTIntegration") = clsConfig._EFTIntegration
            Session("DatafileName") = clsConfig._DatafileName
            Session("AccountingSoftWare") = clsConfig._AccountingSoftWare
            Session("MobileMoneyEFTIntegration") = clsConfig._MobileMoneyEFTIntegration 'Sohail (08 Dec 2014)

            Session("_LoanVocNoType") = clsConfig._LoanVocNoType
            Session("_PaymentVocNoType") = clsConfig._PaymentVocNoType
            Session("_DoNotAllowOverDeductionForPayment") = clsConfig._DoNotAllowOverDeductionForPayment
            Session("_IsDenominationCompulsory") = clsConfig._IsDenominationCompulsory
            Session("_SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
            Session("SavingsVocNoType") = clsConfig._SavingsVocNoType
            Session("AllowAssessor_Before_Emp") = clsConfig._AllowAssessor_Before_Emp
            Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
            Session("ConsiderItemWeightAsNumber") = False
            'Shani (23-Nov123-2016-2016) -- End

            Session("PolicyManagementTNA") = clsConfig._PolicyManagementTNA

            Session("IsAutomaticIssueOnFinalApproval") = clsConfig._IsAutomaticIssueOnFinalApproval

            Session("StaffReqFormNoType") = clsConfig._StaffReqFormNoType
            Session("ApplyStaffRequisition") = clsConfig._ApplyStaffRequisition

            Session("ClosePayrollPeriodIfLeaveIssue") = clsConfig._ClosePayrollPeriodIfLeaveIssue


            Session("PaymentApprovalwithLeaveApproval") = clsConfig._PaymentApprovalwithLeaveApproval
            'Sohail (13 Sep 2019) -- Start
            'Hakielimu Issue # 4152 - 76.1 : Error when using the "Forgot Password" feature. (Object reference not set to an instance of an object.)
            'Session("ClaimRequestVocNoType") = ConfigParameter._Object._ClaimRequestVocNoType
            Session("ClaimRequestVocNoType") = clsConfig._ClaimRequestVocNoType
            'Sohail (13 Sep 2019) -- End

            Session("SickSheetTemplate") = clsConfig._SickSheetTemplate
            Session("SickSheetAllocationId") = clsConfig._SickSheetAllocationId


            Session("CustomPayrollReportAllocIds") = clsConfig._CustomPayrollReportAllocIds
            Session("CustomPayrollReportHeadsIds") = clsConfig._CustomPayrollReportHeadsIds


            'SHANI (20 APR 2015)-START
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            Session("CustomPayrollReportTemplate2AllocIds") = clsConfig._CustomPayrollReportTemplate2AllocIds
            Session("CustomPayrollReportTemplate2HeadsIds") = clsConfig._CustomPayrollReportTemplate2HeadsIds
            'SHANI (20 APR 2015)--END 


            Session("Notify_IssuedLeave_Users") = clsConfig._Notify_IssuedLeave_Users

            Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
            Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
            Session("CascadingTypeId") = clsConfig._CascadingTypeId
            Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
            Session("ScoringOptionId") = clsConfig._ScoringOptionId
            Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
            Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation

            Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies

            'SHANI (16 APR 2015)-START
            Session("DetailedSalaryBreakdownReportHeadsIds") = clsConfig._DetailedSalaryBreakdownReportHeadsIds
            'SHANI (16 APR 2015)--END 


            'SHANI (17 APR 2015)-START
            Session("Notify_Bank_Users") = clsConfig._Notify_Bank_Users
            'SHANI (17 APR 2015)--END 


            'Pinkal (30-Jul-2015) -- Start
            'Enhancement : PUT SETTING FOR TNA WHEN TNA ATTENDANCE DATA SHOULD BE CHANGE AFTER PAYROLL PAYMENT. (ST. JUDE REQUIREMENT GIVEN BY RUTTA)
            Session("AllowToChangeAttendanceAfterPayment") = clsConfig._AllowToChangeAttendanceAfterPayment
            'Pinkal (30-Jul-2015) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Session("OpenAfterExport") = clsConfig._OpenAfterExport
            Session("ExportReportPath") = clsConfig._ExportReportPath
            Session("SMimeRunPath") = clsConfig._SMimeRunPath
            Session("IsArutiDemo") = clsConfig._IsArutiDemo
            Session("StaffReqFormNoPrefix") = clsConfig._StaffReqFormNoPrefix
            Session("StaffReqFormNoType") = clsConfig._StaffReqFormNoType
            Session("DonotAttendanceinSeconds") = clsConfig._DonotAttendanceinSeconds
            Session("FirstCheckInLastCheckOut") = clsConfig._FirstCheckInLastCheckOut
            'Shani(20-Nov-2015) -- End

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Session("EFTCitiDirectCountryCode") = clsConfig._EFTCitiDirectCountryCode
            Session("EFTCitiDirectSkipPriorityFlag") = clsConfig._EFTCitiDirectSkipPriorityFlag
            Session("EFTCitiDirectChargesIndicator") = clsConfig._EFTCitiDirectChargesIndicator
			Session("EFTCitiDirectAddPaymentDetail") = clsConfig._EFTCitiDirectAddPaymentDetail
            'Sohail (09 Jan 2016) -- End

            'Sohail (19 Feb 2016) -- Start
            'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
            Session("Accounting_Country") = clsConfig._Accounting_Country
            'Sohail (19 Feb 2016) -- End

            'Sohail (01 Mar 2016) -- Start
            'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.
            Session("RoundOff_Type") = clsConfig._RoundOff_Type
            'Sohail (01 Mar 2016) -- End


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            Session("SectorRouteAssignToEmp") = clsConfig._SectorRouteAssignToEmp
            'Pinkal (22-Mar-2016) -- End


            'Pinkal (18-Jun-2016) -- Start
            'Enhancement - IMPLEMENTING Leave Liability Setting to Leave Liablity Report.
            Session("LeaveLiabilityReportSetting") = clsConfig._LeaveLiabilityReportSetting
            'Pinkal (18-Jun-2016) -- End


            'Anjan [01 September 2016] -- Start
            'ENHANCEMENT : Including Payslip settings on configuration.
            Session("ShowBankAccountNoOnPayslip") = clsConfig._ShowBankAccountNoOnPayslip
            'Anjan [01 Sepetember 2016] -- End


            'S.SANDEEP [12 OCT 2016] -- START
            'ENHANCEMENT : ACB REPORT CHANGES
            Session("SelectedAllocationForDailyTimeSheetReport_Voltamp") = clsConfig._SelectedAllocationForDailyTimeSheetReport_Voltamp
            Session("ShowEmployeeStatusForDailyTimeSheetReport_Voltamp") = clsConfig._ShowEmployeeStatusForDailyTimeSheetReport_Voltamp
            'S.SANDEEP [12 OCT 2016] -- END


            'Pinkal (18-Nov-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            Session("LeaveAccrueDaysAfterEachMonth") = clsConfig._LeaveAccrueDaysAfterEachMonth
            Session("LeaveAccrueTenureSetting") = clsConfig._LeaveAccrueTenureSetting
            'Pinkal (18-Nov-2016) -- End

            'Nilay (07 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee Timesheet 65.1 - ignore activity percentage restriction for the shift hrs
            Session("AllowToExceedTimeAssignedToActivity") = CBool(clsConfig._AllowToExceedTimeAssignedToActivity)
            'Nilay (07 Feb 2017) -- End

            'Nilay (02-Jan-2017) -- Start
            'Issue #33: Enhancement - Implementing Budget Employee Time Sheet
            Session("AllowOverTimeToEmpTimesheet") = CBool(clsConfig._AllowOverTimeToEmpTimesheet)
            'Nilay (02-Jan-2017) -- End

            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Session("NotAllowIncompleteTimesheet") = CBool(clsConfig._NotAllowIncompleteTimesheet)
            'Nilay (27 Feb 2017) -- End

            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Session("DescriptionMandatoryForActivity") = CBool(clsConfig._DescriptionMandatoryForActivity)
            Session("AllowActivityHoursByPercentage") = CBool(clsConfig._AllowActivityHoursByPercentage)
            'Nilay (21 Mar 2017) -- End

            'Nilay (23 Jan 2017) -- Start
            'CCBRT Enhancement - 65.1 - Show Statutory Message on ESS Payslip.
            Session("StatutoryMessageOnPayslipOnESS") = CStr(clsConfig._StatutoryMessageOnPayslipOnESS)
            'Nilay (23 Jan 2017) -- End



            'S.SANDEEP [28-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : CCBRT, MONTHLY PAYROLL REPORT
            Session("SkipAbsentFromTnAMonthlyPayrollReport") = clsConfig._SkipAbsentFromTnAMonthlyPayrollReport
            Session("IncludePendingApproverFormsInMonthlyPayrollReport") = clsConfig._IncludePendingApproverFormsInMonthlyPayrollReport
            Session("IncludeSystemRetirementMonthlyPayrollReport") = clsConfig._IncludeSystemRetirementMonthlyPayrollReport
            Session("CheckedMonthlyPayrollReport1HeadIds") = clsConfig._CheckedMonthlyPayrollReport1HeadIds
            Session("CheckedMonthlyPayrollReport2HeadIds") = clsConfig._CheckedMonthlyPayrollReport2HeadIds
            Session("CheckedMonthlyPayrollReport3HeadIds") = clsConfig._CheckedMonthlyPayrollReport3HeadIds
            Session("CheckedMonthlyPayrollReport4HeadIds") = clsConfig._CheckedMonthlyPayrollReport4HeadIds
            Session("CheckedMonthlyPayrollReportLeaveIds") = clsConfig._CheckedMonthlyPayrollReportLeaveIds
            'S.SANDEEP [28-FEB-2017] -- END


            'S.SANDEEP [06-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : Training Module Notification
            Session("Ntf_TrainingLevelI_EvalUserIds") = clsConfig._Ntf_TrainingLevelI_EvalUserIds
            Session("Ntf_TrainingLevelIII_EvalUserIds") = clsConfig._Ntf_TrainingLevelIII_EvalUserIds
            'S.SANDEEP [06-MAR-2017] -- END

            'Nilay (27 Feb 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            Session("NotAllowIncompleteTimesheet") = CBool(clsConfig._NotAllowIncompleteTimesheet)
            'Nilay (27 Feb 2017) -- End

            

            'Pinkal (11-AUG-2017) -- Start
            'Enhancement - Working On B5 Plus Company TnA Enhancements.
            Session("IsHolidayConsiderOnWeekend") = clsConfig._IsHolidayConsiderOnWeekend
            Session("IsDayOffConsiderOnWeekend") = clsConfig._IsDayOffConsiderOnWeekend
            Session("IsHolidayConsiderOnDayoff") = clsConfig._IsHolidayConsiderOnDayoff
            'Pinkal (11-AUG-2017) -- End

            'S.SANDEEP [16-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 118
            Session("AllowtoViewSignatureESS") = clsConfig._AllowtoViewSignatureESS
            'S.SANDEEP [16-Jan-2018] -- END

            'Hemant (19 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT#0003941(Rural Electrification Authority) :  Renaming Loan tab on SS in the application window. Client would like to rename the Loan selection tab on SS to Loan/Advance, and rename the advance tab to "blank".
            Session("DontAllowAdvanceOnESS") = clsConfig._DontAllowAdvanceOnESS
            'Hemant (19 Aug 2019) -- End

            'Sohail (31 May 2018) -- Start
            'TANAPA Enhancement - Ref # 247 : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
            Session("WebURLInternal") = clsConfig._WebURLInternal
            'Sohail (31 May 2018) -- End


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            Session("ShowBgTimesheetActivity") = clsConfig._ShowBgTimesheetActivity
            Session("ShowBgTimesheetAssignedActivityPercentage") = clsConfig._ShowBgTimesheetAssignedActivityPercentage
            Session("ShowBgTimesheetActivityProject") = clsConfig._ShowBgTimesheetActivityProject
            Session("ShowBgTimesheetActivityHrsDetail") = clsConfig._ShowBgTimesheetActivityHrsDetail
            Session("RemarkMandatoryForTimesheetSubmission") = clsConfig._RemarkMandatoryForTimesheetSubmission
            'Pinkal (28-Jul-2018) -- End


            'Gajanan (24-Aug-2018) -- Start
            'Enhancement - Implementing Grievance Module.

            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        

            Session("GrievanceApprovalSetting") = clsConfig._GrievanceApprovalSetting
            'Session("GrievanceApprovalSetting") = CInt(enGrievanceApproval.ApproverEmpMapping)

            'Gajanan [10-June-2019] -- End

            Session("GrievanceRefNoType") = clsConfig._GrievanceRefNoType
            Session("GrievanceRefPrefix") = clsConfig._GrievanceRefPrefix
             'Gajanan (24-Aug-2018) -- Start

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            Session("DependantDocsAttachmentMandatory") = clsConfig._DependantDocsAttachmentMandatory
            Session("QualificationCertificateAttachmentMandatory") = clsConfig._QualificationCertificateAttachmentMandatory
            'SHANI (27 JUL 2015) -- End 


            'S.SANDEEP [05 DEC 2015] -- START
            Session("SkipApprovalFlowInPlanning") = clsConfig._SkipApprovalFlowInPlanning
            'S.SANDEEP [05 DEC 2015] -- END

            'S.SANDEEP [23 DEC 2015] -- START
            Session("OnlyOneItemPerCompetencyCategory") = clsConfig._OnlyOneItemPerCompetencyCategory

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'Session("IncludeCustomItemInPlanning") = clsConfig._IncludeCustomItemInPlanning
            Session("EnableBSCAutomaticRating") = clsConfig._EnableBSCAutomaticRating
            Session("DontAllowToEditScoreGenbySys") = clsConfig._DontAllowToEditScoreGenbySys
            Session("GoalsAccomplishedRequiresApproval") = clsConfig._GoalsAccomplishedRequiresApproval
            'Shani (26-Sep-2016) -- End


            'S.SANDEEP [23 DEC 2015] -- END

            'S.SANDEEP [29 DEC 2015] -- START
            Session("AssessmentReportTemplateId") = clsConfig._AssessmentReportTemplateId
            'S.SANDEEP [29 DEC 2015] -- END

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            Session("IsUseAgreedScore") = clsConfig._IsUseAgreedScore
            'Shani (23-Nov123-2016-2016) -- End


            'Shani(14-FEB-2017) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            Session("ReviewerScoreSetting") = clsConfig._ReviewerScoreSetting
            'Shani(14-FEB-2017) -- End

            'Shani(23-FEB-2017) -- Start
            'Enhancement - Add new custom item saving setting requested by (aga khan)
            Session("IsAllowCustomItemFinalSave") = clsConfig._IsAllowCustomItemFinalSave
            'Shani(23-FEB-2017) -- End

            'S.SANDEEP [25-OCT-2017] -- START
            Session("ShowMyGoals") = clsConfig._ShowMyGoals
            Session("ShowCustomHeaders") = clsConfig._ShowCustomHeaders
            Session("ShowGoalsApproval") = clsConfig._ShowGoalsApproval
            Session("ShowSelfAssessment") = clsConfig._ShowSelfAssessment
            Session("ShowMyComptencies") = clsConfig._ShowMyComptencies
            Session("ShowViewPerformanceAssessment") = clsConfig._ShowViewPerformanceAssessment
            'S.SANDEEP [25-OCT-2017] -- END

            'S.SANDEEP [29-NOV-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # (38,41)
            Session("Ntf_FinalAcknowledgementUserIds") = clsConfig._Ntf_FinalAcknowledgementUserIds
            Session("Ntf_GoalsUnlockUserIds") = clsConfig._Ntf_GoalsUnlockUserIds
            'S.SANDEEP [29-NOV-2017] -- END



            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            Session("CreateADUserFromEmpMst") = clsConfig._CreateADUserFromEmpMst
            Session("UserMustchangePwdOnNextLogon") = clsConfig._UserMustchangePwdOnNextLogon
            'Pinkal (18-Aug-2018) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Session("SetRelieverAsMandatoryForApproval") = clsConfig._SetRelieverAsMandatoryForApproval
            Session("AllowToSetRelieverOnLvFormForESS") = clsConfig._AllowToSetRelieverOnLvFormForESS
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Session("RelieverAllocation") = clsConfig._RelieverAllocation
            'Pinkal (13-Mar-2019) -- End


            'Pinkal (25-Mar-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            Session("AllowLvApplicationApplyForBackDates") = clsConfig._AllowLvApplicationApplyForBackDates
            'Pinkal (25-Mar-2019) -- End



            'Hemant (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            Session("AssetDeclarationTemplate") = clsConfig._AssetDeclarationTemplate
            'Hemant (06 Oct 2018) -- End

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On notification center on configuration, provide a section to configure notification to be sent to specified users when a vacancy requisition is final approved in 75.1.
            Session("StaffRequisitionFinalApprovedNotificationUserIds") = clsConfig._StaffRequisitionFinalApprovedNotificationUserIds
            'Sohail (12 Oct 2018) -- End

            'S.SANDEEP [09-OCT-2018] -- START
            Session("EnableTraningRequisition") = clsConfig._EnableTraningRequisition
            Session("DonotAllowToRequestTrainingAfterDays") = clsConfig._DonotAllowToRequestTrainingAfterDays
            'S.SANDEEP [09-OCT-2018] -- END

            'Hemant (22 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            Session("SetOTRequisitionMandatory") = clsConfig._SetOTRequisitionMandatory
            Session("CapOTHrsForHODApprovers") = clsConfig._CapOTHrsForHODApprovers
            'Hemant (22 Oct 2018) -- End

            'S.SANDEEP [20-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}

            'S.SANDEEP [22-NOV-2018] -- START

            'Session("SkipEmployeeMovementApprovalFlow") = ConfigParameter._Object._SkipEmployeeMovementApprovalFlow
            'Session("RejectTransferUserNotification") = ConfigParameter._Object._RejectTransferUserNotification
            'Session("RejectReCategorizationUserNotification") = ConfigParameter._Object._RejectReCategorizationUserNotification
            'Session("RejectProbationUserNotification") = ConfigParameter._Object._RejectProbationUserNotification
            'Session("RejectConfirmationUserNotification") = ConfigParameter._Object._RejectConfirmationUserNotification
            'Session("RejectSuspensionUserNotification") = ConfigParameter._Object._RejectSuspensionUserNotification
            'Session("RejectTerminationUserNotification") = ConfigParameter._Object._RejectTerminationUserNotification
            'Session("RejectRetirementUserNotification") = ConfigParameter._Object._RejectRetirementUserNotification
            'Session("RejectWorkPermitUserNotification") = ConfigParameter._Object._RejectWorkPermitUserNotification
            'Session("RejectResidentPermitUserNotification") = ConfigParameter._Object._RejectResidentPermitUserNotification
            'Session("RejectCostCenterPermitUserNotification") = ConfigParameter._Object._RejectCostCenterPermitUserNotification

            Session("SkipEmployeeMovementApprovalFlow") = clsConfig._SkipEmployeeMovementApprovalFlow
            Session("RejectTransferUserNotification") = clsConfig._RejectTransferUserNotification
            Session("RejectReCategorizationUserNotification") = clsConfig._RejectReCategorizationUserNotification
            Session("RejectProbationUserNotification") = clsConfig._RejectProbationUserNotification
            Session("RejectConfirmationUserNotification") = clsConfig._RejectConfirmationUserNotification
            Session("RejectSuspensionUserNotification") = clsConfig._RejectSuspensionUserNotification
            Session("RejectTerminationUserNotification") = clsConfig._RejectTerminationUserNotification
            Session("RejectRetirementUserNotification") = clsConfig._RejectRetirementUserNotification
            Session("RejectWorkPermitUserNotification") = clsConfig._RejectWorkPermitUserNotification
            Session("RejectResidentPermitUserNotification") = clsConfig._RejectResidentPermitUserNotification
            Session("RejectCostCenterPermitUserNotification") = clsConfig._RejectCostCenterPermitUserNotification
            'S.SANDEEP [22-NOV-2018] -- END
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Session("RejectExemptionUserNotification") = clsConfig._RejectExemptionUserNotification
            'Sohail (21 Oct 2019) -- End
            
            'S.SANDEEP [20-JUN-2018] -- END

            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement for Email Notification After Final Saved In Asset Declaration 
            Session("AssetDeclarationFinalSavedNotificationUserIds") = clsConfig._AssetDeclarationFinalSavedNotificationUserIds
            'Hemant (19 Nov 2018) -- End

            'S.SANDEEP [22-NOV-2018] -- START
            Session("SkipEmployeeApprovalFlow") = clsConfig._SkipEmployeeApprovalFlow
            'S.SANDEEP [22-NOV-2018] -- END


            'Hemant (23 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            Session("AssetDeclarationUnlockFinalSavedNotificationUserIds") = clsConfig._AssetDeclarationUnlockFinalSavedNotificationUserIds
            'Hemant (23 Nov 2018) -- End

            'Sohail (19 Feb 2020) -- Start
            'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
            Session("NonDisclosureDeclarationAcknowledgedNotificationUserIds") = clsConfig._NonDisclosureDeclarationAcknowledgedNotificationUserIds
            'Sohail (19 Feb 2020) -- End
            'Sohail (07 Mar 2020) -- Start
            'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
            Session("FlexCubeJVPostedOracleNotificationUserIds") = clsConfig._FlexCubeJVPostedOracleNotificationUserIds
            'Sohail (07 Mar 2020) -- End

            'Sohail (26 Nov 2018) -- Start
            'NMB Enhancement - Flex Cube JV Integration in 75.1.
            Session("OracleHostName") = clsConfig._OracleHostName
            Session("OraclePortNo") = clsConfig._OraclePortNo
            Session("OracleServiceName") = clsConfig._OracleServiceName
            Session("OracleUserName") = clsConfig._OracleUserName
            Session("OracleUserPassword") = clsConfig._OracleUserPassword
            'Sohail (26 Nov 2018) -- End

            'Sohail (02 Mar 2019) -- Start
            'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
            Session("SQLDataSource") = clsConfig._SQLDataSource
            Session("SQLDatabaseName") = clsConfig._SQLDatabaseName
            Session("SQLDatabaseOwnerName") = clsConfig._SQLDatabaseOwnerName
            Session("SQLUserName") = clsConfig._SQLUserName
            Session("SQLUserPassword") = clsConfig._SQLUserPassword
            'Sohail (02 Mar 2019) -- End


            'Pinkal (03-Dec-2018) -- Start
            'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
            Session("AssetDeclarationFromDate") = clsConfig._AssetDeclarationFromDate.ToString()
            Session("AssetDeclarationToDate") = clsConfig._AssetDeclarationToDate.ToString()
            'Hemant (01 Mar 2022) -- Start            
            'Session("UnLockDaysAfterAssetDeclarationToDate") = CInt(clsConfig._UnLockDaysAfterAssetDeclarationToDate)
            Session("DontAllowAssetDeclarationAfterDays") = CInt(clsConfig._DontAllowAssetDeclarationAfterDays)
            'Hemant (01 Mar 2022) -- End
            Session("NewEmpUnLockDaysforAssetDecWithinAppointmentDate") = CInt(clsConfig._NewEmpUnLockDaysforAssetDecWithinAppointmentDate)
            'Pinkal (03-Dec-2018) -- End

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            Session("AssetDeclarationFromDate") = clsConfig._AssetDeclarationFromDate
            'Hemant (05 Dec 2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            Session("ImportCostCenterP2PServiceURL") = clsConfig._ImportCostCenterP2PServiceURL
            Session("OpexRequestCCP2PServiceURL") = clsConfig._OpexRequestCCP2PServiceURL
            Session("CapexRequestCCP2PServiceURL") = clsConfig._CapexRequestCCP2PServiceURL
            Session("BgtRequestValidationP2PServiceURL") = clsConfig._BgtRequestValidationP2PServiceURL
            Session("NewRequisitionRequestP2PServiceURL") = clsConfig._NewRequisitionRequestP2PServiceURL
            'Pinkal (20-Nov-2018) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            Session("SkipEmployeeApprovalFlow") = clsConfig._SkipEmployeeApprovalFlow
            Session("SendDetailToEmployee") = clsConfig._SendDetailToEmployee
            Session("SMSCompanyDetailToNewEmp") = clsConfig._SMSCompanyDetailToNewEmp
            Session("IsHRFlexcubeIntegrated") = clsConfig._IsHRFlexcubeIntegrated
            Session("FlexcubeServiceCollection") = clsConfig._FlexcubeServiceCollection
            Session("FlexcubeAccountCategory") = clsConfig._FlexcubeAccountCategory
            Session("FlexcubeAccountClass") = clsConfig._FlexcubeAccountClass
            Session("SMSGatewayEmail") = clsConfig._SMSGatewayEmail
            Session("SMSGatewayEmailType") = clsConfig._SMSGatewayEmailType
            Session("EmployeeRejectNotificationUserIds") = clsConfig._EmployeeRejectNotificationUserIds
            Session("EmployeeRejectNotificationTemplateId") = clsConfig._EmployeeRejectNotificationTemplateId
            Session("EmpMandatoryFieldsIDs") = clsConfig._EmpMandatoryFieldsIDs
            Session("PendingEmployeeScreenIDs") = clsConfig._PendingEmployeeScreenIDs
            'S.SANDEEP |08-JAN-2019| -- END

            'S.SANDEEP |18-JAN-2019| -- START
            Session("GoalTypeInclusion") = clsConfig._GoalTypeInclusion
            'S.SANDEEP |18-JAN-2019| -- END
          
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data. 
            Session("SkipApprovalOnEmpData") = clsConfig._SkipApprovalOnEmpData
            Session("PendingEmployeeScreenIDs") = clsConfig._PendingEmployeeScreenIDs
            Session("EmployeeDataRejectedUserIds") = clsConfig._EmployeeDataRejectedUserIds
            'Gajanan [17-DEC-2018] -- End 


            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Session("SectorRouteAssignToExpense") = clsConfig._SectorRouteAssignToExpense
            'Pinkal (20-Feb-2019) -- End


            'Gajanan [17-DEC-2018] -- End 

            'S.SANDEEP |14-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
            Session("MakeEmpAssessCommentsMandatory") = clsConfig._MakeEmpAssessCommentsMandatory
            Session("MakeAsrAssessCommentsMandatory") = clsConfig._MakeAsrAssessCommentsMandatory
            Session("MakeRevAssessCommentsMandatory") = clsConfig._MakeRevAssessCommentsMandatory
            'S.SANDEEP |14-MAR-2019| -- END

            'Sohail (10 Apr 2019) -- Start
            'NMB Enhancement - 76.1 - Error email setting on configuration.
            Session("AdministratorEmailForError") = clsConfig._AdministratorEmailForError
            'Sohail (10 Apr 2019) -- End


            'Gajanan |30-MAR-2019| -- START
            Session("AllowViewEmployeeScale") = clsConfig._AllowViewEmployeeScale
            'Gajanan |30-MAR-2019| -- END


            'Gajanan [8-April-2019] -- Start
            Session("AllowToAddEditEmployeePresentAddress") = clsConfig._AllowToAddEditEmployeePresentAddress
            Session("AllowToAddEditEmployeeDomicileAddress") = clsConfig._AllowToAddEditEmployeeDomicileAddress
            Session("AllowToAddEditEmployeeRecruitmentAddress") = clsConfig._AllowToAddEditEmployeeRecruitmentAddress

            'Gajanan [8-April-2019] -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Session("AllowToEditLeaveApplication") = clsConfig._AllowToEditLeaveApplication
            Session("AllowToDeleteLeaveApplication") = clsConfig._AllowToDeleteLeaveApplication
            'Pinkal (03-May-2019) -- End


            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            Session("ClaimRemarkMandatoryForClaim") = clsConfig._ClaimRemarkMandatoryForClaim
            'Pinkal (20-May-2019) -- End


            'Gajanan [27-May-2019] -- Start              
            Session("DomicileDocsAttachmentMandatory") = clsConfig._DomicileDocsAttachmentMandatory
            'Gajanan [27-May-2019] -- End

'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Session("ScoreCalibrationFormNotype") = clsConfig._ScoreCalibrationFormNotype
            Session("ScoreCalibrationFormNoPrifix") = clsConfig._ScoreCalibrationFormNoPrifix
            Session("NextScoreCalibrationFormNo") = clsConfig._NextScoreCalibrationFormNo
            Session("IsCalibrationSettingActive") = clsConfig._IsCalibrationSettingActive
            'S.SANDEEP |27-MAY-2019| -- END


            'Gajanan [13-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
            Session("EmployeeDisagreeGrievanceNotificationUserIds") = clsConfig._EmployeeDisagreeGrievanceNotificationUserIds
            Session("EmployeeDisagreeGrievanceNotificationEmailTitle") = clsConfig._EmployeeDisagreeGrievanceNotificationEmailTitle
            Session("EmployeeDisagreeGrievanceNotificationTemplateId") = clsConfig._EmployeeDisagreeGrievanceNotificationTemplateId
            'Gajanan [13-July-2019] -- End


            'Gajanan [30-JUL-2019] -- START
            'Enhancement [Ref # 3365 : Crown Paints - Kenya] : Include option to show “Leave Type” on self service Employee time sheet report.
            Session("EmpTimesheetSetting") = clsConfig._EmpTimesheetSetting
            'Gajanan [30-JUL-2019] -- END



            'Pinkal (13-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            Session("AllowEmpAssignedProjectExceedTime") = clsConfig._AllowEmpAssignedProjectExceedTime
            'Pinkal (13-Aug-2019) -- End

            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            Session("Notify_LoanAdvance_Users") = clsConfig._Notify_LoanAdvance_Users
            'Hemant (30 Aug 2019) -- End	

            'Pinkal (29-Aug-2019) -- Start
            'Enhancement NMB - Working on P2P Get Token Service URL.
            Session("GetTokenP2PServiceURL") = clsConfig._GetTokenP2PServiceURL
            'Pinkal (29-Aug-2019) -- End

            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 7 : During staff requisition, remove the condition that requires manpower planning must be done and provide a configurable option that can be turned and off depending on the needs. – TC001 ).
            Session("EnforceManpowerPlanning") = clsConfig._EnforceManpowerPlanning
            'Hemant (03 Sep 2019) -- End

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.
            Session("ClaimRetirementFormNoType") = clsConfig._ClaimRetirementFormNoType
            'Pinkal (11-Sep-2019) -- End

            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            Session("ShowInterdictionDate") = clsConfig._ShowInterdictionDate
            Session("NotificationOnDisciplineFiling") = clsConfig._NotificationOnDisciplineFiling
            Session("ShowReponseTypeOnPosting") = clsConfig._ShowReponseTypeOnPosting
            'S.SANDEEP |01-OCT-2019| -- END

            'Gajanan [1-NOV-2019] -- Start    
            'Enhancement:Worked On NMB ESS Comment  
            Session("PresentAddressDocsAttachmentMandatory") = clsConfig._PresentAddressDocsAttachmentMandatory
            Session("RecruitmentAddressDocsAttachmentMandatory") = clsConfig._RecruitmentAddressDocsAttachmentMandatory
            Session("IdentityDocsAttachmentMandatory") = clsConfig._IdentityDocsAttachmentMandatory
            Session("JobExperienceDocsAttachmentMandatory") = clsConfig._JobExperienceDocsAttachmentMandatory
            'Gajanan [1-NOV-2019] -- End

            'Pinkal (24-Oct-2019) -- Start
            'Enhancement NMB - Working On OT Enhancement for NMB.
            Session("ClaimRetirementTypeId") = clsConfig._ClaimRetirementTypeId

            'Pinkal (02-Jun-2020) -- Start
            'Enhancement NMB - Change OT Requisition Changes required by NMB and guided by Matthew.
            'Session("AllowApplyOTForEachMonthDay") = clsConfig._AllowApplyOTForEachMonthDay
            Session("OTTenureDays") = clsConfig._OTTenureDays
            'Pinkal (02-Jun-2020) -- End


            'Pinkal (24-Oct-2019) -- End

            'Gajanan [6-NOV-2019] -- Start    
            'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   


            'Gajanan [15-NOV-2019] -- Start   
            'Enhancement:Worked On Staff Requisition New Attachment Mandatory Option FOR NMB   
            'Session("StaffRequisitionDocsAttachmentMandatory") = clsConfig._StaffRequisitionDocsAttachmentMandatory
            Session("AdditionalStaffRequisitionDocsAttachmentMandatory") = clsConfig._AdditionalStaffRequisitionDocsAttachmentMandatory
            Session("AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory") = clsConfig._AdditionalAndReplacementStaffRequisitionDocsAttachmentMandatory
            Session("ReplacementStaffRequisitionDocsAttachmentMandatory") = clsConfig._ReplacementStaffRequisitionDocsAttachmentMandatory
            'Gajanan [15-NOV-2019] -- End

            'Gajanan [6-NOV-2019] -- End

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            Session("GrievanceReportingToMaxApprovalLevel") = clsConfig._GrievanceReportingToMaxApprovalLevel
            'Gajanan [24-OCT-2019] -- End

            'Pinkal (15-Nov-2019) -- Start
            'Enhancement  St. Jude[0004149]  - Employee Timesheet Report should be optional for viewing in Employee SelfService.
            Session("AllowToviewEmpTimesheetReportForESS") = clsConfig._AllowToviewEmpTimesheetReportForESS
            'Pinkal (15-Nov-2019) -- End

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : New Screen Training Need Form.
            Session("TrainingNeedFormNoType") = clsConfig._TrainingNeedFormNoType
            Session("TrainingNeedFormNoPrefix") = clsConfig._TrainingNeedFormNoPrefix
            'Sohail (14 Nov 2019) -- End


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            Session("ApplicableLeaveStatus") = clsConfig._ApplicableLeaveStatus
            'Pinkal (03-Jan-2020) -- End


            'Pinkal (30-Mar-2020) -- Start
            'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
            Session("AllowToCancelLeaveButRetainExpense") = clsConfig._AllowToCancelLeaveButRetainExpense
            'Pinkal (30-Mar-2020) -- End


            'Gajanan [28-May-2020] -- Start
            Session("AllowToViewScoreWhileDoingAssesment") = clsConfig._AllowToViewScoreWhileDoingAssesment
            'Gajanan [28-May-2020] -- End

 'Pinkal (30-May-2020) -- Start
            'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.
            Session("AllowToCancelLeaveForClosedPeriod") = clsConfig._AllowToCancelLeaveForClosedPeriod
            'Pinkal (30-May-2020) -- End

            'Gajanan [19-June-2020] -- Start
            'Enhancement NMB Employee Signature Enhancement.
            Session("AllowEmployeeToAddEditSignatureESS") = clsConfig._AllowEmployeeToAddEditSignatureESS
            'Gajanan [19-June-2020] -- End

'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Session("AllowOnlyOneExpenseInClaimApplication") = clsConfig._AllowOnlyOneExpenseInClaimApplication
            'Pinkal (30-May-2020) -- End

            'S.SANDEEP |27-JUN-2020| -- START
            'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
            Session("AllowTerminationIfPaymentDone") = clsConfig._AllowTerminationIfPaymentDone
            'S.SANDEEP |27-JUN-2020| -- END

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-75 #  : Search job page with apply button in ESS (same as MSS, only current open vacancies)..
            Session("applicant_declaration") = clsConfig._ApplicantDeclaration
            Session("VacancyFoundOutFromMandatory") = clsConfig._VacancyFoundOutFromMandatoryInRecruitment
            Session("EarliestPossibleStartDateMandatory") = clsConfig._EarliestPossibleStartDateMandatoryInRecruitment
            Session("CommentsMandatory") = clsConfig._CommentsMandatoryInRecruitment
            Session("isvacancyalert") = clsConfig._isVacancyAlert
            Session("issendjobconfirmationemail") = clsConfig._SendJobConfirmationEmail
            Session("isattachapplicantcv") = clsConfig._AttachApplicantCV
            Session("applicantunkid") = 0
            Session("LocalizationCountryUnkid") = clsConfig._CountryUnkid
            'Sohail (25 Sep 2020) -- End
            'Sohail (04 Nov 2020) -- Start
            'NMB Issue # : - Error Cannot insert duplicate key in applicant code column on import data in recruitment.
            Session("ApplicantCodeNotype") = clsConfig._ApplicantCodeNotype
            Session("ApplicantCodePrifix") = clsConfig._ApplicantCodePrifix
            'Sohail (04 Nov 2020) -- End

'Pinkal (12-Nov-2020) -- Start
            'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
            Session("SkipApprovalFlowForApplicantShortListing") = clsConfig._SkipApprovalFlowForApplicantShortListing
            Session("SkipApprovalFlowForFinalApplicant") = clsConfig._SkipApprovalFlowForFinalApplicant
            'Pinkal (12-Nov-2020) -- End

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            Session("IgnoreNegativeNetPayEmployeesOnJV") = clsConfig._IgnoreNegativeNetPayEmployeesOnJV
            'Sohail (23 Nov 2020) -- End

            'Hemant (23 Dec 2020) -- Start
            'Enhancement # OLD-222 : AFLIFE - Show remaining number of Installments on payslip template 13
            Session("ShowRemainingNoOfLoanInstallmentsOnPayslip") = clsConfig._ShowRemainingNoOfLoanInstallmentsOnPayslip
            'Hemant (23 Dec 2020) -- End


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Session("UnRetiredImprestToPayrollAfterDays") = clsConfig._UnRetiredImprestToPayrollAfterDays
            'Pinkal (10-Feb-2021) -- End

            'Sohail (07 Apr 2021) -- Start
            'TRA Enhancement : : Setting on configuration to hide Non-Dislcosure declaration link on menu for TRA.
            Session("ApplyNonDisclosureDeclaration") = clsConfig._ApplyNonDisclosureDeclaration
            'Sohail (07 Apr 2021) -- End

 'S.SANDEEP |17-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
            Session("DontAllowRatingBeyond100") = clsConfig._DontAllowRatingBeyond100
            'S.SANDEEP |17-MAY-2021| -- END

            'Pinkal (18-May-2021) -- Start
            'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
            Session("AppointeDateUserNotification") = clsConfig._AppointeDateUserNotification
            Session("ConfirmDateUserNotification") = clsConfig._ConfirmDateUserNotification
            Session("BirthDateUserNotification") = clsConfig._BirthDateUserNotification
            Session("SuspensionDateUserNotification") = clsConfig._SuspensionDateUserNotification
            Session("ProbationDateUserNotification") = clsConfig._ProbationDateUserNotification
            Session("EocDateUserNotification") = clsConfig._EocDateUserNotification
            Session("LeavingDateUserNotification") = clsConfig._LeavingDateUserNotification
            Session("RetirementDateUserNotification") = clsConfig._RetirementDateUserNotification
            Session("ReinstatementDateUserNotification") = clsConfig._ReinstatementDateUserNotification
            Session("MarriageDateUserNotification") = clsConfig._MarriageDateUserNotification
            Session("ExemptionDateUserNotification") = clsConfig._ExemptionDateUserNotification
            'Pinkal (18-May-2021) -- End

            'Hemant (24 Nov 2021) -- Start
            'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
            Session("SkipEOCValidationOnLoanTenure") = clsConfig._SkipEOCValidationOnLoanTenure
            'Hemant (24 Nov 2021) -- End

            'S.SANDEEP |10-AUG-2021| -- START
            Session("BSC_StatusColors") = clsConfig._BSC_StatusColors
            Session("ConsiderZeroAsNumUpdateProgress") = clsConfig._ConsiderZeroAsNumUpdateProgress
            'S.SANDEEP |10-AUG-2021| -- END
            '================================ END TO SET CONFIGURATION RELATED SESSIONS=====================================


            '================================ START TO SET COMPANY RELATED SESSIONS=====================================
            'Pinkal (11-Dec-2018) -- Start
            'Issue - Login page is taking time to login in NMB to have set on begining of this function.
            'Dim objCompany As New clsCompany_Master
            'objCompany._Companyunkid = intCompanyID


            ' Dim objCompany As New clsCompany_Master
            'objCompany._Companyunkid = intCompanyID

            'Pinkal (11-Dec-2018) -- End


            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            Session("CompanyCode") = objCompany._Code
            'Pinkal (20-Nov-2018) -- End

            Session("CompanyBankGroupId") = objCompany._Bankgroupunkid
            Session("CompanyBankBranchId") = objCompany._Branchunkid

            'Sohail (26 Feb 2018) -- Start
            'Issue : There is already an open datareader associated with this command which must be closed first. in 70.1.
            Session("Senderaddress") = objCompany._Senderaddress
            Session("Email_Type") = CInt(objCompany._Email_Type)
            Session("Mailserverip") = objCompany._Mailserverip
            Session("Mailserverport") = CInt(objCompany._Mailserverport)
            Session("SenderUsername") = objCompany._Username
            Session("SenderUserPassword") = objCompany._Password
            Session("Isloginssl") = CBool(objCompany._Isloginssl)
            Session("IsCertificateAuthorization") = CBool(objCompany._IsCertificateAuthorization)
            Session("EWS_Domain") = objCompany._EWS_Domain
            Session("EWS_URL") = objCompany._EWS_URL
            'S.SANDEEP [21-SEP-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002552}
            Session("ByPassProxy") = objCompany._Isbypassproxy
            'S.SANDEEP [21-SEP-2018] -- END
            'Sohail (18 Jul 2019) -- Start
            'Internal Enhancement - Ref #  - 76.1 - Show Sender Name instead of sender email in mail box.
            Session("SenderName") = objCompany._Sendername
            'Sohail (18 Jul 2019) -- End

            'Sohail (26 Feb 2018) -- End
            'Sohail (28 Mar 2018) -- Start
            'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            Dim a As Drawing.Bitmap
            If objCompany._Image IsNot Nothing Then
                a = objCompany._Image
            Else
                a = New Drawing.Bitmap(16, 16, Drawing.Imaging.PixelFormat.Format24bppRgb)
                a.MakeTransparent()
            End If
            Dim ms As New System.IO.MemoryStream
            a.Save(ms, Drawing.Imaging.ImageFormat.Png)
            Dim byteImage() As Byte = ms.ToArray
            Session("CompanyLogo") = Convert.ToBase64String(byteImage)
            'Sohail (28 Mar 2018) -- End


            '================================ END TO SET COMPANY RELATED SESSIONS=====================================



            Dim objMaster As New clsMasterData



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Session("UserAccessLevel") = objMaster.GetUserAccessLevel(CInt(Session("UserId")), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("UserAccessModeSetting"))
            'Shani(20-Nov-2015) -- End

            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                HttpContext.Current.Session("UserAccessLevel") = ""
            End If

            If Session("UserAccessLevel") = "" Then
                HttpContext.Current.Session("AccessLevelFilterString") = " AND 1 = 1  "

                HttpContext.Current.Session("AccessLevelBranchFilterString") = " "
                HttpContext.Current.Session("AccessLevelDepartmentGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelDepartmentFilterString") = " "
                HttpContext.Current.Session("AccessLevelSectionGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelSectionFilterString") = " "
                HttpContext.Current.Session("AccessLevelUnitGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelUnitFilterString") = " "
                HttpContext.Current.Session("AccessLevelTeamFilterString") = " "
                HttpContext.Current.Session("AccessLevelJobGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelJobFilterString") = " "
                HttpContext.Current.Session("AccessLevelClassGroupFilterString") = " "
                HttpContext.Current.Session("AccessLevelClassFilterString") = " "
            Else
                HttpContext.Current.Session("AccessLevelFilterString") = UserAccessLevel._AccessLevelFilterString

                HttpContext.Current.Session("AccessLevelBranchFilterString") = UserAccessLevel._AccessLevelBranchFilterString
                HttpContext.Current.Session("AccessLevelDepartmentGroupFilterString") = UserAccessLevel._AccessLevelDepartmentGroupFilterString
                HttpContext.Current.Session("AccessLevelDepartmentFilterString") = UserAccessLevel._AccessLevelDepartmentFilterString
                HttpContext.Current.Session("AccessLevelSectionGroupFilterString") = UserAccessLevel._AccessLevelSectionGroupFilterString
                HttpContext.Current.Session("AccessLevelSectionFilterString") = UserAccessLevel._AccessLevelSectionFilterString
                HttpContext.Current.Session("AccessLevelUnitGroupFilterString") = UserAccessLevel._AccessLevelUnitGroupFilterString
                HttpContext.Current.Session("AccessLevelUnitFilterString") = UserAccessLevel._AccessLevelUnitFilterString
                HttpContext.Current.Session("AccessLevelTeamFilterString") = UserAccessLevel._AccessLevelTeamFilterString
                HttpContext.Current.Session("AccessLevelJobGroupFilterString") = UserAccessLevel._AccessLevelJobGroupFilterString
                HttpContext.Current.Session("AccessLevelJobFilterString") = UserAccessLevel._AccessLevelJobFilterString
                HttpContext.Current.Session("AccessLevelClassGroupFilterString") = UserAccessLevel._AccessLevelClassGroupFilterString
                HttpContext.Current.Session("AccessLevelClassFilterString") = UserAccessLevel._AccessLevelClassFilterString
            End If

            'S.SANDEEP |04-AUG-2021| -- START
            Call SetIP_HOST()
            'Try
            '    If HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
            '        'Session("IP_ADD") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR")).AddressList(0).ToString
            '        'Session("HOST_NAME") = GetHostEntry(Request.ServerVariables("REMOTE_ADDR").ToString).HostName.ToString()
            '        HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            '    Else
            '        HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
            '        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            '    End If

            '    'Gajanan [29-July-2020] -- Start
            '    'Enhancement: Get Hostanme from Database 
            '    'If HttpContext.Current.Session("HOST_NAME") Is Nothing Then HttpContext.Current.Session("HOST_NAME") = ""
            '    'If HttpContext.Current.Session("IP_ADD") = IIf(HttpContext.Current.Session("HOST_NAME").ToString.Trim.Length <= 0, HttpContext.Current.Session("IP_ADD"), HttpContext.Current.Session("HOST_NAME")) Then
            '    '    Dim mstrhost As String = ""
            '    '    mstrhost = GetHostNameFromIp(HttpContext.Current.Session("IP_ADD"))
            '    '    If mstrhost.Trim().Length > 0 Then
            '    '        HttpContext.Current.Session("HOST_NAME") = mstrhost
            '    '    End If
            '    'End If
            '    'If HttpContext.Current.Session("HOST_NAME") Is Nothing Then HttpContext.Current.Session("HOST_NAME") = ""
            '    'If HttpContext.Current.Session("IP_ADD") = IIf(HttpContext.Current.Session("HOST_NAME").ToString.Trim.Length <= 0, HttpContext.Current.Session("IP_ADD"), HttpContext.Current.Session("HOST_NAME")) Then
                '    Dim mstrhost As String = ""
                '    mstrhost = GetHostNameFromIp(HttpContext.Current.Session("IP_ADD"))
                '    If mstrhost.Trim().Length > 0 Then
                '        HttpContext.Current.Session("HOST_NAME") = mstrhost
                '    End If
            '    'End If
            '    'Gajanan [29-July-2020] -- End

            '    'S.SANDEEP |15-AUG-2020| -- START
            '    'ISSUE/ENHANCEMENT : Log Error Only
            'Catch ex As System.Net.Sockets.SocketException
            '    CommonCodes.LogErrorOnly(ex)
            '    HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '    HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '    'S.SANDEEP |15-AUG-2020| -- END
            'Catch ex As Exception
            '    CommonCodes.LogErrorOnly(ex)
            '    HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            '    HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            'End Try
            'S.SANDEEP |04-AUG-2021| -- END

            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objMaster = Nothing
            clsConfig = Nothing
            objCompany = Nothing
            'Pinkal (11-Sep-2020) -- End


            Return True

        Catch ex As Exception
            strErrorMsg = "Error in SetCompanySessions : " & ex.Message
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Function

    Public Function GetCompanyYearInfo(ByRef strErrorMsg As String, ByVal strCompUnkId As Integer) As Boolean
        Dim objDataOperation As New clsDataOperation(True)
        Dim dsList As DataSet
        Dim mSQL As String = ""
        strErrorMsg = ""
        Try
            mSQL = "SELECT  cfcompany_master.companyunkid " & _
                               ", cfcompany_master.code AS CompCode " & _
                               ", cfcompany_master.name AS CompName " & _
                               ", cffinancial_year_tran.yearunkid " & _
                               ", cffinancial_year_tran.financialyear_name " & _
                               ", cffinancial_year_tran.database_name " & _
                               ", cffinancial_year_tran.start_date " & _
                               ", cffinancial_year_tran.end_date " & _
                               ", hrmsConfiguration..cfcompany_master.countryunkid " & _
                               ", cffinancial_year_tran.isclosed " & _
                   "FROM    hrmsConfiguration..cfcompany_master " & _
                                        "JOIN hrmsConfiguration..cffinancial_year_tran ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
                                        "AND cfcompany_master.isactive = 1 " & _
                                        "AND cffinancial_year_tran.isclosed = 0 " & _
                   "WHERE cfcompany_master.companyunkid = " & strCompUnkId & " "

            dsList = objDataOperation.WExecQuery(mSQL, "List")

            For Each dsRow As DataRow In dsList.Tables("List").Rows
                HttpContext.Current.Session("CompCode") = dsRow.Item("CompCode")
                HttpContext.Current.Session("CompName") = dsRow.Item("CompName")
                HttpContext.Current.Session("Fin_year") = dsRow.Item("yearunkid")
                HttpContext.Current.Session("FinancialYear_Name") = dsRow.Item("financialyear_name")
                HttpContext.Current.Session("Database_Name") = dsRow.Item("database_name")
                HttpContext.Current.Session("fin_startdate") = dsRow.Item("start_date")
                HttpContext.Current.Session("fin_enddate") = dsRow.Item("end_date")
                HttpContext.Current.Session("Compcountryid") = dsRow.Item("countryunkid")
                HttpContext.Current.Session("isclosed") = CBool(dsRow.Item("isclosed"))

            Next

            Return True

        Catch ex As Exception
            strErrorMsg = "Error in GetCompanyYearInfo : " & ex.Message
        End Try
    End Function

    Public Function IsAccessGivenUserEmp(ByRef strErrorMsg As String, ByVal intLoginBy As Integer, ByVal intUserEmpUnkID As Integer) As Boolean
        Dim objDataOperation As New clsDataOperation(True)
        Dim StrQ As String = String.Empty
        Dim dList As DataSet
        strErrorMsg = ""
        Try
            If (intLoginBy = Global.User.en_loginby.User) Then

                StrQ = "SELECT * FROM hrmsConfiguration..cfcompanyaccess_privilege " & _
                   " WHERE userunkid = " & intUserEmpUnkID & " AND yearunkid = " & CInt(Session("Fin_year")) & " "

            ElseIf (intLoginBy = Global.User.en_loginby.Employee) Then

                StrQ = "SELECT  employeeunkid, password FROM " & Session("Database_Name") & "..hremployee_master WHERE ISNULL(isapproved, 0) = 1 AND employeeunkid = " & intUserEmpUnkID & " "

            End If

            dList = objDataOperation.WExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                strErrorMsg = "basepage-->IsValidUserEmp Function" & objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
                Return False
            End If

            If dList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                If (intLoginBy = Global.User.en_loginby.User) Then
                    strErrorMsg = "Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator."
                ElseIf (intLoginBy = Global.User.en_loginby.Employee) Then
                    strErrorMsg = "Sorry, You cannot login to system. Reason : Either you are not belong to selected company or you are not approved yet. Please contact Administrator."
                End If
                Return False
            End If

        Catch ex As Exception
            Throw New Exception("basepage-->IsAccessGivenUserEmp Function" & ex.Message)
        End Try
    End Function
    'Sohail (30 Mar 2015) -- End


    'Pinkal (01-Apr-2015) -- Start
    'Enhancement - IMPLEMENT EMPLOYEE RE-CATEGORIZATION FOR TANAPA

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw New Exception("basepage-->b64encode Function" & ex.Message)
        End Try
    End Function

    Public Shared Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            Throw New Exception("basepage-->b64decode Function" & ex.Message)
        End Try
        Return decodedString
    End Function


    'Pinkal (01-Apr-2015) -- End

    'Nilay (28-Aug-2015) -- Start
    <WebMethod()> Public Shared Sub SetCashDenimination(ByVal intRowId As Integer, ByVal strColName As String, ByVal strValue As String)
        Try
            If HttpContext.Current.Session("PayCashDenomination") Is Nothing Then Exit Sub
            Dim dtCashDenomination As DataTable = CType(HttpContext.Current.Session("PayCashDenomination"), DataTable)
            If dtCashDenomination.Rows.Count >= intRowId Then
                If dtCashDenomination.Columns.Contains(strColName) = False Then Exit Sub
                dtCashDenomination.Rows(intRowId)(strColName) = CStr(IIf(strValue.Trim.Length > 0, strValue.Trim, "0"))
                HttpContext.Current.Session("PayCashDenomination") = dtCashDenomination
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Nilay (28-Aug-2015) -- End
    'Sohail (27 Apr 2013) -- Start
    'TRA - ENHANCEMENT
    <WebMethod()> Public Overloads Shared Function CheckLicence(ByVal strModuleName As String) As Boolean
        Try
            If ConfigParameter._Object Is Nothing Then
                If HttpContext.Current.Session("clsuser") Is Nothing Then
                    Return True
                Else
                    Return False
                End If
            End If
        If ConfigParameter._Object._IsArutiDemo = True Then 'IsDemo
            If ConfigParameter._Object._IsExpire = False Then
                    'Throw New Exception("Not Licsened")
                Return True
                'Else
                'Return False
            End If
            'If ArtLic._Object.DaysLeft <= -1 Then
            '    Return False
            'End If
            Return True
        End If

        Select Case strModuleName.Trim.ToUpper

            Case "PAYROLL_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                    Return False
                End If

            Case "LOAN_AND_SAVINGS_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                    Return False
                End If

            Case "EMPLOYEE_BIODATA_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                    Return False
                End If

            Case "LEAVE_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                    Return False
                End If

            Case "TIME_AND_ATTENDANCE_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                    Return False
                End If

            Case "EMPLOYEE_PERFORMANCE_APPRAISAL_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                    Return False
                End If

            Case "TRAININGS_NEEDS_ANALYSIS"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) = False Then
                    Return False
                End If

            Case "ON_JOB_TRAINING_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                    Return False
                End If

            Case "RECRUITMENT_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
                    Return False
                End If

            Case "ONLINE_JOB_APPLICATIONS_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Online_Job_Applications_Management) = False Then
                    Return False
                End If

            Case "EMPLOYEE_DISCIPLINARY_CASES_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                    Return False
                End If

            Case "MEDICAL_BILLS_AND_CLAIMS_MANAGEMENT"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
                    Return False
                End If

            Case "EMPLOYEE_SELF_SERVICE"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Self_Service) = False Then
                    Return False
                End If

            Case "MANAGER_SELF_SERVICE"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False Then
                    Return False
                End If

            Case "EMPLOYEE_ASSETS_DECLARATIONS"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
                    Return False
                End If

            Case "CUSTOM_REPORT_ENGINE"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Custom_Report_Engine) = False Then
                    Return False
                End If

            Case "INTEGRATION_ACCOUNTING_SYSTEM_ERPS"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Accounting_System_ERPs) = False Then
                    Return False
                End If

            Case "INTEGRATION_ELECTRONIC_BANKING_INTERFACE"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Electronic_Banking_Interface) = False Then
                    Return False
                End If

            Case "Integration_Time_Attendance_Devices"
                If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Integration_Time_Attendance_Devices) = False Then
                    Return False
                End If

            Case Else
                Return False
        End Select

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            InsertErrorLog("CheckLicence", ex)
            Return False
        End Try
        'Pinkal (13-Aug-2020) -- End

        Return True
    End Function
    'Sohail (27 Apr 2013) -- End

    'Sohail (24 Nov 2016) -- Start
    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.CheckLicence
    Public Shared Function UpdateThemeID(ByVal intThemeId As Integer) As Boolean
        Try
            If (HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(HttpContext.Current.Session("UserId"))
                objUser._Theme_Id = intThemeId
                objUser.Update(True)
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._LoginEmployeeUnkid = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._Theme_Id = intThemeId


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                '                                        CInt(HttpContext.Current.Session("Fin_year")), _
                '                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                '                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                '                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                '                                        CInt(HttpContext.Current.Session("UserId")), False, _
                '                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                '                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                '                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                '                                        ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
                '                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                '                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))


                objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                                                        CInt(HttpContext.Current.Session("Fin_year")), _
                                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                                                        CInt(HttpContext.Current.Session("UserId")), False, _
                                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                                                      ConfigParameter._Object._CurrentDateAndTime.Date, CBool(HttpContext.Current.Session("CreateADUserFromEmpMst")), _
                                                      CBool(HttpContext.Current.Session("UserMustchangePwdOnNextLogon")), , , _
                                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))

                'Pinkal (18-Aug-2018) -- End


            End If
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Throw New Exception("UpdateThemeID : " & ex.Message)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function

    Public Shared Function UpdateLastViewID(ByVal intLastViewId As Integer) As Boolean
        Try
            If (HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(HttpContext.Current.Session("UserId"))
                objUser._LastView_Id = intLastViewId
                objUser.Update(True)
            Else
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._LoginEmployeeUnkid = CInt(HttpContext.Current.Session("Employeeunkid"))
                objEmp._LastView_Id = intLastViewId


                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

                'objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                '                                        CInt(HttpContext.Current.Session("Fin_year")), _
                '                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                '                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                '                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                '                                        CInt(HttpContext.Current.Session("UserId")), False, _
                '                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                '                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                '                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                '                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                '                                        ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
                '                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                '                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))

                objEmp.Update(CStr(HttpContext.Current.Session("Database_Name")), _
                                                        CInt(HttpContext.Current.Session("Fin_year")), _
                                                        CInt(HttpContext.Current.Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(HttpContext.Current.Session("IsArutiDemo")), _
                                                        CInt(HttpContext.Current.Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(HttpContext.Current.Session("NoOfEmployees")), _
                                                        CInt(HttpContext.Current.Session("UserId")), False, _
                                                        CStr(HttpContext.Current.Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(HttpContext.Current.Session("IsIncludeInactiveEmp")), _
                                                        CBool(HttpContext.Current.Session("AllowToApproveEarningDeduction")), _
                                                      ConfigParameter._Object._CurrentDateAndTime.Date, CBool(HttpContext.Current.Session("CreateADUserFromEmpMst")), _
                                                      CBool(HttpContext.Current.Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(HttpContext.Current.Session("EmployeeAsOnDate")), , _
                                                        CBool(HttpContext.Current.Session("IsImgInDataBase")))

                'Pinkal (18-Aug-2018) -- End

            End If
        Catch ex As Exception
            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Throw New Exception("UpdateLastViewID :- " & ex.Message)
            'Pinkal (13-Aug-2020) -- End
        End Try
    End Function
    'Sohail (24 Nov 2016) -- End

    
    'Shani(01-MAR-2017) -- Start
    <System.Web.Services.WebMethod()> Public Shared Function Delete_File(ByVal strPath As String) As Boolean
        Try

            If System.IO.File.Exists(strPath) Then
                System.IO.File.Delete(strPath)
            End If
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    'Shani(01-MAR-2017) -- End

    'Pinkal (03-Apr-2017) -- Start
    'Enhancement - Working On Active directory Changes for PACRA.

    Public Function IsAuthenticated(ByRef strMsg As String, ByVal mstrIPAddress As String, ByVal mstrDomain As String, ByVal username As String, ByVal pwd As String) As Boolean
        Try

            If mstrDomain.Trim.Length <= 0 Then Return False

            Dim ar() As String = Nothing
            If mstrDomain.Trim.Length > 0 AndAlso mstrDomain.Trim.Contains(".") Then
                ar = mstrDomain.Trim.Split(CChar("."))
                mstrDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrDomain.Trim.Length > 0 Then
                mstrDomain = mstrDomain.Trim.Substring(1)
            End If

            Dim entry As DirectoryEntry = New DirectoryEntry("LDAP://" & mstrIPAddress.Trim & "/" & mstrDomain, username.Trim, pwd.Trim)

            'Bind to the native AdsObject to force authentication.
            Dim obj As Object = entry.NativeObject
            Dim search As DirectorySearcher = New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & username & ")"
            search.PropertiesToLoad.Add("cn")
            Dim result As SearchResult = search.FindOne()

            If (result Is Nothing) Then
                Return False
            End If


            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch exCOM As DirectoryServicesCOMException
            CommonCodes.LogErrorOnly(exCOM)
            'Pinkal (20-Aug-2020) -- End
        Catch ex As Exception
            If ex.Message.Trim.Contains("Unknown error (0x80005000)") Then
                'Pinkal (23-Nov-2018) -- Start
                'Enhancement - Changed Message As Per NMB Requirement.
                'strMsg = "IsAuthenticated : Invalid Username or Password!! Please try again !!!."
                strMsg = "Sorry, Invalid Username or Password. Please Provide correct Username or Password to login."
                'Pinkal (23-Nov-2018) -- End
            Else
                strMsg = "IsAuthenticated : " & ex.Message
            End If
            Return False
        End Try
        Return True
    End Function

    'Pinkal (03-Apr-2017) -- End

    'Sohail (05 Apr 2019) -- Start
    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client and Send error in email to aruti help desk.
    <WebMethod()> Public Shared Function SendErrorReport(ByVal strError As String) As String
        Dim objSendMail As New clsSendMail
        Dim strResult As String = String.Empty
        Try
            If HttpContext.Current.Session("AdministratorEmailForError") Is Nothing OrElse HttpContext.Current.Session("AdministratorEmailForError").ToString.Trim = "" Then
                System.Threading.Thread.Sleep(1000)
                Return "Administrator Email is not set. Please set Administrator Email on Aruti Configuration to Report Error."
                Exit Try
            End If
            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
            'Dim ser As New JavaScriptSerializer()
            'Dim objError As clsErrorlog_Tran = ser.Deserialize(Of clsErrorlog_Tran)(strError.Replace("""\/Date(", "").Replace(")\/""", ""))
            'S.SANDEEP |04-MAR-2020| -- END

            
            'Sohail (12 Apr 2019) -- Start
            'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
            'objSendMail._ToEmail = HttpContext.Current.Session("AdministratorEmailForError").ToString
            'objSendMail._ToEmail = "arutihelpdesk@aruti.com"
            'objSendMail._CCAddress = "anatory@npktechnologies.com"
            'objSendMail._BCCAddress = "anjan@ezeetechnosys.com"
            'Sohail (12 Apr 2019) -- Start
            'NMB Enhancement - 76.1 - Store System Errors in database and allow to resend and archieve system errors.
            'objSendMail._Subject = "Error in Aruti Self Service : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "Error : <B>" & strError & "</B>"
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Code : " & HttpContext.Current.Session("CompCode")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Name : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Database Version : " & HttpContext.Current.Session("DatabaseVersion")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Location : " & HttpContext.Current.Request.Url.AbsoluteUri.Replace("/SendErrorReport", "")
            'objSendMail._Message &= "<BR />"
            'If HttpContext.Current.Session("Firstname").ToString.Trim <> "" Then
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("Firstname").ToString & " " & HttpContext.Current.Session("Surname").ToString
            'Else
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("UserName").ToString
            'End If


            'strResult = objSendMail.SendMail(CInt(HttpContext.Current.Session("CompanyUnkId")))
            'objSendMail._Subject = "Error in Aruti Self Service : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "Error : <B>" & clsCrypto.Dicrypt(objError._Error_Message) & "</B>"
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Code : " & HttpContext.Current.Session("CompCode")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Company Name : " & HttpContext.Current.Session("CompName")
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Database Version : " & objError._Database_Version
            'objSendMail._Message &= "<BR />"
            'objSendMail._Message &= "Location : " & objError._Error_Location
            'objSendMail._Message &= "<BR />"
            'If HttpContext.Current.Session("Firstname").ToString.Trim <> "" Then
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("Firstname").ToString & " " & HttpContext.Current.Session("Surname").ToString
            'Else
            '    objSendMail._Message &= "Reported by : " & HttpContext.Current.Session("UserName").ToString
            'End If

            'strResult = objSendMail.SendMail(CInt(HttpContext.Current.Session("CompanyUnkId")))

            'If strResult.Trim = "" Then
            '    objError._Isemailsent = True
            '    If objError.Update(Nothing, objError) = False Then

            '    End If
            'End If

            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
            'Dim lstError As List(Of clsErrorlog_Tran) = objError.GetListCollection(Nothing, objError._Errorlogunkid)
            Dim objError As New clsErrorlog_Tran
            Dim lstError As List(Of clsErrorlog_Tran) = objError.GetListCollection(Nothing, CInt(strError))
            'S.SANDEEP |04-MAR-2020| -- END

            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                objError._LoginTypeId = enLogin_Mode.MGR_SELF_SERVICE
            Else
                objError._LoginTypeId = enLogin_Mode.EMP_SELF_SERVICE
            End If
            objError._Form_Name = "frmErrorLog"
            objError._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT

            If objError.SendEmail(Nothing, lstError) = False Then
                If objError._Message <> "" Then
                    Return objError._Message
                End If
            End If
            'Sohail (12 Apr 2019) -- End
            'Sohail (12 Apr 2019) -- End

            Return strResult

        Catch ex As Exception
            Return ex.Message
        End Try
    End Function
    'Sohail (05 Apr 2019) -- End

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Public Shared Function DownloadAllDocument(ByVal zipFilename As String, _
                                               ByVal dtTable As DataTable, _
                                               ByVal strSelfServiceURL As String, _
                                               Optional ByVal compression As CompressionOption = CompressionOption.Normal) As String
        Dim strMessage As String = String.Empty
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                If zipFilename.Trim.Length <= 0 Then zipFilename = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                Dim dfile As DataTable = Nothing
                If dtTable.Columns.Contains("AUD") Then
                    dfile = dtTable.Select("AUD" <> "D").CopyToDataTable()
                Else
                    dfile = dtTable
                End If
                If dfile IsNot Nothing AndAlso dfile.Rows.Count > 0 Then
                    If dfile.Columns.Contains("temppath") = False Then dfile.Columns.Add("temppath", Type.GetType("System.String"))
                    If dfile.Columns.Contains("error") = False Then dfile.Columns.Add("error", Type.GetType("System.String"))
                    Dim strError As String = String.Empty : Dim strLocalPath As String = String.Empty
                    Dim fileToAdd As New List(Of String)
                    For Each xRow As DataRow In dfile.Rows
                        If IsDBNull(xRow("file_data")) = False Then
                            strLocalPath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & IIf(xRow("fileuniquename").ToString.Trim.Length <= 0, xRow("filename").ToString, xRow("fileuniquename").ToString).ToString.Replace(" ", "")
                            Dim ms As New MemoryStream(CType(xRow("file_data"), Byte()))
                            Dim fs As New FileStream(strLocalPath, FileMode.Create)
                            ms.WriteTo(fs)
                            ms.Close()
                            fs.Close()
                            fs.Dispose()
                            If strLocalPath <> "" Then
                                fileToAdd.Add(strLocalPath)
                            End If
                        ElseIf IsDBNull(xRow("file_data")) = True Then
                            If xRow("temppath").ToString.Trim <> "" Then
                                If System.IO.File.Exists(xRow("temppath").ToString.Trim) Then
                                    strLocalPath = xRow("temppath").ToString.Trim
                                End If
                            Else
                                strLocalPath = xRow("filepath").ToString
                                strLocalPath = strLocalPath.Replace(strSelfServiceURL, "")
                                If Strings.Left(strLocalPath, 1) <> "/" Then
                                    strLocalPath = "~/" & strLocalPath
                                Else
                                    strLocalPath = "~" & strLocalPath
                                End If
                                strLocalPath = System.Web.HttpContext.Current.Server.MapPath(strLocalPath)
                                If strLocalPath <> "" Then
                                    If IO.File.Exists(strLocalPath) Then
                                        fileToAdd.Add(strLocalPath)
                                    End If
                                End If
                            End If
                        End If
                    Next
                    If fileToAdd.Count > 0 Then
                        strMessage = AddFileToZip(zipFilename, fileToAdd)
                    Else
                        strMessage = "No Files to download."
                    End If
                End If
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Exception
            strMessage = ex.Message
        End Try
        Return strMessage
    End Function

    Private Shared Function AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal) As String
        Dim strMessage As String = String.Empty
        Try
            If zipFilename.Trim.Length <= 0 Then zipFilename = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            Using ms As New MemoryStream()
                Using pkg As Package = Package.Open(ms, FileMode.Create)
                    For Each item As String In fileToAdd
                        Dim destFilename As String = "/" & Path.GetFileName(item)
                        Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                        Dim byt As Byte() = File.ReadAllBytes(item)

                        Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                            CopyStream(CType(fileStream, Stream), part.GetStream())
                        End Using
                    Next
                    pkg.Close()
                End Using
                HttpContext.Current.Response.Clear()
                HttpContext.Current.Response.ContentType = "application/zip"
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
                HttpContext.Current.Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.End()
            End Using
        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception
            strMessage = ex.Message
        Finally
        End Try
        Return strMessage
    End Function

    Private Shared Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop

            'Pinkal (13-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        Catch ex As Exception
            Throw New Exception("CopyStream :- " & ex.Message)
        End Try
        'Pinkal (13-Aug-2020) -- End
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END



    'Pinkal (24-Oct-2019) -- Start
    'Enhancement NMB - Working On OT Enhancement for NMB.

    Public Function CalculateNightHrs(ByVal xShiftID As Integer, ByVal mdtStarttime As DateTime, ByVal mdtEndtime As DateTime) As Integer
        Dim mintTotalNighthrs As Integer = 0
        Try

            Dim objshift As New clsshift_tran
            objshift.GetShiftTran(xShiftID)

            Dim mdtNightFrmHrs As DateTime = objshift._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(mdtStarttime.Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("nightfromhrs")).Distinct().First()
            Dim mdtNightToHrs As DateTime = objshift._dtShiftday.AsEnumerable().Where(Function(x) x.Field(Of Integer)("dayid") = GetWeekDayNumber(WeekdayName(Weekday(mdtStarttime.Date), False, FirstDayOfWeek.Sunday))).Select(Function(x) x.Field(Of DateTime)("nighttohrs")).Distinct().First()

            If mdtStarttime.Date <> Nothing AndAlso (mdtNightFrmHrs <> Nothing AndAlso mdtNightToHrs <> Nothing) Then

                If DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtNightToHrs.AddDays(1)) > 0 AndAlso mdtNightFrmHrs <> mdtNightToHrs Then

                    mdtNightFrmHrs = CDate(mdtStarttime.Date & " " & mdtNightFrmHrs.ToShortTimeString())
                    mdtNightToHrs = CDate(mdtEndtime.Date & " " & mdtNightToHrs.ToShortTimeString())

                    If mdtNightToHrs < mdtNightFrmHrs Then
                        mdtNightToHrs = CDate(mdtStarttime.Date.AddDays(1) & " " & mdtNightToHrs.ToShortTimeString())
                    End If

                    If mdtStarttime <> Nothing AndAlso mdtEndtime <> Nothing Then
                        If mdtStarttime <= mdtNightFrmHrs AndAlso mdtEndtime >= mdtNightToHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtNightToHrs))

                        ElseIf mdtStarttime <= mdtNightFrmHrs AndAlso mdtEndtime <= mdtNightToHrs AndAlso mdtEndtime >= mdtNightFrmHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtEndtime))

                        ElseIf mdtStarttime >= mdtNightFrmHrs AndAlso mdtEndtime <= mdtNightToHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtStarttime, mdtEndtime))

                        ElseIf mdtStarttime >= mdtNightFrmHrs AndAlso mdtEndtime >= mdtNightToHrs Then
                            mintTotalNighthrs = CInt(DateDiff(DateInterval.Second, mdtStarttime, mdtNightToHrs))
                        End If

                    End If   '  If mdtStarttime <> Nothing AndAlso mdtEndtime <> Nothing Then

                End If  ' If DateDiff(DateInterval.Second, mdtNightFrmHrs, mdtNightToHrs.AddDays(1)) > 0 AndAlso mdtNightFrmHrs <> mdtNightToHrs Then

            End If  ' If mdtStarttime.Date <> Nothing AndAlso (mdtNightFrmHrs <> Nothing AndAlso mdtNightToHrs <> Nothing) Then


            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            If mintTotalNighthrs < 0 Then mintTotalNighthrs = 0
            'Pinkal (08-Jan-2020) -- End

        Catch ex As Exception
            'Sohail (01 Feb 2020) -- Start
            'Enhancement # : Passing Exception object in DisplayError in self service.
            'DisplayMessage.DisplayError(ex.Message,Me)
            DisplayMessage.DisplayError(ex.Message, Me)
            'Sohail (01 Feb 2020) -- End
        End Try
        Return mintTotalNighthrs
    End Function

    'Pinkal (24-Oct-2019) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : New Screen Training Need Form.
    <WebMethod()> Public Shared Function ConvertToCurrency(ByVal strNumber As String) As String
        Try
            Dim dec As Decimal
            Decimal.TryParse(strNumber, dec)

            Return Format(dec, "##,##,##,##0.00")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Sohail (14 Nov 2019) -- End

    'S.SANDEEP |17-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PM ERROR
    Public Shared Sub KillIdleSQLSessions()
        'Dim objCONN As New SqlClient.SqlConnection
        'Try
        '    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
        '    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
        '    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
        '    objCONN.ConnectionString = constr
        '    objCONN.Open()

        '    Dim oSQL As String = ""

        '    oSQL = "DECLARE @user_spid INT " & _
        '           "DECLARE CurSPID CURSOR FAST_FORWARD " & _
        '           "FOR " & _
        '           "    SELECT SPID FROM master.dbo.sysprocesses(NOLOCK) WHERE spid > 50 AND STATUS = 'sleeping' AND DATEDIFF(MINUTE, last_batch, GETDATE()) >= 30 AND spid <> @@spid " & _
        '           "OPEN CurSPID " & _
        '           "FETCH NEXT " & _
        '           "FROM CurSPID " & _
        '           "INTO @user_spid " & _
        '           "WHILE (@@FETCH_STATUS = 0) " & _
        '           "    BEGIN " & _
        '           "        EXEC ('KILL ' + @user_spid) " & _
        '           "        FETCH NEXT " & _
        '           "        FROM CurSPID " & _
        '           "        INTO @user_spid " & _
        '           "    END " & _
        '           "CLOSE CurSPID " & _
        '           "DEALLOCATE CurSPID "

        '    Using oCmd As New SqlClient.SqlCommand
        '        oCmd.CommandText = oSQL
        '        oCmd.CommandType = CommandType.Text
        '        oCmd.Connection = objCONN
        '        oCmd.ExecuteNonQuery()
        '    End Using

        'Using objCONN As New SqlClient.SqlConnection
        '    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
        '    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
        '    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
        '    objCONN.ConnectionString = constr
        '    objCONN.Open()

        '    Dim oSQL As String = ""

        '    oSQL = "DECLARE @user_spid INT " & _
        '           "DECLARE CurSPID CURSOR FAST_FORWARD " & _
        '           "FOR " & _
        '           "    SELECT SPID FROM master.dbo.sysprocesses(NOLOCK) WHERE spid > 50 AND STATUS = 'sleeping' AND DATEDIFF(MINUTE, last_batch, GETDATE()) >= 30 AND spid <> @@spid " & _
        '           "OPEN CurSPID " & _
        '           "FETCH NEXT " & _
        '           "FROM CurSPID " & _
        '           "INTO @user_spid " & _
        '           "WHILE (@@FETCH_STATUS = 0) " & _
        '           "    BEGIN " & _
        '           "        EXEC ('KILL ' + @user_spid) " & _
        '           "        FETCH NEXT " & _
        '           "        FROM CurSPID " & _
        '           "        INTO @user_spid " & _
        '           "    END " & _
        '           "CLOSE CurSPID " & _
        '           "DEALLOCATE CurSPID "

        '    Using oCmd As New SqlClient.SqlCommand
        '        oCmd.CommandText = oSQL
        '        oCmd.CommandType = CommandType.Text
        '        oCmd.Connection = objCONN
        '        oCmd.ExecuteNonQuery()
        '    End Using

        '    'Using objCONN As New SqlClient.SqlConnection
        '    '    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
        '    '    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
        '    '    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
        '    '    objCONN.ConnectionString = constr
        '    '    objCONN.Open()

        '    '    Dim oSQL As String = ""

        '    '    oSQL = "DECLARE @user_spid INT " & _
        '    '           "DECLARE CurSPID CURSOR FAST_FORWARD " & _
        '    '           "FOR " & _
        '    '           "    SELECT SPID FROM master.dbo.sysprocesses(NOLOCK) WHERE spid > 50 AND STATUS = 'sleeping' AND DATEDIFF(MINUTE, last_batch, GETDATE()) >= 30 AND spid <> @@spid " & _
        '    '           "OPEN CurSPID " & _
        '    '           "FETCH NEXT " & _
        '    '           "FROM CurSPID " & _
        '    '           "INTO @user_spid " & _
        '    '           "WHILE (@@FETCH_STATUS = 0) " & _
        '    '           "    BEGIN " & _
        '    '           "        EXEC ('KILL ' + @user_spid) " & _
        '    '           "        FETCH NEXT " & _
        '    '           "        FROM CurSPID " & _
        '    '           "        INTO @user_spid " & _
        '    '           "    END " & _
        '    '           "CLOSE CurSPID " & _
        '    '           "DEALLOCATE CurSPID "

        '    '    Using oCmd As New SqlClient.SqlCommand
        '    '        oCmd.CommandText = oSQL
        '    '        oCmd.CommandType = CommandType.Text
        '    '        oCmd.Connection = objCONN
        '    '        oCmd.ExecuteNonQuery()
        '    '    End Using

        '    '    objCONN.Close()
        '    '    SqlClient.SqlConnection.ClearPool(objCONN)

        '    'End Using
        'Catch ex As Exception
        '    'Pinkal (13-Aug-2020) -- Start
        '    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
        '    Throw New Exception(ex.Message)
        '    'Pinkal (13-Aug-2020) -- End
        'Finally
        '    If objCONN.State = ConnectionState.Open Then
        '    objCONN.Close()
        '    End If
        '    objCONN.Dispose()
        'End Try
    End Sub
    'S.SANDEEP |17-MAR-2020| -- END


    'Gajanan [29-July-2020] -- Start
    'Enhancement: Get Hostanme from Database 
    Public Function GetHostNameFromIp(ByVal mstrIp As String) As String
        Try
            Dim exists As Boolean = False
            Dim mstrHostAddress As String = ""

            Using objCONN As New SqlClient.SqlConnection
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN.ConnectionString = constr
                objCONN.Open()

                Dim oSQL As String = ""

                oSQL = "SELECT CASE WHEN OBJECT_ID('hrmsConfiguration.dbo.cfdnslookup', 'U') IS NOT NULL THEN 1 ELSE 0 END "
                Using oCmd As New SqlClient.SqlCommand
                    oCmd.CommandText = oSQL
                    oCmd.CommandType = CommandType.Text
                    oCmd.Connection = objCONN
                    exists = CBool(oCmd.ExecuteScalar() = 1)
                End Using

                If exists Then
                    oSQL = "Select machine_name from hrmsConfiguration..cfdnslookup where ip_address = @ip_address "

                    Using oCmd As New SqlClient.SqlCommand
                        oCmd.CommandText = oSQL
                        oCmd.Parameters.AddWithValue("@ip_address", mstrIp)
                        oCmd.CommandType = CommandType.Text
                        oCmd.Connection = objCONN
                        If IsNothing(oCmd.ExecuteScalar()) = False Then
                            mstrHostAddress = oCmd.ExecuteScalar().ToString()
                        End If
                    End Using
                End If
                objCONN.Close()
                SqlClient.SqlConnection.ClearPool(objCONN)
            End Using
            Return mstrHostAddress
        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Gajanan [29-July-2020] -- End


    'Pinkal (13-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    <WebMethod()> Public Shared Function InsertErrorLog(ByVal mstrMethodName As String, ByVal ex As Exception) As Boolean
        Dim objErrorLog As New clsErrorlog_Tran
        With objErrorLog
            Dim S_dispmsg As String = "Method : " & mstrMethodName & ", Error :  " & ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                S_dispmsg &= "; " & ex.InnerException.Message
            End If
            S_dispmsg = S_dispmsg.Replace("'", "")
            S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
            ._Error_Message = S_dispmsg
            ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
            ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
            ._Companyunkid = HttpContext.Current.Session("Companyunkid")
            If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                ._Database_Version = ""
            Else
                ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
            End If
            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                ._Userunkid = HttpContext.Current.Session("UserId")
                ._Loginemployeeunkid = 0
            Else
                ._Userunkid = 0
                ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
            End If
            ._Isemailsent = False
            ._Isweb = True
            ._Isvoid = False
            ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
            ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
        End With
        Dim strDBName As String = ""
        If HttpContext.Current.Session("mdbname") IsNot Nothing Then
            If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                strDBName = "hrmsConfiguration"
            End If
        End If
        If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then
            Return False
        End If
        Return True
    End Function
    'Pinkal (13-Aug-2020) -- End

    'Sohail (06 Nov 2020) -- Start
    'NMB Enhancement # : - Make navigation path clickable to navigate on respective page on each mandatory filds message on search job page in ESS.
    <WebMethod()> Public Shared Sub SetRecruitmentAttachmentSession()
        Try
            HttpContext.Current.Session("ScanAttchLableCaption") = "Select Employee"
            HttpContext.Current.Session("ScanAttchRefModuleID") = enImg_Email_RefId.Applicant_Job_Vacancy
            HttpContext.Current.Session("ScanAttchenAction") = enAction.ADD_ONE
            HttpContext.Current.Session("ScanAttchToEditIDs") = ""
        Catch ex As Exception
            InsertErrorLog("SetRecruitmentAttachmentSession", ex)
        End Try
    End Sub
    'Sohail (06 Nov 2020) -- End


    'Pinkal (23-Jul-2021)-- Start
    'Dashboard Performance Issue.

    Public Shared Function SetWebLanguage(ByVal mstrDatabaseName As String, ByVal mstrWebFormName As String, ByVal mintLanguageId As Integer, ByVal mstrControlId As String, ByVal mstrControlText As String) As String
        Dim mstrLanguage As String = ""
        Try
            Using clsDataOpr As New clsDataOperation(True)

                Dim strQ As String = " SELECT language " & _
                                               ", language1 " & _
                                               ", language2 " & _
                                               "  FROM " & mstrDatabaseName & "..cflanguage WHERE  formname = @formname AND controlname = @controlname "

                clsDataOpr.ClearParameters()
                clsDataOpr.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
                clsDataOpr.AddParameter("@controlname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlId)
                Dim dsList As DataSet = clsDataOpr.WExecQuery(strQ, "Language")

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If mintLanguageId = 1 Then
                        mstrLanguage = dsList.Tables(0).Rows(0)("language1").ToString()
                    ElseIf mintLanguageId = 2 Then
                        mstrLanguage = dsList.Tables(0).Rows(0)("language2").ToString()
                    Else
                        mstrLanguage = dsList.Tables(0).Rows(0)("language").ToString()
                    End If
                End If

                If mstrLanguage.Trim.Length <= 0 Then mstrLanguage = mstrControlText

            End Using
        Catch ex As Exception
            InsertErrorLog("SetWebLanguage", ex)
        End Try
        Return mstrLanguage
    End Function

    Public Shared Sub SetWebCaption(ByVal mstrDatabaseName As String, ByVal mstrWebFormName As String, ByVal mstrControlId As String, ByVal mstrControlText As String)
        Try
            Using clsDataOpr As New clsDataOperation(True)
                Dim strQ As String = " INSERT INTO " & mstrDatabaseName & "..cflanguage " & _
                                               " ( " & _
                                               "  formname " & _
                                               ", controlname " & _
                                               ", language " & _
                                               ", language1 " & _
                                               ", language2 " & _
                                               " ) " & _
                                               " SELECT " & _
                                               "  @formname " & _
                                               ", @controlname " & _
                                               ", @language " & _
                                               ", @language1 " & _
                                               ", @language2 " & _
                                               " WHERE NOT EXIST " & _
                                               " ( " & _
                                               "   SELECT * FROM " & mstrDatabaseName & "..cflanguage WHERE formname = @formname AND controlname = @controlname " & _
                                               " ); "

                clsDataOpr.ClearParameters()
                clsDataOpr.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
                clsDataOpr.AddParameter("@controlname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlId)
                clsDataOpr.AddParameter("@language", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlText)
                clsDataOpr.AddParameter("@language1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlText)
                clsDataOpr.AddParameter("@language2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrControlText)
                clsDataOpr.WExecQuery(strQ, "Language")
            End Using
        Catch ex As Exception
            InsertErrorLog("SetWebCaption", ex)
        End Try
    End Sub

    'Pinkal (23-Jul-2021) -- End

    'S.SANDEEP |04-AUG-2021| -- START
    Public Sub SetIP_HOST()
        Try
            If HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            Else
                HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(HttpContext.Current.Request.ServerVariables("REMOTE_HOST")).HostName
            End If

            Dim mstrhost As String = ""
            mstrhost = GetHostNameFromIp(HttpContext.Current.Session("IP_ADD"))
            If mstrhost.Trim().Length > 0 Then
                HttpContext.Current.Session("HOST_NAME") = mstrhost
            End If
        Catch ex As System.Net.Sockets.SocketException
            CommonCodes.LogErrorOnly(ex)
            HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
            HttpContext.Current.Session("IP_ADD") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
            HttpContext.Current.Session("HOST_NAME") = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR").ToString
        End Try        
    End Sub
    'S.SANDEEP |04-AUG-2021| -- END

End Class
