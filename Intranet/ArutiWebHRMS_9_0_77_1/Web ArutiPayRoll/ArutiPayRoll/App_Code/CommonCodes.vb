﻿Imports Microsoft.VisualBasic
Imports System.Web.UI
Imports System.Net
Imports Aruti.Data
Imports System.Data
Imports System.Web
Imports System.Web.Script.Serialization
Imports System.Diagnostics

Public Class CommonCodes
    Dim objDataOperation As New eZeeCommonLib.clsDataOperation(True)
    Dim mSQL As String

    'Pinkal (20-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    Dim mstrErrorMsg As String = ""
    'Pinkal (20-Aug-2020) -- End


    Public Sub PkgDtaStrCommon()
        Try
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='hrskill_master') alter table hrskill_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcapplicant_master') alter table rcapplicant_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcapplicantqualification_tran') alter table rcapplicantqualification_tran add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcapplicantskill_tran') alter table rcapplicantskill_tran add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcjobhistory') alter table rcjobhistory add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfcommon_master') alter table cfcommon_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='rcvacancy_master') alter table rcvacancy_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfcity_master') alter table cfcity_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfstate_master') alter table cfstate_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "if not exists(select 1 from sys.columns where name='comp_code' and object_name(object_id)='cfcountry_master') alter table cfcountry_master add comp_code varchar(10)"
            objDataOperation.ExecNonQuery(mSQL)

            mSQL = ""

            mSQL = " if exists(select 1 from sys.procedures where name='Applicate_Insert') drop proc Applicate_Insert" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)

            mSQL = "Create procedure Applicate_Insert (" & vbCrLf
            mSQL += "@applicant_code cName," & vbCrLf
            mSQL += "@titleunkid int," & vbCrLf
            mSQL += "@firstname cName," & vbCrLf
            mSQL += "@surname cName," & vbCrLf
            mSQL += "@othername cName," & vbCrLf
            mSQL += "@gender cGender," & vbCrLf
            mSQL += "@email cEmail," & vbCrLf
            mSQL += "@present_address1 cAddress," & vbCrLf
            mSQL += "@present_address2 cAddress," & vbCrLf
            mSQL += "@present_countryunkid int," & vbCrLf
            mSQL += "@present_stateunkid int," & vbCrLf
            mSQL += "@present_province cState," & vbCrLf
            mSQL += "@present_post_townunkid int," & vbCrLf
            mSQL += "@present_zipcode int," & vbCrLf
            mSQL += "@present_road cName," & vbCrLf
            mSQL += "@present_estate cName," & vbCrLf
            mSQL += "@present_plotno cCardNo," & vbCrLf
            mSQL += "@present_mobileno  cCardNo," & vbCrLf
            mSQL += "@present_alternateno  cCardNo," & vbCrLf
            mSQL += "@present_tel_no cPhone," & vbCrLf
            mSQL += "@present_fax cPhone," & vbCrLf
            mSQL += "@perm_address1 cAddress," & vbCrLf
            mSQL += "@perm_address2 cAddress," & vbCrLf
            mSQL += "@perm_countryunkid int," & vbCrLf
            mSQL += "@perm_stateunkid int," & vbCrLf
            mSQL += "@perm_province cState," & vbCrLf
            mSQL += "@perm_post_townunkid int," & vbCrLf
            mSQL += "@perm_zipcode int," & vbCrLf
            mSQL += "@perm_road cState," & vbCrLf
            mSQL += "@perm_estate cState," & vbCrLf
            mSQL += "@perm_plotno cCardNo," & vbCrLf
            mSQL += "@perm_mobileno cCardNo," & vbCrLf
            mSQL += "@perm_alternateno cCardNo," & vbCrLf
            mSQL += "@perm_tel_no cPhone," & vbCrLf
            mSQL += "@perm_fax cPhone," & vbCrLf
            mSQL += "@birth_date datetime," & vbCrLf
            mSQL += "@marital_statusunkid int," & vbCrLf
            mSQL += "@anniversary_date datetime," & vbCrLf
            mSQL += "@language1unkid int," & vbCrLf
            mSQL += "@language2unkid int," & vbCrLf
            mSQL += "@language3unkid int," & vbCrLf
            mSQL += "@language4unkid int," & vbCrLf
            mSQL += "@nationality int," & vbCrLf
            mSQL += "@userunkid int," & vbCrLf
            mSQL += "@vacancyunkid int," & vbCrLf
            mSQL += "@isimport  bit," & vbCrLf
            mSQL += "@Comp_Code nvarchar(255)," & vbCrLf
            mSQL += "@Syncdatetime datetime," & vbCrLf
            mSQL += "@ImageName nvarchar(100)," & vbCrLf
            mSQL += "@Skill xml," & vbCrLf
            mSQL += "@Qualification Xml," & vbCrLf
            mSQL += "@Job xml  )" & vbCrLf
            mSQL += "as   " & vbCrLf
            mSQL += "DECLARE @TranName varchar , @ErrNo int , @mApplicantID int   " & vbCrLf
            mSQL += "set @TranName = 'SaveApplicate'  " & vbCrLf
            mSQL += "print  @applicant_code" & vbCrLf
            mSQL += " if exists(select applicant_code from rcApplicant_master where  applicant_code = @applicant_code)   " & vbCrLf
            mSQL += "begin" & vbCrLf
            mSQL += "select @applicant_code = max(applicant_code)  from rcapplicant_master " & vbCrLf
            mSQL += "select @applicant_code = @applicant_code + 1   " & vbCrLf
            mSQL += "End" & vbCrLf
            mSQL += "begin tran @TranName   " & vbCrLf
            mSQL += "SET @ErrNo = @@ERROR     " & vbCrLf
            mSQL += "IF @ErrNo <> 0 GOTO Err   " & vbCrLf
            mSQL += "begin" & vbCrLf
            mSQL += "INSERT  INTO rcapplicant_master" & vbCrLf
            mSQL += "(applicant_code ," & vbCrLf
            mSQL += "titleunkid ," & vbCrLf
            mSQL += "firstname ," & vbCrLf
            mSQL += "surname ," & vbCrLf
            mSQL += "othername ," & vbCrLf
            mSQL += "gender ," & vbCrLf
            mSQL += "email ," & vbCrLf
            mSQL += "present_address1 ," & vbCrLf
            mSQL += "present_address2 ," & vbCrLf
            mSQL += "present_countryunkid ," & vbCrLf
            mSQL += "present_stateunkid ," & vbCrLf
            mSQL += "present_province ," & vbCrLf
            mSQL += "present_post_townunkid ," & vbCrLf
            mSQL += "present_zipcode ," & vbCrLf
            mSQL += "present_road ," & vbCrLf
            mSQL += "present_estate ," & vbCrLf
            mSQL += "present_plotno ," & vbCrLf
            mSQL += "present_mobileno  ," & vbCrLf
            mSQL += "present_alternateno  ," & vbCrLf
            mSQL += "present_tel_no ," & vbCrLf
            mSQL += "present_fax ," & vbCrLf
            mSQL += "perm_address1 ," & vbCrLf
            mSQL += "perm_address2 ," & vbCrLf
            mSQL += "perm_countryunkid ," & vbCrLf
            mSQL += "perm_stateunkid ," & vbCrLf
            mSQL += "perm_province ," & vbCrLf
            mSQL += "perm_post_townunkid ," & vbCrLf
            mSQL += "perm_zipcode ," & vbCrLf
            mSQL += "perm_road ," & vbCrLf
            mSQL += "perm_estate ," & vbCrLf
            mSQL += "perm_plotno ," & vbCrLf
            mSQL += "perm_mobileno ," & vbCrLf
            mSQL += "perm_alternateno ," & vbCrLf
            mSQL += "perm_tel_no ," & vbCrLf
            mSQL += "perm_fax ," & vbCrLf
            mSQL += "birth_date ," & vbCrLf
            mSQL += "marital_statusunkid ," & vbCrLf
            mSQL += "anniversary_date ," & vbCrLf
            mSQL += "language1unkid ," & vbCrLf
            mSQL += "language2unkid ," & vbCrLf
            mSQL += "language3unkid ," & vbCrLf
            mSQL += "language4unkid ," & vbCrLf
            mSQL += "nationality ," & vbCrLf
            mSQL += "userunkid ," & vbCrLf
            mSQL += "vacancyunkid ," & vbCrLf
            mSQL += "isimport  ," & vbCrLf
            mSQL += "Comp_Code," & vbCrLf
            mSQL += "Syncdatetime )" & vbCrLf
            mSQL += "values (@applicant_code ," & vbCrLf
            mSQL += "@titleunkid ," & vbCrLf
            mSQL += "@firstname ," & vbCrLf
            mSQL += "@surname ," & vbCrLf
            mSQL += "@othername ," & vbCrLf
            mSQL += "@gender ," & vbCrLf
            mSQL += "@email ," & vbCrLf
            mSQL += "@present_address1 ," & vbCrLf
            mSQL += "@present_address2 ," & vbCrLf
            mSQL += "@present_countryunkid ," & vbCrLf
            mSQL += "@present_stateunkid ," & vbCrLf
            mSQL += "@present_province ," & vbCrLf
            mSQL += "@present_post_townunkid ," & vbCrLf
            mSQL += "@present_zipcode ," & vbCrLf
            mSQL += "@present_road ," & vbCrLf
            mSQL += "@present_estate ," & vbCrLf
            mSQL += "@present_plotno ," & vbCrLf
            mSQL += "	@present_mobileno  ," & vbCrLf
            mSQL += "@present_alternateno  ," & vbCrLf
            mSQL += "@present_tel_no ," & vbCrLf
            mSQL += "@present_fax ," & vbCrLf
            mSQL += "@perm_address1 ," & vbCrLf
            mSQL += "@perm_address2 ," & vbCrLf
            mSQL += "	@perm_countryunkid ," & vbCrLf
            mSQL += "	@perm_stateunkid ," & vbCrLf
            mSQL += "	@perm_province ," & vbCrLf
            mSQL += "	@perm_post_townunkid ," & vbCrLf
            mSQL += "	@perm_zipcode ," & vbCrLf
            mSQL += "	@perm_road ," & vbCrLf
            mSQL += "	@perm_estate ," & vbCrLf
            mSQL += "	@perm_plotno ," & vbCrLf
            mSQL += "	@perm_mobileno ," & vbCrLf
            mSQL += "	@perm_alternateno ," & vbCrLf
            mSQL += "		@perm_tel_no ," & vbCrLf
            mSQL += "		@perm_fax ," & vbCrLf
            mSQL += "		@birth_date ," & vbCrLf
            mSQL += "		@marital_statusunkid ," & vbCrLf
            mSQL += "		@anniversary_date ," & vbCrLf
            mSQL += "		@language1unkid ," & vbCrLf
            mSQL += "	@language2unkid ," & vbCrLf
            mSQL += "	@language3unkid ," & vbCrLf
            mSQL += "	@language4unkid ," & vbCrLf
            mSQL += "	@nationality ," & vbCrLf
            mSQL += "	@userunkid ," & vbCrLf
            mSQL += "	@vacancyunkid ," & vbCrLf
            mSQL += "	@isimport  ," & vbCrLf
            mSQL += "    @Comp_Code," & vbCrLf
            mSQL += "	@Syncdatetime )" & vbCrLf
            mSQL += "	 set @mApplicantID= (select max(applicantunkid) as applicantunkid  from rcApplicant_master )  " & vbCrLf
            mSQL += "	if exists(select 1 from @Skill.nodes('NewDataSet/Skill') pd(x))   " & vbCrLf
            mSQL += "begin" & vbCrLf
            mSQL += "	insert into rcapplicantskill_tran" & vbCrLf
            mSQL += "		(applicantunkid,skillcategoryunkid,skillunkid,remark,Comp_Code)     " & vbCrLf
            mSQL += "	select @mApplicantID   " & vbCrLf
            mSQL += "   ,x.value('(skillcategoryunkid)[1]' , 'int') as skillcategoryunkid      " & vbCrLf
            mSQL += "	,x.value('(skillunkid)[1]','int') as skillunkid" & vbCrLf
            mSQL += "	,x.value('(remark)[1]','cDescription') as remark" & vbCrLf
            mSQL += "       ,x.value('(Comp_code)[1]','nvarchar(50)') as Comp_Code" & vbCrLf
            mSQL += "		 from @Skill.nodes('NewDataSet/Skill') Ld(x)   " & vbCrLf
            mSQL += "          IF  @@ERROR  <> 0 GOTO Err   " & vbCrLf
            mSQL += "End" & vbCrLf
            mSQL += "	if exists(select 1 from @Qualification.nodes('NewDataSet/Qualification') pd(x))   " & vbCrLf
            mSQL += "begin" & vbCrLf
            mSQL += "      select @mApplicantID" & vbCrLf
            mSQL += "		insert into rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,Comp_Code)" & vbCrLf
            mSQL += "		select @mApplicantID" & vbCrLf
            mSQL += "	,x.value('(qualificationgroupunkid)[1]','int') as qualificationgroupunkid" & vbCrLf
            mSQL += "	,x.value('(qualificationunkid)[1]','int') as qualificationunkid" & vbCrLf
            mSQL += "		, convert(datetime,left(x.value('(transaction_date)[1]','varchar(15)'),10),101) as transaction_date" & vbCrLf
            mSQL += "		,x.value('(ReferenceNo)[1]','cCardNo') as reference_no" & vbCrLf
            mSQL += "		,convert(datetime,left(x.value('(startdate)[1]','varchar(15)'),10),101) as award_start_date" & vbCrLf
            mSQL += "		,convert(datetime,left(x.value('(enddate)[1]','varchar(15)'),10),101) as award_end_date" & vbCrLf
            mSQL += "		,x.value('(instituteunkid)[1]','int') as instituteunkid" & vbCrLf
            mSQL += "		,x.value('(remark)[1]','cDescription') as remark" & vbCrLf
            mSQL += "        ,x.value('(Comp_Code)[1]','cDescription') as Comp_Code" & vbCrLf
            mSQL += "		from @Qualification.nodes('NewDataSet/Qualification') Ld(x)" & vbCrLf
            mSQL += "     IF  @@ERROR  <> 0 GOTO Err " & vbCrLf
            mSQL += "End" & vbCrLf
            mSQL += "		if exists(select 1 from @Job.nodes('NewDataSet/Job') Pd(x))" & vbCrLf
            mSQL += "begin" & vbCrLf
            mSQL += "		insert into rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason,Comp_Code)" & vbCrLf
            mSQL += "			select @mApplicantID" & vbCrLf
            mSQL += "			,x.value('(employer)[1]','cName') as employername" & vbCrLf
            mSQL += "		,x.value('(company)[1]','cName') as companyname" & vbCrLf
            mSQL += "		,x.value('(Designation)[1]','CName') As designation" & vbCrLf
            mSQL += "		,x.value('(Responsibility)[1]','cDescription') as responsibility" & vbCrLf
            mSQL += "		,convert(datetime,left( x.value('(JoinDate)[1]','varchar(15)'),10),101) As joiningdate" & vbCrLf
            mSQL += "		,convert(datetime,left(x.value('(terminationdate)[1]','varchar(15)'),10),101)as terminationdate" & vbCrLf
            mSQL += "		,x.value('(Phone)[1]','cPhone') as officephone" & vbCrLf
            mSQL += "		,x.value('(leavingReason)[1]','cDescription') as leavingreason" & vbCrLf
            mSQL += "          ,x.value('(Comp_Code)[1]','cDescription') as Comp_Code" & vbCrLf
            mSQL += "			from @Job.nodes('NewDataSet/Job') Ld(x)" & vbCrLf
            mSQL += "           IF  @@ERROR  <> 0 GOTO Err " & vbCrLf
            mSQL += "End" & vbCrLf

            mSQL += "			IF  @@ERROR  <> 0 GOTO Err        " & vbCrLf
            mSQL += "End" & vbCrLf
            mSQL += "   insert  into hr_images_tran(employeeunkid,imagename,isapplicant,Comp_Code)" & vbCrLf
            mSQL += "        values (@mApplicantID,@ImageName,'True',@Comp_Code)" & vbCrLf
            mSQL += "   GoTo Sucess" & vbCrLf
            mSQL += "Err:" & vbCrLf
            mSQL += "    rollback tran  @TranName       " & vbCrLf
            mSQL += "  RAISERROR ('Error in SP ApplicateMaster. Cannot insert the entry !!!', 16, 1)     " & vbCrLf
            mSQL += "Sucess:" & vbCrLf
            mSQL += "commit tran @TranName  " & vbCrLf
            mSQL += "Return 0" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)

        Catch ex As Exception

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CommonCode-->Pkgdtastr !!!" & ex.Message.ToString & vbCrLf & mSQL)
            Dim errLog As New Diagnostics.EventLog
            mstrErrorMsg = ""
            mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : Pkgdtastr : " & ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                mstrErrorMsg &= "; " & ex.InnerException.Message
            End If
            errLog.WriteEntry(mstrErrorMsg, EventLogEntryType.Warning)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Public Sub PkgDtaStrCompany()
        Try
            
            'StoreProcedure CreateCalendar
            'mSQL = " if exists(select 1 from sys.procedures where name='CreateCalendar') drop proc CreateCalendar" & vbCrLf
            'objDataOperation.ExecNonQuery(mSQL)

            'mSQL = "create proc CreateCalendar (@tablename as nvarchar(20),@fdt as smalldatetime," & vbCrLf
            'mSQL &= " @tdt as smalldatetime,@dept as varchar(5) ,@sect as varchar(5)," & vbCrLf
            'mSQL &= "@job as varchar(5) ," & vbCrLf
            'mSQL &= "@UserId as nvarchar(5),@Employeeid as nvarchar(5))  " & vbCrLf
            'mSQL &= "AS   " & vbCrLf
            'mSQL &= "   BEGIN" & vbCrLf
            'mSQL &= "declare @dt as smalldatetime ,@temp varchar(100) , @sql varchar(4000), @new  varchar(4000) " & vbCrLf
            'mSQL &= "if exists(select name from sys.tables where name= @tablename) " & vbCrLf
            'mSQL &= " select @temp = 'drop table '+ @tablename +''" & vbCrLf
            'mSQL &= "  print @temp" & vbCrLf
            'mSQL &= "exec (@temp)" & vbCrLf

            'mSQL &= "select @dt = @fdt  " & vbCrLf
            'mSQL &= "print @dt" & vbCrLf

            'mSQL &= "select @sql = ' create table ' + @tablename + '( CalendarTableID int identity(1,1) ,leaveplannerunkid int, EmployeeUnkid int, DisplayName varchar(100) , '" & vbCrLf

            'mSQL &= "while @dt<=@tdt  " & vbCrLf
            'mSQL &= "begin" & vbCrLf
            'mSQL &= "select @sql = @sql + '[' + convert(varchar(8),@dt,112) + '] varchar(10) default('+char(39)+' '+char(39)+') , '" & vbCrLf
            'mSQL &= "select @dt = dateadd(d,1,@dt)  " & vbCrLf
            'mSQL &= "End" & vbCrLf
            'mSQL &= "select @sql = left(@sql,len(@sql)-2) + ')'" & vbCrLf
            'mSQL &= "print @sql" & vbCrLf
            'mSQL &= "exec (@sql)" & vbCrLf

            'mSQL &= "select @new ='  select l.leaveplannerunkid as leaveplannerunkid, l.EmployeeUnkID as EmployeeUnkID, e.Firstname as  Displayname into ' + @tablename + '1 from lvleaveplanner l'" & vbCrLf
            'mSQL &= " select @new =  @new +   '    left outer join hremployee_master e on e.EmployeeUnkID = l.EmployeeUnkID ' " & vbCrLf
            'mSQL &= "  select @new =   @new + ' WHERE 1=1'" & vbCrLf
            'mSQL &= "if @Employeeid <> '-1' select @new =   @new + ' AND  l.employeeunkid ='+   @Employeeid  + ''" & vbCrLf
            'mSQL &= " if @UserId <> '-1' select @new =   @new + 'AND l.userunkid =' + @UserId +''" & vbCrLf
            'mSQL &= " if   @dept<>'-1'    select @new =   @new + ' and departmentunkid = '+ @dept     " & vbCrLf
            'mSQL &= " if   @sect<>'-1'    select @new =   @new + ' and sectionunkid ='+ @sect        " & vbCrLf
            'mSQL &= " if   @dept<>'-1'     select @new =   @new + ' and jobunkid ='+  @dept  " & vbCrLf
            'mSQL &= " select @new =   @new +   ' group by l.employeeunkid,l.leaveplannerunkid,e.Firstname  '" & vbCrLf
            'mSQL &= "  exec (@new)" & vbCrLf
            'mSQL &= " select @new ='insert into '+ @tablename  +'(leaveplannerunkid,EmployeeUnkID, DisplayName)  select leaveplannerunkid,EmployeeUnkID, DisplayName  from ' + @tablename +'1  '" & vbCrLf
            'mSQL &= "  select @new = @new + ' '    " & vbCrLf
            'mSQL &= " print @new" & vbCrLf
            'mSQL &= " exec (@new)" & vbCrLf
            'mSQL &= "select @new = 'drop table '+ @tablename +'1 '" & vbCrLf
            'mSQL &= "exec (@new)" & vbCrLf
            'mSQL &= " End" & vbCrLf
            'objDataOperation.ExecNonQuery(mSQL)


            mSQL = " if exists(select 1 from sys.procedures where name='CreateCalendar') drop proc CreateCalendar" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)
            mSQL = "   CREATE proc [dbo].[CreateCalendar] (@tablename as nvarchar(20),@fdt as smalldatetime,@tdt as smalldatetime" & vbCrLf
            mSQL += " ,@dept as varchar(5)" & vbCrLf
            mSQL += " ,@sect as varchar(5)" & vbCrLf
            mSQL += " ,@job as varchar(5)" & vbCrLf
            mSQL += " ,@UserId as nvarchar(5)" & vbCrLf
            mSQL += " ,@Employeeid as nvarchar(5)" & vbCrLf
            mSQL += " ,@datefmtno as int" & vbCrLf
            mSQL += " )  " & vbCrLf
            mSQL += " AS   " & vbCrLf
            mSQL += "  BEGIN" & vbCrLf
            mSQL += " declare @dt as smalldatetime ,@temp varchar(100) , @sql varchar(4000), @new varchar(4000) ,@subqry varchar(4000)" & vbCrLf
            mSQL += " if exists(select name from sys.tables where name= @tablename) " & vbCrLf
            mSQL += " select @temp = 'drop table '+ @tablename +''" & vbCrLf
            mSQL += " exec (@temp)" & vbCrLf
            mSQL += " select @dt = @fdt  " & vbCrLf
            mSQL += " select @sql = ' create table ' + @tablename + '( CalendarTableID int identity(1,1), leaveplannerunkid varchar(500) default('+char(39)+' '+char(39)+'), leavetypeunkid varchar(500) default('+char(39)+' '+char(39)+'), EmployeeUnkid int, Ecode varchar(255), DisplayName varchar(100),   '" & vbCrLf
            mSQL += " while @dt<=@tdt  " & vbCrLf
            mSQL += " begin" & vbCrLf
            mSQL += " select @sql = @sql + '[' + convert(varchar(10),@dt,@datefmtno) + '] varchar(100) default('+char(39)+' '+char(39)+') , '" & vbCrLf
            mSQL += " select @dt = dateadd(d,1,@dt)  " & vbCrLf
            mSQL += " End" & vbCrLf
            mSQL += " select @sql = left(@sql,len(@sql)-2) + ')'" & vbCrLf
            mSQL += " print @sql" & vbCrLf
            mSQL += " exec (@sql)" & vbCrLf
            mSQL += " select @new = ' SELECT l.EmployeeUnkID as EmployeeUnkID, e.employeecode as Ecode, (e.firstname+space(1)+e.surname) as  Displayname   into ' + @tablename + '1 '" & vbCrLf
            mSQL += " select @new = @new +  ' FROM lvleaveplanner l left outer join hremployee_master e on e.EmployeeUnkID = l.EmployeeUnkID ' " & vbCrLf
            mSQL += " select @new = @new + ' WHERE l.isvoid = 0 '" & vbCrLf
            mSQL += " select @new = @new + ' AND ( l.startdate between ' + char(39) + convert(varchar(8),@fdt,112) +char(39)+ ' and ' +char(39)+ convert(varchar(8),@tdt,112) + char(39)" & vbCrLf
            mSQL += " select @new = @new + '       OR l.stopdate between ' + char(39) + convert(varchar(8),@fdt,112) + char(39) + ' and ' +char(39)+ convert(varchar(8),@tdt,112) +char(39)+ ' ) '" & vbCrLf
            mSQL += " print @new" & vbCrLf
            mSQL += " if @Employeeid > 0    select @new =   @new + '    AND  l.employeeunkid ='+   @Employeeid  + ''" & vbCrLf
            mSQL += " if @UserId > 0             select @new =   @new + '    AND l.userunkid IN(-1,' + @UserId +' )'" & vbCrLf
            mSQL += " if @dept > 0                 select @new =   @new + '    AND e.departmentunkid = '+ @dept     " & vbCrLf
            mSQL += " if @sect > 0                  select @new =   @new + '    AND e.sectionunkid ='+ @sect        " & vbCrLf
            mSQL += " if @job > 0                    select @new =   @new + '    AND e.jobunkid ='+  @job  " & vbCrLf
            mSQL += " print @new" & vbCrLf
            mSQL += " select @new = @new +   ' group by l.employeeunkid,e.firstname , e.surname,e.employeecode  '" & vbCrLf
            mSQL += " print (@new )" & vbCrLf
            mSQL += " select @subqry = substring(@new,CHARINDEX('FROM ',@new),len(@new))" & vbCrLf
            mSQL += " select @subqry = replace(@subqry,'group by l.employeeunkid,e.firstname , e.surname','group by l.employeeunkid,l.leaveplannerunkid,l.leavetypeunkid,e.firstname,e.surname')" & vbCrLf
            mSQL += " print @new" & vbCrLf
            mSQL += " exec (@new)" & vbCrLf
            mSQL += " print @subqry " & vbCrLf
            mSQL += " select @new ='insert into '+ @tablename  +'(EmployeeUnkID, Ecode, DisplayName)  select EmployeeUnkID, Ecode, DisplayName  from ' + @tablename +'1  '" & vbCrLf
            mSQL += " select @new = @new + ' '    " & vbCrLf
            mSQL += " exec (@new)" & vbCrLf
            mSQL += " if exists(select name from sys.tables where name= 'mynew') " & vbCrLf
            mSQL += " select @temp = 'drop table mynew'" & vbCrLf
            mSQL += " exec (@temp)" & vbCrLf
            mSQL += " select @new ='select  l.EmployeeUnkID , l.leaveplannerunkid, l.leavetypeunkid  into mynew ' + @subqry + ' '" & vbCrLf
            mSQL += " print @new" & vbCrLf
            mSQL += " exec(@new)" & vbCrLf
            mSQL += " select @new = 'drop table '+ @tablename +'1'" & vbCrLf
            mSQL += " exec (@new)" & vbCrLf
            mSQL += " DECLARE  @EmployeeUnkID  as nvarchar(10) ,@leaveplannerunkid as nvarchar(10),@leavetypeunkid  as nvarchar(10)" & vbCrLf
            mSQL += " DECLARE updateorddet  cursor for  " & vbCrLf
            mSQL += "  select EmployeeUnkID , leaveplannerunkid, leavetypeunkid FROM  mynew   " & vbCrLf
            mSQL += " open updateorddet   " & vbCrLf
            mSQL += " fetch next from updateorddet into @EmployeeUnkID,@leaveplannerunkid,@leavetypeunkid  " & vbCrLf
            mSQL += " while @@fetch_status=0  " & vbCrLf
            mSQL += " begin" & vbCrLf
            mSQL += " print @EmployeeUnkID" & vbCrLf
            mSQL += " print @leaveplannerunkid" & vbCrLf
            mSQL += " print @leavetypeunkid" & vbCrLf
            mSQL += " select @new ='update ' +  @tablename  + ' set  leaveplannerunkid  = leaveplannerunkid  + '+  char(39) + @leaveplannerunkid  + ','+ char(39) + '  , leavetypeunkid  = leavetypeunkid + '+  char(39)  + @leavetypeunkid +   ','+  char(39)  + ' where EmployeeUnkID = '+ @EmployeeUnkID + ' ' " & vbCrLf
            mSQL += " print  @new" & vbCrLf
            mSQL += " exec (@new)" & vbCrLf
            mSQL += " fetch next from updateorddet into @EmployeeUnkID,@leaveplannerunkid,@leavetypeunkid  " & vbCrLf
            mSQL += "            End" & vbCrLf
            mSQL += "            close updateorddet " & vbCrLf
            mSQL += "            Deallocate updateorddet " & vbCrLf
            mSQL += " End" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)



            'Sp CreateMonthCalendar

            mSQL = " if exists(select 1 from sys.procedures where name='CreateMonthCalendar') drop proc CreateMonthCalendar" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)

            mSQL = " create proc CreateMonthCalendar (@tablename as nvarchar(20),@fdt as datetime, @tdt as datetime )      " & vbCrLf
            mSQL &= "AS             " & vbCrLf
            mSQL &= " BEGIN" & vbCrLf
            mSQL &= "declare @dt as smalldatetime ,@temp varchar(100) , @sql varchar(8000), @new  varchar(8000),@msql varchar(8000),@td  as smalldatetime  ,@day int,@totday int,@i as int,@j int,@stwkno as int,@sql1 varchar(8000),@stwend int,@countday int,@startday varchar(10), @CountMonth int " & vbCrLf
            mSQL &= "if exists(select name from sys.tables where name= @tablename)     " & vbCrLf
            mSQL &= " select @temp = 'drop table '+ @tablename +''    " & vbCrLf
            mSQL &= "   print @temp    " & vbCrLf
            mSQL &= " exec (@temp)    " & vbCrLf
            mSQL &= "select @dt = @fdt    " & vbCrLf
            mSQL &= "select @countday=datediff(d,@fdt,@tdt)  " & vbCrLf
            mSQL &= "select @stwkno = datename(wk,@fdt)  " & vbCrLf
            mSQL &= "select @stwend=datename(wk,@tdt)  " & vbCrLf
            mSQL &= "print @dt    " & vbCrLf
            mSQL &= "select @sql = ' create table ' + @tablename + '( CalendarTableID int identity(1,1),MonthYear varchar(30),startday varchar(10),MonthDays int,MonthId int,YearId int,'    " & vbCrLf
            mSQL &= "select @i=1  " & vbCrLf
            mSQL &= "select @j=-1  " & vbCrLf
            mSQL &= "print @totday  " & vbCrLf
            mSQL &= "while @i <=6  " & vbCrLf
            mSQL &= "begin" & vbCrLf
            mSQL &= "select @j=-1  " & vbCrLf
            mSQL &= "while @j<6  " & vbCrLf
            mSQL &= "begin" & vbCrLf
            mSQL &= "select @sql = @sql + '[' + left(datename(dw,@j),3) + '_' + convert(varchar(3), + @i,103)+  '] nvarchar(50) default('+char(39)+' '+char(39)+') , '    " & vbCrLf
            mSQL &= "select @j=@j +1    " & vbCrLf
            mSQL &= "End" & vbCrLf
            mSQL &= " select @i = @i + 1  " & vbCrLf
            mSQL &= "End" & vbCrLf
            mSQL &= "select @sql = left(@sql,len(@sql)-1) + ')'    " & vbCrLf
            mSQL &= "print @sql    " & vbCrLf
            mSQL &= "exec (@sql)    " & vbCrLf
            mSQL &= "select @totday=datediff(mm,@fdt,@tdt)  " & vbCrLf
            mSQL &= "select @day=datepart(m,@fdt)   " & vbCrLf

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            'mSQL &= "select @totday=datepart(m,@tdt)  " & vbCrLf
            'Pinkal (18-Dec-2012) -- End

            mSQL &= "print @day  " & vbCrLf
            mSQL &= "print @totday  " & vbCrLf
            mSQL &= "SET @CountMonth = 0  " & vbCrLf

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            'mSQL &= "while @day <= @totday   " & vbCrLf
            'mSQL &= "begin" & vbCrLf
            'mSQL &= "select @startday = left(datename(dw,ltrim(str(datepart(yyyy,@dt))) + right('0'+ltrim(str(datepart(mm,@dt))) ,2) +  '01'),3)+'_1'  " & vbCrLf
            'mSQL &= "select @countday= day(dateadd(day,-1,dateadd(month,1,dateadd(day,1 - day(@dt),@dt))))  " & vbCrLf
            'mSQL &= "select @new ='insert into '+ @tablename  +'(MonthYear,startday,MonthDays,MonthId,YearId) values ('+char(39) + (datename(m,@dt))+'  -  '+str(year(@dt),4)+char(39)+','+char(39)+@startday+char(39)+','+ str(@countday)+','+STR(MONTH(@dt))+','+STR(YEAR(@dt))+')'  " & vbCrLf
            'mSQL &= "select @dt= DATEADD(m, 1, @dt)  " & vbCrLf
            'mSQL &= "select @day=@day + 1   " & vbCrLf
            'mSQL &= "print @day  " & vbCrLf
            'mSQL &= "exec (@new)   " & vbCrLf

            mSQL &= "while @CountMonth <= @totday   " & vbCrLf
            mSQL &= "begin" & vbCrLf
            mSQL &= "select @startday = left(datename(dw,ltrim(str(datepart(yyyy,@dt))) + right('0'+ltrim(str(datepart(mm,@dt))) ,2) +  '01'),3)+'_1'  " & vbCrLf
            mSQL &= "select @countday= day(dateadd(day,-1,dateadd(month,1,dateadd(day,1 - day(@dt),@dt))))  " & vbCrLf
            mSQL &= "select @new ='insert into '+ @tablename  +'(MonthYear,startday,MonthDays,MonthId,YearId) values ('+char(39) + (datename(m,@dt))+'  -  '+str(year(@dt),4)+char(39)+','+char(39)+@startday+char(39)+','+ str(@countday)+','+STR(MONTH(@dt))+','+STR(YEAR(@dt))+')'  " & vbCrLf
            mSQL &= "select @dt= DATEADD(m, 1, @dt)  " & vbCrLf
            mSQL &= "select @day=@day + 1   " & vbCrLf
            mSQL &= "SET @CountMonth  = @CountMonth + 1 " & vbCrLf
            mSQL &= "print @day  " & vbCrLf
            mSQL &= "exec (@new)   " & vbCrLf

            'Pinkal (18-Dec-2012) -- End

            mSQL &= "End" & vbCrLf
            mSQL &= " End" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)


            'weekdayoccurance function

            mSQL = " if exists(select 1 from sys.objects where name='weekdayoccurance') drop function weekdayoccurance" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)

            mSQL = " create  function weekdayoccurance(  @date as smalldatetime  )" & vbCrLf
            mSQL &= "returns Int" & vbCrLf
            mSQL &= "as" & vbCrLf
            mSQL &= " begin" & vbCrLf
            mSQL &= "declare @startdate as smalldatetime, @dayoccurance as int, @i int" & vbCrLf
            mSQL &= "select @startdate  = (ltrim(str(datepart(yyyy,@date )))+ right('0'+ltrim(str(datepart(mm,@date ))) ,2) + '01') " & vbCrLf
            mSQL &= "select @i = 0" & vbCrLf
            mSQL &= "select @dayoccurance = 0"
            mSQL &= "while @i <= datediff(d,@startdate,@date)" & vbCrLf
            mSQL &= "begin" & vbCrLf
            mSQL &= "if datename(dw,dateadd(d,@i,@startdate)) = datename(dw,@date)" & vbCrLf
            mSQL &= "	select @dayoccurance = @dayoccurance + 1" & vbCrLf
            mSQL &= "select @i = @i + 1" & vbCrLf
            mSQL &= "End" & vbCrLf
            mSQL &= "if datepart(dw,@date) < datepart(dw,@startdate)" & vbCrLf
            mSQL &= "	select @dayoccurance = @dayoccurance + 1" & vbCrLf
            mSQL &= "else" & vbCrLf
            mSQL &= "	select @dayoccurance = @dayoccurance + 0" & vbCrLf
            mSQL &= "return @dayoccurance" & vbCrLf
            mSQL &= "    End" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)


            'lvleaveformdetail function
            mSQL = " if exists(select 1 from sys.objects where name='lvleaveformdetail') drop function lvleaveformdetail" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)


            mSQL = " create  Function lvleaveformdetail" & vbCrLf
            mSQL &= "(       " & vbCrLf
            mSQL &= " @employeeunkid int" & vbCrLf
            mSQL &= ")              " & vbCrLf
            mSQL &= "RETURNS @tbl TABLE       " & vbCrLf
            mSQL &= "(                 " & vbCrLf
            mSQL &= "   employeeunkid(Int)" & vbCrLf
            mSQL &= ", leavedate		smalldatetime" & vbCrLf
            mSQL &= ", weekday		varchar(10)" & vbCrLf
            mSQL &= ", monthyear		varchar(15)" & vbCrLf
            mSQL &= ", lvid			int" & vbCrLf
            mSQL &= ", lvcolorid		int" & vbCrLf
            mSQL &= ",statusunkid int" & vbCrLf
            mSQL &= ")" & vbCrLf
            mSQL &= "AS           " & vbCrLf
            mSQL &= " BEGIN" & vbCrLf
            mSQL &= "declare @startdate as smalldatetime, @returndate as smalldatetime, @totdays  as int, @lvtypeid int, @lvcolor int,@statusunkid int" & vbCrLf
            mSQL &= "declare @mday as int" & vbCrLf
            mSQL &= "declare cur1 cursor" & vbCrLf
            mSQL &= "FOR " & vbCrLf
            mSQL &= "	select a.startdate, a.returndate, datediff(d,a.startdate,a.returndate) as totdays, a.leavetypeunkid , b.color as lvcolor,a.statusunkid" & vbCrLf
            mSQL &= "from lvleaveform  a " & vbCrLf
            mSQL &= "left outer join lvleavetype_master b on a.leavetypeunkid = b.leavetypeunkid " & vbCrLf
            mSQL &= "where a.employeeunkid = @employeeunkid  and statusunkid=@statusunkid" & vbCrLf
            mSQL &= "order by a.employeeunkid, a.applydate" & vbCrLf
            mSQL &= " OPEN cur1" & vbCrLf
            mSQL &= "FETCH NEXT FROM cur1 INTO @startdate, @returndate, @totdays, @lvtypeid , @lvcolor,@statusunkid" & vbCrLf
            mSQL &= "WHILE @@FETCH_STATUS = 0" & vbCrLf
            mSQL &= "BEGIN" & vbCrLf
            mSQL &= "select @mday = 0" & vbCrLf
            mSQL &= "while @mday <= @totdays" & vbCrLf
            mSQL &= "BEGIN" & vbCrLf
            mSQL &= "insert into @tbl (employeeunkid,leavedate, weekday, monthyear, lvid, lvcolorid,statusunkid) " & vbCrLf
            mSQL &= "values (  @employeeunkid , dateadd(d,@mday,@startdate) , left(datename(dw,dateadd(d,@mday,@startdate)),3)+'_'+str(dbo.weekdayoccurance(dateadd(d,@mday,@startdate)),1) , datename(mm,dateadd(d,@mday,@startdate))+' '+str(year(dateadd(d,@mday,@startdate)),4) , @lvtypeid , @lvcolor , @statusunkid)" & vbCrLf
            mSQL &= "select @mday = @mday + 1" & vbCrLf
            mSQL &= "End" & vbCrLf
            mSQL &= "FETCH NEXT FROM cur1 INTO @startdate, @returndate, @totdays, @lvtypeid , @lvcolor,@statusunkid" & vbCrLf
            mSQL &= "   End" & vbCrLf
            mSQL &= "   Return" & vbCrLf
            mSQL &= "  End" & vbCrLf
            objDataOperation.ExecNonQuery(mSQL)

            'select object_name(object_id), * from sys.columns 

            'select * from sys.procedures 
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CommonCode-->Pkgdtastr !!!" & ex.Message.ToString & vbCrLf & mSQL)
            Dim errLog As New Diagnostics.EventLog
            mstrErrorMsg = ""
            mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : Pkgdtastr : " & ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                mstrErrorMsg &= "; " & ex.InnerException.Message
            End If
            errLog.WriteEntry(mstrErrorMsg, EventLogEntryType.Warning)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Public Sub DisplayMessage(ByVal S_dispmsg As String, ByVal f_curform As Page)
        Try
        S_dispmsg = S_dispmsg.Replace("'", "")
        S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
		'Gajanan [18-Mar-2019] -- Start
        'Enhancement - Change Message Box For NMB
        'ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "alert('" & S_dispmsg & "');", True)
        ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "showSuccessMessage('" & S_dispmsg & "');", True)
		'Gajanan [18-Mar-2019] -- End
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            Dim errLog As New Diagnostics.EventLog
            mstrErrorMsg = ""
            mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : DisplayMessage : " & ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                mstrErrorMsg &= "; " & ex.InnerException.Message
            End If
            errLog.WriteEntry(mstrErrorMsg, EventLogEntryType.Warning)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub


    'S.SANDEEP |15-AUG-2020| -- START
    'ISSUE/ENHANCEMENT : Log Error Only
    Public Shared Sub LogErrorOnly(ByVal err As Exception)
        Try
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                Dim S_dispmsg As String = ""
                S_dispmsg = err.Message & "; " & err.StackTrace.ToString
                If err.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & err.InnerException.Message
                End If
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")


                'Pinkal (20-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                If HttpContext.Current.Session("Companyunkid") IsNot Nothing Then
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                End If
                'Pinkal (20-Aug-2020) -- End

                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With

            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If

            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Dim eLog As New EventLog
            'eLog.Source = "ArutiWarning"
            'Dim ErrorMsg As String = ""
            'ErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : LogErrorOnly : " & ex.Message & "; " & ex.StackTrace.ToString
            'If ex.InnerException IsNot Nothing Then
            '    ErrorMsg &= "; " & ex.InnerException.Message
            'End If
            'eLog.WriteEntry(ErrorMsg, EventLogEntryType.Warning)
            'ErrorMsg = ""
            'eLog = Nothing
            WriteEventLog(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
    'S.SANDEEP |15-AUG-2020| -- END


    Public Sub DisplayError(ByVal strErr As String, ByVal f_curform As Page)
        'Sohail (01 Feb 2020) - []
        Try
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                ._Error_Message = strErr
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With

            Dim strDBName As String = ""
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            'If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    'Sohail (29 Apr 2019) -- End
                    'strDBName = HttpContext.Current.Session("mdbname").ToString
                    strDBName = "hrmsConfiguration"
                End If
            Else 'Session Expired
                Exit Sub
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            Else
                'Do Nothng
            End If

            ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "showErroMessage('" & Basepage.m_CustomErrorMsg & "', '" & strErr & "');", True)
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'errLog.Source = "ArutiWarning"
            'mstrErrorMsg = ""
            'mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : DisplayError : " & exError.Message & "; " & exError.StackTrace.ToString
            'If exError.InnerException IsNot Nothing Then
            '    mstrErrorMsg &= "; " & exError.InnerException.Message
            'End If
            'errLog.WriteEntry(mstrErrorMsg, EventLogEntryType.Warning)
            WriteEventLog(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Public Sub DisplayMessage(ByVal S_dispmsg As String, ByVal f_curform As Page, ByVal strLoaction As String)
        Try
        S_dispmsg = S_dispmsg.Replace("'", "")
        S_dispmsg = S_dispmsg.Replace(vbCrLf, "")

        'Gajanan [18-Mar-2019] -- Start
        'Enhancement - Change Message Box For NMB
        'ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "alert('" & S_dispmsg & "');window.location.href='" & strLoaction & "';", True)
        ScriptManager.RegisterClientScriptBlock(f_curform, f_curform.GetType, "OpenMsgBox", "showLocationMessage('" & S_dispmsg & "','" & strLoaction & "');", True)
        'Gajanan [18-Mar-2019] -- End
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'errLog.Source = "ArutiWarning"
            'mstrErrorMsg = ""
            'mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : DisplayMessage : " & ex.Message & "; " & ex.StackTrace.ToString
            'If ex.InnerException IsNot Nothing Then
            '    mstrErrorMsg &= "; " & ex.InnerException.Message
            'End If
            'errLog.WriteEntry(mstrErrorMsg, EventLogEntryType.Warning)
            WriteEventLog(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub


    'Pinkal (20-Aug-2020) -- Start
    'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
    Public Shared Sub WriteEventLog(ByVal ex As Exception)
        Try
            Dim mstrEventLogName As String = "Application"
            Dim mstrEventSource As String = "ASP.NET " & Environment.Version.ToString()
        Dim errLog As EventLog = Nothing
        If EventLog.SourceExists(mstrEventSource) = False Then
            EventLog.CreateEventSource(mstrEventSource, mstrEventLogName)
        End If
        errLog = New EventLog(mstrEventLogName)
        errLog.Source = mstrEventSource
        Dim mstrErrorMsg As String = ""
        mstrErrorMsg = "Location : " & HttpContext.Current.Request.Url.AbsoluteUri & "  , Error : " & ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                mstrErrorMsg &= "; " & ex.InnerException.Message
            End If
        errLog.WriteEntry(mstrErrorMsg, Diagnostics.EventLogEntryType.Warning)
        errLog = Nothing

        Catch exErr As Exception

        End Try
    End Sub

            'Pinkal (20-Aug-2020) -- End



    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

    Public Function Export_ELeaveForm(ByVal mstrExportEPaySlipPath As String, ByVal mstrFormNo As String, ByVal mintEmployeeID As Integer, ByVal mintFormId As Integer _
                                                         , ByVal intLeaveBalanceSetting As Integer, ByVal intYearId As Integer, ByVal mdtdbStartDate As Date, ByVal mdtdbEndDate As Date _
                                                        , ByVal mblnShowLeaveExpense As Boolean, ByVal xLeaveAccrueTenureSetting As Integer, ByVal xLeaveAccrueDaysAfterEachMonth As Integer) As String


        'Pinkal (16-Dec-2016) --  Working on IBA Leave Requirement as per Mr.Rutta's Guidance.[ByVal xLeaveAccrueTenureSetting As Integer, ByVal xLeaveAccrueDaysAfterEachMonth As Integer ]

        'Pinkal (13-Jul-2015) -- WORKING ON C & R ACCESS PRIVILEGE. [ByVal mblnShowLeaveExpense As Boolean]


        Dim mdicAttachmentInfo As New Dictionary(Of Integer, String)
        Dim objLeaveFormReport As New ArutiReports.clsEmployeeLeaveForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Dim StrPath As String = ""
        Try
            Dim strReportName As String = String.Empty
            Dim strOriginalReportName As String = String.Empty

            strReportName = "Employee Leave Form"
            strOriginalReportName = strReportName

            If System.IO.Directory.Exists(mstrExportEPaySlipPath) = False Then
                System.IO.Directory.CreateDirectory(mstrExportEPaySlipPath)
            End If

            strReportName = mstrFormNo & "_" & strReportName.Replace(" ", "_") & ConfigParameter._Object._CurrentDateAndTime.ToString("yyymmddhhmmss")
            objLeaveFormReport._LeaveFormNo = mstrFormNo
            objLeaveFormReport._LeaveFormId = mintFormId

            'Pinkal (11-Jun-2014) -- Start
            'Enhancement : TRA Changes [Problem In Leave Form Attachment]
            objLeaveFormReport._LeaveBalanceSetting = intLeaveBalanceSetting
            objLeaveFormReport._YearId = intYearId
            objLeaveFormReport._Fin_StartDate = mdtdbStartDate
            objLeaveFormReport._Fin_Enddate = mdtdbEndDate
            'Pinkal (11-Jun-2014) -- End


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            objLeaveFormReport._ShowLeaveExpense = mblnShowLeaveExpense
            'Pinkal (13-Jul-2015) -- End


            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            objLeaveFormReport._LeaveAccrueTenureSetting = xLeaveAccrueTenureSetting
            objLeaveFormReport._LeaveAccrueDaysAfterEachMonth = xLeaveAccrueDaysAfterEachMonth
            'Pinkal (16-Dec-2016) -- End



            'Pinkal (27-Apr-2019) -- Start
            'Enhancement - Audit Trail changes.
            objLeaveFormReport._PaymentApprovalwithLeaveApproval = CBool(HttpContext.Current.Session("PaymentApprovalwithLeaveApproval"))
            'Pinkal (27-Apr-2019) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objLeaveFormReport.Send_ELeaveForm(strReportName, mstrExportEPaySlipPath)
            objLeaveFormReport.Send_ELeaveForm(HttpContext.Current.Session("Database_Name"), _
                                               HttpContext.Current.Session("UserId"), _
                                               HttpContext.Current.Session("Fin_year"), _
                                               HttpContext.Current.Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")), _
                                               True, HttpContext.Current.Session("UserAccessModeSetting"), _
                                               strReportName, mstrExportEPaySlipPath)
            'Shani(20-Nov-2015) -- End

            If mdicAttachmentInfo.ContainsKey(mintEmployeeID) Then
                mdicAttachmentInfo(mintEmployeeID) = mdicAttachmentInfo(mintEmployeeID) & "," & strReportName & ".pdf"
            Else
                mdicAttachmentInfo.Add(mintEmployeeID, strReportName & ".pdf")
            End If

            StrPath = strReportName.Replace(" ", "_") & ".pdf"
            strReportName = strOriginalReportName

        Catch ex As Exception
            Throw New Exception("CommonCode-->Export_ELeaveForm :-" & ex.Message.ToString & vbCrLf & mSQL)
        Finally
            objLeaveFormReport = Nothing
        End Try
        Return StrPath
    End Function

    'Pinkal (06-Mar-2014) -- End

    'Sohail (01 Jan 2013) -- Start
    'TRA - ENHANCEMENT

    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    'Public Sub GetCompanyYearInfo(ByVal strCompUnkId As Integer)
    '    Dim dsList As DataSet
    '    Try
    '        mSQL = "SELECT  cfcompany_master.companyunkid " & _
    '                           ", cfcompany_master.code AS CompCode " & _
    '                           ", cfcompany_master.name AS CompName " & _
    '                           ", cffinancial_year_tran.yearunkid " & _
    '                           ", cffinancial_year_tran.financialyear_name " & _
    '                           ", cffinancial_year_tran.database_name " & _
    '                           ", cffinancial_year_tran.start_date " & _
    '                           ", cffinancial_year_tran.end_date "

    '        'Pinkal (09-Nov-2013) -- Start
    '        'Enhancement : Oman Changes
    '        mSQL &= ", cffinancial_year_tran.isclosed "
    '        'Pinkal (09-Nov-2013) -- End

    '        mSQL &= "FROM    hrmsConfiguration..cfcompany_master " & _
    '                                    "JOIN hrmsConfiguration..cffinancial_year_tran ON cfcompany_master.companyunkid = cffinancial_year_tran.companyunkid " & _
    '                                    "AND cfcompany_master.isactive = 1 " & _
    '                                    "AND cffinancial_year_tran.isclosed = 0 " & _
    '                           "WHERE cfcompany_master.companyunkid = " & strCompUnkId & " "

    '        dsList = objDataOperation.WExecQuery(mSQL, "List")

    '        For Each dsRow As DataRow In dsList.Tables("List").Rows
    '            HttpContext.Current.Session("CompCode") = dsRow.Item("CompCode")
    '            HttpContext.Current.Session("CompName") = dsRow.Item("CompName")
    '            HttpContext.Current.Session("Fin_year") = dsRow.Item("yearunkid")
    '            HttpContext.Current.Session("FinancialYear_Name") = dsRow.Item("financialyear_name")
    '            HttpContext.Current.Session("Database_Name") = dsRow.Item("database_name")
    '            HttpContext.Current.Session("fin_startdate") = dsRow.Item("start_date")
    '            HttpContext.Current.Session("fin_enddate") = dsRow.Item("end_date")

    '            'Pinkal (09-Nov-2013) -- Start
    '            'Enhancement : Oman Changes
    '            HttpContext.Current.Session("isclosed") = CBool(dsRow.Item("isclosed"))
    '            'Pinkal (09-Nov-2013) -- End

    '        Next
    '    Catch ex As Exception

    '        Throw New Exception("CommonCode-->Export_ELeaveForm :-" & ex.Message.ToString & vbCrLf & mSQL)
    '    End Try


    'End Sub
    'Sohail (30 Mar 2015) -- End
    'Sohail (01 Jan 2013) -- End

End Class
