﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
#End Region
Partial Class Claim_Retirement_wPg_ClaimRetirement_ApprovalList
    Inherits Basepage

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmClaimRetirementApprovalList"
    Private DisplayMessage As New CommonCodes
    Private objClaimRetirementApproverTran As New clsclaim_retirement_approval_Tran
    Private objExpApprover As New clsExpenseApprover_Master
    Private mintClaimRetirementId As Integer = 0
    Private mintcrApproverunkid As Integer = 0
    Private mdtRetirementApprovalList As DataTable = Nothing
#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Dim objMapping As New clsapprover_Usermapping

                objMapping.GetData(enUserType.crApprover, , CInt(Session("UserId")), )
                objExpApprover._crApproverunkid = objMapping._Approverunkid

                Dim dsAppr As New DataSet
                dsAppr = objExpApprover.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                , True, CBool(Session("IsIncludeInactiveEmp")), True, "", True, objExpApprover._crApproverunkid)
                If dsAppr.Tables("List").Rows.Count > 0 Then
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    txtRetirementApprover.Text = info1.ToTitleCase(dsAppr.Tables("List").Rows(0)("ename").ToString().ToLower())
                    info1 = Nothing
                End If
                dsAppr.Dispose()

                lvClaimRetirementList.DataSource = New List(Of String)
                lvClaimRetirementList.DataBind()
                SetVisibility()

                'Pinkal (03-Mar-2021)-- Start
                'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    ChkMyApprovals.Checked = False
                    ChkMyApprovals.Visible = False
                    txtRetirementApprover.Text = ""
                End If
                'Pinkal (03-Mar-2021) -- End

            Else
                Call SetLanguage()
                mintClaimRetirementId = CInt(Me.ViewState("ClaimRetirementId"))
                mintcrApproverunkid = CInt(Me.ViewState("crApproverunkid"))

                'Pinkal (28-Oct-2021)-- Start
                'Problem in Assigning Leave Accrue Issue.
                'mdtRetirementApprovalList = CType(Me.ViewState("RetirementApprovalList"), DataTable)

                'If mdtRetirementApprovalList IsNot Nothing Then
                '    lvClaimRetirementList.DataSource = mdtRetirementApprovalList
                '    lvClaimRetirementList.DataBind()
                'End If
                'Pinkal (28-Oct-2021)-- End

            End If



        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ClaimRetirementId") = mintClaimRetirementId
            Me.ViewState("crApproverunkid") = mintcrApproverunkid
            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.
            'Me.ViewState("RetirementApprovalList") = mdtRetirementApprovalList
            'Pinkal (28-Oct-2021)-- End
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_PreRender :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objEMaster As New clsEmployee_Master
        Dim objMasterData As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            True, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With



            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()
                .SelectedValue = "2"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_List()
        Dim StrSearch As String = String.Empty
        Try

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewRetiredApplicationApproval")) = False Then Exit Sub
            End If
            'Pinkal (10-Mar-2021) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_retirement_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If txtRetirementNo.Text.Trim.Length > 0 Then
                StrSearch &= "AND cmclaim_retirement_master.claimretirementno LIKE '%" & txtRetirementNo.Text & "%' "
            End If

            If IsDate(dtpFDate.GetDate) AndAlso IsDate(dtpTDate.GetDate) Then
                If dtpFDate.GetDate.Date <> Nothing AndAlso dtpTDate.GetDate.Date <> Nothing Then
                    StrSearch &= "AND CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) >= '" & eZeeDate.convertDate(dtpFDate.GetDate) & "' AND  CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) <= '" & eZeeDate.convertDate(dtpTDate.GetDate) & "' "
                End If
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                If CInt(cboStatus.SelectedValue) = 6 Then
                    StrSearch &= "AND cmclaim_retirement_approval_tran.iscancel = 1"
                Else
                    StrSearch &= "AND cmclaim_retirement_approval_tran.iscancel = 0 AND cmclaim_retirement_master.statusunkid = " & CInt(cboStatus.SelectedValue) & " "
                End If
            End If

            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.

            If ChkMyApprovals.Checked Then
                StrSearch &= "AND hrapprover_usermapping.userunkid = " & CInt(Session("UserId")) & " "
            Else
                ChkMyApprovals.Checked = False
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                StrSearch &= "AND cmclaim_retirement_approval_tran.visibleid <> -1 "
            End If


            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If

       
            'Dim dsList As DataSet = objClaimRetirementApproverTran.GetClaimRetirementApproverExpesneList("List", True, Session("Database_Name").ToString(), _
            '                                                                                                                                               CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), True, -1, StrSearch, -1, Nothing)
            Dim dsList As DataSet = objClaimRetirementApproverTran.GetClaimRetirementApproverExpesneList("List", True, Session("Database_Name").ToString(), _
                                                                                                                                                           CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), True, -1, StrSearch, -1, Nothing, True)
            'Pinkal (28-Oct-2021)-- End

            StrSearch = ""

            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.

            ''START FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    Dim drRow As DataRow() = dsList.Tables(0).Select("mapuserunkid = " & CInt(Session("UserId")), "")
            '    If drRow.Length > 0 Then
            '        Dim objmapuser As New clsapprover_Usermapping
            '        objmapuser.GetData(enUserType.crApprover, CInt(drRow(0)("approverunkid")), , )
            '        objExpApprover._crApproverunkid = objmapuser._Approverunkid
            '        mintcrApproverunkid = objmapuser._Approverunkid
            '    End If
            'End If

            'Dim objExpAppLevel As New clsExApprovalLevel
            'objExpAppLevel._Crlevelunkid = objExpApprover._crLevelunkid

            'END FOR SET FILTER THAT ONLY LOGIN USER CAN SEE HIS AND LOWER LEVEL APPROVER

            'If ChkMyApprovals.Checked and objExpAppLevel._Crlevelunkid > 0 Then
            'StrSearch &= "AND userunkid = " & CInt(Session("UserId")) & " "
            'Else
            'ChkMyApprovals.Checked = False
            'End If

            'If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
            '    StrSearch &= "AND visibleid <> -1 "
            'End If

            'Pinkal (28-Oct-2021)--End

            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
                mdtRetirementApprovalList = New DataView(dsList.Tables(0), StrSearch, "claimretirementno", DataViewRowState.CurrentRows).ToTable
            Else
                mdtRetirementApprovalList = New DataView(dsList.Tables(0), "", "claimretirementno", DataViewRowState.CurrentRows).ToTable
            End If

            'Pinkal (28-Oct-2021)-- Start
            'Problem in Assigning Leave Accrue Issue.
            'Dim dtTable As DataTable = mdtRetirementApprovalList.Clone
            'Dim strRetirementNo As String = ""
            'dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
            'Dim dtRow As DataRow = Nothing
            'For Each drow As DataRow In mdtRetirementApprovalList.Rows
            '    If CStr(drow("claimretirementno")).Trim <> strRetirementNo.Trim Then
            '        dtRow = dtTable.NewRow
            '        strRetirementNo = drow("claimretirementno").ToString()
            '        dtRow("IsGrp") = True
            '        dtRow("claimretirementno") = strRetirementNo
            '        dtTable.Rows.Add(dtRow)
            '    End If

            '    dtRow = dtTable.NewRow
            '    For Each dtcol As DataColumn In mdtRetirementApprovalList.Columns
            '        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
            '    Next
            '    dtRow("IsGrp") = False
            '    dtTable.Rows.Add(dtRow)
            'Next

            'mdtRetirementApprovalList = dtTable.Copy()
            'Pinkal (28-Oct-2021)-- End


            lvClaimRetirementList.DataSource = mdtRetirementApprovalList
            lvClaimRetirementList.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                'Pinkal (10-Mar-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                lvClaimRetirementList.Columns(0).Visible = CBool(Session("AllowToApproveRetiredApplication"))
                'Pinkal (10-Mar-2021) -- End
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                lvClaimRetirementList.Columns(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("SetVisibility:-" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try         'Hemant (13 Aug 2020)
        popupAdvanceFilter.Show()

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            Me.ViewState("mstrAdvanceFilter") = ""
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError("lnkReset_Click:-" & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError("objbtnSearch_Click:-" & ex.Message, Me)
        Finally
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Dim strAdvanceSearch As String
        Try
            strAdvanceSearch = popupAdvanceFilter._GetFilterString
            Me.ViewState("mstrAdvanceFilter") = strAdvanceSearch
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError("popupAdvanceFilter_buttonApply_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("crdtTable") = Nothing
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub lvClaimRetirementList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvClaimRetirementList.ItemCommand
        Try
            If e.CommandName = "Approval" Then

                Dim dsList As DataSet = Nothing
                Dim objMapping As New clsapprover_Usermapping

                'dsList = objMapping.GetList("Mapping", enUserType.crApprover, )
                'If dsList.Tables("Mapping").Rows.Count > 0 Then

                '    Dim dtList1 As DataTable = New DataView(dsList.Tables("Mapping"), "userunkid= " & CInt(Session("UserId")), "", DataViewRowState.CurrentRows).ToTable
                '    If dtList1.Rows.Count > 0 Then
                '        mintcrApproverunkid = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text)
                '        objExpApprover._crApproverunkid = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text)
                '    End If
                'End If

                dsList = objMapping.GetList("Mapping", enUserType.crApprover, "userunkid= " & CInt(Session("UserId")))
                If dsList.Tables("Mapping").Rows.Count > 0 Then
                        mintcrApproverunkid = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text)
                        objExpApprover._crApproverunkid = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text)
                    End If

                If CInt(Session("UserId")) <> CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhmapuserunkid", False, True)).Text) Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "You can't Edit this Imprest detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhIsCancel", False, True)).Text) = True Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "You can't Edit this Imprest detail. Reason: This Imprest is already Cancelled."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 1 Then 'FOR APPROVED
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit this Imprest detail. Reason: This Imprest is already Approved."), Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 3 Then 'FOR REJECTED
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                    Exit Sub
                End If

                'Pinkal (28-Oct-2021)-- Start
                'Problem in Assigning Leave Accrue Issue.

                'START FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT
                'Dim mintApproverId As Integer = 0
                'Dim dtList As DataTable = New DataView(mdtRetirementApprovalList, "employeeunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text) & " AND crmasterunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text) _
                '                                        & " AND approverunkid <> " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text), "statusunkid desc", DataViewRowState.CurrentRows).ToTable
                'If dtList.Rows.Count > 0 Then
                '    Dim objClaimMst As New clsclaim_request_master
                '    Dim objExpapprlevel As New clsExApprovalLevel
                '    Dim objLeaveApprover As New clsleaveapprover_master
                '    Dim objLeaveLevel As New clsapproverlevel_master
                '    objClaimMst._Crmasterunkid = CInt(dtList.Rows(0)("crmasterunkid"))
                '    mintApproverId = mintcrApproverunkid

                '    objExpapprlevel._Crlevelunkid = objExpApprover._crLevelunkid

                '    For i As Integer = 0 To dtList.Rows.Count - 1

                '        If objExpapprlevel._Crpriority > CInt(dtList.Rows(i)("crpriority")) Then
                '            'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING
                '            Dim dList As DataTable = New DataView(dtList, "crlevelunkid = " & CInt(dtList.Rows(i)("crlevelunkid")) & " AND statusunkid = 1", "", DataViewRowState.CurrentRows).ToTable
                '            If dList.Rows.Count > 0 Then Continue For
                '            'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                '            If CInt(dtList.Rows(i)("statusunkid")) = 2 Then
                '                Language.setLanguage(mstrModuleName)
                '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Imprest detail. Reason: This Imprest approval is still pending."), Me)
                '                Exit Sub
                '            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                '                Language.setLanguage(mstrModuleName)
                '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                '                Exit Sub
                '            End If
                '        ElseIf objExpapprlevel._Crpriority <= CInt(dtList.Rows(i)("crpriority")) Then
                '            If CInt(dtList.Rows(i)("statusunkid")) = 1 Then
                '                Language.setLanguage(mstrModuleName)
                '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit this Imprest detail. Reason: This Imprest is already Approved."), Me)
                '                Exit Sub
                '            ElseIf CInt(dtList.Rows(i)("statusunkid")) = 3 Then
                '                Language.setLanguage(mstrModuleName)
                '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                '                Exit Sub
                '            End If
                '        End If
                '    Next
                '    objClaimMst = Nothing
                '    objLeaveApprover = Nothing
                '    objLeaveLevel = Nothing
                'End If


                Dim mintApproverId As Integer = 0
                Dim dRow = lvClaimRetirementList.Items.Cast(Of DataGridItem).Where(Function(x) x.Cells(getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text _
                                                                                       And x.Cells(getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text _
                                                                                       And x.Cells(getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text)


                If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                    Dim objClaimMst As New clsclaim_request_master
                    Dim objExpapprlevel As New clsExApprovalLevel
                    Dim objLeaveApprover As New clsleaveapprover_master
                    Dim objLeaveLevel As New clsapproverlevel_master
                    objClaimMst._Crmasterunkid = CInt(dRow(0).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text)
                    mintApproverId = mintcrApproverunkid

                    objExpapprlevel._Crlevelunkid = objExpApprover._crLevelunkid

                    For i As Integer = 0 To dRow.Count - 1

                        If objExpapprlevel._Crpriority > CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrpriority", False, True)).Text) Then

                            'START FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                            If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrlevelunkid", False, True)).Text = dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrlevelunkid", False, True)).Text _
                            AndAlso CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 1 Then
                                Continue For
                            End If

                            'END FOR CHECK CASE WHETHER ONE USER HAS MULTIPLE APPROVER WITH SAME LEVEL AND STATUS IS NOT PENDING

                            If CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 2 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Imprest detail. Reason: This Imprest approval is still pending."), Me)
                                Exit Sub
                            ElseIf CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 3 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                                Exit Sub
                            End If
                        ElseIf objExpapprlevel._Crpriority <= CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrpriority", False, True)).Text) Then
                            If CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 1 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit this Imprest detail. Reason: This Imprest is already Approved."), Me)
                                Exit Sub
                            ElseIf CInt(dRow(i).Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 3 Then
                                Language.setLanguage(mstrModuleName)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                                Exit Sub
                            End If
                        End If
                    Next
                    objClaimMst = Nothing
                    objLeaveApprover = Nothing
                    objLeaveLevel = Nothing
                End If

                'Pinkal (28-Oct-2021)-- End

                'END FOR CHECK WHETHER LOWER LEVEL APPROV LEAVE OR NOT
                If CDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objcolhTranscationDate", False, True)).Text).Date < ConfigParameter._Object._CurrentDateAndTime.Date And mintApproverId = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text) Then
                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 1 Then   'FOR APPROVED
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "You can't Edit this Imprest detail. Reason: This Imprest is already Approved."), Me)
                        Exit Sub
                    ElseIf CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 3 Then   'FOR REJECTED
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                        Exit Sub
                    End If
                ElseIf CDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objcolhTranscationDate", False, True)).Text).Date > ConfigParameter._Object._CurrentDateAndTime.Date And mintApproverId = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text) Then
                    If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 3 Then   'FOR REJECTED
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected."), Me)
                        Exit Sub
                    End If

                End If
                Session.Add("ClaimRetirementId", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhclaimretirementunkid", False, True)).Text)
                Session.Add("ClaimRetireEmpId", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text)
                Session.Add("crmasterunkid", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text)
                Session.Add("approverunkid", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproverunkid", False, True)).Text)
                Session.Add("approverEmpID", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhapproveremployeeunkid", False, True)).Text)
                Session.Add("Priority", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrpriority", False, True)).Text)
                Session.Add("IsExternalApprover", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhisexternalapprover", False, True)).Text)
                Response.Redirect("~/Claim_Retirement/wPgClaimRetirementApproval.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("lvClaimRetirementList_ItemCommand:-" & ex.Message, Me)
        End Try
    End Sub

    Protected Sub lvClaimRetirementList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvClaimRetirementList.ItemDataBound
        Try
            Call SetDateFormat()
            If e.Item.ItemType = ListItemType.Header Then
                'Pinkal (28-Oct-2021)-- Start
                'Problem in Assigning Leave Accrue Issue.
                Language.setLanguage(mstrModuleName)
                'Pinkal (28-Oct-2021)-- End
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Text
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).ColumnSpan = 3
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhEmployeecode", False, True)).Visible = False
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Visible = False
            End If

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Text = info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Text.ToLower())
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhRetirementApprover", False, True)).Text = info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhRetirementApprover", False, True)).Text.ToLower())

                If CBool(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhIsGrp", False, True)).Text) Then
                    CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton).Visible = False
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        'Pinkal (10-Mar-2021) -- Start
                        'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                        'If lvClaimRetirementList.Columns(0).Visible Then
                        '    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "btnChangeStatus", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Text
                        '    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "btnChangeStatus", False, True)).ColumnSpan = e.Item.Cells.Count - 1
                        '    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "btnChangeStatus", False, True)).Style.Add("text-align", "left")
                        '    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "btnChangeStatus", False, True)).CssClass = "GroupHeaderStyleBorderLeft"
                        '    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Visible = False
                        'Else
                            e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Style.Add("text-align", "left")
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "btnChangeStatus", False, True)).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).CssClass = "GroupHeaderStyleBorderLeft"
                        'End If
                        'Pinkal (10-Mar-2021) -- End
                    Else
                        'Pinkal (10-Mar-2021) -- Start
                        'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                        'e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Text
                        'Pinkal (10-Mar-2021) -- End
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).ColumnSpan = e.Item.Cells.Count - 1
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Style.Add("text-align", "left")
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).CssClass = "GroupHeaderStyleBorderLeft"
                    End If
                    For i = 2 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else

                    'Dim mstrStaus As String = ""
                    'Dim dList As DataTable = Nothing
                    'If e.Item.Cells.Count > 0 AndAlso e.Item.ItemIndex >= 0 AndAlso mdtRetirementApprovalList IsNot Nothing Then
                    '    If mintClaimRetirementId <> CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhclaimretirementunkid", False, True)).Text) Then  'CInt(getColumnId_Datagrid(  e.Item.Cells(17).Text) Then
                    '        'Pinkal (20-Feb-2020) -- Start
                    '        'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
                    '        'dList = New DataView(mdtRetirementApprovalList, "employeeunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text) & " AND crmasterunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text), "", DataViewRowState.CurrentRows).ToTable
                    '        dList = New DataView(mdtRetirementApprovalList, "employeeunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text) & " AND claimretirementunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhclaimretirementunkid", False, True)).Text), "", DataViewRowState.CurrentRows).ToTable
                    '        'Pinkal (20-Feb-2020) -- End
                    '    Else
                    '        'Pinkal (20-Feb-2020) -- Start
                    '        'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
                    '        'dList = New DataView(mdtRetirementApprovalList, "employeeunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text) & " AND crmasterunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrmasterunkid", False, True)).Text), "", DataViewRowState.CurrentRows).ToTable
                    '        dList = New DataView(mdtRetirementApprovalList, "employeeunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhemployeeunkid", False, True)).Text) & " AND claimretirementunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhclaimretirementunkid", False, True)).Text), "", DataViewRowState.CurrentRows).ToTable
                    '        'Pinkal (20-Feb-2020) -- End
                    '    End If
                    '    If dList IsNot Nothing AndAlso dList.Rows.Count > 0 Then
                    '        Dim dr As DataRow() = dList.Select("crpriority >= " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrpriority", False, True)).Text))
                    '        If dr.Length > 0 Then
                    '            For i As Integer = 0 To dr.Length - 1
                    '                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 1 Then
                    '                    mstrStaus = Language.getMessage(mstrModuleName, 6, "Approved By :-  ") & info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhRetirementApprover", False, True)).Text.ToString().ToLower())
                    '                    Exit For
                    '                ElseIf CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 2 Then
                    '                    If CInt(dr(i)("statusunkid")) = 1 Then
                    '                        mstrStaus = Language.getMessage(mstrModuleName, 6, "Approved By :-  ") & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                    '                        Exit For
                    '                    ElseIf CInt(dr(i)("statusunkid")) = 3 Then
                    '                        mstrStaus = Language.getMessage(mstrModuleName, 7, "Rejected By :-  ") & info1.ToTitleCase(dr(i)("approvername").ToString().ToLower())
                    '                        Exit For
                    '                    End If
                    '                ElseIf CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 3 Then
                    '                    mstrStaus = Language.getMessage(mstrModuleName, 7, "Rejected By :-  ") & info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhRetirementApprover", False, True)).Text.ToString().ToLower())
                    '                    Exit For
                    '                End If
                    '            Next
                    '        End If
                    '        If mstrStaus = "" Then
                    '            If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhstatusunkid", False, True)).Text) = 2 Then
                    '                For i As Integer = 0 To dr(0).Table.Rows.Count - 1
                    '                    If CInt(dr(0).Table.Rows(i)("statusunkid")) = 3 Then
                    '                        mstrStaus = Language.getMessage(mstrModuleName, 7, "Rejected By :-  ") & info1.ToTitleCase(dr(0).Table.Rows(i)("approvername").ToString().ToLower())
                    '                        Exit For
                    '                    End If
                    '                Next
                    '            End If
                    '        End If
                    '    End If
                    '    If mstrStaus <> "" Then
                    '        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhStatus", False, True)).Text = mstrStaus
                    '    End If

                    If e.Item.Cells(getColumnId_Datagrid(lvClaimRetirementList, "dgcolhStatus", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(getColumnId_Datagrid(lvClaimRetirementList, "dgcolhStatus", False, True)).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(getColumnId_Datagrid(lvClaimRetirementList, "dgcolhStatus", False, True)).Text = info1.ToTitleCase(e.Item.Cells(getColumnId_Datagrid(lvClaimRetirementList, "dgcolhStatus", False, True)).Text.ToLower())
                        End If

                        If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objcolhTranscationDate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objcolhTranscationDate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objcolhTranscationDate", False, True)).Text = eZeeDate.convertDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objcolhTranscationDate", False, True)).Text).Date.ToShortDateString()
                        End If

                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhRetirementApprover", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhRetirementApprover", False, True)).Text & " - " & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhcrlevelname", False, True)).Text

                        If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhapprovaldate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhapprovaldate", False, True)).Text.Trim <> "&nbsp;" Then
                            e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhapprovaldate", False, True)).Text = eZeeDate.convertDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhapprovaldate", False, True)).Text).Date.ToShortDateString
                        End If

                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhamount", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhamount", False, True)).Text), Session("fmtCurrency").ToString())

                        'Pinkal (07-Nov-2019) -- Start
                        'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhbalance", False, True)).Text = Format(CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "objdgcolhclaimamount", False, True)).Text) - CDec(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhamount", False, True)).Text), Session("fmtcurrency").ToString())
                        'Pinkal (07-Nov-2019) -- End

                        Dim lnkChangeStatus As LinkButton = CType(e.Item.Cells(0).FindControl("lnkEdit"), LinkButton)
                        lnkChangeStatus.ToolTip = Language._Object.getCaption("btnChangeStatus", lnkChangeStatus.Text).Replace("&", "")
                    'Pinkal (28-Oct-2021)-- Start
                    'Problem in Assigning Leave Accrue Issue.
                    'e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Attributes.Add("ClaimRetirementNo", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Text)
                    'e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Attributes.Add("EmpCode", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhEmployeecode", False, True)).Text)
                    'e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Attributes.Add("EmpName", e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Text)
                    'Pinkal (28-Oct-2021)-- End
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).Text = e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhEmployeecode", False, True)).Text & " - " & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Text
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhretirementno", False, True)).ColumnSpan = 3
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhEmployeecode", False, True)).Visible = False
                        e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(lvClaimRetirementList, "dgcolhemployee", False, True)).Visible = False
                    End If

                End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub lvClaimRetirementList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles lvClaimRetirementList.PageIndexChanged
        Try
            lvClaimRetirementList.CurrentPageIndex = e.NewPageIndex
            Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(mstrModuleName, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Language._Object.setCaption(Me.lblRetirementNo.ID, Me.lblRetirementNo.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblFromDate.ID, Me.lblFromDate.Text)
            Language._Object.setCaption(Me.lblToDate.ID, Me.lblToDate.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Language._Object.setCaption(Me.ChkMyApprovals.ID, Me.ChkMyApprovals.Text)
            Language._Object.setCaption(Me.lblRetirementApprover.ID, Me.lblRetirementApprover.Text)

            Language._Object.setCaption(Me.BtnSearch.ID, Me.BtnSearch.Text.Replace("&", ""))
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text.Replace("&", ""))


            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(0).FooterText, Me.lvClaimRetirementList.Columns(0).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(1).FooterText, Me.lvClaimRetirementList.Columns(1).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(2).FooterText, Me.lvClaimRetirementList.Columns(2).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(3).FooterText, Me.lvClaimRetirementList.Columns(3).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(4).FooterText, Me.lvClaimRetirementList.Columns(4).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(5).FooterText, Me.lvClaimRetirementList.Columns(5).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(6).FooterText, Me.lvClaimRetirementList.Columns(6).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(7).FooterText, Me.lvClaimRetirementList.Columns(7).HeaderText)
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(8).FooterText, Me.lvClaimRetirementList.Columns(8).HeaderText)

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Language._Object.setCaption(Me.lvClaimRetirementList.Columns(9).FooterText, Me.lvClaimRetirementList.Columns(9).HeaderText)
            'Pinkal (10-Mar-2021) -- End

        Catch Ex As Exception
            DisplayMessage.DisplayError("SetMessages :- " & Ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)


            Me.lblRetirementNo.Text = Language._Object.getCaption(Me.lblRetirementNo.ID, Me.lblRetirementNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.ChkMyApprovals.Text = Language._Object.getCaption(Me.ChkMyApprovals.ID, Me.ChkMyApprovals.Text)
            Me.lblRetirementApprover.Text = Language._Object.getCaption(Me.lblRetirementApprover.ID, Me.lblRetirementApprover.Text)


            Me.BtnSearch.Text = Language._Object.getCaption(Me.BtnSearch.ID, Me.BtnSearch.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Me.lvClaimRetirementList.Columns(0).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(0).FooterText, Me.lvClaimRetirementList.Columns(0).HeaderText)
            Me.lvClaimRetirementList.Columns(1).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(1).FooterText, Me.lvClaimRetirementList.Columns(1).HeaderText)
            Me.lvClaimRetirementList.Columns(2).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(2).FooterText, Me.lvClaimRetirementList.Columns(2).HeaderText)
            Me.lvClaimRetirementList.Columns(3).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(3).FooterText, Me.lvClaimRetirementList.Columns(3).HeaderText)
            Me.lvClaimRetirementList.Columns(4).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(4).FooterText, Me.lvClaimRetirementList.Columns(4).HeaderText)
            Me.lvClaimRetirementList.Columns(5).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(5).FooterText, Me.lvClaimRetirementList.Columns(5).HeaderText)
            Me.lvClaimRetirementList.Columns(6).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(6).FooterText, Me.lvClaimRetirementList.Columns(6).HeaderText)
            Me.lvClaimRetirementList.Columns(7).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(7).FooterText, Me.lvClaimRetirementList.Columns(7).HeaderText)
            Me.lvClaimRetirementList.Columns(8).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(8).FooterText, Me.lvClaimRetirementList.Columns(8).HeaderText)
            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Me.lvClaimRetirementList.Columns(9).HeaderText = Language._Object.getCaption(Me.lvClaimRetirementList.Columns(9).FooterText, Me.lvClaimRetirementList.Columns(9).HeaderText)
            'Pinkal (10-Mar-2021) -- End


        Catch Ex As Exception
            DisplayMessage.DisplayError("SetLanguage :- " & Ex.Message, Me)
        End Try
    End Sub

  
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "You can't Edit this Imprest detail. Reason: You are logged in into another user login.")
			Language.setMessage(mstrModuleName, 2, "You can't Edit this Imprest detail. Reason: This Imprest is already Cancelled.")
			Language.setMessage(mstrModuleName, 3, "You can't Edit this Imprest detail. Reason: This Imprest is already Approved.")
			Language.setMessage(mstrModuleName, 4, "You can't Edit this Imprest detail. Reason: This Imprest is already Rejected.")
			Language.setMessage(mstrModuleName, 5, "You can't Edit this Imprest detail. Reason: This Imprest approval is still pending.")
			Language.setMessage(mstrModuleName, 6, "Approved By :-")
			Language.setMessage(mstrModuleName, 7, "Rejected By :-")

		Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
