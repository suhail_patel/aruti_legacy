﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class Claim_Retirement_wPg_ClaimRetirementList
    Inherits Basepage

#Region " Private Variables "
    Private Shared ReadOnly mstrModuleName As String = "frmClaimRetirementList"
    Private DisplayMessage As New CommonCodes
    Private objclaimRetirement As New clsclaim_retirement_master
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEMaster As New clsEmployee_Master
        Try
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

          If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With
            objEMaster = Nothing

            
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)

            With cboExpenseCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            Dim objMasterData As New clsMasterData

            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombo = objMasterData.getLeaveStatusList("List")
            dsCombo = objMasterData.getLeaveStatusList("List", "")
            'Pinkal (03-Jan-2020) -- End

            Dim dtab As DataTable = Nothing
            dtab = New DataView(dsCombo.Tables(0), "statusunkid IN (0,1,2,3)", "statusunkid", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "statusunkid"
                .DataTextField = "Name"
                .DataSource = dtab
                .DataBind()
            End With
            objMasterData = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError("FillCombo : " & ex.Message, Me)
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim StrSearch As String = String.Empty
        Try

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewRetireClaimApplication")) = False Then Exit Sub
            End If
            'Pinkal (10-Mar-2021) -- End

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboExpenseCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmclaim_request_master.expensetypeid = '" & CInt(cboExpenseCategory.SelectedValue) & "' "
            End If

            If IsDate(dtpDatefrom.GetDate) AndAlso IsDate(dtpDateto.GetDate) Then
                If dtpDatefrom.GetDate.Date <> Nothing AndAlso dtpDateto.GetDate.Date <> Nothing Then
                    StrSearch &= "AND ISNULL(cmclaim_retirement_master.transactiondate,CONVERT(CHAR(8),cmclaim_request_master.transactiondate,112)) >= '" & eZeeDate.convertDate(dtpDatefrom.GetDate) & "' AND cmclaim_retirement_master.transactiondate <= '" & eZeeDate.convertDate(dtpDateto.GetDate) & "' "
                End If
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearch &= "AND ISNULL(cmclaim_retirement_master.statusunkid,2) = " & CInt(cboStatus.SelectedValue) & " "
            End If

            If StrSearch.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If


            'Pinkal (07-Nov-2019) -- Start
            'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.

            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
                StrSearch &= "AND cmclaim_request_master.employeeunkid = '" & intEmpId & "' "
            End If


            Dim dsList As DataSet = Nothing

            'If isblank Then
            '    dsList = objclaimRetirement.GetList("Retirement", mstrFiler:=StrSearch).Clone()
            'Else
            '    dsList = objclaimRetirement.GetList("Retirement", mstrFiler:=StrSearch)
            'End If

            If isblank Then
                dsList = objclaimRetirement.GetList(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                    , Session("UserAccessModeSetting").ToString(), True, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                    , blnApplyFilter, False, blnApplyFilter, StrSearch).Clone()
            Else
                dsList = objclaimRetirement.GetList(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(Session("UserId")) _
                                                                , Session("UserAccessModeSetting").ToString(), True, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                , blnApplyFilter, False, blnApplyFilter, StrSearch)
            End If

            'Pinkal (07-Nov-2019) -- End



            'Pinkal (20-Feb-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
            If Not dsList.Tables("List").Columns.Contains("IsGrp") Then
                Dim dcCol As New DataColumn("IsGrp", Type.GetType("System.Boolean"))
                dcCol.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcCol)
            End If

            Dim dtTable As DataTable = dsList.Tables(0).Clone


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = dsList.Tables(0).NewRow()
                dr("crmasterunkid") = -1
                dr("claimretirementunkid") = -1
                dr("statusunkid") = -1
                dsList.Tables(0).Rows.Add(dr)
                isblank = True

                dtTable = dsList.Tables(0).Copy()

            ElseIf dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso isblank = False Then
                Dim mdtTran As DataTable
                mdtTran = New DataView(dsList.Tables("List"), "", "employeename", DataViewRowState.CurrentRows).ToTable()
                Dim strCrMasterId As String = ""
                Dim dtRow As DataRow = Nothing

                For Each drow As DataRow In mdtTran.Rows
                    If CStr(drow("crmasterunkid")).Trim <> strCrMasterId.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("crmasterunkid") = drow("crmasterunkid")
                        dtRow("claimretirementunkid") = drow("claimretirementunkid")
                        dtRow("claimrequestno") = ""
                        dtRow("employeename") = drow("claimrequestno").ToString() & " - " & drow("employeename").ToString()
                        dtRow("issubmit_approval") = drow("issubmit_approval")
                        strCrMasterId = drow("crmasterunkid").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In mdtTran.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

            End If

            'gvRetirement.DataSource = dsList.Tables(0)
            'gvRetirement.DataBind()


            'Pinkal (27-Apr-2021)-- Start
            'KBC Enhancement  -  Working on Claim Retirement Enhancement.
            If CInt(cboStatus.SelectedValue) = 3 Then 'Rejected
                dtTable = New DataView(dtTable, "IsGrp = 1 or statusunkid = 3", "", DataViewRowState.CurrentRows).ToTable()
            End If
            'Pinkal (27-Apr-2021) -- End


            gvRetirement.DataSource = dtTable
            gvRetirement.DataBind()

            'Pinkal (20-Feb-2020) -- End

            If isblank Then
                gvRetirement.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError("FillList :- " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (20-Feb-2020) -- Start
    'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Dim xCellColumnSpan As Integer = 0
            rw.BackColor = ColorTranslator.FromHtml("#ECECEC")

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                If gvRetirement.Columns(0).Visible AndAlso gvRetirement.Columns(1).Visible Then
                    xCellColumnSpan = 0
                ElseIf gvRetirement.Columns(0).Visible AndAlso gvRetirement.Columns(1).Visible = False Then
                    xCellColumnSpan = 0
                ElseIf gvRetirement.Columns(0).Visible = False AndAlso gvRetirement.Columns(1).Visible Then
                    xCellColumnSpan = 1
                ElseIf gvRetirement.Columns(0).Visible = False AndAlso gvRetirement.Columns(1).Visible = False Then
                    xCellColumnSpan = 2
                End If
            End If

            rw.Cells(xCellColumnSpan).Font.Bold = True
            rw.Cells(xCellColumnSpan).Style.Add("text-align", "left")
            rw.Cells(xCellColumnSpan).Text = title
            rw.Cells(xCellColumnSpan).ColumnSpan = gd.Columns.Count - 6

            For i = (rw.Cells(xCellColumnSpan).ColumnSpan) To rw.Cells.Count - 2
                rw.Cells(i).Visible = False
            Next

            'Pinkal (10-Mar-2021) -- End
           
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'Pinkal (20-Feb-2020) -- End


#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Not IsPostBack Then
                Call SetControlCaptions()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                FillCombo()
                FillList(True)


                'Pinkal (10-Mar-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    gvRetirement.Columns(0).Visible = CBool(Session("AllowToRetireClaimApplication"))
                    gvRetirement.Columns(1).Visible = CBool(Session("AllowToEditRetiredClaimApplication"))
                End If
                'Pinkal (10-Mar-2021) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError("Page_Load : " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            'If CInt(cboExpenseCategory.SelectedValue) <= 0 Then
            If CInt(cboExpenseCategory.SelectedValue) <= 0 AndAlso CInt(Session("Employeeunkid")) <= 0 Then
                'Pinkal (10-Feb-2021) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Expense Category is compulsory information.Please select Expense Category."), Me)
                cboExpenseCategory.Focus()
                Exit Sub
            End If
            gvRetirement.PageIndex = 0
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = "0"
            cboExpenseCategory.SelectedValue = "0"
            cboStatus.SelectedValue = "0"
            dtpDatefrom.SetDate = Nothing
            dtpDateto.SetDate = Nothing
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnSearch_Click :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError("btnClose_Click:- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region "GridView Events"

    Protected Sub gvRetirement_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRetirement.RowDataBound
        Try

            'Pinkal (20-Feb-2020) -- Start
            'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
            SetDateFormat()
            'Pinkal (20-Feb-2020) -- End

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim lnk As LinkButton = CType(e.Row.FindControl("lnkedit"), LinkButton)

                'Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                'If Convert.ToBoolean(dt.Rows(e.Row.RowIndex)("IsGrp")) = True Then
                If CBool(gvRetirement.DataKeys(e.Row.RowIndex).Values("IsGrp")) = True Then
                    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                    lnk.Visible = False
                    'Me.AddGroup(e.Row, info1.ToTitleCase(dt.Rows(e.Row.RowIndex)("employeename").ToString().ToLower()), gvRetirement)
                    Me.AddGroup(e.Row, info1.ToTitleCase(gvRetirement.DataKeys(e.Row.RowIndex).Values("employeename").ToString().ToLower()), gvRetirement)
                Else

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhtransactionDate", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhtransactionDate", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhtransactionDate", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhtransactionDate", False, True)).Text).ToShortDateString()
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text <> "&nbsp;" Then

                        'Pinkal (04-Jul-2020) -- Start
                        'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
                        'Dim objClaimReqestMaster As New clsclaim_request_master
                        'e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text = Format(objClaimReqestMaster.GetFinalApproverApprovedAmount(CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("employeeunkid").ToString()), CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("expensetypeid").ToString()), CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("crmasterunkid").ToString()), True), Session("fmtCurrency").ToString())
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text), Session("fmtCurrency").ToString())
                        'objClaimReqestMaster = Nothing
                        'Pinkal (04-Jul-2020) -- End
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhRetiredAmount", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhRetiredAmount", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhRetiredAmount", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhRetiredAmount", False, True)).Text).ToString(Session("fmtCurrency").ToString())
                    End If

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhBalance", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhBalance", False, True)).Text <> "&nbsp;" Then
                        '    If CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text) <> 0 AndAlso CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhRetiredAmount", False, True)).Text) <> 0 Then
                        '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhBalance", False, True)).Text = CDec(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhApprovedAmount", False, True)).Text) - CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhRetiredAmount", False, True)).Text)).ToString(Session("fmtCurrency").ToString())
                        '    Else
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhBalance", False, True)).Text = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhBalance", False, True)).Text).ToString(Session("fmtCurrency").ToString())
                        '    End If
                    End If



                    'Pinkal (20-Feb-2020) -- Start
                    'Enhancement Claim Retirement  - Claim Retirement Enhancement given by NMB .
                    'If CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("claimretirementunkid")) > 0 AndAlso CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("statusunkid")) = 2 Then
                    If CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("claimretirementunkid")) > 0 AndAlso CBool(gvRetirement.DataKeys(e.Row.RowIndex).Values("issubmit_approval")) = False AndAlso CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("statusunkid")) = 2 Then
                        'Pinkal (20-Feb-2020) -- End
                        lnk.Visible = True
                    Else
                        lnk.Visible = False
                    End If


                    'Pinkal (07-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                    lnk = Nothing
                    lnk = CType(e.Row.FindControl("lnkRetire"), LinkButton)
                    If CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("statusunkid")) = 2 AndAlso CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("claimretirementunkid")) <= 0 Then
                        lnk.Visible = True
                    Else
                        lnk.Visible = False
                    End If
                    'Pinkal (07-Nov-2019) -- End



                    'Pinkal (03-Mar-2021)-- Start
                    'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
                    If Session("UnRetiredImprestToPayrollAfterDays") IsNot Nothing AndAlso CInt(Session("UnRetiredImprestToPayrollAfterDays")) > 0 Then

                        'Pinkal (27-Apr-2021)-- Start
                        'KBC Enhancement  -  Working on Claim Retirement Enhancement.
                        If gvRetirement.DataKeys(e.Row.RowIndex)("Approvaldate") IsNot Nothing AndAlso IsDBNull(gvRetirement.DataKeys(e.Row.RowIndex)("Approvaldate")) = False Then
                            If CInt(gvRetirement.DataKeys(e.Row.RowIndex)("claimretirementunkid")) <= 0 AndAlso CInt(gvRetirement.DataKeys(e.Row.RowIndex).Values("statusunkid")) = 2 AndAlso DateDiff(DateInterval.Day, eZeeDate.convertDate(gvRetirement.DataKeys(e.Row.RowIndex)("Approvaldate").ToString()).Date, ConfigParameter._Object._CurrentDateAndTime.Date) > CInt(Session("UnRetiredImprestToPayrollAfterDays")) Then
                                lnk.Visible = False
                            End If
                        End If
                        'Pinkal (27-Apr-2021) -- End
                    End If
                    'Pinkal (03-Mar-2021) -- End

                End If

            End If

            'If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhEmployee", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhEmployee", False, True)).Text <> "&nbsp;" Then
            '    Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhEmployee", False, True)).Text = info1.ToTitleCase(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRetirement, "dgcolhEmployee", False, True)).Text.ToString().ToLower())
            '    info1 = Nothing
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError("gvRetirement_RowDataBound :- " & ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvRetirement_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRetirement.RowCommand
        Try

            If e.CommandSource.GetType.FullName = "System.Web.UI.WebControls.LinkButton" Then

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                If e.CommandName = "Retire" Then

                    Session.Add("ClaimRequestId", CInt(gvRetirement.DataKeys(gridRow.RowIndex).Values("crmasterunkid").ToString()))
                    Session.Add("CREmpId", CInt(gvRetirement.DataKeys(gridRow.RowIndex).Values("employeeunkid").ToString()))
                    'Pinkal (07-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                    'Response.Redirect("wPgClaimRetirementInformation.aspx")
                    Response.Redirect(Session("rootpath").ToString() & "Claim_Retirement/wPgClaimRetirementInformation.aspx", False)
                    'Pinkal (07-Nov-2019) -- End
                ElseIf e.CommandName = "Edit" Then
                    Session.Add("ClaimRetirementId", CInt(gvRetirement.DataKeys(gridRow.RowIndex).Values("claimretirementunkid").ToString()))
                    Session.Add("ClaimRequestId", CInt(gvRetirement.DataKeys(gridRow.RowIndex).Values("crmasterunkid").ToString()))
                    Session.Add("CREmpId", CInt(gvRetirement.DataKeys(gridRow.RowIndex).Values("employeeunkid").ToString()))

                    'Pinkal (07-Nov-2019) -- Start
                    'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                    'Response.Redirect("wPgClaimRetirementInformation.aspx")
                    Response.Redirect(Session("rootpath").ToString() & "Claim_Retirement/wPgClaimRetirementInformation.aspx", True)
                    'Pinkal (07-Nov-2019) -- End
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub gvRetirement_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRetirement.PageIndexChanging
        Try
            gvRetirement.PageIndex = e.NewPageIndex
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError("gvRetirement_RowCommand :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblExpenseCategory.ID, Me.lblExpenseCategory.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Language._Object.setCaption(Me.lblDateto.ID, Me.lblDateto.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text.Trim.Replace("&", ""))
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text.Trim.Replace("&", ""))
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))


            Language._Object.setCaption(Me.gvRetirement.Columns(2).FooterText, Me.gvRetirement.Columns(2).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(3).FooterText, Me.gvRetirement.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(4).FooterText, Me.gvRetirement.Columns(4).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(5).FooterText, Me.gvRetirement.Columns(5).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(6).FooterText, Me.gvRetirement.Columns(6).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(7).FooterText, Me.gvRetirement.Columns(7).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(8).FooterText, Me.gvRetirement.Columns(8).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(9).FooterText, Me.gvRetirement.Columns(9).HeaderText)
            Language._Object.setCaption(Me.gvRetirement.Columns(10).FooterText, Me.gvRetirement.Columns(10).HeaderText)


        Catch Ex As Exception
            DisplayMessage.DisplayError("SetControlCaptions :- " & Ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblExpenseCategory.Text = Language._Object.getCaption(Me.lblExpenseCategory.ID, Me.lblExpenseCategory.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Me.lblDateto.Text = Language._Object.getCaption(Me.lblDateto.ID, Me.lblDateto.Text)
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text.Trim.Replace("&", ""))
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text.Trim.Replace("&", ""))
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text.Trim.Replace("&", ""))

            Me.gvRetirement.Columns(2).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(2).FooterText, Me.gvRetirement.Columns(2).HeaderText)
            Me.gvRetirement.Columns(3).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(3).FooterText, Me.gvRetirement.Columns(3).HeaderText)
            Me.gvRetirement.Columns(4).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(4).FooterText, Me.gvRetirement.Columns(4).HeaderText)
            Me.gvRetirement.Columns(5).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(5).FooterText, Me.gvRetirement.Columns(5).HeaderText)
            Me.gvRetirement.Columns(6).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(6).FooterText, Me.gvRetirement.Columns(6).HeaderText)
            Me.gvRetirement.Columns(7).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(7).FooterText, Me.gvRetirement.Columns(7).HeaderText)
            Me.gvRetirement.Columns(8).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(8).FooterText, Me.gvRetirement.Columns(8).HeaderText)
            Me.gvRetirement.Columns(9).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(9).FooterText, Me.gvRetirement.Columns(9).HeaderText)
            Me.gvRetirement.Columns(10).HeaderText = Language._Object.getCaption(Me.gvRetirement.Columns(10).FooterText, Me.gvRetirement.Columns(10).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError("SetLanguage : " & ex.Message, Me)
        End Try
    End Sub

End Class
