﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing

#End Region

Partial Class wPg_RetirementPostToPayroll
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmRetirementPosting"
    Private objRetirementPosting As clsretire_process_tran
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Call SetControlCaptions()
            Call SetMessages()
            Call Language._Object.SaveValue()
            SetLanguage()

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objRetirementPosting = New clsretire_process_tran
            If IsPostBack = False Then
                Call FillCombo()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objemployee As New clsEmployee_Master

            dsList = objemployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            False, "List", True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objemployee = Nothing


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.

            'dsList = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, True)
            'Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable()
            'With cboExpCategory
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dtTable.Copy
            '    .SelectedValue = "0"
            '    .DataBind()
            'End With
            'dtTable.Clear()
            'dtTable = Nothing

            'Dim objExpMst As New clsExpense_Master
            'Dim mstrSearch As String = ""

            'If CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.General_Retirement Then
            '    mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) <= 0"
            'ElseIf CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.ExpenseWise_Retirement Then
            '    mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) > 0"
            'End If

            'dsList = objExpMst.getComboList(-1, True, "List", 0, False, 0, mstrSearch, "")
            'With cboExpense
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = "0"
            '    .DataBind()
            'End With
            'objExpMst = Nothing

            FillExpenseCategory(chkUnReitreTransaction.Checked)
            FillExpense(chkUnReitreTransaction.Checked)

            'Pinkal (10-Feb-2021) -- End

            Dim objPeriod As New clscommom_period_Tran

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "List", True, 1)

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objPeriod = Nothing

            'Dim objtranhead As New clsTransactionHead
            'dsList = objtranhead.getComboList(Session("Database_Name").ToString, "List", True, , , , , , " typeof_id <> " & enTypeOf.Salary)
            'With cboTranhead
            '    .DataValueField = "tranheadunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0)
            '    .SelectedValue = 0
            '    .DataBind()
            'End With
            'objtranhead = Nothing

            Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As DataSet = Nothing
        Dim strSearch As String = ""
        Dim mdtData As DataTable = Nothing
        Try

            If isblank Then
                strSearch = "AND 1=2 "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                strSearch &= "AND cmexpense_master.expensetypeid = " & CInt(cboExpCategory.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND cmretire_process_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If txtRetirementNo.Text.Trim.Length > 0 Then
                strSearch &= "AND cmclaim_retirement_master.claimretirementno LIKE '%" & txtRetirementNo.Text.Trim & "%' "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                strSearch &= "AND cmretire_process_tran.expenseunkid = " & CInt(cboExpense.SelectedValue) & " "
            End If

            If CInt(cboViewBy.SelectedIndex) = 1 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND cmretire_process_tran.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate).ToString() & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND CONVERT(CHAR(8),cmclaim_retirement_master.transactiondate,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate).ToString() & "' "
            End If

            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                strSearch &= "AND cmretire_process_tran.isposted = 0 "
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                strSearch &= "AND cmretire_process_tran.isposted = 1 "
            End If


            'S.SANDEEP |25-FEB-2022| -- START
            'ISSUE : OLD-575
            If chkSkipZeroBalance.Checked Then
                strSearch &= "AND bal_amount <> 0 "
            End If
            'S.SANDEEP |25-FEB-2022| -- END

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                dsList = objRetirementPosting.GetList("List", True, True, , strSearch)
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                dsList = objRetirementPosting.GetList("List", True, True, , strSearch)
            End If

            mdtData = dsList.Tables(0).Copy()

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = mdtData.NewRow()
                dr("ischange") = False
                dr("Ischecked") = False
                dr("Isgroup") = False
                dr("claimretirementunkid") = -1
                dr("claimretirementapprovaltranunkid") = -1
                dr("claimretirementtranunkid") = -1
                mdtData.Rows.Add(dr)
                isblank = True
            End If

            dgvRetirementPosting.DataSource = mdtData
            dgvRetirementPosting.DataBind()

            If isblank Then dgvRetirementPosting.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Function UpdateRow(ByVal drRow As GridViewRow, ByVal mstrPeriod As String) As Boolean
        Try
            If drRow IsNot Nothing Then
                drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhPeriod", False, True)).Text = mstrPeriod
                drRow.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhIschange", False, True)).Text = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.

    Private Sub FillExpenseCategory(ByVal mblnUnRetire As Boolean)
        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try
            If mblnUnRetire = False Then


                With cboViewBy
                    .Items.Clear()
                    Language.setLanguage(mstrModuleName)
                    .Items.Add(Language.getMessage(mstrModuleName, 1, "Show Unposted Retirement Transactions"))
                    .Items.Add(Language.getMessage(mstrModuleName, 2, "Show Posted Retirement Transactions"))
                    .SelectedIndex = 0
                End With

                dsList = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List", False, True)
                dtTable = New DataView(dsList.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable()
                With cboExpCategory
                    .DataValueField = "id"
                    .DataTextField = "name"
                    .DataSource = dtTable.Copy
                    .DataBind()
                End With
            Else

                With cboViewBy
                    .Items.Clear()
                    Language.setLanguage(mstrModuleName)
                    .Items.Add(Language.getMessage(mstrModuleName, 11, "Show Unposted UnRetire Transactions"))
                    .Items.Add(Language.getMessage(mstrModuleName, 12, "Show Posted UnRetire Transactions"))
                    .SelectedIndex = 0
                End With

                dsList = clsExpCommonMethods.Get_ExpenseTypes(True, False, True, "List", True, True)
                With cboExpCategory
                    .DataValueField = "id"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With
            End If
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub FillExpense(ByVal mblnUnRetire As Boolean)
        Dim dsList As DataSet = Nothing
        Try
            Dim objExpMst As New clsExpense_Master
            Dim mstrSearch As String = ""

            If mblnUnRetire = False Then
                If CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.General_Retirement Then
                    mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) <= 0"
                ElseIf CInt(Session("ClaimRetirementTypeId")) = enClaimRetirementType.ExpenseWise_Retirement Then
                    mstrSearch = "ISNULL(cmexpense_master.isimprest,0) = 1 AND ISNULL(cmexpense_master.linkedexpenseunkid,0) > 0"
                End If
            End If

            dsList = objExpMst.getComboList(-1, True, "List", 0, False, 0, mstrSearch, "")
            With cboExpense
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = "0"
                .DataBind()
            End With
            objExpMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
            dsList.Clear()
            dsList = Nothing
        End Try
    End Sub

    Private Sub FillUnRetireList(ByVal isblank As Boolean)
        Dim dsList As DataSet = Nothing
        Dim strSearch As String = ""
        Dim mdtData As DataTable = Nothing
        Try

            If isblank Then
                strSearch = "AND 1=2 "
            End If

            If CInt(cboExpCategory.SelectedValue) > 0 Then
                strSearch &= "AND cmexpense_master.expensetypeid = " & CInt(cboExpCategory.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                strSearch &= "AND ISNULL(cmretire_process_tran.employeeunkid,cmclaim_request_master.employeeunkid) = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If txtRetirementNo.Text.Trim.Length > 0 Then
                strSearch &= "AND cmclaim_request_master.claimretirementno LIKE '%" & txtRetirementNo.Text.Trim & "%' "
            End If

            If CInt(cboExpense.SelectedValue) > 0 Then
                strSearch &= "AND ISNULL(cmretire_process_tran.expenseunkid,cmexpense_master.expenseunkid) = " & CInt(cboExpense.SelectedValue) & " "
            End If

            If CInt(cboViewBy.SelectedIndex) = 1 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                strSearch &= "AND ISNULL(cmretire_process_tran.periodunkid,0) = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            If dtpFromDate.IsNull = False Then
                strSearch &= "AND  CONVERT(CHAR(8),ISNULL(cmclaim_retirement_master.transactiondate,AppClaim.approvaldate),112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate).ToString() & "' "
            End If

            If dtpToDate.IsNull = False Then
                strSearch &= "AND  CONVERT(CHAR(8),ISNULL(cmclaim_retirement_master.transactiondate,AppClaim.approvaldate),112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate).ToString() & "' "
            End If

            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                strSearch &= "AND ISNULL(cmretire_process_tran.isposted,0) = 0 "

                If Session("UnRetiredImprestToPayrollAfterDays") IsNot Nothing AndAlso CInt(Session("UnRetiredImprestToPayrollAfterDays")) > 0 Then
                    strSearch &= "AND DATEDIFF(DAY,AppClaim.approvaldate,GETDATE()) > " & CInt(Session("UnRetiredImprestToPayrollAfterDays")) & " "
                End If

            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                strSearch &= "AND ISNULL(cmretire_process_tran.isposted,0) = 1 "
            End If

            'S.SANDEEP |25-FEB-2022| -- START
            'ISSUE : OLD-575
            If chkSkipZeroBalance.Checked Then
                strSearch &= "AND bal_amount <> 0 "
            End If
            'S.SANDEEP |25-FEB-2022| -- END

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            If CInt(cboViewBy.SelectedIndex) = 0 Then   ' UNPOSTED
                dsList = objRetirementPosting.GetUnRetiredList("List", True, True, , strSearch)
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then   ' POSTED
                dsList = objRetirementPosting.GetUnRetiredList("List", True, True, , strSearch)
            End If

            mdtData = dsList.Tables(0).Copy()

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = mdtData.NewRow()
                dr("ischange") = False
                dr("Ischecked") = False
                dr("Isgroup") = False
                dr("claimretirementunkid") = -1
                dr("claimretirementapprovaltranunkid") = -1
                dr("claimretirementtranunkid") = -1
                dr("crmasterunkid") = -1
                dr("crapprovaltranunkid") = -1
                mdtData.Rows.Add(dr)
                isblank = True
            End If

            dgvRetirementPosting.DataSource = mdtData
            dgvRetirementPosting.DataBind()

            If isblank Then dgvRetirementPosting.Rows(0).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    'Pinkal (10-Feb-2021) -- End


#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboViewBy.SelectedIndex) = 1 And CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            If chkUnReitreTransaction.Checked = False Then
                Call FillList(False)
            Else
                FillUnRetireList(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboViewBy.SelectedIndex = 0
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            cboEmployee.SelectedIndex = 0
            txtRetirementNo.Text = ""
            cboExpCategory.SelectedIndex = 0
            cboExpense.SelectedIndex = 0
            cboPeriod.SelectedIndex = 0
            'cboTranhead.SelectedIndex = 0

            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            chkUnReitreTransaction.Checked = False
            chkUnReitreTransaction_CheckedChanged(chkUnReitreTransaction, New EventArgs())
            'Pinkal (10-Feb-2021) -- End

            Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub btnPost_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPosting.Click, btnUnposting.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            'If CType(sender, Button).ID = btnPosting.ID Then
            '    If CInt(cboTranhead.SelectedValue) <= 0 Then
            '        Language.setLanguage(mstrModuleName)
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Transaction head is compulsory information.Please Select Transaction head."), Me)
            '        cboTranhead.Focus()
            '        Exit Sub
            '    End If
            'End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvRetirementPosting.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please check atleast one retirement transaction to do futher operation on it."), Me)
                Exit Sub
            End If

            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(Session("Database_Name").ToString()) = CInt(cboPeriod.SelectedValue)
                Dim mdtStartDate As Date = objPrd._Start_Date
                Dim mdtEndDate As Date = objPrd._End_Date
                objPrd = Nothing
                Dim lstIDs As List(Of String) = gRow.AsEnumerable().Select(Function(x) dgvRetirementPosting.DataKeys(x.RowIndex).Values("employeeunkid").ToString()).Distinct.ToList()
                Dim strEmpIDs As String = String.Join(",", CType(lstIDs.ToArray(), String()))

                Dim objLeaveTran As New clsTnALeaveTran
                If objLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpIDs, mdtEndDate.Date, enModuleReference.Payroll) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling."), Me)
                    Exit Sub
                End If
            End If

            'For i As Integer = 0 To gRow.Count - 1
            '    If CType(sender, Button).ID = btnPosting.ID Then
            '        gRow(i).Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhTranhead", False, True)).Text = cboTranhead.SelectedItem.Text
            '    ElseIf CType(sender, Button).ID = btnUnposting.ID Then
            '        gRow(i).Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhTranhead", False, True)).Text = ""
            '    End If
            '    gRow(i).Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhIschange", False, True)).Text = True
            'Next
            If CType(sender, Button).ID = btnPosting.ID Then
                gRow.ToList().ForEach(Function(x) UpdateRow(x, cboPeriod.SelectedItem.Text))
            ElseIf CType(sender, Button).ID = btnUnposting.ID Then
                gRow.ToList().ForEach(Function(x) UpdateRow(x, ""))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvRetirementPosting.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkselect"), CheckBox).Checked = True _
                                                                        And CBool(dgvRetirementPosting.DataKeys(x.RowIndex).Values("Isgroup")) = False _
                                                                        And x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhIschange", False, True)).Text = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please check atleast one retirement transaction to do futher operation on it."), Me)
                Exit Sub
            End If

            'Dim mdtMinTransactionDate As Date = Nothing
            'Dim mdtMaxTransactionDate As Date = Nothing

            'mdtMinTransactionDate = gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text <> "&nbsp;" And _
            '                                                x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text.Trim() <> "") _
            '                                                .Select(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text).Min()

            'mdtMaxTransactionDate = gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text <> "&nbsp;" And _
            '                                                            x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text.Trim() <> "") _
            '                                                            .Select(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text).Max()


            Dim mstrRetirementProcessIds As String = ""
            Dim arProcessIdList As New ArrayList()
            Dim xBunchLimit As Integer = 1
            Dim mintDays As Integer = 0


            'mintDays = DateDiff(DateInterval.Day, mdtMinTransactionDate.Date, mdtMaxTransactionDate.AddDays(1))

            'If mintDays > xBunchDays Then
            '    Dim mdtFromDate As Date = mdtMinTransactionDate.Date
            '    Dim mdtToDate As Date = mdtMaxTransactionDate.Date
            '    Dim xCount As Integer = Math.Ceiling(mintDays / xBunchDays)

            '    For i As Integer = 0 To xCount - 1

            '        If dtpToDate.GetDate.Date <= mdtToDate.Date.AddDays(xBunchDays - 1) Then
            '            mdtToDate = mdtToDate.AddDays(DateDiff(DateInterval.Day, mdtMinTransactionDate.Date, mdtToDate.Date.AddDays(1)))
            '        Else
            '            mdtToDate = mdtFromDate.AddDays(xBunchDays - 1)
            '        End If

            '        mstrRetirementProcessIds = String.Join(",", (From p In gRow Where dgvRetirementPosting.DataKeys(p.RowIndex)("transactiondate") >= eZeeDate.convertDate(mdtFromDate.Date) _
            '                                                                         And dgvRetirementPosting.DataKeys(p.RowIndex)("transactiondate") <= eZeeDate.convertDate(mdtToDate.Date) _
            '                                                                         Select (dgvRetirementPosting.DataKeys(p.RowIndex)("crretirementprocessunkid").ToString)).ToArray())

            '        If mstrRetirementProcessIds.Trim.Length > 0 Then
            '            arProcessIdList.Add(mstrRetirementProcessIds)
            '        End If


            '        mdtFromDate = mdtFromDate.Date.AddDays(xBunchDays)
            '        mstrRetirementProcessIds = ""
            '    Next

            'Else
            '    mstrRetirementProcessIds = String.Join(",", (From p In gRow Select (dgvRetirementPosting.DataKeys(p.RowIndex)("crretirementprocessunkid").ToString)).ToArray())
            '    arProcessIdList.Add(mstrRetirementProcessIds)
            'End If


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Dim mstrClaimApprovalIds As String = ""
            Dim arClaimApprovalIdList As New ArrayList()
            'Pinkal (10-Feb-2021) -- End

            Dim xCount As Integer = Math.Ceiling(gRow.Count / xBunchLimit)

            If xCount > xBunchLimit Then

                Dim xStartCount As Integer = 0
                Dim LstApprovedRetirement = gRow.ToList()

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                mstrRetirementProcessIds = ""
                mstrClaimApprovalIds = ""
                'Pinkal (10-Feb-2021) -- End


                For i As Integer = 0 To xCount - 1
                    If LstApprovedRetirement.Count - 1 <= xStartCount Then
                        xStartCount = LstApprovedRetirement.Count - 1
                        xBunchLimit = 1
                    End If

                    'Pinkal (10-Feb-2021) -- Start
                    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                    'mstrRetirementProcessIds = String.Join(",", LstApprovedRetirement.GetRange(xStartCount, xBunchLimit).Select(Function(x) dgvRetirementPosting.DataKeys(x.RowIndex)("crretirementprocessunkid").ToString()).ToArray())
                    mstrRetirementProcessIds = String.Join(",", LstApprovedRetirement.GetRange(xStartCount, xBunchLimit).Select(Function(x) dgvRetirementPosting.DataKeys(x.RowIndex)("crretirementprocessunkid").ToString()).Distinct().ToArray())
                    If mstrRetirementProcessIds.Trim.Length > 0 AndAlso mstrRetirementProcessIds.Trim <> "0" Then
                        arProcessIdList.Add(mstrRetirementProcessIds)
                    End If

                    mstrClaimApprovalIds = String.Join(",", LstApprovedRetirement.GetRange(xStartCount, xBunchLimit).Where(Function(x) dgvRetirementPosting.DataKeys(x.RowIndex)("crretirementprocessunkid") <= 0).Select(Function(x) dgvRetirementPosting.DataKeys(x.RowIndex)("crapprovaltranunkid").ToString()).Distinct().ToArray())
                    If mstrClaimApprovalIds.Trim.Length > 0 AndAlso mstrClaimApprovalIds.Trim <> "0" Then
                        arClaimApprovalIdList.Add(mstrClaimApprovalIds)
                    End If

                    mstrClaimApprovalIds = ""
                    mstrRetirementProcessIds = ""
                    xStartCount += xBunchLimit

                    'Pinkal (10-Feb-2021) -- End

                Next

            Else

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'mstrRetirementProcessIds = String.Join(",", (From p In gRow Select (dgvRetirementPosting.DataKeys(p.RowIndex)("crretirementprocessunkid").ToString)).ToArray())

                mstrRetirementProcessIds = ""
                mstrClaimApprovalIds = ""

                mstrRetirementProcessIds = String.Join(",", (From p In gRow Select (dgvRetirementPosting.DataKeys(p.RowIndex)("crretirementprocessunkid").ToString())).Distinct().ToArray())
                If mstrRetirementProcessIds.Trim.Length > 0 AndAlso mstrRetirementProcessIds.Trim <> "0" Then
                    arProcessIdList.Add(mstrRetirementProcessIds)
                End If

                mstrClaimApprovalIds = String.Join(",", (From p In gRow Where dgvRetirementPosting.DataKeys(p.RowIndex)("crretirementprocessunkid") <= 0 Select (dgvRetirementPosting.DataKeys(p.RowIndex)("crapprovaltranunkid").ToString())).Distinct().ToArray())
                If mstrClaimApprovalIds.Trim.Length > 0 AndAlso mstrClaimApprovalIds.Trim <> "0" Then
                    arClaimApprovalIdList.Add(mstrClaimApprovalIds)
                End If

                mstrClaimApprovalIds = ""
                mstrRetirementProcessIds = ""

                'Pinkal (10-Feb-2021) -- End

            End If


            objRetirementPosting._Userunkid = CInt(Session("UserId"))
            objRetirementPosting._WebFormName = "frmRetirementPosting"
            objRetirementPosting._WebClientIP = Session("IP_ADD").ToString()
            objRetirementPosting._WebHostName = Session("HOST_NAME").ToString()
            objRetirementPosting._IsWeb = True

            If CInt(cboViewBy.SelectedIndex) = 0 Then

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'If objRetirementPosting.Posting_Unposting_Retirement(True, CInt(cboPeriod.SelectedValue), 0, arProcessIdList) = False Then
                If objRetirementPosting.Posting_Unposting_Retirement(True, CInt(cboPeriod.SelectedValue), 0, arProcessIdList, arClaimApprovalIdList) = False Then
                    'Pinkal (10-Feb-2021) -- End
                    DisplayMessage.DisplayMessage(objRetirementPosting._Message, Me)
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Retirement transaction posting done successfully."), Me)
                    FillList(True)
                    btnReset_Click(sender, New EventArgs())
                End If
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then

                'Pinkal (10-Feb-2021) -- Start
                'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
                'If objRetirementPosting.Posting_Unposting_Retirement(False, 0, 0, arProcessIdList) = False Then
                If objRetirementPosting.Posting_Unposting_Retirement(False, 0, 0, arProcessIdList, arClaimApprovalIdList) = False Then
                    'Pinkal (10-Feb-2021) -- End
                    DisplayMessage.DisplayMessage(objRetirementPosting._Message, Me)
                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Retirement transaction unposting done successfully."), Me)
                    FillList(True)
                    btnReset_Click(sender, New EventArgs())
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString() & "UserHome.aspx")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try
            'cboTranhead.SelectedValue = "0"
            cboPeriod.SelectedValue = "0"
            If CInt(cboViewBy.SelectedIndex) = 0 Then
                'cboTranhead.Enabled = True
                cboPeriod.Enabled = True
                btnPosting.Visible = True
                btnUnposting.Visible = False
            ElseIf CInt(cboViewBy.SelectedIndex) = 1 Then
                'cboTranhead.Enabled = False
                btnPosting.Visible = False
                btnUnposting.Visible = True
            End If
            FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid Event "

    Protected Sub dgvRetirementPosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvRetirementPosting.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If CBool(dgvRetirementPosting.DataKeys(e.Row.RowIndex).Values("Isgroup")) = True Then
                    For i = 2 To dgvRetirementPosting.Columns.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                    Dim chk As CheckBox = e.Row.Cells(0).FindControl("chkselect")
                    chk.Visible = False
                    e.Row.Cells(1).Font.Bold = True
                    e.Row.Cells(1).ColumnSpan = dgvRetirementPosting.Columns.Count - 1
                    e.Row.Cells(0).CssClass = "GroupHeaderStyle"
                    e.Row.Cells(1).CssClass = "GroupHeaderStyle"

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhIschange", False, True)).Text = False

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text = ""

                ElseIf CBool(dgvRetirementPosting.DataKeys(e.Row.RowIndex).Values("Isgroup")) = False Then

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhbalamount", False, True)).Text <> "" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhbalamount", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhbalamount", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "colhbalamount", False, True)).Text), Session("fmtcurrency").ToString())
                    End If

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhIschange", False, True)).Text = CBool(dgvRetirementPosting.DataKeys(e.Row.RowIndex).Values("ischange"))

                    If dgvRetirementPosting.DataKeys(e.Row.RowIndex).Values("transactiondate").ToString().Trim.Length > 0 AndAlso dgvRetirementPosting.DataKeys(e.Row.RowIndex).Values("transactiondate").ToString().Trim <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(dgvRetirementPosting, "objcolhtransactiondate", False, True)).Text = eZeeDate.convertDate(dgvRetirementPosting.DataKeys(e.Row.RowIndex).Values("transactiondate").ToString().Trim()).Date
                    End If

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

#End Region

    'Pinkal (10-Feb-2021) -- Start
    'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
#Region "CheckBox Events"

    Protected Sub chkUnReitreTransaction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkUnReitreTransaction.CheckedChanged
        Try
            If chkUnReitreTransaction.Checked Then
                LblRetirementNo.Text = Language.getMessage(mstrModuleName, 9, "Claim No")
            Else
                LblRetirementNo.Text = Language.getMessage(mstrModuleName, 10, "Retirement No")
            End If
            txtRetirementNo.Text = ""
            FillExpenseCategory(chkUnReitreTransaction.Checked)
            FillExpense(chkUnReitreTransaction.Checked)
            cboViewBy_SelectedIndexChanged(cboViewBy, New EventArgs())
            'S.SANDEEP |25-FEB-2022| -- START
            'ISSUE : OLD-575
            RemoveHandler chkSkipZeroBalance.CheckedChanged, AddressOf chkSkipZeroBalance_CheckedChanged
            chkSkipZeroBalance.Checked = False
            If chkUnReitreTransaction.Checked Then
                chkSkipZeroBalance.Visible = False
            Else
                chkSkipZeroBalance.Visible = True
            End If
            AddHandler chkSkipZeroBalance.CheckedChanged, AddressOf chkSkipZeroBalance_CheckedChanged
            'S.SANDEEP |25-FEB-2022| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |25-FEB-2022| -- START
    'ISSUE : OLD-575
    Protected Sub chkSkipZeroBalance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSkipZeroBalance.CheckedChanged
        Try
            Call btnSearch_Click(New Object, New EventArgs)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
    'S.SANDEEP |25-FEB-2022| -- END
    

#End Region
    'Pinkal (10-Feb-2021) -- End



    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(mstrModuleName, Me.lblPageHeader.Text)
            Language._Object.setCaption("gbFilterCriteria", Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.LblRetirementNo.ID, Me.LblRetirementNo.Text)
            Language._Object.setCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Language._Object.setCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Language._Object.setCaption(Me.LblExpense.ID, Me.LblExpense.Text)
            'Language._Object.setCaption(Me.LblTranHead.ID, Me.LblTranHead.Text)
            Language._Object.setCaption(Me.lblViewBy.ID, Me.lblViewBy.Text)
            Language._Object.setCaption(Me.btnUnposting.ID, Me.btnUnposting.Text)
            Language._Object.setCaption(Me.btnPosting.ID, Me.btnPosting.Text)
            Language._Object.setCaption(Me.btnSave.ID, Me.btnSave.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Language._Object.setCaption(Me.LblFromDate.ID, Me.LblFromDate.Text)
            Language._Object.setCaption(Me.LblToDate.ID, Me.LblToDate.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            'Pinkal (10-Mar-2021) -- End

            'S.SANDEEP |25-FEB-2022| -- START
            'ISSUE : OLD-575
            Language._Object.setCaption(Me.chkSkipZeroBalance.ID, Me.chkSkipZeroBalance.Text)
            'S.SANDEEP |25-FEB-2022| -- END

            Language._Object.setCaption(Me.dgvRetirementPosting.Columns(1).FooterText, Me.dgvRetirementPosting.Columns(1).HeaderText)
            Language._Object.setCaption(Me.dgvRetirementPosting.Columns(2).FooterText, Me.dgvRetirementPosting.Columns(2).HeaderText)
            Language._Object.setCaption(Me.dgvRetirementPosting.Columns(3).FooterText, Me.dgvRetirementPosting.Columns(3).HeaderText)
            Language._Object.setCaption(Me.dgvRetirementPosting.Columns(4).FooterText, Me.dgvRetirementPosting.Columns(4).HeaderText)
            Language._Object.setCaption(Me.dgvRetirementPosting.Columns(5).FooterText, Me.dgvRetirementPosting.Columns(5).HeaderText)


        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.LblRetirementNo.Text = Language._Object.getCaption(Me.LblRetirementNo.ID, Me.LblRetirementNo.Text)
            Me.lblExpCategory.Text = Language._Object.getCaption(Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.LblExpense.Text = Language._Object.getCaption(Me.LblExpense.ID, Me.LblExpense.Text)
            'Me.LblTranHead.Text = Language._Object.getCaption(Me.LblTranHead.ID, Me.LblTranHead.Text)
            Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.ID, Me.lblViewBy.Text)
            Me.btnUnposting.Text = Language._Object.getCaption(Me.btnUnposting.ID, Me.btnUnposting.Text).Replace("&", "")
            Me.btnPosting.Text = Language._Object.getCaption(Me.btnPosting.ID, Me.btnPosting.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.ID, Me.LblFromDate.Text)
            Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.ID, Me.LblToDate.Text)
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text)
            'Pinkal (10-Mar-2021) -- End

            'S.SANDEEP |25-FEB-2022| -- START
            'ISSUE : OLD-575
            Me.chkSkipZeroBalance.Text = Language._Object.getCaption(Me.chkSkipZeroBalance.ID, Me.chkSkipZeroBalance.Text)
            'S.SANDEEP |25-FEB-2022| -- END

            Me.dgvRetirementPosting.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvRetirementPosting.Columns(1).FooterText, Me.dgvRetirementPosting.Columns(1).HeaderText)
            Me.dgvRetirementPosting.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvRetirementPosting.Columns(2).FooterText, Me.dgvRetirementPosting.Columns(2).HeaderText)
            Me.dgvRetirementPosting.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvRetirementPosting.Columns(3).FooterText, Me.dgvRetirementPosting.Columns(3).HeaderText)
            Me.dgvRetirementPosting.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvRetirementPosting.Columns(4).FooterText, Me.dgvRetirementPosting.Columns(4).HeaderText)
            Me.dgvRetirementPosting.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvRetirementPosting.Columns(5).FooterText, Me.dgvRetirementPosting.Columns(5).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Show Unposted Retirement Transactions")
            Language.setMessage(mstrModuleName, 2, "Show Posted Retirement Transactions")
            Language.setMessage(mstrModuleName, 3, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 5, "Please check atleast one retirement transaction to do futher operation on it.")
            Language.setMessage(mstrModuleName, 6, "Retirement transaction posting done successfully.")
            Language.setMessage(mstrModuleName, 7, "Retirement transaction unposting done successfully.")
            Language.setMessage(mstrModuleName, 8, "You can't post/unpost selected transactions.Reason: Process Payroll is already done for last date of period in which selected transaction dates are falling.")
            Language.setMessage(mstrModuleName, 9, "Claim No")
            Language.setMessage(mstrModuleName, 10, "Retirement No")
            Language.setMessage(mstrModuleName, 11, "Show Unposted UnRetire Transactions")
            Language.setMessage(mstrModuleName, 12, "Show Posted UnRetire Transactions")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex.Message, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
