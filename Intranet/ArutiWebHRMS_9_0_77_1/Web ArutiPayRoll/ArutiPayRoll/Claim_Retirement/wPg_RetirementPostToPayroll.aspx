﻿<%@ Page Title="Post To Payroll" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_RetirementPostToPayroll.aspx.vb" Inherits="wPg_RetirementPostToPayroll"  %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        $("[id*=chkselectAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkselect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
        
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Post To Payroll"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Retirement Posting"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblViewBy" runat="server" Text="View Type"></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="cboViewBy" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                             <td style="width: 10%">
                                                <asp:Label ID="LblRetirementNo" runat="server" Text="Retirement No"></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:TextBox ID="txtRetirementNo" runat="server" Width="99%">
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                          <td style="width: 10%">
                                                <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                          </td>
                                          <td style="width: 23%">
                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                          </td>
                                          <td style="width: 10%">
                                                <asp:Label ID="lblExpCategory" runat="server" Text="Exp. Cat."></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="cboExpCategory" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                           <%-- <td style="width: 11%">
                                                <asp:Label ID="LblTranHead" runat="server" Text="Tran. Head"></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="cboTranhead" runat="server">
                                                </asp:DropDownList>
                                            </td>--%>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                      <tr style="width: 100%;">
                                          <td style="width: 10%">
                                                <asp:Label ID="LblToDate" runat="server" Text="To Date"></asp:Label>
                                          </td>
                                          <td style="width: 23%">
                                                <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                          </td>
                                           <td style="width: 10%">
                                                <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="cboExpense" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                             <td style="width: 11%">
                                             </td>
                                             <td style="width: 23%">
                                                <asp:CheckBox ID="chkUnReitreTransaction" runat="server" Text="Show UnRetire Transaction"
                                                    AutoPostBack="true"></asp:CheckBox>
                                            </td>
                                      </tr>
                                    </table>
                                    <div class="btn-default">
                                        <%--'S.SANDEEP |25-FEB-2022| -- START--%>
                                        <%--'ISSUE : OLD-575--%>
                                        <div style="float: left">
                                            <asp:CheckBox ID="chkSkipZeroBalance" runat="server" Text="Don't Show Zero Balance"
                                                AutoPostBack="true"></asp:CheckBox>
                                        </div>
                                        <%--'S.SANDEEP |25-FEB-2022| -- END--%>
                                         <asp:Button ID="btnUnposting" runat="server" Text="UnPost" CssClass="btndefault" />
                                        <asp:Button ID="btnPosting" runat="server" Text="Post" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvdata" runat="server" Height="350px" ScrollBars="Auto" Width="100%">
                                                    <asp:GridView ID="dgvRetirementPosting" runat="server" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" ShowFooter="false" Width="99%"
                                                        DataKeyNames="crretirementprocessunkid,claimretirementunkid,claimretirementtranunkid,crmasterunkid,crapprovaltranunkid,Retirement,employeeunkid,Isgroup,Ischecked,ischange,tranheadunkid,transactiondate">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkselectAll" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkselect" runat="server"  />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Retirement" HeaderText="Particulars" FooterText="colhParticulars" />
                                                            <asp:BoundField DataField="Expense" HeaderText="Expenses" FooterText="colhExpense" />
                                                            <asp:BoundField DataField="bal_amount" HeaderText="Balance" FooterText="colhbalamount"
                                                                ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="currency_sign" HeaderText="Currency" FooterText="colhCurrency" />
                                                            <asp:BoundField DataField="Period" HeaderText="Period" FooterText="colhPeriod" />
                                                          <%--  <asp:BoundField DataField="tranhead" HeaderText="Transaction Head" FooterText="colhTranhead"/>--%>
                                                            <asp:BoundField DataField="transactiondate" HeaderText="Transaction Date" FooterText="objcolhtransactiondate"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="ischange" HeaderText="IsChange" FooterText="objcolhIschange"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
