﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class AttributeCollection : CollectionBase, IWriter
    {
        internal AttributeCollection()
        {
        }

        public int Add(ExcelWriter.Schemas.Attribute attribute)
        {
            return base.InnerList.Add(attribute);
        }

        public ExcelWriter.Schemas.Attribute Add(string type)
        {
            ExcelWriter.Schemas.Attribute attribute = new ExcelWriter.Schemas.Attribute(type);
            this.Add(attribute);
            return attribute;
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(ExcelWriter.Schemas.Attribute item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(ExcelWriter.Schemas.Attribute[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(ExcelWriter.Schemas.Attribute item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, ExcelWriter.Schemas.Attribute item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(ExcelWriter.Schemas.Attribute item)
        {
            base.InnerList.Remove(item);
        }

        public ExcelWriter.Schemas.Attribute this[int index]
        {
            get
            {
                return (ExcelWriter.Schemas.Attribute) base.InnerList[index];
            }
        }
    }
}

