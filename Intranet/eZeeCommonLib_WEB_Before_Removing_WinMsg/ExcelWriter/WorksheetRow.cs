﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetRow : IWriter, IReader, ICodeWriter
    {
        private bool _autoFitHeight = true;
        private WorksheetCellCollection _cells;
        private int _height = -2147483648;
        private int _index;
        internal WorksheetTable _table;

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._index > 0)
            {
                Util.AddAssignment(method, targetObject, "Index", this._index);
            }
            if (this._height != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "Height", this._height);
            }
            if (!this._autoFitHeight)
            {
                Util.AddAssignment(method, targetObject, "AutoFitHeight", this._autoFitHeight);
            }
            if (this._cells != null)
            {
                Util.Traverse(type, this._cells, method, targetObject, "Cells");
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._index = Util.GetAttribute(element, "Index", "urn:schemas-microsoft-com:office:spreadsheet", 0);
            this._height = Util.GetAttribute(element, "Height", "urn:schemas-microsoft-com:office:spreadsheet", -2147483648);
            this._autoFitHeight = Util.GetAttribute(element, "AutoFitHeight", "urn:schemas-microsoft-com:office:spreadsheet", true);
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if ((element2 != null) && WorksheetCell.IsElement(element2))
                {
                    WorksheetCell cell = new WorksheetCell();
                    ((IReader) cell).ReadXml(element2);
                    this.Cells.Add(cell);
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Row", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._index > 0)
            {
                writer.WriteAttributeString("s", "Index", "urn:schemas-microsoft-com:office:spreadsheet", this._index.ToString());
            }
            if (this._height != -2147483648)
            {
                writer.WriteAttributeString("s", "Height", "urn:schemas-microsoft-com:office:spreadsheet", this._height.ToString());
            }
            if (!this._autoFitHeight)
            {
                writer.WriteAttributeString("s", "AutoFitHeight", "urn:schemas-microsoft-com:office:spreadsheet", "0");
            }
            if (this._cells != null)
            {
                ((IWriter) this._cells).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Row", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public bool AutoFitHeight
        {
            get
            {
                return this._autoFitHeight;
            }
            set
            {
                this._autoFitHeight = value;
            }
        }

        public WorksheetCellCollection Cells
        {
            get
            {
                if (this._cells == null)
                {
                    this._cells = new WorksheetCellCollection(this);
                }
                return this._cells;
            }
        }

        public int Height
        {
            get
            {
                if ((this._height == -2147483648) && (this._table != null))
                {
                    return (int) this._table.DefaultRowHeight;
                }
                return this._height;
            }
            set
            {
                this._height = value;
            }
        }

        public int Index
        {
            get
            {
                return this._index;
            }
            set
            {
                this._index = value;
            }
        }

        public WorksheetTable Table
        {
            get
            {
                return this._table;
            }
        }
    }
}

