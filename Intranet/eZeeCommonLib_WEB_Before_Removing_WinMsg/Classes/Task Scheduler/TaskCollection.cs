using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

#region  Imports 

using System.ComponentModel;
using System.Runtime.InteropServices;

using TaskScheduler.Definition;

#endregion

namespace TaskScheduler
{

    public sealed class TaskCollection : System.Collections.ReadOnlyCollectionBase, System.IDisposable
	{

#region  Fields 
		private ISchedulingAgent m_SchedulingAgent;

		private static Guid CLSID_Task = new Guid("{148BD520-A2AB-11CE-B11F-00AA00530503}");
		private static Guid IID_ITask = new Guid("{148BD524-A2AB-11CE-B11F-00AA00530503}");
#endregion

#region  Constructors 

		public TaskCollection()
		{
			CreateSAInstance();
			this.Refresh();
		}

		public TaskCollection(string targetComputer)
		{
			CreateSAInstance();
			m_SchedulingAgent.SetTargetComputer(targetComputer);

			this.Refresh();
		}

#endregion

#region  Protected Methods

    ~TaskCollection()
    {
        this.Dispose();
    }

    public void Dispose()
    {
        if (m_SchedulingAgent != null)
        {
            // Release the Task Scheduler object
            Marshal.ReleaseComObject(m_SchedulingAgent);
            m_SchedulingAgent = null;
            GC.SuppressFinalize(this);
        }

    }
#endregion

#region  Public Properties 

		public string get_ItemName(int index)
		{
			return (string)(base.InnerList[index]);
		}

		public Task this[string name]
		{
			get
			{

				ITask ITask = null;

				// Open the Task object
				ITask = m_SchedulingAgent.Activate(name, ref IID_ITask);

				return new Task(name, ITask);

			}
		}

		public Task this[int index]
		{
			get
			{
				ITask ITaskObj = null;
				string Name = null;

				// Get the name
				Name = (string)(base.InnerList[index]);

				// Open the ITask object
				ITaskObj = m_SchedulingAgent.Activate(Name, ref IID_ITask);

				// Return a Task object
				return new Task(Name, ITaskObj);

			}
		}

		public string TargetComputer
		{
			get
			{
				return m_SchedulingAgent.GetTargetComputer();
			}
			set
			{
				m_SchedulingAgent.SetTargetComputer(value);
				this.Refresh();
			}
		}

#endregion

#region  Public Methods 

		public Task Add(string name)
		{
			System.Runtime.InteropServices.ComTypes.IPersistFile oIPF = null;
			ITask oITask = null;

			// Create the new Task object
			oITask = m_SchedulingAgent.NewWorkItem(name, ref CLSID_Task, ref IID_ITask);

			// Get the IPersistFile interface
			oIPF = (System.Runtime.InteropServices.ComTypes.IPersistFile)oITask;
			oIPF.Save(null, true);

			// Refresh the collection
			this.Refresh();

			return new Task(name, oITask);

		}

		public Task Add(string name, string creatorName)
		{
			Task NewTask = null;

			NewTask = Add(name);
			NewTask.Creator = creatorName;

			return NewTask;

		}

		public Task Add(string name, string exeName, string commandLine)
		{
			Task NewTask = null;

			NewTask = Add(name);

			NewTask.ApplicationName = exeName;
			NewTask.CommandLine = commandLine;

			return NewTask;

		}

        public void Refresh()
        {
            IEnumWorkItems IEnum = null;
            IntPtr Ptr = default(IntPtr);
            int Fetched = 0;
            string FileName = null;

            base.InnerList.Clear();

            // Get the Tasks enumerator class
            IEnum = m_SchedulingAgent.Enums();

            try
            {
                // Get all the Tasks
                while (IEnum.Next(1, out Ptr, out Fetched) == 0)
                {
                    // Get the name from the pointer
                    FileName = StrFromPtrPtr(Ptr);

                    // Add the task name to the collection
                    base.InnerList.Add(FileName);
                }
            }
            finally
            {
                // Release the COM object
                Marshal.ReleaseComObject(IEnum);
            }
        }

		public void Remove(int index)
		{
			// Remove the scheduler item
			m_SchedulingAgent.Delete((string)(base.InnerList[index]));

			// Remove the item from Tasks collection
			base.InnerList.RemoveAt(index);
		}

		public void Remove(string name)
		{
			// Remove the scheduler item
			m_SchedulingAgent.Delete(name);

			// Remove the item from Tasks collection
			base.InnerList.Remove(name);
		}

#endregion

#region  Private Methods 

		private void CreateSAInstance()
		{
			System.Guid CLSID = new System.Guid("{148BD52A-A2AB-11CE-B11F-00AA00530503}");
			System.Type SAType = null;

			SAType = Type.GetTypeFromCLSID(CLSID);

			m_SchedulingAgent = (ISchedulingAgent)(Activator.CreateInstance(SAType));

		}

		private string StrFromPtrPtr(IntPtr pointer)
		{
			string tempStrFromPtrPtr = null;
			IntPtr Ptr1 = default(IntPtr);

			// Get the string pointer from the pointer
			Ptr1 = new IntPtr(Marshal.ReadInt32(pointer, 0));

			// Get the string from the pointer
			tempStrFromPtrPtr = Marshal.PtrToStringUni(Ptr1);

			// Release both pointers
			Marshal.FreeCoTaskMem(Ptr1);
			Marshal.FreeCoTaskMem(pointer);

			return tempStrFromPtrPtr;
		}

#endregion

	}

}

