//************************************************************************************************************************************
//Class Name : clsMain.vb [DataOperation]
//Purpose    : 
//Date       : 
//Written By : Sandeep Sharma
//Modified   : Sandeep Sharma
//************************************************************************************************************************************
using Microsoft.SqlServer;
using Microsoft.SqlServer.Management.Common;
using System.Collections;
using System.Data;
using System;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Security.AccessControl;
namespace eZeeCommonLib
{

    public class eZeeDatabase
    {
        private readonly string mstrModuleName = "Class Database";

        private string strServerName = "(Local)";
        private string strInstName = "SQLEXPRESS";
        private static string strDatabaseName = "hrmsConfiguration";
        private string strPassWord = "Qw0hgX7HJ0cge3U8zJbx44dZa+ZiNCzTLLNaxe5cIlv5vcoS7F/wiymPNWvVT+YK";

        /// <summary>
        /// A SQL Database Server Name.
        /// </summary>
        /// <value>String</value>
        /// <returns>String</returns>
        /// <remarks></remarks>
        public string ServerName
        {
            get
            {
                return strServerName;
            }
            set
            {
                strServerName = value;
            }
        }


        /// <summary>
        /// For create database connection.
        /// Modify By: Sandeep
        /// </summary>
        public bool Connect()
        {
            string strConnection = null;
            string strs = null;

            try
            {

                strInstName = "aPayroll";

                //S.SANDEEP [ 11 FEB 2015 ] -- START
                strs = clsSecurity.Decrypt(strPassWord, "ezee");
                //S.SANDEEP [ 11 FEB 2015 ] -- END



                //S.SANDEEP [ 12 NOV 2014 ] -- START
                //strConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = sa;" + "Password=" + strs + "";
                strConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = aruti_sa;" + "Password=" + strs + "";
                //S.SANDEEP [ 12 NOV 2014 ] -- END

                //S.SANDEEP [ 11 FEB 2015 ] -- START
                string strOldConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = sa;" + "Password=" + strs + ""; ;
                //S.SANDEEP [ 11 FEB 2015 ] -- END


                modGlobal.gblnIsReConnect = true;

                if (modGlobal.gsqlTransaction != null)
                {
                    modGlobal.gsqlTransaction.Rollback();
                    modGlobal.gsqlTransaction.Dispose();
                }

                if (modGlobal.gConn != null)
                {
                    System.Data.SqlClient.SqlConnection.ClearAllPools();
                    modGlobal.gConn.Close();
                    modGlobal.gConn.Dispose();
                    modGlobal.gConn = null;
                }

                modGlobal.gblnBindTransaction = false;

                modGlobal.gConn = new System.Data.SqlClient.SqlConnection();
                if (modGlobal.gConn.State != ConnectionState.Closed)
                {
                    modGlobal.gConn.Close();
                }
                modGlobal.gConn.ConnectionString = strConnection;

                //S.SANDEEP [ 11 FEB 2015 ] -- START
                //modGlobal.gConn.Open();
                try
                {
                    modGlobal.gConn.Open();
                }
                catch (Exception)
                {
                    modGlobal.gConn.ConnectionString = strOldConnection;
                    modGlobal.gConn.Open();
                }
                //S.SANDEEP [ 11 FEB 2015 ] -- END


                modGlobal.gblnIsReConnect = false;

                modGlobal.createFormatString();

                return true;

            }
            catch (System.Exception ex)
            {
                string strErrMsg = null;
                strErrMsg = ex.Message + "\r" + "[Connect," + mstrModuleName + "]";
                throw new Exception(strErrMsg);
            }
        }


        public void fillDatabaseList(System.Windows.Forms.ListView lvSource)
        {
            try
            {
                System.Data.SqlClient.SqlCommand cmd = null;
                cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = "Select name from sys.databases WHERE UPPER(name) = 'HRMSCONFIGURATION'";
                cmd.Connection = modGlobal.gConn;
                SqlDataAdapter oDa = null;
                DataTable xDt = new DataTable("List");
                oDa = new SqlDataAdapter();
                oDa.SelectCommand = cmd;
                oDa.Fill(xDt);



                System.Windows.Forms.ListViewItem lstItem = null;

                if (xDt.Rows.Count > 0)
                {
                    lstItem = new System.Windows.Forms.ListViewItem();
                    lstItem.Text = xDt.Rows[0]["name"].ToString();
                    lvSource.Items.Add(lstItem);
                }


            }


            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ArrayList fillDatabaseList()
        {
            try
            {

                System.Data.SqlClient.SqlCommand cmd = null;
                cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = "Select name from sys.databases WHERE UPPER(name) = 'HRMSCONFIGURATION'";
                cmd.Connection = modGlobal.gConn;
                SqlDataAdapter oDa = null;
                DataTable xDt = new DataTable("List");
                oDa = new SqlDataAdapter();
                oDa.SelectCommand = cmd;
                oDa.Fill(xDt);

                ArrayList arrDatabase = new ArrayList();

                int nCount = 0;
                for (nCount = 0; nCount <= xDt.Rows.Count - 1; nCount++)
                {

                    arrDatabase.Add(xDt.Rows[nCount]["name"]);


                }

                return arrDatabase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ArrayList fillTranDatabaseList()
        {
            try
            {
                System.Data.SqlClient.SqlCommand cmd = null;
                cmd = new System.Data.SqlClient.SqlCommand();
                cmd.CommandTimeout = 0;
                cmd.CommandText = "Select name from sys.databases WHERE UPPER(name) LIKE  'TRAN_%'";
                cmd.Connection = modGlobal.gConn;
                SqlDataAdapter oDa = null;
                DataTable xDt = new DataTable("List");
                oDa = new SqlDataAdapter();
                oDa.SelectCommand = cmd;
                oDa.Fill(xDt);

                ArrayList arrDatabase = new ArrayList();

                int nCount = 0;
                for (nCount = 0; nCount <= xDt.Rows.Count - 1; nCount++)
                {
                    arrDatabase.Add(xDt.Rows[nCount]["name"]);
                }

                return arrDatabase;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void change_database(string DatabaseName)
        {
            if (DatabaseName != "")
                strDatabaseName = DatabaseName;

            if (strDatabaseName != "")
                modGlobal.gConn.ChangeDatabase(strDatabaseName);
        }
        //Sohail (26 Apr 2013) - Start
        public static string current_database()
        {
            return modGlobal.gConn.Database.ToString();
        }
        //Sohail (26 Apr 2013) - End


        //S.SANDEEP [ 11 FEB 2015 ] -- START
        private string DataBase_Path()
        {
            System.Data.SqlClient.SqlCommand xSQLCmd = null;
            string strBackUpPath = "";
            string xQry = "";
            try
            {
                if (modGlobal.gConn.State == ConnectionState.Closed | modGlobal.gConn.State == ConnectionState.Broken)
                {
                    modGlobal.gConn.Open();
                }
                xSQLCmd = new System.Data.SqlClient.SqlCommand();
                xSQLCmd.CommandTimeout = 0;
                xSQLCmd.Connection = modGlobal.gConn;

                xQry = "DECLARE @BKPATH AS TABLE (xpath VARCHAR(255)) " +
                       "DECLARE @regInstanceKey VARCHAR(255), @s VARCHAR(255); " +
                       "DECLARE @instance VARCHAR(255) " +
                       "DECLARE @ver VARCHAR(255) " +
                       "DECLARE @key VARCHAR(255) " +
                       "DECLARE @value_name VARCHAR(255) " +
                       "DECLARE @instance_name VARCHAR(255) " +
                       "DECLARE @version INT " +
                       "DECLARE @VAL INT " +
                       "DECLARE @ic INT " +
                       "SET @instance_name = '' " +
                       "SELECT @instance = @@servicename; " +
                       "SELECT @ver = SUBSTRING(CAST(SERVERPROPERTY('productversion') AS VARCHAR(255)), 1, 2); " +
                       "    IF(RIGHT(@ver,1) = '.') SET @version = CAST(LEFT(@ver,1) AS INT) " +
                       "            ELSE SET @version = CAST(@ver AS INT) " +
                       "        IF (@version = 8) BEGIN " +
                       "            IF (@instance <> 'MSSQLSERVER') SET @key = " +
                       "                'SOFTWARE\\Microsoft\\Microsoft SQL Server\\' + @instance + '\\MSSQLServer\\' " +
                       "            ELSE SET @key = 'SOFTWARE\\Microsoft\\MSSQLServer\\MSSQLServer\\' " +
                       "        END " +
                       "        ELSE IF ( @version > 8) BEGIN " +
                       "            EXEC xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE', " +
                       "                    @key = 'SOFTWARE\\Microsoft\\Microsoft SQL Server\\Instance Names\\SQL', " +
                       "                    @vn = @instance, " +
                       "                    @value = @instance_name out " +
                       "                    SET @key = 'SOFTWARE\\Microsoft\\Microsoft SQL Server\\' + @instance_name + '\\MSSQLServer' " +
                       "           END " +
                       "            SET @value_name = 'BackupDirectory' " +
                       "                EXEC xp_instance_regread @rootkey = 'HKEY_LOCAL_MACHINE', " +
                       "                        @k = @key, " +
                       "                        @vn = @value_name, " +
                       "                        @s = @regInstanceKey OUTPUT; " +
                       "    INSERT INTO @BKPATH " +
                       "        VALUES (@regInstanceKey) " +
                       "    IF @regInstanceKey = @instance_name SET @regInstanceKey = NULL " +
                       "SELECT xpath FROM @BKPATH ";

                xSQLCmd.CommandText = xQry;
                SqlDataReader dr = xSQLCmd.ExecuteReader();
                if (dr.HasRows == false)
                {
                    throw new Exception("Invalid database backup path.");
                }
                else
                {
                    while (dr.Read())
                    {
                        strBackUpPath = dr["xpath"].ToString();
                    }
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return strBackUpPath;
        }

        public string Backup(string strFilePath, string strServer)
        {
            System.Data.SqlClient.SqlCommand xSQLCmd = null;
            string strBackUpPath = "";
            string strBackUpFile = "";
            string dBName = null;
            try
            {


                if (!(System.IO.Directory.Exists(strFilePath)))
                {
                    throw new Exception("Invalid database backup path.");
                }


                strBackUpPath = DataBase_Path();

                dBName = modGlobal.gConn.Database.ToString();
                strBackUpFile = string.Format("{0}_{1}", dBName, System.DateTime.Now.ToString("HHmmss_yyyyMMdd"));



                if (strBackUpPath == "")
                {
                    throw new Exception("Database not found!");
                }

                strInstName = "aPayroll";

                //S.SANDEEP [ 11 FEB 2015 ] -- START
                string strs = clsSecurity.Decrypt(strPassWord, "ezee");
                //S.SANDEEP [ 11 FEB 2015 ] -- END

                SqlConnection conn = new SqlConnection("Data Source=" + strServer + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = aruti_sa;" + "Password=" + strs + "");
                try
                {
                    conn.Open();
                }

                catch (Exception)
                {
                    //S.SANDEEP [ 11 FEB 2015 ] -- START
                    string strOldConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = sa;" + "Password=" + strs + ""; ;
                    conn = new SqlConnection(strOldConnection);
                    conn.Open();
                    //S.SANDEEP [ 11 FEB 2015 ] -- END                    
                }
                conn.FireInfoMessageEventOnUserErrors = true;
                conn.InfoMessage += new SqlInfoMessageEventHandler(_InfoMessage);
                xSQLCmd = new System.Data.SqlClient.SqlCommand();
                xSQLCmd.CommandTimeout = 0;
                xSQLCmd.Connection = conn;
                string xString;

                xString = "DBCC SHRINKDATABASE (" + dBName + ",NOTRUNCATE) WITH NO_INFOMSGS";
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();

                xString = String.Format("BACKUP DATABASE {0} TO DISK = N'{1}\\{2}.dat' " +
                                        "WITH FORMAT, COPY_ONLY, INIT, NAME = N'{0} - Full Database " +
                                        "Backup', SKIP, STATS = 1 ", dBName, strBackUpPath, strBackUpFile);


                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();


                /* -- > CLIENT SIDE COPY STOPPED DUE TO LONG TIME & PROCESSING as varbinary(MAX) datatype had limitation of maximum 2 gb table size.
                string pathSource = strBackUpPath + "\\" + dBName + ".dat";

                using (System.IO.FileStream fsSource = new System.IO.FileStream(pathSource,System.IO.FileMode.Open, System.IO.FileAccess.Read))
                {
                    ////long[] backupFromServer = new long[fsSource.Length];

                    System.IO.FileStream fs = new System.IO.FileStream(String.Format("{0}\\{1}",
                                    strFilePath, strBackUpFile + ".dat"), System.IO.FileMode.OpenOrCreate,
                                    System.IO.FileAccess.Write);

                    //for (long i = 0; i < backupFromServer.Length; i++)
                    //{
                    //    fs.WriteByte(backupFromServer[i]);
                    //}

                    for (long i = 0; i < fsSource.Length; i++)
                    {
                        byte y = (byte)fsSource.ReadByte();
                        fs.WriteByte(y);
                    }

                    //fs.Write(backupFromServer, 0, backupFromServer.GetUpperBound(0) + 1);

                    fs.Close();


                    //if (System.IO.File.Exists(strBackUpPath + "\\" + dBName + ".dat") == true)
                    //{
                    //    System.IO.File.Delete(strBackUpPath + "\\" + dBName + ".dat");
                    //}
                }

                xString = String.Format("IF OBJECT_ID('tempdb..##{0}') IS " +
                                        "NOT NULL DROP TABLE ##{0}", "_" + dBName);
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();
                xString = String.Format("CREATE TABLE ##{0} (bck VARBINARY(MAX))",
                                     "_" + dBName);
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();


                xString = String.Format("INSERT INTO ##{0} SELECT bck.* FROM " +
                   "OPENROWSET(BULK '{1}\\{2}.dat',SINGLE_BLOB) bck",
                   "_" + dBName, strBackUpPath, dBName);
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();

                xString = String.Format("SELECT bck FROM ##{0}", "_" + dBName);
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(xString, conn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    DataRow dr = ds.Tables[0].Rows[0];
                    byte[] backupFromServer = new byte[0];
                    backupFromServer = (byte[])dr["bck"];
                    int aSize = new int();

                    aSize = backupFromServer.GetUpperBound(0) + 1;

                    System.IO.FileStream fs = new System.IO.FileStream(String.Format("{0}\\{1}",
                                    strFilePath, strBackUpFile + ".dat"), System.IO.FileMode.OpenOrCreate,
                                    System.IO.FileAccess.Write);

                    fs.Write(backupFromServer, 0, aSize);
                    fs.Close();

                    if (System.IO.File.Exists(strBackUpPath + "\\" + dBName + ".dat") == true)
                    {
                        System.IO.File.Delete(strBackUpPath + "\\" + dBName + ".dat");
                    }

                    xString = String.Format("DROP TABLE ##{0}", "_" + dBName);
                    xSQLCmd.CommandText = xString;
                    xSQLCmd.ExecuteNonQuery();
                }
                */

                conn.Close();

                conn.InfoMessage -= _InfoMessage;
                conn.FireInfoMessageEventOnUserErrors = false;

                ////***** Delete Temp Bakup File
                //System.IO.File.Delete(System.IO.Path.Combine(strBackUpPath, objBackupDeviceItem.Name));
                //***** Move Bakup File to Given Path
                if (System.IO.Directory.Exists(strFilePath) == true && System.IO.File.Exists(System.IO.Path.Combine(strBackUpPath, strBackUpFile + ".dat")) == true)
                {
                    System.IO.File.Copy(System.IO.Path.Combine(strBackUpPath, strBackUpFile + ".dat"),
                                System.IO.Path.Combine(strFilePath, strBackUpFile + ".dat"));

                    //***** Delete Temp Bakup File
                    System.IO.File.Delete(System.IO.Path.Combine(strBackUpPath, strBackUpFile + ".dat"));

                    return System.IO.Path.Combine(strFilePath, strBackUpFile + ".dat");
                }
                else
                {
                    return System.IO.Path.Combine(strBackUpPath, strBackUpFile + ".dat");
                }



                return strFilePath + "\\" + strBackUpFile + ".dat";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Restore(string strFilePath)
        {
            string dBName = null;
            string accessmessage = "Cannot open backup device '" + strFilePath + "'. Operating system error 5(Access is denied).Please put this backup on different drive and try to restore again.";
            System.Data.SqlClient.SqlCommand xSQLCmd = null;
            bool blnAddPassword = false;
            try
            {
                if (!(System.IO.File.Exists(strFilePath)))
                {
                    throw new Exception("Invalid database file path.");
                }
                dBName = modGlobal.gConn.Database.ToString();
                strInstName = "aPayroll";

                //S.SANDEEP [ 11 FEB 2015 ] -- START
                string strs = clsSecurity.Decrypt(strPassWord, "ezee");
                //S.SANDEEP [ 11 FEB 2015 ] -- END

                SqlConnection conn = new SqlConnection("Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = aruti_sa;" + "Password=" + strs + "");
                try
                {
                    conn.Open();
                }
                catch (Exception)
                {
                    //S.SANDEEP [ 11 FEB 2015 ] -- START
                    string strOldConnection = "Data Source=" + strServerName + "\\" + strInstName + ";" + "Persist Security Info=True;" + "User ID = sa;" + "Password=" + strs + ""; ;
                    conn = new SqlConnection(strOldConnection);
                    conn.Open();
                    //S.SANDEEP [ 11 FEB 2015 ] -- END 
                }
                conn.FireInfoMessageEventOnUserErrors = true;
                conn.InfoMessage += new SqlInfoMessageEventHandler(_InfoMessage);
                xSQLCmd = new System.Data.SqlClient.SqlCommand();
                xSQLCmd.CommandTimeout = 0;
                xSQLCmd.Connection = conn;
                string xString;
                object iRetValue = null;

                xString = "SELECT name FROM sys.databases WHERE name = '" + dBName + "'";
                xSQLCmd.CommandText = xString;
                iRetValue = xSQLCmd.ExecuteScalar();

                if (iRetValue == null)
                {
                    throw new Exception("Invalid database name.");
                }

                //xString = "ALTER DATABASE [" + dBName + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;" +
                //          "RESTORE DATABASE [" + dBName + "] FROM  DISK = '" + strFilePath + "' WITH REPLACE,  STATS = 1;" +
                //          "DBCC CHECKDB(" + dBName + ") WITH ALL_ERRORMSGS, NO_INFOMSGS; " +
                //          "ALTER DATABASE [" + dBName + "] SET MULTI_USER; ";

                string xMovePath = "";
                string xDataPath = "";
                xMovePath = DataBase_Path();

                if (System.IO.Directory.Exists(xMovePath) == false)
                {
                    throw new Exception("Invalid database path.");
                }

                /*  SANDEEP
                ISSUE : IF DATABASE COMES WITH PASSWORD FROM OLD COMMON-LIB 
                WORK  : CHECK AND APPLY PASSWORD IF FOUND ---- START */

                try
                {
                    //S.SANDEEP [ 06 JUL 2015 ] -- START
                    if (Give_SQL_USER_Access_Permission("APAYROLL", strFilePath) == false)
                    {
                        Exception ex = new Exception(accessmessage);
                        throw ex;
                    }
                    //S.SANDEEP [ 06 JUL 2015 ] -- END

                xString = "RESTORE HEADERONLY FROM DISK = '" + strFilePath + "' ";
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(xString, conn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                da.Dispose();

                    if (ds.Tables.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    if (Convert.ToString(dr["BackupName"]) == "*** PASSWORD PROTECTED ***")
                    {
                        blnAddPassword = true;
                    }
                }
                    else
                    {
                        Exception ex = new Exception(accessmessage);
                        throw ex;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                /*  SANDEEP
                ISSUE : IF DATABASE COMES WITH PASSWORD FROM OLD COMMON-LIB 
                WORK  : CHECK AND APPLY PASSWORD IF FOUND ---- END */

                xDataPath = xMovePath.Substring(0, xMovePath.LastIndexOf("\\") + 1);

                xDataPath = xDataPath + "DATA\\";

                xString = "ALTER DATABASE [" + dBName + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;";
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();

                //xString = "RESTORE DATABASE [" + dBName + "] FROM  DISK = '" + strFilePath + "' WITH " +
                //          "MOVE '" + dBName + "' TO '" + xMovePath + "\\NEW_" + dBName + ".mdf', " +
                //          "MOVE '" + dBName + "_Log' TO '" + xMovePath + "\\NEW_" + dBName + ".ldf', " +
                //          "REPLACE, STATS = 1;";

                if (blnAddPassword == true)
                {
                    xString = "RESTORE DATABASE [" + dBName + "] FROM  DISK = '" + strFilePath + "' WITH REPLACE, " +
                              "MOVE '" + dBName + "' TO '" + xDataPath + dBName + ".mdf', " +
                              "MOVE '" + dBName + "_log' TO '" + xDataPath + dBName + "_log.ldf', PASSWORD = '" + clsSecurity.Decrypt(strPassWord, "ezee") + "' , STATS = 1;";
                }
                else
                {
                xString = "RESTORE DATABASE [" + dBName + "] FROM  DISK = '" + strFilePath + "' WITH REPLACE, " +
                          "MOVE '" + dBName + "' TO '" + xDataPath + dBName + ".mdf', " +
                              "MOVE '" + dBName + "_log' TO '" + xDataPath + dBName + "_log.ldf', STATS = 1;";
                }
                

                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();

                xString = "DBCC CHECKDB(" + dBName + ") WITH ALL_ERRORMSGS, NO_INFOMSGS; ";
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();

                xString = "ALTER DATABASE [" + dBName + "] SET MULTI_USER; ";
                xSQLCmd.CommandText = xString;
                xSQLCmd.ExecuteNonQuery();



                conn.Close();
                conn.InfoMessage -= _InfoMessage;
                conn.FireInfoMessageEventOnUserErrors = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public event SqlInfoMessageEventHandler Progress;
        private void _InfoMessage(Object sender, SqlInfoMessageEventArgs e)
        {
            if (Progress != null)
            {
                Progress(sender, e);
            }
        }
        //S.SANDEEP [ 11 FEB 2015 ] -- END

        //S.SANDEEP [ 06 JUL 2015 ] -- START
        #region Assign File Privilege

        private bool Give_SQL_USER_Access_Permission(string InstanceName, string filePath)
        {
            bool ispermissiongiven = false;
            bool isInstanceUserFound = false;
            try
            {
                String MachineName = Environment.MachineName;
                DirectoryEntry dirEntry = new DirectoryEntry("WinNT://" + MachineName + ",computer"); //Connect to machine                

                foreach (DirectoryEntry @group in dirEntry.Children)
                {
                    if (string.Compare(@group.SchemaClassName, "group", true) == 0 && @group.Name.IndexOf("mssqluser", StringComparison.OrdinalIgnoreCase) > -1 && (InstanceName == null || InstanceName.Length == 0 || @group.Name.IndexOf(InstanceName, StringComparison.OrdinalIgnoreCase) > -1))
                    {
                        string uname = @group.Path;
                        uname = uname.Substring(uname.IndexOf("//") + 2);
                        uname = uname.Substring(uname.IndexOf("/") + 1);
                        uname = uname.Replace("/", "\\");
                        GiveFilePermission(filePath, uname);
                        ispermissiongiven = true;
                        isInstanceUserFound = true;
                    }
                }
                if (isInstanceUserFound == false)
                {
                    ispermissiongiven = true;
                }
            }
            catch (Exception ex)
            {
                if (ex.Message != null) { return ispermissiongiven; }               
            }
            return ispermissiongiven;
        }

        private void GiveFilePermission(string fullpath, string useraccountname)
        {
            try
            {
                string File = fullpath;
                string UserAccount = useraccountname;
                System.IO.FileInfo FInfo = new System.IO.FileInfo(File);
                FileSecurity FlAcl = new FileSecurity();
                FlAcl = FInfo.GetAccessControl();
                FlAcl.AddAccessRule(new FileSystemAccessRule(UserAccount, FileSystemRights.ReadAndExecute, AccessControlType.Allow));
                FInfo.SetAccessControl(FlAcl);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        //S.SANDEEP [ 06 JUL 2015 ] -- END
    }
}