﻿
Partial Class Controls_NumericTextBox
    Inherits System.Web.UI.UserControl

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Public Enum TextboxType
        Numeric = 1
        Point = 2
    End Enum

#Region " Default Properties "

    Public Property Height() As Unit
        Get
            Return txtNumeric.Height
        End Get
        Set(ByVal value As Unit)
            txtNumeric.Height = value
        End Set
    End Property

    Public Property Width() As Unit
        Get
            Return txtNumeric.Width
        End Get
        Set(ByVal value As Unit)
            txtNumeric.Width = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return txtNumeric.Text
        End Get
        Set(ByVal value As String)
            txtNumeric.Text = value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return txtNumeric.Enabled
        End Get
        Set(ByVal value As Boolean)
            txtNumeric.Enabled = value
        End Set
    End Property

    Public Property Read_Only() As Boolean
        Get
            Return txtNumeric.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            txtNumeric.ReadOnly = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return txtNumeric.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            txtNumeric.AutoPostBack = value
        End Set
    End Property

    Public WriteOnly Property Max() As Integer
        Set(ByVal value As Integer)
            txtNumeric.Attributes.Add("data-max", value)
        End Set
    End Property


    Public WriteOnly Property Min() As Integer
        Set(ByVal value As Integer)
            txtNumeric.Attributes.Add("data-min", value)
        End Set
    End Property

    Public WriteOnly Property Type() As TextboxType
        Set(ByVal value As TextboxType)
            If value = TextboxType.Point Then
                txtNumeric.Attributes.Add("data-rule", "currency")
            End If
        End Set
    End Property

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Public WriteOnly Property Style() As String
        Set(ByVal value As String)
            txtNumeric.Attributes.Add("style", value)
        End Set
    End Property

    Public WriteOnly Property HideUpDown() As Boolean
        Set(ByVal value As Boolean)
            'lnkup.Visible = Not value
            'lnkdown.Visible = Not value
        End Set
    End Property

    Public Property Decimal_() As Decimal
        Get
            Dim decAmt As Decimal = 0
            Decimal.TryParse(txtNumeric.Text, decAmt)
            Return decAmt
        End Get
        Set(ByVal value As Decimal)
            Dim decAmt As Decimal = 0
            Decimal.TryParse(value, decAmt)
            txtNumeric.Text = decAmt.ToString
        End Set
    End Property
    'Sohail (01 Mar 2021) -- End

#End Region

#Region " Default Events "

    Protected Sub txtNumeric_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumeric.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

#End Region
    
End Class
