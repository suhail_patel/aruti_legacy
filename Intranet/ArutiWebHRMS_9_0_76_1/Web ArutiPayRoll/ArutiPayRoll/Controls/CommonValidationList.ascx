﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CommonValidationList.ascx.vb" Inherits="Controls_CommonValidationList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>



<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ModalPopupBG2"
    CancelControlID="btnCValidationCancel" PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel ID="Panel1" runat="server" CssClass="newpopup" Style="display: none; width: 450px;
    z-index: 100002!important;" >
    <div class="panel-primary" style="margin-bottom: 0px;">
        <div class="panel-heading">
            <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 100%">
                                <asp:Panel ID="pnl_gvdetails" runat="server" ScrollBars="Auto" Height="400px" Width="100%">
                                    <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="true" ShowFooter="False"
                                        Width="98%" CellPadding="3" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        
                                    </asp:GridView>
                                </asp:Panel>
                                <%--<asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Width="97%" Height="50px"
                                    Style="margin-bottom: 5px;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None"
                                    ControlToValidate="txtReason" ErrorMessage="Delete Reason can not be blank. "
                                    CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" SetFocusOnError="True"
                                    ValidationGroup="CValidation"></asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="RequiredFieldValidator1"
                                    Width="300px" />--%>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                        <asp:Panel ID="pnlYesNo" runat="server" Visible="false">
                        <asp:Button ID="btnCValidationYes" runat="server" Text="Yes" Width="70px" Style="margin-right: 10px"
                            CssClass="btnDefault" ValidationGroup="CValidation" />
                        <asp:Button ID="btnCValidationNo" runat="server" Text="No" Width="70px" Style="margin-right: 10px"
                            CssClass="btnDefault" />
                        <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                        </asp:Panel>
                        <asp:Panel ID="pnlOkCancel" runat="server" >
                        <asp:Button ID="btnCValidationOk" runat="server" Text="Ok" Width="70px" Style="margin-right: 10px"
                            CssClass="btnDefault" ValidationGroup="CValidation" />
                        <asp:Button ID="btnCValidationCancel" runat="server" Text="Cancel" Width="70px" Style="margin-right: 10px"
                            CssClass="btnDefault" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
