﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AnalysisBy.ascx.vb" Inherits="Controls_AnalysisBy" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    
</style>



<ajaxToolkit:ModalPopupExtender ID="popupAdvanceFilter" runat="server" CancelControlID="HiddenField2"
    PopupControlID="Panel1" TargetControlID="HiddenField1">
</ajaxToolkit:ModalPopupExtender>
<%--<asp:Panel ID="Panel1" runat="server" Style="display: none; padding: 10px; width: 602px;
    border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
    -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
    -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
    BackColor="#5377A9" DefaultButton="btnApply">--%>
<%-- <div>
    </div>
    <hr style="display: block; -webkit-margin-before: 0.5em; -webkit-margin-after: 0.5em;
        -webkit-margin-start: auto; -webkit-margin-end: auto; border-style: inset; border-width: 1px;" />
    <div style="text-align: center; margin-top: 2px; margin-bottom: 5px; height: 350px;
        overflow: auto; background-color: White; overflow: auto">
        <div style="height: 350px; width: 375px; overflow: auto; float: left; border-style: solid;
            border-right-width: 1px; border-bottom-width: 0px; border-left-width: 0px; border-top-width: 0px;">
        </div>
    </div>
    <div style="text-align: right;">
    </div>--%>
<%--</asp:Panel>--%>
<asp:Panel ID="Panel1" runat="server" Style="display: none; width: 602px;" DefaultButton="btnApply">
    <div class="panel-primary">
        <div class="panel-heading">
            <asp:Label ID="lblTitle" runat="server" Text="Analysis By"></asp:Label>
        </div>
        <div class="panel-body">
            <div id="FilterCriteria" class="panel-default">
                <div id="FilterCriteriaBody" class="panel-body-default">
                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 40%">
                                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" Style="float: left; height: 350px;
                                    width: 220px; overflow: auto" AutoPostBack="true" CssClass="mycustomcss">
                                    <ajaxToolkit:TabPanel ID="tbpnlBranch" runat="server" Width="0px" Style="width: 0px;"
                                        HeaderText="Branch" CssClass="tabdata">
                                        <ContentTemplate>
                                            <asp:Panel ID="pnlBranch" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                                <div>
                                                </div>
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlDeptGroup" runat="server" Style="width: 0px;" HeaderText="Department Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel2" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlDept" runat="server" Style="width: 0px;" HeaderText="Department">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel22" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlSectionGroup" runat="server" Style="width: 0px;" HeaderText="Section Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel3" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlSection" runat="server" Style="width: 0px;" HeaderText="Section">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel4" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlUnitGroup" runat="server" Style="width: 0px;" HeaderText="Unit Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel5" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlUnit" runat="server" Style="width: 0px;" HeaderText="Unit">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel6" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlTeam" runat="server" Style="width: 0px;" HeaderText="Team">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel7" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlJobGroup" runat="server" Style="width: 0px;" HeaderText="Job Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel8" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlJob" runat="server" Style="width: 0px;" HeaderText="Job">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel9" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlClassGroup" runat="server" Style="width: 0px;" HeaderText="Class Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel10" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlClass" runat="server" Style="width: 0px;" HeaderText="Class">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel11" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGradeGroup" runat="server" Style="width: 0px;" HeaderText="Grade Group">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel12" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGrade" runat="server" Style="width: 0px;" HeaderText="Grade">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel13" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlGradeLevel" runat="server" Style="width: 0px;" HeaderText="Grade Level">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel14" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                    <ajaxToolkit:TabPanel ID="tbpnlCostCenter" runat="server" Style="width: 0px;" HeaderText="Cost Center">
                                        <ContentTemplate>
                                            <asp:Panel ID="Panel16" runat="server" ScrollBars="Auto" Width="0px" Height="330px">
                                            </asp:Panel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                            </td>
                            <td style="width: 60%">
                                <asp:Panel ID="pnl_gvDetails" runat="server" Height="350px" Width="100%" ScrollBars="Auto">
                                    <asp:GridView ID="gvDetails" runat="server" AutoGenerateColumns="False" Width="98%"
                                        CellPadding="3" DataKeyNames="Id" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Eval("IsCheck") %>' OnCheckedChanged="chkSelect_CheckedChanged"
                                                        AutoPostBack="true" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Id" ReadOnly="true" Visible="false" HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="Name" HeaderText="Report By" FooterText="dgcolhReportBy" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <div class="btn-default">
                        <asp:Label ID="lblMessage" runat="server" Text="" Style="max-width: 475px; display:block;" />
                        <asp:Button ID="btnApply" runat="server" Text="Apply" CssClass="btnDefault" ValidationGroup="Reason" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
