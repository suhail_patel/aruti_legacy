﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MaskedTimeTextBox.ascx.vb"   Inherits="Controls_MaskedTimeTextBox" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="auc" %>

<auc:MaskedEditExtender ID="MaskedEditExtenderHours" runat="server" TargetControlID="txtHours"
    MaskType="Time" UserTimeFormat="TwentyFourHour" AcceptNegative="None" MessageValidatorTip="true"
    AcceptAMPM="false" Mask="99:99">
</auc:MaskedEditExtender>

<asp:TextBox ID="txtHours" runat="server" Style="text-align: right" ></asp:TextBox>

<auc:MaskedEditValidator ID="MaskedEditValidatorHours" runat="server" ControlExtender="MaskedEditExtenderHours"
    ControlToValidate="txtHours" IsValidEmpty="false" MaximumValue="23:59" MinimumValue="00:01">
</auc:MaskedEditValidator>
