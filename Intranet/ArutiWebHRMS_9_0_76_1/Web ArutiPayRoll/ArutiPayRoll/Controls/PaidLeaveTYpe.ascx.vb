﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class Controls_PaidLeaveTYpe
    Inherits System.Web.UI.UserControl
    Dim clsleavetype As New Aruti.Data.clsleavetype_master
    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Sub BindLeaveType()
        Try

            Dim ds As New DataSet
            Dim clsuser As New User
            '  Dim objDataOperation As New eZeeCommonLib.clsDataOperation
            '  ds = clsleavetype.getListForCombo("LeaveType", True, 1)
            ' msql = "select leavetypeunkid as unkid ,leavename from " & Session("mdbname") & "..lvleavetype_master where ispaid='1'"
            ' ds = objDataOperation.WExecQuery(msql, "lvleavetype_master")
            ds = clsleavetype.getListForCombo("LeaveType", True, 1)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlLeave.DataSource = ds.Tables(0)
                ddlLeave.DataValueField = "leavetypeunkid"
                ddlLeave.DataTextField = "name"
                ddlLeave.DataBind()

                'If (Me.Mode = enentrymode.Filter) Then
                '    Dim mItem As New ListItem
                '    mItem.Text = "ALL"
                '    mItem.Value = "-1"
                '    ddlLeave.Items.Insert(0, mItem)
                'End If
                ddlLeave.SelectedIndex = 0
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("PaidLeaveType-->BindLeaveType Method!!!" & ex.Message.ToString)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        Finally
        End Try
    End Sub
    Public Enum enentrymode
        Entry = 1
        Filter = 2
    End Enum

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLeaveType()

        End If
    End Sub
    Private pWidth As Integer
    Public Property width() As Integer
        Get
            Return pWidth
            Setwidth()
        End Get
        Set(ByVal value As Integer)
            pWidth = value
            Setwidth()
        End Set
    End Property

    Private _mode As Integer
    Public Property Mode() As Integer
        Get
            Return _mode

        End Get
        Set(ByVal value As Integer)
            _mode = value

        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return ddlLeave.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            ddlLeave.AutoPostBack = value
        End Set
    End Property

    Public Sub Setwidth()
        ddlLeave.Width = pWidth - 20
    End Sub
    Public Property SelectedValue() As String
        Get
            Return ddlLeave.SelectedValue
        End Get
        Set(ByVal value As String)
            ddlLeave.SelectedValue = value
        End Set
    End Property
    Public Property SelectedText() As String
        Get
            Return ddlLeave.SelectedItem.Text
        End Get
        Set(ByVal value As String)
            ddlLeave.SelectedItem.Text = value
        End Set
    End Property

    Public Property SelectedIndex() As Integer
        Get
            Return ddlLeave.SelectedIndex
        End Get
        Set(ByVal value As Integer)
            ddlLeave.SelectedIndex = value
        End Set
    End Property

    Protected Sub ddlLeave_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlLeave.SelectedIndexChanged
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub
End Class
