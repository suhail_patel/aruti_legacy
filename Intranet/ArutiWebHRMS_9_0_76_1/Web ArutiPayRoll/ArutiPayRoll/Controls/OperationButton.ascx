﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OperationButton.ascx.vb"
    Inherits="Controls_OperationButton" %>
<style type="text/css">
   
</style>

<script>
    /* When the user clicks on the button,
    toggle between hiding and showing the dropdown content */
    function myFunction(sender) {
        var popup = $(sender).prev(".btnoperation").attr("targetcontrolid");
        if (popup != null) {
            var pnlid = '#' + popup;
            var pnl = $(pnlid);
            $(sender).next("#myDropdown").append(pnl.children("a"));
        }
        $(sender).next("#myDropdown").toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {

    if (!event.target.matches('.btnOperationIcon')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if ($(openDropdown).css('display') == 'block') {
                    $(openDropdown).toggle("show");
                }
            }
        }
    }
</script>

<div class="dropdown">
    <asp:Button ID="btnOperation" CssClass="btndefault btnoperation" runat="server" OnClientClick="return false;"
        Text="Operation" />
    <i class="fa fa-caret-down btnOperationIcon" onclick="myFunction(this);" aria-hidden="true">
    </i>
    <div id="myDropdown" class="dropdown-content" style="float: left">
    </div>
</div>
