﻿Imports System.Data
Imports Aruti.Data

Partial Class Controls_AdvanceFilter
    Inherits System.Web.UI.UserControl

    Public Event buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmAdvanceSearch"
    Dim DisplayMessage As New CommonCodes

    Private mstrFilterString As String = String.Empty
    Private mstrEmployeeTableAlias As String = ""

    Private mdicFilterText As New Dictionary(Of String, String)


    Private mdsList As DataSet
    Private mdtFilterInfo As DataTable
    'Sohail (01 Aug 2019) -- Start
    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
    Private dvDetail As DataView
    'Sohail (01 Aug 2019) -- End
    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private mintSelectedAllocationID As Integer = 0
    Private mdicAdvAlloc As New Dictionary(Of Integer, String)
    Private mblnShowSkill As Boolean = False
    'Sohail (14 Nov 2019) -- End
#End Region

#Region " Properties "
    Public Property Title() As String
        Get
            Return lblTitle.Text
        End Get
        Set(ByVal value As String)
            lblTitle.Text = value
        End Set
    End Property

    Public Property Message() As String
        Get
            Return lblMessage.Text
        End Get
        Set(ByVal value As String)
            lblMessage.Text = value
        End Set
    End Property

    Public ReadOnly Property _GetFilterString() As String
        Get
            Return mstrFilterString
        End Get
    End Property

    Public WriteOnly Property _Hr_EmployeeTable_Alias() As String
        Set(ByVal value As String)
            mstrEmployeeTableAlias = value
        End Set
    End Property

    Public ReadOnly Property _GetFilterText() As Dictionary(Of String, String)
        Get
            Return mdicFilterText
        End Get
    End Property

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Public WriteOnly Property _ShowSkill() As Boolean
        Set(ByVal value As Boolean)
            mblnShowSkill = value
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Dim dt As DataTable = Nothing
            If mdtFilterInfo IsNot Nothing Then
                dt = New DataView(mdtFilterInfo, "IsGroup = 0 ", "", DataViewRowState.CurrentRows).ToTable
                If dt.Columns.Contains("GroupId") = True Then
                    dt.Columns("GroupId").ColumnName = "advancefilterallocationid"
                End If
                If dt.Columns.Contains("Id") = True Then
                    dt.Columns("Id").ColumnName = "advancefilterallocationtranunkid"
                End If
                If dt.Columns.Contains("UnkId") = False Then
                    Dim dtCol As New DataColumn("UnkId", System.Type.GetType("System.Int32"))
                    dtCol.AllowDBNull = False
                    dtCol.DefaultValue = -1
                    dt.Columns.Add(dtCol)
                End If
            End If
            Return dt
        End Get
        Set(ByVal value As DataTable)
            mdtFilterInfo = value
            If mdtFilterInfo IsNot Nothing Then
                If mdtFilterInfo.Columns.Contains("advancefilterallocationid") = True Then
                    mdtFilterInfo.Columns("advancefilterallocationid").ColumnName = "GroupId"
                End If
                If mdtFilterInfo.Columns.Contains("advancefilterallocationtranunkid") = True Then
                    mdtFilterInfo.Columns("advancefilterallocationtranunkid").ColumnName = "Id"
                End If
            End If
        End Set
    End Property
    'Sohail (14 Nov 2019) -- End

#End Region

#Region " Methods & Functions "
    Private Sub CreateTable()
        Try
            mdsList = New DataSet()
            mdsList.Tables.Add("List")
            mdsList.Tables(0).Columns.Add("IsCheck", Type.GetType("System.Boolean")).DefaultValue = False
            mdsList.Tables(0).Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
            mdsList.Tables(0).Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""

            Call TabContainer1_ActiveTabChanged(TabContainer1, New System.EventArgs)
            popupAdvanceFilter.Hide()

            If mdsList.Tables(0).Rows.Count <= 0 Then
                Dim r As DataRow = mdsList.Tables(0).NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                mdsList.Tables(0).Rows.Add(r)
            End If

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            Dim dtTable As DataTable = New DataView(mdsList.Tables(0), "", "IsCheck DESC, Name", DataViewRowState.CurrentRows).ToTable
            mdsList.Tables.Clear()
            mdsList.Tables.Add(dtTable)
            'Sohail (01 Aug 2019) -- End
            gvDetails.DataSource = mdsList.Tables(0)
            gvDetails.DataBind()

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateTable :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Private Sub CreateFilterInfoTable()
        Try
            If mdtFilterInfo Is Nothing Then 'Sohail (14 Nov 2019)
            mdtFilterInfo = New DataTable
            mdtFilterInfo.Columns.Add("Id", Type.GetType("System.Int32")).DefaultValue = 0
            mdtFilterInfo.Columns.Add("Name", Type.GetType("System.String")).DefaultValue = ""
            mdtFilterInfo.Columns.Add("GroupName", Type.GetType("System.String")).DefaultValue = ""
            mdtFilterInfo.Columns.Add("IsGroup", Type.GetType("System.Boolean")).DefaultValue = False
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mdtFilterInfo.Columns.Add("GroupId", Type.GetType("System.Int32")).DefaultValue = 0
                'Sohail (14 Nov 2019) -- End
            End If 'Sohail (14 Nov 2019)

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'If mdtFilterInfo.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtFilterInfo.NewRow

            '    r.Item(0) = -111
            '    r.Item(1) = "None" ' To Hide the row and display only Row Header
            '    r.Item(2) = ""
            '    r.Item(3) = True

            '    mdtFilterInfo.Rows.Add(r)
            'End If

            'gvFilterInfo.DataSource = mdtFilterInfo
            'gvFilterInfo.DataBind()
            Call FillFilterInfo()
            'Sohail (14 Nov 2019) -- End
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("CreateFilterInfoTable :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # : Bind Allocation to training course.
    Private Sub FillFilterInfo()
        Try

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If mdtFilterInfo Is Nothing Then Exit Sub

            'If mdtFilterInfo.Rows.Count <= 0 Then
            If mdtFilterInfo IsNot Nothing AndAlso mdtFilterInfo.Rows.Count <= 0 Then
                'Pinkal (20-Aug-2020) -- End
                Dim r As DataRow = mdtFilterInfo.NewRow

                r.Item(0) = -111
                r.Item(1) = "None" ' To Hide the row and display only Row Header
                r.Item(2) = ""
                r.Item(3) = True

                mdtFilterInfo.Rows.Add(r)
            End If

            Dim dt As DataTable = New DataView(mdtFilterInfo, " GroupId > 0 ", "", DataViewRowState.CurrentRows).ToTable(True, "GroupName", "GroupId")
            Dim dtCol As New DataColumn("IsGroup", System.Type.GetType("System.Boolean"))
            dtCol.DefaultValue = True
            dtCol.AllowDBNull = False
            dt.Columns.Add(dtCol)
            dt.Merge(mdtFilterInfo, False)

            dt = New DataView(dt, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            gvFilterInfo.DataSource = dt
            gvFilterInfo.DataBind()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("FillFilterInfo :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
    'Sohail (14 Nov 2019) -- End

    Public Sub Show()
        Try
        Call Reset()
        popupAdvanceFilter.Show()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Show :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Public Sub Hide()
        Try
        popupAdvanceFilter.Hide()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Hide():- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Private Sub Reset()
        Try
            Call CreateFilterInfoTable()
            Call CreateTable()
            mstrFilterString = ""
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Reset :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " Page's Events "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
        Me.ViewState.Add("mstrEmployeeTableAlias", mstrEmployeeTableAlias)
        Me.ViewState.Add("mdsList", mdsList)
        'Sohail (14 Nov 2019) -- Start
        'NMB UAT Enhancement # : Bind Allocation to training course.
        Me.ViewState.Add("mdtFilterInfo", mdtFilterInfo)
        Me.ViewState.Add("mintSelectedAllocationID", mintSelectedAllocationID)
        Me.ViewState.Add("mdicAdvAlloc", mdicAdvAlloc)
        Me.ViewState.Add("mblnShowSkill", mblnShowSkill)
        'Sohail (14 Nov 2019) -- End
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If Me.IsPostBack = False Then
                If mstrEmployeeTableAlias.Trim.Length > 0 Then mstrEmployeeTableAlias = mstrEmployeeTableAlias & "."
                Call CreateTable()
                Call CreateFilterInfoTable()
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                tbpnlSkill.Visible = mblnShowSkill
                Dim objMaster As New clsMasterData
                Dim ds As DataSet = objMaster.GetAdvanceFilterAllocation("List", True)
                mdicAdvAlloc = (From p In ds.Tables(0) Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
                'Sohail (14 Nov 2019) -- End
                Call SetLanguage()
            Else
                mstrEmployeeTableAlias = ViewState("mstrEmployeeTableAlias")
                mdsList = ViewState("mdsList")
                mdtFilterInfo = ViewState("mdtFilterInfo")
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = CInt(ViewState("mintSelectedAllocationID"))
                mdicAdvAlloc = ViewState("mdicAdvAlloc")
                mblnShowSkill = CBool(ViewState("mblnShowSkill"))
                'Sohail (14 Nov 2019) -- End
            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("Page_Load :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

#End Region
 
#Region "CheckBox Event"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try
            Dim dtTab As DataSet = CType(Me.ViewState("mdsList"), DataSet)

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If dtTab Is Nothing Then Exit Sub
            If mdtFilterInfo Is Nothing Then Exit Sub
            'Pinkal (20-Aug-2020) -- End


            For i As Integer = 0 To dtTab.Tables(0).Rows.Count - 1
                If dtTab.Tables(0).Rows(i)("Name").ToString.Trim <> "None" AndAlso CBool(dtTab.Tables(0).Rows(i)("IsCheck")) <> cb.Checked Then
                    Dim gvRow As GridViewRow = gvDetails.Rows(i)

                    'Sohail (01 Aug 2019) -- Start
                    'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
                    If CType(gvRow.FindControl("hdnfld"), HiddenField).Value = "hidden" Then Continue For
                    'Sohail (01 Aug 2019) -- End

                    CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
                    dtTab.Tables(0).Rows(i)("IsCheck") = cb.Checked

                    If cb.Checked = True Then
                        Dim dRow As DataRow = Nothing

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ").Length <= 0 Then
                        '    dRow = mdtFilterInfo.NewRow

                        '    dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                        '    dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                        '    dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                        '    dRow.Item("IsGroup") = True

                        '    mdtFilterInfo.Rows.Add(dRow)
                        'End If
                        'Sohail (14 Nov 2019) -- End
                        dRow = mdtFilterInfo.NewRow

                        dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                        dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                        dRow.Item("GroupId") = mintSelectedAllocationID
                        If mdicAdvAlloc.ContainsKey(mintSelectedAllocationID) = True Then
                            dRow.Item("GroupName") = mdicAdvAlloc.Item(mintSelectedAllocationID)
                        Else
                        dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                        End If
                        'Sohail (14 Nov 2019) -- End
                        dRow.Item("IsGroup") = False

                        mdtFilterInfo.Rows.Add(dRow)
                    Else
                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ")
                        Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ")
                        'Sohail (14 Nov 2019) -- End
                        If dRow.Length > 0 Then
                            mdtFilterInfo.Rows.Remove(dRow(0))
                        End If

                        'Sohail (14 Nov 2019) -- Start
                        'NMB UAT Enhancement # : Bind Allocation to training course.
                        'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ").Length <= 0 Then
                        '   dRow = mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ")
                        If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ").Length <= 0 Then
                            dRow = mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ")
                            'Sohail (14 Nov 2019) -- End
                            If dRow.Length > 0 Then
                                mdtFilterInfo.Rows.Remove(dRow(0))
                            End If
                        End If
                    End If
                    mdtFilterInfo.AcceptChanges()
                End If
            Next
            dtTab.AcceptChanges()
            Me.ViewState("mdsList") = dtTab

            mdtFilterInfo = New DataView(mdtFilterInfo, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            txtSearch.Text = ""
            'Sohail (01 Aug 2019) -- End

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("chkSelectAll_CheckedChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        Finally

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'If mdtFilterInfo.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtFilterInfo.NewRow

            '    r.Item(0) = -111
            '    r.Item(1) = "None" ' To Hide the row and display only Row Header
            '    r.Item(2) = ""
            '    r.Item(3) = True

            '    mdtFilterInfo.Rows.Add(r)
            'End If

            'gvFilterInfo.DataSource = mdtFilterInfo
            'gvFilterInfo.DataBind()
            Call FillFilterInfo()
            'Sohail (14 Nov 2019) -- End

            popupAdvanceFilter.Show()
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataSet = CType(Me.ViewState("mdsList"), DataSet)

            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            If dtTab Is Nothing Then Exit Sub
            If mdtFilterInfo Is Nothing Then Exit Sub
            'Pinkal (20-Aug-2020) -- End

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = cb.Checked
            dtTab.Tables(0).Rows(gvRow.RowIndex)("IsCheck") = cb.Checked
            dtTab.AcceptChanges()
            Me.ViewState("mdsList") = dtTab

            Dim drcheckedRow() As DataRow = dtTab.Tables(0).Select("Ischeck = True")

            If drcheckedRow.Length = dtTab.Tables(0).Rows.Count Then
                CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(gvDetails.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

            If cb.Checked = True Then
                Dim dRow As DataRow = Nothing

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ").Length <= 0 Then
                '    dRow = mdtFilterInfo.NewRow

                '    dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                '    dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                '    dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                '    dRow.Item("IsGroup") = True

                '    mdtFilterInfo.Rows.Add(dRow)
                'End If
                'Sohail (14 Nov 2019) -- End
                dRow = mdtFilterInfo.NewRow

                dRow.Item("Id") = CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id"))
                dRow.Item("Name") = dtTab.Tables(0).Rows(gvRow.RowIndex)("Name").ToString
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                dRow.Item("GroupId") = mintSelectedAllocationID
                If mdicAdvAlloc.ContainsKey(mintSelectedAllocationID) = True Then
                    dRow.Item("GroupName") = mdicAdvAlloc.Item(mintSelectedAllocationID)
                Else
                dRow.Item("GroupName") = TabContainer1.ActiveTab.HeaderText
                End If
                'Sohail (14 Nov 2019) -- End
                dRow.Item("IsGroup") = False

                mdtFilterInfo.Rows.Add(dRow)
            Else
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ")
                Dim dRow() As DataRow = mdtFilterInfo.Select("Id = " & CInt(dtTab.Tables(0).Rows(gvRow.RowIndex)("Id")) & " AND GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ")
                'Sohail (14 Nov 2019) -- End
                If dRow.Length > 0 Then
                    mdtFilterInfo.Rows.Remove(dRow(0))
                End If

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'If mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 0 ").Length <= 0 Then
                '   dRow = mdtFilterInfo.Select("GroupName = '" & TabContainer1.ActiveTab.HeaderText & "' AND IsGroup = 1 ")
                If mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 0 ").Length <= 0 Then
                    dRow = mdtFilterInfo.Select("GroupId = " & mintSelectedAllocationID & " AND IsGroup = 1 ")
                    'Sohail (14 Nov 2019) -- End
                    If dRow.Length > 0 Then
                        mdtFilterInfo.Rows.Remove(dRow(0))
                    End If
                End If
            End If
            mdtFilterInfo.AcceptChanges()

            mdtFilterInfo = New DataView(mdtFilterInfo, "", "GroupName, IsGroup DESC, Name", DataViewRowState.CurrentRows).ToTable

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            txtSearch.Text = ""
            'Sohail (01 Aug 2019) -- End

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("chkSelect_CheckedChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        Finally

            'Sohail (14 Nov 2019) -- Start
            'NMB UAT Enhancement # : Bind Allocation to training course.
            'If mdtFilterInfo.Rows.Count <= 0 Then
            '    Dim r As DataRow = mdtFilterInfo.NewRow

            '    r.Item(0) = -111
            '    r.Item(1) = "None" ' To Hide the row and display only Row Header
            '    r.Item(2) = ""
            '    r.Item(3) = True

            '    mdtFilterInfo.Rows.Add(r)
            'End If

            'gvFilterInfo.DataSource = mdtFilterInfo
            'gvFilterInfo.DataBind()
            Call FillFilterInfo()
            'Sohail (14 Nov 2019) -- End

            popupAdvanceFilter.Show()
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If mstrEmployeeTableAlias.Trim.Length > 0 AndAlso mstrEmployeeTableAlias.EndsWith(".") = False Then
                If mstrEmployeeTableAlias.Trim.Length > 0 Then mstrEmployeeTableAlias = mstrEmployeeTableAlias & "."
            End If

            Dim dRow() As DataRow = mdtFilterInfo.Select("IsGroup = 0")
            If dRow.Length > 0 Then
                Dim mstr As String = ""
                For Each drRow As DataRow In dRow

                    'S.SANDEEP [18 DEC 2015] -- START
                    'Select Case drRow.Item("GroupName").ToString
                    '    Case tbpnlBranch.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "stationunkid in ("
                    '        ElseIf mstr.Trim.Contains("stationunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "stationunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlDept.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "departmentunkid in ("
                    '        ElseIf mstr.Trim.Contains("departmentunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "departmentunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlDeptGroup.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "deptgroupunkid in ("
                    '        ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= "  AND " & mstrEmployeeTableAlias & "deptgroupunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","


                    '    Case tbpnlSection.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "sectionunkid in ("
                    '        ElseIf mstr.Trim.Contains("sectionunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "sectionunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlUnit.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "unitunkid in ("
                    '        ElseIf mstr.Trim.Contains("unitunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "unitunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlJob.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "jobunkid in ("
                    '        ElseIf mstr.Trim.Contains("jobunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "jobunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlSectionGroup.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "sectiongroupunkid in ("
                    '        ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "sectiongroupunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlUnitGroup.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "unitgroupunkid in ("
                    '        ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "unitgroupunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlTeam.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "teamunkid in ("
                    '        ElseIf mstr.Trim.Contains("teamunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "teamunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlJobGroup.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "jobgroupunkid in ("
                    '        ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "jobgroupunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlClassGroup.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "classgroupunkid in ("
                    '        ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "classgroupunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlClass.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "classunkid in ("
                    '        ElseIf mstr.Trim.Contains("classunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "classunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlGradeGroup.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "gradegroupunkid in ("
                    '        ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "gradegroupunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlGrade.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "gradeunkid in ("
                    '        ElseIf mstr.Trim.Contains("gradeunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "gradeunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlGradeLevel.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "gradelevelunkid in ("
                    '        ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "gradelevelunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlCostCenterGroup.HeaderText
                    '        Dim StrIds As String = clscostcenter_master.GetCSV_CC(drRow.Item("Id").ToString)
                    '        If StrIds.Trim <> "" Then
                    '            If mstr.Trim.Length = 0 Then
                    '                mstr = " " & mstrEmployeeTableAlias & "costcenterunkid in ("
                    '            ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                    '                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '                mstr &= " AND " & mstrEmployeeTableAlias & "costcenterunkid in ("
                    '            End If
                    '            mstr &= StrIds & ","
                    '        End If

                    '    Case tbpnlCostCenter.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "costcenterunkid in ("
                    '        ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "costcenterunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlEmployementType.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                    '        ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlReligion.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
                    '        ElseIf mstr.Trim.Contains("religionunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlGender.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "gender in ("
                    '        ElseIf mstr.Trim.Contains("gender") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlNationality.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
                    '        ElseIf mstr.Trim.Contains("nationality") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '        'Sohail (02 Jul 2014) -- Start
                    '        'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                    '    Case tbpnlPayType.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
                    '        ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","

                    '    Case tbpnlPayPoint.HeaderText
                    '        If mstr.Trim.Length = 0 Then
                    '            mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
                    '        ElseIf mstr.Trim.Contains("paypointunkid") = False Then
                    '            mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                    '            mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
                    '        End If
                    '        mstr &= drRow.Item("Id") & ","
                    '        'Sohail (02 Jul 2014) -- End

                    'End Select

                    'Sohail (14 Nov 2019) -- Start
                    'NMB UAT Enhancement # : Bind Allocation to training course.
                    'Select Case drRow.Item("GroupName").ToString
                    '    Case tbpnlBranch.HeaderText
                    Select Case CInt(drRow.Item("GroupId"))
                        Case enAdvanceFilterAllocation.BRANCH
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlBranch.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlBranch.HeaderText, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.stationunkid in ("
                            ElseIf mstr.Trim.Contains("stationunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.stationunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlBranch.HeaderText) = mdicFilterText(tbpnlBranch.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlDept.HeaderText
                        Case enAdvanceFilterAllocation.DEPARTMENT
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlDept.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlDept.HeaderText, "")
                            End If


                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.departmentunkid in ("
                            ElseIf mstr.Trim.Contains("departmentunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.departmentunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlDept.HeaderText) = mdicFilterText(tbpnlDept.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlDeptGroup.HeaderText
                        Case enAdvanceFilterAllocation.DEPARTMENT_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlDeptGroup.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlDeptGroup.HeaderText, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.deptgroupunkid in ("
                            ElseIf mstr.Trim.Contains("deptgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= "  AND ADF.deptgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlDeptGroup.HeaderText) = mdicFilterText(tbpnlDeptGroup.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlSection.HeaderText
                        Case enAdvanceFilterAllocation.SECTION
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlSection.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlSection.HeaderText, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.sectionunkid in ("
                            ElseIf mstr.Trim.Contains("sectionunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.sectionunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlSection.HeaderText) = mdicFilterText(tbpnlSection.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlUnit.HeaderText
                        Case enAdvanceFilterAllocation.UNIT
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlUnit.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlUnit.HeaderText, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.unitunkid in ("
                            ElseIf mstr.Trim.Contains("unitunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.unitunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlUnit.HeaderText) = mdicFilterText(tbpnlUnit.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlJob.HeaderText
                        Case enAdvanceFilterAllocation.JOBS
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlJob.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlJob.HeaderText, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.jobunkid in ("
                            ElseIf mstr.Trim.Contains("jobunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.jobunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlJob.HeaderText) = mdicFilterText(tbpnlJob.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlSectionGroup.HeaderText
                        Case enAdvanceFilterAllocation.SECTION_GROUP
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlSectionGroup.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlSectionGroup.HeaderText, "")
                            End If

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.sectiongroupunkid in ("
                            ElseIf mstr.Trim.Contains("sectiongroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.sectiongroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlSectionGroup.HeaderText) = mdicFilterText(tbpnlSectionGroup.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlUnitGroup.HeaderText
                        Case enAdvanceFilterAllocation.UNIT_GROUP
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlUnitGroup.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlUnitGroup.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.unitgroupunkid in ("
                                'mdicFilterText.Add(tbpnlUnitGroup.HeaderText, "")
                            ElseIf mstr.Trim.Contains("unitgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.unitgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlUnitGroup.HeaderText) = mdicFilterText(tbpnlUnitGroup.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlTeam.HeaderText
                        Case enAdvanceFilterAllocation.TEAM
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlTeam.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlTeam.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.teamunkid in ("
                                'mdicFilterText.Add(tbpnlTeam.HeaderText, "")
                            ElseIf mstr.Trim.Contains("teamunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                                mstr &= " AND ADF.teamunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlTeam.HeaderText) = mdicFilterText(tbpnlTeam.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlJobGroup.HeaderText
                        Case enAdvanceFilterAllocation.JOB_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlJobGroup.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlJobGroup.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.jobgroupunkid in ("
                                'mdicFilterText.Add(tbpnlJobGroup.HeaderText, "")
                            ElseIf mstr.Trim.Contains("jobgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.jobgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlJobGroup.HeaderText) = mdicFilterText(tbpnlJobGroup.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlClassGroup.HeaderText
                        Case enAdvanceFilterAllocation.CLASS_GROUP
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlClassGroup.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlClassGroup.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.classgroupunkid in ("
                                'mdicFilterText.Add(tbpnlClassGroup.HeaderText, "")
                            ElseIf mstr.Trim.Contains("classgroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.classgroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlClassGroup.HeaderText) = mdicFilterText(tbpnlClassGroup.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlClass.HeaderText
                        Case enAdvanceFilterAllocation.CLASSES
                            'Sohail (14 Nov 2019) -- End
                            If mdicFilterText.ContainsKey(tbpnlClass.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlClass.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.classunkid in ("
                                'mdicFilterText.Add(tbpnlClass.HeaderText, "")
                            ElseIf mstr.Trim.Contains("classunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.classunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlClass.HeaderText) = mdicFilterText(tbpnlClass.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGradeGroup.HeaderText
                        Case enAdvanceFilterAllocation.GRADE_GROUP
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlGradeGroup.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlGradeGroup.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradegroupunkid in ("
                                'mdicFilterText.Add(tbpnlGradeGroup.HeaderText, "")
                            ElseIf mstr.Trim.Contains("gradegroupunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradegroupunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlGradeGroup.HeaderText) = mdicFilterText(tbpnlGradeGroup.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGrade.HeaderText
                        Case enAdvanceFilterAllocation.GRADE
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlGrade.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlGrade.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradeunkid in ("
                                'mdicFilterText.Add(tbpnlGrade.HeaderText, "")
                            ElseIf mstr.Trim.Contains("gradeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradeunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlGrade.HeaderText) = mdicFilterText(tbpnlGrade.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGradeLevel.HeaderText
                        Case enAdvanceFilterAllocation.GRADE_LEVEL
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlGradeLevel.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlGradeLevel.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.gradelevelunkid in ("
                                'mdicFilterText.Add(tbpnlGradeLevel.HeaderText, "")
                            ElseIf mstr.Trim.Contains("gradelevelunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.gradelevelunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlGradeLevel.HeaderText) = mdicFilterText(tbpnlGradeLevel.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlCostCenterGroup.HeaderText
                        Case enAdvanceFilterAllocation.COST_CENTER_GROUP
                            'Sohail (14 Nov 2019) -- End

                            Dim StrIds As String = clscostcenter_master.GetCSV_CC(drRow.Item("Id").ToString)
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.costcenterunkid in ("
                                ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.costcenterunkid in ("
                                End If
                                mstr &= StrIds & ","
                            End If

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlCostCenter.HeaderText
                        Case enAdvanceFilterAllocation.COST_CENTER
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlCostCenter.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlCostCenter.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " ADF.costcenterunkid in ("
                                ' mdicFilterText.Add(tbpnlCostCenter.HeaderText, "")
                            ElseIf mstr.Trim.Contains("costcenterunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND ADF.costcenterunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlCostCenter.HeaderText) = mdicFilterText(tbpnlCostCenter.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlEmployementType.HeaderText
                        Case enAdvanceFilterAllocation.EMPLOYEMENT_TYPE
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlEmployementType.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlEmployementType.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                                'mdicFilterText.Add(tbpnlEmployementType.HeaderText, "")
                            ElseIf mstr.Trim.Contains("employmenttypeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "employmenttypeunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlEmployementType.HeaderText) = mdicFilterText(tbpnlEmployementType.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlReligion.HeaderText
                        Case enAdvanceFilterAllocation.RELIGION
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlReligion.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlReligion.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "religionunkid in ("
                                mdicFilterText.Add(tbpnlReligion.HeaderText, "")
                            ElseIf mstr.Trim.Contains("religionunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "religionunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlReligion.HeaderText) = mdicFilterText(tbpnlReligion.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlGender.HeaderText
                        Case enAdvanceFilterAllocation.GENDER
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlGender.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlGender.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "gender in ("
                                ' mdicFilterText.Add(tbpnlGender.HeaderText, "")
                            ElseIf mstr.Trim.Contains("gender") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "gender in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlGender.HeaderText) = mdicFilterText(tbpnlGender.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlNationality.HeaderText
                        Case enAdvanceFilterAllocation.NATIONALITY
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlNationality.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlNationality.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "nationalityunkid in ("
                                'mdicFilterText.Add(tbpnlNationality.HeaderText, "")
                            ElseIf mstr.Trim.Contains("nationality") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "nationalityunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlNationality.HeaderText) = mdicFilterText(tbpnlNationality.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (02 Jul 2014) -- Start
                            'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlPayType.HeaderText
                        Case enAdvanceFilterAllocation.PAY_TYPE
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlPayType.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlPayType.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "paytypeunkid in ("
                                'mdicFilterText.Add(tbpnlPayType.HeaderText, "")
                            ElseIf mstr.Trim.Contains("paytypeunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "paytypeunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlPayType.HeaderText) = mdicFilterText(tbpnlPayType.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlPayPoint.HeaderText
                        Case enAdvanceFilterAllocation.PAY_POINT
                            'Sohail (14 Nov 2019) -- End

                            If mdicFilterText.ContainsKey(tbpnlPayPoint.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlPayPoint.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "paypointunkid in ("
                                'mdicFilterText.Add(tbpnlPayPoint.HeaderText, "")
                            ElseIf mstr.Trim.Contains("paypointunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "paypointunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlPayPoint.HeaderText) = mdicFilterText(tbpnlPayPoint.HeaderText).ToString() & drRow.Item("Name").ToString() & ","
                            'Sohail (02 Jul 2014) -- End

                            'S.SANDEEP |04-JUN-2019| -- START
                            'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlMaritalStatus.HeaderText
                        Case enAdvanceFilterAllocation.MARITAL_STATUS
                            'Sohail (14 Nov 2019) -- End


                            If mdicFilterText.ContainsKey(tbpnlMaritalStatus.HeaderText) = False Then
                                mdicFilterText.Add(tbpnlMaritalStatus.HeaderText, "")
                            End If 'S.SANDEEP |03-OCT-2019| -- START {Not Kept By Developer} -- END

                            If mstr.Trim.Length = 0 Then
                                mstr = " " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
                                'mdicFilterText.Add(tbpnlMaritalStatus.HeaderText, "")
                            ElseIf mstr.Trim.Contains("maritalstatusunkid") = False Then
                                mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                mstr &= " AND " & mstrEmployeeTableAlias & "maritalstatusunkid in ("
                            End If
                            mstr &= drRow.Item("Id") & ","
                            mdicFilterText(tbpnlMaritalStatus.HeaderText) = mdicFilterText(tbpnlMaritalStatus.HeaderText).ToString() & drRow.Item("Name").ToString() & ","

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                            'Case tbpnlRelationFromDpndt.HeaderText
                        Case enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS
                            'Sohail (14 Nov 2019) -- End
                            Dim StrIds As String = clsDependants_Beneficiary_tran.GetCSV_EmpIds(CInt(drRow.Item("Id").ToString))
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= StrIds & ","
                            Else
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= "0,"
                            End If
                            'S.SANDEEP |04-JUN-2019| -- END

                            'Sohail (14 Nov 2019) -- Start
                            'NMB UAT Enhancement # : Bind Allocation to training course.
                        Case enAdvanceFilterAllocation.SKILL
                            Dim StrIds As String = clsEmployee_Skill_Tran.GetCSV_EmpIds(CInt(drRow.Item("Id").ToString))
                            If StrIds.Trim <> "" Then
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= StrIds & ","
                            Else
                                If mstr.Trim.Length = 0 Then
                                    mstr = " ADF.employeeunkid in ("
                                ElseIf mstr.Trim.Contains("employeeunkid") = False Then
                                    mstr = mstr.Substring(0, mstr.Length - 1) & ")"
                                    mstr &= " AND ADF.employeeunkid in ("
                                End If
                                mstr &= "0,"
                            End If
                            'Sohail (14 Nov 2019) -- End

                    End Select
                    'S.SANDEEP [18 DEC 2015] -- END

                    
                Next
                If mstr.Trim <> "" Then
                    mstr = mstr.Substring(0, mstr.Length - 1) & ") "
                Else
                    mstr = " 1 = 1 "
                End If
                mstrFilterString = mstr
            End If

            RaiseEvent buttonApply_Click(sender, e)
            popupAdvanceFilter.Dispose()

        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnApply_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            mstrFilterString = ""
            mstrEmployeeTableAlias = ""

            RaiseEvent buttonClose_Click(sender, e)
            popupAdvanceFilter.Dispose()
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("btnClose_Click :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " GridView's Events "

    Protected Sub gvDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDetails.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(DataBinder.Eval(e.Row.DataItem, "Id")) = -111 AndAlso DataBinder.Eval(e.Row.DataItem, "Name").ToString = "None" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvDetails_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub

    Protected Sub gvFilterInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFilterInfo.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CInt(DataBinder.Eval(e.Row.DataItem, "Id")) = -111 AndAlso DataBinder.Eval(e.Row.DataItem, "Name").ToString = "None" AndAlso DataBinder.Eval(e.Row.DataItem, "GroupName").ToString = "" Then
                    e.Row.Visible = False
                Else
                    If CBool(DataBinder.Eval(e.Row.DataItem, "IsGroup")) = True Then
                        e.Row.Cells(1).Text = DataBinder.Eval(e.Row.DataItem, "GroupName").ToString
                        e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        e.Row.Cells(1).CssClass = "GroupHeaderStyleBorderRight"
                    End If
                End If
            End If
        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("gvFilterInfo_RowDataBound :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
#End Region

#Region " Tab Control's Events "

    Protected Sub TabContainer1_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabContainer1.ActiveTabChanged
        Dim intColType As Integer = 0
        Try
            If TabContainer1.ActiveTab Is tbpnlBranch Then
                Dim objBranch As New clsStation
                mdsList = objBranch.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.BRANCH
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlDeptGroup Then
                Dim objDeptGrp As New clsDepartmentGroup
                mdsList = objDeptGrp.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlDept Then
                Dim objDept As New clsDepartment
                mdsList = objDept.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.DEPARTMENT
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlSectionGroup Then
                Dim objSG As New clsSectionGroup
                mdsList = objSG.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlSection Then
                Dim objSection As New clsSections
                mdsList = objSection.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.SECTION
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlUnitGroup Then
                Dim objUG As New clsUnitGroup
                mdsList = objUG.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlUnit Then
                Dim objUnit As New clsUnits
                mdsList = objUnit.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.UNIT
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlTeam Then
                Dim objTeam As New clsTeams
                mdsList = objTeam.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.TEAM
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlJobGroup Then
                Dim objjobGRP As New clsJobGroup
                mdsList = objjobGRP.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.JOB_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlJob Then
                Dim objJobs As New clsJobs
                mdsList = objJobs.GetList("List", True)
                intColType = 1
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.JOBS
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlClassGroup Then
                Dim objClassGrp As New clsClassGroup
                mdsList = objClassGrp.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.CLASS_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlClass Then
                Dim objClass As New clsClass
                mdsList = objClass.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.CLASSES
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlGradeGroup Then
                Dim objGradeGrp As New clsGradeGroup
                mdsList = objGradeGrp.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlGrade Then
                Dim objGrade As New clsGrade
                mdsList = objGrade.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlGradeLevel Then
                Dim objGradeLvl As New clsGradeLevel
                mdsList = objGradeLvl.GetList("List", True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.GRADE_LEVEL
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlCostCenterGroup Then
                Dim objCCG As New clspayrollgroup_master
                mdsList = objCCG.getListForCombo(enPayrollGroupType.CostCenter, "List")
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER_GROUP
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlCostCenter Then
                Dim objConstCenter As New clscostcenter_master
                mdsList = objConstCenter.GetList("List", True)
                intColType = 2
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.COST_CENTER
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlEmployementType Then
                Dim objEmpType As New clsCommon_Master
                mdsList = objEmpType.GetList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, "List", -1, True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.EMPLOYEMENT_TYPE
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlReligion Then
                Dim objReligion As New clsCommon_Master
                mdsList = objReligion.GetList(clsCommon_Master.enCommonMaster.RELIGION, "List", -1, True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.RELIGION
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlGender Then
                Dim objGender As New clsMasterData
                mdsList = objGender.getGenderList("Gender")
                intColType = 3
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.GENDER
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlNationality Then
                Dim objNationality As New clsMasterData
                mdsList = objNationality.getCountryList("Nationality", )
                intColType = 4
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.NATIONALITY
                'Sohail (14 Nov 2019) -- End
                'Sohail (02 Jul 2014) -- Start
                'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            ElseIf TabContainer1.ActiveTab Is tbpnlPayType Then
                Dim objPayType As New clsCommon_Master
                mdsList = objPayType.GetList(clsCommon_Master.enCommonMaster.PAY_TYPE, "List", -1, True)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_TYPE
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlPayPoint Then
                Dim objPayPoint As New clspaypoint_master
                mdsList = objPayPoint.GetList("List", True)
                intColType = 5
                'Sohail (02 Jul 2014) -- End
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.PAY_POINT
                'Sohail (14 Nov 2019) -- End
                'S.SANDEEP |04-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
            ElseIf TabContainer1.ActiveTab Is tbpnlMaritalStatus Then
                Dim objCommonMst As New clsCommon_Master
                mdsList = objCommonMst.GetList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, "List", -1, True)
                intColType = 6
                objCommonMst = Nothing
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.MARITAL_STATUS
                'Sohail (14 Nov 2019) -- End
            ElseIf TabContainer1.ActiveTab Is tbpnlRelationFromDpndt Then
                Dim objCommonMst As New clsCommon_Master
                mdsList = objCommonMst.GetList(clsCommon_Master.enCommonMaster.RELATIONS, "List", -1, True)
                intColType = 7
                objCommonMst = Nothing
                'S.SANDEEP |04-JUN-2019| -- END
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                mintSelectedAllocationID = enAdvanceFilterAllocation.RELATION_FROM_DEPENDENTS

            ElseIf TabContainer1.ActiveTab Is tbpnlSkill Then
                Dim objSkill As New clsskill_master
                mdsList = objSkill.getComboList("List", False)
                mintSelectedAllocationID = enAdvanceFilterAllocation.SKILL
                objSkill = Nothing
                intColType = 8
                'Sohail (14 Nov 2019) -- End
            End If

            If intColType = 0 Then
                mdsList.Tables(0).Columns(0).ColumnName = "Id"
                mdsList.Tables(0).Columns("name").ColumnName = "Name"
            ElseIf intColType = 1 Then
                'Nilay (15-Dec-2015) -- Start
                'mdsList.Tables(0).Columns("jobgroupunkid").ColumnName = "Id"
                'mdsList.Tables(0).Columns("jobname").ColumnName = "Name"
                mdsList.Tables(0).Columns("jobunkid").ColumnName = "Id"
                mdsList.Tables(0).Columns("jobname").ColumnName = "Name"
                'Nilay (15-Dec-2015) -- End
            ElseIf intColType = 2 Then
                mdsList.Tables(0).Columns(0).ColumnName = "Id"
                mdsList.Tables(0).Columns("costcentername").ColumnName = "Name"
            ElseIf intColType = 3 Then
                mdsList.Tables(0).Columns(1).ColumnName = "Id"
                mdsList.Tables(0).Columns(0).ColumnName = "Name"
            ElseIf intColType = 4 Then
                mdsList.Tables(0).Columns(0).ColumnName = "Id"
                mdsList.Tables(0).Columns("country_name").ColumnName = "Name"
                'Sohail (02 Jul 2014) -- Start
                'Enhancement - Pay Type and Pay Point allocation on Advance Filter.
            ElseIf intColType = 5 Then
                mdsList.Tables(0).Columns(0).ColumnName = "Id"
                mdsList.Tables(0).Columns("paypointname").ColumnName = "Name"
                'Sohail (02 Jul 2014) -- End

                'S.SANDEEP |04-JUN-2019| -- START
                'ISSUE/ENHANCEMENT : [Relation filters on Allocation Advance search]
            ElseIf intColType = 6 Then
                mdsList.Tables(0).Columns("masterunkid").ColumnName = "Id"
                mdsList.Tables(0).Columns("name").ColumnName = "Name"
            ElseIf intColType = 7 Then
                mdsList.Tables(0).Columns("masterunkid").ColumnName = "Id"
                mdsList.Tables(0).Columns("name").ColumnName = "Name"
                'S.SANDEEP |04-JUN-2019| -- END
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
            ElseIf intColType = 8 Then
                mdsList.Tables(0).Columns("skillunkid").ColumnName = "Id"
                mdsList.Tables(0).Columns("name").ColumnName = "Name"
                'Sohail (14 Nov 2019) -- End
            End If

            Dim dtCol As New DataColumn
            dtCol.ColumnName = "IsCheck"
            dtCol.Caption = ""
            dtCol.DataType = System.Type.GetType("System.Boolean")
            dtCol.DefaultValue = False
            mdsList.Tables(0).Columns.Add(dtCol)

            If mdtFilterInfo IsNot Nothing Then
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # : Bind Allocation to training course.
                'Dim allID As List(Of String) = (From p In mdtFilterInfo Where p.Item("GroupName") = TabContainer1.ActiveTab.HeaderText Select (p.Item("Id").ToString)).ToList
                Dim allID As List(Of String) = (From p In mdtFilterInfo Where CInt(p.Item("GroupId")) = mintSelectedAllocationID Select (p.Item("Id").ToString)).ToList
                'Sohail (14 Nov 2019) -- End
                Dim strIDs As String = String.Join(",", allID.ToArray)
                If strIDs.Trim <> "" Then
                    Dim dRow() As DataRow = mdsList.Tables(0).Select("Id IN (" & strIDs.Trim & ") ")
                    For Each drRow As DataRow In dRow
                        drRow.Item("IsCheck") = True
                    Next
                    mdsList.Tables(0).AcceptChanges()
                End If

            End If


        Catch ex As Exception
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception("TabContainer1_ActiveTabChanged :- " & ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Pinkal (20-Aug-2020) -- End
        Finally

            If mdsList.Tables(0).Rows.Count <= 0 Then
                Dim r As DataRow = mdsList.Tables(0).NewRow

                r.Item("Id") = -111
                r.Item("Name") = "None" ' To Hide the row and display only Row Header

                mdsList.Tables(0).Rows.Add(r)
            End If

            'Sohail (01 Aug 2019) -- Start
            'NMB Payroll UAT # TC006 - 76.1 - Advance search for process payroll, payslip, etc. should have search option. Currently user must scroll the entire list to select an item.
            Dim dtTable As DataTable = New DataView(mdsList.Tables(0), "", "IsCheck DESC, Name", DataViewRowState.CurrentRows).ToTable
            mdsList.Tables.Clear()
            mdsList.Tables.Add(dtTable)
            'Sohail (01 Aug 2019) -- End

            gvDetails.DataSource = mdsList.Tables(0)
            gvDetails.DataBind()

            popupAdvanceFilter.Show()
        End Try
    End Sub

#End Region


    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnApply.Text = Language._Object.getCaption(Me.btnApply.ID, Me.btnApply.Text).Replace("&", "")
            Me.tbpnlBranch.HeaderText = Language._Object.getCaption("btnBranch", Me.tbpnlBranch.HeaderText)
            Me.tbpnlDeptGroup.HeaderText = Language._Object.getCaption("btnDeptGrp", Me.tbpnlDeptGroup.HeaderText)
            Me.tbpnlDept.HeaderText = Language._Object.getCaption("btnDept", Me.tbpnlDept.HeaderText)
            Me.tbpnlSection.HeaderText = Language._Object.getCaption("btnSection", Me.tbpnlSection.HeaderText)
            Me.tbpnlUnit.HeaderText = Language._Object.getCaption("btnUnit", Me.tbpnlUnit.HeaderText)
            Me.tbpnlJob.HeaderText = Language._Object.getCaption("btnJob", Me.tbpnlJob.HeaderText)
            Me.tbpnlSectionGroup.HeaderText = Language._Object.getCaption("btnSectionGroup", Me.tbpnlSectionGroup.HeaderText)
            Me.tbpnlUnitGroup.HeaderText = Language._Object.getCaption("btnUnitGroup", Me.tbpnlUnitGroup.HeaderText)
            Me.tbpnlTeam.HeaderText = Language._Object.getCaption("btnTeam", Me.tbpnlTeam.HeaderText)
            Me.tbpnlClass.HeaderText = Language._Object.getCaption("btnClass", Me.tbpnlClass.HeaderText)
            Me.tbpnlClassGroup.HeaderText = Language._Object.getCaption("btnClassGroup", Me.tbpnlClassGroup.HeaderText)
            Me.tbpnlJobGroup.HeaderText = Language._Object.getCaption("btnJobGroup", Me.tbpnlJobGroup.HeaderText)
            Me.tbpnlGradeGroup.HeaderText = Language._Object.getCaption("btnGradeGroup", Me.tbpnlGradeGroup.HeaderText)
            Me.tbpnlGrade.HeaderText = Language._Object.getCaption("btnGrade", Me.tbpnlGrade.HeaderText)
            Me.tbpnlGradeLevel.HeaderText = Language._Object.getCaption("btnGradeLevel", Me.tbpnlGradeLevel.HeaderText)
            Me.tbpnlCostCenterGroup.HeaderText = Language._Object.getCaption("btnCostCenterGroup", Me.tbpnlCostCenterGroup.HeaderText)
            Me.tbpnlCostCenter.HeaderText = Language._Object.getCaption("btnCostCenter", Me.tbpnlCostCenter.HeaderText)
            Me.tbpnlEmployementType.HeaderText = Language._Object.getCaption("btnEmployementType", Me.tbpnlEmployementType.HeaderText)
            Me.tbpnlReligion.HeaderText = Language._Object.getCaption("btnReligion", Me.tbpnlReligion.HeaderText)
            Me.tbpnlGender.HeaderText = Language._Object.getCaption("btnGender", Me.tbpnlGender.HeaderText)
            Me.tbpnlNationality.HeaderText = Language._Object.getCaption("btnNationality", Me.tbpnlNationality.HeaderText)
            Me.tbpnlPayType.HeaderText = Language._Object.getCaption("btnPayType", Me.tbpnlPayType.HeaderText)
            Me.tbpnlPayPoint.HeaderText = Language._Object.getCaption("btnPayPoint", Me.tbpnlPayPoint.HeaderText)

            'Me.tbpnlMaritalStatus.HeaderText = Language._Object.getCaption(Me.btnMaritalStatus.ID, Me.tbpnlMaritalStatus.HeaderText) -> this is not in SS
            

            gvDetails.Columns(2).HeaderText = Language._Object.getCaption(gvDetails.Columns(2).FooterText, gvDetails.Columns(2).HeaderText)
            gvFilterInfo.Columns(1).HeaderText = Language._Object.getCaption(gvFilterInfo.Columns(1).FooterText, gvFilterInfo.Columns(1).HeaderText)


        Catch Ex As Exception
            'DisplayMessage.DisplayError(ex, Me)
            'Pinkal (20-Aug-2020) -- Start
            'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
            CommonCodes.LogErrorOnly(Ex)
            'Pinkal (20-Aug-2020) -- End
        End Try
    End Sub
End Class
