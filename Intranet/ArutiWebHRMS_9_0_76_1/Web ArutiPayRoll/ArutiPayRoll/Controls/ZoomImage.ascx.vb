﻿
Partial Class Controls_ZoomImage
    Inherits System.Web.UI.UserControl
    Private msg As New CommonCodes

#Region " Public Default Properties "

    Public Property Width() As Unit
        Get
            Return imgZoom.Width
        End Get
        Set(ByVal value As Unit)
            imgZoom.Width = value
        End Set
    End Property

    Public Property Height() As Unit
        Get
            Return imgZoom.Height
        End Get
        Set(ByVal value As Unit)
            imgZoom.Height = value
        End Set
    End Property

    Public Property ImageUrl() As String
        Get
            Return imgZoom.ImageUrl
        End Get
        Set(ByVal value As String)
            imgZoom.ImageUrl = value
        End Set
    End Property

    Public Property ImageAlign() As ImageAlign
        Get
            Return imgZoom.ImageAlign
        End Get
        Set(ByVal value As ImageAlign)
            imgZoom.ImageAlign = value
        End Set
    End Property

#End Region

#Region " Public Custom Properties "

    Private mintZoomPercentage As Integer = 250
    Public Property ZoomPercentage() As Integer
        Get
            Return mintZoomPercentage
        End Get
        Set(ByVal value As Integer)
            If value < 100 Then
                mintZoomPercentage = 100
            Else
                mintZoomPercentage = value
            End If

        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Page.IsPostBack = False) Then
                imgZoom.Attributes.Add("onmousemove", "ShowToolTip(this);")
                imgZoom.Attributes.Add("onmouseout", "HideToolTip();")
            End If
        Catch ex As Exception
            'Gajanan (26-Aug-2020) -- Start
            ' Working on IIS Freezing and Dump Issue for NMB.
            'Throw New Exception(ex.Message)
            CommonCodes.LogErrorOnly(ex)
            'Gajanan (26-Aug-2020) -- End
        End Try
    End Sub
End Class
