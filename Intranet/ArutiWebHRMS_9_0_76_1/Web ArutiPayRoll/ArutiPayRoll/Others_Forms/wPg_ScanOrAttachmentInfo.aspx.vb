﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO
Imports System.IO.Packaging

#End Region

Partial Class Others_Forms_wPg_ScanOrAttachmentInfo
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmScanOrAttachmentInfo"
    'Anjan [04 June 2014] -- End
    Private objDocument As New clsScan_Attach_Documents
    Private mstrLableCaption As String = ""
    Private mintRefModuleId As Integer = -1
    Private mAction As enAction
    Private mstrToEditIds As String

    Private mdtTran As DataTable
    Private dtEmployee As DataTable

    'Pinkal (18-Jun-2015) -- Start
    'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
    Private mintEmployeeID As Integer = 0
    Private mintScanRefId As Integer = 0
    Private mblnIsOnAddTransaction As Boolean = False
    Private mintLeaveTypeID As Integer = 0
    Private mintLeaveFormID As Integer = 0
    'Pinkal (18-Jun-2015) -- End

    'Gajanan [02-SEP-2019] -- Start      
    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
    Private mintTransactionID As Integer = -1
    Private mstrTransactionScreenName As String = String.Empty
    Private mintscanAttactRefID As Integer = -1
    'Gajanan [02-SEP-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End

#End Region

#Region " Private Methods Functions "
    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objCMaster As New clsCommon_Master
        Try
            Select Case mintRefModuleId
                Case enImg_Email_RefId.Employee_Module, _
                     enImg_Email_RefId.Dependants_Beneficiaries, _
                     enImg_Email_RefId.Leave_Module, _
                     enImg_Email_RefId.Payroll_Module, _
                     enImg_Email_RefId.Training_Module, _
                     enImg_Email_RefId.Discipline_Module, _
                     enImg_Email_RefId.Applicant_Job_Vacancy
                    'Gajanan [24-Sep-2019] -- Add [Applicant_Job_Vacancy]

                    Dim objEMaster As New clsEmployee_Master

                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'dsCombo = objEMaster.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

                    'Pinkal (18-Jun-2015) -- Start
                    'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
                    'dsCombo = objEMaster.GetEmployeeList("List", True, Session("IsIncludeInactiveEmp"))

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombo = objEMaster.GetEmployeeList("List", IIf(mintEmployeeID > 0, False, True), Session("IsIncludeInactiveEmp"), mintEmployeeID)
                    dsCombo = objEMaster.GetEmployeeList(Session("Database_Name"), _
                                                         Session("UserId"), _
                                                         Session("Fin_year"), _
                                                         Session("CompanyUnkId"), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         Session("UserAccessModeSetting"), True, _
                                                         Session("IsIncludeInactiveEmp"), _
                                                         "List", IIf(mintEmployeeID > 0, False, True), _
                                                         mintEmployeeID)
                    'Shani(20-Nov-2015) -- End

                    'Pinkal (18-Jun-2015) -- End


                    'Sohail (23 Apr 2012) -- End
                    dtEmployee = dsCombo.Tables(0)

                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        'Sohail (23 Apr 2012) -- Start
                        'TRA - ENHANCEMENT
                        'dsCombo = objEMaster.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
                        'Pinkal (18-Jun-2015) -- Start
                        'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
                        'dsCombo = objEMaster.GetEmployeeList("List", True, Session("IsIncludeInactiveEmp"))

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'dsCombo = objEMaster.GetEmployeeList("List", IIf(mintEmployeeID > 0, False, True), Session("IsIncludeInactiveEmp"), mintEmployeeID)
                        dsCombo = objEMaster.GetEmployeeList(Session("Database_Name"), _
                                                             Session("UserId"), _
                                                             Session("Fin_year"), _
                                                             Session("CompanyUnkId"), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                             Session("UserAccessModeSetting"), True, _
                                                             Session("IsIncludeInactiveEmp"), "List", IIf(mintEmployeeID > 0, False, True), mintEmployeeID)
                        'Shani(20-Nov-2015) -- End

                        'Pinkal (18-Jun-2015) -- End
                        'Sohail (23 Apr 2012) -- End

                        With cboEmployee
                            .DataValueField = "employeeunkid"
                            'Nilay (09-Aug-2016) -- Start
                            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                            '.DataTextField = "employeename"
                            .DataTextField = "EmpCodeName"
                            'Nilay (09-Aug-2016) -- End
                            .DataSource = dtEmployee
                            .DataBind()
                        End With
                    Else
                        Dim objglobalassess = New GlobalAccess
                        objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                        cboEmployee.DataSource = objglobalassess.ListOfEmployee
                        cboEmployee.DataTextField = "loginname"
                        cboEmployee.DataValueField = "employeeunkid"
                        cboEmployee.DataBind()
                        dtEmployee = New DataView(dtEmployee, "employeeunkid = " & objglobalassess.employeeid & " ", "", DataViewRowState.CurrentRows).ToTable
                    End If

                Case enImg_Email_RefId.Applicant_Module

                    Dim objAMaster As New clsApplicant_master

                    dsCombo = objAMaster.GetApplicantList("List", True)

                    With cboEmployee
                        .DataValueField = "applicantunkid"
                        .DataTextField = "applicantname"
                        .DataSource = dsCombo.Tables("List")
                        .DataBind()
                    End With
                    dtEmployee = New DataView(objAMaster.GetApplicantList("List", True).Tables(0)).ToTable
            End Select

            dsCombo = objCMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocument
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'dsCombo = objDocument.GetDocType()
            dsCombo = objDocument.GetDocType(CInt(Session("CompanyUnkId").ToString()), False)
            'Gajanan [17-April-2019] -- End

            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
            Dim dtRefID As DataTable = Nothing
            If mintScanRefId > 0 Then

                'Pinkal (27-Oct-2020) -- Start
                'NMB -   Bug solved for attachment from leave form application.
                If mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy Then
                    dtRefID = New DataView(dsCombo.Tables(0), "Id in(0, " & enScanAttactRefId.COVER_LETTER & "," & enScanAttactRefId.CURRICULAM_VITAE & "  )", "", DataViewRowState.CurrentRows).ToTable()
                Else
                dtRefID = New DataView(dsCombo.Tables(0), "Id = " & mintScanRefId, "", DataViewRowState.CurrentRows).ToTable()
                End If
            Else
                If mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy Then
                    dtRefID = New DataView(dsCombo.Tables(0), "Id in(0, " & enScanAttactRefId.COVER_LETTER & "," & enScanAttactRefId.CURRICULAM_VITAE & "  )", "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtRefID = dsCombo.Tables(0)
            End If
                'Pinkal (27-Oct-2020) -- End
            End If
            'Pinkal (18-Jun-2015) -- End


            'Pinkal (27-Oct-2020) -- Start
            'NMB -   Bug solved for attachment from leave form application.

            'Gajanan [24-Sep-2019] -- Start
            'Enhancement - NMB Recruitment Changes.
            'dtRefID = Nothing
            'If mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy Then
            '    dtRefID = New DataView(dsCombo.Tables(0), "Id in(0, " & enScanAttactRefId.COVER_LETTER & "," & enScanAttactRefId.CURRICULAM_VITAE & "  )", "", DataViewRowState.CurrentRows).ToTable()
            'Else
            '    dtRefID = dsCombo.Tables(0)
            'End If
            'Gajanan [24-Sep-2019] -- End

            'Pinkal (27-Oct-2020) -- End



            With cboDocumentType
                .DataValueField = "Id"
                .DataTextField = "Name"
                'Pinkal (18-Jun-2015) -- Start
                'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
                '.DataSource = dsCombo.Tables(0)
                .DataSource = dtRefID
                'Pinkal (18-Jun-2015) -- End
                .DataBind()
            End With

            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            If mintscanAttactRefID > 0 Then
                cboDocumentType.SelectedValue = mintscanAttactRefID
            End If
            'Gajanan [02-SEP-2019] -- End

            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
            cboDocumentType_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (18-Jun-2015) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsCombo.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Try
            lblError.Text = ""
            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            If cboEmployee.SelectedValue <> "" AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                'Gajanan [02-SEP-2019] -- End
                Select Case mintRefModuleId
                    Case enImg_Email_RefId.Employee_Module
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        Language.setLanguage(mstrModuleName)
                        lblError.Text = Language.getMessage(mstrModuleName, 1, "  Employee is mandatory information. Please select Employee to continue.  ")
                        'Anjan [04 June 2014] -- End
                        cboEmployee.Focus()
                    Case enImg_Email_RefId.Applicant_Module
                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        Language.setLanguage(mstrModuleName)
                        lblError.Text = Language.getMessage(mstrModuleName, 4, "  Applicant is mandatory information. Please select Applicant to continue.  ")
                        'Anjan [04 June 2014] -- End
                        cboEmployee.Focus()
                End Select
                Return False
            End If

            If CInt(cboDocument.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                lblError.Text = Language.getMessage(mstrModuleName, 2, "  Document is mandatory information. Please select Document to continue.  ")
                'Anjan [04 June 2014] -- End
                cboDocument.Focus()
                Return False
            End If

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                lblError.Text = Language.getMessage(mstrModuleName, 3, "  Document Type is mandatory information. Please select Document Type to continue.  ")
                'Anjan [04 June 2014] -- End
                cboDocumentType.Focus()
                Return False
            End If



            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
            If mblnIsOnAddTransaction = False Then
                If cboAssocatedValue.Visible = True Then
                    'Shani (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    '*** Make Qualification selection optional as per discussion with Andrew on Skype on 02-May-2015.
                    'If CInt(cboAssocatedValue.SelectedValue) <= 0 Then
                    If CInt(cboAssocatedValue.SelectedValue) <= 0 AndAlso CInt(cboDocumentType.SelectedValue) <> enScanAttactRefId.QUALIFICATIONS Then
                        'Shani (22 Apr 2015) -- End

                        'Anjan [04 June 2014] -- Start
                        'ENHANCEMENT : Implementing Language,requested by Andrew
                        lblError.Text = "  " & cboDocumentType.SelectedItem.Text & Language.getMessage(mstrModuleName, 8, " is mandatory information. Please select ") & cboDocumentType.SelectedItem.Text & Language.getMessage(mstrModuleName, 9, " in order to continue.")
                        'Anjan [04 June 2014] -- End
                        cboAssocatedValue.Focus()
                        Return False
                    End If
                End If
            End If
            'Pinkal (18-Jun-2015) -- End

            If dtpUploadDate.GetDate = CDate("01/Jan/2000") Then
                lblError.Text = " Please select date."
                dtpUploadDate.Focus()
                Return False
            End If


            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'If docUpload.HasFile = False Then
            '    lblError.Text = "  Please upload document to attach.  "
            '    Return False
            'End If
            'SHANI (20 JUN 2015) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsDataValid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Sub AddRow(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Try
            Dim dRow As DataRow = mdtTran.NewRow
            dRow.Item("scanattachtranunkid") = -1
            dRow.Item("documentunkid") = cboDocument.SelectedValue
            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            'dRow.Item("employeeunkid") = cboEmployee.SelectedValue
            If cboEmployee.SelectedValue <> "" Then
                dRow.Item("employeeunkid") = cboEmployee.SelectedValue
            Else
                dRow.Item("employeeunkid") = -1
            End If
            'Gajanan [02-SEP-2019] -- End
            dRow.Item("filename") = f.Name
            dRow.Item("scanattachrefid") = cboDocumentType.SelectedValue
            dRow.Item("modulerefid") = mintRefModuleId
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'dRow.Item("userunkid") = Global.Aruti.Data.User._Object._Userunkid
                dRow.Item("userunkid") = Session("UserId")
                'Sohail (23 Apr 2012) -- End
            Else
                dRow.Item("userunkid") = -1
            End If
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("orgfilepath") = strfullpath

            If cboAssocatedValue.Visible = True Then
            dRow.Item("valuename") = cboAssocatedValue.SelectedItem.Text
            Else
                dRow.Item("valuename") = ""
            End If

            Select Case CInt(cboDocumentType.SelectedValue)
                Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS, enScanAttactRefId.COVER_LETTER
                    dRow.Item("transactionunkid") = -1
                Case Else
                    'Shani (22 Apr 2015) -- Start
                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                    'dRow.Item("transactionunkid") = cboAssocatedValue.SelectedValue


                    If cboAssocatedValue.Visible = False AndAlso cboAssocatedValue.SelectedValue Is Nothing Then
                        dRow.Item("transactionunkid") = -1
                        dRow.Item("valuename") = ""
                    Else
                        dRow.Item("transactionunkid") = cboAssocatedValue.SelectedValue
                        If CInt(cboAssocatedValue.SelectedValue) <= 0 Then
                            dRow.Item("valuename") = ""
                        End If
                    End If
                    'Shani (22 Apr 2015) -- End
            End Select
            dRow.Item("attached_date") = dtpUploadDate.GetDate
            dRow.Item("doctype") = cboDocumentType.SelectedItem.Text
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._Document_Path.LastIndexOf("\") = ConfigParameter._Object._Document_Path.Length - 1 Then
            '    dRow.Item("destfilepath") = ConfigParameter._Object._Document_Path & cboDocumentType.SelectedItem.Text & "\" & f.Name
            'Else
            '    dRow.Item("destfilepath") = ConfigParameter._Object._Document_Path & "\" & cboDocumentType.SelectedItem.Text & "\" & f.Name
            'End If

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'dRow.Item("destfilepath") = ""
            If Session("Document_Path").ToString.LastIndexOf("\") = Session("Document_Path").ToString.Length - 1 Then
                dRow.Item("destfilepath") = Session("Document_Path").ToString & cboDocumentType.SelectedItem.Text & "\" & f.Name
            Else
                dRow.Item("destfilepath") = Session("Document_Path").ToString & "\" & cboDocumentType.SelectedItem.Text & "\" & f.Name
            End If
            'SHANI (20 JUN 2015) -- End

            'Sohail (23 Apr 2012) -- End
            'Sohail (16 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'dRow.Item("names") = cboEmployee.SelectedItem.Text 'Sohail (29 Jan 2013)
            Dim dTemp() As DataRow = Nothing
            Select Case mintRefModuleId

                'Pinkal (18-Jun-2015) -- Start
                'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.

                Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Leave_Module, enImg_Email_RefId.Applicant_Job_Vacancy
                    Dim objEmp As New clsEmployee_Master

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboEmployee.SelectedValue)
                    'Shani(20-Nov-2015) -- End


                    dRow.Item("code") = objEmp._Employeecode
                    dRow.Item("names") = objEmp._Firstname & " " & objEmp._Othername & " " & objEmp._Surname

                    objEmp = Nothing

                    'Pinkal (18-Jun-2015) -- End

                Case enImg_Email_RefId.Applicant_Module
                    Dim objApplicant As New clsApplicant_master
                    objApplicant._Applicantunkid = CInt(cboEmployee.SelectedValue)

                    dRow.Item("code") = objApplicant._Applicant_Code
                    dRow.Item("names") = objApplicant._Firstname & " " & objApplicant._Othername & " " & objApplicant._Surname

                    objApplicant = Nothing
            End Select
            'Sohail (16 Feb 2013) -- End

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            dRow.Item("filesize") = f.Length / 1024
            'SHANI (16 JUL 2015) -- End 

            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
            dRow("file_data") = xDocumentData
            'S.SANDEEP |25-JAN-2019| -- END

            mdtTran.Rows.Add(dRow)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("AddRow:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Try
            '*** To display Row Header when Empty Data Row
            Dim isBlank As Boolean = False
            If mdtTran.Rows.Count <= 0 Then
                Dim r As DataRow = mdtTran.NewRow

                r.Item(3) = "None" ' To Hide the row and display only Row Header
                r.Item(12) = ""
                mdtTran.Rows.Add(r)
                isBlank = True
            End If

            gvScanAttachment.DataSource = mdtTran
            gvScanAttachment.DataBind()

            If isBlank Then
                gvScanAttachment.Rows(0).Visible = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Reset_Control()
        Try
            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            'cboEmployee.SelectedIndex = 0
            If cboEmployee.DataSource <> Nothing Then
                cboEmployee.SelectedIndex = 0
            End If
            'Gajanan [02-SEP-2019] -- End
            cboDocument.SelectedValue = 0
            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
            'cboDocumentType.SelectedValue = 0
            cboDocumentType.SelectedIndex = 0
            'Pinkal (18-Jun-2015) -- End
            Call cboDocumentType_SelectedIndexChanged(cboDocumentType, New System.EventArgs) 'Sohail (15 Jun 2021)
            cboAssocatedValue.SelectedIndex = -1
            dtpUploadDate.SetDate = DateAndTime.Now.Date
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Reset_Control:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try

        Using ms As New MemoryStream()
            Using pkg As Package = Package.Open(ms, FileMode.Create)
                For Each item As String In fileToAdd
                    Dim destFilename As String = "/" & Path.GetFileName(item)
                    Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                    Dim byt As Byte() = File.ReadAllBytes(item)

                    Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                        CopyStream(CType(fileStream, Stream), part.GetStream())
                    End Using
                Next
                pkg.Close()
            End Using
            Response.Clear()
            Response.ContentType = "application/zip"
            Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
            Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
            Response.End()

        End Using
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region " Page's Events "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Exit Sub
                    End If
                End If
            End If
            'SHANI (20 JUN 2015) -- End 

            ViewState.Add("mstrLableCaption", mstrLableCaption)
            ViewState.Add("mintRefModuleId", mintRefModuleId)
            ViewState.Add("mAction", mAction)
            ViewState.Add("mstrToEditIds", mstrToEditIds)

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'ViewState.Add("mdtTran", mdtTran)
            If Session("mdtTran") Is Nothing Then
                Session.Add("mdtTran", mdtTran)
            Else
                Session("mdtTran") = mdtTran
            End If
            'SHANI (20 JUN 2015) -- End
            ViewState.Add("dtEmployee", dtEmployee)

            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            ViewState.Add("TransactionID", mintTransactionID)
            ViewState.Add("ScanAttactRefID", mintscanAttactRefID)
            ViewState.Add("TransactionScreenName", mstrTransactionScreenName)
            'Gajanan [02-SEP-2019] -- End
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End


            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        'S.SANDEEP |16-MAY-2019| -- START
                        'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                        'postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        'Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName.ToString().Replace(" ", "_")))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName.ToString().Replace(" ", "_")))
                        'S.SANDEEP |16-MAY-2019| -- END
                        Exit Sub
                    End If
                End If
                'Pinkal (18-Jun-2015) -- Start
                'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
                Dim ar() As String = Nothing
                ar = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).ToString.Split("|")
                If ar.Length > 0 Then
                    mintLeaveFormID = CInt(ar(0))
                    mintScanRefId = CInt(ar(1))
                End If
                If Session("LVForm_EmployeeID") IsNot Nothing AndAlso CInt(Session("LVForm_EmployeeID")) > 0 Then
                    mintEmployeeID = CInt(Session("LVForm_EmployeeID"))
                End If
                If Session("LVForm_LeaveTypeID") IsNot Nothing AndAlso CInt(Session("LVForm_LeaveTypeID")) > 0 Then
                    mintLeaveTypeID = CInt(Session("LVForm_LeaveTypeID"))
                End If
                If (Session("LVForm_EmployeeID") IsNot Nothing AndAlso CInt(Session("LVForm_EmployeeID")) > 0) AndAlso (Session("LVForm_LeaveTypeID") IsNot Nothing AndAlso CInt(Session("LVForm_LeaveTypeID")) > 0) Then
                    mblnIsOnAddTransaction = True

                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    lblDocType.Visible = False
                    cboDocumentType.Visible = False
                    lblUploadDate.Visible = False
                    dtpUploadDate.Visible = False
                    lblSelection.Visible = False
                    cboAssocatedValue.Visible = False
                    'Pinkal (03-May-2019) -- End

                End If
                'Pinkal (18-Jun-2015) -- End
            End If
            'SHANI (20 JUN 2015) -- End

            If Not IsPostBack Then

                mstrLableCaption = Session("ScanAttchLableCaption").ToString
                mintRefModuleId = CInt(Session("ScanAttchRefModuleID"))
                mAction = CInt(Session("ScanAttchenAction"))
                mstrToEditIds = Session("ScanAttchToEditIDs").ToString

                objlblCaption.Text = Session("ScanAttchLableCaption").ToString
                dtpUploadDate.SetDate = DateAndTime.Now.Date

                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB

                'mintTransactionID = IIf(IsNothing(Session("TransactionID")) = False, CInt(Session("TransactionID").ToString()), 0)
                'mintscanAttactRefID = IIf(IsNothing(Session("ScanAttactRefID")) = False, CInt(Session("ScanAttactRefID").ToString()), 0)
                'mstrTransactionScreenName = IIf(IsNothing(Session("TransactionScreenName")) = False, CStr(Session("TransactionScreenName").ToString()), String.Empty)

                If IsNothing(Session("TransactionID")) = False Then
                    mintTransactionID = CInt(Session("TransactionID").ToString())
                Else
                    mintTransactionID = 0
                End If


                If mintTransactionID > 0 Then
                    ViewState("TransactionID") = Session("TransactionID")
                    Session.Remove("TransactionID")
                End If


                If IsNothing(Session("TransactionScreenName")) = False Then
                    mstrTransactionScreenName = Session("TransactionScreenName").ToString()
                Else
                    mstrTransactionScreenName = String.Empty
                End If



                If mstrTransactionScreenName.Length > 0 Then
                    ViewState("TransactionScreenName") = Session("TransactionScreenName")
                    Session.Remove("TransactionScreenName")
                End If


                If IsNothing(Session("ScanAttactRefID")) = False Then
                    mintscanAttactRefID = CInt(Session("ScanAttactRefID"))
                Else
                    mintscanAttactRefID = 0
                End If


                If mintscanAttactRefID > 0 Then
                    ViewState("ScanAttactRefID") = CInt(Session("ScanAttactRefID"))
                    Session.Remove("ScanAttactRefID")
                End If
                'Gajanan [02-SEP-2019] -- End

                Call FillCombo()
                ' Call FillList()

                If mAction = enAction.EDIT_ONE Then
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Call objDocument.GetList("List", mstrToEditIds)

                    Call objDocument.GetList("List", mstrToEditIds, Session("Document_Path"))


                    'Sohail (23 Apr 2012) -- End
                    'btnEdit.Visible = True
                    'btnEdit.BringToFront()
                    btnScan.Enabled = False
                    btnAdd.Enabled = False
                    cboEmployee.Enabled = False
                    'objbtnSearch.Enabled = False
                    'AddHandler lvScanAttachment.SelectedIndexChanged, AddressOf lvScanAttachment_SelectedIndexChanged
                End If



                If mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy AndAlso Session("LoginBy") = Global.User.en_loginby.Employee Then
                    Call objDocument.GetList("List", "", Session("Document_Path"), CInt(Session("Employeeunkid")), False, Nothing, , " scanattachrefid in(" & enScanAttactRefId.COVER_LETTER & "," & enScanAttactRefId.CURRICULAM_VITAE & "  )")
                End If


                mdtTran = objDocument._Datatable.Copy


                If mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy AndAlso Session("LoginBy") = Global.User.en_loginby.Employee Then
                    Call FillList()
                End If

                If mAction = enAction.EDIT_ONE Then
                    Call FillList()
                    'Shani(08-Jan-2016) -- Start
                ElseIf Session("ScanAttachData") IsNot Nothing AndAlso CType(Session("ScanAttachData"), DataTable).Rows.Count > 0 Then
                    mdtTran = CType(Session("ScanAttachData"), DataTable).Copy
                    Call FillList()
                    'Shani(08-Jan-2016) -- End

                End If
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = "~\UserHome.aspx"
                End If
                'Sohail (09 Nov 2020) -- End

            Else


                mstrLableCaption = ViewState("mstrLableCaption")
                mintRefModuleId = ViewState("mintRefModuleId")
                mAction = ViewState("mAction")
                mstrToEditIds = ViewState("mstrToEditIds")

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'mdtTran = ViewState("mdtTran")
                mdtTran = Session("mdtTran")
                'SHANI (20 JUN 2015) -- End
                dtEmployee = ViewState("dtEmployee")
                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB

                'mintTransactionID = IIf(IsNothing(ViewState("TransactionID")) = False, CInt(ViewState("TransactionID").ToString()), 0)
                'mintscanAttactRefID = IIf(IsNothing(ViewState("ScanAttactRefID")) = False, CInt(ViewState("ScanAttactRefID").ToString()), 0)
                'mstrTransactionScreenName = IIf(IsNothing(ViewState("TransactionScreenName")) = False, CStr(ViewState("TransactionScreenName").ToString()), String.Empty)


                If IsNothing(ViewState("TransactionID")) = False Then
                    mintTransactionID = CInt(ViewState("TransactionID").ToString())
                Else
                    mintTransactionID = 0
                End If



                If IsNothing(ViewState("ScanAttactRefID")) = False Then
                    mintscanAttactRefID = CInt(ViewState("ScanAttactRefID").ToString())
                Else
                    mintscanAttactRefID = 0
                End If


                If IsNothing(ViewState("TransactionScreenName")) = False Then
                    mstrTransactionScreenName = CStr(ViewState("TransactionScreenName").ToString())
                Else
                    mstrTransactionScreenName = String.Empty
                End If
                'Gajanan [02-SEP-2019] -- End
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Combobox's Events "
    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            cboDocumentType.SelectedValue = 0

            'Gajanan [24-Sep-2019] -- Start
            'Enhancement - NMB Recruitment Changes.
            If mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy AndAlso Session("LoginBy") = Global.User.en_loginby.User Then
                Call objDocument.GetList("List", "", Session("Document_Path"), cboEmployee.SelectedValue, False, Nothing, , " scanattachrefid in(" & enScanAttactRefId.COVER_LETTER & "," & enScanAttactRefId.CURRICULAM_VITAE & "  )")
                mdtTran = objDocument._Datatable.Copy
                Call FillList()
            End If
            'Gajanan [24-Sep-2019] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboDocumentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDocumentType.SelectedIndexChanged
        Try
            If CInt(cboDocumentType.SelectedValue) > 0 Then
                Dim blnFlag As Boolean = False
                lblSelection.Text = "Select " & cboDocumentType.SelectedItem.Text

                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                If CInt(cboDocumentType.SelectedValue) = enScanAttactRefId.STAFF_REQUISITION Then
                    lblSelection.Text = "Reference Number"
                    cboEmployee.Enabled = False
                Else
                    cboEmployee.Enabled = True
                End If
                'Gajanan [02-SEP-2019] -- End

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                If (Session("LVForm_EmployeeID") IsNot Nothing AndAlso CInt(Session("LVForm_EmployeeID")) > 0) AndAlso (Session("LVForm_LeaveTypeID") IsNot Nothing AndAlso CInt(Session("LVForm_LeaveTypeID")) > 0) Then
                    cboAssocatedValue.Visible = False
                Else
                    cboAssocatedValue.Visible = True
                End If
                'Pinkal (03-May-2019) -- End

                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                Dim mintEmployeeid As Integer = 0
                If cboEmployee.SelectedValue <> "" Then
                    mintEmployeeid = CInt(cboEmployee.SelectedValue)
                End If
                'Gajanan [02-SEP-2019] -- End

                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Dim ds As DataSet = objDocument.GetAssociatedValue(CInt(cboDocumentType.SelectedValue), CInt(cboEmployee.SelectedValue), mintRefModuleId, True)

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim ds As DataSet = objDocument.GetAssociatedValue(CInt(cboDocumentType.SelectedValue), CInt(cboEmployee.SelectedValue), mintRefModuleId, True, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
                Dim ds As DataSet = objDocument.GetAssociatedValue(Session("Database_Name"), _
                                                                   Session("UserId"), _
                                                                   Session("Fin_year"), _
                                                                   Session("CompanyUnkId"), _
                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                   Session("UserAccessModeSetting"), True, _
                                                                   Session("IsIncludeInactiveEmp"), _
                                                                   CInt(cboDocumentType.SelectedValue), _
                                                                   mintEmployeeid, mintRefModuleId, _
                                                                   Session("FinancialYear_Name"), _
                                                                   Session("fin_startdate"), _
                                                                   Session("fin_enddate"), mstrTransactionScreenName, True, mintTransactionID)
                'Gajanan [02-SEP-2019] - [CInt(cboEmployee.SelectedValue)=mintEmployeeid], [mstrTransactionScreenName]
                'Shani(20-Nov-2015) -- End

                'Sohail (23 Apr 2012) -- End

                'Pinkal (07-May-2015) -- Start
                'Leave Enhancement - CHANGES IN LEAVE FORM FOR SCAN ATTACH DOCUMENTS.
                If mblnIsOnAddTransaction AndAlso mintScanRefId = enScanAttactRefId.LEAVEFORMS Then
                    If ds.Tables(0).Rows.Count > 0 Then

                        'Shani(23-Jan-2016) -- Start
                        '
                        Dim drRow() As DataRow
                        If mintLeaveFormID > 0 Then
                            drRow = ds.Tables(0).Select("Id <> " & mintLeaveFormID)
                        Else
                            drRow = ds.Tables(0).Select("Id <> 0")
                        End If
                        'Shani(23-Jan-2016) -- End

                        If drRow.Length > 0 Then
                            For i As Integer = 0 To drRow.Length - 1
                                drRow(i).Delete()
                            Next
                        End If
                        ds.AcceptChanges()
                    End If
                End If
                'Pinkal (07-May-2015) -- End

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 Then
                    With cboAssocatedValue
                        Select Case CInt(cboDocumentType.SelectedValue)
                            Case enScanAttactRefId.IDENTITYS, enScanAttactRefId.QUALIFICATIONS, enScanAttactRefId.LEAVEFORMS, enScanAttactRefId.TRAININGS, enScanAttactRefId.JOBHISTORYS
                                .DataSource = Nothing
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                blnFlag = False
                            Case enScanAttactRefId.MEMBERSHIPS
                                .DataSource = Nothing
                                .DataValueField = "Mem_Id"
                                .DataTextField = "Mem_Name"
                                blnFlag = False
                            Case enScanAttactRefId.CURRICULAM_VITAE, enScanAttactRefId.OTHERS
                                blnFlag = True
                            Case enScanAttactRefId.DISCIPLINES
                                .DataSource = Nothing
                                Dim dtRow As DataRow = ds.Tables(0).NewRow
                                dtRow.Item("disciplinefileunkid") = 0
                                dtRow.Item("incident") = "Select"
                                ds.Tables(0).Rows.Add(dtRow)
                                ds.Tables(0).DefaultView.Sort = "disciplinefileunkid"
                                .DataValueField = "disciplinefileunkid"
                                .DataTextField = "incident"
                                blnFlag = False

                            Case enScanAttactRefId.ASSET_DECLARATION
                                .DataSource = Nothing
                                .DataValueField = "assetdeclarationunkid"
                                .DataTextField = "employeename"
                                blnFlag = False

                                'SHANI (20 JUN 2015) -- Start
                                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                            Case enScanAttactRefId.DEPENDANTS
                                .DataSource = Nothing
                                .DataValueField = "DpndtTranId"
                                .DataTextField = "DpndtBefName"
                                blnFlag = False
                                'SHANI (20 JUN 2015) -- End

                                'Gajanan [02-SEP-2019] -- Start      
                                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                            Case enScanAttactRefId.STAFF_REQUISITION
                                .DataSource = Nothing
                                .DataValueField = "staffrequisitiontranunkid"
                                .DataTextField = "formno"
                                blnFlag = False
                                'Gajanan [02-SEP-2019] -- End

                        End Select

                        If blnFlag = False Then
                            .DataSource = ds.Tables(0)
                            .DataBind()
                            If ds.Tables(0).Rows.Count > 0 Then .SelectedIndex = 0
                        Else
                            lblSelection.Text = "" : cboAssocatedValue.Visible = False
                        End If
                    End With
                Else
                    lblSelection.Text = "" : cboAssocatedValue.Visible = False
                End If
            Else
                lblSelection.Text = "" : cboAssocatedValue.Visible = False
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboDocumentType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " Gridview's Events "

    Protected Sub gvScanAttachment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvScanAttachment.RowCommand
        Try
            If e.CommandName = "Remove" Then
                mdtTran.Rows(CInt(e.CommandArgument)).Item("AUD") = "D"
                gvScanAttachment.DataSource = mdtTran
                gvScanAttachment.DataBind()
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
            ElseIf e.CommandName.ToString().ToUpper() = "DOWNLOAD" Then
                Dim xRow As DataRow = Nothing : Dim xPath As String = ""
                xRow = mdtTran.Rows(CInt(e.CommandArgument))
                If CInt(xRow.Item("scanattachtranunkid")) > 0 Then
                    xPath = xRow.Item("filepath").ToString
                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                    If Strings.Left(xPath, 1) <> "/" Then
                        xPath = "~/" & xPath
                    Else
                        xPath = "~" & xPath
                    End If
                    xPath = Server.MapPath(xPath)
                ElseIf xRow.Item("GUID").ToString.Trim <> "" Then
                    xPath = xRow.Item("orgfilepath").ToString
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        DisplayMessage.DisplayMessage("File does not Exist...", Me)
                        Exit Sub
                    End If
                    fileInfo = Nothing
                    Dim strFile As String = xPath
                    Response.ContentType = "image/jpg/pdf"
                    Response.AddHeader("Content-Disposition", "attachment;filename=""" & xRow("filename").ToString() & """")
                    Response.Clear()
                    Response.TransmitFile(strFile)
                    HttpContext.Current.ApplicationInstance.CompleteRequest()
                End If
                'S.SANDEEP |16-MAY-2019| -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("gvScanAttachment_RowCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    'Protected Sub gvScanAttachment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvScanAttachment.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then

    '            If e.Row.Cells(4).Text = "None" AndAlso e.Row.Cells(4).Text = "&nbsp;" Then
    '                e.Row.Visible = False
    '            Else
    '                Dim dTemp() As DataRow = Nothing
    '                Dim dtRow As DataRowView = e.Row.DataItem

    '                If IsDBNull(dtRow.Item("AUD")) = False AndAlso dtRow.Item("AUD").ToString = "D" Then
    '                    e.Row.Visible = False
    '                Else

    '                    If CInt(dtRow.Item("employeeunkid")) > 0 Then
    '                        Select Case mintRefModuleId

    '                            'Pinkal (18-Jun-2015) -- Start
    '                            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
    '                            Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Leave_Module
    '                                dTemp = dtEmployee.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
    '                                'Pinkal (18-Jun-2015) -- End
    '                            Case enImg_Email_RefId.Applicant_Module
    '                                dTemp = dtEmployee.Select("applicantunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
    '                        End Select
    '                    Else
    '                        Select Case mintRefModuleId
    '                            Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Leave_Module
    '                                dTemp = dtEmployee.Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
    '                            Case enImg_Email_RefId.Applicant_Module
    '                                dTemp = dtEmployee.Select("applicantunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
    '                        End Select
    '                    End If

    '                    If dTemp.Length > 0 Then
    '                        Select Case mintRefModuleId
    '                            Case enImg_Email_RefId.Employee_Module
    '                                e.Row.Cells(1).Text = dTemp(0)("employeecode").ToString
    '                                e.Row.Cells(2).Text = dTemp(0)("employeename").ToString
    '                            Case enImg_Email_RefId.Applicant_Module
    '                                e.Row.Cells(1).Text = dTemp(0)("applicant_code").ToString
    '                                e.Row.Cells(2).Text = dTemp(0)("applicantname").ToString
    '                        End Select
    '                    End If


    '                    If CInt(dtRow.Item("documentunkid")) > 0 Then
    '                        cboDocument.SelectedValue = CInt(dtRow.Item("documentunkid"))
    '                        e.Row.Cells(3).Text = cboDocument.SelectedItem.Text
    '                    Else
    '                        e.Row.Cells(3).Text = ""
    '                    End If


    '                    'SHANI (20 JUN 2015) -- Start
    '                    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    '                    'If CInt(dtRow.Item("scanattachrefid")) > 0 Then
    '                    '    cboDocumentType.SelectedValue = CInt(dtRow.Item("scanattachrefid"))
    '                    'End If
    '                    'e.Row.Cells(5).Text = cboDocumentType.SelectedItem.Text.ToString
    '                    'SHANI (20 JUN 2015) -- End
    '                    Select Case mintRefModuleId
    '                        Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Leave_Module
    '                            'dtRow.Item("") = cboDocumentType.Text.ToString
    '                            'lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
    '                            'lvItem.SubItems.Add("-1")
    '                            e.Row.Cells(6).Text = dtRow.Item("employeeunkid")
    '                            e.Row.Cells(7).Text = -1
    '                        Case enImg_Email_RefId.Applicant_Module
    '                            'lvItem.SubItems.Add("-1")
    '                            'lvItem.SubItems.Add(dtRow.Item("employeeunkid").ToString)
    '                            e.Row.Cells(6).Text = -1
    '                            e.Row.Cells(7).Text = dtRow.Item("employeeunkid")
    '                    End Select
    '                End If


    '            End If

    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("gvScanAttachment_RowDataBound:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    Protected Sub gvScanAttachment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvScanAttachment.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Sohail (15 Jun 2021) -- Start
                'NMB Issue : : Entry with 'None' coming after adding attachment.
                'If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhFileName", False, True)).Text = "None" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhFileName", False, True)).Text = "&nbsp;" Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhFileName", False, True)).Text = "None" AndAlso e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhName", False, True)).Text = "&nbsp;" Then
                    'Sohail (15 Jun 2021) -- End
                    e.Row.Visible = False
                Else
                    Dim dTemp() As DataRow = Nothing
                    Dim dtRow As DataRowView = e.Row.DataItem

                    If IsDBNull(dtRow.Item("AUD")) = False AndAlso dtRow.Item("AUD").ToString = "D" Then
                        e.Row.Visible = False
                    Else

                        If CInt(dtRow.Item("employeeunkid")) > 0 Then
                            Select Case mintRefModuleId
                                Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Discipline_Module, enImg_Email_RefId.Leave_Module
                                    dTemp = dtEmployee.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
                                Case enImg_Email_RefId.Applicant_Module
                                    dTemp = dtEmployee.Select("applicantunkid = '" & CInt(dtRow.Item("employeeunkid")) & "'")
                            End Select
                        Else
                            Select Case mintRefModuleId
                                Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Leave_Module
                                    dTemp = dtEmployee.Select("employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                                Case enImg_Email_RefId.Applicant_Module
                                    dTemp = dtEmployee.Select("applicantunkid = '" & CInt(cboEmployee.SelectedValue) & "'")
                            End Select
                        End If

                        If IsNothing(dTemp) = False AndAlso dTemp.Length > 0 Then 'Gajanan [02-SEP-2019] - [IsNothing(dTemp) = False AndAlso]
                            Select Case mintRefModuleId
                                Case enImg_Email_RefId.Employee_Module
                                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhCode", False, True)).Text = dTemp(0)("employeecode").ToString
                                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhName", False, True)).Text = dTemp(0)("employeename").ToString
                                Case enImg_Email_RefId.Applicant_Module
                                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhCode", False, True)).Text = dTemp(0)("applicant_code").ToString
                                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhName", False, True)).Text = dTemp(0)("applicantname").ToString
                            End Select
                        End If


                        If CInt(dtRow.Item("documentunkid")) > 0 Then
                            cboDocument.SelectedValue = CInt(dtRow.Item("documentunkid"))
                            e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhDocument", False, True)).Text = cboDocument.SelectedItem.Text
                        Else
                            e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhDocument", False, True)).Text = ""
                        End If

                        Select Case mintRefModuleId
                            Case enImg_Email_RefId.Employee_Module, enImg_Email_RefId.Leave_Module
                                e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhemployeeunkid", False, True)).Text = dtRow.Item("employeeunkid")
                                e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhapplicantid", False, True)).Text = -1
                            Case enImg_Email_RefId.Applicant_Module
                                e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhemployeeunkid", False, True)).Text = -1
                                e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvScanAttachment, "colhapplicantid", False, True)).Text = dtRow.Item("employeeunkid")
                        End Select
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("gvScanAttachment_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region " Button's Events "
    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If IsDataValid() = False Then
                Exit Sub
            End If


            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'Dim f As New System.IO.FileInfo(docUpload.FileName)
            'If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, CInt(cboEmployee.SelectedValue)) = False Then
            '    Dim strFullPath As String = System.Configuration.ConfigurationManager.AppSettings("DocSavePath") & "/" & docUpload.FileName
            '    strFullPath = Server.MapPath("../" & strFullPath)
            '    docUpload.SaveAs(strFullPath)
            '    Call AddRow(f, strFullPath)
            'Else
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    Language.setLanguage(mstrModuleName)
            '    lblError.Text = Language.getMessage(mstrModuleName, 6, "  Selected information is already present for particular employee.  ")
            '    'Anjan [04 June 2014] -- End
            '    Exit Sub
            'End If
            'f = Nothing
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                mdtTran = Session("mdtTran")


                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName).ToLower
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                'Sohail (14 Feb 2022) -- Start
                'Enhancement :  : ZRA -Allowing only doc and pdf attachments on recruitment portal for ZRA.
                If Session("CompanyGroupName").ToString.ToUpper = "ZAMBIA REVENUE AUTHORITY" AndAlso mintRefModuleId = enImg_Email_RefId.Applicant_Job_Vacancy Then
                    If Not (mstrExtension = ".doc" OrElse mstrExtension = ".docx" OrElse mstrExtension = ".pdf") Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please select only Document or PDF file."), Me)
                        Exit Sub
                    End If
                End If
                'Sohail (14 Feb 2022) -- End

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                    Dim xRow() As DataRow = mdtTran.Select("scanattachrefid = '" & CInt(cboDocumentType.SelectedValue) & "' AND modulerefid = '" & mintRefModuleId & "' AND filename = '" & f.Name & "' AND AUD <> 'D' ")
                    If xRow.Length > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Selected information is already present for particular employee."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |16-MAY-2019| -- END

                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                'If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, CInt(cboEmployee.SelectedValue)) = False Then
                Dim mintEmployeeid As Integer = 0
                If cboEmployee.SelectedValue <> "" Then
                    mintEmployeeid = CInt(cboEmployee.SelectedValue)
                End If
                'If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, IIf(cboEmployee.SelectedValue <> "", CInt(cboEmployee.SelectedValue), 0), -1, IIf(mintRefModuleId = CInt(enImg_Email_RefId.Staff_Requisition), mintTransactionID, -1)) = False Then
                If objDocument.IsExist(mintRefModuleId, CInt(cboDocumentType.SelectedValue), f.Name, mintEmployeeid, -1, IIf(mintRefModuleId = CInt(enImg_Email_RefId.Staff_Requisition), mintTransactionID, -1)) = False Then
                    'Gajanan [02-SEP-2019] -- End
                    Call AddRow(f, f.FullName)
                Else
                    Language.setLanguage(mstrModuleName)
                    lblError.Text = Language.getMessage(mstrModuleName, 6, "  Selected information is already present for particular employee.  ")
                End If
            End If
            'SHANI (20 JUN 2015) -- End

            Call FillList()

            Call Reset_Control()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAdd_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
            If mblnIsOnAddTransaction Then
                Response.Redirect(Session("servername") & "~/Leave/wPg_LeaveFormAddEdit.aspx?" & Server.UrlEncode(clsCrypto.Encrypt(mintLeaveFormID & "|" & mintScanRefId)), False)
            
			ElseIf IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
			
            Else
                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                'Response.Redirect("~\UserHome.aspx", False)
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                'Response.Redirect("~\UserHome.aspx", False)
                Response.Redirect(mstrURLReferer, False)
                'Sohail (09 Nov 2020) -- End
                'Gajanan [02-SEP-2019] -- End
            End If
            'Pinkal (18-Jun-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim drRow() As DataRow



            'Sohail (29 Jan 2013) -- Start
            'TRA - ENHANCEMENT
            If Session("Document_Path").ToString.Trim = "" Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please set Documents Path from Aruti Configuration -> Path."), Me)
                'Anjan [04 June 2014] -- End
                Exit Try
            ElseIf mdtTran.Rows.Count <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please add atleast one document."), Me)
                'Anjan [04 June 2014] -- End
                Exit Try
            End If
            'Sohail (29 Jan 2013) -- End



            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.
            drRow = mdtTran.Select("AUD <> 'D'")
            If drRow.Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please add atleast one document."), Me)
                Exit Sub
            End If
            drRow = Nothing
            'Pinkal (18-Jun-2015) -- End


            drRow = mdtTran.Select("filename = 'None' AND valuename = '' ")
            If drRow.Length > 0 Then
                drRow(0).Delete()
                mdtTran.AcceptChanges()
            End If

            'Pinkal (18-Jun-2015) -- Start
            'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.

            If mblnIsOnAddTransaction = False Then

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                Dim mdsDoc As DataSet
                Dim mstrFolderName As String = ""
                mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                Dim strFileName As String = ""
                For Each dRow As DataRow In mdtTran.Rows
                    mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                        'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        'Pinkal (20-Nov-2018) -- End
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                        strFileName = dRow("fileuniquename")
                        Dim strServerPath As String = dRow("filepath")
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString

                        If strServerPath.Contains(strPath) Then
                            strServerPath = strServerPath.Replace(strPath, "")
                            If Strings.Left(strServerPath, 1) <> "/" Then
                                strServerPath = "~/" & strServerPath
                            Else
                                strServerPath = "~" & strServerPath

                            End If

                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If File.Exists(Server.MapPath(strServerPath)) Then

                                If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                                End If

                                File.Move(Server.MapPath(strServerPath), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))

                                dRow("filepath") = strPath
                                dRow.AcceptChanges()
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If

                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath")
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub

                        End If

                    End If
                Next
                'SHANI (16 JUL 2015) -- End 

                objDocument._Datatable = mdtTran
                objDocument.InsertUpdateDelete_Documents()

                'Sohail (29 Jan 2013) -- Start
                'TRA - ENHANCEMENT
                If objDocument._Message <> "" Then
                    DisplayMessage.DisplayMessage(objDocument._Message, Me)
                    Exit Try
                End If
                'Sohail (29 Jan 2013) -- End

                'Gajanan [02-SEP-2019] -- Start      
                'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                'Response.Redirect("~\UserHome.aspx", False)
                'Sohail (15 Jun 2021) -- Start
                'NMB Issue : : once the attachment is saved, system redirects user to home page.
                'Response.Redirect("~\UserHome.aspx", False)
                If IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                    Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                    Session.Remove("IsfromRecruitment")
                Else
                Response.Redirect("~\UserHome.aspx", False)
                End If
                'Sohail (15 Jun 2021) -- End
                'Gajanan [02-SEP-2019] -- End

            ElseIf mblnIsOnAddTransaction = True Then
                Session("ScanAttachData") = mdtTran
                Response.Redirect(Session("servername") & "~/Leave/wPg_LeaveFormAddEdit.aspx?" & Server.UrlEncode(clsCrypto.Encrypt(mintLeaveFormID & "|" & mintScanRefId)), False)
            End If

            'Pinkal (18-Jun-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Try
            Dim filelist As New List(Of String)
            For Each row As GridViewRow In gvScanAttachment.Rows
                Dim xPath As String = ""
                Dim xrow() As DataRow
                If CInt(gvScanAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                    xrow = mdtTran.Select("scanattachtranunkid = " & CInt(gvScanAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
                Else
                    xrow = mdtTran.Select("GUID = '" & gvScanAttachment.DataKeys(row.RowIndex)("GUID").ToString & "'")
                End If

                If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                    xPath = xrow(0).Item("filepath").ToString
                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                    If Strings.Left(xPath, 1) <> "/" Then
                        xPath = "~/" & xPath
                    Else
                        xPath = "~" & xPath
                    End If
                    xPath = Server.MapPath(xPath)

                ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                    xPath = xrow(0).Item("orgfilepath").ToString
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        DisplayMessage.DisplayMessage("File does not Exist...", Me)
                        Exit Sub
                    End If
                    fileInfo = Nothing
                End If
                filelist.Add(xPath)
            Next
            'Gajanan [02-SEP-2019] -- Start      
            'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
            'AddFileToZip(cboDocumentType.SelectedText.Replace(" ", "_") & "-" & cboEmployee.SelectedText.Replace(" ", "_") & ".zip", filelist)
            If mintRefModuleId = enImg_Email_RefId.Staff_Requisition Then
                AddFileToZip(cboDocumentType.SelectedText.Replace(" ", "_") & "-" & cboAssocatedValue.SelectedText.Replace(" ", "_") & ".zip", filelist)
            Else
                AddFileToZip(cboDocumentType.SelectedText.Replace(" ", "_") & "-" & cboEmployee.SelectedText.Replace(" ", "_") & ".zip", filelist)
            End If
            'Gajanan [02-SEP-2019] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]--END

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnScan.Text = Language._Object.getCaption(Me.btnScan.ID, Me.btnScan.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.lblDocType.Text = Language._Object.getCaption(Me.lblDocType.ID, Me.lblDocType.Text)
            Me.lblDocument.Text = Language._Object.getCaption(Me.lblDocument.ID, Me.lblDocument.Text)
            'S.SANDEEP |16-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
            'Me.btnRemove.Text = Language._Object.getCaption(Me.btnRemove.ID, Me.btnRemove.Text).Replace("&", "")
            'S.SANDEEP |16-MAY-2019| -- END
            Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")

            Me.lblUploadDate.Text = Language._Object.getCaption(Me.lblUploadDate.ID, Me.lblUploadDate.Text)
            'Me.lblSelection.Text = Language._Object.getCaption(Me.lblSelection.ID, Me.lblSelection.Text) 'Sohail (15 Jun 2021)

            Me.gvScanAttachment.Columns(1).HeaderText = Language._Object.getCaption(Me.gvScanAttachment.Columns(1).FooterText, Me.gvScanAttachment.Columns(1).HeaderText)
            Me.gvScanAttachment.Columns(2).HeaderText = Language._Object.getCaption(Me.gvScanAttachment.Columns(2).FooterText, Me.gvScanAttachment.Columns(2).HeaderText)
            Me.gvScanAttachment.Columns(3).HeaderText = Language._Object.getCaption(Me.gvScanAttachment.Columns(3).FooterText, Me.gvScanAttachment.Columns(3).HeaderText)
            Me.gvScanAttachment.Columns(4).HeaderText = Language._Object.getCaption(Me.gvScanAttachment.Columns(4).FooterText, Me.gvScanAttachment.Columns(4).HeaderText)
            Me.gvScanAttachment.Columns(5).HeaderText = Language._Object.getCaption(Me.gvScanAttachment.Columns(5).FooterText, Me.gvScanAttachment.Columns(5).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class
