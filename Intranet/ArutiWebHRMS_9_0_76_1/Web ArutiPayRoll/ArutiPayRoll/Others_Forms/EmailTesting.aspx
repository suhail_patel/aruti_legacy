﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="EmailTesting.aspx.vb"
    Inherits="Others_Forms_EmailTesting" Title="Email Testing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}

$("[id*=ChkAllSelectedEmp]").live("click", function () {
        var chkHeader = $(this);
        var grid = $(this).closest("table");
        $("input[type=checkbox]", grid).each(function () {
            if (chkHeader.is(":checked")) {
                debugger;
                $(this).attr("checked", "checked");
                
            } else {
                $(this).removeAttr("checked");
            }
        });
    });
    $("[id*=ChkSelectedEmp]").live("click", function () {
        var grid = $(this).closest("table");
        var chkHeader = $("[id*=chkHeader]", grid);
        var row = $(this).closest("tr")[0];
        
        debugger;
        if (!$(this).is(":checked")) {
            var row = $(this).closest("tr")[0];
            chkHeader.removeAttr("checked");
        } else {
           
            if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                chkHeader.attr("checked", "checked");
            }
        }
    });


    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Email Testing"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Email Testing"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="Label1" runat="server" Text="Mail Server IP"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtIp" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="Label2" runat="server" Text="Mail Server Port"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtPort" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="Label3" runat="server" Text="Username"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtUsername" runat="server" ReadOnly="true" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <asp:CheckBox ID="chkUseCertificate" runat="server" Text="Use Certificate Authentication"
                                            Enabled="false" />
                                    </div>
                                    <div class="row2">
                                        <asp:CheckBox ID="chkssl" runat="server" Text="Login using SSL" Enabled="false" />
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 50%; text-align: right">
                                            <asp:Label ID="Label6" runat="server" Text="Count" Width="12%"></asp:Label>
                                            <asp:Label ID="lblCount" runat="server" Text="0" Width="15%" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 50%; height: 350px; overflow: auto; padding-right: 2px;
                                            border-right: 1px solid">
                                            <asp:GridView ID="gvUserList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="userunkid">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkSelectedEmp" runat="server" />
                                                            <asp:HiddenField ID="hfFirstName" runat="server" Value='<%#Eval("firstname")%>' />
                                                            <asp:HiddenField ID="hfLastName" runat="server" Value='<%#Eval("lastname")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="User Name" DataField="Username" FooterText="dgcolhUName" />
                                                    <asp:BoundField HeaderText="Email ID" DataField="email" FooterText="dgcolhUEmail" />
                                                    
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="ib" style="width: 40%; vertical-align: top;">
                                            <asp:Label ID="Label4" runat="server" Text="Message"></asp:Label>
                                            <asp:TextBox ID="txtMsg" runat="server" TextMode="MultiLine" Rows="6"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Send Mail" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
