﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_EmployeeLeaveFormReport.aspx.vb" Inherits="Reports_Rpt_EmployeeLeaveFormReport"
    Title="Employee Leave Form Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 35%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Leave Form Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblLeaveType" runat="server" Text="Leave"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="LblLeaveForm" runat="server" Text="Leave Form"></asp:Label>
                                            </td>
                                            <td style="width: 70%">
                                                <asp:DropDownList ID="cboLeaveForm" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkIncludeDependandList" runat="server" Text="Include Depedents List" />
                                            </td>
                                        </tr>
                                      <%--    'Pinkal (02-May-2017) -- Start
                                          'Enhancement - Working on Leave Enhancement for HJF.--%>
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                            </td>
                                            <td style="width: 70%">
                                                <asp:CheckBox ID="chkDonotShowExpense" runat="server" Checked ="true" Text="Do not Show Expenses Section if no Expense is set" />
                                            </td>
                                        </tr>
                                          <%--    'Pinkal (02-May-2017) -- End.--%>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </li>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
