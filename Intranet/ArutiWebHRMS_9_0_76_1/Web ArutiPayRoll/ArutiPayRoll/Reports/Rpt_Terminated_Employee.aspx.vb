﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Terminated_Employee
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objTerminatedEmp As clsTerminatedEmpWithReason


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmTerminatedEmpWithReason"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim dsList As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmp.GetEmployeeList("Employee", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmp.GetEmployeeList("Employee", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Employee", True)

            'Shani(24-Aug-2015) -- End
            With drpEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = 0
            End With
            objEmp = Nothing

            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            With cboFilterType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Display Between Selected Date Range"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Display Start Between Selected Date Range"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Display End Between Selected Date Range"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Display Start & End Between Selected Start and End Date Range"))
                .SelectedIndex = 0
            End With

            Dim objMasterdata As New clsMasterData
            dsList = objMasterdata.GetTerminateEmployeeReportType(True, "ReportType")
            With cboReportType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("ReportType")
                .DataBind()
                .SelectedValue = 0
            End With
            dsList = Nothing
            objMasterdata = Nothing
            Call cboReportType_SelectedIndexChanged(cboReportType, New EventArgs)
            'S.SANDEEP |02-MAR-2020| -- END


            dsList = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objTerminatedEmp.SetDefaultValue()
            objTerminatedEmp._EmployeeID = CInt(drpEmployee.SelectedValue)

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objTerminatedEmp._EmployeeName = drpEmployee.Text
            objTerminatedEmp._EmployeeName = drpEmployee.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End

            objTerminatedEmp._StartDate = dtFromdate.GetDate.Date
            objTerminatedEmp._EndDate = dtTodate.GetDate.Date
            objTerminatedEmp._IncludeInactiveEmp = chkInactiveemp.Checked
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objTerminatedEmp._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End


            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL

            objTerminatedEmp._ReportType = CType(cboReportType.SelectedValue, enTerminatedEmployeeReportType)

            If CType(cboReportType.SelectedValue, enTerminatedEmployeeReportType) = enTerminatedEmployeeReportType.TerminatedEmployee Then
                objTerminatedEmp._ReportName = Language.getMessage(mstrModuleName, 3, "Terminated Employee Report")
            Else
                objTerminatedEmp._ReportName = Language.getMessage(mstrModuleName, 2, "Suspended Employee Report")
            End If

            objTerminatedEmp._ReportFilterIdx = cboFilterType.SelectedIndex
            objTerminatedEmp._ReportFilterName = cboFilterType.Text
            If pnlEndDateFilter.Enabled Then
                objTerminatedEmp._EndDateFrom = dtpEndDateFrom.GetDate
                objTerminatedEmp._EndDateTo = dtpEndDateTo.GetDate
            End If
            'S.SANDEEP |02-MAR-2020| -- END

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
    End Function

    Private Sub ResetValue()
        Try
            objTerminatedEmp.setDefaultOrderBy(0)
            drpEmployee.SelectedIndex = 0
            dtFromdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtTodate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            chkInactiveemp.Visible = False
            chkInactiveemp.Checked = False
            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            cboReportType.SelectedIndex = 0
            'S.SANDEEP |02-MAR-2020| -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objTerminatedEmp = New clsTerminatedEmpWithReason(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                dtFromdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtTodate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                chkInactiveemp.Visible = False
                'Nilay (01-Feb-2015) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objTerminatedEmp.setDefaultOrderBy(0)
            objTerminatedEmp._CompanyUnkId = Session("CompanyUnkId")
            objTerminatedEmp._UserUnkId = Session("UserId")
            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objTerminatedEmp._UserAccessFilter = Session("AccessLevelFilterString")
            'S.SANDEEP [ 12 NOV 2012 ] -- END

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objTerminatedEmp.generateReport(0, enPrintAction.None, enExportAction.None)
            objTerminatedEmp.generateReportNew(Session("Database_Name"), _
                                               Session("UserId"), _
                                               Session("Fin_year"), _
                                               Session("CompanyUnkId"), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                               Session("UserAccessModeSetting"), True, _
                                               Session("ExportReportPath"), _
                                               Session("OpenAfterExport"), _
                                               0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objTerminatedEmp._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region

    'S.SANDEEP |02-MAR-2020| -- START
    'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case CInt(cboReportType.SelectedValue)
                Case enTerminatedEmployeeReportType.SuspensionEmployee
                    cboFilterType.Enabled = True
                Case Else
                    cboFilterType.SelectedIndex = 0 : pnlEndDateFilter.Enabled = False
                    cboFilterType.Enabled = False
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboFilterType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFilterType.SelectedIndexChanged
        Try
            If cboFilterType.SelectedIndex = 3 Then
                pnlEndDateFilter.Enabled = True
            Else
                pnlEndDateFilter.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |02-MAR-2020| -- END

    Protected Sub drpEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.DataBound
        Try
        If drpEmployee.Items.Count > 0 Then
            For Each lstItem As ListItem In drpEmployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            drpEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblfromdate.Text = Language._Object.getCaption(Me.lblfromdate.ID, Me.lblfromdate.Text)
        Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.ID, Me.lblEndDate.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (06-May-2014) -- End


End Class
