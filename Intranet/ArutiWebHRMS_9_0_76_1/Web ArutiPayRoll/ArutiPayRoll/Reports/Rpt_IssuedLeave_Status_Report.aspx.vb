﻿Option Strict On 'Shani(11-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_IssuedLeave_Status_Report
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objIssuedLvForm As clsIssued_lvFormDetail_Report
    'Pinkal (11-Sep-2020) -- End




    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mstrModuleName As String = "frmIssued_LvFormDetail_Report"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise


            'If Session("LoginBy") = Global.User.en_loginby.User Then

            '    'Shani(20-Nov-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    '    dsList = objEmployee.GetList("Employee", False, True)
            '    'Else
            '    '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            '    'End If

            '    'Dim dRow As DataRow = dsList.Tables(0).NewRow
            '    'dRow("employeeunkid") = 0
            '    'dRow("name") = "Select"
            '    'dsList.Tables(0).Rows.InsertAt(dRow, 0)
            '    dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                         Session("UserId"), _
            '                                         Session("Fin_year"), _
            '                                         Session("CompanyUnkId"), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         Session("UserAccessModeSetting"), True, _
            '                                         Session("IsIncludeInactiveEmp"), "Employee", True)
            '    'Shani(20-Nov-2015) -- End
            'ElseIf Session("LoginBy") = Global.User.en_loginby.Employee Then

            '    'Shani(20-Nov-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'dsList = objEmployee.GetList("Employee", False, True, , , CInt(Session("Employeeunkid")), Session("AccessLevelFilterString"))
            '    dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                         Session("UserId"), _
            '                                         Session("Fin_year"), _
            '                                         Session("CompanyUnkId"), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                         Session("UserAccessModeSetting"), True, _
            '                                         Session("IsIncludeInactiveEmp"), "Employee", False, CInt(Session("Employeeunkid")))
            '    'Shani(20-Nov-2015) -- End

                'End If

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString, True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            'Shani(11-Feb-2016) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Employee")
                .DataBind()
            End With
            objEmployee = Nothing

            Dim objLeave As New clsleavetype_master

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", True)
            'Else
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            'End If
                dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            'Pinkal (06-Dec-2019) -- End

            'Pinkal (25-May-2019) -- End


            With cboLeave
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeave = Nothing


            cboEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpStartdate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            cboEmployee.SelectedIndex = 0
            cboLeave.SelectedIndex = 0
            cboLeaveFormNo.SelectedIndex = 0
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objIssuedLvForm As clsIssued_lvFormDetail_Report) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objIssuedLvForm.SetDefaultValue()
            'objIssuedLvForm._ReportId = CInt(cboReportType.SelectedIndex)
            'objIssuedLvForm._ReportTypeName = cboReportType.Text
            objIssuedLvForm._YearId = CInt(Session("Fin_year"))
            objIssuedLvForm._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            If Not dtpStartdate.IsNull Then
                objIssuedLvForm._FromDate = dtpStartdate.GetDate.Date
            Else
                objIssuedLvForm._FromDate = Nothing
            End If
            If Not dtpToDate.IsNull Then
                objIssuedLvForm._ToDate = dtpToDate.GetDate.Date
            Else
                objIssuedLvForm._ToDate = Nothing
            End If
            objIssuedLvForm._EmployeeID = CInt(cboEmployee.SelectedValue)
            objIssuedLvForm._EmployeeName = cboEmployee.SelectedItem.Text
            objIssuedLvForm._LeaveId = CInt(cboLeave.SelectedValue)
            objIssuedLvForm._LeaveName = cboLeave.SelectedItem.Text
            objIssuedLvForm._LeaveFormId = CInt(cboLeaveFormNo.SelectedValue)
            objIssuedLvForm._LeaveForm = cboLeaveFormNo.SelectedItem.Text
            'objIssuedLvForm._Advance_Filter = mstrAdvanceFilter
            'objIssuedLvForm._ViewByIds = mstrViewByIds
            'objIssuedLvForm._ViewIndex = mintViewIndex
            'objIssuedLvForm._ViewByName = mstrViewByName
            'objIssuedLvForm._Analysis_Fields = mstrAnalysis_Fields
            'objIssuedLvForm._Analysis_Join = mstrAnalysis_Join
            'objIssuedLvForm._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objIssuedLvForm._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GName
            'objIssuedLvForm._Report_GroupName = mstrReport_GroupName

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objIssuedLvForm._IncludeAccessFilterQry = False
            'Pinkal (06-Jan-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objIssuedLvForm = New clsIssued_lvFormDetail_Report
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                dtpStartdate.SetDate = Nothing
                dtpToDate.SetDate = Nothing
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objIssuedLvForm As New clsIssued_lvFormDetail_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try

            If CInt(cboLeave.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                cboLeave.Focus()
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objIssuedLvForm) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End


            objIssuedLvForm._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objIssuedLvForm._OpenAfterExport = False
            objIssuedLvForm._CompanyUnkId = CInt(Session("CompanyUnkId"))

            'Pinkal (01-Dec-2016) -- Start
            'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
            If CInt(Session("Employeeunkid")) > 0 Then
                objIssuedLvForm._UserName = Session("DisplayName").ToString()
            End If
            'Pinkal (01-Dec-2016) -- End

            objIssuedLvForm._UserUnkId = CInt(Session("UserId"))
            objIssuedLvForm._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            objIssuedLvForm.setDefaultOrderBy(0)


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objIssuedLvForm.Generate_IssuedLeaveStatusReport()
            objIssuedLvForm.Generate_IssuedLeaveStatusReport(Session("Database_Name").ToString(), _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             Session("EmployeeAsOnDate").ToString, _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             CBool(Session("IsIncludeInactiveEmp")), True)
            'Shani(20-Nov-2015) -- End

            If objIssuedLvForm._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objIssuedLvForm._FileNameAfterExported
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'Shani [ 18 NOV 2015 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Shani [ 18 NOV 2014 ] -- END
                'Gajanan (3 Jan 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objIssuedLvForm = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboLeave.SelectedIndexChanged
        Try
            Dim objLvForm As New clsleaveform


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            'Dim dsList As DataSet = objLvForm.getListForCombo(CInt(cboLeave.SelectedValue), "", CInt(cboEmployee.SelectedValue), True, True)
            Dim dsList As DataSet = objLvForm.getListForCurrentYearLeaveForm(CInt(Session("Fin_year")), CInt(cboLeave.SelectedValue), "7", CInt(cboEmployee.SelectedValue), True, True)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "yearunkid = 0  or yearunkid = " & CInt(Session("Fin_year")), "", DataViewRowState.CurrentRows).ToTable
            'Pinkal (24-May-2014) -- End


            With cboLeaveFormNo
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = dtTable
                .DataBind()
            End With
            objLvForm = Nothing

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.Title = Language._Object.getCaption(mstrModuleName, objIssuedLvForm._ReportName)
            'Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objIssuedLvForm._ReportName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Pinkal (11-Sep-2020) -- End


        Me.LblFromDate.Text = Language._Object.getCaption(Me.LblFromDate.ID, Me.LblFromDate.Text)
        Me.LblToDate.Text = Language._Object.getCaption(Me.LblToDate.ID, Me.LblToDate.Text)
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
        Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.ID, Me.lblLeaveName.Text)
        Me.LblLeaveFormNo.Text = Language._Object.getCaption(Me.LblLeaveFormNo.ID, Me.LblLeaveFormNo.Text)

        Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
