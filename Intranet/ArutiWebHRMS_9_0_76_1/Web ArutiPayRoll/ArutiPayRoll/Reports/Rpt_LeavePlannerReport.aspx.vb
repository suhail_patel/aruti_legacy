﻿Option Strict On 'Shani(11 Feb 2016)
#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_LeavePlannerReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objLeavePlannerRpt As clsLeavePlanReport
    'Pinkal (11-Sep-2020) -- End


    Private ReadOnly mstrModuleName As String = "frmLeavePlanReport"
#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet


            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            'If Session("LoginBy") = Global.User.en_loginby.User Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    '    dsList = objEmployee.GetList("Employee", False, True)
            '    'Else
            '    '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            '    'End If
            '    dsList = objEmployee.GetList(Session("Database_Name").ToString(), _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      Session("UserAccessModeSetting"), True, _
            '                                      Session("IsIncludeInactiveEmp"), "Employee", _
            '                                      Session("ShowFirstAppointmentDate"))
            '    'Shani(24-Aug-2015) -- End



            '    Dim dRow As DataRow = dsList.Tables(0).NewRow
            '    dRow("employeeunkid") = 0

            '    'Pinkal (06-May-2014) -- Start
            '    'Enhancement : Language Changes 
            '    Language.setLanguage(mstrModuleName)
            '    dRow("name") = Language.getMessage(mstrModuleName, 1, "Select")
            '    'Pinkal (06-May-2014) -- End

            '    dsList.Tables(0).Rows.InsertAt(dRow, 0)

            'ElseIf Session("LoginBy") = Global.User.en_loginby.Employee Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'dsList = objEmployee.GetList("Employee", False, True, , , CInt(Session("Employeeunkid")), Session("AccessLevelFilterString"))
            '    dsList = objEmployee.GetList(Session("Database_Name"), _
            '                                      Session("UserId"), _
            '                                      Session("Fin_year"), _
            '                                      Session("CompanyUnkId"), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      Session("UserAccessModeSetting"), True, _
            '                                      Session("IsIncludeInactiveEmp"), "Employee", _
            '                                      Session("ShowFirstAppointmentDate"), _
            '                                      CInt(Session("Employeeunkid")), , _
            '                                      Session("AccessLevelFilterString"))
            '    'Shani(24-Aug-2015) -- End


            'End If
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)
            'Shani(11-Feb-2016) -- End


            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing


            Dim objJob As New clsJobs
            dsList = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Job")
                .DataBind()
            End With
            objJob = Nothing

            Dim objLeave As New clsleavetype_master

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", True)
            'Else
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            'End If
                dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            'Pinkal (06-Dec-2019) -- End

            'Pinkal (25-May-2019) -- End


            With cboLeave
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeave = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            dtpLeaveFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpLeaveToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedValue = "0"
            cboLeave.SelectedValue = "0"
            cboJob.SelectedValue = "0"
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLeavePlannerRpt As clsLeavePlanReport) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtpLeaveFromDate.GetDate = Nothing OrElse dtpLeaveToDate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End

            objLeavePlannerRpt.SetDefaultValue()



            objLeavePlannerRpt._LeaveFromDate = dtpLeaveFromDate.GetDate.Date
            objLeavePlannerRpt._LeaveToDate = dtpLeaveToDate.GetDate.Date

            objLeavePlannerRpt._EmployeeId = CInt(cboEmployee.SelectedValue)
            If CInt(cboEmployee.SelectedValue) > 0 Then
                objLeavePlannerRpt._Employee = cboEmployee.SelectedItem.Text
            End If

            objLeavePlannerRpt._JobId = CInt(cboJob.SelectedValue)

            objLeavePlannerRpt._IncludeInactiveEmp = CBool(Session("IsIncludeInactiveEmp"))

            If CInt(cboJob.SelectedValue) > 0 Then
                objLeavePlannerRpt._Job = cboJob.SelectedItem.Text
            End If

            objLeavePlannerRpt._LeaveId = CInt(cboLeave.SelectedValue)
            If CInt(cboLeave.SelectedValue) > 0 Then
                objLeavePlannerRpt._Leave = cboLeave.SelectedItem.Text
            End If

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objLeavePlannerRpt._IncludeAccessFilterQry = False
            'Pinkal (06-Jan-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeavePlannerRpt = New clsLeavePlanReport
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End



            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLangauge()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                dtpLeaveFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpLeaveToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeavePlannerRpt As New clsLeavePlanReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If SetFilter() = False Then Exit Sub
            If SetFilter(objLeavePlannerRpt) = False Then Exit Sub
            'Pinkal (11-Sep-2020) -- End

            objLeavePlannerRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objLeavePlannerRpt._UserUnkId = CInt(Session("UserId"))
            objLeavePlannerRpt._UserAccessFilter = Session("AccessLevelFilterString").ToString()


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Pinkal (01-Dec-2016) -- Start
            'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
            If CInt(Session("Employeeunkid")) > 0 Then
                objLeavePlannerRpt._UserName = Session("DisplayName").ToString()
            End If
            'Pinkal (01-Dec-2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objLeavePlannerRpt.generateReport(0, enPrintAction.None, enExportAction.None)
            objLeavePlannerRpt.generateReportNew(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 Session("ExportReportPath").ToString(), _
                                                 CBool(Session("OpenAfterExport")), _
                                                 0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Shani(20-Nov-2015) -- End

            Session("objRpt") = objLeavePlannerRpt._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(11-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeavePlannerRpt = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

    Private Sub SetLangauge()

        Language.setLanguage(mstrModuleName)


        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        'Me.Title = Language._Object.getCaption(mstrModuleName, objLeavePlannerRpt._ReportName)
        'Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objLeavePlannerRpt._ReportName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Pinkal (11-Sep-2020) -- End


        Me.lblLeaveName.Text = Language._Object.getCaption(Me.lblLeaveName.ID, Me.lblLeaveName.Text)
        Me.lblLeaveTodate.Text = Language._Object.getCaption(Me.lblLeaveTodate.ID, Me.lblLeaveTodate.Text)
        Me.LblLeaveFromDate.Text = Language._Object.getCaption(Me.LblLeaveFromDate.ID, Me.LblLeaveFromDate.Text)
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
        Me.LblJob.Text = Language._Object.getCaption(Me.LblJob.ID, Me.LblJob.Text)

    End Sub

End Class
