﻿<%@ Page Language="VB" Title="Employee Schedule Report" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="Rpt_EmpScheduleReport.aspx.vb" Inherits="Reports_Rpt_EmpScheduleReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Schedule Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                <div style="text-align: right">
                                    <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table width="100%">
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="lblRType" runat="server" Text="Report Type"></asp:Label>
                                        </td>
                                        <td colspan= "3">
                                            <asp:DropDownList ID="cboReportType" runat="server" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                        </td>
                                       <td colspan= "3">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="True" />
                                        </td>
                                    </tr>
                                    
                                     <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                        </td>
                                         <td style="width: 30%">
                                            <uc2:DateCtrl Id = "dtpFromDate" runat="server" Width= "100" />
                                        </td>
                                        <td style="width: 20%">
                                            <asp:Label ID="LblToDate" runat="server" Text="To Date"></asp:Label>
                                        </td>
                                         <td style="width: 30%">
                                            <uc2:DateCtrl Id = "dtpToDate" runat="server" Width= "100"  />
                                        </td>
                                    </tr>
                                    
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="LblDepartment" runat="server" Text="Department"></asp:Label>
                                        </td>
                                        <td colspan= "3">
                                            <asp:DropDownList ID="cboDepartment" runat="server" />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="LblSection" runat="server" Text="Section"></asp:Label>
                                        </td>
                                        <td colspan= "3">
                                            <asp:DropDownList ID="cboSection" runat="server" />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                        </td>
                                        <td colspan= "3">
                                            <asp:DropDownList ID="cboEmployee" runat="server"  />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="lblShift" runat="server" Text="Shift"></asp:Label>
                                        </td>
                                        <td colspan= "3">
                                            <asp:DropDownList ID="cboShift" runat="server"  />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                            <asp:Label ID="lblPolicy" runat="server" Text="Policy"></asp:Label>
                                        </td>
                                        <td colspan= "3">
                                            <asp:DropDownList ID="cboPolicy" runat="server"  />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 20%">
                                        </td>
                                        <td colspan= "3">
                                            <asp:CheckBox ID="chkInactiveemp" Text="Include Inactive Employee" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                    <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                    <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                </div>
                            </div>
                        </div>
                        <uc1:AnalysisBy ID="popAnalysisby" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
