﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Reports_Rpt_NonDisclosureDeclareStatusRerport
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmNonDisclosureDeclareStatusRerport"
    'Private objclsAssetDeclarationT2 As clsAssetDeclarationT2
    'Dim objAssetDeclare As New clsAssetdeclaration_masterT2
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = ""
    'Dim dtFinYearList As DataTable
    'Private mdtFinStartDate As DateTime
    'Private mdtFinEndDate As DateTime
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Call SetMessages()
            Call Language._Object.SaveValue()
            'Call SetControlCaptions()

            If Not IsPostBack Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call GetControlCaptions()

                FillCombo()
                ResetValue()
            Else
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                'dtFinYearList = Me.ViewState("dtFinYearList")
                'mdtFinStartDate = Me.ViewState("mdtFinStartDate")
                'mdtFinEndDate = Me.ViewState("mdtFinEndDate")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            'Me.ViewState.Add("dtFinYearList", dtFinYearList)
            'Me.ViewState.Add("mdtFinStartDate", mdtFinStartDate)
            'Me.ViewState.Add("mdtFinEndDate", mdtFinEndDate)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objclsMasterData As New clsMasterData
        Dim dtTable As DataTable

        Try
            objEmp = New clsEmployee_Master
            Dim blnIncludeInactiveEmp As Boolean = False
            Dim dsList As DataSet = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               blnIncludeInactiveEmp, "Employee", True, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombo = objclsMasterData.getComboListAssetDeclarationStatusList("Status")
            dtTable = New DataView(dsCombo.Tables(0), " ID <> " & enAssetDeclarationStatusType.NOT_ATTEMPTED & " ", "", DataViewRowState.CurrentRows).ToTable
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable.Copy
                .DataBind()
                .SelectedValue = 0
            End With

            dsCombo = objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"))

            'dtFinYearList = dsCombo.Tables("Year").Copy

            With cboFinalcialYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dsCombo.Tables("Year")
                .DataBind()
                .SelectedValue = CInt(Session("Fin_Year"))
            End With

            'If Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").Trim.ToString <> "" Then
            '    cboFinalcialYear.SelectedValue = -99
            'End If

            'cboFinalcialYear_SelectedIndexChanged(cboFinalcialYear, New System.EventArgs)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Private Sub ResetValue()
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
            dtpAsOnDate.SetDate = ConfigParameter._Object._CurrentDateAndTime

            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            'objRptAssetDeclareStatus.setDefaultOrderBy(cboEmployee.SelectedIndex)
            chkInactiveemp.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboFinalcialYear.SelectedIndex) < 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Financial year is compulsory information.Please Select Financial year."), Me)
                cboFinalcialYear.Focus()
                Return False
            End If
            If dtpAsOnDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select As On Date."), Me)
                dtpAsOnDate.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region "Button Event"

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim objNDStatus As New ArutiReports.clsNonDisclosureDeclareStatusRerport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        Try

            If IsValidData() = False Then Exit Sub

            Dim objMaster As New clsMasterData
            Dim blnApplyAccessFilter As Boolean = True

            Dim dicDB As New Dictionary(Of String, String)

            objNDStatus._EmployeeId = cboEmployee.SelectedValue
            objNDStatus._EmployeeName = cboEmployee.SelectedItem.Text
            objNDStatus._DeclarationStatusId = cboStatus.SelectedValue
            objNDStatus._DeclarationStatusName = cboStatus.SelectedItem.Text
            objNDStatus._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objNDStatus._AsOnDate = dtpAsOnDate.GetDate.Date
            
            'objNDStatus._FinStartDate = mdtFinStartDate
            'objNDStatus._FinEndDate = mdtFinEndDate
            objNDStatus._FinancialYearId = cboFinalcialYear.SelectedValue
            objNDStatus._FinancialYearName = cboFinalcialYear.SelectedItem.Text


            objNDStatus._ViewByIds = mstrStringIds
            objNDStatus._ViewIndex = mintViewIdx
            objNDStatus._ViewByName = mstrStringName
            objNDStatus._Analysis_Fields = mstrAnalysis_Fields
            objNDStatus._Analysis_Join = mstrAnalysis_Join
            objNDStatus._Analysis_OrderBy = mstrAnalysis_OrderBy
            objNDStatus._Report_GroupName = mstrReport_GroupName
            objNDStatus._IncluderInactiveEmp = chkInactiveemp.Checked

            objNDStatus._Advance_Filter = mstrAdvanceFilter
            Dim strFilePath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            objNDStatus.Generate_DetailReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), dtpAsOnDate.GetDate(), dtpAsOnDate.GetDate(), Session("UserAccessModeSetting").ToString(), chkInactiveemp.Checked, strFilePath, False, CInt(Session("Base_CurrencyId")))

            If objNDStatus._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = strFilePath & "\" & objNDStatus._FileNameAfterExported

                Export.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSetAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetAnalysis.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (20 Jun 2020) -- Start
    'NMB Issue : # : The close button on employee confidentiality and employee non-disclosure agreement report are not working.
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (20 Jun 2020) -- End

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(lblPageHeader.ID, Me.Title)
            Language._Object.setCaption(Me.gbFilterCriteria.ID, Me.gbFilterCriteria.Text)

            Language._Object.setCaption(Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Language._Object.setCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            Language._Object.setCaption(Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblDate.ID, Me.lblDate.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnExport.ID, Me.btnExport.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, Me.Title)
            gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.ID, Me.gbFilterCriteria.Text)

            lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            lnkSetAnalysis.Text = Language._Object.getCaption(Me.lnkSetAnalysis.ID, Me.lnkSetAnalysis.Text)
            lblFinalcialYear.Text = Language._Object.getCaption(Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
            lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text)
            btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text)
            btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text)
            Me.lblFinalcialYear.Text = Language._Object.getCaption(Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select As On Date.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
