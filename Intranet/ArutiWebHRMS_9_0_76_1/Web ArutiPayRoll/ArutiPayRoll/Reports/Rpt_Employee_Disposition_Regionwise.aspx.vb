﻿#Region " Imports "

Imports System.Data
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Employee_Disposition_Regionwise
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objDisposition As clsEmployeeDisposition_Report
    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private ReadOnly mstrModuleName As String = "frmEmployeeDisposition_Report"
    'S.SANDEEP [ 19 JUN 2014 ] -- END

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objRegion As New clsClassGroup
        Dim dsList As New DataSet
        Try
            dsList = objRegion.getComboList("Region", True)
            With drpRegion
                .DataValueField = "classgroupunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("Region")
                .DataBind()
                .SelectedValue = 0
            End With
            objRegion = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            objDisposition.SetDefaultValue()

            objDisposition._ClassGroupUnkId = drpRegion.SelectedValue
            objDisposition._ClassGroupName = drpRegion.SelectedItem.Text
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objDisposition._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            drpRegion.SelectedValue = 0
            chkInactiveemp.Checked = False
            objDisposition.setDefaultOrderBy(0)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objDisposition = New clsEmployeeDisposition_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'S.SANDEEP [ 19 JUN 2014 ] -- START
            'ENHANCEMENT : LANGUAGE CHANGES
            Call SetLanguage()
            'S.SANDEEP [ 19 JUN 2014 ] -- END

            If Not IsPostBack Then
                Call FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objDisposition._CompanyUnkId = Session("CompanyUnkId")
            objDisposition._UserUnkId = Session("UserId")


            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            objDisposition._UserAccessFilter = Session("AccessLevelFilterString")
            'Pinkal (12-Nov-2012) -- End

            objDisposition.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'objDisposition.generateReport(0, enPrintAction.None, enExportAction.None)
            objDisposition.generateReportNew(Session("Database_Name"), _
                                             Session("UserId"), _
                                             Session("Fin_year"), _
                                             Session("CompanyUnkId"), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             Session("UserAccessModeSetting"), True, _
                                             Session("ExportReportPath"), _
                                             Session("OpenAfterExport"), _
                                             0, enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(15-Feb-2016) -- End

            Session("objRpt") = objDisposition._Rpt

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region


    'S.SANDEEP [ 19 JUN 2014 ] -- START
    'ENHANCEMENT : LANGUAGE CHANGES
    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End


        Me.lblRegion.Text = Language._Object.getCaption(Me.lblRegion.ID, Me.lblRegion.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)

        Me.BtnReport.Text = Language._Object.getCaption(Me.BtnReport.ID, Me.BtnReport.Text).Replace("&", "")
        Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")
        Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 19 JUN 2014 ] -- END


End Class
