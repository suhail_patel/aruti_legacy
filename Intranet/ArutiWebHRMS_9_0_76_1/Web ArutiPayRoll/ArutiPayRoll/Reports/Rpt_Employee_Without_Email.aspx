﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Employee_Without_Email.aspx.vb" Inherits="Reports_Rpt_Employee_Without_Email" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 35%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
            <div class="panel-primary">
                <div class="panel-heading">
                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Without Email Report"></asp:Label>
                </div>
                <div class="panel-body">
                    <div id="FilterCriteria" class="panel-default">
                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                            <div style="float: left;">
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </div>
                        </div>
                        <div id="FilterCriteriaBody" class="panel-body-default">
                            <table width="100%">
                                <tr style="width: 100%">
                                    <td style="width: 20%">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                    </td>
                                    <td style="width: 80%">
                                        <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td style="width: 15%">
                                    </td>
                                    <td style="width: 50%">
                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Report" Checked="false"
                                            CssClass="chkbx" />
                                    </td>
                                </tr>
                            </table>
                            <div class="btn-default">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>
            
        </asp:Panel>
    </center>
</asp:Content>
