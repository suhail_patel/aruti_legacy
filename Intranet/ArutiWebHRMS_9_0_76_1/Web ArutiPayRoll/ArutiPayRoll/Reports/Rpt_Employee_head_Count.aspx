﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Employee_head_Count.aspx.vb" Inherits="Reports_Rpt_Employee_head_Count"
    Title="Employee Head Count Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 55%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                <div class="panel-heading">
                    <asp:Label ID="lblPageHeader" runat="server" Text="Employee Head Count Report"></asp:Label>
                </div>
                <div class="panel-body">
                    <div id="FilterCriteria" class="panel-default">
                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                            <div style="float: left;">
                                <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                            </div>
                        </div>
                        <div id="FilterCriteriaBody" class="panel-body-default">
                            <table width="100%">
                                <tr style="width: 100%">
                                    <td style="width: 20%">
                                        <asp:Label ID="lblViewBy" runat="server" Text="View By"></asp:Label>
                                    </td>
                                    <td style="width: 40%">
                                        <asp:DropDownList ID="cboViewBy" runat="server" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 40%" >
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:CheckBox ID="chkShowChartDept" runat="server" Text="Show Chart" Checked="false"
                                                        CssClass="chkbx" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                        Checked="false" CssClass="chkbx" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td style="width: 100%" colspan="4">
                                        <asp:CheckBox ID="chkIncludeNAEmp" runat="server" Text="Include Employee With No Gender in Total Heads"
                                            Checked="false" CssClass="chkbx" />
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td style="width: 20%">
                                        <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>
                                    </td>
                                    <td style="width: 80%" colspan="3">
                                        <asp:DropDownList ID="drpName" AutoPostBack="true" Width="50%" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td style="width: 20%">
                                        <asp:Label ID="lblTotalFemaleFrom" runat="server" Text="Total Female From"></asp:Label>
                                    </td>
                                    <td style="width: 80%" colspan="3">
                                        <table style="width: 50%">
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="TxttotFemaleFrom" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%; text-align: center;">
                                                    <asp:Label ID="lblTotalFemaleTo" runat="server" Text="To"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="Txttotalfemaleto" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td style="width: 20%">
                                        <asp:Label ID="lblTotalMaleFrom" runat="server" Text="Total Male From"></asp:Label>
                                    </td>
                                    <td style="width: 80%" colspan="3">
                                        <table style="width: 50%">
                                            <tr>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="TxttotalMaleFrom" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%; text-align: center;">
                                                    <asp:Label ID="lblMaleTo" runat="server" Text="To"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="TxttotalMaleTo" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="width: 100%">
                                    <td style="width: 20%">
                                        <asp:Label ID="lblTotalHeadsFrom" runat="server" Text="Total Head From"></asp:Label>
                                    </td>
                                    <td style="width: 80%" colspan="3">
                                        <table style="width:50%">
                                            <tr>
                                                <td style="width:40%">
                                                    <asp:TextBox ID="TxttotalHeadFrom" runat="server"></asp:TextBox>
                                                </td>
                                                <td style="width: 20%; text-align: center;">
                                                    <asp:Label ID="lblTHTo" runat="server" Text="To"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:TextBox ID="TxttotalHeadTo" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="btn-default">
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>
            
        </asp:Panel>
    </center>
</asp:Content>
