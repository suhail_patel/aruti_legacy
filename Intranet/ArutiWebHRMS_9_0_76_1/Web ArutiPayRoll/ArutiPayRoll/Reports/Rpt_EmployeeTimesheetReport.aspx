﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_EmployeeTimesheetReport.aspx.vb"
    Inherits="Reports_Rpt_EmployeeTimesheetReport" Title="Employee Timesheet Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 60%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Time Sheet"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkSetAnalysis" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblFromDate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 15%; text-align: left">
                                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtpToDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblShift" runat="server" Text="Shift"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="cboShift" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="cboEmployee" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblAttCode" runat="server" Text="AttCode"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="cboAttCode" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="row2">
                                        <div class="ibwm" style="width: 25%">
                                        </div>
                                        <div class="ib" style="width: 40%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                        </div>
                                        <div class="ib" style="width: 30%">
                                            <asp:CheckBox ID="chkShowHoliday" runat="server" Text="Show Holiday" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ibwm" style="width: 25%">
                                        </div>
                                        <div class="ib" style="width: 40%">
                                                <asp:CheckBox ID="chkShowEachEmpOnNewPage" runat="server" Text="Show Each Employee On New Page"
                                                    Checked="true" />
                                        </div>
                                        <div class="ib" style="width: 30%">
                                            <asp:CheckBox ID="chkBreaktime" runat="server" Text="Show Break Time" />
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnlShowTiming" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 40%">
                                                <asp:CheckBox ID="chkShowTimingsIn24Hrs" runat="server" Text="Show Timing in 24 HRS Format" />
                                            </div>
                                            <div class="ib" style="width: 30%">
                                                <asp:CheckBox ID="chkShowLeaveType" runat="server" Text="Show Leave Type" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlShift" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 40%">
                                                <asp:CheckBox ID="chkShift" runat="server" Text="Show Shift" />
                                            </div>
                                            <div class="ib" style="width: 30%">
                                                <asp:CheckBox ID="chkShowBaseHrs" runat="server" Text="Show Base Hrs" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlShowTotalAbsentDays" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkShowTotalAbsentDays" runat="server" Text="Show Total Absent" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                     <%--'Pinkal (08-Aug-2019) -- Start
                                     'Enhancement [004032 - TRZ] - Working on Off days hours should not count as part of the total Base hours in Employee timesheet report.--%>
                                     <asp:Panel ID="pnlIncludeBaseHrsinTotalBaseHrs" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkIncludeOFFHrsInBaseHrs" runat="server" Checked="true" Text="Include OFF Hours in Total Base Hours" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                     <%--'Pinkal (13-Sep-2021)-- Start
                                     'Biocon Enhancement : Adding New Option to count Late Coming and Early Going in Short hrs.--%>
                                      <asp:Panel ID="pnlShowAbsentAfterEmpTerminated" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkShowAbsentAfterEmpTerminated" runat="server" Checked="True" Text="Show Absent Days On Timesheet After Employee Terminated"/>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlShowShortHrsOnBaseHrs" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkShowShortHrsOnBaseHrs" runat="server" Checked="false" Text="Show Short Hrs in Place of Base Hrs"
                                                    AutoPostBack="true" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                       <asp:Panel ID="pnlShowLateComingInShortHrs" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkShowLateComingInShortHrs" runat="server" Checked="false" Text="Show Late Coming/ Early Leaving Consider As Short Hrs" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlDisplayShortHrsWhenEmpAbsent" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkDisplayShortHrsWhenEmpAbsent" runat="server" Checked="false"
                                                    Text="Display Short Hrs When Employee is Absent" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlShowActualTimingAfterRoundOff" runat="server">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 25%">
                                            </div>
                                            <div class="ib" style="width: 70%">
                                                <asp:CheckBox ID="chkShowActualTimingAfterRoundOff" runat="server" Checked="false"
                                                    Text="Show Actual Time After Gracing Time" />
                                            </div>
                                        </div>
                                    </asp:Panel>
                                     <%--'Pinkal (13-Sep-2021) -- End--%>
                                    <div class="row2">
                                        <div class="ibwm" style="width: 25%">
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:LinkButton ID="lnkSave" runat="server" Text="Save Selection" CssClass="lnkhover"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-- 'Pinkal (08-Aug-2019) -- End--%>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <uc9:AnalysisBy ID="popAnalysisby" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
