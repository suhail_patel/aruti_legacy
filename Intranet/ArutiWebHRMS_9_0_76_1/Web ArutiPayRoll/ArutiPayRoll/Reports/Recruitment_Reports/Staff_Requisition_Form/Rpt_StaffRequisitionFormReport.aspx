﻿<%@ Page Title="Staff Requisition Form Report" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="Rpt_StaffRequisitionFormReport.aspx.vb" Inherits="Reports_Recruitment_Reports_Staff_Requisition_Form_Rpt_StaffRequisitionFormReport" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc6" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition Form Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default " style="position: relative;">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 33%">
                                                <asp:Label ID="lblRequisitiontype" runat="server" Text="Requisition Type"></asp:Label>
                                                <asp:DropDownList ID="cboRequisitionType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                                <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 33%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 66%; vertical-align: top;" colspan="2">
                                                <table width="100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 50%">
                                                            <asp:Label ID="lblWorkStartDateFrom" runat="server" Text="Work Date From"></asp:Label><br />
                                                            <uc2:DateCtrl ID="dtpWStartDateFrom" runat="server" AutoPostBack="false" />
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:Label ID="lblWorkStartDateTo" runat="server" Text="To"></asp:Label><br />
                                                            <uc2:DateCtrl ID="dtpWStartDateTo" runat="server" AutoPostBack="false" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 50%">
                                                            <asp:Label ID="lblPositionFrom" runat="server" Text="Position From"></asp:Label>
                                                            <uc6:NumericText ID="nudPositionFrom" runat="server" Max="100" Min="0" />
                                                        </td>
                                                        <td style="width: 50%">
                                                            <asp:Label ID="lblPositionTo" runat="server" Text="To"></asp:Label>
                                                            <uc6:NumericText ID="nudPositionTo" runat="server" Max="100" Min="0" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="2">
                                                            <input type="text" id="txtSearchFormNo" name="txtSearchFormNo" placeholder="Search Form No."
                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchFormNo', '<%= dgvFormNo.ClientID %>');" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%" colspan="2">
                                                            <div class="table-responsive" style="max-height: 170px; overflow: auto;">
                                                                <asp:GridView ID="dgvFormNo" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                    AllowPaging="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                    RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" RowStyle-Wrap="false"
                                                                    DataKeyNames="staffrequisitiontranunkid, formno">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="objchkFormNo" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'objdgcolhFCheck');" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="objdgcolhFCheck" runat="server" Text=" " CssClass="chk-sm" Checked='<%# Eval("IsCheck")%>'/>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="FormNoWithJobTitle" HeaderText="Staff Requisition Form No"
                                                                            ReadOnly="true" FooterText="dgcolhFormNo" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 33%; vertical-align: top;">
                                                <table width="100%">
                                                    <td style="width: 33%">
                                                        <asp:Label ID="lblRequisitionBy" runat="server" Text="Requisition By"></asp:Label>
                                                        <asp:DropDownList ID="cboRequistionBy" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <input type="text" id="txtSearchAllocation" name="txtSearchAllocation" placeholder="Search Allocation."
                                                                maxlength="50" class="form-control" onkeyup="FromSearching('txtSearchAllocation', '<%= dgvAllocation.ClientID %>');" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div class="table-responsive" style="max-height: 215px; overflow: auto;">
                                                                <asp:GridView ID="dgvAllocation" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                                    AllowPaging="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                    RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" RowStyle-Wrap="false"
                                                                    DataKeyNames="id, name">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="objchkAllocation" runat="server" Text=" " CssClass="chk-sm" onclick="CheckAll(this, 'objdgcolhACheck');" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="objdgcolhACheck" runat="server" Text=" " CssClass="chk-sm" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="name" HeaderText=" " ReadOnly="true" FooterText="objdgcolhAllocation" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                        <div class="btn-default">
                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                            <asp:Button ID="btnReport" runat="server" Text="Report" CssClass="btndefault" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                        </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script type="text/javascript">

        function CheckAll(chkbox, chkgvSelect) {
            var grid = $(chkbox).closest(".table-responsive");
            $("[id*=" + chkgvSelect + "]", grid).filter(function() { return $(this).closest('tr').css("display") != "none" }).prop("checked", $(chkbox).prop("checked"));
        }

        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };
        
        function FromSearching(txtID, gridClientID) {
            if (gridClientID.toString().includes('%') == true)
                gridClientID = gridClientID.toString().replace('%', '').replace('>', '').replace('<', '').replace('>', '').replace('.ClientID', '').replace('%', '').trim();

            if ($('#' + txtID).val().length > 0) {
                $('#' + $$(gridClientID).get(0).id + ' tbody tr').hide();
                $('#' + $$(gridClientID).get(0).id + ' tbody tr:first').show();
               
                  $('#' + $$(gridClientID).get(0).id + ' tbody tr td:containsNoCase(\'' + $('#' + txtID).val() + '\')').parent().show();

            }
            else if ($('#' + txtID).val().length == 0) {
                resetFromSearchValue(txtID, gridClientID,);
            }
            if ($('#' + $$(gridClientID).get(0).id + ' tbody tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue(txtID, gridClientID);
            }
        }

        function resetFromSearchValue(txtID, gridClientID) {


            $('#' + txtID).val('');
              $('#' + $$(gridClientID).get(0).id + ' tbody tr').show();
              $('.norecords').remove();            
            $('#' + txtID).focus();
        }
       
    </script>

</asp:Content>
