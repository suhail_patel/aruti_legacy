﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_EmployeeRelation.aspx.vb" Inherits="Reports_Rpt_EmployeeRelation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Relation Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportType" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="drpReportType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblAsonDate" runat="server" Text="As on Date"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <uc2:DateCtrl ID="dtpAsonDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="LblCondition" runat="server" Text="Condition"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboCondition" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblRelation" runat="server" Text="Relation"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="drpRelation" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployeeCode" runat="server" Text="Code"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:TextBox ID="TxtCode" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <asp:Panel ID="pnlname" runat="server">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                                                </td>
                                                <td style="width: 80%">
                                                    <asp:TextBox ID="TxtName" runat="server"></asp:TextBox>
                                                </td>
                                            </asp:Panel>
                                        </tr>
                                        <tr>
                                            <asp:Panel ID="pnlfname" runat="server">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblFirstname" runat="server" Text="First Name"></asp:Label>
                                                </td>
                                                <td style="width: 80%">
                                                    <asp:TextBox ID="TxtFirstName" runat="server"></asp:TextBox>
                                                </td>
                                            </asp:Panel>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblLastname" runat="server" Text="Last Name"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:TextBox ID="TxtLastName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                    Checked="false" CssClass="chkbx"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:CheckBox ID="chkshowdependantswithoutphotos" runat="server" Text="Show Dependants Without Photos/Picture"
                                                    Checked="false" CssClass="chkbx"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:CheckBox ID="chkShowExemptedDependents" runat="server" Text="Show Only Exempted Dependents"
                                                    AutoPostBack="true" Checked="false" CssClass="chkbx"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%">
                                                <asp:CheckBox ID="chkShowSubGrandTotal" runat="server" Text="Show Sub And Grand Total Relation Wise"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="Div3" class="panel-default">
                                <div id="Div1" class="panel-heading-default">
                                    <div style="float: left">
                                        <asp:Label ID="lnInActiveDependentOption" runat="server" Text="InActive Dependent Options"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%">
                                                <asp:CheckBox ID="chkIncludeInactiveDependents" runat="server" AutoPostBack="true"
                                                    Checked="false" CssClass="chkbx" Text="Include Inactive Dependents" />
                                            </td>
                                            <td style="width: 50%">
                                                <asp:CheckBox ID="chkShowInactiveDependents" runat="server" AutoPostBack="true" Checked="false"
                                                    CssClass="chkbx" Text="Show Inactive Dependent Only" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="width: 98%; text-align:left; height: 50px; border:1px solid #ddd;">
                                        <asp:RadioButtonList ID="rdInActiveOption" runat="server" RepeatDirection="Vertical"
                                            AutoPostBack="true">
                                            <asp:ListItem Text="Age Limit Wise" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Dependant Exception Wise" Value="2" Selected="false"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
