﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Employee_Analysis.aspx.vb" Inherits="Reports_Rpt_Employee_Analysis" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Age Analysis Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 5%">
                                                <asp:Label ID="lblDateAsOn" runat="server" Text="As on Date"></asp:Label>
                                            </td>
                                            <td style="width: 50%">
                                                <uc2:DateCtrl ID="dtasondate" runat="server"  AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 5%">
                                                <asp:Label ID="lblEmployeeCode" runat="server" Text="Code"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TxtCode" runat="server" Width="25%" AutoPostBack="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 5%">
                                                <asp:DropDownList ID="drpemployee" Width="60%" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width:5%"></td>
                                            <td style="width: 95%" colspan="2">
                                                <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width:5%"></td>
                                            <td style="width: 95%" colspan="2">
                                                <asp:CheckBox ID="chkReportType" runat="server" Text="Generate on Appointment date"
                                                    Checked="false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width:5%"></td>
                                            <td style="width: 90%" colspan="2">
                                                <asp:CheckBox ID="chkSlabWiseReport" runat="server" Text="Slab Wise Report" Checked="true"
                                                    CssClass="chkbx" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblFromYear" runat="server" Text="From Year"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="TxtFromYear" runat="server" Enabled="false" Style="text-align: right;
                                                    background-color: White"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="nudFromYear" runat="server" Width="60" Minimum="0"
                                                    Maximum="60" TargetControlID="TxtFromYear">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                            <td style="width: 15%;">
                                                <asp:Label ID="lblToYear" runat="server" Text="To Year"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="TxtToYear" runat="server" Enabled="false" Style="text-align: right;
                                                    background-color: White" Width="100px"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="nudToYear" runat="server" Width="60" Minimum="0" Maximum="60"
                                                    TargetControlID="TxtToYear">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                            <td style="width: 15%; text-align: left">
                                                <asp:Label ID="lblYearGap" runat="server" Text="Year Gap"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="TxtYearGap" runat="server" Enabled="false" Style="text-align: right;
                                                    background-color: White"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="nudYearGap" runat="server" Width="60" Minimum="1"
                                                    Maximum="50" TargetControlID="TxtYearGap">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblCondition" runat="server" Text="Condition"></asp:Label>
                                            </td>
                                            <td style="width: 25px" colspan="2">
                                                <asp:DropDownList ID="cboCondition" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblYear" style="margin-left:20%" runat="server" Text="Year"></asp:Label>
                                            </td>
                                            <td style="width: 25%" colspan="2">
                                                <asp:TextBox ID="txtYear" runat="server" Enabled="false" Style="text-align: right;
                                                    background-color: White"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="nudYear" runat="server" Width="60" Minimum="0" Maximum="100"
                                                    TargetControlID="txtYear">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                        </tr>
                                        <%--<tr style="width: 100%">
                                    <td style="width: 100%" colspan="6">--%>
                                        <%--</td>
                                </tr>--%>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
