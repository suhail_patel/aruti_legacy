﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_Job_Employment_History.aspx.vb"  Inherits="Reports_Rpt_Job_Employment_History" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register src="~/Controls/Closebutton.ascx" tagname="Closebutton" tagprefix="uc1" %>
<%@ Register src="~/Controls/DateCtrl.ascx" tagname="DateCtrl" tagprefix="uc2" %>
<%@ Register src="~/Controls/EmployeeList.ascx" tagname="EmployeeList" tagprefix="uc4" %>
<%@ Register src="~/Controls/GetComboList.ascx" tagname="DropDownList" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
 <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

<script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
 </script> 

<center>
    <asp:Panel ID="Panel1" runat="server" Style="width: 45%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Job Employement History Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID = "lblEmployee" 
                                                        runat="server" Text = "Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="True" ></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID = "lblEmployer" 
                                                            runat="server" Text = "Employer"></asp:Label>
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                                <asp:TextBox ID="txtEmployer" runat="server" AutoPostBack="True"></asp:TextBox >
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID = "lblFromDate" 
                                                       runat="server" Text = "Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl  ID="dtStartDate" runat="server"  AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                            <td style="width: 15%; text-align: left">
                                                <asp:Label ID = "lblToStartDate" style="margin-left:10px"
                                                    runat="server" Text = "To"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtStartDateTo" runat="server"  AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID = "lblEndDate" 
                                                        runat="server" Text = "End Date"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl  ID="dtEndFrom" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                            <td style="width: 15%; text-align: left">
                                                <asp:Label ID = "lblToEndDate" style="margin-left:10px"
                                                     runat="server" Text = "To"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc2:DateCtrl ID="dtEndDateTo" runat="server" AutoPostBack="false"></uc2:DateCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width:100%">
                                            <td style="width:25%">
                                                <asp:Label ID = "lblJob" runat="server" Text = "Empl.Job"></asp:Label>
                                            </td>
                                            <td style="width:55%" colspan="3">
                                                <asp:TextBox ID="txtEmplJob" runat="server"  ></asp:TextBox > 
                                            </td>
                                        </tr>
                                        <tr style="width:100%">
                                            <td style="width:25%">
                                            </td>
                                            <td style="width:55%" colspan="3">
                                                <asp:CheckBox ID = "chkInactiveemp" runat="server"  
                                                    Text = "Include Inactive Employee" Checked = "false" CssClass="chkbx" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                
                                            </td>
                                            <td style="width: 75%" colspan="3">
                                               <asp:CheckBox ID = "chkShowEmpScale" runat="server"  Text = "Show Employee Scale" Checked = "false" CssClass="chkbx" /> 
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                       <asp:Button ID = "BtnReset" runat ="server" Text = "Reset"  CssClass="btndefault" />
                            <asp:Button ID = "BtnReport" runat ="server" Text = "Report"  CssClass="btndefault" />
                            <asp:Button ID = "BtnClose" runat ="server" Text = "Close"  CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</center>
         
         
</asp:Content>


                        
                      
                        
                      
                      
                          
                            
                         