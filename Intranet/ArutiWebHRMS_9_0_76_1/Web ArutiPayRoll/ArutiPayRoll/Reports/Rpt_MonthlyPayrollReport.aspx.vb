﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_MonthlyPayrollReport
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmMonthlyPayrollReport"
    Private objMPayrollReport As clsMonthlyPayrollReport
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrSavedHeadTypeIds As String = String.Empty
    Private mstrSavedLeaveTypeIds As String = String.Empty
    Private mintFirstOpenPeriod As Integer = 0
    Private mstrAdvanceFilter As String = String.Empty
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objperiod As New clscommom_period_Tran
        Dim objMembership As New clsmembership_master
        Dim objCMaster As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), _
                                            True, True, "Emp", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Emp")
                .SelectedValue = 0
                .DataBind()
            End With

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, Session("Fin_year"), 1)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), Session("Database_Name"), CDate(Session("fin_startdate")), "Period", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = mintFirstOpenPeriod
                .DataBind()
            End With

            dsList = objMembership.getListForCombo("List", True)
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TERMINATION, True, "List")
            With cboExitReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
            With cboCatReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))                                '0'
                .Items.Add(Language.getMessage(mstrModuleName, 2, "New/Re-hired Staff"))                    '1'
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Exit Staff"))                            '2'
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Staff On Leave"))                        '3'
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Staff Promotion & Recategorization"))    '4'
                .SelectedIndex = 0
                Call cboReportType_SelectedIndexChanged(New Object(), New EventArgs())
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing : objperiod = Nothing : objMembership = Nothing
            objCMaster = Nothing : objMaster = Nothing : dsList.Dispose()
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If cboReportType.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Please select at-least one report type to continue."), Me)
                cboReportType.Focus()
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Please select period to export report."), Me)
                cboPeriod.Focus()
                Return False
            End If

            objMPayrollReport._ReportTypeId = cboReportType.SelectedIndex
            objMPayrollReport._ReportTypeName = cboReportType.SelectedItem.Text
            objMPayrollReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objMPayrollReport._PeriodName = cboPeriod.SelectedItem.Text
            Dim oPrd As New clscommom_period_Tran
            oPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            objMPayrollReport._StartDate = oPrd._Start_Date
            objMPayrollReport._EndDate = oPrd._End_Date
            oPrd = Nothing
            objMPayrollReport._EmployeeId = CInt(cboEmployee.SelectedValue)
            objMPayrollReport._EmployeeName = cboEmployee.SelectedItem.Text
            objMPayrollReport._MembershipId = CInt(cboMembership.SelectedValue)
            objMPayrollReport._MembershipName = cboMembership.SelectedItem.Text
            objMPayrollReport._ExitReasonId = CInt(cboExitReason.SelectedValue)
            objMPayrollReport._ExitReasonName = cboExitReason.SelectedItem.Text
            objMPayrollReport._CheckedLeaveTypeIds = lvLeaveType.Items.Cast(Of ListItem).Where(Function(x) x.Selected = True).ToDictionary(Function(y) CInt(y.Value), Function(z) z.Text) 'lvLeaveType.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(y) y.SubItems(colhLName.Index).Text)
            objMPayrollReport._CheckedTransHeadIds = lvCustomTranHead.Items.Cast(Of ListItem).Where(Function(x) x.Selected = True).ToDictionary(Function(y) CInt(y.Value), Function(z) z.Text) 'lvCustomTranHead.CheckedItems.Cast(Of ListViewItem).ToDictionary(Function(x) CInt(x.Tag), Function(y) y.SubItems(colhCName.Index).Text)
            objMPayrollReport._ViewByIds = mstrStringIds
            objMPayrollReport._ViewIndex = mintViewIdx
            objMPayrollReport._ViewByName = mstrStringName
            objMPayrollReport._Analysis_Fields = mstrAnalysis_Fields
            objMPayrollReport._Analysis_Join = mstrAnalysis_Join
            objMPayrollReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMPayrollReport._Report_GroupName = mstrReport_GroupName
            objMPayrollReport._AdvanceFilter = mstrAdvanceFilter
            objMPayrollReport._SkipAbsentFromTnA = chkSkipAbsentTnA.Checked
            objMPayrollReport._IncludeLeavePendingApprovedForms = chkIncludePendingApprovedforms.Checked
            objMPayrollReport._IncludeSystemRetirementMonthlyPayrollReport = chkincludesystemretirement.Checked
            objMPayrollReport._CategorizationReasonId = CInt(cboCatReason.SelectedValue)
            objMPayrollReport._CategorizationReasonName = cboCatReason.SelectedItem.Text

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboExitReason.SelectedValue = 0
            cboMembership.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            mstrAdvanceFilter = ""
            chkSkipAbsentTnA.Checked = Session("SkipAbsentFromTnAMonthlyPayrollReport")
            chkIncludePendingApprovedforms.Checked = Session("IncludePendingApproverFormsInMonthlyPayrollReport")
            chkincludesystemretirement.Checked = Session("IncludeSystemRetirementMonthlyPayrollReport")
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillHeadList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            lvCustomTranHead.Items.Clear()

            dsList = objTranHead.getComboList(Session("Database_Name"), "Heads", False, enTranHeadType.EarningForEmployees)

            lvCustomTranHead.DataSource = dsList.Tables("Heads")
            lvCustomTranHead.DataTextField = "name"
            lvCustomTranHead.DataValueField = "tranheadunkid"
            lvCustomTranHead.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "FillHeadList", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillLeaveList()
        Try
            Dim objLeaveType As New clsleavetype_master

            Dim dsList As DataSet = objLeaveType.getListForCombo("Leave")

            lvLeaveType.DataSource = dsList.Tables("Leave")
            lvLeaveType.DataTextField = "name"
            lvLeaveType.DataValueField = "leavetypeunkid"
            lvLeaveType.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objMPayrollReport = New clsMonthlyPayrollReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            SetLanguage()
            If Not IsPostBack Then
                Call FillCombo()
                Call FillHeadList()
                Call FillLeaveList()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objMPayrollReport.Export_MonthlyPayroll_Report(Session("Database_Name"), _
                                                           Session("UserId"), _
                                                           Session("Fin_year"), _
                                                           Session("CompanyUnkId"), _
                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                           Session("UserAccessModeSetting"), True, Session("OpenAfterExport"), _
                                                           IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), Session("fmtCurrency"))

            If objMPayrollReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMPayrollReport._FileNameAfterExported

                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End


            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnExport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            Select Case cboReportType.SelectedIndex
                Case 0  'SELECT
                    Call ResetValue()
                    lvCustomTranHead.Enabled = False : lvLeaveType.Enabled = False
                    cboPeriod.Enabled = False : cboEmployee.Enabled = False : cboMembership.Enabled = False : cboExitReason.Enabled = False
                    cboCatReason.Enabled = False

                    chkIncludePendingApprovedforms.Checked = False : chkIncludePendingApprovedforms.Enabled = False
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = False : chkSkipAbsentTnA.Enabled = False

                Case 1  'NEW STAFF
                    lvCustomTranHead.Enabled = True : lvLeaveType.Enabled = False : cboPeriod.Enabled = True : cboEmployee.Enabled = True : cboMembership.Enabled = True : cboExitReason.Enabled = True
                    cboExitReason.Enabled = False
                    chkIncludePendingApprovedforms.Checked = False : chkIncludePendingApprovedforms.Enabled = False
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = False : chkSkipAbsentTnA.Enabled = False
                    cboCatReason.Enabled = False

                Case 2  'EXIT STAFF
                    lvCustomTranHead.Enabled = True : lvLeaveType.Enabled = False : cboPeriod.Enabled = True : cboEmployee.Enabled = True : cboMembership.Enabled = True : cboExitReason.Enabled = True
                    cboMembership.Enabled = False
                    cboExitReason.Enabled = True
                    chkIncludePendingApprovedforms.Checked = False
                    chkincludesystemretirement.Checked = Session("IncludeSystemRetirementMonthlyPayrollReport") : chkincludesystemretirement.Enabled = True
                    chkSkipAbsentTnA.Checked = False : chkSkipAbsentTnA.Enabled = False
                    cboCatReason.Enabled = False

                Case 3  'STAFF ON LEAVE
                    lvCustomTranHead.Enabled = True : lvLeaveType.Enabled = True : cboPeriod.Enabled = True : cboEmployee.Enabled = True : cboMembership.Enabled = True : cboExitReason.Enabled = True
                    cboMembership.Enabled = False
                    cboExitReason.Enabled = False

                    chkIncludePendingApprovedforms.Checked = Session("IncludePendingApproverFormsInMonthlyPayrollReport") : chkIncludePendingApprovedforms.Enabled = True
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = Session("SkipAbsentFromTnAMonthlyPayrollReport") : chkSkipAbsentTnA.Enabled = True
                    cboCatReason.Enabled = False

                Case 4  'STAFF PROMOTION & RECATEGORIZATION
                    lvCustomTranHead.Enabled = True : lvLeaveType.Enabled = False : cboPeriod.Enabled = True : cboEmployee.Enabled = True : cboMembership.Enabled = True : cboExitReason.Enabled = True
                    cboMembership.Enabled = False
                    cboExitReason.Enabled = False

                    chkIncludePendingApprovedforms.Checked = False : chkIncludePendingApprovedforms.Enabled = False
                    chkincludesystemretirement.Checked = False : chkincludesystemretirement.Enabled = False
                    chkSkipAbsentTnA.Checked = False : chkSkipAbsentTnA.Enabled = False
                    cboCatReason.Enabled = True
            End Select
            Call FillHeadList() : If lvLeaveType.Enabled = True Then Call FillLeaveList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " CheckboxList Events "

    Protected Sub lvLeaveType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvLeaveType.DataBound
        Try
            mstrSavedLeaveTypeIds = Session("CheckedMonthlyPayrollReportLeaveIds")
            If mstrSavedLeaveTypeIds.Trim.Length > 0 Then
                For Each iValue As String In mstrSavedLeaveTypeIds.Split(",")
                    Dim iVal As String = iValue
                    If lvLeaveType.Items.Cast(Of ListItem).Where(Function(x) x.Value = iVal).Count > 0 Then
                        Dim item As ListItem = lvLeaveType.Items.Cast(Of ListItem).Where(Function(x) x.Value = iVal).First()
                        If item IsNot Nothing Then
                            item.Selected = True
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvCustomTranHead_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvCustomTranHead.DataBound
        Try
            Select Case cboReportType.SelectedIndex
                Case 1
                    mstrSavedHeadTypeIds = Session("CheckedMonthlyPayrollReport1HeadIds")
                Case 2
                    mstrSavedHeadTypeIds = Session("CheckedMonthlyPayrollReport2HeadIds")
                Case 3
                    mstrSavedHeadTypeIds = Session("CheckedMonthlyPayrollReport3HeadIds")
                Case 4
                    mstrSavedHeadTypeIds = Session("CheckedMonthlyPayrollReport4HeadIds")
            End Select
            If mstrSavedHeadTypeIds.Trim.Length > 0 Then
                For Each iValue As String In mstrSavedHeadTypeIds.Split(",")
                    Dim iVal As String = iValue
                    If lvCustomTranHead.Items.Cast(Of ListItem).Where(Function(x) x.Value = iVal).Count > 0 Then
                        Dim item As ListItem = lvCustomTranHead.Items.Cast(Of ListItem).Where(Function(x) x.Value = iVal).First()
                        If item IsNot Nothing Then
                            item.Selected = True
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkSaveLeaveSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSaveLeaveSelection.Click
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Session("Companyunkid")
            Dim xCheckedItems As String = String.Empty
            If lvLeaveType.Items.Cast(Of ListItem).Where(Function(x) x.Selected = True).Count > 0 Then
                xCheckedItems = String.Join(",", lvLeaveType.Items.Cast(Of ListItem).Where(Function(x) x.Selected = True).Select(Function(y) y.Value).ToArray)
                objConfig._CheckedMonthlyPayrollReportLeaveIds = xCheckedItems
            End If
            objConfig._SkipAbsentFromTnAMonthlyPayrollReport = chkSkipAbsentTnA.Checked
            objConfig._IncludePendingApproverFormsInMonthlyPayrollReport = chkIncludePendingApprovedforms.Checked
            objConfig.updateParam()
            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Settings saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkSaveHeadSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSaveHeadSelection.Click
        Try
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = Session("Companyunkid")
            Dim xCheckedItems As String = String.Empty
            If lvCustomTranHead.Items.Cast(Of ListItem).Where(Function(x) x.Selected = True).Count > 0 Then
                xCheckedItems = String.Join(",", lvCustomTranHead.Items.Cast(Of ListItem).Where(Function(x) x.Selected = True).Select(Function(y) y.Value).ToArray)
                Select Case cboReportType.SelectedIndex
                    Case 1
                        objConfig._CheckedMonthlyPayrollReport1HeadIds = xCheckedItems
                    Case 2
                        objConfig._CheckedMonthlyPayrollReport2HeadIds = xCheckedItems
                    Case 3
                        objConfig._CheckedMonthlyPayrollReport3HeadIds = xCheckedItems
                    Case 4
                        objConfig._CheckedMonthlyPayrollReport4HeadIds = xCheckedItems
                End Select
            End If
            objConfig._IncludeSystemRetirementMonthlyPayrollReport = chkincludesystemretirement.Checked
            objConfig.updateParam()
            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Settings saved successfully."), Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Language(s) "

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text)
            Me.BtnExport.Text = Language._Object.getCaption(Me.BtnExport.ID, Me.BtnExport.Text)
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text)
            Me.lblMembership.Text = Language._Object.getCaption(Me.lblMembership.ID, Me.lblMembership.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.ID, Me.lblReportType.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblStaffExitReason.Text = Language._Object.getCaption(Me.lblStaffExitReason.ID, Me.lblStaffExitReason.Text)
            Me.lnkSaveHeadSelection.Text = Language._Object.getCaption(Me.lnkSaveHeadSelection.ID, Me.lnkSaveHeadSelection.Text)
            Me.lnkSaveLeaveSelection.Text = Language._Object.getCaption(Me.lnkSaveLeaveSelection.ID, Me.lnkSaveLeaveSelection.Text)
            Me.chkSkipAbsentTnA.Text = Language._Object.getCaption(Me.chkSkipAbsentTnA.ID, Me.chkSkipAbsentTnA.Text)
            Me.chkIncludePendingApprovedforms.Text = Language._Object.getCaption(Me.chkIncludePendingApprovedforms.ID, Me.chkIncludePendingApprovedforms.Text)
            Me.chkincludesystemretirement.Text = Language._Object.getCaption(Me.chkincludesystemretirement.ID, Me.chkincludesystemretirement.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
