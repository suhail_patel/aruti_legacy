﻿#Region " Imports "

Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Text
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
#End Region

Partial Class Reports_Rpt_Employee_head_Count
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private objHeadCount As clsEmployeeHeadCount


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeHeadCount"
    'Pinkal (06-May-2014) -- End


#End Region

#Region "Public Functions"

    Public Sub FillCombo()
        Try
            'S.SANDEEP [ 20 DEC 2013 ] -- START
            Dim dList As New DataSet
            dList = objHeadCount.GetDisplayBy
            With cboViewBy
                .DataValueField = "id"
                .DataTextField = "iName"
                .DataSource = dList.Tables(0)
                .DataBind()
                .SelectedValue = 1
                Call cboViewBy_SelectedIndexChanged(New Object, New EventArgs)
            End With
            'If rdAllocation.SelectedIndex = 0 Then
            '    LblName.Text = "Branch"
            '    drpName.DataSource = (New clsStation).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "stationunkid"
            '    drpName.DataTextField = "name"

            'ElseIf rdAllocation.SelectedIndex = 1 Then
            '    LblName.Text = "Unit"
            '    drpName.DataSource = (New clsUnits).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "unitunkid"
            '    drpName.DataTextField = "name"

            'ElseIf rdAllocation.SelectedIndex = 2 Then
            '    LblName.Text = "Department"
            '    drpName.DataSource = (New clsDepartment).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "departmentunkid"
            '    drpName.DataTextField = "name"

            'ElseIf rdAllocation.SelectedIndex = 3 Then
            '    LblName.Text = "Job"
            '    drpName.DataSource = (New clsJobs).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "jobunkid"
            '    drpName.DataTextField = "name"

            'ElseIf rdAllocation.SelectedIndex = 4 Then
            '    LblName.Text = "Grade"
            '    drpName.DataSource = (New clsGrade).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "gradeunkid"
            '    drpName.DataTextField = "name"

            'ElseIf rdAllocation.SelectedIndex = 5 Then
            '    LblName.Text = "Section"
            '    drpName.DataSource = (New clsSections).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "sectionunkid"
            '    drpName.DataTextField = "name"

            'ElseIf rdAllocation.SelectedIndex = 6 Then
            '    LblName.Text = "Class"
            '    drpName.DataSource = (New clsClass).getComboList(, True).Tables(0)
            '    drpName.DataValueField = "classesunkid"
            '    drpName.DataTextField = "name"
            'End If
            'drpName.DataBind()
            'S.SANDEEP [ 20 DEC 2013 ] -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'rdAllocation.SelectedIndex = 2
            cboViewBy.SelectedValue = 1
            'S.SANDEEP [ 20 DEC 2013 ] -- END
            drpName.SelectedValue = 0
            TxttotFemaleFrom.Text = ""
            Txttotalfemaleto.Text = ""
            TxttotalMaleFrom.Text = ""
            TxttotalMaleTo.Text = ""
            TxttotalHeadFrom.Text = ""
            TxttotalHeadTo.Text = ""
            chkInactiveemp.Checked = False
            chkShowChartDept.Checked = False
            'S.SANDEEP [ 29 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            chkIncludeNAEmp.Checked = False
            'S.SANDEEP [ 29 JAN 2013 ] -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objHeadCount.SetDefaultValue()

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'If rdAllocation.SelectedIndex = 0 Then
            '    objHeadCount._Employee = True

            'ElseIf rdAllocation.SelectedIndex = 1 Then
            '    objHeadCount._CostCenter = True

            'ElseIf rdAllocation.SelectedIndex = 2 Then
            '    objHeadCount._Department = True

            'ElseIf rdAllocation.SelectedIndex = 3 Then
            '    objHeadCount._Job = True

            'ElseIf rdAllocation.SelectedIndex = 4 Then
            '    objHeadCount._Grade = True

            'ElseIf rdAllocation.SelectedIndex = 5 Then
            '    objHeadCount._Section = True

            'ElseIf rdAllocation.SelectedIndex = 6 Then
            '    objHeadCount._Class = True
            'End If
            'objHeadCount._Id = drpName.SelectedValue

            objHeadCount._Id = drpName.SelectedValue

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            ''objHeadCount._Name = drpName.SelectedText
            ''objHeadCount._Report_Type_Name = cboViewBy.SelectedText
            objHeadCount._Name = drpName.SelectedItem.Text
            objHeadCount._Report_Type_Name = cboViewBy.SelectedItem.Text
            'Nilay (01-Feb-2015) -- End

            objHeadCount._DisplayViewIdx = cboViewBy.SelectedValue
            'S.SANDEEP [ 20 DEC 2013 ] -- END



            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objHeadCount._Name = drpName.Text
            objHeadCount._Name = drpName.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End

            'S.SANDEEP [ 20 DEC 2013 ] -- START
            'objHeadCount._Report_Type_Name = rdAllocation.SelectedValue
            'S.SANDEEP [ 20 DEC 2013 ] -- END

            objHeadCount._FemaleFrom = IIf(TxttotFemaleFrom.Text.Trim.Length > 0, TxttotFemaleFrom.Text, "0")
            objHeadCount._MaleFrom = IIf(TxttotalMaleFrom.Text.Trim.Length > 0, TxttotalMaleFrom.Text, "0")
            objHeadCount._HeadFrom = IIf(TxttotalHeadFrom.Text.Trim.Length > 0, TxttotalHeadFrom.Text, "0")
            objHeadCount._FemaleTo = IIf(Txttotalfemaleto.Text.Trim.Length > 0, Txttotalfemaleto.Text, "0")
            objHeadCount._MaleTo = IIf(TxttotalMaleTo.Text.Trim.Length > 0, TxttotalMaleTo.Text, "0")
            objHeadCount._HeadTo = IIf(TxttotalHeadTo.Text.Trim.Length > 0, TxttotalHeadTo.Text, "0")
            objHeadCount._IsActive = chkInactiveemp.Checked
            objHeadCount._ShowChartDept = chkShowChartDept.Checked

            'S.SANDEEP [ 29 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objHeadCount._IsNAIncluded = chkIncludeNAEmp.Checked
            'S.SANDEEP [ 29 JAN 2013 ] -- END

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objHeadCount._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            Return True

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function


#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objHeadCount = New clsEmployeeHeadCount(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                'S.SANDEEP [ 20 DEC 2013 ] -- START
                'rdAllocation.SelectedIndex = 2
                'S.SANDEEP [ 20 DEC 2013 ] -- END
                FillCombo()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region "Radio Button Event"


    'S.SANDEEP [ 20 DEC 2013 ] -- START
    'Protected Sub rdAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdAllocation.SelectedIndexChanged
    '    Try
    '        FillCombo()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("rdAllocation_SelectedIndexChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub cboViewBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboViewBy.SelectedIndexChanged
        Try

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            ''lblName.Text = cboViewBy.SelectedText
            lblName.Text = cboViewBy.SelectedItem.Text
            'Nilay (01-Feb-2015) -- End
            Select Case CInt(cboViewBy.SelectedValue)
                Case 1  'BRANCH
                    drpName.DataSource = (New clsStation).getComboList(, True).Tables(0)
                    drpName.DataValueField = "stationunkid"
                    drpName.DataTextField = "name"
                Case 2  'DEPARTMENT GROUP
                    drpName.DataSource = (New clsDepartmentGroup).getComboList("list", True).Tables(0)
                    drpName.DataValueField = "deptgroupunkid"
                    drpName.DataTextField = "name"
                Case 3  'DEPARTMENT
                    drpName.DataSource = (New clsDepartment).getComboList(, True).Tables(0)
                    drpName.DataValueField = "departmentunkid"
                    drpName.DataTextField = "name"
                Case 4  'SECTION GROUP
                    drpName.DataSource = (New clsSectionGroup).getComboList(, True).Tables(0)
                    drpName.DataValueField = "sectiongroupunkid"
                    drpName.DataTextField = "name"
                Case 5  'SECTION
                    drpName.DataSource = (New clsSections).getComboList(, True).Tables(0)
                    drpName.DataValueField = "sectionunkid"
                    drpName.DataTextField = "name"
                Case 6  'UNIT GROUP
                    drpName.DataSource = (New clsUnitGroup).getComboList(, True).Tables(0)
                    drpName.DataValueField = "unitgroupunkid"
                    drpName.DataTextField = "name"
                Case 7  'UNIT
                    drpName.DataSource = (New clsUnits).getComboList(, True).Tables(0)
                    drpName.DataValueField = "unitunkid"
                    drpName.DataTextField = "name"
                Case 8  'TEAM
                    drpName.DataSource = (New clsTeams).getComboList(, True).Tables(0)
                    drpName.DataValueField = "teamunkid"
                    drpName.DataTextField = "name"
                Case 9  'JOB GROUP
                    drpName.DataSource = (New clsJobGroup).getComboList(, True).Tables(0)
                    drpName.DataValueField = "jobgroupunkid"
                    drpName.DataTextField = "name"
                Case 10 'JOB
                    drpName.DataSource = (New clsJobs).getComboList(, True).Tables(0)
                    drpName.DataValueField = "jobunkid"
                    drpName.DataTextField = "name"
                Case 11 'GRADE GROUP
                    drpName.DataSource = (New clsGradeGroup).getComboList(, True).Tables(0)
                    drpName.DataValueField = "gradegroupunkid"
                    drpName.DataTextField = "name"
                Case 12 'GRADE
                    drpName.DataSource = (New clsGrade).getComboList(, True).Tables(0)
                    drpName.DataValueField = "gradeunkid"
                    drpName.DataTextField = "name"
                Case 13 'GRADE LEVEL
                    drpName.DataSource = (New clsGradeLevel).getComboList(, True).Tables(0)
                    drpName.DataValueField = "gradelevelunkid"
                    drpName.DataTextField = "name"
                Case 14 'CLASS GROUP
                    drpName.DataSource = (New clsClassGroup).getComboList(, True).Tables(0)
                    drpName.DataValueField = "classgroupunkid"
                    drpName.DataTextField = "name"
                Case 15 'CLASS
                    drpName.DataSource = (New clsClass).getComboList(, True).Tables(0)
                    drpName.DataValueField = "classesunkid"
                    drpName.DataTextField = "name"
                Case 16 'COSTCENTER
                    drpName.DataSource = (New clscostcenter_master).getComboList(, True).Tables(0)
                    drpName.DataValueField = "costcenterunkid"
                    drpName.DataTextField = "costcentername"
                Case 17 'RELEGION
                    drpName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELIGION, True, True).Tables(0)
                    drpName.DataValueField = "masterunkid"
                    drpName.DataTextField = "name"
                Case 18 'NATIONALITY
                    drpName.DataSource = (New clsMasterData).getCountryList("List", True).Tables(0)
                    drpName.DataValueField = "countryunkid"
                    drpName.DataTextField = "country_name"
                Case 19 'EMPLOYMENT TYPE
                    drpName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE, True, True).Tables(0)
                    drpName.DataValueField = "masterunkid"
                    drpName.DataTextField = "name"
                Case 20 'MARITAL STATUS
                    drpName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, True).Tables(0)
                    drpName.DataValueField = "masterunkid"
                    drpName.DataTextField = "name"
                Case 21 'LANGUAGE
                    drpName.DataSource = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, True).Tables(0)
                    drpName.DataValueField = "masterunkid"
                    drpName.DataTextField = "name"
            End Select
            drpName.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboViewBy_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 20 DEC 2013 ] -- END


#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objHeadCount.setDefaultOrderBy(0)
            objHeadCount._CompanyUnkId = Session("CompanyUnkId")
            objHeadCount._UserUnkId = Session("UserId")
            'S.SANDEEP [ 12 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objHeadCount._UserAccessFilter = Session("AccessLevelFilterString")
            'S.SANDEEP [ 12 NOV 2012 ] -- END

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objHeadCount.generateReport(0, enPrintAction.None, enExportAction.None)
            objHeadCount.generateReportNew(Session("Database_Name"), _
                                           Session("UserId"), _
                                           Session("Fin_year"), _
                                           Session("CompanyUnkId"), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           Session("UserAccessModeSetting"), True, _
                                           Session("ExportReportPath"), _
                                           Session("OpenAfterExport"), 0, _
                                           enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objHeadCount._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    ''Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    ''    Try
    ''        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError(ex, Me)
    ''    End Try
    ''End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End



        Me.lblTotalFemaleTo.Text = Language._Object.getCaption(Me.lblTotalFemaleTo.ID, Me.lblTotalFemaleTo.Text)
        Me.lblTotalFemaleFrom.Text = Language._Object.getCaption(Me.lblTotalFemaleFrom.ID, Me.lblTotalFemaleFrom.Text)
        Me.lblName.Text = Language._Object.getCaption(Me.lblName.ID, Me.lblName.Text)
        Me.lblTHTo.Text = Language._Object.getCaption(Me.lblTHTo.ID, Me.lblTHTo.Text)
        Me.lblTotalHeadsFrom.Text = Language._Object.getCaption(Me.lblTotalHeadsFrom.ID, Me.lblTotalHeadsFrom.Text)
        Me.lblMaleTo.Text = Language._Object.getCaption(Me.lblMaleTo.ID, Me.lblMaleTo.Text)
        Me.lblTotalMaleFrom.Text = Language._Object.getCaption(Me.lblTotalMaleFrom.ID, Me.lblTotalMaleFrom.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
        Me.chkShowChartDept.Text = Language._Object.getCaption(Me.chkShowChartDept.ID, Me.chkShowChartDept.Text)
        Me.chkIncludeNAEmp.Text = Language._Object.getCaption(Me.chkIncludeNAEmp.ID, Me.chkIncludeNAEmp.Text)
        Me.lblViewBy.Text = Language._Object.getCaption(Me.lblViewBy.ID, Me.lblViewBy.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End


End Class
