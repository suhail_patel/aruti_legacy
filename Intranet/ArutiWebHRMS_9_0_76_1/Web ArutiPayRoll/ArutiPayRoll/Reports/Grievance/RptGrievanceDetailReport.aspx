﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="RptGrievanceDetailReport.aspx.vb"
    Inherits="Reports_Grievance_RptGrievanceDetailReport" Title="Grievance Detail Report" %>

<%--<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc6" %>--%>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js" type="text/javascript"></script>

    <script src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script type="text/javascript">
function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Detail Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                <%-- <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" CssClass="lnkhover"></asp:LinkButton>
                                    </div>--%>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="lblfromdate" runat="server" Text="From Date" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 25%;" class="ib">
                                        <uc3:DateCtrl ID="dtfromdate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div style="width: 15%;" class="ib">
                                        <asp:Label ID="lbltodate" runat="server" Text="To" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 25%;" class="ib">
                                        <uc3:DateCtrl ID="dttodate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="lblfromemployee" runat="server" Text="From Employee" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 70%;" class="ib">
                                        <asp:DropDownList ID="drpfromemployee" runat="server" Width="385px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="lblaginstemployee" runat="server" Text="Against Employee" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 70%;" class="ib">
                                        <asp:DropDownList ID="drpaginstemployee" runat="server" Width="385px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="lblrefno" runat="server" Text="Ref. No" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 32%;" class="ib">
                                        <asp:TextBox ID="txtrefno" runat="server" Width="100%"></asp:TextBox>
                                    </div>
                                    <div style="width: 35%;" class="ib">
                                        <asp:CheckBox ID="chkshowcommitee" runat="server" Width="100%" Text="Show Commitee Members" />
                                    </div>
                                </div>
                                <div class="row2" style="width: 100%">
                                    <div style="width: 25%;" class="ib">
                                        <asp:Label ID="lblgrestatus" runat="server" Text="Status" Width="100%"></asp:Label>
                                    </div>
                                    <div style="width: 70%;" class="ib">
                                        <asp:DropDownList ID="drpgrestatus" runat="server" Width="385px">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                    <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <%--  <uc6:AnalysisBy ID="AnalysisBy" runat="server" />--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
