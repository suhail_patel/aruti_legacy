﻿<%@ Page Title="Leave Liability Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_LeaveLiabilityReport.aspx.vb" Inherits="Reports_Rpt_LeaveLiabilityReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ExportReport.ascx"  TagName="Export" TagPrefix="uc9" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 40%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave Liability Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Style="font-weight: bold; font-size: 12px" Font-Underline="false" Text="Analysis By"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                    
                                      <%--'Pinkal (03-May-2019) -- Start
                                      'Enhancement - Working on Leave UAT Changes for NMB.--%>
                                      <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="LblAsonDate" runat="server" Text="As on Date"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <uc2:DateCtrl ID="dtpAsonDate" runat="server"/>
                                            </td>
                                        </tr>
                                     <%--'Pinkal (03-May-2019) -- End--%>
                                    
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblLeaveName" runat="server" Text="Leave"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboLeave" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkOtherEarningBasicSal" runat="server" style="margin-top: 10px" Checked="false" AutoPostBack="true"
                                                    Text="Basic Salary As Other Earning" />
                                            </td>
                                        </tr>
                                        <asp:Panel ID="pnl_OtherEarning" runat="server" Enabled="false">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblOtherEarning" runat="server" Text="Other Earnings"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboOtherEarning" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </asp:Panel>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnExport" runat="server" CssClass="btndefault" Text="Export" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:AnalysisBy ID="popAnalysisby" runat="server" />
                    <uc9:Export runat="server" ID="Export" />



                </ContentTemplate>
                 <Triggers>
                 <asp:PostBackTrigger ControlID="Export" />
</Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
