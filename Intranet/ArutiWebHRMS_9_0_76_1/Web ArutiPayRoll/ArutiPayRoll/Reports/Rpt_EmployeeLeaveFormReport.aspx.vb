﻿Option Strict On 'Shani(11 Feb 2016)
#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
Imports System

#End Region


Partial Class Reports_Rpt_EmployeeLeaveFormReport
    Inherits Basepage


#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Dim objLeaveForm As clsEmployeeLeaveForm
    'Pinkal (11-Sep-2020) -- End




    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmEmployeeLeaveForm"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Public Sub FillCombo()
        Try

            Dim objEmployee As New clsEmployee_Master
            Dim dsList As New DataSet

            'Shani(11-Feb-2016) -- Start
            'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
            'If Session("LoginBy") = Global.User.en_loginby.User Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'If CBool(Session("IsIncludeInactiveEmp")) = True Then
            '    '    dsList = objEmployee.GetList("Employee", False, True)
            '    'Else
            '    '    dsList = objEmployee.GetList("Employee", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            '    'End If
            '    dsList = objEmployee.GetList(Session("Database_Name"), _
            '                                 Session("UserId"), _
            '                                 Session("Fin_year"), _
            '                                 Session("CompanyUnkId"), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 Session("UserAccessModeSetting"), True, _
            '                                 Session("IsIncludeInactiveEmp"), "Employee", _
            '                                 Session("ShowFirstAppointmentDate"))
            '    'Shani(24-Aug-2015) -- End

            '    Dim dRow As DataRow = dsList.Tables(0).NewRow
            '    dRow("employeeunkid") = 0
            '    dRow("name") = "Select"
            '    dsList.Tables(0).Rows.InsertAt(dRow, 0)

            'ElseIf Session("LoginBy") = Global.User.en_loginby.Employee Then

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'dsList = objEmployee.GetList("Employee", False, True, , , CInt(Session("Employeeunkid")), Session("AccessLevelFilterString"))
            '    dsList = objEmployee.GetList(Session("Database_Name"), _
            '                                 Session("UserId"), _
            '                                 Session("Fin_year"), _
            '                                 Session("CompanyUnkId"), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                 Session("UserAccessModeSetting"), True, _
            '                                 Session("IsIncludeInactiveEmp"), "Employee", _
            '                                 Session("ShowFirstAppointmentDate"), _
            '                                 CInt(Session("Employeeunkid")), , _
            '                                 Session("AccessLevelFilterString"))
            '    'Shani(24-Aug-2015) -- End


            'End If

            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            'Shani(11-Feb-2016) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objEmployee = Nothing

            Dim objLeave As New clsleavetype_master

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (06-Dec-2019) -- Start
            'Enhancement SPORT PESA -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.
            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", True)
            'Else
            '    dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            'End If
                dsList = objLeave.getListForCombo("Leave", True, 1, Session("Database_Name").ToString, "", False)
            'Pinkal (06-Dec-2019) -- End
            'Pinkal (25-May-2019) -- End


            With cboLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
            End With
            objLeave = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function Validation() As Boolean
        Try

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

            'If txtLeaveForm.Text.Trim = "" Then
            '    DisplayMessage.DisplayMessage("Leave Form No cannot be blank. Leave Form No is required information.", Me)
            '    txtLeaveForm.Focus()
            '    Exit Function
            'End If

            If CInt(cboLeaveType.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                cboLeaveType.Focus()
                Return False

            ElseIf CInt(cboLeaveForm.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Leave Form is compulsory information.Please Select Leave Form."), Me)
                cboLeaveForm.Focus()
                Return False
            End If

            'Pinkal (06-Mar-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Public Sub ResetValue()
        Try
            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
            'txtLeaveForm.Text = ""
            cboLeaveForm.SelectedIndex = 0
            'Pinkal (06-Mar-2014) -- End
            cboEmployee.SelectedIndex = 0
            cboLeaveType.SelectedIndex = 0

            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : TRA Changes Leave Enhancement
            chkIncludeDependandList.Checked = False
            'Pinkal (19-Jun-2014) -- End


            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            chkDonotShowExpense.Checked = True
            'Pinkal (02-May-2017) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByRef objLeaveForm As clsEmployeeLeaveForm) As Boolean
        'Pinkal (11-Sep-2020) -- End
        Try
            objLeaveForm.SetDefaultValue()
            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
            'objLeaveForm._LeaveFormNo = txtLeaveForm.Text.Trim
            objLeaveForm._LeaveFormId = CInt(cboLeaveForm.SelectedValue)
            'Pinkal (06-Mar-2014) -- End

            objLeaveForm._EmployeeId = CInt(cboEmployee.SelectedValue)
            objLeaveForm._EmployeeName = cboEmployee.SelectedItem.Text
            objLeaveForm._LeaveTypeId = CInt(cboLeaveType.SelectedValue)
            objLeaveForm._LeaveTypeName = cboLeaveType.SelectedItem.Text


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            objLeaveForm._YearId = CInt(Session("Fin_year"))
            objLeaveForm._LeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            objLeaveForm._Fin_StartDate = CDate(Session("fin_startdate")).Date
            objLeaveForm._Fin_Enddate = CDate(Session("fin_enddate")).Date
            'Pinkal (24-May-2014) -- End

            'Pinkal (19-Jun-2014) -- Start
            'Enhancement : TRA Changes Leave Enhancement
            objLeaveForm._IncludeDependents = chkIncludeDependandList.Checked
            'Pinkal (19-Jun-2014) -- End


            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            objLeaveForm._DoNotShowExpesneIfNoExpense = chkDonotShowExpense.Checked
            'Pinkal (02-May-2017) -- End


            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 

            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If



            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeaveForm = New clsEmployeeLeaveForm
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()

                'Pinkal (06-Mar-2014) -- Start
                'Enhancement : TRA Changes  Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 
                cboLeaveType_SelectedIndexChanged(New Object, New EventArgs())
                'Pinkal (06-Mar-2014) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveForm As New clsEmployeeLeaveForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'Pinkal (11-Sep-2020) -- End

        Try
            If Validation() Then


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'If SetFilter() = False Then Exit Sub
                If SetFilter(objLeaveForm) = False Then Exit Sub
                'Pinkal (11-Sep-2020) -- End


                GUI.fmtCurrency = CStr(Session("fmtCurrency"))
                objLeaveForm._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objLeaveForm._UserUnkId = CInt(Session("UserId"))
                objLeaveForm._UserAccessFilter = CStr(Session("AccessLevelFilterString"))


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'Pinkal (01-Dec-2016) -- Start
                'Enhancement - Working on Report Changes For Employee Login In ESS Report [Display Employee Name Currently It is displaying Wrong Name].
                If CInt(Session("Employeeunkid")) > 0 Then
                    objLeaveForm._UserName = Session("DisplayName").ToString()
                End If
                'Pinkal (01-Dec-2016) -- End



                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
                objLeaveForm._LeaveAccrueTenureSetting = CInt(Session("LeaveAccrueTenureSetting"))
                objLeaveForm._LeaveAccrueDaysAfterEachMonth = CInt(Session("LeaveAccrueDaysAfterEachMonth"))
                'Pinkal (16-Dec-2016) -- End


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objLeaveForm.generateReport(0, enPrintAction.None, enExportAction.None)
                objLeaveForm.generateReportNew(CStr(Session("Database_Name")), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               CStr(Session("UserAccessModeSetting")), True, _
                                               CStr(Session("ExportReportPath")), _
                                               CBool(Session("OpenAfterExport")), _
                                               0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
                'Shani(20-Nov-2015) -- End

                Session("objRpt") = objLeaveForm._Rpt
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Open New Window And Show Report Every Report
                'Response.Redirect("../Aruti Report Structure/Report.aspx")

                'Shani(11-Feb-2016) -- Start
                'Leave,TNA And Medical Reprt Convert to Active Employee Query Wise
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                If Session("objRpt") IsNot Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
                'Shani(11-Feb-2016) -- End

                'Shani(24-Aug-2015) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

    'Pinkal (06-Mar-2014) -- Start
    'Enhancement : TRA Changes Leave form in ESS is not user friendly to employees. They system is asking for leave form number which employee does not know. 

#Region "Dropdown Events"

    Protected Sub cboLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objForm As New clsleaveform
            Dim dsList As DataSet = Nothing


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            'Pinkal (24-May-2014) -- Start
            'Enhancement : TRA Changes [Exclude Next Year Recurrent Holiday From Employee Leave Form As Per Mr.Andrew AND TRA Requirement AND DON'T CHANGE IT]
            'dsList = objForm.getListForCombo(CInt(cboLeaveType.SelectedValue), "", CInt(cboEmployee.SelectedValue), True, True)
            dsList = objForm.getListForCurrentYearLeaveForm(CInt(Session("Fin_year")), CInt(cboLeaveType.SelectedValue), "", CInt(cboEmployee.SelectedValue), True, True)
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "yearunkid <=0 or yearunkid = " & CInt(Session("Fin_year")), "", DataViewRowState.CurrentRows).ToTable
            'Pinkal (24-May-2014) -- End


            cboLeaveForm.DataValueField = "formunkid"
            cboLeaveForm.DataTextField = "name"
            cboLeaveForm.DataSource = dtTable
            cboLeaveForm.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboLeaveType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (06-Mar-2014) -- End


    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.Title = Language._Object.getCaption(mstrModuleName, objLeaveForm._ReportName)
            'Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objLeaveForm._ReportName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Pinkal (11-Sep-2020) -- End

            


        Me.LblLeaveType.Text = Language._Object.getCaption(Me.LblLeaveType.ID, Me.LblLeaveType.Text)
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
        Me.LblLeaveForm.Text = Language._Object.getCaption(Me.LblLeaveForm.ID, Me.LblLeaveForm.Text)

        Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnReport.Text = Language._Object.getCaption(Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Me.chkIncludeDependandList.Text = Language._Object.getCaption(Me.chkIncludeDependandList.ID, Me.chkIncludeDependandList.Text)


        'Pinkal (02-May-2017) -- Start
        'Enhancement - Working on Leave Enhancement for HJF.
        Me.chkDonotShowExpense.Text = Language._Object.getCaption(Me.chkDonotShowExpense.ID, Me.chkDonotShowExpense.Text)
        'Pinkal (02-May-2017) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
