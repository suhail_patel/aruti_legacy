﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_Rpt_Employee_Analysis
    Inherits Basepage


#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Dim objAgeAnalysis As clsEmployeeAgeAnalysisReport


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeAgeAnalysisReport"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Methods "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = ObjEmp.GetEmployeeList("Emp", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            dsList = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "Emp", True)
            'Shani(24-Aug-2015) -- End
         
            With drpemployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Dim objMaster As New clsMasterData
            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(True)
            dsList = objMaster.GetCondition(True, True, False, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dRow As DataRow = dsList.Tables(0).NewRow
            dRow.Item("id") = "5"
            dRow.Item("Name") = "="
            dsList.Tables(0).Rows.InsertAt(dRow, 1)
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            objMaster = Nothing
            'S.SANDEEP [ 22 FEB 2013 ] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            dsList.Dispose()
            ObjEmp = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            drpemployee.SelectedValue = 0
            dtasondate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            TxtCode.Text = ""
            objAgeAnalysis.setDefaultOrderBy(0)
            chkInactiveemp.Checked = False
            chkSlabWiseReport.Checked = True
            chkReportType.Checked = False
            txtFromYear.Text = "21"
            TxtToYear.Text = "60"
            TxtYearGap.Text = "5"
            'mstrStringIds = ""
            'mstrStringName = ""
            ' mintViewIdx = -1
            'mstrAnalysis_Fields = ""
            ' mstrAnalysis_Join = ""
            ' mstrAnalysis_OrderBy = ""
            ' mstrReport_GroupName = ""

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Call chkSlabWiseReport_CheckedChanged(chkSlabWiseReport, New System.EventArgs)
            'Nilay (01-Feb-2015) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try

            'Pinkal (31-Jul-2020) -- Start
            'Optimization  - Working on Approve/Reject Update Progress in Assessment.	
            If dtasondate.GetDate = Nothing Then
                DisplayMessage.DisplayMessage("Please Select Proper date.", Me)
                Return False
            End If
            'Pinkal (31-Jul-2020) -- End

            objAgeAnalysis.SetDefaultValue()
            If chkSlabWiseReport.Checked = True Then
                If CInt(TxtToYear.Text) < CInt(TxtFromYear.Text) Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Ending Year cannot be less or equal to Starting Year."), Me)
                    'Pinkal (06-May-2014) -- End
                    TxtToYear.Focus()
                    Return False
                End If

                If (CInt(TxtToYear.Text) - CInt(TxtFromYear.Text)) < CInt(TxtYearGap.Text) Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Year Gap cannot be less than the Year difference of From Year and To Year."), Me)
                    'Pinkal (06-May-2014) -- End
                    TxtYearGap.Focus()
                    Return False
                End If
            End If
            objAgeAnalysis._DateAsOn = dtasondate.GetDate.Date
            objAgeAnalysis._EmployeeCode = TxtCode.Text
            objAgeAnalysis._EmployeeId = drpemployee.SelectedValue


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objAgeAnalysis._EmployeeName = drpemployee.Text
            objAgeAnalysis._EmployeeName = drpemployee.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            objAgeAnalysis._IsActive = chkInactiveemp.Checked
            objAgeAnalysis._IsFromAppointmentDate = chkReportType.Checked
            If chkSlabWiseReport.Checked = True Then
                objAgeAnalysis._StartingYear = CInt(txtFromYear.Text)
                objAgeAnalysis._EndingYear = CInt(TxtToYear.Text)
                objAgeAnalysis._YearGap = CInt(TxtYearGap.Text)
            End If


            'S.SANDEEP [ 22 FEB 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If cboCondition.Visible = True Then
                If cboCondition.SelectedValue > 0 Then
                    objAgeAnalysis._Condition = cboCondition.SelectedItem.Text
                    objAgeAnalysis._AgeFilter = CInt(txtYear.Text)
                End If
            End If
            'S.SANDEEP [ 22 FEB 2013 ] -- END


            'objAgeAnalysis._ViewByIds = mstrStringIds
            'objAgeAnalysis._ViewIndex = mintViewIdx
            'objAgeAnalysis._ViewByName = mstrStringName
            'objAgeAnalysis._Analysis_Fields = mstrAnalysis_Fields
            'objAgeAnalysis._Analysis_Join = mstrAnalysis_Join
            'objAgeAnalysis._Analysis_OrderBy = mstrAnalysis_OrderBy
            'objAgeAnalysis._Report_GroupName = mstrReport_GroupName
            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objAgeAnalysis._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objAgeAnalysis = New clsEmployeeAgeAnalysisReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
                dtasondate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                txtFromYear.Text = "21"
                TxtToYear.Text = "60"
                TxtYearGap.Text = "5"
                'For i As Integer = 0 To objAgeAnalysis.Field_OnDetailReport.Count - 1
                '    chkSort.Items.Add(objAgeAnalysis.Field_OnDetailReport.ColumnItem(i).DisplayName)
                'Next
                'chkSort.Items(0).Selected = True

                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Call chkSlabWiseReport_CheckedChanged(sender, e)
                'S.SANDEEP [ 22 FEB 2013 ] -- END

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objAgeAnalysis._CompanyUnkId = Session("CompanyUnkId")
            objAgeAnalysis._UserUnkId = Session("UserId")


            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            objAgeAnalysis._UserAccessFilter = Session("AccessLevelFilterString")
            'Pinkal (12-Nov-2012) -- End


            objAgeAnalysis.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objAgeAnalysis.generateReport(0, enPrintAction.None, enExportAction.None)
            objAgeAnalysis.generateReportNew(Session("Database_Name"), _
                                             Session("UserId"), _
                                             Session("Fin_year"), _
                                             Session("CompanyUnkId"), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                             Session("UserAccessModeSetting"), True, _
                                             Session("ExportReportPath"), _
                                             Session("OpenAfterExport"), 0, _
                                             enPrintAction.None, enExportAction.None, _
                                             Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objAgeAnalysis._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "CheckBox Event"

    Protected Sub chkSlabWiseReport_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSlabWiseReport.CheckedChanged
        Try
            If chkSlabWiseReport.Checked Then
                TxtFromYear.Text = "21"
                TxtToYear.Text = "60"
                TxtYearGap.Text = "5"
                lblFromYear.Visible = True
                lblToYear.Visible = True
                lblYearGap.Visible = True
                TxtFromYear.Visible = True
                TxtToYear.Visible = True
                TxtYearGap.Visible = True
                nudFromYear.Enabled = True
                nudToYear.Enabled = True
                nudYearGap.Enabled = True
                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lblCondition.Visible = False
                cboCondition.Visible = False
                lblYear.Visible = False
                nudYear.Enabled = False
                txtYear.Visible = False
                'S.SANDEEP [ 22 FEB 2013 ] -- END
            Else
                lblFromYear.Visible = False
                lblToYear.Visible = False
                lblYearGap.Visible = False
                TxtFromYear.Visible = False
                TxtToYear.Visible = False
                TxtYearGap.Visible = False
                nudFromYear.Enabled = False
                nudToYear.Enabled = False
                nudYearGap.Enabled = False
                'S.SANDEEP [ 22 FEB 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                lblCondition.Visible = True
                cboCondition.Visible = True
                lblYear.Visible = True
                nudYear.Enabled = True
                txtYear.Visible = True
                'S.SANDEEP [ 22 FEB 2013 ] -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSlabWiseReport_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Protected Sub drpemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpemployee.DataBound
        Try
        If drpemployee.Items.Count > 0 Then
            For Each lstItem As ListItem In drpemployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            drpemployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, objAgeAnalysis._ReportName)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, objAgeAnalysis._ReportName)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, objAgeAnalysis._ReportName)
        'Nilay (01-Feb-2015) -- End
        Me.lblDateAsOn.Text = Language._Object.getCaption(Me.lblDateAsOn.ID, Me.lblDateAsOn.Text)
        Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
        Me.chkReportType.Text = Language._Object.getCaption(Me.chkReportType.ID, Me.chkReportType.Text)
        Me.chkSlabWiseReport.Text = Language._Object.getCaption(Me.chkSlabWiseReport.ID, Me.chkSlabWiseReport.Text)
        Me.lblYearGap.Text = Language._Object.getCaption(Me.lblYearGap.ID, Me.lblYearGap.Text)
        Me.lblToYear.Text = Language._Object.getCaption(Me.lblToYear.ID, Me.lblToYear.Text)
        Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.ID, Me.lblFromYear.Text)
        Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.ID, Me.lblYear.Text)
        Me.lblCondition.Text = Language._Object.getCaption(Me.lblCondition.ID, Me.lblCondition.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (06-May-2014) -- End


End Class
