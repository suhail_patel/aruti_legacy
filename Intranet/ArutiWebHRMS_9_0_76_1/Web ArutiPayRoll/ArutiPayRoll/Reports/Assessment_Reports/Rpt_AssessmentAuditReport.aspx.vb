﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_AssessmentAuditReport
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentAuditReport"
    Private objAssessAudit As clsAssessmentAuditReport
    Private mstrAdvanceFilter As String = String.Empty
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                 Session("UserId"), _
                                                 Session("Fin_year"), _
                                                 Session("CompanyUnkId"), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 Session("UserAccessModeSetting"), True, _
                                                 Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With


            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "BSC Planning Audit Report"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Assessment Audit Report"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboReportType.SelectedIndex = 0
            mstrAdvanceFilter = ""
            chkConsiderEmployeeTermination.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            objAssessAudit.SetDefaultValue()

            If CInt(cboReportType.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please Select Report Type."), Me)
                cboPeriod.Focus()
                Exit Function
            End If

            If CInt(cboPeriod.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Function
            End If

            If CInt(cboReportType.SelectedIndex) = 2 AndAlso CInt(cboReportMode.SelectedIndex) = 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please Select Report Mode."), Me)
                cboReportMode.Focus()
                Exit Function
            End If

            objAssessAudit._PeriodId = CInt(cboPeriod.SelectedValue)
            objAssessAudit._ReportTypeId = CInt(cboReportType.SelectedIndex)
            objAssessAudit._ReportTypeName = cboReportType.SelectedItem.Text
            objAssessAudit._PeriodName = cboPeriod.SelectedItem.Text
            If CInt(cboReportMode.SelectedIndex) > 0 Then
                objAssessAudit._ReportModeId = CInt(cboReportMode.SelectedIndex)
                objAssessAudit._ReportModeName = cboReportMode.SelectedItem.Text
            End If
            If CInt(cboStatus.SelectedIndex) <= 0 Then
                objAssessAudit._ViewIndex = 1
            Else
                objAssessAudit._ViewIndex = 0
            End If

            Select Case cboReportType.SelectedIndex
                Case 1
                    objAssessAudit._StatusId = CInt(CType(cboStatus.SelectedItem, ListItem).Value)
                Case 2
                    objAssessAudit._StatusId = -1
                    If CInt(cboStatus.SelectedIndex) = 1 Then
                        objAssessAudit._StatusId = 1
                    ElseIf CInt(cboStatus.SelectedIndex) = 2 Then
                        objAssessAudit._StatusId = 0
                    End If
            End Select

            objAssessAudit._StatusName = cboStatus.SelectedItem.Text
            objAssessAudit._Report_GroupName = "Status"
            objAssessAudit._EmployeeId = CInt(cboEmployee.SelectedValue)
            objAssessAudit._EmployeeName = cboEmployee.SelectedItem.Text
            objAssessAudit._AdvanceFilter = mstrAdvanceFilter

            If chkConsiderEmployeeTermination.Checked = False Then
                mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate")).Date
                mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate")).Date
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objAssessAudit = New clsAssessmentAuditReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If Not IsPostBack Then
                Call FillCombo()
            Else
                mdtStartDate = Me.ViewState("mdtStartDate")
                mdtEndDate = Me.ViewState("mdtEndDate")
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtStartDate") = mdtStartDate
            Me.ViewState("mdtEndDate") = mdtEndDate
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty
            objAssessAudit.Export_Assessment_Report(CInt(cboReportType.SelectedIndex), _
                                                    Session("Database_Name").ToString, _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    mdtStartDate, _
                                                    mdtEndDate, _
                                                    CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)
            If strFileName.Trim.Length <= 0 Then strFileName = objAssessAudit._FileNameAfterExported.Trim
            If strFileName.Contains(".xls") = False Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
            Else
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
            End If
            Export.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdvFilter.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            cboStatus.Items.Clear() : cboReportMode.Items.Clear()
            Select Case cboReportType.SelectedIndex
                Case 0  'SELECT
                    cboStatus.Items.Add(New ListItem(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                    cboReportMode.Items.Add(New ListItem(Language.getMessage("frmNotAssessedEmployeeReport", 4, "Select"), 0))
                    cboReportMode.Enabled = False
                    cboStatus.SelectedIndex = 0
                Case 1  'BSC Planning Audit Report
                    With cboStatus
                        .Items.Add(New ListItem(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                        .Items.Add(New ListItem(Language.getMessage("clsBSC_Planning_Report", 1, "Submitted For Approval"), enObjective_Status.SUBMIT_APPROVAL))
                        .Items.Add(New ListItem(Language.getMessage("clsBSC_Planning_Report", 101, "Approved"), enObjective_Status.FINAL_SAVE))
                        .Items.Add(New ListItem(Language.getMessage("clsBSC_Planning_Report", 3, "Opened For Changes"), enObjective_Status.OPEN_CHANGES))
                        .Items.Add(New ListItem(Language.getMessage("clsBSC_Planning_Report", 44, "Not Submitted For Approval"), enObjective_Status.NOT_SUBMIT))
                        .Items.Add(New ListItem(Language.getMessage("clsBSC_Planning_Report", 51, "Periodic Review"), enObjective_Status.PERIODIC_REVIEW))
                        .Items.Add(New ListItem(Language.getMessage("clsBSC_Planning_Report", 5, "Not Planned"), 999))
                    End With
                    cboReportMode.Enabled = False
                    cboStatus.SelectedIndex = 0
                Case 2  'Assessment Audit Report
                    cboReportMode.Enabled = True
                    With cboStatus
                        .Items.Add(New ListItem(Language.getMessage(mstrModuleName, 4, "Select"), 0))
                        .Items.Add(New ListItem(Language.getMessage(mstrModuleName, 6, "Committed Assessment"), 1))
                        .Items.Add(New ListItem(Language.getMessage(mstrModuleName, 7, "Not Committed Assessment"), 1))
                    End With

                    With cboReportMode
                        .Items.Clear()
                        .Items.Add(New ListItem(Language.getMessage("frmNotAssessedEmployeeReport", 4, "Select"), 0))
                        .Items.Add(New ListItem(Language.getMessage("frmNotAssessedEmployeeReport", 55, "Self Assessment"), 1))
                        .Items.Add(New ListItem(Language.getMessage("frmNotAssessedEmployeeReport", 6, "Assessor Assessment"), 2))
                        .Items.Add(New ListItem(Language.getMessage("frmNotAssessedEmployeeReport", 7, "Reviewer Assessment"), 3))
                    End With

                    cboStatus.SelectedIndex = 0
                    cboReportMode.SelectedIndex = 0
            End Select
            If cboReportType.SelectedIndex > 0 Then
                objAssessAudit.setDefaultOrderBy(cboReportType.SelectedIndex)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
            Else
                mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s)"

    Private Sub chkConsiderEmployeeTermination_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConsiderEmployeeTermination.CheckedChanged
        Try
            If chkConsiderEmployeeTermination.Checked = True AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
            Else
                mdtStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                mdtEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
