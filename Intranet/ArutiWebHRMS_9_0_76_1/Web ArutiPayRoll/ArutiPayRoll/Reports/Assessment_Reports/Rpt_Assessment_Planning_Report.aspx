﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Assessment_Planning_Report.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_Assessment_Planning_Report" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="uc" TagPrefix="datectrl" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="popupAnalysisBy" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

      <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }

        //        function change_event(drp) {
        //            if ($(drp).find(":selected").index() == 0) {
        //                document.getElementById("<%=lnkAnalysisBy.ClientID%>").style.display = "block"
        //            }
        //            else {
        //                document.getElementById("<%=lnkAnalysisBy.ClientID%>").style.display = "none";
        //            }
        //        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Planning Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <%--'S.SANDEEP |17-MAR-2020| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : PM ERROR--%>
                                    <div style="float: right;">
                                        <asp:LinkButton ID="lnkAnalysisBy" runat="server" Text="Analysis By" Font-Underline="false"></asp:LinkButton>
                                    </div>
                                    <%--'S.SANDEEP |17-MAR-2020| -- END--%>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportPlanning" runat="server" Text="Report Type"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboReportType" runat="server" Width="99%" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <%--'S.SANDEEP |27-MAY-2019| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]--%>
                                    <asp:Panel ID="pnlCalibrationScore" runat="server">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score"></asp:Label>
                                                </td>
                                                <td style="width: 80%">
                                                    <asp:DropDownList ID="cboScoreOption" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <%--'S.SANDEEP |27-MAY-2019| -- END--%>
                                    <asp:Panel ID="pnlBSC" runat="server" Width="100%">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="cboStatus" runat="server" Width="99%">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblSDateFrom" runat="server" Text="Status From"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <datectrl:uc ID="dtpS_Dt_From" runat="server" />
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblSDateTo" runat="server" Text="To"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <datectrl:uc ID="dtpS_Dt_To" runat="server" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="4">
                                                    <asp:CheckBox ID="chkIncludeSummary" runat="server" Text="Display Summary Report"
                                                        Checked="true" />
                                                    <asp:CheckBox ID="chkExcludeInactive" runat="server" Text="Exclude Inactive Employee"
                                                        Checked="true" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblAppointment" runat="server" Text="Appointed"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="cboAppointment" runat="server" Width="99%" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="objlblCaption" runat="server" Text="#Value"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <datectrl:uc ID="dtpDate1" runat="server" />
                                                </td>
                                                <td style="width: 20%">
                                                    <asp:Label ID="lblTo" runat="server" Text="To"></asp:Label>
                                                </td>
                                                <td style="width: 20%">
                                                    <datectrl:uc ID="dtpDate2" runat="server" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                </td>
                                                <td colspan="3">
                                                    <asp:CheckBox ID="chkConsiderEmployeeTermination" runat="server" Text="Consider Employee Active Status On Period Date" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlEmp" runat="server" Width="100%">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Panel ID="Panel2" runat="server" Width="100%" Height="250px" ScrollBars="Auto">
                                                        <asp:DataGrid ID="dgvAEmployee" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:TemplateColumn ItemStyle-Width="25">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="EmpCodeName" HeaderText="Employee" />
                                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:CheckBox ID="chkShowComputedScore" runat="server" Text="Show Computed Score" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:popupAnalysisBy ID="popupAnalysisBy" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
