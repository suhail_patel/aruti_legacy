﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region
Partial Class Reports_Assessment_Reports_Rpt_Assessment_Score_Report
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessmentSoreRatingReport"
    Private objScoreRating As clsAssessSoreRatingReport
    Private mdtStartDate As DateTime = Nothing
    Private mdtEndDate As DateTime = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private DisplayMessage As New CommonCodes
    Private mdtDisplayCol As DataTable
    Private mdtAllocation As DataTable

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim ObjEmp As New clsEmployee_Master
        Dim ObjPeriod As New clscommom_period_Tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = ObjEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = ObjPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            Call cboPeriod_SelectedIndexChanged(New Object(), New EventArgs())

            With cboReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 13, "Based On Single Period"))
                .Items.Add(Language.getMessage(mstrModuleName, 14, "Based On Multiple Period"))
                .SelectedIndex = 0
            End With
            Call cboReportType_SelectedIndexChanged(New Object(), New EventArgs())


            With cboSubReportType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Based on Employee"))
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Based on Allocation"))
                If Session("CompCode") = "FZ" Or Session("CompCode") = "FZT" Or Session("CompCode") = "FZM" Then
                    .Items.Add(Language.getMessage(mstrModuleName, 12, "Based on Competencies"))
                End If
                .SelectedIndex = 0
            End With
            Call cboSubReportType_SelectedIndexChanged(New Object(), New EventArgs())



            Dim objMData As New clsMasterData
            dsCombos = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsCombos.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & ", " & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboAllocations
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = 1
            End With
            Call cboAllocations_SelectedIndexChanged(New Object(), New EventArgs())

            Dim objMaster As New clsMasterData
            dsCombos = objMaster.GetCondition(True, True, False, False, False)
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            objMaster = Nothing

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            Dim objCScoreMaster As New clsComputeScore_master
            dsCombos = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cboScoreOption
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-MAY-2019| -- END



            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            objMaster = New clsMasterData
            dsCombos = objMaster.getComboListPAYYEAR(CInt(Session("Fin_year")), _
                                                     Session("FinancialYear_Name").ToString(), _
                                                     CInt(Session("CompanyUnkId")), "Year", True, True)
            With drpFinancialYear


                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Year")
                .SelectedValue = 0
                .DataBind()
            End With
            objMaster = Nothing


            dgvPeriod.DataSource = New String() {}
            dgvPeriod.DataBind()

            lvAllocation.DataSource = New String() {}
            lvAllocation.DataBind()

            lvDisplayCol.DataSource = New String() {}
            lvDisplayCol.DataBind()
            'Gajanan [17-SEP-2019] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsCombos.Dispose() : ObjEmp = Nothing : ObjPeriod = Nothing
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            chkOption.Checked = False
            Call chkOption_CheckedChanged(New Object(), New EventArgs())
            mstrAdvanceFilter = ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            If cboSubReportType.SelectedIndex = 0 AndAlso cboReportType.SelectedIndex = 0 Then
                'Gajanan [17-SEP-2019] -- End
                If cboPeriod.SelectedValue <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue."), Me)
                    cboPeriod.Focus()
                    Return False
                End If
            End If


            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            If cboSubReportType.SelectedIndex = 0 Then
                'Gajanan [17-SEP-2019] -- End
                If cboEmployee.SelectedValue <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is mandatory information. Please select employee to continue."), Me)
                    cboEmployee.Focus()
                    Return False
                End If
            End If

            objScoreRating._EmployeeId = cboEmployee.SelectedValue
            objScoreRating._EmployeeName = cboEmployee.SelectedItem.Text

            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            If lvAllocation.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkabox"), CheckBox).Checked = True).Count <= 0 Then
                'Gajanan [17-SEP-2019] -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please select atleast one allocation to export report."), Me)
                lvAllocation.Focus()
                Return False
            End If
            If chkOption.Checked = True Then
                If CInt(cboCondition.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, condition is mandatory information."), Me)
                    cboCondition.Focus()
                    Return False
                End If
            End If



            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            'If CType(Me.ViewState("mdtDisplayCol"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() <= 0 Then
            If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count <= 0 Then
                'Gajanan [17-SEP-2019] -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please select atleast one column to display in report."), Me)
                lvDisplayCol.Focus()
                Return False
            End If


            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            If cboReportType.SelectedIndex = 1 Then
                If dgvPeriod.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkpbox"), CheckBox).Checked = True).Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please check atleast one period from the list in order to generate report."), Me)
                    Return False
                End If

                objScoreRating._DicPeriodChecked = dgvPeriod.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkpbox"), CheckBox).Checked = True).ToDictionary(Function(x) CInt(x.Cells(getColumnId_Datagrid(dgvPeriod, "objdgcolhPId", False, True)).Text), Function(y) CStr(y.Cells(getColumnId_Datagrid(dgvPeriod, "dgcolhPeriod", False, True)).Text))
            End If
            'Gajanan [17-SEP-2019] -- End


            objScoreRating._ReportType_Id = cboReportType.SelectedIndex
            objScoreRating._ReportType_Name = cboReportType.SelectedItem.Text

            If cboSubReportType.SelectedIndex = 1 Or _
               cboSubReportType.SelectedIndex = 2 Then
                If chkConsiderEmployeeTermination.Checked = True Then
                    objScoreRating._StartDate = mdtStartDate
                    objScoreRating._EndDate = mdtEndDate
                Else
                    objScoreRating._StartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                    objScoreRating._EndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                End If

                'Gajanan [17-SEP-2019] -- Start    
                'Enhancement:Working On Assesment Score report

                'Dim StrStringIds As String = ""
                'StrStringIds = String.Join(",", CType(Me.ViewState("mdtAllocation"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) CStr(y.Field(Of Integer)("Id"))).ToArray())
                'objScoreRating._Allocation = cboAllocations.SelectedItem.Text

                Dim StrStringIds As String = ""
                If lvAllocation.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkabox"), CheckBox).Checked = True).Count > 0 Then
                    StrStringIds = String.Join(",", dgvPeriod.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkpbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvAllocation, "Id", False, False)).Text.ToString()).ToArray())
                End If
                objScoreRating._Allocation = cboAllocations.SelectedItem.Text
                'Gajanan [17-SEP-2019] -- End

                If StrStringIds.Length > 0 Then
                    Select Case cboAllocations.SelectedValue
                        Case enAllocation.BRANCH
                            StrStringIds = "Alloc.stationunkid IN(" & StrStringIds & ") "
                        Case enAllocation.DEPARTMENT_GROUP
                            StrStringIds = "Alloc.deptgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.DEPARTMENT
                            StrStringIds = "Alloc.departmentunkid IN(" & StrStringIds & ") "
                        Case enAllocation.SECTION_GROUP
                            StrStringIds = "Alloc.sectiongroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.SECTION
                            StrStringIds = "Alloc.sectionunkid IN(" & StrStringIds & ") "
                        Case enAllocation.UNIT_GROUP
                            StrStringIds = "Alloc.unitgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.UNIT
                            StrStringIds = "Alloc.unitunkid IN(" & StrStringIds & ") "
                        Case enAllocation.TEAM
                            StrStringIds = "Alloc.teamunkid IN(" & StrStringIds & ") "
                        Case enAllocation.JOB_GROUP
                            StrStringIds = "Jobs.jobgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.JOBS
                            StrStringIds = "Jobs.jobunkid IN(" & StrStringIds & ") "
                        Case enAllocation.CLASS_GROUP
                            StrStringIds = "Alloc.classgroupunkid IN(" & StrStringIds & ") "
                        Case enAllocation.CLASSES
                            StrStringIds = "Alloc.classunkid IN(" & StrStringIds & ") "
                    End Select
                End If
                objScoreRating._AllocationIds = StrStringIds
            End If
            objScoreRating._PeriodId = cboPeriod.SelectedValue
            objScoreRating._PeriodName = cboPeriod.SelectedItem.Text
            objScoreRating._OnProbationSelected = chkOption.Checked
            objScoreRating._Caption = chkOption.Text
            objScoreRating._Condition = cboCondition.SelectedItem.Text
            If dtpAppDate.IsNull = False Then
                objScoreRating._AppointmentDate = dtpAppDate.GetDate.Date
            End If


            'Dim iCols, iJoins, iDisplay As String
            'iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty

            'If Me.ViewState("mdtDisplayCol") IsNot Nothing Then
            '    iCols = String.Join(vbCrLf, CType(Me.ViewState("mdtDisplayCol"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) CStr(y.Field(Of String)("objcolhSelectCol"))).ToArray())
            '    iJoins = String.Join(vbCrLf, CType(Me.ViewState("mdtDisplayCol"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) CStr(y.Field(Of String)("objcolhJoin"))).ToArray())
            '    iDisplay = String.Join(vbCrLf, CType(Me.ViewState("mdtDisplayCol"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Select(Function(y) CStr(y.Field(Of String)("objcolhDisplay"))).ToArray())
            'End If


            Dim iCols, iJoins, iDisplay As String
            iCols = String.Empty : iJoins = String.Empty : iDisplay = String.Empty


            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            'If Me.ViewState("mdtDisplayCol") IsNot Nothing Then
            If lvDisplayCol.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Count > 0 Then
                'Gajanan [17-SEP-2019] -- End

                iCols = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhSelectCol", False, False)).Text.ToString()).ToArray())
                iJoins = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhJoin", False, False)).Text.ToString()).ToArray())
                iDisplay = String.Join(vbCrLf, lvDisplayCol.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox"), CheckBox).Checked = True).Select(Function(x) x.Cells(getColumnId_Datagrid(lvDisplayCol, "objcolhDisplay", False, False)).Text.ToString()).ToArray())
            End If

            objScoreRating._SelectedCols = iCols
            objScoreRating._SelectedJoin = iJoins
            objScoreRating._DisplayCols = iDisplay
            objScoreRating._ScoringOptionId = Session("ScoringOptionId")
            objScoreRating._AdvanceFilter = mstrAdvanceFilter
            objScoreRating._SelfAssignCompetencies = Session("Self_Assign_Competencies")
            objScoreRating._ShowScaleOnReport = chkShowEmpSalary.Checked

            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            objScoreRating._IsCalibrationSettingActive = Session("IsCalibrationSettingActive")
            objScoreRating._DisplayScoreName = cboScoreOption.SelectedItem.Text
            objScoreRating._DisplayScoreType = cboScoreOption.SelectedValue
            'S.SANDEEP |27-MAY-2019| -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub Fill_Column_List()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dRow As DataRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 900
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 8, "Grade Group")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 901
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 9, "Grade")
                dsList.Tables(0).Rows.Add(dRow)

                dRow = dsList.Tables(0).NewRow
                dRow.Item("Id") = 902
                dRow.Item("Name") = Language.getMessage(mstrModuleName, 5, "Grade Level")
                dsList.Tables(0).Rows.Add(dRow)

                'dRow = dsList.Tables(0).NewRow
                'dRow.Item("Id") = 903
                'dRow.Item("Name") = Language.getMessage(mstrModuleName, 6, "Cost Center")
                'dsList.Tables(0).Rows.Add(dRow)

                mdtDisplayCol = dsList.Tables(0).Copy()

                Dim dCol As New DataColumn
                dCol.ColumnName = "ischeck"
                dCol.DataType = GetType(System.Boolean)
                dCol.DefaultValue = False
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhSelectCol"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhJoin"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                dCol = New DataColumn
                dCol.ColumnName = "objcolhDisplay"
                dCol.DataType = GetType(System.String)
                dCol.DefaultValue = ""
                mdtDisplayCol.Columns.Add(dCol)

                For Each dtRow As DataRow In mdtDisplayCol.Rows
                    Select Case CInt(dtRow.Item("Id"))
                        Case enAllocation.BRANCH
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(estm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrstation_master AS estm ON estm.stationunkid = Alloc.stationunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrdepartment_group_master AS edgm ON edgm.deptgroupunkid = Alloc.deptgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.DEPARTMENT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(edpt.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrdepartment_master AS edpt ON edpt.departmentunkid = Alloc.departmentunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsectiongroup_master AS esgm ON esgm.sectiongroupunkid = Alloc.sectiongroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.SECTION
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(esec.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrsection_master AS esec ON esec.sectionunkid = Alloc.sectionunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eugm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunitgroup_master AS eugm ON eugm.unitgroupunkid = Alloc.unitgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.UNIT
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eutm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrunit_master AS eutm ON eutm.unitunkid = Alloc.unitunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.TEAM
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(etem.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrteam_master AS etem ON etem.teamunkid = Alloc.teamunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOB_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrjobgroup_master AS ejgm ON ejgm.jobgroupunkid = Jobs.jobgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.JOBS
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ejbm.job_name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrjob_master AS ejbm ON ejbm.jobunkid = Jobs.jobunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASS_GROUP
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecgm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclassgroup_master AS ecgm ON ecgm.classgroupunkid = Alloc.classgroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.CLASSES
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecls.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrclasses_master AS ecls ON ecls.classesunkid = Alloc.classunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 900
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(eggm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "JOIN hrgradegroup_master eggm ON grds.gradegroupunkid = eggm.gradegroupunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 901
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egdm.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrgrade_master egdm ON grds.gradeunkid = egdm.gradeunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case 902
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(egdl.name,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN hrgradelevel_master egdl ON grds.gradelevelunkid = egdl.gradelevelunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                        Case enAllocation.COST_CENTER
                            dtRow.Item("objcolhSelectCol") = ",ISNULL(ecct.costcentername,'') AS [" & dtRow.Item("Name").ToString() & "]"
                            dtRow.Item("objcolhJoin") = "LEFT JOIN prcostcenter_master AS ecct ON ecct.costcenterunkid = CC.costcenterunkid"
                            dtRow.Item("objcolhDisplay") = ",[" & dtRow.Item("Name").ToString() & "]"
                    End Select
                Next

                lvDisplayCol.DataSource = mdtDisplayCol
                lvDisplayCol.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMData = Nothing : dsList = Nothing
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dList As New DataSet
        Try
            Select Case cboAllocations.SelectedValue
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.GetList("List")
                    Call Fill_List(dList.Tables(0), "stationunkid", "name")
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "deptgroupunkid", "name")
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.GetList("List")
                    Call Fill_List(dList.Tables(0), "departmentunkid", "name")
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectiongroupunkid", "name")
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.GetList("List")
                    Call Fill_List(dList.Tables(0), "sectionunkid", "name")
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitgroupunkid", "name")
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.GetList("List")
                    Call Fill_List(dList.Tables(0), "unitunkid", "name")
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.GetList("List")
                    Call Fill_List(dList.Tables(0), "teamunkid", "name")
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobgroupunkid", "name")
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.GetList("List")
                    Call Fill_List(dList.Tables(0), "jobunkid", "JobName")
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.GetList("List")
                    Call Fill_List(dList.Tables(0), "classgroupunkid", "name")
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.GetList("List")
                    Call Fill_List(dList.Tables(0), "classesunkid", "name")
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_List(ByVal dTable As DataTable, ByVal StrIdColName As String, ByVal StrDisColName As String)
        Try
            mdtAllocation = New DataTable("List")
            Dim dCol As New DataColumn
            dCol.ColumnName = "Id"
            dCol.DataType = GetType(System.Int32)
            dCol.DefaultValue = 0
            mdtAllocation.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "Name"
            dCol.DataType = GetType(System.String)
            dCol.DefaultValue = ""
            mdtAllocation.Columns.Add(dCol)


            'Gajanan [17-SEP-2019] -- Start    
            'Enhancement:Working On Assesment Score report
            'dCol = New DataColumn
            'dCol.ColumnName = "ischeck"
            'dCol.DataType = GetType(System.Boolean)
            'dCol.DefaultValue = False
            'mdtAllocation.Columns.Add(dCol)
            'Gajanan [17-SEP-2019] -- End

            For Each dtRow As DataRow In dTable.Rows
                Dim drow As DataRow = mdtAllocation.NewRow()
                drow.Item("Id") = dtRow.Item(StrIdColName)
                drow.Item("Name") = dtRow.Item(StrDisColName)
                mdtAllocation.Rows.Add(drow)
            Next

            lvAllocation.Columns(1).HeaderText = cboAllocations.SelectedItem.Text
            lvAllocation.DataSource = mdtAllocation
            lvAllocation.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillPeriod(ByVal intYearId As Integer)
        Try
            If intYearId > 0 Then
                Dim dsList As New DataSet
                Dim ObjPeriod As New clscommom_period_Tran
                dsList = ObjPeriod.getListForCombo(enModuleReference.Assessment, intYearId, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", False, 0, False)
                ObjPeriod = Nothing

                dgvPeriod.DataSource = dsList.Tables(0)
                dgvPeriod.DataBind()


            Else
                dgvPeriod.DataSource = Nothing
            End If
        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End
        Finally
        End Try
    End Sub
#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)

            objScoreRating = New clsAssessSoreRatingReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        If Not IsPostBack Then
            Call FillCombo()
        End If

        'S.SANDEEP |27-MAY-2019| -- START
        'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
        If CBool(Session("IsCalibrationSettingActive")) Then
            lblDisplayScore.Visible = True : cboScoreOption.Visible = True
        Else
            lblDisplayScore.Visible = False : cboScoreOption.Visible = False
        End If
        'S.SANDEEP |27-MAY-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If SetFilter() = False Then Exit Sub
            Call SetDateFormat()
            Dim strFileName As String = String.Empty
            Select Case cboSubReportType.SelectedIndex
                Case 0  'EMPLOYEE BASED 



                    If dgvPeriod.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkpbox"), CheckBox).Checked = True).Count <= 0 Then
                        objScoreRating.Export_Assessment_Report(CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                False, CInt(Session("UserId")), CInt(Session("CompanyUnkId")), strFileName)

                    Else
                        objScoreRating.Export_Assessment_Report_MultiPeriod(CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                            IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                                            False, CInt(Session("UserId")), CInt(Session("CompanyUnkId")), strFileName)

                    End If


                Case 1  'ALLOCATION BASED

                    'S.SANDEEP |27-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                    'objScoreRating.Report_Allocation_Based_New(Session("Database_Name").ToString, _
                    '                                           CInt(Session("UserId")), _
                    '                                           CInt(Session("Fin_year")), _
                    '                                           CInt(Session("CompanyUnkId")), _
                    '                                           CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                    '                                           CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                    '                                           CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

                    If dgvPeriod.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CType(x.FindControl("chkpbox"), CheckBox).Checked = True).Count <= 0 Then
                        objScoreRating.Report_Allocation_Based_New(Session("Database_Name").ToString, _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                   CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                   CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False, CBool(Session("IsCalibrationSettingActive")))
                    Else
                        objScoreRating.Report_Allocation_Based_MultiPeriod(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                               CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                               CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False, CBool(Session("IsCalibrationSettingActive")))
                    End If
                    'S.SANDEEP |27-MAY-2019| -- END


                Case 2  'ALLOCATION BASED (FINCA)
                    objScoreRating.Report_Based_On_Competencies(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                                CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)
            End Select

            If strFileName.Trim.Length <= 0 Then strFileName = objScoreRating._FileNameAfterExported.Trim

            If strFileName.Trim <> "" Then

                'S.SANDEEP |24-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : SCORE ROUNDING OPTION
                'Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
                If strFileName.Contains(".xls") = False Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName & ".xls"
                Else
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & strFileName
                End If
                'S.SANDEEP |24-DEC-2019| -- END
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
                Export.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "


    'Gajanan [17-SEP-2019] -- Start    
    'Enhancement:Working On Assesment Score report
    Protected Sub cboSubReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSubReportType.SelectedIndexChanged
        Try
            Select Case cboSubReportType.SelectedIndex
                Case 0
                    pnlAllocation.Enabled = False
                    txtSearch.Text = ""
                    cboEmployee.Enabled = True
                    txtSearch.Enabled = False
                    pnlCondition.Enabled = False
                    mstrAdvanceFilter = ""
                    chkConsiderEmployeeTermination.Enabled = False
                    chkConsiderEmployeeTermination.Checked = False
                    chkShowEmpSalary.Checked = False
                    dtpAppDate.Enabled = False
                    txtSearch.Enabled = True
                    If cboReportType.SelectedIndex = 0 Then objsp1.Enabled = False
                    If cboReportType.SelectedIndex = 0 Then cboPeriod.Enabled = True


                Case 1, 2
                    cboEmployee.SelectedValue = 0
                    cboEmployee.Enabled = False

                    'Gajanan [17-SEP-2019] -- Start    
                    'Enhancement:Working On Assesment Score report
                    pnlAllocation.Enabled = True
                    pnlCondition.Enabled = True
                    'Gajanan [17-SEP-2019] -- End


                    chkConsiderEmployeeTermination.Enabled = True
                    chkConsiderEmployeeTermination.Checked = False

                    Call Fill_Column_List()
                    If Me.ViewState("mdtDisplayCol") IsNot Nothing Then
                        Me.ViewState("mdtDisplayCol") = mdtDisplayCol
                    Else
                        Me.ViewState.Add("mdtDisplayCol", mdtDisplayCol)
                    End If
                    Call cboAllocations_SelectedIndexChanged(New Object(), New EventArgs())
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [17-SEP-2019] -- End

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            If cboReportType.SelectedIndex <= 0 Then
                drpFinancialYear.SelectedValue = 0
                objsp1.Enabled = False
                If cboSubReportType.Items.Count > 0 Then cboSubReportType.SelectedIndex = 0
                If cboReportType.SelectedIndex = 0 Then cboPeriod.Enabled = True
            Else
                objsp1.Enabled = True
                cboPeriod.SelectedValue = 0
                cboPeriod.Enabled = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboFiancialYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpFinancialYear.SelectedIndexChanged
        Try
            Call FillPeriod(drpFinancialYear.SelectedValue)
        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "cboFiancialYear_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End           
        Finally
        End Try
    End Sub

    Private Sub cboAllocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocations.SelectedIndexChanged
        Try
            Call Fill_Data()
            If Me.ViewState("mdtAllocation") IsNot Nothing Then
                Me.ViewState("mdtAllocation") = mdtAllocation
            Else
                Me.ViewState.Add("mdtAllocation", mdtAllocation)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                mdtStartDate = objPrd._Start_Date
                mdtEndDate = objPrd._End_Date
                objPrd = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Event(s) "

    Protected Sub txtFrmSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim dView As DataView = CType(Me.ViewState("mdtAllocation"), DataTable).DefaultView
            Dim StrSearch As String = String.Empty
            If txtSearch.Text.Trim.Length > 0 Then
                StrSearch = "Name LIKE '%" & txtSearch.Text & "%' "
            End If
            dView.RowFilter = StrSearch
            lvAllocation.DataSource = dView
            lvAllocation.DataBind()
            lvAllocation.Focus()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure txtFrmSearch_TextChanged : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    'Protected Sub chkpHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If lvDisplayCol.Items.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim chkValue As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = CType(Me.ViewState("mdtDisplayCol"), DataTable).DefaultView
    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvItem As DataGridItem = lvDisplayCol.Items(j)
    '            CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtDisplayCol"), DataTable).Select("Id = '" & gvItem.Cells(5).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '            j += 1
    '        Next
    '        Me.ViewState("mdtDisplayCol") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkpbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        If gvItem.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtDisplayCol"), DataTable).Select("Id = '" & gvItem.Cells(5).Text & "'")
    '            dRow(0).Item("ischeck") = cb.Checked
    '            CType(Me.ViewState("mdtDisplayCol"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub


    'Gajanan [17-SEP-2019] -- Start    
    'Enhancement:Working On Assesment Score report

    'Protected Sub chkaHeder_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If lvAllocation.Items.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim chkValue As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = CType(Me.ViewState("mdtAllocation"), DataTable).DefaultView
    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvItem As DataGridItem = lvAllocation.Items(j)
    '            CType(gvItem.FindControl("chkabox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtAllocation"), DataTable).Select("Id = '" & gvItem.Cells(2).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '            j += 1
    '        Next
    '        Me.ViewState("mdtAllocation") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkaHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkabox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        If gvItem.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtAllocation"), DataTable).Select("Id = '" & gvItem.Cells(2).Text & "'")
    '            dRow(0).Item("ischeck") = cb.Checked
    '            CType(Me.ViewState("mdtAllocation"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkabox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    'Gajanan [17-SEP-2019] -- End

    Private Sub chkOption_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOption.CheckedChanged
        Try
            cboCondition.Enabled = chkOption.Checked
            dtpAppDate.Enabled = chkOption.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Gajanan [17-SEP-2019] -- Start    
    'Enhancement:Working On Assesment Score report
    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If lvDisplayCol.Items.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim chkValue As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = CType(Me.ViewState("mdtDisplayCol"), DataTable).DefaultView
    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvItem As DataGridItem = lvDisplayCol.Items(j)
    '            CType(gvItem.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtDisplayCol"), DataTable).Select("Id = '" & gvItem.Cells(5).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '            j += 1
    '        Next
    '        Me.ViewState("mdtDisplayCol") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvItem As DataGridItem = DirectCast(cb.NamingContainer, DataGridItem)
    '        If gvItem.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("mdtDisplayCol"), DataTable).Select("Id = '" & gvItem.Cells(5).Text & "'")
    '            dRow(0).Item("ischeck") = cb.Checked
    '            CType(Me.ViewState("mdtDisplayCol"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub
    'Gajanan [17-SEP-2019] -- End


#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is mandatory information. Please select employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage(mstrModuleName, 3, "Based on Employee")
            Language.setMessage(mstrModuleName, 4, "Based on Allocation")
            Language.setMessage(mstrModuleName, 5, "Grade Level")
            Language.setMessage(mstrModuleName, 7, "Sorry, condition is mandatory information.")
            Language.setMessage(mstrModuleName, 8, "Grade Group")
            Language.setMessage(mstrModuleName, 9, "Grade")
            Language.setMessage(mstrModuleName, 10, "Please select atleast one allocation to export report.")
            Language.setMessage(mstrModuleName, 11, "Please select atleast one column to display in report.")
            Language.setMessage(mstrModuleName, 12, "Based on Competencies")
            Language.setMessage(mstrModuleName, 13, "Based On Single Period")
            Language.setMessage(mstrModuleName, 14, "Based On Multiple Period")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
