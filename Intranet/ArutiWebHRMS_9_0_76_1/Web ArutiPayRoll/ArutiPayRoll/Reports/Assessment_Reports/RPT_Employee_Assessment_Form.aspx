﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="RPT_Employee_Assessment_Form.aspx.vb" Inherits="Reports_RPT_Employee_Assessment_Form" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

       <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Assessemt Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <asp:Panel ID="pnlMandatroy" runat="server">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td colspan="2">
                                                    <asp:Label ID="gbFilterCriteria" runat="server" Text="Mandatory Info" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboEmployee" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlNotHTMLTemp" runat="server">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:CheckBox ID="chkProbationTemplate" runat="server" Text="Probation Template" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <%--'S.SANDEEP |27-MAY-2019| -- START--%>
                                    <%--'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]--%>
                                    <asp:Panel ID="pnlCalibrationScore" runat="server">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblDisplayScore" runat="server" Text="Display Score"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboScoreOption" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <%--'S.SANDEEP |27-MAY-2019| -- END--%>
                                    <asp:Panel ID="pnlPeriodSelection" runat="server">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td colspan="2">
                                                    <asp:Label ID="gbMultiperiod" runat="server" Text="Period Selection" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblFromPeriod" runat="server" Text="From Period"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboFromPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblToPeriod" runat="server" Text="To Period"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboToPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlYearSelection" runat="server" Visible="false">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td colspan="2">
                                                    <asp:Label ID="gbYearlyBasis" runat="server" Text="Year Selection" Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblYear" runat="server" Text="From Year"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboFromYear" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 25%">
                                                    <asp:Label ID="lblToYear" runat="server" Text="To Year"></asp:Label>
                                                </td>
                                                <td style="width: 75%">
                                                    <asp:DropDownList ID="cboToYear" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="BtnReport" runat="server" Text="Report" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupEmpAssessForm" runat="server" CancelControlID="btnEmpReportingClose"
                        PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2" Drag="true" PopupDragHandleControlID="pnlEmpReporting">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="newpopup" Style="width: 90%;
                        height: 588px" DefaultButton="btnEmpReportingClose" ScrollBars="Auto">
                        <table>
                            <tr>
                                <td>
                                    <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="btn-default">
                                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
