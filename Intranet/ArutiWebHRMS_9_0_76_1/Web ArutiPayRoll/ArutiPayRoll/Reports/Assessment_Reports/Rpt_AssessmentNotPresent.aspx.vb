﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Assessment_Reports_Rpt_AssessmentNotPresent
    Inherits Basepage

#Region " Prviate Variables "

    Private ReadOnly mstrModuleName As String = "frmNotAssessedEmployeeReport"
    Private objNAReport As clsNotAssessedEmployee_Report
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAdvanceFilter As String = String.Empty
    Private DisplayMessage As New CommonCodes

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name").ToString, Session("fin_startdate"), "Period", True, 1)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0                
            End With

            With cboViewMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 2, "General Assessment"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "BSC Assessment"))
            End With

            With cboReportMode
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 4, "Select"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Self Assessment"))
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Assessor Assessment"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Reviewer Assessment"))
            End With

            dsList = objMaster.GetAD_Parameter_List(False, "List")
            With cboAppointment
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 1
            End With
            Call cboAppointment_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboPeriod.SelectedValue = 0
            cboViewMode.SelectedIndex = 0
            cboReportMode.SelectedIndex = 0
            mstrAdvanceFilter = ""
            cboAppointment.SelectedValue = 1
            chkConsiderEmployeeTermination.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Please select Period."), Me)
                cboPeriod.Focus()
                Return False
            End If

            If CInt(cboReportMode.SelectedIndex) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Please select Report Mode."), Me)
                cboReportMode.Focus()
                Return False
            End If

            If cboAppointment.SelectedValue = enAD_Report_Parameter.APP_DATE_FROM Then
                If dtpDate1.IsNull = True AndAlso dtpDate2.IsNull = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, Appointment To Date is mandatory information. Please set Appointment To Date."), Me)
                    dtpDate2.Focus()
                    Return False
                ElseIf dtpDate1.IsNull = False AndAlso dtpDate2.IsNull = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, Appointment From Date is mandatory information. Please set Appointment From Date."), Me)
                    dtpDate1.Focus()
                    Return False
                ElseIf dtpDate1.IsNull = True AndAlso dtpDate2.IsNull = True Then
                    If dtpDate2.GetDate.Date < dtpDate1.GetDate.Date Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, Appointment To Date cannot be less than Appointment From Date."), Me)
                        dtpDate2.Focus()
                        Return False
                    End If
                End If
            End If

            objNAReport.SetDefaultValue()

            objNAReport._Advance_Filter = mstrAdvanceFilter
            objNAReport._ViewByIds = mstrStringIds
            objNAReport._ViewIndex = mintViewIdx
            objNAReport._ViewByName = mstrStringName
            objNAReport._Analysis_Fields = mstrAnalysis_Fields
            objNAReport._Analysis_Join = mstrAnalysis_Join
            objNAReport._Analysis_OrderBy = mstrAnalysis_OrderBy
            objNAReport._Report_GroupName = mstrReport_GroupName
            objNAReport._ModeSelectedId = cboViewMode.SelectedIndex
            objNAReport._ModeSelectedName = cboViewMode.Text
            objNAReport._PeriodId = CInt(cboPeriod.SelectedValue)
            objNAReport._PeriodName = cboPeriod.Text
            objNAReport._ReportModeId = cboReportMode.SelectedIndex
            objNAReport._ReportModeName = cboReportMode.Text
            objNAReport._AppointmentTypeId = cboAppointment.SelectedValue
            objNAReport._AppointmentTypeName = cboAppointment.Text
            If dtpDate1.IsNull = False Then
                objNAReport._Date1 = dtpDate1.GetDate.Date
            End If

            If dtpDate2.Visible = True Then
                If dtpDate2.IsNull = False Then
                    objNAReport._Date2 = dtpDate2.GetDate.Date
                End If
            End If
            objNAReport._ConsiderEmployeeTerminationOnPeriodDate = chkConsiderEmployeeTermination.Checked

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub BtnReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objNAReport._UserUnkId = CInt(Session("UserId"))
            objNAReport.Export_Not_Assessed_Report(Session("Database_Name").ToString, _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                   CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                   CStr(Session("UserAccessModeSetting")), True, IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If objNAReport._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objNAReport._FileNameAfterExported
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Forms Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            objNAReport = New clsNotAssessedEmployee_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

        If Not IsPostBack Then
            Call FillCombo()
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboAppointment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAppointment.SelectedIndexChanged
        Try
            objlblCaption.Text = cboAppointment.SelectedItem.Text
            Select Case CInt(cboAppointment.SelectedValue)
                Case enAD_Report_Parameter.APP_DATE_FROM
                    If dtpDate2.Visible = False Then dtpDate2.Visible = True
                    If lblTo.Visible = False Then lblTo.Visible = True
                Case enAD_Report_Parameter.APP_DATE_BEFORE
                    dtpDate2.Visible = False : lblTo.Visible = False
                Case enAD_Report_Parameter.APP_DATE_AFTER
                    dtpDate2.Visible = False : lblTo.Visible = False
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
