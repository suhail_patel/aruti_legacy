﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="Rpt_Performance_AppraisalReport.aspx.vb" Inherits="Reports_Assessment_Reports_Rpt_Performance_AppraisalReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-ui.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 40%;">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Performance Evaluation Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%" valign="top">
                                                <asp:Label ID="lblGradeGroup" runat="server" Text="Grade Group"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:Panel ID="Panel2" runat="server" Width="99%" Height="180px" ScrollBars="Auto">
                                                    <asp:DataGrid ID="lvGradeGroup" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateColumn ItemStyle-Width="25">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Grade Groups" />
                                                            <asp:BoundColumn DataField="gradegroupunkid" HeaderText="Id" Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width :100%">
                                            <td style="width:20%"></td>
                                            <td style="width:80%">
                                            <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include inactive employee" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

