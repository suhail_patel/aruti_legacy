﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region


Partial Class Reports_OT_Requisition_Reports_Rpt_OT_Requisition_SummaryReport
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Dim objOTRequisitionDetail As New clsOTRequisition_Report
    'Pinkal (03-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmOTRequisition_SummaryReport"
    Private mdtPeriodStartDate As DateTime = Nothing
    Private mdtPeriodEndDate As DateTime = Nothing

    Private mstrViewByIds As String = String.Empty
    Private mintViewIndex As Integer = 0
    Private mstrViewByName As String = String.Empty
    Private mstrAnalysisFields As String = String.Empty
    Private mstrAnalysisJoin As String = String.Empty
    Private mstrAnalysisOrderBy As String = String.Empty
    Private mstrAnalysisOrderByGName As String = String.Empty
    Private mstrReportGroupName As String = String.Empty


#End Region

#Region "Private Function"

    Public Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            Dim objEmp As New clsEmployee_Master

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 Session("UserAccessModeSetting").ToString, True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "Employee", _
                                                 blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
            End With
            objEmp = Nothing

            dsList.Clear()
            dsList = Nothing
            'Dim objPeriod As New clscommom_period_Tran
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), FinancialYear._Object._Database_Start_Date.Date _
            '                                                   , "List", True, 0, False)
            'With cboPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0).Copy
            '    .DataBind()
            '    .SelectedValue = "0"
            'End With
            'objPeriod = Nothing

            'dsList.Clear()
            'dsList = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            'cboPeriod.SelectedIndex = 0
            'cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs())
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboEmployee.SelectedIndex = 0
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Public Function SetFilter() As Boolean
    Public Function SetFilter(ByVal objOTRequisitionDetail As clsOTRequisition_Report) As Boolean
        'Pinkal (03-Sep-2020) -- End
        Try
            objOTRequisitionDetail.SetDefaultValue()
            objOTRequisitionDetail._ReportName = lblPageHeader.Text
            'objOTRequisitionDetail._PeriodId = CInt(cboPeriod.SelectedValue)
            'objOTRequisitionDetail._Period = cboPeriod.SelectedItem.Text
            objOTRequisitionDetail._EmployeeID = CInt(cboEmployee.SelectedValue)
            objOTRequisitionDetail._EmployeeName = cboEmployee.SelectedItem.Text
            objOTRequisitionDetail._PeriodStartDate = dtpFromDate.GetDate.Date
            objOTRequisitionDetail._PeriodEndDate = dtpToDate.GetDate.Date
            objOTRequisitionDetail._NormalOTTranHeadId = 0
            objOTRequisitionDetail._HolidayOTTranHeadId = 0
            objOTRequisitionDetail._ViewByIds = mstrViewByIds
            objOTRequisitionDetail._ViewIndex = mintViewIndex
            objOTRequisitionDetail._ViewByName = mstrViewByName
            objOTRequisitionDetail._Analysis_Fields = mstrAnalysisFields
            objOTRequisitionDetail._Analysis_Join = mstrAnalysisJoin
            objOTRequisitionDetail._Analysis_OrderBy = mstrAnalysisOrderBy
            objOTRequisitionDetail._Analysis_OrderBy_GName = mstrAnalysisOrderByGName
            objOTRequisitionDetail._Report_GroupName = mstrReportGroupName
            objOTRequisitionDetail._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objOTRequisitionDetail._OpenAfterExport = False

            GUI.fmtCurrency = Session("fmtCurrency").ToString()
            objOTRequisitionDetail._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objOTRequisitionDetail._UserUnkId = CInt(Session("UserId"))
            objOTRequisitionDetail._UserAccessFilter = CStr(Session("AccessLevelFilterString"))

            If CInt(Session("Employeeunkid")) > 0 Then
                objOTRequisitionDetail._UserName = Session("DisplayName").ToString()
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then objOTRequisitionDetail._IncludeAccessFilterQry = False

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If Not IsPostBack Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End
                SetLanguage()
                Call FillCombo()
                ResetValue()
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then lnkAnalysisBy.Visible = False
            Else
                mdtPeriodStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtPeriodEndDate = CDate(Me.ViewState("PeriodEndDate"))
                mstrViewByIds = CStr(Me.ViewState("ViewByIds"))
                mintViewIndex = CInt(Me.ViewState("ViewIndex"))
                mstrViewByName = CStr(Me.ViewState("ViewByName"))
                mstrAnalysisFields = CStr(Me.ViewState("AnalysisFields"))
                mstrAnalysisJoin = CStr(Me.ViewState("AnalysisJoin"))
                mstrAnalysisOrderBy = CStr(Me.ViewState("AnalysisOrderBy"))
                mstrAnalysisOrderByGName = CStr(Me.ViewState("AnalysisOrderByGName"))
                mstrReportGroupName = CStr(Me.ViewState("ReportGroupName"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("PeriodEndDate") = mdtPeriodEndDate
            Me.ViewState("ViewByIds") = mstrViewByIds
            Me.ViewState("ViewIndex") = mintViewIndex
            Me.ViewState("ViewByName") = mstrViewByName
            Me.ViewState("AnalysisFields") = mstrAnalysisFields
            Me.ViewState("AnalysisJoin") = mstrAnalysisJoin
            Me.ViewState("AnalysisOrderBy") = mstrAnalysisOrderBy
            Me.ViewState("AnalysisOrderByGName") = mstrAnalysisOrderByGName
            Me.ViewState("ReportGroupName") = mstrReportGroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objOTRequisitionDetail As clsOTRequisition_Report
        'Pinkal (03-Sep-2020) -- End

        Try
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period."), Me)
            '    cboPeriod.Focus()
            '    Exit Sub
            'End If

            SetDateFormat()


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'If SetFilter() = False Then Exit Sub
            objOTRequisitionDetail = New clsOTRequisition_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            If SetFilter(objOTRequisitionDetail) = False Then Exit Sub
            'Pinkal (03-Sep-2020) -- End




            'Pinkal (05-May-2020) -- Start
            'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
            'objOTRequisitionDetail.Generate_OTRequisitionSummaryReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
            '                                                                       , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(mdtPeriodEndDate), Session("UserAccessModeSetting").ToString() _
            '                                                                       , True, True, True)

            objOTRequisitionDetail.Generate_OTRequisitionSummaryReport(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                                   , CInt(Session("CompanyUnkId")), eZeeDate.convertDate(dtpToDate.GetDate.Date), Session("UserAccessModeSetting").ToString() _
                                                                                     , True, True, True)
            'Pinkal (05-May-2020) -- End

          


            If objOTRequisitionDetail._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objOTRequisitionDetail._FileNameAfterExported
                Export.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objOTRequisitionDetail = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub


#End Region

#Region "Combobox Event"

    'Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '    Try
    '        If CInt(cboPeriod.SelectedValue) > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
    '            LblPeriodValue.Text = objPeriod._TnA_StartDate.Date & " - " & objPeriod._TnA_EndDate.Date
    '            mdtPeriodStartDate = objPeriod._TnA_StartDate.Date
    '            mdtPeriodEndDate = objPeriod._TnA_EndDate.Date
    '        Else
    '            LblPeriodValue.Text = ""
    '            mdtPeriodStartDate = Nothing
    '            mdtPeriodEndDate = Nothing
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region "LinkButton Events"

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrViewByIds = popupAnalysisBy._ReportBy_Ids
            mstrViewByName = popupAnalysisBy._ReportBy_Name
            mintViewIndex = popupAnalysisBy._ViewIndex
            mstrAnalysisFields = popupAnalysisBy._Analysis_Fields
            mstrAnalysisJoin = popupAnalysisBy._Analysis_Join
            mstrAnalysisOrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReportGroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysisOrderByGName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrViewByIds = ""
            mstrViewByName = ""
            mintViewIndex = 0
            mstrAnalysisFields = ""
            mstrAnalysisJoin = ""
            mstrAnalysisOrderBy = ""
            mstrAnalysisOrderByGName = ""
            mstrReportGroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)

        Me.lnkAnalysisBy.Text = Language._Object.getCaption(Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
        Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.ID, Me.lblFromDate.Text)
        Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.ID, Me.lblToDate.Text)
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)

        Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
        Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Period is compulsory information.Please Select Period.")
            Language.setMessage(mstrModuleName, 2, "Transaction Head mapping is compulsory information.Please map any one transaction head.")
            Language.setMessage(mstrModuleName, 3, "Selection Saved Successfully.")
            Language.setMessage(mstrModuleName, 4, "OT Requisition Detail Report")
            Language.setMessage(mstrModuleName, 5, "OT Requisition Summary Report")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
