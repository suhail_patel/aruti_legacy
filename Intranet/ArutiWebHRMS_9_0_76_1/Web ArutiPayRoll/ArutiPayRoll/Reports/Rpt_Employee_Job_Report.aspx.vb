﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Rpt_Employee_Job_Report
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmJobReport"
    Private objJob As clsJobReport
    Private mstrStringIds As String = String.Empty
    Private mstrStringName As String = String.Empty
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""

    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region " Private Function "
    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objClass As New clsClass
            Dim objDept As New clsDepartment
            Dim objJob As New clsJobs

            dsList = objJob.getComboList("Job", True)
            cboJob.DataTextField = "name"
            cboJob.DataValueField = "jobunkid"
            cboJob.DataSource = dsList.Tables("Job")
            cboJob.DataBind()
            'Hemant (22 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            If Session("intJobunkid") IsNot Nothing Then
                cboJob.SelectedValue = CInt(Session("intJobunkid"))
                cboJob_SelectedIndexChanged(cboJob, New EventArgs)
            End If
            'Hemant (22 Aug 2019) -- End

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)

            cboEmployee.DataTextField = "EmpCodeName"
            cboEmployee.DataValueField = "employeeunkid"
            cboEmployee.DataSource = dsList.Tables("List")
            cboEmployee.DataBind()

            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : Job report displayed on staff requisition and approval pages should be as per the NMB template.)
            Dim objclsMasterData As New clsMasterData
            dsList = objclsMasterData.getComboListForJobReportTemplate("JobReportList")
            cboReportType.DataValueField = "Id"
            cboReportType.DataTextField = "Name"
            cboReportType.DataSource = dsList.Tables("JobReportList")
            cboReportType.DataBind()
            'Hemant (15 Nov 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            objJob.setDefaultOrderBy(0)
            cboJob.SelectedIndex = 0

            cboEmployee.SelectedValue = 0
            cboJob.Enabled = True
            cboEmployee.Enabled = True
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = -1
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        Try
            Language.setLanguage(mstrModuleName)

            If CInt(cboJob.SelectedValue) <= 0 AndAlso CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select Job Or Employee to view this report."), Me)
                Exit Function
            End If

            objJob.SetDefaultValue()

            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objCategorize As New clsemployee_categorization_Tran
                Dim dsJobList As DataSet = objCategorize.Get_Current_Job(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CInt(cboEmployee.SelectedValue))
                If dsJobList IsNot Nothing AndAlso dsJobList.Tables(0).Rows.Count > 0 Then
                    objJob._JobId = CInt(dsJobList.Tables(0).Rows(0)("jobunkid"))
                End If
                dsJobList = Nothing
                objCategorize = Nothing

                Dim objGrade As New clsMasterData
                Dim dsGrade As DataSet = objGrade.Get_Current_Scale("List", CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                If dsGrade IsNot Nothing AndAlso dsGrade.Tables(0).Rows.Count > 0 Then
                    objJob._GradeId = dsGrade.Tables(0).Rows(0)("gradeunkid")
                    objJob._EmployeeGrade = dsGrade.Tables(0).Rows(0)("Grade")
                End If

                objJob._EmployeeId = CInt(cboEmployee.SelectedValue)
                objJob._EmployeeName = cboEmployee.SelectedItem.ToString.Split("-")(1).Trim
            Else
                objJob._JobId = CInt(cboJob.SelectedValue)
                objJob._JobName = cboJob.SelectedItem.Text
            End If
            objJob._EmployeeAsOnDate = ConfigParameter._Object._EmployeeAsOnDate

            objJob._ReportId = CInt(cboReportType.SelectedValue) 'Hemant (15 Nov 2019)

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function
#End Region

#Region " Forms "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objJob = New clsJobReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            SetLanguage()

            If Not IsPostBack Then
                Call FillCombo()
                'Hemant (22 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                If CInt(Session("intJobunkid")) <= 0 Then
                    'Hemant (22 Aug 2019) -- End
                    Call ResetValue()
                End If
                Session("intJobunkid") = Nothing
                'Hemant (22 Aug 2019)
                'Sohail (19 Mar 2020) -- Start
                'NMB Enhancement # : Job listing report should be selected as default report type.
                cboReportType.SelectedValue = CInt(enJob_Report_Template.Job_Listing_Report).ToString
                'Sohail (19 Mar 2020) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If Not SetFilter() Then Exit Sub

            objJob.generateReportNew(Session("Database_Name"), _
                                                   Session("UserId"), _
                                                   Session("Fin_year"), _
                                                   Session("CompanyUnkId"), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   Session("UserAccessModeSetting"), True, _
                                                   Session("ExportReportPath"), _
                                                   Session("OpenAfterExport"), _
                                                   0, enPrintAction.None, enExportAction.None)

            Session("objRpt") = objJob._Rpt
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            'Hemant (03 Sep 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No - 14 : Upon clicking close on the report opened, system should go back to the approval/rejection page for that staff requisition ).
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
                'Hemant (03 Sep 2019) -- End
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If 'Hemant (03 Sep 2019)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "ComboBox Event"

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim cbo As DropDownList = CType(sender, DropDownList)

            If cbo.ID.ToUpper = "CBOJOB" Then
                If CInt(cboJob.SelectedValue) <= 0 Then
                    cboEmployee.Enabled = True
                Else
                    cboEmployee.Enabled = False
                End If
            ElseIf cbo.ID.ToUpper = "CBOEMPLOYEE" Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    cboJob.Enabled = True
                Else
                    cboJob.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Protected Sub cboemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.DataBound
        Try
        If cboEmployee.Items.Count > 0 Then
            For Each lstItem As ListItem In cboEmployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            cboEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        
    End Sub

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.ID, Me.lblJob.Text)
        Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.ID, Me.lblReportType.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please Select Job Or Employee to view this report.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
