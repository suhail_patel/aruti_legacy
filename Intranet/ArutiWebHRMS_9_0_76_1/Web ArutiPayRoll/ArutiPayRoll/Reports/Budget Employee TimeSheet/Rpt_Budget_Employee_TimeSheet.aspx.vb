﻿
#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Reports_Budget_Employee_TimeSheet_Rpt_Budget_Employee_TimeSheet
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmEmpBudgetTimesheetReport"
    Dim objEmpTimesheet As clsEmpBudgetTimesheetReport
    Private mdtTable As DataTable = Nothing

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private mstrAllocationIds As String = ""
    'Pinkal (28-Jul-2018) -- End

#End Region

#Region "Constructor"
    Public Sub New()
        'objEmpTimesheet = New clsEmpBudgetTimesheetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
        'objEmpTimesheet.SetDefaultValue()
    End Sub
#End Region

#Region " Page's Events "
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objEmpTimesheet = New clsEmpBudgetTimesheetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            Call SetLanguage()

            If IsPostBack = False Then
                Call FillCombo()
                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                GetValue()
                'Pinkal (28-Jul-2018) -- End
            Else
                mdtTable = CType(Me.ViewState("EmployeeTable"), DataTable)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("EmployeeTable") = mdtTable
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing

            Dim objPeriod As New clscommom_period_Tran
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")).Date, "List", True)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objPeriod = Nothing

            Dim objEmployee As New clsEmployee_Master

            Dim intEmployeeID As Integer = 0
            Dim blnIncludeAccessFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                intEmployeeID = CInt(Session("Employeeunkid"))
                blnIncludeAccessFilter = False
            End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "EmpList", True, intEmployeeID, , , , , , , , , , , , , , , , blnIncludeAccessFilter)

            mdtTable = dsList.Tables("EmpList")

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = mdtTable
                .DataBind()
                If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    .SelectedValue = CStr(Session("Employeeunkid"))
                Else
                    .SelectedValue = "0"
                End If
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try
            objEmpTimesheet = New clsEmpBudgetTimesheetReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            objEmpTimesheet.SetDefaultValue()


            'Pinkal (22-Jul-2019) -- Start
            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
            'objEmpTimesheet._EmployeeID = CInt(cboEmployee.SelectedValue)
            'Dim dRow As DataRow() = mdtTable.Select("employeeunkid=" & CInt(cboEmployee.SelectedValue))
            'If dRow.Length > 0 Then
            '    objEmpTimesheet._EmployeeName = CStr(dRow(0).Item("employeename"))
            '    objEmpTimesheet._EmployeeCode = CStr(dRow(0).Item("employeecode"))
            'End If
            'Pinkal (22-Jul-2019) -- End

            objEmpTimesheet._PeriodId = CInt(cboPeriod.SelectedValue)
            objEmpTimesheet._PeriodName = cboPeriod.SelectedItem.Text


            'Pinkal (22-Jul-2019) -- Start
            'Enhancement [0003971] - An Audit report to show when time and date which the timesheet was submitted and approved/rejected.
            objEmpTimesheet._dtEmployee = mdtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(cboEmployee.SelectedValue)).ToList().CopyToDataTable()
            'Pinkal (22-Jul-2019) -- End


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            objEmpTimesheet._StartDate = objPeriod._TnA_StartDate.Date
            objEmpTimesheet._EndDate = objPeriod._TnA_EndDate.Date
            objPeriod = Nothing

            objEmpTimesheet._ViewReportInHTML = chkShowReportInHtml.Checked
            If CInt(Session("Employeeunkid")) > 0 Then
                objEmpTimesheet._UserName = Session("DisplayName").ToString()
            End If

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                objEmpTimesheet._UserUnkId = CInt(Session("UserId"))
            End If

            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            objEmpTimesheet._ShowSubmissionRemark = chkShowSubmissionRemark.Checked
            'Pinkal (28-Jul-2018) -- End

            objEmpTimesheet._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objEmpTimesheet._OpenAfterExport = False

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

    Public Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet

        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.Emplyoee_Project_Code_Timesheet_Report)

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrAllocationIds = dsList.Tables(0).Rows(0)("transactionheadid").ToString()
                If mstrAllocationIds.Trim.Length > 0 Then
                    Dim ar() As String = mstrAllocationIds.Trim().Split(Convert.ToChar(","))
                    If ar.Length > 0 Then
                        chkShowReportInHtml.Checked = CBool(ar(0))
                        chkShowSubmissionRemark.Checked = CBool(ar(1))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub

    'Pinkal (28-Jul-2018) -- End

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboPeriod.SelectedValue = "0"
            cboEmployee.SelectedValue = "0"
            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            chkShowReportInHtml.Checked = False
            'Nilay (01 Apr 2017) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            'Hemant (13 Aug 2020) -- Start
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End         
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory information.Please Select period."), Me)
                cboPeriod.Focus()
                Exit Sub
            ElseIf CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Employee is compulsory information.Please Select employee."), Me)
                cboEmployee.Focus()
                Exit Sub
            End If

            If SetFilter() = False Then Exit Sub

            Call SetDateFormat()

            'Nilay (21 Mar 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'objEmpTimesheet.Generate_DetailReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString, _
            '                                      CStr(Session("UserAccessModeSetting")), True)
            objEmpTimesheet.Generate_DetailReport(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString, _
                                                  CStr(Session("UserAccessModeSetting")), True, False)
            'Nilay (21 Mar 2017) -- End



            If objEmpTimesheet._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objEmpTimesheet._FileNameAfterExported
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog('" & Session("rootpath").ToString & "');", True)
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnExport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Protected Sub btnSaveSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveSettings.Click
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim intUnkid As Integer = -1
        Dim mblnFlag As Boolean = False
        Try

            objUserDefRMode._Reportunkid = enArutiReport.Emplyoee_Project_Code_Timesheet_Report
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportmodeid = 0
            objUserDefRMode._Headtypeid = 1
            objUserDefRMode._EarningTranHeadIds = IIf(chkShowReportInHtml.Checked, 1, 0) & "," & IIf(chkShowSubmissionRemark.Checked, 1, 0)
            intUnkid = objUserDefRMode.isExist(enArutiReport.Emplyoee_Project_Code_Timesheet_Report, 0, 0, objUserDefRMode._Headtypeid)

            objUserDefRMode._Reportmodeunkid = intUnkid

            If intUnkid <= 0 Then
                mblnFlag = objUserDefRMode.Insert()
            Else
                mblnFlag = objUserDefRMode.Update()
            End If

            If mblnFlag Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Selection Saved Successfully."), Me)
            Else
                DisplayMessage.DisplayMessage(objUserDefRMode._Message, Me)
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Pinkal (28-Jul-2018) -- End


#End Region

#Region " Language "
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.LblPeriod.Text = Language._Object.getCaption(Me.LblPeriod.ID, Me.LblPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

End Class
