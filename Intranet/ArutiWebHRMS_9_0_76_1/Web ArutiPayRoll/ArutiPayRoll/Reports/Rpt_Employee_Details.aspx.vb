﻿
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data


Partial Class Reports_Rpt_Employee_Details
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objEmployeeBiodata As clsEmployeeBiodata


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeBiodata"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim dsList As DataSet
        Dim objEmployee As New clsEmployee_Master

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsList = objEmployee.GetEmployeeList("List", True, CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, eZeeDate.convertDate(Session("EmployeeAsOnDate")).ToShortDateString, , , Session("AccessLevelFilterString"))
            'Else
            '    dsList = objEmployee.GetEmployeeList("List", True, Not CBool(Session("IsIncludeInactiveEmp")), , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If

            dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True)

            'Shani(24-Aug-2015) -- End

            With drpemployee
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Private Sub ResetValue()

        Try
            drpemployee.SelectedValue = 0
            chkIsDomicile.Checked = False

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Checked = False
            'Pinkal (24-Apr-2013) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ResetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Function setFilter() As Boolean

        Try
            objEmployeeBiodata.SetDefaultValue()
            objEmployeeBiodata._EmployeeId = drpemployee.SelectedValue

            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            'objEmployeeBiodata._EmployeeName = drpemployee.Text
            objEmployeeBiodata._EmployeeName = drpemployee.SelectedItem.Text
            'Pinkal (24-Apr-2013) -- End


            If chkIsDomicile.Checked = True Then
                objEmployeeBiodata._IsDomicileAddress = True
            Else
                objEmployeeBiodata._IsDomicileAddress = False
            End If


            'Pinkal (03-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objEmployeeBiodata._IsImageInDb = Session("IsImgInDataBase")
            'Pinkal (03-Apr-2013) -- End


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objEmployeeBiodata._ShowEmployeeScale = chkShowEmpScale.Checked
            'Pinkal (24-Apr-2013) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objEmployeeBiodata._FormateCurrency = Session("fmtCurrency")
            'Shani(24-Aug-2015) -- End

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                objEmployeeBiodata._IncludeAccessFilterQry = False
            End If
            'Shani(15-Feb-2016) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            objEmployeeBiodata._AsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            'Sohail (18 May 2019) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("setFilter:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Function

#End Region

#Region " Forms "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            objEmployeeBiodata = New clsEmployeeBiodata(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If Not IsPostBack Then
                Call FillCombo()
            End If

            'Pinkal (30-Apr-2013) -- Start
            'Enhancement : TRA Changes
            chkShowEmpScale.Visible = Session("AllowToViewScale")
            'Pinkal (30-Apr-2013) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If drpemployee.SelectedValue <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Employee is mandatory information.Please select Employee."), Me)
                'Pinkal (06-May-2014) -- End
                drpemployee.Focus()
                Exit Sub
            End If

            If Not setFilter() Then Exit Sub
            objEmployeeBiodata._CompanyUnkId = Session("CompanyUnkId")
            objEmployeeBiodata._UserUnkId = Session("UserId")

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployeeBiodata.generateReport(0, enPrintAction.None, enExportAction.None)
            objEmployeeBiodata.generateReportNew(Session("Database_Name"), _
                                                 Session("UserId"), _
                                                 Session("Fin_year"), _
                                                 Session("CompanyUnkId"), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                 Session("UserAccessModeSetting"), True, _
                                                 Session("ExportReportPath"), _
                                                 Session("OpenAfterExport"), 0, _
                                                 enPrintAction.None, enExportAction.None, Session("Base_CurrencyId"))
            'Shani(24-Aug-2015) -- End

            Session("objRpt") = objEmployeeBiodata._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")

            'Shani(15-Feb-2016) -- Start
            'Report not showing Data in ESS due to Access Level Filter
            If Session("objRpt") IsNot Nothing Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            End If
            'Shani(15-Feb-2016) -- End	

            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END
#End Region

    Protected Sub drpemployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpemployee.DataBound
        Try         'Hemant (13 Aug 2020)
        If drpemployee.Items.Count > 0 Then
            For Each lstItem As ListItem In drpemployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            drpemployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End



        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.chkIsDomicile.Text = Language._Object.getCaption(Me.chkIsDomicile.ID, Me.chkIsDomicile.Text)
        Me.chkShowEmpScale.Text = Language._Object.getCaption(Me.chkShowEmpScale.ID, Me.chkShowEmpScale.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End


End Class
