﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO
Imports System.Text
Imports System.Data

#End Region

Partial Class wPg_TrainingImpactEvaluation
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Private objImpactMst As clshrtnatraining_impact_master
    Private objImpactKRA As clshrtnatraining_impact_kra
    Private objImpactDevelopment As clshrtnatraining_impact_development
    Private objImpactObjective As clshrtnatraining_impact_objective
    Private objImpactFeedBack As clshrtnatraining_impact_feedback
    Private mintImpactUnkId As Integer

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmTrainingImpact"
    'Anjan [04 June 2014] -- End

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End


            objImpactMst = New clshrtnatraining_impact_master
            objImpactKRA = New clshrtnatraining_impact_kra
            objImpactDevelopment = New clshrtnatraining_impact_development
            objImpactObjective = New clshrtnatraining_impact_objective
            objImpactFeedBack = New clshrtnatraining_impact_feedback



            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmTrainingImpact"
            StrModuleName2 = "mnuTrainingInformation"
            StrModuleName3 = "mnuTrainingEvaluation"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END



            Dim clsuser As New User
            clsuser = CType(Session("clsuser"), User)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Me.ViewState("Empunkid") = clsuser.UserID
                Me.ViewState("Empunkid") = Session("UserId")
                'Sohail (30 Mar 2015) -- End
                'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)
                pnlPartA.Enabled = True
                pnlPartB.Enabled = False
                pnlPartC.Enabled = True

                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                btnFinalSave.Visible = CBool(Session("AllowToSave_CompleteLevelIIIEvaluation"))
                'Sohail (23 Nov 2012) -- End

            Else
                'Aruti.Data.User._Object._Userunkid = -1 'Sohail (23 Apr 2012)
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                Me.ViewState("Empunkid") = Session("Employeeunkid")
                'Sohail (30 Mar 2015) -- End
                pnlPartA.Enabled = False
                pnlPartB.Enabled = True
                pnlPartC.Enabled = False
            End If

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'SHANI [01 FEB 2015]--END

            If (Page.IsPostBack = False) Then

                FillCombo()
                FillQuestion()
                BtnBack.Enabled = False
                BtnNext.Enabled = False
                If Request.QueryString("ProcessId") <> "" Then
                    mintImpactUnkId = Val(b64decode(Request.QueryString("ProcessId")))
                    Me.ViewState.Add("ImpactunkID", mintImpactUnkId)
                    objImpactMst._Impactunkid = mintImpactUnkId
                    Me.ViewState("ItemAKPI") = objImpactMst._dsKRAList.Tables(0)
                    Me.ViewState("ItemADevelopmentGAP") = objImpactMst._dsDevelopmentList.Tables(0)
                    Me.ViewState("ItemBObjective") = objImpactMst._dsObjectiveList.Tables(0)
                    Me.ViewState("FeedBack") = objImpactMst._dsFeedBackList.Tables(0)
                    drpCourse.Enabled = False
                    drpEmployee.Enabled = False
                    GetValue()
                Else
                    Me.ViewState("FeedBack") = objImpactFeedBack._FeedBackList.Tables(0)
                    FillFeedBackList()
                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    If Session("Training_EmpUnkID") IsNot Nothing Then
                        drpEmployee.SelectedValue = Session("Training_EmpUnkID")
                    End If
                    'SHANI [09 Mar 2015]--END
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleNewButton(False)
        'ToolbarEntry1.VisibleModeFormButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    'Private Function b64decode(ByVal StrDecode As String) As String
    '    Dim decodedString As String = ""
    '    Try
    '        decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    '    Return decodedString
    'End Function

    Private Sub FillCombo()
        Dim dsList As DataSet = Nothing
        Try


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            Dim objCourse As New clsTraining_Scheduling
            dsList = objCourse.getComboList("List", True, -1, True)
            drpCourse.DataTextField = "Name"
            drpCourse.DataValueField = "Id"
            drpCourse.DataSource = dsList.Tables(0)
            Me.ViewState("course") = dsList.Tables(0)
            drpCourse.DataBind()

            Dim objEmployee As New clsEmployee_Master
            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If

                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Employee", True)

                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 04 MAY 2012 ] -- END

                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()


                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    dsList = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If

                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Employee", True)

                'Shani(24-Aug-2015) -- End
                'S.SANDEEP [ 04 MAY 2012 ] -- END
            End If

            With drpLineManager
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0).Copy
                Me.ViewState("SuperVisor") = dsList.Tables(0).Copy()
                .DataBind()
            End With

            'drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())
            drpCourse_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillQuestion()
        Dim dsList As DataSet = Nothing
        Try
            Dim objFeedBackItem As New clshrtnafdbk_item_master
            dsList = objFeedBackItem.GetList("Question", True, True)

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("fdbkitemunkid") = 0
            drRow("name") = "Select"
            drRow("resultgroupunkid") = 0
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            drpQuestion.DataTextField = "name"
            drpQuestion.DataValueField = "fdbkitemunkid"
            drpQuestion.DataSource = dsList.Tables(0)
            drpQuestion.DataBind()
            Me.ViewState("Question") = dsList.Tables(0)

            drpQuestion_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetPartACaption()
        Try
            If Me.ViewState("coursetypeid") Is Nothing Then Exit Sub


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            If CInt(Me.ViewState("coursetypeid")) = enCourseType.Job_Capability Then


                LblItemA.Text = Language.getMessage(mstrModuleName, 1, "Item 1(a):")
                LblItemACaption.Text = Language.getMessage(mstrModuleName, 2, "Job Capability or Training Related Performance Gap identified during Continuous Performance Assessment or Appraisal Process.")

                lblItemAKPI.Text = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                lblItemAJobCapability.Text = Language.getMessage(mstrModuleName, 4, "Identified Job Capability or Training Related Performance Deficiency/Gap")
                GvItemAKPIList.Columns(2).HeaderText = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                gvItemAJobCapability.Columns(2).HeaderText = Language.getMessage(mstrModuleName, 4, "Identified Job Capability or Training Related Performance Deficiency/Gap")

                lblPartBDesc.Text = Language.getMessage(mstrModuleName, 5, "Item 1(b):")
                LblItemBCaption.Text = Language.getMessage(mstrModuleName, 6, "Training/Programme Objective(s) or Expected Outcomes after attending the training: This should address Job Capability or Training Related Performance Gap Identified on Item 1(a). (Your expectation on what the staff will be able to do at workplace after attending the training/programme).")
                lblItemBTrainingObjective.Text = Language.getMessage(mstrModuleName, 8, "Training Objective")
                gvItemBKPIList.Columns(0).HeaderText = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                gvItemBList.Columns(2).HeaderText = Language.getMessage(mstrModuleName, 8, "Training Objective")

            ElseIf CInt(Me.ViewState("coursetypeid")) = enCourseType.Career_Development Then

                LblItemA.Text = Language.getMessage(mstrModuleName, 9, "Item 2(a):")
                LblItemACaption.Text = Language.getMessage(mstrModuleName, 10, "Career Development Need Identified during Continuous Performance Assessment or Appraisal Process.")

                lblItemAKPI.Text = Language.getMessage(mstrModuleName, 11, "Key Performance Indicator or Key Results Area for senior position.")
                lblItemAJobCapability.Text = Language.getMessage(mstrModuleName, 12, "Identified Career Development Gap")
                GvItemAKPIList.Columns(2).HeaderText = Language.getMessage(mstrModuleName, 11, "Key Performance Indicator or Key Results Area for senior position.")
                gvItemAJobCapability.Columns(2).HeaderText = Language.getMessage(mstrModuleName, 12, "Identified Career Development Gap")


                lblPartBDesc.Text = Language.getMessage(mstrModuleName, 13, "Item 2(b):")
                LblItemBCaption.Text = Language.getMessage(mstrModuleName, 14, "Training/Programme Objective(s) or Expected Outcome after attending the training: This should address the identified Career Development Gap Identified on Item 2(a). (Your expectation on what the staff needs to have for preparation for immediate higher responsibilities or senior position).")
                lblItemBTrainingObjective.Text = Language.getMessage(mstrModuleName, 15, "Training/Programme Objective")
                gvItemBKPIList.Columns(0).HeaderText = Language.getMessage(mstrModuleName, 3, "Key Performance Indicator or Key Results Area")
                gvItemBList.Columns(2).HeaderText = Language.getMessage(mstrModuleName, 15, "Training/Programme Objective")

            End If
            'Anjan [04 June 2014] -- End



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetPartBCaption()
        Try

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            lblPartBDesc.Text = Language.getMessage(mstrModuleName, 27, "Six (6) Months ago you attended the above mentioned training/programme. As part of level three training impact evaluation process you are requested to provide Self Assessment on Competence enhancement/Improvement in relation to the explanations provided on Part A item 1(b).")
            lblPartBJobCapabilities.Text = Language.getMessage(mstrModuleName, 28, "Job capabilities enhanced/Improved after attending the training/programme.")
            lblNametask.Text = Language.getMessage(mstrModuleName, 29, "Name or Describe tasks that you are able to perform competently to the required standards after attending the Training in relation to Items 1(a).")
            'Anjan [04 June 2014] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetPartCCaption()
        Try

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            lblPartCDesc.Text = Language.getMessage(mstrModuleName, 30, "Six (6) Months ago a member of your staff attended the above training programme. It is expected that following his/her return you have been giving him/her support, coaching and guidance to facilitate the transfer of the learning into the workplace for enhanced competences in the prior identified Job capability/training related performance gap and career Development Needs. To that end as part of Level Three Training Impact Evaluation Process you are kindly requested to briefly answer the following questions and return the form to us.")
            'Anjan [04 June 2014] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            Language.setLanguage(mstrModuleName)
            If CInt(drpCourse.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Course is compulsory information.Please Select Course."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                drpCourse.Focus()
                Return False

            ElseIf CInt(drpEmployee.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Employee is compulsory information.Please Select Employee."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End

                drpEmployee.Focus()
                Return False

            ElseIf CInt(drpLineManager.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Line Manager/Supervisor is compulsory information.Please Select Line Manager/Supervisor."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                drpLineManager.Focus()
                Return False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub FillPartAItemAKPIList()
        Try
            Dim dtItemAKPI As DataTable = Me.ViewState("ItemAKPI")

            If dtItemAKPI IsNot Nothing Then

                Dim dtTable As DataTable = New DataView(dtItemAKPI, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

                If (Not dtTable Is Nothing) Then

                    GvItemAKPIList.DataSource = dtTable
                    GvItemAKPIList.DataBind()

                    gvItemBKPIList.DataSource = dtTable
                    gvItemBKPIList.DataBind()

                End If

            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                GvItemAKPIList.CurrentPageIndex = 0
                GvItemAKPIList.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Private Sub FillPartAItemA_DeveoplmentGAPList()
        Try

            Dim dtItemADevelopmentGAP As DataTable = Me.ViewState("ItemADevelopmentGAP")
            If dtItemADevelopmentGAP IsNot Nothing Then

                Dim dtTable As DataTable = New DataView(dtItemADevelopmentGAP, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

                If (Not dtTable Is Nothing) Then
                    gvItemAJobCapability.DataSource = dtTable
                    gvItemAJobCapability.DataBind()
                End If
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                gvItemAJobCapability.CurrentPageIndex = 0
                gvItemAJobCapability.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Private Sub FillPartAItemB_ObjectiveList()
        Try
            Dim dtItemBObjective As DataTable = Me.ViewState("ItemBObjective")
            If dtItemBObjective IsNot Nothing Then

                Dim dtTable As DataTable = New DataView(dtItemBObjective, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

                If (Not dtTable Is Nothing) Then
                    gvItemBList.DataSource = dtTable
                    gvItemBList.DataBind()
                End If

            End If
        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                gvItemBList.CurrentPageIndex = 0
                gvItemBList.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Private Sub FillPartBKPIList()
        Try

            Dim dtItemAKPI As DataTable = Me.ViewState("ItemAKPI")

            If dtItemAKPI IsNot Nothing Then

                Dim dtTable As DataTable = New DataView(dtItemAKPI, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

                If (Not dtTable Is Nothing) Then

                    GvPartBKRA.DataSource = dtTable
                    GvPartBKRA.DataBind()
                End If

            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                GvPartBKRA.CurrentPageIndex = 0
                GvPartBKRA.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Public Sub FillFeedBackList()
        Try
            Dim dtItemFeedback As DataTable = Me.ViewState("FeedBack")
            If dtItemFeedback IsNot Nothing Then

                Dim dtTable As DataTable = New DataView(dtItemFeedback, "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

                If (Not dtTable Is Nothing) Then

                    GvFeedBackList.DataSource = dtTable
                    GvFeedBackList.DataBind()
                End If

            Else
                GvFeedBackList.DataSource = dtItemFeedback
                GvFeedBackList.DataBind()
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                GvFeedBackList.CurrentPageIndex = 0
                GvFeedBackList.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Private Sub DeleteItemAKPI()
        Try
            Dim dtTable As DataTable = Me.ViewState("ItemAKPI")

            Dim drRow As DataRow() = dtTable.Select("kra='" & GvItemAKPIList.Items(Me.ViewState("Index")).Cells(2).Text.Trim & "' AND AUD <> 'D'")

            If drRow.Length > 0 Then

                If Me.ViewState("ImpactunkID") IsNot Nothing Then



                    If (popupDelete.Reason.Trim = "") Then  'SHANI [01 FEB 2015] -- If (txtreasondel.Text = "") Then
                        DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)
                        popupDelete.Show()
                        Exit Sub
                    End If

                    drRow(0)("AUD") = "D"
                    drRow(0)("isvoid") = True
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'drRow(0)("voiduserunkid") = Aruti.Data.User._Object._Userunkid
                    drRow(0)("voiduserunkid") = Session("UserId")
                    'Sohail (23 Apr 2012) -- End
                    drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    drRow(0)("voidreason") = popupDelete.Reason.Trim 'SHANI [01 FEB 2015] -- txtreasondel.Text.Trim
                Else
                    drRow(0)("AUD") = "D"
                End If

                drRow(0).AcceptChanges()
            End If

            Me.ViewState("ItemAKPI") = dtTable

            FillPartAItemAKPIList()
            FillPartBKPIList()
            txtItemAKPI.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub DeleteItemAGAP()
        Try
            Dim dtTable As DataTable = Me.ViewState("ItemADevelopmentGAP")

            Dim drRow As DataRow() = dtTable.Select("development_area='" & gvItemAJobCapability.Items(Me.ViewState("Index")).Cells(2).Text.Trim & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                If Me.ViewState("ImpactunkID") IsNot Nothing Then

                    If (popupDelete.Reason.Trim = "") Then 'SHANI [01 FEB 2015] -- If (txtreasondel.Text = "") Then
                        DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)
                        popupDelete.Show()
                        Exit Sub
                    End If

                    drRow(0)("AUD") = "D"
                    drRow(0)("isvoid") = True
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'drRow(0)("voiduserunkid") = Aruti.Data.User._Object._Userunkid
                    drRow(0)("voiduserunkid") = Session("UserId")
                    'Sohail (23 Apr 2012) -- End
                    drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    drRow(0)("voidreason") = popupDelete.Reason.Trim 'SHANI [01 FEB 2015] -- txtreasondel.Text.Trim
                Else
                    drRow(0)("AUD") = "D"
                End If
                drRow(0).AcceptChanges()
            End If
            Me.ViewState("ItemADevelopmentGAP") = dtTable
            FillPartAItemA_DeveoplmentGAPList()
            txtItemAJobCapability.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub DeleteItemBObjective()
        Try
            Dim dtTable As DataTable = Me.ViewState("ItemBObjective")

            Dim drRow As DataRow() = dtTable.Select("training_objective='" & gvItemBList.Items(Me.ViewState("Index")).Cells(2).Text.Trim & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                If Me.ViewState("ImpactunkID") IsNot Nothing Then

                    If (popupDelete.Reason.Trim = "") Then 'SHANI [01 FEB 2015] -- If (txtreasondel.Text = "") Then
                        DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)
                        popupDelete.Show()
                        Exit Sub
                    End If

                    drRow(0)("AUD") = "D"
                    drRow(0)("isvoid") = True
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'drRow(0)("voiduserunkid") = Aruti.Data.User._Object._Userunkid
                    drRow(0)("voiduserunkid") = Session("UserId")
                    'Sohail (23 Apr 2012) -- End
                    drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    drRow(0)("voidreason") = popupDelete.Reason.Trim 'SHANI [01 FEB 2015] -- txtreasondel.Text.Trim
                Else
                    drRow(0)("AUD") = "D"
                End If
                drRow(0).AcceptChanges()
            End If
            Me.ViewState("ItemBObjective") = dtTable
            FillPartAItemB_ObjectiveList()
            txtItemBTrainingObjective.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub DeleteItemFeedBack()
        Try
            Dim dtTable As DataTable = Me.ViewState("FeedBack")

            Dim drRow As DataRow() = dtTable.Select("question='" & GvFeedBackList.Items(Me.ViewState("Index")).Cells(2).Text.Trim & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                If Me.ViewState("ImpactunkID") IsNot Nothing Then

                    If (popupDelete.Reason.Trim = "") Then 'SHANI [01 FEB 2015] -- If (txtreasondel.Text = "") Then
                        DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)
                        popupDelete.Show()
                        Exit Sub
                    End If

                    drRow(0)("AUD") = "D"
                    drRow(0)("isvoid") = True
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'drRow(0)("voiduserunkid") = Aruti.Data.User._Object._Userunkid
                    drRow(0)("voiduserunkid") = Session("UserId")
                    'Sohail (23 Apr 2012) -- End
                    drRow(0)("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    drRow(0)("voidreason") = popupDelete.Reason.Trim 'SHANI [01 FEB 2015] -- txtreasondel.Text.Trim
                Else
                    drRow(0)("AUD") = "D"
                End If
                drRow(0).AcceptChanges()
            End If
            Me.ViewState("FeedBack") = dtTable
            FillFeedBackList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            drpCourse.SelectedValue = objImpactMst._Schedulingid
            drpEmployee.SelectedValue = objImpactMst._Employeeunkid

            drpCourse_SelectedIndexChanged(New Object(), New EventArgs())
            'drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            drpLineManager.SelectedValue = objImpactMst._Managerunkid
            txtPartBNameTask.Text = objImpactMst._Self_Remark
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objImpactMst._Schedulingid = CInt(drpCourse.SelectedValue)
            objImpactMst._Courseunkid = CInt(Me.ViewState("courseunkid"))
            objImpactMst._Employeeunkid = CInt(drpEmployee.SelectedValue)
            objImpactMst._Managerunkid = CInt(drpLineManager.SelectedValue)
            objImpactMst._Self_Remark = txtPartBNameTask.Text.Trim
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objImpactMst._Userunkid = Aruti.Data.User._Object._Userunkid
            objImpactMst._Userunkid = Session("UserId")
            'Sohail (23 Apr 2012) -- End


            '======= START FOR KRA============

            Dim dtTable As DataTable = Me.ViewState("ItemAKPI")

            If dtTable.Rows.Count > 0 Then
                objImpactKRA._KRAList.Tables(0).Rows.Clear()
                For i As Integer = 0 To dtTable.Rows.Count - 1

                    Dim drRow As DataRow = objImpactKRA._KRAList.Tables(0).NewRow
                    drRow("kra") = dtTable.Rows(i)("kra").ToString()

                    If Me.ViewState("ImpactunkID") Is Nothing Or CInt(Me.ViewState("ImpactunkID")) <= 0 Then
                        If dtTable.Rows(i)("AUD").ToString() = "" Then
                            drRow("AUD") = "A"
                        Else
                            drRow("AUD") = dtTable.Rows(i)("AUD").ToString()
                        End If
                        drRow("kraunkid") = -1
                        drRow("impactunkid") = -1
                    Else
                        drRow("kraunkid") = IIf(dtTable.Rows(i)("kraunkid").ToString() = "", -1, dtTable.Rows(i)("kraunkid"))
                        drRow("impactunkid") = IIf(dtTable.Rows(i)("impactunkid").ToString() = "", -1, dtTable.Rows(i)("impactunkid"))
                        drRow("AUD") = dtTable.Rows(i)("AUD").ToString()
                        drRow("isvoid") = IIf(dtTable.Rows(i)("isvoid").ToString() = "", False, dtTable.Rows(i)("isvoid"))
                        drRow("voiduserunkid") = IIf(dtTable.Rows(i)("voiduserunkid").ToString() = "", -1, dtTable.Rows(i)("voiduserunkid"))
                        drRow("voiddatetime") = IIf(dtTable.Rows(i)("voiddatetime").ToString() = "", DBNull.Value, dtTable.Rows(i)("voiddatetime"))
                        drRow("voidreason") = IIf(dtTable.Rows(i)("voidreason").ToString() = "", "", dtTable.Rows(i)("voidreason"))
                    End If
                    drRow("GUID") = dtTable.Rows(i)("GUID").ToString()
                    objImpactKRA._KRAList.Tables(0).Rows.Add(drRow)
                Next


            End If
            objImpactMst._dsKRAList = objImpactKRA._KRAList

            '======= END FOR KRA============


            '======= START FOR DEVELOPMENT============
            dtTable = Nothing
            dtTable = Me.ViewState("ItemADevelopmentGAP")

            If dtTable.Rows.Count > 0 Then
                objImpactDevelopment._DevelopmentList.Tables(0).Rows.Clear()

                For i As Integer = 0 To dtTable.Rows.Count - 1

                    Dim drRow As DataRow = objImpactDevelopment._DevelopmentList.Tables(0).NewRow
                    drRow("development_area") = dtTable.Rows(i)("development_area").ToString()

                    If Me.ViewState("ImpactunkID") Is Nothing Or CInt(Me.ViewState("ImpactunkID")) <= 0 Then
                        drRow("developmentunkid") = -1
                        drRow("impactunkid") = -1
                        If dtTable.Rows(i)("AUD").ToString() = "" Then
                            drRow("AUD") = "A"
                        Else
                            drRow("AUD") = dtTable.Rows(i)("AUD").ToString()
                        End If
                    Else
                        drRow("developmentunkid") = IIf(dtTable.Rows(i)("developmentunkid").ToString() = "", -1, dtTable.Rows(i)("developmentunkid"))
                        drRow("impactunkid") = IIf(dtTable.Rows(i)("impactunkid").ToString() = "", -1, dtTable.Rows(i)("impactunkid"))
                        drRow("AUD") = dtTable.Rows(i)("AUD").ToString()
                        drRow("isvoid") = IIf(dtTable.Rows(i)("isvoid").ToString() = "", False, dtTable.Rows(i)("isvoid"))
                        drRow("voiduserunkid") = IIf(dtTable.Rows(i)("voiduserunkid").ToString() = "", -1, dtTable.Rows(i)("voiduserunkid"))
                        drRow("voiddatetime") = IIf(dtTable.Rows(i)("voiddatetime").ToString() = "", DBNull.Value, dtTable.Rows(i)("voiddatetime"))
                        drRow("voidreason") = IIf(dtTable.Rows(i)("voidreason").ToString() = "", "", dtTable.Rows(i)("voidreason"))
                    End If
                    drRow("GUID") = dtTable.Rows(i)("GUID").ToString()
                    objImpactDevelopment._DevelopmentList.Tables(0).Rows.Add(drRow)
                Next


            End If
            objImpactMst._dsDevelopmentList = objImpactDevelopment._DevelopmentList

            '======= END FOR DEVELOPMENT============

            '======= START FOR OBJECTIVE============
            dtTable = Nothing
            dtTable = Me.ViewState("ItemBObjective")

            If dtTable.Rows.Count > 0 Then
                objImpactObjective._ObjectiveList.Tables(0).Rows.Clear()

                For i As Integer = 0 To dtTable.Rows.Count - 1

                    Dim drRow As DataRow = objImpactObjective._ObjectiveList.Tables(0).NewRow
                    drRow("training_objective") = dtTable.Rows(i)("training_objective").ToString()

                    If Me.ViewState("ImpactunkID") Is Nothing Or CInt(Me.ViewState("ImpactunkID")) <= 0 Then
                        drRow("objectiveunkid") = -1
                        drRow("impactunkid") = -1
                        If dtTable.Rows(i)("AUD").ToString() = "" Then
                            drRow("AUD") = "A"
                        Else
                            drRow("AUD") = dtTable.Rows(i)("AUD").ToString()
                        End If
                    Else
                        drRow("objectiveunkid") = IIf(dtTable.Rows(i)("objectiveunkid").ToString() = "", -1, dtTable.Rows(i)("objectiveunkid"))
                        drRow("impactunkid") = IIf(dtTable.Rows(i)("impactunkid").ToString() = "", -1, dtTable.Rows(i)("impactunkid"))
                        drRow("AUD") = dtTable.Rows(i)("AUD").ToString()
                        drRow("isvoid") = IIf(dtTable.Rows(i)("isvoid").ToString() = "", False, dtTable.Rows(i)("isvoid"))
                        drRow("voiduserunkid") = IIf(dtTable.Rows(i)("voiduserunkid").ToString() = "", -1, dtTable.Rows(i)("voiduserunkid"))
                        drRow("voiddatetime") = IIf(dtTable.Rows(i)("voiddatetime").ToString() = "", DBNull.Value, dtTable.Rows(i)("voiddatetime"))
                        drRow("voidreason") = IIf(dtTable.Rows(i)("voidreason").ToString() = "", "", dtTable.Rows(i)("voidreason"))
                    End If
                    drRow("GUID") = dtTable.Rows(i)("GUID").ToString()
                    objImpactObjective._ObjectiveList.Tables(0).Rows.Add(drRow)
                Next



            End If
            objImpactMst._dsObjectiveList = objImpactObjective._ObjectiveList

            '======= END FOR OBJECTIVE============


            '======= START FOR FEEDBACK============
            dtTable = Nothing
            dtTable = Me.ViewState("FeedBack")

            For i As Integer = 0 To dtTable.Rows.Count - 1
                dtTable.Rows(i)("feedbackunkid") = IIf(dtTable.Rows(i)("feedbackunkid").ToString() = "", -1, dtTable.Rows(i)("feedbackunkid"))
                dtTable.Rows(i)("impactunkid") = IIf(dtTable.Rows(i)("impactunkid").ToString() = "", -1, dtTable.Rows(i)("impactunkid"))
                dtTable.Rows(i)("AUD") = dtTable.Rows(i)("AUD").ToString()
                dtTable.Rows(i)("isvoid") = IIf(dtTable.Rows(i)("isvoid").ToString() = "", False, dtTable.Rows(i)("isvoid"))
                dtTable.Rows(i)("voiduserunkid") = IIf(dtTable.Rows(i)("voiduserunkid").ToString() = "", -1, dtTable.Rows(i)("voiduserunkid"))
                dtTable.Rows(i)("voiddatetime") = IIf(dtTable.Rows(i)("voiddatetime").ToString() = "", DBNull.Value, dtTable.Rows(i)("voiddatetime"))
                dtTable.Rows(i)("voidreason") = IIf(dtTable.Rows(i)("voidreason").ToString() = "", "", dtTable.Rows(i)("voidreason"))
            Next
            Dim mdsFeedback As New DataSet
            mdsFeedback.Tables.Add(dtTable)
            objImpactMst._dsFeedBackList = mdsFeedback

            '======= END FOR FEEDBACK============

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            drpCourse.SelectedIndex = 0

            drpEmployee.SelectedIndex = 0
            'drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            drpCourse_SelectedIndexChanged(New Object(), New EventArgs())


            drpLineManager.SelectedIndex = 0
            txtItemAKPI.Text = ""
            TxtComments.Text = ""
            txtItemAJobCapability.Text = ""
            txtItemBTrainingObjective.Text = ""
            txtPartBNameTask.Text = ""

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreasondel.Text = "" 
            'SHANI [01 FEB 2015]--END
            mvwImpact.ActiveViewIndex = 0
            BtnBack.Enabled = False
            BtnNext.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearViewState()
        Try
            Me.ViewState("ImpactunkID") = Nothing
            Me.ViewState("courseunkid") = Nothing
            Me.ViewState("coursetypeid") = Nothing
            Me.ViewState("ItemAKPI") = Nothing
            Me.ViewState("ItemADevelopmentGAP") = Nothing
            Me.ViewState("ItemBObjective") = Nothing
            Me.ViewState("FeedBack") = Nothing
            Me.ViewState("Question") = Nothing
            Me.ViewState("GridID") = Nothing
            Me.ViewState("Index") = Nothing
            FillFeedBackList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combo's Event(s) "

    Private Sub drpCourse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpCourse.SelectedIndexChanged, drpEmployee.SelectedIndexChanged
        Try

            If Me.ViewState("course") IsNot Nothing AndAlso CInt(drpCourse.SelectedValue) > 0 Then

                mvwImpact.Visible = True
                BtnNext.Enabled = True

                Dim dr As DataRow() = CType(Me.ViewState("course"), DataTable).Select("Id = " & CInt(drpCourse.SelectedValue))

                If dr.Length > 0 Then

                    Me.ViewState("courseunkid") = CInt(dr(0)("courseunkid"))
                    Me.ViewState("coursetypeid") = CInt(dr(0)("coursetypeid"))

                    If Not IsDBNull(dr(0)("start_date")) Then
                        dtStartDate.SetDate = CDate(dr(0)("start_date")).Date
                    End If

                    If Not IsDBNull(dr(0)("end_date")) Then
                        dtEndDate.SetDate = CDate(dr(0)("end_date")).Date
                    End If

                    SetPartACaption()
                    SetPartBCaption()
                    SetPartCCaption()

                Else
                    Me.ViewState("courseunkid") = Nothing
                    Me.ViewState("coursetypeid") = Nothing

                End If

            Else
                mvwImpact.ActiveViewIndex = 0
                mvwImpact.Visible = False
                BtnBack.Enabled = False
                BtnNext.Enabled = False
                dtStartDate.SetDate = Nothing
                dtEndDate.SetDate = Nothing
            End If


            If Me.ViewState("SuperVisor") IsNot Nothing Then
                Dim dtList As DataTable = Me.ViewState("SuperVisor")
                With drpLineManager
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        .DataSource = New DataView(dtList, "employeeunkid <> " & CInt(drpEmployee.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
                    Else
                        .DataSource = dtList
                    End If
                    .DataBind()
                End With
            End If

            If Me.ViewState("ImpactunkID") Is Nothing Then

                '============ START FOR PART A ITEM 1(A) KPI/KRA LIST
                Dim mdtItemAKPI As DataSet = objImpactMst.GetKRAFromTrainingPriority(CInt(Me.ViewState("courseunkid")), CInt(drpEmployee.SelectedValue))
                Me.ViewState("ItemAKPI") = mdtItemAKPI.Tables(0)
                FillPartAItemAKPIList()
                '============ END FOR PART A ITEM 1(A) KPI/KRA LIST

                '============ START FOR PART A ITEM 1(A) OR ITEM 2(A) DEVELOPMENT GAP LIST
                Dim mdtItemADevelopmentGAP As DataSet = objImpactMst.GetDevelopmentGAPFromAssessment(CInt(Me.ViewState("courseunkid")), CInt(drpEmployee.SelectedValue))
                Me.ViewState("ItemADevelopmentGAP") = mdtItemADevelopmentGAP.Tables(0)
                FillPartAItemA_DeveoplmentGAPList()
                '============ END FOR PART A ITEM 1(A) OR ITEM 2(A)  DEVELOPMENT GAP LIST

                '============ START FOR PART B ITEM 1(B) OR ITEM 2(B) OBJECTIVE LIST
                Dim mdtItemBObjective As DataSet = mdtItemAKPI.Copy
                Me.ViewState("ItemBObjective") = mdtItemBObjective.Tables(0)
                FillPartAItemB_ObjectiveList()
                '============ END FOR PART B ITEM 1(B) OR ITEM 2(B) OBJECTIVE LIST

                FillPartBKPIList()
            Else
                FillPartAItemAKPIList()
                FillPartAItemA_DeveoplmentGAPList()
                FillPartAItemB_ObjectiveList()
                FillPartBKPIList()
                FillFeedBackList()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub drpQuestion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpQuestion.SelectedIndexChanged
        Dim dsList As DataSet
        Try
            If Me.ViewState("Question") IsNot Nothing Then

                Dim dr As DataRow() = CType(Me.ViewState("Question"), DataTable).Select("fdbkitemunkid = " & CInt(drpQuestion.SelectedValue))

                If CInt(dr(0)("resultgroupunkid")) >= 0 Then
                    Dim objResult As New clsresult_master
                    dsList = objResult.getComboList("Result", True, CInt(dr(0)("resultgroupunkid")))
                    drpAnswer.DataTextField = "name"
                    drpAnswer.DataValueField = "Id"
                    drpAnswer.DataSource = dsList.Tables(0)
                    drpAnswer.DataBind()
                    lblAnswer.Visible = True
                    drpAnswer.Visible = True
                Else
                    lblAnswer.Visible = False
                    drpAnswer.Visible = False
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Button's Event"

    Protected Sub btnAddItemAKPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemAKPI.Click
        Try
            If txtItemAKPI.Text.Length > 0 Then

                Dim dtTable As DataTable = Me.ViewState("ItemAKPI")

                Dim dRow As DataRow() = dtTable.Select("kra='" & txtItemAKPI.Text.Trim & "' AND AUD <> 'D'")
                If dRow.Length > 0 Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "This KPI/KRA is already exist in the list.Please define new KPI/KRA."), Me)
                    'Anjan [04 June 2014] -- End
                    txtItemAKPI.Focus()
                    Exit Sub
                End If

                Dim drRow As DataRow = dtTable.NewRow
                drRow("kra") = txtItemAKPI.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                dtTable.Rows.Add(drRow)

                Me.ViewState("ItemAKPI") = dtTable

                FillPartAItemAKPIList()
                FillPartAItemB_ObjectiveList()
                FillPartBKPIList()
                txtItemAKPI.Text = ""

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEditItemAKPI_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItemAKPI.Click
        Try
            Language.setLanguage(mstrModuleName)
            If GvItemAKPIList.SelectedItem Is Nothing Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation on it."), Me)
                'Anjan [04 June 2014] -- End


                Exit Sub
            End If

            If txtItemAKPI.Text.Trim.Length <= 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = lblItemAKPI.Text.ToString & Language.getMessage(mstrModuleName, 19, " cannot be blank. ") & lblItemAKPI.Text & Language.getMessage(mstrModuleName, 20, " is required information.")
                DisplayMessage.DisplayMessage(StrMessage, Me)
                txtItemAKPI.Text = ""
                Exit Sub
            End If

            Dim dtTable As DataTable = Me.ViewState("ItemAKPI")

            Dim dRow As DataRow() = Nothing

            If GvItemAKPIList.Items(GvItemAKPIList.SelectedItem.DataSetIndex).Cells(3).Text.Trim = "" Then
                dRow = dtTable.Select("kra='" & txtItemAKPI.Text.Trim & "' AND AUD <> 'D' AND kra <> '" & GvItemAKPIList.Items(GvItemAKPIList.SelectedItem.DataSetIndex).Cells(2).Text.Trim & "'")
            ElseIf GvItemAKPIList.Items(GvItemAKPIList.SelectedItem.DataSetIndex).Cells(3).Text.Trim <> "" Then
                dRow = dtTable.Select("kra='" & txtItemAKPI.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & GvItemAKPIList.Items(GvItemAKPIList.SelectedItem.DataSetIndex).Cells(3).Text.Trim & "'")
            End If

            If dRow.Length > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "This KPI/KRA is already exist in the list.Please define new KPI/KRA."), Me)
                txtItemAKPI.Focus()
                Exit Sub
            End If

            Dim drRow As DataRow() = dtTable.Select("kra='" & GvItemAKPIList.Items(GvItemAKPIList.SelectedItem.DataSetIndex).Cells(2).Text.Trim & "'")
            If drRow.Length > 0 Then
                drRow(0)("kra") = txtItemAKPI.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If

            Me.ViewState("ItemAKPI") = dtTable

            FillPartAItemAKPIList()
            FillPartBKPIList()
            txtItemAKPI.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddItemAGAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemAGAP.Click
        Try

            If txtItemAJobCapability.Text.Length > 0 Then

                Dim dtTable As DataTable = Me.ViewState("ItemADevelopmentGAP")

                Dim dRow As DataRow() = dtTable.Select("development_area='" & txtItemAJobCapability.Text.Trim & "' AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    Dim StrMessage As String = String.Empty

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    StrMessage = lblItemAJobCapability.Text.ToString & Language.getMessage(mstrModuleName, 22, " is already exist in the list.Please define new ") & lblItemAJobCapability.Text & "."
                    'Anjan [04 June 2014] -- End


                    DisplayMessage.DisplayMessage(StrMessage, Me)
                    txtItemAJobCapability.Focus()
                    Exit Sub
                End If

                Dim drRow As DataRow = dtTable.NewRow
                drRow("development_area") = txtItemAJobCapability.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                dtTable.Rows.Add(drRow)

                Me.ViewState("ItemADevelopmentGAP") = dtTable

                FillPartAItemA_DeveoplmentGAPList()
                txtItemAJobCapability.Text = ""

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEditItemAGAP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItemAGAP.Click
        Try

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014] -- End

            If gvItemAJobCapability.SelectedItem Is Nothing Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation on it."), Me)
                Exit Sub
            End If

            If txtItemAJobCapability.Text.Trim.Length <= 0 Then
                Dim StrMessage As String = String.Empty
                StrMessage = lblItemAJobCapability.Text.ToString & Language.getMessage(mstrModuleName, 19, " cannot be blank. ") & lblItemAJobCapability.Text & Language.getMessage(mstrModuleName, 20, " is required information.")
                DisplayMessage.DisplayMessage(StrMessage, Me)
                txtItemAJobCapability.Focus()
                Exit Sub
            End If

            Dim dtTable As DataTable = Me.ViewState("ItemADevelopmentGAP")

            Dim dRow As DataRow() = Nothing

            If gvItemAJobCapability.Items(gvItemAJobCapability.SelectedItem.DataSetIndex).Cells(3).Text = "" Then
                dRow = dtTable.Select("development_area='" & txtItemAJobCapability.Text.Trim & "' AND AUD <> 'D' AND development_area <> '" & gvItemAJobCapability.Items(gvItemAJobCapability.SelectedItem.DataSetIndex).Cells(2).Text & "'")

            ElseIf gvItemAJobCapability.Items(gvItemAJobCapability.SelectedItem.DataSetIndex).Cells(3).Text <> "" Then
                dRow = dtTable.Select("development_area='" & txtItemAJobCapability.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & gvItemAJobCapability.Items(gvItemAJobCapability.SelectedItem.DataSetIndex).Cells(3).Text & "'")
            End If

            If dRow.Length > 0 Then
                Dim StrMessage As String = String.Empty
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                StrMessage = lblItemAJobCapability.Text.ToString & Language.getMessage(mstrModuleName, 22, " is already exist in the list.Please define new ") & lblItemAJobCapability.Text & "."
                'Anjan [04 June 2014] -- End

                DisplayMessage.DisplayMessage(StrMessage, Me)
                txtItemAJobCapability.Focus()
                Exit Sub
            End If


            Dim drRow As DataRow() = dtTable.Select("development_area='" & gvItemAJobCapability.Items(gvItemAJobCapability.SelectedItem.DataSetIndex).Cells(2).Text & "'")
            If drRow.Length > 0 Then
                drRow(0)("development_area") = txtItemAJobCapability.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If
            Me.ViewState("ItemADevelopmentGAP") = dtTable
            FillPartAItemA_DeveoplmentGAPList()
            txtItemAJobCapability.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddItemB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddItemB.Click
        Try
            If txtItemBTrainingObjective.Text.Trim.Length > 0 Then

                Dim dtTable As DataTable = Me.ViewState("ItemBObjective")

                Dim dRow As DataRow()
                dRow = dtTable.Select("training_objective='" & txtItemBTrainingObjective.Text.Trim & "' AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    Dim StrMessage As String = String.Empty

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "This Training Objective is already exist in the list.Please define new Training Objective."), Me)
                    'Anjan [04 June 2014] -- End
                    txtItemBTrainingObjective.Focus()
                    Exit Sub
                End If

                Dim drRow As DataRow = dtTable.NewRow
                drRow("training_objective") = txtItemBTrainingObjective.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                dtTable.Rows.Add(drRow)

                Me.ViewState("ItemBObjective") = dtTable

                FillPartAItemB_ObjectiveList()
                txtItemBTrainingObjective.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEditItemB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItemB.Click
        Try


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014] -- End

            If gvItemBList.SelectedItem Is Nothing Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation on it."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            If txtItemBTrainingObjective.Text.Trim.Length <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Training Objective cannot be blank.Training Objective are required information."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            Dim dRow As DataRow() = Nothing

            Dim dtTable As DataTable = Me.ViewState("ItemBObjective")

            If gvItemBList.Items(gvItemBList.SelectedItem.DataSetIndex).Cells(3).Text = "" Then
                dRow = dtTable.Select("training_objective='" & txtItemBTrainingObjective.Text.Trim & "' AND AUD <> 'D' AND training_objective <> '" & gvItemBList.Items(gvItemBList.SelectedItem.DataSetIndex).Cells(2).Text & "'")

            ElseIf gvItemBList.Items(gvItemBList.SelectedItem.DataSetIndex).Cells(3).Text <> "" Then
                dRow = dtTable.Select("training_objective='" & txtItemBTrainingObjective.Text.Trim & "' AND AUD <> 'D' AND GUID <> '" & gvItemBList.Items(gvItemBList.SelectedItem.DataSetIndex).Cells(3).Text & "'")
            End If

            If dRow.Length > 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "This Training Objective is already exist in the list.Please define new Training Objective."), Me)
                'Anjan [04 June 2014] -- End
                txtItemBTrainingObjective.Focus()
                Exit Sub
            End If

            Dim drRow As DataRow() = dtTable.Select("training_objective='" & gvItemBList.Items(gvItemBList.SelectedItem.DataSetIndex).Cells(2).Text & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                drRow(0)("training_objective") = txtItemBTrainingObjective.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If
            Me.ViewState("ItemBObjective") = dtTable
            FillPartAItemB_ObjectiveList()
            txtItemBTrainingObjective.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddFeedback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddFeedback.Click
        Try
            If CInt(drpQuestion.SelectedValue) > 0 Then

                Dim dtTable As DataTable = Me.ViewState("FeedBack")

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End

                If CInt(drpAnswer.SelectedValue) <= 0 And drpAnswer.Visible = True Then
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Feedback Answer is compuslory information.Please Select Feedback Answer."), Me)
                    'Sohail (23 Mar 2019) -- End

                    'Anjan [04 June 2014] -- End
                    drpAnswer.Focus()
                    Exit Sub
                End If

                Dim dRow As DataRow() = dtTable.Select("questionunkid=" & CInt(drpQuestion.SelectedValue) & " AND AUD <> 'D'")
                If dRow.Length > 0 Then
                    Dim StrMessage As String = String.Empty
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "This Feedback Question is already exist in the list.Please define new Feedback Question."), Me)
                    drpQuestion.Focus()
                    Exit Sub
                End If

                Dim drRow As DataRow = dtTable.NewRow
                drRow("questionunkid") = CInt(drpQuestion.SelectedValue)
                drRow("question") = drpQuestion.SelectedItem.Text
                If drpAnswer.Visible Then
                    drRow("answerunkid") = CInt(drpAnswer.SelectedValue)
                    drRow("answer") = drpAnswer.SelectedItem.Text
                Else
                    drRow("answerunkid") = -1
                    drRow("answer") = ""
                End If
                drRow("manager_remark") = TxtComments.Text.Trim
                drRow("AUD") = "A"
                drRow("GUID") = Guid.NewGuid.ToString()
                dtTable.Rows.Add(drRow)

                Me.ViewState("FeedBack") = dtTable

                FillFeedBackList()
                drpQuestion.SelectedIndex = 0
                drpQuestion_SelectedIndexChanged(sender, e)
                drpAnswer.SelectedIndex = 0
                TxtComments.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEditFeedBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditFeedBack.Click
        Try

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014] -- End

            If GvFeedBackList.SelectedItem Is Nothing Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Please Select Item from the list to do further operation on it."), Me)
                'Anjan [04 June 2014] -- End
                Exit Sub
            End If

            If CInt(drpQuestion.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Feedback Question is Compulsory information.Please Select Feedback Question."), Me)
                'Anjan [04 June 2014] -- End
                drpQuestion.Focus()
                Exit Sub
            End If

            If CInt(drpAnswer.SelectedValue) <= 0 And drpAnswer.Visible = True Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Feedback Answer is Compulsory information.Please Select Feedback Answer."), Me)
                'Anjan [04 June 2014] -- End
                drpAnswer.Focus()
                Exit Sub
            End If

            Dim dRow As DataRow() = Nothing


            Dim dtTable As DataTable = Me.ViewState("FeedBack")

            If GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(5).Text = "" Then
                dRow = dtTable.Select("questionunkid=" & CInt(drpQuestion.SelectedValue) & " AND AUD <> 'D' AND questionunkid <> " & CInt(GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(6).Text))

            ElseIf GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(5).Text <> "" Then
                dRow = dtTable.Select("questionunkid=" & CInt(drpQuestion.SelectedValue) & " AND AUD <> 'D' AND GUID <> '" & GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(5).Text & "'")
            End If

            If dRow.Length > 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "This Feedback Question is already exist in the list.Please define new Feedback Question."), Me)
                'Anjan [04 June 2014] -- End
                drpQuestion.Focus()
                Exit Sub
            End If

            Dim drRow As DataRow() = dtTable.Select("questionunkid=" & CInt(GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(6).Text) & " AND AUD <> 'D'")
            If drRow.Length > 0 Then
                drRow(0)("questionunkid") = CInt(drpQuestion.SelectedValue)
                drRow(0)("question") = drpQuestion.SelectedItem.Text
                If drpAnswer.Visible Then
                    drRow(0)("answerunkid") = CInt(drpAnswer.SelectedValue)
                    drRow(0)("answer") = drpAnswer.SelectedItem.Text
                Else
                    drRow(0)("answerunkid") = -1
                    drRow(0)("answer") = ""
                End If
                drRow(0)("manager_remark") = TxtComments.Text.Trim
                drRow(0)("AUD") = "U"
                drRow(0).AcceptChanges()
            End If
            Me.ViewState("FeedBack") = dtTable
            FillFeedBackList()
            drpQuestion.SelectedIndex = 0
            drpAnswer.SelectedIndex = 0
            TxtComments.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try

            If Me.ViewState("GridID") = 1 Then    'ITEM A KPI 

                DeleteItemAKPI()

            ElseIf Me.ViewState("GridID") = 2 Then   'ITEM A DEVELOPMENT AREA

                DeleteItemAGAP()

            ElseIf Me.ViewState("GridID") = 3 Then  'ITEM B TRAINING OBJECTIVE

                DeleteItemBObjective()

            ElseIf Me.ViewState("GridID") = 4 Then  'FEEDBACK

                DeleteItemFeedBack()

            End If
            Me.ViewState("GridID") = Nothing
            Me.ViewState("Index") = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Try
            If mvwImpact.ActiveViewIndex = 2 Then
                BtnNext.Enabled = True
                mvwImpact.ActiveViewIndex = 1
            ElseIf mvwImpact.ActiveViewIndex = 1 Then
                mvwImpact.ActiveViewIndex = 0
                BtnBack.Enabled = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNext.Click
        Try

            If mvwImpact.ActiveViewIndex = 0 Then
                BtnBack.Enabled = True
                mvwImpact.ActiveViewIndex = 1
            ElseIf mvwImpact.ActiveViewIndex = 1 Then
                mvwImpact.ActiveViewIndex = 2
                BtnNext.Enabled = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try

            If Validation() Then


                objImpactMst._Iscomplete = False

                If Me.ViewState("ImpactunkID") Is Nothing Or CInt(Me.ViewState("ImpactunkID")) <= 0 Then
                    SetValue()
                    If objImpactMst.Insert = False Then
                        DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & objImpactMst._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry Saved Sucessfully." & objImpactMst._Message, Me)
                        ClearViewState()
                        ClearObject()
                    End If

                Else
                    objImpactMst._Impactunkid = CInt(Me.ViewState("ImpactunkID"))
                    SetValue()
                    If objImpactMst.Update = False Then
                        DisplayMessage.DisplayMessage("Entry Not Updated. Reason : " & objImpactMst._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry Updated Sucessfully." & objImpactMst._Message, Me)
                        ClearViewState()
                        ClearObject()
                        Response.Redirect("~/Training/wPg_TrainingImpactEvaluation.aspx")
                    End If

                End If

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnFinalSave.Click
        Try

            If Validation() Then
                popupFinalize.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnFinalize.Click
    Protected Sub BtnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalize.buttonYes_Click
        'SHANI [01 FEB 2015]--END
        Try

            If Me.ViewState("ImpactunkID") Is Nothing Or CInt(Me.ViewState("ImpactunkID")) <= 0 Then
                SetValue()
                objImpactMst._Iscomplete = True
                If objImpactMst.Insert = False Then
                    DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & objImpactMst._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Saved Sucessfully." & objImpactMst._Message, Me)

                    'S.SANDEEP [06-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Training Module Notification
                    If Session("Ntf_TrainingLevelIII_EvalUserIds").ToString.Trim.Length > 0 Then
                        If (Session("LoginBy") = Global.User.en_loginby.User) Then
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objImpactMst.SendNotification(CInt(drpCourse.SelectedValue), CInt(drpEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.MGR_SELF_SERVICE, 0, Session("Ntf_TrainingLevelIII_EvalUserIds"), 0, mstrModuleName)
                            objImpactMst.SendNotification(CInt(drpCourse.SelectedValue), CInt(drpEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.MGR_SELF_SERVICE, 0, Session("Ntf_TrainingLevelIII_EvalUserIds"), 0, mstrModuleName, CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objImpactMst.SendNotification(CInt(drpCourse.SelectedValue), CInt(drpEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.EMP_SELF_SERVICE, Session("Userunkid"), Session("Ntf_TrainingLevelIII_EvalUserIds"), Session("Employeeunkid"), mstrModuleName)
                            objImpactMst.SendNotification(CInt(drpCourse.SelectedValue), CInt(drpEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), enLogin_Mode.EMP_SELF_SERVICE, Session("Userunkid"), Session("Ntf_TrainingLevelIII_EvalUserIds"), Session("Employeeunkid"), mstrModuleName, CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    End If
                    'S.SANDEEP [06-MAR-2017] -- END

                    ClearViewState()
                    ClearObject()
                End If

            Else
                objImpactMst._Impactunkid = CInt(Me.ViewState("ImpactunkID"))
                objImpactMst._Iscomplete = True
                SetValue()
                If objImpactMst.Update = False Then
                    DisplayMessage.DisplayMessage("Entry Not Updated. Reason : " & objImpactMst._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Updated Sucessfully." & objImpactMst._Message, Me)
                    ClearViewState()
                    ClearObject()
                    Response.Redirect("~/Training/wPg_TrainingImpactList.aspx")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Me.ViewState.Clear()
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect("~\Training\wPg_TrainingImpactList.aspx")
            'SHANI [09 Mar 2015]--END 
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Me.ViewState.Clear()
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

#Region "DataGrid Event"

#Region "Item A KPI Grid"

    Protected Sub GvItemAKPIList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GvItemAKPIList.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then

                If e.Item.Cells(2).Text.Contains("&nbsp;") Then
                    e.Item.Cells(2).Text = e.Item.Cells(2).Text.Replace(e.Item.Cells(2).Text, "")
                End If
                If e.Item.Cells(3).Text.Contains("&nbsp;") Then
                    e.Item.Cells(3).Text = e.Item.Cells(3).Text.Replace(e.Item.Cells(3).Text, "")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvItemAKPIList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvItemAKPIList.ItemCommand
        Try
            Me.ViewState("Index") = e.Item.ItemIndex
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvItemAKPIList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvItemAKPIList.DeleteCommand
        Try

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreasondel.Text = ""
            'SHANI [01 FEB 2015]--END

            If Me.ViewState("ImpactunkID") IsNot Nothing Then
                If CInt(Me.ViewState("ImpactunkID")) > 0 Then
                    Me.ViewState("GridID") = 1
                    popupDelete.Show()
                End If
            Else
                DeleteItemAKPI()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvItemAKPIList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GvItemAKPIList.PageIndexChanged
        Try
            GvItemAKPIList.CurrentPageIndex = e.NewPageIndex
            FillPartAItemAKPIList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvItemAKPIList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvItemAKPIList.SelectedIndexChanged
        Try
            If GvItemAKPIList.SelectedItem Is Nothing Then Exit Sub
            txtItemAKPI.Text = GvItemAKPIList.Items(GvItemAKPIList.SelectedItem.DataSetIndex).Cells(2).Text.Trim
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Item A Development Area Grid"

    Protected Sub gvItemAJobCapability_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvItemAJobCapability.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then

                If e.Item.Cells(2).Text.Contains("&nbsp;") Then
                    e.Item.Cells(2).Text = e.Item.Cells(2).Text.Replace(e.Item.Cells(2).Text, "")
                End If
                If e.Item.Cells(3).Text.Contains("&nbsp;") Then
                    e.Item.Cells(3).Text = e.Item.Cells(3).Text.Replace(e.Item.Cells(3).Text, "")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemAJobCapability_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvItemAJobCapability.ItemCommand
        Try
            Me.ViewState("Index") = e.Item.ItemIndex
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemAJobCapability_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvItemAJobCapability.DeleteCommand
        Try

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreasondel.Text = ""
            'SHANI [01 FEB 2015]--END

            If Me.ViewState("ImpactunkID") IsNot Nothing Then
                If CInt(Me.ViewState("ImpactunkID")) > 0 Then
                    Me.ViewState("GridID") = 2
                    popupDelete.Show()
                End If
            Else
                DeleteItemAGAP()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemAJobCapability_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemAJobCapability.SelectedIndexChanged
        Try
            If gvItemAJobCapability.SelectedItem Is Nothing Then Exit Sub
            txtItemAJobCapability.Text = gvItemAJobCapability.Items(gvItemAJobCapability.SelectedItem.DataSetIndex).Cells(2).Text.Trim
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemAJobCapability_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvItemAJobCapability.PageIndexChanged
        Try
            gvItemAJobCapability.CurrentPageIndex = e.NewPageIndex
            FillPartAItemA_DeveoplmentGAPList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Item B Training Objective Grid"

    Protected Sub gvItemBList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvItemBList.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then

                If e.Item.Cells(2).Text.Contains("&nbsp;") Then
                    e.Item.Cells(2).Text = e.Item.Cells(2).Text.Replace(e.Item.Cells(2).Text, "")
                End If
                If e.Item.Cells(3).Text.Contains("&nbsp;") Then
                    e.Item.Cells(3).Text = e.Item.Cells(3).Text.Replace(e.Item.Cells(3).Text, "")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemBList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvItemBList.ItemCommand
        Try
            Me.ViewState("Index") = e.Item.ItemIndex
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemBList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvItemBList.DeleteCommand
        Try

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreasondel.Text = ""
            'SHANI [01 FEB 2015]--END

            If Me.ViewState("ImpactunkID") IsNot Nothing Then
                If CInt(Me.ViewState("ImpactunkID")) > 0 Then
                    Me.ViewState("GridID") = 3
                    popupDelete.Show()
                End If
            Else
                DeleteItemBObjective()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemBList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItemBList.SelectedIndexChanged
        Try
            If gvItemBList.SelectedItem Is Nothing Then Exit Sub
            txtItemBTrainingObjective.Text = gvItemBList.Items(gvItemBList.SelectedItem.DataSetIndex).Cells(2).Text.Trim
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvItemBList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvItemBList.PageIndexChanged
        Try
            gvItemBList.CurrentPageIndex = e.NewPageIndex
            FillPartAItemB_ObjectiveList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Feedback Grid "

    Protected Sub GvFeedBackList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GvFeedBackList.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then

                If e.Item.Cells(2).Text.Contains("&nbsp;") Then
                    e.Item.Cells(2).Text = e.Item.Cells(2).Text.Replace(e.Item.Cells(2).Text, "")
                End If
                If e.Item.Cells(3).Text.Contains("&nbsp;") Then
                    e.Item.Cells(3).Text = e.Item.Cells(3).Text.Replace(e.Item.Cells(3).Text, "")
                End If
                If e.Item.Cells(4).Text.Contains("&nbsp;") Then
                    e.Item.Cells(4).Text = e.Item.Cells(4).Text.Replace(e.Item.Cells(4).Text, "")
                End If
                If e.Item.Cells(5).Text.Contains("&nbsp;") Then
                    e.Item.Cells(5).Text = e.Item.Cells(5).Text.Replace(e.Item.Cells(5).Text, "")
                End If
                If e.Item.Cells(6).Text.Contains("&nbsp;") Then
                    e.Item.Cells(6).Text = e.Item.Cells(6).Text.Replace(e.Item.Cells(6).Text, "")
                End If
                If e.Item.Cells(7).Text.Contains("&nbsp;") Then
                    e.Item.Cells(7).Text = e.Item.Cells(7).Text.Replace(e.Item.Cells(7).Text, "")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvFeedBackList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvFeedBackList.ItemCommand
        Try
            Me.ViewState("Index") = e.Item.ItemIndex
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvFeedBackList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvFeedBackList.DeleteCommand
        Try
            If Me.ViewState("ImpactunkID") IsNot Nothing Then
                If CInt(Me.ViewState("ImpactunkID")) > 0 Then
                    Me.ViewState("GridID") = 4
                    popupDelete.Show()
                End If
            Else
                DeleteItemFeedBack()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvFeedBackList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvFeedBackList.SelectedIndexChanged
        Try
            If GvFeedBackList.SelectedItem Is Nothing Then Exit Sub
            drpQuestion.SelectedValue = GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(6).Text.Trim
            drpQuestion_SelectedIndexChanged(sender, e)
            If CInt(GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(7).Text.Trim) > 0 Then
                drpAnswer.SelectedValue = GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(7).Text.Trim
            Else
                drpAnswer.SelectedValue = 0
            End If
            TxtComments.Text = GvFeedBackList.Items(GvFeedBackList.SelectedItem.DataSetIndex).Cells(4).Text.Trim
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvFeedBackList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GvFeedBackList.PageIndexChanged
        Try
            GvFeedBackList.CurrentPageIndex = e.NewPageIndex
            FillFeedBackList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Item B KPI Grid"

    Protected Sub gvItemBKPIList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvItemBKPIList.PageIndexChanged
        Try
            gvItemBKPIList.CurrentPageIndex = e.NewPageIndex
            FillPartAItemAKPIList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Part B KPI Grid"

    Protected Sub GvPartBKRA_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GvPartBKRA.PageIndexChanged
        Try
            GvPartBKRA.CurrentPageIndex = e.NewPageIndex
            FillPartBKPIList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvPartBKRA_PageIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#End Region

#Region "ToolBar Event"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\Training\wPg_TrainingImpactList.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]--END

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.ID, Me.lblCourse.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblEndDate.Text = Language._Object.getCaption(Me.lblEndDate.ID, Me.lblEndDate.Text)
            Me.LblItemACaption.Text = Language._Object.getCaption(Me.LblItemACaption.ID, Me.LblItemACaption.Text)
            Me.LblItemA.Text = Language._Object.getCaption(Me.LblItemA.ID, Me.LblItemA.Text)
            Me.lblItemAKPI.Text = Language._Object.getCaption(Me.lblItemAKPI.ID, Me.lblItemAKPI.Text)
            Me.lblItemAJobCapability.Text = Language._Object.getCaption(Me.lblItemAJobCapability.ID, Me.lblItemAJobCapability.Text)

            Me.btnAddItemAGAP.Text = Language._Object.getCaption(Me.btnAddItemAGAP.ID, Me.btnAddItemAGAP.Text)



            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.ID, Me.btnFinalSave.Text).Replace("&", "")

            'TAB A

            Me.LblItemB.Text = Language._Object.getCaption(Me.LblItemB.ID, Me.LblItemB.Text)
            Me.lblItemBTrainingObjective.Text = Language._Object.getCaption(Me.lblItemBTrainingObjective.ID, Me.lblItemBTrainingObjective.Text)
            Me.LblItemBCaption.Text = Language._Object.getCaption(Me.LblItemBCaption.ID, Me.LblItemBCaption.Text)

            Me.btnEditItemB.Text = Language._Object.getCaption(Me.btnEditItemB.ID, Me.btnEditItemB.Text)
            Me.btnEditItemAGAP.Text = Language._Object.getCaption(Me.btnEditItemAGAP.ID, Me.btnEditItemAGAP.Text)
            Me.btnAddItemB.Text = Language._Object.getCaption(Me.btnAddItemB.ID, Me.btnAddItemB.Text)

            Me.btnEditItemAKPI.Text = Language._Object.getCaption(Me.btnEditItemAKPI.ID, Me.btnEditItemAKPI.Text)

            Me.btnAddItemAKPI.Text = Language._Object.getCaption(Me.btnAddItemAKPI.ID, Me.btnAddItemAKPI.Text)
            Me.lblLineManager.Text = Language._Object.getCaption(Me.lblLineManager.ID, Me.lblLineManager.Text)

            Me.GvItemAKPIList.Columns(2).HeaderText = Language._Object.getCaption(Me.GvItemAKPIList.Columns(2).FooterText, Me.GvItemAKPIList.Columns(2).HeaderText)
            Me.gvItemAJobCapability.Columns(2).HeaderText = Language._Object.getCaption(Me.gvItemAJobCapability.Columns(2).FooterText, Me.gvItemAJobCapability.Columns(2).HeaderText)
            Me.gvItemBList.Columns(2).HeaderText = Language._Object.getCaption(Me.gvItemBList.Columns(2).FooterText, Me.gvItemBList.Columns(2).HeaderText)

            'TAB B
            Me.lblPartBDesc.Text = Language._Object.getCaption(Me.lblPartBDesc.ID, Me.lblPartBDesc.Text)
            Me.lblPartBJobCapabilities.Text = Language._Object.getCaption(Me.lblPartBJobCapabilities.ID, Me.lblPartBJobCapabilities.Text)
            Me.lblNametask.Text = Language._Object.getCaption(Me.lblNametask.ID, Me.lblNametask.Text)

            Me.gvItemBKPIList.Columns(0).HeaderText = Language._Object.getCaption(Me.gvItemBKPIList.Columns(0).FooterText, Me.gvItemBKPIList.Columns(0).HeaderText)
            Me.GvPartBKRA.Columns(0).HeaderText = Language._Object.getCaption(Me.GvPartBKRA.Columns(0).FooterText, Me.GvPartBKRA.Columns(0).HeaderText)

            'TAB C

            Me.btnEditFeedBack.Text = Language._Object.getCaption(Me.btnEditFeedBack.ID, Me.btnEditFeedBack.Text)
            Me.btnAddFeedback.Text = Language._Object.getCaption(Me.btnAddFeedback.ID, Me.btnAddFeedback.Text)
            Me.lblQuestion.Text = Language._Object.getCaption(Me.lblQuestion.ID, Me.lblQuestion.Text)
            Me.lblPartCDesc.Text = Language._Object.getCaption(Me.lblPartCDesc.ID, Me.lblPartCDesc.Text)
            Me.lblAnswer.Text = Language._Object.getCaption(Me.lblAnswer.ID, Me.lblAnswer.Text)
            Me.lblComments.Text = Language._Object.getCaption(Me.lblComments.ID, Me.lblComments.Text)

            Me.GvFeedBackList.Columns(0).HeaderText = Language._Object.getCaption(Me.GvFeedBackList.Columns(0).FooterText, Me.GvFeedBackList.Columns(0).HeaderText)
            Me.GvFeedBackList.Columns(1).HeaderText = Language._Object.getCaption(Me.GvFeedBackList.Columns(1).FooterText, Me.GvFeedBackList.Columns(1).HeaderText)
            Me.GvFeedBackList.Columns(2).HeaderText = Language._Object.getCaption(Me.GvFeedBackList.Columns(2).FooterText, Me.GvFeedBackList.Columns(2).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

End Class
