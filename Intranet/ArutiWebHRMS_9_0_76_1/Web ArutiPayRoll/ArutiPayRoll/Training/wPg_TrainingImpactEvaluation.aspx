﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_TrainingImpactEvaluation.aspx.vb" Inherits="wPg_TrainingImpactEvaluation"
    Title="Training Impact Evaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Impact Evaluation"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default">
                                    <div id="FilterCriteria" class="panel-default">
                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lblDetialHeader" runat="server" Text="Training Impact Evaluation Add/Edit"></asp:Label>
                                            </div>
                                        </div>
                                        <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                            <table style="width: 80%">
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblCourse" runat="server" Text="Course"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="drpCourse" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="drpEmployee" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <uc2:DateCtrl ID="dtStartDate" runat="server" AutoPostBack="false" Enabled="false" />
                                                    </td>
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblLineManager" runat="server" Text="Line Manager / Supervisor"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="drpLineManager" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="3">
                                                        <uc2:DateCtrl ID="dtEndDate" runat="server" AutoPostBack="false" Enabled="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <asp:MultiView ID="mvwImpact" runat="server" ActiveViewIndex="0" Visible="false">
                                        <asp:View runat="server" ID="StepA">
                                            <asp:Panel ID="pnlPartA" runat="server" Width="100%">
                                                <div id="Setp_A" class="panel-default">
                                                    <div id="Div4" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lbl_PartA" runat="server" Text="Part-A"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div5" class="panel-body-default" style="text-align: left">
                                                        <div class="grpheader">
                                                            <asp:Label ID="LblItemA" runat="server"></asp:Label>
                                                        </div>
                                                        <div>
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%; padding-bottom: 5px" colspan="2">
                                                                        <h4>
                                                                            <asp:Label ID="LblItemACaption" runat="server"></asp:Label>
                                                                        </h4>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 50%; padding-right: 10px;">
                                                                        <asp:Label ID="lblItemAKPI" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 50%; padding-left: 10px;">
                                                                        <asp:Label ID="lblItemAJobCapability" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 50%; padding-right: 10px;">
                                                                        <asp:TextBox ID="txtItemAKPI" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                    <td style="width: 50%; padding-left: 10px;">
                                                                        <asp:TextBox ID="txtItemAJobCapability" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 50%; padding-right: 10px; vertical-align: top">
                                                                        <asp:Panel ID="pnlItemAKPIList" runat="server" ScrollBars="Auto" Width="100%">
                                                                            <asp:DataGrid ID="GvItemAKPIList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgSelectItemAKPI" runat="server" CssClass="gridedit" CommandName="Select"
                                                                                                    ToolTip="Edit"></asp:LinkButton>
                                                                                            </span>
                                                                                            <%--  <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png"
                                                                                                ToolTip="Edit" CommandName="Select" />--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgDeleteItemAKPI" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                    ToolTip="Delete"></asp:LinkButton>
                                                                                            </span>
                                                                                            <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png"
                                                                                                ToolTip="Delete" CommandName="Delete" />--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="kra" ReadOnly="True" FooterText="colhItemAKPI" />
                                                                                    <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                    <td style="width: 50%; padding-left: 10px; vertical-align: top">
                                                                        <asp:Panel ID="pnlItemAJobCapability" runat="server" ScrollBars="Auto" Width="100%">
                                                                            <asp:DataGrid ID="gvItemAJobCapability" runat="server" AutoGenerateColumns="False"
                                                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgSelectItemAJobCapability" runat="server" CssClass="gridedit"
                                                                                                    CommandName="Select" ToolTip="Edit"></asp:LinkButton>
                                                                                            </span>
                                                                                            <%--  <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png"
                                                                                                ToolTip="Edit" CommandName="Select" />--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgDeleteItemAJobCapability" runat="server" CssClass="griddelete"
                                                                                                    CommandName="Delete" ToolTip="Delete"></asp:LinkButton>
                                                                                            </span>
                                                                                            <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png"
                                                                                                ToolTip="Delete" CommandName="Delete" />--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="development_area" ReadOnly="True" FooterText="colhItemAJobCapability" />
                                                                                    <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 50%; text-align: right; padding-right: 10px;">
                                                                        <asp:Button ID="btnAddItemAKPI" runat="server" Text="Add" CssClass="btndefault" />
                                                                        <asp:Button ID="btnEditItemAKPI" runat="server" Text="Edit" CssClass="btndefault" />
                                                                    </td>
                                                                    <td style="width: 50%; text-align: right; padding-left: 10px;">
                                                                        <asp:Button ID="btnAddItemAGAP" runat="server" Text="Add" CssClass="btndefault" />
                                                                        <asp:Button ID="btnEditItemAGAP" runat="server" Text="Edit" CssClass="btndefault" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div id="Div3" class="grpheader">
                                                            <asp:Label ID="LblItemB" runat="server"></asp:Label>
                                                        </div>
                                                        <div id="Div8">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td colspan="2" style="width: 100%; padding-bottom: 5px">
                                                                        <h4>
                                                                            <asp:Label ID="LblItemBCaption" runat="server"></asp:Label>
                                                                        </h4>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td colspan="2" style="width: 100%">
                                                                        <asp:Label ID="lblItemBTrainingObjective" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td colspan="2" style="width: 100%">
                                                                        <asp:TextBox ID="txtItemBTrainingObjective" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 50%; padding-right: 10px; vertical-align: top">
                                                                        <asp:Panel ID="pnlItemBKPIList" runat="server" ScrollBars="Auto" Width="100%">
                                                                            <asp:DataGrid ID="gvItemBKPIList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false" ShowFooter="false">
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="kra" ReadOnly="True" FooterText="colhPartAItemBKPI" />
                                                                                    <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                    <td style="width: 50%; padding-left: 10px; vertical-align: top">
                                                                        <asp:Panel ID="pnlItemBList" runat="server" ScrollBars="Auto" Width="100%">
                                                                            <asp:DataGrid ID="gvItemBList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgSelectItemAJobCapability" runat="server" CssClass="gridedit"
                                                                                                    CommandName="Select" ToolTip="Edit"></asp:LinkButton>
                                                                                            </span>
                                                                                            <%-- <asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png"
                                                                                                ToolTip="Edit" CommandName="Select" />--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Delete"
                                                                                        HeaderStyle-HorizontalAlign="Center">
                                                                                        <ItemStyle HorizontalAlign="Center" />
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="ImgDeleteItemAJobCapability" runat="server" CssClass="griddelete"
                                                                                                    CommandName="Delete" ToolTip="Delete"></asp:LinkButton>
                                                                                            </span>
                                                                                            <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png"
                                                                                                ToolTip="Delete" CommandName="Delete" />--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="training_objective" ReadOnly="True" FooterText="colhItemBTrainingObjective" />
                                                                                    <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                </Columns>
                                                                                <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                    Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td colspan="2" style="width: 100%; text-align: right">
                                                                        <asp:Button ID="btnAddItemB" runat="server" Text="Add" CssClass="btndefault" />
                                                                        <asp:Button ID="btnEditItemB" runat="server" Text="Edit" CssClass="btndefault" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                        <asp:View ID="StepB" runat="server">
                                            <asp:Panel ID="pnlPartB" runat="server" Width="100%">
                                                <div id="Setp_B" class="panel-default">
                                                    <div id="Div6" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lbl_PartB" runat="server" Text="Part-B"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div7" class="panel-body-default">
                                                        <div class="grpheader" style="width: 100%; border-radius: 0px; height: 30px">
                                                            <asp:Label ID="lblPartBDesc" runat="server"></asp:Label>
                                                        </div>
                                                        <div>
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%; padding-bottom: 5px" colspan="2">
                                                                        <h4>
                                                                            <asp:Label ID="lblPartBJobCapabilities" runat="server"></asp:Label>
                                                                        </h4>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%" colspan="2">
                                                                        <asp:Label ID="lblNametask" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 50%; padding-right: 5px; vertical-align: top;">
                                                                        <asp:Panel ID="pnlPartBKRA" runat="server" ScrollBars="Auto" Width="100%">
                                                                            <asp:DataGrid ID="GvPartBKRA" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="kra" HeaderText="Key Performance Indicator or Key Results Area for Senior Position"
                                                                                        ReadOnly="True" FooterText="colhPartBKRA" />
                                                                                    <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                    <td style="width: 50%; padding-right: 5px; vertical-align: top">
                                                                        <asp:TextBox ID="txtPartBNameTask" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                            </asp:Panel>
                                        </asp:View>
                                        <asp:View ID="StepC" runat="server">
                                            <asp:Panel ID="pnlPartC" runat="server" Width="100%">
                                                <div id="Setp_C" class="panel-default">
                                                    <div id="Div9" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lbl_PartC" runat="server" Text="Part-C"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div10" class="panel-body-default">
                                                        <div class="grpheader" style="width: 100%; border-radius: 0px; height: 30px">
                                                            <asp:Label ID="lblPartCDesc" runat="server"></asp:Label>
                                                        </div>
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblQuestion" runat="server" Text="Question"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:DropDownList ID="drpQuestion" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblComments" runat="server" Text="Comment"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:TextBox ID="TxtComments" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblAnswer" runat="server" Text="Answer"></asp:Label>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:DropDownList ID="drpAnswer" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="width: 50%" colspan="2">
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td colspan="4" style="width: 100%; text-align: right">
                                                                    <asp:Button ID="btnAddFeedback" runat="server" Text="Add" Width="75px" CssClass="btndefault" />
                                                                    <asp:Button ID="btnEditFeedBack" runat="server" Text="Edit" Width="75px" CssClass="btndefault" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td colspan="4" style="width: 100%">
                                                                    <asp:Panel ID="pnlFeedBackList" runat="server" ScrollBars="Auto" Width="100%">
                                                                        <asp:DataGrid ID="GvFeedBackList" runat="server" AutoGenerateColumns="False" Width="99%"
                                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderText="Edit">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="ImgSelectFeedback" runat="server" CssClass="gridedit" CommandName="Select"
                                                                                                ToolTip="Edit"></asp:LinkButton>
                                                                                        </span>
                                                                                        <%-- <asp:ImageButton ID="" runat="server" CommandName="Select" ImageUrl="~/images/edit.png"
                                                                                            ToolTip="Edit" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderText="Delete">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="ImgDeleteFeedback" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                                        </span>
                                                                                        <%--<asp:ImageButton ID="" runat="server" CommandName="Delete" ImageUrl="~/images/remove.png"
                                                                                            ToolTip="Delete" />--%>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="question" HeaderText="Question" ReadOnly="True" FooterText="colhQuestion" />
                                                                                <asp:BoundColumn DataField="answer" HeaderText="Answer" ReadOnly="True" FooterText="colhAnswer" />
                                                                                <asp:BoundColumn DataField="manager_remark" HeaderText="Comment" ReadOnly="True"
                                                                                    FooterText="colhAnswer" />
                                                                                <asp:BoundColumn DataField="GUID" HeaderText="GUID" ReadOnly="True" Visible="false" />
                                                                                <asp:BoundColumn DataField="questionunkid" HeaderText="questionunkid" ReadOnly="True"
                                                                                    Visible="false" />
                                                                                <asp:BoundColumn DataField="answerunkid" HeaderText="answerunkid" ReadOnly="True"
                                                                                    Visible="false" />
                                                                            </Columns>
                                                                            <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                                Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:View>
                                    </asp:MultiView>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnBack" runat="server" Text="Back" CssClass="btndefault" />
                                        <asp:Button ID="BtnNext" runat="server" Text="Next" CssClass="btndefault" />
                                        <asp:Button ID="btnFinalSave" runat="server" Text="Final Save" CssClass="btndefault" />
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popupDelete" runat="server" Title="Are you Sure You Want To delete?:" />
                    <ucCfnYesno:Confirmation ID="popupFinalize" runat="server" Message="Are you sure you want to Finally save this Imapct feedback.After this you won't be allowed to EDIT this Impact feedback.Do you want to continue?"
                        Title="Confirmation" />
                    <%--  <cc1:ModalPopupExtender ID="popupFinalize" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="BtnFinalizeClose" PopupControlID="pnlFinalize" TargetControlID="pnl3" />
                    <asp:Panel ID="pnlFinalize" Width="36%" Height="70px" runat="server" Style="display: none"
                        CssClass="modalPopup">
                        <asp:Panel ID="pnl3" runat="server" Width="100%" Height="45px">
                            <asp:Label ID="lblMessage" runat="server" Text=""
                                Width="100%" />
                        </asp:Panel>
                        <li class="col" style="margin-right: 0; width: 98%; text-align: right;">
                            <asp:Button ID="BtnFinalize" runat="server" Text="Yes" Width="15%" CssClass="btndefault" />
                            <asp:Button ID="BtnFinalizeClose" runat="server" Text="No" Width="15%" CssClass="btndefault" />
                        </li>
                    </asp:Panel>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
