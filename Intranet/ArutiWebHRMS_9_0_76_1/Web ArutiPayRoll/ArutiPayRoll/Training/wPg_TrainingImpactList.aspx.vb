﻿
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language
Imports System.IO
Imports System.Text
Imports System.Data

#End Region


Partial Class wPg_TrainingImpactList
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim objImpact As clshrtnatraining_impact_master


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmTrainingImpactList"
    'Anjan [04 June 2014] -- End


#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If



            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End


            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmTrainingImpactList"
            StrModuleName2 = "mnuTrainingInformation"
            StrModuleName3 = "mnuTrainingEvaluation"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            objImpact = New clshrtnatraining_impact_master
            Dim clsuser As New User
            clsuser = CType(Session("clsuser"), User)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Me.ViewState("Empunkid") = clsuser.UserID
                Me.ViewState("Empunkid") = Session("UserId")
                'Sohail (30 Mar 2015) -- End
                'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)

                'Sohail (23 Nov 2012) -- Start
                'TRA - ENHANCEMENT
                btnNew.Visible = CBool(Session("AllowToAddLevelIIIEvaluation"))
                'Sohail (23 Nov 2012) -- End
            Else
                'Aruti.Data.User._Object._Userunkid = -1 'Sohail (23 Apr 2012)
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                Me.ViewState("Empunkid") = Session("Employeeunkid")
                'Sohail (30 Mar 2015) -- End
            End If


            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
            'SHANI [01 FEB 2015]--END
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                dgView.Columns(3).Visible = True
            Else
                dgView.Columns(3).Visible = False
            End If


            If (Page.IsPostBack = False) Then
                FillCombo()

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Training_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = Session("Training_EmpUnkID")
                    Session.Remove("Training_EmpUnkID")
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Call BtnSearch_Click(BtnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END 

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub FillCombo()

        Dim dsList As DataSet = Nothing
        Try


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            Dim objCourse As New clsTraining_Scheduling
            dsList = objCourse.getComboList("List", True, -1, True)
            drpCourse.DataTextField = "Name"
            drpCourse.DataValueField = "Id"
            drpCourse.DataSource = dsList.Tables(0)
            drpCourse.DataBind()

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                Dim objEmp As New clsEmployee_Master
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsList = objEmp.GetEmployeeList("Emp", True, True)
                'End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsList = objEmp.GetEmployeeList("Emp", True, )
                '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If
                dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                     Session("UserId"), _
                                                     Session("Fin_year"), _
                                                     Session("CompanyUnkId"), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                     Session("UserAccessModeSetting"), True, _
                                                     Session("IsIncludeInactiveEmp"), "Emp", True)
                'Shani(24-Aug-2015) -- End

                'Sohail (23 Apr 2012) -- End
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("Emp")
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
                Dim objEmp As New clsEmployee_Master
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))

                dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Emp", True)

                'Shani(24-Aug-2015) -- End
                'Sohail (23 Apr 2012) -- End
            End If

            drpLineManager.DataTextField = "employeename"
            drpLineManager.DataValueField = "employeeunkid"
            drpLineManager.DataSource = dsList.Tables(0).Copy()
            drpLineManager.DataBind()



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim dsImpact As DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            If (Session("LoginBy") = Global.User.en_loginby.User) AndAlso CBool(Session("AllowToViewLevel3EvaluationList")) = False Then Exit Sub
            'Sohail (23 Nov 2012) -- End

            dsImpact = objImpact.GetList("List", True)

            If CInt(drpCourse.SelectedValue) > 0 Then
                StrSearching &= "AND trainingschedulingunkid = " & CInt(drpCourse.SelectedValue)
            End If

            If CInt(drpEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = " & CInt(drpEmployee.SelectedValue)
            End If

            If CInt(drpLineManager.SelectedValue) > 0 Then
                StrSearching &= "AND managerunkid = " & CInt(drpLineManager.SelectedValue)
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsImpact.Tables("List"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsImpact.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            If (Not dtTable Is Nothing) Then
                dgView.DataSource = dtTable
                dgView.DataKeyField = "impactunkid"
                dgView.DataBind()
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub


#End Region

#Region "Button' Event"

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Session("Training_EmpUnkID") = drpEmployee.SelectedValue
            'SHANI [09 Mar 2015]--END 
            Response.Redirect("~/Training/wPg_TrainingImpactEvaluation.aspx")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpCourse.SelectedIndex = 0
            drpEmployee.SelectedIndex = 0
            drpLineManager.SelectedIndex = 0

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'FillList()
            dgView.DataSource = New List(Of String)
            dgView.DataBind()
            'SHANI [09 Mar 2015]--END 
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try

            If (popup_DeleteReason.Reason.Trim = "") Then 'SHANI [01 FEB 2015]-If (txtreasondel.Text = "") Then 
                DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)
                popup_DeleteReason.Show() 'SHANI [01 FEB 2015]-popupDelete.Show()
                Exit Sub
            End If

            objImpact._Impactunkid = Me.ViewState("ImpactID")
            With objImpact
                ._Isvoid = True
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                '._Voiduserunkid = Aruti.Data.User._Object._Userunkid
                ._Voiduserunkid = Session("UserId")
                'Sohail (23 Apr 2012) -- End
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'SHANI [01 FEB 2015]-txtreasondel.Text.Trim
                If .Delete(Me.ViewState("ImpactID")) = False Then
                    DisplayMessage.DisplayMessage(._Message, Me)
                Else
                    Call FillList()
                    Me.ViewState("ImpactID") = Nothing
                End If
            End With

            popup_DeleteReason.Dispose() 'SHANI [01 FEB 2015]-pnlpopup.Dispose()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Me.ViewState.Clear()
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region "DataGrid Event"

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Try
            If e.CommandName = "Select" Then

                If CBool(e.Item.Cells(5).Text) = True Then
                    DisplayMessage.DisplayMessage("Sorry, You cannot Edit this Evaluation. Reason: This Evaluation is already completed.", Me)
                    Exit Sub
                End If

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                Session("Training_EmpUnkID") = drpEmployee.SelectedValue
                'SHANI [09 Mar 2015]--END 

                Response.Redirect("wPg_TrainingImpactEvaluation.aspx?ProcessId=" & b64encode(Val(dgView.DataKeys(e.Item.ItemIndex))))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.DeleteCommand
        Try

            If CBool(e.Item.Cells(5).Text) = True Then
                DisplayMessage.DisplayMessage("Sorry, You cannot delete this Evaluation. Reason: This Evaluation is already completed.", Me)
                Exit Sub
            End If

            Me.ViewState.Add("ImpactID", dgView.DataKeys(e.Item.ItemIndex))
            popup_DeleteReason.Show() 'SHANI [01 FEB 2015]-popupDelete.Show()
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (23 Nov 2012) -- Start
    'TRA - ENHANCEMENT
    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try
            If (e.Item.ItemIndex >= 0) Then
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    dgView.Columns(0).Visible = Session("AllowToEditLevelIIIEvaluation")
                    dgView.Columns(1).Visible = Session("AllowToDeleteLevelIIIEvaluation")
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (23 Nov 2012) -- End

#End Region

#Region "ToolBar Event"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try
    '        'Sohail (23 Nov 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        If (Session("LoginBy") = Global.User.en_loginby.User) AndAlso CBool(Session("AllowToAddLevelIIIEvaluation")) = False Then
    '            ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '            Exit Sub
    '        End If

    '        Response.Redirect("~\Training\wPg_TrainingImpactEvaluation.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]--END

            Me.lblCourse.Text = Language._Object.getCaption(Me.lblCourse.ID, Me.lblCourse.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.lblLineManager.Text = Language._Object.getCaption(Me.lblLineManager.ID, Me.lblLineManager.Text)

            Me.dgView.Columns(0).HeaderText = Language._Object.getCaption(Me.dgView.Columns(0).FooterText, Me.dgView.Columns(0).HeaderText).Replace("&", "")
            Me.dgView.Columns(1).HeaderText = Language._Object.getCaption(Me.dgView.Columns(1).FooterText, Me.dgView.Columns(1).HeaderText).Replace("&", "")
            Me.dgView.Columns(2).HeaderText = Language._Object.getCaption(Me.dgView.Columns(2).FooterText, Me.dgView.Columns(2).HeaderText)
            Me.dgView.Columns(3).HeaderText = Language._Object.getCaption(Me.dgView.Columns(3).FooterText, Me.dgView.Columns(3).HeaderText)
            Me.dgView.Columns(4).HeaderText = Language._Object.getCaption(Me.dgView.Columns(4).FooterText, Me.dgView.Columns(4).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

   
End Class
