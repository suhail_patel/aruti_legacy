﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class wPgEmployeeFeedbackList
    Inherits Basepage

#Region " Private Variable(s) "

    Private objFMaster As clshrtnafeedback_master
    Dim msg As New CommonCodes

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEmployeeFeedbackList"
    'Anjan [04 June 2014] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Dim objEmp As New clsEmployee_Master
        Dim objTSch As New clsTraining_Scheduling
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                '    dsFill = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsFill = objEmp.GetEmployeeList("Emp", True, )
                'End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsFill = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsFill = objEmp.GetEmployeeList("Emp", True, )
                '    dsFill = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If
                dsFill = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Emp", True)

                'Shani(24-Aug-2015) -- End
                'Sohail (23 Apr 2012) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsFill.Tables("Emp").Copy
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            dsFill = objTSch.getComboList("List", True, Session("Fin_year"), True)
            With cboCourseTitle
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsFill.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = 0
            End If
            cboCourseTitle.SelectedValue = 0
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try


            If (Session("LoginBy") = Global.User.en_loginby.User) AndAlso CBool(Session("AllowToViewLevel1EvaluationList")) = False Then Exit Sub


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            dsList = objFMaster.GetList("List")

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = '" & CInt(cboEmployee.SelectedValue) & "' "
            End If

            If CInt(cboCourseTitle.SelectedValue) > 0 Then
                StrSearching &= "AND trainingschedulingunkid = '" & CInt(cboCourseTitle.SelectedValue) & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dtTable = New DataView(dsList.Tables(0), StrSearching, "TrainingAttended", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(dsList.Tables(0), "", "TrainingAttended", DataViewRowState.CurrentRows).ToTable
            End If

            dgvList.DataSource = dtTable
            dgvList.DataKeyField = "feedbackmasterunkid"
            dgvList.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Sub delete()
        Try
            popup1.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            objFMaster = New clshrtnafeedback_master

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeFeedbackList"
            StrModuleName2 = "mnuTrainingInformation"
            StrModuleName3 = "mnuTrainingEvaluation"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            Dim clsuser As New User
            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End
                    'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)
                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End
                    'Aruti.Data.User._Object._Userunkid = -1 'Sohail (23 Apr 2012)
                End If
                Call FillCombo()


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'SHANI [01 FEB 2015]--END
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    dgvList.Columns(4).Visible = True
                    dgvList.Columns(5).Visible = True

                    'Pinkal (12-Feb-2015) -- Start
                    'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                    btnNew.Visible = CBool(Session("AllowToAddLevelIEvaluation"))
                    dgvList.Columns(0).Visible = CBool(Session("AllowToEditLevelIEvaluation"))
                    dgvList.Columns(1).Visible = CBool(Session("AllowToDeleteLevelIEvaluation"))
                    'Pinkal (12-Feb-2015) -- End

                Else
                    dgvList.Columns(4).Visible = False
                    dgvList.Columns(5).Visible = False
                End If
                'Pinkal (22-Mar-2012) -- End

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("EmpFeedBack_EmpUnkID") IsNot Nothing Then
                    cboEmployee.SelectedValue = Session("EmpFeedBack_EmpUnkID")
                    Session.Remove("EmpFeedBack_EmpUnkID")
                End If
                'SHANI [09 Mar 2015]--END 

                If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 Then
                    Call FillGrid()
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END

        End Try
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END

    End Sub

    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ClearObject()
            Call FillGrid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub BntNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try         'Hemant (13 Aug 2020)
        'SHANI [09 Mar 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        Session("EmpFeedBack_EmpUnkID") = cboEmployee.SelectedValue
        'SHANI [09 Mar 2015]--END 

        Response.Redirect("~\Training\wPgEmployeeFeedback.aspx")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup1.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try
            If (popup1.Reason.Trim = "") Then 'SHANI [01 FEB 2015]-If (txtreason.Text = "") Then 
                msg.DisplayMessage(" Please enter delete reason.", Me)
                Exit Sub
            End If

            objFMaster._Isvoid = True
            objFMaster._Voidreason = popup1.Reason.Trim  'SHANI [01 FEB 2015]-txtreason.Text
            objFMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objFMaster._Voiduserunkid = Aruti.Data.User._Object._Userunkid
            objFMaster._Voiduserunkid = Session("UserId")
            'Sohail (23 Apr 2012) -- End
            objFMaster.Delete(Me.ViewState("Unkid"))
            popup1.Dispose()
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Event(s) "

    Protected Sub dgvList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvList.ItemDataBound
        Try
            If (e.Item.ItemIndex >= 0) Then
                If e.Item.Cells(3).Text <> "&nbsp;" AndAlso e.Item.Cells(3).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(3).Text = CDate(eZeeDate.convertDate(e.Item.Cells(3).Text).ToString).Date
                End If
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvList.DeleteCommand
        Try
            If CBool(e.Item.Cells(6).Text) = True Then
                msg.DisplayMessage("Sorry, you cannot edit this information Reason : This information is already completed.", Me)
                Exit Sub
            End If

            Me.ViewState.Add("Unkid", dgvList.DataKeys(e.Item.ItemIndex))

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreason.Visible = True
            'SHANI [01 FEB 2015]--END
            delete()
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvList.ItemCommand
        Try
            If CBool(e.Item.Cells(6).Text) = True Then
                msg.DisplayMessage("Sorry, you cannot edit this information Reason : This information is already completed.", Me)
                Exit Sub
            End If

            If e.CommandName = "Select" Then

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                Session("EmpFeedBack_EmpUnkID") = cboEmployee.SelectedValue
                'SHANI [09 Mar 2015]--END 

                Response.Redirect("~\Training\wPgEmployeeFeedback.aspx?ProcessId=" & b64encode(Val(dgvList.DataKeys(e.Item.ItemIndex))))
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgvList.PageIndexChanged
        Try
            dgvList.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

#Region "ToolBar Event"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try
    '        Response.Redirect("~\Training\wPgEmployeeFeedback.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

    'Pinkal (22-Mar-2012) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.lblCourseTitle.Text = Language._Object.getCaption(Me.lblCourseTitle.ID, Me.lblCourseTitle.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.dgvList.Columns(0).HeaderText = Language._Object.getCaption("btnEdit", Me.dgvList.Columns(0).HeaderText).Replace("&", "")
            Me.dgvList.Columns(1).HeaderText = Language._Object.getCaption("btnDelete", Me.dgvList.Columns(1).HeaderText).Replace("&", "")
            Me.dgvList.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvList.Columns(3).FooterText, Me.dgvList.Columns(3).HeaderText)
            Me.dgvList.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvList.Columns(4).FooterText, Me.dgvList.Columns(4).HeaderText)
            Me.dgvList.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvList.Columns(5).FooterText, Me.dgvList.Columns(5).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End


End Class
