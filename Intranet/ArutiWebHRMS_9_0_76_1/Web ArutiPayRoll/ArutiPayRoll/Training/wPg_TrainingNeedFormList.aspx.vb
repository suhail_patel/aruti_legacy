﻿
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
Imports System.Drawing

#End Region

Partial Class Training_wPg_TrainingNeedFormList
    Inherits Basepage

    Private Enum colTrainingNeed
        Edit = 0
        Delete = 1
        PeriodName = 2
        StartDate = 3
        EndDate = 4
        TrainingCourseName = 5
        PriorityName = 6
    End Enum

#Region " Private Variable(s) "
    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmTrainingNeedFormList"
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mintTrainingneedformunkid As Integer = 0
#End Region

#Region " Form's Event(s) "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintTrainingneedformunkid", mintTrainingneedformunkid)
            Me.ViewState.Add("mdtPeriodStart", mdtPeriodStart)
            Me.ViewState.Add("mdtPeriodEnd", mdtPeriodEnd)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Trainings_Needs_Analysis) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If (Page.IsPostBack = False) Then

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call GetControlCaptions()

                Call SetVisibility()
                Call FillCombo()
                Call FillList(True)
            Else
                mintTrainingneedformunkid = CInt(ViewState("mintTrainingneedformunkid"))
                mdtPeriodStart = CDate(ViewState("mdtPeriodStart"))
                mdtPeriodEnd = CDate(ViewState("mdtPeriodEnd"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsCombo As DataSet
        Try
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "List", True, 0)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs)
            End With

            dsCombo = objMaster.GetPriority(True)
            With cboPriority
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillTrainingCourse()
        Dim objCommon As New clsCommon_Master
        Dim objCItems As New clscompeteny_customitem_tran
        Dim dsCombo As DataSet
        Dim dtTable As DataTable
        Try
            'If chkIncludeTraining.Checked = True Then
            '    dsCombo = objCItems.getTrainingCourseComboList(CInt(cboPeriod.SelectedValue), True, Nothing)
            '    dtTable = New DataView(dsCombo.Tables(0)).ToTable
            'Else
            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    dtTable = New DataView(dsCombo.Tables(0), "masterunkid IN (0) ", "", DataViewRowState.CurrentRows).ToTable
            'Else
            dtTable = New DataView(dsCombo.Tables(0)).ToTable
            'End If
            'End If

            With cboDevRequire
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList(Optional ByVal blnShowColumnHeaderOnly As Boolean = False)
        Dim objTNFTran As New clsTraining_need_form_tran
        Dim dsList As DataSet = Nothing
        Dim strFilter As String = ""
        Try
            If blnShowColumnHeaderOnly = True OrElse CBool(Session("AllowToViewTrainingNeedForm")) = False Then
                strFilter &= " AND 1 = 2 "
            Else
                If txtFormNo.Text.Trim <> "" Then
                    strFilter &= " AND hrtraining_need_form_master.formno LIKE '%" & txtFormNo.Text & "%' "
                End If
                If CInt(cboPeriod.SelectedValue) > 0 Then
                    strFilter &= " AND hrtraining_need_form_tran.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
                End If
                If CInt(cboDevRequire.SelectedValue) > 0 Then
                    strFilter &= " AND hrtraining_need_form_tran.trainingcourseunkid = " & CInt(cboDevRequire.SelectedValue) & " "
                End If
                If CInt(cboPriority.SelectedValue) > 0 Then
                    strFilter &= " AND hrtraining_need_form_tran.priority = " & CInt(cboPriority.SelectedValue) & " "
                End If
                If dtpDatefrom.GetDate <> Nothing Then
                    strFilter &= " AND CONVERT(CHAR(8), hrtraining_need_form_tran.start_date, 112) >= '" & eZeeDate.convertDate(dtpDatefrom.GetDate.Date) & "' "
                End If
                If dtpDateTo.GetDate <> Nothing Then
                    strFilter &= " AND CONVERT(CHAR(8), hrtraining_need_form_tran.end_date, 112) <= '" & eZeeDate.convertDate(dtpDateTo.GetDate.Date) & "' "
                End If
            End If

            dsList = objTNFTran.GetList("List", 0, strFilter, True)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            mintTrainingneedformunkid = 0
            Dim blnVisible As Boolean = True
            If dsList.Tables(0).Rows.Count <= 0 Then
                Dim dr As DataRow = dsList.Tables(0).NewRow
                dsList.Tables(0).Rows.Add(dr)
                blnVisible = False
            End If
            dgvTrainingNeed.DataSource = dsList.Tables(0)
            dgvTrainingNeed.DataBind()
            dgvTrainingNeed.Rows(0).Visible = blnVisible
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = CBool(Session("AllowToAddTrainingNeedForm"))
            dgvTrainingNeed.Columns(colTrainingNeed.Edit).Visible = CBool(Session("AllowToEditTrainingNeedForm"))
            dgvTrainingNeed.Columns(colTrainingNeed.Delete).Visible = CBool(Session("AllowToDeleteTrainingNeedForm"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date
            End If

            Call FillTrainingCourse()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("trainingneedformunkid") = Nothing
            Response.Redirect(Session("servername") & "~/Training/wPg_TrainingNeedForm.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboDevRequire.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            txtFormNo.Text = ""
            dtpDatefrom.SetDate = Nothing
            dtpDateTo.SetDate = Nothing

            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.ViewState("IsLink") IsNot Nothing Then
                Response.Redirect("~\Index.aspx", False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonYes_Click
        Try
            popupDelReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDelReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelReason.buttonDelReasonYes_Click
        Dim objTNFMaster As New clsTraining_need_form_master
        Try

            If mintTrainingneedformunkid > 0 Then

                objTNFMaster._Trainingneedformunkid = mintTrainingneedformunkid
                objTNFMaster._Isvoid = True
                objTNFMaster._Voiduserunkid = CInt(Session("UserId"))
                objTNFMaster._Voiddatetime = DateTime.Now
                objTNFMaster._Voidreason = popupDelReason.Reason.ToString

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    objTNFMaster._AuditUserId = CInt(Session("UserId"))
                Else
                    objTNFMaster._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                End If

                objTNFMaster._AuditDate = DateTime.Now
                objTNFMaster._ClientIP = Session("IP_ADD").ToString()
                objTNFMaster._HostName = Session("HOST_NAME").ToString()
                objTNFMaster._FormName = mstrModuleName
                objTNFMaster._Isweb = True

                If objTNFMaster.Delete() = True Then
                    Call FillList()
                End If

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTNFMaster = Nothing
        End Try
    End Sub

#End Region

#Region " GridView Event "
    Protected Sub dgvTrainingNeed_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvTrainingNeed.RowCommand
        Try
            Dim TNFSrNo As Integer
            If e.CommandName = "Change" Then

                TNFSrNo = CInt(e.CommandArgument)

                Session("trainingneedformunkid") = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingneedformunkid").ToString)
                Response.Redirect(Session("servername") & "~/Training/wPg_TrainingNeedForm.aspx", False)

            ElseIf e.CommandName = "Remove" Then
                TNFSrNo = CInt(e.CommandArgument)

                mintTrainingneedformunkid = CInt(dgvTrainingNeed.DataKeys(TNFSrNo).Item("trainingneedformunkid").ToString)

                popupYesNo.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvTrainingNeed_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvTrainingNeed.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If IsDBNull(CType(e.Row.DataItem, DataRowView).Item(0)) = True Then Exit Sub

                If CBool(dgvTrainingNeed.DataKeys(e.Row.RowIndex)("IsGrp").ToString) = True Then
                    e.Row.Cells(colTrainingNeed.PeriodName).Text = "Form No. : " & DataBinder.Eval(e.Row.DataItem, "formno").ToString
                    e.Row.Cells(colTrainingNeed.PeriodName).ColumnSpan = e.Row.Cells.Count - CInt(colTrainingNeed.PeriodName)
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    For i As Integer = CInt(colTrainingNeed.PeriodName) + 1 To e.Row.Cells.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next

                    Dim lnkEdit As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)
                    lnkEdit.Visible = False

                    Dim lnkDelete As LinkButton = TryCast(e.Row.FindControl("ImgDelete"), LinkButton)
                    lnkDelete.Visible = False

                Else
                    e.Row.Cells(colTrainingNeed.StartDate).Text = CDate(e.Row.Cells(colTrainingNeed.StartDate).Text).ToString("dd-MMM-yyyy")
                    e.Row.Cells(colTrainingNeed.EndDate).Text = CDate(e.Row.Cells(colTrainingNeed.EndDate).Text).ToString("dd-MMM-yyyy")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            Language._Object.setCaption(Me.lblFormNo.ID, Me.lblFormNo.Text)
            Language._Object.setCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Language._Object.setCaption(Me.lblDatefrom.ID, Me.lblDatefrom.Text)
            Language._Object.setCaption(Me.lblDateTo.ID, Me.lblDateTo.Text)
            Language._Object.setCaption(Me.lblDevRequire.ID, Me.lblDevRequire.Text)
            Language._Object.setCaption(Me.lblPriority.ID, Me.lblPriority.Text)

            Language._Object.setCaption(dgvTrainingNeed.Columns(0).FooterText, dgvTrainingNeed.Columns(0).HeaderText)
            Language._Object.setCaption(dgvTrainingNeed.Columns(1).FooterText, dgvTrainingNeed.Columns(1).HeaderText)
            Language._Object.setCaption(dgvTrainingNeed.Columns(2).FooterText, dgvTrainingNeed.Columns(2).HeaderText)
            Language._Object.setCaption(dgvTrainingNeed.Columns(3).FooterText, dgvTrainingNeed.Columns(3).HeaderText)
            Language._Object.setCaption(dgvTrainingNeed.Columns(4).FooterText, dgvTrainingNeed.Columns(4).HeaderText)
            Language._Object.setCaption(dgvTrainingNeed.Columns(5).FooterText, dgvTrainingNeed.Columns(5).HeaderText)
            Language._Object.setCaption(dgvTrainingNeed.Columns(6).FooterText, dgvTrainingNeed.Columns(6).HeaderText)

            Language._Object.setCaption(Me.btnNew.ID, Me.btnNew.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            Language._Object.setCaption(Me.popupYesNo.ID, Me.popupYesNo.Title)
            Language._Object.setCaption(Me.popupDelReason.ID, Me.popupDelReason.Title)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            lblDetailHeader.Text = Language._Object.getCaption(Me.lblDetailHeader.ID, Me.lblDetailHeader.Text)
            lblFormNo.Text = Language._Object.getCaption(Me.lblFormNo.ID, Me.lblFormNo.Text)
            lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            lblDatefrom.Text = Language._Object.getCaption(Me.lblDatefrom.ID, Me.lblDatefrom.Text)
            lblDateTo.Text = Language._Object.getCaption(Me.lblDateTo.ID, Me.lblDateTo.Text)
            lblDevRequire.Text = Language._Object.getCaption(Me.lblDevRequire.ID, Me.lblDevRequire.Text)
            lblPriority.Text = Language._Object.getCaption(Me.lblPriority.ID, Me.lblPriority.Text)

            dgvTrainingNeed.Columns(0).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(0).FooterText, dgvTrainingNeed.Columns(0).HeaderText)
            dgvTrainingNeed.Columns(1).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(1).FooterText, dgvTrainingNeed.Columns(1).HeaderText)
            dgvTrainingNeed.Columns(2).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(2).FooterText, dgvTrainingNeed.Columns(2).HeaderText)
            dgvTrainingNeed.Columns(3).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(3).FooterText, dgvTrainingNeed.Columns(3).HeaderText)
            dgvTrainingNeed.Columns(4).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(4).FooterText, dgvTrainingNeed.Columns(4).HeaderText)
            dgvTrainingNeed.Columns(5).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(5).FooterText, dgvTrainingNeed.Columns(5).HeaderText)
            dgvTrainingNeed.Columns(6).HeaderText = Language._Object.getCaption(dgvTrainingNeed.Columns(6).FooterText, dgvTrainingNeed.Columns(6).HeaderText)

            btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            popupYesNo.Title = Language._Object.getCaption(Me.popupYesNo.ID, Me.popupYesNo.Title)
            popupYesNo.Message = Language.getMessage(mstrModuleName, 1, "Are you sure you want to delete this Training Need Form?")
            popupDelReason.Title = Language._Object.getCaption(Me.popupDelReason.ID, Me.popupDelReason.Title)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setLanguage(mstrModuleName)

            Language.setMessage(mstrModuleName, 1, "Are you sure you want to delete this Training Need Form?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
