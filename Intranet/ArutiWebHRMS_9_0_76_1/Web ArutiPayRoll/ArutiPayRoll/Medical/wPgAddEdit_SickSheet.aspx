﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPgAddEdit_SickSheet.aspx.vb"
    Inherits="Medical_wPgAddEdit_SickSheet" MasterPageFile="~/home.master"
    Title="Sick Sheet" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 70%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Sick Sheet Form"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Sick Sheet Add/Edit"></asp:Label>
                                    </div>
                    </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position:relative">
                                    <table style="width: 100%">
                        <tr style="width: 100%">
                            <td style="width: 15%">
                                                <asp:Label ID="lblSheetNo" runat="server" Text="Sick Sheet No " />
                            </td>
                            <td style="width: 35%">
                                                <asp:TextBox ID="txtSickSheetNo" runat="server" />
                            </td>
                            <td style="width: 15%">
                                                <asp:Label ID="lblDate" runat="server" Text="Date" />
                            </td>
                            <td style="width: 35%">
                                <uc2:DateCtrl ID="dtSheetdate" runat="server" AutoPostBack="false" />
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 15%">
                                                <asp:Label ID="lblProvider" runat="server" Text="Provider" />
                            </td>
                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpProvider" runat="server">
                                                </asp:DropDownList>
                            </td>
                            <td style="width: 15%">
                                                <asp:Label ID="lblEmployeecode" runat="server" Text="Employee Code" />
                            </td>
                            <td style="width: 35%">
                                                <asp:TextBox ID="txtEmployeeCode" runat="server" AutoPostBack="true" />
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 15%">
                                                <asp:Label ID="lblEmployeeName" runat="server" Text="Employee Name" />
                            </td>
                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpEmployeeName" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                            </td>
                            <td style="width: 15%">
                                                <asp:Label ID="LblSickSheetAllocation" runat="server" Text="#Allocation" />
                            </td>
                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboSickSheetAllocation" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 15%">
                                                <asp:Label ID="lblJob" runat="server" Text="Job / Position" />
                            </td>
                            <td style="width: 35%">
                                                <asp:TextBox ID="txtJob" runat="server" ReadOnly="true" />
                            </td>
                            <td style="width: 15%">
                            </td>
                            <td style="width: 35%">
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td style="width: 15%">
                                                <asp:Label ID="lblCover" runat="server" Text="Medical Cover" />
                            </td>
                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpMedicalCover" runat="server">
                                                </asp:DropDownList>
                            </td>
                            <td style="width: 15%">
                            </td>
                            <td style="width: 35%">
                            </td>
                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%" colspan="4">
                                                <asp:Panel ID="pnl_gvdependants" style="margin-top:5px" ScrollBars="Auto" Height="300px" runat="server">
                                                    <asp:DataGrid ID="gvdependants" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                        <Columns>
                            <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkAllDependant" runat="server" OnCheckedChanged="chkAllDependance_CheckedChanged"
                                        AutoPostBack="true" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkDependant" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="dependants" HeaderText="Dependant" ReadOnly="true" FooterText="colhEmpName"
                                HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="colhGender"
                                HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                HeaderStyle-HorizontalAlign="Left" FooterText="colhAge" />
                            <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                HeaderStyle-HorizontalAlign="Left" FooterText="colhRelation" />
                            <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundColumn DataField="dependent_Id" Visible="false"></asp:BoundColumn>
                        </Columns>
                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        <asp:HiddenField ID="hdpopup" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc8:ConfirmYesNo ID="popYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
<%--<asp:Panel ID="" CssClass="pnlcontentmain" Width="80%" Height="400px" runat="server"
    Style="margin: 0 auto;">
    <table style="width: 100%" id="tblmain">
        <tr style="width: 100%;">
            <td>
                <div id="Loginhe">
                    <uc1:Closebutton ID="Closebotton1" runat="server" PageHeading="Sick Sheet" />
                </div>
            </td>
        </tr>
        <tr style="width: 90%;" valign="top">
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td align="center" valign="top">
                            <uc3:ToolbarControl ID="ToolbarEntry1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="width: 100%">
            <td>
                <table style="width: 100%; padding-right: 10px">
                </table>
            </td>
        </tr>
        <tr>
            <td>
                </td>
            </tr>
            <tr>
                <td style="width: 100%" align="right">
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
</asp:Panel>--%>
