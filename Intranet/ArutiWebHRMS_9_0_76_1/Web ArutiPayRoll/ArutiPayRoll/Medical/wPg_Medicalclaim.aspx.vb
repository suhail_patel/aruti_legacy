﻿Option Strict On


#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class Medical_wPg_Medicalclaim
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim objMedicalclaim_master As clsmedical_claim_master
    Dim objMedicalclaim_tran As clsmedical_claim_Tran
    Dim mdtTran As DataTable = Nothing


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmMedicalClaim_AddEdit"
    Private ReadOnly mstrModuleName1 As String = "frmImport_MedicalClaim"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsFill As DataSet = Nothing
        Try


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            'FOR EMPLOYEE 
            'Dim objEmployee As New clsEmployee_Master

            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            ''Pinkal (25-APR-2012) -- End
            'drpEmployee.DataTextField = "employeename"
            'drpEmployee.DataValueField = "employeeunkid"
            'drpEmployee.DataSource = dsFill.Tables("Employee")
            'drpEmployee.DataBind()

            'drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            'Pinkal (21-Jul-2014) -- End

            'FOR YEAR
            dsFill = Nothing
            Dim objPeriod As New clscommom_period_Tran

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, StatusType.Open)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), "Period", True, StatusType.Open, , , Session("Database_Name"))
            dsFill = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")).Date, "Period", True, StatusType.Open)
            'Shani(20-Nov-2015) -- End

            'Pinkal (25-APR-2012) -- End

            drpPeriod.DataTextField = "name"
            drpPeriod.DataValueField = "periodunkid"
            drpPeriod.DataSource = dsFill.Tables("Period")
            drpPeriod.DataBind()


            'FOR MEDICAL INSTITUTE
            dsFill = Nothing
            Dim objInstitute As New clsinstitute_master

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            'dsFill = objInstitute.getListForCombo(True, "Institute", True, 1)
            dsFill = objInstitute.getListForCombo(True, "Institute", True, 1, False, CInt(Session("UserId")))
            'Pinkal (25-APR-2012) -- End


            drpProvider.DataTextField = "name"
            drpProvider.DataValueField = "instituteunkid"
            drpProvider.DataSource = dsFill.Tables("Institute")
            drpProvider.DataBind()


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            drpProvider_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (21-Jul-2014) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Try
            drpProvider.SelectedValue = objMedicalclaim_master._Instituteunkid.ToString()
            drpProvider_SelectedIndexChanged(New Object(), New EventArgs())

            If Session("MedicalclaimId") IsNot Nothing Then
                drpProvider.Enabled = False
            End If

            drpPeriod.SelectedValue = objMedicalclaim_master._Periodunkid.ToString()
            txtInvoiceNo.Text = objMedicalclaim_master._InvoiceNo
            txtTotInvoiceAmt.Text = Format(CDec(objMedicalclaim_master._TotalInvoiceAmount), Session("fmtCurrency").ToString())
            If objMedicalclaim_master._InvoiceDate = Nothing Then
                dtpInvoiceDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            Else
                dtpInvoiceDate.SetDate = objMedicalclaim_master._InvoiceDate.Date
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objMedicalclaim_master._Instituteunkid = CInt(drpProvider.SelectedValue)
            objMedicalclaim_master._InvoiceNo = txtInvoiceNo.Text
            objMedicalclaim_master._TotalInvoiceAmount = CDec(txtTotInvoiceAmt.Text)
            objMedicalclaim_master._Periodunkid = CInt(drpPeriod.SelectedValue)
            objMedicalclaim_master._Statusunkid = 1 'Pending
            objMedicalclaim_master._IsFinal = CBool(Me.ViewState("isfinal"))
            objMedicalclaim_master._InvoiceDate = dtpInvoiceDate.GetDate
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Try

            Dim dtClaimTable As DataTable = New DataView(CType(Session("ClaimList"), DataTable), "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

            If dtClaimTable IsNot Nothing Then
                GvMedicalClaim.DataSource = dtClaimTable
                GvMedicalClaim.DataBind()

                If dtClaimTable.Rows.Count > 0 Then
                    Dim dtrow As DataRow() = dtClaimTable.Select("AUD <> 'D'")
                    Dim dtTable As DataTable = dtClaimTable.Clone
                    If dtrow.Length > 0 Then
                        For i As Integer = 0 To dtrow.Length - 1
                            dtTable.ImportRow(dtrow(i))
                        Next
                        txtTotalAmount.Text = Format(CDec(dtTable.Compute("sum(amount)", "1=1")), Session("fmtCurrency").ToString())

                        'Pinkal (21-Jul-2014) -- Start
                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                        If dtClaimTable IsNot Nothing AndAlso dtClaimTable.Rows.Count > 0 AndAlso dtClaimTable.Rows(0)("sicksheetno").ToString.Trim.Length > 0 Then
                            txtSickSheetNo.Text = dtClaimTable.Rows(0)("sicksheetno").ToString().Trim
                            txtSickSheetNo_TextChanged(New Object(), New EventArgs())
                        End If

                        If dtClaimTable IsNot Nothing AndAlso dtClaimTable.Rows.Count > 0 AndAlso CInt(dtClaimTable.Rows(0)("employeeunkid")) > 0 Then
                            drpEmployee.SelectedValue = CInt(dtClaimTable.Rows(0)("employeeunkid")).ToString()
                            drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())
                        End If
                        'Pinkal (21-Jul-2014) -- End


                    Else
                        txtSickSheetNo.Text = ""
                        txtClaimno.Text = ""
                        txtRemark.Text = ""
                        txtclaimAmount.Text = Format(0, Session("fmtCurrency").ToString())
                        txtTotalAmount.Text = Format(0, Session("fmtCurrency").ToString())
                        If drpdepedants.Items.Count > 0 Then drpdepedants.SelectedIndex = 0
                        If drpPeriod.Items.Count > 0 Then drpPeriod.SelectedIndex = 0
                        chkEmpexempted.Checked = False
                    End If

                End If

            End If
            If GvMedicalClaim.Rows.Count > 0 Then
                dtpInvoiceDate.Enabled = False
            Else
                dtpInvoiceDate.Enabled = True
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                GvMedicalClaim.PageIndex = 0
                GvMedicalClaim.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Private Sub BlankObjects()
        Try
            If drpEmployee.Items.Count > 0 Then drpEmployee.SelectedIndex = 0
            If drpdepedants.Items.Count > 0 Then drpdepedants.SelectedIndex = 0
            If drpPeriod.Items.Count > 0 Then drpPeriod.SelectedIndex = 0
            If drpProvider.Items.Count > 0 Then drpProvider.SelectedIndex = 0
            txtRemark.Text = ""
            txtSickSheetNo.Text = ""
            txtClaimno.Text = ""
            txtclaimAmount.Text = Format(0, Session("fmtCurrency").ToString())
            txtInvoiceNo.Text = ""
            txtTotInvoiceAmt.Text = Format(0, Session("fmtCurrency").ToString())
            txtTotalAmount.Text = Format(0, Session("fmtCurrency").ToString())
            dtClaimdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(drpEmployee.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Employee is compulsory information.Please Select Employee.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Select Employee."), Me)
                'Pinkal (06-May-2014) -- End
                drpEmployee.Focus()
                Return False

            ElseIf CInt(drpPeriod.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Pay Period is compulsory information.Please Select Pay Period.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period."), Me)
                'Pinkal (06-May-2014) -- End
                drpPeriod.Focus()
                Return False

            ElseIf CInt(drpProvider.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Provider is compulsory information.Please Select Provider.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider."), Me)
                'Pinkal (06-May-2014) -- End
                drpProvider.Focus()
                Return False

            ElseIf txtSickSheetNo.Enabled AndAlso txtSickSheetNo.Text.Trim = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Sick Sheet No cannot be blank.Sick Sheet No is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sick Sheet No cannot be blank.Sick Sheet No is required information."), Me)
                'Pinkal (06-May-2014) -- End
                txtSickSheetNo.Focus()
                Return False

            ElseIf txtclaimAmount.Text.Trim = "" Or CDec(txtclaimAmount.Text) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Claim Amount cannot be blank.Claim Amount is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Claim Amount cannot be blank.Claim Amount is required information."), Me)
                'Pinkal (06-May-2014) -- End

                txtclaimAmount.Focus()
                Return False

                'Pinkal (10-Dec-2012) -- Start
                'Enhancement : TRA Changes
            ElseIf dtClaimdate.IsNull Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Claim Date is compulsory information.Please Select Claim Date.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 10, "Claim Date is compulsory information.Please Select Claim Date."), Me)
                'Pinkal (06-May-2014) -- End


                dtClaimdate.Focus()
                Return False
                'Pinkal (10-Dec-2012) -- End


            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub ClearControls()
        Try
            If drpdepedants.Items.Count > 0 Then drpdepedants.SelectedIndex = 0
            txtRemark.Text = ""
            Me.ViewState("SickSheetId") = Nothing
            txtClaimno.Text = ""
            dtClaimdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            txtclaimAmount.Text = Format(CDec("0"), Session("fmtCurrency").ToString())
            chkEmpexempted.Checked = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearControls :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidSickSheetNo(ByVal isEdit As Boolean) As Boolean
        Try

            'START FOR CHECK UNIQUE CLAIM NO

            Dim drClaimno As DataRow() = Nothing
            Dim dtTable As DataTable = CType(Session("ClaimList"), DataTable)

            If isEdit = False Then
                drClaimno = dtTable.Select("claimno = '" & txtClaimno.Text.Trim & "' AND claimno <> '' AND AUD <> 'D'")
            Else
                If Me.ViewState("ClaimTranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("ClaimTranunkid")) > 0 Then
                    drClaimno = dtTable.Select("claimno = '" & txtClaimno.Text.Trim & "' AND claimno <> '' AND AUD <> 'D' AND claimtranunkid <> " & CInt(Me.ViewState("ClaimTranunkid")))
                Else
                    drClaimno = dtTable.Select("claimno = '" & txtClaimno.Text.Trim & "' AND claimno <> '' AND GUID <> '" & Me.ViewState("GUID").ToString() & "'")
                End If
            End If

            If drClaimno.Length > 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("This Claim No is already added to the list.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "This Claim No is already added to the list."), Me)
                'Pinkal (06-May-2014) -- End

                txtClaimno.Focus()
                Return False
            End If

            'END FOR CHECK UNIQUE CLAIM NO 


            'START FOR CHECK UNIQUE SICKSHEET NO 

            If txtSickSheetNo.Enabled Then

                Dim objSickSheet As New clsmedical_sicksheet

                If objSickSheet.isExist(txtSickSheetNo.Text.Trim) = False Then

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'DisplayMessage.DisplayMessage("Invalid Sick Sheet No.", Me)
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Invalid Sick Sheet No."), Me)
                    'Pinkal (06-May-2014) -- End
                    txtClaimno.Focus()
                    Return False

                ElseIf objSickSheet.isExist(txtSickSheetNo.Text.Trim, -1, CInt(drpEmployee.SelectedValue)) = False Then

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'DisplayMessage.DisplayMessage("This Sick Sheet No is not valid for this employee.", Me)
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This Sick Sheet No is not valid for this employee."), Me)
                    'Pinkal (06-May-2014) -- End
                    txtClaimno.Focus()
                    Return False
                End If

            End If

            'END FOR CHECK UNIQUE SICKSHEET NO 

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidSickSheetNo :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Function IsEmployeeExistForthisInvoice(ByVal isEdit As Boolean) As Boolean
        Try

            If Session("ClaimList") IsNot Nothing Then

                Dim dtClaim As DataTable = CType(Session("ClaimList"), DataTable)

                If dtClaim.Rows.Count > 0 Then
                    Dim drRow As DataRow() = Nothing
                    Dim strSearching As String = ""

                    If CInt(drpEmployee.SelectedValue) > 0 AndAlso CInt(drpdepedants.SelectedValue) <= 0 Then
                        strSearching = "employeeunkid = " & CInt(drpEmployee.SelectedValue) & " AND sicksheetno = '" & txtSickSheetNo.Text.Trim & "' AND dependantsunkid <= 0 AND AUD <>'D'"

                    ElseIf CInt(drpEmployee.SelectedValue) > 0 AndAlso CInt(drpdepedants.SelectedValue) > 0 Then
                        strSearching = "employeeunkid = " & CInt(drpEmployee.SelectedValue) & " AND sicksheetno = '" & txtSickSheetNo.Text.Trim & "' AND dependantsunkid =  " & CInt(drpdepedants.SelectedValue) & " AND AUD <> 'D'"

                    End If

                    strSearching &= " AND claimno ='" & txtClaimno.Text.Trim & "' AND claimno <> ''"

                    If isEdit Then
                        strSearching &= " AND claimtranunkid <> " & CInt(Me.ViewState("ClaimTranunkid"))
                    End If

                    drRow = dtClaim.Select(strSearching, "")

                    If drRow.Length > 0 Then

                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Language Changes 
                        'DisplayMessage.DisplayMessage("This Employee has same Sick sheet No for this invoice.Please define another sick sheet no.", Me)
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "This Employee has same Sick sheet No for this invoice.Please define another sick sheet no."), Me)
                        'Pinkal (06-May-2014) -- End


                        Return False
                    End If

                End If

            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsEmployeeExistForthisInvoice :-  " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Function ImportValidation() As Boolean
        Try
            If drpImportEmployeeCode.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Employee Code is compulsory information.Please map employee code.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Employee Code is compulsory information.Please map employee code."), Me)
                'Pinkal (06-May-2014) -- End


                drpImportEmployeeCode.Focus()
                Return False

            ElseIf drpImportEmployee.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Employee is compulsory information.Please map employee.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 6, "Employee is compulsory information.Please map employee."), Me)
                'Pinkal (06-May-2014) -- End

                drpImportEmployee.Focus()
                Return False

            ElseIf drpImportDependant.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Dependant is compulsory information.Please map depedent.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 7, "Dependant is compulsory information.Please map depedent."), Me)
                'Pinkal (06-May-2014) -- End


                drpImportDependant.Focus()
                Return False

            ElseIf drpImportSickSheet.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Sick Sheet is compulsory information.Please map sicksheet.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 8, "Sick Sheet is compulsory information.Please map sicksheet."), Me)
                'Pinkal (06-May-2014) -- End


                drpImportSickSheet.Focus()
                Exit Function

            ElseIf drpImportClaimNo.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Claim No is compulsory information.Please map claim no.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 9, "Claim No is compulsory information.Please map claim no."), Me)
                'Pinkal (06-May-2014) -- End


                drpImportClaimNo.Focus()
                Return False

            ElseIf drpImportClaimDate.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Claim Date is compulsory information.Please claim date.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 10, "Claim Date is compulsory information.Please claim date."), Me)
                'Pinkal (06-May-2014) -- End
                drpImportClaimDate.Focus()
                Return False

            ElseIf drpImportClaimAmount.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Claim Amount is compulsory information.Please map claim amount.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 11, "Claim Amount is compulsory information.Please map claim amount."), Me)
                'Pinkal (06-May-2014) -- End
                drpImportClaimAmount.Focus()
                Return False


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
            ElseIf drpImportExemptFromDeduction.Text = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Exempt From Deduction is compulsory information.Please map Exempt From Deduction.", Me)
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 18, "Exempt From Deduction is compulsory information.Please map Exempt From Deduction."), Me)
                'Pinkal (06-May-2014) -- End
                drpImportExemptFromDeduction.Focus()
                Return False
                'Pinkal (18-Dec-2012) -- End


            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ImportValidation :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Sub CreateDataForExcel()
        Try
            Dim exError As Exception
            Dim dtClaim As DataTable = CType(Session("ClaimList"), DataTable)
            Dim mdtOther As DataTable = CType(Session("ImportList"), DataTable)

            If dtClaim.Columns.Contains("employeecode") = False Then
                dtClaim.Columns.Add("employeecode", Type.GetType("System.String"))
            End If

            Dim objEmployee As New clsEmployee_Master
            Dim objsickSheet As New clsmedical_sicksheet
            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim objInstitute As New clsinstitute_master
            objInstitute._Instituteunkid = CInt(drpProvider.SelectedValue)

            For i As Integer = 0 To mdtOther.Rows.Count - 1

                Dim drRow As DataRow = dtClaim.NewRow

                drRow("claimtranunkid") = -1
                drRow("claimunkid") = -1
                drRow("employeeunkid") = -1
                drRow("dependantsunkid") = -1
                drRow("sicksheetunkid") = -1

                drRow("employeecode") = mdtOther.Rows(i)(drpImportEmployeeCode.Text)
                drRow("employeename") = mdtOther.Rows(i)(drpImportEmployee.Text)
                drRow("dependentsname") = mdtOther.Rows(i)(drpImportDependant.Text)
                drRow("sicksheetno") = mdtOther.Rows(i)(drpImportSickSheet.Text)
                drRow("claimno") = mdtOther.Rows(i)(drpImportClaimNo.Text)
                drRow("claimdate") = mdtOther.Rows(i)(drpImportClaimDate.Text)
                If IsDBNull(mdtOther.Rows(i)(drpImportClaimAmount.Text)) Or mdtOther.Rows(i)(drpImportClaimAmount.Text).ToString.Trim = "" Then
                    drRow("amount") = 0
                Else
                    drRow("amount") = mdtOther.Rows(i)(drpImportClaimAmount.Text)
                End If

                If mdtOther.Columns.Contains("remark") Then
                    drRow("remarks") = mdtOther.Rows(i)("remark")
                Else
                    drRow("remark") = ""
                End If

                If mdtOther.Columns.Contains("isempexempted") Then
                    drRow("isempexempted") = CBool(mdtOther.Rows(i)("isempexempted"))
                Else
                    drRow("isempexempted") = False
                End If

                If CInt(drRow.Item("employeeunkid")) <= 0 Then
                    drRow.Item("employeeunkid") = objEmployee.GetEmployeeUnkid("", drRow.Item("employeecode").ToString())
                End If

                If CInt(drRow.Item("employeeunkid")) <= 0 Then

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'exError = New Exception("Employee Not Found.")
                    Language.setLanguage(mstrModuleName1)
                    exError = New Exception(Language.getMessage(mstrModuleName1, 4, "Employee Not Found."))
                    'Pinkal (06-May-2014) -- End
                    Throw exError
                End If

                If CInt(drRow.Item("dependantsunkid")) <= 0 Then
                    'Sohail (18 May 2019) -- Start
                    'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                    'Dim dtEmployee = objDependant.GetQualifiedDepedant(CInt(drRow.Item("employeeunkid")), True, False, Nothing)
                    Dim dtEmployee = objDependant.GetQualifiedDepedant(CInt(drRow.Item("employeeunkid")), True, False, Nothing, dtAsOnDate:=Now)
                    'Sohail (18 May 2019) -- End
                    If dtEmployee.Tables(0).Rows.Count > 0 Then
                        Dim row As DataRow() = dtEmployee.Tables(0).Select("dependants = '" & drRow.Item("dependentsname").ToString() & "'")
                        If row.Length > 0 Then
                            drRow.Item("dependantsunkid") = row(0)("dependent_Id")
                        End If
                    End If
                End If

                If CInt(drRow.Item("dependantsunkid")) <= 0 And drRow.Item("dependentsname").ToString() <> "" Then

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'exError = New Exception("Dependants Not Found.")
                    Language.setLanguage(mstrModuleName1)
                    exError = New Exception(Language.getMessage(mstrModuleName1, 12, "Dependants Not Found."))
                    'Pinkal (06-May-2014) -- End


                End If

                If CInt(drRow.Item("sicksheetunkid")) <= 0 Then
                    If objInstitute._IsFormRequire Then

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Dim dtEmployee As DataSet = objsickSheet.GetList("List", True, drRow.Item("sicksheetno").ToString(), -1, Session("IsIncludeInactiveEmp"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("AccessLevelFilterString"), CInt(Session("UserId")))
                        Dim dtEmployee As DataSet = objsickSheet.GetList(Session("Database_Name").ToString(), _
                                                                         CInt(Session("UserId")), _
                                                                         CInt(Session("Fin_year")), _
                                                                         CInt(Session("CompanyUnkId")), _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                         Session("UserAccessModeSetting").ToString(), True, _
                                                                         True, "List", True, _
                                                                         drRow.Item("sicksheetno").ToString(), -1, "")
                        'Shani(20-Nov-2015) -- End

                        If dtEmployee.Tables(0).Rows.Count > 0 Then
                            drRow.Item("sicksheetunkid") = dtEmployee.Tables(0).Rows(0)("sicksheetunkid").ToString()
                        Else

                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            'exError = New Exception("Invalid Sick Sheet No.")
                            Language.setLanguage(mstrModuleName)
                            exError = New Exception(Language.getMessage(mstrModuleName, 9, "Invalid Sick Sheet No."))
                            'Pinkal (06-May-2014) -- End

                            Throw exError
                        End If

                    End If

                End If


                If CInt(drRow.Item("sicksheetunkid")) > 0 And CInt(drRow.Item("employeeunkid")) > 0 Then

                    If objInstitute._IsFormRequire Then

                        If objsickSheet.isExist(drRow.Item("sicksheetno").ToString(), , CInt(drRow.Item("employeeunkid"))) = False Then

                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            'exError = New Exception("This Sick Sheet No is not valid for this employee.")
                            Language.setLanguage(mstrModuleName)
                            exError = New Exception(Language.getMessage(mstrModuleName, 10, "This Sick Sheet No is not valid for this employee."))
                            'Pinkal (06-May-2014) -- End
                            Throw exError
                        End If

                    End If
                End If

                If CStr(drRow.Item("Claimno")).Trim.Length > 0 Then
                    Dim drClaimno As DataRow() = Nothing
                    drClaimno = dtClaim.Select("claimno = '" & CStr(drRow.Item("Claimno")).Trim & "' AND employeeunkid <>" & CInt(drRow.Item("employeeunkid")) & " AND employeeunkid <> -1 ")
                    If drClaimno.Length > 0 Then

                        'Pinkal (06-May-2014) -- Start
                        'Enhancement : Language Changes 
                        'exError = New Exception("This Claim No is already exist.")
                        Language.setLanguage(mstrModuleName1)
                        exError = New Exception(Language.getMessage(mstrModuleName1, 15, "This Claim No is already exist."))
                        'Pinkal (06-May-2014) -- End


                        Throw exError
                    End If
                End If

                drRow("AUD") = "A"
                drRow("GUID") = ""
                dtClaim.Rows.Add(drRow)

            Next
            Session("ClaimList") = dtClaim


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("CreateDataForExcel :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ClearImportControls()
        Try
            ModalPopupExtender1.Dispose()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearImportControls :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ForSave()
        Try
            If Session("Claimunkid") Is Nothing Then
                SetValue()
                objMedicalclaim_master.Insert(CType(Session("ClaimList"), DataTable))

                If objMedicalclaim_master._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage("Entry Not Saved Due To " & objMedicalclaim_master._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                    BlankObjects()
                End If

            ElseIf Session("Claimunkid") IsNot Nothing AndAlso CInt(Session("Claimunkid")) > 0 Then
                objMedicalclaim_master._Claimunkid = CInt(Session("Claimunkid"))
                SetValue()
                objMedicalclaim_master.Update(CType(Session("ClaimList"), DataTable))

                If objMedicalclaim_master._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage("Entry Not Updated Due To " & objMedicalclaim_master._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Updated Successfully !!!!!", Me)
                    BlankObjects()
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ForSave :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Medical_Bills_and_Claims_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objMedicalclaim_master = New clsmedical_claim_master
            objMedicalclaim_tran = New clsmedical_claim_Tran
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'Nilay (01-Feb-2015) -- End
            If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                objMedicalclaim_master._Userunkid = CInt(Session("UserId"))
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmMedicalClaim_AddEdit"
            StrModuleName2 = "mnuMedical"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            If Not IsPostBack Then
                btnSave.Visible = CBool(Session("AllowToSaveMedicalClaim"))
                btnFinalSave.Visible = CBool(Session("AllowToFinalSaveMedicalClaim"))
                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                btnAdd.Visible = CBool(Session("AddMedicalClaim"))
                btnEdit.Visible = CBool(Session("EditMedicalClaim"))
                btnDelete.Visible = CBool(Session("DeleteMedicalClaim"))
                GvMedicalClaim.Columns(0).Visible = CBool(Session("EditMedicalClaim"))
                'Pinkal (12-Feb-2015) -- End
                FillCombo()

                dtpInvoiceDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If Session("Claimunkid") IsNot Nothing Then
                    objMedicalclaim_master._Claimunkid = CInt(Session("Claimunkid"))
                    dtClaimdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    GetValue()
                    drpProvider.Enabled = False
                    objMedicalclaim_tran._Claimunkid = objMedicalclaim_master._Claimunkid

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objMedicalclaim_tran.GetClaimTran(Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), CInt(Session("UserId")))
                    objMedicalclaim_tran.GetClaimTran(Session("Database_Name").ToString(), _
                                                      CInt(Session("UserId")), _
                                                      CInt(Session("Fin_year")), _
                                                      CInt(Session("CompanyUnkId")), _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                      Session("UserAccessModeSetting").ToString(), True, _
                                                      True, objMedicalclaim_master._Claimunkid)
                    'Shani(20-Nov-2015) -- End


                    If Session("ClaimList") Is Nothing Then
                        Session.Add("ClaimList", objMedicalclaim_tran._DataList)
                    End If

                Else
                    dtClaimdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

                End If


                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                If Session("ClaimList") Is Nothing Then
                    Session.Add("ClaimList", objMedicalclaim_tran._DataList.Clone)
                End If

                txtclaimAmount.Text = Format(CDec(txtclaimAmount.Text), Session("fmtCurrency").ToString())
                txtTotInvoiceAmt.Text = Format(CDec(txtTotInvoiceAmt.Text), Session("fmtCurrency").ToString())
                txtTotalAmount.Text = Format(CDec(txtTotalAmount.Text), Session("fmtCurrency").ToString())

                FillList()

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleImageSaprator1(False)
    '    ToolbarEntry1.VisibleImageSaprator2(False)
    '    ToolbarEntry1.VisibleImageSaprator3(False)
    '    ToolbarEntry1.VisibleImageSaprator4(False)
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End



    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnGet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGet.Click
        Try

            If FlUpload.HasFile Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                Dim str As String = System.IO.Path.GetExtension(Me.FlUpload.PostedFile.FileName)
                If str.ToUpper <> ".XLS" AndAlso str.ToUpper <> ".XLSX" Then GoTo filevalid
                'Shani(24-Aug-2015) -- End

                'Dim iExcelData As New ExcelData
                'Dim ds As DataSet = iExcelData.Import(FlUpload.PostedFile.FileName)
                'Dim mdtOther As DataTable = iExcelData.Import(FlUpload.PostedFile.FileName).Tables(0).Copy

                Dim ds As DataSet = Aruti.Data.modGlobal.OpenXML_Import(FlUpload.PostedFile.FileName)
                Dim mdtOther As DataTable = Aruti.Data.modGlobal.OpenXML_Import(FlUpload.PostedFile.FileName).Tables(0).Copy

                If mdtOther IsNot Nothing Then

                    If mdtOther.Rows.Count > 0 Then

                        Dim drRow As DataRow() = mdtOther.Select("Employeecode IS NULL AND employee IS NULL AND Dependant IS NULL AND Sicksheet IS NULL AND Claimno IS NULL  AND ClaimDate IS NULL AND ClaimAmount IS NULL")
                        If drRow.Length > 0 Then
                            For i As Integer = 0 To drRow.Length - 1
                                mdtOther.Rows.Remove(drRow(i))
                            Next
                            mdtOther.AcceptChanges()
                        End If

                    End If
                End If


                drpImportEmployeeCode.Items.Clear()
                drpImportEmployee.Items.Clear()
                drpImportDependant.Items.Clear()
                drpImportSickSheet.Items.Clear()
                drpImportClaimNo.Items.Clear()
                drpImportClaimDate.Items.Clear()
                drpImportClaimAmount.Items.Clear()


                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                drpImportExemptFromDeduction.Items.Clear()
                'Pinkal (18-Dec-2012) -- End


                drpImportEmployeeCode.Items.Add("")
                drpImportEmployee.Items.Add("")
                drpImportDependant.Items.Add("")
                drpImportSickSheet.Items.Add("")
                drpImportClaimNo.Items.Add("")
                drpImportClaimDate.Items.Add("")
                drpImportClaimAmount.Items.Add("")

                'Pinkal (18-Dec-2012) -- Start
                'Enhancement : TRA Changes
                drpImportExemptFromDeduction.Items.Add("")
                'Pinkal (18-Dec-2012) -- End


                For Each dtColumns As DataColumn In mdtOther.Columns
                    drpImportEmployeeCode.Items.Add(dtColumns.ColumnName)
                    drpImportEmployee.Items.Add(dtColumns.ColumnName)
                    drpImportDependant.Items.Add(dtColumns.ColumnName)
                    drpImportSickSheet.Items.Add(dtColumns.ColumnName)
                    drpImportClaimNo.Items.Add(dtColumns.ColumnName)
                    drpImportClaimDate.Items.Add(dtColumns.ColumnName)
                    drpImportClaimAmount.Items.Add(dtColumns.ColumnName)

                    'Pinkal (18-Dec-2012) -- Start
                    'Enhancement : TRA Changes
                    drpImportExemptFromDeduction.Items.Add(dtColumns.ColumnName)
                    'Pinkal (18-Dec-2012) -- End

                Next

                If Session("ImportList") Is Nothing Then
                    Session.Add("ImportList", mdtOther)
                End If

            Else
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please the select proper file to Import Data from.", Me)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
filevalid:
                'Shani(24-Aug-2015) -- End

                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Please the select proper file to Import Data from."), Me)
                'Pinkal (06-May-2014) -- End
                ModalPopupExtender1.Hide()
            End If
            ModalPopupExtender1.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnGet_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnImportOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportOk.Click
        Try
            If ImportValidation() Then
                CreateDataForExcel()
                FillList()
                ModalPopupExtender1.Dispose()
            Else
                ModalPopupExtender1.Show()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnImportOk_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportData.Click
        Try

            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes

            If CInt(drpProvider.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Provider is compulsory information.Please Select Provider.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider."), Me)
                'Pinkal (06-May-2014) -- End
                drpProvider.Focus()
                Exit Sub
            End If

            'Pinkal (18-Dec-2012) -- End

            drpImportEmployeeCode.Items.Clear()
            drpImportEmployee.Items.Clear()
            drpImportDependant.Items.Clear()
            drpImportSickSheet.Items.Clear()
            drpImportClaimNo.Items.Clear()
            drpImportClaimDate.Items.Clear()
            drpImportClaimAmount.Items.Clear()


            'Pinkal (18-Dec-2012) -- Start
            'Enhancement : TRA Changes
            drpImportExemptFromDeduction.Items.Clear()
            'Pinkal (18-Dec-2012) -- End


            ModalPopupExtender1.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnImport_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            If Validation() Then

                If IsValidSickSheetNo(False) = False Then Exit Sub

                If IsEmployeeExistForthisInvoice(False) = False Then Exit Sub

                If txtSickSheetNo.Text.Trim.Length > 0 Then
                    Dim objsickSheet As New clsmedical_sicksheet

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim dsList As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim, -1, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), CInt(Session("UserId")))
                    Dim dsList As DataSet = objsickSheet.GetList(Session("Database_Name").ToString(), _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                                 True, "List", True, txtSickSheetNo.Text.Trim)
                    'Shani(20-Nov-2015) -- End


                    'Pinkal (18-Dec-2012) -- End


                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        Me.ViewState.Add("SickSheetId", dsList.Tables(0).Rows(0)("sicksheetunkid").ToString())
                    End If
                End If


                Dim dtTable As DataTable = CType(Session("ClaimList"), DataTable)

                Dim dtRow As DataRow = Nothing
                dtRow = dtTable.NewRow
                dtRow("claimtranunkid") = -1
                dtRow("claimunkid") = IIf(Session("MedicalclaimId") Is Nothing, -1, CInt(Session("MedicalclaimId")))
                dtRow("employeeunkid") = CInt(drpEmployee.SelectedValue)
                dtRow("employeename") = drpEmployee.SelectedItem.Text
                dtRow("dependantsunkid") = IIf(CInt(drpdepedants.SelectedValue) > 0, CInt(drpdepedants.SelectedValue), -1)
                dtRow("dependentsname") = IIf(CInt(drpdepedants.SelectedValue) > 0, drpdepedants.SelectedItem.Text, "")
                dtRow("sicksheetunkid") = IIf(Me.ViewState("SickSheetId") Is Nothing, -1, CInt(Me.ViewState("SickSheetId")))
                dtRow("sicksheetno") = txtSickSheetNo.Text
                dtRow("claimno") = txtClaimno.Text.Trim

                If dtClaimdate.IsNull = True Then
                    dtRow("claimdate") = DBNull.Value
                Else
                    dtRow("claimdate") = dtClaimdate.GetDate.Date
                End If
                dtRow("amount") = Format(CDec(txtclaimAmount.Text), Session("fmtCurrency").ToString())
                dtRow("remarks") = txtRemark.Text.Trim
                dtRow("AUD") = "A"
                dtRow("GUID") = Guid.NewGuid.ToString
                dtRow("isempexempted") = chkEmpexempted.Checked
                dtTable.Rows.Add(dtRow)
                dtTable.AcceptChanges()

                Session("ClaimList") = dtTable

                FillList()
                ClearControls()
                drpEmployee.Focus()

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnAdd_Click :-  " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try

            If Me.ViewState("ClaimTranunkid") Is Nothing AndAlso Me.ViewState("GUID") Is Nothing Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please select Medical Claim from the list to perform further operation on it.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub
            End If

            If Validation() Then

                If IsValidSickSheetNo(True) = False Then Exit Sub

                If IsEmployeeExistForthisInvoice(True) = False Then Exit Sub


                If txtSickSheetNo.Text.Trim.Length > 0 Then
                    Dim objsickSheet As New clsmedical_sicksheet

                    'Pinkal (25-APR-2012) -- Start
                    'Enhancement : TRA Changes
                    'Dim dsList As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim)

                    'Pinkal (5-MAY-2012) -- Start
                    'Enhancement : TRA Changes

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim dsList As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim, -1, Session("IsIncludeInactiveEmp"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("AccessLevelFilterString"), CInt(Session("UserId")))
                    Dim dsList As DataSet = objsickSheet.GetList(Session("Database_Name").ToString(), _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                                 True, "List", True, txtSickSheetNo.Text.Trim, -1, "")
                    'Shani(20-Nov-2015) -- End

                    'Pinkal (5-MAY-2012) -- End

                    'Pinkal (25-APR-2012) -- End


                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        Me.ViewState.Add("SickSheetId", dsList.Tables(0).Rows(0)("sicksheetunkid").ToString())
                    End If
                End If

                Dim dttable As DataTable = CType(Session("ClaimList"), DataTable)
                Dim drRow As DataRow() = Nothing

                If CInt(Me.ViewState("ClaimTranunkid")) > 0 Then
                    drRow = dttable.Select("ClaimTranunkid = " & CInt(Me.ViewState("ClaimTranunkid")))

                ElseIf Me.ViewState("GUID").ToString.Trim().Length > 0 Then
                    drRow = dttable.Select("GUID = '" & Me.ViewState("GUID").ToString() & "'")
                End If

                If drRow.Length > 0 Then

                    With drRow(0)
                        .Item("claimtranunkid") = IIf(Me.ViewState("ClaimTranunkid") Is Nothing, -1, Me.ViewState("ClaimTranunkid"))
                        .Item("claimunkid") = IIf(Session("MedicalclaimId") Is Nothing, -1, CInt(Session("MedicalclaimId")))
                        .Item("employeeunkid") = CInt(drpEmployee.SelectedValue)
                        .Item("employeename") = drpEmployee.SelectedItem.Text
                        If CInt(drpdepedants.SelectedValue) <= 0 Then
                            .Item("dependantsunkid") = -1
                            .Item("dependentsname") = ""
                        Else
                            .Item("dependantsunkid") = CInt(drpdepedants.SelectedValue)
                            .Item("dependentsname") = drpdepedants.SelectedItem.Text
                        End If
                        '.Item("periodunkid") = CInt(drpPeriod.SelectedValue)
                        '.Item("period_name") = drpPeriod.SelectedItem.Text
                        .Item("sicksheetunkid") = IIf(Me.ViewState("SickSheetId") Is Nothing, -1, CInt(Me.ViewState("SickSheetId")))
                        .Item("sicksheetno") = txtSickSheetNo.Text
                        .Item("claimno") = txtClaimno.Text.Trim


                        'Pinkal (12-Nov-2012) -- Start
                        'Enhancement : TRA Changes
                        If dtClaimdate.IsNull = True Then
                            .Item("claimdate") = DBNull.Value
                        Else
                            .Item("claimdate") = dtClaimdate.GetDate.Date
                        End If
                        'Pinkal (12-Nov-2012) -- End


                        .Item("amount") = CDec(txtclaimAmount.Text.Trim)
                        .Item("remarks") = txtRemark.Text.Trim

                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                            .Item("AUD") = "U"
                        End If

                        'Pinkal (18-Dec-2012) -- Start
                        'Enhancement : TRA Changes
                        .Item("isempexempted") = chkEmpexempted.Checked
                        'Pinkal (18-Dec-2012) -- End

                    End With
                    Session("ClaimList") = dttable
                    FillList()
                    ClearControls()
                    drpEmployee.SelectedIndex = 0
                    txtSickSheetNo.Text = ""
                End If
            End If
            Me.ViewState("ClaimTranunkid") = Nothing
            Me.ViewState("GUID") = Nothing
            drpEmployee.Enabled = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnEdit_Click :-  " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try


            'Dim sb As Object = New StringBuilder()
            'sb.AppendFormat("var foo = window.confirm('Are you sure you want to delete?');\n")
            'sb.Append("if (foo)\n")
            'sb.Append("__doPostBack('ConfirmationPostBackEventTarget', foo);\n")
            'ClientScript.RegisterStartupScript(Me.GetType, "DeleteKy", sb.ToString())

            If Me.ViewState("ClaimTranunkid") Is Nothing AndAlso Me.ViewState("GUID") Is Nothing Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please select Medical Claim from the list to perform further operation on it.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please select Medical Claim from the list to perform further operation on it."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub
            End If

            Dim dttable As DataTable = CType(Session("ClaimList"), DataTable)
            Dim drRow As DataRow() = Nothing

            If CInt(Me.ViewState("ClaimTranunkid")) > 0 Then
                drRow = dttable.Select("ClaimTranunkid = " & CInt(Me.ViewState("ClaimTranunkid")))

            ElseIf Me.ViewState("GUID").ToString.Trim().Length > 0 Then
                drRow = dttable.Select("GUID = '" & Me.ViewState("GUID").ToString() & "'")
            End If

            If drRow.Length > 0 Then
                drRow(0)("AUD") = "D"
                dttable.AcceptChanges()
                Session("ClaimList") = dttable
                FillList()
                ClearControls()
                drpEmployee.SelectedIndex = 0
                txtSickSheetNo.Text = ""
            End If
            Me.ViewState("ClaimTranunkid") = Nothing
            Me.ViewState("GUID") = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnDelete_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If CInt(drpPeriod.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Pay Period is compulsory information.Please Select Pay Period.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period."), Me)
                'Pinkal (06-May-2014) -- End
                drpPeriod.Focus()
                Exit Sub

            ElseIf CInt(drpProvider.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Provider is compulsory information.Please Select Provider.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider."), Me)
                'Pinkal (06-May-2014) -- End

                drpProvider.Focus()
                Exit Sub

            ElseIf txtInvoiceNo.Text.Trim = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                '  DisplayMessage.DisplayMessage("Invoice No cannot be blank.Invoice No is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Invoice No cannot be blank.Invoice No is required information."), Me)
                'Pinkal (06-May-2014) -- End
                txtInvoiceNo.Focus()
                Exit Sub

            ElseIf txtTotInvoiceAmt.Text.Trim = "" Or txtTotInvoiceAmt.Text = "0" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Total Invoice Amount cannot be blank.Total Invoice Amount is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Total Invoice Amount cannot be blank.Total Invoice Amount is required information."), Me)
                'Pinkal (06-May-2014) -- End

                txtTotInvoiceAmt.Focus()
                Exit Sub

            ElseIf GvMedicalClaim.Rows.Count = 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please Add atleast one Medical Claim for this employee.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Please Add atleast one Medical Claim."), Me)
                'Pinkal (06-May-2014) -- End
                drpEmployee.Focus()
                Exit Sub

            End If

            If Me.ViewState("isfinal") Is Nothing Then
                Me.ViewState.Add("isfinal", False)
            End If

            ForSave()



            objMedicalclaim_master._Claimunkid = -1
            Session("Claimunkid") = Nothing
            GvMedicalClaim.DataSource = CType(Session("ClaimList"), DataTable).Clone
            GvMedicalClaim.DataBind()
            Session("ClaimList") = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Dim blnFlag As Boolean = False
        Try

            If CInt(drpPeriod.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Pay Period is compulsory information.Please Select Pay Period.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Pay Period is compulsory information.Please Select Pay Period."), Me)
                'Pinkal (06-May-2014) -- End


                drpPeriod.Focus()
                Exit Sub

            ElseIf CInt(drpProvider.SelectedValue) = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Provider is compulsory information.Please Select Provider.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Provider is compulsory information.Please Select Provider."), Me)
                'Pinkal (06-May-2014) -- End


                drpProvider.Focus()
                Exit Sub

            ElseIf txtInvoiceNo.Text.Trim = "" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Invoice No cannot be blank.Invoice No is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Invoice No cannot be blank.Invoice No is required information."), Me)
                'Pinkal (06-May-2014) -- End


                txtInvoiceNo.Focus()
                Exit Sub

            ElseIf txtTotInvoiceAmt.Text.Trim = "" Or txtTotInvoiceAmt.Text = "0.00" Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Total Invoice Amount cannot be blank.Total Invoice Amount is required information.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Total Invoice Amount cannot be blank.Total Invoice Amount is required information."), Me)
                'Pinkal (06-May-2014) -- End
                txtTotInvoiceAmt.Focus()
                Exit Sub

            ElseIf GvMedicalClaim.Rows.Count = 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Please Add atleast one Medical Claim for this employee.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Please Add atleast one Medical Claim."), Me)
                'Pinkal (06-May-2014) -- End
                drpEmployee.Focus()
                Exit Sub

            ElseIf Format(CDec(txtTotInvoiceAmt.Text), Session("fmtCurrency").ToString()) <> Format(CDec(txtTotalAmount.Text), Session("fmtCurrency").ToString()) Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'DisplayMessage.DisplayMessage("Total Invoice Amout must be equal to total amount.", Me)
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Total Invoice Amout must be equal to total amount."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub

            End If

            If Me.ViewState("isfinal") Is Nothing Then
                Me.ViewState.Add("isfinal", True)
            End If

            ForSave()

            objMedicalclaim_master._Claimunkid = -1
            Session("Claimunkid") = Nothing
            GvMedicalClaim.DataSource = CType(Session("ClaimList"), DataTable).Clone
            GvMedicalClaim.DataBind()
            Session("ClaimList") = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnFinalSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        'Shani [ 24 DEC 2014 ] -- END
        Try
            ClearControls()
            drpPeriod.SelectedIndex = 0
            txtInvoiceNo.Text = ""
            txtTotInvoiceAmt.Text = Format(CDec("0"), Session("fmtCurrency").ToString())
            Session("ClaimList") = Nothing
            GvMedicalClaim.DataSource = Nothing
            GvMedicalClaim.DataBind()
            Session("MedicalclaimId") = Nothing
            Session("ClaimList") = Nothing
            Session("Claimunkid") = Nothing

            Response.Redirect(Session("rootpath").ToString() & "Medical/wPg_MedicalClaimList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " DropDown Event(s) "

    Protected Sub drpEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.SelectedIndexChanged
        Dim dsFill As DataSet = Nothing
        Try


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            'Dim objDependant As New clsDependants_Beneficiary_tran
            'dsFill = objDependant.GetQualifiedDepedant(CInt(drpEmployee.SelectedValue), True)
            'drpdepedants.DataValueField = "dependent_Id"
            'drpdepedants.DataTextField = "dependants"
            'drpdepedants.DataSource = dsFill.Tables(0)
            'drpdepedants.DataBind()

            Dim objDependant As New clsDependants_Beneficiary_tran
            If CInt(Me.ViewState("SickSheetId")) > 0 Then
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsFill = objDependant.GetQualifiedDepedant(CInt(drpEmployee.SelectedValue), True, False, CDate(Me.ViewState("SickSheetDate")).Date, True)
                dsFill = objDependant.GetQualifiedDepedant(CInt(drpEmployee.SelectedValue), True, False, CDate(Me.ViewState("SickSheetDate")).Date, True, dtAsOnDate:=CDate(Me.ViewState("SickSheetDate")).Date)
                'Sohail (18 May 2019) -- End
            Else
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'dsFill = objDependant.GetQualifiedDepedant(CInt(drpEmployee.SelectedValue), True, False, dtpInvoiceDate.GetDate.Date, True)
                dsFill = objDependant.GetQualifiedDepedant(CInt(drpEmployee.SelectedValue), True, False, dtpInvoiceDate.GetDate.Date, True, dtAsOnDate:=dtpInvoiceDate.GetDate.Date)
                'Sohail (18 May 2019) -- End
            End If

            drpdepedants.DataValueField = "dependent_Id"
            drpdepedants.DataTextField = "dependants"
            drpdepedants.DataSource = dsFill.Tables(0)
            drpdepedants.DataBind()

            'Pinkal (21-Jul-2014) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpEmployee_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub drpProvider_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpProvider.SelectedIndexChanged
        Try
            txtSickSheetNo.Text = ""

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            Dim strFilter As String = String.Empty
            Dim objEmployee As New clsEmployee_Master
            Dim dsFill As DataSet = Nothing


            'If CInt(drpProvider.SelectedValue) > 0 Then

            '    Dim objProvider As New clsinstitute_master
            '    objProvider._Instituteunkid = CInt(drpProvider.SelectedValue)

            '    If objProvider._IsFormRequire Then
            '        txtSickSheetNo.Enabled = True

            '        Dim objEmployee As New clsEmployee_Master
            '        Dim dsFill As DataSet = Nothing

            '        If Session("MedicalclaimId") IsNot Nothing Then
            '            dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            '        Else
            '            dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , " employeeunkid = 0")
            '        End If

            '        drpEmployee.DataTextField = "employeename"
            '        drpEmployee.DataValueField = "employeeunkid"
            '        drpEmployee.DataSource = dsFill.Tables("Employee")
            '        drpEmployee.DataBind()

            '        drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            '    Else
            '        txtSickSheetNo.Enabled = False

            '        'Pinkal (21-Jul-2014) -- Start
            '        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            '        Dim objEmployee As New clsEmployee_Master
            '        Dim dsFill As DataSet = Nothing
            '        If Session("MedicalclaimId") IsNot Nothing Then
            '            dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            '        Else
            '            dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
            '        End If

            '        drpEmployee.DataTextField = "employeename"
            '        drpEmployee.DataValueField = "employeeunkid"
            '        drpEmployee.DataSource = dsFill.Tables("Employee")
            '        drpEmployee.DataBind()

            '        drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            '        'Pinkal (21-Jul-2014) -- End

            '    End If

            'ElseIf CInt(drpProvider.SelectedValue) <= 0 Then

            '    'Pinkal (21-Jul-2014) -- Start
            '    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            '    Dim objEmployee As New clsEmployee_Master
            '    Dim dsFill As DataSet = Nothing
            '    If Session("MedicalclaimId") IsNot Nothing Then
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            '    Else
            '        dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , " employeeunkid = 0")
            '    End If

            '    drpEmployee.DataTextField = "employeename"
            '    drpEmployee.DataValueField = "employeeunkid"
            '    drpEmployee.DataSource = dsFill.Tables("Employee")
            '    drpEmployee.DataBind()

            '    drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())

            '    'Pinkal (21-Jul-2014) -- End
            'End If
            If CInt(drpProvider.SelectedValue) > 0 Then
                Dim objProvider As New clsinstitute_master
                objProvider._Instituteunkid = CInt(drpProvider.SelectedValue)

                If objProvider._IsFormRequire Then
                    txtSickSheetNo.Enabled = True
                    If Session("MedicalclaimId") Is Nothing Then
                        strFilter = "hremployee_master.employeeunkid = 0"
                    End If
                Else
                    txtSickSheetNo.Enabled = False
                End If
            ElseIf CInt(drpProvider.SelectedValue) <= 0 Then
                If Session("MedicalclaimId") Is Nothing Then
                    strFilter = "hremployee_master.employeeunkid = 0"
                End If
            End If

            dsFill = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                 True, "Employee", True, , , , , , , , , , , , , , , strFilter)

            With drpEmployee
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataValueField = "employeeunkid"
                .DataSource = dsFill.Tables("Employee")
                .DataBind()
            End With

            drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpProvider_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Event(s)"

    Protected Sub txtSickSheetNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSickSheetNo.TextChanged
        Try

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            If txtSickSheetNo.Text.Trim.Length > 0 Then
                Dim objsickSheet As New clsmedical_sicksheet

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsList As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim)
                Dim dsList As DataSet = objsickSheet.GetList(Session("Database_Name").ToString(), _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                             Session("UserAccessModeSetting").ToString(), True, _
                                                             True, "List", True, txtSickSheetNo.Text.Trim, -1, "")
                'Shani(20-Nov-2015) -- End

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Me.ViewState.Add("SickSheetId", dsList.Tables(0).Rows(0)("sicksheetunkid").ToString())

                    Dim mintEmployeeId As Integer = -1
                    Dim mintServiceProviderId As Integer = -1

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Dim dsSicksheet As DataSet = objsickSheet.GetList("List", True, txtSickSheetNo.Text.Trim)


                    'Pinkal (06-Jan-2016) -- Start
                    'Enhancement - Working on Changes in SS for Leave Module.

                    'Dim dsSicksheet As DataSet = objsickSheet.GetList(Session("Database_Name").ToString(), _
                    '                                                  CInt(Session("UserId")), _
                    '                                                  CInt(Session("Fin_year")), _
                    '                                                  CInt(Session("CompanyUnkId")), _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                    '                                                  Session("UserAccessModeSetting")., True, _
                    '                                                  Session("IsIncludeInactiveEmp"), "List", True, txtSickSheetNo.Text.Trim)

                    'Me.ViewState("SickSheetDate") = Nothing
                    'If dsSicksheet IsNot Nothing AndAlso dsSicksheet.Tables(0).Rows.Count > 0 Then
                    '    mintEmployeeId = CInt(dsSicksheet.Tables(0).Rows(0)("employeeunkid"))
                    '    mintServiceProviderId = CInt(dsSicksheet.Tables(0).Rows(0)("instituteunkid"))
                    '    Me.ViewState.Add("SickSheetDate", eZeeDate.convertDate(dsSicksheet.Tables(0).Rows(0)("sicksheetdate").ToString()).Date)
                    'End If

                    Me.ViewState("SickSheetDate") = Nothing
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mintEmployeeId = CInt(dsList.Tables(0).Rows(0)("employeeunkid"))
                        mintServiceProviderId = CInt(dsList.Tables(0).Rows(0)("instituteunkid"))
                        Me.ViewState.Add("SickSheetDate", eZeeDate.convertDate(dsList.Tables(0).Rows(0)("sicksheetdate").ToString()).Date)
                    End If

                    'Pinkal (06-Jan-2016) -- End

                    'Shani(20-Nov-2015) -- End


                    If mintServiceProviderId = CInt(drpProvider.SelectedValue) Then
                        Dim objEmployee As New clsEmployee_Master
                        Dim dsFill As DataSet = Nothing

                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If Session("MedicalclaimId") IsNot Nothing Then
                        '    dsFill = objEmployee.GetEmployeeList("Employee", True,  True, mintEmployeeId, , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , , Session("AccessLevelFilterString"))
                        'Else
                        '    dsFill = objEmployee.GetEmployeeList("Employee", True, , mintEmployeeId, , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                        'End If

                        dsFill = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                            Session("UserAccessModeSetting").ToString(), True, _
                                                            True, "Employee", True, mintEmployeeId)

                        'Shani(24-Aug-2015) -- End

                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        'drpEmployee.DataTextField = "employeename"
                        drpEmployee.DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        drpEmployee.DataValueField = "employeeunkid"
                        drpEmployee.DataSource = dsFill.Tables("Employee")
                        drpEmployee.DataBind()
                        drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())
                    Else
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "This Sick Sheet No is not valid for this provider."), Me)
                        Exit Sub
                    End If

                Else
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "This Sick Sheet No is not valid for this provider."), Me)
                    Exit Sub
                End If
            End If

            'Pinkal (21-Jul-2014) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSickSheetNo_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtclaimAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtclaimAmount.TextChanged, txtTotInvoiceAmt.TextChanged
        Try
            If CType(sender, System.Web.UI.WebControls.TextBox).Text.Trim.Length > 0 Then
                CType(sender, System.Web.UI.WebControls.TextBox).Text = Format(CDec(CType(sender, System.Web.UI.WebControls.TextBox).Text), Session("fmtCurrency").ToString())
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtclaimAmount_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub GvMedicalClaim_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvMedicalClaim.RowDataBound
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Row.RowIndex >= 0 Then

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If Session("DateFormat").ToString() <> Nothing Then
                '    e.Row.Cells(4).Text = CDate(e.Row.Cells(4).Text).Date.ToString(Session("DateFormat").ToString())
                'Else
                '    e.Row.Cells(4).Text = CDate(e.Row.Cells(4).Text).Date.ToShortDateString()
                'End If
                e.Row.Cells(4).Text = CDate(e.Row.Cells(4).Text).Date.ToShortDateString()
                'Pinkal (16-Apr-2016) -- End


                e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency").ToString())
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvMedicalClaim_RowDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (06-Jan-2016) -- Start
    'Enhancement - Working on Changes in SS for Leave Module.

    'Protected Sub GvMedicalClaim_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GvMedicalClaim.PageIndexChanged
    '    Try
    '        GvMedicalClaim.PageIndex = e.NewPageIndex
    '        FillList()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("GvMedicalClaim_PageIndexChanged :- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Pinkal (06-Jan-2016) -- End

    Protected Sub GvMedicalClaim_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GvMedicalClaim.SelectedIndexChanged
        Try

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If Session("ClaimList") Is Nothing Then Exit Sub
            'Pinkal (06-Jan-2016) -- End

            Dim dtTable As DataTable = New DataView(CType(Session("ClaimList"), DataTable), "AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable

            If Me.ViewState("ClaimTranunkid") Is Nothing Then
                Me.ViewState.Add("ClaimTranunkid", CInt(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("claimtranunkid")))
            Else
                Me.ViewState("ClaimTranunkid") = CInt(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("claimtranunkid"))
                drpEmployee.Enabled = False
            End If
            txtRemark.Text = dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("remarks").ToString()
            txtSickSheetNo.Text = dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("sicksheetno").ToString()

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            txtSickSheetNo_TextChanged(New Object(), New EventArgs())

            Me.ViewState("SickSheetId") = CInt(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("sicksheetunkid"))
            dtClaimdate.SetDate = CDate(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("claimdate")).Date
            txtClaimno.Text = dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("claimno").ToString()

            drpEmployee.SelectedValue = CInt(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("employeeunkid")).ToString()
            drpEmployee_SelectedIndexChanged(sender, e)

            If CInt(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("dependantsunkid")) > 0 Then
                drpdepedants.SelectedValue = CInt(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("dependantsunkid")).ToString()
            Else
                drpdepedants.SelectedValue = "0"
            End If

            'Pinkal (21-Jul-2014) -- End

            txtclaimAmount.Text = Format(CDec(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("amount")), Session("fmtCurrency").ToString())
            chkEmpexempted.Checked = CBool(dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("isempexempted").ToString())

            If Me.ViewState("GUID") Is Nothing Then
                Me.ViewState.Add("GUID", dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("GUID").ToString())
            Else
                Me.ViewState("GUID") = dtTable.Rows(GvMedicalClaim.SelectedRow.RowIndex)("GUID").ToString()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvMedicalClaim_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ToolBar Event(s)"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Medical/wPg_MedicalClaimList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        ClearControls()
    '        txtInvoiceNo.Text = ""

    '        'Pinkal (25-APR-2012) -- Start
    '        'Enhancement : TRA Changes
    '        'txtTotInvoiceAmt.Text = Format(CDec("0"), GUI.fmtCurrency)
    '        txtTotInvoiceAmt.Text = Format(CDec("0"), Session("fmtCurrency"))
    '        'Pinkal (25-APR-2012) -- End


    '        Session("ClaimList") = Nothing
    '        Session("ClaimList") = Nothing
    '        GvMedicalClaim.DataSource = Nothing
    '        GvMedicalClaim.DataBind()
    '        Session("MedicalclaimId") = Nothing
    '        Session("ClaimList") = Nothing
    '        Session("Claimunkid") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

#Region "Datepicker Event"

    Private Sub dtpInvoiceDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpInvoiceDate.TextChanged
        Try
            drpEmployee_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpInvoiceDate_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblDependants.Text = Language._Object.getCaption(Me.lblDependants.ID, Me.lblDependants.Text)
        Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
        Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
        Me.lblInstutution.Text = Language._Object.getCaption(Me.lblInstutution.ID, Me.lblInstutution.Text)
        Me.lblSickSheetNo.Text = Language._Object.getCaption(Me.lblSickSheetNo.ID, Me.lblSickSheetNo.Text)
        Me.lblClaimdate.Text = Language._Object.getCaption(Me.lblClaimdate.ID, Me.lblClaimdate.Text)
        Me.LblInvoiceDate.Text = Language._Object.getCaption(Me.LblInvoiceDate.ID, Me.LblInvoiceDate.Text)
        Me.lblInvoiceno.Text = Language._Object.getCaption(Me.lblInvoiceno.ID, Me.lblInvoiceno.Text)
        Me.lblClaimno.Text = Language._Object.getCaption(Me.lblClaimno.ID, Me.lblClaimno.Text)
        Me.lblClaimAmount.Text = Language._Object.getCaption(Me.lblClaimAmount.ID, Me.lblClaimAmount.Text)
        Me.chkEmpexempted.Text = Language._Object.getCaption(Me.chkEmpexempted.ID, Me.chkEmpexempted.Text)
        Me.lblTotInvoiceAmt.Text = Language._Object.getCaption(Me.lblTotInvoiceAmt.ID, Me.lblTotInvoiceAmt.Text)
        Me.lblTotAmt.Text = Language._Object.getCaption(Me.lblTotAmt.ID, Me.lblTotAmt.Text)

        Me.GvMedicalClaim.Columns(0).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(0).FooterText, Me.GvMedicalClaim.Columns(0).HeaderText)
        Me.GvMedicalClaim.Columns(1).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(1).FooterText, Me.GvMedicalClaim.Columns(1).HeaderText)
        Me.GvMedicalClaim.Columns(2).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(2).FooterText, Me.GvMedicalClaim.Columns(2).HeaderText)
        Me.GvMedicalClaim.Columns(3).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(3).FooterText, Me.GvMedicalClaim.Columns(3).HeaderText)
        Me.GvMedicalClaim.Columns(4).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(4).FooterText, Me.GvMedicalClaim.Columns(4).HeaderText)
        Me.GvMedicalClaim.Columns(5).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(5).FooterText, Me.GvMedicalClaim.Columns(5).HeaderText)
        Me.GvMedicalClaim.Columns(6).HeaderText = Language._Object.getCaption(Me.GvMedicalClaim.Columns(6).FooterText, Me.GvMedicalClaim.Columns(6).HeaderText)

        Me.btnAdd.Text = Language._Object.getCaption(Me.btnAdd.ID, Me.btnAdd.Text).Replace("&", "")
        Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.ID, Me.btnEdit.Text).Replace("&", "")
        Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.ID, Me.btnDelete.Text).Replace("&", "")
        Me.btnImportData.Text = Language._Object.getCaption(Me.btnImportData.ID, Me.btnImportData.Text).Replace("&", "")
        Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
        Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.ID, Me.btnFinalSave.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Me.btnImportOk.Text = Language._Object.getCaption("btnImportData", Me.btnImportOk.Text).Replace("&", "")
        Me.btnImportCancel.Text = Language._Object.getCaption("btnClose", Me.btnImportCancel.Text).Replace("&", "")

        Language.setLanguage(mstrModuleName1)

        Me.LblText.Text = Language._Object.getCaption(mstrModuleName1, Me.LblText.Text)

        Me.lblImportEmployeeCode.Text = Language._Object.getCaption("lblEmployeecode", Me.lblImportEmployeeCode.Text)
        Me.lblImportEmployee.Text = Language._Object.getCaption("lblEmployee", Me.lblImportEmployee.Text)
        Me.lblImportDepedant.Text = Language._Object.getCaption("lblDependants", Me.lblImportDepedant.Text)
        Me.lblImportSicksheet.Text = Language._Object.getCaption("lblSicksheetNo", Me.lblImportSicksheet.Text)
        Me.lblImportClaimNo.Text = Language._Object.getCaption("lblClaimNo", Me.lblImportClaimNo.Text)
        Me.lblImportClaimDate.Text = Language._Object.getCaption("lblClaimDate", Me.lblImportClaimDate.Text)
        Me.lblImportClaimAmount.Text = Language._Object.getCaption("lblClaimAmount", Me.lblImportClaimAmount.Text)
        Me.LblEmpExempted.Text = Language._Object.getCaption(Me.LblEmpExempted.ID, Me.LblEmpExempted.Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


End Class
