﻿<%@ Page Title="Confidentiality Agreement and Non-Disclosure Declaration" Language="VB"
    MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_Empnondisclosure_declaration.aspx.vb"
    Inherits="Employee_NonDisclosure_Declaration_wPg_Empnondisclosure_declaration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    
    <script type="text/javascript">
        function displaysign() {
            var img = document.getElementById('<%=pnlSign.ClientID %>');
            img.style.display = "block";
        }    

        function GetInstruct(str) {
            var dv = document.getElementById("divInstruction");
            if (dv != null && str != null)
                dv.innerHTML = str.toString();
        }
        
        function FileUploadChangeEvent() {
        var cnt = $('.flupload').length;
                for (i = 0; i < cnt; i++) {
                    var fupld = $('.flupload')[i].id;
                    if (fupld != null)
                    fileUpLoadChange($('.flupload')[i].id);
                }
        }
    </script>
    
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Confidentiality Agreement and Non-Disclosure Declaration"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetailHeader" runat="server" Text="Confidentiality Agreement and Non-Disclosure Declaration"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div id="divInstruction" style="width: 100%; overflow: auto;">
                                        <div style="width: 100%; text-align: center; margin-bottom: 15px;">
                                            <asp:Label ID="lblDeclarationTitle" runat="server" Font-Bold="true" Font-Underline="true"
                                                Font-Size="Medium" Text="Confidentiality Agreement and Non-Disclosure Declaration"></asp:Label>
                                        </div>                                        
                                        <asp:Label ID="lblNonDisclosureDeclaration" runat="server"></asp:Label>
                                        <div class="row2" style="margin-top: 10px">
                                            <div class="ib" style="width: 60%; vertical-align: top">
                                                <div class="row2">
                                                    <div class="ib" style="width: 100%">
                                                        <asp:Label ID="lblDeclaredBy" runat="server" Text="Declared by:" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 20%">
                                                        <asp:Label ID="lblEmpDeclareDate" runat="server" Text="Date:"></asp:Label>
                                                    </div>
                                                    <div class="ib" style="width: 20%">
                                                        <asp:Label ID="objlblEmpDeclareDate" runat="server" Text="&nbsp;"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 20%">
                                                        <asp:Label ID="lblEmpName" runat="server" Text="Name:"></asp:Label>
                                                    </div>
                                                    <div class="ib" style="width: 30%">
                                                        <asp:Label ID="objlblEmpName" runat="server" Text="&nbsp;"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ib" style="width: 35%">
                                                <asp:Panel ID="pnlSign" runat="server" ToolTip="If this is not your
signature, upload new signature">
                                                    <div class="row2">
                                                        <div class="ib" style="width: 100%">
                                                            <asp:Label ID="lblempsign" runat="server" Text="Employee Signature:" Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="row2">
                                                        <div class="ib" style="width: 100%">
                                                            <asp:Image ID="imgSignature" runat="server" Width="150px" Height="75px" ToolTip="If this is not your
signature, upload new signature" />
                                                            <asp:Label ID="lblnosign" runat="server" Text="Signature Not Available" ForeColor="Red"
                                                                Font-Bold="true"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="row2">
                                                        <div class="ib" style="width: 100%">
                                                            <asp:UpdatePanel ID="UPUploadSig" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <uc9:FileUpload ID="flUploadsig" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                            </div>                         
                                            </div>
                                                    <div class="row2">
                                                        <div class="ib" style="width: 100%">
                                                            <asp:CheckBox ID="chkconfirmSign" ToolTip="Confirm Signature" runat="server" Text="I confirm Signatures" />
                                            </div>
                                            </div>
                                                </asp:Panel>
                                            </div>                                           
                                        </div>
                                        <%--<div style="float:left;width:100%;;margin:20px 0px 10px 0px;">
                                            <asp:Label ID="lblWitnessBy" runat="server" Text="Witnessed by on behalf of NMB Bank Plc:" Font-Bold="true" ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="lblWitness1Date" runat="server" Text="Date:" ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="objlblWitness1Date" runat="server" Text="&nbsp;" ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="lblWitness2Date" runat="server" Text="Date:" ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="objlblWitness2Date" runat="server" Text="&nbsp;" ></asp:Label>
                                        </div>
                                        
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="lblWitness1Name" runat="server" Text="Name:"  ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="objlblWitness1Name" runat="server" Text="&nbsp;" ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="lblWitness2Name" runat="server" Text="Name:" ></asp:Label>
                                        </div>
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="objlblWitness2Name" runat="server" Text="&nbsp;" ></asp:Label>
                                        </div>
                                                   
                                        <div style="float:left;width:25%;margin:5px 0px;">&nbsp;</div> 
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="objlblWitness1Job" runat="server" Text="&nbsp"  ></asp:Label>
                                        </div> 
                                        <div style="float:left;width:25%;margin:5px 0px;">&nbsp;</div> 
                                        <div style="float:left;width:25%;margin:5px 0px;">
                                            <asp:Label ID="objlblWitness2Job" runat="server" Text="&nbsp"  ></asp:Label>
                                        </div>                            
                                        <div style="float:left;width:100%;margin:5px 0px;">--%>
                                            <%--<asp:CheckBox ID="chkAcknowledgement" runat="server" Text="I Acknowledge" />--%>
                                        <%--</div>--%>                                         
                                    </div>
                                    <div class="btn-default" style="text-align: center;">
                                        <asp:Button ID="btnConfirmSign" runat="server" Text="Confirm Signature" CssClass="btnDefault" />
                                        <asp:Button ID="btnAcknowledge" runat="server" Text="I Acknowledge" CssClass="btnDefault" />                                        
                                        <asp:Button ID="btnPreview" runat="server" Text="Preview" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    <uc8:ConfirmYesNo ID="popupYesNo" runat="server" Title="Are you sure you want to continue to make an acknowledgement?"
                        Message="You are about to make an acknowledgement to the confidentiality clause. Are you sure you want to continue?" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
