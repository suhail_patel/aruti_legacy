﻿<%@ Page Title="Unlock Employee for Non-Declaration Disclosure" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_Empunlocknondisclosuredec.aspx.vb" Inherits="Employee_NonDisclosure_Declaration_wPg_Empunlocknondisclosuredec" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
        
    </script>
    
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 80%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Unlock Employee for Non-Declaration Disclosure"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblPageHeader2" runat="server" Text="Unlock Employee for Non-Declaration Disclosure"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div2" class="panel-body-default">
                                    <asp:Panel ID="pnl_dgView" runat="server" Height="300px" ScrollBars="Auto">
                                        <asp:GridView ID="GvLockEmployeeList" runat="server" AutoGenerateColumns="False"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="ndlockunkid, employeeunkid, lockuserunkid, unlockuserunkid, islock, nextlockdatetime, loginemployeeunkid" >
                                            <Columns>
                                             <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" onclick="selectall(this);" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" />
                                                        </ItemTemplate>
                                                  </asp:TemplateField>
                                                <asp:BoundField DataField="employeecode" HeaderText="Emp. Code" HeaderStyle-Width="25%" FooterText = "colhEmployeecode"></asp:BoundField>
                                                <asp:BoundField DataField="employeename" HeaderText="Employee Name" HeaderStyle-Width="50%" FooterText = "colhEmployeename"></asp:BoundField>
                                                <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock DateTime" HeaderStyle-Width="25%" FooterText ="colhLockDateTime"> </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Label ID="lblUnlockDays" runat="server" Text="Unlock employees for days " style="float:left;" ></asp:Label>
                                        <asp:TextBox ID="txtUnlockDays" runat="server" CssClass="RightTextAlign" style="width:90px;float:left;margin-left:15px;" onKeypress="return onlyNumbers(this, event);" Text="0" ></asp:TextBox>                                       
                                        <asp:Button ID="btnUnlock" runat="server" CssClass="btndefault" Text="Unlock" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  <uc2:Confirmation ID="popup_YesNo" Title="Confirmation" runat="server" Message=""/>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
    <script type="text/javascript">

        function selectall(chk) {
            $("[id$='chkSelect']").attr('checked', chk.checked);
        }
       
           
    </script>
    
</asp:Content>

