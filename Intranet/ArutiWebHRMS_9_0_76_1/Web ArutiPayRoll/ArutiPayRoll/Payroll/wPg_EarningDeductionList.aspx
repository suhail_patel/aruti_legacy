﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_EarningDeductionList.aspx.vb" Inherits="Payroll_wPg_EarningDeductionList"
    Title="Employee Earning and Deduction List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var sctop;
            var prm;
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
                sctop = $("#scrollable-container").scrollTop();
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                 if (args.get_error() == undefined) {
                     //$("#scrollable-container").scrollTop($(scroll.Y).val());
                     $("#scrollable-container").scrollTop(sctop);
                }
            }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Earning and Deduction List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover"
                                            Style="vertical-align: top"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                               <asp:Label ID="lblCalcType" runat="server" Text="Calc. Type" />
                                            </td>
                                            <td style="width: 20%">
                                               <asp:DropDownList ID="cboCalcType" runat="server" AutoPostBack="true" >
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 13%">
                                                 <asp:Label ID="lblTranHead" runat="server" Text="Transaction Head" />
                                            </td>
                                            <td style="width: 20%">
                                                 <asp:DropDownList ID="cboTranHead" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 13%">
                                                <asp:Label ID="lblperiod" runat="server" Text="As On Period" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td colspan="2" style="width: 13%">
                                                <asp:CheckBox ID="chkIncludeInactiveEmployee" runat="server" Text="Include Inactive Employees" />
                                            </td>
                                            <%--<td  style="width: 20%">
                                            </td>--%>
                                            <td  style="width: 13%">
                                                <asp:Label ID="lblApprovalStatus" runat="server" Text="Approval Status" />
                                            </td>
                                            <td  style="width: 20%">
                                                <asp:DropDownList ID="cboEDApprovalStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Label ID="lblTotalAmt" runat="server" Text="Total Flat Rate Amount" style="display:inline-block;width:180px;text-align:left;" ></asp:Label>
                                        <asp:Label ID="objlblTotalAmt" runat="server" Text="0.00" style="display:inline-block;width:120px;text-align:right;" ></asp:Label>
                                        <asp:HiddenField ID="hfobjlblTotalAmt" runat="server" Value="0.00" />
                                        <asp:Button ID="btnNew" runat="server" Text="New" Visible="false" CssClass="btnDefault" />
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btnDefault" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnDisApprove" runat="server" Text="Reject" CssClass="btnDefault" UseSubmitBehavior="false" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container"  onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 99%; overflow: auto; height: 275px; margin-left: 6px">
                                <asp:GridView ID="dgED" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                    ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" DataKeyNames="edunkid, typeof_id, IsGrp, employeeunkid, tranheadunkid, employeename, trnheadname, amount, periodunkid, membershiptranunkid, userunkid, IsApproved" >
                                    <Columns>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="false" onclick="selectall(this);"  />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="false" onclick="selectallgroupchild(this);" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Change"
                                                        ToolTip="Edit" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <span class="gridiconbc">
                                                    <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Remove"
                                                        ToolTip="Delete" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                                </span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="trnheadcode" HeaderText="Code" ReadOnly="True" FooterText="dgcolhTrnHeadCode" />
                                        <asp:BoundField DataField="trnheadname" HeaderText="Transaction Head" ReadOnly="True"
                                            ItemStyle-Width="150px" FooterText="dgcolhTrnHead" />
                                        <asp:BoundField DataField="trnheadtype_name" HeaderText="Head Type" ReadOnly="True"
                                            ItemStyle-Width="150px" FooterText="dgcolhTranHeadType" />
                                        <asp:BoundField DataField="typeof_name" HeaderText="Type Of" ReadOnly="True" ItemStyle-Width="150px"
                                            FooterText="dgcolhTypeOf" />
                                        <asp:BoundField DataField="calctype_name" HeaderText="Calculation Type" ReadOnly="true"
                                            ItemStyle-Width="150px" FooterText="dgcolhCalcType" />
                                        <asp:BoundField DataField="period_name" HeaderText="Period" ReadOnly="True" ItemStyle-Width="100px"
                                            FooterText="colhPeriod" />
                                        <asp:BoundField DataField="amount" HeaderText="Amount" ReadOnly="True" ItemStyle-HorizontalAlign="Right"
                                            HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" ItemStyle-CssClass="colhAmount" />
                                        <asp:BoundField DataField="isapproved" HeaderText="Status" ReadOnly="True" ItemStyle-HorizontalAlign="Center"
                                            FooterText="colhApprovePending" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <uc9:ConfirmYesNo ID="popupApprove" runat="server" Title="Approve ED Heads" />
                    <uc9:ConfirmYesNo ID="popupDisapprove" runat="server" Title="Disapprove ED Heads" />
                    <uc10:DeleteReason ID="popupDelete" runat="server" Title="Are you sure you want to delete this Earning Deduction?" ValidationGroup="popupDelete" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc10:DeleteReason ID="popApproveReason" runat="server" Title="Comments" ValidationGroup="popApproveReason" />
                    <uc10:DeleteReason ID="popRejectReason" runat="server" Title="Comments" ValidationGroup="popRejectReason" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
    <script type="text/javascript">

        var totalamt = 0;        
            function selectall(chk) {
                $("[id*='chkSelect']").attr('checked', chk.checked);
                settotal();
            }

            function selectallgroupchild(chk) {
                if ($(chk).closest('tr').next('tr').children('td').length > $(chk).closest('tr').children('td').length) {
                    selectnextrow($(chk).closest('tr').next('tr'), chk);
                }
                settotal();
            }

            function selectnextrow(obj, chk) {
                $(obj).find("[id*='chkSelect']").attr('checked', chk.checked);
                if ($(obj).next('tr').children('td').length == $(obj).children('td').length) {
                    selectnextrow($(obj.next('tr')), chk);              
                }
            }
           
            function settotal() {
                totalamt = 0;
                $$('dgED').find("[id*='chkSelect']:checked").closest('tr').each(function() {
                    totalamt += parseFloat($(this).children('td.colhAmount').text().replace(',', '').replace('', '0'));
                })
                if (totalamt.toString().indexOf('.') >= 0)
                    $$('objlblTotalAmt').text(totalamt);
                else
                    $$('objlblTotalAmt').text(totalamt.toString().concat('.00'));

                $$('hfobjlblTotalAmt').val($$('objlblTotalAmt').text());
            }
           
    </script>
</asp:Content>
