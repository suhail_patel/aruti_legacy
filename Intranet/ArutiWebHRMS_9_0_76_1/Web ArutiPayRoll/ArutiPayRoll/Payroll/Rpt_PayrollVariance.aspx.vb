﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Payroll_Rpt_PayrollVariance
    Inherits Basepage


#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objReconciliation As clsPayrollVariance
    Private mstrFromDatabaseName As String
    Private mstrToDatabaseName As String
    Private mintFirstOpenPeriod As Integer = 0 'Sohail (20 Oct 2014)

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPayrollVariance"
    'Anjan [04 June 2014] -- End
    'Sohail (23 Nov 2016) -- Start
    'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mintFromYearUnkId As Integer
    Private mintToYearUnkId As Integer
    'Sohail (23 Nov 2016) -- End
#End Region

#Region " Page's Event "

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintFirstOpenPeriod", mintFirstOpenPeriod)
            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Me.ViewState.Add("mstrFromDatabaseName", mstrFromDatabaseName)
            Me.ViewState.Add("mstrToDatabaseName", mstrToDatabaseName)
            Me.ViewState.Add("mintFromYearUnkId", mintFromYearUnkId)
            Me.ViewState.Add("mintToYearUnkId", mintToYearUnkId)
            'Sohail (23 Nov 2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Nilay (10-Feb-2016) -- Start
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            objReconciliation = New clsPayrollVariance(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            If Not IsPostBack Then
                FillCombo()
                ResetValue()
            Else
                mintFirstOpenPeriod = CInt(ViewState("mintFirstOpenPeriod"))
                'Sohail (23 Nov 2016) -- Start
                'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                mstrFromDatabaseName = ViewState("mstrFromDatabaseName").ToString
                mstrToDatabaseName = ViewState("mstrToDatabaseName").ToString
                mintFromYearUnkId = CInt(ViewState("mintFromYearUnkId"))
                mintToYearUnkId = CInt(ViewState("mintToYearUnkId"))
                'Sohail (23 Nov 2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Try
            Dim objEmp As New clsEmployee_Master
            Dim objperiod As New clscommom_period_Tran
            Dim objBranch As New clsStation
            Dim dsList As New DataSet
            Dim objMaster As New clsMasterData
            Dim dtTable As DataTable

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
            '                                Session("UserId"), _
            '                                Session("Fin_year"), _
            '                                Session("CompanyUnkId"), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                Session("UserAccessModeSetting"), True, _
            '                                Session("IsIncludeInactiveEmp"), "Emp", True)

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(24-Aug-2015) -- End

            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With
            objEmp = Nothing

            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_Year")), 1) 'Sohail (27 Oct 2014)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, "Period", True, , , , Session("Database_Name"))

            'Nilay (10-Feb-2016) -- Start
            'dsList = objperiod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "Period", True)
            dsList = objperiod.getListForCombo(enModuleReference.Payroll, 0, CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                '.SelectedValue = 0
                .SelectedValue = CStr(mintFirstOpenPeriod)
            End With

            With cboPeriodTo
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
                '.SelectedValue = 0
                .SelectedValue = CStr(mintFirstOpenPeriod)
            End With
            objperiod = Nothing

            dsList = objMaster.getComboListForHeadType("HeadType")
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            'dtTable = New DataView(dsList.Tables("HeadType"), " ID IN (0, " & enTranHeadType.EarningForEmployees & "," & enTranHeadType.DeductionForEmployee & "," & enTranHeadType.EmployeesStatutoryDeductions & ") ", "", DataViewRowState.CurrentRows).ToTable
            dtTable = New DataView(dsList.Tables("HeadType"), "", "", DataViewRowState.CurrentRows).ToTable
            'Sohail (23 Feb 2017) -- End
            With cboTrnHeadType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "0"
            End With

            cboTrnHeadType_SelectedIndexChanged(New Object(), New EventArgs())

            'Shani [ 18 NOV 2015 ] -- START
            'Issue : Chrome is not supporting ShowModalDialog option now."
            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            Call cboPeriodTo_SelectedIndexChanged(cboPeriodTo, Nothing)
            'Shani [ 18 NOV 2014 ] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployee.SelectedValue = "0"
            cboTrnHeadType.SelectedValue = "0"
            cboTranHead.SelectedValue = "0"
            'cboPeriod.SelectedValue = 0
            'cboPeriodTo.SelectedValue = 0
            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            chkShowEmployerContribution.Checked = False
            chkShowInformationalHeads.Checked = False
            'Sohail (23 Feb 2017) -- End
            cboPeriod.SelectedValue = CStr(mintFirstOpenPeriod)
            cboPeriodTo.SelectedValue = CStr(mintFirstOpenPeriod)
            If cboPeriod.SelectedIndex > 0 Then cboPeriod.SelectedIndex = cboPeriodTo.SelectedIndex - 1
            chkIgnoreZeroVariance.Checked = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Protected Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dsList = objTranHead.getComboList("TranHead", True, CInt(cboTrnHeadType.SelectedValue))
            dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", True, CInt(cboTrnHeadType.SelectedValue))
            'Sohail (03 Feb 2016) -- End
            With cboTranHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("TranHead")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            chkShowEmployerContribution.Enabled = True
            chkShowInformationalHeads.Enabled = True
            If CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EarningForEmployees OrElse CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.DeductionForEmployee OrElse CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EmployeesStatutoryDeductions Then
                chkShowEmployerContribution.Checked = False
                chkShowEmployerContribution.Enabled = False

                chkShowInformationalHeads.Checked = False
                chkShowInformationalHeads.Enabled = False

            ElseIf CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.EmployersStatutoryContributions Then
                chkShowInformationalHeads.Checked = False
                chkShowInformationalHeads.Enabled = False

                chkShowEmployerContribution.Checked = True
                chkShowEmployerContribution.Enabled = False
            ElseIf CInt(cboTrnHeadType.SelectedValue) = enTranHeadType.Informational Then
                chkShowEmployerContribution.Checked = False
                chkShowEmployerContribution.Enabled = False

                chkShowInformationalHeads.Checked = True
                chkShowInformationalHeads.Enabled = False
            End If
            'Sohail (23 Feb 2017) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If

            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If Me.ViewState("FromDatabase") Is Nothing Then
            '    Me.ViewState.Add("FromDatabase", mstrFromDatabaseName)
            'Else
            '    Me.ViewState("FromDatabase") = mstrFromDatabaseName
            'End If
            mintFromYearUnkId = objPeriod._Yearunkid
            'Sohail (23 Nov 2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPeriodTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriodTo.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriodTo.SelectedValue)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If

            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If Me.ViewState("ToDatabase") Is Nothing Then
            '    Me.ViewState.Add("ToDatabase", mstrToDatabaseName)
            'Else
            '    Me.ViewState("ToDatabase") = mstrToDatabaseName
            'End If
            mintToYearUnkId = objPeriod._Yearunkid
            'Sohail (23 Nov 2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Sub
            End If

            If CInt(cboPeriodTo.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Period is compulsory infomation. Please select period to continue."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriodTo.Focus()
                Exit Sub
            End If

            If cboPeriodTo.SelectedIndex <= cboPeriod.SelectedIndex Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, " To Period cannot be less than or equal to Form Period"), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriodTo.Focus()
                Exit Sub
            End If

            objReconciliation.SetDefaultValue()

            objReconciliation._EmpId = CInt(cboEmployee.SelectedValue)
            objReconciliation._EmpName = cboEmployee.SelectedItem.Text

            objReconciliation._FromPeriodId = cboPeriod.SelectedValue
            objReconciliation._FromPeriodName = cboPeriod.SelectedItem.Text

            objReconciliation._NextPeriodId = CInt(cboPeriodTo.SelectedValue)
            objReconciliation._NextPeriodName = cboPeriodTo.SelectedItem.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objReconciliation._PeriodStartDate = objPeriod._Start_Date
            'Nilay (10-Feb-2016) -- Start
            Dim dtStartDate As Date = objPeriod._Start_Date
            'Nilay (10-Feb-2016) -- End

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriodTo.SelectedValue)
            objReconciliation._PeriodEndDate = objPeriod._End_Date
            'Nilay (10-Feb-2016) -- Start
            Dim dtEndDate As Date = objPeriod._End_Date
            'Nilay (10-Feb-2016) -- End

            objPeriod = Nothing

            objReconciliation._TranHeadTypeId = CInt(cboTrnHeadType.SelectedValue)
            objReconciliation._TranHeadTypeName = cboTrnHeadType.Text
            objReconciliation._IgnoreZeroVariance = chkIgnoreZeroVariance.Checked

            'Sohail (23 Feb 2017) -- Start
            'CCBRT Enhancement - 65.1 - Allow to Show Employer Statutory Contribution and Informational heads in Payroll Variance Report.
            objReconciliation._ShowEmployerContribution = chkShowEmployerContribution.Checked
            objReconciliation._ShowInformationalHeads = chkShowInformationalHeads.Checked
            'Sohail (23 Feb 2017) -- End

            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            objReconciliation._ShowVariancePercentageColumns = chkShowVariancePercentageColumns.Checked
            'Hemant (22 Jan 2019) -- End

            objReconciliation._TranHeadUnkId = CInt(cboTranHead.SelectedValue)
            'Sohail (23 Nov 2016) -- Start
            'Enhancement - 64.1 - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objReconciliation._FromDatabaseName = Me.ViewState("FromDatabase").ToString()
            'objReconciliation._ToDatabaseName = Me.ViewState("ToDatabase").ToString()
            objReconciliation._FromDatabaseName = mstrFromDatabaseName
            objReconciliation._ToDatabaseName = mstrToDatabaseName

            objReconciliation._FromYearUnkId = mintFromYearUnkId
            objReconciliation._ToYearUnkId = mintToYearUnkId
            'Sohail (23 Nov 2016) -- End
            objReconciliation._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            objReconciliation._BaseCurrencyId = CInt(Session("Base_CurrencyId"))
            objReconciliation._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objReconciliation._OpenAfterExport = False
            GUI.fmtCurrency = CStr(Session("fmtcurrency"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objReconciliation.Export_Recociliaton_Report()

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Nilay (10-Feb-2016) -- Start
            'objReconciliation.Export_Recociliaton_Report(Session("UserId"), _
            '                                        Session("CompanyUnkId"), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                        Session("UserAccessModeSetting"), True, _
            '                                        Session("IsIncludeInactiveEmp"), True, _
            '                                        Session("fmtCurrency"), _
            '                                        Session("Base_CurrencyId"), _
            '                                        Session("ExportReportPath"), _
            '                                        Session("OpenAfterExport"))

            objReconciliation.Export_Recociliaton_Report(CInt(Session("UserId")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    dtStartDate, _
                                                    dtEndDate, _
                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                    False, True, _
                                                    CStr(Session("fmtCurrency")), _
                                                    CInt(Session("Base_CurrencyId")), _
                                                    IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                    False)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            If objReconciliation._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objReconciliation._FileNameAfterExported

                'Shani [ 18 NOV 2015 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

                'Shani [ 18 NOV 2014 ] -- END
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnExport_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnReset_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try

            'Hemant (29 Dec 2018) -- Start
            'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
            'Response.Redirect("~\UserHome.aspx", False)
            If Session("ReturnURL") IsNot Nothing AndAlso Session("ReturnURL").ToString.Trim <> "" Then
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If

            'Hemant (29 Dec 2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BtnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("FromDatabase") = Nothing
    '        Me.ViewState("ToDatabase") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End



#End Region

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End


            Me.lblPeriodFrom.Text = Language._Object.getCaption(Me.lblPeriodFrom.ID, Me.lblPeriodFrom.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriodTo.Text = Language._Object.getCaption(Me.lblPeriodTo.ID, Me.lblPeriodTo.Text)
            Me.lblTrnHeadType.Text = Language._Object.getCaption(Me.lblTrnHeadType.ID, Me.lblTrnHeadType.Text)
            Me.chkIgnoreZeroVariance.Text = Language._Object.getCaption(Me.chkIgnoreZeroVariance.ID, Me.chkIgnoreZeroVariance.Text)
            Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.ID, Me.lblTranHead.Text)
            'Hemant (22 Jan 2019) -- Start
            'Ref # 0003301 : For payroll variance report, there should be a check box to include % column but by default should be unchecked. Also payroll total variance report should have % column.
            Me.chkShowVariancePercentageColumns.Text = Language._Object.getCaption(Me.chkShowVariancePercentageColumns.ID, Me.chkShowVariancePercentageColumns.Text)
            'Hemant (22 Jan 2019) -- End
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

    
End Class
