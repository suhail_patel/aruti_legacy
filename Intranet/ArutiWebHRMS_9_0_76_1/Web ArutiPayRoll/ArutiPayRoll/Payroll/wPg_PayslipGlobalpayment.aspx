﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_PayslipGlobalpayment.aspx.vb" Inherits="Payroll_wPg_PayslipGlobalpayment"
    Title="Payslip Global Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/CashDenomination.ascx" TagName="CashDenomination" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Payslip Global Payment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div4" class="panel-default">
                                <div id="Div5" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td id="gridview" style="width: 39%">
                                                <div id="Div6" class="panel-default">
                                                    <div id="Div7" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="Label1" runat="server" Text="Employee List"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div8" class="panel-body-default" style="padding:0px">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                                        height: 485px; overflow: auto; vertical-align: top; border: 1px solid #DDD;">
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <asp:GridView ID="GvGlobalPayment" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                                                    ShowFooter="False" Width="99%" CellPadding="3" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                                                    RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" DataKeyNames="employeeunkid">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                                            HeaderStyle-HorizontalAlign="Center">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged" />
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                                    OnCheckedChanged="chkSelect_OnCheckedChanged" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="employeename" HeaderText="Name" ReadOnly="True" HeaderStyle-HorizontalAlign="Left"
                                                                                            FooterText="colhName" ItemStyle-Width="65%" HeaderStyle-Width="65%" />
                                                                                        <asp:BoundField DataField="amount" HeaderText="Balance Amount" ReadOnly="True" HeaderStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-HorizontalAlign="Right" FooterText="colhAmount" ItemStyle-Width="30%"
                                                                                            HeaderStyle-Width="30%" />
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                                                                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                                                <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                                <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                                <asp:AsyncPostBackTrigger ControlID="dtpPaymentDate" EventName="TextChanged" />
                                                                                <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <h4 class="heading1" style="width: 100%; border-bottom: 1px solid #DDD; text-align: left;">
                                                                        &nbsp;<asp:Label ID="gbAdvanceAmountInfo" runat="server" Text="Total Payment Amount"></asp:Label></h4>
                                                                    <table width="100%">
                                                                        <tr style="width: 100%">
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:Label ID="lblTotalPaymentAmount" runat="server" Text="Total Payment Amt. (Tsh)"></asp:Label>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="dtpPaymentDate" EventName="TextChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:UpdatePanel ID="UpdatePanel17" runat="server" UpdateMode="Conditional">
                                                                                            <ContentTemplate>
                                                                                                <asp:TextBox ID="txtTotalPaymentAmount" Style="text-align: right;" runat="server"
                                                                                                    ReadOnly="true" Text="0"></asp:TextBox>
                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="txtCashPerc" EventName="TextChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td id="Controls" style="width: 59%; vertical-align: top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="FilterCriteria" class="panel-default">
                                                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                                    <div style="float: left;">
                                                                        <asp:Label ID="gbFilterCriteria" runat="server" Text="Employee Filter Criteria"></asp:Label>
                                                                    </div>
                                                                    <div style="text-align: right;">
                                                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                                                            Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                                <div id="FilterCriteriaBody" class="panel-body-default">
                                                                    <table width="100%">
                                                                        <tr style="width:100%;">
                                                                            <td style="width: 13%">
                                                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" Width="100%"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 85%" colspan="3">
                                                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width:100%;">
                                                                            <td style="width: 15%">
                                                                                <asp:Label ID="lblEmpBank" runat="server" Text="Emp. Bank"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:DropDownList ID="cboEmpBank" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:Label ID="lblEmpBranch" runat="server" Text="Emp. Branch"></asp:Label>
                                                                                <asp:Label ID="lblCutOffAmount" runat="server" Text="Cut Off Amount" Visible="false"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:DropDownList ID="cboEmpBranch" runat="server">
                                                                                </asp:DropDownList>
                                                                                <asp:TextBox ID="txtCutOffAmount" runat="server" AutoPostBack="true" Visible="false"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width:100%;">
                                                                            <td style="width: 15%">
                                                                                <asp:Label ID="lblPayPoint" runat="server" Text="Pay Point"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:DropDownList ID="cboPayPoint" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td style="width: 15%">
                                                                                <asp:Label ID="lblPayType" runat="server" Text="Pay Type"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:DropDownList ID="cboPayType" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <div class="btn-default">
                                                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <div id="Div2" class="panel-default">
                                                                <div id="Div3" class="panel-heading-default">
                                                                    <div style="float: left;">
                                                                        <asp:Label ID="gbPaymentInfo" runat="server" Text="Payment Information"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <div id="Div1" class="panel-body-default">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPayYear" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td style="width: 13%">
                                                                                
                                                                            </td>
                                                                            <td style="width: 29%">
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                               <asp:Label ID="lblPaymentDatePeriod" runat="server" Text="Payment Date Period"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:UpdatePanel ID="UpdatePanel19" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboPaymentDatePeriod" runat="server" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPayYear" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td style="width: 13%">
                                                                                 <asp:Label ID="lblPayYear" runat="server" Text="Pay Year" Visible="false"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 29%">
                                                                                <asp:UpdatePanel ID="UpdatePanel14" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboPayYear" runat="server" AutoPostBack="true" Visible="false"> 
                                                                                </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblPaymentDate" runat="server" Text="Payment Date"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <uc2:DateCtrl ID="dtpPaymentDate" runat="server"></uc2:DateCtrl>
                                                                            </td>
                                                                            <td colspan="2" style="width: 42%">
                                                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:Label ID="objlblExRate" runat="server" Width="100%"></asp:Label>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="dtpPaymentDate" EventName="TextChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblAgainstVoucher" runat="server" Text="Payment voucher #"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:UpdatePanel ID="UpdatePanel15" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtVoucher" runat="server"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td style="width: 13%">
                                                                                <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 29%">
                                                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboPaymentMode" runat="server" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td style="width: 13%">
                                                                                <asp:UpdatePanel ID="UpdatePanel10" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtCashPerc" runat="server" AutoPostBack="true"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td style="width: 29%">
                                                                                <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:Label ID="lblCashPerc" runat="server" Text="%" Width="20px"></asp:Label>
                                                                                        <asp:Label ID="lblACHolder" runat="server" Text="To Non Bank A/C Holder"></asp:Label>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblBankGroup" runat="server" Text="Bank Group"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboBankGroup" runat="server" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                            <td colspan="2" rowspan="2" valign="top" style="width: 42%">
                                                                                <asp:UpdatePanel ID="UpdatePanel11" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:Label ID="lblCashDescr" runat="server" Font-Bold="true" Text="Each Selected Employee will get the amount of above given percentage of their Balance Amount."></asp:Label>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 35%">
                                                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboBankGroup" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblAccountNo" runat="server" Text="Account No"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 77%" colspan="3">
                                                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="cboAccountNo" runat="server">
                                                                                        </asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboBranch" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblCheque" runat="server" Text="Cheque No"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 77%" colspan="3">
                                                                                <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtChequeNo" runat="server"> </asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                    <Triggers>
                                                                                        <asp:AsyncPostBackTrigger ControlID="cboPaymentMode" EventName="SelectedIndexChanged" />
                                                                                        <asp:AsyncPostBackTrigger ControlID="btnProcess" EventName="Click" />
                                                                                    </Triggers>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 23%">
                                                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 77%" colspan="3">
                                                                                <asp:UpdatePanel ID="UpdatePanel18" runat="server" UpdateMode="Conditional">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Height="50px"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnProcess" runat="server" Text="Process" Width="75px" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="75px" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc7:CashDenomination ID="popupCashDenom" runat="server" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
