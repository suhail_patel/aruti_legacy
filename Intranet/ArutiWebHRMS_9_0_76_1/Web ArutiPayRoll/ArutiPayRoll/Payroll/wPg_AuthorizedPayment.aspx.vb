﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Data.SqlClient

#End Region

Partial Class Payroll_wPg_AuthorizedPayment
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    'Hemant (04 Sep 2020) -- Start
    'Bug : Application Performance issue
    'Private objPaymentTran As clsPayment_tran
    'Hemant (04 Sep 2020) -- End
    Private trd As Thread

    Private mdtTable As DataTable 'Sohail (10 Mar 2014)

    'Nilay (10-Feb-2016) -- Start
    Private mdtPayPeriodStartDate As Date
    Private mdtPayPeriodEndDate As Date
    'Nilay (10-Feb-2016) -- End

    Private mstrAdvanceFilter As String = "" 'Sohail (18 Feb 2014)

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmAuthorizeUnauthorizePayment"
    'Anjan [04 June 2014] -- End

    Private objCONN As SqlConnection 'Sohail (17 Dec 2014)
#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objPaymentTran As clsPayment_tran
        'Hemant (04 Sep 2020) -- End
        Try
            'Sohail (17 Dec 2014) -- Start
            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
            'If Session("clsuser") Is Nothing Or Session("IsAuthorized") Is Nothing Then
            '    Exit Sub
            'End If

            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                Me.Title = ""

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'Me.Closebotton1.PageHeading = ""
                Me.lblPageHeader.Text = ""
                'SHANI [01 FEB 2015]--END 

                If Request.QueryString.Count > 0 Then
                    If Request.UrlReferrer IsNot Nothing AndAlso Request.UrlReferrer.OriginalString.ToString().Contains("PaymentApproval") Then
                        GoTo FromPaymentAuthorize
                    End If
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))

                    If arr.Length = 5 Then

                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))

                        Session("IsAuthorized") = CBool(arr(2))
                        Me.ViewState.Add("mintPeriodUnkId", CInt(arr(3)))
                        Me.ViewState.Add("mintCurrencyUnkId", CInt(arr(4)))


                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objCommon As New CommonCodes
                        'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End
                        HttpContext.Current.Session("mdbname") = Session("Database_Name")
                        gobjConfigOptions = New clsConfigOptions
                        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False) Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If

                        Dim clsConfig As New clsConfigOptions
                        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        Session("fmtCurrency") = clsConfig._CurrencyFormat
                        Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim
                        Session("SetPayslipPaymentApproval") = clsConfig._SetPayslipPaymentApproval
                        Session("NotifyPayroll_Users") = clsConfig._Notify_Payroll_Users

                        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            Session("ArutiSelfServiceURL") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                        Else
                            Session("ArutiSelfServiceURL") = clsConfig._ArutiSelfServiceURL
                        End If


                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try


                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        'Sohail (21 Mar 2015) -- Start
                        'Enhancement - New UI Notification Link Changes.
                        'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()
                        Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                        'Sohail (21 Mar 2015) -- End
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        'gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        'gobjLocalization._LangId = HttpContext.Current.Session("LangId")
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If
                        'Sohail (30 Mar 2015) -- End

                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'Dim objUserPrivilege As New clsUserPrivilege
                        'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                        'Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance
                        'Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
                        'Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
                        'Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense

                        'Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave
                        'Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave

                        'Dim objMaster As New clsMasterData
                        'Session("UserAccessLevel") = objMaster.GetUserAccessLevel(CInt(Session("UserId")), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("UserAccessModeSetting"))
                        'If Session("LoginBy") = Global.User.en_loginby.Employee Then
                        '    Session("UserAccessLevel") = ""
                        'End If

                        'If Session("UserAccessLevel") = "" Then
                        '    Session("AccessLevelFilterString") = " AND 1 = 1 "

                        '    Session("AccessLevelBranchFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelDepartmentGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelDepartmentFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelSectionGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelSectionFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelUnitGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelUnitFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelTeamFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelJobGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelJobFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelClassGroupFilterString") = " AND 1 = 1 "
                        '    Session("AccessLevelClassFilterString") = " AND 1 = 1 "
                        'Else
                        '    Session("AccessLevelFilterString") = UserAccessLevel._AccessLevelFilterString

                        '    Session("AccessLevelBranchFilterString") = UserAccessLevel._AccessLevelBranchFilterString
                        '    Session("AccessLevelDepartmentGroupFilterString") = UserAccessLevel._AccessLevelDepartmentGroupFilterString
                        '    Session("AccessLevelDepartmentFilterString") = UserAccessLevel._AccessLevelDepartmentFilterString
                        '    Session("AccessLevelSectionGroupFilterString") = UserAccessLevel._AccessLevelSectionGroupFilterString
                        '    Session("AccessLevelSectionFilterString") = UserAccessLevel._AccessLevelSectionFilterString
                        '    Session("AccessLevelUnitGroupFilterString") = UserAccessLevel._AccessLevelUnitGroupFilterString
                        '    Session("AccessLevelUnitFilterString") = UserAccessLevel._AccessLevelUnitFilterString
                        '    Session("AccessLevelTeamFilterString") = UserAccessLevel._AccessLevelTeamFilterString
                        '    Session("AccessLevelJobGroupFilterString") = UserAccessLevel._AccessLevelJobGroupFilterString
                        '    Session("AccessLevelJobFilterString") = UserAccessLevel._AccessLevelJobFilterString
                        '    Session("AccessLevelClassGroupFilterString") = UserAccessLevel._AccessLevelClassGroupFilterString
                        '    Session("AccessLevelClassFilterString") = UserAccessLevel._AccessLevelClassFilterString
                        'End If
                        'Sohail (30 Mar 2015) -- End

                        Call SetLanguage()
                        'If CBool(Session("mblnFromApprove")) = True Then
                        '    btnProcess.Text = Language.getMessage(mstrModuleName, 12, "Approve Payment")
                        '    lblRemarks.Text = Language.getMessage(mstrModuleName, 13, "Approval Remarks")
                        '    Closebotton1.PageHeading = Language.getMessage(mstrModuleName, 11, "Approve Payment")
                        'ElseIf CBool(Session("mblnFromApprove")) = False Then
                        '    btnProcess.Text = Language.getMessage(mstrModuleName, 15, "Void Approved Payment")
                        '    lblRemarks.Text = Language.getMessage(mstrModuleName, 16, "Disapproval Remarks")
                        '    Closebotton1.PageHeading = Language.getMessage(mstrModuleName, 14, "Void Approved Payment")
                        'End If

                        Call CreateListTable()
                        Call FillCombo()
                        cboPayPeriod.SelectedValue = CStr(arr(3))
                        cboCurrency.SelectedValue = CStr(arr(4))
                        Call cboCurrency_SelectedIndexChanged(sender, e)

                        'Hemant (29 Dec 2018) -- Start
                        'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
                        lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
                        'Hemant (29 Dec 2018) -- End
                        'Hemant (17 Oct 2019) -- Start
                        lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
                        'Hemant (17 Oct 2019) -- End
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                        HttpContext.Current.Session("Login") = True
                        'Sohail (30 Mar 2015) -- End
                    Else
                        Exit Sub
                    End If
                Else
                    Exit Sub
                End If
            End If

FromPaymentAuthorize:

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False) Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False) Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            
            'Sohail (17 Dec 2014) -- End

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End


            objPaymentTran = New clsPayment_tran

            If Not IsPostBack Then
                'Hemant (04 Sep 2020) -- Start
                'Bug : Application Performance issue
                GC.Collect()
                'Hemant (04 Sep 2020) -- End
                If ViewState("mintPeriodUnkId") Is Nothing Then 'Sohail (17 Dec 2014)
                Call CreateListTable() 'Sohail (10 Mar 2014)
                FillCombo()
                End If 'Sohail (17 Dec 2014)

                'Hemant (29 Dec 2018) -- Start
                'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
                lnkPayrollVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
                'Hemant (29 Dec 2018) -- End
                'Hemant (17 Oct 2019) -- Start
                lnkPayrollTotalVarianceReport.Visible = clsArutiReportClass.Is_Report_Assigned(enArutiReport.PayrollTotalVariance, CInt(Session("UserId")), CInt(Session("Companyunkid")))
                'Hemant (17 Oct 2019) -- End
                'Sohail (18 Feb 2014) -- Start
                'Enhancement - AGKN
            Else
                'Nilay (10-Feb-2016) -- Start
                mdtPayPeriodStartDate = CDate(Me.ViewState("PayPeriodStartDate"))
                mdtPayPeriodEndDate = CDate(Me.ViewState("PayPeriodEndDate"))
                'Nilay (10-Feb-2016) -- End
                mstrAdvanceFilter = CStr(ViewState("mstrAdvanceFilter"))
                'Sohail (18 Feb 2014) -- End
                mdtTable = CType(ViewState("mdtTable"), DataTable) 'Sohail (10 Mar 2014)
            End If
            If CBool(Session("IsAuthorized")) = True Then
                btnProcess.Text = Language.getMessage(mstrModuleName, 17, "Authorize Payment")
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                btnProcess.Enabled = CBool(Session("AllowAuthorizePayslipPayment"))
                'Varsha Rana (17-Oct-2017) -- End
            ElseIf CBool(Session("IsAuthorized")) = False Then
                btnProcess.Text = Language.getMessage(mstrModuleName, 20, "Void Authorized Payment")
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                btnProcess.Enabled = CBool(Session("AllowVoidAuthorizedPayslipPayment"))
                'Varsha Rana (17-Oct-2017) -- End
            End If
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Closebotton1.PageHeading = btnProcess.Text
            lblPageHeader.Text = btnProcess.Text
            'SHANI [01 FEB 2015]--END 

            Me.Title = btnProcess.Text
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'Sohail (17 Dec 2014) -- Start
        'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
        'Me.IsLoginRequired = True
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
        'Sohail (17 Dec 2014) -- End
    End Sub

    'Sohail (18 Feb 2014) -- Start
    'Enhancement - AGKN
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter) 'Sohail (18 Feb 2014)
        Me.ViewState.Add("mdtTable", mdtTable) 'Sohail (10 Mar 2014)
        'Nilay (10-Feb-2016) -- Start
        Me.ViewState("PayPeriodStartDate") = mdtPayPeriodStartDate
        Me.ViewState("PayPeriodEndDate") = mdtPayPeriodEndDate
        'Nilay (10-Feb-2016) -- End

    End Sub
    'Sohail (18 Feb 2014) -- End

#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objExRate As New clsExchangeRate
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombo As DataSet
        Dim mdicCurrency As New Dictionary(Of Integer, String)
        Try

            'Sohail (21 Jan 2014) -- Start
            'Enhancement - Oman
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("fin_year")), "PayPeriod", True, 1, True, , Session("Database_Name"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("fin_year")), "PayPeriod", True, 1, False, , Session("Database_Name"))
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "PayPeriod", True, 1, False)
            'Shani(20-Nov-2015) -- End

            'Sohail (21 Jan 2014) -- End
            With cboPayPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("PayPeriod")
                .DataBind()
                .SelectedValue = "0"
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - Authorize payment total not matching with global payment total.
                If .Items.Count > 0 Then
                    Call cboCurrency_SelectedIndexChanged(Me, New System.EventArgs())
                End If
                'Sohail (26 May 2017) -- End
            End With

            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            Dim intBaseCountryId As Integer = 0
            'Sohail (16 Oct 2019) -- End
            dsCombo = objExRate.getComboList("ExRate", False)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    Me.ViewState.Add("CurrencySign", dtTable.Rows(0).Item("currency_sign").ToString())
                    Me.ViewState.Add("CurrencyID", CInt(dtTable.Rows(0).Item("exchangerateunkid")))
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    intBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                    'Sohail (16 Oct 2019) -- End
                End If

                With cboCurrency
                    .DataValueField = "countryunkid"
                    .DataTextField = "currency_sign"
                    .DataSource = dsCombo.Tables("ExRate")
                    .DataBind()
                    'Sohail (16 Oct 2019) -- Start
                    'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                    '.SelectedIndex = 0
                    If intBaseCountryId > 0 Then
                        .SelectedValue = intBaseCountryId.ToString
                    Else
                        .SelectedIndex = 0
                    End If
                    'Sohail (16 Oct 2019) -- End
                End With

                For Each dsRow As DataRow In dsCombo.Tables(0).Rows
                    If mdicCurrency.ContainsKey(CInt(dsRow.Item("exchangerateunkid"))) = False Then
                        mdicCurrency.Add(CInt(dsRow.Item("exchangerateunkid")), dsRow.Item("currency_sign").ToString)
                    End If
                Next
                Me.ViewState.Add("DicCurrency", mdicCurrency)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            objExRate = Nothing
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsCombo) = False Then
                dsCombo.Clear()
                dsCombo = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet = Nothing
        Dim strFilter As String = ""
        Dim decTotAmt As Decimal = 0
        Dim mstrPaymentTranIDs As String = ""
        Dim mintBankPayment As Integer = 0
        Dim mintCashPayment As Integer = 0
        Dim mdecCashPayment As Decimal = 0
        Dim mdecBankPayment As Decimal = 0

        Dim objPaymentTran As New clsPayment_tran 'Sohail (17 Dec 2014)

        Try

            mdtTable.Rows.Clear() 'Sohail (10 Mar 2014)
            txtTotalAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                objBankpaidVal.Text = mintBankPayment.ToString()
                objCashpaidVal.Text = mintCashPayment.ToString()
                objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString()
                GvAuthorizedPayment.DataSource = Nothing
                GvAuthorizedPayment.DataBind()
                txtTotalAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))
                Exit Try
            End If

            strFilter = "prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " " & _
                        "AND prpayment_tran.countryunkid = " & CInt(cboCurrency.SelectedValue) & " "

            If CBool(Session("IsAuthorized")) = True Then
                strFilter &= "AND ISNULL(isauthorized, 0) = 0 "
                If CBool(Session("SetPayslipPaymentApproval")) = True Then
                    strFilter &= "AND ISNULL(prpayment_tran.isapproved, 0) = 1 "
                End If

            Else
                strFilter &= "AND ISNULL(isauthorized, 0) = 1 "
            End If

            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If cboPmtVoucher.SelectedIndex > 0 Then
                strFilter &= " AND (prpayment_tran.voucherno = '" & cboPmtVoucher.Text & "' OR prglobalvoc_master.globalvocno = '" & cboPmtVoucher.Text & "') "
            End If
            If mstrAdvanceFilter.Trim <> "" Then
                strFilter &= " AND " & mstrAdvanceFilter
            End If
            'Sohail (18 Feb 2014) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPaymentTran.GetList("List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter, Session("AccessLevelFilterString"))

            'Nilay (10-Feb-2016) -- Start
            'dsList = objPaymentTran.GetList(Session("Database_Name"), _
            '                              Session("UserId"), _
            '                              Session("Fin_year"), _
            '                              Session("CompanyUnkId"), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                              Session("UserAccessModeSetting"), True, _
            '                              Session("IsIncludeInactiveEmp"), "List", _
            '                              clsPayment_tran.enPaymentRefId.PAYSLIP, 0, _
            '                              clsPayment_tran.enPayTypeId.PAYMENT, _
            '                              strFilter, "")

            dsList = objPaymentTran.GetList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            mdtPayPeriodStartDate, _
                                            mdtPayPeriodEndDate, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "List", _
                                            clsPayment_tran.enPaymentRefId.PAYSLIP, 0, _
                                            clsPayment_tran.enPayTypeId.PAYMENT, _
                                            True, strFilter)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("currency") = False Then
                dsList.Tables(0).Columns.Add("currency", Type.GetType("System.String")).DefaultValue = ""
            End If

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            Dim dtRow As DataRow
            Dim dRow As DataRow
            'Sohail (10 Mar 2014) -- End

            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'If mstrPaymentTranIDs = "" Then
                '    mstrPaymentTranIDs = dsList.Tables("List").Rows(i)("paymenttranunkid").ToString
                'Else
                '    mstrPaymentTranIDs &= ", " & dsList.Tables("List").Rows(i)("paymenttranunkid").ToString
                'End If

                'decTotAmt += CDec(dsList.Tables("List").Rows(i)("expaidamt"))

                'If Me.ViewState("DicCurrency") IsNot Nothing Then
                '    Dim mdicCurrency As Dictionary(Of Integer, String) = Me.ViewState("DicCurrency")
                '    If mdicCurrency.ContainsKey(CInt(dsList.Tables("List").Rows(i)("paidcurrencyid"))) = True Then
                '        dsList.Tables("List").Rows(i)("currency") = mdicCurrency.Item(CInt(dsList.Tables("List").Rows(i)("paidcurrencyid")))
                '    Else
                '        dsList.Tables("List").Rows(i)("currency") = Me.ViewState("CurrencySign").ToString()
                '    End If
                'End If
                dtRow = dsList.Tables("List").Rows(i)
                dRow = mdtTable.NewRow()

                dRow.Item("paymentauthorizetranunkid") = -1
                dRow.Item("paymenttranunkid") = CInt(dtRow.Item("paymenttranunkid"))
                dRow.Item("employeeunkid") = CInt(dtRow.Item("employeeunkid"))
                dRow.Item("employeecode") = dtRow.Item("employeecode").ToString
                dRow.Item("EmpName") = dtRow.Item("EmpName").ToString
                dRow.Item("expaidamt") = Format(CDec(dtRow.Item("expaidamt")), CStr(Session("fmtCurrency")))
                dRow.Item("expaidamt_tag") = CDec(dtRow.Item("expaidamt"))

                decTotAmt += CDec(dtRow.Item("expaidamt"))

                If Me.ViewState("DicCurrency") IsNot Nothing Then
                    Dim mdicCurrency As Dictionary(Of Integer, String) = CType(Me.ViewState("DicCurrency"), Global.System.Collections.Generic.Dictionary(Of Integer, String))
                    If mdicCurrency.ContainsKey(CInt(dsList.Tables("List").Rows(i)("paidcurrencyid"))) = True Then
                        dRow.Item("paidcurrency") = mdicCurrency.Item(CInt(dsList.Tables("List").Rows(i)("paidcurrencyid")))
                    Else
                        dRow.Item("paidcurrency") = Me.ViewState("CurrencySign").ToString()
                    End If
                End If
                dRow.Item("paymentmode") = dtRow.Item("paymentmode").ToString
                'Sohail (10 Mar 2014) -- End



                If CInt(dsList.Tables("List").Rows(i)("paymentmode")) = enPaymentMode.CASH Or CInt(dsList.Tables("List").Rows(i)("paymentmode")) = enPaymentMode.CASH_AND_CHEQUE Then
                    mintCashPayment += 1
                    'mdecCashPayment += CDec(dsList.Tables("List").Rows(i)("expaidamt")) 'Sohail (26 May 2017)
                ElseIf CInt(dsList.Tables("List").Rows(i)("paymentmode")) = enPaymentMode.CHEQUE Or CInt(dsList.Tables("List").Rows(i)("paymentmode")) = enPaymentMode.TRANSFER Then
                    mintBankPayment += 1
                    'mdecBankPayment += CDec(dsList.Tables("List").Rows(i)("expaidamt")) 'Sohail (26 May 2017)
                End If


                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                mdtTable.Rows.Add(dRow)
                'Sohail (10 Mar 2014) -- End

            Next

            'GvAuthorizedPayment.DataSource = dsList.Tables(0)
            'GvAuthorizedPayment.DataBind()

            Me.ViewState.Add("PaymentIds", mstrPaymentTranIDs)
            Me.ViewState.Add("BankPaymentCount", mintBankPayment)
            Me.ViewState.Add("BankPayment", mdecBankPayment.ToString(Session("fmtCurrency").ToString))
            Me.ViewState.Add("CashPaymentCount", mintCashPayment)
            Me.ViewState.Add("CashPayment", mdecCashPayment.ToString(Session("fmtCurrency").ToString))

            objBankpaidVal.Text = mintBankPayment.ToString()
            objCashpaidVal.Text = mintCashPayment.ToString()
            objTotalpaidVal.Text = (mintBankPayment + mintCashPayment).ToString()

            'txtTotalAmount.Text = Format(decTotAmt, Session("fmtCurrency")) 'Sohail (10 Mar 2014)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            'If dsList Is Nothing Then
            '    dsList = New DataSet
            '    dsList.Tables.Add("List")
            '    dsList.Tables(0).Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False 'Sohail (10 Mar 2014)
            '    dsList.Tables(0).Columns.Add("Date", System.Type.GetType("System.String")).DefaultValue = ""
            '    dsList.Tables(0).Columns.Add("Voc", System.Type.GetType("System.String")).DefaultValue = ""
            '    dsList.Tables(0).Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
            '    dsList.Tables(0).Columns.Add("EmpName", System.Type.GetType("System.String")).DefaultValue = ""
            '    dsList.Tables(0).Columns.Add("expaidamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
            '    dsList.Tables(0).Columns.Add("currency", System.Type.GetType("System.String")).DefaultValue = ""
            'End If
            'If dsList.Tables(0).Rows.Count <= 0 Then

            '    Dim r As DataRow = dsList.Tables(0).NewRow
            '    r.Item(3) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = ""

            '    dsList.Tables(0).Rows.Add(r)
            'End If

            'GvAuthorizedPayment.DataSource = dsList.Tables(0)
            'GvAuthorizedPayment.DataBind()
            If mdtTable.Rows.Count <= 0 Then
                Dim r As DataRow = mdtTable.NewRow

                r.Item(4) = "None" ' To Hide the row and display only Row Header
                r.Item(5) = ""

                mdtTable.Rows.Add(r)
            End If

            GvAuthorizedPayment.DataSource = mdtTable
            GvAuthorizedPayment.DataBind()
            'Sohail (10 Mar 2014) -- End
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
            If IsNothing(dsList) = False Then
                dsList.Clear()
                dsList = Nothing
            End If
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Send Email Notification to lower levels when payment disapproved.
    Private Sub CreateListTable()
        Try
            mdtTable = New DataTable
            With mdtTable
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("paymentauthorizetranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("paymenttranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("employeeunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("employeecode", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("EmpName", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("expaidamt", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("expaidamt_tag", System.Type.GetType("System.Decimal")).DefaultValue = 0
                .Columns.Add("paidcurrency", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("paymentmode", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In GvAuthorizedPayment.Rows
                If CInt(mdtTable.Rows(gvr.RowIndex).Item("employeeunkid")) > 0 AndAlso mdtTable.Rows(gvr.RowIndex).Item("employeecode").ToString <> "" Then
                    cb = CType(GvAuthorizedPayment.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                    cb.Checked = blnCheckAll

                    mdtTable.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim decTotAmt As Decimal = 0
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllEmployee(chkSelectAll.Checked)

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'decTotAmt = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True Select CDec(p.Item("expaidamt"))).Sum()
            decTotAmt = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True Select CDec(p.Item("expaidamt_tag"))).Sum()
            'Sohail (26 May 2017) -- End

            txtTotalAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim decTotAmt As Decimal
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtTable.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

            'Sohail (26 May 2017) -- Start
            'Issue - 67.1 - Authorize payment total not matching with global payment total.
            'decTotAmt = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True Select CDec(p.Item("expaidamt"))).Sum()
            decTotAmt = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True Select CDec(p.Item("expaidamt_tag"))).Sum()
            'Sohail (26 May 2017) -- End
            txtTotalAmount.Text = Format(decTotAmt, CStr(Session("fmtCurrency")))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (10 Mar 2014) -- End

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please select Pay Period. Pay Period is mandatory information.", Me)
                'Sohail (23 Mar 2019) -- End
                Return False
            ElseIf CInt(cboCurrency.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please select Currency. Currency is mandatory information.", Me)
                'Sohail (23 Mar 2019) -- End
                Return False
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'ElseIf GvAuthorizedPayment.Rows.Count <= 0 Then
                'DisplayMessage.DisplayError(ex, Me)
            ElseIf mdtTable.Select("IsChecked = 1").Length <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please select atleast one employee to Authorize / Void Authorized.", Me)
                'Sohail (23 Mar 2019) -- End
                'Sohail (10 Mar 2014) -- End
                Return False
            End If

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            If gobjEmailList.Count > 0 AndAlso Session("NotifyPayroll_Users").ToString.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage("Sending Email(s) process is in progress from other module. Please wait for some time.", Me)
                Return False
            End If
            'Sohail (10 Mar 2014) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Function Set_Notification(ByVal strUserName As String _
                                   , ByVal strAuthorizeUserName As String _
                                   , ByVal strPeriodName As String _
                                   , ByVal intBankPaid As Integer _
                                   , ByVal intCashPaid As Integer _
                                   , ByVal intTotalPaid As Integer _
                                   , ByVal decBankAmount As Decimal _
                                   , ByVal decCashAmount As Decimal _
                                   , ByVal decTotalAmount As Decimal _
                                   , ByVal strCurrencySign As String _
                                   , ByVal strRemarks As String _
                                   ) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)

            If Session("NotifyPayroll_Users").ToString().Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("Dear <b>" & strUserName & "</b>,</span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Payslip Payment has been authorized for the period of <b>" & strPeriodName & " </b> for <b>" & (intCashPaid + intBankPaid).ToString & "</b> employees. This Payment has been authorized by <b>" & strAuthorizeUserName & "</b> from Machine : <b>" & Session("HOST_NAME").ToString & "</b> and IPAddress : <b>" & Session("IP_ADD").ToString & "</b></span></p>")
                If CBool(Session("IsAuthorized")) = True Then
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Payslip Payment has been authorized for the period of <b>" & strPeriodName & " </b> for <b>" & (intCashPaid + intBankPaid).ToString & "</b> employees. This Payment has been authorized by <b>" & strAuthorizeUserName & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                Else
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that Payslip Payment Authorization has been voided for the period of <b>" & strPeriodName & " </b> for <b>" & (intCashPaid + intBankPaid).ToString & "</b> employees. This Payment Authorization has been voided by <b>" & strAuthorizeUserName & "</b> from Machine : <b>" & getHostName.ToString & "</b> and IPAddress : <b>" & getIP.ToString & "</b></span></p>")
                End If
                'Sohail (10 Mar 2014) -- End
                StrMessage.Append(vbCrLf)

                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                'For Each sId As String In Session("NotifyPayroll_Users").ToString().Split(CChar("||"))(0).Split(CChar(","))
                '    Select Case CInt(sId)
                '        Case enNotificationPayroll.AUTHORIZED_PAYEMNT
                'Sohail (10 Mar 2014) -- End
                StrMessage.Append("<TABLE border='1' style='margin-left:50px;width:502px;font-size:9.0pt;'>")

                StrMessage.Append("<TR style='background-color:Purple;color:White; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & ";'>")

                StrMessage.Append("<TD align='center' style='width:300px'>")
                StrMessage.Append("Particulars")
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append("Emp. Count")
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append("Amount (" & strCurrencySign & ")")
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append("Total Bank Payment")
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(intBankPaid.ToString)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decBankAmount.ToString(Session("fmtCurrency").ToString))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append("Total Cash Payment")
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD width='200px' align='center'>")
                StrMessage.Append(intCashPaid.ToString)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD width='200px' align='right'>")
                StrMessage.Append(decCashAmount.ToString(Session("fmtCurrency").ToString))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:300px'>")
                StrMessage.Append("Total Payment")
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='center' style='width:200px'>")
                StrMessage.Append(intTotalPaid.ToString)
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD align='right' style='width:200px'>")
                StrMessage.Append(decTotalAmount.ToString(Session("fmtCurrency").ToString))
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")
                '-----------------------------

                StrMessage.Append("<TR style = 'font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")
                StrMessage.Append("<TD style='width:200px'>")
                StrMessage.Append("Remarks")
                StrMessage.Append("</TD>")

                StrMessage.Append("<TD colspan='2' >")
                StrMessage.Append(strRemarks)
                StrMessage.Append("</TD>")

                StrMessage.Append("</TR>")

                StrMessage.Append("</TABLE>")

                blnFlag = True

                'Sohail (10 Mar 2014) -- Start
                'Enhancement - Send Email Notification to lower levels when payment disapproved.
                '    End Select
                'Next
                'Sohail (10 Mar 2014) -- End
                StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                StrMessage.Append("</span></p>")
                StrMessage.Append("</BODY></HTML>")

                If blnFlag = False Then
                    StrMessage = StrMessage.Remove(0, StrMessage.Length)
                End If
            End If
            Return StrMessage.ToString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Send Email Notification to lower levels when payment disapproved.
            'Dim dicNotification As Dictionary(Of String, String) = Me.ViewState("Notification")
            'If dicNotification.Keys.Count > 0 Then
            '    Dim objSendMail As New clsSendMail
            '    For Each sKey As String In dicNotification.Keys
            '        If dicNotification(sKey).Trim.Length > 0 Then
            '            objSendMail._ToEmail = sKey
            '            objSendMail._Subject = "Notification of Payslip Payment Authorized."
            '            objSendMail._Message = dicNotification(sKey)
            '            objSendMail.SendMail()
            '        End If
            '    Next
            'End If
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    'S.SANDEEP [ 28 JAN 2014 ] -- START
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    'S.SANDEEP [ 28 JAN 2014 ] -- END
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
            'Sohail (10 Mar 2014) -- End
        Catch ex As Exception
            Call DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ComboBox Event"

    Protected Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, cboPayPeriod.SelectedIndexChanged
        Try
            'Sohail (18 Feb 2014) -- Start
            'Enhancement - AGKN
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim objPayment As New clsPayment_tran
                Dim dsCombo As DataSet
                dsCombo = objPayment.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CStr(Session("Database_Name")), CInt(cboPayPeriod.SelectedValue))
                With cboPmtVoucher
                    .DataValueField = "voucherno"
                    .DataTextField = "voucherno"
                    .DataSource = dsCombo.Tables("Voucher")
                    .DataBind()
                End With

                'Nilay (10-Feb-2016) -- Start
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
                mdtPayPeriodStartDate = CDate(objPeriod._Start_Date)
                mdtPayPeriodEndDate = CDate(objPeriod._End_Date)
                objPeriod = Nothing

                objPayment = Nothing
            Else
                mdtPayPeriodStartDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                mdtPayPeriodEndDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                'Nilay (10-Feb-2016) -- End

            End If

            'Sohail (18 Feb 2014) -- End
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event"

    Protected Sub GvAuthorizedPayment_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvAuthorizedPayment.RowDataBound
        Try
            'Sohail (16 Oct 2019) -- Start
            'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
            If e.Row.RowType = DataControlRowType.Header OrElse e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(3).Visible = CBool(Session("AllowToViewPaidAmount"))
            End If
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).Enabled = CBool(Session("AllowToViewPaidAmount"))
            End If
            'Sohail (16 Oct 2019) -- End

            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(1).Text = "None" AndAlso e.Row.Cells(2).Text = "&nbsp;" Then
                    e.Row.Visible = False
                Else
                    e.Row.Cells(3).Text = CDec(e.Row.Cells(3).Text).ToString(Session("fmtcurrency").ToString)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvAuthorizedPayment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GvAuthorizedPayment.RowCommand
        Try


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcess.Click
        'Hemant (04 Sep 2020) -- Start
        'Bug : Application Performance issue
        Dim objPaymentTran As New clsPayment_tran
        'Hemant (04 Sep 2020) -- End
        Dim blnResult As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub

            Blank_ModuleName()
            objPaymentTran._WebFormName = "frmAuthorizeUnauthorizePayment"
            StrModuleName2 = "mnuPayroll"
            objPaymentTran._WebIP = CStr(Session("IP_ADD"))
            objPaymentTran._WebHostName = CStr(Session("HOST_NAME"))

            'Sohail (10 Mar 2014) -- Start
            'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
            Dim mstrPaymentTranIDs As String = ""
            Dim arrList As List(Of String) = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True Select (p.Item("paymenttranunkid").ToString)).ToList
            mstrPaymentTranIDs = String.Join(",", arrList.ToArray)
            'Sohail (10 Mar 2014) -- End

            blnResult = objPaymentTran.UpdatePaymentAuthorization(CBool(Session("IsAuthorized")), mstrPaymentTranIDs, CInt(Session("UserId")), True, txtRemarks.Text.Trim)

            If blnResult = False And objPaymentTran._Message <> "" Then
                DisplayMessage.DisplayMessage(objPaymentTran._Message, Me)
            Else
                If CBool(Session("IsAuthorized")) = True Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Payment Authorization process completed successfully.", Me)
                    'Sohail (23 Mar 2019) -- End
                Else
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Void Authorized Payment process completed successfully.", Me)
                    'Sohail (23 Mar 2019) -- End
                End If

                Dim mintBankPayment As Integer = 0
                Dim mintCashPayment As Integer = 0
                Dim mdecCashPayment As Decimal = 0
                Dim mdecBankPayment As Decimal = 0

                mintBankPayment = CInt(Me.ViewState("BankPaymentCount"))
                mdecBankPayment = CDec(Me.ViewState("BankPayment"))
                mintCashPayment = CInt(Me.ViewState("CashPaymentCount"))
                mdecCashPayment = CDec(Me.ViewState("CashPayment"))


                'Dim dicNotification As New Dictionary(Of String, String) 'Sohail (10 Mar 2014)
                If Session("NotifyPayroll_Users").ToString().Trim.Length > 0 Then
                    'Sohail (10 Mar 2014) -- Start
                    'Enhancement - Send Email Notification to lower levels when payment disapproved.
                    Dim objMaster As New clsMasterData
                    gobjEmailList = New List(Of clsEmailCollection)
                    Dim strIDs As String = ""
                    If CBool(Session("IsAuthorized")) = True Then
                        If Session("NotifyPayroll_Users").ToString().Split(CChar("||"))(0).Split(CChar(",")).Contains(CStr(enNotificationPayroll.AUTHORIZED_PAYEMNT)) = True Then
                            strIDs = Session("NotifyPayroll_Users").ToString().Split(CChar("||"))(2)
                        End If
                    Else
                        ''Send notification to those users who has privilede to Authorize Payment again.
                        'Dim objApproverMap As New clspayment_approver_mapping
                        'strIDs = objApproverMap.GetAuthorizePaymentUserUnkIDs()
                        If Session("NotifyPayroll_Users").ToString().Split(CChar("||"))(0).Split(CChar(",")).Contains(CStr(enNotificationPayroll.VOID_AUTHORIZED_PAYEMNT)) = True Then
                            strIDs = Session("NotifyPayroll_Users").ToString().Split(CChar("||"))(2)
                        End If
                    End If
                    'Sohail (10 Mar 2014) -- End

                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    'Sohail (10 Mar 2014) -- Start
                    'Enhancement - Send Email Notification to lower levels when payment disapproved.
                    'For Each sId As String In Session("NotifyPayroll_Users").ToString().Split(CChar("||"))(2).Split(CChar(","))
                    For Each sId As String In strIDs.Split(CChar(","))
                        'Sohail (10 Mar 2014) -- End
                        If sId.Trim = "" Then Continue For 'Sohail (15 Mar 2014)

                        objUsr._Userunkid = CInt(sId)

                        'Sohail (10 Mar 2014) -- Start
                        'Enhancement - Check list box on Payment Approval and Authorize Payment Screen.
                        'Sohail (16 Nov 2016) -- Start
                        'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                        'Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(CInt(sId), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("Database_Name")))
                        Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(CStr(Session("Database_Name")), CInt(sId), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mdtPayPeriodStartDate, mdtPayPeriodEndDate, CStr(Session("UserAccessModeSetting")), True, False)
                        'Sohail (16 Nov 2016) -- End

                        'Sohail (26 Apr 2017) -- Start
                        'AKFKU Issue - 66.1 - Emails were going to the users who dont have access to authorized employees due to brackets were missing for OrElse block.
                        'Dim lstBankPaid As List(Of DataRow) = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso CInt(p.Item("paymentmode").ToString) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.TRANSFER Select p).ToList
                        'mintBankPayment = lstBankPaid.Count

                        'mdecBankPayment = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso CInt(p.Item("paymentmode").ToString) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.TRANSFER Select CDec(p.Item("expaidamt_tag").ToString)).Sum()


                        'Dim lstCashPaid As List(Of DataRow) = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH_AND_CHEQUE Select p).ToList
                        'mintCashPayment = lstCashPaid.Count

                        'mdecCashPayment = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH_AND_CHEQUE Select CDec(p.Item("expaidamt_tag").ToString)).Sum()
                        Dim lstBankPaid As List(Of DataRow) = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode").ToString) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.TRANSFER) Select p).ToList
                        mintBankPayment = lstBankPaid.Count

                        mdecBankPayment = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode").ToString) = enPaymentMode.CHEQUE OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.TRANSFER) Select CDec(p.Item("expaidamt_tag").ToString)).Sum()


                        Dim lstCashPaid As List(Of DataRow) = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH_AND_CHEQUE) Select p).ToList
                        mintCashPayment = lstCashPaid.Count

                        mdecCashPayment = (From p In mdtTable Where CBool(p.Item("IsChecked")) = True AndAlso strAppEmpIDs.Split(CChar(",")).Contains(p.Item("employeeunkid").ToString) AndAlso (CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH OrElse CInt(p.Item("paymentmode").ToString) = enPaymentMode.CASH_AND_CHEQUE) Select CDec(p.Item("expaidamt_tag").ToString)).Sum()

                        If (mintBankPayment + mintCashPayment) <= 0 Then Continue For
                        'Sohail (26 Apr 2017) -- End
                        'Sohail (10 Mar 2014) -- End

                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname, Session("Firstname").ToString() & " " & Session("Surname").ToString(), cboPayPeriod.SelectedItem.Text, mintBankPayment, mintCashPayment, (mintBankPayment + mintCashPayment), mdecBankPayment, mdecCashPayment, (mdecBankPayment + mdecCashPayment), cboCurrency.SelectedItem.Text, txtRemarks.Text.Trim)
                        'Sohail (10 Mar 2014) -- Start
                        'Enhancement - Send Email Notification to lower levels when payment disapproved.
                        'If dicNotification.ContainsKey(objUsr._Email) = False Then
                        '    dicNotification.Add(objUsr._Email, StrMessage)
                        'End If
                        If CBool(Session("IsAuthorized")) = True Then
                            gobjEmailList.Add(New clsEmailCollection(objUsr._Email, "Notification of Payslip Payment Authorized.", StrMessage, "frmAuthorizeUnauthorizePayment", 0, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, objUsr._Email))
                        Else
                            gobjEmailList.Add(New clsEmailCollection(objUsr._Email, "Notification of Payslip Payment Authorization Voided.", StrMessage, "frmAuthorizeUnauthorizePayment", 0, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, objUsr._Email))
                        End If
                        'Sohail (10 Mar 2014) -- End
                    Next
                    objUsr = Nothing
                    'Me.ViewState.Add("Notification", dicNotification) 'Sohail (10 Mar 2014)

                    'Sohail (17 Dec 2014) -- Start
                    'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment.
                    'trd = New Thread(AddressOf Send_Notification)
                    'trd.IsBackground = True
                    'trd.Start()
                    Call Send_Notification()
                    'Sohail (17 Dec 2014) -- End

                End If

                cboPayPeriod.SelectedValue = "0"
                FillList()
                txtRemarks.Text = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (04 Sep 2020) -- Start
            'Bug : Application Performance issue
        Finally
            objPaymentTran = Nothing
            'Hemant (04 Sep 2020) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("PaymentIds") = Nothing
            Me.ViewState("DicCurrency") = Nothing
            Me.ViewState("CurrencySign") = Nothing
            Me.ViewState("CurrencyID") = Nothing
            Me.ViewState("BankPaymentCount") = Nothing
            Me.ViewState("BankPayment") = Nothing
            Me.ViewState("CashPaymentCount") = Nothing
            Me.ViewState("CashPayment") = Nothing
            Me.ViewState("Notification") = Nothing
            'Nilay (10-Feb-2016) -- Start
            'Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Me.ViewState("PaymentIds") = Nothing
    '        Me.ViewState("DicCurrency") = Nothing
    '        Me.ViewState("CurrencySign") = Nothing
    '        Me.ViewState("CurrencyID") = Nothing
    '        Me.ViewState("BankPaymentCount") = Nothing
    '        Me.ViewState("BankPayment") = Nothing
    '        Me.ViewState("CashPaymentCount") = Nothing
    '        Me.ViewState("CashPayment") = Nothing
    '        Me.ViewState("Notification") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

    'Sohail (18 Feb 2014) -- Start
    'Enhancement - AGKN
#Region " Other Controls Events "
    Protected Sub objbtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub objbtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            If cboPmtVoucher.Items.Count > 0 Then cboPmtVoucher.SelectedIndex = 0
            mstrAdvanceFilter = ""
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAdvanceFilter_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Hemant (29 Dec 2018) -- Start
    'Enhancement - On payroll authorizattion screen, provide a link to payroll variance report. When user clicks on the link, he/she to view the payroll variance report of the month that has been processed in 76.1.
    Protected Sub lnkPayrollVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollVarianceReport.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect("Rpt_PayrollVariance.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkPayrollVarianceReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Hemant (29 Dec 2018) -- End

    'Hemant (17 Oct 2019) -- Start
    Protected Sub lnkPayrollTotalVarianceReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollTotalVarianceReport.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect("Rpt_Payroll_Total_Variance.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayMessage("lnkPayrollTotalVarianceReport_Click:- " & ex.Message, Me)
        End Try
    End Sub
    'Hemant (17 Oct 2019) -- End


    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Sohail (18 Feb 2014) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 

            Me.gbAmountInfo.Text = Language._Object.getCaption(Me.gbAmountInfo.ID, Me.gbAmountInfo.Text)
            Me.lblTotalAmount.Text = Language._Object.getCaption(Me.lblTotalAmount.ID, Me.lblTotalAmount.Text)
            Me.gbPaymentInfo.Text = Language._Object.getCaption(Me.gbPaymentInfo.ID, Me.gbPaymentInfo.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.btnProcess.Text = Language._Object.getCaption(Me.btnProcess.ID, Me.btnProcess.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.gbPaymentModeInfo.Text = Language._Object.getCaption(Me.gbPaymentModeInfo.ID, Me.gbPaymentModeInfo.Text)
            Me.lblCashPaid.Text = Language._Object.getCaption(Me.lblCashPaid.ID, Me.lblCashPaid.Text)
            Me.lblbankPaid.Text = Language._Object.getCaption(Me.lblbankPaid.ID, Me.lblbankPaid.Text)
            Me.lblTotalPaid.Text = Language._Object.getCaption(Me.lblTotalPaid.ID, Me.lblTotalPaid.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Me.lblPmtVoucher.Text = Language._Object.getCaption(Me.lblPmtVoucher.ID, Me.lblPmtVoucher.Text)

            Me.GvAuthorizedPayment.Columns(1).HeaderText = Language._Object.getCaption(Me.GvAuthorizedPayment.Columns(1).FooterText, Me.GvAuthorizedPayment.Columns(1).HeaderText)
            Me.GvAuthorizedPayment.Columns(2).HeaderText = Language._Object.getCaption(Me.GvAuthorizedPayment.Columns(2).FooterText, Me.GvAuthorizedPayment.Columns(2).HeaderText)
            Me.GvAuthorizedPayment.Columns(3).HeaderText = Language._Object.getCaption(Me.GvAuthorizedPayment.Columns(3).FooterText, Me.GvAuthorizedPayment.Columns(3).HeaderText)
            Me.GvAuthorizedPayment.Columns(4).HeaderText = Language._Object.getCaption(Me.GvAuthorizedPayment.Columns(4).FooterText, Me.GvAuthorizedPayment.Columns(4).HeaderText)



        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Anjan [04 June 2014] -- End


End Class
