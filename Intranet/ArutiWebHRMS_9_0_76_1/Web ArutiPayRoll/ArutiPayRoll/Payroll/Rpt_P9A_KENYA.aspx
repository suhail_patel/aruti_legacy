﻿<%@ Page Title="P9A Report" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_P9A_KENYA.aspx.vb" Inherits="Payroll_Rpt_P9A_KENYA" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 65%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="P9A Report"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <asp:UpdatePanel ID="updt_pnl" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <table id="tblP9A" runat="server" style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                    </td>
                                                    <td style="width: 81%" colspan="4">
                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblFromPeriod" runat="server" Text="From Period"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboFromPeriod" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 18%">
                                                        <asp:Label ID="lblToPeriod" runat="server" Text="To Period"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboToPeriod" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblQuarterValue" runat="server" Text="Quarter Value"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboQuarterValue" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 18%">
                                                        <asp:Label ID="lblE1" runat="server" Text="E1"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboE1" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblE2" runat="server" Text="E2"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboE2" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 18%">
                                                        <asp:Label ID="lblE3" runat="server" Text="E3"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboE3" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblAmountOfInterest" runat="server" Text="Amount Of Interest"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboAmountOfInterest" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 18%">
                                                        <asp:Label ID="lblTaxCharged" runat="server" Text="Tax Charged"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboTaxCharged" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblPersonalRelief" runat="server" Text="Personal Relief"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboPersonalRelief" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 18%">
                                                        <asp:Label ID="lblMembership" runat="server" Text="Membership"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboMembership" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblInsuranceRelief" runat="server" Text="Insurance Relief"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboInsuranceRelief" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="width: 3%">
                                                    </td>
                                                    <td style="width: 18%">
                                                        <asp:Label ID="lblTaxableIcome" runat="server" Text="Taxable Icome"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <asp:DropDownList ID="cboTaxableIcome" runat="server" AutoPostBack="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblBasicSalaryFormula" runat="server" Text="Basic Salary Formula"></asp:Label>
                                                    </td>
                                                    <td style="width: 81%" colspan="4">
                                                        <asp:TextBox ID="txtBasicSalaryFormula" runat="server" >
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 19%">
                                                        <asp:Label ID="lblTaxChargedFormula" runat="server" Text="Tax Charged Formula"></asp:Label>
                                                    </td>
                                                    <td style="width: 81%" colspan="4">
                                                        <asp:TextBox ID="txtTaxChargedFormula" runat="server" >
                                                        </asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
