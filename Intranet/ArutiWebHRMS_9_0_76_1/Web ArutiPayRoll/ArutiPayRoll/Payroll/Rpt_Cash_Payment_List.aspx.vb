﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region


Partial Class Payroll_Rpt_Cash_Payment_List
    Inherits Basepage


#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private objCashPaymentList As clsCashPaymentList

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmCashPaymentList"
    'Anjan [04 June 2014 ] -- End


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

			'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (29 Jan 2016) -- End
			
            objCashPaymentList = New clsCashPaymentList(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            SetLanguage()
            'Anjan [04 June 2014 ] -- End

            Dim mintCurrPeriodID As Integer = 0
            If Not IsPostBack Then
                FillCombo()
                ResetValue()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objExRate As New clsExchangeRate
        Dim objEmp As New clsEmployee_Master
        Dim objBranch As New clsStation
        Dim objPeriod As New clscommom_period_Tran
        Dim objCurrentPeriodId As clsMasterData
        Dim mintCurrentPeriodId As Integer = 0

        Try
            objEmp = New clsEmployee_Master
            objPeriod = New clscommom_period_Tran
            objCurrentPeriodId = New clsMasterData


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
            '                                Session("UserId"), _
            '                                Session("Fin_year"), _
            '                                Session("CompanyUnkId"), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                Session("UserAccessModeSetting"), True, _
            '                                Session("IsIncludeInactiveEmp"), "Emp", True, )

            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True, )
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            With cboEmployeeName
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (01 Feb 2016) -- Start
            'Enhancement - Show employee middle name as well for Cash Payment Report for KBC.
            'mintCurrentPeriodId = objCurrentPeriodId.getCurrentPeriodID(enModuleReference.Payroll, ConfigParameter._Object._CurrentDateAndTime, 1, Session("Fin_year"))
            mintCurrentPeriodId = objCurrentPeriodId.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1, , True)
            'Sohail (01 Feb 2016) -- End


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, , , , Session("Database_Name"))
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = CStr(mintCurrentPeriodId)
                Me.ViewState.Add("CurrentPeriodId", mintCurrentPeriodId)
            End With

            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objBranch = Nothing

            cboPayAdvance.Items.Clear()

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End

            cboPayAdvance.Items.Add(Language.getMessage(mstrModuleName, 4, "Net Pay"))
            cboPayAdvance.Items.Add(Language.getMessage(mstrModuleName, 5, "Advance"))
            cboPayAdvance.SelectedIndex = 0

            dsList = objExRate.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_name"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboReportMode
                .Items.Clear()
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                .Items.Add(Language.getMessage(mstrModuleName, 1, "DRAFT"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "AUTHORIZED"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
            dsList = Nothing
            objEmp = Nothing
            objPeriod = Nothing
            objCurrentPeriodId = Nothing
            objExRate = Nothing
        End Try

    End Sub

    Private Sub ResetValue()
        Try
            cboEmployeeName.SelectedValue = "0"
            cboPeriod.SelectedValue = CStr(Me.ViewState("CurrentPeriodId"))
            txtEmpcode.Text = ""
            txtAmount.Text = "0"
            txtAmountTo.Text = "0"
            chkShowSign.Checked = True
            objCashPaymentList.setDefaultOrderBy(1)
            cboPayAdvance.SelectedIndex = 0
            chkIncludeInactiveEmp.Checked = False
            cboBranch.SelectedValue = "0"
            cboCurrency.SelectedValue = "0"
            cboReportMode.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean

        Try
            objCashPaymentList.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objCashPaymentList._Period = CInt(cboPeriod.SelectedValue)
                objCashPaymentList._PeriodName = cboPeriod.SelectedItem.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objCashPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
                objCashPaymentList._EmpName = cboEmployeeName.SelectedItem.Text
            End If

            If Trim(txtEmpcode.Text) <> "" Then
                objCashPaymentList._EmpCode = txtEmpcode.Text
            End If

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objCashPaymentList._Amount = CDec(txtAmount.Text)
                objCashPaymentList._AmountTo = CDec(txtAmountTo.Text)
            End If

            If chkShowSign.Checked Then
                objCashPaymentList._IsShowSign = True
            Else
                objCashPaymentList._IsShowSign = False
            End If

            objCashPaymentList._NetPayAdvanceId = CInt(cboPayAdvance.SelectedIndex)
            objCashPaymentList._NetPayAdvance = cboPayAdvance.SelectedItem.Text
            objCashPaymentList._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked
            objCashPaymentList._BranchId = CInt(cboBranch.SelectedValue)
            objCashPaymentList._Branch_Name = cboBranch.SelectedItem.Text
            objCashPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objCashPaymentList._CurrencyName = cboCurrency.SelectedItem.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            objCashPaymentList._PeriodStartDate = objPeriod._Start_Date
            objCashPaymentList._PeriodEndDate = objPeriod._End_Date
            objCashPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            objCashPaymentList._ReportModeName = cboReportMode.SelectedItem.Text

            objCashPaymentList._UserUnkId = CInt(Session("UserId"))
            objCashPaymentList._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objCashPaymentList._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Function

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If CDec(cboPeriod.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Period is mandatory information.Please select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub
            objCashPaymentList.setDefaultOrderBy(1)
            'Nilay (10-Feb-2016) -- Start
            'objCashPaymentList.generateReport(0, enPrintAction.None, enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            objCashPaymentList.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                 objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, _
                                                 Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                                 0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Nilay (10-Feb-2016) -- End

            Session("objRpt") = objCashPaymentList._Rpt

            'Sohail (29 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Cash Payment Report for KBC.
            Select Case CInt(Session("PayslipTemplate"))
                Case 2 'TRA Format
                    objCashPaymentList._IsLogoCompanyInfo = False
                    objCashPaymentList._IsOnlyCompanyInfo = False
                    objCashPaymentList._IsOnlyLogo = True
                Case Else
                    Select Case CInt(Session("LogoOnPayslip"))
                        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                            objCashPaymentList._IsLogoCompanyInfo = True
                            objCashPaymentList._IsOnlyCompanyInfo = False
                            objCashPaymentList._IsOnlyLogo = False
                        Case enLogoOnPayslip.COMPANY_DETAILS
                            objCashPaymentList._IsLogoCompanyInfo = False
                            objCashPaymentList._IsOnlyCompanyInfo = True
                            objCashPaymentList._IsOnlyLogo = False
                        Case enLogoOnPayslip.LOGO
                            objCashPaymentList._IsLogoCompanyInfo = False
                            objCashPaymentList._IsOnlyCompanyInfo = False
                            objCashPaymentList._IsOnlyLogo = True
                    End Select
            End Select
            'Sohail (29 Jan 2016) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("CurrentPeriodId") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("CurrentPeriodId") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblEmpCode.Text = Language._Object.getCaption(Me.lblEmpCode.ID, Me.lblEmpCode.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.ID, Me.lblAmountTo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblType.Text = Language._Object.getCaption(Me.lblType.ID, Me.lblType.Text)
            Me.chkShowSign.Text = Language._Object.getCaption(Me.chkShowSign.ID, Me.chkShowSign.Text)
            Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.ID, Me.chkIncludeInactiveEmp.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblReportMode.Text = Language._Object.getCaption(Me.lblReportMode.ID, Me.lblReportMode.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014 ] -- End

End Class
