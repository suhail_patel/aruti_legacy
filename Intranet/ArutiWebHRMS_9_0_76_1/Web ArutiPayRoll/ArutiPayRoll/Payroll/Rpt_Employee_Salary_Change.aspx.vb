﻿Option Strict On 'Nilay (10-Feb-2016)
#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region


Partial Class Payroll_Rpt_Employee_Salary_Change
    Inherits Basepage

#Region " Private Variable "

    Dim DisplayMessage As New CommonCodes
    Private objESC As clsEmployee_Salary_Change_Report

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEmployee_Salary_Chnage_Report"
    'Anjan [04 June 2014 ] -- End

    'SHANI (18 NOV 2014) -- Start
    'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
    Private mintFirstOpenPeriod As Integer = 0
    'SHANI (18 NOV 2014) -- End

    'Nilay (10-Feb-2016) -- Start
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    'Nilay (10-Feb-2016) -- End

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Nilay (10-Feb-2016) -- Start
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            objESC = New clsEmployee_Salary_Change_Report(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            SetLanguage()
            'Anjan [04 June 2014 ] -- End

            If Not IsPostBack Then
                FillCombo()
                ResetValue()

                'Shani [ 18 NOV 2015 ] -- START
                'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
            Else
                mintFirstOpenPeriod = CInt(Me.ViewState("FirstOpenPeriod"))
                'Nilay (10-Feb-2016) -- Start
                mdtStartDate = CDate(Me.ViewState("StartDate"))
                mdtEndDate = CDate(Me.ViewState("EndDate"))
                'Nilay (10-Feb-2016) -- End

                'Shani [ 18 NOV 2014 ] -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Shani [ 18 NOV 2015 ] -- START
    'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("FirstOpenPeriod") Is Nothing Then
                Me.ViewState.Add("FirstOpenPeriod", mintFirstOpenPeriod)
            Else
                Me.ViewState("FirstOpenPeriod") = mintFirstOpenPeriod
            End If

            'Nilay (10-Feb-2016) -- Start
            Me.ViewState("StartDate") = mdtStartDate
            Me.ViewState("EndDate") = mdtEndDate
            'Nilay (10-Feb-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani [ 18 NOV 2014 ] -- END
#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmp As New clsEmployee_Master
        'SHANI (18 NOV 2014) -- Start
        'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        'SHANI (18 NOV 2014) -- End
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            True, "Emp", True)

            'Shani(24-Aug-2015) -- End
            With cboEmployeeName
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes

            Dim objCommon As New clsCommon_Master
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.SALINC_REASON, True, "SalReason")
            With cboReason
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("SalReason")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (06-May-2014) -- End

            'Shani (18 NOV 2014) -- Start
            'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
            '                                   CDate(Session("fin_startdate")), "Period", True)

            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, CStr(Session("Database_Name")), _
                                               CDate(Session("fin_startdate")), "Period", True)
            'Nilay (10-Feb-2016) -- End


            'Shani(20-Nov-2015) -- End

            With cboFromPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = CStr(mintFirstOpenPeriod)
            End With

            With cboToPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
                .SelectedValue = CStr(mintFirstOpenPeriod)
            End With
            Me.ViewState.Add("dtPeriod", dsList.Tables(0))
            'SHANI (18 NOV 2014) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            cboEmployeeName.SelectedValue = "0"
            txtAmount.Text = ""
            txtAmountTo.Text = ""
            txtIncrement.Text = ""
            txtIncrementTo.Text = ""
            txtNewScale.Text = ""
            txtNewScaleTo.Text = ""
            dtpDateFrom.SetDate = Nothing
            dtpDateTo.SetDate = Nothing
            'SHANI (18 NOV 2014) -- Start
            'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
            cboFromPeriod.SelectedValue = CStr(mintFirstOpenPeriod)
            cboToPeriod.SelectedValue = CStr(mintFirstOpenPeriod)
            'SHANI (18 NOV 2014) -- End

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            cboReason.SelectedValue = "0"
            'Pinkal (06-May-2014) -- End


            objESC.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function SetFilter() As Boolean
        'Shani (18 NOV 2014) -- Start
        'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
        Dim objdtPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim strPeriodIDs As String = ""
        Dim arrDatabaseName As New ArrayList
        Dim i As Integer = 0
        Dim intPrevYearID As Integer = 0
        'Shani (18 NOV 2014) -- END
        Try
            'Shani (18 NOV 2014) -- Start
            'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
            objdtPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboFromPeriod.SelectedValue)
            'Nilay (10-Feb-2016) -- Start
            'Dim dtStart As Date = objdtPeriod._Start_Date
            mdtStartDate = objdtPeriod._Start_Date
            'Nilay (10-Feb-2016) -- End
            objdtPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboToPeriod.SelectedValue)
            'Nilay (10-Feb-2016) -- Start
            'Dim dtEnd As Date = objdtPeriod._End_Date
            mdtEndDate = objdtPeriod._End_Date
            'Nilay (10-Feb-2016) -- End

            If CInt(cboFromPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select From Period."), Me)
                cboFromPeriod.Focus()
                Exit Function
            ElseIf CInt(cboToPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please Select To Period."), Me)
                cboToPeriod.Focus()
                Exit Function
            ElseIf cboFromPeriod.SelectedIndex > cboToPeriod.SelectedIndex Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, To Period should be greater than From Period."), Me)
                cboToPeriod.Focus()
                Exit Function
            Else
                If dtpDateFrom.IsNull = False Then
                    'Nilay (10-Feb-2016) -- Start
                    'If eZeeDate.convertDate(dtpDateFrom.GetDate) < eZeeDate.convertDate(dtStart) OrElse eZeeDate.convertDate(dtpDateFrom.GetDate) > eZeeDate.convertDate(dtEnd) Then
                    If eZeeDate.convertDate(dtpDateFrom.GetDate) < eZeeDate.convertDate(mdtStartDate) OrElse eZeeDate.convertDate(dtpDateFrom.GetDate) > eZeeDate.convertDate(mdtEndDate) Then
                        'Nilay (10-Feb-2016) -- End
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, From Date should be in between From Period start date and end date."), Me)
                        dtpDateFrom.Focus()
                        Exit Function
                    End If
                End If

                If dtpDateTo.IsNull = False Then
                    'Nilay (10-Feb-2016) -- Start
                    'If eZeeDate.convertDate(dtpDateTo.GetDate) < eZeeDate.convertDate(dtStart) OrElse eZeeDate.convertDate(dtpDateTo.GetDate) > eZeeDate.convertDate(dtEnd) Then
                    If eZeeDate.convertDate(dtpDateTo.GetDate) < eZeeDate.convertDate(mdtStartDate) OrElse eZeeDate.convertDate(dtpDateTo.GetDate) > eZeeDate.convertDate(mdtEndDate) Then
                        'Nilay (10-Feb-2016) -- End
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, To Date should be in between To Period start date and end date."), Me)
                        dtpDateTo.Focus()
                        Exit Function
                    End If
                End If
            End If
            'Shani (18 NOV 2014) -- END

            objESC._Employeeunkid = CInt(cboEmployeeName.SelectedValue)
            objESC._Emp_Name = cboEmployeeName.SelectedItem.Text

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objESC._Current_ScaleFrom = CDec(txtAmount.Text)
                objESC._Current_ScaleTo = CDec(txtAmountTo.Text)
            End If

            If Trim(txtIncrement.Text) <> "" And Trim(txtIncrementTo.Text) <> "" Then
                objESC._Increment_From = CDec(txtIncrement.Text)
                objESC._Increment_To = CDec(txtIncrementTo.Text)
            End If

            If Trim(txtNewScale.Text) <> "" And Trim(txtNewScaleTo.Text) <> "" Then
                objESC._NewScale_From = CDec(txtNewScale.Text)
                objESC._NewScale_To = CDec(txtNewScaleTo.Text)
            End If

            objESC._IncludeInactiveEmp = chkIncludeInactiveEmp.Checked

            'Shani (18 NOV 2014) -- Start
            'Enhancement - Provide selection for closed financial year period on Employee Salary Change Report.
            Dim dtpriod As DataTable = CType(Me.ViewState("dtPeriod"), DataTable)

            For Each dsRow As DataRow In dtpriod.Rows
                If i >= cboFromPeriod.SelectedIndex AndAlso i <= cboToPeriod.SelectedIndex Then
                    strPeriodIDs &= ", " & dsRow.Item("periodunkid").ToString
                    If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                        dsList = objCompany.GetFinancialYearList(CInt(Session("Companyunkid")), , "Database", CInt(dsRow.Item("yearunkid")))
                        If dsList.Tables("Database").Rows.Count > 0 Then
                            arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                        End If
                    End If
                    intPrevYearID = CInt(dsRow.Item("yearunkid"))
                End If
                i += 1
            Next
            If strPeriodIDs.Trim <> "" Then strPeriodIDs = strPeriodIDs.Substring(2)
            If arrDatabaseName.Count <= 0 Then Return False

            objESC._PeriodIDs = strPeriodIDs
            objESC._Arr_DatabaseName = arrDatabaseName
            objESC._CurrentDatabaseName = CStr(Session("Database_Name"))

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboFromPeriod.SelectedValue)
            objESC._PeriodStartDate = objPeriod._Start_Date

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboToPeriod.SelectedValue)
            objESC._PeriodEndDate = objPeriod._End_Date
            objPeriod = Nothing
            'Shani (18 NOV 2014) -- END


            If dtpDateFrom.IsNull = False Then
                objESC._IncrementDateFrom = dtpDateFrom.GetDate.Date
            End If

            If dtpDateTo.IsNull = False Then
                objESC._IncrementDateTo = dtpDateTo.GetDate.Date
            End If

            objESC._UserUnkId = CInt(Session("UserId"))
            objESC._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objESC._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))

            'Pinkal (06-May-2014) -- Start
            'Enhancement : TRA Changes
            objESC._ReasonId = CInt(cboReason.SelectedValue)
            objESC._Reason = cboReason.SelectedItem.Text
            'Pinkal (06-May-2014) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If Not SetFilter() Then Exit Sub
            objESC.setDefaultOrderBy(0)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Nilay (10-Feb-2016) -- Start
            'objESC.generateReport(0, enPrintAction.None, enExportAction.None)
            objESC.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                     mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True, _
                                     CStr(Session("ExportReportPath")), CBool(Session("OpenAfterExport")), _
                                     0, enPrintAction.None, enExportAction.None, CInt(Session("Base_CurrencyId")))
            'Nilay (10-Feb-2016) -- End

            Session("objRpt") = objESC._Rpt

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(24-Aug-2015) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReport_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblNewScale.Text = Language._Object.getCaption(Me.lblNewScale.ID, Me.lblNewScale.Text)
            Me.lblNewScaleTo.Text = Language._Object.getCaption(Me.lblNewScaleTo.ID, Me.lblNewScaleTo.Text)
            Me.lblIncrementTo.Text = Language._Object.getCaption(Me.lblIncrementTo.ID, Me.lblIncrementTo.Text)
            Me.lblIncrement.Text = Language._Object.getCaption(Me.lblIncrement.ID, Me.lblIncrement.Text)
            Me.chkIncludeInactiveEmp.Text = Language._Object.getCaption(Me.chkIncludeInactiveEmp.ID, Me.chkIncludeInactiveEmp.Text)
            Me.lblAmountTo.Text = Language._Object.getCaption(Me.lblAmountTo.ID, Me.lblAmountTo.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
            Me.lblDateFrom.Text = Language._Object.getCaption(Me.lblDateFrom.ID, Me.lblDateFrom.Text)
            Me.lblDateTo.Text = Language._Object.getCaption(Me.lblDateTo.ID, Me.lblDateTo.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.ID, Me.lblReason.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
End Class
