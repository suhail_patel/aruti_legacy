﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Payroll_rpt_PayrollSummary
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private objCompanyTotal As clsPayrollSummary
    Private intCurrPeriodID As Integer = 0
    Private mstrReport_GroupName As String = ""
    Private mdecEx_Rate As Decimal = 1
    Private mstrCurr_Sign As String = String.Empty

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPayrollSummary"
    'Anjan [04 June 2014 ] -- End

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Private mintLogoSetting As Integer = 1
    'Sohail (23 Sep 2016) -- End
    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
    Private dtHeads As DataTable
    Private mstrInfoHeadIDs As String = ""
    'Sohail (30 Dec 2019) -- End
#End Region

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
#Region " Private Enum "
    Private Enum enHeadTypeId
        IncludeInactiveEmployee = 1
        IgnoreZeroValueHeads = 2
        ShowEmployerContribution = 3
        ShowInformationalHeads = 4
        ShowAnalysisOnNewPage = 5
        LogoSettings = 6
        'Sohail (30 Dec 2019) -- Start
        'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
        InfoHeadIDs = 7
        'Sohail (30 Dec 2019) -- End
    End Enum
#End Region
    'Sohail (23 Sep 2016) -- End

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (21 Jan 2016) -- End

            objCompanyTotal = New clsPayrollSummary(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End

            If Not IsPostBack Then
                FillCombo()
                'Sohail (30 Dec 2019) -- Start
                'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                pnlInfoheads.Enabled = False
                'Sohail (30 Dec 2019) -- End
                ResetValue()
                'Sohail (30 Dec 2019) -- Start
                'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                Call FillList()
                'Sohail (30 Dec 2019) -- End
                'Sohail (09 Feb 2016) -- Start
                'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
                Me.ViewState.Add("ExchangeRate", mdecEx_Rate)
                Me.ViewState.Add("CurrencySign", mstrCurr_Sign)
                'Sohail (09 Feb 2016) -- End
            Else
                'Sohail (23 Sep 2016) -- Start
                'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
                mintLogoSetting = CInt(ViewState("mintLogoSetting"))
                'Sohail (23 Sep 2016) -- End
                'Sohail (30 Dec 2019) -- Start
                'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                dtHeads = CType(ViewState("dtHeads"), DataTable)
                mstrInfoHeadIDs = ViewState("mstrInfoHeadIDs").ToString
                'Sohail (30 Dec 2019) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mintLogoSetting", mintLogoSetting)
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            ViewState("dtHeads") = dtHeads
            ViewState("mstrInfoHeadIDs") = mstrInfoHeadIDs
            'Sohail (30 Dec 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (23 Sep 2016) -- End

#End Region

#Region "Public Function"

    Public Sub FillCombo()
        Try
            Dim objExRate As New clsExchangeRate
            Dim dsList As New DataSet


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True).Tables(0)
            cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True).Tables(0)
            'Shani(20-Nov-2015) -- End

            cboPeriod.DataValueField = "periodunkid"
            cboPeriod.DataTextField = "name"
            cboPeriod.SelectedValue = "0"
            cboPeriod.DataBind()

            dsList = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .SelectedValue = "0"
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            dsList = objTranHead.getComboList(Session("Database_Name").ToString, "List", False, enTranHeadType.Informational)

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            If mstrInfoHeadIDs.Trim <> "" Then
                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (mstrInfoHeadIDs.Split(CChar(",")).Contains(p.Item("tranheadunkid").ToString)) Select (p)).ToList
                For Each r As DataRow In lstRow
                    r.Item("IsChecked") = True
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            'Dim dtTable As New DataTable
            'Dim xCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            'Dim xCol1 As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            'dtTable.Columns.Add(xCol)
            'If marrDetailedSalaryBreakdownReportHeadsIds.Count > 0 Then

            '    Dim xFilter As String = String.Empty
            '    dtTable.Columns("IsChecked").DefaultValue = True
            '    For Each xVal As Integer In marrDetailedSalaryBreakdownReportHeadsIds
            '        xFilter = "" : xFilter = "prtranhead_master.tranheadunkid = '" & xVal & "' "
            '        dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, , , , , , xFilter)
            '        dtTable.Merge(dsList.Tables(0), True)
            '    Next

            '    xFilter = "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", CType(marrDetailedSalaryBreakdownReportHeadsIds.ToArray(Type.GetType("System.String")), String())) & ")"
            '    dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , xFilter)
            '    dsList.Tables(0).Columns.Add(xCol1)

            '    For Each xrow As DataRow In dsList.Tables(0).Rows
            '        xrow("IsChecked") = False
            '        xrow.AcceptChanges()
            '    Next

            '    dtTable.Merge(dsList.Tables(0), True)
            'Else
            '    dtTable.Columns("IsChecked").DefaultValue = False
            '    dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, CInt(cboTrnHeadType.SelectedValue))
            '    dtTable.Merge(dsList.Tables(0), True)
            'End If

            'Me.ViewState.Add("dtTranHead", dtTable)
            dtHeads = dsList.Tables(0)
            dtHeads.DefaultView.Sort = "IsChecked DESC, name "

            With dgHeads
                .DataSource = dtHeads.DefaultView
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (30 Dec 2019) -- End

    Public Sub ResetValue()
        Try

            Dim objMaster As New clsMasterData

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date)
            'Sohail (29 Jan 2016) -- Start
            'Enhancement - New Loan Status filter "In Progress with Loan Deducted" for Loan Report.
            'intCurrPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Today.Date, Session("Fin_year"))
            intCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            'Sohail (29 Jan 2016) -- End
            'Shani(20-Nov-2015) -- End

            cboPeriod.SelectedValue = CStr(intCurrPeriodID)
            objMaster = Nothing
            objCompanyTotal.setDefaultOrderBy(0)
            chkInactiveemp.Checked = False
            chkIgnorezeroHead.Checked = True
            lblExRate.Text = ""
            mdecEx_Rate = 1
            mstrCurr_Sign = String.Empty

            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            chkShowEmployersContribution.Checked = False
            chkShowInformational.Checked = False
            Call GetValue()
            'Sohail (23 Sep 2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (23 Sep 2016) -- Start
    'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            dsList = objUserDefRMode.GetList("List", enArutiReport.PayrollSummary)

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.IncludeInactiveEmployee
                            chkInactiveemp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.IgnoreZeroValueHeads
                            chkIgnorezeroHead.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowEmployerContribution
                            chkShowEmployersContribution.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.ShowInformationalHeads
                            chkShowInformational.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Sohail (30 Dec 2019) -- Start
                            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                            Call chkShowInformational_CheckedChanged(chkShowInformational, New System.EventArgs)
                            'Sohail (30 Dec 2019) -- End

                        Case enHeadTypeId.ShowAnalysisOnNewPage
                            chkShowEachAnalysisOnNewPage.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.LogoSettings

                            'If CInt(radLogoCompanyInfo.Tag) = CInt(dsRow.Item("transactionheadid")) Then
                            '    radLogoCompanyInfo.Checked = True
                            'ElseIf CInt(radCompanyDetails.Tag) = CInt(dsRow.Item("transactionheadid")) Then
                            '    radCompanyDetails.Checked = True
                            'ElseIf CInt(radLogo.Tag) = CInt(dsRow.Item("transactionheadid")) Then
                            '    radLogo.Checked = True
                            'Else
                            '    Select Case ConfigParameter._Object._LogoOnPayslip
                            '        Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                            '            radLogoCompanyInfo.Checked = True
                            '        Case enLogoOnPayslip.COMPANY_DETAILS
                            '            radCompanyDetails.Checked = True
                            '        Case enLogoOnPayslip.LOGO
                            '            radLogo.Checked = True
                            '    End Select
                            'End If

                            'Sohail (30 Dec 2019) -- Start
                            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
                        Case enHeadTypeId.InfoHeadIDs
                            mstrInfoHeadIDs = dsRow.Item("transactionheadid").ToString
                            'Sohail (30 Dec 2019) -- End
                    End Select
                Next
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (23 Sep 2016) -- End

    Public Function SetFilter() As Boolean
        Try
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            mstrInfoHeadIDs = ""
            'Sohail (30 Dec 2019) -- End

            If CInt(Me.ViewState("ExchangeRate")) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Function
            End If

            objCompanyTotal.SetDefaultValue()
            objCompanyTotal._PeriodId = CInt(cboPeriod.SelectedValue)
            objCompanyTotal._PeriodName = cboPeriod.SelectedItem.Text
            objCompanyTotal._IsActive = chkInactiveemp.Checked
            objCompanyTotal._IgnoreZeroHeads = chkIgnorezeroHead.Checked
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objCompanyTotal._PeriodStartDate = objPeriod._Start_Date
            objCompanyTotal._PeriodEndDate = objPeriod._End_Date
            objCompanyTotal._Ex_Rate = CDec(Me.ViewState("ExchangeRate"))
            objCompanyTotal._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            objCompanyTotal._ShowAnalysisOnNewPage = chkShowEachAnalysisOnNewPage.Checked
            objCompanyTotal._ShowEmployerContribution = chkShowEmployersContribution.Checked
            objCompanyTotal._ShowInformational = chkShowInformational.Checked
            'Sohail (23 Sep 2016) -- End
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            mstrInfoHeadIDs = String.Join(",", (From p In dtHeads Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("tranheadunkid").ToString)).ToArray)
            objCompanyTotal._InfoHeadIDs = mstrInfoHeadIDs
            'Sohail (30 Dec 2019) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region "Combobox Event"

    Protected Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, _
                                                                                                                cboPeriod.SelectedIndexChanged
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    Dim dsList As DataSet
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = CStr(dsList.Tables("ExRate").Rows(0)("currency_sign"))
                        lblExRate.Text = Language.getMessage(mstrModuleName, 3, "Exchange Rate :") & " " & CDbl(mdecEx_Rate) & " "
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                End If
            End If

            If Me.ViewState("ExchangeRate") Is Nothing Then
                Me.ViewState.Add("ExchangeRate", mdecEx_Rate)
            Else
                Me.ViewState("ExchangeRate") = mdecEx_Rate
            End If

            If Me.ViewState("ExchangeRate") Is Nothing Then
                Me.ViewState.Add("CurrencySign", mstrCurr_Sign)
            Else
                Me.ViewState("CurrencySign") = mstrCurr_Sign
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub BtnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReport.Click
        Try
            If CInt(cboPeriod.SelectedValue) = 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014] -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Sub
            End If
            If SetFilter() = False Then Exit Sub

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            'Sohail (23 Sep 2016) -- Start
            'MWAUWSA Enhancement -  63.1 - Show Employer Contribution and Show Informational Heads options on Payroll Summary.
            If mintLogoSetting > 0 Then
                Select Case mintLogoSetting
                    Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                        objCompanyTotal._IsLogoCompanyInfo = True
                        objCompanyTotal._IsOnlyCompanyInfo = False
                        objCompanyTotal._IsOnlyLogo = False
                    Case enLogoOnPayslip.COMPANY_DETAILS
                        objCompanyTotal._IsLogoCompanyInfo = False
                        objCompanyTotal._IsOnlyCompanyInfo = True
                        objCompanyTotal._IsOnlyLogo = False
                    Case enLogoOnPayslip.LOGO
                        objCompanyTotal._IsLogoCompanyInfo = False
                        objCompanyTotal._IsOnlyCompanyInfo = False
                        objCompanyTotal._IsOnlyLogo = True
                End Select
            Else
                'Sohail (23 Sep 2016) -- End
                Select Case CInt(Session("PayslipTemplate"))
                    Case 2 'TRA Format
                        objCompanyTotal._IsLogoCompanyInfo = False
                        objCompanyTotal._IsOnlyCompanyInfo = False
                        objCompanyTotal._IsOnlyLogo = True
                    Case Else
                        Select Case CInt(Session("LogoOnPayslip"))
                            Case enLogoOnPayslip.LOGO_AND_COMPANY_DETAILS
                                objCompanyTotal._IsLogoCompanyInfo = True
                                objCompanyTotal._IsOnlyCompanyInfo = False
                                objCompanyTotal._IsOnlyLogo = False
                            Case enLogoOnPayslip.COMPANY_DETAILS
                                objCompanyTotal._IsLogoCompanyInfo = False
                                objCompanyTotal._IsOnlyCompanyInfo = True
                                objCompanyTotal._IsOnlyLogo = False
                            Case enLogoOnPayslip.LOGO
                                objCompanyTotal._IsLogoCompanyInfo = False
                                objCompanyTotal._IsOnlyCompanyInfo = False
                                objCompanyTotal._IsOnlyLogo = True
                        End Select
                End Select
                'Sohail (21 Jan 2016) -- End
            End If 'Sohail (23 Sep 2016)

            objCompanyTotal._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objCompanyTotal._UserUnkId = CInt(Session("UserId"))
            objCompanyTotal._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objCompanyTotal._BaseCurrencyId = CInt(Session("Base_CurrencyId"))
            objCompanyTotal._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objCompanyTotal._UserUnkId = CInt(Session("UserId"))
            objCompanyTotal.setDefaultOrderBy(0)
            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'objCompanyTotal.generateReport(0, enPrintAction.None, Aruti.Data.enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            objCompanyTotal.generateReportNew(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            objPeriod._Start_Date, _
                                            objPeriod._End_Date, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            Session("ExportReportPath").ToString, _
                                            CBool(Session("OpenAfterExport")), _
                                            0, enPrintAction.None, Aruti.Data.enExportAction.None, _
                                            CInt(Session("Base_CurrencyId")))
            'Sohail (09 Feb 2016) -- End
            Session("objRpt") = objCompanyTotal._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(24-Aug-2015) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Sohail (30 Dec 2019) -- Start
    'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
#Region " CheckBox's Events "

    Protected Sub chkHeadSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkHeadSelectAll As CheckBox = TryCast(sender, CheckBox)
            'Dim dtTranHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            If chkHeadSelectAll Is Nothing Then Exit Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In dgHeads.Rows
                cb = CType(dgHeads.Rows(gvr.RowIndex).FindControl("chkHeadSelect"), CheckBox)
                cb.Checked = chkHeadSelectAll.Checked
            Next
            For Each gvr As DataRow In dtHeads.Rows
                gvr("IsChecked") = chkHeadSelectAll.Checked
            Next
            dtHeads.AcceptChanges()

            'marrDetailedSalaryBreakdownReportHeadsIds.Clear()
            'marrDetailedSalaryBreakdownReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeadSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = TryCast(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = dtHeads.Select("tranheadunkid = '" & CInt(dgHeads.DataKeys(gvRow.RowIndex).Value) & "'")
                If drRow.Length > 0 Then
                    drRow(0)("IsChecked") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If

            'marrDetailedSalaryBreakdownReportHeadsIds.Clear()
            'marrDetailedSalaryBreakdownReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkShowInformational_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInformational.CheckedChanged
        Try
            pnlInfoheads.Enabled = chkShowInformational.Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Textbox Events "

    Protected Sub txtSearchTranHeads_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTranHeads.TextChanged
        Try
            If dtHeads.Rows.Count <= 0 Then Exit Sub
            Dim dtView As DataView = dtHeads.DefaultView
            dtView.RowFilter = "Name LIKE '%" & txtSearchTranHeads.Text & "%' OR Code LIKE '%" & txtSearchTranHeads.Text & "%' "
            dtView.Sort = "IsChecked DESC, name "

            With dgHeads
                .DataSource = dtView
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Sohail (30 Dec 2019) -- End

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("ExchangeRate") = Nothing
    '        Me.ViewState("CurrencySign") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.chkIgnorezeroHead.Text = Language._Object.getCaption(Me.chkIgnorezeroHead.ID, Me.chkIgnorezeroHead.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.ID, Me.lblExRate.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            'Sohail (30 Dec 2019) -- Start
            'FINCA DRC Enhancement # 00003108: Payroll Summary Reconciliation - Add INCLUDE HEADS Option on Payroll Summary report for informational heads like we have on Earning and Deduction spreadsheet.
            Me.dgHeads.Columns(1).HeaderText = Language._Object.getCaption(Me.dgHeads.Columns(1).FooterText, Me.dgHeads.Columns(1).HeaderText)
            Me.dgHeads.Columns(2).HeaderText = Language._Object.getCaption(Me.dgHeads.Columns(2).FooterText, Me.dgHeads.Columns(2).HeaderText)
            'Sohail (30 Dec 2019) -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014 ] -- End


End Class
