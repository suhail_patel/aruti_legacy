﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region

Partial Class Payroll_Rpt_Payroll_Head_Count
    Inherits Basepage

#Region "Private Variables"

    Dim DisplayMessage As New CommonCodes
    Private objPayrollHeadCount As clsPayrollHeadCount

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPayrollHeadCount"
    'Anjan [04 June 2014 ] -- End

#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objPayrollHeadCount = New clsPayrollHeadCount(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            SetLanguage()
            'Anjan [04 June 2014 ] -- End

            Dim mintCurrPeriodID As Integer = 0
            If Not IsPostBack Then
                Dim objMaster As New clsMasterData
                mintCurrPeriodID = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
                Me.ViewState.Add("CurrentPeriodID", mintCurrPeriodID)
                FillCombo()
                ResetValue()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Public Function"

    Public Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True, , , , Session("Database_Name"))
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "Period", True)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()
                .SelectedValue = CStr(Me.ViewState("CurrentPeriodID"))
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboPeriod.SelectedValue = CStr(Me.ViewState("CurrentPeriodID"))
            chkInactiveemp.Checked = False
            objPayrollHeadCount.setDefaultOrderBy(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet
        Dim intPrevPeriodID As Integer
        Dim strBfDatabaseName As String
        Try

            If CInt(cboPeriod.SelectedValue) <= 0 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Function
            End If

            objPayrollHeadCount.SetDefaultValue()

            objPayrollHeadCount._PeriodId = CInt(cboPeriod.SelectedValue)
            objPayrollHeadCount._PeriodName = cboPeriod.SelectedItem.Text
            objPayrollHeadCount._IncludeInactiveEmployee = chkInactiveemp.Checked

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objPayrollHeadCount._PeriodStartDate = objPeriod._Start_Date
            objPayrollHeadCount._PeriodEndDate = objPeriod._End_Date

            If objPeriod._Start_Date = CDate(Session("fin_startdate")).Date OrElse objPeriod._Yearunkid <> CInt(Session("Fin_year")) Then 'If Selected Period is current Year's First Period OR is Previous year's period.
                Dim objTempPeriod As New clscommom_period_Tran
                Dim dtTable As DataTable

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objTempPeriod.GetList("Period", enModuleReference.Payroll, True, , , True)
                dsList = objTempPeriod.GetList("Period", enModuleReference.Payroll, CInt(Session("Fin_year")), CDate(Session("fin_startdate")), True, , , True)
                'Shani(20-Nov-2015) -- End

                dtTable = New DataView(dsList.Tables("Period"), "end_date < " & eZeeDate.convertDate(objPeriod._Start_Date) & " ", "end_date DESC", DataViewRowState.CurrentRows).ToTable

                If dtTable.Rows.Count > 0 Then

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date), , CInt(dtTable.Rows(0).Item("yearunkid")))
                    intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date), CInt(dtTable.Rows(0).Item("yearunkid")))
                    'Shani(20-Nov-2015) -- End

                End If

                objTempPeriod._Periodunkid(CStr(Session("Database_Name"))) = intPrevPeriodID
                dsList = objMaster.Get_Database_Year_List("Database", True, CInt(Session("Compcountryid")))
                dtTable = New DataView(dsList.Tables("Database"), "yearunkid = " & objTempPeriod._Yearunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    strBfDatabaseName = dtTable.Rows(0).Item("database_name").ToString
                Else
                    strBfDatabaseName = Session("Database_Name").ToString()
                End If
                objTempPeriod = Nothing
            Else

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                intPrevPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date), CInt(Session("Fin_year")))
                'Shani(20-Nov-2015) -- End

                strBfDatabaseName = Session("Database_Name").ToString()
            End If

            If intPrevPeriodID > 0 Then
                objPeriod = New clscommom_period_Tran
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intPrevPeriodID

                objPayrollHeadCount._PreviousPeriodId = intPrevPeriodID
                objPayrollHeadCount._BF_DatabaseName = strBfDatabaseName
                objPayrollHeadCount._PreviousPeriodStartDate = objPeriod._Start_Date
                objPayrollHeadCount._PreviousPeriodEndDate = objPeriod._End_Date
            Else
                objPayrollHeadCount._PreviousPeriodId = 0
                objPayrollHeadCount._BF_DatabaseName = strBfDatabaseName
                objPayrollHeadCount._PreviousPeriodStartDate = DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date)
                objPayrollHeadCount._PreviousPeriodEndDate = DateAndTime.DateAdd(DateInterval.Day, -1, objPeriod._Start_Date)
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub

            objPayrollHeadCount._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objPayrollHeadCount._UserUnkId = CInt(Session("UserId"))
            objPayrollHeadCount._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            objPayrollHeadCount._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objPayrollHeadCount._UserUnkId = CInt(Session("UserId"))
            objPayrollHeadCount.setDefaultOrderBy(0)
            'Nilay (10-Feb-2016) -- Start
            'objPayrollHeadCount.generateReport(0, enPrintAction.None, Aruti.Data.enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            objPayrollHeadCount.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                               objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, _
                                               Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                               0, enPrintAction.None, Aruti.Data.enExportAction.None)
            'Nilay (10-Feb-2016) -- End

            Session("objRpt") = objPayrollHeadCount._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(24-Aug-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("CurrentPeriodID") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End



#End Region


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            ' Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.chkInactiveemp.Text = Language._Object.getCaption(Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014 ] -- End


End Class
