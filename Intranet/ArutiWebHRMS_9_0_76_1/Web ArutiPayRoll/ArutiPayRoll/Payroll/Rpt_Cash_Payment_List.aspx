﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="Rpt_Cash_Payment_List.aspx.vb" Inherits="Payroll_Rpt_Cash_Payment_List"
    Title="Cash Payment List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script type="text/javascript">

      function onlyNumbers(txtBox, e) {
          //        var e = event || evt; // for trans-browser compatibility
          //        var charCode = e.which || e.keyCode;
          if (window.event)
              var charCode = window.event.keyCode;       // IE
          else
              var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

          if (charCode == 13)
              return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 45%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Cash Payment List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployeeName" runat="server" Text="Emp.Name"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboEmployeeName" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmpCode" runat="server" Text="Emp.Code"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtEmpcode" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboPeriod" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblAmountTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtAmountTo" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblType" runat="server" Text="NetPay/Adv."></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboPayAdvance" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20% ">
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboBranch" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Paid Currency"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboCurrency" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkShowSign" runat="server" Text="Show Signature"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblReportMode" runat="server" Text="Report Mode"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="cboReportMode" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee">
                                                </asp:CheckBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnReport" runat="server" CssClass="btndefault" Text="Report" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
