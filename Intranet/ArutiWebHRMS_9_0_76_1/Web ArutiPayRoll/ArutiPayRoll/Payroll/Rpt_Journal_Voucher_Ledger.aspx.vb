﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti
Imports ArutiReports
Imports Aruti.Data
Imports ArutiReport
Imports System.Data
Imports eZee.Common
#End Region


Partial Class Payroll_Rpt_Journal_Voucher_Ledger
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objMainLedger As clsJVLedgerReport

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmJVLedgerReport"
    'Anjan [04 June 2014 ] -- End
    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
    Private mdtLoanList As DataTable
    'Sohail (07 Mar 2020) -- End

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Private ReadOnly mstrModuleName1 As String = "frmPayrollJournalColumns"
    Private objUserDefRMode As New clsUserDef_ReportMode
    Private mstrPayrollJournalColumnsIds As String = String.Empty
    Private mblnShowColumnHeader As Boolean = False
    Private mintExportMode As Integer = 0
    Private mblnSaveAsTXT As Boolean = False
    Private mblnTabDelimiter As Boolean = False
    Private marrPayrollJournalColumnIds As New ArrayList
    Private dtPayrollJournalTable As New DataTable
    Private mblnIsCrystalReport As Boolean = False
    'Sohail (26 Mar 2020) -- End

    'Sohail (10 Apr 2020) -- Start
    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrAnalysis_OrderBy_GroupName As String = ""
    'Sohail (10 Apr 2020) -- End
    'Hemant (13 Jul 2020) -- Start
    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
    Private mstrDateFormat As String = String.Empty
    'Hemant (13 Jul 2020) -- End

#End Region

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
#Region " Private Enum "
    Private Enum enReportMode
        CUSTOM_COLUMNS = 0
        SHOW_REPORT_HEADER = 1
        EXPORT_MODE = 2
        SAVE_AS_TXT = 3
        TAB_DELIMITER = 4
        'Hemant (13 Jul 2020) -- Start
        'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
        DATE_FORMAT = 5
        'Hemant (13 Jul 2020) -- End
    End Enum
#End Region
    'Sohail (26 Mar 2020) -- End

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Nilay (10-Feb-2016) -- Start
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            Aruti.Data.User._Object._Userunkid = CInt(Session("UserId"))
            'Sohail (26 Mar 2020) -- End

            objMainLedger = New clsJVLedgerReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            SetLanguage()
            'Anjan [04 June 2014 ] -- End

            Dim mintCurrPeriodID As Integer = 0
            If Not IsPostBack Then
                FillCombo()
                ResetValue()
                chkShowGroupByCCGroup_CheckedChanged(New Object(), New EventArgs())

                If CInt(Session("AccountingSoftWare")) = enIntegration.SAP_JV Then
                    FillList()
                End If
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
                Call CreateLoanListTable()
                Call FillLoanScheme()
            Else
                mdtLoanList = CType(ViewState("mdtLoanList"), DataTable)
                'Sohail (07 Mar 2020) -- End
                'Sohail (26 Mar 2020) -- Start
                'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
                mblnIsCrystalReport = CBool(ViewState("mblnIsCrystalReport"))
                mstrPayrollJournalColumnsIds = Me.ViewState("mstrPayrollJournalColumnsIds").ToString
                marrPayrollJournalColumnIds = CType(Me.ViewState("marrPayrollJournalColumnIds"), ArrayList)
                dtPayrollJournalTable = CType(Me.ViewState("dtPayrollJournalTable"), DataTable)
                'Sohail (26 Mar 2020) -- End
                'Sohail (10 Apr 2020) -- Start
                'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                mstrAnalysis_OrderBy_GroupName = ViewState("mstrAnalysis_OrderBy_GroupName").ToString
                'Sohail (10 Apr 2020) -- End
                'Hemant (13 Jul 2020) -- Start
                'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                mstrDateFormat = CStr(Me.ViewState("mstrDateFormat"))
                'Hemant (13 Jul 2020) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mdtLoanList", mdtLoanList)
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            ViewState("mblnIsCrystalReport") = mblnIsCrystalReport
            ViewState("mstrPayrollJournalColumnsIds") = mstrPayrollJournalColumnsIds
            ViewState("marrPayrollJournalColumnIds") = marrPayrollJournalColumnIds
            ViewState("dtPayrollJournalTable") = dtPayrollJournalTable
            'Sohail (26 Mar 2020) -- End
            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            Me.ViewState.Add("mstrStringIds", mstrStringIds)
            Me.ViewState.Add("mstrStringName", mstrStringName)
            Me.ViewState.Add("mintViewIdx", mintViewIdx)
            Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
            Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
            Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
            Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
            Me.ViewState.Add("mstrAnalysis_OrderBy_GroupName", mstrAnalysis_OrderBy_GroupName)
            'Sohail (10 Apr 2020) -- End
            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            Me.ViewState("mstrDateFormat") = mstrDateFormat
            'Hemant (13 Jul 2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Mar 2020) -- End

#End Region

#Region " Public Functions "

    Public Sub FillCombo()

        Dim objMaster As New clsMasterData
        Try
            Dim objExRate As New clsExchangeRate

            With cboReportType
                .Items.Clear()
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                .Items.Add(Language.getMessage(mstrModuleName, 1, "Main Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 2, "General Staff Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 3, "Cost Center Ledger"))
                .Items.Add(Language.getMessage(mstrModuleName, 5, "Combined JV"))
                .SelectedIndex = 0
            End With

            cboReportType_SelectedIndexChanged(New Object(), New EventArgs())

            'Nilay (10-Feb-2016) -- Start
            'Dim mintFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, CInt(Session("Fin_year")))
            Dim mintFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "Period", True, 0).Tables(0)
            cboPeriod.DataSource = (New clscommom_period_Tran).getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), _
                                                                               CDate(Session("fin_startdate")), "Period", True, 0).Tables(0)
            'Shani(20-Nov-2015) -- End

            cboPeriod.DataValueField = "periodunkid"
            cboPeriod.DataTextField = "Name"
            cboPeriod.DataBind()
            cboPeriod.SelectedValue = CStr(mintFirstOpenPeriod)
            Me.ViewState.Add("FirstOpenPeriod", mintFirstOpenPeriod)

            Dim objBranch As New clsStation
            Dim dsList As New DataSet
            dsList = objBranch.getComboList("List", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            objBranch = Nothing : dsList.Dispose()
            Dim objCCGroup As New clspayrollgroup_master
            dsList = objCCGroup.getListForCombo(enPayrollGroupType.CostCenter, "CCGroup", True)
            With cboCCGroup
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("CCGroup")
                .DataBind()
                .SelectedValue = "0"
            End With
            objCCGroup = Nothing

            dsList = objExRate.getComboList("ExRate", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsList.Tables("ExRate")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (03 Mar 2014) -- Start
            'Enhancement - iScala2 JV for Aga Khan
            dsList = objMaster.GetComboListForCustomCCenter("CCenter")
            With cboCustomCCenter
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("CCenter")
                .DataBind()
            End With
            'Sohail (03 Mar 2014) -- End

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            Dim objHead As New clsTransactionHead
            dsList = objHead.getComboList(Session("Database_Name").ToString, "Head", True)
            With cboTranHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("Head")
                .SelectedValue = "0"
                .DataBind()
            End With
            objHead = Nothing 'Sohail (08 Jul 2017)
            'Sohail (02 Sep 2016) -- End

            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            Dim objCC As New clscostcenter_master
            dsList = objCC.getComboList("CC", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dsList.Tables("CC")
                .SelectedValue = "0"
                .DataBind()
            End With
            objCC = Nothing
            'Sohail (08 Jul 2017) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMaster = Nothing
        End Try
    End Sub

    Public Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet
        Try
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'dsList = objTranHead.getComboList("TranHead", False, enTranHeadType.EmployersStatutoryContributions)
            dsList = objTranHead.getComboList(Session("Database_Name").ToString, "TranHead", False, enTranHeadType.EmployersStatutoryContributions)
            'Sohail (03 Feb 2016) -- End

            If dsList IsNot Nothing Then
                If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                    Dim dc As New DataColumn
                    dc.ColumnName = "IsChecked"
                    dc.DataType = Type.GetType("System.Boolean")
                    dc.DefaultValue = False
                    dsList.Tables(0).Columns.Add(dc)
                End If

                If dsList.Tables(0).Rows.Count <= 0 Then
                    Dim drRow As DataRow = dsList.Tables(0).NewRow
                    drRow("IsChecked") = False
                    drRow("tranheadunkid") = 0
                    drRow("code") = ""
                    drRow("name") = ""
                    dsList.Tables(0).Rows.Add(drRow)
                End If

            End If

            gbFilterTBCJV.DataSource = dsList.Tables(0)
            gbFilterTBCJV.DataBind()

            Me.ViewState.Add("EmpContribPayableHead", dsList.Tables(0))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTranHead = Nothing
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ResetValue()
        Try
            cboPeriod.SelectedValue = CStr(Me.ViewState("FirstOpenPeriod"))
            txtDebitAmountFrom.Text = "0"
            txtDebitAmountTo.Text = "0"
            txtCreditAmountFrom.Text = "0"
            txtCreditAMountTo.Text = "0"

            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)
            chkShowSummaryBottom.Checked = False
            chkShowSummaryNewPage.Checked = False
            cboBranch.SelectedValue = "0"
            cboCurrency.SelectedValue = "0"

            chkShowGroupByCCGroup.Checked = False
            cboCCGroup.SelectedValue = "0"
            chkIncludeEmployerContribution.Checked = True

            'Sohail (03 Mar 2014) -- Start
            'Enhancement - iScala2 JV for Aga Khan
            cboCustomCCenter.SelectedValue = "0"
            chkShowColumnHeader.Checked = False
            'Sohail (03 Mar 2014) -- End

            chkIgnoreZero.Checked = True

            lblExRate.Text = ""
            Me.ViewState("Ex_Rate") = 1

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            cboReportType.SelectedIndex = 0
            Call cboReportType_SelectedIndexChanged(cboReportType, New System.EventArgs)
            Call chkShowGroupByCCGroup_CheckedChanged(chkShowGroupByCCGroup, New System.EventArgs)
            'Nilay (01-Feb-2015) -- End
            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            cboTranHead.SelectedValue = "0"
            'Sohail (02 Sep 2016) -- End
            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            cboCostCenter.SelectedValue = "0"
            'Sohail (08 Jul 2017) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function SetFilter() As Boolean
        Try

            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Return False
            End If

            If CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Function
            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._ShowSummaryAtBootom = chkShowSummaryBottom.Checked
            objMainLedger._ShowSummaryOnNextPage = chkShowSummaryNewPage.Checked
            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._ShowGroupByCCenerGroup = chkShowGroupByCCGroup.Checked
            objMainLedger._CCenterGroupId = CInt(cboCCGroup.SelectedValue)
            objMainLedger._CCenterGroup_Name = cboCCGroup.Text
            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (11 May 2018) -- Start
            'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
            objMainLedger._IncludeEmpCodeNameOnDebit = chkIncludeEmpCodeNameOnDebit.Checked
            'Sohail (11 May 2018) -- End

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date)) 'Sohail (08 Jul 2020)
            objPeriod = Nothing

            'Sohail (14 Jun 2013) -- Start
            'TRA - ENHANCEMENT
            Dim strIDs As String = ""
            Dim objTranHead As New clsTransactionHead
            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            If dtTable IsNot Nothing AndAlso gbFilterTBCJV.Visible = True Then
                Dim intCheckedCount As Integer = dtTable.Select("IsChecked=1").Length
                For Each dt As DataRow In dtTable.Rows
                    If intCheckedCount > 0 AndAlso CBool(dt("IsChecked")) = False Then
                        strIDs &= CInt(dt("tranheadunkid")) & "," & objTranHead.GetEmpContribPayableHeadID(CInt(dt("tranheadunkid"))) & ","
                    End If
                Next
                If strIDs.ToString().Length > 0 Then
                    strIDs = strIDs.ToString().Trim.Substring(0, strIDs.ToString().Trim.Length - 1)
                End If
            End If

            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs
            'Sohail (14 Jun 2013) -- End

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'objMainLedger._Analysis_Join = mstrAnalysis_Join 'Sohail (08 Jul 2020)
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            objMainLedger._PostingDate = dtpPostingDate.GetDate()
            'Hemant (13 Jul 2020) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
    Private Sub CreateLoanListTable()
        Try
            mdtLoanList = New DataTable
            With mdtLoanList
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillLoanScheme()
        Dim objLoanScheme As New clsLoan_Scheme
        Dim dsList As DataSet
        Try
            mdtLoanList.Rows.Clear()

            Dim mblnSchemeShowOnEss As Boolean = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then mblnSchemeShowOnEss = True
            dsList = objLoanScheme.getComboList(False, "Loan", -1, "", mblnSchemeShowOnEss)


            Dim dRow As DataRow

            For Each dsRow As DataRow In dsList.Tables("Loan").Rows
                dRow = mdtLoanList.NewRow

                dRow.Item("id") = CInt(dsRow.Item("loanschemeunkid"))
                dRow.Item("code") = dsRow.Item("Code").ToString
                dRow.Item("name") = dsRow.Item("name").ToString

                mdtLoanList.Rows.Add(dRow)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLoanScheme = Nothing

            If mdtLoanList.Rows.Count <= 0 Then
                Dim r As DataRow = mdtLoanList.NewRow

                r.Item(2) = "None" ' To Hide the row and display only Row Header
                r.Item(3) = ""

                mdtLoanList.Rows.Add(r)
            End If

            gvScheme.DataSource = mdtLoanList
            gvScheme.DataBind()
        End Try
    End Sub

    Private Sub CheckAllScheme(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In gvScheme.Rows
                
                If CInt(mdtLoanList.Rows(gvr.RowIndex).Item("id")) > 0 AndAlso mdtLoanList.Rows(gvr.RowIndex).Item("name").ToString <> "" Then
                    cb = CType(gvScheme.Rows(gvr.RowIndex).FindControl("objdgcolhCheck"), CheckBox)
                    cb.Checked = blnCheckAll

                    mdtLoanList.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If

            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub objchkSelectAllLoanScheme_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllScheme(chkSelectAll.Checked)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub objdgcolhCheck_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtLoanList.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked

                mdtLoanList.AcceptChanges()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Mar 2020) -- End

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Private Sub GetPayrollJournalValue()
        Dim dsList As DataSet
        Try
            marrPayrollJournalColumnIds.Clear()
            dsList = objUserDefRMode.GetList("List", enArutiReport.JournalVoucherLedger, CInt(cboExportMode.SelectedValue), CInt(cboReportType.SelectedIndex))
            If dsList.Tables("List").Rows.Count > 0 Then
                For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                    If i = enReportMode.CUSTOM_COLUMNS Then 'Column order
                        marrPayrollJournalColumnIds.AddRange(dsList.Tables("List").Rows(i).Item("transactionheadid").ToString.Split(CChar(",")))

                    ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                        chkShowColumnHeaderPayrollJournal.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))

                    ElseIf i = enReportMode.EXPORT_MODE Then
                        cboExportMode.SelectedValue = CInt(dsList.Tables("List").Rows(i).Item("transactionheadid")).ToString

                    ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT
                        chkSaveAsTXT.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))

                    ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER
                        chkTABDelimiter.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        'Hemant (13 Jul 2020) -- Start
                        'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                    ElseIf i = enReportMode.DATE_FORMAT Then 'DATE FORMAT
                        If dsList.Tables("List").Rows.Count > 5 Then
                            txtDateFormat.Text = CStr(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If
                        'Hemant (13 Jul 2020) -- End
                    End If
                Next
            Else
                chkShowColumnHeaderPayrollJournal.Checked = False
                chkSaveAsTXT.Checked = False
                chkTABDelimiter.Checked = False
            End If

            Call FillPayrollJournalList()

        Catch ex As Exception
            popup_PayrollJournal.Show()
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetPayrollJournalValue()
        Try
            objUserDefRMode._Reportmodeid = CInt(cboExportMode.SelectedValue)
            objUserDefRMode._Reporttypeid = CInt(cboReportType.SelectedIndex)
            objUserDefRMode._Reportunkid = enArutiReport.JournalVoucherLedger
        Catch ex As Exception
            popup_PayrollJournal.Show()
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillPayrollJournalList()
        Dim objMaster As New clsMasterData
        Dim dsTicked As DataSet = Nothing
        Dim dsUnticked As DataSet = Nothing
        Dim lvItem As ListViewItem
        Try
            Dim xColCheck As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            Dim xColName As New DataColumn("ID", System.Type.GetType("System.Int64"))
            Dim xColID As New DataColumn("Name", System.Type.GetType("System.String"))

            dtPayrollJournalTable.Clear()
            dtPayrollJournalTable.Columns.Clear()
            dtPayrollJournalTable.Columns.Add(xColCheck)
            dtPayrollJournalTable.Columns.Add(xColID)
            dtPayrollJournalTable.Columns.Add(xColName)

            Dim dicTicked As Dictionary(Of Integer, DataRow) = Nothing

            If marrPayrollJournalColumnIds.Count > 0 Then
                dsTicked = objMaster.GetComboListForPayrollJournalColumns("List", False, Not mblnIsCrystalReport, "ID IN (" & String.Join(",", CType(marrPayrollJournalColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
                dicTicked = (From p In dsTicked.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("Id")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW)

                dsUnticked = objMaster.GetComboListForPayrollJournalColumns("List", False, Not mblnIsCrystalReport, "ID NOT IN (" & String.Join(",", CType(marrPayrollJournalColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
            Else
                dsUnticked = objMaster.GetComboListForPayrollJournalColumns("List", False, Not mblnIsCrystalReport)
            End If

            If dsTicked IsNot Nothing Then

                Dim dtRow As DataRow = Nothing
                For Each id As Integer In CType(marrPayrollJournalColumnIds.Clone, ArrayList)
                    dtRow = dicTicked.Item(id)
                    Dim xRow As DataRow = dtPayrollJournalTable.NewRow
                    xRow("IsChecked") = True
                    xRow("ID") = dtRow.Item("Id")
                    xRow("Name") = dtRow.Item("NAME")
                    dtPayrollJournalTable.Rows.Add(xRow)
                Next

            End If

            If dsUnticked IsNot Nothing Then
                For Each dtRow As DataRow In dsUnticked.Tables(0).Rows
                    Dim xRow As DataRow = dtPayrollJournalTable.NewRow
                    If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report Then
                        If CInt(dtRow.Item("Id")) = enPayrollJournal_Columns.Debit OrElse CInt(dtRow.Item("Id")) = enPayrollJournal_Columns.Credit Then
                            xRow("IsChecked") = True
                        Else
                            xRow("IsChecked") = False
                        End If
                    Else
                        xRow("IsChecked") = False
                    End If
                    xRow("ID") = dtRow.Item("Id")
                    xRow("Name") = dtRow.Item("NAME")
                    dtPayrollJournalTable.Rows.Add(xRow)
                    lvItem = Nothing
                Next
            End If
            With lvPayrollJournalColumns
                .DataSource = dtPayrollJournalTable
                .DataBind()
            End With

        Catch ex As Exception
            popup_PayrollJournal.Show()
            DisplayMessage.DisplayError(ex, Me)
        Finally
            lvItem = Nothing
        End Try
    End Sub

    Private Function IsPayrollJournalValidate() As Boolean
        Try
            Select Case CInt(cboExportMode.SelectedValue)
                Case enPayrollJournal_Export_Mode.CSV, enPayrollJournal_Export_Mode.XLS, enPayrollJournal_Export_Mode.Crystal_Report
                    Dim xRow() As DataRow = dtPayrollJournalTable.Select("IsChecked=True")

                    If xRow.Length <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Please Select atleast one column to export Payroll Journal report."), Me)
                        lvPayrollJournalColumns.Focus()
                        popup_PayrollJournal.Show()
                        Return False
                    ElseIf CInt(cboExportMode.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Please Select Export Mode."), Me)
                        cboExportMode.Focus()
                        Return False
                    ElseIf CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report AndAlso xRow.Length < 4 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Please Select atleast minimum 4 columns to export Payroll Journal report."), Me)
                        lvPayrollJournalColumns.Focus()
                        Return False
                        'Hemant (13 Jul 2020) -- Start
                        'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                    ElseIf CBool(dtPayrollJournalTable.Select("Id = " & enPayrollJournal_Columns.Posting_Date & " ")(0).Item("isChecked")) = True AndAlso txtDateFormat.Text = "" Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Please Enter Date Format."), Me)
                        txtDateFormat.Focus()
                        popup_PayrollJournal.Show()
                        Return False
                        'Hemant (13 Jul 2020) -- End
                    End If
            End Select

            mstrPayrollJournalColumnsIds = String.Join(",", (From p In dtPayrollJournalTable Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("ID").ToString)).ToArray)
            mblnShowColumnHeader = chkShowColumnHeaderPayrollJournal.Checked
            mintExportMode = CInt(cboExportMode.SelectedValue)
            mblnSaveAsTXT = chkSaveAsTXT.Checked
            mblnTabDelimiter = chkTABDelimiter.Checked
            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            mstrDateFormat = txtDateFormat.Text
            'Hemant (13 Jul 2020) -- End
            Call SetPayrollJournalValue()

            Return True
        Catch ex As Exception
            popup_PayrollJournal.Show()
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Export_CSV_XLS()
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            If SetFilter() = False Then Exit Sub
            If IsPayrollJournalValidate() = False Then Exit Sub
            GUI.fmtCurrency = Session("fmtCurrency").ToString


            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = lnkPayrollJournalReport.Text.Replace("&", "").Replace(".", "")

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False

            Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            Aruti.Data.User._Object._Userunkid = CInt(Session("UserId"))


            Dim mstrPath As String = ""
            Dim strDilimiter As String = ""

            If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.CSV Then
                objMainLedger._OpenAfterExport = False


                If chkSaveAsTXT.Checked = True Then
                    mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\PayrollJournal.txt"
                Else
                    mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\PayrollJournal.csv"
                End If

                If chkTABDelimiter.Checked = True Then
                    strDilimiter = vbTab
                Else
                    strDilimiter = ","
                End If


            ElseIf CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.XLS Then
                objMainLedger._OpenAfterExport = False
                mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                
            End If

            If objMainLedger.Generate_PayrollJournalReport(Session("Database_Name").ToString, _
                                                                               CInt(Session("UserId")), _
                                                                               CInt(Session("Fin_year")), _
                                                                               CInt(Session("CompanyUnkId")), _
                                                                               objPeriod._Start_Date, _
                                                                               objPeriod._End_Date, _
                                                                               Session("UserAccessModeSetting").ToString, True, _
                                                                               CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                               Session("fmtCurrency").ToString, _
                                                                                CInt(Session("Base_CurrencyId")), _
                                                                               CStr(Session("UserName")), mstrPath, False, _
                                                                               CInt(cboExportMode.SelectedValue), _
                                                                               mstrPayrollJournalColumnsIds, _
                                                                               chkShowColumnHeaderPayrollJournal.Checked, _
                                                                               chkSaveAsTXT.Checked, _
                                                                               chkTABDelimiter.Checked, _
                                                                               mstrDateFormat) = True Then
                'Hemant (13 Jul 2020) -- [mstrDateFormat]

                If mstrPath.Trim <> "" Then
                    If objMainLedger._FileNameAfterExported.Trim <> "" Then
                        Session("ExFileName") = mstrPath & "\" & objMainLedger._FileNameAfterExported
                    Else
                        Session("ExFileName") = mstrPath
                    End If

                    Export.Show()
                Else
                    Session("objRpt") = objMainLedger._Rpt
                    'Response.Redirect("../Aruti Report Structure/Report.aspx")
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
                End If
                

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (26 Mar 2020) -- End

#End Region

#Region "Combobox Event"

    Private Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            'Sohail (26 Mar 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            lnkPayrollJournalReport.Visible = False
            lnkPayrollJournalExport.Visible = False
            If cboReportType.SelectedIndex = 3 Then
                lnkPayrollJournalReport.Visible = True
                lnkPayrollJournalExport.Visible = True
            End If
            'Sohail (26 Mar 2020) -- End

            If cboReportType.SelectedIndex = 0 OrElse cboReportType.SelectedIndex = 3 Then
                If chkShowSummaryBottom.Visible = True Then chkShowSummaryBottom.Checked = False
                If chkShowSummaryNewPage.Visible = True Then chkShowSummaryNewPage.Checked = False

                chkShowSummaryBottom.Visible = False
                chkShowSummaryNewPage.Visible = False
            Else
                chkShowSummaryBottom.Visible = True
                chkShowSummaryNewPage.Visible = True
            End If

            'Sohail (27 Jun 2014) -- Start
            'Enhancement - Enable Include Employer Contribution option for Staff, CC and Combined JV report for TANAPA. 
            'If cboReportType.SelectedIndex = 0 Then
            '    chkIncludeEmployerContribution.Checked = True
            '    chkIncludeEmployerContribution.Enabled = True
            'Else

            '    chkIncludeEmployerContribution.Checked = True
            '    chkIncludeEmployerContribution.Enabled = False
            'End If
            chkIncludeEmployerContribution.Checked = True
            'Sohail (27 Jun 2014) -- End

            'Sohail (02 May 2020) -- Start
            'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
            lblPostingDate.Visible = False
            dtpPostingDate.Visible = False
            'Sohail (02 May 2020) -- End

            'Sohail (10 Aug 2020) -- Start
            'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "Cargo Wise JV" for Freight Forwarders Kenya Ltd.
            lblCostCenter.Visible = False
            cboCostCenter.Visible = False
            'Sohail (10 Aug 2020) -- End

            'Sohail (11 May 2018) -- Start
            'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
            chkIncludeEmpCodeNameOnDebit.Visible = False
            'Sohail (11 May 2018) -- End

            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.Sun_Account Then
                lnkSunJVExport.Visible = True
            Else
                lnkSunJVExport.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.iScala Then
                lnkiScalaJVExport.Visible = True
            Else
                lnkiScalaJVExport.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.SAP_JV Then
                lnkTBCJVExport.Visible = True
                pnlTranHead.Visible = True
            Else
                lnkTBCJVExport.Visible = False
                pnlTranHead.Visible = False
            End If

            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.Sun_Account_5 Then
                lnkSunJV5Export.Visible = True
            Else
                lnkSunJV5Export.Visible = False
            End If

            'Sohail (03 Mar 2014) -- Start
            'Enhancement - iScala2 JV for Aga Khan
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.iScala_2 Then
                lnkiScala2JVExport.Visible = True

                lblCustomCCenter.Visible = True
                cboCustomCCenter.Visible = True
                lblInvoiceRef.Visible = True
                txtInvoiceRef.Visible = True
                chkShowColumnHeader.Visible = True
            Else
                lnkiScala2JVExport.Visible = False

                If cboCustomCCenter.Visible = True Then cboCustomCCenter.SelectedValue = "0"
                lblCustomCCenter.Visible = False
                cboCustomCCenter.Visible = False
                If txtInvoiceRef.Visible = True Then txtInvoiceRef.Text = ""
                lblInvoiceRef.Visible = False
                txtInvoiceRef.Visible = False
                If chkShowColumnHeader.Visible = True Then chkShowColumnHeader.Checked = False
                chkShowColumnHeader.Visible = False
            End If
            'Sohail (03 Mar 2014) -- End

            'Sohail (06 Dec 2014) -- Start
            'Enhancement - New JV integration with XERO accounting.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.XERO Then
                lnkXEROJVExport.Visible = True
            Else
                lnkXEROJVExport.Visible = False
            End If
            'Sohail (06 Dec 2014) -- End

            'Sohail (19 Feb 2016) -- Start
            'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.NETSUITE_ERP Then
                lnkNetSuiteERPJVExport.Visible = True
            Else
                lnkNetSuiteERPJVExport.Visible = False
            End If
            'Sohail (19 Feb 2016) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - New JV Integration with Flex Cube Retail.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.FLEX_CUBE_RETAIL Then
                lnkFlexCubeRetailJVExport.Visible = True
            Else
                lnkFlexCubeRetailJVExport.Visible = False
            End If
            'Sohail (06 Aug 2016) -- End

            'Sohail (02 Sep 2016) -- Start
            'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.SUN_ACCOUNT_PROJECT_JV Then
                lnkSunAccountProjectJVExport.Visible = True
                lblTranHead.Visible = True
                cboTranHead.Visible = True
            Else
                lnkSunAccountProjectJVExport.Visible = False
                lblTranHead.Visible = False
                cboTranHead.Visible = False
            End If
            'Sohail (02 Sep 2016) -- End

            'Sohail (08 Jul 2017) -- Start
            'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = enIntegration.DYNAMICS_NAV Then
                lnkDynamicsNavJVExport.Visible = True
                chkShowColumnHeader.Visible = True

                lblCostCenter.Visible = True
                cboCostCenter.Visible = True
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                'lblPostingDate.Enabled = True
                'dtpPostingDate.Enabled = True
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
                'Sohail (02 May 2020) -- End
            Else
                lnkDynamicsNavJVExport.Visible = False
                If chkShowColumnHeader.Checked = True Then chkShowColumnHeader.Checked = False
                chkShowColumnHeader.Visible = False
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                'dtpPostingDate.SetDate = Nothing
                'lblPostingDate.Enabled = False
                'dtpPostingDate.Enabled = False
                'Sohail (02 May 2020) -- End

                lblCostCenter.Visible = False
                cboCostCenter.Visible = False
            End If
            'Sohail (08 Jul 2017) -- End

            'Sohail (17 Feb 2018) -- Start
            'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.FLEX_CUBE_UPLD) Then
                lnkFlexCubeUPLDJVExport.Visible = True

                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                'lblPostingDate.Enabled = True
                'dtpPostingDate.Enabled = True
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
                'Sohail (02 May 2020) -- End
            Else
                lnkFlexCubeUPLDJVExport.Visible = False

                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                'dtpPostingDate.SetDate = Nothing
                'lblPostingDate.Enabled = False
                'dtpPostingDate.Enabled = False
                'Sohail (02 May 2020) -- End
            End If
            'Sohail (17 Feb 2018) -- End

            'Sohail (21 Feb 2018) -- Start
            'CCK Enhancement : Ref. No. 174 - SAP Salary Journal - (RefNo: 174) in 70.1.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.SAP_JV_BS_ONE) Then
                lnkSAPJVBSOneExport.Visible = True
                'Sohail (11 May 2018) -- Start
                'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
                chkIncludeEmpCodeNameOnDebit.Visible = True
                'Sohail (11 May 2018) -- End
            Else
                lnkSAPJVBSOneExport.Visible = False
            End If
            'Sohail (21 Feb 2018) -- End

            'Sohail (26 Nov 2018) -- Start
            'NMB Enhancement - Flex Cube JV Integration in 75.1.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.FLEX_CUBE_JV) Then
                lnkFlexCubeJVExport.Visible = True
                'Sohail (10 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
                lnkFlexCubeJVPostOracle.Visible = True
                'Sohail (10 Jan 2019) -- End
                'Sohail (16 Oct 2019) -- Start
                'NMB Enhancement # : Add privileges for "Allow To Post Flexcube JV To Oracle" to prevent erroneous postings by other users to Payroll GL accounts.
                lnkFlexCubeJVPostOracle.Enabled = CBool(Session("AllowToPostFlexcubeJVToOracle"))
                'Sohail (09 Oct 2019) -- End
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                lnkFlexCubeLoanBatchExport.Visible = True
                pnlLoanSchemeList.Visible = True
                'Sohail (07 Mar 2020) -- End
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
                'Sohail (02 May 2020) -- End
            Else
                lnkFlexCubeJVExport.Visible = False
                'Sohail (10 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
                lnkFlexCubeJVPostOracle.Visible = False
                'Sohail (10 Jan 2019) -- End
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
                lnkFlexCubeLoanBatchExport.Visible = False
                pnlLoanSchemeList.Visible = False
                'Sohail (07 Mar 2020) -- End
            End If
            'Sohail (26 Nov 2018) -- End

            'Sohail (02 Mar 2019) -- Start
            'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.BR_JV) Then
                lnkBRJVExport.Visible = True
                lnkBRJVPostSQL.Visible = True
            Else
                lnkBRJVExport.Visible = False
                lnkBRJVPostSQL.Visible = False
            End If
            'Sohail (02 Mar 2019) -- End

            'Hemant (15 Mar 2019) -- Start
            'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.SAP_ECC_6_0_JV) Then
                lnkSAPECC6_0JVExport.Visible = True
            Else
                lnkSAPECC6_0JVExport.Visible = False
            End If
            'Hemant (15 Mar 2019) -- End

            'Hemant (18 Apr 2019) -- Start
            'MWAUWASA ENHANCEMENT - Ref # 3747 - 76.1 : SAGE 300 JV integration.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.SAGE_300_JV) Then
                lnkSAGE300JVExport.Visible = True
            Else
                lnkSAGE300JVExport.Visible = False
            End If
            'Hemant (18 Apr 2019) -- End
            'Hemant (29 May 2019) -- Start
            'MUWASA ENHANCEMENT - Ref # 3329 - 76.1 : PASTEL V2 COMBINED JV INTEGRATION.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.PASTEL_V2_JV) Then
                lnkPASTELV2JVExport.Visible = True
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                'lblPostingDate.Enabled = True
                'dtpPostingDate.Enabled = True
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True
                'Sohail (02 May 2020) -- End
            Else
                lnkPASTELV2JVExport.Visible = False
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
                'lblPostingDate.Enabled = False
                'dtpPostingDate.Enabled = False
                'Sohail (02 May 2020) -- End
            End If
            'Hemant (29 May 2019) -- End

            'Sohail (08 Apr 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.SAGE_300_ERP_JV) Then
                lnkSAGE300ERPJVExport.Visible = True
				'Gajanan [15-April-2020] -- Start
                lnkSAGE300ERPImport.Visible = True
                'Gajanan [15-April-2020] -- End
            Else
                lnkSAGE300ERPJVExport.Visible = False
                'Gajanan [15-April-2020] -- Start
                lnkSAGE300ERPImport.Visible = False
                'Gajanan [15-April-2020] -- End
            End If
            'Sohail (08 Apr 2020) -- End

            'Sohail (22 Jun 2020) -- Start
            'CORAL BEACH CLUB enhancement : 0004743 : New JV Integration Coral Sun JV.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.CORAL_SUN_JV) Then
                lnkCoralSunJVExport.Visible = True
            Else
                lnkCoralSunJVExport.Visible = False
            End If
            'Sohail (22 Jun 2020) -- End

            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.QuickBook) Then
                lblPostingDate.Visible = True
                dtpPostingDate.Visible = True           
            End If
            'Hemant (13 Jul 2020) -- End

            'Sohail (10 Aug 2020) -- Start
            'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "Cargo Wise JV" for Freight Forwarders Kenya Ltd.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.CARGO_WISE_JV) Then
                lnkCargoWiseJVExport.Visible = True
                lblCostCenter.Visible = True
                cboCostCenter.Visible = True
            Else
                lnkCargoWiseJVExport.Visible = False
            End If
            'Sohail (10 Aug 2020) -- End

            'Sohail (04 Sep 2020) -- Start
            'Benchmark Enhancement : OLD-73 #  : New Accounting software Integration "SAP Standard JV".
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.SAP_STANDARD_JV) Then
                lnkSAPStandardJVExport.Visible = True
            Else
                lnkSAPStandardJVExport.Visible = False
            End If
            'Sohail (04 Sep 2020) -- End

            'Sohail (10 Sep 2020) -- Start
            'Oxygen Communication Ltd Enhancement : OLD-68 #  : New Accounting software Integration "SAGE Evolution JV".
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.SAGE_EVOLUTION_JV) Then
                lnkSAGEEvolutionJVExport.Visible = True
                lblCostCenter.Visible = True
                cboCostCenter.Visible = True
            Else
                lnkSAGEEvolutionJVExport.Visible = False
            End If
            'Sohail (10 Sep 2020) -- End

            'Sohail (16 Nov 2021) -- Start
            'Enhancement : OLD-492 : New Payroll JV Integration - Hakika Bank JV.
            If cboReportType.SelectedIndex = 3 AndAlso CInt(Session("AccountingSoftWare")) = CInt(enIntegration.HAKIKA_BANK_JV) Then
                lnkHakikaBankJVExport.Visible = True
            Else
                lnkHakikaBankJVExport.Visible = False
            End If
            'Sohail (16 Nov 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                        , cboPeriod.SelectedIndexChanged

        Dim mdecEx_Rate As Decimal = 0
        Dim mstrCurr_Sign As String = ""
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    Dim dsList As DataSet
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = CStr(dsList.Tables("ExRate").Rows(0)("currency_sign"))
                        lblExRate.Text = "Exchange Rate :" & " " & CDbl(mdecEx_Rate) & " "
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                End If
            End If
            Me.ViewState.Add("Ex_Rate", mdecEx_Rate)
            Me.ViewState.Add("CurrencySign", mstrCurr_Sign)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Protected Sub cboExportMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExportMode.SelectedIndexChanged
        Try
            chkShowColumnHeaderPayrollJournal.Visible = True
            If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.XLS OrElse CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report Then
                chkSaveAsTXT.Visible = False
                chkSaveAsTXT.Checked = False
                chkTABDelimiter.Visible = False
                chkTABDelimiter.Checked = False

                If CInt(cboExportMode.SelectedValue) = enPayrollJournal_Export_Mode.Crystal_Report Then
                    chkShowColumnHeaderPayrollJournal.Visible = False
                    chkShowColumnHeader.Checked = False
                End If
            Else
                chkSaveAsTXT.Visible = True
                chkTABDelimiter.Visible = True
            End If

            Call GetPayrollJournalValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_PayrollJournal.Show()
        End Try
    End Sub
    'Sohail (26 Mar 2020) -- End

#End Region

#Region "CheckBox Event"

    Protected Sub chkShowSummaryBottom_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowSummaryBottom.CheckedChanged
        Try
            If chkShowSummaryBottom.Checked = True Then chkShowSummaryNewPage.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkShowSummaryNewPage_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowSummaryNewPage.CheckedChanged
        Try
            If chkShowSummaryNewPage.Checked = True Then chkShowSummaryBottom.Checked = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkShowGroupByCCGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowGroupByCCGroup.CheckedChanged
        Try
            If chkShowGroupByCCGroup.Checked = True Then
                cboCCGroup.Enabled = True
            Else
                cboCCGroup.Enabled = False
                cboCCGroup.SelectedValue = "0"
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CheckAllEmployee(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            For Each gvr As GridViewRow In gbFilterTBCJV.Rows
                If CInt(dtTable.Rows(gvr.RowIndex).Item("tranheadunkid")) > 0 AndAlso dtTable.Rows(gvr.RowIndex).Item("name").ToString <> "" Then
                    cb = CType(gbFilterTBCJV.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                    cb.Checked = blnCheckAll
                    dtTable.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try
            Call CheckAllEmployee(chkSelectAll.Checked)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                dtTable.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If SetFilter() = False Then Exit Sub
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)
            'Nilay (10-Feb-2016) -- Start
            'objMainLedger.generateReport(0, enPrintAction.None, enExportAction.None)
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            objMainLedger.generateReportNew(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                            objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, _
                                            Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport")), _
                                            0, enPrintAction.None, enExportAction.None)
            'Nilay (10-Feb-2016) -- End

            Session("objRpt") = objMainLedger._Rpt
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Open New Window And Show Report Every Report
            'Response.Redirect("../Aruti Report Structure/Report.aspx")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Protected Sub btnPayrollJournalSaveSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayrollJournalSaveSelection.Click
        Try
            If IsPayrollJournalValidate() = False Then Exit Sub

            For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                objUserDefRMode = New clsUserDef_ReportMode
                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.JournalVoucherLedger
                objUserDefRMode._Reporttypeid = CInt(cboReportType.SelectedIndex)
                objUserDefRMode._Reportmodeid = CInt(cboExportMode.SelectedValue)

                If i = enReportMode.CUSTOM_COLUMNS Then 'Custom Columns


                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrPayrollJournalColumnsIds
                    intUnkid = objUserDefRMode.isExist(enArutiReport.JournalVoucherLedger, CInt(cboReportType.SelectedIndex), CInt(cboExportMode.SelectedValue), i)


                ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = chkShowColumnHeaderPayrollJournal.Checked.ToString
                    intUnkid = objUserDefRMode.isExist(enArutiReport.JournalVoucherLedger, CInt(cboReportType.SelectedIndex), CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.EXPORT_MODE Then

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = CInt(cboExportMode.SelectedValue).ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.JournalVoucherLedger, CInt(cboReportType.SelectedIndex), CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = chkSaveAsTXT.Checked.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.JournalVoucherLedger, CInt(cboReportType.SelectedIndex), CInt(cboExportMode.SelectedValue), i)

                ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = chkTABDelimiter.Checked.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.JournalVoucherLedger, CInt(cboReportType.SelectedIndex), CInt(cboExportMode.SelectedValue), i)

                    'Hemant (13 Jul 2020) -- Start
                    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                ElseIf i = enReportMode.DATE_FORMAT Then 'DATE FORMAT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrDateFormat.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.JournalVoucherLedger, CInt(cboReportType.SelectedIndex), CInt(cboExportMode.SelectedValue), i)
                    'Hemant (13 Jul 2020) -- End

                End If
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next

            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Information Successfully Saved."), Me)

            'Call Export_CSV_XLS()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_PayrollJournal.Show()
        End Try
    End Sub

    Protected Sub btnPayrollJournalOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayrollJournalOK.Click
        Try
            Call Export_CSV_XLS()
        Catch ex As Exception
            popup_PayrollJournal.Show()
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (26 Mar 2020) -- End

#End Region

#Region "LinkButton Event"

    Protected Sub lnkiScalaJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkiScalaJVExport.Click
        Try

            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else
            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "iScala"

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objMainLedger.iScala_JV_Export_CombinedJVReport(mstrPath) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objMainLedger.iScala_JV_Export_CombinedJVReport(Session("Database_Name"), _
            '                                                   Session("UserId"), _
            '                                                   Session("Fin_year"), _
            '                                                   Session("CompanyUnkId"), _
            '                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                   Session("UserAccessModeSetting"), True, _
            '                                                   Session("IsIncludeInactiveEmp"), True, _
            '                                                   Session("fmtCurrency"), _
            '                                                   Session("Base_CurrencyId"), _
            '                                                   Session("UserName"), mstrPath, _
            '                                                   Session("OpenAfterExport")) = True Then

            If objMainLedger.iScala_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               CStr(Session("UserAccessModeSetting")), True, _
                                                               CBool(Session("IsIncludeInactiveEmp")), True, _
                                                               CStr(Session("fmtCurrency")), _
                                                               CInt(Session("Base_CurrencyId")), _
                                                               CStr(Session("UserName")), mstrPath, _
                                                               CBool(Session("OpenAfterExport"))) = True Then

                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported
                    'Shani [ 10 DEC 2014 ] -- START
                    'Issue : Chrome is not supporting ShowModalDialog option now."
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                    'Shani [ 10 DEC 2014 ] -- END

                End If
            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (03 Mar 2014) -- Start
    'Enhancement - iScala2 JV for Aga Khan
    Protected Sub lnkiScala2JVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkiScala2JVExport.Click
        Try

            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCustomCCenter.SelectedIndex) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Please Select Custom Cost Center."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCustomCCenter.Focus()
                Exit Try
            Else
            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "iScala2"

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            objMainLedger._CustomCCetnterId = CInt(cboCustomCCenter.SelectedValue)
            objMainLedger._CustomCCetnterName = cboCustomCCenter.SelectedItem.Text
            objMainLedger._InvoiceReference = txtInvoiceRef.Text.Trim
            objMainLedger._ShowColumnHeader = chkShowColumnHeader.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objMainLedger.iScala2_JV_Export_CombinedJVReport(mstrPath) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objMainLedger.iScala2_JV_Export_CombinedJVReport(Session("Database_Name"), _
            '                                                    Session("UserId"), _
            '                                                    Session("Fin_year"), _
            '                                                    Session("CompanyUnkId"), _
            '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                    Session("UserAccessModeSetting"), True, _
            '                                                    Session("IsIncludeInactiveEmp"), True, _
            '                                                    Session("fmtCurrency"), _
            '                                                    Session("Base_CurrencyId"), _
            '                                                    Session("UserName"), mstrPath, _
            '                                                    Session("OpenAfterExport")) = True Then

            If objMainLedger.iScala2_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            objPeriod._Start_Date, _
                                                            objPeriod._End_Date, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            CBool(Session("IsIncludeInactiveEmp")), True, _
                                                            CStr(Session("fmtCurrency")), _
                                                            CInt(Session("Base_CurrencyId")), _
                                                            CStr(Session("UserName")), mstrPath, _
                                                            CBool(Session("OpenAfterExport"))) = True Then
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported
                    'Shani [ 10 DEC 2014 ] -- START
                    'Issue : Chrome is not supporting ShowModalDialog option now."
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                    'Shani [ 10 DEC 2014 ] -- END

                End If
            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (03 Mar 2014) -- End

    Protected Sub lnkSunJV5Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSunJV5Export.Click
        Try

            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "Sun_JV_5"

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objMainLedger.Sun5_JV_Export_CombinedJVReport(mstrPath) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objMainLedger.Sun5_JV_Export_CombinedJVReport(Session("Database_Name"), _
            '                                               Session("UserId"), _
            '                                               Session("Fin_year"), _
            '                                               Session("CompanyUnkId"), _
            '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                               Session("UserAccessModeSetting"), True, _
            '                                               Session("IsIncludeInactiveEmp"), True, _
            '                                               Session("fmtCurrency"), _
            '                                               Session("Base_CurrencyId"), _
            '                                               Session("UserName"), _
            '                                               Session("Accounting_TransactionReference"), mstrPath, _
            '                                               Session("OpenAfterExport")) = True Then

            If objMainLedger.Sun5_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                       CStr(Session("fmtCurrency")), _
                                                       CInt(Session("Base_CurrencyId")), _
                                                       CStr(Session("UserName")), _
                                                       CStr(Session("Accounting_TransactionReference")), mstrPath, _
                                                       CBool(Session("OpenAfterExport"))) = True Then
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported
                    'Shani [ 10 DEC 2014 ] -- START
                    ''Issue : Chrome is not supporting ShowModalDialog option now."
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                    'Shani [ 10 DEC 2014 ] -- END

                End If
            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSunJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSunJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Nilay (16-Apr-2016) -- Start
            'ENHANCEMENT - 59.1 - Allow YYYY01 period code format for July period for FY Jul to June for HERON Portico.
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode
            'Nilay (16-Apr-2016) -- End

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/Sun JV.txt"


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objMainLedger.Sun_JV_Export_CombinedJVReport(mstrPath) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objMainLedger.Sun_JV_Export_CombinedJVReport(Session("Database_Name"), _
            '                                                Session("UserId"), _
            '                                                Session("Fin_year"), _
            '                                                Session("CompanyUnkId"), _
            '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                Session("UserAccessModeSetting"), True, _
            '                                                Session("IsIncludeInactiveEmp"), True, _
            '                                                Session("fmtCurrency"), _
            '                                                Session("Base_CurrencyId"), _
            '                                                Session("UserName"), mstrPath, _
            '                                                Session("Accounting_TransactionReference"), _
            '                                                Session("Accounting_JournalType"), _
            '                                                Session("Accounting_JVGroupCode")) = True Then

            If objMainLedger.Sun_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        objPeriod._Start_Date, _
                                                        objPeriod._End_Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                        CStr(Session("fmtCurrency")), _
                                                        CInt(Session("Base_CurrencyId")), _
                                                        CStr(Session("UserName")), mstrPath, _
                                                        CStr(Session("Accounting_TransactionReference")), _
                                                        CStr(Session("Accounting_JournalType")), _
                                                        CStr(Session("Accounting_JVGroupCode"))) = True Then
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Shani [ 10 DEC 2014 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

                'Shani [ 10 DEC 2014 ] -- END

            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkTBCJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTBCJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)


            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim strIDs As String = ""
            Dim objTranHead As New clsTransactionHead
            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            If dtTable IsNot Nothing Then
                Dim intCheckedCount As Integer = dtTable.Select("IsChecked=1").Length
                For Each dt As DataRow In dtTable.Rows
                    If intCheckedCount > 0 AndAlso CBool(dt("IsChecked")) = False Then
                        strIDs &= CInt(dt("tranheadunkid")) & "," & objTranHead.GetEmpContribPayableHeadID(CInt(dt("tranheadunkid"))) & ","
                    End If
                Next
                If strIDs.ToString().Length > 0 Then
                    strIDs = strIDs.ToString().Trim.Substring(0, strIDs.ToString().Trim.Length - 1)
                End If
            End If

            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/SAP_JV.csv"

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objMainLedger.TBC_JV_Export_CombinedJVReport(mstrPath) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objMainLedger.TBC_JV_Export_CombinedJVReport(Session("Database_Name"), _
            '                                                Session("UserId"), _
            '                                                Session("Fin_year"), _
            '                                                Session("CompanyUnkId"), _
            '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                Session("UserAccessModeSetting"), True, _
            '                                                Session("IsIncludeInactiveEmp"), True, _
            '                                                Session("fmtCurrency"), _
            '                                                Session("Base_CurrencyId"), _
            '                                                Session("UserName"), _
            '                                                ConfigParameter._Object._CurrentDateAndTime, mstrPath) = True Then

            If objMainLedger.TBC_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        objPeriod._Start_Date, _
                                                        objPeriod._End_Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                        CStr(Session("fmtCurrency")), _
                                                        CInt(Session("Base_CurrencyId")), _
                                                        CStr(Session("UserName")), _
                                                        DateAndTime.Now.Date, mstrPath) = True Then
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Shani [ 10 DEC 2014 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

                'Shani [ 10 DEC 2014 ] -- END

            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkXEROJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkXEROJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)


            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim strIDs As String = ""
            Dim objTranHead As New clsTransactionHead
            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            If dtTable IsNot Nothing Then
                Dim intCheckedCount As Integer = dtTable.Select("IsChecked=1").Length
                For Each dt As DataRow In dtTable.Rows
                    If intCheckedCount > 0 AndAlso CBool(dt("IsChecked")) = False Then
                        strIDs &= CInt(dt("tranheadunkid")) & "," & objTranHead.GetEmpContribPayableHeadID(CInt(dt("tranheadunkid"))) & ","
                    End If
                Next
                If strIDs.ToString().Length > 0 Then
                    strIDs = strIDs.ToString().Trim.Substring(0, strIDs.ToString().Trim.Length - 1)
                End If
            End If

            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/XERO_JV.csv"


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objMainLedger.XERO_JV_Export_CombinedJVReport(mstrPath) = True Then

            'Nilay (10-Feb-2016) -- Start
            'If objMainLedger.XERO_JV_Export_CombinedJVReport(Session("Database_Name"), _
            '                                               Session("UserId"), _
            '                                               Session("Fin_year"), _
            '                                               Session("CompanyUnkId"), _
            '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                               Session("UserAccessModeSetting"), True, _
            '                                               Session("IsIncludeInactiveEmp"), True, _
            '                                               Session("fmtCurrency"), _
            '                                               Session("Base_CurrencyId"), _
            '                                               Session("UserName"), _
            '                                               mstrPath, _
            '                                               Session("Accounting_TransactionReference"), _
            '                                               Session("Accounting_JournalType")) = True Then

            If objMainLedger.XERO_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                       CStr(Session("fmtCurrency")), _
                                                       CInt(Session("Base_CurrencyId")), _
                                                       CStr(Session("UserName")), _
                                                       mstrPath, _
                                                       CStr(Session("Accounting_TransactionReference")), _
                                                       CStr(Session("Accounting_JournalType"))) = True Then
                'Nilay (10-Feb-2016) -- End

                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (19 Feb 2016) -- Start
    'Enhancement - New JV Integration with NetSuite ERP in 57.2 and 58.1 SP.
    Protected Sub lnkNetSuiteERPJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNetSuiteERPJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)


            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim strIDs As String = ""
            Dim objTranHead As New clsTransactionHead
            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            If dtTable IsNot Nothing Then
                Dim intCheckedCount As Integer = dtTable.Select("IsChecked=1").Length
                For Each dt As DataRow In dtTable.Rows
                    If intCheckedCount > 0 AndAlso CBool(dt("IsChecked")) = False Then
                        strIDs &= CInt(dt("tranheadunkid")) & "," & objTranHead.GetEmpContribPayableHeadID(CInt(dt("tranheadunkid"))) & ","
                    End If
                Next
                If strIDs.ToString().Length > 0 Then
                    strIDs = strIDs.ToString().Trim.Substring(0, strIDs.ToString().Trim.Length - 1)
                End If
            End If

            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_Country = Session("Accounting_Country").ToString

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/NetSuiteERP_JV.csv"

            If objMainLedger.NETSUITE_ERP_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                       CStr(Session("fmtCurrency")), _
                                                       CInt(Session("Base_CurrencyId")), _
                                                       CStr(Session("UserName")), _
                                                       mstrPath _
                                                       ) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (19 Feb 2016) -- End

    'Sohail (06 Aug 2016) -- Start
    'Enhancement - 63.1 - New JV Integration with Flex Cube Retail.
    Protected Sub lnkFlexCubeRetailJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFlexCubeRetailJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/FlexCubeRetailJV.txt"


            If objMainLedger.FlexCubeRetail_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        objPeriod._Start_Date, _
                                                                        objPeriod._End_Date, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                        CStr(Session("fmtCurrency")), _
                                                                        CInt(Session("Base_CurrencyId")), _
                                                                        CStr(Session("UserName")), mstrPath, _
                                                                        CStr(Session("Accounting_TransactionReference")), _
                                                                        CStr(Session("Accounting_JournalType")), _
                                                                        CStr(Session("Accounting_JVGroupCode"))) = True Then


                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (06 Aug 2016) -- End

    'Sohail (02 Sep 2016) -- Start
    'Enhancement -  64.1 - New Budget Redesign for Marie Stopes (New JV Report Sun Account Project JV).
    Protected Sub lnkSunAccountProjectJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSunAccountProjectJVExport.Click
        Dim objBudget As New clsBudget_MasterNew
        Dim dsList As DataSet
        Try

            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboTranHead.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Please Select Transaction head. Transaction head is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                cboTranHead.Focus()
                Exit Try
            Else

            End If

            dsList = objBudget.GetComboList("Budget", False, True)
            If dsList.Tables("Budget").Rows.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, There is no default budget set."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "Sun_JV_5"

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing 'Sohail (19 Feb 2016)

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            If objMainLedger.SunAccountProject_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        objPeriod._Start_Date, _
                                                        objPeriod._End_Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                        CStr(Session("fmtCurrency")), _
                                                        CInt(Session("Base_CurrencyId")), _
                                                        CStr(Session("UserName")), _
                                                        mstrPath, _
                                                        CBool(Session("OpenAfterExport")), _
                                                        CInt(dsList.Tables("Budget").Rows(0).Item("budgetunkid")), _
                                                        CInt(cboTranHead.SelectedValue) _
                                                        ) = True Then


                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End


                End If
            End If
            objPeriod = Nothing 'Sohail (19 Feb 2016)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (02 Sep 2016) -- End

    'Sohail (08 Jul 2017) -- Start
    'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
    Protected Sub lnkDynamicsNavJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDynamicsNavJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)


            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCostCenter.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCostCenter.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            objMainLedger._ShowColumnHeader = chkShowColumnHeader.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim strIDs As String = ""
            Dim objTranHead As New clsTransactionHead
            Dim dtTable As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)
            If dtTable IsNot Nothing Then
                Dim intCheckedCount As Integer = dtTable.Select("IsChecked=1").Length
                For Each dt As DataRow In dtTable.Rows
                    If intCheckedCount > 0 AndAlso CBool(dt("IsChecked")) = False Then
                        strIDs &= CInt(dt("tranheadunkid")) & "," & objTranHead.GetEmpContribPayableHeadID(CInt(dt("tranheadunkid"))) & ","
                    End If
                Next
                If strIDs.ToString().Length > 0 Then
                    strIDs = strIDs.ToString().Trim.Substring(0, strIDs.ToString().Trim.Length - 1)
                End If
            End If

            objMainLedger._EmpContributionHeadUnCheckedIDs = strIDs

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_Country = Session("Accounting_Country").ToString

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/DynamicsNav_JV.csv"

            If objMainLedger.Dynamics_Nav_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                       CStr(Session("fmtCurrency")), _
                                                       CInt(Session("Base_CurrencyId")), _
                                                       CStr(Session("UserName")), _
                                                       mstrPath, _
                                                       CInt(cboCostCenter.SelectedValue) _
                                                       ) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (08 Jul 2017) -- End

    'Sohail (17 Feb 2018) -- Start
    'AMANA Bank Enhancement : Ref. No. 172 - UPLOAD FILE FROM ARUTI TO FLEX CUBE - (RefNo: 171) in 70.1.
    Protected Sub lnkFlexCubeUPLDJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFlexCubeUPLDJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try

            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/FlexCubeUPLDJV.txt"


            If objMainLedger.FlexCubeUPLD_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        objPeriod._Start_Date, _
                                                                        objPeriod._End_Date, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                        CStr(Session("fmtCurrency")), _
                                                                        CInt(Session("Base_CurrencyId")), _
                                                                        CStr(Session("UserName")), mstrPath, _
                                                                        CStr(Session("Accounting_TransactionReference")), _
                                                                        CStr(Session("Accounting_JournalType")), _
                                                                        CStr(Session("Accounting_JVGroupCode"))) = True Then


                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (17 Feb 2018) -- End

    'Sohail (21 Feb 2018) -- Start
    'CCK Enhancement : Ref. No. 174 - SAP Salary Journal - (RefNo: 174) in 70.1.
    Protected Sub lnkSAPJVBSOneExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAPJVBSOneExport.Click
        Try

            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try
            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub
            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = "Sun_JV_5"

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            'Sohail (11 May 2018) -- Start
            'CCK Enhancement - Ref # 235 : Salary Journal – Having employee details for debit entries in 72.1.
            objMainLedger._IncludeEmpCodeNameOnDebit = chkIncludeEmpCodeNameOnDebit.Checked
            'Sohail (11 May 2018) -- End

            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            If objMainLedger.SAP_JV_BS_One_Report(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                       CStr(Session("fmtCurrency")), _
                                                       CInt(Session("Base_CurrencyId")), _
                                                       CStr(Session("UserName")), _
                                                        mstrPath, _
                                                       CBool(Session("OpenAfterExport"))) = True Then


                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End


                End If
            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (21 Feb 2018) -- End

    'Sohail (26 Nov 2018) -- Start
    'NMB Enhancement - Flex Cube JV Integration in 75.1.
    Protected Sub lnkFlexCubeJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFlexCubeJVExport.Click, lnkFlexCubeJVPostOracle.Click
        'Sohail (10 Jan 2019) - [lnkFlexCubeJVPostOracle.Click]
        'Sohail (13 Dec 2018) -- Start
        'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
        Dim mstrPath As String = ""
        'Sohail (13 Dec 2018) -- End
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try
                'Sohail (02 May 2020) -- Start
                'NMB Enhancement #: Posting date option to post VALUE_DATE in Flex cube JV.
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), Me)
                dtpPostingDate.Focus()
                Exit Try
                'Sohail (02 May 2020) -- End
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            'Sohail (10 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
            'mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/FlexCubeJV.csv"
            If CType(sender, LinkButton).ID = lnkFlexCubeJVExport.ID Then
                mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/FlexCubeJV.csv"
            Else
                mstrPath = ""
            End If
            'Sohail (10 Jan 2019) -- End
            'Sohail (13 Dec 2018) -- End

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            'If objPeriod._FlexCube_BatchNo.Trim <> "" Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & objPeriod._FlexCube_BatchNo & ".", Me)
            '    Exit Try
            'End If
            If CType(sender, LinkButton).ID = lnkFlexCubeJVPostOracle.ID Then
                Dim objPayroll As New clsPayrollProcessTran
                Dim objTnALeave As New clsTnALeaveTran
                Dim objPayment As New clsPayment_tran
                Dim objPayAuthorize As New clsPayment_authorize_tran
                Dim objJVPosting As New clsJVPosting_Tran
                Dim dsList As DataSet
                Dim intTotalPaymentCount As Integer = 0
                Dim intTotalAuthorizedtCount As Integer = 0
                Dim strAdvanceFilter As String = ""
                Dim strBatchNo As String = ""
                'Sohail (29 Aug 2019) -- Start
                'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
                If CBool(Session("AllowToPostJVOnHolidays")) = False Then
                    Dim objHoliday As New clsholiday_master
                    If objHoliday.isHoliday(ConfigParameter._Object._CurrentDateAndTime) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry, You cannot post JV not holiday."), Me)
                        Exit Try
                    End If
                End If
                'Sohail (29 Aug 2019) -- End
                'Sohail (10 Apr 2020) -- Start
                'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
                'If CInt(cboBranch.SelectedValue) > 0 Then
                '    dsList = objPayroll.Get_UnProcessed_Employee(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, " ADF.stationunkid <= 0 ")
                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        dsList.Tables(0).Columns.Remove("employeeunkid")
                '        dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                '        Language.setLanguage(mstrModuleName)
                '        popupValidationList.Message = Language.getMessage(mstrModuleName, 20, "Sorry, You cannot do branch wise posting. Reason: Branch is not assigned to some of the employees.")
                '        popupValidationList.Data_Table = dsList.Tables(0)
                '        popupValidationList.Show()
                '        Exit Try
                '    End If

                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, , " AND stationunkid <= 0 ") = True Then
                '        Language.setLanguage(mstrModuleName)
                '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, You cannot do branch wise posting. Reason: Non-Branch wise posting is already done for selected period."), Me)
                '        Exit Try
                '    End If

                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), CInt(cboBranch.SelectedValue), str_BatchNo:=strBatchNo) = True Then
                '        Language.setLanguage(mstrModuleName)
                '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, Posting is already done for selected branch and selected period with batch no") & " " & strBatchNo & ".", Me)
                '        Exit Try
                '    End If
                'Else
                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, , " AND stationunkid > 0 ") = True Then
                '        Language.setLanguage(mstrModuleName)
                '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Sorry, You cannot do posting without selecting any branch. Reason: Branch wise posting is already done for some branches for selected period."), Me)
                '        Exit Try
                '    End If

                '    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, str_BatchNo:=strBatchNo) = True Then
                '        Language.setLanguage(mstrModuleName)
                '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & strBatchNo & ".", Me)
                '        Exit Try
                '    End If
                'End If
                'If CInt(cboBranch.SelectedValue) > 0 Then
                '    strAdvanceFilter = " ADF.stationunkid = " & CInt(cboBranch.SelectedValue) & " "
                'End If
                'dsList = objPayroll.Get_UnProcessed_Employee(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, strAdvanceFilter)
                'If dsList.Tables(0).Rows.Count > 0 Then
                '    dsList.Tables(0).Columns.Remove("employeeunkid")
                '    dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                '    Language.setLanguage(mstrModuleName)
                '    popupValidationList.Message = Language.getMessage(mstrModuleName, 24, "Sorry, Process payroll is not done for the last date of the selected period for some of the employees.")
                '    popupValidationList.Data_Table = dsList.Tables(0)
                '    popupValidationList.Show()
                '    Exit Try
                'End If
                'dsList = objTnALeave.Get_Balance_List(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), True, "", "Balance", , CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                'If dsList.Tables("Balance").Select("balanceamount > 0").Length > 0 Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip."), Me)
                '    Exit Try
                'End If

                'intTotalPaymentCount = objPayment.GetTotalPaymentCount(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                'intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)

                'If intTotalAuthorizedtCount <> intTotalPaymentCount Then
                '    Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, Payment authorization is not for some of the employee(s). Please authorize payment for all employees."), Me)
                '    Exit Try
                'End If
                If mintViewIdx > 0 Then
                    dsList = objPayroll.Get_UnProcessed_Employee(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, " ADF." & mstrAnalysis_OrderBy.Split(CChar("."))(1) & " <= 0 ")
                    If dsList.Tables(0).Rows.Count > 0 Then
                        dsList.Tables(0).Columns.Remove("employeeunkid")
                        dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                        Language.setLanguage(mstrModuleName)
                        popupValidationList.Message = Language.getMessage(mstrModuleName, 33, "Sorry, You cannot do #allocation# wise posting. Reason: #allocation# is not assigned to some of the employees.").Replace("#allocation#", mstrReport_GroupName.Replace(":", ""))
                        popupValidationList.Data_Table = dsList.Tables(0)
                        popupValidationList.Show()
                        Exit Try
                    End If

                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, "", , " AND allocationbyid <> " & mintViewIdx & "  ") = True Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, You cannot do #allocation# wise posting. Reason: Non-#allocation# wise posting is already done for selected period.").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")), Me)
                        Exit Try
                    End If

                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), mintViewIdx, mstrStringIds, str_BatchNo:=strBatchNo) = True Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Posting is already done for selected #allocation# and selected period with batch no").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")) & " " & strBatchNo & ".", Me)
                        Exit Try
                    End If
                Else
                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, "", , " AND allocationbyid > 0 ") = True Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot do posting without selecting any allocation. Reason: allocation wise posting is already done for selected period.").Replace("#allocation#", mstrReport_GroupName.Replace(":", "")), Me)
                        Exit Try
                    End If

                    If objJVPosting.isExist(CInt(cboPeriod.SelectedValue), 0, "", str_BatchNo:=strBatchNo) = True Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & strBatchNo & ".", Me)
                        Exit Try
                    End If
                End If
                If mintViewIdx > 0 Then
                    strAdvanceFilter = " ADF." & mstrAnalysis_OrderBy.Split(CChar("."))(1) & " IN (" & mstrStringIds & ") "
                End If
                dsList = objPayroll.Get_UnProcessed_Employee(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, False, "Employee", CInt(cboPeriod.SelectedValue), True, False, strAdvanceFilter)
                If dsList.Tables(0).Rows.Count > 0 Then
                    dsList.Tables(0).Columns.Remove("employeeunkid")
                    dsList.Tables(0).Columns("employeename").SetOrdinal(0)
                    Language.setLanguage(mstrModuleName)
                    popupValidationList.Message = Language.getMessage(mstrModuleName, 24, "Sorry, Process payroll is not done for the last date of the selected period for some of the employees.")
                    popupValidationList.Data_Table = dsList.Tables(0)
                    popupValidationList.Show()
                    Exit Try
                End If
                dsList = objTnALeave.Get_Balance_List(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), True, "", "Balance", , CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                If dsList.Tables("Balance").Select("balanceamount > 0").Length > 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Some payslip's are still unpaid in this period. Please make payment for unpaid payslip."), Me)
                    Exit Try
                End If

                intTotalPaymentCount = objPayment.GetTotalPaymentCount(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)
                intTotalAuthorizedtCount = objPayAuthorize.GetAuthorizedTotalCount(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), False, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(cboPeriod.SelectedValue), , strAdvanceFilter)

                If intTotalAuthorizedtCount <> intTotalPaymentCount Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, Payment authorization is not for some of the employee(s). Please authorize payment for all employees."), Me)
                    Exit Try
                End If
                'Sohail (10 Apr 2020) -- End

                Language.setLanguage(mstrModuleName)
                popupSure.Message = Language.getMessage(mstrModuleName, 27, "Are you sure you want to post Flex Cube JV to Oracle?")
                popupSure.Show()
                Exit Try

                'objMainLedger._DontAllowToPostIfHeadUnMapped = True
                'objMainLedger._Loginemployeeunkid = 0
                'objMainLedger._Isweb = False
                'objMainLedger._FormName = mstrModuleName
                'objMainLedger._ClientIP = getIP()
                'objMainLedger._HostName = getHostName()
                'objMainLedger._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            End If
            'Sohail (24 Jun 2019) -- End

            'Sohail (24 Jun 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            ''Sohail (13 Dec 2018) -- Start
            ''NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            ''If objMainLedger.FlexCubeNMB_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
            ''                                                            CInt(Session("UserId")), _
            ''                                                            CInt(Session("Fin_year")), _
            ''                                                            CInt(Session("CompanyUnkId")), _
            ''                                                            objPeriod._Start_Date, _
            ''                                                            objPeriod._End_Date, _
            ''                                                            CStr(Session("UserAccessModeSetting")), True, _
            ''                                                            CBool(Session("IsIncludeInactiveEmp")), True, _
            ''                                                            CStr(Session("fmtCurrency")), _
            ''                                                            CInt(Session("Base_CurrencyId")), _
            ''                                                            CStr(Session("UserName")), _
            ''                                                            Session("OracleHostName").ToString, _
            ''                                                            Session("OraclePortNo").ToString, _
            ''                                                            Session("OracleServiceName").ToString, _
            ''                                                            Session("OracleUserName").ToString, _
            ''                                                            Session("OracleUserPassword").ToString _
            ''                                                           ) = True Then

            ''DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), Me)
            'If objMainLedger.FlexCubeNMB_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
            '                                                            CInt(Session("UserId")), _
            '                                                            CInt(Session("Fin_year")), _
            '                                                            CInt(Session("CompanyUnkId")), _
            '                                                            objPeriod._Start_Date, _
            '                                                            objPeriod._End_Date, _
            '                                                            CStr(Session("UserAccessModeSetting")), True, _
            '                                                            CBool(Session("IsIncludeInactiveEmp")), True, _
            '                                                            CStr(Session("fmtCurrency")), _
            '                                                            CInt(Session("Base_CurrencyId")), _
            '                                                            CStr(Session("UserName")), _
            '                                                            mstrPath, _
            '                                                            Session("OracleHostName").ToString, _
            '                                                            Session("OraclePortNo").ToString, _
            '                                                            Session("OracleServiceName").ToString, _
            '                                                            Session("OracleUserName").ToString, _
            '                                                            Session("OracleUserPassword").ToString _
            '                                                           ) = True Then

            '    'Sohail (10 Jan 2019) -- Start
            '    'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
            '    'Session("ExFileName") = mstrPath
            '    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
            '    If mstrPath.Trim <> "" Then
            '        Session("ExFileName") = mstrPath
            '        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
            '        Export.Show()
            '    Else
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), Me)
            '    End If
            '    'Sohail (10 Jan 2019) -- End
            '    'Sohail (13 Dec 2018) -- End

            'End If

            'objPeriod = Nothing
            Call ExportFlexCubeJV(CType(sender, LinkButton))
            'Sohail (24 Jun 2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement - Exporting Flex Cube NMB JV to CSV in 75.1.
            'Sohail (10 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Provide 2 different options for Flex Cube NMB JV to Export to CSV and Post to Oracle.
            'If ex.Message.ToUpper.Contains("ORACLE") = True Then
            '    Session("ExFileName") = mstrPath
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
            'Gajanan (3 Jan 2019) -- Start
            'Enhancement :Add Report Export Control 
            'Export.Show()

            'End If
            'Sohail (10 Jan 2019) -- End
            'Sohail (13 Dec 2018) -- End
        End Try
    End Sub
    'Sohail (26 Nov 2018) -- End

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : New report Flex Cube Loan Batch when Flexcube JV selected as Accounting Software on configuration.
    Protected Sub lnkFlexCubeLoanBatchExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFlexCubeLoanBatchExport.Click
        Dim mstrPath As String = ""
        Dim strLoanIDs As String = ""
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try

            Else
                strLoanIDs = String.Join(",", (From p In mdtLoanList.Select("IsChecked = 1") Select (p.Item("id").ToString)).ToArray)

                If strLoanIDs.Trim.Length <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Please Select atleast one loan scheme."), Me)
                    gvScheme.Focus()
                    Exit Try
                End If

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            
            mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/FlexCubeLoanBatch.csv"

            If objMainLedger.FlexCubeNMB_LoanDeduction_Export(CStr(Session("Database_Name")), _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                objPeriod._Start_Date, _
                                                                objPeriod._End_Date, _
                                                                CStr(Session("UserAccessModeSetting")), True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                CStr(Session("fmtCurrency")), _
                                                                CInt(Session("Base_CurrencyId")), _
                                                                CStr(Session("UserName")), _
                                                                mstrPath _
                                                                , strLoanIDs _
                                                               ) = True Then

                If mstrPath.Trim <> "" Then
                    Session("ExFileName") = mstrPath
                    Export.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), Me)
                End If
            End If

            objPeriod = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Mar 2020) -- End

    'Sohail (24 Jun 2019) -- Start
    'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
    Protected Sub popupSure_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupSure.buttonYes_Click
        Try

            Call ExportFlexCubeJV(lnkFlexCubeJVPostOracle)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ExportFlexCubeJV(ByVal sender As LinkButton)
        Dim mstrPath As String = ""
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False

            If CType(sender, LinkButton).ID = lnkFlexCubeJVPostOracle.ID Then
                objMainLedger._DontAllowToPostIfHeadUnMapped = True
                objMainLedger._Loginemployeeunkid = 0
                objMainLedger._Isweb = False
                objMainLedger._FormName = mstrModuleName
                objMainLedger._ClientIP = CStr(Session("IP_ADD"))
                objMainLedger._HostName = CStr(Session("HOST_NAME"))
                objMainLedger._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                'Sohail (07 Mar 2020) -- Start
                'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
                objMainLedger._FlexCubeJVPostedOracleNotificationUserIds = Session("FlexCubeJVPostedOracleNotificationUserIds").ToString
                objMainLedger._LoginModeId = enLogin_Mode.MGR_SELF_SERVICE
                'Sohail (07 Mar 2020) -- End

                mstrPath = ""
            Else
                mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/FlexCubeJV.csv"
            End If

            If objMainLedger.FlexCubeNMB_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        objPeriod._Start_Date, _
                                                                        objPeriod._End_Date, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                        CStr(Session("fmtCurrency")), _
                                                                        CInt(Session("Base_CurrencyId")), _
                                                                        CStr(Session("UserName")), _
                                                                        mstrPath, _
                                                                        Session("OracleHostName").ToString, _
                                                                        Session("OraclePortNo").ToString, _
                                                                        Session("OracleServiceName").ToString, _
                                                                        Session("OracleUserName").ToString, _
                                                                        Session("OracleUserPassword").ToString _
                                                                       ) = True Then

                If mstrPath.Trim <> "" Then
                    Session("ExFileName") = mstrPath
                    Export.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), Me)
                End If
                'Sohail (24 Jun 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - TC012: There should be payroll authorization check before payroll batch posting to flex cube.
            Else
                If objMainLedger._ShowCommonValidationList = True AndAlso objMainLedger._CommonValidationTable IsNot Nothing Then
                    Dim dtTable As DataTable = objMainLedger._CommonValidationTable
                    'Sohail (10 Jun 2020) -- Start
                    'NMB Enhancement # : Validate employee bank accout no with bank branch using view shared by nmb and if fails then show list of bank accounts with export option in Flexcube JV.
                    Dim strTitle As String = ""
                    If dtTable.Columns.Contains("branch_code") = True AndAlso dtTable.Columns.Contains("account_no") = True Then
                        strTitle = Language.getMessage(mstrModuleName, 37, "Sorry, Some employee bank account no. are not matched with branch code.")
                        dtTable.Columns("remark").SetOrdinal(0)
                        dtTable.Columns("account_no").SetOrdinal(1)
                        For i As Integer = 3 To dtTable.Columns.Count - 1
                            dtTable.Columns.RemoveAt(3)
                        Next
                    Else
                        strTitle = Language.getMessage(mstrModuleName, 28, "Sorry, Some transaction heads are not mapped with any account. Please map heads with account to post Flex Cube JV to Oracle.")
                        'Sohail (10 Jun 2020) -- End
                    dtTable.Columns("trnheadname").SetOrdinal(0)
                    dtTable.Columns("tranheadcode").SetOrdinal(1)
                    For i As Integer = 2 To dtTable.Columns.Count - 1
                        dtTable.Columns.RemoveAt(2)
                    Next
                    dtTable.Columns("trnheadname").ColumnName = Language.getMessage(mstrModuleName, 29, "Head Name")
                    dtTable.Columns("tranheadcode").ColumnName = Language.getMessage(mstrModuleName, 30, "Head Code")
                    End If 'Sohail (10 Jun 2020)
                    Language.setLanguage(mstrModuleName)
                    'Sohail (10 Jun 2020) -- Start
                    'NMB Enhancement # :Validate employee bank accout no with bank branch using view shared by nmb and if fails then show list of bank accounts with export option in Flexcube JV.
                    'popupValidationList.Message = Language.getMessage(mstrModuleName, 28, "Sorry, Some transaction heads are not mapped with any account. Please map heads with account to post Flex Cube JV to Oracle.")
                    popupValidationList.Message = strTitle
                    'Sohail (10 Jun 2020) -- End
                    popupValidationList.Data_Table = dtTable
                    popupValidationList.Show()
                    Exit Try
                End If
                'Sohail (24 Jun 2019) -- End
            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (24 Jun 2019) -- End

    'Sohail (02 Mar 2019) -- Start
    'Mkombozi Bank Enhancement - Ref # 0002673 - 76.1 - BR-CBS JV Accounting integration to export data to CBS SQL database in xml format.
    Protected Sub lnkBRJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBRJVExport.Click, lnkBRJVPostSQL.Click
        Dim mstrPath As String = ""
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            If CType(sender, LinkButton).ID = lnkBRJVExport.ID Then
                mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/BRJV.csv"
            Else
                mstrPath = ""
            End If

            If objPeriod._FlexCube_BatchNo.Trim <> "" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & objPeriod._FlexCube_BatchNo & ".", Me)
                Exit Try
            End If

            If objMainLedger.BR_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           CStr(Session("UserAccessModeSetting")), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                           CStr(Session("fmtCurrency")), _
                                                           CInt(Session("Base_CurrencyId")), _
                                                           CStr(Session("UserName")), _
                                                           mstrPath, _
                                                           Session("SQLDataSource").ToString, _
                                                           Session("SQLDatabaseName").ToString, _
                                                           Session("SQLDatabaseOwnerName").ToString, _
                                                           Session("SQLUserName").ToString, _
                                                           Session("SQLUserPassword").ToString _
                                                          ) = True Then

                If mstrPath.Trim <> "" Then
                    Session("ExFileName") = mstrPath
                    Export.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Data Posted Successfuly."), Me)
                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (02 Mar 2019) -- End

    'Hemant (15 Mar 2019) -- Start
    'MELIA ARUSHA ENHANCEMENT - Ref # 3593 - 76.1 : SAP ECC 6.0 JV integration.
    Protected Sub lnkSAPECC6_0JVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAPECC6_0JVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objPeriod._FlexCube_BatchNo.Trim <> "" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Posting is already done for selected period with batch no") & " " & objPeriod._FlexCube_BatchNo & ".", Me)
                Exit Try
            End If

            If objMainLedger.SAP_ECC_6_0_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           CStr(Session("UserAccessModeSetting")), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                           CStr(Session("fmtCurrency")), _
                                                           CInt(Session("Base_CurrencyId")), _
                                                           CStr(Session("UserName")), _
                                                           mstrPath, _
                                                           CBool(Session("OpenAfterExport")) _
                                                          ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (15 Mar 2019) -- End

    'Hemant (18 Apr 2019) -- Start
    'MWAUWASA ENHANCEMENT - Ref # 3747 - 76.1 : SAGE 300 JV integration.
    Protected Sub lnkSAGE300JVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAGE300JVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.SAGE_300_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           CStr(Session("UserAccessModeSetting")), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                           CStr(Session("fmtCurrency")), _
                                                           CInt(Session("Base_CurrencyId")), _
                                                           CStr(Session("UserName")), _
                                                           mstrPath, _
                                                           CBool(Session("OpenAfterExport")) _
                                                          ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (18 Apr 2019) -- End

    'Hemant (29 May 2019) -- Start
    'MUWASA ENHANCEMENT - Ref # 3329 - 76.1 : PASTEL V2 COMBINED JV INTEGRATION.
    Protected Sub lnkPASTELV2JVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPASTELV2JVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            Dim objPeriod As New clscommom_period_Tran
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Please set Posting Date. Posting Date is mandatory information."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            ElseIf CInt(cboPeriod.SelectedValue) > 0 AndAlso dtpPostingDate.GetDate <> Nothing Then
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                If objPeriod._Start_Date > dtpPostingDate.GetDate.Date Or objPeriod._End_Date < dtpPostingDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Posting Date should be in selected Period."), Me)
                    dtpPostingDate.Focus()
                    Exit Try
                End If
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.PASTEL_V2_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           CStr(Session("UserAccessModeSetting")), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                           CStr(Session("fmtCurrency")), _
                                                           CInt(Session("Base_CurrencyId")), _
                                                           CStr(Session("UserName")), _
                                                           mstrPath, _
                                                           CBool(Session("OpenAfterExport")) _
                                                          ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (29 May 2019) -- End

    'Sohail (26 Mar 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Protected Sub lnkPayrollJournalReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPayrollJournalReport.Click _
                                                                                                       , lnkPayrollJournalExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            Dim objPeriod As New clscommom_period_Tran
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try
            
            Else

                Dim objMaster As New clsMasterData
                Dim dsList As DataSet
                mblnIsCrystalReport = True
                If CType(sender, LinkButton).ID = lnkPayrollJournalExport.ID Then
                    mblnIsCrystalReport = False
                End If

                dsList = objMaster.GetComboListForPayrollJournalExportMode("ExportMode", False, mblnIsCrystalReport, Not mblnIsCrystalReport)
                With cboExportMode
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("ExportMode")
                    .DataBind()
                    Call cboExportMode_SelectedIndexChanged(cboExportMode, New System.EventArgs)
                End With

                popup_PayrollJournal.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (08 Apr 2020) -- Start
    'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
    Protected Sub lnkSAGE300ERPJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAGE300ERPJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'Sohail (17 Apr 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode
            'Sohail (17 Apr 2020) -- End

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName
            'Sohail (10 Apr 2020) -- End

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.SAGE_300_ERP_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           CStr(Session("UserAccessModeSetting")), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                           CStr(Session("fmtCurrency")), _
                                                           CInt(Session("Base_CurrencyId")), _
                                                           CStr(Session("UserName")), _
                                                           mstrPath, _
                                                           False _
                                                          ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (08 Apr 2020) -- End

    'Gajanan [15-April-2020] -- Start
    Protected Sub lnkSAGE300ERPImport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAGE300ERPImport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            'Sohail (21 May 2020) -- Start
            'Ifakara Enhancement # 0004640 : Payroll Journal and 2 importation templates for SAGE system.
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode
            'Sohail (21 May 2020) -- End

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.SAGE_300_ERP_Import_CombinedJVReport(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           objPeriod._Start_Date, _
                                                           objPeriod._End_Date, _
                                                           CStr(Session("UserAccessModeSetting")), True, _
                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                           CStr(Session("fmtCurrency")), _
                                                           CInt(Session("Base_CurrencyId")), _
                                                           CStr(Session("UserName")), _
                                                           mstrPath, _
                                                           False _
                                                          ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [15-April-2020] -- Ends

    'Sohail (22 Jun 2020) -- Start
    'CORAL BEACH CLUB enhancement : 0004743 : New JV Integration Coral Sun JV.
    Protected Sub lnkCoralSunJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCoralSunJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            'Sohail (08 Jul 2020) -- Start
            'NMB Issue : # : Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer.
            'objMainLedger._Analysis_Join = mstrAnalysis_Join
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (08 Jul 2020) -- End
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.Coral_Sun_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   objPeriod._Start_Date, _
                                                                   objPeriod._End_Date, _
                                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                                   CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                   CStr(Session("fmtCurrency")), _
                                                                   CInt(Session("Base_CurrencyId")), _
                                                                   CStr(Session("UserName")), _
                                                                   mstrPath, _
                                                                   False _
                                                                  ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (22 Jun 2020) -- End

    'Sohail (10 Aug 2020) -- Start
    'Freight Forwarders Kenya Enhancement : OLD-58 #  : New Accounting software Integration "Cargo Wise JV" for Freight Forwarders Kenya Ltd.
    Protected Sub lnkCargoWiseJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCargoWiseJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCostCenter.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information."), Me)
                cboCostCenter.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.Cargo_Wise_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   objPeriod._Start_Date, _
                                                                   objPeriod._End_Date, _
                                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                                   CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                   CStr(Session("fmtCurrency")), _
                                                                   CInt(Session("Base_CurrencyId")), _
                                                                   CStr(Session("UserName")), _
                                                                   mstrPath, _
                                                                   False, _
                                                                   CInt(cboCostCenter.SelectedValue), _
                                                                   True _
                                                                  ) = True Then

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (10 Aug 2020) -- End

    'Sohail (04 Sep 2020) -- Start
    'Benchmark Enhancement : OLD-73 #  : New Accounting software Integration "SAP Standard JV".
    Protected Sub lnkSAPStandardJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAPStandardJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/SAPStandardJV.csv"


            If objMainLedger.SAP_Standard_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        objPeriod._Start_Date, _
                                                                        objPeriod._End_Date, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                        CStr(Session("fmtCurrency")), _
                                                                        CInt(Session("Base_CurrencyId")), _
                                                                        CStr(Session("UserName")), mstrPath, False, 0, _
                                                                        False, True) = True Then


                Session("ExFileName") = mstrPath
                Export.Show()


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Sep 2020) -- End

    'Sohail (10 Sep 2020) -- Start
    'Oxygen Communication Ltd Enhancement : OLD-68 #  : New Accounting software Integration "SAGE Evolution JV".
    Protected Sub lnkSAGEEvolutionJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSAGEEvolutionJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub


            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try
            ElseIf CInt(cboCostCenter.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Please Select Cost Center. Cost Center is mandatory information."), Me)
                cboCostCenter.Focus()
                Exit Try

            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            'Sohail (23 Nov 2020) -- Start
            'NMB Enhancement : # : Give setting on configuration "Ignore Negative Net Pay Employees on JV".
            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))
            'Sohail (23 Nov 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._PeriodCode = objPeriod._Period_Code

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "/SAGEEvolutionJV.csv"


            If objMainLedger.SAGE_Evolution_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        objPeriod._Start_Date, _
                                                                        objPeriod._End_Date, _
                                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                        CStr(Session("fmtCurrency")), _
                                                                        CInt(Session("Base_CurrencyId")), _
                                                                        CStr(Session("UserName")), mstrPath, False, CInt(cboCostCenter.SelectedValue), _
                                                                        False) = True Then


                Session("ExFileName") = mstrPath
                Export.Show()


            End If
            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (10 Sep 2020) -- End

    'Sohail (16 Nov 2021) -- Start
    'Enhancement : OLD-492 : New Payroll JV Integration - Hakika Bank JV.
    Protected Sub lnkHakikaBankJVExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkHakikaBankJVExport.Click
        Try
            Dim mdecDebitAmountFrom As Decimal = 0
            Dim mdecDebitAmountTo As Decimal = 0
            Dim mdecCreditAmountFrom As Decimal = 0
            Dim mdecCreditAMountTo As Decimal = 0

            Decimal.TryParse(txtDebitAmountFrom.Text, mdecDebitAmountFrom)
            Decimal.TryParse(txtDebitAmountTo.Text, mdecDebitAmountTo)
            Decimal.TryParse(txtCreditAmountFrom.Text, mdecCreditAmountFrom)
            Decimal.TryParse(txtCreditAMountTo.Text, mdecCreditAMountTo)

            objMainLedger.SetDefaultValue()
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Please Select Period."), Me)
                cboPeriod.Focus()
                Exit Try

            ElseIf CDec(Me.ViewState("Ex_Rate")) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Exit Sub

            ElseIf CInt(cboReportType.SelectedIndex) <> 3 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Please Select Combined JV."), Me)
                cboReportType.Focus()
                Exit Try
            Else

            End If

            objMainLedger._PeriodId = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodName = cboPeriod.SelectedItem.Text

            objMainLedger._DebitAmountFrom = CStr(mdecDebitAmountFrom)
            objMainLedger._DebitAmountTo = CStr(mdecDebitAmountTo)

            objMainLedger._CreditAmountFrom = CStr(mdecCreditAmountFrom)
            objMainLedger._CreditAmountTo = CStr(mdecCreditAMountTo)

            objMainLedger._ReportId = cboReportType.SelectedIndex
            objMainLedger._ReportName = cboReportType.SelectedItem.Text

            objMainLedger._BranchId = CInt(cboBranch.SelectedValue)
            objMainLedger._Branch_Name = cboBranch.SelectedItem.Text

            objMainLedger._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked
            objMainLedger._IgnoreZero = chkIgnoreZero.Checked
            objMainLedger._PostingDate = dtpPostingDate.GetDate()

            objMainLedger._IgnoreNegativeNetPayEmployeesOnJV = CBool(Session("IgnoreNegativeNetPayEmployeesOnJV"))

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPeriod.SelectedValue)
            objMainLedger._PeriodStartDate = objPeriod._Start_Date
            objMainLedger._PeriodEndDate = objPeriod._End_Date
            objMainLedger._SunJV_Code = objPeriod._Sunjv_PeriodCode

            objMainLedger._Accounting_TransactionReference = CStr(Session("Accounting_TransactionReference"))
            objMainLedger._Accounting_JournalType = CStr(Session("Accounting_JournalType"))
            objMainLedger._Accounting_JVGroupCode = CStr(Session("Accounting_JVGroupCode"))

            objMainLedger._ViewByIds = mstrStringIds
            objMainLedger._ViewIndex = mintViewIdx
            objMainLedger._ViewByName = mstrStringName
            objMainLedger._Analysis_Fields = mstrAnalysis_Fields
            objMainLedger._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            objMainLedger._Analysis_OrderBy = mstrAnalysis_OrderBy
            objMainLedger._Report_GroupName = mstrReport_GroupName
            objMainLedger._Analysis_OrderBy_GName = mstrAnalysis_OrderBy_GroupName

            objMainLedger._UserUnkId = CInt(Session("UserId"))
            objMainLedger._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objMainLedger._UserAccessFilter = CStr(Session("AccessLevelFilterString"))
            GUI.fmtCurrency = CStr(Session("fmtCurrency"))
            objMainLedger.setDefaultOrderBy(cboReportType.SelectedIndex)

            objMainLedger._Ex_Rate = CDec(Me.ViewState("Ex_Rate"))
            If Me.ViewState("CurrencySign") IsNot Nothing Then
                objMainLedger._Currency_Sign = CStr(Me.ViewState("CurrencySign"))
            End If
            objMainLedger._OpenAfterExport = False
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)


            If objMainLedger.Hakika_Bank_JV_Export_CombinedJVReport(CStr(Session("Database_Name")), _
                                                                   CInt(Session("UserId")), _
                                                                   CInt(Session("Fin_year")), _
                                                                   CInt(Session("CompanyUnkId")), _
                                                                   objPeriod._Start_Date, _
                                                                   objPeriod._End_Date, _
                                                                   CStr(Session("UserAccessModeSetting")), True, _
                                                                   CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                   CStr(Session("fmtCurrency")), _
                                                                   CInt(Session("Base_CurrencyId")), _
                                                                   CStr(Session("UserName")), _
                                                                   mstrPath, _
                                                                   False, _
                                                                   CStr(Session("CompanyCode")) _
                                                                   ) = True Then
                'Sohail (01 Dec 2021) - [Session("CompanyCode")]

                If objMainLedger._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objMainLedger._FileNameAfterExported

                    Export.Show()

                End If

            End If

            objPeriod = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (16 Nov 2021) -- End


    Protected Sub ChangeLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim xRow As DataRow = dtPayrollJournalTable.Rows(rowIndex)
            Dim xNewRow As DataRow = dtPayrollJournalTable.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvPayrollJournalColumns.Rows.Count - 1 Then Exit Sub
                dtPayrollJournalTable.Rows.RemoveAt(rowIndex)
                dtPayrollJournalTable.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtPayrollJournalTable.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtPayrollJournalTable.Rows.Remove(xRow)
                dtPayrollJournalTable.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtPayrollJournalTable.Rows.IndexOf(xNewRow)
            End If
            With lvPayrollJournalColumns
                .DataSource = dtPayrollJournalTable
                .DataBind()
            End With
            dtPayrollJournalTable.AcceptChanges()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_PayrollJournal.Show()
        End Try
    End Sub

    Public Sub lvPayrollJournalColumns_ItemChecked(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Try
            If chk Is Nothing Then Exit Try
            If lvPayrollJournalColumns.Rows.Count <= 0 Then Exit Sub
            Select Case chk.ToolTip.ToUpper
                Case "ALL"
                    For Each dgvRow As GridViewRow In lvPayrollJournalColumns.Rows
                        Dim cb As CheckBox = CType(lvPayrollJournalColumns.Rows(dgvRow.RowIndex).FindControl("chkSelect"), CheckBox)
                        cb.Checked = chk.Checked
                        If mblnIsCrystalReport = True Then
                            If CInt(dtPayrollJournalTable.Rows(dgvRow.RowIndex)("Id")) = enPayrollJournal_Columns.Debit OrElse CInt(dtPayrollJournalTable.Rows(dgvRow.RowIndex)("Id")) = enPayrollJournal_Columns.Credit Then
                                cb.Checked = True
                            End If
                        End If
                    Next
                    dtPayrollJournalTable.AsEnumerable().Cast(Of DataRow).ToList.ForEach(Function(x) CheckUncheckAll(x, chk.Checked))
                    dtPayrollJournalTable.AcceptChanges()
                    'Hemant (13 Jul 2020) -- Start
                    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                    If chk.Checked = True Then
                        txtDateFormat.Enabled = True
                    Else
                        txtDateFormat.Text = "dd/MM/yyyy"
                        txtDateFormat.Enabled = False
                    End If
                    'Hemant (13 Jul 2020) -- End

                Case "CHECKED"
                    Dim gvRow As GridViewRow = CType(chk.NamingContainer, GridViewRow)
                    Dim xRow() As DataRow = dtPayrollJournalTable.Select("ID=" & dtPayrollJournalTable.Rows(gvRow.RowIndex)("Id").ToString)
                    If xRow.Length > 0 Then
                        If mblnIsCrystalReport = True Then
                            If CInt(xRow(0).Item("id")) = enPayrollJournal_Columns.Debit OrElse CInt(xRow(0).Item("id")) = enPayrollJournal_Columns.Credit Then
                                chk.Checked = True
                            End If
                        End If
                        xRow(0).Item("IsChecked") = chk.Checked
                        xRow(0).AcceptChanges()
                    End If

                    'Hemant (13 Jul 2020) -- Start
                    'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
                    If CInt(dtPayrollJournalTable.Rows(gvRow.RowIndex).Item("Id").ToString) = enPayrollJournal_Columns.Posting_Date Then
                        If chk.Checked = True Then
                            txtDateFormat.Enabled = True
                        Else
                            txtDateFormat.Text = "dd/MM/yyyy"
                            txtDateFormat.Enabled = False
                        End If
                    End If
                    'Hemant (13 Jul 2020) -- End

            End Select
            marrPayrollJournalColumnIds.AddRange((From p In dtPayrollJournalTable Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("ID").ToString)).ToArray)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_PayrollJournal.Show()
        End Try
    End Sub

    Private Function CheckUncheckAll(ByVal x As DataRow, ByVal blnCheckAll As Boolean) As Boolean
        Try
            If mblnIsCrystalReport = True Then
                If CInt(x.Item("id")) = enPayrollJournal_Columns.Debit OrElse CInt(x.Item("id")) = enPayrollJournal_Columns.Credit Then
                    x.Item("ischecked") = True
                Else
                    x.Item("ischecked") = blnCheckAll
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
    'Sohail (26 Mar 2020) -- End

    'Sohail (10 Apr 2020) -- Start
    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            popupAnalysisBy._EffectiveDate = objPeriod._End_Date
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
            mstrAnalysis_OrderBy_GroupName = popupAnalysisBy._Analysis_OrderBy_GName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
            mstrAnalysis_OrderBy_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (10 Apr 2020) -- End

#End Region

#Region "GridView Event"

    Protected Sub gvTranHead_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gbFilterTBCJV.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            Dim mdtTran As DataTable = CType(Me.ViewState("EmpContribPayableHead"), DataTable)

            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(2).Text = "&nbsp;" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                    e.Row.Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Sohail (07 Mar 2020) -- Start
    'NMB Enhancement # : Once Flex Cube JV is posted to oracle, trigger email to specified users mapped on configuration.
#Region "TextBox's Events"

    Protected Sub txtSearchScheme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchScheme.TextChanged
        Try
            If mdtLoanList IsNot Nothing Then
                Dim dvLoanList As DataView = mdtLoanList.DefaultView
                If dvLoanList.Table.Rows.Count > 0 Then
                    dvLoanList.RowFilter = "Code like '%" & txtSearchScheme.Text.Trim & "%' OR name like '%" & txtSearchScheme.Text.Trim & "%' "
                    gvScheme.DataSource = dvLoanList.ToTable
                    gvScheme.DataBind()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Sohail (07 Mar 2020) -- End

#Region "ImageButton Events"


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblDebitAmountFrom.Text = Language._Object.getCaption(Me.lblDebitAmountFrom.ID, Me.lblDebitAmountFrom.Text)
            Me.lblDebitAmountTo.Text = Language._Object.getCaption(Me.lblDebitAmountTo.ID, Me.lblDebitAmountTo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblCreditAMountFrom.Text = Language._Object.getCaption(Me.lblCreditAMountFrom.ID, Me.lblCreditAMountFrom.Text)
            Me.lblCreditAMountTo.Text = Language._Object.getCaption(Me.lblCreditAMountTo.ID, Me.lblCreditAMountTo.Text)
            Me.lblReportType.Text = Language._Object.getCaption(Me.lblReportType.ID, Me.lblReportType.Text)
            Me.chkShowSummaryBottom.Text = Language._Object.getCaption(Me.chkShowSummaryBottom.ID, Me.chkShowSummaryBottom.Text)
            Me.chkShowSummaryNewPage.Text = Language._Object.getCaption(Me.chkShowSummaryNewPage.ID, Me.chkShowSummaryNewPage.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.ID, Me.lblBranch.Text)
            Me.chkShowGroupByCCGroup.Text = Language._Object.getCaption(Me.chkShowGroupByCCGroup.ID, Me.chkShowGroupByCCGroup.Text)
            Me.lblCCGroup.Text = Language._Object.getCaption(Me.lblCCGroup.ID, Me.lblCCGroup.Text)
            Me.chkIncludeEmployerContribution.Text = Language._Object.getCaption(Me.chkIncludeEmployerContribution.ID, Me.chkIncludeEmployerContribution.Text)
            Me.lnkSunJVExport.Text = Language._Object.getCaption(Me.lnkSunJVExport.ID, Me.lnkSunJVExport.Text)
            Me.lblExRate.Text = Language._Object.getCaption(Me.lblExRate.ID, Me.lblExRate.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lnkiScalaJVExport.Text = Language._Object.getCaption(Me.lnkiScalaJVExport.ID, Me.lnkiScalaJVExport.Text)
            Me.lnkTBCJVExport.Text = Language._Object.getCaption(Me.lnkTBCJVExport.ID, Me.lnkTBCJVExport.Text)
            Me.chkIgnoreZero.Text = Language._Object.getCaption(Me.chkIgnoreZero.ID, Me.chkIgnoreZero.Text)
            Me.lnkSunJV5Export.Text = Language._Object.getCaption(Me.lnkSunJV5Export.ID, Me.lnkSunJV5Export.Text)
            Me.lnkiScala2JVExport.Text = Language._Object.getCaption(Me.lnkiScala2JVExport.ID, Me.lnkiScala2JVExport.Text)
            Me.lblCustomCCenter.Text = Language._Object.getCaption(Me.lblCustomCCenter.ID, Me.lblCustomCCenter.Text)
            Me.chkShowColumnHeader.Text = Language._Object.getCaption(Me.chkShowColumnHeader.ID, Me.chkShowColumnHeader.Text)
            Me.lblInvoiceRef.Text = Language._Object.getCaption(Me.lblInvoiceRef.ID, Me.lblInvoiceRef.Text)
            Me.gbFilterTBCJV.Columns(1).HeaderText = Language._Object.getCaption(Me.gbFilterTBCJV.Columns(1).FooterText, Me.gbFilterTBCJV.Columns(1).HeaderText)
            Me.gbFilterTBCJV.Columns(2).HeaderText = Language._Object.getCaption(Me.gbFilterTBCJV.Columns(2).FooterText, Me.gbFilterTBCJV.Columns(2).HeaderText)
            Me.lnkXEROJVExport.Text = Language._Object.getCaption(Me.lnkXEROJVExport.ID, Me.lnkXEROJVExport.Text).Replace("&", "")
            Me.lnkNetSuiteERPJVExport.Text = Language._Object.getCaption(Me.lnkNetSuiteERPJVExport.ID, Me.lnkNetSuiteERPJVExport.Text).Replace("&", "")
            Me.lnkFlexCubeRetailJVExport.Text = Language._Object.getCaption(Me.lnkFlexCubeRetailJVExport.ID, Me.lnkFlexCubeRetailJVExport.Text).Replace("&", "") 'Sohail (06 Aug 2016)
            Me.lnkSunAccountProjectJVExport.Text = Language._Object.getCaption(Me.lnkSunAccountProjectJVExport.ID, Me.lnkSunAccountProjectJVExport.Text).Replace("&", "")
            Me.lblTranHead.Text = Language._Object.getCaption(Me.lblTranHead.ID, Me.lblTranHead.Text)
            Me.lnkDynamicsNavJVExport.Text = Language._Object.getCaption(Me.lnkDynamicsNavJVExport.ID, Me.lnkDynamicsNavJVExport.Text).Replace("&", "")
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.ID, Me.lblCostCenter.Text).Replace("&", "")
            Me.lblPostingDate.Text = Language._Object.getCaption(Me.lblPostingDate.ID, Me.lblPostingDate.Text)
            Me.lnkFlexCubeUPLDJVExport.Text = Language._Object.getCaption(Me.lnkFlexCubeUPLDJVExport.ID, Me.lnkFlexCubeUPLDJVExport.Text).Replace("&", "")
            Me.lnkSAPJVBSOneExport.Text = Language._Object.getCaption(Me.lnkSAPJVBSOneExport.ID, Me.lnkSAPJVBSOneExport.Text).Replace("&", "")
            Me.lnkFlexCubeJVExport.Text = Language._Object.getCaption(Me.lnkFlexCubeJVExport.ID, Me.lnkFlexCubeJVExport.Text).Replace("&", "")
            Me.lnkFlexCubeJVPostOracle.Text = Language._Object.getCaption(Me.lnkFlexCubeJVPostOracle.ID, Me.lnkFlexCubeJVPostOracle.Text).Replace("&", "")
            Me.lnkBRJVExport.Text = Language._Object.getCaption(Me.lnkBRJVExport.ID, Me.lnkBRJVExport.Text).Replace("&", "")
            Me.lnkBRJVPostSQL.Text = Language._Object.getCaption(Me.lnkBRJVPostSQL.ID, Me.lnkBRJVPostSQL.Text).Replace("&", "")
            Me.lnkSAPECC6_0JVExport.Text = Language._Object.getCaption(Me.lnkSAPECC6_0JVExport.ID, Me.lnkSAPECC6_0JVExport.Text).Replace("&", "")
            Me.lnkPASTELV2JVExport.Text = Language._Object.getCaption(Me.lnkPASTELV2JVExport.ID, Me.lnkPASTELV2JVExport.Text).Replace("&", "")
            Me.lnkFlexCubeLoanBatchExport.Text = Language._Object.getCaption(Me.lnkFlexCubeLoanBatchExport.ID, Me.lnkFlexCubeLoanBatchExport.Text).Replace("&", "")
            Me.lvPayrollJournalColumns.Columns(1).HeaderText = Language._Object.getCaption(Me.lvPayrollJournalColumns.Columns(1).FooterText, Me.lvPayrollJournalColumns.Columns(1).HeaderText)
            Me.gvScheme.Columns(2).HeaderText = Language._Object.getCaption(Me.gvScheme.Columns(2).FooterText, Me.gvScheme.Columns(2).HeaderText)
            Me.gvScheme.Columns(3).HeaderText = Language._Object.getCaption(Me.gvScheme.Columns(3).FooterText, Me.gvScheme.Columns(3).HeaderText)
            Me.lnkPayrollJournalExport.Text = Language._Object.getCaption(Me.lnkPayrollJournalExport.ID, Me.lnkPayrollJournalExport.Text)
            Me.lnkPayrollJournalReport.Text = Language._Object.getCaption(Me.lnkPayrollJournalReport.ID, Me.lnkPayrollJournalReport.Text)
            Me.lnkSAGE300ERPJVExport.Text = Language._Object.getCaption(Me.lnkSAGE300ERPJVExport.ID, Me.lnkSAGE300ERPJVExport.Text)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.ID, Me.lnkAdvanceFilter.Text)
            Me.lnkSAGE300ERPImport.Text = Language._Object.getCaption(Me.lnkSAGE300ERPImport.ID, Me.lnkSAGE300ERPImport.Text)
            Me.lnkCoralSunJVExport.Text = Language._Object.getCaption(Me.lnkCoralSunJVExport.ID, Me.lnkCoralSunJVExport.Text)
            Me.lnkCargoWiseJVExport.Text = Language._Object.getCaption(Me.lnkCargoWiseJVExport.ID, Me.lnkCargoWiseJVExport.Text)
            Me.lnkSAPStandardJVExport.Text = Language._Object.getCaption(Me.lnkSAPStandardJVExport.ID, Me.lnkSAPStandardJVExport.Text)
            Me.lnkSAGEEvolutionJVExport.Text = Language._Object.getCaption(Me.lnkSAGEEvolutionJVExport.ID, Me.lnkSAGEEvolutionJVExport.Text)
            'Hemant (13 Jul 2020) -- Start
            'ENHANCEMENT(HJFMRI Tanzania) #0004784 : To add posting date (DD/MM/YYYY)option on CUSTOM combine Journal voucher Report
            Me.lblDateFormat.Text = Language._Object.getCaption(Me.lblDateFormat.ID, Me.lblDateFormat.Text)
            'Hemant (13 Jul 2020) -- End
            Me.lnkHakikaBankJVExport.Text = Language._Object.getCaption(Me.lnkHakikaBankJVExport.ID, Me.lnkHakikaBankJVExport.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
   
End Class
