﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports Microsoft.VisualBasic
#End Region

Partial Class Budget_Timesheet_wPg_ExemptEmployee
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmExemptEmployeeList"
    Private ReadOnly mstrModuleName1 As String = "frmExemptEmployee"
    Private blnpopupExemptEmployee As Boolean = False
    Private mdtExemptEmployee As DataTable = Nothing
    Private mdtExemptEmployeeList As DataTable = Nothing
    Dim objExemptEmp As New clstsexemptemployee_tran
    Private mintExemptEmployeeunkid As Integer = 0
#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                Call SetMessages()
                FillListCombo()
                FillExemptEmployeeList(True)
                SetVisibilty()
            Else
                blnpopupExemptEmployee = CBool(ViewState("blnpopupExemptEmployee "))
                mdtExemptEmployee = CType(ViewState("mdtExemptEmployee"), DataTable)
                mdtExemptEmployeeList = CType(ViewState("mdtExemptEmployeeList"), DataTable)
                mintExemptEmployeeunkid = CInt(ViewState("mintExemptEmployeeunkid"))
            End If

            If blnpopupExemptEmployee Then
                popupExemptEmployee.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupExemptEmployee ") = blnpopupExemptEmployee
            ViewState("mdtExemptEmployee") = mdtExemptEmployee
            ViewState("mdtExemptEmployeeList") = mdtExemptEmployeeList
            ViewState("mintExemptEmployeeunkid") = mintExemptEmployeeunkid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

#Region "List Form Methods"

    Private Sub FillListCombo()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet = Nothing

            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", True, _
                                                     , , , , , , , , , , , , , , , , , )

            With drpEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            objEmployee = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillExemptEmployeeList(ByVal isblank As Boolean)
        Try

            If CBool(Session("AllowToViewBudgetTimesheetExemptEmployeeList")) = False Then Exit Sub

            Dim objExemptEmployee As New clstsexemptemployee_tran
            Dim dsList As DataSet = Nothing

            Dim strSearch As String = ""
            If CInt(drpEmployee.SelectedValue) > 0 Then
                strSearch = "AND hremployee_master.employeeunkid = " & CInt(drpEmployee.SelectedValue)
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            If isblank = False Then
                dsList = objExemptEmployee.GetList("List", True, strSearch)
            Else
                dsList = objExemptEmployee.GetList("List", True, strSearch).Clone()
            End If

            If isblank = True OrElse dsList.Tables("List").Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables("List").NewRow()
                drRow("employeeunkid") = -1
                drRow("Employee") = ""
                dsList.Tables("List").Rows.Add(drRow)
                isblank = True
            End If


            mdtExemptEmployeeList = dsList.Tables(0)

            gvExemptEmployeeList.DataSource = mdtExemptEmployeeList
            gvExemptEmployeeList.DataBind()

            If isblank Then
                gvExemptEmployeeList.Rows(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibilty()
        Try
            btnNew.Visible = CBool(Session("AllowToAddBudgetTimesheetExemptEmployee"))
            gvExemptEmployeeList.Columns(0).Visible = CBool(Session("AllowToDeleteBudgetTimesheetExemptEmployee"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Add/Edit Form Methods"

    Private Sub FillEmployee(ByVal isblank As Boolean)
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsList As DataSet = Nothing



            If isblank = False Then
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", False, _
                                                         , , , , , , , , , , , , , , , , , )
            Else
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                         CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "List", False, _
                                                         , , , , , , , , , , , , , , , , , ).Clone()
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Columns.Contains("ischecked") = False Then
                Dim dc As New DataColumn("ischecked", Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                dsList.Tables(0).Columns.Add(dc)
            End If

            If isblank = True OrElse dsList.Tables("List").Rows.Count <= 0 Then
                Dim drRow As DataRow = dsList.Tables("List").NewRow()
                drRow("ischecked") = False
                drRow("employeeunkid") = -1
                drRow("empcodename") = ""
                dsList.Tables("List").Rows.Add(drRow)
                isblank = True
            End If


            mdtExemptEmployee = dsList.Tables(0)

            gvExemptEmployee.DataSource = mdtExemptEmployee
            gvExemptEmployee.DataBind()

            If isblank Then
                gvExemptEmployee.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue(ByVal xFormName As String)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objExemptEmp._AuditUserunkid = CInt(Session("UserId"))
            End If
            objExemptEmp._WebClientIP = CStr(Session("IP_ADD"))
            objExemptEmp._WebHostName = CStr(Session("HOST_NAME"))
            objExemptEmp._WebFormName = xFormName
            objExemptEmp._IsWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Button Events"

#Region "List Form Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            blnpopupExemptEmployee = True
            popupExemptEmployee.Show()
            txtSearch.Text = ""
            FillEmployee(False)
            dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillExemptEmployeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpEmployee.SelectedValue = "0"
            FillExemptEmployeeList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintExemptEmployeeunkid = Convert.ToInt32(lnk.CommandArgument.ToString())
            objExemptEmp._Tsempexemptunkid = mintExemptEmployeeunkid
            popupConfirmYesNo.Title = Language.getMessage(mstrModuleName, 1, "Confirmation")
            popupConfirmYesNo.Message = Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this exempt employee?")
            popupConfirmYesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupConfirmYesNo_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupConfirmYesNo.buttonYes_Click
        Try
            popupDeleteReason.Title = Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this exempt employee?")
            popupDeleteReason.Reason = ""
            popupDeleteReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            objExemptEmp._Tsempexemptunkid = mintExemptEmployeeunkid
            objExemptEmp._Isvoid = True
            objExemptEmp._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objExemptEmp._Voiduserunkid = CInt(Session("UserId"))
            objExemptEmp._Voidreason = popupDeleteReason.Reason
            SetAtValue(mstrModuleName)

            blnFlag = objExemptEmp.Delete(mintExemptEmployeeunkid)
            If blnFlag = False And objExemptEmp._Message <> "" Then
                DisplayMessage.DisplayMessage(objExemptEmp._Message, Me)
                Exit Sub
            Else
                FillExemptEmployeeList(False)
                mintExemptEmployeeunkid = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Add/Edit Form Events"

    Protected Sub btnExemptEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExemptEmployee.Click
        Try
            If mdtExemptEmployee Is Nothing Then Exit Sub

            Dim xCount As Integer = mdtExemptEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).Count

            If xCount <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Please check atleast one employee to do further operation on it."), Me)
                Exit Sub
            End If

            Dim drList As List(Of DataRow) = mdtExemptEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).ToList()

            If drList.Count > 0 Then
                Dim mblnFlag As Boolean = False
                Dim mblnExist As Boolean = False

                Dim idx As Integer = -1
                For Each dr As DataRow In drList
                    Dim mstrEmployeecode As String = dr("EmpCodeName").ToString()
                    Dim rows = gvExemptEmployee.Rows.Cast(Of GridViewRow)().Where(Function(xRow) xRow.Cells(1).Text.Equals(mstrEmployeecode))

                    If objExemptEmp.isExist(CInt(dr("employeeunkid")), -1) Then
                        idx = rows(0).RowIndex
                        If idx >= 0 Then
                            gvExemptEmployee.Rows(idx).ForeColor = System.Drawing.Color.Red
                        End If
                        mblnExist = True
                        Continue For
                    End If

                    objExemptEmp._Transactiondate = dtpDate.GetDate.Date
                    objExemptEmp._Employeeunkid = CInt(dr("employeeunkid"))
                    objExemptEmp._Userunkid = CInt(Session("UserId"))
                    SetAtValue(mstrModuleName)
                    mblnFlag = objExemptEmp.Insert()

                    If mblnFlag = False Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Problem in exempting employee(s)."), Me)
                        Exit For
                    End If

                Next

                If mblnExist Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, You cannot exempting some of the selected employee(s). Reason : some of the selected employee(s) are already exempted and will be highlighted in red."), Me)
                ElseIf mblnFlag Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Employee(s) exempted successfully."), Me)
                    If mblnExist = False Then FillEmployee(False)
                    txtSearch.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExemptClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExemptClose.Click
        Try
            blnpopupExemptEmployee = False
            popupExemptEmployee.Hide()
            mdtExemptEmployee.Rows.Clear()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "CheckBox Events"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelectAll As CheckBox = CType(sender, CheckBox)

            If mdtExemptEmployee.Rows.Count <= 1 Then Exit Sub '1 STANDS HIDDEN BLANK ROW.

            For Each item As GridViewRow In gvExemptEmployee.Rows
                If item.RowType = DataControlRowType.DataRow Then

                    'Pinkal (04-Apr-2020) -- Start
                    'Bug -   Problem Found when filter employee and check that employee at that time it was taking first employee only.
                    'CType(item.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                    'mdtExemptEmployee.Rows(item.RowIndex)("ischecked") = chkSelectAll.Checked
                    Dim drRow As DataRow() = mdtExemptEmployee.Select("employeeunkid=" & CInt(gvExemptEmployee.DataKeys(item.RowIndex).Values("employeeunkid")))
                    If drRow.Length > 0 Then
                        drRow(0)("ischecked") = chkSelectAll.Checked
                        Dim gvRow As GridViewRow = gvExemptEmployee.Rows(item.RowIndex)
                        CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = chkSelectAll.Checked
                    End If
                    'Pinkal (04-Apr-2020) -- End
                End If
            Next
            mdtExemptEmployee.AcceptChanges()
            CType(gvExemptEmployee.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = CType(sender, CheckBox).Checked
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupExemptEmployee.Show()
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)

            Dim dsRow As DataRow() = mdtExemptEmployee.Select("employeeunkid=" & CInt(gvExemptEmployee.DataKeys(item.RowIndex).Values("employeeunkid")))
            If dsRow.Length > 0 Then
                dsRow(0)("ischecked") = chkSelect.Checked
                dsRow(0).AcceptChanges()
            End If
            Dim xSelectedCount As Integer = mdtExemptEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).DefaultIfEmpty().Count
            If xSelectedCount = mdtExemptEmployee.Rows.Count Then
                CType(gvExemptEmployee.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(gvExemptEmployee.HeaderRow.FindControl("chkSelectAll"), CheckBox).Checked = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupExemptEmployee.Show()
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub gvExemptEmployee_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExemptEmployee.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim chkSelect As CheckBox = CType(e.Row.FindControl("chkselect"), CheckBox)
                chkSelect.Checked = CBool(gvExemptEmployee.DataKeys(e.Row.RowIndex).Values("ischecked"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Textbox Events"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If mdtExemptEmployee IsNot Nothing Then
                Dim dvEmployee As DataView = mdtExemptEmployee.DefaultView()
                If dvEmployee.Table.Rows.Count > 0 Then
                    If txtSearch.Text.Trim.Length > 0 Then
                        dvEmployee.RowFilter = "EmpCodeName like '%" & txtSearch.Text.Trim() & "%'"
                    End If
                    gvExemptEmployee.DataSource = dvEmployee.ToTable()
                    gvExemptEmployee.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupExemptEmployee.Show()
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Title)
            Language._Object.setCaption(lblPageHeader.ID, lblPageHeader.Text)
            Language._Object.setCaption(lblCaption.ID, lblCaption.Text)
            Language._Object.setCaption(lblEmployee.ID, lblEmployee.Text)

            Language._Object.setCaption(btnNew.ID, btnNew.Text.Replace("&", ""))
            Language._Object.setCaption(btnSearch.ID, btnSearch.Text.Replace("&", ""))
            Language._Object.setCaption(btnReset.ID, btnReset.Text.Replace("&", ""))
            Language._Object.setCaption(btnClose.ID, btnClose.Text.Replace("&", ""))

            Language._Object.setCaption(gvExemptEmployeeList.Columns(1).FooterText, gvExemptEmployeeList.Columns(1).HeaderText)


            Language.setLanguage(mstrModuleName1)
            Language._Object.setCaption(LblTitle.ID, LblTitle.Text)
            Language._Object.setCaption(LblExemptEmployee.ID, LblExemptEmployee.Text)
            Language._Object.setCaption(lblDate.ID, lblDate.Text)

            Language._Object.setCaption(btnExemptEmployee.ID, btnExemptEmployee.Text.Replace("&", ""))
            Language._Object.setCaption(btnExemptClose.ID, btnExemptClose.Text.Replace("&", ""))

            Language._Object.setCaption(gvExemptEmployee.Columns(1).FooterText, gvExemptEmployee.Columns(1).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Title = Language._Object.getCaption(mstrModuleName, Title)
            lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, lblPageHeader.Text)
            lblCaption.Text = Language._Object.getCaption(lblCaption.ID, lblCaption.Text)
            lblEmployee.Text = Language._Object.getCaption(lblEmployee.ID, lblEmployee.Text)

            btnNew.Text = Language._Object.getCaption(btnNew.ID, btnNew.Text.Replace("&", ""))
            btnSearch.Text = Language._Object.getCaption(btnSearch.ID, btnSearch.Text.Replace("&", ""))
            btnReset.Text = Language._Object.getCaption(btnReset.ID, btnReset.Text.Replace("&", ""))
            btnClose.Text = Language._Object.getCaption(btnClose.ID, btnClose.Text.Replace("&", ""))

            gvExemptEmployeeList.Columns(1).HeaderText = Language._Object.getCaption(gvExemptEmployeeList.Columns(1).FooterText, gvExemptEmployeeList.Columns(1).HeaderText)


            Language.setLanguage(mstrModuleName1)
            LblTitle.Text = Language._Object.getCaption(LblTitle.ID, LblTitle.Text)
            LblExemptEmployee.Text = Language._Object.getCaption(LblExemptEmployee.ID, LblExemptEmployee.Text)
            lblDate.Text = Language._Object.getCaption(lblDate.ID, lblDate.Text)

            btnExemptEmployee.Text = Language._Object.getCaption(btnExemptEmployee.ID, btnExemptEmployee.Text.Replace("&", ""))
            btnExemptClose.Text = Language._Object.getCaption(btnExemptClose.ID, btnExemptClose.Text.Replace("&", ""))

            Me.gvExemptEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.gvExemptEmployee.Columns(1).FooterText, Me.gvExemptEmployee.Columns(1).HeaderText)
        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Confirmation")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this exempt employee?")

            Language.setMessage(mstrModuleName1, 1, "Please check atleast one employee to do further operation on it.")
            Language.setMessage(mstrModuleName1, 2, "Problem in exempting employee(s).")
            Language.setMessage(mstrModuleName1, 3, "Sorry, You cannot exempting some of the selected employee(s). Reason : some of the selected employee(s) are already exempted and will be highlighted in red.")
            Language.setMessage(mstrModuleName1, 4, "Employee(s) exempted successfully.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
