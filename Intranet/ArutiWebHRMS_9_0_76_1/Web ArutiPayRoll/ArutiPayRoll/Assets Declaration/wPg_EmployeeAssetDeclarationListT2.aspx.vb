﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports System.IO

#End Region
'Last Change By Gajanan 30.11.2018

Partial Class HR_wPg_EmployeeAssetDeclarationListT2
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Dim mintAssetDeclarationT2Unkid As Integer = -1
    Dim strSearching As String = ""

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Dim dsAssetList As DataSet

    'Dim dtBusiness As DataTable
    'Dim dtStaffOwnership As DataTable
    'Dim dtBankAccounts As DataTable
    'Dim dtProperties As DataTable
    'Dim dtLiabilities As DataTable
    'Dim dtRelatives As DataTable


    ''Gajanan (06 Oct 2018) -- Start
    ''Enhancement : Implementing New Module of Asset Declaration Template 2
    'Dim dtDependantBusinessS8 As DataTable
    'Dim dtDependantShareS9 As DataTable
    'Dim dtDependantBank10 As DataTable
    'Dim dtDependantPropertiesS11 As DataTable
    ''Gajanan(06 Oct 2018) -- End

    'Dim dsCurrBusiness As DataSet
    'Dim dsCurrShare As DataSet
    'Dim dsCurrBank As DataSet
    'Dim dsCurrProperties As DataSet
    'Dim dsCurrLiabilities As DataSet
    'Dim dsCurrRelatives As DataSet
    'Dim dsCurrOtherBusiness As DataSet

    ''Gajanan (06 Oct 2018) -- Start
    ''Enhancement : Implementing New Module of Asset Declaration Template 2
    'Dim dsCurrMonthlyEarningS8 As DataSet
    'Dim dsCurrMarketValueS9 As DataSet
    'Dim dsCurrAmount10 As DataSet
    'Dim dsCurrEstimatedValueS11 As DataSet
    ''Gajanan(06 Oct 2018) -- End

    'Dim dsCurrResources As DataSet
    'Dim dsCurrDebt As DataSet
    'Sohail (07 Dec 2018) -- End
    'Gajanan (06 Oct 2018) -- Start
    'Enhancement : Implementing New Module of Asset Declaration Template 2
    Dim dsCurrMonthlyEarningS8 As DataSet
    Dim dsCurrMarketValueS9 As DataSet
    Dim dsCurrAmount10 As DataSet
    Dim dsCurrEstimatedValueS11 As DataSet
    'Gajanan(06 Oct 2018) -- End
  


    Dim blnpopupAddEdit As Boolean = False


    Private Shared ReadOnly mstrModuleName As String = "frmEmpAssetDeclarationListT2"
    Private Shared ReadOnly mstrModuleName1 As String = "frmEmpAssetDeclarationT2"



    Private menAction As enAction = enAction.ADD_ONE

    Private mintOldEmployeeUnkId As Integer = -1
    Private mstrOldTinNo As String = String.Empty
    Private mdtOldTransactionDate As DateTime
    Private mblnIsbtnEditClick As Boolean = False
    'Hemant (15 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement
    Private mblnOldIsAcknowledged As Boolean = False
    'Hemant (15 Nov 2018) -- End

    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Dim dtDependantList As DataTable
    'Sohail (07 Dec 2018) -- End
    'Hemant (03 Dec 2018) -- End

    'Hemant (05 Dec 2018) -- Start
    'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Dim dtFinYearList As DataTable
    'Sohail (07 Dec 2018) -- End
    Private mdtFinStartDate As DateTime
    Private mdtFinEndDate As DateTime
    'Hemant (05 Dec 2018) -- End


#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtFinYearList As DataTable
        'Sohail (07 Dec 2018) -- End
        Try


            Dim objEmployee As New clsEmployee_Master
            Dim strfilter As String
            Dim dsList As DataSet = Nothing
            Dim objclsMasterData As New clsMasterData

            If (Session("loginBy") = Global.User.en_loginby.Employee) Then


                Dim objglobalassess = New GlobalAccess

                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()

                cboStep1_Employee.DataSource = objglobalassess.ListOfEmployee.Copy
                cboStep1_Employee.DataTextField = "loginname"
                cboStep1_Employee.DataValueField = "employeeunkid"
                cboStep1_Employee.DataBind()


                'drpRelativeEmployee.DataSource = objglobalassess.ListOfEmployee.Copy
                'drpRelativeEmployee.DataTextField = "loginname"
                'drpRelativeEmployee.DataValueField = "employeeunkid"
                'drpRelativeEmployee.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                'strfilter = "hremployee_master.employeeunkid <> " + CStr(Session("Employeeunkid")) + ""

                'dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    Session("UserAccessModeSetting").ToString(), True, _
                '                                    True, "Employee", True, , False, False, 0, 0, 0, 0, 0, , , , , , , strfilter, , False, False)
                ''Hemant (15 Nov 2019) -- [Session("ShowFirstAppointmentDate") --> True]
                'drpStep7_RelativeEmployee.DataSource = dsList.Tables("Employee").Copy
                'drpStep7_RelativeEmployee.DataTextField = "EmpCodeName"
                'drpStep7_RelativeEmployee.DataValueField = "employeeunkid"
                'drpStep7_RelativeEmployee.DataBind()
                'Hemant (02 Feb 2022) -- End

                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                cboStep1_Employee_SelectedIndexChanged(Nothing, Nothing)
                'Hemant (14 Nov 2018) -- End
            Else



                dsList = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                True, "Employee", True)
                'Hemant (15 Nov 2019) -- [Session("IsIncludeInactiveEmp") --> True]
                With drpEmployee
                    .DataValueField = "employeeunkid"

                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = 0
                End With

                With cboStep1_Employee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee").Copy
                    .DataBind()
                    .SelectedValue = 0
                End With

                With drpStep7_RelativeEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee").Copy
                    .DataBind()
                    .SelectedValue = 0
                End With

            End If

            Dim objExchange As New clsExchangeRate
            Dim objAsset_business_dealT2_tran As New clsAsset_business_dealT2_tran
            Dim dsCombos As DataSet = objExchange.getComboList("BaseCurr", False, True)
            If dsCombos.Tables("BaseCurr").Rows.Count > 0 Then
                Me.ViewState.Add("Exchangerateunkid", CInt(dsCombos.Tables("BaseCurr").Rows(0).Item("exchangerateunkid")))
                Me.ViewState.Add("currencysign", dsCombos.Tables("BaseCurr").Rows(0).Item("currency_sign").ToString)
            End If

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            If Session("AssetDeclarationFromDate").ToString = "" Then
                dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
                'Hemant (23 Jan 2019) -- Start
                'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
                'ElseIf eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
            ElseIf eZeeDate.convertDate(Session("AssetDeclarationFromDate")) IsNot Nothing Then
                'Hemant (23 Jan 2019) -- End
                'Hemant (02 Jan 2019) -- Start
                'NMB - ENHANCEMENT
                'dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"), , , True, Session("financialyear_name"))
                'Hemant (23 Jan 2019) -- Start
                'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
                'dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True)
                dsCombos = objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True)
                Dim StrYear As String
                Dim intYear As Integer
                If eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    intYear = eZeeDate.convertDate(Session("AssetDeclarationFromDate")).Year - CDate(Session("fin_startdate")).Year
                Else
                    intYear = -1
                End If
                StrYear = CDate(Session("fin_startdate")).AddYears(intYear).Year & "-" & CDate(Session("fin_enddate")).AddYears(intYear).Year
                Dim i As Integer = 0
                Do While i <= dsCombos.Tables(0).Rows.Count - 1
                    If dsCombos.Tables(0).Rows(i)("financialyear_name").ToString() = StrYear Then
                    Else
                        dsCombos.Tables(0).Rows(i).Delete()
                        dsCombos.Tables(0).AcceptChanges()
                        i -= 1
                    End If
                    i += 1
                Loop
                'Hemant (23 Jan 2019) -- End
                'Hemant (02 Jan 2019) -- End               
            Else
                dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
            End If

            dtFinYearList = dsCombos.Tables("Year").Copy

            With drpFinalcialYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dtFinYearList
                .DataBind()
                .SelectedValue = 0
            End With
            'Hemant (10 Dec 2018) -- Start
            If Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").Trim.ToString <> "" Then
                drpFinalcialYear.SelectedValue = -99
            End If
            'Hemant (10 Dec 2018) -- End
            'Hemant (23 Jan 2019) -- Start
            'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
            If CInt(drpFinalcialYear.SelectedIndex) >= 0 Then
            drpFinalcialYear_SelectedIndexChanged(Nothing, Nothing)
            End If
            'Hemant (23 Jan 2019) -- End


            With cboStep1_FinancialYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dtFinYearList.Copy
                .DataBind()
                .SelectedValue = 0
            End With
            'Hemant (05 Dec 2018) -- End


            dsCombos = objclsMasterData.getComboListAssetsupplierborrowerList("Clients")
            With drpStep2_Is_supplier_client
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Clients").Copy
                .DataBind()
                .SelectedValue = 0
            End With

            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")
            'With cboStep2_Sector
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombos.Tables("Asset Sector").Copy
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'Hemant (02 Feb 2022) -- End

            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECURITIES, True, "Asset Securities")
            With drpStep3_Securitiestype
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Asset Securities").Copy
                .DataBind()
                .SelectedValue = 0
            End With

            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.LIABILITY_TYPE_ASSET_DECLARATION_TEMPLATE2, True, "Liability Type")
            With drpStep6_LiabilityTypeLiabilities
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Liability Type").Copy
                .DataBind()
                .SelectedValue = 0
            End With
            'Hemant (15 Nov 2018) -- End

            If CInt(cboStep1_Employee.SelectedValue) > 0 Then

                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboStep1_Employee.SelectedValue)

                txtStep1_EmployeeCode.Text = objEmployee._Employeecode

                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmployee._Departmentunkid
                txtStep1_Department.Text = objDept._Name
                objDept = Nothing

                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                txtStep1_OfficePosition.Text = objJob._Job_Name
                objJob = Nothing

                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                txtStep1_WorkStation.Text = objClass._Name
                objClass = Nothing

                If objEmployee._Appointeddate.Date > Session("fin_startdate") Then
                    Session("FinStartDate") = objEmployee._Appointeddate.Date
                Else

                    Session("FinStartDate") = Session("fin_startdate")
                End If


                Dim EndDate As Date = Session("fin_enddate")
                If objEmployee._Termination_From_Date.Date <> Nothing Then

                    EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < Session("fin_enddate"), objEmployee._Termination_From_Date.Date, Session("fin_enddate")))
                End If
                If objEmployee._Termination_To_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate))
                End If

                If EndDate <> Session("fin_enddate") Then
                    Session("FinEndDate") = EndDate
                Else
                    Session("FinEndDate") = Session("fin_enddate")
                End If
            Else
                txtStep1_EmployeeCode.Text = ""
                txtStep1_Department.Text = ""
                txtStep1_OfficePosition.Text = ""
                txtStep1_WorkStation.Text = ""
                dtpStep1_Date.SetDate = DateAndTime.Today.Date
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim objMaster As New clsMasterData
        Dim blnApplyAccessFilter As Boolean = True
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsAssetList As DataSet
        'Sohail (07 Dec 2018) -- End

        Try

            If Session("LoginBy") = Global.User.en_loginby.User Then
                If CBool(Session("ViewAssetsDeclarationList")) = False Then Exit Sub

            Else
                blnApplyAccessFilter = False

            End If

            'Hemant (02 Jan 2019) -- Start
            'NMB - ENHANCEMENT
            'If CInt(drpEmployee.SelectedValue) <= 0 Then Exit Try
            If CInt(drpEmployee.SelectedValue) <= 0 Then Exit Try
            'Hemant (02 Jan 2019) -- End



            Dim dicDB As New Dictionary(Of String, String)


            Dim dicDBDate As New Dictionary(Of String, String)
            Dim dicDBYearId As New Dictionary(Of String, Integer)

            'Hemant (11 Feb 2019) -- Start
            'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
            'If chkIncludeClosedYearTrans.Checked = True Then
            '    Dim ds As DataSet = objMaster.Get_Database_Year_List("List", True, Session("CompanyUnkId"))
            '    dicDB = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearName = p.Item("financialyear_name").ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.YearName)


            '    dicDBDate = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .Date = (p.Item("start_date").ToString & "|" & p.Item("end_date").ToString).ToString}).ToDictionary(Function(x) x.DBName, Function(y) y.Date)
            '    dicDBYearId = (From p In ds.Tables("List") Select New With {Key .DBName = p.Item("database_name").ToString, Key .YearId = CInt(p.Item("yearunkid"))}).ToDictionary(Function(x) x.DBName, Function(y) y.YearId)


            'Else
            '    dicDB.Add(Session("Database_Name"), Session("FinancialYear_Name"))
            '    dicDBDate.Add(Session("Database_Name"), (eZeeDate.convertDate(Session("fin_startdate")) & "|" & eZeeDate.convertDate(Session("fin_enddate"))).ToString)
            '    dicDBYearId.Add(Session("Database_Name"), Session("Fin_year"))
            'End If
                dicDB.Add(Session("Database_Name"), Session("FinancialYear_Name"))
                dicDBDate.Add(Session("Database_Name"), (eZeeDate.convertDate(Session("fin_startdate")) & "|" & eZeeDate.convertDate(Session("fin_enddate"))).ToString)
                dicDBYearId.Add(Session("Database_Name"), Session("Fin_year"))
            'Hemant (11 Feb 2019) -- End


            dsAssetList = clsAssetdeclaration_masterT2.GetList(Session("UserId"), _
                                                             Session("CompanyUnkId"), _
                                                             Session("UserAccessModeSetting"), True, _
                                                             True, "List", _
                                                             dicDB, dicDBDate, dicDBYearId, , _
                                                             CInt(drpEmployee.SelectedValue), , , , , , "A.employeename", blnApplyAccessFilter, _
                                                             mdtFinStartDate, mdtFinEndDate, chkIncludeClosedYearTrans.Checked)
            'Hemant (15 Nov 2019) -- [Session("IsIncludeInactiveEmp") --> True]
            'Hemant (11 Feb 2019) -- [chkIncludeClosedYearTrans.Checked]
            'Hemant (05 Dec 2018) -- [mdtFinStartDate,mdtFinEndDate]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim blnRecordExist As Boolean = True
            If dsAssetList.Tables(0).Rows.Count <= 0 Then
                blnRecordExist = False
                    Dim r As DataRow = dsAssetList.Tables(0).NewRow
                    dsAssetList.Tables(0).Rows.Add(r)
                End If

                gvList.DataSource = dsAssetList
                gvList.DataBind()

            If blnRecordExist = False Then
                gvList.Rows(0).Visible = False
            End If
            'Sohail (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If (Not dsAssetList Is Nothing) Then

            '    If dsAssetList.Tables(0).Rows.Count = 0 Then
            '        Dim r As DataRow = dsAssetList.Tables(0).NewRow

            '        r.Item(3) = "None" ' To Hide the row and display only Row Header
            '        r.Item(4) = ""
            '        dsAssetList.Tables(0).Rows.Add(r)
            '    End If
            '    gvList.DataSource = dsAssetList
            '    gvList.DataBind()

            'Else
            '    DisplayMessage.DisplayError(ex, Me)
            'End If
            'Sohail (07 Dec 2018) -- End
        End Try
    End Sub

    Private Sub SetLabelRate()
        Try
            Select Case mvwAssetDeclaration.ActiveViewIndex

                Case 0
                    lblExchangeRate.Text = ""
                Case 1
                    drpCurrencyBusiness_SelectedIndexChanged(drpStep2_CurrencyBusiness, New EventArgs())
                Case 2
                    drpCurrencyBusiness_SelectedIndexChanged(drpStep3_CurrencyShare, New EventArgs())
                Case 3
                    drpCurrencyBusiness_SelectedIndexChanged(drpStep4_CurrencyBank, New EventArgs())
                Case 4
                    drpCurrencyBusiness_SelectedIndexChanged(drpStep5_CurrencyProperties, New EventArgs())
                Case 5
                    drpCurrencyBusiness_SelectedIndexChanged(drpStep6_OrigCurrencyLiabilities, New EventArgs())
                Case 7
                    drpCurrencyBusiness_SelectedIndexChanged(drpStep8_CurrencyMonthlyEarning, New EventArgs())

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Private Sub Update_DataGridview_DataTable_Extra_Columns(ByVal objdgview As GridView, ByRef drRow As DataRow, ByVal intCountryId As Integer)
    '    Try

    '        drRow.Item("userunkid") = -1

    '        If IsDBNull(drRow.Item("transactiondate")) Or drRow.Item("transactiondate") Is Nothing Then
    '            drRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
    '        End If

    '        Dim objExRate As New clsExchangeRate
    '        Dim dsList As DataSet
    '        Dim decBaseRate As Decimal = 0
    '        Dim decPaidRate As Decimal = 0
    '        Dim decPaidAmt As Decimal = 0
    '        Dim decPaidMarketvalue As Decimal = 0
    '        Dim decEstimateValue As Decimal = 0
    '        'Gajanan (06 Oct 2018) -- Start
    '        'Enhancement : Implementing New Module of Asset Declaration Template 2
    '        Dim decmonthlyannualearnings8 As Decimal = 0
    '        Dim decmarket_value9 As Decimal = 0
    '        Dim decamounts10 As Decimal = 0
    '        Dim decestimated_values11 As Decimal = 0
    '        'Gajanan(06 Oct 2018) -- End


    '        dsList = objExRate.GetList("ExRate", True, , , intCountryId, True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
    '        If dsList.Tables("ExRate").Rows.Count > 0 Then
    '            If objdgview.ID = gvLiabilities.ID Then
    '                drRow.Item("origcurrencyunkid") = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
    '                drRow.Item("origbasecurrencyunkid") = Me.ViewState("Exchangerateunkid")
    '                drRow.Item("outcurrencyunkid") = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
    '                drRow.Item("outbasecurrencyunkid") = Me.ViewState("Exchangerateunkid")
    '            Else
    '                drRow.Item("currencyunkid") = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
    '                drRow.Item("basecurrencyunkid") = Me.ViewState("Exchangerateunkid")
    '            End If

    '            decBaseRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
    '            decPaidRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))

    '            If objdgview.ID = gvBusiness.ID Then
    '                decPaidAmt = drRow.Item("monthly_annual_earnings")
    '            ElseIf objdgview.ID = gvStaffOwnerShip.ID Then
    '                decPaidMarketvalue = drRow.Item("market_value")
    '            ElseIf objdgview.ID = gvRealProperties.ID Then
    '                decEstimateValue = drRow.Item("estimated_value")

    '                'Gajanan (06 Oct 2018) -- Start
    '                'Enhancement : Implementing New Module of Asset Declaration Template 2
    '            ElseIf objdgview.ID = gvDependantBusiness.ID Then
    '                decmonthlyannualearnings8 = drRow.Item("monthly_annual_earnings")

    '            ElseIf objdgview.ID = gvDependantShare.ID Then
    '                decmarket_value9 = drRow.Item("market_value")

    '            ElseIf objdgview.ID = gvDependantBank.ID Then
    '                decamounts10 = drRow.Item("amount")

    '            ElseIf objdgview.ID = gvDependantProperties.ID Then
    '                decestimated_values11 = drRow.Item("estimated_value")
    '                'Gajanan(06 Oct 2018) -- End

    '                'Else
    '                '    decPaidAmt = drRow.Item("value")
    '            End If

    '            If objdgview.ID = gvLiabilities.ID Then
    '                drRow.Item("origbaseexchangerate") = decBaseRate
    '                drRow.Item("origexchangerate") = decPaidRate

    '                drRow.Item("outbaseexchangerate") = decBaseRate
    '                drRow.Item("outexchangerate") = decPaidRate

    '            Else
    '                drRow.Item("baseexchangerate") = decBaseRate
    '                drRow.Item("exchangerate") = decPaidRate

    '            End If

    '            If decPaidRate <> 0 Then
    '                If objdgview.ID = gvBusiness.ID Then
    '                    drRow.Item("basemonthly_annual_earnings") = decPaidAmt * decBaseRate / decPaidRate
    '                End If
    '                If objdgview.ID = gvStaffOwnerShip.ID Then
    '                    drRow.Item("basemarket_value") = decPaidMarketvalue * decBaseRate / decPaidRate
    '                End If
    '                If objdgview.ID = gvRealProperties.ID Then
    '                    drRow.Item("baseestimated_value") = decEstimateValue * decBaseRate / decPaidRate
    '                End If

    '                'Gajanan (06 Oct 2018) -- Start
    '                'Enhancement : Implementing New Module of Asset Declaration Template 2
    '                If objdgview.ID = gvDependantBusiness.ID Then
    '                    drRow.Item("basemonthly_annual_earnings") = decmonthlyannualearnings8 * decBaseRate / decPaidRate
    '                End If


    '                If objdgview.ID = gvDependantShare.ID Then
    '                    drRow.Item("market_value") = decmarket_value9 * decBaseRate / decPaidRate
    '                End If

    '                If objdgview.ID = gvDependantBank.ID Then
    '                    drRow.Item("amount") = decamounts10 * decBaseRate / decPaidRate
    '                End If

    '                If objdgview.ID = gvDependantProperties.ID Then
    '                    drRow.Item("estimated_value") = decestimated_values11 * decBaseRate / decPaidRate
    '                End If
    '                'Gajanan(06 Oct 2018) -- End
    '            Else
    '                If objdgview.ID = gvBusiness.ID Then
    '                    drRow.Item("basemonthly_annual_earnings") = decPaidAmt * decBaseRate
    '                End If
    '                If objdgview.ID = gvStaffOwnerShip.ID Then
    '                    drRow.Item("basemarket_value") = decPaidMarketvalue * decBaseRate
    '                End If
    '                If objdgview.ID = gvRealProperties.ID Then
    '                    drRow.Item("baseestimated_value") = decEstimateValue * decBaseRate
    '                End If

    '                'Gajanan (06 Oct 2018) -- Start
    '                'Enhancement : Implementing New Module of Asset Declaration Template 2
    '                If objdgview.ID = gvDependantBusiness.ID Then
    '                    drRow.Item("basemonthly_annual_earnings") = decmonthlyannualearnings8 * decBaseRate
    '                End If

    '                If objdgview.ID = gvDependantShare.ID Then
    '                    drRow.Item("market_value") = decmarket_value9 * decBaseRate
    '                End If

    '                If objdgview.ID = gvDependantBank.ID Then
    '                    drRow.Item("amount") = decamounts10 * decBaseRate
    '                End If

    '                If objdgview.ID = gvDependantProperties.ID Then
    '                    drRow.Item("estimated_value") = decestimated_values11 * decBaseRate
    '                End If

    '                'Gajanan(06 Oct 2018) -- End
    '            End If

    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Private Function GetCurrencyRate(ByVal intCountryUnkID As Integer, ByRef decBaseExRate As Decimal, ByRef decPaidExRate As Decimal, ByRef intPaidCurrencyunkid As Integer) As Boolean
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            dsList = objExRate.GetList("ExRate", True, , , intCountryUnkID, True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            decBaseExRate = 0
            decPaidExRate = 0
            intPaidCurrencyunkid = 0
            If dtTable.Rows.Count > 0 Then
                decBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                decPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                intPaidCurrencyunkid = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
    'Sohail (07 Dec 2018) -- End

    Private Function IsValidData() As Boolean
        Try

            lblExchangeRate.Text = ""
            lblStep1_EmployeeError.Text = ""

            'Hemant (23 Jan 2019) -- Start
            'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
            If CInt(cboStep1_FinancialYear.SelectedIndex) < 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Financial year is compulsory information.Please Select Financial year."), Me)
                cboStep1_FinancialYear.Focus()
                Return False
            End If
            'Hemant (23 Jan 2019) -- End

            If CInt(cboStep1_Employee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Employee is compulsory information.Please Select Employee."), Me)
                cboStep1_Employee.Focus()
                Return False
            End If

            If mintAssetDeclarationT2Unkid <= 0 Then
                Dim objAssetDeclare As New clsAssetdeclaration_masterT2
                'Hemant (05 Dec 2018) -- Start
                'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                'If objAssetDeclare.isExist(CInt(cboStep1_Employee.SelectedValue)) = True Then
                'lblStep1_EmployeeError.Text = "Sorry! Asset declaration for this employee is already exist."
                If objAssetDeclare.isExist(CInt(cboStep1_Employee.SelectedValue), 0, 0, Nothing, mdtFinStartDate, mdtFinEndDate) = True Then
                    'Sohail (12 Feb 2020) -- Start
                    'NMB issue # : Language not getting reflected.
                    'lblStep1_EmployeeError.Text = "Sorry! Asset declaration for this employee is already exist for selected financial year."
                    lblStep1_EmployeeError.Text = Language.getMessage(mstrModuleName1, 1, "Sorry! Asset declaration for this employee is already exist for selected financial year.")
                    'Sohail (12 Feb 2020) -- End
                    'Hemant (05 Dec 2018) -- End
                    cboStep1_Employee.Focus()
                    popupAddEdit.Show()
                    Return False
                End If

                If CheckUnLockEmployee(CInt(cboStep1_Employee.SelectedValue)) = False Then Return False
              
            End If

            If dtpStep1_Date.IsNull = True Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please enter Transaction Date.", Me)
                'Sohail (23 Mar 2019) -- End
                popupAddEdit.Show()
                dtpStep1_Date.Focus()
                Return False
                'Hemant (02 Jan 2019) -- Start
                'NMB - ENHANCEMENT
                'ElseIf dtpStep1_Date.GetDate < Session("fin_startdate") OrElse dtpStep1_Date.GetDate > Session("fin_enddate") Then
                '    DisplayMessage.DisplayError(ex, Me)
                '    popupAddEdit.Show()
                '    dtpStep1_Date.Focus()
                '    Return False
                'Hemant (02 Jan 2019) -- End
            End If

            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            If txtStep1_Tin_No.Text.Length <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Please enter Tin Number."), Me)
                'Sohail (23 Mar 2019) -- End
                popupAddEdit.Show()
                txtStep1_Tin_No.Enabled = True
                txtStep1_Tin_No.Focus()
                Return False
            End If
            Dim strTinNumber As String = String.Empty
            strTinNumber = txtStep1_Tin_No.Text.Replace("_", "")
            If strTinNumber.Length < 11 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Tin Number Must be 9 Digit"), Me)
                'Sohail (23 Mar 2019) -- End
                popupAddEdit.Show()
                txtStep1_Tin_No.Enabled = True
                txtStep1_Tin_No.Focus()
                Return False
            End If
            'Hemant (19 Nov 2018) -- End

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub activemenu(ByVal position As Integer)
        Try
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "removecss", "removeactiveclass();", True)
            For Each c As Control In Panelmenu.Controls
                If TypeOf c Is LinkButton Then
                    Dim lb As LinkButton = Panelmenu.FindControl(c.ID.ToString())
                    If CInt(lb.Attributes("position").ToString) = position Then
                        lb.CssClass = "active"
                    Else
                        lb.CssClass = ""
                    End If

                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (19 Nov 2018) -- Start
    'Enhancement : Changes for NMB Requirement for Email Notification After Final Saved In Asset Declaration 
    Private Sub Send_Notification(ByVal lstWebEmail As List(Of clsEmailCollection))
        'Hemant (02 Feb 2022) -- [lstWebEmail]
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'If gobjEmailList.Count > 0 Then
            If lstWebEmail.Count > 0 Then
                'Hemant (02 Feb 2022) -- End
                Dim objSendMail As New clsSendMail
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                'For Each obj In gobjEmailList
                For Each obj In lstWebEmail
                    'Hemant (02 Feb 2022) -- End
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                    Catch ex As Exception

                    End Try
                Next
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                'gobjEmailList.Clear()
                lstWebEmail.Clear()
                'Hemant (02 Feb 2022) -- End
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'If gobjEmailList.Count > 0 Then
            '    gobjEmailList.Clear()
            'End If
            If lstWebEmail.Count > 0 Then
                lstWebEmail.Clear()
            End If
            'Hemant (02 Feb 2022) -- End
        End Try
    End Sub
    'Hemant (19 Nov 2018) -- End


    'Pinkal (03-Dec-2018) -- Start
    'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.

    Private Function CheckUnLockEmployee(ByVal xEmployeeId As Integer) As Boolean
        Try
            Dim mdtDeclarationStartDate As Date = Nothing
            Dim mdtDeclarationEndDate As Date = Nothing

            If (Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").ToString().Length > 0) Then
                mdtDeclarationStartDate = eZeeDate.convertDate(Session("AssetDeclarationFromDate").ToString()).Date
            End If
            If Session("AssetDeclarationToDate") IsNot Nothing AndAlso Session("AssetDeclarationToDate").ToString().Length > 0 Then
                mdtDeclarationEndDate = eZeeDate.convertDate(Session("AssetDeclarationToDate").ToString()).Date
            End If

            If mdtDeclarationStartDate <> Nothing OrElse mdtDeclarationEndDate <> Nothing Then
                'Hemant (01 Mar 2022) -- Start            
                'Dim mintEmpUnLockDaysAfterAssetDeclarationToDate As Integer = CInt(Session("UnLockDaysAfterAssetDeclarationToDate"))
                Dim mintDontAllowAssetDeclarationAfterDays As Integer = CInt(Session("DontAllowAssetDeclarationAfterDays"))
                'Hemant (01 Mar 2022) -- End
                Dim mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate As Integer = CInt(Session("NewEmpUnLockDaysforAssetDecWithinAppointmentDate"))

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = xEmployeeId

                Dim mdtDate As Date = objEmployee._Appointeddate.Date

                If objEmployee._Reinstatementdate <> Nothing AndAlso objEmployee._Appointeddate.Date < objEmployee._Reinstatementdate.Date Then
                    mdtDate = objEmployee._Reinstatementdate.Date
                End If


                If mdtDate.Date < mdtDeclarationStartDate.Date Then  'CONSIDER AS OLD EMPLOYEE
                    'If mdtDeclarationEndDate.AddDays(mintEmpUnLockDaysAfterAssetDeclarationToDate).Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    'Hemant (01 Mar 2022) -- Start            
                    'If DateDiff(DateInterval.Day, mdtDeclarationEndDate, ConfigParameter._Object._CurrentDateAndTime.Date) > 0 AndAlso DateDiff(DateInterval.Day, mdtDeclarationEndDate, ConfigParameter._Object._CurrentDateAndTime.Date) <= mintEmpUnLockDaysAfterAssetDeclarationToDate Then
                    If DateDiff(DateInterval.Day, mdtDeclarationEndDate, ConfigParameter._Object._CurrentDateAndTime.Date) > 0 AndAlso DateDiff(DateInterval.Day, mdtDeclarationEndDate, ConfigParameter._Object._CurrentDateAndTime.Date) > mintDontAllowAssetDeclarationAfterDays Then
                        'Hemant (01 Mar 2022) -- End
                        If LockUnlockEmployee(True) = False Then Return False
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 91, "Sorry, you cannot do your declaration as last date of declaration has been passed.") & Language.getMessage(mstrModuleName1, 92, "Please contact your administrator/manager to do futher operation on it."), Me)
                        Return False
                    Else
                        If LockUnlockEmployee(False) = False Then Return False
                    End If

                ElseIf mdtDate.Date > mdtDeclarationStartDate.Date Then    'CONSIDER AS NEW EMPLOYEE
                    'If mdtDate.AddDays(mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate).Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    If mdtDate.AddDays(mintNewEmpUnLockDaysforAssetDecWithinAppointmentDate).Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                        If LockUnlockEmployee(True) = False Then Return False
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 91, "Sorry, you cannot do your declaration as last date of declaration has been passed.") & Language.getMessage(mstrModuleName1, 92, "Please contact your administrator/manager to do futher operation on it."), Me)
                        Return False
                    End If
                End If
                objEmployee = Nothing
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Function LockUnlockEmployee(ByVal blnIslock As Boolean) As Boolean
        Dim objLockUnlock As New clsassetdecemp_lockunlock
        Try
            Dim mintAdLockId As Integer = 0
            Dim mblnisLock As Boolean = False
            If objLockUnlock.isExist(CInt(cboStep1_Employee.SelectedValue), mblnisLock, mintAdLockId) Then
                If mintAdLockId > 0 And blnIslock = mblnisLock Then Return True
                objLockUnlock._Adlockunkid = mintAdLockId
            End If

            objLockUnlock._Employeeunkid = CInt(cboStep1_Employee.SelectedValue)
            objLockUnlock._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockUnlock._Islock = blnIslock

            If (Session("loginBy") = Global.User.en_loginby.User) Then
                If blnIslock Then
                    objLockUnlock._Lockuserunkid = CInt(Session("UserId"))
                Else
                    objLockUnlock._Unlockuserunkid = CInt(Session("UserId"))
                End If
                objLockUnlock._Loginemployeeunkid = 0
                objLockUnlock._AuditUserunkid = CInt(Session("UserId"))
            ElseIf (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objLockUnlock._Lockuserunkid = 0
                objLockUnlock._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                If blnIslock = False Then
                    objLockUnlock._Unlockuserunkid = -999 'THIS MEANS EMPLOYEE HIMSELF UNLOCK IT BY COMPLETING LOCKING TENURE AND APPLY FOR NEW DECLARATION.
                    objLockUnlock._AuditUserunkid = -999
                End If
            End If

            objLockUnlock._ClientIp = Session("IP_ADD")
            objLockUnlock._AuditDateTime = ConfigParameter._Object._CurrentDateAndTime
            objLockUnlock._HostName = Session("HOST_NAME")
            objLockUnlock._FormName = mstrModuleName1
            objLockUnlock._IsFromWeb = True

            If blnIslock AndAlso mintAdLockId <= 0 Then
                objLockUnlock._Isunlocktenure = False
                If objLockUnlock.Insert() = False Then
                    DisplayMessage.DisplayMessage(objLockUnlock._Message, Me)
                    Return False
                End If
            ElseIf blnIslock = False AndAlso mintAdLockId > 0 Then
                objLockUnlock._Isunlocktenure = True
                If objLockUnlock.Update() = False Then
                    DisplayMessage.DisplayMessage(objLockUnlock._Message, Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objLockUnlock = Nothing
        End Try
    End Function

    'Pinkal (03-Dec-2018) -- End

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Private Function SetValueAssetDeclaratationMaster() As clsAssetdeclaration_masterT2
        Dim objAssetDeclare As New clsAssetdeclaration_masterT2
        Try
            objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            objAssetDeclare._Employeeunkid = CInt(cboStep1_Employee.SelectedValue)
            If txtStep1_Tin_No.Text.Trim = "" Then
                objAssetDeclare._Tin_No = 0
            Else
                objAssetDeclare._Tin_No = txtStep1_Tin_No.Text.Trim
            End If

            objAssetDeclare._IsAcknowledged = chkAcknowledgement_Step13.Checked

            objAssetDeclare._Finyear_Start = Session("FinStartDate")
            objAssetDeclare._Finyear_End = Session("FinEndDate")
            objAssetDeclare._FinancialYearunkid = CInt(cboStep1_FinancialYear.SelectedValue)
            'Hemant (02 Jan 2019) -- Start
            'NMB - ENHANCEMENT
            'If objAssetDeclare._FinancialYearunkid = -99 Then
            If objAssetDeclare._FinancialYearunkid = -99 OrElse objAssetDeclare._FinancialYearunkid <> CInt(Session("Fin_year")) Then
                'Hemant (02 Jan 2019) -- End
                objAssetDeclare._Finyear_Start = mdtFinStartDate
                objAssetDeclare._Finyear_End = mdtFinEndDate
            End If


            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._Loginemployeeunkid = -1
            End If
            objAssetDeclare._Isvoid = False
            objAssetDeclare._Voiddatetime = Nothing
            objAssetDeclare._Voidreason = ""
            objAssetDeclare._Voiduserunkid = -1
            objAssetDeclare._TransactionDate = dtpStep1_Date.GetDate

            objAssetDeclare._Savedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            objAssetDeclare._HostName = CStr(Session("HOST_NAME"))
            objAssetDeclare._FormName = mstrModuleName1
            objAssetDeclare._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return objAssetDeclare
    End Function
    'Sohail (07 Dec 2018) -- End

    'Hemant (02 Feb 2022) -- Start            
    'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
    Private Sub FillCurrencyCombo()
        Dim objExchange As New clsExchangeRate
        Dim dsCombo As DataSet

        Try
            dsCombo = objExchange.getComboList("Currency", True)
            With drpStep2_CurrencyBusiness
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency")
                .DataBind()
            End With

            With drpStep3_CurrencyShare
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep4_CurrencyBank
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep5_CurrencyProperties
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep6_OrigCurrencyLiabilities
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep6_OutCurrencyLiabilities
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep8_CurrencyMonthlyEarning
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep9_Dependantmarketvalue
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep10_DependantBankCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

            With drpStep11_DependantPropertiesEstimatevalue
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("Currency").Copy
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objExchange = Nothing
        End Try
    End Sub

    Private Sub FillRelationsCombo()
        Dim dsCombos As DataSet
        Try

            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
            With drpStep7_Relationship
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Relations").Copy
                .DataBind()
                .SelectedValue = 0
            End With

            With drpStep8_RelationShip
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relations").Copy
                .DataBind()
            End With

            With drpStep9_DependantRelationship
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relations").Copy
                .DataBind()
            End With

            With drpStep10_RelationShip
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relations").Copy
                .DataBind()
            End With

            With drpStep11_RelationShip
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relations").Copy
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAssetSectorCombo()
        Dim dsCombos As DataSet
        Try
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")
            With cboStep2_Sector
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Asset Sector").Copy
                .DataBind()
                .SelectedValue = 0
            End With

            With drpStep8_Sector
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Asset Sector").Copy
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (02 Feb 2022) -- End

#End Region

#Region " Page's Event "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintAssetDeclarationt2Unkid", mintAssetDeclarationT2Unkid)
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Me.ViewState.Add("dsAssetList", dsAssetList)
            'Me.ViewState.Add("dtBusiness", dtBusiness)
            'Me.ViewState.Add("dtShare", dtStaffOwnership)
            'Me.ViewState.Add("dtBank", dtBankAccounts)
            'Me.ViewState.Add("dtProperties", dtProperties)
            'Me.ViewState.Add("dtLiabilities", dtLiabilities)
            'Me.ViewState.Add("dtRelatives", dtRelatives)
            ''Gajanan (06 Oct 2018) -- Start
            ''Enhancement : Implementing New Module of Asset Declaration Template 2
            'Me.ViewState.Add("dtDependantBusinessS8", dtDependantBusinessS8)
            'Me.ViewState.Add("dtDependantShareS9", dtDependantShareS9)
            'Me.ViewState.Add("dtDependantBank10", dtDependantBank10)
            'Me.ViewState.Add("dtDependantPropertiesS11", dtDependantPropertiesS11)
            ''Gajanan(06 Oct 2018) -- End 

            'Me.ViewState.Add("dtCurrBusiness", dsCurrBusiness)
            'Me.ViewState.Add("dtCurrShare", dsCurrShare)
            'Me.ViewState.Add("dtCurrBank", dsCurrBank)
            'Me.ViewState.Add("dtCurrProperties", dsCurrProperties)
            'Me.ViewState.Add("dtCurrLiabilities", dsCurrLiabilities)
            'Me.ViewState.Add("dtCurrRelatives", dsCurrRelatives)
            'Me.ViewState.Add("dtCurrOtherBusiness", dsCurrOtherBusiness)

            'Me.ViewState.Add("dsCurrResources", dsCurrResources)
            'Me.ViewState.Add("dsCurrDebt", dsCurrDebt)
            'Sohail (07 Dec 2018) -- End

            Me.ViewState.Add("SrNo", Me.ViewState("SrNo"))
            Me.ViewState.Add("BusinessSrNo", Me.ViewState("BusinessSrNo"))
            Me.ViewState.Add("ShareSrNo", Me.ViewState("ShareSrNo"))
            Me.ViewState.Add("BankSrNo", Me.ViewState("BankSrNo"))
            Me.ViewState.Add("PropertiesSrNo", Me.ViewState("PropertiesSrNo"))
            Me.ViewState.Add("LiabilitiesSrNo", Me.ViewState("LiabilitiesSrNo"))
            Me.ViewState.Add("RelativesSrNo", Me.ViewState("RelativesSrNo"))
            Me.ViewState.Add("OtherBusinessSrNo", Me.ViewState("OtherBusinessSrNo"))
            'Gajanan (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            Me.ViewState.Add("DependantBusinessSrNo", Me.ViewState("DependantBusinessSrNo"))
            Me.ViewState.Add("DependantShareSrNo", Me.ViewState("DependantShareSrNo"))
            Me.ViewState.Add("DependantBankSrNo", Me.ViewState("DependantBankSrNo"))
            Me.ViewState.Add("DependantPropertiesSrNo", Me.ViewState("DependantPropertiesSrNo"))
            'Gajanan(06 Oct 2018) -- End


            Me.ViewState.Add("ResourcesNo", Me.ViewState("ResourcesNo"))
            Me.ViewState.Add("DebtNo", Me.ViewState("DebtNo"))


            Me.ViewState.Add("DelUnkId", Me.ViewState("DelUnkId"))
            Me.ViewState.Add("FinalSaveUnkId", Me.ViewState("FinalSaveUnkId"))
            Me.ViewState.Add("UnlockFinalSaveUnkId", Me.ViewState("UnlockFinalSaveUnkId"))

            Me.ViewState.Add("blnpopupAddEdit", blnpopupAddEdit)

            Me.ViewState.Add("mintOldEmployeeUnkId", mintOldEmployeeUnkId)
            Me.ViewState.Add("mdtOldTransactionDate", mdtOldTransactionDate)
            Me.ViewState.Add("mstrOldTinNo", mstrOldTinNo)
            Me.ViewState.Add("menAction", menAction)
            Me.ViewState.Add("mblnIsbtnEditClick", mblnIsbtnEditClick)
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            Me.ViewState.Add("mblnOldIsAcknowledged", mblnOldIsAcknowledged)
            'Hemant (15 Nov 2018) -- End

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Me.ViewState.Add("dtDependantList", dtDependantList)
            'Sohail (07 Dec 2018) -- End
            'Hemant (03 Dec 2018) -- End

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Me.ViewState.Add("dtFinYearList", dtFinYearList)
            'Sohail (07 Dec 2018) -- End
            Me.ViewState.Add("mdtFinStartDate", mdtFinStartDate)
            Me.ViewState.Add("mdtFinEndDate", mdtFinEndDate)
            'Hemant (05 Dec 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (22 Nov 2018) -- Start
            'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
            'Call SetLanguage()
            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                'Sohail (22 Feb 2020) -- Start
                'NMB Enhancement # : Allow to final save asset declaration on add / edit screen.
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    btnFinalSave.Enabled = CBool(Session("FinalSaveAssetDeclaration"))
                Else
                    btnFinalSave.Enabled = True
                End If
                'Sohail (22 Feb 2020) -- End
            End If
            'Sohail (22 Nov 2018) -- End


            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Assets_Declarations) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If Not IsPostBack AndAlso Session("LoginBy") = Global.User.en_loginby.User Then
                BtnNew.Visible = Session("AddAssetDeclaration")
            End If


            If Not IsPostBack AndAlso mintAssetDeclarationT2Unkid <= 0 Then

                Call FillCombo()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Call FillCurrencyCombo()
                Call FillRelationsCombo()
                Call FillAssetSectorCombo()
                'Hemant (02 Feb 2022) -- End
                Call FillComboBusinessDealings()
                Call FillComboShare()
                Call FillComboBank()
                Call FillComboProperties()
                Call FillComboLiabilities()
                Call FillComboRelatives()

                'Gajanan (06 Oct 2018) -- Start
                'Enhancement : Implementing New Module of Asset Declaration Template 2
                Call FillComboDeptBusiness()
                Call FillComboDeptShare()
                Call FillComboDeptBank()
                Call FillComboDeptProperties()
                'Gajanan(06 Oct 2018) -- End

                Call FillBusinessGrid()
                Call FillStaffOwnerShipGrid()
                Call FillBankAccountsGrid()
                Call FillRealPropertiesGrid()
                Call FillLiabilitiesGrid()
                Call FillRelativesGrid()

                'Gajanan (06 Oct 2018) -- Start
                'Enhancement : Implementing New Module of Asset Declaration Template 2
                Call FillDeptBusinessGrid()
                Call FillDeptShareGrid()
                Call FillDeptBankGrid()
                Call FillDeptPropertiesGrid()
                'Gajanan(06 Oct 2018) -- End

                If Not Page.ClientScript.IsStartupScriptRegistered("load") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "load", "GetInstruct('');", True)
                End If

                dtpStep2_RegistrationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

                txtRelativeEmployee.Visible = False


                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    gvList.Columns(0).Visible = CBool(Session("EditAssetDeclaration"))
                    gvList.Columns(1).Visible = CBool(Session("DeleteAssetDeclaration"))
                    gvList.Columns(2).Visible = CBool(Session("FinalSaveAssetDeclaration"))
                    gvList.Columns(3).Visible = CBool(Session("UnlockFinalSaveAssetDeclaration")) 'Sohail (27 Feb 2015)
                End If

            Else

                'dsAssetList = Me.ViewState("dsAssetList") 'Sohail (07 Dec 2018)
                mintAssetDeclarationT2Unkid = Me.ViewState("mintAssetDeclarationt2Unkid")

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtBusiness = Me.ViewState("dtBusiness")
                'dtStaffOwnership = Me.ViewState("dtShare")
                'dtBankAccounts = Me.ViewState("dtBank")
                'dtProperties = Me.ViewState("dtProperties")
                'dtLiabilities = Me.ViewState("dtLiabilities")
                'dtRelatives = Me.ViewState("dtRelatives")

                ''Gajanan (06 Oct 2018) -- Start
                ''Enhancement : Implementing New Module of Asset Declaration Template 2
                'dtDependantBusinessS8 = Me.ViewState("dtDependantBusinessS8")
                'dtDependantShareS9 = Me.ViewState("dtDependantShareS9")
                'dtDependantBank10 = Me.ViewState("dtDependantBank10")
                'dtDependantPropertiesS11 = Me.ViewState("dtDependantPropertiesS11")
                ''Gajanan(06 Oct 2018) -- End
                'Sohail (07 Dec 2018) -- End

                Dim str As String = Session("AssetDeclarationInstruction").ToString.Replace(vbCrLf, "<br />")
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "load", "GetInstruct('" & str & "');", True)

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dsCurrBusiness = Me.ViewState("dsCurrBusiness")
                'dsCurrShare = Me.ViewState("dsCurrShare")
                'dsCurrBank = Me.ViewState("dsCurrBank")
                'dsCurrProperties = Me.ViewState("dsCurrProperties")
                'dsCurrLiabilities = Me.ViewState("dsCurrLiabilities")
                'dsCurrRelatives = Me.ViewState("dsCurrRelatives")
                'dsCurrOtherBusiness = Me.ViewState("dsCurrOtherBusiness")

                'dsCurrResources = Me.ViewState("dsCurrResources")
                'dsCurrDebt = Me.ViewState("dsCurrDebt")
                'Sohail (07 Dec 2018) -- End

                blnpopupAddEdit = Me.ViewState("blnpopupAddEdit")

                mintOldEmployeeUnkId = Me.ViewState("mintOldEmployeeUnkId")
                mdtOldTransactionDate = Me.ViewState("mdtOldTransactionDate")
                mstrOldTinNo = Me.ViewState("mstrOldTinNo")

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                mblnOldIsAcknowledged = Me.ViewState("mblnOldIsAcknowledged")
                'Hemant (15 Nov 2018) -- End

                menAction = Me.ViewState("menAction")
                mblnIsbtnEditClick = Me.ViewState("mblnIsbtnEditClick")

                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtDependantList = Me.ViewState("dtDependantList")
                'Sohail (07 Dec 2018) -- End
                'Hemant (03 Dec 2018) -- End

                'Hemant (05 Dec 2018) -- Start
                'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtFinYearList = Me.ViewState("dtFinYearList")
                'Sohail (07 Dec 2018) -- End
                mdtFinStartDate = Me.ViewState("mdtFinStartDate")
                mdtFinEndDate = Me.ViewState("mdtFinEndDate")
                'Hemant (05 Dec 2018) -- End


                If blnpopupAddEdit Then
                    popupAddEdit.Show()
                Else
                    popupAddEdit.Hide()
                End If


                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                ''START FOR CHECK WHETHER PopupAddEdit Open or NOT

                'Dim c As Control = Nothing
                'Dim ctrlStr As String = String.Empty
                'For Each ctl As String In Page.Request.Form

                '    If ctl Is Nothing Or ctl = "" Then Continue For

                '    If ctl.EndsWith(".x") OrElse ctl.EndsWith(".y") Then
                '        ctrlStr = ctl.Substring(0, ctl.Length - 2)
                '        c = Page.FindControl(ctrlStr)
                '    Else
                '        c = Page.FindControl(ctl)
                '    End If
                '    If (c IsNot Nothing AndAlso TypeOf c Is System.Web.UI.WebControls.Button OrElse TypeOf c Is System.Web.UI.WebControls.ImageButton) AndAlso _
                '        (c.ID = BtnSearch.ID OrElse c.ID = BtnReset.ID OrElse c.ID = popupYesNo.ID OrElse c.ID = popupFinalYesNo.ID OrElse c.ID = popupUnlockFinalYesNo.ID OrElse c.ID = "btnGvRemove" OrElse c.ID = "btnGvFinalSave" OrElse c.ID = "btnGvUnlockFinalSave") Then
                '        'Me.ViewState("OpenPopupAddEdit") = False
                '    End If

                '    'END FOR CHECK WHETHER PopupAddEdit Open or NOT

                'Next
                'Hemant (02 Feb 2022) -- End



                'If Me.ViewState("OpenPopupAddEdit") IsNot Nothing AndAlso CBool(Me.ViewState("OpenPopupAddEdit")) Then
                '    popupAddEdit.Show()
                'End If



            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Button's Event(s) "


    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try

            'Pinkal (03-Dec-2018) -- Start
            'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
            If (Session("LoginBy") = Global.User.en_loginby.Employee) AndAlso CInt(drpEmployee.SelectedValue) > 0 Then
                If CheckUnLockEmployee(CInt(drpEmployee.SelectedValue)) = False Then Exit Sub
            End If
            'Pinkal (03-Dec-2018) -- End

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            'Hemant (23 Jan 2019) -- Start
            'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
            If cboStep1_FinancialYear.SelectedIndex >= 0 Then
                'Hemant (23 Jan 2019) -- End
            cboStep1_FinancialYear.SelectedIndex = 0
            cboStep1_FinancialYear_SelectedIndexChanged(Nothing, Nothing)
            cboStep1_FinancialYear.Enabled = True
            End If


            'Hemant (05 Dec 2018) -- End

            'Hemant (10 Dec 2018) -- Start
            'Hemant (23 Jan 2019) -- Start
            'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
            'If Session("AssetDeclarationFromDate") IsNot Nothing AndAlso Session("AssetDeclarationFromDate").Trim.ToString <> "" Then
            '    cboStep1_FinancialYear.SelectedValue = -99
            '    cboStep1_FinancialYear_SelectedIndexChanged(Nothing, Nothing)
            '    'Hemant (02 Jan 2019) -- Start
            '    'NMB - ENHANCEMENT
            '    'cboStep1_FinancialYear.Enabled = False
            '    'Hemant (02 Jan 2019) -- End
            'End If
            'Hemant (23 Jan 2019) -- End
            'Hemant (10 Dec 2018) -- End

            cboStep1_Employee.SelectedIndex = 0
            'Hemant (22 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            'dtpDate.Enabled = True
            dtpStep1_Date.Enabled = False
            'Hemant (22 Nov 2018) -- End
            cboStep1_Employee.Enabled = True
            txtStep1_EmployeeCode.Enabled = True
            txtStep1_OfficePosition.Enabled = True
            txtStep1_Department.Enabled = True
            txtStep1_WorkStation.Enabled = True
            lblStep1_EmployeeError.Visible = True
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            txtStep1_Tin_No.Enabled = True
            'Hemant (19 Nov 2018) -- End
            lblExchangeRate.Text = ""
            Reset_Business()
            Reset_BankAccounts()
            Reset_Relatives()
            Reset_RealProperties()
            Reset_StaffOwnerShip()
            Reset_Liabilities()
            Reset_DeptBusiness()
            Reset_DependantShare()
            Reset_DeptBank()
            Reset_DeptProperties()

            dtpStep1_Date.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            txtStep1_EmployeeCode.Text = ""
            txtStep1_OfficePosition.Text = ""
            txtStep1_Department.Text = ""
            txtStep1_WorkStation.Text = ""
            'Hemant (22 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            'txtTin_No.Text = ""
            txtStep1_Tin_No.Text = "000-000-000"
            'Hemant (22 Nov 2018) -- End


            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            chkAcknowledgement_Step13.Checked = False
            'Hemant (15 Nov 2018) -- End

            Call FillBusinessGrid()
            Call FillStaffOwnerShipGrid()
            Call FillBankAccountsGrid()
            Call FillRealPropertiesGrid()
            Call FillLiabilitiesGrid()
            Call FillRelativesGrid()

            'Gajanan (06 Oct 2018) -- Start
            'Enhancement : Implementing New Module of Asset Declaration Template 2
            Call FillDeptBusinessGrid()
            Call FillDeptShareGrid()
            Call FillDeptBankGrid()
            Call FillDeptPropertiesGrid()
            'Gajanan(06 Oct 2018) -- End

            mintAssetDeclarationT2Unkid = -1
            lblStep1_EmployeeError.Text = ""
            mvwAssetDeclaration.ActiveViewIndex = 0
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            cboStep1_Employee_SelectedIndexChanged(Nothing, Nothing)
            'Hemant (19 Nov 2018) -- End
            blnpopupAddEdit = True
            popupAddEdit.Show()

            menAction = enAction.ADD_CONTINUE
            mblnIsbtnEditClick = False
            activemenu(0)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Hemant (23 Jan 2019) -- Start
            'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
            If CInt(drpFinalcialYear.SelectedIndex) < 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Please Select Financial Year to Continue"), Me)
                Exit Sub
            End If
            'Hemant (23 Jan 2019) -- End
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Please Select Employee to Continue"), Me)
                Exit Sub
            End If
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpEmployee.SelectedIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try


            If IsValidData() = False Then
                Exit Sub
            End If

            If mvwAssetDeclaration.ActiveViewIndex = 0 Then
                If mblnIsbtnEditClick = True AndAlso (mintOldEmployeeUnkId <> cboStep1_Employee.SelectedValue Or mdtOldTransactionDate <> dtpStep1_Date.GetDate Or mstrOldTinNo <> txtStep1_Tin_No.Text.Trim _
                                                      Or mblnOldIsAcknowledged <> chkAcknowledgement_Step13.Checked) Then
                    'Hemant (15 Nov 2018) -- [mblnOldIsAcknowledged]
                    menAction = enAction.EDIT_ONE
                End If
            End If

            If mvwAssetDeclaration.ActiveViewIndex < mvwAssetDeclaration.Views.Count - 1 Then
                mvwAssetDeclaration.ActiveViewIndex += 1

                activemenu(mvwAssetDeclaration.ActiveViewIndex)

                SetLabelRate()
            Else
                popupYesNo.Show()
            End If



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If mvwAssetDeclaration.ActiveViewIndex > 0 Then
                mvwAssetDeclaration.ActiveViewIndex -= 1

                activemenu(mvwAssetDeclaration.ActiveViewIndex)


                SetLabelRate()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup1.buttonDelReasonYes_Click
        Try

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmpAssetDeclarationList"
            StrModuleName2 = "mnuAssetDeclaration"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")


            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If


            Dim objAssetDeclare As New clsAssetdeclaration_masterT2
            objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(Me.ViewState("DelUnkId"))
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._VoidLoginEmployeeunkid = Session("Employeeunkid")
            Else
                objAssetDeclare._VoidLoginEmployeeunkid = -1
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            objAssetDeclare._HostName = CStr(Session("HOST_NAME"))

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetDeclare._FormName = mstrModuleName1
            objAssetDeclare._FormName = mstrModuleName
            'Hemant (07 Feb 2019) -- End

            objAssetDeclare._IsFromWeb = True

            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare.Void(CInt(Me.ViewState("DelUnkId")), -1, ConfigParameter._Object._CurrentDateAndTime, popup1.Reason) ' txtreasondel.Text SHANI [17 JAN 2015]
            Else
                objAssetDeclare.Void(CInt(Me.ViewState("DelUnkId")), Session("UserId"), ConfigParameter._Object._CurrentDateAndTime, popup1.Reason) ' txtreasondel.Text SHANI [17 JAN 2015]
            End If
            Call FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub btnSaveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonYes_Click
        Dim objAssetDeclare As New clsAssetdeclaration_masterT2
        Try


            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmpAssetDeclarationT2"
            StrModuleName2 = "mnuAssetDeclaration"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")


            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If


            'objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'objAssetDeclare._Employeeunkid = CInt(cboStep1_Employee.SelectedValue)
            'If txtStep1_Tin_No.Text.Trim = "" Then
            '    objAssetDeclare._Tin_No = 0
            'Else
            '    objAssetDeclare._Tin_No = txtStep1_Tin_No.Text.Trim
            'End If

            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'objAssetDeclare._IsAcknowledged = chkAcknowledgement_Step13.Checked
            ''Hemant (15 Nov 2018) -- End

            'objAssetDeclare._Finyear_Start = Session("FinStartDate")
            'objAssetDeclare._Finyear_End = Session("FinEndDate")
            ''Hemant (05 Dec 2018) -- Start
            ''NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2            
            'objAssetDeclare._FinancialYearunkid = CInt(cboStep1_FinancialYear.SelectedValue)
            'If objAssetDeclare._FinancialYearunkid = -99 Then
            '    objAssetDeclare._Finyear_Start = mdtFinStartDate
            '    objAssetDeclare._Finyear_End = mdtFinEndDate
            'End If
            ''Hemant (05 Dec 2018) -- End

            
            ''objAssetDeclare._Resources = txtResources.Text.Trim

            'If (Session("loginBy") = Global.User.en_loginby.Employee) Then
            '    objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
            '    objAssetDeclare._Userunkid = -1
            'Else
            '    objAssetDeclare._Userunkid = Session("UserId")
            '    objAssetDeclare._Loginemployeeunkid = -1
            'End If
            'objAssetDeclare._Isvoid = False
            'objAssetDeclare._Voiddatetime = Nothing
            'objAssetDeclare._Voidreason = ""
            'objAssetDeclare._Voiduserunkid = -1
            'objAssetDeclare._TransactionDate = dtpStep1_Date.GetDate
            objAssetDeclare = SetValueAssetDeclaratationMaster()
            'Sohail (07 Dec 2018) -- End

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow() As DataRow

            'drRow = dtBusiness.Select("company_org_name = 'None' AND company_tin_no = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtBusiness.AcceptChanges()
            'End If

            'drRow = dtStaffOwnership.Select("certificate_no = 'None' AND securitiestypeunkid = -1 ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtStaffOwnership.AcceptChanges()
            'End If

            ''Hemant (05 Dec 2018) -- Start
            ''NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2 assetbankt2tranunkid = -1 and assetdeclarationt2unkid = -1
            ''drRow = dtBankAccounts.Select(" bank_name = 'None' AND account_type = '' ")
            'drRow = dtBankAccounts.Select(" assetbankt2tranunkid = -1 and assetdeclarationt2unkid = -1 ")
            ''Hemant (05 Dec 2018) -- End
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtBankAccounts.AcceptChanges()
            'End If

            'drRow = dtProperties.Select("asset_name = 'None' AND registration_title_no = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtProperties.AcceptChanges()
            'End If

            'drRow = dtLiabilities.Select("institution_name = 'None' AND liability_type = '' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtLiabilities.AcceptChanges()
            'End If

            'drRow = dtRelatives.Select("relative_employeeunkid = '-1' AND relationshipunkid = '-1' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtRelatives.AcceptChanges()
            'End If

            ''Gajanan (06 Oct 2018) -- Start
            ''Enhancement : Implementing New Module of Asset Declaration Template 2
            'drRow = dtDependantBusinessS8.Select("assetbusinessdealt2depntranunkid = '-2' AND assetdeclarationt2unkid = '-2' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtDependantBusinessS8.AcceptChanges()
            'End If


            'drRow = dtDependantShareS9.Select("assetsecuritiest2depntranunkid = '-2' AND assetdeclarationt2unkid = '-2' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtDependantShareS9.AcceptChanges()
            'End If

            'drRow = dtDependantBank10.Select("assetbankt2depntranunkid = '-2' AND assetdeclarationt2unkid = '-2' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtDependantBank10.AcceptChanges()
            'End If

            'drRow = dtDependantPropertiesS11.Select("assetpropertiest2depntranunkid = '-2' AND assetdeclarationt2unkid = '-2' ")
            'If drRow.Length > 0 Then
            '    drRow(0).Delete()
            '    dtDependantPropertiesS11.AcceptChanges()
            'End If
            ''Gajanan(06 Oct 2018) -- End
            'Sohail (07 Dec 2018) -- End

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'objAssetDeclare._Datasource_Business = dtBusiness
            'objAssetDeclare._Datasource_StaffOwnerShip = dtStaffOwnership
            'objAssetDeclare._Datasource_BankAccounts = dtBankAccounts
            'objAssetDeclare._Datasource_RealProperties = dtProperties
            'objAssetDeclare._Datasource_Liabilities = dtLiabilities
            'objAssetDeclare._Datasource_Relatives = dtRelatives
            ''Gajanan (06 Oct 2018) -- Start
            ''Enhancement : Implementing New Module of Asset Declaration Template 2
            'objAssetDeclare._Datasource_DependantBusiness = dtDependantBusinessS8
            'objAssetDeclare._Datasource_DependantShare = dtDependantShareS9
            'objAssetDeclare._Datasource_DependantBank = dtDependantBank10
            'objAssetDeclare._Datasource_DependantProperties = dtDependantPropertiesS11
            ''Gajanan(06 Oct 2018) -- End

            'objAssetDeclare._Savedate = ConfigParameter._Object._CurrentDateAndTime
            'objAssetDeclare._Finalsavedate = Nothing
            'objAssetDeclare._Unlockfinalsavedate = Nothing

            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
            '    objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            'Else
            '    objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            'End If
            'objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            'objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            'objAssetDeclare._HostName = CStr(Session("HOST_NAME"))
            'objAssetDeclare._FormName = mstrModuleName1
            'objAssetDeclare._IsFromWeb = True
            'Sohail (07 Dec 2018) -- End

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            'If objAssetDeclare.InsertData(Session("Database_Name"), ConfigParameter._Object._CurrentDateAndTime, menAction) = False Then
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If objAssetDeclare.InsertData(Session("Database_Name"), ConfigParameter._Object._CurrentDateAndTime, menAction, mdtFinStartDate, mdtFinEndDate) = False Then
            '    'Hemant (05 Dec 2018) -- End
            '    DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
            'Else
            'Sohail (22 Feb 2020) -- Start
            'NMB Enhancement # : Allow to final save asset declaration on add / edit screen.
            'If objAssetDeclare.Update(enAction.EDIT_ONE, Nothing) = False Then
            Dim blnResult As Boolean = False
            If mintAssetDeclarationT2Unkid <= 0 Then
                blnResult = objAssetDeclare.Insert(Nothing, 0)
            Else
                blnResult = objAssetDeclare.Update(enAction.EDIT_ONE, Nothing)
            End If
            If blnResult = False Then
                'Sohail (22 Feb 2020) -- End
                If objAssetDeclare._Message <> "" Then DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
                Exit Try
            Else
            'Sohail (07 Dec 2018) -- End
                Call FillList()
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Entry Saved Successfuly!"), Me)
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'mintAssetDeclarationT2Unkid = 0
            mintAssetDeclarationT2Unkid = -1
            'Sohail (07 Dec 2018) -- End
                blnpopupAddEdit = False
                popupAddEdit.Hide()
                Reset_Business()
                Reset_BankAccounts()
                Reset_Relatives()
                Reset_RealProperties()
                Reset_StaffOwnerShip()
                Reset_Liabilities()
                Reset_DeptBusiness()
                Reset_DependantShare()
                Reset_DeptBank()
                Reset_DeptProperties()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonNo_Click
        Try
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanAttachDocuments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanAttachDocuments.Click
        Try
            Session("ScanAttchLableCaption") = "Select Employee"
            Session("ScanAttchRefModuleID") = enImg_Email_RefId.Employee_Module
            Session("ScanAttchenAction") = enAction.ADD_ONE
            Session("ScanAttchToEditIDs") = ""
            Response.Redirect(Session("servername") & "~/Others_Forms/wPg_ScanOrAttachmentInfo.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnFinalSaveYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalYesNo.buttonYes_Click
        Try

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            StrModuleName2 = "mnuAssetDeclaration"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")


            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If


            Dim objAssetDeclare As New clsAssetdeclaration_masterT2

            objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(Me.ViewState("FinalSaveUnkId"))

            objAssetDeclare._Isfinalsaved = True
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._Loginemployeeunkid = -1
            End If


            objAssetDeclare._Finalsavedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Unlockfinalsavedate = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            objAssetDeclare._HostName = CStr(Session("HOST_NAME"))
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetDeclare._FormName = mstrModuleName1
            objAssetDeclare._FormName = mstrModuleName
            'Hemant (07 Feb 2019) -- End
            objAssetDeclare._IsFromWeb = True


            'Sohail (22 Feb 2020) -- Start
            'NMB Enhancement # : Allow to final save asset declaration on add / edit screen.
            If objAssetDeclare.Update(enAction.EDIT_ONE) = False AndAlso objAssetDeclare._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
            Else
                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement for Email Notification After Final Saved In Asset Declaration 

                'Hemant (23 Nov 2018) -- Start
                'Enhancement : Changes As per Rutta Request for UAT 
                'objAssetDeclare.SendMailToUserForFinalSaved(objAssetDeclare._Employeeunkid, Session("AssetDeclarationFinalSavedNotificationUserIds").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, False, enLogin_Mode.MGR_SELF_SERVICE, Session("Employeeunkid"), CInt(Session("UserId")), objAssetDeclare._Isfinalsaved)
                objAssetDeclare.SendMailToUserForFinalSaved(objAssetDeclare._Employeeunkid, Session("AssetDeclarationFinalSavedNotificationUserIds").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, False, enLogin_Mode.MGR_SELF_SERVICE, Session("Employeeunkid"), CInt(Session("UserId")), objAssetDeclare._Isfinalsaved, Session("fin_startdate"), Session("fin_enddate"))
                'Hemant (23 Nov 2018) -- End

                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                'Send_Notification()
                Send_Notification(objAssetDeclare._WebEmailList)
                'Hemant (02 Feb 2022) -- End
                'Hemant (19 Nov 2018) -- End

                'Gajanan (28 Nov 2018) -- Start
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Declaration Submitted Successfully."), Me)
                'Gajanan(28 Nov 2018) -- End
                Call FillList()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnFinalSaveNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupFinalYesNo.buttonNo_Click
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If IsValidData() = False Then
                Exit Sub
            End If

            popupYesNo.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popupUnlockFinalYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupUnlockFinalYesNo.buttonNo_Click
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupUnlockFinalYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupUnlockFinalYesNo.buttonYes_Click
        Try
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            StrModuleName2 = "mnuAssetDeclaration"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            Dim objAssetDeclare As New clsAssetdeclaration_masterT2

            objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(Me.ViewState("UnlockFinalSaveUnkId"))

            objAssetDeclare._Isfinalsaved = False
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._Loginemployeeunkid = -1
            End If


            objAssetDeclare._Finalsavedate = Nothing
            objAssetDeclare._Unlockfinalsavedate = ConfigParameter._Object._CurrentDateAndTime

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            objAssetDeclare._HostName = CStr(Session("HOST_NAME"))
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetDeclare._FormName = mstrModuleName1
            objAssetDeclare._FormName = mstrModuleName
            'Hemant (07 Feb 2019) -- End

            objAssetDeclare._IsFromWeb = True

            If objAssetDeclare.Update(enAction.EDIT_ONE) = False AndAlso objAssetDeclare._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
            Else
                Dim objDocument As New clsScan_Attach_Documents
                Dim strIds As String = ""
                Dim dtTable As DataTable

                'S.SANDEEP |04-SEP-2021| -- START
                'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                'dtTable = New DataView(objDocument.GetList(Session("Document_Path"), "Attachment", "").Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(Me.ViewState("UnlockFinalSaveUnkId")) & " AND UNKID = " & objAssetDeclare._Employeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                dtTable = New DataView(objDocument.GetList(Session("Document_Path"), "Attachment", "", objAssetDeclare._Employeeunkid, , , , , CBool(IIf(objAssetDeclare._Employeeunkid <= 0, True, False))).Tables(0), "scanattachrefid = " & enScanAttactRefId.ASSET_DECLARATION & " AND modulerefid = " & enImg_Email_RefId.Employee_Module & " AND transactionunkid = " & CInt(Me.ViewState("UnlockFinalSaveUnkId")) & " AND UNKID = " & objAssetDeclare._Employeeunkid & " ", "", DataViewRowState.CurrentRows).ToTable
                'S.SANDEEP |04-SEP-2021| -- END

                If dtTable.Rows.Count > 0 Then
                    For Each dtRow As DataRow In dtTable.Rows
                        strIds &= "," & dtRow.Item("scanattachtranunkid").ToString

                        If dtRow("filepath").ToString <> "" AndAlso dtRow("fileuniquename").ToString <> "" Then
                            Dim strFilepath As String = dtRow("filepath")
                            Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                            If strFilepath.Contains(strArutiSelfService) Then
                                strFilepath = strFilepath.Replace(strArutiSelfService, "")
                                If Strings.Left(strFilepath, 1) <> "/" Then
                                    strFilepath = "~/" & strFilepath
                                Else
                                    strFilepath = "~" & strFilepath
                                End If

                                If File.Exists(Server.MapPath(strFilepath)) Then
                                    File.Delete(Server.MapPath(strFilepath))
                                Else
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "File connot exitst localpath"), Me)
                                    Exit Sub
                                End If
                            Else
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "File connot exitst localpath"), Me)
                                Exit Sub
                            End If

                        End If

                    Next
                    If strIds.Trim.Length > 0 Then
                        strIds = Mid(strIds, 2)

                        If objDocument.Delete(strIds) = False Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Problem in deleting information."), Me)
                            Exit Try
                        End If
                    End If
                End If
                'Hemant (23 Nov 2018) -- Start
                'Enhancement : Changes As per Rutta Request for UAT 
                objAssetDeclare.SendMailToUserForFinalSaved(objAssetDeclare._Employeeunkid, Session("AssetDeclarationUnlockFinalSavedNotificationUserIds").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, False, enLogin_Mode.MGR_SELF_SERVICE, Session("Employeeunkid"), CInt(Session("UserId")), objAssetDeclare._Isfinalsaved, Session("fin_startdate"), Session("fin_enddate"))
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Send_Notification(objAssetDeclare._WebEmailList)
                'Hemant (02 Feb 2022) -- End
                'Hemant (23 Nov 2018) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully."), Me)
                Call FillList()
            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseAddEdit.Click
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'mintAssetDeclarationT2Unkid = 0
            mintAssetDeclarationT2Unkid = -1
            'Sohail (07 Dec 2018) -- End
            blnpopupAddEdit = False
            popupAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub menu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkmenu_Instruction.Click, lnkmenu_BusinessDealings.Click, lnkmenu_StaffOwnership.Click _
                                                                                                 , lnkmenu_BankAccounts.Click, lnkmenu_Realproperties.Click, lnkmenu_Liabilities.Click _
                                                                                                 , lnkmenu_Relatives.Click, lnkmenu_DependantBusiness.Click, lnkmenu_DependantShares.Click _
                                                                                                 , lnkmenu_DependantBank.Click, lnkmenu_DependantProperties.Click, lnkmenu_SignOff.Click
        'Hemant (15 Nov 2018) ----[lnkmenu_SignOff]
        Try

            If IsValidData() = False Then
                Exit Sub
            End If
            Dim lnk As LinkButton = TryCast((sender), LinkButton)
            Dim pos As Integer = CInt(lnk.Attributes("position").ToString())
            mvwAssetDeclaration.ActiveViewIndex = pos
            activemenu(pos)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (22 Feb 2020) -- Start
    'NMB Enhancement # : Allow to final save asset declaration on add / edit screen.
    Protected Sub btnFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalSave.Click
        Try
            If IsValidData() = False Then
                Exit Sub
            End If

            If chkAcknowledgement_Step13.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is not acknowledged"), Me)
                Exit Sub
            End If

            Dim objAssetDeclare As New clsAssetdeclaration_masterT2
            If mintAssetDeclarationT2Unkid > 0 Then
                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If
            End If

            objAssetDeclare = SetValueAssetDeclaratationMaster()

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmpAssetDeclaration"
            StrModuleName2 = "mnuAssetDeclaration"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")


            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            objAssetDeclare._Isfinalsaved = True
            If (Session("loginBy") = Global.User.en_loginby.Employee) Then
                objAssetDeclare._Loginemployeeunkid = Session("Employeeunkid")
                objAssetDeclare._Userunkid = -1
            Else
                objAssetDeclare._Userunkid = Session("UserId")
                objAssetDeclare._Loginemployeeunkid = -1
            End If


            objAssetDeclare._Finalsavedate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._Unlockfinalsavedate = Nothing

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetDeclare._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetDeclare._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objAssetDeclare._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objAssetDeclare._ClientIp = CStr(Session("IP_ADD"))
            objAssetDeclare._HostName = CStr(Session("HOST_NAME"))
            objAssetDeclare._FormName = mstrModuleName
            objAssetDeclare._IsFromWeb = True

            Dim blnResult As Boolean = False
            If mintAssetDeclarationT2Unkid > 0 Then
                blnResult = objAssetDeclare.Update(enAction.EDIT_ONE)
            Else
                blnResult = objAssetDeclare.Insert(Nothing, 0)
            End If
            If blnResult = False Then
                If objAssetDeclare._Message <> "" Then DisplayMessage.DisplayMessage(objAssetDeclare._Message, Me)
                Exit Try
            Else
                objAssetDeclare.SendMailToUserForFinalSaved(objAssetDeclare._Employeeunkid, Session("AssetDeclarationFinalSavedNotificationUserIds").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, False, enLogin_Mode.MGR_SELF_SERVICE, Session("Employeeunkid"), CInt(Session("UserId")), objAssetDeclare._Isfinalsaved, Session("fin_startdate"), Session("fin_enddate"))

                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                'Send_Notification()
                Send_Notification(objAssetDeclare._WebEmailList)
                'Hemant (02 Feb 2022) -- End

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Declaration Submitted Successfully."), Me)

                Call FillList()

                mintAssetDeclarationT2Unkid = -1
                blnpopupAddEdit = False
                popupAddEdit.Hide()
                Reset_Business()
                Reset_BankAccounts()
                Reset_Relatives()
                Reset_RealProperties()
                Reset_StaffOwnerShip()
                Reset_Liabilities()
                Reset_DeptBusiness()
                Reset_DependantShare()
                Reset_DeptBank()
                Reset_DeptProperties()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (22 Feb 2020) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvList_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvList.PageIndexChanging
        Try
            gvList.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvList.DataSource = dsAssetList
            'gvList.DataBind()
            Call FillList()
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvList.RowCommand
        Try
            If e.CommandName <> "Print" Then

                'Hemant (05 Dec 2018) -- Start
                'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If dsAssetList.Tables(0).Rows(e.CommandArgument).Item("finyear_start") IsNot DBNull.Value Then
                '    If Session("fin_startdate") > dsAssetList.Tables(0).Rows(e.CommandArgument).Item("finyear_start") Then
                '        If dsAssetList.Tables(0).Rows(CInt(e.CommandArgument))("yearname").ToString.Trim <> Session("FinancialYear_Name") Then
                '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), Me)
                '            Exit Sub
                '        End If
                '    End If
                'End If
                Dim SrNo As Integer = CInt(e.CommandArgument)
                If gvList.DataKeys(SrNo).Item("finyear_start") IsNot DBNull.Value Then
                    'Hemant (02 Jan 2019) -- Start
                    'NMB - ENHANCEMENT
                    'If Session("fin_startdate") > gvList.DataKeys(SrNo).Item("finyear_start") Then
                    'Hemant (11 Feb 2019) -- Start
                    'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
                    'If CDate(Session("fin_startdate")).AddYears(-1) > gvList.DataKeys(SrNo).Item("finyear_start") Then
                    '    'Hemant (02 Jan 2019) -- End
                    '    If gvList.DataKeys(SrNo).Item("yearname").ToString.Trim <> Session("FinancialYear_Name") Then
                    '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), Me)
                    '        Exit Sub
                    '    End If
                    'End If
                    'Hemant (01 Mar 2019) -- Start
                    'ISSUE - Message "This is past year transaction." throw when Employee financial year AD start date greater than financial year Database start date
                    'If mdtFinStartDate >= gvList.DataKeys(SrNo).Item("finyear_start") AndAlso mdtFinEndDate <= gvList.DataKeys(SrNo).Item("finyear_end") Then
                    If mdtFinStartDate <= gvList.DataKeys(SrNo).Item("finyear_start") AndAlso mdtFinEndDate >= gvList.DataKeys(SrNo).Item("finyear_end") Then
                        'Hemant (01 Mar 2019) -- End
                    Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction."), Me)
                    Exit Sub
                End If
                    'Hemant (11 Feb 2019) -- End
                End If
                'Sohail (07 Dec 2018) -- End
                'Hemant (05 Dec 2018) -- End


            End If
            If e.CommandName = "Change" Then
                activemenu(0)
                lblStep1_EmployeeError.Text = ""
                lblExchangeRate.Text = ""
                Reset_Business()
                Reset_BankAccounts()
                Reset_Relatives()
                Reset_RealProperties()
                Reset_StaffOwnerShip()
                Reset_Liabilities()
                Reset_DeptBusiness()
                Reset_DependantShare()
                Reset_DeptBank()
                Reset_DeptProperties()

                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("SrNo") = SrNo

                Dim objAssetDeclare As New clsAssetdeclaration_masterT2

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationt2unkid"))
                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(gvList.DataKeys(SrNo).Item("assetdeclarationt2unkid"))
                'Sohail (07 Dec 2018) -- End

                'Hemant (01 Mar 2022) -- Start            
                If (Session("LoginBy") = Global.User.en_loginby.Employee) AndAlso CInt(objAssetDeclare._Employeeunkid) > 0 Then
                    If CheckUnLockEmployee(CInt(objAssetDeclare._Employeeunkid)) = False Then Exit Sub
                End If
                'Hemant (01 Mar 2022) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If

                'Hemant (05 Dec 2018) -- Start
                'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'Dim drFinYearList As DataRow() = dtFinYearList.Select("financialyear_name = '" & CDate(dsAssetList.Tables(0).Rows(SrNo)("finyear_start")).Date.Year & "-" & CDate(dsAssetList.Tables(0).Rows(SrNo)("finyear_end")).Date.Year & "' ", "financialyear_name")
                Dim objclsMasterData As New clsMasterData
                Dim dsCombos As DataSet
                If Session("AssetDeclarationFromDate").ToString = "" Then
                    dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
                ElseIf eZeeDate.convertDate(Session("AssetDeclarationFromDate")) <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    'Hemant (02 Jan 2019) -- Start
                    'NMB - ENHANCEMENT
                    'dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"), , , True, Session("financialyear_name"))
                    dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True)
                    'Hemant (02 Jan 2019) -- End
                Else
                    dsCombos = objclsMasterData.Get_Database_Year_List("Year", False, Session("CompanyUnkId"))
                End If

                Dim dtFinYearList As DataTable = dsCombos.Tables("Year")
                Dim drFinYearList As DataRow() = dtFinYearList.Select("financialyear_name = '" & CDate(gvList.DataKeys(SrNo)("finyear_start")).Date.Year & "-" & CDate(gvList.DataKeys(SrNo)("finyear_end")).Date.Year & "' ", "financialyear_name")
                'Sohail (07 Dec 2018) -- End
                If drFinYearList.Length > 0 Then
                    cboStep1_FinancialYear.SelectedValue = drFinYearList(0).Item("yearunkid").ToString
                End If
                cboStep1_FinancialYear_SelectedIndexChanged(Nothing, Nothing)
                cboStep1_FinancialYear.Enabled = False
                'Hemant (05 Dec 2018) -- End


                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'cboStep1_Employee.SelectedValue = dsAssetList.Tables(0).Rows(SrNo)("employeeunkid")
                cboStep1_Employee.SelectedValue = gvList.DataKeys(SrNo)("employeeunkid")
                'Sohail (07 Dec 2018) -- End
                Call cboStep1_Employee_SelectedIndexChanged(sender, New EventArgs)

                'Hemant (26 Nov 2018) -- Start
                'Enhancement : Changes As per Rutta Request for UAT 
                'txtEmployeeCode.Text = dsAssetList.Tables(0).Rows(SrNo).Item("noofemployment").ToString
                'txtOfficePosition.Text = dsAssetList.Tables(0).Rows(SrNo).Item("position").ToString
                'txtDepartment.Text = dsAssetList.Tables(0).Rows(SrNo).Item("centerforwork").ToString
                'txtWorkStation.Text = dsAssetList.Tables(0).Rows(SrNo).Item("workstation").ToString
                'Hemant (26 Nov 2018) -- End

                Dim strTinNumber As String = String.Empty
                Dim strTinNumberWithDash As String = String.Empty
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'strTinNumber = dsAssetList.Tables(0).Rows(SrNo).Item("Tin_No").ToString
                strTinNumber = gvList.DataKeys(SrNo).Item("Tin_No").ToString
                'Sohail (07 Dec 2018) -- End
                If strTinNumber.Length >= 3 And strTinNumber.Contains("-") Then
                    strTinNumber = strTinNumber.Replace("-", "")
                End If
                If strTinNumber.Length >= 3 Then
                    For index As Integer = 0 To strTinNumber.Length - 1
                        If index > 0 AndAlso (index Mod 3) = 0 Then
                            strTinNumberWithDash &= "-"
                        End If
                        strTinNumberWithDash &= strTinNumber(index)
                    Next
                End If

                txtStep1_Tin_No.Text = strTinNumberWithDash

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'chkAcknowledgement_Step13.Checked = dsAssetList.Tables(0).Rows(SrNo).Item("isacknowledged").ToString
                chkAcknowledgement_Step13.Checked = gvList.DataKeys(SrNo).Item("isacknowledged").ToString
                'Sohail (07 Dec 2018) -- End
                'Hemant (15 Nov 2018) -- End

                dtpStep1_Date.Enabled = False
                cboStep1_Employee.Enabled = False
                txtStep1_EmployeeCode.Enabled = False
                txtStep1_OfficePosition.Enabled = False
                txtStep1_Department.Enabled = False
                txtStep1_WorkStation.Enabled = False
                lblStep1_EmployeeError.Visible = False
                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                txtStep1_Tin_No.Enabled = True
                'Hemant (19 Nov 2018) -- End

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'mintOldEmployeeUnkId = Convert.ToInt16(dsAssetList.Tables(0).Rows(SrNo)("employeeunkid"))
                'mstrOldTinNo = dsAssetList.Tables(0).Rows(SrNo).Item("Tin_No").ToString
                'mdtOldTransactionDate = Convert.ToDateTime(dsAssetList.Tables(0).Rows(SrNo).Item("transactiondate").ToString).Date
                mintOldEmployeeUnkId = Convert.ToInt16(gvList.DataKeys(SrNo)("employeeunkid"))
                mstrOldTinNo = gvList.DataKeys(SrNo).Item("Tin_No").ToString
                mdtOldTransactionDate = Convert.ToDateTime(gvList.DataKeys(SrNo).Item("transactiondate").ToString).Date
                'Sohail (07 Dec 2018) -- End

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'mblnOldIsAcknowledged = Convert.ToBoolean(dsAssetList.Tables(0).Rows(SrNo).Item("isacknowledged"))
                mblnOldIsAcknowledged = Convert.ToBoolean(gvList.DataKeys(SrNo).Item("isacknowledged"))
                'Sohail (07 Dec 2018) -- End
                'Hemant (15 Nov 2018) -- End

                'txtResources.Text = dsAssetList.Tables(0).Rows(SrNo).Item("resources").ToString
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtpStep1_Date.SetDate = CDate(dsAssetList.Tables(0).Rows(SrNo).Item("transactiondate").ToString).Date 'Sohail (06 Apr 2012)

                'mintAssetDeclarationT2Unkid = CInt(dsAssetList.Tables(0).Rows(SrNo).Item("assetdeclarationt2unkid"))
                dtpStep1_Date.SetDate = CDate(gvList.DataKeys(SrNo).Item("transactiondate").ToString).Date 'Sohail (06 Apr 2012)

                mintAssetDeclarationT2Unkid = CInt(gvList.DataKeys(SrNo).Item("assetdeclarationt2unkid"))
                'Sohail (07 Dec 2018) -- End

                Call FillBusinessGrid()
                Call FillStaffOwnerShipGrid()
                Call FillBankAccountsGrid()
                Call FillRealPropertiesGrid()
                Call FillLiabilitiesGrid()
                Call FillRelativesGrid()

                'Gajanan (06 Oct 2018) -- Start
                'Enhancement : Implementing New Module of Asset Declaration Template 2
                Call FillDeptBusinessGrid()
                Call FillDeptShareGrid()
                Call FillDeptBankGrid()
                Call FillDeptPropertiesGrid()
                'Gajanan(06 Oct 2018) -- End



                mvwAssetDeclaration.ActiveViewIndex = 0

                blnpopupAddEdit = True
                popupAddEdit.Show()


                blnpopupAddEdit = True
                mblnIsbtnEditClick = True


            ElseIf e.CommandName = "Remove" Then
                Me.ViewState("DelUnkId") = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)

                Dim objAssetDeclare As New clsAssetdeclaration_masterT2

                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(Me.ViewState("DelUnkId"))

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If

                popupAddEdit.Hide()

                popup1.Show()

            ElseIf e.CommandName = "FinalSave" Then
                Me.ViewState("FinalSaveUnkId") = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                Dim SrNo As Integer = CInt(e.CommandArgument)

                Dim objAssetDeclare As New clsAssetdeclaration_masterT2

                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(Me.ViewState("FinalSaveUnkId"))

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                If objAssetDeclare._IsAcknowledged = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is not acknowledged"), Me)
                    Exit Sub
                End If
                'Hemant (15 Nov 2018) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved."), Me)
                    Exit Sub
                End If

                Dim objDocument As New clsScan_Attach_Documents


                Dim objAssetBusiness As New clsAsset_business_dealT2_tran
                Dim decBusinessTotal As Decimal = 0



                Dim dtTable As DataTable

                objAssetBusiness._Assetdeclarationt2unkid(Session("Database_Name")) = CInt(Me.ViewState("FinalSaveUnkId"))

                dtTable = objAssetBusiness._Datasource





                popupFinalYesNo.Show()

            ElseIf e.CommandName = "UnlockFinalSave" Then
                Me.ViewState("UnlockFinalSaveUnkId") = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                Dim SrNo As Integer = CInt(e.CommandArgument)

                Dim objAssetDeclare As New clsAssetdeclaration_masterT2

                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(Me.ViewState("UnlockFinalSaveUnkId"))

                If objAssetDeclare._Isfinalsaved = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry! You can not Unlock Final Save. Reason: This transaction is not Final Saved."), Me)
                    Exit Sub
                End If


                popupUnlockFinalYesNo.Show()


            ElseIf e.CommandName = "Print" Then

                Dim objAssetDeclare As New clsAssetdeclaration_masterT2 'SHANI (16 APR 2015)-START

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'Dim objRptAssetDeclare As New ArutiReports.clsAssetDeclarationT2
                'objRptAssetDeclare._AssetDeclarationUnkid = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                'objRptAssetDeclare._EmployeeUnkid = CInt(dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("employeeunkid"))
                'objRptAssetDeclare._EmployeeCode = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("noofemployment").ToString
                'objRptAssetDeclare._EmployeeName = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("employeename").ToString
                'objRptAssetDeclare._WorkStation = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("workstation").ToString
                'objRptAssetDeclare._Title = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("position").ToString
                ''Gajanan (06 Nov 2018) -- Start
                ''Enhancement : Change Asset Declaration Template 2 Missing Column Issue In Report
                'objRptAssetDeclare._EmployeeDepartment = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("position").ToString
                'objRptAssetDeclare._EmployeeDesignation = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("centerforwork").ToString
                'objRptAssetDeclare._EmployeeTINno = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("Tin_no").ToString
                'objRptAssetDeclare._CompanyUnkId = Session("CompanyUnkId")
                ''Gajanan(06 Nov 2018) -- End

                'objRptAssetDeclare._FinancialYear = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("finyear_start").ToString

                'objRptAssetDeclare._DatabaseName = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("databasename")
                'objAssetDeclare._DatabaseName = dsAssetList.Tables(0).Rows(CInt(e.CommandArgument)).Item("databasename")

                'objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(gvList.DataKeys(CInt(e.CommandArgument)).Value)
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Dim objRptAssetDeclare As New ArutiReports.clsAssetDeclarationT2
                objRptAssetDeclare._AssetDeclarationUnkid = CInt(gvList.DataKeys(SrNo).Item("assetdeclarationt2unkid"))
                objRptAssetDeclare._EmployeeUnkid = CInt(gvList.DataKeys(SrNo).Item("employeeunkid"))
                objRptAssetDeclare._EmployeeCode = gvList.DataKeys(SrNo).Item("noofemployment").ToString
                objRptAssetDeclare._EmployeeName = gvList.DataKeys(SrNo).Item("employeename").ToString
                objRptAssetDeclare._WorkStation = gvList.DataKeys(SrNo).Item("workstation").ToString
                objRptAssetDeclare._Title = gvList.DataKeys(SrNo).Item("position").ToString
                objRptAssetDeclare._EmployeeDepartment = gvList.DataKeys(SrNo).Item("position").ToString
                objRptAssetDeclare._EmployeeDesignation = gvList.DataKeys(SrNo).Item("centerforwork").ToString
                objRptAssetDeclare._EmployeeTINno = gvList.DataKeys(SrNo).Item("Tin_No").ToString
                objRptAssetDeclare._CompanyUnkId = Session("CompanyUnkId")
                objRptAssetDeclare._FinancialYear = gvList.DataKeys(SrNo).Item("finyear_start").ToString
                objRptAssetDeclare._DatabaseName = gvList.DataKeys(SrNo).Item("databasename")
                objAssetDeclare._DatabaseName = gvList.DataKeys(SrNo).Item("databasename")
                objAssetDeclare._Assetdeclaration2unkid(Session("Database_Name")) = CInt(gvList.DataKeys(SrNo).Item("assetdeclarationt2unkid"))
                'Sohail (07 Dec 2018) -- End

                If objAssetDeclare._Isfinalsaved = True Then
                    objRptAssetDeclare._NonOfficialDeclaration = ""
                Else
                    objRptAssetDeclare._NonOfficialDeclaration = Language.getMessage(mstrModuleName, 14, "Non Official Declaration")
                End If

                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    objRptAssetDeclare.generateReportNew(Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
                Else
                    objRptAssetDeclare.generateReportNew(Session("Database_Name").ToString(), 1, CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting").ToString(), True, Session("ExportReportPath").ToString(), CBool(Session("OpenAfterExport")), 0, enPrintAction.None, enExportAction.None)
                End If
                Session("objRpt") = objRptAssetDeclare._Rpt

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Anjan [24 February 2015] -- Start
                'ENHANCEMENT : Implementing session privileges for MSS

                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    e.Row.Cells(0).Visible = Session("EditAssetDeclaration")
                    e.Row.Cells(1).Visible = Session("DeleteAssetDeclaration")
                    e.Row.Cells(2).Visible = Session("FinalSaveAssetDeclaration")
                    e.Row.Cells(3).Visible = Session("UnlockFinalSaveAssetDeclaration")
                End If
                'Anjan [24 February 2015] -- End

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If e.Row.Cells(5).Text = "None" AndAlso e.Row.Cells(6).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'End If
                'Hemant (02 Jan 2019) -- Start
                'NMB - ENHANCEMENT
                'If IsDBNull(gvBusiness.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                If IsDBNull(gvList.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Hemant (02 Jan 2019) -- End
                    'Sohail (07 Dec 2018) -- End
                'SHANI (16 APR 2015)-START
                'Enhancement #449 - On Employee Asset Declaration employee should be able to see other Previous year asset Form.

                'Hemant (05 Dec 2018) -- Start
                'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    'If dsAssetList.Tables(0).Rows(e.Row.ID).Item("finyear_start") IsNot DBNull.Value Then
                    '    If Session("fin_startdate") > dsAssetList.Tables(0).Rows(e.Row.ID).Item("finyear_start") Then
                    '        If e.Row.Cells(9).Text.ToString.Trim <> Session("FinancialYear_Name") Then
                    '            e.Row.Style.Add("color", "green")
                    '        End If
                    '        'Hemant (05 Dec 2018) -- End
                    '    End If
                    'End If
                    If gvList.DataKeys(e.Row.RowIndex).Item("finyear_start") IsNot DBNull.Value Then
                        'Hemant (02 Jan 2019) -- Start
                        'NMB - ENHANCEMENT
                        'If Session("fin_startdate") > gvList.DataKeys(e.Row.ID).Item("finyear_start") Then
                        'Hemant (11 Feb 2019) -- Start
                        'ISSUE : Error Occurs "The Variable @finyear_start has already been declared" while ticked  Include closed Year Transaction 
                        'If CDate(Session("fin_startdate")).AddYears(-1) > gvList.DataKeys(e.Row.ID).Item("finyear_start") Then
                        '    'Hemant (02 Jan 2019) -- End                            
                        '    If e.Row.Cells(9).Text.ToString.Trim <> Session("FinancialYear_Name") Then
                        '        e.Row.Style.Add("color", "green")
                        '    End If
                        'End If
                        If mdtFinStartDate <= gvList.DataKeys(e.Row.RowIndex).Item("finyear_start") AndAlso mdtFinEndDate >= gvList.DataKeys(e.Row.RowIndex).Item("finyear_end") Then
                        Else
                    e.Row.Style.Add("color", "green")
                End If
                        'Hemant (11 Feb 2019) -- End
                End If
                    'Sohail (07 Dec 2018) -- End
                'SHANI (16 APR 2015)--END
                End If 'Sohail (07 Dec 2018)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " DropDown List Events "
    Protected Sub cboStep1_Employee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStep1_Employee.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        'Hemant (13 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        Dim strfilter As String
        Dim dsList As DataSet = Nothing
        Dim objAssetDeclare As New clsAssetdeclaration_masterT2
        'Hemant (13 Nov 2018) -- End

        'Hemant (03 Dec 2018) -- Start
        'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
        Dim objDependant As New clsDependants_Beneficiary_tran
        'Hemant (03 Dec 2018) -- End

        Try
            If CInt(cboStep1_Employee.SelectedValue) > 0 Then

                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(cboStep1_Employee.SelectedValue)

                txtStep1_EmployeeCode.Text = objEmployee._Employeecode

                Dim objDept As New clsDepartment
                objDept._Departmentunkid = objEmployee._Departmentunkid
                txtStep1_Department.Text = objDept._Name
                objDept = Nothing

                Dim objJob As New clsJobs
                objJob._Jobunkid = objEmployee._Jobunkid
                txtStep1_OfficePosition.Text = objJob._Job_Name
                objJob = Nothing

                Dim objClass As New clsClass
                objClass._Classesunkid = objEmployee._Classunkid
                txtStep1_WorkStation.Text = objClass._Name
                objClass = Nothing

                'Hemant (22 Nov 2018) -- Start
                'Enhancement : Changes As per Rutta Request for UAT 
                Dim objSectionGroup As New clsSectionGroup
                objSectionGroup._Sectiongroupunkid = objEmployee._Sectiongroupunkid
                txtStep1_Department2.Text = objSectionGroup._Name
                objClass = Nothing
                'Hemant (22 Nov 2018) -- End


                If objEmployee._Appointeddate.Date > Session("fin_startdate") Then
                    Session("FinStartDate") = objEmployee._Appointeddate.Date
                Else
                    Session("FinStartDate") = Session("fin_startdate")
                End If

                Dim EndDate As Date = Session("fin_enddate")
                If objEmployee._Termination_From_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_From_Date.Date < Session("fin_enddate"), objEmployee._Termination_From_Date.Date, Session("fin_enddate")))
                End If
                If objEmployee._Termination_To_Date.Date <> Nothing Then
                    EndDate = CDate(IIf(objEmployee._Termination_To_Date.Date < EndDate, objEmployee._Termination_To_Date.Date, EndDate))
                End If


                If EndDate <> Session("fin_enddate") Then
                    Session("FinEndDate") = EndDate
                Else
                    Session("FinEndDate") = Session("fin_enddate")
                End If

                'Hemant (13 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                strfilter = "hremployee_master.employeeunkid <> " + cboStep1_Employee.SelectedValue + ""

                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    Session("UserAccessModeSetting").ToString(), True, _
                                                    True, "Employee", True, , False, False, 0, 0, 0, 0, 0, , , , , , , strfilter, , False, False)
                'Hemant (15 Nov 2019) -- [Session("ShowFirstAppointmentDate") --> True]
                drpStep7_RelativeEmployee.DataSource = dsList.Tables("Employee").Copy
                drpStep7_RelativeEmployee.DataTextField = "EmpCodeName"
                drpStep7_RelativeEmployee.DataValueField = "employeeunkid"
                drpStep7_RelativeEmployee.DataBind()


                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'strfilter = "hremployee_master.employeeunkid = " + cboStep1_Employee.SelectedValue + ""
                'dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                '                          CInt(Session("UserId")), _
                '                          CInt(Session("Fin_year")), _
                '                          CInt(Session("CompanyUnkId")), _
                '                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                          CStr(Session("UserAccessModeSetting")), True, _
                '                          CBool(Session("IsIncludeInactiveEmp")), "List", , , strfilter, _
                '                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                'dtDependantList = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'Sohail (07 Dec 2018) -- End
                'Hemant (03 Dec 2018) -- End


                'Hemant (14 Nov 2018) -- Start
                'Enhancement : Changes for Asset Declaration template2
                If (Session("loginBy") = Global.User.en_loginby.User) Then
                    'Hemant (14 Nov 2018) -- End
                    lblStep1_EmployeeError.Text = ""
                    'Hemant (05 Dec 2018) -- Start
                    'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
                    'If objAssetDeclare.isExist(CInt(cboStep1_Employee.SelectedValue)) = True Then
                    'lblStep1_EmployeeError.Text = "Sorry! Asset declaration for this employee is already exist."
                    'Sohail (30 Jan 2020) -- Start
                    'NMB Issue # : Unable to edit asset declaration.
                    'If objAssetDeclare.isExist(CInt(cboStep1_Employee.SelectedValue), 0, 0, Nothing, mdtFinStartDate, mdtFinEndDate) = True Then
                    If objAssetDeclare.isExist(CInt(cboStep1_Employee.SelectedValue), mintAssetDeclarationT2Unkid, 0, Nothing, mdtFinStartDate, mdtFinEndDate) = True Then
                        'Sohail (30 Jan 2020) -- End
                        'Sohail (12 Feb 2020) -- Start
                        'NMB issue # : Language not getting reflected.
                        'lblStep1_EmployeeError.Text = "Sorry! Asset declaration for this employee is already exist for selected financial year."
                        lblStep1_EmployeeError.Text = Language.getMessage(mstrModuleName1, 1, "Sorry! Asset declaration for this employee is already exist for selected financial year.")
                        'Sohail (12 Feb 2020) -- End
                        'Hemant (05 Dec 2018) -- End
                        cboStep1_Employee.Focus()
                        popupAddEdit.Show()
                    End If
                End If

                'Hemant (13 Nov 2018) -- End

                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                Set_SignOffDeclarations()
                'Hemant (15 Nov 2018) -- End

                'Hemant (19 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                txtStep1_Tin_No.Focus()
                'Hemant (19 Nov 2018) -- End


            Else
                txtStep1_EmployeeCode.Text = ""
                txtStep1_Department.Text = ""
                txtStep1_OfficePosition.Text = ""
                txtStep1_WorkStation.Text = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub drpCurrencyBusiness_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpStep2_CurrencyBusiness.SelectedIndexChanged, drpStep3_CurrencyShare.SelectedIndexChanged _
                                                                                                                                                                                , drpStep4_CurrencyBank.SelectedIndexChanged
        Try

            Dim cbo As Object = Nothing


            cbo = sender

            Dim mstrClientID As String = cbo.ClientID.ToString().Replace("_drpCombo", "")

            If mstrClientID = drpStep2_CurrencyBusiness.ClientID Then

                If txtStep2_MonthlyAnnualEarnings.Text.Trim.Length <= 0 Then
                    txtStep2_MonthlyAnnualEarnings.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtStep2_MonthlyAnnualEarnings.Text = Format(CDec(txtStep2_MonthlyAnnualEarnings.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpStep3_CurrencyShare.ClientID Then

                If txtStep3_Marketvalue.Text.Trim.Length <= 0 Then
                    txtStep3_Marketvalue.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtStep3_Marketvalue.Text = Format(CDec(txtStep3_Marketvalue.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpStep5_CurrencyProperties.ClientID Then

                If txtStep5_EstimatedValueRealProperties.Text.Trim.Length <= 0 Then
                    txtStep5_EstimatedValueRealProperties.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtStep5_EstimatedValueRealProperties.Text = Format(CDec(txtStep5_EstimatedValueRealProperties.Text.Trim), Session("fmtcurrency"))
                End If


            ElseIf mstrClientID = drpStep6_OrigCurrencyLiabilities.ClientID Then

                If txtStep6_OriginalBalanceLiabilities.Text.Trim.Length <= 0 Then
                    txtStep6_OriginalBalanceLiabilities.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtStep6_OriginalBalanceLiabilities.Text = Format(CDec(txtStep6_OriginalBalanceLiabilities.Text.Trim), Session("fmtcurrency"))
                End If

            ElseIf mstrClientID = drpStep6_OutCurrencyLiabilities.ClientID Then

                If txtStep6_OutstandingBalanceLiabilities.Text.Trim.Length <= 0 Then
                    txtStep6_OutstandingBalanceLiabilities.Text = Format(0, Session("fmtcurrency"))
                Else
                    txtStep6_OutstandingBalanceLiabilities.Text = Format(CDec(txtStep6_OutstandingBalanceLiabilities.Text.Trim), Session("fmtcurrency"))
                End If

            End If


            If cbo.SelectedIndex > 0 Then
                Dim objExRate As New clsExchangeRate
                Dim dsList As DataSet
                Dim dtTable As DataTable
                Dim mdecBaseExRate As Decimal = 0
                Dim mdecPaidExRate As Decimal = 0

                dsList = objExRate.GetList("ExRate", True, , , CInt(cbo.SelectedValue), True, ConfigParameter._Object._CurrentDateAndTime.Date, True)
                dtTable = New DataView(dsList.Tables("ExRate")).ToTable
                If dtTable.Rows.Count > 0 Then
                    mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                    mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                    lblExchangeRate.Text = Format(mdecBaseExRate, Session("fmtCurrency")) & " " & Me.ViewState("currencysign") & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                End If

            Else
                lblExchangeRate.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    'Hemant (05 Dec 2018) -- Start
    'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
    Protected Sub cboStep1_FinancialYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStep1_FinancialYear.SelectedIndexChanged
        Try
            If CInt(cboStep1_FinancialYear.SelectedValue) = -99 Then
                mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(1)
                mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(1)
                'Hemant (02 Jan 2019) -- Start
                'NMB - ENHANCEMENT
                'Hemant (23 Jan 2019) -- Start
                'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
                'ElseIf CInt(cboStep1_FinancialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                '    mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                'mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
            ElseIf CInt(drpFinalcialYear.SelectedValue) = -98 Then
                mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
            ElseIf CInt(cboStep1_FinancialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                Dim objclsMasterData As New clsMasterData
                For Each drRow As DataRow In objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True).Tables(0).Rows
                    If CInt(drpFinalcialYear.SelectedValue) = CInt(drRow.Item("yearunkid")) Then
                        mdtFinStartDate = CDate(eZeeDate.convertDate(drRow.Item("start_date"))).Date
                        mdtFinEndDate = CDate(eZeeDate.convertDate(drRow.Item("end_date"))).Date
                        Exit For
                    End If
                Next
                'Hemant (23 Jan 2019) -- End

                'Hemant (02 Jan 2019) -- End
            Else
                mdtFinStartDate = Session("Fin_StartDate")
                mdtFinEndDate = Session("Fin_EndDate")
            End If

            Dim objAssetDeclare As New clsAssetdeclaration_masterT2
            If cboStep1_Employee.SelectedValue > 0 Then
                If objAssetDeclare.isExist(CInt(cboStep1_Employee.SelectedValue), 0, 0, Nothing, mdtFinStartDate, mdtFinEndDate) = True Then
                    'Sohail (12 Feb 2020) -- Start
                    'NMB issue # : Language not getting reflected.
                    'lblStep1_EmployeeError.Text = "Sorry! Asset declaration for this employee is already exist for selected financial year."
                    lblStep1_EmployeeError.Text = Language.getMessage(mstrModuleName1, 1, "Sorry! Asset declaration for this employee is already exist for selected financial year.")
                    'Sohail (12 Feb 2020) -- End
                    cboStep1_Employee.Focus()
                    popupAddEdit.Show()
                Else
                    lblStep1_EmployeeError.Text = ""
                End If
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboStep1_FinancialYear_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    Protected Sub drpFinalcialYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpFinalcialYear.SelectedIndexChanged
        Try
            If CInt(drpFinalcialYear.SelectedValue) = -99 Then
                mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(1)
                mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(1)
                'Hemant (02 Jan 2019) -- Start
                'NMB - ENHANCEMENT
                'ElseIf CInt(drpFinalcialYear.SelectedValue) = -98 Then
                'Hemant (23 Jan 2019) -- Start
                'ISSUE : NMB request to have only one Financial Year displayed during declaration period.year of the start date set on a tenure regardless of financial year is closed or open
                'ElseIf CInt(drpFinalcialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                '    mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                '    mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
            ElseIf CInt(drpFinalcialYear.SelectedValue) = -98 Then
                mdtFinStartDate = CDate(Session("Fin_StartDate")).Date.AddYears(-1)
                mdtFinEndDate = CDate(Session("Fin_EndDate")).Date.AddYears(-1)
            ElseIf CInt(drpFinalcialYear.SelectedValue) <> CInt(Session("Fin_year")) Then
                Dim objclsMasterData As New clsMasterData
                For Each drRow As DataRow In objclsMasterData.Get_Database_Year_List("Year", True, Session("CompanyUnkId"), , , True, Session("financialyear_name"), True).Tables(0).Rows
                    If CInt(drpFinalcialYear.SelectedValue) = CInt(drRow.Item("yearunkid")) Then
                        mdtFinStartDate = CDate(eZeeDate.convertDate(drRow.Item("start_date"))).Date
                        mdtFinEndDate = CDate(eZeeDate.convertDate(drRow.Item("end_date"))).Date
                        Exit For
                    End If
                Next
                'Hemant (23 Jan 2019) -- End
                'Hemant (02 Jan 2019) -- End
            Else
                mdtFinStartDate = Session("Fin_StartDate")
                mdtFinEndDate = Session("Fin_EndDate")
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpFinalcialYear_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Hemant (05 Dec 2018) -- End

#End Region

#Region "Date's Events "
    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpStep1_Date.TextChanged
        Try
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Business Dealings "

#Region " Private Method Functions "

    Private Sub FillComboBusinessDealings()
        Dim objExchange As New clsExchangeRate
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrBusiness As DataSet
        'Sohail (07 Dec 2018) -- End
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrBusiness = objExchange.getComboList("Currency", True)
            'With drpStep2_CurrencyBusiness

            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrBusiness.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillBusinessGrid()
        Dim objAsset_business_dealT2_tran As New clsAsset_business_dealT2_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtBusiness As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'objAsset_business_dealT2_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtBusiness = objAsset_business_dealT2_tran._Datasource

            'If dtBusiness.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtBusiness.NewRow

            '    r.Item(3) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = ""

            '    r.Item(16) = 0


            '    dtBusiness.Rows.Add(r)
            'End If
            Dim dsBusiness As DataSet = objAsset_business_dealT2_tran.GetList("List", mintAssetDeclarationT2Unkid)

            dtBusiness = dsBusiness.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtBusiness.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtBusiness.NewRow
                dtBusiness.Rows.Add(r)
            End If
            'Sohail (07 Dec 2018) -- End

            gvBusiness.DataSource = dtBusiness
            gvBusiness.DataBind()

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'SetBusinessTotal()
            If blnRecordExist = False Then
                gvBusiness.Rows(0).Visible = False
            End If
            'Sohail (07 Dec 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function IsValid_Business() As Boolean
        Try

            If dtpStep2_RegistrationDate.IsNull OrElse eZeeDate.convertDate(dtpStep2_RegistrationDate.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtpStep2_RegistrationDate.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblerrordtpRegistrationDate.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If



            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            Dim strCompanyTinNumber As String = String.Empty
            strCompanyTinNumber = txtStep2_CompanyTinNo.Text.Replace("_", "")
            If strCompanyTinNumber.Length < 11 Then
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 54, "Company Tin Number Must be 9 Digit"), Me)
                popupAddEdit.Show()
                txtStep2_CompanyTinNo.Focus()
                Return False
            End If
            'Hemant (19 Nov 2018) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Reset_Business()
        Try
            cboStep2_Sector.SelectedIndex = 0
            txtStep2_Company_Name.Text = ""
            'Hemant (22 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            'txtCompanyTinNo.Text = ""
            txtStep2_CompanyTinNo.Text = "000-000-000"
            'Hemant (22 Nov 2018) -- End
            txtStep2_Addresslocation.Text = ""
            txtStep2_PositionHeld.Text = ""
            txtStep2_BusinessType.Text = ""
            drpStep2_Is_supplier_client.SelectedIndex = 0

            txtStep2_MonthlyAnnualEarnings.Text = "0"
            txtStep2_BusinessContactNo.Text = ""
            txtStep2_ShareholdersNames.Text = ""
            BtnAddBusiness.Enabled = True
            BtnUpdateBusiness.Enabled = False

            drpStep2_CurrencyBusiness.SelectedIndex = 0
            dtpStep2_RegistrationDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Private Sub SetBusinessTotal()
    '    Try
    '        If dtBusiness IsNot Nothing Then
    '            Dim drRow() As DataRow = dtBusiness.Select("AUD <> 'D'")
    '            If drRow.Length > 0 Then
    '                'txtCashExistingBusiness.Text = Format(CDec(dtBusiness.Compute("sum(baseamount)", "AUD <> 'D'")), Session("fmtCurrency"))
    '            Else
    '                'txtCashExistingBusiness.Text = Format(0, Session("fmtCurrency"))
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Sohail (07 Dec 2018) -- End
    'Pinkal (30-Jan-2013) -- End

#End Region

#Region " Button's Events "
    Protected Sub BtnAddBusiness_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddBusiness.Click
        Try
            If IsValid_Business() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtBusiness.NewRow

            'drRow.Item("assetbusinessdealt2tranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("assetsectorunkid") = Convert.ToInt16(cboStep2_Sector.SelectedValue)
            'drRow.Item("company_org_name") = txtStep2_Company_Name.Text.Trim
            'drRow.Item("company_tin_no") = txtStep2_CompanyTinNo.Text.Trim
            'drRow.Item("registration_date") = dtpStep2_RegistrationDate.GetDate
            'drRow.Item("address_location") = txtStep2_Addresslocation.Text.Trim
            'drRow.Item("business_type") = txtStep2_BusinessType.Text.Trim
            'drRow.Item("position_held") = txtStep2_PositionHeld.Text.Trim
            'drRow.Item("is_supplier_client") = Convert.ToInt16(drpStep2_Is_supplier_client.SelectedValue)
            'drRow.Item("countryunkid") = CInt(drpStep2_CurrencyBusiness.SelectedItem.Value)
            'drRow.Item("monthly_annual_earnings") = Convert.ToDecimal(txtStep2_MonthlyAnnualEarnings.Text.Trim)
            'drRow.Item("business_contact_no") = txtStep2_BusinessContactNo.Text.Trim
            'drRow.Item("shareholders_names") = txtStep2_ShareholdersNames.Text.Trim
            'drRow.Item("isfinalsaved") = 0

            'Update_DataGridview_DataTable_Extra_Columns(gvBusiness, drRow, CInt(drpStep2_CurrencyBusiness.SelectedValue))

            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
            '    drRow.Item("audituserunkid") = CInt(Session("UserId"))
            'Else
            '    drRow.Item("loginemployeeunkid") = CInt(Session("Employeeunkid"))
            'End If

            'drRow.Item("IP") = Session("IP_ADD").ToString()
            'drRow.Item("host") = Session("HOST_NAME").ToString()
            'drRow.Item("form_name") = mstrModuleName
            'drRow.Item("isweb") = True

            'dtBusiness.Rows.Add(drRow)

            'gvBusiness.DataSource = dtBusiness
            'gvBusiness.DataBind()

            'SetBusinessTotal()
            Dim objBussDeal As New clsAsset_business_dealT2_tran
            objBussDeal._Assetbusinessdealt2tranunkid = -1
            objBussDeal._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objBussDeal._Assetsectorunkid = Convert.ToInt16(cboStep2_Sector.SelectedValue)
            objBussDeal._Company_Org_Name = txtStep2_Company_Name.Text.Trim
            objBussDeal._Company_Tin_No = txtStep2_CompanyTinNo.Text.Trim
            objBussDeal._Registration_Date = dtpStep2_RegistrationDate.GetDate
            objBussDeal._Address_Location = txtStep2_Addresslocation.Text.Trim
            objBussDeal._Business_Type = txtStep2_BusinessType.Text.Trim
            objBussDeal._Position_Held = txtStep2_PositionHeld.Text.Trim
            objBussDeal._Is_Supplier_Client = Convert.ToInt16(drpStep2_Is_supplier_client.SelectedValue)
            objBussDeal._Countryunkid = CInt(drpStep2_CurrencyBusiness.SelectedItem.Value)
            objBussDeal._Monthly_Annual_Earnings = Convert.ToDecimal(txtStep2_MonthlyAnnualEarnings.Text.Trim)
            objBussDeal._Business_Contact_No = txtStep2_BusinessContactNo.Text.Trim
            objBussDeal._Shareholders_Names = txtStep2_ShareholdersNames.Text.Trim
            objBussDeal._Isfinalsaved = 0

            objBussDeal._Userunkid = CInt(Session("UserId"))
            If objBussDeal._Transactiondate = Nothing Then
                objBussDeal._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep2_CurrencyBusiness.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objBussDeal._Currencyunkid = intPaidCurrencyunkid
            objBussDeal._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objBussDeal._Baseexchangerate = decBaseExRate
            objBussDeal._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objBussDeal._Basemonthly_Annual_Earnings = objBussDeal._Monthly_Annual_Earnings * decBaseExRate / decPaidExRate
            Else
                objBussDeal._Basemonthly_Annual_Earnings = objBussDeal._Monthly_Annual_Earnings
            End If
            'Update_DataGridview_DataTable_Extra_Columns(gvBusiness, drRow, CInt(drpStep2_CurrencyBusiness.SelectedValue))

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBussDeal._AuditUserId = CInt(Session("UserId"))
            Else
                objBussDeal._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objBussDeal._ClientIP = Session("IP_ADD").ToString()
            objBussDeal._HostName = Session("HOST_NAME").ToString()
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objBussDeal._FormName = mstrModuleName
            objBussDeal._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End
            objBussDeal._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objBussDeal.Insert(Nothing, objAssetDeclare) = False Then
                If objBussDeal._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBussDeal._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objBussDeal._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillBusinessGrid()
            'Sohail (07 Dec 2018) -- End

            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            'Call Reset_Business()
            txtStep2_ShareholdersNames.Text = ""
            'Hemant (15 Nov 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub BtnUpdateBusiness_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnUpdateBusiness.Click
        Try
            If IsValid_Business() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("BusinessSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtBusiness.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtBusiness.Rows(SrNo)

            'drRow.Item("assetsectorunkid") = Convert.ToInt16(cboStep2_Sector.SelectedValue)
            'drRow.Item("company_org_name") = txtStep2_Company_Name.Text.Trim
            'drRow.Item("company_tin_no") = txtStep2_CompanyTinNo.Text.Trim
            'drRow.Item("registration_date") = dtpStep2_RegistrationDate.GetDate
            'drRow.Item("address_location") = txtStep2_Addresslocation.Text.Trim
            'drRow.Item("business_type") = txtStep2_BusinessType.Text.Trim
            'drRow.Item("position_held") = txtStep2_PositionHeld.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpStep2_CurrencyBusiness.SelectedItem.Value)
            'drRow.Item("monthly_annual_earnings") = Convert.ToDecimal(txtStep2_MonthlyAnnualEarnings.Text.Trim)
            'drRow.Item("business_contact_no") = txtStep2_BusinessContactNo.Text.Trim
            'drRow.Item("shareholders_names") = txtStep2_ShareholdersNames.Text.Trim
            'drRow.Item("is_supplier_client") = Convert.ToInt16(drpStep2_Is_supplier_client.SelectedValue)
            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvBusiness, drRow, CInt(drpStep2_CurrencyBusiness.SelectedValue))

            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
            '    drRow.Item("audituserunkid") = CInt(Session("UserId"))
            'Else
            '    drRow.Item("loginemployeeunkid") = CInt(Session("Employeeunkid"))
            'End If

            'drRow.Item("IP") = Session("IP_ADD").ToString()
            'drRow.Item("host") = Session("HOST_NAME").ToString()
            'drRow.Item("form_name") = mstrModuleName
            'drRow.Item("isweb") = True

            'dtBusiness.AcceptChanges()

            'gvBusiness.DataSource = dtBusiness
            'gvBusiness.DataBind()


            'SetBusinessTotal()
            Dim objBussDeal As New clsAsset_business_dealT2_tran
            objBussDeal.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvBusiness.DataKeys(SrNo).Item("assetbusinessdealt2tranunkid")), Nothing)
            objBussDeal._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objBussDeal._Assetsectorunkid = Convert.ToInt16(cboStep2_Sector.SelectedValue)
            objBussDeal._Company_Org_Name = txtStep2_Company_Name.Text.Trim
            objBussDeal._Company_Tin_No = txtStep2_CompanyTinNo.Text.Trim
            objBussDeal._Registration_Date = dtpStep2_RegistrationDate.GetDate
            objBussDeal._Address_Location = txtStep2_Addresslocation.Text.Trim
            objBussDeal._Business_Type = txtStep2_BusinessType.Text.Trim
            objBussDeal._Position_Held = txtStep2_PositionHeld.Text.Trim
            objBussDeal._Countryunkid = CInt(drpStep2_CurrencyBusiness.SelectedItem.Value)
            objBussDeal._Monthly_Annual_Earnings = Convert.ToDecimal(txtStep2_MonthlyAnnualEarnings.Text.Trim)
            objBussDeal._Business_Contact_No = txtStep2_BusinessContactNo.Text.Trim
            objBussDeal._Shareholders_Names = txtStep2_ShareholdersNames.Text.Trim
            objBussDeal._Is_Supplier_Client = Convert.ToInt16(drpStep2_Is_supplier_client.SelectedValue)
            objBussDeal._Isfinalsaved = 0

            objBussDeal._Userunkid = CInt(Session("UserId"))
            If objBussDeal._Transactiondate = Nothing Then
                objBussDeal._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep2_CurrencyBusiness.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objBussDeal._Currencyunkid = intPaidCurrencyunkid
            objBussDeal._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objBussDeal._Baseexchangerate = decBaseExRate
            objBussDeal._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objBussDeal._Basemonthly_Annual_Earnings = objBussDeal._Monthly_Annual_Earnings * decBaseExRate / decPaidExRate
            Else
                objBussDeal._Basemonthly_Annual_Earnings = objBussDeal._Monthly_Annual_Earnings
            End If
            'Update_DataGridview_DataTable_Extra_Columns(gvBusiness, drRow, CInt(drpStep2_CurrencyBusiness.SelectedValue))

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBussDeal._AuditUserId = CInt(Session("UserId"))
            Else
                objBussDeal._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objBussDeal._ClientIP = Session("IP_ADD").ToString()
            objBussDeal._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objBussDeal._FormName = mstrModuleName
            objBussDeal._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objBussDeal._IsFromWeb = True


            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objBussDeal.Update(False, Nothing, objAssetDeclare) = False Then
                If objBussDeal._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBussDeal._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objBussDeal._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillBusinessGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_Business()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub btnResetBusiness_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBusiness.Click
        Try
            Call Reset_Business()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteBusinessDeal_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteBusinessDeal.buttonDelReasonYes_Click
        Dim objBussDeal As New clsAsset_business_dealT2_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("BusinessSrNo"))
            objBussDeal.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvBusiness.DataKeys(SrNo).Item("assetbusinessdealt2tranunkid")), Nothing)
            objBussDeal._Isvoid = True
            objBussDeal._Voiduserunkid = CInt(Session("UserId"))
            objBussDeal._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objBussDeal._Voidreason = popupDeleteBusinessDeal.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBussDeal._AuditUserId = CInt(Session("UserId"))
            Else
                objBussDeal._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objBussDeal._ClientIP = Session("IP_ADD").ToString()
            objBussDeal._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objBussDeal._FormName = mstrModuleName
            objBussDeal._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objBussDeal._IsFromWeb = True

            If objBussDeal.Void(CInt(gvBusiness.DataKeys(SrNo).Item("assetbusinessdealt2tranunkid")), CInt(Session("UserId")), objBussDeal._Voiddatetime, objBussDeal._Voidreason, Nothing) = True Then
                Call FillBusinessGrid()
            ElseIf objBussDeal._Message <> "" Then
                DisplayMessage.DisplayMessage(objBussDeal._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

#End Region

#Region " GridView Events "

    Protected Sub gvBusiness_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBusiness.PageIndexChanging
        Try
            gvBusiness.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvBusiness.DataSource = dtBusiness
            'gvBusiness.DataBind()
            Call FillBusinessGrid()
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvBusiness_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBusiness.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("BusinessSrNo") = SrNo

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'cboStep2_Sector.SelectedValue = Convert.ToInt16(dtBusiness.Rows(SrNo).Item("assetsectorunkid").ToString)
                'txtStep2_Company_Name.Text = dtBusiness.Rows(SrNo).Item("company_org_name").ToString
                'txtStep2_CompanyTinNo.Text = dtBusiness.Rows(SrNo).Item("company_tin_no").ToString
                'txtStep2_Addresslocation.Text = dtBusiness.Rows(SrNo).Item("address_location").ToString
                'txtStep2_PositionHeld.Text = dtBusiness.Rows(SrNo).Item("position_held").ToString
                'dtpStep2_RegistrationDate.SetDate = Convert.ToDateTime(dtBusiness.Rows(SrNo).Item("registration_date").ToString)
                'txtStep2_BusinessType.Text = dtBusiness.Rows(SrNo).Item("business_type").ToString
                'drpStep2_Is_supplier_client.SelectedValue = Convert.ToInt16(dtBusiness.Rows(SrNo).Item("is_supplier_client").ToString)
                'txtStep2_MonthlyAnnualEarnings.Text = Format(dtBusiness.Rows(SrNo).Item("monthly_annual_earnings"), Session("fmtCurrency"))
                'drpStep2_CurrencyBusiness.SelectedValue = dtBusiness.Rows(SrNo).Item("countryunkid").ToString
                'txtStep2_BusinessContactNo.Text = dtBusiness.Rows(SrNo).Item("business_contact_no").ToString
                'txtStep2_ShareholdersNames.Text = dtBusiness.Rows(SrNo).Item("shareholders_names").ToString
                cboStep2_Sector.SelectedValue = CInt(gvBusiness.DataKeys(SrNo).Item("assetsectorunkid").ToString)
                txtStep2_Company_Name.Text = gvBusiness.Rows(SrNo).Cells(3).Text.Replace("&amp;", "&")
                txtStep2_CompanyTinNo.Text = gvBusiness.Rows(SrNo).Cells(4).Text
                dtpStep2_RegistrationDate.SetDate = gvBusiness.Rows(SrNo).Cells(6).Text
                txtStep2_Addresslocation.Text = gvBusiness.Rows(SrNo).Cells(7).Text
                txtStep2_BusinessType.Text = gvBusiness.Rows(SrNo).Cells(8).Text
                txtStep2_PositionHeld.Text = gvBusiness.Rows(SrNo).Cells(9).Text
                drpStep2_Is_supplier_client.SelectedValue = CInt(gvBusiness.DataKeys(SrNo).Item("is_supplier_client").ToString)
                txtStep2_MonthlyAnnualEarnings.Text = Format(CDec(gvBusiness.Rows(SrNo).Cells(11).Text), Session("fmtCurrency"))
                drpStep2_CurrencyBusiness.SelectedValue = CInt(gvBusiness.DataKeys(SrNo).Item("countryunkid").ToString)
                Call drpCurrencyBusiness_SelectedIndexChanged(drpStep2_CurrencyBusiness, New System.EventArgs)
                txtStep2_BusinessContactNo.Text = gvBusiness.Rows(SrNo).Cells(12).Text
                txtStep2_ShareholdersNames.Text = gvBusiness.Rows(SrNo).Cells(13).Text
                'Sohail (07 Dec 2018) -- End
                BtnAddBusiness.Enabled = False
                BtnUpdateBusiness.Enabled = True
            ElseIf e.CommandName = "Remove" Then

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtBusiness.Rows(CInt(e.CommandArgument)).Delete()

                'If dtBusiness.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtBusiness.NewRow

                '    r.Item(3) = "None" ' To Hide the row and display only Row Header
                '    r.Item(4) = ""

                '    r.Item(16) = 0

                '    dtBusiness.Rows.Add(r)
                'End If
                'dtBusiness.AcceptChanges()

                'gvBusiness.DataSource = dtBusiness
                'gvBusiness.DataBind()


                'SetBusinessTotal()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_Business()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("BusinessSrNo") = SrNo

                popupDeleteBusinessDeal.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Protected Sub gvBusiness_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBusiness.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If e.Row.Cells(3).Text = "None" AndAlso e.Row.Cells(4).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvBusiness.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Sohail (07 Dec 2018) -- End
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvBusiness, "colhStep2_registration_date", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvBusiness, "colhStep2_registration_date", False, True)).Text).ToShortDateString
                    e.Row.Cells(11).Text = Format(CDec(e.Row.Cells(11).Text), Session("fmtCurrency"))

                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    'Dim intCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(5).Text = ""
                    'End If


                    'Dim intSectorId As Integer
                    'Integer.TryParse(e.Row.Cells(2).Text, intSectorId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")

                    'Dim drSector As DataRow() = dsList.Tables(0).Select("masterunkid = " & intSectorId & " ", "name")

                    'If drSector.Length > 0 Then
                    '    e.Row.Cells(2).Text = drSector(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(2).Text = ""
                    'End If

                    'Dim intSupplierBorrowerId As Integer
                    'Integer.TryParse(e.Row.Cells(10).Text, intSupplierBorrowerId)
                    'dsList = (New clsMasterData).getComboListAssetsupplierborrowerList("List")
                    'Dim drClient As DataRow() = dsList.Tables(0).Select("id = " & intSupplierBorrowerId & " ", "name")

                    'If drClient.Length > 0 Then
                    '    e.Row.Cells(10).Text = drClient(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(10).Text = ""
                    'End If
                    'Sohail (07 Dec 2018) -- End

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "TextBox Events"
    Protected Sub txtMonthlyAnnualEarnings_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep2_MonthlyAnnualEarnings.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep2_MonthlyAnnualEarnings.Text = Format(CDec(txtStep2_MonthlyAnnualEarnings.Text), Session("fmtCurrency"))
            If txtStep2_MonthlyAnnualEarnings.Text.Trim <> "" Then txtStep2_MonthlyAnnualEarnings.Text = Format(CDec(txtStep2_MonthlyAnnualEarnings.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Staff Ownership "

#Region " Private Method Functions "
    Private Sub FillComboShare()
        Dim objExchange As New clsExchangeRate
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrShare As DataSet
        'Sohail (07 Dec 2018) -- End
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrShare = objExchange.getComboList("Currency", True)
            'With drpStep3_CurrencyShare

            '    .DataValueField = "countryunkid"

            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrShare.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillStaffOwnerShipGrid()
        Dim objAsset_securitiesT2_tran As New clsAsset_securitiesT2_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtStaffOwnership As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.


            'objAsset_securitiesT2_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtStaffOwnership = objAsset_securitiesT2_tran._Datasource

            'If dtStaffOwnership.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtStaffOwnership.NewRow

            '    r.Item(3) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = -1
            '    dtStaffOwnership.Rows.Add(r)
            'End If

            Dim dsStaffOwnership As DataSet = objAsset_securitiesT2_tran.GetList("List", mintAssetDeclarationT2Unkid)

            dtStaffOwnership = dsStaffOwnership.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtStaffOwnership.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtStaffOwnership.NewRow
                dtStaffOwnership.Rows.Add(r)
            End If

            'Gajanan (07 Dec 2018) -- End

            gvStaffOwnerShip.DataSource = dtStaffOwnership
            gvStaffOwnerShip.DataBind()

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'SetBusinessTotal()
            If blnRecordExist = False Then
                gvStaffOwnerShip.Rows(0).Visible = False
            End If
            'Gajanan (07 Dec 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValid_StaffOwnerShip() As Boolean
        Try
            If dtpStep3_Acquisition_date.IsNull OrElse eZeeDate.convertDate(dtpStep3_Acquisition_date.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtpStep3_Acquisition_date.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblerrordtpAcquisition_date.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Reset_StaffOwnerShip()
        Try

            drpStep3_Securitiestype.SelectedIndex = 0
            txtStep3_Certificate_no.Text = ""
            txtStep3_No_of_shares.Text = "0"
            txtStep3_Company_business_name.Text = ""
            txtStep3_Marketvalue.Text = ""

            dtpStep3_Acquisition_date.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

            btnAddStaffOwnerShip.Enabled = True
            btnUpdateStaffOwnerShip.Enabled = False
            drpStep3_CurrencyShare.SelectedIndex = 0

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Dropdown Events"
    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    'Protected Sub drpSecuritiestype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpStep3_Securitiestype.SelectedIndexChanged
    '    Try

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        popupAddEdit.Show()
    '    End Try
    'End Sub
    'Sohail (07 Dec 2018) -- End

#End Region

#Region " Button's Events "
    Protected Sub btnAddStaffOwnerShip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddStaffOwnerShip.Click
        Try
            If IsValid_StaffOwnerShip() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtStaffOwnership.NewRow

            'drRow.Item("assetsecuritiest2tranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("securitiestypeunkid") = drpStep3_Securitiestype.SelectedValue
            'drRow.Item("certificate_no") = txtStep3_Certificate_no.Text.Trim
            'drRow.Item("no_of_shares") = CInt(txtStep3_No_of_shares.Text.Trim)
            'drRow.Item("company_business_name") = txtStep3_Company_business_name.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpStep3_CurrencyShare.SelectedItem.Value)
            'drRow.Item("market_value") = txtStep3_Marketvalue.Text.Trim
            'drRow.Item("acquisition_date") = dtpStep3_Acquisition_date.GetDate
            'drRow.Item("isfinalsaved") = 0

            'Update_DataGridview_DataTable_Extra_Columns(gvStaffOwnerShip, drRow, CInt(drpStep3_CurrencyShare.SelectedValue))


            'dtStaffOwnership.Rows.Add(drRow)

            'gvStaffOwnerShip.DataSource = dtStaffOwnership
            'gvStaffOwnerShip.DataBind()


            Dim objStaffOwnership As New clsAsset_securitiesT2_tran

            objStaffOwnership._Assetsecuritiest2tranunkid = -1
            objStaffOwnership._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid.ToString()
            objStaffOwnership._Securitiestypeunkid = drpStep3_Securitiestype.SelectedValue
            objStaffOwnership._Certificate_No = txtStep3_Certificate_no.Text.Trim
            objStaffOwnership._No_Of_Shares = CInt(txtStep3_No_of_shares.Text.Trim)
            objStaffOwnership._Company_Business_Name = txtStep3_Company_business_name.Text.Trim
            objStaffOwnership._Countryunkid = CInt(drpStep3_CurrencyShare.SelectedItem.Value)
            objStaffOwnership._Market_Value = txtStep3_Marketvalue.Text.Trim
            objStaffOwnership._Acquisition_Date = dtpStep3_Acquisition_date.GetDate
            objStaffOwnership._Isfinalsaved = 0


            objStaffOwnership._Userunkid = CInt(Session("UserId"))
            If objStaffOwnership._Transactiondate = Nothing Then
                objStaffOwnership._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep3_CurrencyShare.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objStaffOwnership._Currencyunkid = intPaidCurrencyunkid
            objStaffOwnership._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objStaffOwnership._Baseexchangerate = decBaseExRate
            objStaffOwnership._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objStaffOwnership._Basemarket_Value = objStaffOwnership._Market_Value * decBaseExRate / decPaidExRate
            Else
                objStaffOwnership._Basemarket_Value = objStaffOwnership._Market_Value
            End If


            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objStaffOwnership._AuditUserid = CInt(Session("UserId"))
            Else
                objStaffOwnership._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objStaffOwnership._ClientIp = Session("IP_ADD").ToString()
            objStaffOwnership._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objStaffOwnership._FormName = mstrModuleName
            objStaffOwnership._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objStaffOwnership._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objStaffOwnership.Insert(Nothing, objAssetDeclare) = False Then
                If objStaffOwnership._Message <> "" Then
                    DisplayMessage.DisplayMessage(objStaffOwnership._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objStaffOwnership._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillStaffOwnerShipGrid()

            'Sohail (07 Dec 2018) -- End

            Call Reset_StaffOwnerShip()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateStaffOwnerShip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateStaffOwnerShip.Click
        Try
            If IsValid_StaffOwnerShip() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("ShareSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtStaffOwnership.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtStaffOwnership.Rows(SrNo)

            'drRow.Item("securitiestypeunkid") = drpStep3_Securitiestype.SelectedValue
            'drRow.Item("certificate_no") = txtStep3_Certificate_no.Text.Trim
            ''Hemant (26 Nov 2018) -- Start
            ''Enhancement : Changes As per Rutta Request for UAT 
            ''drRow.Item("no_of_shares") = txtNo_of_shares.Text.Trim
            'drRow.Item("no_of_shares") = CInt(txtStep3_No_of_shares.Text.Trim)
            ''Hemant (26 Nov 2018) -- End
            'drRow.Item("company_business_name") = txtStep3_Company_business_name.Text.Trim
            'drRow.Item("market_value") = txtStep3_Marketvalue.Text.Trim
            'drRow.Item("countryunkid") = CInt(drpStep3_CurrencyShare.SelectedItem.Value)
            'drRow.Item("acquisition_date") = dtpStep3_Acquisition_date.GetDate

            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvStaffOwnerShip, drRow, CInt(drpStep3_CurrencyShare.SelectedValue))

            'dtStaffOwnership.AcceptChanges()

            'gvStaffOwnerShip.DataSource = dtStaffOwnership
            'gvStaffOwnerShip.DataBind()


            Dim objStaffOwnership As New clsAsset_securitiesT2_tran

            objStaffOwnership.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvStaffOwnerShip.DataKeys(SrNo).Item("assetsecuritiest2tranunkid")), Nothing)
            objStaffOwnership._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objStaffOwnership._Securitiestypeunkid = drpStep3_Securitiestype.SelectedValue
            objStaffOwnership._Certificate_No = txtStep3_Certificate_no.Text.Trim
            objStaffOwnership._No_Of_Shares = CInt(txtStep3_No_of_shares.Text.Trim)
            objStaffOwnership._Company_Business_Name = txtStep3_Company_business_name.Text.Trim
            objStaffOwnership._Market_Value = txtStep3_Marketvalue.Text.Trim
            objStaffOwnership._Countryunkid = CInt(drpStep3_CurrencyShare.SelectedItem.Value)
            objStaffOwnership._Acquisition_Date = dtpStep3_Acquisition_date.GetDate
            objStaffOwnership._Isfinalsaved = 0

            objStaffOwnership._Userunkid = CInt(Session("UserId"))
            If objStaffOwnership._Transactiondate = Nothing Then
                objStaffOwnership._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep3_CurrencyShare.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objStaffOwnership._Currencyunkid = intPaidCurrencyunkid
            objStaffOwnership._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objStaffOwnership._Baseexchangerate = decBaseExRate
            objStaffOwnership._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objStaffOwnership._Basemarket_Value = objStaffOwnership._Market_Value * decBaseExRate / decPaidExRate
            Else
                objStaffOwnership._Basemarket_Value = objStaffOwnership._Market_Value
            End If


            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objStaffOwnership._AuditUserid = CInt(Session("UserId"))
            Else
                objStaffOwnership._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objStaffOwnership._ClientIp = Session("IP_ADD").ToString()
            objStaffOwnership._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objStaffOwnership._FormName = mstrModuleName
            objStaffOwnership._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End


            objStaffOwnership._IsFromWeb = True


            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objStaffOwnership.Update(False, Nothing, objAssetDeclare) = False Then
                If objStaffOwnership._Message <> "" Then
                    DisplayMessage.DisplayMessage(objStaffOwnership._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objStaffOwnership._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillStaffOwnerShipGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_StaffOwnerShip()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetStaffOwnerShip_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetStaffOwnerShip.Click
        Try
            Call Reset_StaffOwnerShip()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Gajanan (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteStaffOwnership_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteStaffOwnership.buttonDelReasonYes_Click
        Dim objStaff As New clsAsset_securitiesT2_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("ShareSrNo"))
            objStaff.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvStaffOwnerShip.DataKeys(SrNo).Item("assetsecuritiest2tranunkid")), Nothing)
            objStaff._Isvoid = True
            objStaff._Voiduserunkid = CInt(Session("UserId"))
            objStaff._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objStaff._Voidreason = popupDeleteStaffOwnership.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objStaff._AuditUserid = CInt(Session("UserId"))
            Else
                objStaff._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objStaff._ClientIp = Session("IP_ADD").ToString()
            objStaff._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objStaff._FormName = mstrModuleName
            objStaff._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objStaff._IsFromWeb = True

            If objStaff.Void(CInt(gvStaffOwnerShip.DataKeys(SrNo).Item("assetsecuritiest2tranunkid")), CInt(Session("UserId")), objStaff._Voiddatetime, objStaff._Voidreason, Nothing) = True Then
                Call FillStaffOwnerShipGrid()
            ElseIf objStaff._Message <> "" Then
                DisplayMessage.DisplayMessage(objStaff._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan (07 Dec 2018) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvStaffOwnerShip_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvStaffOwnerShip.PageIndexChanging
        Try
            gvStaffOwnerShip.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvStaffOwnerShip.DataSource = dtStaffOwnership
            'gvStaffOwnerShip.DataBind()
            Call FillStaffOwnerShipGrid()
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvStaffOwnerShip_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvStaffOwnerShip.RowCommand
        Try
            If e.CommandName = "Change" Then

                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ShareSrNo") = SrNo
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'drpStep3_Securitiestype.SelectedValue = Convert.ToInt16(dtStaffOwnership.Rows(SrNo).Item("securitiestypeunkid").ToString)
                'txtStep3_Certificate_no.Text = dtStaffOwnership.Rows(SrNo).Item("certificate_no").ToString
                ''Hemant (23 Nov 2018) -- Start
                ''Enhancement : Changes As per Rutta Request for UAT 
                ''txtStep3_No_of_shares.Text = dtStaffOwnership.Rows(SrNo).Item("no_of_shares").ToString
                'txtStep3_No_of_shares.Text = Format(dtStaffOwnership.Rows(SrNo).Item("no_of_shares"), Session("fmtCurrency"))
                ''Hemant (23 Nov 2018) -- End
                'txtStep3_Company_business_name.Text = dtStaffOwnership.Rows(SrNo).Item("company_business_name").ToString
                'txtStep3_Marketvalue.Text = Format(dtStaffOwnership.Rows(SrNo).Item("market_value"), Session("fmtCurrency"))
                'drpStep3_CurrencyShare.SelectedValue = dtStaffOwnership.Rows(SrNo).Item("countryunkid").ToString

                'dtpStep3_Acquisition_date.SetDate = Convert.ToDateTime(dtStaffOwnership.Rows(SrNo).Item("acquisition_date").ToString)


                drpStep3_Securitiestype.SelectedValue = CInt(gvStaffOwnerShip.DataKeys(SrNo).Item("securitiestypeunkid").ToString)
                txtStep3_Certificate_no.Text = gvStaffOwnerShip.Rows(SrNo).Cells(3).Text
                txtStep3_No_of_shares.Text = Format(CDec(gvStaffOwnerShip.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))


                txtStep3_Company_business_name.Text = gvStaffOwnerShip.Rows(SrNo).Cells(5).Text
                txtStep3_Marketvalue.Text = Format(CDec(gvStaffOwnerShip.Rows(SrNo).Cells(7).Text), Session("fmtCurrency"))
                drpStep3_CurrencyShare.SelectedValue = CInt(gvStaffOwnerShip.DataKeys(SrNo).Item("countryunkid").ToString)

                dtpStep3_Acquisition_date.SetDate = Convert.ToDateTime(gvStaffOwnerShip.Rows(SrNo).Cells(8).Text)
                'Sohail (07 Dec 2018) -- End

                btnAddStaffOwnerShip.Enabled = False
                btnUpdateStaffOwnerShip.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtStaffOwnership.Rows(CInt(e.CommandArgument)).Delete()

                'If dtStaffOwnership.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtStaffOwnership.NewRow

                '    r.Item(3) = "None" ' To Hide the row and display only Row Header
                '    r.Item(4) = -1
                '    dtStaffOwnership.Rows.Add(r)
                'End If
                'dtStaffOwnership.AcceptChanges()

                'gvStaffOwnerShip.DataSource = dtStaffOwnership
                'gvStaffOwnerShip.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_StaffOwnerShip()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("ShareSrNo") = SrNo

                popupDeleteStaffOwnership.Show()
                'Sohail (07 Dec 2018) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvStaffOwnerShip_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvStaffOwnerShip.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Gajanan (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

                'If e.Row.Cells(3).Text = "None" AndAlso e.Row.Cells(2).Text = "-1" Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvStaffOwnerShip.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Gajanan (07 Dec 2018) -- End
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvStaffOwnerShip, "colhStep3_Acquisition_date", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvStaffOwnerShip, "colhStep3_Acquisition_date", False, True)).Text).ToShortDateString
                    e.Row.Cells(7).Text = Format(CDec(e.Row.Cells(7).Text), Session("fmtCurrency"))

                    'Hemant (23 Nov 2018) -- Start
                    'Enhancement : Changes As per Rutta Request for UAT 
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvStaffOwnerShip, "colhStep3_No_of_shares_step3", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvStaffOwnerShip, "colhStep3_No_of_shares_step3", False, True)).Text), Session("fmtCurrency"))
                    'Hemant (23 Nov 2018) -- End


                    'Gajanan (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

                    'Dim intCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(6).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(6).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(6).Text = ""
                    'End If

                    'Dim intSecuritiesId As Integer
                    'Integer.TryParse(e.Row.Cells(2).Text, intSecuritiesId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECURITIES, True, "Asset Securities")

                    'Dim drSecurities As DataRow() = dsList.Tables(0).Select("masterunkid = " & intSecuritiesId & " ", "name")

                    'If drSecurities.Length > 0 Then
                    '    e.Row.Cells(2).Text = drSecurities(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(2).Text = ""
                    'End If

                    'Gajanan (07 Dec 2018) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
#Region "TextBox Events"
    Protected Sub txtStep3_No_of_shares_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep3_No_of_shares.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep3_No_of_shares.Text = Format(CDec(txtStep3_No_of_shares.Text), Session("fmtCurrency"))
            If txtStep3_No_of_shares.Text.Trim <> "" Then txtStep3_No_of_shares.Text = Format(CDec(txtStep3_No_of_shares.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub txtMarketvalue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep3_Marketvalue.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep3_Marketvalue.Text = Format(CDec(txtStep3_Marketvalue.Text), Session("fmtCurrency"))
            If txtStep3_Marketvalue.Text.Trim <> "" Then txtStep3_Marketvalue.Text = Format(CDec(txtStep3_Marketvalue.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (23 Nov 2018) -- End

#End Region

#Region " Bank Accounts "

#Region " Private Method Functions "
    Private Sub FillComboBank()
        Dim objExchange As New clsExchangeRate
        'Hemant (19 Nov 2018) -- Start
        'Enhancement : Changes for NMB Requirement
        Dim dsCombos As DataSet
        Dim objAccType As New clsBankAccType
        'Hemant (19 Nov 2018) -- End
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrBank As DataSet
        'Sohail (07 Dec 2018) -- End
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrBank = objExchange.getComboList("Currency", True)
            'With drpStep4_CurrencyBank
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrBank.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            dsCombos = objAccType.getComboList(True, "AccType")
            With drpStep4_AccountTypeBankAccounts
                .DataValueField = "accounttypeunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("AccType")
                .DataBind()
            End With
            'Hemant (19 Nov 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillBankAccountsGrid()
        Dim objAssetBankAccounts As New clsAsset_bankT2_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtBankAccounts As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.



            'objAssetBankAccounts._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtBankAccounts = objAssetBankAccounts._Datasource

            'If dtBankAccounts.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtBankAccounts.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    'Hemant (03 Dec 2018) -- Start
            '    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            '    'r.Item(3) = ""
            '    r.Item(3) = "-1"
            '    'Hemant (03 Dec 2018) -- End
            '    dtBankAccounts.Rows.Add(r)
            'End If

            Dim dsBank As DataSet = objAssetBankAccounts.GetList("List", mintAssetDeclarationT2Unkid)
            dtBankAccounts = dsBank.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtBankAccounts.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtBankAccounts.NewRow
                dtBankAccounts.Rows.Add(r)
            End If
            'Gajanan (07 Dec 2018) -- End

            gvBankAccounts.DataSource = dtBankAccounts
            gvBankAccounts.DataBind()

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If blnRecordExist = False Then
                gvBankAccounts.Rows(0).Visible = False
            End If
            'Gajanan (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValid_BankAccounts() As Boolean
        Try
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Reset_BankAccounts()
        Try
            txtStep4_BankName.Text = ""
            txtStep4_AccountNoBank.Text = ""
            txtStep4_AccountTypeBankAccounts.Text = ""
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            drpStep4_AccountTypeBankAccounts.SelectedValue = "0"
            'Hemant (19 Nov 2018) -- End

            txtStep4_DepositsSourceBankAccounts.Text = ""

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            txtStep4_BankSpecify.Text = ""
            'Hemant (03 Dec 2018) -- End


            btnAddBankAccounts.Enabled = True
            btnUpdateBankAccounts.Enabled = False

            drpStep4_CurrencyBank.SelectedIndex = 0

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddBankAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddBankAccounts.Click
        Try
            If IsValid_BankAccounts() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtBankAccounts.NewRow

            'drRow.Item("assetbankt2tranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("bank_name") = txtStep4_BankName.Text.Trim
            'drRow.Item("account_no") = txtStep4_AccountNoBank.Text.Trim

            'drRow.Item("countryunkid") = CInt(drpStep4_CurrencyBank.SelectedItem.Value)
            'drRow.Item("account_type") = txtStep4_AccountTypeBankAccounts.Text.Trim
            ''Hemant (19 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("accounttypeunkid") = drpStep4_AccountTypeBankAccounts.SelectedValue
            ''Hemant (19 Nov 2018) -- End

            ''Sohail (30 Nov 2018) -- Start
            ''NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            'drRow.Item("specify") = txtStep4_BankSpecify.Text.Trim
            ''Sohail (30 Nov 2018) -- End

            'drRow.Item("deposits_source") = txtStep4_DepositsSourceBankAccounts.Text.Trim


            'drRow.Item("isfinalsaved") = 0


            'Update_DataGridview_DataTable_Extra_Columns(gvBankAccounts, drRow, CInt(drpStep4_CurrencyBank.SelectedValue))


            'dtBankAccounts.Rows.Add(drRow)

            'gvBankAccounts.DataSource = dtBankAccounts
            'gvBankAccounts.DataBind()
            Dim objAssetBankAccounts As New clsAsset_bankT2_tran

            objAssetBankAccounts._Assetbanktrant2unkid = -1
            objAssetBankAccounts._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetBankAccounts._Bank_Name = txtStep4_BankName.Text.Trim
            objAssetBankAccounts._Account_No = txtStep4_AccountNoBank.Text.Trim
            objAssetBankAccounts._Countryunkid = CInt(drpStep4_CurrencyBank.SelectedItem.Value)
            objAssetBankAccounts._Account_Type = txtStep4_AccountTypeBankAccounts.Text.Trim
            objAssetBankAccounts._Accounttypeunkid = drpStep4_AccountTypeBankAccounts.SelectedValue
            objAssetBankAccounts._Specify = txtStep4_BankSpecify.Text.Trim
            objAssetBankAccounts._Deposits_Source = txtStep4_DepositsSourceBankAccounts.Text.Trim
            objAssetBankAccounts._Isfinalsaved = 0

            objAssetBankAccounts._Userunkid = CInt(Session("UserId"))
            If objAssetBankAccounts._Transactiondate = Nothing Then
                objAssetBankAccounts._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep4_CurrencyBank.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetBankAccounts._Currencyunkid = intPaidCurrencyunkid
            objAssetBankAccounts._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetBankAccounts._Baseexchangerate = decBaseExRate
            objAssetBankAccounts._Exchangerate = decPaidExRate

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetBankAccounts._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetBankAccounts._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetBankAccounts._ClientIp = Session("IP_ADD").ToString()
            objAssetBankAccounts._HostName = Session("HOST_NAME").ToString()
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetBankAccounts._FormName = mstrModuleName
            objAssetBankAccounts._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End
            objAssetBankAccounts._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetBankAccounts.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetBankAccounts._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetBankAccounts._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetBankAccounts._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillBankAccountsGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_BankAccounts()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateBankAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateBankAccounts.Click
        Try
            If IsValid_BankAccounts() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("BankSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtBankAccounts.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtBankAccounts.Rows(SrNo)

            'drRow.Item("bank_name") = txtStep4_BankName.Text.Trim
            'drRow.Item("account_no") = txtStep4_AccountNoBank.Text.Trim

            'drRow.Item("countryunkid") = CInt(drpStep4_CurrencyBank.SelectedItem.Value)
            'drRow.Item("account_type") = txtStep4_AccountTypeBankAccounts.Text.Trim
            ''Hemant (19 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("accounttypeunkid") = drpStep4_AccountTypeBankAccounts.SelectedValue
            ''Hemant (19 Nov 2018) -- End

            ''Sohail (30 Nov 2018) -- Start
            ''NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            'drRow.Item("specify") = txtStep4_BankSpecify.Text.Trim
            ''Sohail (30 Nov 2018) -- End

            'drRow.Item("deposits_source") = txtStep4_DepositsSourceBankAccounts.Text.Trim


            'drRow.Item("isfinalsaved") = 0


            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvBankAccounts, drRow, CInt(drpStep4_CurrencyBank.SelectedValue))

            'dtBankAccounts.AcceptChanges()

            'gvBankAccounts.DataSource = dtBankAccounts
            'gvBankAccounts.DataBind()

            Dim objAssetBankAccounts As New clsAsset_bankT2_tran

            objAssetBankAccounts.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvBankAccounts.DataKeys(SrNo).Item("assetbankt2tranunkid")), Nothing)
            objAssetBankAccounts._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetBankAccounts._Bank_Name = txtStep4_BankName.Text.Trim
            objAssetBankAccounts._Account_No = txtStep4_AccountNoBank.Text.Trim
            objAssetBankAccounts._Countryunkid = CInt(drpStep4_CurrencyBank.SelectedItem.Value)
            objAssetBankAccounts._Account_Type = txtStep4_AccountTypeBankAccounts.Text.Trim
            objAssetBankAccounts._Accounttypeunkid = drpStep4_AccountTypeBankAccounts.SelectedValue
            objAssetBankAccounts._Specify = txtStep4_BankSpecify.Text.Trim
            objAssetBankAccounts._Deposits_Source = txtStep4_DepositsSourceBankAccounts.Text.Trim
            objAssetBankAccounts._Isfinalsaved = 0

            objAssetBankAccounts._Userunkid = CInt(Session("UserId"))
            If objAssetBankAccounts._Transactiondate = Nothing Then
                objAssetBankAccounts._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep4_CurrencyBank.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetBankAccounts._Currencyunkid = intPaidCurrencyunkid
            objAssetBankAccounts._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetBankAccounts._Baseexchangerate = decBaseExRate
            objAssetBankAccounts._Exchangerate = decPaidExRate

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetBankAccounts._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetBankAccounts._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetBankAccounts._ClientIp = Session("IP_ADD").ToString()
            objAssetBankAccounts._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetBankAccounts._FormName = mstrModuleName
            objAssetBankAccounts._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End


            objAssetBankAccounts._IsFromWeb = True


            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetBankAccounts.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetBankAccounts._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetBankAccounts._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetBankAccounts._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillBankAccountsGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_BankAccounts()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetBankAccounts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBankAccounts.Click
        Try
            Call Reset_BankAccounts()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub


    'Gajanan (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteBankAccount_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteBankAccount.buttonDelReasonYes_Click
        Dim objAssetBankAccounts As New clsAsset_bankT2_tran

        Try
            'Sohail (18 Feb 2020) -- Start
            'NMB Issue # : system deletes always first transaction on deleting any bank accounts transactions in asset declaration.
            'Dim SrNo As Integer = CInt(Me.ViewState("ShareSrNo"))
            Dim SrNo As Integer = CInt(Me.ViewState("BankSrNo"))
            'Sohail (18 Feb 2020) -- End
            objAssetBankAccounts.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvBankAccounts.DataKeys(SrNo).Item("assetbankt2tranunkid")), Nothing)
            objAssetBankAccounts._Isvoid = True
            objAssetBankAccounts._Voiduserunkid = CInt(Session("UserId"))
            objAssetBankAccounts._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetBankAccounts._Voidreason = popupDeleteBankAccount.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetBankAccounts._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetBankAccounts._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetBankAccounts._ClientIp = Session("IP_ADD").ToString()
            objAssetBankAccounts._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetBankAccounts._FormName = mstrModuleName
            objAssetBankAccounts._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetBankAccounts._IsFromWeb = True

            If objAssetBankAccounts.Void(CInt(gvBankAccounts.DataKeys(SrNo).Item("assetbankt2tranunkid")), CInt(Session("UserId")), objAssetBankAccounts._Voiddatetime, objAssetBankAccounts._Voidreason, Nothing) = True Then
                Call FillBankAccountsGrid()
            ElseIf objAssetBankAccounts._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetBankAccounts._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan (07 Dec 2018) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvBankAccounts_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvBankAccounts.PageIndexChanging
        Try
            gvBankAccounts.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvBankAccounts.DataSource = dtBankAccounts
            'gvBankAccounts.DataBind()
            Call FillBankAccountsGrid()
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvBankAccounts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBankAccounts.RowCommand
        Try
            If e.CommandName = "Change" Then

                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("BankSrNo") = SrNo

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'txtStep4_BankName.Text = dtBankAccounts.Rows(SrNo).Item("bank_name").ToString
                'txtStep4_AccountTypeBankAccounts.Text = dtBankAccounts.Rows(SrNo).Item("account_type").ToString
                ''Hemant (19 Nov 2018) -- Start
                ''Enhancement : Changes for NMB Requirement
                'drpStep4_AccountTypeBankAccounts.Text = dtBankAccounts.Rows(SrNo).Item("accounttypeunkid").ToString
                ''Hemant (19 Nov 2018) -- End
                ''Sohail (30 Nov 2018) -- Start
                ''NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                'txtStep4_BankSpecify.Text = dtBankAccounts.Rows(SrNo).Item("specify").ToString
                ''Sohail (30 Nov 2018) -- End
                'txtStep4_AccountNoBank.Text = dtBankAccounts.Rows(SrNo).Item("account_no").ToString
                'drpStep4_CurrencyBank.SelectedValue = dtBankAccounts(SrNo).Item("countryunkid").ToString
                'txtStep4_DepositsSourceBankAccounts.Text = dtBankAccounts.Rows(SrNo).Item("deposits_source").ToString
                'Sohail (07 Dec 2018) -- End


                txtStep4_BankName.Text = gvBankAccounts.Rows(SrNo).Cells(2).Text
                drpStep4_AccountTypeBankAccounts.Text = CInt(gvBankAccounts.DataKeys(SrNo).Item("accounttypeunkid").ToString)
                txtStep4_BankSpecify.Text = gvBankAccounts.Rows(SrNo).Cells(7).Text
                txtStep4_AccountNoBank.Text = gvBankAccounts.Rows(SrNo).Cells(4).Text
                drpStep4_CurrencyBank.SelectedValue = CInt(gvBankAccounts.DataKeys(SrNo).Item("countryunkid").ToString)
                txtStep4_DepositsSourceBankAccounts.Text = gvBankAccounts.Rows(SrNo).Cells(6).Text

                btnAddBankAccounts.Enabled = False
                btnUpdateBankAccounts.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtBankAccounts.Rows(CInt(e.CommandArgument)).Delete()

                'If dtBankAccounts.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtBankAccounts.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = ""
                '    dtBankAccounts.Rows.Add(r)
                'End If
                'dtBankAccounts.AcceptChanges()

                'gvBankAccounts.DataSource = dtBankAccounts
                'gvBankAccounts.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_BankAccounts()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("BankSrNo") = SrNo
                popupDeleteBankAccount.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvBankAccounts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBankAccounts.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "-1" Then
                '    'Hemant (03 Dec 2018) -- End
                '    e.Row.Visible = False
                'Else
                'Sohail (07 Dec 2018) -- End
                If IsDBNull(gvBankAccounts.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Gajanan (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

                    'Dim intCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(5).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(5).Text = ""
                    'End If

                    ''Hemant (19 Nov 2018) -- Start
                    ''Enhancement : Changes for NMB Requirement
                    'Dim objAccType As New clsBankAccType
                    'Dim intAccountTypeId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvBankAccounts, "colhStep4_AccountTypeBank", False, True)).Text, intAccountTypeId)
                    'dsList = objAccType.getComboList(True, "AccType")

                    'Dim drAccountType As DataRow() = dsList.Tables(0).Select("accounttypeunkid = " & intAccountTypeId & " ", "name")

                    'If drAccountType.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvBankAccounts, "colhStep4_AccountTypeBank", False, True)).Text = drAccountType(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvBankAccounts, "colhStep4_AccountTypeBank", False, True)).Text = ""
                    'End If
                    ''Hemant (19 Nov 2018) -- End


                    'Gajanan (07 Dec 2018) -- End

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region " Real Properties "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboProperties()
        Dim objExchange As New clsExchangeRate
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrProperties As DataSet
        'Sohail (07 Dec 2018) -- End
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrProperties = objExchange.getComboList("Currency", True)
            'With drpStep5_CurrencyProperties

            '    'Pinkal (30-Jan-2013) -- Start
            '    'Enhancement : TRA Changes
            '    '.DataValueField = "exchangerateunkid"
            '    .DataValueField = "countryunkid"
            '    'Pinkal (30-Jan-2013) -- End


            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrProperties.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (06 Apr 2012) -- End

    Private Sub FillRealPropertiesGrid()
        Dim objAssetRealProperties As New clsAsset_propertiesT2_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtProperties As DataTable
        'Sohail (07 Dec 2018) -- End
        Try
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

            'objAssetRealProperties._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtProperties = objAssetRealProperties._Datasource

            'If dtProperties.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtProperties.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = ""
            '    dtProperties.Rows.Add(r)
            'End If

            Dim dsProperties As DataSet = objAssetRealProperties.GetList("List", mintAssetDeclarationT2Unkid)
            dtProperties = dsProperties.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtProperties.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtProperties.NewRow
                dtProperties.Rows.Add(r)
            End If


            'Gajanan (07 Dec 2018) -- End
            gvRealProperties.DataSource = dtProperties
            gvRealProperties.DataBind()

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If blnRecordExist = False Then
                gvRealProperties.Rows(0).Visible = False
            End If
            'Gajanan (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValid_RealProperties() As Boolean
        Try
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement

            If dtpStep5_AcquisitionDateProperties.IsNull OrElse eZeeDate.convertDate(dtpStep5_AcquisitionDateProperties.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtpStep5_AcquisitionDateProperties.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblerrorAcquisitionDate_step5Properties.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If
            'Hemant (15 Nov 2018) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Reset_RealProperties()
        Try
            txtStep5_AssetName.Text = ""
            txtStep5_RegistrationTitleNoRealProperties.Text = ""
            txtStep5_FundsSourceRealProperties.Text = ""
            txtStep5_AssetUseRealProperties.Text = ""
            txtStep5_Location.Text = ""
            txtStep5_EstimatedValueRealProperties.Text = ""
            txtStep5_AssetLocationRealProperties.Text = ""
            lblStep5_RealPropertiesError.Text = ""

            btnAddRealProperties.Enabled = True
            btnUpdateRealProperties.Enabled = False

            drpStep5_CurrencyProperties.SelectedIndex = 0



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddRealProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRealProperties.Click
        Try
            If IsValid_RealProperties() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtProperties.NewRow

            'drRow.Item("assetpropertiest2tranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("asset_name") = txtStep5_AssetName.Text.Trim
            'drRow.Item("registration_title_no") = txtStep5_RegistrationTitleNoRealProperties.Text.Trim
            'drRow.Item("funds_source") = txtStep5_FundsSourceRealProperties.Text.Trim
            'drRow.Item("asset_use") = txtStep5_AssetUseRealProperties.Text.Trim

            'drRow.Item("countryunkid") = CInt(drpStep5_CurrencyProperties.SelectedItem.Value)


            'drRow.Item("location") = txtStep5_Location.Text.Trim

            'If txtStep5_EstimatedValueRealProperties.Text.Trim = "" Then
            '    drRow.Item("estimated_value") = 0
            'Else
            '    drRow.Item("estimated_value") = txtStep5_EstimatedValueRealProperties.Text.Trim
            'End If

            'drRow.Item("asset_location") = txtStep5_AssetLocationRealProperties.Text.Trim

            'drRow.Item("isfinalsaved") = 0

            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("acquisition_date") = dtpStep5_AcquisitionDateProperties.GetDate
            ''Hemant (15 Nov 2018) -- End

            'Update_DataGridview_DataTable_Extra_Columns(gvRealProperties, drRow, CInt(drpStep5_CurrencyProperties.SelectedValue))


            'dtProperties.Rows.Add(drRow)

            'gvRealProperties.DataSource = dtProperties
            'gvRealProperties.DataBind()

            Dim objAssetRealProperties As New clsAsset_propertiesT2_tran


            objAssetRealProperties._Assetpropertiest2tranunkid = -1
            objAssetRealProperties._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetRealProperties._Asset_Name = txtStep5_AssetName.Text.Trim
            objAssetRealProperties._Registration_Title_No = txtStep5_RegistrationTitleNoRealProperties.Text.Trim
            objAssetRealProperties._Funds_Source = txtStep5_FundsSourceRealProperties.Text.Trim
            objAssetRealProperties._Asset_Use = txtStep5_AssetUseRealProperties.Text.Trim
            objAssetRealProperties._Countryunkid = CInt(drpStep5_CurrencyProperties.SelectedItem.Value)
            objAssetRealProperties._Location = txtStep5_Location.Text.Trim

            If txtStep5_EstimatedValueRealProperties.Text.Trim = "" Then
                objAssetRealProperties._Estimated_Value = 0
            Else
                objAssetRealProperties._Estimated_Value = txtStep5_EstimatedValueRealProperties.Text.Trim
            End If

            objAssetRealProperties._Asset_Location = txtStep5_AssetLocationRealProperties.Text.Trim
            objAssetRealProperties._Isfinalsaved = 0
            objAssetRealProperties._Acquisition_Date = dtpStep5_AcquisitionDateProperties.GetDate

            objAssetRealProperties._Userunkid = CInt(Session("UserId"))
            If objAssetRealProperties._Transactiondate = Nothing Then
                objAssetRealProperties._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep5_CurrencyProperties.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetRealProperties._Currencyunkid = intPaidCurrencyunkid
            objAssetRealProperties._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetRealProperties._Baseexchangerate = decBaseExRate
            objAssetRealProperties._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetRealProperties._Baseestimated_Value = objAssetRealProperties._Estimated_Value * decBaseExRate / decPaidExRate
            Else
                objAssetRealProperties._Baseestimated_Value = objAssetRealProperties._Estimated_Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetRealProperties._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetRealProperties._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetRealProperties._ClientIp = Session("IP_ADD").ToString()
            objAssetRealProperties._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetRealProperties._FormName = mstrModuleName
            objAssetRealProperties._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetRealProperties._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetRealProperties.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetRealProperties._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetRealProperties._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetRealProperties._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillRealPropertiesGrid()

            'Sohail (07 Dec 2018) -- End

            Call Reset_RealProperties()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateRealProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateRealProperties.Click
        Try
            If IsValid_RealProperties() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("PropertiesSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtProperties.Rows(SrNo).BeginEdit()
            'Dim drRow As DataRow = dtProperties.Rows(SrNo)

            'drRow.Item("asset_name") = txtStep5_AssetName.Text.Trim
            'drRow.Item("registration_title_no") = txtStep5_RegistrationTitleNoRealProperties.Text.Trim
            'drRow.Item("funds_source") = txtStep5_FundsSourceRealProperties.Text.Trim
            'drRow.Item("asset_use") = txtStep5_AssetUseRealProperties.Text.Trim

            'drRow.Item("countryunkid") = CInt(drpStep5_CurrencyProperties.SelectedItem.Value)


            'drRow.Item("location") = txtStep5_Location.Text.Trim

            'If txtStep5_EstimatedValueRealProperties.Text.Trim = "" Then
            '    drRow.Item("estimated_value") = 0
            'Else
            '    drRow.Item("estimated_value") = txtStep5_EstimatedValueRealProperties.Text.Trim
            'End If

            'drRow.Item("asset_location") = txtStep5_AssetLocationRealProperties.Text.Trim


            'drRow.Item("isfinalsaved") = 0

            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("acquisition_date") = dtpStep5_AcquisitionDateProperties.GetDate.Date
            ''Hemant (15 Nov 2018) -- End

            'drRow.Item("AUD") = "U"
            'Update_DataGridview_DataTable_Extra_Columns(gvRealProperties, drRow, CInt(drpStep5_CurrencyProperties.SelectedValue))


            'dtProperties.AcceptChanges()

            'gvRealProperties.DataSource = dtProperties
            'gvRealProperties.DataBind()

            Dim objAssetRealProperties As New clsAsset_propertiesT2_tran

            objAssetRealProperties.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvRealProperties.DataKeys(SrNo).Item("assetpropertiest2tranunkid")), Nothing)
            objAssetRealProperties._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetRealProperties._Asset_Name = txtStep5_AssetName.Text.Trim
            objAssetRealProperties._Registration_Title_No = txtStep5_RegistrationTitleNoRealProperties.Text.Trim
            objAssetRealProperties._Funds_Source = txtStep5_FundsSourceRealProperties.Text.Trim
            objAssetRealProperties._Asset_Use = txtStep5_AssetUseRealProperties.Text.Trim
            objAssetRealProperties._Countryunkid = CInt(drpStep5_CurrencyProperties.SelectedItem.Value)


            objAssetRealProperties._Location = txtStep5_Location.Text.Trim

            If txtStep5_EstimatedValueRealProperties.Text.Trim = "" Then
                objAssetRealProperties._Estimated_Value = 0
            Else
                objAssetRealProperties._Estimated_Value = txtStep5_EstimatedValueRealProperties.Text.Trim
            End If
            objAssetRealProperties._Asset_Location = txtStep5_AssetLocationRealProperties.Text.Trim
            objAssetRealProperties._Isfinalsaved = 0
            objAssetRealProperties._Acquisition_Date = dtpStep5_AcquisitionDateProperties.GetDate.Date

            objAssetRealProperties._Userunkid = CInt(Session("UserId"))
            If objAssetRealProperties._Transactiondate = Nothing Then
                objAssetRealProperties._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep5_CurrencyProperties.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetRealProperties._Currencyunkid = intPaidCurrencyunkid
            objAssetRealProperties._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetRealProperties._Baseexchangerate = decBaseExRate
            objAssetRealProperties._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetRealProperties._Baseestimated_Value = objAssetRealProperties._Estimated_Value * decBaseExRate / decPaidExRate
            Else
                objAssetRealProperties._Baseestimated_Value = objAssetRealProperties._Estimated_Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetRealProperties._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetRealProperties._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetRealProperties._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetRealProperties._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetRealProperties._ClientIp = Session("IP_ADD").ToString()
            objAssetRealProperties._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetRealProperties._FormName = mstrModuleName
            objAssetRealProperties._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetRealProperties._IsFromWeb = True


            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetRealProperties.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetRealProperties._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetRealProperties._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetRealProperties._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillRealPropertiesGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_RealProperties()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetRealProperties_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetRealProperties.Click
        Try
            Call Reset_RealProperties()
            popupAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Gajanan (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteProperties_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteProperties.buttonDelReasonYes_Click
        Dim objAssetRealProperties As New clsAsset_propertiesT2_tran
        Try
            'Sohail (18 Feb 2020) -- Start
            'NMB Issue # : system deletes always first transaction on deleting any real properties transactions in asset declaration.
            'Dim SrNo As Integer = CInt(Me.ViewState("ShareSrNo"))
            Dim SrNo As Integer = CInt(Me.ViewState("PropertiesSrNo"))
            'Sohail (18 Feb 2020) -- End
            objAssetRealProperties.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvRealProperties.DataKeys(SrNo).Item("assetpropertiest2tranunkid")), Nothing)
            objAssetRealProperties._Isvoid = True
            objAssetRealProperties._Voiduserunkid = CInt(Session("UserId"))
            objAssetRealProperties._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAssetRealProperties._Voidreason = popupDeleteProperties.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetRealProperties._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetRealProperties._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetRealProperties._ClientIp = Session("IP_ADD").ToString()
            objAssetRealProperties._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetRealProperties._FormName = mstrModuleName
            objAssetRealProperties._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetRealProperties._IsFromWeb = True

            If objAssetRealProperties.Void(CInt(gvRealProperties.DataKeys(SrNo).Item("assetpropertiest2tranunkid")), CInt(Session("UserId")), objAssetRealProperties._Voiddatetime, objAssetRealProperties._Voidreason, Nothing) = True Then
                Call FillRealPropertiesGrid()
            ElseIf objAssetRealProperties._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetRealProperties._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan (07 Dec 2018) -- End
#End Region

#Region " GridView Events "

    Protected Sub gvRealProperties_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRealProperties.PageIndexChanging
        Try
            gvRealProperties.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvRealProperties.DataSource = dtProperties
            'gvRealProperties.DataBind()
            Call FillRealPropertiesGrid()
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvRealProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRealProperties.RowCommand
        Try
            If e.CommandName = "Change" Then
                lblStep5_RealPropertiesError.Text = ""

                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("PropertiesSrNo") = SrNo
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.


                'txtStep5_AssetName.Text = dtProperties.Rows(SrNo).Item("asset_name").ToString
                'txtStep5_RegistrationTitleNoRealProperties.Text = dtProperties.Rows(SrNo).Item("registration_title_no").ToString
                'txtStep5_FundsSourceRealProperties.Text = dtProperties.Rows(SrNo).Item("funds_source").ToString
                'txtStep5_AssetUseRealProperties.Text = dtProperties.Rows(SrNo).Item("asset_use").ToString

                'drpStep5_CurrencyProperties.SelectedValue = dtProperties.Rows(SrNo).Item("countryunkid").ToString


                'txtStep5_Location.Text = dtProperties.Rows(SrNo).Item("location").ToString
                'txtStep5_EstimatedValueRealProperties.Text = Format(dtProperties.Rows(SrNo).Item("estimated_value"), Session("fmtCurrency"))
                'txtStep5_AssetLocationRealProperties.Text = dtProperties.Rows(SrNo).Item("asset_location").ToString

                ''Hemant (15 Nov 2018) -- Start
                ''Enhancement : Changes for NMB Requirement
                'If dtProperties.Rows(SrNo).Item("acquisition_date") IsNot DBNull.Value Then
                '    dtpStep5_AcquisitionDateProperties.SetDate = Convert.ToDateTime(dtProperties.Rows(SrNo).Item("acquisition_date"))
                'End If
                ''Hemant (15 Nov 2018) -- End

                'Sohail (19 Feb 2020) -- Start
                'NMB Issue # : .Net 4.0 is encoding single quote to &#39;.
                'txtStep5_AssetName.Text = gvRealProperties.Rows(SrNo).Cells(2).Text
                'txtStep5_RegistrationTitleNoRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(4).Text
                'txtStep5_FundsSourceRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(7).Text
                'txtStep5_AssetUseRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(9).Text
                txtStep5_AssetName.Text = gvRealProperties.Rows(SrNo).Cells(2).Text.Replace("&#39;", "'")
                txtStep5_RegistrationTitleNoRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(4).Text.Replace("&#39;", "'")
                txtStep5_FundsSourceRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(7).Text.Replace("&#39;", "'")
                txtStep5_AssetUseRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(9).Text.Replace("&#39;", "'")
                'Sohail (19 Feb 2020) -- End
                drpStep5_CurrencyProperties.SelectedValue = CInt(gvRealProperties.DataKeys(SrNo).Item("countryunkid").ToString)

                txtStep5_Location.Text = gvRealProperties.Rows(SrNo).Cells(3).Text
                txtStep5_EstimatedValueRealProperties.Text = Format(CDec(gvRealProperties.Rows(SrNo).Cells(5).Text), Session("fmtCurrency"))
                txtStep5_AssetLocationRealProperties.Text = gvRealProperties.Rows(SrNo).Cells(7).Text

                If gvRealProperties.Rows(SrNo).Cells(10) IsNot DBNull.Value Then
                    dtpStep5_AcquisitionDateProperties.SetDate = Convert.ToDateTime(gvRealProperties.Rows(SrNo).Cells(10).Text)
                End If
                'Sohail (07 Dec 2018) -- End

                btnAddRealProperties.Enabled = False
                btnUpdateRealProperties.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtProperties.Rows(CInt(e.CommandArgument)).Delete()

                'If dtProperties.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtProperties.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(4) = ""
                '    dtProperties.Rows.Add(r)
                'End If
                'dtProperties.AcceptChanges()

                'gvRealProperties.DataSource = dtProperties
                'gvRealProperties.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_RealProperties()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("PropertiesSrNo") = SrNo
                popupDeleteProperties.Show()

                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvRealProperties_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRealProperties.RowDataBound
        Try
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(4).Text = "&nbsp;" Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvRealProperties.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Sohail (07 Dec 2018) -- End
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency"))
                    'Hemant (15 Nov 2018) -- Start
                    'Enhancement : Changes for NMB Requirement
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRealProperties, "colhStep5_AcquisitionDateProperties", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRealProperties, "colhStep5_AcquisitionDateProperties", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvRealProperties, "colhStep5_AcquisitionDateProperties", False, True)).Text).ToShortDateString
                    End If
                    'Hemant (15 Nov 2018) -- End

                    'Dim intCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(6).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(6).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(6).Text = ""
                    'End If

                    End If

                End If
            'Gajanan (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
#Region "TextBox Events"
    Protected Sub txtEstimatedValueRealProperties_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep5_EstimatedValueRealProperties.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep5_EstimatedValueRealProperties.Text = Format(CDec(txtStep5_EstimatedValueRealProperties.Text), Session("fmtCurrency"))
            If txtStep5_EstimatedValueRealProperties.Text.Trim <> "" Then txtStep5_EstimatedValueRealProperties.Text = Format(CDec(txtStep5_EstimatedValueRealProperties.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (23 Nov 2018) -- End


#End Region

#Region " Liabilities "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboLiabilities()
        Dim objExchange As New clsExchangeRate
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrLiabilities As DataSet
        'Sohail (07 Dec 2018) -- End
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrLiabilities = objExchange.getComboList("Currency", True)
            'With drpStep6_OrigCurrencyLiabilities
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrLiabilities.Tables("Currency")
            '    .DataBind()
            'End With

            'With drpStep6_OutCurrencyLiabilities
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    'Sohail (07 Dec 2018) -- Start
            '    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            '    '.DataSource = dsCurrLiabilities.Tables("Currency")
            '    .DataSource = dsCurrLiabilities.Tables("Currency").Copy
            '    'Sohail (07 Dec 2018) -- End
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillLiabilitiesGrid()
        Dim objAssetLiabilities As New clsAsset_liabilitiesT2_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtLiabilities As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'objAssetLiabilities._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtLiabilities = objAssetLiabilities._Datasource

            'If dtLiabilities.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtLiabilities.NewRow

            '    r.Item(2) = "None" ' To Hide the row and display only Row Header
            '    r.Item(3) = ""
            '    dtLiabilities.Rows.Add(r)
            'End If
            Dim dsLiabilities As DataSet = objAssetLiabilities.GetList("List", mintAssetDeclarationT2Unkid)
            dtLiabilities = dsLiabilities.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtLiabilities.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtLiabilities.NewRow
                dtLiabilities.Rows.Add(r)
            End If
            'Sohail (07 Dec 2018) -- End

            gvLiabilities.DataSource = dtLiabilities
            gvLiabilities.DataBind()

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'SetBusinessTotal()
            If blnRecordExist = False Then
                gvLiabilities.Rows(0).Visible = False
            End If
            'Sohail (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValid_Liabilities() As Boolean
        Try



            If dtStep6_Liability_dateLiabilities.IsNull OrElse eZeeDate.convertDate(dtStep6_Liability_dateLiabilities.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtStep6_Liability_dateLiabilities.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblErrordtLiability_date_step6Liabilities.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If

            If dtStep6_Payoff_dateLiabilities.IsNull OrElse eZeeDate.convertDate(dtStep6_Payoff_dateLiabilities.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtStep6_Payoff_dateLiabilities.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblErrordtPayoff_date_step6Liabilities.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If
            'Hemant (15 Nov 2018) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Reset_Liabilities()
        Try
            txtStep6_InstitutionName.Text = ""
            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            'txtLiabilityType.Text = ""
            txtStep6_RemarkLiabilities.Text = ""
            drpStep6_LiabilityTypeLiabilities.SelectedValue = 0


            dtStep6_Liability_dateLiabilities.SetDate = Nothing
            dtStep6_Payoff_dateLiabilities.SetDate = Nothing
            'Hemant (15 Nov 2018) -- End
            txtStep6_OriginalBalanceLiabilities.Text = ""
            drpStep6_OrigCurrencyLiabilities.SelectedIndex = 0
            txtStep6_OutstandingBalanceLiabilities.Text = ""
            drpStep6_OutCurrencyLiabilities.SelectedIndex = 0
            txtStep6_Duration.Text = ""
            txtStep6_LoanPurposeLiabilities.Text = ""

            btnAddLiabilities.Enabled = True
            btnUpdateLiabilities.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddLiabilities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddLiabilities.Click
        Try
            If IsValid_Liabilities() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtLiabilities.NewRow

            'drRow.Item("assetliabilitiest2tranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("institution_name") = txtStep6_InstitutionName.Text.Trim
            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            ''drRow.Item("liability_type") = txtLiabilityType.Text.Trim
            'drRow.Item("liability_type") = txtStep6_RemarkLiabilities.Text.Trim
            'drRow.Item("liabilitytypeunkid") = Convert.ToInt32(drpStep6_LiabilityTypeLiabilities.SelectedValue)
            'drRow.Item("liabilitydate") = dtStep6_Liability_dateLiabilities.GetDate.Date
            'drRow.Item("payoffdate") = dtStep6_Payoff_dateLiabilities.GetDate.Date
            ''Hemant (15 Nov 2018) -- End
            'drRow.Item("original_balance") = txtStep6_OriginalBalanceLiabilities.Text.Trim
            'drRow.Item("outstanding_balance") = txtStep6_OutstandingBalanceLiabilities.Text.Trim

            'drRow.Item("origcountryunkid") = CInt(drpStep6_OrigCurrencyLiabilities.SelectedItem.Value)
            'drRow.Item("outcountryunkid") = CInt(drpStep6_OutCurrencyLiabilities.SelectedItem.Value)


            'drRow.Item("loan_purpose") = txtStep6_LoanPurposeLiabilities.Text.Trim

            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("duration") = txtStep6_Duration.Text.Trim

            'Update_DataGridview_DataTable_Extra_Columns(gvLiabilities, drRow, CInt(drpStep6_OrigCurrencyLiabilities.SelectedValue))

            'Update_DataGridview_DataTable_Extra_Columns(gvLiabilities, drRow, CInt(drpStep6_OutCurrencyLiabilities.SelectedValue))

            'dtLiabilities.Rows.Add(drRow)

            'gvLiabilities.DataSource = dtLiabilities
            'gvLiabilities.DataBind()

            Dim objAssetLiabilities As New clsAsset_liabilitiesT2_tran
            objAssetLiabilities._Assetliabilitiest2tranunkid = -1
            objAssetLiabilities._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetLiabilities._Institution_Name = txtStep6_InstitutionName.Text.Trim
            objAssetLiabilities._Liability_Type = txtStep6_RemarkLiabilities.Text.Trim
            objAssetLiabilities._Liabilitytypeunkid = Convert.ToInt32(drpStep6_LiabilityTypeLiabilities.SelectedValue)
            objAssetLiabilities._Liabilitydate = dtStep6_Liability_dateLiabilities.GetDate.Date
            objAssetLiabilities._Payoffdate = dtStep6_Payoff_dateLiabilities.GetDate.Date
            objAssetLiabilities._Loan_Purpose = txtStep6_LoanPurposeLiabilities.Text.Trim
            objAssetLiabilities._Isfinalsaved = 0
            objAssetLiabilities._Duration = txtStep6_Duration.Text.Trim
            objAssetLiabilities._Original_Balance = CDec(txtStep6_OriginalBalanceLiabilities.Text.Trim)
            objAssetLiabilities._Origcountryunkid = CInt(drpStep6_OrigCurrencyLiabilities.SelectedItem.Value)
            objAssetLiabilities._Outstanding_Balance = CDec(txtStep6_OutstandingBalanceLiabilities.Text.Trim)
            objAssetLiabilities._Outcountryunkid = CInt(drpStep6_OutCurrencyLiabilities.SelectedItem.Value)

            objAssetLiabilities._Userunkid = CInt(Session("UserId"))
            If objAssetLiabilities._Transactiondate = Nothing Then
                objAssetLiabilities._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep6_OrigCurrencyLiabilities.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetLiabilities._Origcurrencyunkid = intPaidCurrencyunkid
            objAssetLiabilities._Origbasecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetLiabilities._Origbaseexchangerate = decBaseExRate
            objAssetLiabilities._Origexchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetLiabilities._BaseOriginalBalance = objAssetLiabilities._Original_Balance * decBaseExRate / decPaidExRate
            Else
                objAssetLiabilities._BaseOriginalBalance = objAssetLiabilities._Original_Balance
            End If
            intCountryUnkId = 0
            decBaseExRate = 0
            decPaidExRate = 0
            intPaidCurrencyunkid = 0
            Call GetCurrencyRate(CInt(drpStep6_OutCurrencyLiabilities.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetLiabilities._Outcurrencyunkid = intPaidCurrencyunkid
            objAssetLiabilities._Outbasecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetLiabilities._Outbaseexchangerate = decBaseExRate
            objAssetLiabilities._Outexchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetLiabilities._BaseOutstandingBalance = objAssetLiabilities._Outstanding_Balance * decBaseExRate / decPaidExRate
            Else
                objAssetLiabilities._BaseOutstandingBalance = objAssetLiabilities._Outstanding_Balance
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetLiabilities._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetLiabilities._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetLiabilities._ClientIp = Session("IP_ADD").ToString()
            objAssetLiabilities._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetLiabilities._FormName = mstrModuleName
            objAssetLiabilities._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetLiabilities._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetLiabilities.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetLiabilities._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetLiabilities._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetLiabilities._Assetdeclarationt2unkid(Session("Database_Name"))
            End If


            FillLiabilitiesGrid()

            'Sohail (07 Dec 2018) -- End

            Call Reset_Liabilities()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateLiabilities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateLiabilities.Click
        Try
            If IsValid_Liabilities() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("LiabilitiesSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtLiabilities.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtLiabilities.Rows(SrNo)

            'drRow.Item("institution_name") = txtStep6_InstitutionName.Text.Trim
            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            ''drRow.Item("liability_type") = txtLiabilityType.Text.Trim
            'drRow.Item("liability_type") = txtStep6_RemarkLiabilities.Text.Trim
            'drRow.Item("liabilitytypeunkid") = Convert.ToInt32(drpStep6_LiabilityTypeLiabilities.SelectedValue)
            'drRow.Item("liabilitydate") = dtStep6_Liability_dateLiabilities.GetDate.Date
            'drRow.Item("payoffdate") = dtStep6_Payoff_dateLiabilities.GetDate.Date
            ''Hemant (15 Nov 2018) -- End
            'drRow.Item("original_balance") = txtStep6_OriginalBalanceLiabilities.Text.Trim
            'drRow.Item("outstanding_balance") = txtStep6_OutstandingBalanceLiabilities.Text.Trim

            'drRow.Item("origcountryunkid") = CInt(drpStep6_OrigCurrencyLiabilities.SelectedItem.Value)
            'drRow.Item("outcountryunkid") = CInt(drpStep6_OutCurrencyLiabilities.SelectedItem.Value)

            'drRow.Item("duration") = Convert.ToDecimal(txtStep6_Duration.Text.Trim)
            'drRow.Item("loan_purpose") = txtStep6_LoanPurposeLiabilities.Text.Trim

            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("AUD") = "U"

            'Update_DataGridview_DataTable_Extra_Columns(gvLiabilities, drRow, CInt(drpStep6_OrigCurrencyLiabilities.SelectedValue))

            'Update_DataGridview_DataTable_Extra_Columns(gvLiabilities, drRow, CInt(drpStep6_OutCurrencyLiabilities.SelectedValue))

            'dtLiabilities.AcceptChanges()

            'gvLiabilities.DataSource = dtLiabilities
            'gvLiabilities.DataBind()
            Dim objAssetLiabilities As New clsAsset_liabilitiesT2_tran
            objAssetLiabilities.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvLiabilities.DataKeys(SrNo).Item("assetliabilitiest2tranunkid")), Nothing)
            objAssetLiabilities._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetLiabilities._Institution_Name = txtStep6_InstitutionName.Text.Trim
            objAssetLiabilities._Liability_Type = txtStep6_RemarkLiabilities.Text.Trim
            objAssetLiabilities._Liabilitytypeunkid = Convert.ToInt32(drpStep6_LiabilityTypeLiabilities.SelectedValue)
            objAssetLiabilities._Liabilitydate = dtStep6_Liability_dateLiabilities.GetDate.Date
            objAssetLiabilities._Payoffdate = dtStep6_Payoff_dateLiabilities.GetDate.Date
            objAssetLiabilities._Loan_Purpose = txtStep6_LoanPurposeLiabilities.Text.Trim
            objAssetLiabilities._Isfinalsaved = 0
            objAssetLiabilities._Duration = txtStep6_Duration.Text.Trim
            objAssetLiabilities._Original_Balance = CDec(txtStep6_OriginalBalanceLiabilities.Text.Trim)
            objAssetLiabilities._Origcountryunkid = CInt(drpStep6_OrigCurrencyLiabilities.SelectedItem.Value)
            objAssetLiabilities._Outstanding_Balance = CDec(txtStep6_OutstandingBalanceLiabilities.Text.Trim)
            objAssetLiabilities._Outcountryunkid = CInt(drpStep6_OutCurrencyLiabilities.SelectedItem.Value)

            objAssetLiabilities._Userunkid = CInt(Session("UserId"))
            If objAssetLiabilities._Transactiondate = Nothing Then
                objAssetLiabilities._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep6_OrigCurrencyLiabilities.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetLiabilities._Origcurrencyunkid = intPaidCurrencyunkid
            objAssetLiabilities._Origbasecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetLiabilities._Origbaseexchangerate = decBaseExRate
            objAssetLiabilities._Origexchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetLiabilities._BaseOriginalBalance = objAssetLiabilities._Original_Balance * decBaseExRate / decPaidExRate
            Else
                objAssetLiabilities._BaseOriginalBalance = objAssetLiabilities._Original_Balance
            End If
            intCountryUnkId = 0
            decBaseExRate = 0
            decPaidExRate = 0
            intPaidCurrencyunkid = 0
            Call GetCurrencyRate(CInt(drpStep6_OutCurrencyLiabilities.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAssetLiabilities._Outcurrencyunkid = intPaidCurrencyunkid
            objAssetLiabilities._Outbasecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAssetLiabilities._Outbaseexchangerate = decBaseExRate
            objAssetLiabilities._Outexchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAssetLiabilities._BaseOutstandingBalance = objAssetLiabilities._Outstanding_Balance * decBaseExRate / decPaidExRate
            Else
                objAssetLiabilities._BaseOutstandingBalance = objAssetLiabilities._Outstanding_Balance
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetLiabilities._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetLiabilities._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetLiabilities._ClientIp = Session("IP_ADD").ToString()
            objAssetLiabilities._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetLiabilities._FormName = mstrModuleName
            objAssetLiabilities._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetLiabilities._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetLiabilities.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetLiabilities._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetLiabilities._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetLiabilities._Assetdeclarationt2unkid(Session("Database_Name"))
            End If


            FillLiabilitiesGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_Liabilities()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetLiabilities_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetLiabilities.Click
        Try
            Call Reset_Liabilities()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub popupDeleteLiabilities_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteLiabilities.buttonDelReasonYes_Click
        Dim objLiabilities As New clsAsset_liabilitiesT2_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("LiabilitiesSrNo"))
            objLiabilities.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvLiabilities.DataKeys(SrNo).Item("assetliabilitiest2tranunkid")), Nothing)
            objLiabilities._Isvoid = True
            objLiabilities._Voiduserunkid = CInt(Session("UserId"))
            objLiabilities._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objLiabilities._Voidreason = popupDeleteLiabilities.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objLiabilities._AuditUserid = CInt(Session("UserId"))
            Else
                objLiabilities._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objLiabilities._ClientIp = Session("IP_ADD").ToString()
            objLiabilities._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objLiabilities._FormName = mstrModuleName
            objLiabilities._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objLiabilities._IsFromWeb = True

            If objLiabilities.Void(CInt(gvLiabilities.DataKeys(SrNo).Item("assetliabilitiest2tranunkid")), CInt(Session("UserId")), objLiabilities._Voiddatetime, objLiabilities._Voidreason, Nothing) = True Then
                Call FillLiabilitiesGrid()
            ElseIf objLiabilities._Message <> "" Then
                DisplayMessage.DisplayMessage(objLiabilities._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

#End Region

#Region " GridView Events "

    Protected Sub gvLiabilities_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvLiabilities.PageIndexChanging
        Try
            gvLiabilities.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvLiabilities.DataSource = dtLiabilities
            'gvLiabilities.DataBind()
            Call FillLiabilitiesGrid()
            'Sohail (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvLiabilities_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLiabilities.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("LiabilitiesSrNo") = SrNo

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'txtStep6_InstitutionName.Text = dtLiabilities.Rows(SrNo).Item("institution_name").ToString
                'Hemant (15 Nov 2018) -- Start
                'Enhancement : Changes for NMB Requirement
                'txtLiabilityType.Text = dtLiabilities.Rows(SrNo).Item("liability_type").ToString
                'txtStep6_RemarkLiabilities.Text = dtLiabilities.Rows(SrNo).Item("liability_type").ToString
                'drpStep6_LiabilityTypeLiabilities.SelectedValue = dtLiabilities.Rows(SrNo).Item("liabilitytypeunkid").ToString
                'If dtLiabilities.Rows(SrNo).Item("liabilitydate") IsNot DBNull.Value Then
                '    dtStep6_Liability_dateLiabilities.SetDate = CDate(dtLiabilities.Rows(SrNo).Item("liabilitydate"))
                'End If
                'If dtLiabilities.Rows(SrNo).Item("payoffdate") IsNot DBNull.Value Then
                '    dtStep6_Payoff_dateLiabilities.SetDate = CDate(dtLiabilities.Rows(SrNo).Item("payoffdate"))
                'End If

                ''Hemant (15 Nov 2018) -- End
                'txtStep6_OriginalBalanceLiabilities.Text = Format(dtLiabilities.Rows(SrNo).Item("original_balance"), Session("fmtCurrency"))
                'drpStep6_OrigCurrencyLiabilities.SelectedValue = dtLiabilities.Rows(SrNo).Item("origcountryunkid").ToString

                'txtStep6_OutstandingBalanceLiabilities.Text = Format(dtLiabilities.Rows(SrNo).Item("outstanding_balance"), Session("fmtCurrency"))
                'drpStep6_OutCurrencyLiabilities.SelectedValue = dtLiabilities.Rows(SrNo).Item("outcountryunkid").ToString

                'txtStep6_Duration.Text = Format(dtLiabilities.Rows(SrNo).Item("duration"), Session("fmtCurrency"))

                'txtStep6_LoanPurposeLiabilities.Text = dtLiabilities.Rows(SrNo).Item("loan_purpose").ToString
                txtStep6_InstitutionName.Text = gvLiabilities.Rows(SrNo).Cells(2).Text
                drpStep6_LiabilityTypeLiabilities.SelectedValue = CInt(gvLiabilities.DataKeys(SrNo).Item("liabilitytypeunkid"))
                If gvLiabilities.DataKeys(SrNo).Item("liabilitydate") IsNot DBNull.Value Then
                    dtStep6_Liability_dateLiabilities.SetDate = CDate(gvLiabilities.DataKeys(SrNo).Item("liabilitydate"))
                End If
                If gvLiabilities.DataKeys(SrNo).Item("payoffdate") IsNot DBNull.Value Then
                    dtStep6_Payoff_dateLiabilities.SetDate = CDate(gvLiabilities.DataKeys(SrNo).Item("payoffdate"))
                End If

                txtStep6_OriginalBalanceLiabilities.Text = Format(CDec(gvLiabilities.Rows(SrNo).Cells(4).Text), Session("fmtCurrency"))
                drpStep6_OrigCurrencyLiabilities.SelectedValue = CInt(gvLiabilities.DataKeys(SrNo).Item("origcountryunkid").ToString)

                txtStep6_OutstandingBalanceLiabilities.Text = Format(CDec(gvLiabilities.Rows(SrNo).Cells(6).Text), Session("fmtCurrency"))
                drpStep6_OutCurrencyLiabilities.SelectedValue = CInt(gvLiabilities.DataKeys(SrNo).Item("outcountryunkid").ToString)

                txtStep6_Duration.Text = Format(CDec(gvLiabilities.Rows(SrNo).Cells(8).Text), Session("fmtCurrency"))

                txtStep6_LoanPurposeLiabilities.Text = gvLiabilities.Rows(SrNo).Cells(9).Text
                txtStep6_RemarkLiabilities.Text = gvLiabilities.Rows(SrNo).Cells(10).Text
                'Sohail (07 Dec 2018) -- End

                btnAddLiabilities.Enabled = False
                btnUpdateLiabilities.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtLiabilities.Rows(CInt(e.CommandArgument)).Delete()

                'If dtLiabilities.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtLiabilities.NewRow

                '    r.Item(2) = "None" ' To Hide the row and display only Row Header
                '    r.Item(3) = ""
                '    dtLiabilities.Rows.Add(r)
                'End If
                'dtLiabilities.AcceptChanges()

                'gvLiabilities.DataSource = dtLiabilities
                'gvLiabilities.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_Liabilities()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("LiabilitiesSrNo") = SrNo

                popupDeleteLiabilities.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvLiabilities_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLiabilities.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "0" Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvLiabilities.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Sohail (07 Dec 2018) -- End
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency"))

                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))
                    'Sohail (07 Dec 2018) -- End

                    'Hemant (13 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    e.Row.Cells(8).Text = Format(CDec(e.Row.Cells(8).Text), Session("fmtCurrency"))
                    'Hemant (13 Nov 2018) -- End

                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    'Dim intOrigCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(5).Text, intOrigCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intOrigCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(5).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(5).Text = ""
                    'End If

                    'e.Row.Cells(6).Text = Format(CDec(e.Row.Cells(6).Text), Session("fmtCurrency"))

                    'Dim intOutCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(5).Text, intOutCountryId)
                    'dsList = objExRate.GetList("Currency", True, False, 0, intOutCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(7).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(7).Text = ""
                    'End If

                    ''Hemant (15 Nov 2018) -- Start
                    ''Enhancement : Changes for NMB Requirement

                    'Dim intLiabilityType As Integer
                    'Integer.TryParse(e.Row.Cells(3).Text, intLiabilityType)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.LIABILITY_TYPE_ASSET_DECLARATION_TEMPLATE2, True, "Liability Type")

                    'Dim drLiabilityType As DataRow() = dsList.Tables(0).Select("masterunkid = " & intLiabilityType & " ", "name")

                    'If drLiabilityType.Length > 0 Then
                    '    e.Row.Cells(3).Text = drLiabilityType(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(3).Text = ""
                    'End If
                    'Sohail (07 Dec 2018) -- End

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvLiabilities, "colhStep6_Liability_dateLiabilities", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvLiabilities, "colhStep6_Liability_dateLiabilities", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvLiabilities, "colhStep6_Liability_dateLiabilities", False, True)).Text).ToShortDateString
                    End If
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvLiabilities, "colhStep6_Payoff_dateLiabilities", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvLiabilities, "colhStep6_Payoff_dateLiabilities", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvLiabilities, "colhStep6_Payoff_dateLiabilities", False, True)).Text).ToShortDateString
                    End If

                    'Hemant (15 Nov 2018) -- End

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
#Region "TextBox Events"
    Protected Sub txtOriginalBalanceLiabilities_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep6_OriginalBalanceLiabilities.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep6_OriginalBalanceLiabilities.Text = Format(CDec(txtStep6_OriginalBalanceLiabilities.Text), Session("fmtCurrency"))
            If txtStep6_OriginalBalanceLiabilities.Text.Trim <> "" Then txtStep6_OriginalBalanceLiabilities.Text = Format(CDec(txtStep6_OriginalBalanceLiabilities.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOutstandingBalanceLiabilities_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep6_OutstandingBalanceLiabilities.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep6_OutstandingBalanceLiabilities.Text = Format(CDec(txtStep6_OutstandingBalanceLiabilities.Text), Session("fmtCurrency"))
            If txtStep6_OutstandingBalanceLiabilities.Text.Trim <> "" Then txtStep6_OutstandingBalanceLiabilities.Text = Format(CDec(txtStep6_OutstandingBalanceLiabilities.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtDuration_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep6_Duration.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep6_Duration.Text = Format(CDec(txtStep6_Duration.Text), Session("fmtCurrency"))
            If txtStep6_Duration.Text.Trim <> "" Then txtStep6_Duration.Text = Format(CDec(txtStep6_Duration.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Hemant (23 Nov 2018) -- End


#End Region

#Region " Relatives "

#Region " Private Method Functions "
    'Sohail (06 Apr 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub FillComboRelatives()
        Dim dsCombos As DataSet
        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
            'With drpStep7_Relationship
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombos.Tables("Relations").Copy
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'Hemant (02 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillRelativesGrid()
        Dim objAssetRelatives As New clsAsset_relativesT2_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtRelatives As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

            'objAssetRelatives._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtRelatives = objAssetRelatives._Datasource

            'If dtRelatives.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtRelatives.NewRow

            '    r.Item(3) = "None" ' To Hide the row and display only Row Header
            '    r.Item(4) = "-1"
            '    dtRelatives.Rows.Add(r)
            'End If
            Dim dsRelatives As DataSet = objAssetRelatives.GetList("List", mintAssetDeclarationT2Unkid)
            dtRelatives = dsRelatives.Tables(0)
            Dim blnRecordExist As Boolean = True
            If dtRelatives.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtRelatives.NewRow
                dtRelatives.Rows.Add(r)
            End If
            'Hemant (07 Dec 2018) -- End
            gvRelatives.DataSource = dtRelatives
            gvRelatives.DataBind()

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If blnRecordExist = False Then
                gvRelatives.Rows(0).Visible = False
            End If
            'Hemant (07 Dec 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValid_Relatives() As Boolean
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'Dim drRelations As DataRow()
            'If mblnIsbtnEditClick = True Then
            '    drRelations = dtRelatives.Select(" assetrelativest2tranunkid <> " & Convert.ToInt32(dtRelatives.Rows(CInt(Me.ViewState("RelativesSrNo")))("assetrelativest2tranunkid")) & " AND relative_employeeunkid = " & drpStep7_RelativeEmployee.SelectedValue & "  ")
            'Else
            '    drRelations = dtRelatives.Select(" relative_employeeunkid = " & drpStep7_RelativeEmployee.SelectedValue & "  ")
            'End If

            'If drRelations.Length > 0 Then
            '    'Gajanan (28 Nov 2018) -- Start
            '    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry,you cannot add same Relative in employeement again."), Me)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 68, "Sorry,you cannot add same Relative in employeement again."), Me)
            '    'Gajanan (28 Nov 2018) -- End
            '    Return False
            'End If
            ''Hemant (15 Nov 2018) -- End

            '***** Dim drRelations As DataRow()
            'If mblnIsbtnEditClick = True Then
            '    drRelations = dtRelatives.Select(" assetrelativest2tranunkid <> " & Convert.ToInt32(dtRelatives.Rows(CInt(Me.ViewState("RelativesSrNo")))("assetrelativest2tranunkid")) & " AND relative_employeeunkid = " & drpStep7_RelativeEmployee.SelectedValue & "  ")
            'Else
            '    drRelations = dtRelatives.Select(" relative_employeeunkid = " & drpStep7_RelativeEmployee.SelectedValue & "  ")
            'End If

            'If drRelations.Length > 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 68, "Sorry,you cannot add same Relative in employeement again."), Me)
            '    Return False
            'End If
            'Sohail (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Reset_Relatives()
        Try
            drpStep7_RelativeEmployee.SelectedIndex = 0
            drpStep7_Relationship.SelectedIndex = 0
            txtRelativeEmployee.Text = ""
            chkOthers.Checked = False
            btnAddRelatives.Enabled = True
            btnUpdateRelatives.Enabled = False
            'Hemant (13 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            drpStep7_Relationship.Enabled = True
            'Hemant (13 Nov 2018) -- End
            chkOthers_CheckedChanged(Nothing, Nothing) 'Sohail (07 Dec 2018)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "
    Protected Sub btnAddRelatives_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddRelatives.Click
        Try
            If IsValid_Relatives() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtRelatives.NewRow

            'drRow.Item("assetrelativest2tranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("relative_employeeunkid") = drpStep7_RelativeEmployee.SelectedValue
            'If chkOthers.Checked = True AndAlso txtRelativeEmployee.Text.Length > 0 Then
            '    drRow.Item("relative_employeename") = txtRelativeEmployee.Text.ToString
            'Else
            '    drRow.Item("relative_employeename") = drpStep7_RelativeEmployee.SelectedItem.Text.ToString
            'End If

            'drRow.Item("relationshipunkid") = drpStep7_Relationship.SelectedValue

            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("userunkid") = -1

            'If IsDBNull(drRow.Item("transactiondate")) Or drRow.Item("transactiondate") Is Nothing Then
            '    drRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
            'End If

            'dtRelatives.Rows.Add(drRow)

            'gvRelatives.DataSource = dtRelatives
            'gvRelatives.DataBind()

            Dim objAssetRelatives As New clsAsset_relativesT2_tran

            objAssetRelatives._Assetrelativest2tranunkid = -1
            objAssetRelatives._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetRelatives._Relative_Employeeunkid = drpStep7_RelativeEmployee.SelectedValue
            If chkOthers.Checked = True AndAlso txtRelativeEmployee.Text.Length > 0 Then
                objAssetRelatives._Relative_Employeename = txtRelativeEmployee.Text.ToString
            Else
                objAssetRelatives._Relative_Employeename = drpStep7_RelativeEmployee.SelectedItem.Text.ToString
            End If

            objAssetRelatives._Relationshipunkid = drpStep7_Relationship.SelectedValue

            objAssetRelatives._Isfinalsaved = 0


            objAssetRelatives._Userunkid = CInt(Session("UserId"))
            If objAssetRelatives._Transactiondate = Nothing Then
                objAssetRelatives._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetRelatives._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetRelatives._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetRelatives._ClientIp = Session("IP_ADD").ToString()
            objAssetRelatives._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetRelatives._FormName = mstrModuleName
            objAssetRelatives._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetRelatives._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetRelatives.Insert(Nothing, objAssetDeclare) = False Then
                If objAssetRelatives._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetRelatives._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetRelatives._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillRelativesGrid()

            'Sohail (07 Dec 2018) -- End

            Call Reset_Relatives()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateRelatives_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateRelatives.Click
        Try
            If IsValid_Relatives() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("RelativesSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtRelatives.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtRelatives.Rows(SrNo)

            'drRow.Item("relative_employeeunkid") = drpStep7_RelativeEmployee.SelectedValue

            'If chkOthers.Checked = True AndAlso txtRelativeEmployee.Text.Length > 0 Then
            '    drRow.Item("relative_employeename") = txtRelativeEmployee.Text.ToString
            'Else
            '    drRow.Item("relative_employeename") = drpStep7_RelativeEmployee.SelectedItem.Text.ToString
            'End If

            'drRow.Item("relationshipunkid") = drpStep7_Relationship.SelectedValue

            'drRow.Item("isfinalsaved") = 0

            'drRow.Item("userunkid") = -1

            'If IsDBNull(drRow.Item("transactiondate")) Or drRow.Item("transactiondate") Is Nothing Then
            '    drRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
            'End If

            'drRow.Item("AUD") = "U"

            'dtRelatives.AcceptChanges()

            'gvRelatives.DataSource = dtRelatives
            'gvRelatives.DataBind()
            Dim objAssetRelatives As New clsAsset_relativesT2_tran
            objAssetRelatives.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvRelatives.DataKeys(SrNo).Item("assetrelativest2tranunkid")), Nothing)
            objAssetRelatives._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAssetRelatives._Relative_Employeeunkid = drpStep7_RelativeEmployee.SelectedValue
            If chkOthers.Checked = True AndAlso txtRelativeEmployee.Text.Length > 0 Then
                objAssetRelatives._Relative_Employeename = txtRelativeEmployee.Text.ToString
            Else
                objAssetRelatives._Relative_Employeename = drpStep7_RelativeEmployee.SelectedItem.Text.ToString
            End If

            objAssetRelatives._Relationshipunkid = drpStep7_Relationship.SelectedValue

            objAssetRelatives._Isfinalsaved = 0


            objAssetRelatives._Userunkid = CInt(Session("UserId"))
            If objAssetRelatives._Transactiondate = Nothing Then
                objAssetRelatives._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAssetRelatives._AuditUserid = CInt(Session("UserId"))
            Else
                objAssetRelatives._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAssetRelatives._ClientIp = Session("IP_ADD").ToString()
            objAssetRelatives._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAssetRelatives._FormName = mstrModuleName
            objAssetRelatives._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAssetRelatives._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAssetRelatives.Update(False, Nothing, objAssetDeclare) = False Then
                If objAssetRelatives._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssetRelatives._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAssetRelatives._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillRelativesGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_Relatives()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetRelatives_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetRelatives.Click
        Try
            Call Reset_Relatives()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteRelatives_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteRelatives.buttonDelReasonYes_Click
        Dim objRelatives As New clsAsset_relativesT2_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("RelativesSrNo"))
            objRelatives.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvRelatives.DataKeys(SrNo).Item("assetrelativest2tranunkid")), Nothing)
            objRelatives._Isvoid = True
            objRelatives._Voiduserunkid = CInt(Session("UserId"))
            objRelatives._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objRelatives._Voidreason = popupDeleteRelatives.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objRelatives._AuditUserid = CInt(Session("UserId"))
            Else
                objRelatives._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objRelatives._ClientIp = Session("IP_ADD").ToString()
            objRelatives._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objRelatives._FormName = mstrModuleName
            objRelatives._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objRelatives._IsFromWeb = True

            If objRelatives.Void(CInt(gvRelatives.DataKeys(SrNo).Item("assetrelativest2tranunkid")), CInt(Session("UserId")), objRelatives._Voiddatetime, objRelatives._Voidreason, Nothing) = True Then
                Call FillRelativesGrid()
            ElseIf objRelatives._Message <> "" Then
                DisplayMessage.DisplayMessage(objRelatives._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 Dec 2018) -- End

#End Region

#Region " GridView Events "

    Protected Sub gvRelatives_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvRelatives.PageIndexChanging
        Try
            gvRelatives.PageIndex = e.NewPageIndex
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'gvRelatives.DataSource = dtRelatives
            'gvRelatives.DataBind()
            Call FillRelativesGrid()
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvRelatives_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRelatives.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("RelativesSrNo") = SrNo

                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If Convert.ToInt16(dtRelatives.Rows(SrNo).Item("relative_employeeunkid")) > 0 Then
                '    drpStep7_RelativeEmployee.SelectedValue = Convert.ToInt16(dtRelatives.Rows(SrNo).Item("relative_employeeunkid"))
                '    chkOthers.Checked = False
                'Else
                '    txtRelativeEmployee.Text = dtRelatives.Rows(SrNo).Item("relative_employeename").ToString
                '    chkOthers.Checked = True
                'End If
                'chkOthers_CheckedChanged(sender, e)
                'drpStep7_Relationship.SelectedValue = Convert.ToInt16(dtRelatives.Rows(SrNo).Item("relationshipunkid"))
                If Convert.ToInt16(gvRelatives.DataKeys(SrNo).Item("relative_employeeunkid")) > 0 Then
                    drpStep7_RelativeEmployee.SelectedValue = Convert.ToInt16(gvRelatives.DataKeys(SrNo).Item("relative_employeeunkid"))
                    chkOthers.Checked = False
                Else
                    txtRelativeEmployee.Text = gvRelatives.Rows(SrNo).Cells(2).Text
                    chkOthers.Checked = True
                End If
                chkOthers_CheckedChanged(chkOthers, e)
                drpStep7_Relationship.SelectedValue = Convert.ToInt16(gvRelatives.DataKeys(SrNo).Item("relationshipunkid"))
                'Sohail (07 Dec 2018) -- End

                btnAddRelatives.Enabled = False
                btnUpdateRelatives.Enabled = True

            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtRelatives.Rows(CInt(e.CommandArgument)).Delete()

                'If dtRelatives.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtRelatives.NewRow

                '    r.Item(3) = "None" ' To Hide the row and display only Row Header
                '    r.Item(4) = "-1"
                '    dtRelatives.Rows.Add(r)
                'End If
                'dtRelatives.AcceptChanges() 'Sohail (24 Feb 2015) - Issue - error in save method after editing and then deleting row [deleted row information cannot be accessed through the row]

                'gvRelatives.DataSource = dtRelatives
                'gvRelatives.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_Relatives()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("RelativesSrNo") = SrNo

                popupDeleteRelatives.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvRelatives_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvRelatives.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(3).Text = "-1" Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvRelatives.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Hemant (07 Dec 2018) -- End

                    'Dim dsList As DataSet
                    'Dim intRelativeShipId As Integer
                    'Integer.TryParse(e.Row.Cells(3).Text, intRelativeShipId)

                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)

                    'Dim drRelations As DataRow() = dsList.Tables(0).Select("masterunkid = " & intRelativeShipId & " ", "name")

                    'If drRelations.Length > 0 Then
                    '    e.Row.Cells(3).Text = drRelations(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(3).Text = ""
                    'End If

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Checkbox Events "
    Protected Sub chkOthers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOthers.CheckedChanged
        Try         'Hemant (13 Aug 2020)
        If chkOthers.Checked = True Then
            txtRelativeEmployee.Visible = True
            drpStep7_RelativeEmployee.SelectedIndex = 0
            drpStep7_RelativeEmployee.Enabled = False
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            drpStep7_RelativeEmployee.ValidationGroup = ""
            rfStep7_RelativeEmployee.ValidationGroup = ""

            txtRelativeEmployee.ValidationGroup = "Relatives"
            rftxtRelativeEmployee.ValidationGroup = "Relatives"
            'Gajanan(07 Dec 2018) -- End
        Else

            txtRelativeEmployee.Visible = False
            drpStep7_RelativeEmployee.Enabled = True
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            drpStep7_RelativeEmployee.ValidationGroup = "Relatives"
            rfStep7_RelativeEmployee.ValidationGroup = "Relatives"

            txtRelativeEmployee.ValidationGroup = ""
            rftxtRelativeEmployee.ValidationGroup = ""
            'Gajanan(07 Dec 2018) -- End

        End If

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub
#End Region

#End Region

#Region "Dependant Business or Commercial  "

#Region " Private Method Functions "

    Private Sub FillComboDeptBusiness()
        Dim objExchange As New clsExchangeRate
        Dim objclsMasterData As New clsMasterData
        Dim dsCombos As DataSet
        Dim dsDeptBusiness As DataSet
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrMonthlyEarningS8 As DataSet
        'Sohail (07 Dec 2018) -- End

        Try

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
            'With drpStep8_RelationShip
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("Relations")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End
            'Hemant (03 Dec 2018) -- End

            dsDeptBusiness = objclsMasterData.getComboListAssetsupplierborrowerList("List")
            With drpStep8_SupplierClient
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsDeptBusiness.Tables("List")
                .DataBind()
            End With


            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrMonthlyEarningS8 = objExchange.getComboList("Currency", True)
            'With drpStep8_CurrencyMonthlyEarning
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrMonthlyEarningS8.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")
            'With drpStep8_Sector
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "Name"
            '    .DataSource = dsCombos.Tables("Asset Sector").Copy
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'Hemant (02 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillDeptBusinessGrid()
        Dim objAsset_businessdealT2depn_tran As New clsAsset_businessdealT2depn_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtDependantBusinessS8 As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'objAsset_businessdealT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtDependantBusinessS8 = objAsset_businessdealT2depn_tran._Datasource

            'If dtDependantBusinessS8.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtDependantBusinessS8.NewRow
            '    dtDependantBusinessS8.Rows.Add(r)
            'End If
            Dim dsDependantBusinessS8 As DataSet = objAsset_businessdealT2depn_tran.GetList("List", mintAssetDeclarationT2Unkid)

            dtDependantBusinessS8 = dsDependantBusinessS8.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtDependantBusinessS8.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtDependantBusinessS8.NewRow
                dtDependantBusinessS8.Rows.Add(r)
            End If
            'Hemant (07 Dec 2018) -- End

            gvDependantBusiness.DataSource = dtDependantBusinessS8
            gvDependantBusiness.DataBind()

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If blnRecordExist = False Then
                gvDependantBusiness.Rows(0).Visible = False
            End If
            'Hemant (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset_DeptBusiness()
        Try
            drpStep8_Sector.SelectedIndex = 0
            txtStep8_Companyname.Text = ""
            'Hemant (22 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            'txtCompanytinno_step8.Text = ""
            txtStep8_Companytinno.Text = "000-000-000"
            'Hemant (22 Nov 2018) -- End
            dtStep8_DateofRegistration.SetDate = Nothing
            txtStep8_BussinessType.Text = ""
            txtStep8_Position.Text = ""
            txtStep8_MonthlyEarning.Text = ""
            drpStep8_CurrencyMonthlyEarning.SelectedIndex = 0
            drpStep8_SupplierClient.SelectedIndex = 0
            txtStep8_Contactno.Text = ""
            txtStep8_Shareholder.Text = ""
            txtStep8_Address.Text = ""

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            drpStep8_RelationShip.SelectedValue = 0
            If (drpStep8_DependantName.Items.Count > 0) Then
                drpStep8_DependantName.SelectedIndex = 0
            End If
            'Hemant (03 Dec 2018) -- End

            btnAddDependantBusiness_step8.Enabled = True
            btnUpdateDependantBusiness_step8.Enabled = False

        Catch ex As Exception
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (03 Dec 2018) -- End
        End Try
    End Sub

    Private Function IsValid_DepetBusiness() As Boolean
        Try
            If dtStep8_DateofRegistration.IsNull OrElse eZeeDate.convertDate(dtStep8_DateofRegistration.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtStep8_DateofRegistration.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblerrordtDateofRegistration_step8.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If

            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            Dim strCompanyTinNumber As String = String.Empty
            strCompanyTinNumber = txtStep8_Companytinno.Text.Replace("_", "")
            If strCompanyTinNumber.Length < 11 Then
                'Gajanan (28 Nov 2018) -- Start
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 54, "Company Tin Number Must be 9 Digit"), Me)
                'Gajanan (28 Nov 2018) -- End
                popupAddEdit.Show()
                txtStep8_Companytinno.Focus()
                Return False
            End If
            'Hemant (19 Nov 2018) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "
    Protected Sub btnAddDependantBusiness_step8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDependantBusiness_step8.Click
        Try
            If IsValid_DepetBusiness() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtDependantBusinessS8.NewRow

            'drRow.Item("assetbusinessdealt2depntranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("relationshipunkid") = drpStep8_RelationShip.SelectedValue
            'drRow.Item("dependantunkid") = drpStep8_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("assetsectorunkid") = drpStep8_Sector.SelectedValue
            'drRow.Item("company_org_name") = txtStep8_Companyname.Text
            'drRow.Item("company_tin_no") = txtStep8_Companytinno.Text
            'drRow.Item("registration_date") = dtStep8_DateofRegistration.GetDate
            'drRow.Item("address_location") = txtStep8_Address.Text
            'drRow.Item("business_type") = txtStep8_BussinessType.Text
            'drRow.Item("position_held") = txtStep8_Position.Text
            'drRow.Item("is_supplier_client") = drpStep8_SupplierClient.SelectedValue
            'drRow.Item("countryunkid") = CInt(drpStep8_CurrencyMonthlyEarning.SelectedItem.Value)

            'drRow.Item("business_contact_no") = txtStep8_Contactno.Text
            'drRow.Item("shareholders_names") = txtStep8_Shareholder.Text
            'drRow.Item("monthly_annual_earnings") = txtStep8_MonthlyEarning.Text.Trim


            'drRow.Item("isfinalsaved") = 0

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantBusiness, drRow, CInt(drpStep8_CurrencyMonthlyEarning.SelectedValue))
            'dtDependantBusinessS8.Rows.Add(drRow)

            'gvDependantBusiness.DataSource = dtDependantBusinessS8
            'gvDependantBusiness.DataBind()

            Dim objAsset_businessdealT2depn_tran As New clsAsset_businessdealT2depn_tran


            objAsset_businessdealT2depn_tran._Assetbusinessdealt2depntranunkid = -1
            objAsset_businessdealT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_businessdealT2depn_tran._Relationshipunkid = drpStep8_RelationShip.SelectedValue
            objAsset_businessdealT2depn_tran._Dependantunkid = drpStep8_DependantName.SelectedValue
            objAsset_businessdealT2depn_tran._Assetsectorunkid = drpStep8_Sector.SelectedValue
            objAsset_businessdealT2depn_tran._Company_Org_Name = txtStep8_Companyname.Text
            objAsset_businessdealT2depn_tran._Company_Tin_No = txtStep8_Companytinno.Text
            objAsset_businessdealT2depn_tran._Registration_Date = dtStep8_DateofRegistration.GetDate
            objAsset_businessdealT2depn_tran._Address_Location = txtStep8_Address.Text
            objAsset_businessdealT2depn_tran._Business_Type = txtStep8_BussinessType.Text
            objAsset_businessdealT2depn_tran._Position_Held = txtStep8_Position.Text
            objAsset_businessdealT2depn_tran._Is_Supplier_Client = drpStep8_SupplierClient.SelectedValue
            objAsset_businessdealT2depn_tran._Countryunkid = CInt(drpStep8_CurrencyMonthlyEarning.SelectedItem.Value)

            objAsset_businessdealT2depn_tran._Business_Contact_No = txtStep8_Contactno.Text
            objAsset_businessdealT2depn_tran._Shareholders_Names = txtStep8_Shareholder.Text
            objAsset_businessdealT2depn_tran._Monthly_Annual_Earnings = txtStep8_MonthlyEarning.Text.Trim
            objAsset_businessdealT2depn_tran._Isfinalsaved = 0

            objAsset_businessdealT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_businessdealT2depn_tran._Transactiondate = Nothing Then
                objAsset_businessdealT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep8_CurrencyMonthlyEarning.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_businessdealT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_businessdealT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_businessdealT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_businessdealT2depn_tran._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAsset_businessdealT2depn_tran._Basemonthly_Annual_Earnings = objAsset_businessdealT2depn_tran._Monthly_Annual_Earnings * decBaseExRate / decPaidExRate
            Else
                objAsset_businessdealT2depn_tran._Basemonthly_Annual_Earnings = objAsset_businessdealT2depn_tran._Monthly_Annual_Earnings
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_businessdealT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_businessdealT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_businessdealT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_businessdealT2depn_tran._HostName = Session("HOST_NAME").ToString()
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_businessdealT2depn_tran._FormName = mstrModuleName
            objAsset_businessdealT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End
            objAsset_businessdealT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_businessdealT2depn_tran.Insert(Nothing, objAssetDeclare) = False Then
                If objAsset_businessdealT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_businessdealT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_businessdealT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillDeptBusinessGrid()

            'Sohail (07 Dec 2018) -- End

            'Hemant (15 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            'Call Reset_DeptBusiness()
            txtStep8_Shareholder.Text = ""
            'Hemant (15 Nov 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateDependantBusiness_step8_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDependantBusiness_step8.Click
        Try
            If IsValid_DepetBusiness() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("DependantBusinessSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtDependantBusinessS8.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtDependantBusinessS8.Rows(SrNo)

            ''drRow.Item("assetbusinessdealt2depntranunkid") = -1
            ''drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("relationshipunkid") = drpStep8_RelationShip.SelectedValue
            'drRow.Item("dependantunkid") = drpStep8_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("assetsectorunkid") = drpStep8_Sector.SelectedValue
            'drRow.Item("company_org_name") = txtStep8_Companyname.Text
            'drRow.Item("company_tin_no") = txtStep8_Companytinno.Text
            'drRow.Item("registration_date") = dtStep8_DateofRegistration.GetDate
            'drRow.Item("address_location") = txtStep8_Address.Text
            'drRow.Item("business_type") = txtStep8_BussinessType.Text
            'drRow.Item("position_held") = txtStep8_Position.Text
            'drRow.Item("is_supplier_client") = drpStep8_SupplierClient.SelectedValue
            'drRow.Item("countryunkid") = CInt(drpStep8_CurrencyMonthlyEarning.SelectedItem.Value)

            'drRow.Item("business_contact_no") = txtStep8_Contactno.Text
            'drRow.Item("shareholders_names") = txtStep8_Shareholder.Text
            'drRow.Item("monthly_annual_earnings") = txtStep8_MonthlyEarning.Text.Trim
            'drRow.Item("isfinalsaved") = 0
            'drRow.Item("AUD") = "U"

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantBusiness, drRow, CInt(drpStep8_CurrencyMonthlyEarning.SelectedValue))

            'dtDependantBusinessS8.AcceptChanges()
            'gvDependantBusiness.DataSource = dtDependantBusinessS8
            'gvDependantBusiness.DataBind()

            Dim objAsset_businessdealT2depn_tran As New clsAsset_businessdealT2depn_tran
            objAsset_businessdealT2depn_tran.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantBusiness.DataKeys(SrNo).Item("assetbusinessdealt2depntranunkid")), Nothing)
            objAsset_businessdealT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_businessdealT2depn_tran._Relationshipunkid = drpStep8_RelationShip.SelectedValue
            objAsset_businessdealT2depn_tran._Dependantunkid = drpStep8_DependantName.SelectedValue
            objAsset_businessdealT2depn_tran._Assetsectorunkid = drpStep8_Sector.SelectedValue
            objAsset_businessdealT2depn_tran._Company_Org_Name = txtStep8_Companyname.Text
            objAsset_businessdealT2depn_tran._Company_Tin_No = txtStep8_Companytinno.Text
            objAsset_businessdealT2depn_tran._Registration_Date = dtStep8_DateofRegistration.GetDate
            objAsset_businessdealT2depn_tran._Address_Location = txtStep8_Address.Text
            objAsset_businessdealT2depn_tran._Business_Type = txtStep8_BussinessType.Text
            objAsset_businessdealT2depn_tran._Position_Held = txtStep8_Position.Text
            objAsset_businessdealT2depn_tran._Is_Supplier_Client = drpStep8_SupplierClient.SelectedValue
            objAsset_businessdealT2depn_tran._Countryunkid = CInt(drpStep8_CurrencyMonthlyEarning.SelectedItem.Value)
            objAsset_businessdealT2depn_tran._Business_Contact_No = txtStep8_Contactno.Text
            objAsset_businessdealT2depn_tran._Shareholders_Names = txtStep8_Shareholder.Text
            objAsset_businessdealT2depn_tran._Monthly_Annual_Earnings = txtStep8_MonthlyEarning.Text.Trim
            objAsset_businessdealT2depn_tran._Isfinalsaved = 0

            objAsset_businessdealT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_businessdealT2depn_tran._Transactiondate = Nothing Then
                objAsset_businessdealT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep8_CurrencyMonthlyEarning.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_businessdealT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_businessdealT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_businessdealT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_businessdealT2depn_tran._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAsset_businessdealT2depn_tran._Basemonthly_Annual_Earnings = objAsset_businessdealT2depn_tran._Monthly_Annual_Earnings * decBaseExRate / decPaidExRate
            Else
                objAsset_businessdealT2depn_tran._Basemonthly_Annual_Earnings = objAsset_businessdealT2depn_tran._Monthly_Annual_Earnings
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_businessdealT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_businessdealT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_businessdealT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_businessdealT2depn_tran._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_businessdealT2depn_tran._FormName = mstrModuleName
            objAsset_businessdealT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAsset_businessdealT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_businessdealT2depn_tran.Update(False, Nothing, objAssetDeclare) = False Then
                If objAsset_businessdealT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_businessdealT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_businessdealT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillDeptBusinessGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DeptBusiness()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub popupDeleteDependantBusinessDeal_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteDependantBusinessDeal.buttonDelReasonYes_Click
        Dim objDependantBussDeal As New clsAsset_businessdealT2depn_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("DependantBusinessSrNo"))
            objDependantBussDeal.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantBusiness.DataKeys(SrNo).Item("assetbusinessdealt2depntranunkid")), Nothing)
            objDependantBussDeal._Isvoid = True
            objDependantBussDeal._Voiduserunkid = CInt(Session("UserId"))
            objDependantBussDeal._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objDependantBussDeal._Voidreason = popupDeleteDependantBusinessDeal.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objDependantBussDeal._AuditUserid = CInt(Session("UserId"))
            Else
                objDependantBussDeal._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objDependantBussDeal._ClientIp = Session("IP_ADD").ToString()
            objDependantBussDeal._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objDependantBussDeal._FormName = mstrModuleName
            objDependantBussDeal._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objDependantBussDeal._IsFromWeb = True

            If objDependantBussDeal.Void(CInt(gvDependantBusiness.DataKeys(SrNo).Item("assetbusinessdealt2depntranunkid")), CInt(Session("UserId")), objDependantBussDeal._Voiddatetime, objDependantBussDeal._Voidreason, Nothing) = True Then
                Call FillDeptBusinessGrid()
            ElseIf objDependantBussDeal._Message <> "" Then
                DisplayMessage.DisplayMessage(objDependantBussDeal._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Protected Sub btnResetBank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetBank.Click
    '    Try
    '        Call Reset_Bank()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        popupAddEdit.Show()
    '    End Try
    'End Sub
#End Region

#Region " GridView Events "

    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub gvDependantBusiness_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDependantBusiness.PageIndexChanging
        Try
            gvDependantBusiness.PageIndex = e.NewPageIndex
            Call FillDeptBusinessGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 Dec 2018) -- End

    Protected Sub gvDependantBusiness_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDependantBusiness.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantBusinessSrNo") = SrNo
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'drpStep8_RelationShip.SelectedValue = dtDependantBusinessS8.Rows(SrNo).Item("relationshipunkid")
                'drpStep8_RelationShip_SelectedIndexChanged(Nothing, Nothing)
                'drpStep8_DependantName.SelectedValue = dtDependantBusinessS8.Rows(SrNo).Item("dependantunkid")
                ''Hemant (03 Dec 2018) -- End
                'drpStep8_Sector.SelectedValue = dtDependantBusinessS8.Rows(SrNo).Item("assetsectorunkid")
                'txtStep8_Companyname.Text = dtDependantBusinessS8.Rows(SrNo).Item("company_org_name")
                'txtStep8_Companytinno.Text = dtDependantBusinessS8.Rows(SrNo).Item("company_tin_no")

                'dtStep8_DateofRegistration.SetDate = Convert.ToDateTime(dtDependantBusinessS8.Rows(SrNo).Item("registration_date").ToString)
                'txtStep8_BussinessType.Text = dtDependantBusinessS8.Rows(SrNo).Item("business_type")
                'txtStep8_Position.Text = dtDependantBusinessS8.Rows(SrNo).Item("position_held")
                ''Hemant (23 Nov 2018) -- Start
                ''Enhancement : Changes As per Rutta Request for UAT 
                ''txtMonthlyEarning_step8.Text = dtDependantBusinessS8.Rows(SrNo).Item("monthly_annual_earnings")
                'txtStep8_MonthlyEarning.Text = Format(dtDependantBusinessS8.Rows(SrNo).Item("monthly_annual_earnings"), Session("fmtCurrency"))
                ''Hemant (23 Nov 2018) -- End
                'drpStep8_CurrencyMonthlyEarning.SelectedValue = dtDependantBusinessS8.Rows(SrNo).Item("countryunkid")
                'drpStep8_SupplierClient.SelectedValue = dtDependantBusinessS8.Rows(SrNo).Item("is_supplier_client")
                'txtStep8_Contactno.Text = dtDependantBusinessS8.Rows(SrNo).Item("business_contact_no")
                'txtStep8_Shareholder.Text = dtDependantBusinessS8.Rows(SrNo).Item("shareholders_names")
                'txtStep8_Address.Text = dtDependantBusinessS8.Rows(SrNo).Item("address_location")

                drpStep8_RelationShip.SelectedValue = CInt(gvDependantBusiness.DataKeys(SrNo).Item("relationshipunkid").ToString)
                drpStep8_RelationShip_SelectedIndexChanged(drpStep8_RelationShip, New System.EventArgs)
                drpStep8_DependantName.SelectedValue = CInt(gvDependantBusiness.DataKeys(SrNo).Item("dependantunkid").ToString)
                drpStep8_Sector.SelectedValue = CInt(gvDependantBusiness.DataKeys(SrNo).Item("assetsectorunkid").ToString)
                txtStep8_Companyname.Text = gvDependantBusiness.Rows(SrNo).Cells(5).Text.Replace("&amp;", "&")
                txtStep8_Companytinno.Text = gvDependantBusiness.Rows(SrNo).Cells(6).Text
                dtStep8_DateofRegistration.SetDate = Convert.ToDateTime(gvDependantBusiness.Rows(SrNo).Cells(7).Text)
                txtStep8_BussinessType.Text = gvDependantBusiness.Rows(SrNo).Cells(9).Text
                txtStep8_Position.Text = gvDependantBusiness.Rows(SrNo).Cells(10).Text
                txtStep8_MonthlyEarning.Text = Format(CDec(gvDependantBusiness.Rows(SrNo).Cells(13).Text), Session("fmtCurrency"))
                drpStep8_CurrencyMonthlyEarning.SelectedValue = CInt(gvDependantBusiness.DataKeys(SrNo).Item("countryunkid").ToString)
                drpStep8_SupplierClient.SelectedValue = CInt(gvDependantBusiness.DataKeys(SrNo).Item("is_supplier_client").ToString)
                txtStep8_Contactno.Text = gvDependantBusiness.Rows(SrNo).Cells(14).Text
                txtStep8_Shareholder.Text = gvDependantBusiness.Rows(SrNo).Cells(15).Text
                txtStep8_Address.Text = gvDependantBusiness.Rows(SrNo).Cells(8).Text
                'Sohail (07 Dec 2018) -- End

                btnAddDependantBusiness_step8.Enabled = False
                btnUpdateDependantBusiness_step8.Enabled = True


            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtDependantBusinessS8.Rows(CInt(e.CommandArgument)).Delete()

                'If dtDependantBusinessS8.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtDependantBusinessS8.NewRow
                '    dtDependantBusinessS8.Rows.Add(r)
                'End If
                'dtDependantBusinessS8.AcceptChanges()

                'gvDependantBusiness.DataSource = dtDependantBusinessS8
                'gvDependantBusiness.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_DeptBusiness()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantBusinessSrNo") = SrNo

                popupDeleteDependantBusinessDeal.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvDependantBusiness_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDependantBusiness.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If CInt(gvDependantBusiness.DataKeys(e.Row.RowIndex)("assetbusinessdealt2depntranunkid").ToString()) <= -2 AndAlso _
                '               CInt(gvDependantBusiness.DataKeys(e.Row.RowIndex)("assetdeclarationt2unkid").ToString()) <= -2 Then
                '    e.Row.Visible = False
                'Else
                'Hemant (07 Dec 2018) -- End

                If IsDBNull(gvDependantBusiness.DataKeys(e.Row.RowIndex).Item(0)) = False Then

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_RegDate", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_RegDate", False, True)).Text).ToShortDateString

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_monthlyearning", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_monthlyearning", False, True)).Text), Session("fmtCurrency"))

                    'Hemant (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

                    'Dim intCountryId As Integer

                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_CurrencyShare", False, True)).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_CurrencyShare", False, True)).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_CurrencyShare", False, True)).Text = ""
                    'End If

                    'Dim objclsMasterData As New clsMasterData
                    'Dim intSupplierBorrowerId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_is_supplier_client", False, True)).Text, intSupplierBorrowerId)
                    'dsList = objclsMasterData.getComboListAssetsupplierborrowerList("List")

                    'Dim drClient As DataRow() = dsList.Tables(0).Select("id = " & intSupplierBorrowerId & " ", "name")

                    'If drClient.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_is_supplier_client", False, True)).Text = drClient(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_is_supplier_client", False, True)).Text = ""
                    'End If


                    ''Gajanan (29 Nov 2018) -- Start
                    'Dim intSectorId As Integer
                    ''Hemant (03 Dec 2018) -- Start
                    ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    ''Integer.TryParse(e.Row.Cells(2).Text, intSectorId)
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_Sector", False, True)).Text, intSectorId)
                    ''Hemant (03 Dec 2018) -- End
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECTOR, True, "Asset Sector")

                    'Dim drSector As DataRow() = dsList.Tables(0).Select("masterunkid = " & intSectorId & " ", "name")

                    'If drSector.Length > 0 Then
                    '    'Hemant (03 Dec 2018) -- Start
                    '    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    '    'e.Row.Cells(2).Text = drSector(0).Item("name").ToString
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_Sector", False, True)).Text = drSector(0).Item("name").ToString
                    '    'Hemant (03 Dec 2018) -- End
                    'Else
                    '    'Hemant (03 Dec 2018) -- Start
                    '    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    '    'e.Row.Cells(2).Text = ""
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_Sector", False, True)).Text = ""
                    '    'Hemant (03 Dec 2018) -- End
                    'End If
                    ''Gajanan (29 Nov 2018) -- End

                    ''Hemant (03 Dec 2018) -- Start
                    ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    'Dim intRelationId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_Relationship", False, True)).Text, intRelationId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)

                    'Dim drRelation As DataRow() = dsList.Tables(0).Select("masterunkid = " & intRelationId & " ", "name")

                    'If drRelation.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_Relationship", False, True)).Text = drRelation(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_Relationship", False, True)).Text = ""
                    'End If


                    'Dim intDependantId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_DependantName", False, True)).Text, intDependantId)
                    'If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    '    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & intDependantId & " ", "DpndtBefName")

                    '    If drDependantList.Length > 0 Then
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_DependantName", False, True)).Text = drDependantList(0).Item("DpndtBefName").ToString
                    '    Else
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBusiness, "colhStep8_DependantName", False, True)).Text = ""
                    '    End If
                    'End If
                    'Hemant (07 Dec 2018) -- End
                    'Hemant (03 Dec 2018) -- End

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
#Region "TextBox Events"
    Protected Sub txtMonthlyEarning_step8_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep8_MonthlyEarning.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep8_MonthlyEarning.Text = Format(CDec(txtStep8_MonthlyEarning.Text), Session("fmtCurrency"))
            If txtStep8_MonthlyEarning.Text.Trim <> "" Then txtStep8_MonthlyEarning.Text = Format(CDec(txtStep8_MonthlyEarning.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (23 Nov 2018) -- End

    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
#Region "Dropdown Events"
    Protected Sub drpStep8_RelationShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpStep8_RelationShip.SelectedIndexChanged
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim dsList As DataSet
        Dim dtDependantList As DataTable
        'Sohail (07 Dec 2018) -- End
        Try
            Dim dtParticularRelationList As DataTable
            If drpStep8_RelationShip.SelectedValue > 0 Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "List", , , "hremployee_master.employeeunkid = " + cboStep1_Employee.SelectedValue + " ", _
                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                dtDependantList = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'Sohail (07 Dec 2018) -- End
                dtParticularRelationList = New DataView(dtDependantList, "relationid = " & drpStep8_RelationShip.SelectedValue & " ", "", DataViewRowState.CurrentRows).ToTable
                Dim r As DataRow = dtParticularRelationList.NewRow
                r.Item("DpndtBefName") = "Select" '
                r.Item("DpndtTranId") = "0"
                dtParticularRelationList.Rows.InsertAt(r, 0)
                With drpStep8_DependantName
                    .DataValueField = "DpndtTranId"
                    .DataTextField = "DpndtBefName"
                    .DataSource = dtParticularRelationList
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (03 Dec 2018) -- End
#End Region

#Region "Dependant Share  "

#Region " Private Method Functions "

    Private Sub FillComboDeptShare()
        Dim objExchange As New clsExchangeRate
        Dim objclsMasterData As New clsMasterData
        Dim dsCombos As DataSet
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrMarketValueS9 As DataSet
        'Sohail (07 Dec 2018) -- End

        Try
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
            'With drpStep9_DependantRelationship
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("Relations")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECURITIES, True, "Asset Securities")
            With DrpStep9_DependantSecurityType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Asset Securities")
                .DataBind()
            End With

            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrMarketValueS9 = objExchange.getComboList("Currency", True)
            'With drpStep9_Dependantmarketvalue
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrMarketValueS9.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillDeptShareGrid()
        Dim objAsset_securitiesT2depn_tran As New clsAsset_securitiesT2depn_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtDependantShareS9 As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'objAsset_securitiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtDependantShareS9 = objAsset_securitiesT2depn_tran._Datasource

            'If dtDependantShareS9.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtDependantShareS9.NewRow
            '    dtDependantShareS9.Rows.Add(r)
            'End If
            Dim dsDependantShareS9 As DataSet = objAsset_securitiesT2depn_tran.GetList("List", mintAssetDeclarationT2Unkid)
            dtDependantShareS9 = dsDependantShareS9.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtDependantShareS9.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtDependantShareS9.NewRow
                dtDependantShareS9.Rows.Add(r)
            End If
            'Sohail (07 Dec 2018) -- End

            gvDependantShare.DataSource = dtDependantShareS9
            gvDependantShare.DataBind()

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'SetBusinessTotal()
            If blnRecordExist = False Then
                gvDependantShare.Rows(0).Visible = False
            End If
            'Sohail (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset_DependantShare()
        Try
            drpStep9_DependantRelationship.SelectedIndex = 0
            DrpStep9_DependantSecurityType.SelectedIndex = 0
            txtStep9_DependantCertificateno.Text = ""
            txtStep9_Dependantno_of_shares.Text = ""
            txtDependantbusinessname_step9.Text = ""
            dtStep9_Dependantacquisition_date.SetDate = Nothing
            txtStep9_Dependantmarketvalue.Text = 0
            drpStep9_Dependantmarketvalue.SelectedIndex = 0




            'Hemant (13 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            txtStep9_Dependantno_of_shares.Text = "0"
            'Hemant (13 Nov 2018) -- End
            btnAddDependantShare_step9.Enabled = True
            btnUpdateDependantShare_step9.Enabled = False
            'Hemant (13 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'dtDependantShareS9.Clear()
            'gvDependantShare.DataSource = dtDependantShareS9
            'gvDependantShare.DataBind()
            'Hemant (13 Nov 2018) -- End

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            drpStep9_DependantRelationship.SelectedValue = 0
            If (drpStep9_DependantName.Items.Count > 0) Then
                drpStep9_DependantName.SelectedIndex = 0
            End If
            'Hemant (03 Dec 2018) -- End
        Catch ex As Exception
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (03 Dec 2018) -- End
        End Try
    End Sub

    Private Function Isvalid_DependantShare() As Boolean
        Try
            If dtStep9_Dependantacquisition_date.IsNull OrElse eZeeDate.convertDate(dtStep9_Dependantacquisition_date.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtStep9_Dependantacquisition_date.GetDate) <= "19000101"]
                'Gajanan (28 Nov 2018) -- Start
                'lblerrordtDependantacquisition_date_step9.Text = "Please Select Proper Date"
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                'Gajanan (28 Nov 2018) -- End
                Return False
            End If



            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Function

#End Region

#Region " Button's Events "
    Protected Sub btnAddDependantShare_step9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDependantShare_step9.Click
        Try
            If Isvalid_DependantShare() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtDependantShareS9.NewRow

            'drRow.Item("assetsecuritiest2depntranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("relationshipunkid") = drpStep9_DependantRelationship.SelectedValue
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("dependantunkid") = drpStep9_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("securitiestypeunkid") = DrpStep9_DependantSecurityType.SelectedValue
            'drRow.Item("certificate_no") = txtStep9_DependantCertificateno.Text
            'drRow.Item("no_of_shares") = CInt(txtStep9_Dependantno_of_shares.Text)
            'drRow.Item("company_business_name") = txtDependantbusinessname_step9.Text
            'drRow.Item("acquisition_date") = dtStep9_Dependantacquisition_date.GetDate
            'drRow.Item("countryunkid") = CInt(drpStep9_Dependantmarketvalue.SelectedItem.Value)
            'drRow.Item("market_value") = txtStep9_Dependantmarketvalue.Text.Trim
            'drRow.Item("isfinalsaved") = 0

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantShare, drRow, CInt(drpStep9_Dependantmarketvalue.SelectedValue))
            'dtDependantShareS9.Rows.Add(drRow)

            'gvDependantShare.DataSource = dtDependantShareS9
            'gvDependantShare.DataBind()
            Dim objAsset_securitiesT2depn_tran As New clsAsset_securitiesT2depn_tran
            objAsset_securitiesT2depn_tran._Assetsecuritiest2depntranunkid = -1
            objAsset_securitiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_securitiesT2depn_tran._Relationshipunkid = drpStep9_DependantRelationship.SelectedValue
            objAsset_securitiesT2depn_tran._Dependantunkid = drpStep9_DependantName.SelectedValue
            objAsset_securitiesT2depn_tran._Securitiestypeunkid = DrpStep9_DependantSecurityType.SelectedValue
            objAsset_securitiesT2depn_tran._Certificate_No = txtStep9_DependantCertificateno.Text
            objAsset_securitiesT2depn_tran._No_Of_Shares = CInt(txtStep9_Dependantno_of_shares.Text)
            objAsset_securitiesT2depn_tran._Company_Business_Name = txtDependantbusinessname_step9.Text
            objAsset_securitiesT2depn_tran._Acquisition_Date = dtStep9_Dependantacquisition_date.GetDate
            objAsset_securitiesT2depn_tran._Countryunkid = CInt(drpStep9_Dependantmarketvalue.SelectedItem.Value)
            objAsset_securitiesT2depn_tran._Market_Value = txtStep9_Dependantmarketvalue.Text.Trim
            objAsset_securitiesT2depn_tran._Isfinalsaved = 0

            objAsset_securitiesT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_securitiesT2depn_tran._Transactiondate = Nothing Then
                objAsset_securitiesT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep9_Dependantmarketvalue.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_securitiesT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_securitiesT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_securitiesT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_securitiesT2depn_tran._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAsset_securitiesT2depn_tran._Basemarket_Value = objAsset_securitiesT2depn_tran._Market_Value * decBaseExRate / decPaidExRate
            Else
                objAsset_securitiesT2depn_tran._Basemarket_Value = objAsset_securitiesT2depn_tran._Market_Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_securitiesT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_securitiesT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_securitiesT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_securitiesT2depn_tran._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_securitiesT2depn_tran._FormName = mstrModuleName
            objAsset_securitiesT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAsset_securitiesT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_securitiesT2depn_tran.Insert(Nothing, objAssetDeclare) = False Then
                If objAsset_securitiesT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_securitiesT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_securitiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillDeptShareGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DependantShare()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateDependantShare_step9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDependantShare_step9.Click
        Try

            'Hemant (26 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT
            If Isvalid_DependantShare() = False Then
                Exit Try
            End If
            'Hemant (26 Nov 2018) -- End

            'If IsValid_Bank() = False Then
            '    Exit Try
            'End If

            Dim SrNo As Integer = Me.ViewState("DependantShareSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtDependantShareS9.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtDependantShareS9.Rows(SrNo)


            ''drRow.Item("assetsecuritiest2depntranunkid") = -1
            ''drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            'drRow.Item("relationshipunkid") = drpStep9_DependantRelationship.SelectedValue
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("dependantunkid") = drpStep9_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("securitiestypeunkid") = DrpStep9_DependantSecurityType.SelectedValue
            'drRow.Item("certificate_no") = txtStep9_DependantCertificateno.Text
            ''Hemant (26 Nov 2018) -- Start
            ''Enhancement : Changes As per Rutta Request for UAT 
            ''drRow.Item("no_of_shares") = txtDependantno_of_shares_step9.Text
            'drRow.Item("no_of_shares") = CInt(txtStep9_Dependantno_of_shares.Text)
            ''Hemant (26 Nov 2018) -- End
            'drRow.Item("company_business_name") = txtDependantbusinessname_step9.Text
            'drRow.Item("acquisition_date") = dtStep9_Dependantacquisition_date.GetDate
            'drRow.Item("countryunkid") = CInt(drpStep9_Dependantmarketvalue.SelectedItem.Value)
            'drRow.Item("market_value") = txtStep9_Dependantmarketvalue.Text.Trim
            'drRow.Item("isfinalsaved") = 0
            'drRow.Item("AUD") = "U"

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantShare, drRow, CInt(drpStep9_Dependantmarketvalue.SelectedValue))

            'dtDependantShareS9.AcceptChanges()
            'gvDependantShare.DataSource = dtDependantShareS9
            'gvDependantShare.DataBind()
            Dim objAsset_securitiesT2depn_tran As New clsAsset_securitiesT2depn_tran
            objAsset_securitiesT2depn_tran.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantShare.DataKeys(SrNo).Item("assetsecuritiest2depntranunkid")), Nothing)
            objAsset_securitiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_securitiesT2depn_tran._Relationshipunkid = drpStep9_DependantRelationship.SelectedValue
            objAsset_securitiesT2depn_tran._Dependantunkid = drpStep9_DependantName.SelectedValue
            objAsset_securitiesT2depn_tran._Securitiestypeunkid = DrpStep9_DependantSecurityType.SelectedValue
            objAsset_securitiesT2depn_tran._Certificate_No = txtStep9_DependantCertificateno.Text
            objAsset_securitiesT2depn_tran._No_Of_Shares = CInt(txtStep9_Dependantno_of_shares.Text)
            objAsset_securitiesT2depn_tran._Company_Business_Name = txtDependantbusinessname_step9.Text
            objAsset_securitiesT2depn_tran._Acquisition_Date = dtStep9_Dependantacquisition_date.GetDate
            objAsset_securitiesT2depn_tran._Countryunkid = CInt(drpStep9_Dependantmarketvalue.SelectedItem.Value)
            objAsset_securitiesT2depn_tran._Market_Value = txtStep9_Dependantmarketvalue.Text.Trim
            objAsset_securitiesT2depn_tran._Isfinalsaved = 0

            objAsset_securitiesT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_securitiesT2depn_tran._Transactiondate = Nothing Then
                objAsset_securitiesT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep9_Dependantmarketvalue.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_securitiesT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_securitiesT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_securitiesT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_securitiesT2depn_tran._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAsset_securitiesT2depn_tran._Basemarket_Value = objAsset_securitiesT2depn_tran._Market_Value * decBaseExRate / decPaidExRate
            Else
                objAsset_securitiesT2depn_tran._Basemarket_Value = objAsset_securitiesT2depn_tran._Market_Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_securitiesT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_securitiesT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_securitiesT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_securitiesT2depn_tran._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_securitiesT2depn_tran._FormName = mstrModuleName
            objAsset_securitiesT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAsset_securitiesT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_securitiesT2depn_tran.Update(False, Nothing, objAssetDeclare) = False Then
                If objAsset_securitiesT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_securitiesT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_securitiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillDeptShareGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DependantShare()
        Catch ex As Exception
            'Hemant (23 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (23 Nov 2018) -- End
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetDependantShare_step9_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetDependantShare_step9.Click
        Try
            Call Reset_DependantShare()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteDependantShare_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteDependantShare.buttonDelReasonYes_Click
        Dim objAsset_securitiesT2depn_tran As New clsAsset_securitiesT2depn_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("DependantShareSrNo"))
            objAsset_securitiesT2depn_tran.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantShare.DataKeys(SrNo).Item("assetsecuritiest2depntranunkid")), Nothing)
            objAsset_securitiesT2depn_tran._Isvoid = True
            objAsset_securitiesT2depn_tran._Voiduserunkid = CInt(Session("UserId"))
            objAsset_securitiesT2depn_tran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAsset_securitiesT2depn_tran._Voidreason = popupDeleteDependantShare.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_securitiesT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_securitiesT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_securitiesT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_securitiesT2depn_tran._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_securitiesT2depn_tran._FormName = mstrModuleName
            objAsset_securitiesT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAsset_securitiesT2depn_tran._IsFromWeb = True

            If objAsset_securitiesT2depn_tran.Void(CInt(gvDependantShare.DataKeys(SrNo).Item("assetsecuritiest2depntranunkid")), CInt(Session("UserId")), objAsset_securitiesT2depn_tran._Voiddatetime, objAsset_securitiesT2depn_tran._Voidreason, Nothing) = True Then
                FillDeptShareGrid()
            ElseIf objAsset_securitiesT2depn_tran._Message <> "" Then
                DisplayMessage.DisplayMessage(objAsset_securitiesT2depn_tran._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

#End Region

#Region " GridView Events "

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub gvDependantShare_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDependantShare.PageIndexChanging
        Try
            gvDependantShare.PageIndex = e.NewPageIndex
            Call FillDeptShareGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

    Protected Sub gvDependantShare_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDependantShare.RowCommand
        Try
            If e.CommandName = "Change" Then

                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantShareSrNo") = SrNo
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'drpStep9_DependantRelationship.SelectedValue = dtDependantShareS9.Rows(SrNo).Item("relationshipunkid")
                ''Hemant (03 Dec 2018) -- Start
                ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'drpStep9_DependantRelationship_SelectedIndexChanged(Nothing, Nothing)
                'drpStep9_DependantName.SelectedValue = dtDependantShareS9.Rows(SrNo).Item("dependantunkid")
                ''Hemant (03 Dec 2018) -- End
                'DrpStep9_DependantSecurityType.SelectedValue = dtDependantShareS9.Rows(SrNo).Item("securitiestypeunkid")
                'txtStep9_DependantCertificateno.Text = dtDependantShareS9.Rows(SrNo).Item("certificate_no")
                ''Hemant (23 Nov 2018) -- Start
                ''Enhancement : Changes As per Rutta Request for UAT 
                ''txtDependantno_of_shares_step9.Text = dtDependantShareS9.Rows(SrNo).Item("no_of_shares")
                'txtStep9_Dependantno_of_shares.Text = Format(dtDependantShareS9.Rows(SrNo).Item("no_of_shares"), Session("fmtCurrency"))
                ''Hemant (23 Nov 2018) -- End
                'txtDependantbusinessname_step9.Text = dtDependantShareS9.Rows(SrNo).Item("company_business_name")
                'dtStep9_Dependantacquisition_date.SetDate = Convert.ToDateTime(dtDependantShareS9.Rows(SrNo).Item("acquisition_date").ToString)
                ''Hemant (23 Nov 2018) -- Start
                ''Enhancement : Changes As per Rutta Request for UAT 
                ''txtDependantmarketvalue_step9.Text = dtDependantShareS9.Rows(SrNo).Item("market_value")
                'txtStep9_Dependantmarketvalue.Text = Format(dtDependantShareS9.Rows(SrNo).Item("market_value"), Session("fmtCurrency"))
                ''Hemant (23 Nov 2018) -- End
                'drpStep9_Dependantmarketvalue.SelectedValue = dtDependantShareS9.Rows(SrNo).Item("countryunkid")
                drpStep9_DependantRelationship.SelectedValue = CInt(gvDependantShare.DataKeys(SrNo).Item("relationshipunkid"))
                drpStep9_DependantRelationship_SelectedIndexChanged(drpStep9_DependantRelationship, New System.EventArgs)
                drpStep9_DependantName.SelectedValue = CInt(gvDependantShare.DataKeys(SrNo).Item("dependantunkid"))
                DrpStep9_DependantSecurityType.SelectedValue = gvDependantShare.DataKeys(SrNo).Item("securitiestypeunkid")
                txtStep9_DependantCertificateno.Text = gvDependantShare.Rows(SrNo).Cells(5).Text
                txtStep9_Dependantno_of_shares.Text = Format(CDec(gvDependantShare.Rows(SrNo).Cells(6).Text), Session("fmtCurrency"))
                txtDependantbusinessname_step9.Text = gvDependantShare.Rows(SrNo).Cells(7).Text
                dtStep9_Dependantacquisition_date.SetDate = Convert.ToDateTime(gvDependantShare.DataKeys(SrNo).Item("acquisition_date").ToString)
                txtStep9_Dependantmarketvalue.Text = Format(CDec(gvDependantShare.Rows(SrNo).Cells(9).Text), Session("fmtCurrency"))
                drpStep9_Dependantmarketvalue.SelectedValue = CInt(gvDependantShare.DataKeys(SrNo).Item("countryunkid"))
                'Sohail (07 Dec 2018) -- End

                btnAddDependantShare_step9.Enabled = False
                btnUpdateDependantShare_step9.Enabled = True


            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtDependantShareS9.Rows(CInt(e.CommandArgument)).Delete()

                'If dtDependantShareS9.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtDependantShareS9.NewRow
                '    dtDependantShareS9.Rows.Add(r)
                'End If
                'dtDependantShareS9.AcceptChanges()

                'gvDependantShare.DataSource = dtDependantShareS9
                'gvDependantShare.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_DependantShare()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantShareSrNo") = SrNo

                popupDeleteDependantShare.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvDependantShare_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDependantShare.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If CInt(gvDependantShare.DataKeys(e.Row.RowIndex)("assetsecuritiest2depntranunkid").ToString()) <= -2 AndAlso _
                'CInt(gvDependantShare.DataKeys(e.Row.RowIndex)("assetdeclarationt2unkid").ToString()) <= -2 Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvDependantShare.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Sohail (07 Dec 2018) -- End

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_acquisition_date", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_acquisition_date", False, True)).Text).ToShortDateString

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_market_value", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_market_value", False, True)).Text), Session("fmtCurrency"))

                    'Hemant (23 Nov 2018) -- Start
                    'Enhancement : Changes As per Rutta Request for UAT 
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_noofshares", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_noofshares", False, True)).Text), Session("fmtCurrency"))
                    'Hemant (23 Nov 2018) -- End

                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    'Dim intCountryId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Currency", False, True)).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Currency", False, True)).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Currency", False, True)).Text = ""
                    'End If


                    'Dim intSecuritiesId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Securitytype", False, True)).Text, intSecuritiesId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ASSET_SECURITIES, True, "Asset Securities")
                    'Dim drSecurities As DataRow() = dsList.Tables(0).Select("masterunkid = " & intSecuritiesId & " ", "name")
                    'If drSecurities.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Securitytype", False, True)).Text = drSecurities(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Securitytype", False, True)).Text = ""
                    'End If


                    'Dim intRelationId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Relationship", False, True)).Text, intRelationId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)

                    'Dim drRelation As DataRow() = dsList.Tables(0).Select("masterunkid = " & intRelationId & " ", "name")

                    'If drRelation.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Relationship", False, True)).Text = drRelation(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_Relationship", False, True)).Text = ""
                    'End If

                    ''Hemant (03 Dec 2018) -- Start
                    ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    'Dim intDependantId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_DependantName", False, True)).Text, intDependantId)
                    'If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    '    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & intDependantId & " ", "DpndtBefName")
                    '    If drDependantList.Length > 0 Then
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_DependantName", False, True)).Text = drDependantList(0).Item("DpndtBefName").ToString
                    '    Else
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantShare, "colhStep9_DependantName", False, True)).Text = ""
                    '    End If
                    'End If
                    ''Hemant (03 Dec 2018) -- End
                    'Sohail (07 Dec 2018) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region


    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
#Region "TextBox Events"

    Protected Sub txtDependantmarketvalue_step9_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep9_Dependantmarketvalue.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep9_Dependantmarketvalue.Text = Format(CDec(txtStep9_Dependantmarketvalue.Text), Session("fmtCurrency"))
            If txtStep9_Dependantmarketvalue.Text.Trim <> "" Then txtStep9_Dependantmarketvalue.Text = Format(CDec(txtStep9_Dependantmarketvalue.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtStep9_Dependantno_of_shares_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep9_Dependantno_of_shares.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep9_Dependantno_of_shares.Text = Format(CDec(txtStep9_Dependantno_of_shares.Text), Session("fmtCurrency"))
            If txtStep9_Dependantno_of_shares.Text.Trim <> "" Then txtStep9_Dependantno_of_shares.Text = Format(CDec(txtStep9_Dependantno_of_shares.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Hemant (23 Nov 2018) -- End

    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
#Region "Dropdown Events"
    Protected Sub drpStep9_DependantRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpStep9_DependantRelationship.SelectedIndexChanged
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim dsList As DataSet
        Dim dtDependantList As DataTable
        'Sohail (07 Dec 2018) -- End
        Try
            Dim dtParticularRelationList As DataTable
            If drpStep9_DependantRelationship.SelectedValue > 0 Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "List", , , "hremployee_master.employeeunkid = " + cboStep1_Employee.SelectedValue + " ", _
                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                dtDependantList = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'Sohail (07 Dec 2018) -- End
                dtParticularRelationList = New DataView(dtDependantList, "relationid = " & drpStep9_DependantRelationship.SelectedValue & " ", "", DataViewRowState.CurrentRows).ToTable
                Dim r As DataRow = dtParticularRelationList.NewRow
                r.Item("DpndtBefName") = "Select" '
                r.Item("DpndtTranId") = "0"
                dtParticularRelationList.Rows.InsertAt(r, 0)
                With drpStep9_DependantName
                    .DataValueField = "DpndtTranId"
                    .DataTextField = "DpndtBefName"
                    .DataSource = dtParticularRelationList
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (03 Dec 2018) -- End

#End Region

#Region "Dependant Bank "

#Region " Private Method Functions "

    Private Sub FillComboDeptBank()
        Dim objExchange As New clsExchangeRate
        Dim objclsMasterData As New clsMasterData
        'Hemant (19 Nov 2018) -- Start
        'Enhancement : Changes for NMB Requirement
        Dim dsCombos As DataSet
        Dim objAccType As New clsBankAccType
        'Hemant (19 Nov 2018) -- End
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrAmount10 As DataSet
        'Sohail (07 Dec 2018) -- End
        Try

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
            'With drpStep10_RelationShip
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("Relations")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End            
            'Hemant (03 Dec 2018) -- End

            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrAmount10 = objExchange.getComboList("Currency", True)
            'With drpStep10_DependantBankCurrency
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrAmount10.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            dsCombos = objAccType.getComboList(True, "AccType")
            With drpStep10_DependantAccounttype
                .DataValueField = "accounttypeunkid"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("AccType")
                .DataBind()
            End With
            'Hemant (19 Nov 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillDeptBankGrid()
        Dim objAsset_bankT2depn_tran As New clsAsset_bankT2depn_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtDependantBank10 As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

            'objAsset_bankT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtDependantBank10 = objAsset_bankT2depn_tran._Datasource

            'If dtDependantBank10.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtDependantBank10.NewRow


            '    dtDependantBank10.Rows.Add(r)
            'End If
            Dim dsDependantBank10 As DataSet = objAsset_bankT2depn_tran.GetList("List", mintAssetDeclarationT2Unkid)

            dtDependantBank10 = dsDependantBank10.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtDependantBank10.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtDependantBank10.NewRow
                dtDependantBank10.Rows.Add(r)
            End If
            'Hemant (07 Dec 2018) -- End

            gvDependantBank.DataSource = dtDependantBank10
            gvDependantBank.DataBind()

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If blnRecordExist = False Then
                gvDependantBank.Rows(0).Visible = False
            End If
            'Hemant (07 Dec 2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset_DeptBank()
        Try
            txtStep10_DependantBankname.Text = ""
            txtStep10_DependantAccounttype.Text = ""
            txtStep10_DependantAccountno.Text = ""
            drpStep10_DependantBankCurrency.SelectedValue = 0
            btnAddDependantBankAccount_step10.Enabled = True
            btnUpdateDependantBankAccount_step10.Enabled = False
            'Hemant (19 Nov 2018) -- Start
            'Enhancement : Changes for NMB Requirement
            drpStep10_DependantAccounttype.SelectedValue = 0
            'Hemant (19 Nov 2018) -- End
            'Sohail (30 Nov 2018) -- Start
            'NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            txtcolhStep10_Specify.Text = ""
            'Sohail (30 Nov 2018) -- End

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            drpStep10_RelationShip.SelectedValue = 0
            If (drpStep10_DependantName.Items.Count > 0) Then
                drpStep10_DependantName.SelectedIndex = 0
            End If
            'Hemant (03 Dec 2018) -- End
        Catch ex As Exception
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (03 Dec 2018) -- End
        End Try
    End Sub

    Private Function IsValid_DepetBank() As Boolean
        Try


            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
#End Region

#Region " Button's Events "
    Protected Sub btnAddDependantBankAccount_step10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDependantBankAccount_step10.Click
        Try
            If IsValid_DepetBank() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtDependantBank10.NewRow

            'drRow.Item("assetbankt2depntranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("relationshipunkid") = drpStep10_RelationShip.SelectedValue
            'drRow.Item("dependantunkid") = drpStep10_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("bank_name") = txtStep10_DependantBankname.Text
            'drRow.Item("account_type") = txtStep10_DependantAccounttype.Text
            ''Hemant (19 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("accounttypeunkid") = drpStep10_DependantAccounttype.SelectedValue
            ''Hemant (19 Nov 2018) -- End
            'drRow.Item("account_no") = txtStep10_DependantAccountno.Text
            'drRow.Item("countryunkid") = CInt(drpStep10_DependantBankCurrency.SelectedValue)
            'drRow.Item("amount") = 0
            ''Sohail (30 Nov 2018) -- Start
            ''NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            'drRow.Item("specify") = txtcolhStep10_Specify.Text
            ''Sohail (30 Nov 2018) -- End
            'drRow.Item("isfinalsaved") = 0
            'Update_DataGridview_DataTable_Extra_Columns(gvDependantBank, drRow, CInt(drpStep10_DependantBankCurrency.SelectedValue))
            'dtDependantBank10.Rows.Add(drRow)

            'gvDependantBank.DataSource = dtDependantBank10
            'gvDependantBank.DataBind()

            Dim objAsset_bankT2depn_tran As New clsAsset_bankT2depn_tran
            objAsset_bankT2depn_tran._Assetbank2depntranunkid = -1
            objAsset_bankT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_bankT2depn_tran._Relationshipunkid = drpStep10_RelationShip.SelectedValue
            objAsset_bankT2depn_tran._Dependantunkid = drpStep10_DependantName.SelectedValue
            objAsset_bankT2depn_tran._Bank_Name = txtStep10_DependantBankname.Text
            objAsset_bankT2depn_tran._Account_Type = txtStep10_DependantAccounttype.Text
            objAsset_bankT2depn_tran._Accounttypeunkid = drpStep10_DependantAccounttype.SelectedValue
            objAsset_bankT2depn_tran._Account_No = txtStep10_DependantAccountno.Text
            objAsset_bankT2depn_tran._Countryunkid = CInt(drpStep10_DependantBankCurrency.SelectedValue)
            objAsset_bankT2depn_tran._Amount = 0
            objAsset_bankT2depn_tran._Specify = txtcolhStep10_Specify.Text
            objAsset_bankT2depn_tran._Isfinalsaved = 0

            objAsset_bankT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_bankT2depn_tran._Transactiondate = Nothing Then
                objAsset_bankT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep10_DependantBankCurrency.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_bankT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_bankT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_bankT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_bankT2depn_tran._Exchangerate = decPaidExRate
            objAsset_bankT2depn_tran._Baseamount = 0

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_bankT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_bankT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_bankT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_bankT2depn_tran._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_bankT2depn_tran._FormName = mstrModuleName
            objAsset_bankT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objAsset_bankT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_bankT2depn_tran.Insert(Nothing, objAssetDeclare) = False Then
                If objAsset_bankT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_bankT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_bankT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillDeptBankGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DeptBank()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateDependantBankAccount_step10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDependantBankAccount_step10.Click
        Try
            If IsValid_DepetBank() = False Then
                Exit Try
            End If

            Dim SrNo As Integer = Me.ViewState("DependantBankSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtDependantBank10.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtDependantBank10.Rows(SrNo)

            ''drRow.Item("assetbankt2depntranunkid") = -1
            ''drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("relationshipunkid") = drpStep10_RelationShip.SelectedValue
            'drRow.Item("dependantunkid") = drpStep10_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("bank_name") = txtStep10_DependantBankname.Text
            'drRow.Item("account_type") = txtStep10_DependantAccounttype.Text
            ''Hemant (19 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("accounttypeunkid") = drpStep10_DependantAccounttype.SelectedValue
            ''Hemant (19 Nov 2018) -- End
            'drRow.Item("account_no") = txtStep10_DependantAccountno.Text
            'drRow.Item("countryunkid") = CInt(drpStep10_DependantBankCurrency.SelectedValue)
            'drRow.Item("amount") = 0
            ''Sohail (30 Nov 2018) -- Start
            ''NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
            'drRow.Item("specify") = txtcolhStep10_Specify.Text
            ''Sohail (30 Nov 2018) -- End
            'drRow.Item("isfinalsaved") = 0
            'drRow.Item("AUD") = "U"

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantBank, drRow, CInt(drpStep10_DependantBankCurrency.SelectedValue))

            'dtDependantBank10.AcceptChanges()
            'gvDependantBank.DataSource = dtDependantBank10
            'gvDependantBank.DataBind()

            Dim objBankDepn As New clsAsset_bankT2depn_tran
            objBankDepn.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantBank.DataKeys(SrNo).Item("assetbankt2depntranunkid")), Nothing)
            objBankDepn._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objBankDepn._Relationshipunkid = Convert.ToInt16(drpStep10_RelationShip.SelectedValue)
            objBankDepn._Dependantunkid = drpStep10_DependantName.SelectedValue
            objBankDepn._Bank_Name = txtStep10_DependantBankname.Text
            objBankDepn._Account_Type = txtStep10_DependantAccounttype.Text
            objBankDepn._Accounttypeunkid = drpStep10_DependantAccounttype.SelectedValue
            objBankDepn._Account_No = txtStep10_DependantAccountno.Text
            objBankDepn._Countryunkid = CInt(drpStep10_DependantBankCurrency.SelectedValue)
            objBankDepn._Amount = 0
            objBankDepn._Specify = txtcolhStep10_Specify.Text
            objBankDepn._Isfinalsaved = 0

            objBankDepn._Userunkid = CInt(Session("UserId"))
            If objBankDepn._Transactiondate = Nothing Then
                objBankDepn._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep10_DependantBankCurrency.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objBankDepn._Currencyunkid = intPaidCurrencyunkid
            objBankDepn._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objBankDepn._Baseexchangerate = decBaseExRate
            objBankDepn._Exchangerate = decPaidExRate
            objBankDepn._Baseamount = 0

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBankDepn._AuditUserid = CInt(Session("UserId"))
            Else
                objBankDepn._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objBankDepn._ClientIp = Session("IP_ADD").ToString()
            objBankDepn._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objBankDepn._FormName = mstrModuleName
            objBankDepn._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objBankDepn._IsFromWeb = True


            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objBankDepn.Update(False, Nothing, objAssetDeclare) = False Then
                If objBankDepn._Message <> "" Then
                    DisplayMessage.DisplayMessage(objBankDepn._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objBankDepn._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            Call FillDeptBankGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DeptBank()

        Catch ex As Exception
            'Hemant (23 Nov 2018) -- Start
            'Enhancement : Changes As per Rutta Request for UAT 
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (23 Nov 2018) -- End
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetDependantBankAccount_step10_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetDependantBankAccount_step10.Click
        Try
            Call Reset_DeptBank()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteDependantBankAccount_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteDependantBankAccount.buttonDelReasonYes_Click
        Dim objBankDepn As New clsAsset_bankT2depn_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("DependantBankSrNo"))
            objBankDepn.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantBank.DataKeys(SrNo).Item("assetbankt2depntranunkid")), Nothing)
            objBankDepn._Isvoid = True
            objBankDepn._Voiduserunkid = CInt(Session("UserId"))
            objBankDepn._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objBankDepn._Voidreason = popupDeleteDependantBankAccount.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objBankDepn._AuditUserid = CInt(Session("UserId"))
            Else
                objBankDepn._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objBankDepn._ClientIp = Session("IP_ADD").ToString()
            objBankDepn._HostName = Session("HOST_NAME").ToString()

            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objBankDepn._FormName = mstrModuleName
            objBankDepn._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End

            objBankDepn._IsFromWeb = True

            If objBankDepn.Void(CInt(gvDependantBank.DataKeys(SrNo).Item("assetbankt2depntranunkid")), CInt(Session("UserId")), objBankDepn._Voiddatetime, objBankDepn._Voidreason, Nothing) = True Then
                Call FillDeptBankGrid()
            ElseIf objBankDepn._Message <> "" Then
                DisplayMessage.DisplayMessage(objBankDepn._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 Dec 2018) -- End


#End Region

#Region " GridView Events "

    'Hemant (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub gvDependantBank_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDependantBank.PageIndexChanging
        Try
            gvDependantBank.PageIndex = e.NewPageIndex
            Call FillDeptBankGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (07 Dec 2018) -- End

    Protected Sub gvDependantBank_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDependantBank.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantBankSrNo") = SrNo
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'drpStep10_RelationShip.SelectedValue = dtDependantBank10.Rows(SrNo).Item("relationshipunkid")
                'drpStep10_RelationShip_SelectedIndexChanged(Nothing, Nothing)
                'drpStep10_DependantName.SelectedValue = dtDependantBank10.Rows(SrNo).Item("dependantunkid")
                ''Hemant (03 Dec 2018) -- End
                'txtStep10_DependantBankname.Text = dtDependantBank10.Rows(SrNo).Item("bank_name").ToString
                'txtStep10_DependantAccounttype.Text = dtDependantBank10.Rows(SrNo).Item("account_type").ToString
                ''Hemant (19 Nov 2018) -- Start
                ''Enhancement : Changes for NMB Requirement
                'drpStep10_DependantAccounttype.SelectedValue = dtDependantBank10.Rows(SrNo).Item("accounttypeunkid").ToString
                ''Hemant (19 Nov 2018) -- End
                'txtStep10_DependantAccountno.Text = dtDependantBank10.Rows(SrNo).Item("account_no").ToString
                'drpStep10_DependantBankCurrency.SelectedValue = dtDependantBank10.Rows(SrNo).Item("countryunkid")

                ''Sohail (30 Nov 2018) -- Start
                ''NMB Enhancement - On employee declaration, bank account and dependant bank fields there should be a free text box named "specify" for user to input other account names in 75.1.
                'txtcolhStep10_Specify.Text = dtDependantBank10.Rows(SrNo).Item("specify").ToString
                ''Sohail (30 Nov 2018) -- End

                drpStep10_RelationShip.SelectedValue = CInt(gvDependantBank.DataKeys(SrNo).Item("relationshipunkid").ToString)
                drpStep10_RelationShip_SelectedIndexChanged(drpStep10_RelationShip, New System.EventArgs)
                drpStep10_DependantName.SelectedValue = CInt(gvDependantBank.DataKeys(SrNo).Item("dependantunkid").ToString)
                txtStep10_DependantBankname.Text = gvDependantBank.Rows(SrNo).Cells(4).Text
                drpStep10_DependantAccounttype.SelectedValue = CInt(gvDependantBank.DataKeys(SrNo).Item("accounttypeunkid").ToString)
                txtStep10_DependantAccountno.Text = gvDependantBank.Rows(SrNo).Cells(6).Text
                drpStep10_DependantBankCurrency.SelectedValue = CInt(gvDependantBank.DataKeys(SrNo).Item("countryunkid").ToString)
                txtcolhStep10_Specify.Text = gvDependantBank.Rows(SrNo).Cells(9).Text
                'Sohail (07 Dec 2018) -- End

                btnAddDependantBankAccount_step10.Enabled = False
                btnUpdateDependantBankAccount_step10.Enabled = True


            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtDependantBank10.Rows(CInt(e.CommandArgument)).Delete()

                'If dtDependantBank10.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtDependantBank10.NewRow
                '    dtDependantBank10.Rows.Add(r)
                'End If
                'dtDependantBank10.AcceptChanges()

                'gvDependantBank.DataSource = dtDependantBank10
                'gvDependantBank.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_DeptBank()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantBankSrNo") = SrNo

                popupDeleteDependantBankAccount.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvDependantBank_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDependantBank.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Hemant (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If CInt(gvDependantBank.DataKeys(e.Row.RowIndex)("assetbankt2depntranunkid").ToString()) <= -2 AndAlso _
                '                CInt(gvDependantBank.DataKeys(e.Row.RowIndex)("assetdeclarationt2unkid").ToString()) <= -2 Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvDependantBank.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Hemant (07 Dec 2018) -- End

                    'Hemant (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

                    'Dim intCountryId As Integer

                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_countryunkid", False, True)).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_countryunkid", False, True)).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_countryunkid", False, True)).Text = ""
                    'End If

                    ''Hemant (19 Nov 2018) -- Start
                    ''Enhancement : Changes for NMB Requirement
                    'Dim objAccType As New clsBankAccType
                    'Dim intAccountTypeId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_accounttype", False, True)).Text, intAccountTypeId)
                    'dsList = objAccType.getComboList(True, "AccType")

                    'Dim drAccountType As DataRow() = dsList.Tables(0).Select("accounttypeunkid = " & intAccountTypeId & " ", "name")

                    'If drAccountType.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_accounttype", False, True)).Text = drAccountType(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_accounttype", False, True)).Text = ""
                    'End If
                    ''Hemant (19 Nov 2018) -- End

                    ''Hemant (03 Dec 2018) -- Start
                    ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    'Dim intRelationId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_Relationship", False, True)).Text, intRelationId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)

                    'Dim drRelation As DataRow() = dsList.Tables(0).Select("masterunkid = " & intRelationId & " ", "name")

                    'If drRelation.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_Relationship", False, True)).Text = drRelation(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_Relationship", False, True)).Text = ""
                    'End If


                    'Dim intDependantId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_DependantName", False, True)).Text, intDependantId)
                    'If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    '    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & intDependantId & " ", "DpndtBefName")

                    '    If drDependantList.Length > 0 Then
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_DependantName", False, True)).Text = drDependantList(0).Item("DpndtBefName").ToString
                    '    Else
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantBank, "colhStep10_DependantName", False, True)).Text = ""
                    '    End If
                    'End If
                    'Hemant (07 Dec 2018) -- End
                    'Hemant (03 Dec 2018) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
#Region "Dropdown Events"
    Protected Sub drpStep10_RelationShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpStep10_RelationShip.SelectedIndexChanged
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim dsList As DataSet
        Dim dtDependantList As DataTable
        'Sohail (07 Dec 2018) -- End
        Try
            Dim dtParticularRelationList As DataTable
            If drpStep10_RelationShip.SelectedValue > 0 Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "List", , , "hremployee_master.employeeunkid = " + cboStep1_Employee.SelectedValue + " ", _
                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                dtDependantList = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'Sohail (07 Dec 2018) -- End
                dtParticularRelationList = New DataView(dtDependantList, "relationid = " & drpStep10_RelationShip.SelectedValue & " ", "", DataViewRowState.CurrentRows).ToTable
                Dim r As DataRow = dtParticularRelationList.NewRow
                r.Item("DpndtBefName") = "Select" '
                r.Item("DpndtTranId") = "0"
                dtParticularRelationList.Rows.InsertAt(r, 0)
                With drpStep10_DependantName
                    .DataValueField = "DpndtTranId"
                    .DataTextField = "DpndtBefName"
                    .DataSource = dtParticularRelationList
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (03 Dec 2018) -- End

#End Region

#Region "Dependant Properties "

#Region " Private Method Functions "

    Private Sub FillComboDeptProperties()
        Dim objExchange As New clsExchangeRate
        Dim objclsMasterData As New clsMasterData
        'Hemant (03 Dec 2018) -- Start
        'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
        Dim dsCombos As DataSet
        'Hemant (03 Dec 2018) -- End
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dsCurrEstimatedValueS11 As DataSet
        'Sohail (07 Dec 2018) -- End
        Try

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)
            'With drpStep11_RelationShip
            '    .DataValueField = "masterunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("Relations")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End
            'Hemant (03 Dec 2018) -- End

            'Hemant (02 Feb 2022) -- Start            
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
            'dsCurrEstimatedValueS11 = objExchange.getComboList("Currency", True)
            'With drpStep11_DependantPropertiesEstimatevalue
            '    .DataValueField = "countryunkid"
            '    .DataTextField = "currency_sign"
            '    .DataSource = dsCurrEstimatedValueS11.Tables("Currency")
            '    .DataBind()
            'End With
            'Hemant (02 Feb 2022) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillDeptPropertiesGrid()
        Dim objAsset_propertiesT2depn_tran As New clsAsset_propertiesT2depn_tran
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim dtDependantPropertiesS11 As DataTable
        'Sohail (07 Dec 2018) -- End
        Try

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'objAsset_propertiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid

            'dtDependantPropertiesS11 = objAsset_propertiesT2depn_tran._Datasource

            'If dtDependantPropertiesS11.Rows.Count <= 0 Then
            '    Dim r As DataRow = dtDependantPropertiesS11.NewRow


            '    dtDependantPropertiesS11.Rows.Add(r)
            'End If
            Dim dsDependantPropertiesS11 As DataSet = objAsset_propertiesT2depn_tran.GetList("List", mintAssetDeclarationT2Unkid)
            dtDependantPropertiesS11 = dsDependantPropertiesS11.Tables(0)

            Dim blnRecordExist As Boolean = True
            If dtDependantPropertiesS11.Rows.Count <= 0 Then
                blnRecordExist = False
                Dim r As DataRow = dtDependantPropertiesS11.NewRow
                dtDependantPropertiesS11.Rows.Add(r)
            End If
            'Sohail (07 Dec 2018) -- End

            gvDependantProperties.DataSource = dtDependantPropertiesS11
            gvDependantProperties.DataBind()

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'SetBusinessTotal()
            If blnRecordExist = False Then
                gvDependantProperties.Rows(0).Visible = False
            End If
            'Sohail (07 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub Reset_DeptProperties()
        Try
            txtStep11_DependantPropertiesAsset.Text = ""
            txtDependantPropertiesLocation_step11.Text = ""
            txtDependantPropertiesRegistrationNo_step11.Text = ""
            txtStep11_DependantPropertiesEstimatevalue.Text = 0
            drpStep11_DependantPropertiesEstimatevalue.SelectedIndex = 0

            btnAddDependantProperties_step11.Enabled = True
            btnUpdateDependantProperties_step11.Enabled = False

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            drpStep11_RelationShip.SelectedValue = 0
            If (drpStep11_DependantName.Items.Count > 0) Then
                drpStep11_DependantName.SelectedIndex = 0
            End If
            'Hemant (03 Dec 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValid_DeptProperties() As Boolean
        Try
            If dtpStep11_AcquisitionDateDepnProperties.IsNull OrElse eZeeDate.convertDate(dtpStep11_AcquisitionDateDepnProperties.GetDate) <= "19000101" Then
                'Hemant (01 Feb 2022) -- [OrElse eZeeDate.convertDate(dtpStep11_AcquisitionDateDepnProperties.GetDate) <= "19000101"]
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 51, "Please Select Proper Date"), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button's Events "
    Protected Sub btnAddDependantProperties_step11_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddDependantProperties_step11.Click
        Try
            If IsValid_DeptProperties() = False Then
                Exit Try
            End If

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Dim drRow As DataRow = dtDependantPropertiesS11.NewRow

            'drRow.Item("assetpropertiest2depntranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("relationshipunkid") = drpStep11_RelationShip.SelectedValue
            'drRow.Item("dependantunkid") = drpStep11_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("asset_name") = txtStep11_DependantPropertiesAsset.Text
            'drRow.Item("location") = txtDependantPropertiesLocation_step11.Text
            'drRow.Item("registration_title_no") = txtDependantPropertiesRegistrationNo_step11.Text
            'drRow.Item("countryunkid") = CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedItem.Value)
            'drRow.Item("estimated_value") = txtStep11_DependantPropertiesEstimatevalue.Text
            'drRow.Item("isfinalsaved") = 0

            ''Hemant (15 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("acquisition_date") = dtpStep11_AcquisitionDateDepnProperties.GetDate
            ''Hemant (15 Nov 2018) -- End

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantProperties, drRow, CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedValue))
            'dtDependantPropertiesS11.Rows.Add(drRow)

            'gvDependantProperties.DataSource = dtDependantPropertiesS11
            'gvDependantProperties.DataBind()
            Dim objAsset_propertiesT2depn_tran As New clsAsset_propertiesT2depn_tran

            objAsset_propertiesT2depn_tran._Assetpropertiest2depntranunkid = -1
            objAsset_propertiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_propertiesT2depn_tran._Relationshipunkid = drpStep11_RelationShip.SelectedValue
            objAsset_propertiesT2depn_tran._Dependantunkid = drpStep11_DependantName.SelectedValue
            objAsset_propertiesT2depn_tran._Asset_Name = txtStep11_DependantPropertiesAsset.Text
            objAsset_propertiesT2depn_tran._Location = txtDependantPropertiesLocation_step11.Text
            objAsset_propertiesT2depn_tran._Registration_Title_No = txtDependantPropertiesRegistrationNo_step11.Text
            objAsset_propertiesT2depn_tran._Countryunkid = CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedItem.Value)
            objAsset_propertiesT2depn_tran._Estimated_Value = CDec(txtStep11_DependantPropertiesEstimatevalue.Text)
            objAsset_propertiesT2depn_tran._Isfinalsaved = 0
            objAsset_propertiesT2depn_tran._Acquisition_Date = dtpStep11_AcquisitionDateDepnProperties.GetDate

            objAsset_propertiesT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_propertiesT2depn_tran._Transactiondate = Nothing Then
                objAsset_propertiesT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_propertiesT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_propertiesT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_propertiesT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_propertiesT2depn_tran._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAsset_propertiesT2depn_tran._Baseestimated_Value = objAsset_propertiesT2depn_tran._Estimated_Value * decBaseExRate / decPaidExRate
            Else
                objAsset_propertiesT2depn_tran._Baseestimated_Value = objAsset_propertiesT2depn_tran._Estimated_Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_propertiesT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_propertiesT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_propertiesT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_propertiesT2depn_tran._HostName = Session("HOST_NAME").ToString()
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_propertiesT2depn_tran._FormName = mstrModuleName
            objAsset_propertiesT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End
            objAsset_propertiesT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_propertiesT2depn_tran.Insert(Nothing, objAssetDeclare) = False Then
                If objAsset_propertiesT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_propertiesT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_propertiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillDeptPropertiesGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DeptProperties()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnUpdateDependantProperties_step11_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDependantProperties_step11.Click
        Try

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            If IsValid_DeptProperties() = False Then
                Exit Try
            End If
            'Hemant (03 Dec 2018) -- End

            Dim SrNo As Integer = Me.ViewState("DependantPropertiesSrNo")
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'dtDependantPropertiesS11.Rows(SrNo).BeginEdit()

            'Dim drRow As DataRow = dtDependantPropertiesS11.Rows(SrNo)

            ''drRow.Item("assetpropertiest2depntranunkid") = -1
            'drRow.Item("assetdeclarationt2unkid") = mintAssetDeclarationT2Unkid
            ''Hemant (03 Dec 2018) -- Start
            ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            'drRow.Item("relationshipunkid") = drpStep11_RelationShip.SelectedValue
            'drRow.Item("dependantunkid") = drpStep11_DependantName.SelectedValue
            ''Hemant (03 Dec 2018) -- End
            'drRow.Item("asset_name") = txtStep11_DependantPropertiesAsset.Text
            'drRow.Item("location") = txtDependantPropertiesLocation_step11.Text
            'drRow.Item("registration_title_no") = txtDependantPropertiesRegistrationNo_step11.Text
            'drRow.Item("countryunkid") = CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedItem.Value)
            'drRow.Item("estimated_value") = txtStep11_DependantPropertiesEstimatevalue.Text
            'drRow.Item("isfinalsaved") = 0
            'drRow.Item("AUD") = "U"

            ''Hemant (23 Nov 2018) -- Start
            ''Enhancement : Changes for NMB Requirement
            'drRow.Item("acquisition_date") = dtpStep11_AcquisitionDateDepnProperties.GetDate.Date
            ''Hemant (23 Nov 2018) -- End

            'Update_DataGridview_DataTable_Extra_Columns(gvDependantProperties, drRow, CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedValue))

            'dtDependantPropertiesS11.AcceptChanges()
            'gvDependantProperties.DataSource = dtDependantPropertiesS11
            'gvDependantProperties.DataBind()
            Dim objAsset_propertiesT2depn_tran As New clsAsset_propertiesT2depn_tran

            objAsset_propertiesT2depn_tran.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantProperties.DataKeys(SrNo).Item("assetpropertiest2depntranunkid")), Nothing)
            objAsset_propertiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name")) = mintAssetDeclarationT2Unkid
            objAsset_propertiesT2depn_tran._Relationshipunkid = drpStep11_RelationShip.SelectedValue
            objAsset_propertiesT2depn_tran._Dependantunkid = drpStep11_DependantName.SelectedValue
            objAsset_propertiesT2depn_tran._Asset_Name = txtStep11_DependantPropertiesAsset.Text
            objAsset_propertiesT2depn_tran._Location = txtDependantPropertiesLocation_step11.Text
            objAsset_propertiesT2depn_tran._Registration_Title_No = txtDependantPropertiesRegistrationNo_step11.Text
            objAsset_propertiesT2depn_tran._Countryunkid = CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedItem.Value)
            objAsset_propertiesT2depn_tran._Estimated_Value = txtStep11_DependantPropertiesEstimatevalue.Text
            objAsset_propertiesT2depn_tran._Isfinalsaved = 0
            objAsset_propertiesT2depn_tran._Acquisition_Date = dtpStep11_AcquisitionDateDepnProperties.GetDate

            objAsset_propertiesT2depn_tran._Userunkid = CInt(Session("UserId"))
            If objAsset_propertiesT2depn_tran._Transactiondate = Nothing Then
                objAsset_propertiesT2depn_tran._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            Dim intCountryUnkId As Integer = 0
            Dim decBaseExRate As Decimal = 0
            Dim decPaidExRate As Integer = 0
            Dim intPaidCurrencyunkid As Integer = 0
            Call GetCurrencyRate(CInt(drpStep11_DependantPropertiesEstimatevalue.SelectedItem.Value), decBaseExRate, decPaidExRate, intPaidCurrencyunkid)
            objAsset_propertiesT2depn_tran._Currencyunkid = intPaidCurrencyunkid
            objAsset_propertiesT2depn_tran._Basecurrencyunkid = Me.ViewState("Exchangerateunkid")
            objAsset_propertiesT2depn_tran._Baseexchangerate = decBaseExRate
            objAsset_propertiesT2depn_tran._Exchangerate = decPaidExRate
            If decPaidExRate <> 0 Then
                objAsset_propertiesT2depn_tran._Baseestimated_Value = objAsset_propertiesT2depn_tran._Estimated_Value * decBaseExRate / decPaidExRate
            Else
                objAsset_propertiesT2depn_tran._Baseestimated_Value = objAsset_propertiesT2depn_tran._Estimated_Value
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_propertiesT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_propertiesT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_propertiesT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_propertiesT2depn_tran._HostName = Session("HOST_NAME").ToString()
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_propertiesT2depn_tran._FormName = mstrModuleName
            objAsset_propertiesT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End
            objAsset_propertiesT2depn_tran._IsFromWeb = True

            Dim objAssetDeclare As clsAssetdeclaration_masterT2 = SetValueAssetDeclaratationMaster()
            If objAsset_propertiesT2depn_tran.Update(False, Nothing, objAssetDeclare) = False Then
                If objAsset_propertiesT2depn_tran._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAsset_propertiesT2depn_tran._Message, Me)
                End If
            Else
                mintAssetDeclarationT2Unkid = objAsset_propertiesT2depn_tran._Assetdeclarationt2unkid(Session("Database_Name"))
            End If

            FillDeptPropertiesGrid()
            'Sohail (07 Dec 2018) -- End

            Call Reset_DeptProperties()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub btnResetDependantProperties_step11_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetDependantProperties_step11.Click
        Try
            Call Reset_DeptProperties()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub popupDeleteDependantProperties_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteDependantProperties.buttonDelReasonYes_Click
        Dim objAsset_propertiesT2depn_tran As New clsAsset_propertiesT2depn_tran
        Try
            Dim SrNo As Integer = CInt(Me.ViewState("DependantPropertiesSrNo"))
            objAsset_propertiesT2depn_tran.GetDataByUnkId(Session("Database_Name").ToString, CInt(gvDependantProperties.DataKeys(SrNo).Item("assetpropertiest2depntranunkid")), Nothing)
            objAsset_propertiesT2depn_tran._Isvoid = True
            objAsset_propertiesT2depn_tran._Voiduserunkid = CInt(Session("UserId"))
            objAsset_propertiesT2depn_tran._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objAsset_propertiesT2depn_tran._Voidreason = popupDeleteDependantProperties.Reason.ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objAsset_propertiesT2depn_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objAsset_propertiesT2depn_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objAsset_propertiesT2depn_tran._ClientIp = Session("IP_ADD").ToString()
            objAsset_propertiesT2depn_tran._HostName = Session("HOST_NAME").ToString()
            'Hemant (07 Feb 2019) -- Start
            'ISSUE/ENHANCEMENT : {Audit Trails} in 76.1.
            'objAsset_propertiesT2depn_tran._FormName = mstrModuleName
            objAsset_propertiesT2depn_tran._FormName = mstrModuleName1
            'Hemant (07 Feb 2019) -- End
            objAsset_propertiesT2depn_tran._IsFromWeb = True

            If objAsset_propertiesT2depn_tran.Void(CInt(gvDependantProperties.DataKeys(SrNo).Item("assetpropertiest2depntranunkid")), CInt(Session("UserId")), objAsset_propertiesT2depn_tran._Voiddatetime, objAsset_propertiesT2depn_tran._Voidreason, Nothing) = True Then
                FillDeptPropertiesGrid()
            ElseIf objAsset_propertiesT2depn_tran._Message <> "" Then
                DisplayMessage.DisplayMessage(objAsset_propertiesT2depn_tran._Message, Me)
                Exit Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

#End Region

#Region " GridView Events "

    'Sohail (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Protected Sub gvDependantProperties_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvDependantProperties.PageIndexChanging
        Try
            gvDependantProperties.PageIndex = e.NewPageIndex
            Call FillDeptPropertiesGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (07 Dec 2018) -- End

    Protected Sub gvDependantProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDependantProperties.RowCommand
        Try
            If e.CommandName = "Change" Then
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantPropertiesSrNo") = SrNo
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'drpStep11_RelationShip.SelectedValue = dtDependantPropertiesS11.Rows(SrNo).Item("relationshipunkid")
                'drpStep11_RelationShip_SelectedIndexChanged(Nothing, Nothing)
                'drpStep11_DependantName.SelectedValue = dtDependantPropertiesS11.Rows(SrNo).Item("dependantunkid")
                ''Hemant (03 Dec 2018) -- End
                'txtStep11_DependantPropertiesAsset.Text = dtDependantPropertiesS11.Rows(SrNo).Item("asset_name")
                'txtDependantPropertiesLocation_step11.Text = dtDependantPropertiesS11.Rows(SrNo).Item("location")
                'txtDependantPropertiesRegistrationNo_step11.Text = dtDependantPropertiesS11.Rows(SrNo).Item("registration_title_no")
                ''Hemant (23 Nov 2018) -- Start
                ''Enhancement : Changes As per Rutta Request for UAT 
                ''txtDependantPropertiesEstimatevalue_step11.Text = dtDependantPropertiesS11.Rows(SrNo).Item("estimated_value")
                'txtStep11_DependantPropertiesEstimatevalue.Text = Format(dtDependantPropertiesS11.Rows(SrNo).Item("estimated_value"), Session("fmtCurrency"))
                ''Hemant (23 Nov 2018) -- End
                'drpStep11_DependantPropertiesEstimatevalue.SelectedValue = dtDependantPropertiesS11.Rows(SrNo).Item("countryunkid")


                'btnAddDependantProperties_step11.Enabled = False
                'btnUpdateDependantProperties_step11.Enabled = True

                ''Hemant (23 Nov 2018) -- Start
                ''Enhancement : Changes for NMB Requirement
                'If dtDependantPropertiesS11.Rows(SrNo).Item("acquisition_date") IsNot DBNull.Value Then
                '    dtpStep11_AcquisitionDateDepnProperties.SetDate = Convert.ToDateTime(dtDependantPropertiesS11.Rows(SrNo).Item("acquisition_date"))
                'End If
                ''Hemant (23 Nov 2018) -- End
                drpStep11_RelationShip.SelectedValue = CInt(gvDependantProperties.DataKeys(SrNo).Item("relationshipunkid"))
                drpStep11_RelationShip_SelectedIndexChanged(drpStep11_RelationShip, New System.EventArgs)
                drpStep11_DependantName.SelectedValue = CInt(gvDependantProperties.DataKeys(SrNo).Item("dependantunkid"))
                txtStep11_DependantPropertiesAsset.Text = gvDependantProperties.Rows(SrNo).Cells(4).Text
                txtDependantPropertiesLocation_step11.Text = gvDependantProperties.Rows(SrNo).Cells(5).Text
                txtDependantPropertiesRegistrationNo_step11.Text = gvDependantProperties.Rows(SrNo).Cells(6).Text
                txtStep11_DependantPropertiesEstimatevalue.Text = Format(CDec(gvDependantProperties.Rows(SrNo).Cells(8).Text), Session("fmtCurrency"))
                drpStep11_DependantPropertiesEstimatevalue.SelectedValue = CInt(gvDependantProperties.DataKeys(SrNo).Item("countryunkid"))


                btnAddDependantProperties_step11.Enabled = False
                btnUpdateDependantProperties_step11.Enabled = True

                If gvDependantProperties.DataKeys(SrNo).Item("acquisition_date") IsNot DBNull.Value Then
                    dtpStep11_AcquisitionDateDepnProperties.SetDate = Convert.ToDateTime(gvDependantProperties.DataKeys(SrNo).Item("acquisition_date"))
                End If
                'Sohail (07 Dec 2018) -- End
            ElseIf e.CommandName = "Remove" Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'dtDependantPropertiesS11.Rows(CInt(e.CommandArgument)).Delete()

                'If dtDependantBusinessS8.Rows.Count <= 0 Then
                '    Dim r As DataRow = dtDependantBusinessS8.NewRow
                '    dtDependantBusinessS8.Rows.Add(r)
                'End If
                'dtDependantPropertiesS11.AcceptChanges()

                'gvDependantProperties.DataSource = dtDependantPropertiesS11
                'gvDependantProperties.DataBind()
                'Hemant (02 Feb 2022) -- Start            
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee asset declaration.   
                Reset_DeptProperties()
                'Hemant (02 Feb 2022) -- End
                Dim SrNo As Integer = CInt(e.CommandArgument)
                Me.ViewState("DependantPropertiesSrNo") = SrNo

                popupDeleteDependantProperties.Show()
                'Sohail (07 Dec 2018) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupAddEdit.Show()
        End Try
    End Sub

    Protected Sub gvDependantProperties_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDependantProperties.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                'If CInt(gvDependantProperties.DataKeys(e.Row.RowIndex)("assetpropertiest2depntranunkid").ToString()) <= -2 AndAlso _
                'CInt(gvDependantProperties.DataKeys(e.Row.RowIndex)("assetdeclarationt2unkid").ToString()) <= -2 Then
                '    e.Row.Visible = False
                'Else
                If IsDBNull(gvDependantProperties.DataKeys(e.Row.RowIndex).Item(0)) = False Then
                    'Sohail (07 Dec 2018) -- End
                    'e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhRegDate_step8", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhRegDate_step8", False, True)).Text).ToShortDateString

                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_estimated_value", False, True)).Text = Format(CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_estimated_value", False, True)).Text), Session("fmtCurrency"))

                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    'Dim intCountryId As Integer

                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_Currency", False, True)).Text, intCountryId)
                    'Dim objExRate As New clsExchangeRate
                    'Dim dsList As DataSet = objExRate.GetList("Currency", True, False, 0, intCountryId, False, Nothing, True)
                    'If dsList.Tables(0).Rows.Count > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_Currency", False, True)).Text = dsList.Tables(0).Rows(0)("currency_sign")
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_Currency", False, True)).Text = ""
                    'End If
                    'Sohail (07 Dec 2018) -- End

                    'Hemant (23 Nov 2018) -- Start
                    'Enhancement : Changes for NMB Requirement
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_AcquisitionDateDepnProperties", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_AcquisitionDateDepnProperties", False, True)).Text = CDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_AcquisitionDateDepnProperties", False, True)).Text).ToShortDateString
                    End If
                    'Hemant (23 Nov 2018) -- End

                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    ''Hemant (03 Dec 2018) -- Start
                    ''NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    'Dim intRelationId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_Relationship", False, True)).Text, intRelationId)
                    'dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relations", -1, False, True, 0, True)

                    'Dim drRelation As DataRow() = dsList.Tables(0).Select("masterunkid = " & intRelationId & " ", "name")

                    'If drRelation.Length > 0 Then
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_Relationship", False, True)).Text = drRelation(0).Item("name").ToString
                    'Else
                    '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_Relationship", False, True)).Text = ""
                    'End If


                    'Dim intDependantId As Integer
                    'Integer.TryParse(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_DependantName", False, True)).Text, intDependantId)
                    'If dtDependantList IsNot Nothing AndAlso dtDependantList.Rows.Count > 0 Then
                    '    Dim drDependantList As DataRow() = dtDependantList.Select("DpndtTranId = " & intDependantId & " ", "DpndtBefName")

                    '    If drDependantList.Length > 0 Then
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_DependantName", False, True)).Text = drDependantList(0).Item("DpndtBefName").ToString
                    '    Else
                    '        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvDependantProperties, "colhStep11_DependantName", False, True)).Text = ""
                    '    End If
                    'End If
                    ''Hemant (03 Dec 2018) -- End
                    'Sohail (07 Dec 2018) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Hemant (23 Nov 2018) -- Start
    'Enhancement : Changes As per Rutta Request for UAT 
#Region "TextBox Events"
    Protected Sub txtDependantPropertiesEstimatevalue_step11_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStep11_DependantPropertiesEstimatevalue.TextChanged
        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'txtStep11_DependantPropertiesEstimatevalue.Text = Format(CDec(txtStep11_DependantPropertiesEstimatevalue.Text), Session("fmtCurrency"))
            If txtStep11_DependantPropertiesEstimatevalue.Text.Trim <> "" Then txtStep11_DependantPropertiesEstimatevalue.Text = Format(CDec(txtStep11_DependantPropertiesEstimatevalue.Text), Session("fmtCurrency"))
            'Sohail (07 Dec 2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (23 Nov 2018) -- End
    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
#Region "Dropdown Events"
    Protected Sub drpStep11_RelationShip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpStep11_RelationShip.SelectedIndexChanged
        'Sohail (07 Dec 2018) -- Start
        'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim dsList As DataSet
        Dim dtDependantList As DataTable
        'Sohail (07 Dec 2018) -- End
        Try
            Dim dtParticularRelationList As DataTable
            If drpStep11_RelationShip.SelectedValue > 0 Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "List", , , "hremployee_master.employeeunkid = " + cboStep1_Employee.SelectedValue + " ", _
                                          CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

                dtDependantList = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
                'Sohail (07 Dec 2018) -- End
                dtParticularRelationList = New DataView(dtDependantList, "relationid = " & drpStep11_RelationShip.SelectedValue & " ", "", DataViewRowState.CurrentRows).ToTable
                Dim r As DataRow = dtParticularRelationList.NewRow
                r.Item("DpndtBefName") = "Select" '
                r.Item("DpndtTranId") = "0"
                dtParticularRelationList.Rows.InsertAt(r, 0)
                With drpStep11_DependantName
                    .DataValueField = "DpndtTranId"
                    .DataTextField = "DpndtBefName"
                    .DataSource = dtParticularRelationList
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Hemant (03 Dec 2018) -- End

#End Region

#Region "Sign Off "

#Region " Private Method Functions "
    Private Sub Set_SignOffDeclarations()
        Try
            Dim strPara1 As String = String.Empty
            Dim strPara2 As String = String.Empty
            Dim strPara3 As String = String.Empty
            Dim strPara4 As String = String.Empty
            Dim strSignatureLine As String = String.Empty
            'strPara1 = "I " & drpEmpListAddEdit.SelectedItem.ToString.Split("-")(1).Trim & ". Do hereby declare that I have read and understood the NMB Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct."
            'Hemant (10 May 2021) -- Start
            'strPara1 = Language.getMessage(mstrModuleName1, 81, "I") & " " & cboStep1_Employee.SelectedItem.ToString.Split("-")(1).Trim & Language.getMessage(mstrModuleName1, 82, ". Do hereby declare that I have read and understood the NMB Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct.")

            'txtPara1_Step12.Text = strPara1
            'strPara2 = Language.getMessage(mstrModuleName1, 83, "I undertake to fully comply with the letter and spirit of the Code of Conduct. I understand that I have a personal responsibility to ensure that I am fully aware of the requirements under the Code of Conduct and if I am not aware of the requirements, I should familiarize myself with the content before signing this acknowledgement form.")

            'txtPara2_Step12.Text = strPara2
            'strPara3 = Language.getMessage(mstrModuleName1, 84, "I have made the disclosures above as required under the NMB Code of Conduct and confirm that the said disclosures are made willingly and reflect the truth of my status as of the date indicated below.")

            'txtPara3_Step12.Text = strPara3
            'strPara4 = Language.getMessage(mstrModuleName1, 85, "I further understand that in the event that the said disclosures are found to be incomplete or untrue, I will face disciplinary action which may include termination of my employment with the Bank.")


            'txtPara4_Step12.Text = strPara4
            strPara1 = Language.getMessage(mstrModuleName1, 81, "I") & " " & cboStep1_Employee.SelectedItem.ToString.Split("-")(1).Trim & Language.getMessage(mstrModuleName1, 82, ". Do hereby declare that I have read and understood the #CompanyCode# Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct.")
            strPara1 = strPara1.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara1_Step12.Text = strPara1

            strPara2 = Language.getMessage(mstrModuleName1, 83, "I undertake to fully comply with the letter and spirit of the Code of Conduct. I understand that I have a personal responsibility to ensure that I am fully aware of the requirements under the Code of Conduct and if I am not aware of the requirements, I should familiarize myself with the content before signing this acknowledgement form.")
            strPara2 = strPara2.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara2_Step12.Text = strPara2

            strPara3 = Language.getMessage(mstrModuleName1, 84, "I have made the disclosures above as required under the #CompanyCode# Code of Conduct and confirm that the said disclosures are made willingly and reflect the truth of my status as of the date indicated below.")
            strPara3 = strPara3.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara3_Step12.Text = strPara3

            strPara4 = Language.getMessage(mstrModuleName1, 85, "I further understand that in the event that the said disclosures are found to be incomplete or untrue, I will face disciplinary action which may include termination of my employment with the Bank.")
            strPara4 = strPara4.Replace("#CompanyCode#", Session("CompanyCode").ToString())
            txtPara4_Step12.Text = strPara4
            'Hemant (10 May 2021) -- End

            strSignatureLine = Language.getMessage(mstrModuleName1, 86, "Signature:") & " " & cboStep1_Employee.SelectedItem.ToString.Split("-")(1).Trim

            txtSignatureLine_Step12.Text = strSignatureLine
            txtDate_Step12.Text = Language.getMessage(mstrModuleName1, 87, "Date:") & " " & ConfigParameter._Object._CurrentDateAndTime.Date & ""

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Button's Events "

#End Region

#End Region

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(lblPageHeader.ID, Me.Title)
            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.chkIncludeClosedYearTrans.ID, Me.chkIncludeClosedYearTrans.Text)
            Language._Object.setCaption(Me.BtnSearch.ID, Me.BtnSearch.Text)
            Language._Object.setCaption(Me.BtnReset.ID, Me.BtnReset.Text)

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            Language._Object.setCaption(Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
            'Hemant (05 Dec 2018) -- End

            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            Language._Object.setCaption("btnEdit", gvList.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete", gvList.Columns(1).HeaderText)
            Language._Object.setCaption("mnuFinalSave", gvList.Columns(2).HeaderText)
            Language._Object.setCaption("mnuUnlockFinalSave", gvList.Columns(3).HeaderText)
            Language._Object.setCaption("mnuPrintDeclaration", gvList.Columns(4).HeaderText)
            Language._Object.setCaption(gvList.Columns(5).FooterText, gvList.Columns(5).HeaderText)
            Language._Object.setCaption(gvList.Columns(6).FooterText, gvList.Columns(6).HeaderText)
            Language._Object.setCaption(gvList.Columns(7).FooterText, gvList.Columns(7).HeaderText)
            Language._Object.setCaption(gvList.Columns(8).FooterText, gvList.Columns(8).HeaderText)
            Language._Object.setCaption(gvList.Columns(9).FooterText, gvList.Columns(9).HeaderText)

            Language._Object.setCaption(Me.btnScanAttachDocuments.ID, Me.btnScanAttachDocuments.Text)
            Language._Object.setCaption(Me.BtnNew.ID, Me.BtnNew.Text)
            Language._Object.setCaption(Me.BtnClose.ID, Me.BtnClose.Text)

            Language.setMessage(mstrModuleName, 6, "After submission, you can not edit / delete declaration,")
            Language.setMessage(mstrModuleName, 90, "Are you sure you want to sumbit this declaration?")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to Unlock Final Save Declaration?")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this declaration?")
           
            Language.setLanguage(mstrModuleName1)

            Language._Object.setCaption(lnkmenu_Instruction.ID, Me.lnkmenu_Instruction.Text)
            Language._Object.setCaption(lnkmenu_BusinessDealings.ID, Me.lnkmenu_BusinessDealings.Text)
            Language._Object.setCaption(lnkmenu_StaffOwnership.ID, Me.lnkmenu_StaffOwnership.Text)
            Language._Object.setCaption(lnkmenu_BankAccounts.ID, Me.lnkmenu_BankAccounts.Text)
            Language._Object.setCaption(lnkmenu_Realproperties.ID, Me.lnkmenu_Realproperties.Text)
            Language._Object.setCaption(lnkmenu_Liabilities.ID, Me.lnkmenu_Liabilities.Text)
            Language._Object.setCaption(lnkmenu_Relatives.ID, Me.lnkmenu_Relatives.Text)
            Language._Object.setCaption(lnkmenu_DependantBusiness.ID, Me.lnkmenu_DependantBusiness.Text)
            Language._Object.setCaption(lnkmenu_DependantShares.ID, Me.lnkmenu_DependantShares.Text)
            Language._Object.setCaption(lnkmenu_DependantBank.ID, Me.lnkmenu_DependantBank.Text)
            Language._Object.setCaption(lnkmenu_DependantProperties.ID, Me.lnkmenu_DependantProperties.Text)
            Language._Object.setCaption(lnkmenu_SignOff.ID, Me.lnkmenu_SignOff.Text)

            Language._Object.setCaption(lblInstruction.ID, Me.lblInstruction.Text)
            Language._Object.setCaption(gbEmployee.ID, Me.gbEmployee.Text)
            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            Language._Object.setCaption(lblStep1_FinancialYear.ID, Me.lblStep1_FinancialYear.Text)
            'Hemant (05 Dec 2018) -- End
            Language._Object.setCaption(lblStep1_Employee.ID, Me.lblStep1_Employee.Text)
            Language._Object.setCaption(lblStep1_Date.ID, Me.lblStep1_Date.Text)
            Language._Object.setCaption(lblStep1_EmployeeCode.ID, Me.lblStep1_EmployeeCode.Text)
            Language._Object.setCaption(lblStep1_Departments.ID, Me.lblStep1_Departments.Text)

            Language._Object.setCaption(lblStep1_OfficePosition.ID, Me.lblStep1_OfficePosition.Text)
            Language._Object.setCaption(lblStep1_CenterforWork.ID, Me.lblStep1_CenterforWork.Text)
            Language._Object.setCaption(Me.lblStep1_WorkStation.ID, Me.lblStep1_WorkStation.Text)
            Language._Object.setCaption(Me.lblStep1_Tin_No.ID, Me.lblStep1_Tin_No.Text)
            Language.setMessage(mstrModuleName1, 20, "Are You Sure you want to save this declaration?")
            Language.setMessage(mstrModuleName1, 21, "Confirmation")

            'step2 
            Language._Object.setCaption(Me.lblBusinessDealings_Step2.ID, Me.lblBusinessDealings_Step2.Text)

            Language._Object.setCaption(Me.colhStep2_Sector.ID, Me.colhStep2_Sector.Text)
            Language._Object.setCaption(Me.RfStep2_Sector.ID, Me.RfStep2_Sector.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_Company_Name.ID, Me.colhStep2_Company_Name.Text)
            Language._Object.setCaption(Me.RfStep2_Company_NameError.ID, Me.RfStep2_Company_NameError.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_company_tin_no.ID, Me.colhStep2_company_tin_no.Text)
            Language._Object.setCaption(Me.rfStep2_CompanyTinNo.ID, Me.rfStep2_CompanyTinNo.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_registration_date.ID, Me.colhStep2_registration_date.Text)

            Language._Object.setCaption(Me.colhStep2_address_location.ID, Me.colhStep2_address_location.Text)
            Language._Object.setCaption(Me.rfStep2_Addresslocation.ID, Me.rfStep2_Addresslocation.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_business_type.ID, Me.colhStep2_business_type.Text)
            Language._Object.setCaption(Me.rFStep2_BusinessType.ID, Me.rFStep2_BusinessType.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_Position_held.ID, Me.colhStep2_Position_held.Text)
            Language._Object.setCaption(Me.rfStep2_PositionHeld.ID, Me.rfStep2_PositionHeld.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_Is_supplier_client.ID, Me.colhStep2_Is_supplier_client.Text)
            Language._Object.setCaption(Me.rfStep2_Is_supplier_client.ID, Me.rfStep2_Is_supplier_client.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_Monthly_annual_earnings.ID, Me.colhStep2_Monthly_annual_earnings.Text)
            Language._Object.setCaption(Me.rfStep2_MonthlyAnnualEarnings.ID, Me.rfStep2_MonthlyAnnualEarnings.ErrorMessage)

            Language._Object.setCaption(Me.rfStep2_CurrencyBusiness.ID, Me.rfStep2_CurrencyBusiness.ErrorMessage)


            Language._Object.setCaption(Me.colhStep2_Business_contact_no.ID, Me.colhStep2_Business_contact_no.Text)
            Language._Object.setCaption(Me.rfStep2_BusinessContactNo.ID, Me.rfStep2_BusinessContactNo.ErrorMessage)

            Language._Object.setCaption(Me.colhStep2_Shareholders_names.ID, Me.colhStep2_Shareholders_names.Text)
            Language._Object.setCaption(Me.rfStep2_ShareholdersNames.ID, Me.rfStep2_ShareholdersNames.ErrorMessage)

            Language._Object.setCaption(Me.BtnAddBusiness.ID, Me.BtnAddBusiness.Text)
            Language._Object.setCaption(Me.BtnUpdateBusiness.ID, Me.BtnUpdateBusiness.Text)
            Language._Object.setCaption(Me.btnResetBusiness.ID, Me.btnResetBusiness.Text)

            Language._Object.setCaption(gvBusiness.Columns(5).FooterText, gvBusiness.Columns(5).HeaderText)
            Language._Object.setCaption("btnEdit_step2", gvBusiness.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step2", gvBusiness.Columns(1).HeaderText)

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 30, "Are you sure you want to delete selected business deal?")
            'Gajanan (07 Dec 2018) -- End


            'Step 3
            Language._Object.setCaption(Me.lblStaffOwnerShip_step3.ID, Me.lblStaffOwnerShip_step3.Text)

            Language._Object.setCaption(Me.colhStep3_Securitiestype.ID, Me.colhStep3_Securitiestype.Text)
            Language._Object.setCaption(Me.rfStep3_Securitiestype.ID, Me.rfStep3_Securitiestype.ErrorMessage)

            Language._Object.setCaption(Me.colhStep3_Certificate_no.ID, Me.colhStep3_Certificate_no.Text)
            Language._Object.setCaption(Me.rfStep3_Certificate_no.ID, Me.rfStep3_Certificate_no.ErrorMessage)

            Language._Object.setCaption(Me.colhStep3_No_of_shares_step3.ID, Me.colhStep3_No_of_shares_step3.Text)
            Language._Object.setCaption(Me.rfStep3_No_of_shares.ID, Me.rfStep3_No_of_shares.ErrorMessage)

            Language._Object.setCaption(Me.colhStep3_Company_business_name.ID, Me.colhStep3_Company_business_name.Text)
            Language._Object.setCaption(Me.rfStep3_Company_business_name.ID, Me.rfStep3_Company_business_name.ErrorMessage)

            Language._Object.setCaption(Me.colhStep3_Marketvalue_step3.ID, Me.colhStep3_Marketvalue_step3.Text)
            Language._Object.setCaption(Me.rfStep3_Marketvalue.ID, Me.rfStep3_Marketvalue.Text)

            Language._Object.setCaption(Me.rfStep3_CurrencyShare.ID, Me.rfStep3_CurrencyShare.ErrorMessage)
            Language._Object.setCaption(Me.colhStep3_Acquisition_date.ID, Me.colhStep3_Acquisition_date.Text)

            Language._Object.setCaption(Me.btnAddStaffOwnerShip.ID, Me.btnAddStaffOwnerShip.Text)
            Language._Object.setCaption(Me.btnUpdateStaffOwnerShip.ID, Me.btnUpdateStaffOwnerShip.Text)
            Language._Object.setCaption(Me.btnResetStaffOwnerShip.ID, Me.btnResetStaffOwnerShip.Text)

            Language._Object.setCaption(gvStaffOwnerShip.Columns(6).FooterText, gvStaffOwnerShip.Columns(6).HeaderText)
            Language._Object.setCaption("btnEdit_step3", gvStaffOwnerShip.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step3", gvStaffOwnerShip.Columns(1).HeaderText)
            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.

            Language.setMessage(mstrModuleName1, 31, "Are you sure you want to delete selected staff Ownership?")
           
            'Gajanan (07 Dec 2018) -- End

            'Step4
            Language._Object.setCaption(Me.lblBankAccounts_step4.ID, Me.lblBankAccounts_step4.Text)

            Language._Object.setCaption(Me.colhStep4_BankName.ID, Me.colhStep4_BankName.Text)
            Language._Object.setCaption(Me.rfStep4_BankName.ID, Me.rfStep4_BankName.Text)

            Language._Object.setCaption(Me.colhStep4_AccountTypeBank.ID, Me.colhStep4_AccountTypeBank.Text)
            Language._Object.setCaption(Me.rfStep4_AccountTypeBankAccounts.ID, Me.rfStep4_AccountTypeBankAccounts.ErrorMessage)

            Language._Object.setCaption(Me.colhStep4_AccountNo.ID, Me.colhStep4_AccountNo.Text)
            Language._Object.setCaption(Me.rfStep4_AccountNoBank.ID, Me.rfStep4_AccountNoBank.ErrorMessage)

            Language._Object.setCaption(Me.colhStep4_DepositsSourceBank.ID, Me.colhStep4_DepositsSourceBank.Text)
            Language._Object.setCaption(Me.rfStep4_DepositsSourceBankAccounts.ID, Me.rfStep4_DepositsSourceBankAccounts.ErrorMessage)

            Language._Object.setCaption(Me.colhStep4_Specify.ID, Me.colhStep4_Specify.Text)

            Language._Object.setCaption(Me.rfStep4_CurrencyBank.ID, Me.rfStep4_CurrencyBank.ErrorMessage)

            Language._Object.setCaption(Me.btnAddBankAccounts.ID, Me.btnAddBankAccounts.Text)
            Language._Object.setCaption(Me.btnUpdateBankAccounts.ID, Me.btnUpdateBankAccounts.Text)
            Language._Object.setCaption(Me.btnResetBankAccounts.ID, Me.btnResetBankAccounts.Text)

            Language._Object.setCaption(gvBankAccounts.Columns(5).FooterText, gvBankAccounts.Columns(5).HeaderText)
            Language._Object.setCaption("btnEdit_step4", gvBankAccounts.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step4", gvBankAccounts.Columns(1).HeaderText)

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 32, "Are you sure you want to delete selected bank account?")
            'Gajanan (07 Dec 2018) -- End

            'Step5
            Language._Object.setCaption(Me.lblRealproperties_step5.ID, Me.lblRealproperties_step5.Text)
            Language._Object.setCaption(Me.colhStep5_AssetNameProperties.ID, Me.colhStep5_AssetNameProperties.Text)
            Language._Object.setCaption(Me.rfStep5_AssetName.ID, Me.rfStep5_AssetName.ErrorMessage)

            Language._Object.setCaption(Me.colhStep5_LocationProperties.ID, Me.colhStep5_LocationProperties.Text)

            Language._Object.setCaption(Me.colhStep5_RegistrationTitleNoProperties.ID, Me.colhStep5_LocationProperties.Text)

            Language._Object.setCaption(Me.colhStep5_EstimatedValueProperties.ID, Me.colhStep5_EstimatedValueProperties.Text)
            Language._Object.setCaption(Me.rfStep5_EstimatedValueRealProperties.ID, Me.rfStep5_EstimatedValueRealProperties.ErrorMessage)

            Language._Object.setCaption(Me.rfStep5_CurrencyProperties.ID, Me.rfStep5_CurrencyProperties.ErrorMessage)

            Language._Object.setCaption(Me.colhStep5_FundsSourceProperties.ID, Me.colhStep5_FundsSourceProperties.Text)
            Language._Object.setCaption(Me.rfStep5_FundsSourceRealProperties.ID, Me.rfStep5_FundsSourceRealProperties.ErrorMessage)

            Language._Object.setCaption(Me.colhStep5_AssetLocationProperties.ID, Me.colhStep5_AssetLocationProperties.Text)
            Language._Object.setCaption(Me.colhStep5_AcquisitionDateProperties.ID, Me.colhStep5_AcquisitionDateProperties.Text)

            Language._Object.setCaption(Me.colhStep5_AssetUseProperties.ID, Me.colhStep5_AssetUseProperties.Text)
            Language._Object.setCaption(Me.rfStep5_AssetUseRealProperties.ID, Me.rfStep5_AssetUseRealProperties.ErrorMessage)

            Language._Object.setCaption(Me.btnAddRealProperties.ID, Me.btnAddRealProperties.Text)
            Language._Object.setCaption(Me.btnUpdateRealProperties.ID, Me.btnUpdateRealProperties.Text)
            Language._Object.setCaption(Me.btnResetRealProperties.ID, Me.btnResetRealProperties.Text)



            Language._Object.setCaption(gvRealProperties.Columns(6).FooterText, gvRealProperties.Columns(6).HeaderText)
            Language._Object.setCaption("btnEdit_step5", gvRealProperties.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step5", gvRealProperties.Columns(1).HeaderText)


            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 33, "Are you sure you want to delete selected properties?")
            'Gajanan (07 Dec 2018) -- End

            'Step 6
            '
            Language._Object.setCaption(Me.lblStep6_Liabilities.ID, Me.lblStep6_Liabilities.Text)

            Language._Object.setCaption(Me.colhStep6_InstitutionNameLiabilities.ID, Me.colhStep6_InstitutionNameLiabilities.Text)
            Language._Object.setCaption(Me.rfStep6_InstitutionName.ID, Me.rfStep6_InstitutionName.ErrorMessage)

            Language._Object.setCaption(Me.colhStep6_LiabilityTypeLiabilities.ID, Me.colhStep6_LiabilityTypeLiabilities.Text)
            Language._Object.setCaption(Me.rfStep6_LiabilityTypeLiabilities.ID, Me.rfStep6_LiabilityTypeLiabilities.ErrorMessage)

            Language._Object.setCaption(Me.colhStep6_OriginalBalanceLiabilities.ID, Me.colhStep6_OriginalBalanceLiabilities.Text)
            Language._Object.setCaption(Me.rfStep6_OriginalBalanceLiabilities.ID, Me.rfStep6_OriginalBalanceLiabilities.ErrorMessage)
            Language._Object.setCaption(Me.rfStep6_OrigCurrencyLiabilities.ID, Me.rfStep6_OrigCurrencyLiabilities.ErrorMessage)



            Language._Object.setCaption(Me.colhStep6_OutstandingBalanceLiabilities.ID, Me.colhStep6_OutstandingBalanceLiabilities.Text)
            Language._Object.setCaption(Me.rfStep6_OutstandingBalanceLiabilities.ID, Me.rfStep6_OutstandingBalanceLiabilities.ErrorMessage)
            Language._Object.setCaption(Me.rfStep6_OutCurrencyLiabilities.ID, Me.rfStep6_OutCurrencyLiabilities.ErrorMessage)



            Language._Object.setCaption(Me.colhStep6_DurationLiabilities.ID, Me.colhStep6_DurationLiabilities.Text)
            Language._Object.setCaption(Me.rfStep6_Duration.ID, Me.rfStep6_Duration.ErrorMessage)


            Language._Object.setCaption(Me.colhStep6_LoanPurposeLiabilities.ID, Me.colhStep6_LoanPurposeLiabilities.Text)
            Language._Object.setCaption(Me.rfStep6_LoanPurposeLiabilities.ID, Me.rfStep6_LoanPurposeLiabilities.ErrorMessage)

            Language._Object.setCaption(Me.colhStep6_Liability_dateLiabilities.ID, Me.colhStep6_Liability_dateLiabilities.Text)
            Language._Object.setCaption(Me.colhStep6_Payoff_dateLiabilities.ID, Me.colhStep6_Payoff_dateLiabilities.Text)


            Language._Object.setCaption(Me.colhStep6_Remark_step6Liabilities.ID, Me.colhStep6_Remark_step6Liabilities.Text)
            Language._Object.setCaption(Me.rfStep6_RemarkLiabilities.ID, Me.rfStep6_RemarkLiabilities.ErrorMessage)



            Language._Object.setCaption(Me.btnAddLiabilities.ID, Me.btnAddLiabilities.Text)
            Language._Object.setCaption(Me.btnUpdateLiabilities.ID, Me.btnUpdateLiabilities.Text)
            Language._Object.setCaption(Me.btnResetLiabilities.ID, Me.btnResetLiabilities.Text)


            'gvLiabilities.Columns(2).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(2).FooterText, gvLiabilities.Columns(2).HeaderText)
            'gvLiabilities.Columns(3).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(3).FooterText, gvLiabilities.Columns(3).HeaderText)
            'gvLiabilities.Columns(4).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(4).FooterText, gvLiabilities.Columns(4).HeaderText)
            Language._Object.setCaption(gvLiabilities.Columns(5).FooterText, gvLiabilities.Columns(5).HeaderText)
            'gvLiabilities.Columns(6).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(6).FooterText, gvLiabilities.Columns(6).HeaderText)
            Language._Object.setCaption(gvLiabilities.Columns(7).FooterText, gvLiabilities.Columns(7).HeaderText)
            'gvLiabilities.Columns(8).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(8).FooterText, gvLiabilities.Columns(8).HeaderText)
            'gvLiabilities.Columns(9).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(9).FooterText, gvLiabilities.Columns(9).HeaderText)
            'gvLiabilities.Columns(10).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(9).FooterText, gvLiabilities.Columns(9).HeaderText)
            'gvLiabilities.Columns(11).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(9).FooterText, gvLiabilities.Columns(9).HeaderText)
            'gvLiabilities.Columns(12).HeaderText = Language._Object.setCaption(gvLiabilities.Columns(9).FooterText, gvLiabilities.Columns(9).HeaderText)
            Language._Object.setCaption("btnEdit_step6", gvLiabilities.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step6", gvLiabilities.Columns(1).HeaderText)

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 41, "Are you sure you want to delete selected liabilities?")
            'Sohail (07 Dec 2018) -- End

            'Step 7

            Language._Object.setCaption(Me.lblRelativesInEmployment_Step7.ID, Me.lblRelativesInEmployment_Step7.Text)

            Language._Object.setCaption(Me.colhStep7_RelativeEmployeeRelatives.ID, Me.colhStep7_RelativeEmployeeRelatives.Text)
            Language._Object.setCaption(Me.rfStep7_RelativeEmployee.ID, Me.rfStep7_RelativeEmployee.ErrorMessage)

            Language._Object.setCaption(colhStep7_relationshipRelatives.ID, Me.colhStep7_relationshipRelatives.Text)
            Language._Object.setCaption(Me.rfStep7_Relationship.ID, Me.rfStep7_Relationship.ErrorMessage)

            Language._Object.setCaption(chkOthers.ID, Me.chkOthers.Text)

            Language._Object.setCaption(txtRelativeEmployee.ID, Me.txtRelativeEmployee.Text)
            Language._Object.setCaption(Me.rftxtRelativeEmployee.ID, Me.rftxtRelativeEmployee.ErrorMessage)

            Language._Object.setCaption(btnAddRelatives.ID, Me.btnAddRelatives.Text)
            Language._Object.setCaption(btnUpdateRelatives.ID, Me.btnUpdateRelatives.Text)
            Language._Object.setCaption(btnResetRelatives.ID, Me.btnResetRelatives.Text)

            'Language._Object.setCaption(gvRelatives.Columns(2).FooterText, gvRelatives.Columns(2).HeaderText)
            'Language._Object.setCaption(gvRelatives.Columns(3).FooterText, gvRelatives.Columns(3).HeaderText)
            Language._Object.setCaption("btnEdit_step7", gvRelatives.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step7", gvRelatives.Columns(1).HeaderText)

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 34, "Are you sure you want to delete selected Relative?")
            'Hemant (07 Dec 2018) -- End


            'step8
            Language._Object.setCaption(Me.lblDependantBusinessTitle_Step8.ID, Me.lblDependantBusinessTitle_Step8.Text)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Language._Object.setCaption(Me.colhStep8_RelationShip.ID, Me.colhStep8_RelationShip.Text)
            Language._Object.setCaption(Me.rfStep8_RelationShip.ID, Me.rfStep8_RelationShip.ErrorMessage)
            Language._Object.setCaption(Me.colhStep8_DependantName.ID, Me.colhStep8_DependantName.Text)
            Language._Object.setCaption(Me.rfStep8_DependantName.ID, Me.rfStep8_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End

            Language._Object.setCaption(Me.colhStep8_Sector.ID, Me.colhStep8_Sector.Text)
            Language._Object.setCaption(Me.rfStep8_Sector.ID, Me.rfStep8_Sector.ErrorMessage)

            Language._Object.setCaption(Me.colhStep8_CompanyName.ID, Me.colhStep8_CompanyName.Text)
            Language._Object.setCaption(Me.rfStep8_Companyname.ID, Me.rfStep8_Companyname.ErrorMessage)
            Language._Object.setCaption(Me.colhStep8_CompanyTinNo.ID, Me.colhStep8_CompanyTinNo.Text)
            Language._Object.setCaption(Me.rfStep8_Companytinno.ID, Me.rfStep8_Companytinno.ErrorMessage)
            Language._Object.setCaption(Me.colhStep8_RegDate.ID, Me.colhStep8_RegDate.Text)
            Language._Object.setCaption(Me.colhStep8_BusinessType.ID, Me.colhStep8_BusinessType.Text)
            Language._Object.setCaption(Me.rfStep8_BussinessType.ID, Me.rfStep8_BussinessType.ErrorMessage)
            Language._Object.setCaption(Me.colhStep8_position_held.ID, Me.colhStep8_position_held.Text)
            Language._Object.setCaption(Me.rfStep8_Position.ID, Me.rfStep8_Position.ErrorMessage)
            Language._Object.setCaption(Me.colhStep8_monthlyearning.ID, Me.colhStep8_monthlyearning.Text)
            Language._Object.setCaption(Me.rfStep8_MonthlyEarning.ID, Me.rfStep8_MonthlyEarning.ErrorMessage)
            Language._Object.setCaption(Me.rfStep8_CurrencyMonthlyEarning.ID, Me.rfStep8_CurrencyMonthlyEarning.ErrorMessage)

            Language._Object.setCaption(Me.colhStep8_is_supplier_client.ID, Me.colhStep8_is_supplier_client.Text)
            Language._Object.setCaption(Me.rfStep8_SupplierClient.ID, Me.rfStep8_SupplierClient.ErrorMessage)

            Language._Object.setCaption(Me.colhStep8_contactno.ID, Me.colhStep8_contactno.Text)
            Language._Object.setCaption(Me.rfStep8_Contactno.ID, Me.rfStep8_Contactno.ErrorMessage)

            Language._Object.setCaption(Me.colhStep8_shareholders_names.ID, Me.colhStep8_shareholders_names.Text)
            Language._Object.setCaption(Me.rfStep8_Shareholder.ID, Me.rfStep8_Shareholder.ErrorMessage)

            Language._Object.setCaption(Me.colhStep8_Address.ID, Me.colhStep8_Address.Text)
            Language._Object.setCaption(Me.rfStep8_Address.ID, Me.rfStep8_Address.ErrorMessage)


            Language._Object.setCaption(Me.btnAddDependantBusiness_step8.ID, Me.btnAddDependantBusiness_step8.Text)
            Language._Object.setCaption(Me.btnUpdateDependantBusiness_step8.ID, Me.btnUpdateDependantBusiness_step8.Text)
            Language._Object.setCaption(Me.btnResetDependantBusiness_step8.ID, Me.btnResetDependantBusiness_step8.Text)

            
            Language._Object.setCaption(gvDependantBusiness.Columns(10).FooterText, gvDependantBusiness.Columns(10).HeaderText)
            Language._Object.setCaption("btnEdit_step8", gvDependantBusiness.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step8", gvDependantBusiness.Columns(1).HeaderText)

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 35, "Are you sure you want to delete selected Dependant Business Deal?")
            'Hemant (07 Dec 2018) -- End

            'step 9
            Language._Object.setCaption(Me.lblDependantShareTitle_Step9.ID, Me.lblDependantShareTitle_Step9.Text)
            Language._Object.setCaption(Me.colhStep9_Relationship.ID, Me.colhStep9_Relationship.Text)
            Language._Object.setCaption(Me.rfStep9_DependantRelationship.ID, Me.rfStep9_DependantRelationship.ErrorMessage)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Language._Object.setCaption(Me.colhStep9_DependantName.ID, Me.colhStep9_DependantName.Text)
            Language._Object.setCaption(Me.rfStep9_DependantName.ID, Me.rfStep9_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End


            Language._Object.setCaption(Me.colhStep9_Securitytype.ID, Me.colhStep9_Securitytype.Text)
            Language._Object.setCaption(Me.rfStep9_DependantSecurityType.ID, Me.rfStep9_DependantSecurityType.ErrorMessage)


            Language._Object.setCaption(Me.colhStep9_certificateno.ID, Me.colhStep9_certificateno.Text)
            Language._Object.setCaption(Me.rfStep9_DependantCertificateno.ID, Me.rfStep9_DependantCertificateno.ErrorMessage)
            Language._Object.setCaption(Me.colhStep9_noofshares.ID, Me.colhStep9_noofshares.Text)
            Language._Object.setCaption(Me.rfStep9_Dependantno_of_shares.ID, Me.rfStep9_Dependantno_of_shares.ErrorMessage)

            Language._Object.setCaption(Me.colhStep9_businessname.ID, Me.colhStep9_businessname.Text)
            Language._Object.setCaption(Me.rfDependantbusinessname_step9.ID, Me.rfDependantbusinessname_step9.ErrorMessage)

            Language._Object.setCaption(Me.colhStep9_acquisition_date.ID, Me.colhStep9_acquisition_date.Text)

            Language._Object.setCaption(Me.colhStep9_market_value.ID, Me.colhStep9_market_value.Text)
            Language._Object.setCaption(Me.rfStep9_Dependantmarketvalue.ID, Me.rfStep9_Dependantmarketvalue.ErrorMessage)
            Language._Object.setCaption(Me.rfStep9_DependantmarketvalueCurrency.ID, Me.rfStep9_DependantmarketvalueCurrency.ErrorMessage)


            Language._Object.setCaption(Me.btnAddDependantShare_step9.ID, Me.btnAddDependantShare_step9.Text)
            Language._Object.setCaption(Me.btnUpdateDependantShare_step9.ID, Me.btnUpdateDependantShare_step9.Text)
            Language._Object.setCaption(Me.btnResetDependantShare_step9.ID, Me.btnResetDependantShare_step9.Text)

            
            Language._Object.setCaption(gvDependantShare.Columns(7).FooterText, gvDependantShare.Columns(7).HeaderText)
            Language._Object.setCaption("btnEdit_step9", gvDependantShare.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step9", gvDependantShare.Columns(1).HeaderText)

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 42, "Are you sure you want to delete selected dependant share?")
            'Sohail (07 Dec 2018) -- End

            'step 10
            Language._Object.setCaption(Me.lblDependantBankTitle_Step10.ID, Me.lblDependantBankTitle_Step10.Text)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Language._Object.setCaption(Me.colhStep10_RelationShip.ID, Me.colhStep10_RelationShip.Text)
            Language._Object.setCaption(Me.rfStep10_RelationShip.ID, Me.rfStep10_RelationShip.ErrorMessage)
            Language._Object.setCaption(Me.colhStep10_DependantName.ID, Me.colhStep10_DependantName.Text)
            Language._Object.setCaption(Me.rfStep10_DependantName.ID, Me.rfStep10_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End

            Language._Object.setCaption(Me.colhStep10_bankname.ID, Me.colhStep10_bankname.Text)
            Language._Object.setCaption(Me.rfStep10_DependantBankname.ID, Me.rfStep10_DependantBankname.ErrorMessage)

            Language._Object.setCaption(Me.colhStep10_accounttype.ID, Me.colhStep10_accounttype.Text)
            Language._Object.setCaption(Me.rfStep10_DependantAccounttype.ID, Me.rfStep10_DependantAccounttype.ErrorMessage)

            Language._Object.setCaption(Me.colhStep10_accountno.ID, Me.colhStep10_accountno.Text)
            Language._Object.setCaption(Me.rfStep10_DependantAccountno.ID, Me.rfStep10_DependantAccountno.ErrorMessage)

            Language._Object.setCaption(Me.colhStep10_Specify.ID, Me.colhStep10_Specify.Text)
            Language._Object.setCaption(Me.colhStep10_countryunkid.ID, Me.colhStep10_countryunkid.Text)

            Language._Object.setCaption(Me.rfStep10_DependantBankCurrency.ID, Me.rfStep10_DependantBankCurrency.ErrorMessage)

            Language._Object.setCaption(Me.btnAddDependantBankAccount_step10.ID, Me.btnAddDependantBankAccount_step10.Text)
            Language._Object.setCaption(Me.btnUpdateDependantBankAccount_step10.ID, Me.btnUpdateDependantBankAccount_step10.Text)
            Language._Object.setCaption(Me.btnResetDependantBankAccount_step10.ID, Me.btnResetDependantBankAccount_step10.Text)

           
            Language._Object.setCaption(gvDependantBank.Columns(5).FooterText, gvDependantBank.Columns(5).HeaderText)
            Language._Object.setCaption("btnEdit_step10", gvDependantBank.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step10", gvDependantBank.Columns(1).HeaderText)

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 36, "Are you sure you want to delete selected Dependant Bank Account?")
            'Hemant (07 Dec 2018) -- End

            'step 11
            Language._Object.setCaption(Me.lblDependantPropertiesTitle_Step11.ID, Me.lblDependantPropertiesTitle_Step11.Text)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Language._Object.setCaption(Me.colhStep11_RelationShip.ID, Me.colhStep11_RelationShip.Text)
            Language._Object.setCaption(Me.rfStep11_RelationShip.ID, Me.rfStep11_RelationShip.ErrorMessage)
            Language._Object.setCaption(Me.colhStep11_DependantName.ID, Me.colhStep11_DependantName.Text)
            Language._Object.setCaption(Me.rfStep11_DependantName.ID, Me.rfStep11_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End
            Language._Object.setCaption(Me.colhStep11_asset_name.ID, Me.colhStep11_asset_name.Text)
            Language._Object.setCaption(Me.rfStep11_DependantPropertiesAsset.ID, Me.rfStep11_DependantPropertiesAsset.ErrorMessage)

            Language._Object.setCaption(Me.colhStep11_location.ID, Me.colhStep11_location.Text)

            Language._Object.setCaption(Me.colhStep11_registrationtitleno.ID, Me.colhStep11_registrationtitleno.Text)
            Language._Object.setCaption(Me.colhStep11_estimated_value.ID, Me.colhStep11_estimated_value.Text)
            Language._Object.setCaption(Me.rfStep11_DependantPropertiesEstimatevalue.ID, Me.rfStep11_DependantPropertiesEstimatevalue.ErrorMessage)
            Language._Object.setCaption(Me.rfStep11_DependantPropertiesEstimatevalueCurrency.ID, Me.rfStep11_DependantPropertiesEstimatevalueCurrency.ErrorMessage)

            Language._Object.setCaption(Me.colhStep11_AcquisitionDateDepnProperties.ID, Me.colhStep11_AcquisitionDateDepnProperties.Text)


            Language._Object.setCaption(Me.btnAddDependantProperties_step11.ID, Me.btnAddDependantProperties_step11.Text)
            Language._Object.setCaption(Me.btnUpdateDependantProperties_step11.ID, Me.btnUpdateDependantProperties_step11.Text)
            Language._Object.setCaption(Me.btnResetDependantProperties_step11.ID, Me.btnResetDependantProperties_step11.Text)

          
            Language._Object.setCaption(gvDependantProperties.Columns(5).FooterText, gvDependantProperties.Columns(5).HeaderText)
            Language._Object.setCaption("btnEdit_step11", gvDependantProperties.Columns(0).HeaderText)
            Language._Object.setCaption("btnDelete_step11", gvDependantProperties.Columns(1).HeaderText)

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Language.setMessage(mstrModuleName1, 43, "Are you sure you want to delete selected dependant properties?")
            'Sohail (07 Dec 2018) -- End

            'step 12
            Language._Object.setCaption(Me.lblSighOffTitle_Step12.ID, Me.lblSighOffTitle_Step12.Text)
            Language._Object.setCaption(Me.txtHeader_Step12.ID, Me.txtHeader_Step12.Text)
            'Language._Object.setCaption(Me.txtPara1_Step12.ID, Me.txtPara1_Step12.Text)
            'Language._Object.setCaption(Me.txtPara2_Step12.ID, Me.txtPara2_Step12.Text)
            'Language._Object.setCaption(Me.txtPara3_Step12.ID, Me.txtPara3_Step12.Text)
            'Language._Object.setCaption(Me.txtPara4_Step12.ID, Me.txtPara4_Step12.Text)
            'Language._Object.setCaption(Me.txtSignatureLine_Step12.ID, Me.txtSignatureLine_Step12.Text)
            'Language._Object.setCaption(Me.txtDate_Step12.ID, Me.txtDate_Step12.Text)
            Language._Object.setCaption(Me.chkAcknowledgement_Step13.ID, Me.chkAcknowledgement_Step13.Text)

            Language._Object.setCaption(Me.lblExchangeRate.ID, Me.lblExchangeRate.Text)
            Language._Object.setCaption(Me.btnBack.ID, Me.btnBack.Text)

            Language._Object.setCaption(Me.btnNext.ID, Me.btnNext.Text)

            Language._Object.setCaption(Me.btnSaveClose.ID, Me.btnSaveClose.Text)

            Language._Object.setCaption(Me.btnCloseAddEdit.ID, Me.btnCloseAddEdit.Text)
            'Sohail (26 Feb 2020) -- Start
            'NMB Issue # : not able to rename the FINAL SAVE button to "Submit" from Language.
            Language._Object.setCaption(Me.btnFinalSave.ID, Me.btnFinalSave.Text)
            'Sohail (26 Feb 2020) -- End


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(lblPageHeader.ID, Me.Title)

            Me.lblDetialHeader.Text = Language._Object.getCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.chkIncludeClosedYearTrans.Text = Language._Object.getCaption(Me.chkIncludeClosedYearTrans.ID, Me.chkIncludeClosedYearTrans.Text)

            Me.BtnSearch.Text = Language._Object.getCaption(Me.BtnSearch.ID, Me.BtnSearch.Text).Replace("&", "")
            Me.BtnReset.Text = Language._Object.getCaption(Me.BtnReset.ID, Me.BtnReset.Text).Replace("&", "")

            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            Me.lblFinalcialYear.Text = Language._Object.getCaption(Me.lblFinalcialYear.ID, Me.lblFinalcialYear.Text)
            'Hemant (05 Dec 2018) -- End

            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            gvList.Columns(0).HeaderText = Language._Object.getCaption("btnEdit", gvList.Columns(0).HeaderText).Replace("&", "")
            gvList.Columns(1).HeaderText = Language._Object.getCaption("btnDelete", gvList.Columns(1).HeaderText).Replace("&", "")
            gvList.Columns(2).HeaderText = Language._Object.getCaption("mnuFinalSave", gvList.Columns(2).HeaderText)
            gvList.Columns(3).HeaderText = Language._Object.getCaption("mnuUnlockFinalSave", gvList.Columns(3).HeaderText)
            gvList.Columns(4).HeaderText = Language._Object.getCaption("mnuPrintDeclaration", gvList.Columns(4).HeaderText)
            gvList.Columns(5).HeaderText = Language._Object.getCaption(gvList.Columns(5).FooterText, gvList.Columns(5).HeaderText)
            gvList.Columns(6).HeaderText = Language._Object.getCaption(gvList.Columns(6).FooterText, gvList.Columns(6).HeaderText)
            gvList.Columns(7).HeaderText = Language._Object.getCaption(gvList.Columns(7).FooterText, gvList.Columns(7).HeaderText)
            gvList.Columns(8).HeaderText = Language._Object.getCaption(gvList.Columns(8).FooterText, gvList.Columns(8).HeaderText)
            gvList.Columns(9).HeaderText = Language._Object.getCaption(gvList.Columns(9).FooterText, gvList.Columns(9).HeaderText)

            Me.btnScanAttachDocuments.Text = Language._Object.getCaption(Me.btnScanAttachDocuments.ID, Me.btnScanAttachDocuments.Text).Replace("&", "")
            Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

            popupFinalYesNo.Message = Language.getMessage(mstrModuleName, 6, "After submission, you can not edit / delete declaration,Are you sure you want to sumbit this declaration?")
            popupFinalYesNo.Title = Language.getMessage(mstrModuleName, 19, "Are You Sure you want to final save this declaration?")

            popupUnlockFinalYesNo.Message = Language.getMessage(mstrModuleName, 8, "Are you sure you want to unlock final save declaration?")
            popupUnlockFinalYesNo.Title = Language.getMessage(mstrModuleName, 9, "Unlock Final Save")

            popup1.Title = Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this declaration?")

           

            Language.setLanguage(mstrModuleName1)

            'step1 
            Me.lnkmenu_Instruction.Text = Language._Object.getCaption(lnkmenu_Instruction.ID, Me.lnkmenu_Instruction.Text)
            Me.lnkmenu_BusinessDealings.Text = Language._Object.getCaption(lnkmenu_BusinessDealings.ID, Me.lnkmenu_BusinessDealings.Text)
            Me.lnkmenu_StaffOwnership.Text = Language._Object.getCaption(lnkmenu_StaffOwnership.ID, Me.lnkmenu_StaffOwnership.Text)
            Me.lnkmenu_BankAccounts.Text = Language._Object.getCaption(lnkmenu_BankAccounts.ID, Me.lnkmenu_BankAccounts.Text)
            Me.lnkmenu_Realproperties.Text = Language._Object.getCaption(lnkmenu_Realproperties.ID, Me.lnkmenu_Realproperties.Text)
            Me.lnkmenu_Liabilities.Text = Language._Object.getCaption(lnkmenu_Liabilities.ID, Me.lnkmenu_Liabilities.Text)
            Me.lnkmenu_Relatives.Text = Language._Object.getCaption(lnkmenu_Relatives.ID, Me.lnkmenu_Relatives.Text)
            Me.lnkmenu_DependantBusiness.Text = Language._Object.getCaption(lnkmenu_DependantBusiness.ID, Me.lnkmenu_DependantBusiness.Text)
            Me.lnkmenu_DependantShares.Text = Language._Object.getCaption(lnkmenu_DependantShares.ID, Me.lnkmenu_DependantShares.Text)
            Me.lnkmenu_DependantBank.Text = Language._Object.getCaption(lnkmenu_DependantBank.ID, Me.lnkmenu_DependantBank.Text)
            Me.lnkmenu_DependantProperties.Text = Language._Object.getCaption(lnkmenu_DependantProperties.ID, Me.lnkmenu_DependantProperties.Text)
            Me.lnkmenu_SignOff.Text = Language._Object.getCaption(lnkmenu_SignOff.ID, Me.lnkmenu_SignOff.Text)

            Me.lblInstruction.Text = Language._Object.getCaption(lblInstruction.ID, Me.lblInstruction.Text)
            Me.gbEmployee.Text = Language._Object.getCaption(gbEmployee.ID, Me.gbEmployee.Text)
            'Hemant (05 Dec 2018) -- Start
            'NMB Enhancement : Changes for adding Financial Year in  Asset Declaration Template2
            Me.lblStep1_FinancialYear.Text = Language._Object.getCaption(lblStep1_FinancialYear.ID, Me.lblStep1_FinancialYear.Text)
            'Hemant (05 Dec 2018) -- End
            Me.lblStep1_Employee.Text = Language._Object.getCaption(lblStep1_Employee.ID, Me.lblStep1_Employee.Text)
            Me.lblStep1_EmployeeError.Text = Language._Object.getCaption(lblStep1_EmployeeError.ID, Me.lblStep1_EmployeeError.Text)
            Me.lblStep1_Date.Text = Language._Object.getCaption(lblStep1_Date.ID, Me.lblStep1_Date.Text)

            Me.lblStep1_EmployeeCode.Text = Language._Object.getCaption(lblStep1_EmployeeCode.ID, Me.lblStep1_EmployeeCode.Text)
            Me.lblStep1_OfficePosition.Text = Language._Object.getCaption(lblStep1_OfficePosition.ID, Me.lblStep1_OfficePosition.Text)
            Me.lblStep1_CenterforWork.Text = Language._Object.getCaption(lblStep1_CenterforWork.ID, Me.lblStep1_CenterforWork.Text)
            Me.lblStep1_Departments.Text = Language._Object.getCaption(lblStep1_Departments.ID, Me.lblStep1_Departments.Text)
            Me.lblStep1_WorkStation.Text = Language._Object.getCaption(Me.lblStep1_WorkStation.ID, Me.lblStep1_WorkStation.Text)
            Me.lblStep1_Tin_No.Text = Language._Object.getCaption(Me.lblStep1_Tin_No.ID, Me.lblStep1_Tin_No.Text)
           
            popupYesNo.Message = Language.getMessage(mstrModuleName1, 20, "Are You Sure you want to save this declaration?")
            popupYesNo.Title = Language.getMessage(mstrModuleName1, 21, "Confirmation")

            'step2 
            lblBusinessDealings_Step2.Text = Language._Object.getCaption(Me.lblBusinessDealings_Step2.ID, Me.lblBusinessDealings_Step2.Text)

            colhStep2_Sector.Text = Language._Object.getCaption(Me.colhStep2_Sector.ID, Me.colhStep2_Sector.Text)
            Me.RfStep2_Sector.ErrorMessage = Language._Object.getCaption(Me.RfStep2_Sector.ID, Me.RfStep2_Sector.ErrorMessage)

            colhStep2_Company_Name.Text = Language._Object.getCaption(Me.colhStep2_Company_Name.ID, Me.colhStep2_Company_Name.Text)
            Me.RfStep2_Company_NameError.ErrorMessage = Language._Object.getCaption(Me.RfStep2_Company_NameError.ID, Me.RfStep2_Company_NameError.ErrorMessage)

            Me.colhStep2_company_tin_no.Text = Language._Object.getCaption(Me.colhStep2_company_tin_no.ID, Me.colhStep2_company_tin_no.Text)
            Me.rfStep2_CompanyTinNo.ErrorMessage = Language._Object.getCaption(Me.rfStep2_CompanyTinNo.ID, Me.rfStep2_CompanyTinNo.ErrorMessage)

            Me.colhStep2_registration_date.Text = Language._Object.getCaption(Me.colhStep2_registration_date.ID, Me.colhStep2_registration_date.Text)

            Me.colhStep2_address_location.Text = Language._Object.getCaption(Me.colhStep2_address_location.ID, Me.colhStep2_address_location.Text)
            Me.rfStep2_Addresslocation.ErrorMessage = Language._Object.getCaption(Me.rfStep2_Addresslocation.ID, Me.rfStep2_Addresslocation.ErrorMessage)

            Me.colhStep2_business_type.Text = Language._Object.getCaption(Me.colhStep2_business_type.ID, Me.colhStep2_business_type.Text)
            Me.rFStep2_BusinessType.ErrorMessage = Language._Object.getCaption(Me.rFStep2_BusinessType.ID, Me.rFStep2_BusinessType.ErrorMessage)

            Me.colhStep2_Position_held.Text = Language._Object.getCaption(Me.colhStep2_Position_held.ID, Me.colhStep2_Position_held.Text)
            Me.rfStep2_PositionHeld.ErrorMessage = Language._Object.getCaption(Me.rfStep2_PositionHeld.ID, Me.rfStep2_PositionHeld.ErrorMessage)

            Me.colhStep2_Is_supplier_client.Text = Language._Object.getCaption(Me.colhStep2_Is_supplier_client.ID, Me.colhStep2_Is_supplier_client.Text)
            Me.rfStep2_Is_supplier_client.ErrorMessage = Language._Object.getCaption(Me.rfStep2_Is_supplier_client.ID, Me.rfStep2_Is_supplier_client.ErrorMessage)

            Me.colhStep2_Monthly_annual_earnings.Text = Language._Object.getCaption(Me.colhStep2_Monthly_annual_earnings.ID, Me.colhStep2_Monthly_annual_earnings.Text)
            Me.rfStep2_MonthlyAnnualEarnings.ErrorMessage = Language._Object.getCaption(Me.rfStep2_MonthlyAnnualEarnings.ID, Me.rfStep2_MonthlyAnnualEarnings.ErrorMessage)

            Me.rfStep2_CurrencyBusiness.ErrorMessage = Language._Object.getCaption(Me.rfStep2_CurrencyBusiness.ID, Me.rfStep2_CurrencyBusiness.ErrorMessage)


            colhStep2_Business_contact_no.Text = Language._Object.getCaption(Me.colhStep2_Business_contact_no.ID, Me.colhStep2_Business_contact_no.Text)
            Me.rfStep2_BusinessContactNo.ErrorMessage = Language._Object.getCaption(Me.rfStep2_BusinessContactNo.ID, Me.rfStep2_BusinessContactNo.ErrorMessage)

            Me.colhStep2_Shareholders_names.Text = Language._Object.getCaption(Me.colhStep2_Shareholders_names.ID, Me.colhStep2_Shareholders_names.Text)
            Me.rfStep2_ShareholdersNames.ErrorMessage = Language._Object.getCaption(Me.rfStep2_ShareholdersNames.ID, Me.rfStep2_ShareholdersNames.ErrorMessage)

            BtnAddBusiness.Text = Language._Object.getCaption(Me.BtnAddBusiness.ID, Me.BtnAddBusiness.Text)
            BtnUpdateBusiness.Text = Language._Object.getCaption(Me.BtnUpdateBusiness.ID, Me.BtnUpdateBusiness.Text)
            btnResetBusiness.Text = Language._Object.getCaption(Me.btnResetBusiness.ID, Me.btnResetBusiness.Text)



            gvBusiness.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step2", gvBusiness.Columns(0).HeaderText).Replace("&", "")
            gvBusiness.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step2", gvBusiness.Columns(1).HeaderText).Replace("&", "")
            gvBusiness.Columns(2).HeaderText = Language._Object.getCaption(gvBusiness.Columns(2).FooterText, gvBusiness.Columns(2).HeaderText)
            gvBusiness.Columns(3).HeaderText = Language._Object.getCaption(gvBusiness.Columns(3).FooterText, gvBusiness.Columns(3).HeaderText)
            gvBusiness.Columns(4).HeaderText = Language._Object.getCaption(gvBusiness.Columns(4).FooterText, gvBusiness.Columns(4).HeaderText)
            gvBusiness.Columns(5).HeaderText = Language._Object.getCaption(gvBusiness.Columns(5).FooterText, gvBusiness.Columns(5).HeaderText)
            gvBusiness.Columns(6).HeaderText = Language._Object.getCaption(gvBusiness.Columns(6).FooterText, gvBusiness.Columns(6).HeaderText)
            gvBusiness.Columns(7).HeaderText = Language._Object.getCaption(gvBusiness.Columns(7).FooterText, gvBusiness.Columns(7).HeaderText)
            gvBusiness.Columns(8).HeaderText = Language._Object.getCaption(gvBusiness.Columns(8).FooterText, gvBusiness.Columns(8).HeaderText)
            gvBusiness.Columns(9).HeaderText = Language._Object.getCaption(gvBusiness.Columns(9).FooterText, gvBusiness.Columns(9).HeaderText)
            gvBusiness.Columns(10).HeaderText = Language._Object.getCaption(gvBusiness.Columns(10).FooterText, gvBusiness.Columns(10).HeaderText)
            gvBusiness.Columns(11).HeaderText = Language._Object.getCaption(gvBusiness.Columns(11).FooterText, gvBusiness.Columns(11).HeaderText)
            gvBusiness.Columns(12).HeaderText = Language._Object.getCaption(gvBusiness.Columns(12).FooterText, gvBusiness.Columns(12).HeaderText)
            gvBusiness.Columns(13).HeaderText = Language._Object.getCaption(gvBusiness.Columns(13).FooterText, gvBusiness.Columns(13).HeaderText)


            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteBusinessDeal.Title = Language.getMessage(mstrModuleName1, 30, "Are you sure you want to delete selected business deal?")
            'Gajanan (07 Dec 2018) -- End


            'Step 3
            Me.lblStaffOwnerShip_step3.Text = Language._Object.getCaption(Me.lblStaffOwnerShip_step3.ID, Me.lblStaffOwnerShip_step3.Text)

            Me.colhStep3_Securitiestype.Text = Language._Object.getCaption(Me.colhStep3_Securitiestype.ID, Me.colhStep3_Securitiestype.Text)
            Me.rfStep3_Securitiestype.ErrorMessage = Language._Object.getCaption(Me.rfStep3_Securitiestype.ID, Me.rfStep3_Securitiestype.ErrorMessage)

            Me.colhStep3_Certificate_no.Text = Language._Object.getCaption(Me.colhStep3_Certificate_no.ID, Me.colhStep3_Certificate_no.Text)
            Me.rfStep3_Certificate_no.ErrorMessage = Language._Object.getCaption(Me.rfStep3_Certificate_no.ID, Me.rfStep3_Certificate_no.ErrorMessage)

            Me.colhStep3_No_of_shares_step3.Text = Language._Object.getCaption(Me.colhStep3_No_of_shares_step3.ID, Me.colhStep3_No_of_shares_step3.Text)
            Me.rfStep3_No_of_shares.ErrorMessage = Language._Object.getCaption(Me.rfStep3_No_of_shares.ID, Me.rfStep3_No_of_shares.ErrorMessage)

            Me.colhStep3_Company_business_name.Text = Language._Object.getCaption(Me.colhStep3_Company_business_name.ID, Me.colhStep3_Company_business_name.Text)
            Me.rfStep3_Company_business_name.ErrorMessage = Language._Object.getCaption(Me.rfStep3_Company_business_name.ID, Me.rfStep3_Company_business_name.ErrorMessage)

            Me.colhStep3_Marketvalue_step3.Text = Language._Object.getCaption(Me.colhStep3_Marketvalue_step3.ID, Me.colhStep3_Marketvalue_step3.Text)
            Me.rfStep3_Marketvalue.ErrorMessage = Language._Object.getCaption(Me.rfStep3_Marketvalue.ID, Me.rfStep3_Marketvalue.Text)

            Me.rfStep3_CurrencyShare.ErrorMessage = Language._Object.getCaption(Me.rfStep3_CurrencyShare.ID, Me.rfStep3_CurrencyShare.ErrorMessage)
            colhStep3_Acquisition_date.Text = Language._Object.getCaption(Me.colhStep3_Acquisition_date.ID, Me.colhStep3_Acquisition_date.Text)

            btnAddStaffOwnerShip.Text = Language._Object.getCaption(Me.btnAddStaffOwnerShip.ID, Me.btnAddStaffOwnerShip.Text).Replace("&", "")
            btnUpdateStaffOwnerShip.Text = Language._Object.getCaption(Me.btnUpdateStaffOwnerShip.ID, Me.btnUpdateStaffOwnerShip.Text).Replace("&", "")
            btnResetStaffOwnerShip.Text = Language._Object.getCaption(Me.btnResetStaffOwnerShip.ID, Me.btnResetStaffOwnerShip.Text).Replace("&", "")

            gvStaffOwnerShip.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step3", gvStaffOwnerShip.Columns(0).HeaderText).Replace("&", "")
            gvStaffOwnerShip.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step3", gvStaffOwnerShip.Columns(1).HeaderText).Replace("&", "")
            gvStaffOwnerShip.Columns(2).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(2).FooterText, gvStaffOwnerShip.Columns(2).HeaderText)
            gvStaffOwnerShip.Columns(3).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(3).FooterText, gvStaffOwnerShip.Columns(3).HeaderText)
            gvStaffOwnerShip.Columns(4).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(4).FooterText, gvStaffOwnerShip.Columns(4).HeaderText)
            gvStaffOwnerShip.Columns(5).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(5).FooterText, gvStaffOwnerShip.Columns(5).HeaderText)
            gvStaffOwnerShip.Columns(6).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(6).FooterText, gvStaffOwnerShip.Columns(6).HeaderText)
            gvStaffOwnerShip.Columns(7).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(7).FooterText, gvStaffOwnerShip.Columns(7).HeaderText)
            gvStaffOwnerShip.Columns(8).HeaderText = Language._Object.getCaption(gvStaffOwnerShip.Columns(8).FooterText, gvStaffOwnerShip.Columns(8).HeaderText)


            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteStaffOwnership.Title = Language.getMessage(mstrModuleName1, 31, "Are you sure you want to delete selected staff Ownership?")
            'Gajanan (07 Dec 2018) -- End


            'Step4
            Me.lblBankAccounts_step4.Text = Language._Object.getCaption(Me.lblBankAccounts_step4.ID, Me.lblBankAccounts_step4.Text)

            Me.colhStep4_BankName.Text = Language._Object.getCaption(Me.colhStep4_BankName.ID, Me.colhStep4_BankName.Text)
            Me.rfStep4_BankName.Text = Language._Object.getCaption(Me.rfStep4_BankName.ID, Me.rfStep4_BankName.Text)

            Me.colhStep4_AccountTypeBank.Text = Language._Object.getCaption(Me.colhStep4_AccountTypeBank.ID, Me.colhStep4_AccountTypeBank.Text)
            Me.rfStep4_AccountTypeBankAccounts.ErrorMessage = Language._Object.getCaption(Me.rfStep4_AccountTypeBankAccounts.ID, Me.rfStep4_AccountTypeBankAccounts.ErrorMessage)

            Me.colhStep4_AccountNo.Text = Language._Object.getCaption(Me.colhStep4_AccountNo.ID, Me.colhStep4_AccountNo.Text)
            Me.rfStep4_AccountNoBank.ErrorMessage = Language._Object.getCaption(Me.rfStep4_AccountNoBank.ID, Me.rfStep4_AccountNoBank.ErrorMessage)

            Me.colhStep4_DepositsSourceBank.Text = Language._Object.getCaption(Me.colhStep4_DepositsSourceBank.ID, Me.colhStep4_DepositsSourceBank.Text)
            Me.rfStep4_DepositsSourceBankAccounts.ErrorMessage = Language._Object.getCaption(Me.rfStep4_DepositsSourceBankAccounts.ID, Me.rfStep4_DepositsSourceBankAccounts.ErrorMessage)

            Me.colhStep4_Specify.Text = Language._Object.getCaption(Me.colhStep4_Specify.ID, Me.colhStep4_Specify.Text)

            Me.rfStep4_CurrencyBank.ErrorMessage = Language._Object.getCaption(Me.rfStep4_CurrencyBank.ID, Me.rfStep4_CurrencyBank.ErrorMessage)

            Me.btnAddBankAccounts.Text = Language._Object.getCaption(Me.btnAddBankAccounts.ID, Me.btnAddBankAccounts.Text)
            Me.btnUpdateBankAccounts.Text = Language._Object.getCaption(Me.btnUpdateBankAccounts.ID, Me.btnUpdateBankAccounts.Text)
            Me.btnResetBankAccounts.Text = Language._Object.getCaption(Me.btnResetBankAccounts.ID, Me.btnResetBankAccounts.Text)

            gvBankAccounts.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step4", gvBankAccounts.Columns(0).HeaderText).Replace("&", "")
            gvBankAccounts.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step4", gvBankAccounts.Columns(1).HeaderText).Replace("&", "")
            gvBankAccounts.Columns(2).HeaderText = Language._Object.getCaption(gvBankAccounts.Columns(2).FooterText, gvBankAccounts.Columns(2).HeaderText)
            gvBankAccounts.Columns(3).HeaderText = Language._Object.getCaption(gvBankAccounts.Columns(3).FooterText, gvBankAccounts.Columns(3).HeaderText)
            gvBankAccounts.Columns(4).HeaderText = Language._Object.getCaption(gvBankAccounts.Columns(4).FooterText, gvBankAccounts.Columns(4).HeaderText)
            gvBankAccounts.Columns(5).HeaderText = Language._Object.getCaption(gvBankAccounts.Columns(5).FooterText, gvBankAccounts.Columns(5).HeaderText)
            gvBankAccounts.Columns(6).HeaderText = Language._Object.getCaption(gvBankAccounts.Columns(6).FooterText, gvBankAccounts.Columns(6).HeaderText)
            gvBankAccounts.Columns(7).HeaderText = Language._Object.getCaption(gvBankAccounts.Columns(5).FooterText, gvBankAccounts.Columns(5).HeaderText)


            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteBankAccount.Title = Language.getMessage(mstrModuleName1, 32, "Are you sure you want to delete selected bank account?")
            'Gajanan (07 Dec 2018) -- End


            'Step5

            Me.lblRealproperties_step5.Text = Language._Object.getCaption(Me.lblRealproperties_step5.ID, Me.lblRealproperties_step5.Text)
            Me.colhStep5_AssetNameProperties.Text = Language._Object.getCaption(Me.colhStep5_AssetNameProperties.ID, Me.colhStep5_AssetNameProperties.Text)
            Me.rfStep5_AssetName.ErrorMessage = Language._Object.getCaption(Me.rfStep5_AssetName.ID, Me.rfStep5_AssetName.ErrorMessage)

            Me.colhStep5_LocationProperties.Text = Language._Object.getCaption(Me.colhStep5_LocationProperties.ID, Me.colhStep5_LocationProperties.Text)

            Me.colhStep5_RegistrationTitleNoProperties.Text = Language._Object.getCaption(Me.colhStep5_RegistrationTitleNoProperties.ID, Me.colhStep5_LocationProperties.Text)

            Me.colhStep5_EstimatedValueProperties.Text = Language._Object.getCaption(Me.colhStep5_EstimatedValueProperties.ID, Me.colhStep5_EstimatedValueProperties.Text)
            Me.rfStep5_EstimatedValueRealProperties.ErrorMessage = Language._Object.getCaption(Me.rfStep5_EstimatedValueRealProperties.ID, Me.rfStep5_EstimatedValueRealProperties.ErrorMessage)

            Me.rfStep5_CurrencyProperties.ErrorMessage = Language._Object.getCaption(Me.rfStep5_CurrencyProperties.ID, Me.rfStep5_CurrencyProperties.ErrorMessage)

            Me.colhStep5_FundsSourceProperties.Text = Language._Object.getCaption(Me.colhStep5_FundsSourceProperties.ID, Me.colhStep5_FundsSourceProperties.Text)
            Me.rfStep5_FundsSourceRealProperties.ErrorMessage = Language._Object.getCaption(Me.rfStep5_FundsSourceRealProperties.ID, Me.rfStep5_FundsSourceRealProperties.ErrorMessage)

            colhStep5_AssetLocationProperties.Text = Language._Object.getCaption(Me.colhStep5_AssetLocationProperties.ID, Me.colhStep5_AssetLocationProperties.Text)
            colhStep5_AcquisitionDateProperties.Text = Language._Object.getCaption(Me.colhStep5_AcquisitionDateProperties.ID, Me.colhStep5_AcquisitionDateProperties.Text)

            Me.colhStep5_AssetUseProperties.Text = Language._Object.getCaption(Me.colhStep5_AssetUseProperties.ID, Me.colhStep5_AssetUseProperties.Text)
            Me.rfStep5_AssetUseRealProperties.ErrorMessage = Language._Object.getCaption(Me.rfStep5_AssetUseRealProperties.ID, Me.rfStep5_AssetUseRealProperties.ErrorMessage)

            Me.btnAddRealProperties.Text = Language._Object.getCaption(Me.btnAddRealProperties.ID, Me.btnAddRealProperties.Text).Replace("&", "")
            Me.btnUpdateRealProperties.Text = Language._Object.getCaption(Me.btnUpdateRealProperties.ID, Me.btnUpdateRealProperties.Text).Replace("&", "")
            Me.btnResetRealProperties.Text = Language._Object.getCaption(Me.btnResetRealProperties.ID, Me.btnResetRealProperties.Text).Replace("&", "")

            gvRealProperties.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step5", gvRealProperties.Columns(0).HeaderText).Replace("&", "")
            gvRealProperties.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step5", gvRealProperties.Columns(1).HeaderText).Replace("&", "")
            gvRealProperties.Columns(2).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(2).FooterText, gvRealProperties.Columns(2).HeaderText)
            gvRealProperties.Columns(3).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(3).FooterText, gvRealProperties.Columns(3).HeaderText)
            gvRealProperties.Columns(4).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(4).FooterText, gvRealProperties.Columns(4).HeaderText)
            gvRealProperties.Columns(5).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(5).FooterText, gvRealProperties.Columns(5).HeaderText)
            gvRealProperties.Columns(6).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(6).FooterText, gvRealProperties.Columns(6).HeaderText)
            gvRealProperties.Columns(7).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(7).FooterText, gvRealProperties.Columns(7).HeaderText)
            gvRealProperties.Columns(8).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(8).FooterText, gvRealProperties.Columns(8).HeaderText)
            gvRealProperties.Columns(9).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(9).FooterText, gvRealProperties.Columns(9).HeaderText)
            gvRealProperties.Columns(10).HeaderText = Language._Object.getCaption(gvRealProperties.Columns(10).FooterText, gvRealProperties.Columns(10).HeaderText)

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteProperties.Title = Language.getMessage(mstrModuleName1, 33, "Are you sure you want to delete selected properties?")
            'Gajanan (07 Dec 2018) -- End

            'Step 6


            Me.lblStep6_Liabilities.Text = Language._Object.getCaption(Me.lblStep6_Liabilities.ID, Me.lblStep6_Liabilities.Text)

            Me.colhStep6_InstitutionNameLiabilities.Text = Language._Object.getCaption(Me.colhStep6_InstitutionNameLiabilities.ID, Me.colhStep6_InstitutionNameLiabilities.Text)
            Me.rfStep6_InstitutionName.ErrorMessage = Language._Object.getCaption(Me.rfStep6_InstitutionName.ID, Me.rfStep6_InstitutionName.ErrorMessage)

            Me.colhStep6_LiabilityTypeLiabilities.Text = Language._Object.getCaption(Me.colhStep6_LiabilityTypeLiabilities.ID, Me.colhStep6_LiabilityTypeLiabilities.Text)
            Me.rfStep6_LiabilityTypeLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_LiabilityTypeLiabilities.ID, Me.rfStep6_LiabilityTypeLiabilities.ErrorMessage)

            Me.colhStep6_OriginalBalanceLiabilities.Text = Language._Object.getCaption(Me.colhStep6_OriginalBalanceLiabilities.ID, Me.colhStep6_OriginalBalanceLiabilities.Text)
            Me.rfStep6_OriginalBalanceLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_OriginalBalanceLiabilities.ID, Me.rfStep6_OriginalBalanceLiabilities.ErrorMessage)
            Me.rfStep6_OrigCurrencyLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_OrigCurrencyLiabilities.ID, Me.rfStep6_OrigCurrencyLiabilities.ErrorMessage)



            Me.colhStep6_OutstandingBalanceLiabilities.Text = Language._Object.getCaption(Me.colhStep6_OutstandingBalanceLiabilities.ID, Me.colhStep6_OutstandingBalanceLiabilities.Text)
            Me.rfStep6_OutstandingBalanceLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_OutstandingBalanceLiabilities.ID, Me.rfStep6_OutstandingBalanceLiabilities.ErrorMessage)
            Me.rfStep6_OutCurrencyLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_OutCurrencyLiabilities.ID, Me.rfStep6_OutCurrencyLiabilities.ErrorMessage)



            Me.colhStep6_DurationLiabilities.Text = Language._Object.getCaption(Me.colhStep6_DurationLiabilities.ID, Me.colhStep6_DurationLiabilities.Text)
            Me.rfStep6_Duration.ErrorMessage = Language._Object.getCaption(Me.rfStep6_Duration.ID, Me.rfStep6_Duration.ErrorMessage)


            Me.colhStep6_LoanPurposeLiabilities.Text = Language._Object.getCaption(Me.colhStep6_LoanPurposeLiabilities.ID, Me.colhStep6_LoanPurposeLiabilities.Text)
            Me.rfStep6_LoanPurposeLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_LoanPurposeLiabilities.ID, Me.rfStep6_LoanPurposeLiabilities.ErrorMessage)

            Me.colhStep6_Liability_dateLiabilities.Text = Language._Object.getCaption(Me.colhStep6_Liability_dateLiabilities.ID, Me.colhStep6_Liability_dateLiabilities.Text)

            Me.colhStep6_Payoff_dateLiabilities.Text = Language._Object.getCaption(Me.colhStep6_Payoff_dateLiabilities.ID, Me.colhStep6_Payoff_dateLiabilities.Text)


            Me.colhStep6_Remark_step6Liabilities.Text = Language._Object.getCaption(Me.colhStep6_Remark_step6Liabilities.ID, Me.colhStep6_Remark_step6Liabilities.Text)
            Me.rfStep6_RemarkLiabilities.ErrorMessage = Language._Object.getCaption(Me.rfStep6_RemarkLiabilities.ID, Me.rfStep6_RemarkLiabilities.ErrorMessage)


            Me.btnAddLiabilities.Text = Language._Object.getCaption(Me.btnAddLiabilities.ID, Me.btnAddLiabilities.Text).Replace("&", "")
            Me.btnUpdateLiabilities.Text = Language._Object.getCaption(Me.btnUpdateLiabilities.ID, Me.btnUpdateLiabilities.Text).Replace("&", "")
            Me.btnResetLiabilities.Text = Language._Object.getCaption(Me.btnResetLiabilities.ID, Me.btnResetLiabilities.Text).Replace("&", "")

            gvLiabilities.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step6", gvLiabilities.Columns(0).HeaderText).Replace("&", "")
            gvLiabilities.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step6", gvLiabilities.Columns(1).HeaderText).Replace("&", "")
            gvLiabilities.Columns(2).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(2).FooterText, gvLiabilities.Columns(2).HeaderText)
            gvLiabilities.Columns(3).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(3).FooterText, gvLiabilities.Columns(3).HeaderText)
            gvLiabilities.Columns(4).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(4).FooterText, gvLiabilities.Columns(4).HeaderText)
            gvLiabilities.Columns(5).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(5).FooterText, gvLiabilities.Columns(5).HeaderText)
            gvLiabilities.Columns(6).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(6).FooterText, gvLiabilities.Columns(6).HeaderText)
            gvLiabilities.Columns(7).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(7).FooterText, gvLiabilities.Columns(7).HeaderText)
            gvLiabilities.Columns(8).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(8).FooterText, gvLiabilities.Columns(8).HeaderText)
            gvLiabilities.Columns(9).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(9).FooterText, gvLiabilities.Columns(9).HeaderText)
            gvLiabilities.Columns(10).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(10).FooterText, gvLiabilities.Columns(10).HeaderText)
            gvLiabilities.Columns(11).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(11).FooterText, gvLiabilities.Columns(11).HeaderText)
            gvLiabilities.Columns(12).HeaderText = Language._Object.getCaption(gvLiabilities.Columns(12).FooterText, gvLiabilities.Columns(12).HeaderText)

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteLiabilities.Title = Language.getMessage(mstrModuleName1, 41, "Are you sure you want to delete selected liabilities?")
            'Sohail (07 Dec 2018) -- End

            'Step 7

            Me.lblRelativesInEmployment_Step7.Text = Language._Object.getCaption(Me.lblRelativesInEmployment_Step7.ID, Me.lblRelativesInEmployment_Step7.Text)

            Me.colhStep7_RelativeEmployeeRelatives.Text = Language._Object.getCaption(Me.colhStep7_RelativeEmployeeRelatives.ID, Me.colhStep7_RelativeEmployeeRelatives.Text)
            Me.rfStep7_RelativeEmployee.ErrorMessage = Language._Object.getCaption(Me.rfStep7_RelativeEmployee.ID, Me.rfStep7_RelativeEmployee.ErrorMessage)

            Me.colhStep7_relationshipRelatives.Text = Language._Object.getCaption(colhStep7_relationshipRelatives.ID, Me.colhStep7_relationshipRelatives.Text)
            Me.rfStep7_Relationship.ErrorMessage = Language._Object.getCaption(Me.rfStep7_Relationship.ID, Me.rfStep7_Relationship.ErrorMessage)

            Me.chkOthers.Text = Language._Object.getCaption(chkOthers.ID, Me.chkOthers.Text)

            Me.txtRelativeEmployee.Text = Language._Object.getCaption(txtRelativeEmployee.ID, Me.txtRelativeEmployee.Text)
            Me.rftxtRelativeEmployee.ErrorMessage = Language._Object.getCaption(Me.rftxtRelativeEmployee.ID, Me.rftxtRelativeEmployee.ErrorMessage)


            Me.btnAddRelatives.Text = Language._Object.getCaption(btnAddRelatives.ID, Me.btnAddRelatives.Text).Replace("&", "")
            Me.btnUpdateRelatives.Text = Language._Object.getCaption(btnUpdateRelatives.ID, Me.btnUpdateRelatives.Text).Replace("&", "")
            Me.btnResetRelatives.Text = Language._Object.getCaption(btnResetRelatives.ID, Me.btnResetRelatives.Text).Replace("&", "")

            gvRelatives.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step7", gvRelatives.Columns(0).HeaderText).Replace("&", "")
            gvRelatives.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step7", gvRelatives.Columns(1).HeaderText).Replace("&", "")
            gvRelatives.Columns(2).HeaderText = Language._Object.getCaption(gvRelatives.Columns(2).FooterText, gvRelatives.Columns(2).HeaderText)
            gvRelatives.Columns(3).HeaderText = Language._Object.getCaption(gvRelatives.Columns(3).FooterText, gvRelatives.Columns(3).HeaderText)

            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteRelatives.Title = Language.getMessage(mstrModuleName1, 34, "Are you sure you want to delete selected Relative?")
            'Hemant (07 Dec 2018) -- End

            'Step 8
            Me.lblDependantBusinessTitle_Step8.Text = Language._Object.getCaption(Me.lblDependantBusinessTitle_Step8.ID, Me.lblDependantBusinessTitle_Step8.Text)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Me.colhStep8_RelationShip.Text = Language._Object.getCaption(Me.colhStep8_RelationShip.ID, Me.colhStep8_RelationShip.Text)
            Me.rfStep8_RelationShip.ErrorMessage = Language._Object.getCaption(Me.rfStep8_RelationShip.ID, Me.rfStep8_RelationShip.ErrorMessage)
            Me.colhStep8_DependantName.Text = Language._Object.getCaption(Me.colhStep8_DependantName.ID, Me.colhStep8_DependantName.Text)
            Me.rfStep8_DependantName.ErrorMessage = Language._Object.getCaption(Me.rfStep8_DependantName.ID, Me.rfStep8_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End
            Me.colhStep8_Sector.Text = Language._Object.getCaption(Me.colhStep8_Sector.ID, Me.colhStep8_Sector.Text)
            Me.rfStep8_Sector.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Sector.ID, Me.rfStep8_Sector.ErrorMessage)

            Me.colhStep8_CompanyName.Text = Language._Object.getCaption(Me.colhStep8_CompanyName.ID, Me.colhStep8_CompanyName.Text)
            Me.rfStep8_Companyname.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Companyname.ID, Me.rfStep8_Companyname.ErrorMessage)
            Me.colhStep8_CompanyTinNo.Text = Language._Object.getCaption(Me.colhStep8_CompanyTinNo.ID, Me.colhStep8_CompanyTinNo.Text)
            Me.rfStep8_Companytinno.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Companytinno.ID, Me.rfStep8_Companytinno.ErrorMessage)
            Me.colhStep8_RegDate.Text = Language._Object.getCaption(Me.colhStep8_RegDate.ID, Me.colhStep8_RegDate.Text)
            Me.colhStep8_BusinessType.Text = Language._Object.getCaption(Me.colhStep8_BusinessType.ID, Me.colhStep8_BusinessType.Text)
            Me.rfStep8_BussinessType.ErrorMessage = Language._Object.getCaption(Me.rfStep8_BussinessType.ID, Me.rfStep8_BussinessType.ErrorMessage)
            Me.colhStep8_position_held.Text = Language._Object.getCaption(Me.colhStep8_position_held.ID, Me.colhStep8_position_held.Text)
            Me.rfStep8_Position.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Position.ID, Me.rfStep8_Position.ErrorMessage)
            Me.colhStep8_monthlyearning.Text = Language._Object.getCaption(Me.colhStep8_monthlyearning.ID, Me.colhStep8_monthlyearning.Text)
            Me.rfStep8_MonthlyEarning.ErrorMessage = Language._Object.getCaption(Me.rfStep8_MonthlyEarning.ID, Me.rfStep8_MonthlyEarning.ErrorMessage)
            Me.rfStep8_CurrencyMonthlyEarning.ErrorMessage = Language._Object.getCaption(Me.rfStep8_CurrencyMonthlyEarning.ID, Me.rfStep8_CurrencyMonthlyEarning.ErrorMessage)

            Me.colhStep8_is_supplier_client.Text = Language._Object.getCaption(Me.colhStep8_is_supplier_client.ID, Me.colhStep8_is_supplier_client.Text)
            Me.rfStep8_SupplierClient.ErrorMessage = Language._Object.getCaption(Me.rfStep8_SupplierClient.ID, Me.rfStep8_SupplierClient.ErrorMessage)

            Me.colhStep8_contactno.Text = Language._Object.getCaption(Me.colhStep8_contactno.ID, Me.colhStep8_contactno.Text)
            Me.rfStep8_Contactno.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Contactno.ID, Me.rfStep8_Contactno.ErrorMessage)

            colhStep8_shareholders_names.Text = Language._Object.getCaption(Me.colhStep8_shareholders_names.ID, Me.colhStep8_shareholders_names.Text)
            Me.rfStep8_Shareholder.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Shareholder.ID, Me.rfStep8_Shareholder.ErrorMessage)

            Me.colhStep8_Address.Text = Language._Object.getCaption(Me.colhStep8_Address.ID, Me.colhStep8_Address.Text)
            Me.rfStep8_Address.ErrorMessage = Language._Object.getCaption(Me.rfStep8_Address.ID, Me.rfStep8_Address.ErrorMessage)
            
            btnAddDependantBusiness_step8.Text = Language._Object.getCaption(Me.btnAddDependantBusiness_step8.ID, Me.btnAddDependantBusiness_step8.Text).Replace("&", "")
            btnUpdateDependantBusiness_step8.Text = Language._Object.getCaption(Me.btnUpdateDependantBusiness_step8.ID, Me.btnUpdateDependantBusiness_step8.Text).Replace("&", "")
            btnResetDependantBusiness_step8.Text = Language._Object.getCaption(Me.btnResetDependantBusiness_step8.ID, Me.btnResetDependantBusiness_step8.Text).Replace("&", "")

            gvDependantBusiness.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step8", gvDependantBusiness.Columns(0).HeaderText).Replace("&", "")
            gvDependantBusiness.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step8", gvDependantBusiness.Columns(1).HeaderText).Replace("&", "")
            gvDependantBusiness.Columns(2).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(2).FooterText, gvDependantBusiness.Columns(2).HeaderText)
            gvDependantBusiness.Columns(3).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(3).FooterText, gvDependantBusiness.Columns(3).HeaderText)
            gvDependantBusiness.Columns(4).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(4).FooterText, gvDependantBusiness.Columns(4).HeaderText)
            gvDependantBusiness.Columns(5).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(5).FooterText, gvDependantBusiness.Columns(5).HeaderText)
            gvDependantBusiness.Columns(6).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(6).FooterText, gvDependantBusiness.Columns(6).HeaderText)
            gvDependantBusiness.Columns(7).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(7).FooterText, gvDependantBusiness.Columns(7).HeaderText)
            gvDependantBusiness.Columns(8).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(8).FooterText, gvDependantBusiness.Columns(8).HeaderText)
            gvDependantBusiness.Columns(9).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(9).FooterText, gvDependantBusiness.Columns(9).HeaderText)
            gvDependantBusiness.Columns(10).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(10).FooterText, gvDependantBusiness.Columns(10).HeaderText)
            gvDependantBusiness.Columns(11).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(11).FooterText, gvDependantBusiness.Columns(11).HeaderText)
            gvDependantBusiness.Columns(12).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(12).FooterText, gvDependantBusiness.Columns(12).HeaderText)
            gvDependantBusiness.Columns(13).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(13).FooterText, gvDependantBusiness.Columns(13).HeaderText)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            gvDependantBusiness.Columns(14).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(14).FooterText, gvDependantBusiness.Columns(14).HeaderText)
            gvDependantBusiness.Columns(15).HeaderText = Language._Object.getCaption(gvDependantBusiness.Columns(15).FooterText, gvDependantBusiness.Columns(15).HeaderText)
            'Hemant (24 Dec 2018) -- End


            'Hemant (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteDependantBusinessDeal.Title = Language.getMessage(mstrModuleName1, 35, "Are you sure you want to delete selected Dependant Business Deal?")
            'Hemant (07 Dec 2018) -- End


            'step9
            Me.lblDependantShareTitle_Step9.Text = Language._Object.getCaption(Me.lblDependantShareTitle_Step9.ID, Me.lblDependantShareTitle_Step9.Text)
            Me.colhStep9_Relationship.Text = Language._Object.getCaption(Me.colhStep9_Relationship.ID, Me.colhStep9_Relationship.Text)
            Me.rfStep9_DependantRelationship.ErrorMessage = Language._Object.getCaption(Me.rfStep9_DependantRelationship.ID, Me.rfStep9_DependantRelationship.ErrorMessage)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Me.colhStep9_DependantName.Text = Language._Object.getCaption(Me.colhStep9_DependantName.ID, Me.colhStep9_DependantName.Text)
            Me.rfStep9_DependantName.ErrorMessage = Language._Object.getCaption(Me.rfStep9_DependantName.ID, Me.rfStep9_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End

            Me.colhStep9_Securitytype.Text = Language._Object.getCaption(Me.colhStep9_Securitytype.ID, Me.colhStep9_Securitytype.Text)
            Me.rfStep9_DependantSecurityType.ErrorMessage = Language._Object.getCaption(Me.rfStep9_DependantSecurityType.ID, Me.rfStep9_DependantSecurityType.ErrorMessage)


            Me.colhStep9_certificateno.Text = Language._Object.getCaption(Me.colhStep9_certificateno.ID, Me.colhStep9_certificateno.Text)
            Me.rfStep9_DependantCertificateno.ErrorMessage = Language._Object.getCaption(Me.rfStep9_DependantCertificateno.ID, Me.rfStep9_DependantCertificateno.ErrorMessage)
            Me.colhStep9_noofshares.Text = Language._Object.getCaption(Me.colhStep9_noofshares.ID, Me.colhStep9_noofshares.Text)
            Me.rfStep9_Dependantno_of_shares.ErrorMessage = Language._Object.getCaption(Me.rfStep9_Dependantno_of_shares.ID, Me.rfStep9_Dependantno_of_shares.ErrorMessage)

            Me.colhStep9_businessname.Text = Language._Object.getCaption(Me.colhStep9_businessname.ID, Me.colhStep9_businessname.Text)
            Me.rfDependantbusinessname_step9.ErrorMessage = Language._Object.getCaption(Me.rfDependantbusinessname_step9.ID, Me.rfDependantbusinessname_step9.ErrorMessage)

            Me.colhStep9_acquisition_date.Text = Language._Object.getCaption(Me.colhStep9_acquisition_date.ID, Me.colhStep9_acquisition_date.Text)

            Me.colhStep9_market_value.Text = Language._Object.getCaption(Me.colhStep9_market_value.ID, Me.colhStep9_market_value.Text)
            rfStep9_Dependantmarketvalue.ErrorMessage = Language._Object.getCaption(Me.rfStep9_Dependantmarketvalue.ID, Me.rfStep9_Dependantmarketvalue.ErrorMessage)
            rfStep9_DependantmarketvalueCurrency.ErrorMessage = Language._Object.getCaption(Me.rfStep9_DependantmarketvalueCurrency.ID, Me.rfStep9_DependantmarketvalueCurrency.ErrorMessage)

            btnAddDependantShare_step9.Text = Language._Object.getCaption(Me.btnAddDependantShare_step9.ID, Me.btnAddDependantShare_step9.Text).Replace("&", "")
            btnUpdateDependantShare_step9.Text = Language._Object.getCaption(Me.btnUpdateDependantShare_step9.ID, Me.btnUpdateDependantShare_step9.Text).Replace("&", "")
            btnResetDependantShare_step9.Text = Language._Object.getCaption(Me.btnResetDependantShare_step9.ID, Me.btnResetDependantShare_step9.Text).Replace("&", "")

            gvDependantShare.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step9", gvDependantShare.Columns(0).HeaderText).Replace("&", "")
            gvDependantShare.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step9", gvDependantShare.Columns(1).HeaderText).Replace("&", "")
            gvDependantShare.Columns(2).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(2).FooterText, gvDependantShare.Columns(2).HeaderText)
            gvDependantShare.Columns(3).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(3).FooterText, gvDependantShare.Columns(3).HeaderText)
            gvDependantShare.Columns(4).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(4).FooterText, gvDependantShare.Columns(4).HeaderText)
            gvDependantShare.Columns(5).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(5).FooterText, gvDependantShare.Columns(5).HeaderText)
            gvDependantShare.Columns(6).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(6).FooterText, gvDependantShare.Columns(6).HeaderText)
            gvDependantShare.Columns(7).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(7).FooterText, gvDependantShare.Columns(7).HeaderText)
            gvDependantShare.Columns(8).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(8).FooterText, gvDependantShare.Columns(8).HeaderText)
            gvDependantShare.Columns(9).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(9).FooterText, gvDependantShare.Columns(9).HeaderText)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            gvDependantShare.Columns(10).HeaderText = Language._Object.getCaption(gvDependantShare.Columns(10).FooterText, gvDependantShare.Columns(10).HeaderText)
            'Hemant (24 Dec 2018) -- End

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            popupDeleteDependantShare.Title = Language.getMessage(mstrModuleName1, 42, "Are you sure you want to delete selected dependant share?")
            'Sohail (07 Dec 2018) -- End

            'step 10
            Me.lblDependantBankTitle_Step10.Text = Language._Object.getCaption(Me.lblDependantBankTitle_Step10.ID, Me.lblDependantBankTitle_Step10.Text)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Me.colhStep10_RelationShip.Text = Language._Object.getCaption(Me.colhStep10_RelationShip.ID, Me.colhStep10_RelationShip.Text)
            Me.rfStep10_RelationShip.ErrorMessage = Language._Object.getCaption(Me.rfStep10_RelationShip.ID, Me.rfStep10_RelationShip.ErrorMessage)
            Me.colhStep10_DependantName.Text = Language._Object.getCaption(Me.colhStep10_DependantName.ID, Me.colhStep10_DependantName.Text)
            Me.rfStep10_DependantName.ErrorMessage = Language._Object.getCaption(Me.rfStep10_DependantName.ID, Me.rfStep10_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End
            Me.colhStep10_bankname.Text = Language._Object.getCaption(Me.colhStep10_bankname.ID, Me.colhStep10_bankname.Text)
            Me.rfStep10_DependantBankname.ErrorMessage = Language._Object.getCaption(Me.rfStep10_DependantBankname.ID, Me.rfStep10_DependantBankname.ErrorMessage)

            Me.colhStep10_accounttype.Text = Language._Object.getCaption(Me.colhStep10_accounttype.ID, Me.colhStep10_accounttype.Text)
            Me.rfStep10_DependantAccounttype.ErrorMessage = Language._Object.getCaption(Me.rfStep10_DependantAccounttype.ID, Me.rfStep10_DependantAccounttype.ErrorMessage)

            Me.colhStep10_accountno.Text = Language._Object.getCaption(Me.colhStep10_accountno.ID, Me.colhStep10_accountno.Text)
            Me.rfStep10_DependantAccountno.ErrorMessage = Language._Object.getCaption(Me.rfStep10_DependantAccountno.ID, Me.rfStep10_DependantAccountno.ErrorMessage)

            Me.colhStep10_Specify.Text = Language._Object.getCaption(Me.colhStep10_Specify.ID, Me.colhStep10_Specify.Text)
            Me.colhStep10_countryunkid.Text = Language._Object.getCaption(Me.colhStep10_countryunkid.ID, Me.colhStep10_countryunkid.Text)

            Me.rfStep10_DependantBankCurrency.ErrorMessage = Language._Object.getCaption(Me.rfStep10_DependantBankCurrency.ID, Me.rfStep10_DependantBankCurrency.ErrorMessage)

            btnAddDependantBankAccount_step10.Text = Language._Object.getCaption(Me.btnAddDependantBankAccount_step10.ID, Me.btnAddDependantBankAccount_step10.Text).Replace("&", "")
            btnUpdateDependantBankAccount_step10.Text = Language._Object.getCaption(Me.btnUpdateDependantBankAccount_step10.ID, Me.btnUpdateDependantBankAccount_step10.Text).Replace("&", "")
            btnResetDependantBankAccount_step10.Text = Language._Object.getCaption(Me.btnResetDependantBankAccount_step10.ID, Me.btnResetDependantBankAccount_step10.Text).Replace("&", "")

            gvDependantBank.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step10", gvDependantBank.Columns(0).HeaderText).Replace("&", "")
            gvDependantBank.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step10", gvDependantBank.Columns(1).HeaderText).Replace("&", "")
            gvDependantBank.Columns(2).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(2).FooterText, gvDependantBank.Columns(2).HeaderText)
            gvDependantBank.Columns(3).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(3).FooterText, gvDependantBank.Columns(3).HeaderText)
            gvDependantBank.Columns(4).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(4).FooterText, gvDependantBank.Columns(4).HeaderText)
            gvDependantBank.Columns(5).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(5).FooterText, gvDependantBank.Columns(5).HeaderText)
            gvDependantBank.Columns(6).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(6).FooterText, gvDependantBank.Columns(6).HeaderText)
            gvDependantBank.Columns(7).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(7).FooterText, gvDependantBank.Columns(7).HeaderText)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            gvDependantBank.Columns(8).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(8).FooterText, gvDependantBank.Columns(8).HeaderText)
            gvDependantBank.Columns(9).HeaderText = Language._Object.getCaption(gvDependantBank.Columns(9).FooterText, gvDependantBank.Columns(9).HeaderText)
            'Hemant (24 Dec 2018) -- End

            'step 11
            Me.lblDependantPropertiesTitle_Step11.Text = Language._Object.getCaption(Me.lblDependantPropertiesTitle_Step11.ID, Me.lblDependantPropertiesTitle_Step11.Text)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            Me.colhStep11_RelationShip.Text = Language._Object.getCaption(Me.colhStep11_RelationShip.ID, Me.colhStep11_RelationShip.Text)
            Me.rfStep11_RelationShip.ErrorMessage = Language._Object.getCaption(Me.rfStep11_RelationShip.ID, Me.rfStep11_RelationShip.ErrorMessage)
            Me.colhStep11_DependantName.Text = Language._Object.getCaption(Me.colhStep11_DependantName.ID, Me.colhStep11_DependantName.Text)
            Me.rfStep11_DependantName.ErrorMessage = Language._Object.getCaption(Me.rfStep11_DependantName.ID, Me.rfStep11_DependantName.ErrorMessage)
            'Hemant (03 Dec 2018) -- End
            Me.colhStep11_asset_name.Text = Language._Object.getCaption(Me.colhStep11_asset_name.ID, Me.colhStep11_asset_name.Text)
            Me.rfStep11_DependantPropertiesAsset.ErrorMessage = Language._Object.getCaption(Me.rfStep11_DependantPropertiesAsset.ID, Me.rfStep11_DependantPropertiesAsset.ErrorMessage)

            Me.colhStep11_location.Text = Language._Object.getCaption(Me.colhStep11_location.ID, Me.colhStep11_location.Text)

            Me.colhStep11_registrationtitleno.Text = Language._Object.getCaption(Me.colhStep11_registrationtitleno.ID, Me.colhStep11_registrationtitleno.Text)
            colhStep11_estimated_value.Text = Language._Object.getCaption(Me.colhStep11_estimated_value.ID, Me.colhStep11_estimated_value.Text)
            Me.rfStep11_DependantPropertiesEstimatevalue.ErrorMessage = Language._Object.getCaption(Me.rfStep11_DependantPropertiesEstimatevalue.ID, Me.rfStep11_DependantPropertiesEstimatevalue.ErrorMessage)
            Me.rfStep11_DependantPropertiesEstimatevalueCurrency.ErrorMessage = Language._Object.getCaption(Me.rfStep11_DependantPropertiesEstimatevalueCurrency.ID, Me.rfStep11_DependantPropertiesEstimatevalueCurrency.ErrorMessage)

            Me.colhStep11_AcquisitionDateDepnProperties.Text = Language._Object.getCaption(Me.colhStep11_AcquisitionDateDepnProperties.ID, Me.colhStep11_AcquisitionDateDepnProperties.Text)

            btnAddDependantProperties_step11.Text = Language._Object.getCaption(Me.btnAddDependantProperties_step11.ID, Me.btnAddDependantProperties_step11.Text).Replace("&", "")
            btnUpdateDependantProperties_step11.Text = Language._Object.getCaption(Me.btnUpdateDependantProperties_step11.ID, Me.btnUpdateDependantProperties_step11.Text).Replace("&", "")
            btnResetDependantProperties_step11.Text = Language._Object.getCaption(Me.btnResetDependantProperties_step11.ID, Me.btnResetDependantProperties_step11.Text).Replace("&", "")

            gvDependantProperties.Columns(0).HeaderText = Language._Object.getCaption("btnEdit_step11", gvDependantProperties.Columns(0).HeaderText).Replace("&", "")
            gvDependantProperties.Columns(1).HeaderText = Language._Object.getCaption("btnDelete_step11", gvDependantProperties.Columns(1).HeaderText).Replace("&", "")
            gvDependantProperties.Columns(2).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(2).FooterText, gvDependantProperties.Columns(2).HeaderText)
            gvDependantProperties.Columns(3).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(3).FooterText, gvDependantProperties.Columns(3).HeaderText)
            gvDependantProperties.Columns(4).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(4).FooterText, gvDependantProperties.Columns(4).HeaderText)
            gvDependantProperties.Columns(5).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(5).FooterText, gvDependantProperties.Columns(5).HeaderText)
            gvDependantProperties.Columns(6).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(6).FooterText, gvDependantProperties.Columns(6).HeaderText)
            gvDependantProperties.Columns(7).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(7).FooterText, gvDependantProperties.Columns(7).HeaderText)
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            gvDependantProperties.Columns(8).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(8).FooterText, gvDependantProperties.Columns(8).HeaderText)
            gvDependantProperties.Columns(9).HeaderText = Language._Object.getCaption(gvDependantProperties.Columns(9).FooterText, gvDependantProperties.Columns(9).HeaderText)
            'Hemant (24 Dec 2018) -- End

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'Hemant (24 Dec 2018) -- Start
            'ISSUE : Verifying Languages of all controls of all forms for Asset Declaration Templates
            'popupDeleteDependantShare.Title = Language.getMessage(mstrModuleName1, 43, "Are you sure you want to delete selected dependant properties?")
            popupDeleteDependantProperties.Title = Language.getMessage(mstrModuleName1, 43, "Are you sure you want to delete selected dependant properties?")
            'Hemant (24 Dec 2018) -- End

            'Sohail (07 Dec 2018) -- End

            'step 12
            lblSighOffTitle_Step12.Text = Language._Object.getCaption(Me.lblSighOffTitle_Step12.ID, Me.lblSighOffTitle_Step12.Text)
            txtHeader_Step12.Text = Language._Object.getCaption(Me.txtHeader_Step12.ID, Me.txtHeader_Step12.Text)

            'txtPara1_Step12.Text = Language._Object.getCaption(Me.txtPara1_Step12.ID, Me.txtPara1_Step12.Text)
            'txtPara2_Step12.Text = Language._Object.getCaption(Me.txtPara2_Step12.ID, Me.txtPara2_Step12.Text)
            'txtPara3_Step12.Text = Language._Object.getCaption(Me.txtPara3_Step12.ID, Me.txtPara3_Step12.Text)
            'txtPara4_Step12.Text = Language._Object.getCaption(Me.txtPara4_Step12.ID, Me.txtPara4_Step12.Text)
            'txtSignatureLine_Step12.Text = Language._Object.getCaption(Me.txtSignatureLine_Step12.ID, Me.txtSignatureLine_Step12.Text)
            'txtDate_Step12.Text = Language._Object.getCaption(Me.txtDate_Step12.ID, Me.txtDate_Step12.Text)

            'txtPara1_Step12.Text = Language.getMessage(mstrModuleName1, 81, "I") & " " & cboStep1_Employee.SelectedItem.ToString.Split("-")(1).Trim & Language.getMessage(mstrModuleName1, 82, ". Do hereby declare that I have read and understood the NMB Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct.")
            'txtPara2_Step12.Text = Language.getMessage(mstrModuleName1, 83, "I undertake to fully comply with the letter and spirit of the Code of Conduct. I understand that I have a personal responsibility to ensure that I am fully aware of the requirements under the Code of Conduct and if I am not aware of the requirements, I should familiarize myself with the content before signing this acknowledgement form.")
            'txtPara3_Step12.Text = Language.getMessage(mstrModuleName1, 84, "I have made the disclosures above as required under the NMB Code of Conduct and confirm that the said disclosures are made willingly and reflect the truth of my status as of the date indicated below.")
            'txtPara4_Step12.Text = Language.getMessage(mstrModuleName1, 85, "I further understand that in the event that the said disclosures are found to be incomplete or untrue, I will face disciplinary action which may include termination of my employment with the Bank.")
            'txtSignatureLine_Step12.Text = Language.getMessage(mstrModuleName1, 86, "Signature:")
            'txtDate_Step12.Text = Language.getMessage(mstrModuleName1, 87, "Date:")

            'txtSignatureLine_Step12.Text = Language._Object.getCaption(Me.txtSignatureLine_Step12.ID, Me.txtSignatureLine_Step12.Text)
            'txtDate_Step12.Text = Language._Object.getCaption(Me.txtDate_Step12.ID, Me.txtDate_Step12.Text)
            chkAcknowledgement_Step13.Text = Language._Object.getCaption(Me.chkAcknowledgement_Step13.ID, Me.chkAcknowledgement_Step13.Text)

            Me.lblExchangeRate.Text = Language._Object.getCaption(Me.lblExchangeRate.ID, Me.lblExchangeRate.Text)
            lblExchangeRate.Text = (Language._Object.getCaption(Me.lblExchangeRate.ID, Me.lblExchangeRate.Text))
            Me.btnBack.Text = (Language._Object.getCaption(Me.btnBack.ID, Me.btnBack.Text))

            Me.btnNext.Text = Language._Object.getCaption(Me.btnNext.ID, Me.btnNext.Text)

            Me.btnSaveClose.Text = (Language._Object.getCaption(Me.btnSaveClose.ID, Me.btnSaveClose.Text))

            Me.btnCloseAddEdit.Text = (Language._Object.getCaption(Me.btnCloseAddEdit.ID, Me.btnCloseAddEdit.Text))
            'Sohail (26 Feb 2020) -- Start
            'NMB Issue # : not able to rename the FINAL SAVE button to "Submit" from Language.
            Me.btnFinalSave.Text = Language._Object.getCaption(Me.btnFinalSave.ID, Me.btnFinalSave.Text)
            'Sohail (26 Feb 2020) -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


   
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is not acknowledged")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this declaration?")
			Language.setMessage(mstrModuleName, 6, "After submission, you can not edit / delete declaration,Are you sure you want to sumbit this declaration?")
            Language.setMessage(mstrModuleName, 7, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved.")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to unlock final save declaration?")
			Language.setMessage(mstrModuleName, 9, "Unlock Final Save")
            Language.setMessage(mstrModuleName, 10, "This Asset Declaration Unlock Final Saved Successfully.")
            Language.setMessage(mstrModuleName, 11, "Sorry! You can not Unlock Final Save. Reason: This transaction is not Final Saved.")
            Language.setMessage(mstrModuleName, 12, "Problem in deleting information.")
            Language.setMessage(mstrModuleName, 14, "Non Official Declaration")
            Language.setMessage(mstrModuleName, 15, "Sorry! You can not perform any operation on this transaction. Reason: This is past year transaction.")
			Language.setMessage(mstrModuleName, 16, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved.")
			Language.setMessage(mstrModuleName, 17, "Declaration Submitted Successfully.")
			Language.setMessage(mstrModuleName, 18, "Sorry! You can not perform any operation on this transaction. Reason: This transaction is Final Saved.")
			Language.setMessage(mstrModuleName, 19, "Are You Sure you want to final save this declaration?")
			Language.setMessage(mstrModuleName1, 20, "Are You Sure you want to save this declaration?")
			Language.setMessage(mstrModuleName1, 21, "Confirmation")
			Language.setMessage(mstrModuleName, 22, "Emplolyee is compulsory information.Please Select Emplolyee.")
			Language.setMessage(mstrModuleName, 23, "Sorry! Asset declaration for this employee is already exist.")
			Language.setMessage(mstrModuleName, 24, "Sorry! Transaction Date should be in between")
			Language.setMessage(mstrModuleName, 25, "Please enter Tin Number.")
			Language.setMessage(mstrModuleName, 26, "Tin Number Must be 9 Digit")
			Language.setMessage(mstrModuleName, 27, "Entry Saved Successfuly!")
			Language.setMessage(mstrModuleName, 28, "File connot exitst localpath")
			Language.setMessage(mstrModuleName, 29, "Please Select Employee to Continue")
			Language.setMessage(mstrModuleName1, 30, "Are you sure you want to delete selected business deal?")
			Language.setMessage(mstrModuleName1, 31, "Are you sure you want to delete selected staff Ownership?")
			Language.setMessage(mstrModuleName1, 32, "Are you sure you want to delete selected bank account?")
            Language.setMessage(mstrModuleName1, 33, "Are you sure you want to delete selected properties?")
            Language.setMessage(mstrModuleName1, 34, "Are you sure you want to delete selected Relative?")
            Language.setMessage(mstrModuleName1, 35, "Are you sure you want to delete selected Dependant Business Deal?")
            Language.setMessage(mstrModuleName1, 36, "Are you sure you want to delete selected Dependant Bank Account?")
            Language.setMessage(mstrModuleName, 37, "Financial year is compulsory information.Please Select Financial year.")
            Language.setMessage(mstrModuleName, 38, "Please Select Financial Year to Continue")
            Language.setMessage(mstrModuleName1, 41, "Are you sure you want to delete selected liabilities?")
            Language.setMessage(mstrModuleName1, 42, "Are you sure you want to delete selected dependant share?")
            Language.setMessage(mstrModuleName1, 43, "Are you sure you want to delete selected dependant properties?")
            Language.setMessage(mstrModuleName1, 51, "Please Select Proper Date")
            Language.setMessage(mstrModuleName1, 54, "Company Tin Number Must be 9 Digit")
            Language.setMessage(mstrModuleName1, 68, "Sorry,you cannot add same Relative in employeement again.")
            Language.setMessage(mstrModuleName1, 81, "I")
            Language.setMessage(mstrModuleName1, 82, ". Do hereby declare that I have read and understood the #CompanyCode# Code of Conduct and I understand that I must at all times comply with the standards set in the Code of Conduct.")
            Language.setMessage(mstrModuleName1, 83, "I undertake to fully comply with the letter and spirit of the Code of Conduct. I understand that I have a personal responsibility to ensure that I am fully aware of the requirements under the Code of Conduct and if I am not aware of the requirements, I should familiarize myself with the content before signing this acknowledgement form.")
            Language.setMessage(mstrModuleName1, 84, "I have made the disclosures above as required under the #CompanyCode# Code of Conduct and confirm that the said disclosures are made willingly and reflect the truth of my status as of the date indicated below.")
            Language.setMessage(mstrModuleName1, 85, "I further understand that in the event that the said disclosures are found to be incomplete or untrue, I will face disciplinary action which may include termination of my employment with the Bank.")
            Language.setMessage(mstrModuleName1, 86, "Signature:")
            Language.setMessage(mstrModuleName1, 87, "Date:")
	    Language.setMessage(mstrModuleName1, 91, "Sorry, you cannot do your declaration as last date of declaration has been passed.")
	    Language.setMessage(mstrModuleName1, 92, "Please contact your administrator/manager to do futher operation on it.")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

   
End Class
