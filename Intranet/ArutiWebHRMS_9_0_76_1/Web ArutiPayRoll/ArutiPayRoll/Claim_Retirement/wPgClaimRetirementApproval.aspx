<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPgClaimRetirementApproval.aspx.vb"
    Inherits="Claim_Retirement_wPgClaimRetirementApproval" Title="Claim Retirement Approval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment" TagPrefix="PrviewAttchmet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

     <center>
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary" style="width: 95%;">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Retirement Information"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2" style="height: 170px;">
                                    <div class="ib" style="width: 40%; vertical-align: top; border-right: 1px solid #ccc;
                                        height: 100%">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 30%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 65%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                         <div class="row2">
                                            <div class="ibwm" style="width: 30%">
                                                <asp:Label ID="LblclaimRetirementNo" runat="server" Text="Retirement No"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 37%">
                                                <asp:TextBox ID="txtClaimRetirementNo" runat="server" ReadOnly="true"/>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 30%">
                                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No."></asp:Label>
                                            </div>
                                            <div class="ib" style="width: 37%">
                                                <asp:TextBox ID="txtClaimNo" runat="server" ReadOnly="true">
                                                </asp:TextBox>
                                            </div>
                                            <div class="ibwm" style="width: 27%; text-align: right">
                                                <asp:LinkButton ID="lnkViewClaimDetail" runat="server" Text="View Claim Detail" Font-Underline="false">
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 30%">
                                                <asp:Label ID="lblImprestAmount" runat="server" Text="Imprest Amount"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 37%">
                                                <asp:TextBox ID="txtImprestAmount" runat="server" ReadOnly="true" Style="text-align: right"
                                                    Text="0.00" onKeypress="return onlyNumbers(this,event);">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibwm" style="width: 55%;">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 18%">
                                                <asp:Label ID="lblImprest" runat="server" Text="Imprest"></asp:Label>
                                            </div>
                                            <div class="ib" style="width: 40%">
                                                <asp:DropDownList ID="cboImprest" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="ibwm" style="width: 12%">
                                                <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 25%">
                                                <asp:TextBox ID="txtBalance" Style="text-align: right" Text="0.00" runat="server"
                                                    ReadOnly="true">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 18%">
                                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                            </div>
                                            <div class="ib" style="width: 40%">
                                                <asp:DropDownList ID="cboCostCenter" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="ibwm" style="width: 12%">
                                                <asp:Label ID="lblQuantity" runat="server" Text="Quantity"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 25%">
                                                <asp:TextBox ID="txtQuantity" runat="server" AutoPostBack="true" Style="text-align: right"
                                                    Text="1.00" onKeypress="return onlyNumbers(this,event);">
                                                </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 18%; vertical-align: top">
                                                <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                            </div>
                                              <%-- 'Pinkal (14-Nov-2019) -- Start
                                             'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.--%>
                                            <div class="ib" style="width: 40%">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="6" style="box-sizing:border-box">
                                                </asp:TextBox>
                                            </div>
                                              <%--'Pinkal (14-Nov-2019) -- End--%>
                                            <div class="ibwm" style="width: 38%; vertical-align: top">
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 32%">
                                                        <asp:Label ID="LblUnitprice" runat="server" Text="Unit Price"></asp:Label>
                                                    </div>
                                                    <div class="ibwm" style="width: 65%">
                                                        <asp:TextBox ID="txtUnitPrice" runat="server" AutoPostBack="true" Style="text-align: right"
                                                            Text="1.00" onKeypress="return onlyNumbers(this,event);">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 32%">
                                                        <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                                    </div>
                                                    <div class="ibwm" style="width: 65%">
                                                        <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" Text="1.00"
                                                            ReadOnly="true" onKeypress="return onlyNumbers(this,event);">
                                                        </asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ibwm" style="width: 32%">
                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                                    </div>
                                                    <div class="ibwm" style="width: 65%">
                                                     <%-- 'Pinkal (14-Nov-2019) -- Start
                                                             'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.--%>
                                                        <asp:DropDownList ID="cboCurrency" runat="server" Width = "152px" />
                                                     <%--'Pinkal (14-Nov-2019) -- End--%>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btndefault" />
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btndefault" Visible="false" />
                                </div>
                            </div>
                        </div>
                        <div id="Div1" class="panel-default">
                            <div id="Div3" class="panel-body-default">
                                <asp:Panel ID="Panel2" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                    <asp:GridView ID="dgvRetirement" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        DataKeyNames="Claimretirementtranunkid,Claimretirementunkid,crmasterunkid,expenseunkid,GUID"
                                        HeaderStyle-Font-Bold="false" Width="98%">
                                        <Columns>
                                            <asp:TemplateField FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Update" CssClass="gridedit"
                                                            ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CommandName="Delete" CssClass="griddelete"
                                                            ToolTip="Delete"></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"   CommandName="Preview">
                                                               <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                      </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="expense" HeaderText="Imprest" FooterText="dgcolhImprest" />
                                            <asp:BoundField DataField="quantity" HeaderText="Quantity" FooterText="dgcolhQuantity"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="unitprice" HeaderText="Unit Price" FooterText="dgcolhUnitPrice"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="balance" HeaderText="Balance" FooterText="dgcolhBalance"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" Visible="false" />
                                            <asp:BoundField DataField="expense_remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                                
                                 <div class="row2" style="display: flex;flex-direction:row;  justify-content: flex-end;align-items: center" >
                                    <div class="ib">
                                        <asp:Label ID="LblRetirementGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                    </div>
                                    <div class="ib" >
                                       <asp:TextBox ID="txtRetirementGrandTotal" runat="server" ReadOnly="true" Width="85%" style="text-align:right;float: right"></asp:TextBox>
                                    </div>
                                   <div class="ib">
                                                <asp:Label ID="LblBalancedue" runat="server" Text="Balance Due"></asp:Label>
                                    </div>
                                    <div class="ib" >
                                               <asp:TextBox ID="txtBalanceDue" runat="server" ReadOnly="true" Width="85%" style="text-align:right; float: right" ></asp:TextBox>
                                    </div>
                                  </div>
                                
                                <div class="btn-default">
                                    <asp:Button ID="btnViewAttchment" runat="server" Visible="false" Text="View Scan/Attchment"  CssClass="btndefault" />
                                    <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btndefault" />
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />                        
                    <uc3:Confirmation ID="popup_UnitPriceYesNo" runat="server" Message="" Title="Confirmation" />
                    <uc3:Confirmation ID="popup_RemarkYesNo" runat="server" Message="" Title="Confirmation" />
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="" />
                </div>
                <cc1:ModalPopupExtender ID="popupClaimDetail" runat="server" BackgroundCssClass="ModalPopupBG"
                    CancelControlID="HiddenField2" PopupControlID="pnlClaimDetail" TargetControlID="HiddenField2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlClaimDetail" runat="server" CssClass="newpopup" Width="70%" Style="display: none;">
                    <div class="panel-primary" style="margin: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="LblClaimDetails" runat="server" Text="Claim Details"></asp:Label>
                            <asp:LinkButton ID="lnkViewDependantList" runat="server" Text="View Depedents List"
                                Font-Underline="false" ForeColor="White" Style="float: right; font-weight: bold">
                            </asp:LinkButton>
                        </div>
                        <div class="panel-body">
                            <div id="Div2" class="panel-default">
                                <div id="Div4" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 46%; height: 170px; vertical-align: top; border-right: 1px solid #ccc;">
                                            <div class="row2">
                                                <div class="ibwm" style="width: 20%">
                                                    <asp:Label ID="lblCDExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 74%">
                                                    <asp:DropDownList ID="cboCDExpCategory" runat="server" Width="315px" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 20%">
                                                    <asp:Label ID="lblCDClaimNo" runat="server" Text="Claim No."></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 25%">
                                                    <asp:TextBox ID="txtCDClaimNo" runat="server" Enabled="false">
                                                    </asp:TextBox>
                                                </div>
                                                <div class="ibwm" style="width: 13%">
                                                    <asp:Label ID="lblCDClaimDate" runat="server" Text="Date."></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 33%">
                                                    <uc2:DateCtrl ID="dtpCDClaimDate" runat="server" AutoPostBack="false" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 20%">
                                                    <asp:Label ID="lblCDClaimEmployee" runat="server" Text="Employee"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 76%">
                                                    <asp:DropDownList ID="cboCDClaimEmployee" runat="server" Width="315px">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 20%">
                                                    <asp:Label ID="lblCDClaimDomicileAddress" runat="server" Text="Domicile Address"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 74%">
                                                    <asp:TextBox ID="txtCDClaimDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                        ReadOnly="true">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ibwm" style="width: 50%; height: 170px;">
                                            <div class="row2">
                                                <div class="ibwm" style="width: 18%">
                                                    <asp:Label ID="lblCDClaimExpense" runat="server" Text="Expense"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 37%">
                                                    <asp:DropDownList ID="cboCDClaimExpense" runat="server" Width="177px">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="ibwm" style="width: 15%">
                                                        <asp:Label ID="lblCDClaimCurrency" runat="server" Text="Currency"></asp:Label>
                                                </div>
                                                <div class="ibwm" style="width: 24%">
                                                        <asp:DropDownList ID="cboCDClaimCurrency" runat="server" Width="130px"/>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 18%">
                                                    <asp:Label ID="lblCDClaimCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 40%">
                                                    <asp:DropDownList ID="cboCDClaimCostCenter" runat="server" Width="240px">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="ibwm" style="width: 12%">
                                                </div>
                                                <div class="ibwm" style="width: 25%">
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ibwm" style="width: 18%;vertical-align:top" >
                                                    <asp:Label ID="LblCDClaimRemark" runat="server" Text="Claim Remark" ></asp:Label>
                                                </div>
                                                 <div class="ib" style="width: 76%">
                                                    <asp:TextBox ID="txtCDClaimRemark" runat="server" TextMode="MultiLine" Width="100%" Rows = "5" ReadOnly = "true"></asp:TextBox>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <asp:Panel ID="pnldgvClaimData" runat="server" Height="130px" ScrollBars="Auto">
                                            <asp:DataGrid ID="dgvClaimData" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" HeaderStyle-Font-Bold="false"
                                                ItemStyle-CssClass="griviewitem" ShowFooter="False" Style="margin: auto" Width="99%">
                                                <Columns>
                                                    <asp:BoundColumn DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc" />
                                                    <asp:BoundColumn DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route" />
                                                    <asp:BoundColumn DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" />
                                                    <asp:BoundColumn DataField="quantity" FooterText="dgcolhQty" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Quantity" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Unit Price" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="amount" FooterText="dgcolhAmount" HeaderStyle-HorizontalAlign="Right"
                                                        HeaderText="Amount" ItemStyle-HorizontalAlign="Right" />
                                                    <asp:BoundColumn DataField="expense_remark" FooterText="dgcolhExpenseRemark" HeaderText="Expense Remark"
                                                        Visible="true" />
                                                    <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" Visible="false" />
                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" Visible="false" />
                                                    <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" Visible="false" />
                                                </Columns>
                                                <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Left" Mode="NumericPages" />
                                            </asp:DataGrid>
                                        </asp:Panel>
                                     </div>
                                    <div class="row2" style="display: flex;flex-direction:row;  justify-content: flex-end;align-items: center" >
                                            <div class="ib">
                                                <asp:Label ID="LblClaimGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                            </div>
                                            <div class="ib" >
                                               <asp:TextBox ID="txtClaimGrandTotal" runat="server" ReadOnly="true" Width="85%" style="text-align:right; float: right" ></asp:TextBox>
                                            </div>
                                           
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCDCloseClaimDetail" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                        <asp:HiddenField ID="HiddenField21" runat="server" />
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popupDependantList" runat="server" BackgroundCssClass="ModalPopupBG"
                    CancelControlID="btnCloseDependantList" PopupControlID="pnlDependantList" TargetControlID="lnkViewDependantList"
                    Drag="true" PopupDragHandleControlID="pnlDependantList">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlDependantList" runat="server" CssClass="newpopup" Width="60%" Style="display: none;">
                    <div class="panel-primary" style="margin: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label></div>
                        <div class="panel-body">
                            <div id="Div5" class="panel-default">
                                <div id="Div6" class="panel-body-default">
                                    <asp:Panel ID="pnlEmpDependentpopup" runat="server" Width="100%" Height="100%">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" FooterText="dgcolhName" />
                                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" FooterText="dgcolhGender" />
                                                            <asp:BoundColumn DataField="age" HeaderText="Age" ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhAge" />
                                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhMonth" />
                                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" FooterText="dgcolhRelation" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCloseDependantList" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                
                 <cc1:ModalPopupExtender ID="popupRejectRemark" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btnRemarkClose" DropShadow="true" PopupControlID="pnlReject"
                        TargetControlID="txtRejectRemark">
                    </cc1:ModalPopupExtender>
                 <asp:Panel ID="pnlReject" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px" DefaultButton="btnRemarkOk">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div7" class="panel-default">
                                    <div id="Div8" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Rejection Remark"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div9" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtRejectRemark" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnRemarkOk" runat="server" Text="Ok" CssClass="btnDefault" />
                                            <asp:Button ID="btnRemarkClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                              
            </ContentTemplate>
             <Triggers>
                    <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
                </Triggers>
        </asp:UpdatePanel>
    </center>

</asp:Content>
