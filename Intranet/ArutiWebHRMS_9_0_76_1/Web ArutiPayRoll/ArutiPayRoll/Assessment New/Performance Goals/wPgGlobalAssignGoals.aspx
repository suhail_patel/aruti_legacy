<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPgGlobalAssignGoals.aspx.vb"
    Inherits="Assessment_New_Performance_Goals_wPgGlobalAssignGoals" Title="Owners Global Assign Goals" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function () {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "none"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Owners Global Assign Goals"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 40%; vertical-align: top">
                                                <div id="Div1" class="panel-default">
                                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Goal Owner And Period Detail"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div2" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblOwner" runat="server" Text="Ower"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:DropDownList ID="cboOwner" runat="server" AutoPostBack="true" Width="100%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 40%">
                                                                    <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                                </td>
                                                                <td style="width: 60%">
                                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" Width="100%">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnSeach" runat="server" Text="Search" CssClass="btndefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="Div3" class="panel-default">
                                                    <div id="Div4" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblAssignedTo" runat="server" Text="Assigned TO" Style="float: left"></asp:Label>
                                                        </div>
                                                        <dir style="text-align: right; margin-top: -5px">
                                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover"
                                                                Style="vertical-align: top"></asp:LinkButton>
                                                            <asp:ImageButton ID="imgbtnReset" runat="server" ImageUrl="~/images/reset_20.png"
                                                                Style="vertical-align: top" />
                                                        </dir>
                                                    </div>
                                                    <div id="Div5" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td colspan="2" style="width: 100%">
                                                                    <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="width: 100%">
                                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="height: 405px;
                                                                        overflow: auto; border-bottom: solid 2px #DDD;" class="gridscroll">
                                                                        <asp:Panel ID="pnl_dgvOwr" runat="server">
                                                                        <asp:DataGrid ID="dgvOwner" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkAllSelect_CheckedChanged" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("ischeck") %>'
                                                                                            OnCheckedChanged="chkSelect_CheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                                    HeaderStyle-Width="80px" ItemStyle-Width="80px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                                    HeaderStyle-Width="239px" ItemStyle-Width="239px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeeunkid" Visible="false" FooterText="objdgcolhEmpId">
                                                                                </asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 60%; vertical-align: top">
                                                <div id="Div6" class="panel-default">
                                                    <div id="Div7" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblOwrItemHeader" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div8" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <%--<tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <asp:TextBox ID="txtSearchItems" runat="server" AutoPostBack="true" Width="99%"></asp:TextBox>
                                                                </td>
                                                            </tr>--%>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%">
                                                                    <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="height: 578px;
                                                                        max-width: 630px; overflow: auto; border-bottom: solid 2px #DDD;" class="gridscroll">
                                                                        <asp:Panel ID="pnl_dgvItems" runat="server">
                                                                            <%--<asp:DataGrid ID="dgvItems" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="150%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-Width="20px" ItemStyle-Width="20px">
                                                                                    <HeaderTemplate>
                                                                                        <asp:CheckBox ID="chkItemAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkItemAllSelect_CheckedChanged" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:CheckBox ID="chkItemSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("fcheck") %>'
                                                                                            OnCheckedChanged="chkItemSelect_CheckedChanged" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:TemplateColumn HeaderStyle-Width="300px" ItemStyle-Width="300px">
                                                                                    <HeaderTemplate>
                                                                                        <asp:Label ID="lblOwrFieldGridHeader" runat="server" Text="" />
                                                                                    </HeaderTemplate>
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblOwrFieldItem" runat="server" Text='<%# Eval("OwrFieldItem") %>' />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="dgcolhfstdate"
                                                                                    HeaderStyle-Width="70px" ItemStyle-Width="70px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="dgcolhfeddate"
                                                                                    HeaderStyle-Width="70px" ItemStyle-Width="70px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="dgcolhfstatus"
                                                                                    HeaderStyle-Width="70px" ItemStyle-Width="70px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhfweight"
                                                                                    HeaderStyle-Width="50px" ItemStyle-Width="50px"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="dgcolhfpct_complete"
                                                                                    HeaderStyle-Width="76px" ItemStyle-Width="80px"></asp:BoundColumn>
                                                                            </Columns>
                                                                            </asp:DataGrid>--%>
                                                                            <asp:GridView ID="dgvItems" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99.5%">
                                                                                <Columns>
                                                                                    <asp:TemplateField>
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="chkItemAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkItemAllSelect_CheckedChanged" /></HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="chkItemSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("fcheck") %>'
                                                                                                OnCheckedChanged="chkItemSelect_CheckedChanged" /></ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                    </asp:Panel>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
