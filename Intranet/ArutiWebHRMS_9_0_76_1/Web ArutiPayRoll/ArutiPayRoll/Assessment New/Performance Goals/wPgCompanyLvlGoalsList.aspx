﻿<%@ Page Title="Company Goals List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgCompanyLvlGoalsList.aspx.vb" Inherits="wPgCompanyLvlGoalsList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>

    <script type="text/javascript">
    var prm;
    prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(endRequestHandler);
    function endRequestHandler(sender, args) {
                SetGeidScrolls();
    }
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "none"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Company Goals List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="lblPeriod" runat="server" Width="100%" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="objlblField1" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="objlblField3" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="objlblField5" runat="server" Width="100%" Text="#Caption"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue1" runat="server" AutoPostBack="true" Height="25px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue3" runat="server" AutoPostBack="true" Height="25px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue5" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="lblPerspective" runat="server" Text="Perspective"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="objlblField2" runat="server" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="objlblField4" runat="server" Text="#Caption"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboPerspective" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue2" runat="server" AutoPostBack="true" Height="25px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboFieldValue4" runat="server" AutoPostBack="true" Height="25px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="lblStartDate" runat="server" Text="Start Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="lblEndDate" runat="server" Text="End Date"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="lblGoalOwner" runat="server" Text="Goal Owner"></asp:Label>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:Label ID="objlblCaption" runat="server" Text="#Caption"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%;" align="left">
                                                <uc2:DateCtrl ID="dtpStartDate" runat="server" />
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <uc2:DateCtrl ID="dtpEndDate" runat="server" />
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboAllocations" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 25%;" align="left">
                                                <asp:DropDownList ID="cboOwner" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <asp:Panel ID="pnl_dgvData" runat="server" Style="width: 99%; overflow: auto; height: 450px"
                                        CssClass="gridscroll">
                                        <asp:GridView ID="dgvData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                            Width="150%" AutoGenerateColumns="false">
                                            <Columns>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Label ID="objlblTotalWeight" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
