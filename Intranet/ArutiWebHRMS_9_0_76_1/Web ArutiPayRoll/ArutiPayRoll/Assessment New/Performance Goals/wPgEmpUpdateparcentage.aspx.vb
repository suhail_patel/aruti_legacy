﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
#End Region

Partial Class Assessment_New_Performance_Goals_wPgEmpUpdateparcentage
    Inherits Basepage
#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmEmpUpdatePercentage"

    'Shani(14-APR-2016) -- Start
    Private mintAssessorMasterId As Integer = 0
    Private mintAssessorEmployeeId As Integer = 0
    'Shani(14-APR-2016) -- End 


#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Dim mintFldTypeId As Integer = -1
                Dim dtOwner As New DataTable
                FillCombo()
                Me.ViewState.Add("dtFinalTab", dtOwner)
                Me.ViewState.Add("mintExOrder", 0)
                Me.ViewState.Add("AdvanceFilter", "")
                Me.ViewState.Add("strLinkedFld", "")
                Me.ViewState.Add("OwrFldTypeId", mintFldTypeId)
            End If
            If CStr(Me.ViewState("strLinkedFld")).Trim.Length > 0 Then
                dgvItems.Columns(1).HeaderText = CStr(Me.ViewState("strLinkedFld"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Try
            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = intCurrentPeriodId
            End With
            'Shani (09-May-2016) -- [.SelectedValue = intCurrentPeriodId]

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    dsList = objEmp.GetEmployeeList("Emp", False, True, Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0

            'S.SANDEEP [10 DEC 2015] -- START
            'Shani(14-APR-2016) -- Start
            'Dim blnApplyUACFilter As Boolean = True
            Dim blnApplyUACFilter As Boolean = False
            Dim strFilterQry As String = ""
            'Shani(14-APR-2016) -- End 
            'S.SANDEEP [10 DEC 2015] -- END

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))

                'S.SANDEEP [10 DEC 2015] -- START
                blnApplyUACFilter = False
                'S.SANDEEP [10 DEC 2015] -- END

                'Shani(14-APR-2016) -- Start
            Else
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master
                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                If dsMapEmp.Tables("List").Rows.Count > 0 Then

                    mintAssessorMasterId = CInt(dsMapEmp.Tables("List").Rows(0)("Id"))
                    mintAssessorEmployeeId = CInt(dsMapEmp.Tables("List").Rows(0)("EmpId"))

                    dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                    strFilterQry = " hremployee_master.employeeunkid IN "
                    csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                End If
                'Shani(14-APR-2016) -- End 



            End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyUACFilter) 'Shani(14-APR-2016) -- [strFilterQry]


            'S.SANDEEP [10 DEC 2015] -- START {blnApplyUACFilter} -- END
            'Shani(24-Aug-2015) -- End

            With cboOwner
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    .SelectedValue = 0
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    .SelectedValue = Session("Employeeunkid")
                End If
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Set_Caption()
        Dim mintLinkedFld As Integer
        Dim mintExOrder As Integer
        Dim mintOwrFldTypeId As Integer
        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(cboPeriod.SelectedValue)
            mintLinkedFld = objFMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            Dim objFMaster As New clsAssess_Field_Master
            mintExOrder = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMaster = Nothing : objFMapping = Nothing
            lblOwrItemHeader.Text = strLinkedFld & " " & Language.getMessage(mstrModuleName, 1, "Information")
            dgvItems.Columns(1).HeaderText = strLinkedFld
            Me.ViewState("mintExOrder") = mintExOrder
            Me.ViewState("strLinkedFld") = strLinkedFld
            Me.ViewState("OwrFldTypeId") = mintOwrFldTypeId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Items_Grid()
        Dim dtFinalTab As New DataTable
        Dim dtFinalTabView As DataView
        Try

            Dim objOwner As New clsassess_empfield1_master
            'S.SANDEEP |05-APR-2019| -- START
            'dtFinalTab = objOwner.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinalTab = objOwner.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List", Session("CascadingTypeId"))
            'S.SANDEEP |05-APR-2019| -- END
            dtFinalTab.Columns.Add("fcheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            dtFinalTab.Columns.Add("fchange", System.Type.GetType("System.Boolean")).DefaultValue = False
            dgvItems.AutoGenerateColumns = False

            Dim xCol As New BoundColumn()
            xCol.HeaderText = dgvItems.Columns(1).HeaderText
            xCol.HeaderStyle.Width = Unit.Pixel(380)
            xCol.ItemStyle.Width = Unit.Pixel(380)
            xCol.FooterStyle.Width = Unit.Pixel(380)
            Select Case CInt(Me.ViewState("mintExOrder"))
                Case enWeight_Types.WEIGHT_FIELD1
                    xCol.DataField = "Field1"
                Case enWeight_Types.WEIGHT_FIELD2
                    xCol.DataField = "Field2"
                Case enWeight_Types.WEIGHT_FIELD3
                    xCol.DataField = "Field3"
                Case enWeight_Types.WEIGHT_FIELD4
                    xCol.DataField = "Field4"
                Case enWeight_Types.WEIGHT_FIELD5
                    xCol.DataField = "Field5"
            End Select
            dgvItems.Columns.RemoveAt(1)
            dgvItems.Columns.AddAt(1, xCol)
            Dim xRow = From iRow In dtFinalTab.AsEnumerable() Where iRow.IsNull("fcheck") = True Select iRow
            For Each mRow As DataRow In xRow
                mRow.Item("fcheck") = False
            Next
            dtFinalTabView = dtFinalTab.DefaultView
            Dim strSearch As String = ""
            dgvItems.DataSource = dtFinalTabView
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If Me.ViewState("strLinkedFld") IsNot Nothing Then
                dgvItems.Columns(1).HeaderText = CStr(Me.ViewState("strLinkedFld"))
            End If
            'SHANI [01 FEB 2015]--END 
            dgvItems.DataBind()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidData(ByVal dtFinalTab As DataTable) As Boolean
        Try
            Dim mintPercent As Decimal = 0
            Decimal.TryParse(txtPercent.Text, mintPercent)
            If mintPercent > 100 Or mintPercent <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Percentage Completed should be between 1 and 100."), Me)
                txtPercent.Focus()
                Return False
            End If

            If radApplyTochecked.Checked = False AndAlso radApplyToAll.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Please select atleast one operation type to set percentage."), Me)
                Return False
            End If

            If radApplyTochecked.Checked = True Then
                dtFinalTab.AcceptChanges()
                Dim xRow() As DataRow = Nothing
                xRow = dtFinalTab.Select("fcheck = true")
                If xRow.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, please check atleast one of the") & " " & Me.ViewState("strLinkedFld") & " " & _
                                    Language.getMessage(mstrModuleName, 4, "goal(s) to assign Percentage."), Me)
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub BindData(ByVal dtFinalTab As DataTable)
        Dim dtFinalTabView As DataView
        Try
            Dim xCol As New BoundColumn()
            xCol.HeaderText = dgvItems.Columns(1).HeaderText
            Select Case CInt(Me.ViewState("mintExOrder"))
                Case enWeight_Types.WEIGHT_FIELD1
                    xCol.DataField = "Field1"
                Case enWeight_Types.WEIGHT_FIELD2
                    xCol.DataField = "Field2"
                Case enWeight_Types.WEIGHT_FIELD3
                    xCol.DataField = "Field3"
                Case enWeight_Types.WEIGHT_FIELD4
                    xCol.DataField = "Field4"
                Case enWeight_Types.WEIGHT_FIELD5
                    xCol.DataField = "Field5"
            End Select
            dgvItems.Columns.RemoveAt(1)
            dgvItems.Columns.AddAt(1, xCol)
            dtFinalTabView = dtFinalTab.DefaultView
            Dim strSearch As String = ""
            dgvItems.DataSource = dtFinalTabView
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If Me.ViewState("strLinkedFld") IsNot Nothing Then
                dgvItems.Columns(1).HeaderText = CStr(Me.ViewState("strLinkedFld"))
            End If
            'SHANI [01 FEB 2015]--END 
            dgvItems.DataBind()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            If IsValidData(dtFinalTab) = False Then Exit Sub
            Dim rows As IEnumerable(Of DataRow) = Nothing
            If radApplyToAll.Checked = True Then
                Dim chk As New CheckBox
                chk.Checked = True
                Call chkItemAllSelect_CheckedChanged(chk, Nothing)
            End If
            rows = dtFinalTab.Rows.Cast(Of DataRow)().Where(Function(r) r("fcheck").ToString() = "True")
            For Each row As DataRow In rows
                row("pct_complete") = txtPercent.Text
                row("fchange") = True
            Next
            BindData(dtFinalTab)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSeach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSeach.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Please select Period and Owner to do global assign operation."), Me)
                Exit Sub
            End If
            Call Fill_Items_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            If dtFinalTab IsNot Nothing Then
                Dim blnflag As Boolean = False
                dtFinalTab.AcceptChanges()
                Dim rows As IEnumerable(Of DataRow) = dtFinalTab.Rows.Cast(Of DataRow)().Where(Function(r) r("fchange").ToString() = "True")
                Dim mGoalIdx As Integer = 1
                For Each row As DataRow In rows
                    Select Case CInt(Me.ViewState("mintExOrder"))
                        Case enWeight_Types.WEIGHT_FIELD1
                            Dim objOField1 As New clsassess_empfield1_master
                            objOField1._Empfield1unkid = CInt(row("empfield1unkid"))
                            objOField1._Pct_Completed = CDec(row("pct_complete"))
                            blnflag = objOField1.Update()
                            If blnflag = False AndAlso objOField1._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField1._Message, Me)
                                Exit Sub
                            End If
                            objOField1 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD2
                            Dim objOField2 As New clsassess_empfield2_master
                            objOField2._Empfield2unkid = CInt(row("empfield2unkid"))
                            objOField2._Pct_Completed = CDec(row("pct_complete"))
                            blnflag = objOField2.Update()
                            If blnflag = False AndAlso objOField2._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField2._Message, Me)
                                Exit Sub
                            End If
                            objOField2 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD3
                            Dim objOField3 As New clsassess_empfield3_master
                            objOField3._Empfield3unkid = CInt(row("empfield3unkid"))
                            objOField3._Pct_Completed = CDec(row("pct_complete"))
                            blnflag = objOField3.Update()
                            If blnflag = False AndAlso objOField3._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField3._Message, Me)
                                Exit Sub
                            End If
                            objOField3 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD4
                            Dim objOField4 As New clsassess_empfield4_master
                            objOField4._Empfield4unkid = CInt(row("empfield4unkid"))
                            objOField4._Pct_Completed = CDec(row("pct_complete"))
                            blnflag = objOField4.Update()
                            If blnflag = False AndAlso objOField4._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField4._Message, Me)
                                Exit Sub
                            End If
                            objOField4 = Nothing

                        Case enWeight_Types.WEIGHT_FIELD5
                            Dim objOField5 As New clsassess_empfield5_master
                            objOField5._Empfield5unkid = CInt(row("empfield5unkid"))
                            objOField5._Pct_Completed = CDec(row("pct_complete"))
                            blnflag = objOField5.Update()
                            If blnflag = False AndAlso objOField5._Message <> "" Then
                                DisplayMessage.DisplayMessage(objOField5._Message, Me)
                                Exit Sub
                            End If
                            objOField5 = Nothing
                    End Select
                    mGoalIdx += 1
                Next
                Call Fill_Items_Grid()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region

#Region "Control Event"
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboOwner.SelectedIndexChanged
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                dgvItems.DataSource = Nothing : dgvItems.DataBind()
                If dtFinalTab IsNot Nothing Then dtFinalTab.Rows.Clear()
            ElseIf CInt(cboPeriod.SelectedValue) > 0 Then
                Call Set_Caption()
            End If
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkItemAllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvItems.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkItemSelect"), CheckBox)
                chk.Checked = chkAll.Checked
                dtFinalTab.Rows(iRow.ItemIndex)("fcheck") = chkAll.Checked
            Next
            dtFinalTab.AcceptChanges()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkItemSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtFinalTab As DataTable = CType(Me.ViewState("dtFinalTab"), DataTable)
        Try
            Dim chkItem As CheckBox = CType(sender, CheckBox)
            Dim xRow As DataGridItem = CType(chkItem.NamingContainer, DataGridItem)
            If xRow.ItemIndex >= 0 Then
                dtFinalTab.Rows(xRow.ItemIndex)("fcheck") = chkItem.Checked
            End If
            dtFinalTab.AcceptChanges()
            Me.ViewState("dtFinalTab") = dtFinalTab
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
    Protected Sub dgvItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvItems.ItemDataBound
        Try
            Call SetDateFormat()
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                If e.Item.Cells(2).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(2).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToShortDateString
                End If
                If e.Item.Cells(3).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(3).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).Date.ToShortDateString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (16-Apr-2016) -- End
#End Region

End Class
