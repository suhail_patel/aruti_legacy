﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgAssessorEvaluation.aspx.vb" Inherits="Assessment_New_Performance_Evaluation_wPgAssessorEvaluation"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        function ChangeApplicantFilterImage(imgID, divID) {
            var pathname = document.location.href;
            var arr = pathname.split('/');
            var img = document.getElementById(imgID);

            if (img) {

                var imgURL = document.getElementById(imgID).src.split('/');
                var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';

                if (imgURL[imgURL.length - 1] == 'plus.png') {
                    document.getElementById(imgID).src = URL + "images/minus.png";
                    document.getElementById(divID).style.display = 'block';
                }
                if (imgURL[imgURL.length - 1] == 'minus.png') {
                    document.getElementById(imgID).src = URL + "images/plus.png";
                    document.getElementById(divID).style.display = 'none';
                }
            }
        }

        function setlocation() {
            document.getElementById("divBSC").style.display = "none";
            document.getElementById("divGE").style.display = "none";
            document.getElementById("divCItem").style.display = "none";
            var exOrder = '<%= Session("Perf_EvaluationOrder") %>';

            var res = exOrder.split("|");
            var order = 2;
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    if (res[i] == "1") {
                        document.getElementById("divBSC").style.display = "block";
                        document.getElementById("divBSC").style.order = order;
                        order = order + 1;
                    }
                    if (res[i] == "2") {
                        document.getElementById("divGE").style.display = "block";
                        document.getElementById("divGE").style.order = order;
                        order = order + 1;
                    }
                    if (res[i] == "3") {
                        document.getElementById("divCItem").style.display = "block";
                        document.getElementById("divCItem").style.order = order;
                        order = order + 1;
                    }
                }
            }
        }
        $(document).ready(function() {
            setlocation();
        });

        function set_location() {
            setlocation();
        }

        function isvalid_value(ctrl) {

            var prd = document.getElementById("<%=cboPeriod.ClientID%>");
            var emp = document.getElementById("<%=cboEmployee.ClientID%>");
            var ctmid = $(ctrl).closest("tr").find("td[Id='ctmid']").text();

            PageMethods.IsValidCustomValue(ctmid, $(ctrl).val(), $(emp).val(), $(prd).val(), onSuccess, onFailure);

            function onSuccess(str) {
                if (str.toString().length != 0) {
                    $(ctrl).val('')
                    alert(str);
                }
            }

            function onFailure(err) {
                alert(err.get_message());
            }
        }

        function change_event_GE(drp) {

            var ScoringOptionId = '<%= Session("ScoringOptionId") %>';
            var asr = document.getElementById("<%=cboAssessor.ClientID%>");
            var prd = document.getElementById("<%=cboPeriod.ClientID%>");
            var dtp = document.getElementById("<%= dtpAssessdate.ClientID %>" + '_TxtDate');
            var emp = document.getElementById("<%=cboEmployee.ClientID%>");
            var EmployeeAsOnDate = '<%= Session("EmployeeAsOnDate") %>';
            var IsUseAgreedScore = '<%= Session("IsUseAgreedScore") %>';
            var Self_Assign_Competencies = '<%= Session("Self_Assign_Competencies") %>';
            //            var test = document.getElementById("<%= objlblGEScr.ClientID %>");
            //            var result = test.innerHTML;
            var row = $(drp).closest("tr");
            var itemid = $(drp).closest("tr").find("td[Id='CItemId']").text();
            var iAGrpId = $(drp).closest("tr").find("td[Id='AGrpId']").text();
            var userid = '<%= Session("UserId") %>';

            var ivalue = 0;
            var irmark = "";
            var iagscr = 0;

            if (ScoringOptionId == "1") {
                if ($(drp).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0]) {
                ivalue = $(drp).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0].value;
                }

                if (IsUseAgreedScore.toLowerCase() == 'true') {
                    if ($(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0]) {
                        iagscr = $(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value;
                    }
                }
                if (iagscr.length === 0) {
                    if (IsUseAgreedScore.toLowerCase() == 'true') {
                        if ($(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0])
                        { $(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value = ivalue; }
                        iagscr = ivalue;
                    }
                }
            }
            else if (ScoringOptionId == "2") {
                if ($(drp).closest("tr.griviewitem").find("td[Id='ascr']").find("select")[0]) {
                ivalue = $(drp).closest("tr.griviewitem").find("td[Id='ascr']").find("select")[0].value;
                }
                if (IsUseAgreedScore.toLowerCase() == 'true') {
                    if ($(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0]) {
                        iagscr = $(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0].value;
                    }
                }
                if (isNaN(iagscr) === true) {
                    if (IsUseAgreedScore.toLowerCase() == 'true') {
                        if ($(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0]) {
                        $(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0].value = ivalue;
                        }
                        iagscr = ivalue;
                    }
                }
            }
            if ($(drp).closest("tr").find("textarea")[0]) {
            irmark = $(drp).closest("tr").find("textarea")[0].value;
            }

            if (ScoringOptionId == "1") {
                var wgt = $(drp).closest("tr").find("td[Id='bWeight']").text();
                //'S.SANDEEP |10-AUG-2020| -- START
                //ISSUE/ENHANCEMENT : SPRINT-4 {2020}
                //if (parseFloat($(drp).val()) > parseFloat(wgt)) {
                //'S.SANDEEP |10-AUG-2020| -- END
                if (parseFloat(ivalue) > parseFloat(wgt)) {
                    alert('Sorry, You cannot enter score beyond the weight set for the selected goal. Please set proper score.');
                    $(drp).val('');
                    $(drp).focus();
                    if ($(drp).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0]) {
                    $(drp).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0].value = "";
                    }

                    if (IsUseAgreedScore.toLowerCase() == 'true') {
                        if ($(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0]) {
                            $(drp).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value = "";
                        }
                    }
                    return;
                }
            }
            else if (ScoringOptionId == "2") {
                if (isNaN(ivalue) == true) {
                    ivalue = 0;
                    return;
                }
            }

            //            var itemWgt = $(drp).closest("tr").find("td[Id='bWeight']").text();
            //            var iMaxScl = $(drp).closest("tr").find("td[Id='bmax']").text();
            //            var btrnId = $(drp).closest("tr").find("td[Id='btrnId']").text();

            //            PageMethods.ComputeScoreGE(ScoringOptionId, $(prd).val(), $(emp).val(), EmployeeAsOnDate, IsUseAgreedScore, Self_Assign_Competencies, parseFloat(ivalue), itemid, iAGrpId, btrnId, irmark, itemWgt, iMaxScl, $(dtp).val(), iagscr, userid, $(asr).val(), onSuccess, onFailure);

            //            function onSuccess(str) {
            //                result = parseFloat(str);
            //                test.innerHTML = result.toFixed(2);
            //            }

            //            function onFailure(err) {
            //                alert(err.get_message());
            //            }
        }

        function change_event(drp) {

            var ctrl = document.getElementById(drp);
            if (ctrl === null) { ctrl = drp; }
            var ScoringOptionId = '<%= Session("ScoringOptionId") %>';
            var asr = document.getElementById("<%=cboAssessor.ClientID%>");
            var prd = document.getElementById("<%=cboPeriod.ClientID%>");
            var dtp = document.getElementById("<%= dtpAssessdate.ClientID %>" + '_TxtDate');
            var emp = document.getElementById("<%=cboEmployee.ClientID%>");
            var EmployeeAsOnDate = '<%= Session("EmployeeAsOnDate") %>';
            var IsUseAgreedScore = '<%= Session("IsUseAgreedScore") %>';
            var AutoRating = '<%= Session("EnableBSCAutomaticRating") %>';
            //S.SANDEEP |17-MAY-2021| -- START
            //ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
            var auto100 = '<%= Session("DontAllowRatingBeyond100") %>';
            var sysgen = '<%= Session("DontAllowToEditScoreGenbySys") %>'
            //S.SANDEEP |17-MAY-2021| -- END
            var Self_Assign_Competencies = '<%= Session("Self_Assign_Competencies") %>';
            //            var test = document.getElementById("<%= objlblBSCScr.ClientID %>");
            //            var result = test.innerHTML;
            var row = $(ctrl).closest("tr");
            var ilnkid = $(ctrl).closest("tr").find("td[Id='LinkedField']").text();
            var tds = $(ctrl).closest("tr").find("td[Id='objdgcolhempf" + ilnkid + "']").text();
            var userid = '<%= Session("UserId") %>';

            var ivalue = 0;
            var irmark = "";
            var iagscr = 0;

            if (ScoringOptionId == "1") {
                if ($(ctrl).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0]) {
                ivalue = $(ctrl).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0].value;
                }
                if (IsUseAgreedScore.toLowerCase() == 'true') {
                    if ($(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0]) {
                    iagscr = $(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value;
                    }
                    if (iagscr.length === 0) {
                        $(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value = ivalue;
                        iagscr = ivalue;
                    }
                }
            }
            else if (ScoringOptionId == "2") {
                if ($(ctrl).closest("tr.griviewitem").find("td[Id='ascr']").find("select")[0]) {
                ivalue = $(ctrl).closest("tr.griviewitem").find("td[Id='ascr']").find("select")[0].value;
                }
                if (IsUseAgreedScore.toLowerCase() == 'true') {
                    if ($(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0]) {
                    iagscr = $(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0].value;
                    }
                    if (isNaN(iagscr) === true) {
                        $(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("select")[0].value = ivalue;
                        iagscr = ivalue;
                    }
                }
            }
            if ($(ctrl).closest("tr").find("textarea")[0]) {
            irmark = $(ctrl).closest("tr").find("textarea")[0].value;
            }
            if (ScoringOptionId == "1") {
                if (AutoRating == 'False') {
                    var wgt = $(ctrl).closest("tr").find("td[Id='bWeight']").text();
                    if (parseFloat(ivalue) > parseFloat(wgt)) {
                        alert('Sorry, You cannot enter score beyond the weight set for the selected goal. Please set proper score.');
                        $(ctrl).val('');
                        $(ctrl).focus();
                        $(ctrl).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0].value = "";
                        if (IsUseAgreedScore.toLowerCase() == 'true') { $(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value = ""; }
                        return;
                    }
                }
                else if (AutoRating.toLowerCase() == 'true') {
                    if (auto100.toLowerCase() == 'true') {
                        if (parseFloat(ivalue) > 100) {
                            $(ctrl).closest("tr.griviewitem").find("td[Id='ascr']").find("input")[0].value = '100';
                            if (IsUseAgreedScore.toLowerCase() == 'true') { $(ctrl).closest("tr.griviewitem").find("td[Id='agsr']").find("input")[0].value = '100'; }
                        }
                    }
                }
            }
            else if (ScoringOptionId == "2") {
                if (isNaN(ivalue) == true) {
                    ivalue = 0;
                    return;
                }
            }

            //            var empfld1 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf1']").text();
            //            var empfld2 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf2']").text();
            //            var empfld3 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf3']").text();
            //            var empfld4 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf4']").text();
            //            var empfld5 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf5']").text();
            //            var itemWgt = $(ctrl).closest("tr").find("td[Id='bWeight']").text();
            //            var iMaxScl = $(ctrl).closest("tr").find("td[Id='bmax']").text();
            //            var btrnId = $(ctrl).closest("tr").find("td[Id='btrnId']").text();
            //            var bprsid = $(ctrl).closest("tr").find("td[Id='bpId']").text();

            //            PageMethods.ComputeScoreBSC(ScoringOptionId, $(prd).val(), $(emp).val(), EmployeeAsOnDate, IsUseAgreedScore, Self_Assign_Competencies, ivalue, tds, btrnId, bprsid, empfld1, empfld2, empfld3, empfld4, empfld5, irmark, itemWgt, iMaxScl, $(dtp).val(), iagscr, userid, $(asr).val(), onSuccess, onFailure);

            //            function onSuccess(str) {
            //                result = parseFloat(str);
            //                test.innerHTML = result.toFixed(2);
            //            }

            //            function onFailure(err) {
            //                alert(err.get_message());
            //            }
        }
    </script>

    <asp:HiddenField ID="hdf_topposition" runat="server" />
    <asp:HiddenField ID="hdf_leftposition" runat="server" />

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });


        var xPos, yPos, xmainPos, ymainPos;
        function beginRequestHandler(sender, args) {
            debugger;
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());

            if ($get('<%=objpnlAssesmentItem.ClientID%>') != null) {
                xPos = $get('<%=objpnlAssesmentItem.ClientID%>').scrollLeft;
                yPos = $get('<%=objpnlAssesmentItem.ClientID%>').scrollTop;
            }

            if ($get('<%=pnlmain.ClientID%>') != null) {
                xmainPos = $get('<%=pnlmain.ClientID%>').scrollLeft;
                ymainPos = $get('<%=pnlmain.ClientID%>').scrollTop;
            }


            if ($('#div7') != null && yPos <= 0) {
                xPos = $('#div7').scrollLeft();
                yPos = $('#div7').scrollTop();
            }

        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls()
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }

            if ($get('<%=objpnlAssesmentItem.ClientID%>') != null) {
                $get('<%=objpnlAssesmentItem.ClientID%>').scrollLeft = xPos;
                $get('<%=objpnlAssesmentItem.ClientID%>').scrollTop = yPos;
            }

            if ($get('<%=pnlmain.ClientID%>') != null) {
                $get('<%=pnlmain.ClientID%>').scrollLeft = xmainPos;
                $get('<%=pnlmain.ClientID%>').scrollTop = ymainPos;
            }
        }
    </script>

    <style type="text/css">
        .flex-container
        {
            display: flex;
            flex-direction: column;
            align-items: stretch;
        }
        .sub-flex-container
        {
            display: flex;
            flex-direction: column;
            align-items: stretch;
        }
    </style>
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Assessor Performance Evaluation"></asp:Label>
                        </div>
                        <div id="FilterCriteriaBody" class="panel-body-default">
                            <div class="row2">
                                <div style="width: 6%" class="ib">
                                    <asp:Label ID="lblAssessor" runat="server" Text="Assessor"></asp:Label>
                                </div>
                                <div style="width: 18%" class="ib">
                                    <asp:DropDownList ID="cboAssessor" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div style="width: 6%" class="ib">
                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                </div>
                                <div style="width: 18%" class="ib">
                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div style="width: 5%" class="ib">
                                    <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                </div>
                                <div style="width: 18%" class="ib">
                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div style="width: 3%" class="ib">
                                    <asp:Label ID="lblAssessDate" runat="server" Text="Date"></asp:Label>
                                </div>
                                <div style="width: 14%" class="ib">
                                    <uc2:DateCtrl ID="dtpAssessdate" runat="server" />
                                </div>
                                <div style="width: 0%" class="ib">
                                    <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" OnClientClick="set_location();"
                                        Visible="false" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <asp:Panel ID="pnlmain" runat="server" Width="100%" CssClass="panel-body flex-container gridscroll"
                            Style="max-height: 1400px">
                            <div id="divInstruction" class="panel-default" style="order: 1">
                                <div id="Div2" class="panel-heading-default" onclick="ChangeApplicantFilterImage('img1','divInstructionValue');">
                                    <div style="float: left;">
                                        <img id="img1" src="../../images/plus.png" alt="" />
                                        <asp:Label ID="lblInstruction" runat="server" Text="Assessment Instruction" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>
                                <div id="divInstructionValue" style="height: 400px; display: none; margin-left: 2px;
                                    margin-right: 2px">
                                    <asp:TextBox ID="txtInstruction" ReadOnly="true" runat="server" TextMode="MultiLine"
                                        Height="100%" Width="99%" CssClass="textarea" Wrap="true"></asp:TextBox>
                                </div>
                            </div>
                            <div id="div5" class="panel-default" style="order: 2">
                                <div id="Div6" class="panel-heading-default">
                                    <asp:Label ID="lblAssesmentItemHeader" runat="server" Text="Assessment Items" Font-Bold="true"></asp:Label>
                                </div>
                                <div id="div7" style="height: 100%; margin-left: 2px; margin-right: 2px">
                                    <asp:Panel ID="objpnlAssesmentItem" runat="server" Width="100%" CssClass="gridscroll sub-flex-container"
                                        ScrollBars="Auto" Style="max-height: 1263px">
                                        <div id="divBSC" class="panel-default" style="display: none">
                                            <div id="Div1" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblBSC" runat="server" Text="Objectives/Goals/Targets" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div style="float: right;">
                                                    <div class="row2" style="font-weight: bold;">
                                                        <div class="ib">
                                                            <asp:Label ID="objlblBSCWgt" runat="server" Text="Total Weight :" Visible="false"></asp:Label>
                                                        </div>
                                                        <div class="ib" style="display: none">
                                                            <asp:Label ID="objlblBSCScrCaption" runat="server" Text="Score : " Visible="false"></asp:Label>
                                                            <asp:Label ID="objlblBSCScr" runat="server" Visible="false"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divBSCValue" style="height: 400px; margin-left: 2px; margin-right: 2px">
                                                <asp:Panel ID="objpnlBSC" runat="server" Width="100%" CssClass="gridscroll" Style="max-width: 1130px;
                                                    max-height: 390px;" ScrollBars="Auto">
                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                        <asp:DataGrid ID="dgvBSC" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="Field1" HeaderText="" FooterText="objdgcolhBSCField1" />
                                                                <%--0--%>
                                                                <asp:BoundColumn DataField="Field2" HeaderText="" FooterText="objdgcolhBSCField2" />
                                                                <%--1--%>
                                                                <asp:BoundColumn DataField="Field3" HeaderText="" FooterText="objdgcolhBSCField3" />
                                                                <%--2--%>
                                                                <asp:BoundColumn DataField="Field4" HeaderText="" FooterText="objdgcolhBSCField4" />
                                                                <%--3--%>
                                                                <asp:BoundColumn DataField="Field5" HeaderText="" FooterText="objdgcolhBSCField5" />
                                                                <%--4--%>
                                                                <asp:BoundColumn DataField="Field6" HeaderText="" FooterText="objdgcolhBSCField6" />
                                                                <%--5--%>
                                                                <asp:BoundColumn DataField="Field7" HeaderText="" FooterText="objdgcolhBSCField7" />
                                                                <%--6--%>
                                                                <asp:BoundColumn DataField="Field8" HeaderText="" FooterText="objdgcolhBSCField8" />
                                                                <%--7--%>
                                                                <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" FooterText="dgcolhSDate" />
                                                                <%--8--%>
                                                                <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" FooterText="dgcolhEDate" />
                                                                <%--9--%>
                                                                <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="dgcolhCompleted" />
                                                                <%--10--%>
                                                                <asp:BoundColumn DataField="dgoaltype" HeaderText="Goal Type" ReadOnly="true" FooterText="dgcolhGoalType" />
                                                                <%--11--%>
                                                                <asp:BoundColumn DataField="dgoalvalue" HeaderText="Goal Value" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgoalvalue" />
                                                                <%--12--%>
                                                                <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                                                <%--13--%>
                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhBSCWeight" />
                                                                <%--14--%>
                                                                <asp:BoundColumn DataField="eself" HeaderText="" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolheselfBSC" />
                                                                <%--15--%>
                                                                <asp:BoundColumn DataField="eremark" HeaderText="eremark" FooterText="dgcolheremarkBSC" />
                                                                <%--16--%>
                                                                <asp:TemplateColumn HeaderText="Result" FooterText="dgcolhaselfBSC" HeaderStyle-Width="6%"
                                                                    ItemStyle-Width="6%">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhaselfBSC" runat="server" CssClass="removeTextcss decimal"
                                                                            Visible="false" Text='<%# Eval("aself") %>' Style="text-align: right" onblur="getBSCscrollPosition()"
                                                                            onchange="change_event(this);"></asp:TextBox>
                                                                        <asp:DropDownList ID="dgcolhSelBSCSel" runat="server" Visible="false" onblur="getBSCscrollPosition()"
                                                                            onchange="change_event(this);">
                                                                        </asp:DropDownList>
                                                                        <%--Width="98%"--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--17--%>
                                                                <asp:TemplateColumn HeaderText="Remark" FooterText="dgcolharemarkBSC">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolharemarkBSC" runat="server" CssClass="removeTextcss" Text='<%# Eval("aremark") %>'
                                                                            onblur="getBSCscrollPosition()" TextMode="MultiLine" Rows="3" onchange="change_event(this);"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--18--%>
                                                                <asp:TemplateColumn HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreBSC" HeaderStyle-Width="6%"
                                                                    ItemStyle-Width="6%">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhAgreedScoreBSC" runat="server" Text='<%# Eval("agreedscore") %>'
                                                                            Visible="false" CssClass="removeTextcss decimal" Style="text-align: right" onblur="getBSCscrollPosition()"
                                                                            onchange="change_event(this);"></asp:TextBox>
                                                                        <asp:DropDownList ID="dgcolhAgreedScoreSelBSCSel" runat="server" Width="98%" Visible="false"
                                                                            onblur="getBSCscrollPosition()" onchange="change_event(this);">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--19--%>
                                                                <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpBSC" Visible="false" />
                                                                <%--20--%>
                                                                <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdBSC" Visible="false" />
                                                                <%--21--%>
                                                                <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhScaleMasterId"
                                                                    Visible="false" />
                                                                <%--22--%>
                                                                <asp:BoundColumn DataField="empfield1unkid" FooterText="objdgcolhempfield1unkid" />
                                                                <%--23--%>
                                                                <asp:BoundColumn DataField="empfield2unkid" FooterText="objdgcolhempfield2unkid" />
                                                                <%--24--%>
                                                                <asp:BoundColumn DataField="empfield3unkid" FooterText="objdgcolhempfield3unkid" />
                                                                <%--25--%>
                                                                <asp:BoundColumn DataField="empfield4unkid" FooterText="objdgcolhempfield4unkid" />
                                                                <%--26--%>
                                                                <asp:BoundColumn DataField="empfield5unkid" FooterText="objdgcolhempfield5unkid" />
                                                                <%--27--%>
                                                                <asp:BoundColumn DataField="LinkedFieldId" FooterText="objdgcolhLinkedFieldId" />
                                                                <%--28--%>
                                                                <asp:BoundColumn DataField="" FooterText="objdgcolhMaxScale" />
                                                                <%--29--%>
                                                                <asp:BoundColumn DataField="analysistranunkid" FooterText="objdgcolhanalysistranunkid">
                                                                </asp:BoundColumn>
                                                                <%--30--%>
                                                                <asp:BoundColumn DataField="perspectiveunkid" FooterText="objdgcolperspectiveunkid">
                                                                </asp:BoundColumn>
                                                                <%--31--%>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div id="divGE" class="panel-default" style="display: none">
                                            <div id="Div3" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <asp:Label ID="lblGE" runat="server" Text="Competencies" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div style="float: right;">
                                                    <div class="row2" style="font-weight: bold;">
                                                        <div class="ib">
                                                            <asp:Label ID="objlblGEWgt" runat="server" Text="Total Weight :" Visible="false"></asp:Label>
                                                        </div>
                                                        <div class="ib" style="display: none">
                                                            <asp:Label ID="objlblGEScrCaption" runat="server" Text="Score : " Visible="false"></asp:Label>
                                                            <asp:Label ID="objlblGEScr" runat="server" Visible="false"></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="divGEValue" style="height: 400px; margin-left: 2px; margin-right: 2px">
                                                <asp:Panel ID="objpnlGE" runat="server" Width="100%" ScrollBars="Auto" Style="max-height: 390px;">
                                                    <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                        <asp:DataGrid ID="dgvGE" runat="server" Width="99%" AutoGenerateColumns="false" CssClass="gridview"
                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                                    ItemStyle-Width="45%" HeaderStyle-Width="45%" />
                                                                <%--0--%>
                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                                    ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <%--1--%>
                                                                <asp:BoundColumn DataField="eself" HeaderText="Self -Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolheselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <%--2--%>
                                                                <asp:BoundColumn DataField="escore" HeaderText="Final Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="objdgcolhedisplayGE" Visible="false" />
                                                                <%--3--%>
                                                                <asp:BoundColumn DataField="eremark" HeaderText="Self -Remark" FooterText="dgcolheremarkGE"
                                                                    ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                                <%--4--%>
                                                                <asp:TemplateColumn HeaderText="Result" FooterText="dgcolhaselfGE" ItemStyle-Width="6%"
                                                                    HeaderStyle-Width="6%">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhaselfGE" runat="server" Text='<%# Eval("aself") %>' Style="text-align: right"
                                                                            Visible="false" CssClass="removeTextcss" Width="100%" onblur="getGEscrollPosition()"
                                                                            onchange="change_event_GE(this);"></asp:TextBox>
                                                                        <asp:DropDownList ID="dgcolhSelGESel" runat="server" Width="98%" Visible="false"
                                                                            onblur="getBSCscrollPosition()" onchange="change_event_GE(this);">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--5--%>
                                                                <asp:BoundColumn DataField="" HeaderText="Final Score" FooterText="objdgcolhadisplayGE"
                                                                    HeaderStyle-Width="100px" Visible="false" />
                                                                <%--6--%>
                                                                <asp:TemplateColumn HeaderText="Remark" FooterText="dgcolharemarkGE" ItemStyle-Width="15%"
                                                                    HeaderStyle-Width="15%">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolharemarkGE" runat="server" Text='<%# Eval("aremark") %>' Width="100%"
                                                                            CssClass="removeTextcss" onblur="getGEscrollPosition()" TextMode="MultiLine"
                                                                            Rows="3" onchange="change_event_GE(this);"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--7--%>
                                                                <asp:TemplateColumn HeaderText="Agreed Score" FooterText="dgcolhaAgreedScoreGE" ItemStyle-Width="6%"
                                                                    HeaderStyle-Width="6%">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhaAgreedScoreGE" runat="server" Style="text-align: right" CssClass="removeTextcss"
                                                                            Width="100%" onblur="getGEscrollPosition()" Text='<%# Eval("agreedscore") %>'
                                                                            onchange="change_event_GE(this);" Visible="false">
                                                                        </asp:TextBox>
                                                                        <asp:DropDownList ID="dgcolhaAgreedScoreSelGESel" runat="server" Width="98%" Visible="false"
                                                                            onblur="getBSCscrollPosition()" onchange="change_event_GE(this);">
                                                                        </asp:DropDownList>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--8--%>
                                                                <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhscalemasterunkidGE"
                                                                    Visible="false" />
                                                                <%--9--%>
                                                                <asp:BoundColumn DataField="competenciesunkid" FooterText="objdgcolhcompetenciesunkidGE" />
                                                                <%--10--%>
                                                                <asp:BoundColumn DataField="assessgroupunkid" FooterText="objdgcolhassessgroupunkidGE" />
                                                                <%--11--%>
                                                                <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                                <%--12--%>
                                                                <asp:BoundColumn DataField="IsPGrp" FooterText="objdgcolhIsPGrpGE" Visible="false" />
                                                                <%--13--%>
                                                                <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                                <%--14--%>
                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkCol" runat="server" CommandName="viewdescription" Font-Underline="false"
                                                                            Enabled="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--15--%>
                                                                <asp:BoundColumn DataField="" FooterText="objdgcolhMaxScale" />
                                                                <%--16--%>
                                                                <asp:BoundColumn DataField="analysistranunkid" FooterText="objdgcolhanalysistranunkid">
                                                                </asp:BoundColumn>
                                                                <%--17--%>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                        <div id="divCItem" class="panel-default" style="display: none">
                                            <div id="Div4" class="panel-heading-default">
                                                <div style="float: left;">
                                                    <img id="img4" src="../../images/plus.png" alt="" style="display: none" />
                                                    <asp:Label ID="lblCItem" runat="server" Text="Custom Section" Font-Bold="true"></asp:Label>
                                                </div>
                                            </div>
                                            <div id="divCItemValue" style="height: 300px; overflow: auto; margin-left: 2px; margin-right: 2px;
                                                position: relative;">
                                                <div class="row2" style="margin: 10px; font-weight: bold">
                                                    <div style="padding: 10px; background-color: #DDD; color: #000; font-weight: bold">
                                                        <asp:Label ID="lblCustomHeaderval" runat="server" Text="Section Header(s)"></asp:Label>
                                                    </div>
                                                </div>
                                                <hr />
                                                <div class="row2">
                                                    <div style="display: flex; justify-content: flex-end; width: 100%;">
                                                        <asp:LinkButton ID="lnkprevious" runat="server" CssClass="btndefault" Style="text-align: center;
                                                            min-width: 30px; margin: 0 10px" ToolTip="Previous">
                                                        <i class="fa fa-arrow-circle-left"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="lnkNext" runat="server" CssClass="btndefault" Style="text-align: center;
                                                            min-width: 30px; margin: 0 10px" ToolTip="Next">
                                                        <i class="fa fa-arrow-circle-right"></i>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <asp:GridView ID="dgvItems" runat="server" Width="99.5%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        ItemStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                        DataKeyNames="analysisunkid,viewmodeid,periodunkid,itemtypeid,selectionmodeid,GUID,ismanual,Header_Id,Header_Name,Is_Allow_Multiple">
                                                        <Columns>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="btn-default">
                            <asp:Button ID="btnSaveCommit" runat="server" CssClass="btndefault" Text="Submit" />
                            <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                        </div>
                        <div id="divCItemAddEdit">
                            <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                                CancelControlID="hdf_cItem" DropShadow="true" BackgroundCssClass="ModalPopupBG"
                                PopupControlID="pnl_CItemAddEdit" Drag="True">
                            </cc1:ModalPopupExtender>
                            <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="newpopup" Style="display: none;
                                width: 750px;">
                                <div class="panel-primary" style="margin-bottom: 0px">
                                    <div class="panel-heading">
                                        <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                                    </div>
                                    <div class="panel-body">
                                        <div id="Div10" class="panel-default">
                                            <div id="Div12" class="panel-body-default">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%;">
                                                            <asp:Panel ID="pnl_dgv_Citems" runat="server" Style="height: 360px !important" ScrollBars="Auto">
                                                                <asp:DataGrid ID="dgv_Citems" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                                    <Columns>
                                                                        <asp:BoundColumn HeaderText="Custom Items" DataField="custom_item" FooterText="dgcolhItems"
                                                                            HeaderStyle-Width="450px" ItemStyle-Width="450px"></asp:BoundColumn>
                                                                        <%--0--%>
                                                                        <asp:TemplateColumn FooterText="dgcolhValue" HeaderText="Custom Value" HeaderStyle-Width="250px"
                                                                            ItemStyle-Width="250px">
                                                                            <ItemTemplate>
                                                                                <%--<asp:TextBox ID="txtFreetext" runat="server" TextMode="MultiLine" Rows="3" Width="100%"
                                                                                    Text='<%# Eval("custom_value") %>' AutoPostBack="false" CssClass="removeTextcss"
                                                                                    onchange="isvalid_value(this);" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="cboSelection" runat="server" Width="238px" AutoPostBack="false"
                                                                                    onchange="isvalid_value(this);" Visible="false">
                                                                                </asp:DropDownList>
                                                                                <uc2:DateCtrl ID="dtpSelection" runat="server" AutoPostBack="false" Visible="false"
                                                                                    onchange="isvalid_value(this);" />
                                                                                <uc4:NumericText ID="txtNUM" runat="server" Width="100%" AutoPostBack="false" cssclass="removeTextcss"
                                                                                    onchange="isvalid_value(this);" Text='<%# Eval("custom_value") %>' Visible="false" />--%>
                                                                                    <asp:TextBox ID="txtFreetext" runat="server" TextMode="MultiLine" Rows="3" Width="100%"
                                                                                    Text='<%# Eval("custom_value") %>' CssClass="removeTextcss" Visible="false"></asp:TextBox>
                                                                                <asp:DropDownList ID="cboSelection" runat="server" Width="238px" Visible="false">
                                                                                </asp:DropDownList>
                                                                                <uc2:DateCtrl ID="dtpSelection" runat="server" Visible="false" />
                                                                                <uc4:NumericText ID="txtNUM" runat="server" Width="100%" cssclass="removeTextcss"
                                                                                    Text='<%# Eval("custom_value") %>' Visible="false" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateColumn>
                                                                        <%--1--%>
                                                                        <asp:BoundColumn DataField="itemtypeid" Visible="false" FooterText="clmCntType">
                                                                        </asp:BoundColumn>
                                                                        <%--2--%>
                                                                        <asp:BoundColumn DataField="rOnly" Visible="false" FooterText="clmrOnly"></asp:BoundColumn>
                                                                        <%--3--%>
                                                                        <asp:BoundColumn DataField="customitemunkid" FooterText="objdgcustomitemunkid"></asp:BoundColumn>
                                                                        <%--4--%>
                                                                        <asp:BoundColumn DataField="selectionmodeid" Visible="false"></asp:BoundColumn>
                                                                        <%--5--%>
                                                                        <asp:BoundColumn DataField="isdefaultentry" Visible="false"></asp:BoundColumn>
                                                                        <%--6--%>
                                                                        <%--'S.SANDEEP |08-JAN-2019| -- START--%>
                                                                        <asp:BoundColumn DataField="iscompletedtraining" Visible="false"></asp:BoundColumn>
                                                                        <%--7--%>
                                                                        <%--'S.SANDEEP |08-JAN-2019| -- END--%>
                                                                        <asp:BoundColumn DataField="selectedid" Visible="false" FooterText="objdgcolhselectedid">
                                                                        </asp:BoundColumn>
                                                                        <%--8--%>
                                                                        <asp:BoundColumn DataField="ddate" Visible="false" FooterText="objdgcolhddate"></asp:BoundColumn>
                                                                        <%--9--%>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div class="btn-default">
                                                    <asp:HiddenField ID="hdf_cItem" runat="server" />
                                                    <asp:Button ID="btnIAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                    <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btndefault" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <uc5:DelReason ID="delCUstomItem" runat="server" />
                    <uc3:CnfCtrl ID="cnfSubmit" runat="server" Title="Aruti" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
