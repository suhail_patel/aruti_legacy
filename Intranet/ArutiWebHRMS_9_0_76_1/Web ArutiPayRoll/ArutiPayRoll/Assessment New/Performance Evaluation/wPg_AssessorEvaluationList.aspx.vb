﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
#End Region
Partial Class wPg_AssessorEvaluationList
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmAssessorEvaluationList"
    'S.SANDEEP |25-MAR-2019| -- START
    Private mstrModuleName1 As String = "frmPerformanceEvaluation"
    'S.SANDEEP |25-MAR-2019| -- END
    Private objEvaluation As New clsevaluation_analysis_master
    Private mstrAdvanceFilter As String = String.Empty
    Private DisplayMessage As New CommonCodes
    'S.SANDEEP [18 DEC 2015] -- START
    Private mintAnalysisUnkid As Integer = 0
    'S.SANDEEP [18 DEC 2015] -- END
#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call SetVisibility()
                Call FillCombo()

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Assessor_Filter") IsNot Nothing Then
                    cboAssessor.SelectedValue = CStr(Session("Assessor_Filter")).Split("-")(0).Trim
                    Call cboAssessor_SelectedIndexChanged(cboAssessor.SelectedValue, Nothing)
                    cboEmployee.SelectedValue = CStr(Session("Assessor_Filter")).Split("-")(1).Trim
                    Session.Remove("Assessor_Filter")
                    Call btnSearch_Click(btnSearch, Nothing)
                End If
                'SHANI [09 Mar 2015]--END 

                'S.SANDEEP [18 DEC 2015] -- START
            Else
                mintAnalysisUnkid = Me.ViewState("mintAnalysisUnkid")
                'S.SANDEEP [18 DEC 2015] -- END

            End If
            If Me.ViewState("AdvanceFilter") IsNot Nothing Then
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("AdvanceFilter") Is Nothing Then
                Me.ViewState.Add("AdvanceFilter", mstrAdvanceFilter)
            Else
                Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            End If

            'S.SANDEEP [18 DEC 2015] -- START
            Me.ViewState("mintAnalysisUnkid") = mintAnalysisUnkid
            'S.SANDEEP [18 DEC 2015] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objYear As New clsMasterData
        Dim dsCombo As DataSet = Nothing
        Dim mstrAssessorIds As String = String.Empty
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEvaluation.getAssessorComboList("List", True, False, Session("UserId"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombo = objEvaluation.getAssessorComboList(Session("Database_Name"), _
            '                                             Session("UserId"), _
            '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
            '                                             Session("IsIncludeInactiveEmp"), "List", True, False)

            dsCombo = objEvaluation.getAssessorComboList(Session("Database_Name"), _
                                                                              Session("UserId"), _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), True, _
                                                         Session("IsIncludeInactiveEmp"), "List", _
                                                         clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

            'Shani (12-Jan-2017) --[clsAssessor.enARVisibilityTypeId.VISIBLE]


            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(24-Aug-2015) -- End

            With cboAssessor
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                'Shani(14-Sep-2015) -- Start
                'Issue: TRA Training Comments & Changes Requested By Dennis
                '.SelectedValue = 0
                .SelectedIndex = 0
                'Shani(14-Sep-2015) -- End
            End With


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsCombo = objYear.getComboListPAYYEAR(Session("Fin_year"), Session("FinancialYear_Name"), Session("CompanyUnkId"), "Year", True, True)
            'Shani(20-Nov-2015) -- End

            With cboYear
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Year")
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP [17 NOV 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 0)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 0, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "Period", True, 0)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "Period", True, 0)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'Shani(24-Dec-2015) -- Start
            'Enhancement - CCBRT WORK

            'Shani (09-May-2016) -- Start
            'Dim intPeriodUnkID As Integer = 0
            'Integer.TryParse(New DataView(dsCombo.Tables(0), "end_date <= " & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime), "end_date DESC", DataViewRowState.CurrentRows).ToTable.Rows(0)("periodunkid").ToString, intPeriodUnkID)
            Dim intPeriodUnkID As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            'Shani(24-Dec-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("Period")
                .DataBind()

                'Shani(24-Dec-2015) -- Start
                'Enhancement - CCBRT WORK
                '.SelectedValue = 0
                .SelectedValue = intPeriodUnkID
                'Shani(24-Dec-2015) -- End

            End With

            'S.SANDEEP [18 DEC 2015] -- START
            Dim mdicViewType As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)
            mdicViewType.Add(0, "Select")
            mdicViewType.Add(clsevaluation_analysis_master.enAssessmentType.ASSESSMENT_NOT_DONE, "Pending Assessment")
            mdicViewType.Add(clsevaluation_analysis_master.enAssessmentType.ASSESSMENT_ON_GOING, "On-Going Assessment")
            mdicViewType.Add(clsevaluation_analysis_master.enAssessmentType.ASSESSMENT_COMMITTED, "Committed Assessment")
            With cboViewType
                .DataSource = mdicViewType
                .DataTextField = "Value"
                .DataValueField = "Key"
                .DataBind()
                .SelectedValue = 0
            End With
            'S.SANDEEP [18 DEC 2015] -- END

            Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)

            'S.SANDEEP [31 DEC 2015] -- START
            If CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)) > 0 AndAlso CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                Call btnSearch_Click(New Object, New EventArgs)
            End If
            'S.SANDEEP [31 DEC 2015] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnNew.Visible = CBool(Session("AddAssessmentAnalysis"))


            'S.SANDEEP [18 DEC 2015] -- START
            'btnNew.Visible = CBool(Session("AllowtoAddAssessorEvaluation"))
            btnNew.Visible = False
            'S.SANDEEP [18 DEC 2015] -- END

            'S.SANDEEP [28 MAY 2015] -- END

            'S.SANDEEP [04 JUN 2015] -- START
            chkShowcommitted.Checked = True
            chkShowUncommitted.Checked = True
            'S.SANDEEP [04 JUN 2015] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP [18 DEC 2015] -- START
    'Private Sub FillList()
    '    Dim dsList As New DataSet
    '    Dim StrSearching As String = String.Empty
    '    Dim dtTable As DataTable
    '    Try

    '        'S.SANDEEP [28 MAY 2015] -- START
    '        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    '        'If CBool(Session("AllowToViewAssessorAssessmentList")) = True Then
    '        If CBool(Session("AllowtoViewAssessorEvaluationList")) = True Then
    '            'S.SANDEEP [28 MAY 2015] -- END


    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsList = objEvaluation.GetList("List", enAssessmentMode.APPRAISER_ASSESSMENT, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("UserId"))

    '            'If CInt(cboEmployee.SelectedValue) > 0 Then
    '            '    StrSearching &= "AND assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
    '            'End If

    '            'If CInt(cboAssessor.SelectedValue) > 0 Then
    '            '    StrSearching &= "AND assessormasterunkid = " & CInt(cboAssessor.SelectedValue)
    '            'End If

    '            'If CInt(cboYear.SelectedValue) > 0 Then
    '            '    StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
    '            'End If

    '            'If CInt(cboPeriod.SelectedValue) > 0 Then
    '            '    StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
    '            'End If

    '            'If dtpDate.IsNull = False Then
    '            '    StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpDate.GetDate) & "'"
    '            'End If

    '            'If chkShowcommitted.Checked = True And chkShowUncommitted.Checked = False Then
    '            '    StrSearching &= "AND iscommitted = " & True & " "
    '            'ElseIf chkShowcommitted.Checked = False And chkShowUncommitted.Checked = True Then
    '            '    StrSearching &= "AND iscommitted = " & False & " "
    '            'End If

    '            'If mstrAdvanceFilter.Length > 0 Then
    '            '    StrSearching &= "AND " & mstrAdvanceFilter
    '            'End If

    '            'If StrSearching.Length > 0 Then
    '            '    StrSearching = StrSearching.Substring(3)
    '            '    dtTable = New DataView(dsList.Tables(0), StrSearching, "assessoremployeeunkid", DataViewRowState.CurrentRows).ToTable
    '            'Else
    '            '    dtTable = New DataView(dsList.Tables(0), "", "assessoremployeeunkid", DataViewRowState.CurrentRows).ToTable
    '            'End If

    '            If CInt(cboEmployee.SelectedValue) > 0 Then
    '                StrSearching &= "AND hrevaluation_analysis_master.assessedemployeeunkid = " & CInt(cboEmployee.SelectedValue)
    '            End If

    '            If CInt(cboAssessor.SelectedValue) > 0 Then
    '                StrSearching &= "AND hrevaluation_analysis_master.assessormasterunkid = " & CInt(cboAssessor.SelectedValue)
    '            End If

    '            If CInt(cboYear.SelectedValue) > 0 Then
    '                StrSearching &= "AND cfcommon_period_tran.yearunkid = " & CInt(cboYear.SelectedValue)
    '            End If

    '            If CInt(cboPeriod.SelectedValue) > 0 Then
    '                StrSearching &= "AND hrevaluation_analysis_master.periodunkid = " & CInt(cboPeriod.SelectedValue)
    '            End If

    '            If dtpDate.IsNull = False Then
    '                StrSearching &= "AND CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) = '" & eZeeDate.convertDate(dtpDate.GetDate) & "'"
    '            End If

    '            If chkShowcommitted.Checked = True And chkShowUncommitted.Checked = False Then
    '                StrSearching &= "AND ISNULL(iscommitted,0) = " & True & " "
    '            ElseIf chkShowcommitted.Checked = False And chkShowUncommitted.Checked = True Then
    '                StrSearching &= "AND ISNULL(iscommitted,0) = " & False & " "
    '            End If

    '            If mstrAdvanceFilter.Length > 0 Then
    '                StrSearching &= "AND " & mstrAdvanceFilter
    '            End If

    '            If StrSearching.Length > 0 Then
    '                StrSearching = StrSearching.Substring(3)
    '            End If

    '            dsList = objEvaluation.GetList(Session("Database_Name"), _
    '                                           Session("UserId"), _
    '                                           Session("Fin_year"), _
    '                                           Session("CompanyUnkId"), _
    '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                           Session("UserAccessModeSetting"), True, _
    '                                           Session("IsIncludeInactiveEmp"), "List", _
    '                                           enAssessmentMode.APPRAISER_ASSESSMENT, StrSearching)

    '            dtTable = New DataView(dsList.Tables(0), "", "assessoremployeeunkid", DataViewRowState.CurrentRows).ToTable




    '            'Shani(24-Aug-2015) -- End

    '            'S.SANDEEP [28 MAY 2015] -- START
    '            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    '            'lvAssesmentList.Columns(0).Visible = CBool(Session("EditAssessmentAnalysis"))
    '            'lvAssesmentList.Columns(1).Visible = CBool(Session("DeleteAssessmentAnalysis"))
    '            'lvAssesmentList.Columns(2).Visible = CBool(Session("Allow_UnlockCommittedGeneralAssessment"))

    '            lvAssesmentList.Columns(0).Visible = CBool(Session("AllowtoEditAssessorEvaluation"))
    '            lvAssesmentList.Columns(1).Visible = CBool(Session("AllowtoDeleteAssessorEvaluation"))
    '            lvAssesmentList.Columns(2).Visible = CBool(Session("AllowtoUnlockcommittedAssessorEvaluation"))
    '            'S.SANDEEP [28 MAY 2015] -- END



    '            'S.SANDEEP [21 JAN 2015] -- START
    '            'If CBool(Session("ConsiderItemWeightAsNumber")) Then
    '            '    lvAssesmentList.Columns(8).Visible = True
    '            '    lvAssesmentList.Columns(9).Visible = False
    '            'Else
    '            '    lvAssesmentList.Columns(8).Visible = False
    '            '    lvAssesmentList.Columns(9).Visible = True
    '            'End If
    '            'S.SANDEEP [21 JAN 2015] -- END


    '            lvAssesmentList.AutoGenerateColumns = False
    '            lvAssesmentList.DataSource = dtTable
    '            lvAssesmentList.DataBind()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("FillList :-" & ex.Message, Me)
    '    Finally
    '    End Try
    'End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try
            If CBool(Session("AllowtoViewAssessorEvaluationList")) = True Then

                dtTable = objEvaluation.GetListNew(Session("Database_Name"), _
                                                   Session("UserId"), _
                                                   Session("Fin_year"), _
                                                   Session("CompanyUnkId"), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                   Session("UserAccessModeSetting"), True, _
                                                   Session("IsIncludeInactiveEmp"), _
                                                   "List", _
                                                   cboPeriod.SelectedValue, _
                                                   enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                   cboAssessor.SelectedValue, _
                                                   CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), _
                                                   CInt(IIf(cboYear.SelectedValue = "", 0, cboYear.SelectedValue)), _
                                                   IIf(dtpDate.IsNull = True, Nothing, dtpDate.GetDate), _
                                                   CInt(IIf(cboViewType.SelectedValue = "", 0, cboViewType.SelectedValue)), _
                                                   mstrAdvanceFilter, clsevaluation_analysis_master.enPAEvalTypeId.EO_NONE)


                'S.SANDEEP [21-NOV-2017] -- START
                ''Shani(06-Feb-2016) -- Start
                ''PA Changes Given By CCBRT
                ''lvAssesmentList.Columns(0).Visible = CBool(Session("AllowtoEditAssessorEvaluation"))
                'lvAssesmentList.Columns(0).Visible = CBool(Session("AllowtoAddAssessorEvaluation"))
                ''Shani(06-Feb-2016) -- End
                'lvAssesmentList.Columns(1).Visible = CBool(Session("AllowtoDeleteAssessorEvaluation"))
                'lvAssesmentList.Columns(2).Visible = CBool(Session("AllowtoUnlockcommittedAssessorEvaluation"))

                lvAssesmentList.Columns(1).Visible = CBool(Session("AllowtoAddAssessorEvaluation"))
                lvAssesmentList.Columns(2).Visible = CBool(Session("AllowtoDeleteAssessorEvaluation"))
                lvAssesmentList.Columns(3).Visible = CBool(Session("AllowtoUnlockcommittedAssessorEvaluation"))
                'S.SANDEEP [21-NOV-2017] -- END



                lvAssesmentList.AutoGenerateColumns = False
                lvAssesmentList.DataSource = dtTable
                lvAssesmentList.DataBind()
            End If

            'S.SANDEEP |21-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
            If CBool(Session("IsCompanyNeedReviewer")) = False Then
                lvAssesmentList.Columns(9).Visible = False
            End If
            'S.SANDEEP |21-AUG-2019| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'S.SANDEEP [18 DEC 2015] -- END

    'Shani (24-May-2016) -- Start
    Private Sub ItemProcess()
        Dim strCommand As String = ""
        Dim intRowIndex As Integer = -1
        Try
            strCommand = hdf_CommondName.Value
            Integer.TryParse(hdf_ItemIndex.Value, intRowIndex)
            If lvAssesmentList.Items.Count > 0 Then
                If strCommand.ToUpper = "UNLOCK" Then

                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text) <= 0 Then
                        DisplayMessage.DisplayMessage("Sorry, you have not done the assessment for the this employee for the selected period.", Me)
                        Exit Sub
                    End If

                    If CBool(lvAssesmentList.Items(intRowIndex).Cells(30).Text) = False Then
                        DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not committed.", Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |22-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                    'If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                    '                         CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text), _
                    '                         CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text)) = True Then
                    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
                    '    Exit Sub
                    'End If
                    If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                             CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text), _
                                             CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), , , , , , False) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |22-JUL-2019| -- END

                    'S.SANDEEP |13-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                    Dim xMessage As String = String.Empty
                    'S.SANDEEP |13-NOV-2019| -- END
                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(16).Text) = 1 Then
                        'S.SANDEEP |13-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                        'If objEvaluation.Unlock_Commit(CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), _
                        '                               CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text)) = True Then
                        If objEvaluation.Unlock_Commit(CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), _
                                                       CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text), xMessage) = True Then
                            'S.SANDEEP |13-NOV-2019| -- END
                            objEvaluation._Analysisunkid = CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text)
                            objEvaluation._Iscommitted = False
                            objEvaluation._Committeddatetime = Nothing
                            'S.SANDEEP [27-APR-2017] -- START
                            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                            'objEvaluation.Update()
                            objEvaluation.Update(Nothing, Nothing, Nothing, 0, Nothing, False, False, Nothing, False)
                            'S.SANDEEP [27-APR-2017] -- END
                            If objEvaluation._Message <> "" Then
                                DisplayMessage.DisplayMessage(objEvaluation._Message, Me)
                            End If
                            Call FillList()
                        Else
                            'S.SANDEEP |13-NOV-2019| -- START
                            'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal."), Me)
                            If xMessage.Trim.Length > 0 Then DisplayMessage.DisplayMessage(xMessage, Me)
                            'S.SANDEEP |13-NOV-2019| -- END
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed."), Me)
                        Exit Sub
                    End If

                ElseIf strCommand.ToUpper = "EDIT" Then
                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(16).Text) = 2 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), Me)
                        Exit Sub
                    End If

                    If CBool(lvAssesmentList.Items(intRowIndex).Cells(30).Text) = True Then
                        DisplayMessage.DisplayMessage("Sorry, you cannot do edit operation. Reason : Assessment is committed.", Me)
                        Exit Sub
                    End If

                    Session("Action") = enAction.EDIT_ONE
                    Session("Unkid") = lvAssesmentList.Items(intRowIndex).Cells(11).Text

                    If CInt(cboAssessor.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                        Session("Assessor_Filter") = cboAssessor.SelectedValue + "-" + cboEmployee.SelectedValue
                    End If

                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx", False)

                ElseIf strCommand.ToUpper = "DELETE" Then
                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(16).Text) = 2 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), Me)
                        Exit Sub
                    End If

                    If CBool(lvAssesmentList.Items(intRowIndex).Cells(30).Text) = True Then
                        DisplayMessage.DisplayMessage("Sorry, you cannot do delete operation. Reason : Assessment is committed.", Me)
                        Exit Sub
                    End If

                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text) <= 0 Then
                        DisplayMessage.DisplayMessage("Sorry, you have not done the assessment for the this employee for the selected period.", Me)
                        Exit Sub
                    End If

                    hdf_analysisunkid.Value = lvAssesmentList.Items(intRowIndex).Cells(11).Text
                    hdf_PeriodID.Value = lvAssesmentList.Items(intRowIndex).Cells(14).Text
                    lblMessage.Text = Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
                    lblTitle.Text = "Aruti"
                    txtMessage.Text = ""
                    popup_YesNo.Show()

                ElseIf strCommand.ToUpper = "ASSESS" Then

                    Dim sMsg As String = String.Empty
                    Dim intPeriodId As Integer = 0
                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(12).Text) > 0 Then
                        intPeriodId = lvAssesmentList.Items(intRowIndex).Cells(14).Text
                    Else
                        intPeriodId = cboPeriod.SelectedValue
                    End If

                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    'sMsg = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), lvAssesmentList.Items(intRowIndex).Cells(13).Text, intPeriodId, Session("Self_Assign_Competencies"), Session("IncludeCustomItemInPlanning)
                    'sMsg = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), lvAssesmentList.Items(intRowIndex).Cells(13).Text, intPeriodId, Session("Self_Assign_Competencies"))
                    'If sMsg.Trim.Length > 0 Then
                    '    DisplayMessage.DisplayMessage(sMsg, Me)
                    '    Exit Sub
                    'End If
                    'Shani (26-Sep-2016) -- End

                    'If CInt(lvAssesmentList.Items(intRowIndex).Cells(16).Text) = 2 Then
                    '    DisplayMessage.DisplayMessage("Sorry, you cannot assess this employee. Reason : Period is closed.", Me)
                    '    Exit Sub
                    'End If

                    'If CBool(lvAssesmentList.Items(intRowIndex).Cells(30).Text) = True Then
                    '    DisplayMessage.DisplayMessage("Sorry, you cannot do assess operation. Reason : Assessment is committed.", Me)
                    '    Exit Sub
                    'End If

                    'If Session("AllowAssessor_Before_Emp") = False Then
                    '    If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, lvAssesmentList.Items(intRowIndex).Cells(13).Text, lvAssesmentList.Items(intRowIndex).Cells(14).Text) = False Then
                    '        DisplayMessage.DisplayError(ex,Me)
                    '                                    "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period.", Me)
                    '        Exit Sub
                    '    End If
                    'End If

                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text) > 0 Then
                        cnfEditOperation.Title = "Aruti"
                        cnfEditOperation.Message = "You have already done the evaluation for this employee for the selected period. Would you like to open it in edit mode?"
                        mintAnalysisUnkid = lvAssesmentList.Items(intRowIndex).Cells(11).Text
                        cnfEditOperation.Show()
                    Else
                        Session("Action") = enAction.ADD_ONE
                        Session("Unkid") = -1
                        Session("PaAssessMstUnkid") = CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue))
                        Session("PaAssessEmpUnkid") = lvAssesmentList.Items(intRowIndex).Cells(13).Text
                        Session("PaAssessPeriodUnkid") = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx", False)
                    End If
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ItemProcess :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani (24-May-2016) -- End


#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("Action") = enAction.ADD_ONE
            Session("Unkid") = -1

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(cboAssessor.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
                Session("Assessor_Filter") = cboAssessor.SelectedValue + "-" + cboEmployee.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END 

            Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try

            'Shani(14-Sep-2015) -- Start
            'Issue: TRA Training Comments & Changes Requested By Dennis
            'cboAssessor.SelectedValue = 0
            cboAssessor.SelectedIndex = 0
            'Shani(14-Sep-2015) -- End

            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            chkShowcommitted.Checked = True
            'S.SANDEEP [04 JUN 2015] -- START
            chkShowUncommitted.Checked = True
            'S.SANDEEP [04 JUN 2015] -- END
            mstrAdvanceFilter = String.Empty

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Call FillList()
            lvAssesmentList.DataSource = New List(Of String)
            lvAssesmentList.DataBind()
            'SHANI [01 FEB 2015]--END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            'S.SANDEEP [31 DEC 2015] -- START
            'If CInt(cboAssessor.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Please Select Assessor to continue", Me)
            '    Exit Sub
            'End If

            If CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Assessor to continue", Me)
                Exit Sub
            End If
            'S.SANDEEP [31 DEC 2015] -- END


            'S.SANDEEP [18 DEC 2015] -- START
            'If CInt(cboEmployee.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
            '    Exit Sub
            'End If

            'S.SANDEEP [31 DEC 2015] -- START
            'If CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage("Please Select Period to continue", Me)
            '    Exit Sub
            'End If

            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Period to continue", Me)
                Exit Sub
            End If
            'S.SANDEEP [31 DEC 2015] -- END

            'S.SANDEEP [18 DEC 2015] -- END

            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            objEvaluation._Voidreason = txtMessage.Text
            objEvaluation._Isvoid = True
            objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEvaluation._Voiduserunkid = CInt(Session("UserId"))
            objEvaluation.Delete(CInt(hdf_analysisunkid.Value), enAssessmentMode.SELF_ASSESSMENT, CInt(hdf_PeriodID.Value))
            If objEvaluation._Message <> "" Then
                DisplayMessage.DisplayMessage(objEvaluation._Message, Me)
            Else
                'S.SANDEEP |15-MAR-2019| -- START
                Dim intRowIndex As Integer = -1
                Integer.TryParse(hdf_ItemIndex.Value, intRowIndex)
                Dim objComputeMst As New clsComputeScore_master
                objComputeMst._Isvoid = True
                objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objComputeMst._Voidreason = txtMessage.Text
                objComputeMst._Voiduserunkid = CInt(Session("UserId"))
                If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text), _
                                               CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text), _
                                               CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), _
                                               enAssessmentMode.APPRAISER_ASSESSMENT) = True Then

                Else
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex,Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                'S.SANDEEP |15-MAR-2019| -- END
                Call FillList()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP [18 DEC 2015] -- START
    Protected Sub cnfEditOperation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfEditOperation.buttonYes_Click
        Try
            Session("Action") = enAction.EDIT_ONE
            Session("Unkid") = Me.ViewState("mintAnalysisUnkid")

            Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [18 DEC 2015] -- END

    'Shani (24-May-2016) -- Start
    Protected Sub btn_ValidYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ValidYes.Click
        Dim intRowIndex As Integer = -1
        Integer.TryParse(hdf_ItemIndex.Value, intRowIndex)
        Try
            If intRowIndex <= -1 Then Exit Sub
            If txtVoidMessage.Text.Trim.Length <= 0 Then popup_ComputeYesNo.Show()

            Dim objComputeMst As New clsComputeScore_master
            objComputeMst._Isvoid = True
            objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objComputeMst._Voidreason = txtVoidMessage.Text
            objComputeMst._Voiduserunkid = CInt(Session("UserId"))
            If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text), _
                                                CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text), _
                                                CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), _
                                                enAssessmentMode.APPRAISER_ASSESSMENT) = True Then
                Call ItemProcess()
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex,Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani (24-May-2016) -- End


#End Region

#Region "GridView Event"

    'Shani (24-May-2016) -- Start
    'Protected Sub lvAssesmentList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvAssesmentList.ItemCommand
    '    Try
    '        If e.CommandName.ToUpper = "UNLOCK" Then
    '            If lvAssesmentList.Items.Count > 0 Then

    '                'S.SANDEEP [18 DEC 2015] -- START
    '                'If CBool(e.Item.Cells(12).Text) = False Then
    '                '    DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not committed.", Me)
    '                '    Exit Sub
    '                'End If

    '                If CInt(e.Item.Cells(11).Text) <= 0 Then
    '                    DisplayMessage.DisplayMessage("Sorry, you have not done the assessment for the this employee for the selected period.", Me)
    '                    Exit Sub
    '                End If

    '                If CBool(e.Item.Cells(30).Text) = False Then
    '    DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not committed.", Me)
    '    Exit Sub
    'End If
    '                'S.SANDEEP [18 DEC 2015] -- END


    '                'S.SANDEEP [18 DEC 2015] -- START
    '                'If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(10).Text)) = True Then
    '                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
    '                '    Exit Sub
    '                'End If

    '                If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(e.Item.Cells(13).Text), CInt(e.Item.Cells(14).Text)) = True Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
    '    Exit Sub
    'End If
    '                'S.SANDEEP [18 DEC 2015] -- END



    '                'S.SANDEEP [18 DEC 2015] -- START
    '                If CInt(e.Item.Cells(16).Text) = 1 Then
    '                    'If CInt(e.Item.Cells(13).Text) = 1 Then
    '                    'If objEvaluation.Unlock_Commit(CInt(e.Item.Cells(10).Text), CInt(e.Item.Cells(11).Text)) = True Then
    '                    'objEvaluation._Analysisunkid = CInt(e.Item.Cells(14).Text)
    '                    If objEvaluation.Unlock_Commit(CInt(e.Item.Cells(13).Text), CInt(e.Item.Cells(14).Text)) = True Then
    '                        objEvaluation._Analysisunkid = CInt(e.Item.Cells(11).Text)
    '                        'S.SANDEEP [18 DEC 2015] -- END
    '                        objEvaluation._Iscommitted = False
    '                        objEvaluation._Committeddatetime = Nothing
    '                        objEvaluation.Update()
    '                        If objEvaluation._Message <> "" Then
    '                            DisplayMessage.DisplayMessage(objEvaluation._Message, Me)
    '                        End If
    '                        Call FillList()
    '                    Else
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock this committed information. Reason : Its already linked with Appraisal."), Me)
    '                        Exit Sub
    '                    End If
    '                Else
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed."), Me)
    '                    Exit Sub
    '                End If
    '            End If
    '        ElseIf e.CommandName.ToUpper = "EDIT" Then

    '            'S.SANDEEP [18 DEC 2015] -- START
    '            'If CInt(e.Item.Cells(13).Text) = 2 Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), Me)
    '            '    Exit Sub
    '            'End If

    '            'If CBool(e.Item.Cells(12).Text) = True Then
    '            '    DisplayMessage.DisplayMessage("Sorry, you cannot do edit operation. Reason : Assessment is committed.", Me)
    '            '    Exit Sub
    '            'End If

    '            If CInt(e.Item.Cells(16).Text) = 2 Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), Me)
    '    Exit Sub
    'End If

    '            If CBool(e.Item.Cells(30).Text) = True Then
    '    DisplayMessage.DisplayMessage("Sorry, you cannot do edit operation. Reason : Assessment is committed.", Me)
    '    Exit Sub
    'End If
    '            'S.SANDEEP [18 DEC 2015] -- END


    '            Session("Action") = enAction.EDIT_ONE
    '            Session("Unkid") = e.Item.Cells(14).Text

    '            'SHANI [09 Mar 2015]-START
    '            'Enhancement - REDESIGN SELF SERVICE.
    '            If CInt(cboAssessor.SelectedValue) > 0 AndAlso CInt(cboEmployee.SelectedValue) > 0 Then
    '                Session("Assessor_Filter") = cboAssessor.SelectedValue + "-" + cboEmployee.SelectedValue
    '            End If
    '            'SHANI [09 Mar 2015]--END 

    '            Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx", False)

    '        ElseIf e.CommandName.ToUpper = "DELETE" Then

    '            'S.SANDEEP [18 DEC 2015] -- START
    '            'If CInt(e.Item.Cells(13).Text) = 2 Then
    '            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), Me)
    '            '    Exit Sub
    '            'End If

    '            'If CBool(e.Item.Cells(12).Text) = True Then
    '            '    DisplayMessage.DisplayMessage("Sorry, you cannot do delete operation. Reason : Assessment is committed.", Me)
    '            '    Exit Sub
    '            'End If

    '            If CInt(e.Item.Cells(16).Text) = 2 Then
    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), Me)
    '    Exit Sub
    'End If

    '            If CBool(e.Item.Cells(30).Text) = True Then
    '    DisplayMessage.DisplayMessage("Sorry, you cannot do delete operation. Reason : Assessment is committed.", Me)
    '    Exit Sub
    'End If

    '            If CInt(e.Item.Cells(11).Text) <= 0 Then
    '                DisplayMessage.DisplayMessage("Sorry, you have not done the assessment for the this employee for the selected period.", Me)
    '                Exit Sub
    '            End If
    '            'S.SANDEEP [18 DEC 2015] -- END


    '            'S.SANDEEP [18 DEC 2015] -- START
    '            'hdf_analysisunkid.Value = e.Item.Cells(14).Text
    '            'hdf_PeriodID.Value = e.Item.Cells(10).Text

    '            hdf_analysisunkid.Value = e.Item.Cells(11).Text
    '            hdf_PeriodID.Value = e.Item.Cells(14).Text
    '            'S.SANDEEP [18 DEC 2015] -- END

    '            lblMessage.Text = Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
    '            lblTitle.Text = "Aruti"
    '            txtMessage.Text = ""
    '            popup_YesNo.Show()

    '            'S.SANDEEP [18 DEC 2015] -- START
    '        ElseIf e.CommandName.ToUpper = "ASSESS" Then


    '            'S.SANDEEP [02 Jan 2016] -- START
    '            Dim sMsg As String = String.Empty
    '            Dim intPeriodId As Integer = 0
    '            If CInt(e.Item.Cells(12).Text) > 0 Then
    '                intPeriodId = e.Item.Cells(14).Text
    '            Else
    '                intPeriodId = cboPeriod.SelectedValue
    '            End If
    '            sMsg = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), e.Item.Cells(13).Text, intPeriodId, Session("Self_Assign_Competencies"), Session("IncludeCustomItemInPlanning"))
    '            If sMsg.Trim.Length > 0 Then
    '                DisplayMessage.DisplayMessage(sMsg, Me)
    '                Exit Sub
    '            End If
    '            'S.SANDEEP [02 Jan 2016] -- END

    '            If CInt(e.Item.Cells(16).Text) = 2 Then
    '                DisplayMessage.DisplayMessage("Sorry, you cannot assess this employee. Reason : Period is closed.", Me)
    '                Exit Sub
    '            End If

    '            If CBool(e.Item.Cells(30).Text) = True Then
    '                DisplayMessage.DisplayMessage("Sorry, you cannot do assess operation. Reason : Assessment is committed.", Me)
    '                Exit Sub
    '            End If

    '            If Session("AllowAssessor_Before_Emp") = False Then
    '                If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, e.Item.Cells(13).Text, e.Item.Cells(14).Text) = False Then
    '                    DisplayMessage.DisplayError(ex,Me)
    '                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period.", Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            If CInt(e.Item.Cells(11).Text) > 0 Then
    '                cnfEditOperation.Title = "Aruti"
    '                cnfEditOperation.Message = "You have already done the evaluation for this employee for the selected period. Would you like to open it in edit mode?"
    '                mintAnalysisUnkid = e.Item.Cells(11).Text
    '                cnfEditOperation.Show()
    '            Else
    '                Session("Action") = enAction.ADD_ONE
    '                Session("Unkid") = -1
    '                Session("PaAssessMstUnkid") = CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue))
    '                Session("PaAssessEmpUnkid") = e.Item.Cells(13).Text
    '                Session("PaAssessPeriodUnkid") = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
    '                Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgAssessorEvaluation.aspx", False)
    '            End If
    '            'S.SANDEEP [18 DEC 2015] -- END
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("lvAssesmentList_ItemCommand :-" & ex.Message, Me)
    '    End Try
    'End Sub
    Protected Sub lvAssesmentList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvAssesmentList.ItemCommand
        Try
            'Shani (24-May-2016) -- Start


            If e.CommandName.ToUpper = "UNLOCK" Then
                If lvAssesmentList.Items.Count > 0 Then
                    If CInt(e.Item.Cells(11).Text) <= 0 Then
                        DisplayMessage.DisplayMessage("Sorry, you have not done the assessment for the this employee for the selected period.", Me)
                        Exit Sub
                    End If

                    If CBool(e.Item.Cells(30).Text) = False Then
                        'S.SANDEEP |25-MAR-2019| -- START
                        'DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not committed.", Me)
                        DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not submitted.", Me)
                        'S.SANDEEP |25-MAR-2019| -- END
                        Exit Sub
                    End If

                    'S.SANDEEP |22-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                    If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(e.Item.Cells(13).Text), CInt(e.Item.Cells(14).Text), , , , , , False) = True Then
                        'If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(e.Item.Cells(13).Text), CInt(e.Item.Cells(14).Text)) = True Then
                        'S.SANDEEP |22-JUL-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
                        Exit Sub
                    End If

                    If CInt(e.Item.Cells(16).Text) <> 1 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this committed information. Reason : Period is already Closed."), Me)
                        Exit Sub
                    End If
                End If
            ElseIf e.CommandName.ToUpper = "EDIT" Then
                If CInt(e.Item.Cells(16).Text) = 2 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(30).Text) = True Then
                    'S.SANDEEP |25-MAR-2019| -- START
                    'DisplayMessage.DisplayMessage("Sorry, you cannot do edit operation. Reason : Assessment is committed.", Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot do the edit operation. Reason : Assessment is submitted."), Me)
                    'S.SANDEEP |25-MAR-2019| -- END
                    Exit Sub
                End If

            ElseIf e.CommandName.ToUpper = "DELETE" Then
                If CInt(e.Item.Cells(16).Text) = 2 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(30).Text) = True Then
                    'S.SANDEEP |25-MAR-2019| -- START
                    'DisplayMessage.DisplayMessage("Sorry, you cannot do delete operation. Reason : Assessment is committed.", Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, you cannot do the delete operation. Reason : Assessment is submitted."), Me)
                    'S.SANDEEP |25-MAR-2019| -- END
                    Exit Sub
                End If

                If CInt(e.Item.Cells(11).Text) <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry, you have not done the assessment for the this employee for the selected period.", Me)
                    Exit Sub
                End If
            ElseIf e.CommandName.ToUpper = "ASSESS" Then
                Dim sMsg As String = String.Empty
                Dim intPeriodId As Integer = 0

                If CInt(e.Item.Cells(12).Text) > 0 Then
                    intPeriodId = e.Item.Cells(14).Text
                Else
                    intPeriodId = cboPeriod.SelectedValue
                End If

                'S.SANDEEP [07-NOV-2018] -- START
                'sMsg = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), e.Item.Cells(13).Text, intPeriodId, Session("Self_Assign_Competencies"))
                sMsg = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), e.Item.Cells(13).Text, intPeriodId, Session("Self_Assign_Competencies"), enAssessmentMode.APPRAISER_ASSESSMENT, True)
                'S.SANDEEP [07-NOV-2018] -- END


                If sMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    Exit Sub
                End If

                If CInt(e.Item.Cells(16).Text) = 2 Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot assess this employee. Reason : Period is closed.", Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(30).Text) = True Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot do assess operation. Reason : Assessment is committed.", Me)
                    Exit Sub
                End If

                If Session("AllowAssessor_Before_Emp") = False Then
                    'S.SANDEEP |26-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                    'If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, e.Item.Cells(13).Text, e.Item.Cells(14).Text) = False Then
                    'If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, e.Item.Cells(13).Text, e.Item.Cells(14).Text, , , , , , True) = False Then
                    If CBool(e.Item.Cells(29).Text) = False Then
                        'S.SANDEEP |26-AUG-2019| -- END

                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex,Me)
                        '                            "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period.", Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period.", ), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If
            End If
            hdf_CommondName.Value = e.CommandName
            hdf_ItemIndex.Value = e.Item.ItemIndex
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If objEvaluation.CheckComputionIsExist(CInt(e.Item.Cells(11).Text), _
            '                                       enAssessmentMode.APPRAISER_ASSESSMENT, _
            '                                       CInt(e.Item.Cells(14).Text)) = True Then
            '    lblComputeTitle.Text = "Aruti"
            '    lblComputeMessage.Text = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compoute score process is already done for the selected period. Are you want to Void computation score?")
            '    txtVoidMessage.Text = ""
            '    popup_ComputeYesNo.Show()
            'Else
            '    Call ItemProcess()
            'End If
            Call ItemProcess()
            'S.SANDEEP |12-FEB-2019| -- END

            'Shani (24-May-2016) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvAssesmentList_ItemCommand :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani (24-May-2016) -- End

    Protected Sub lvAssesmentList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvAssesmentList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'S.SANDEEP |25-MAR-2019| -- START
                If CInt(e.Item.Cells(11).Text) <= 0 Then
                    CType(e.Item.Cells(3).FindControl("imgUnlock"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("imgDelete"), LinkButton).Visible = False
                End If
                'S.SANDEEP |25-MAR-2019| -- END

                If CBool(e.Item.Cells(28).Text) = True Then
                    CType(e.Item.Cells(1).FindControl("lnkAssessEmp"), LinkButton).Visible = False
                    e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                    For i As Integer = 1 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                    e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                Else

                    If CBool(e.Item.Cells(30).Text) = True Then 'ASSESSOR COMMITTED CELL VALUE
                        e.Item.ForeColor = Color.RoyalBlue
                    End If

                    e.Item.Cells(0).Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & e.Item.Cells(0).Text
                    If e.Item.Cells(6).Text.Trim <> "" AndAlso e.Item.Cells(6).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text.ToString).ToShortDateString
                    End If
                    Dim xTotalScore As Decimal = 0

                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...

                    'Select Case CInt(e.Item.Cells(19).Text.Trim)
                    '    Case 1
                    '        '******************************************** SELF BSC SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                    '                                                  True, Session("ScoringOptionId"), _
                    '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                                  e.Item.Cells(13).Text, e.Item.Cells(14).Text, , , , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                    '        e.Item.Cells(7).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** SELF BSC SCORE ******************************' END


                    '        '******************************************** ASSESSOR BSC SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                    '                                                  True, Session("ScoringOptionId"), _
                    '                                                  enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                                  e.Item.Cells(13).Text, e.Item.Cells(14).Text, , , e.Item.Cells(22).Text, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                    '        e.Item.Cells(8).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** ASSESSOR BSC SCORE ******************************' END


                    '        '******************************************** REVIEWER BSC SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                    '                                                  True, Session("ScoringOptionId"), _
                    '                                                  enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                                  e.Item.Cells(13).Text, e.Item.Cells(14).Text, , , e.Item.Cells(22).Text, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                    '        e.Item.Cells(9).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** REVIEWER BSC SCORE ******************************' END

                    '    Case 2

                    '        '******************************************** SELF COMPETENCY SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                    '                                                  False, _
                    '                                                  Session("ScoringOptionId"), _
                    '                                                  enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                                  e.Item.Cells(13).Text, _
                    '                                                  e.Item.Cells(14).Text, _
                    '                                                  e.Item.Cells(17).Text, , , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                    '        e.Item.Cells(7).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** SELF COMPETENCY SCORE ******************************' END


                    '        '******************************************** ASSESSOR COMPETENCY SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                    '                                                  False, _
                    '                                                  Session("ScoringOptionId"), _
                    '                                                  enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                                  e.Item.Cells(13).Text, _
                    '                                                  e.Item.Cells(14).Text, _
                    '                                                  e.Item.Cells(17).Text, , e.Item.Cells(22).Text, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                    '        e.Item.Cells(8).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** ASSESSOR COMPETENCY SCORE ******************************' END


                    '        '******************************************** REVIEWER COMPETENCY SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                    '                                                  False, _
                    '                                                  Session("ScoringOptionId"), _
                    '                                                  enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                                  e.Item.Cells(13).Text, _
                    '                                                  e.Item.Cells(14).Text, _
                    '                                                  e.Item.Cells(17).Text, , e.Item.Cells(22).Text, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

                    '        'S.SANDEEP [06 Jan 2016] -- START
                    '        'e.Item.Cells(8).Text = xTotalScore.ToString("#########0.#0")
                    '        e.Item.Cells(9).Text = xTotalScore.ToString("#########0.#0")
                    '        'S.SANDEEP [06 Jan 2016] -- END
                    '        '******************************************** REVIEWER COMPETENCY SCORE ******************************' END

                    'End Select

                    '''''' Only for Information code  for forloop in enum start======
                    'Dim val = [Enum].GetValues(GetType(enAssessmentMode))
                    'For Each en As enAssessmentMode In [Enum].GetValues(GetType(enAssessmentMode))

                    'Next
                    '''''' Only for Information code  for forloop in enum END ========


                    'S.SANDEEP [27-APR-2017] -- START
                    'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                    'Select Case CInt(e.Item.Cells(19).Text.Trim)
                    '    Case 1
                    '        '******************************************** SELF BSC SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                    '                                                  IsBalanceScoreCard:=True, _
                    '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                    '                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                    '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                                  xEmployeeId:=e.Item.Cells(13).Text, _
                    '                                                  xPeriodId:=e.Item.Cells(14).Text, _
                    '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                    '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                    '        e.Item.Cells(7).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** SELF BSC SCORE ******************************' END


                    '        '******************************************** ASSESSOR BSC SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                    '                                                  IsBalanceScoreCard:=True, _
                    '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                    '                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                    '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                                  xEmployeeId:=e.Item.Cells(13).Text, _
                    '                                                  xPeriodId:=e.Item.Cells(14).Text, _
                    '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                    '                                                  xAssessorReviewerId:=e.Item.Cells(22).Text, _
                    '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                    '        e.Item.Cells(8).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** ASSESSOR BSC SCORE ******************************' END


                    '        '******************************************** REVIEWER BSC SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                    '                                                  IsBalanceScoreCard:=True, _
                    '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                    '                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                    '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                    '                                                  xEmployeeId:=e.Item.Cells(13).Text, _
                    '                                                  xPeriodId:=e.Item.Cells(14).Text, _
                    '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                    '                                                  xAssessorReviewerId:=e.Item.Cells(22).Text, _
                    '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                    '        e.Item.Cells(9).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** REVIEWER BSC SCORE ******************************' END

                    '    Case 2

                    '        '******************************************** SELF COMPETENCY SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                    '                                                  IsBalanceScoreCard:=False, _
                    '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                    '                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                    '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                                  xEmployeeId:=e.Item.Cells(13).Text, _
                    '                                                  xPeriodId:=e.Item.Cells(14).Text, _
                    '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                    '                                                  xAssessGrpId:=e.Item.Cells(17).Text, _
                    '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                    '        e.Item.Cells(7).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** SELF COMPETENCY SCORE ******************************' END


                    '        '******************************************** ASSESSOR COMPETENCY SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                    '                                                  IsBalanceScoreCard:=False, _
                    '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                    '                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                    '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                                  xEmployeeId:=e.Item.Cells(13).Text, _
                    '                                                  xPeriodId:=e.Item.Cells(14).Text, _
                    '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                    '                                                  xAssessGrpId:=e.Item.Cells(17).Text, _
                    '                                                  xAssessorReviewerId:=e.Item.Cells(22).Text, _
                    '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                    '        e.Item.Cells(8).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** ASSESSOR COMPETENCY SCORE ******************************' END


                    '        '******************************************** REVIEWER COMPETENCY SCORE ******************************' START
                    '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                    '                                                  IsBalanceScoreCard:=False, _
                    '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                    '                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                    '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                    '                                                  xEmployeeId:=e.Item.Cells(13).Text, _
                    '                                                  xPeriodId:=e.Item.Cells(14).Text, _
                    '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                    '                                                  xAssessGrpId:=e.Item.Cells(17).Text, _
                    '                                                  xAssessorReviewerId:=e.Item.Cells(22).Text, _
                    '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                    '        e.Item.Cells(9).Text = xTotalScore.ToString("#########0.#0")
                    '        '******************************************** REVIEWER COMPETENCY SCORE ******************************' END

                    'End Select
                    Dim dtComputeScore As DataTable = Nothing
                    'S.SANDEEP |27-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                    'dtComputeScore = (New clsComputeScore_master).GetComputeScore(e.Item.Cells(13).Text, e.Item.Cells(14).Text, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
                    dtComputeScore = (New clsComputeScore_master).GetComputeScore(e.Item.Cells(13).Text, e.Item.Cells(14).Text, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, Session("IsCalibrationSettingActive"))
                    'S.SANDEEP |27-MAY-2019| -- END

                    Select Case CInt(e.Item.Cells(19).Text.Trim)
                        Case 1
                            '******************************************** SELF BSC SCORE ******************************' START
                            'S.SANDEEP [01-OCT-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                            xTotalScore = 0
                            'S.SANDEEP [01-OCT-2018] -- END
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                xTotalScore = xScore(0)
                            End If

                            e.Item.Cells(7).Text = xTotalScore.ToString("###################0.#0")
                            '******************************************** SELF BSC SCORE ******************************' END


                            '******************************************** ASSESSOR BSC SCORE ******************************' START
                            'S.SANDEEP [01-OCT-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                            xTotalScore = 0
                            'S.SANDEEP [01-OCT-2018] -- END
                            xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE _
                                                                        And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                        .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                xTotalScore = xScore(0)
                            End If
                            e.Item.Cells(8).Text = xTotalScore.ToString("###################0.#0")
                            '******************************************** ASSESSOR BSC SCORE ******************************' END


                            '******************************************** REVIEWER BSC SCORE ******************************' START
                            'S.SANDEEP [01-OCT-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                            xTotalScore = 0
                            'S.SANDEEP [01-OCT-2018] -- END
                            xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                xTotalScore = xScore(0)
                            End If

                            e.Item.Cells(9).Text = xTotalScore.ToString("###################0.#0")
                            '******************************************** REVIEWER BSC SCORE ******************************' END

                        Case 2

                            '******************************************** SELF COMPETENCY SCORE ******************************' START
                            'S.SANDEEP [01-OCT-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                            xTotalScore = 0
                            'S.SANDEEP [01-OCT-2018] -- END

                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of String)("competency_value")).ToList
                            If xScore.Count > 0 Then
                                If xScore(0).ToString.Trim.Length > 0 Then
                                    For Each xVal As String In xScore(0).ToString.Split(CChar(","))
                                        If CInt(e.Item.Cells(17).Text) = CInt(xVal.Split(CChar("|"))(0)) Then
                                            xTotalScore = CDec(xVal.Split(CChar("|"))(1)).ToString("###################0.#0")
                                        End If
                                    Next
                                    'S.SANDEEP [06-JUL-2017] -- START
                                    'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                                Else
                                    xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
                                                                                 And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                                 .Select(Function(x) x.Field(Of Decimal)("formula_value").ToString()).ToList()

                                    If xScore.Count > 0 Then
                                        xTotalScore = CDec(xScore(0))
                                    End If
                                    'S.SANDEEP [06-JUL-2017] -- END
                                End If
                            End If

                            e.Item.Cells(7).Text = xTotalScore.ToString("###################0.#0")
                            '******************************************** SELF COMPETENCY SCORE ******************************' END


                            '******************************************** ASSESSOR COMPETENCY SCORE ******************************' START
                            'S.SANDEEP [01-OCT-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                            xTotalScore = 0
                            'S.SANDEEP [01-OCT-2018] -- END
                            xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of String)("competency_value")).ToList
                            If xScore.Count > 0 Then
                                If xScore(0).ToString.Trim.Length > 0 Then
                                    For Each xVal As String In xScore(0).ToString.Split(CChar(","))
                                        If CInt(e.Item.Cells(17).Text) = CInt(xVal.Split(CChar("|"))(0)) Then
                                            xTotalScore = CDec(xVal.Split(CChar("|"))(1)).ToString("###################0.#0")
                                        End If
                                    Next
                                    'S.SANDEEP [06-JUL-2017] -- START
                                    'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                                Else
                                    xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE _
                                                                                 And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                                 .Select(Function(x) x.Field(Of Decimal)("formula_value").ToString()).ToList()

                                    If xScore.Count > 0 Then
                                        xTotalScore = CDec(xScore(0))
                                    End If
                                    'S.SANDEEP [06-JUL-2017] -- END
                                End If
                            End If
                            e.Item.Cells(8).Text = xTotalScore.ToString("###################0.#0")
                            '******************************************** ASSESSOR COMPETENCY SCORE ******************************' END


                            '******************************************** REVIEWER COMPETENCY SCORE ******************************' START
                            'S.SANDEEP [01-OCT-2018] -- START
                            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                            xTotalScore = 0
                            'S.SANDEEP [01-OCT-2018] -- END
                            xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of String)("competency_value")).ToList
                            If xScore.Count > 0 Then
                                If xScore(0).ToString.Trim.Length > 0 Then
                                    For Each xVal As String In xScore(0).ToString.Split(CChar(","))
                                        If CInt(e.Item.Cells(17).Text) = CInt(xVal.Split(CChar("|"))(0)) Then
                                            xTotalScore = CDec(xVal.Split(CChar("|"))(1)).ToString("###################0.#0")
                                        End If
                                    Next
                                    'S.SANDEEP [06-JUL-2017] -- START
                                    'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                                Else
                                    xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE _
                                                                                 And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                                                 .Select(Function(x) x.Field(Of Decimal)("formula_value").ToString()).ToList()

                                    If xScore.Count > 0 Then
                                        xTotalScore = CDec(xScore(0))
                                    End If
                                    'S.SANDEEP [06-JUL-2017] -- END
                                End If
                            End If
                            e.Item.Cells(9).Text = xTotalScore.ToString("###################0.#0")
                            '******************************************** REVIEWER COMPETENCY SCORE ******************************' END

                    End Select
                    'S.SANDEEP [27-APR-2017] -- END



                    'Shani (23-Nov123-2016-2016) -- End
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvAssesmentList_ItemDataBound :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Control Event"
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            'S.SANDEEP [18 DEC 2015] -- START
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            'S.SANDEEP [18 DEC 2015] -- END
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub cboAssessor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            Dim objAssessor As New clsevaluation_analysis_master

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "List", True, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("UserId"))


            'S.SANDEEP [10 DEC 2015] -- START
            'Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(Session("Database_Name"), _
            '                                                             Session("UserId"), _
            '                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
            '                                                             Session("IsIncludeInactiveEmp"), _
            '                                                             CInt(cboAssessor.SelectedValue), "List", True)

            Dim dsList As DataSet = objAssessor.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                                         Session("UserId"), _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                         Session("IsIncludeInactiveEmp"), _
                                                                         CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), "List", True)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            cboEmployee.DataTextField = "NAME"
            cboEmployee.DataValueField = "Id"
            cboEmployee.DataSource = dsList.Tables(0)
            cboEmployee.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboAssessor_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            'If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
            '    Call FillList()
            'End If
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |12-FEB-2019| -- END


    'S.SANDEEP |19-AUG-2020| -- START
    'ISSUE/ENHANCEMENT : Language Changes
    Protected Sub lnkInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False, Session("Database_Name"))
            gvRating.AutoGenerateColumns = False
            gvRating.DataSource = dsList
            gvRating.DataBind()
            objRating = Nothing
            popupinfo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |19-AUG-2020| -- END

#End Region

End Class
