﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
#End Region

Partial Class wPg_SelfEvaluationList
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmSelfEvaluationList"
    Private objEvaluation As New clsevaluation_analysis_master
    Private mstrAdvanceFilter As String = String.Empty
    Private DisplayMessage As New CommonCodes
#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call SetVisibility()
                Call FillCombo()

                'S.SANDEEP [04 JUN 2015] -- START
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    Call btnSearch_Click(btnSearch, Nothing)
                End If
                'S.SANDEEP [04 JUN 2015] -- END

            End If
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                lnkAllocation.Visible = True
                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                btnNew.Visible = Session("AllowtoAddSelfEvaluation")
                'S.SANDEEP [28 MAY 2015] -- END
            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                lnkAllocation.Visible = False
            End If
            If Me.ViewState("AdvanceFilter") IsNot Nothing Then
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("AdvanceFilter") Is Nothing Then
                Me.ViewState.Add("AdvanceFilter", mstrAdvanceFilter)
            Else
                Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region "Private Methods"

    Private Sub FillCombo()
        Dim objYear As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsFill As New DataSet
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objYear.getComboListPAYYEAR("Year", True, , , , True)
            dsFill = objYear.getComboListPAYYEAR(Session("Fin_year"), Session("FinancialYear_Name"), Session("CompanyUnkId"), "Year", True, True)
            'Shani(20-Nov-2015) -- End

            With cboYear
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsFill.Tables("Year")
                .DataBind()
                .SelectedValue = 0
            End With

            'S.SANDEEP [17 NOV 2015] -- START
            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 0)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "Period", True, 0, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, _
            '                                   Session("Fin_year"), _
            '                                   Session("Database_Name"), _
            '                                   Session("fin_startdate"), _
            '                                   "Period", True, 0)

            dsFill = objPeriod.getListForCombo(enModuleReference.Assessment, _
                                               0, _
                                               Session("Database_Name"), _
                                               Session("fin_startdate"), _
                                               "Period", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriod As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsFill.Tables("Period")
                .DataBind()
                .SelectedValue = intCurrentPeriod
            End With

            'Shani (09-May-2016) -- Change[0-->intCurrentPeriod]

            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
                'Else
                '    dsFill = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If


                'Shani(14-APR-2016) -- Start
                Dim strFilterQry As String = String.Empty

                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master
                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                strFilterQry = " hremployee_master.employeeunkid NOT IN "
                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    csvIds = String.Join(",", dsMapEmp.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("EmpId").ToString()).ToArray())
                End If
                If csvIds.Trim.Length > 0 Then
                    strFilterQry &= "(" & csvIds & ")"
                Else
                    strFilterQry = ""
                End If

                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    dsFill = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)

                    If strFilterQry.Trim.Length > 0 Then
                        strFilterQry &= " AND hremployee_master.employeeunkid IN "
                    Else
                        strFilterQry = " hremployee_master.employeeunkid IN "
                    End If
                    csvIds = String.Join(",", dsFill.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                End If
                objEval = Nothing

                'Shani(14-APR-2016) -- End 



                'Shani(14-APR-2016) -- Start

                'dsFill = objEmployee.GetEmployeeList(Session("Database_Name"), _
                '                                     Session("UserId"), _
                '                                     Session("Fin_year"), _
                '                                     Session("CompanyUnkId"), _
                '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                     Session("UserAccessModeSetting"), True, _
                '                                     Session("IsIncludeInactiveEmp"), "List", True)

                dsFill = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                     Session("UserId"), _
                                                     Session("Fin_year"), _
                                                     Session("CompanyUnkId"), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                     Session("UserAccessModeSetting"), True, _
                                                     Session("IsIncludeInactiveEmp"), "List", True, , , , , , , , , , , , , , , strFilterQry, , False)
                'Shani(14-APR-2016) -- End

                'Shani(24-Aug-2015) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsFill.Tables("List")
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try
            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                If CBool(Session("AllowtoViewSelfEvaluationList")) = False Then Exit Sub
            End If
            'S.SANDEEP [28 MAY 2015] -- END

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'dsList = objEvaluation.GetList("List", enAssessmentMode.SELF_ASSESSMENT, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("UserId"))

            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    'S.SANDEEP [04 JUN 2015] -- START
            '    'If CBool(Session("AllowToViewSelfAssessmentList")) = False Then dsList.Tables(0).Rows.Clear()
            '    If CBool(Session("AllowtoViewSelfEvaluationList")) = False Then dsList.Tables(0).Rows.Clear()
            '    'S.SANDEEP [04 JUN 2015] -- END
            'End If

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    StrSearching &= "AND selfemployeeunkid = " & CInt(cboEmployee.SelectedValue)
            'End If

            'If CInt(cboYear.SelectedValue) > 0 Then
            '    StrSearching &= "AND yearunkid = " & CInt(cboYear.SelectedValue)
            'End If

            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    StrSearching &= "AND periodunkid = " & CInt(cboPeriod.SelectedValue)
            'End If

            'If dtpDate.IsNull = False Then
            '    StrSearching &= "AND assessmentdate = '" & eZeeDate.convertDate(dtpDate.GetDate) & "'"
            'End If

            'If chkShowcommitted.Checked = True And chkShowUncommitted.Checked = False Then
            '    StrSearching &= "AND iscommitted = " & True & " "
            'ElseIf chkShowcommitted.Checked = False And chkShowUncommitted.Checked = True Then
            '    StrSearching &= "AND iscommitted = " & False & " "
            'End If

            'If mstrAdvanceFilter.Length > 0 Then
            '    StrSearching &= "AND " & mstrAdvanceFilter
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables(0), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hrevaluation_analysis_master.selfemployeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboYear.SelectedValue) > 0 Then
                StrSearching &= "AND cfcommon_period_tran.yearunkid = " & CInt(cboYear.SelectedValue) & " "
            End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                StrSearching &= "AND hrevaluation_analysis_master.periodunkid = " & CInt(cboPeriod.SelectedValue) & " "
            End If

            If dtpDate.IsNull = False Then
                StrSearching &= "AND CONVERT(CHAR(8),hrevaluation_analysis_master.assessmentdate,112) = '" & eZeeDate.convertDate(dtpDate.GetDate) & "'"
            End If

            'Shani(19-APR-2016) -- Start
            'Get Assessor Wise Employee Given by Andrew / matthew
            'If chkShowcommitted.Checked = True And chkShowUncommitted.Checked = False Then
            '    StrSearching &= "AND ISNULL(iscommitted,0) = " & True & " "
            'ElseIf chkShowcommitted.Checked = False And chkShowUncommitted.Checked = True Then
            '    StrSearching &= "AND ISNULL(iscommitted,0) = " & False & " "
            'End If

            If chkShowcommitted.Checked = True And chkShowUncommitted.Checked = False Then
                StrSearching &= "AND ISNULL(iscommitted,0) = 1 "
            ElseIf chkShowcommitted.Checked = False And chkShowUncommitted.Checked = True Then
                StrSearching &= "AND ISNULL(iscommitted,0) = 0 "
            End If
            'Shani(19-APR-2016) -- End

            If mstrAdvanceFilter.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            Dim blnInclude_UAC As Boolean = True
            'Shani(19-APR-2016) -- Start
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                blnInclude_UAC = False
            End If
            'Shani(19-APR-2016) -- End 


            'S.SANDEEP |27-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
            'dsList = objEvaluation.GetList(Session("Database_Name"), _
            '                               Session("UserId"), _
            '                               Session("Fin_year"), _
            '                               Session("CompanyUnkId"), _
            '                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                               Session("UserAccessModeSetting"), True, _
            '                               Session("IsIncludeInactiveEmp"), "List", _
            '                               enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue), StrSearching, blnInclude_UAC) 'S.SANDEEP [27-APR-2017] -- START {CInt(cboPeriod.SelectedValue)} -- END

            dsList = objEvaluation.GetList(Session("Database_Name"), _
                                           Session("UserId"), _
                                           Session("Fin_year"), _
                                           Session("CompanyUnkId"), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           Session("UserAccessModeSetting"), True, _
                                           Session("IsIncludeInactiveEmp"), "List", _
                                           enAssessmentMode.SELF_ASSESSMENT, _
                                           CInt(cboPeriod.SelectedValue), _
                                           Session("IsCalibrationSettingActive"), StrSearching, blnInclude_UAC) 'S.SANDEEP [27-APR-2017] -- START {CInt(cboPeriod.SelectedValue)} -- END
            'S.SANDEEP |27-MAY-2019| -- END

            dtTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By CCBRT
                'lvAssesmentList.Columns(0).Visible = CBool(Session("AllowtoEditSelfEvaluation"))
                lvAssesmentList.Columns(0).Visible = CBool(Session("AllowtoAddSelfEvaluation"))
                'Shani(06-Feb-2016) -- End
                lvAssesmentList.Columns(1).Visible = CBool(Session("AllowtoDeleteSelfEvaluation"))
                lvAssesmentList.Columns(2).Visible = CBool(Session("AllowtoUnlockcommittedSelfEvaluation"))
            End If
            'S.SANDEEP [28 MAY 2015] -- END


            'lvAssesmentList.Columns(0).Visible = CBool(Session("EditEmployeeAssessment"))
            'lvAssesmentList.Columns(1).Visible = CBool(Session("DeleteEmployeeAssessment"))
            'lvAssesmentList.Columns(2).Visible = CBool(Session("Allow_UnlockCommittedGeneralAssessment"))

            'S.SANDEEP [21 JAN 2015] -- START
            'If CBool(Session("ConsiderItemWeightAsNumber")) Then
            '    lvAssesmentList.Columns(8).Visible = True
            '    lvAssesmentList.Columns(9).Visible = False
            'Else
            '    lvAssesmentList.Columns(8).Visible = False
            '    lvAssesmentList.Columns(9).Visible = True
            'End If
            'S.SANDEEP [21 JAN 2015] -- END


            'SHANI [27 Mar 2015]-START
            'ENHANCEMENT : 
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                lvAssesmentList.Columns(2).Visible = True
            Else
                lvAssesmentList.Columns(2).Visible = False
            End If
            'SHANI [27 Mar 2015]-END

            lvAssesmentList.AutoGenerateColumns = False
            lvAssesmentList.DataSource = dtTable
            lvAssesmentList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            chkShowcommitted.Checked = True
            'S.SANDEEP [04 JUN 2015] -- START
            chkShowUncommitted.Checked = True
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Shani (24-May-2016) -- Start
    Private Sub ItemProcess()
        Dim strCommand As String = ""
        Dim intRowIndex As Integer = -1
        Try
            strCommand = hdf_CommondName.Value
            Integer.TryParse(hdf_ItemIndex.Value, intRowIndex)
            If lvAssesmentList.Items.Count > 0 Then
                If strCommand.ToUpper = "UNLOCK" Then

                    'S.SANDEEP |13-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                    Dim xMessage As String = String.Empty
                    'S.SANDEEP |13-NOV-2019| -- END
                    If CInt(lvAssesmentList.Items(intRowIndex).Cells(13).Text) = 1 Then
                        'S.SANDEEP |13-NOV-2019| -- START
                        'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                        'If objEvaluation.Unlock_Commit(CInt(lvAssesmentList.Items(intRowIndex).Cells(10).Text), _
                        '                               CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text)) = True Then
                        If objEvaluation.Unlock_Commit(CInt(lvAssesmentList.Items(intRowIndex).Cells(10).Text), _
                                                       CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text), xMessage) = True Then
                            'S.SANDEEP |13-NOV-2019| -- END
                            objEvaluation._Analysisunkid = CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text)
                            objEvaluation._Iscommitted = False
                            objEvaluation._Committeddatetime = Nothing
                            'S.SANDEEP [27-APR-2017] -- START
                            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                            'objEvaluation.Update()
                            objEvaluation.Update(Nothing, Nothing, Nothing, 0, Nothing, False, False, Nothing, False)
                            'S.SANDEEP [27-APR-2017] -- END

                            If objEvaluation._Message <> "" Then
                                DisplayMessage.DisplayMessage(objEvaluation._Message, Me)
                            End If
                            Call FillList()
                        Else
                            'S.SANDEEP |13-NOV-2019| -- START
                            'ISSUE/ENHANCEMENT : CALIBRATION ISSUES
                            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), Me)
                            If xMessage.Trim.Length > 0 Then DisplayMessage.DisplayMessage(xMessage, Me)
                            'S.SANDEEP |13-NOV-2019| -- END
                            Exit Sub
                        End If
                    End If

                ElseIf strCommand.ToUpper = "EDIT" Then
                    Session("Unkid") = lvAssesmentList.Items(intRowIndex).Cells(14).Text
                    Session("Action") = enAction.EDIT_ONE
                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgEmployeeEvaluation.aspx", False)

                ElseIf strCommand.ToUpper = "DELETE" Then
                    hdf_analysisunkid.Value = lvAssesmentList.Items(intRowIndex).Cells(14).Text
                    hdf_PeriodID.Value = lvAssesmentList.Items(intRowIndex).Cells(10).Text
                    lblMessage.Text = Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
                    lblTitle.Text = "Aruti"
                    txtMessage.Text = ""
                    popup_YesNo.Show()
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ItemProcess :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani (24-May-2016) -- End

#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'S.SANDEEP [07-NOV-2018] -- START
            'Dim sMsg As String = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)), Session("Self_Assign_Competencies"))
            Dim sMsg As String = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)), Session("Self_Assign_Competencies"), enAssessmentMode.SELF_ASSESSMENT, True)
            'S.SANDEEP [07-NOV-2018] -- END

            If sMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If

            Session("Action") = enAction.ADD_ONE
            Session("Unkid") = -1
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgEmployeeEvaluation.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = 0
            End If
            cboPeriod.SelectedValue = 0
            cboYear.SelectedValue = 0
            chkShowcommitted.Checked = True
            mstrAdvanceFilter = String.Empty
            lvAssesmentList.DataSource = Nothing
            lvAssesmentList.DataBind()
            'S.SANDEEP [04 JUN 2015] -- START
            'chkShowUncommitted.Checked = False
            chkShowUncommitted.Checked = True
            'S.SANDEEP [04 JUN 2015] -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage("Employee is mandatory information. Please select employee to continue.", Me)
                Exit Sub
            End If
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            objEvaluation._Voidreason = txtMessage.Text
            objEvaluation._Isvoid = True
            objEvaluation._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEvaluation._Voiduserunkid = CInt(Session("UserId"))
            objEvaluation.Delete(CInt(hdf_analysisunkid.Value), enAssessmentMode.SELF_ASSESSMENT, CInt(hdf_PeriodID.Value))
            If objEvaluation._Message <> "" Then
                DisplayMessage.DisplayMessage(objEvaluation._Message, Me)
            Else
                'S.SANDEEP |15-MAR-2019| -- START
                Dim intRowIndex As Integer = -1
                Integer.TryParse(hdf_ItemIndex.Value, intRowIndex)
                Dim objComputeMst As New clsComputeScore_master
                objComputeMst._Isvoid = True
                objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objComputeMst._Voidreason = txtMessage.Text
                objComputeMst._Voiduserunkid = CInt(Session("UserId"))
                If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), _
                                                    CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text), _
                                                    CInt(lvAssesmentList.Items(intRowIndex).Cells(10).Text), _
                                                    enAssessmentMode.SELF_ASSESSMENT) = True Then

                Else
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex,Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                'S.SANDEEP |15-MAR-2019| -- END
                Call FillList()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupDeleteReason_buttonDelReasonYes_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani (24-May-2016) -- Start
    Protected Sub btn_ValidYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_ValidYes.Click
        Dim intRowIndex As Integer = -1
        Integer.TryParse(hdf_ItemIndex.Value, intRowIndex)
        Try
            If intRowIndex <= -1 Then Exit Sub
            If txtVoidMessage.Text.Trim.Length <= 0 Then popup_ComputeYesNo.Show()

            Dim objComputeMst As New clsComputeScore_master
            objComputeMst._Isvoid = True
            objComputeMst._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objComputeMst._Voidreason = txtVoidMessage.Text
            objComputeMst._Voiduserunkid = CInt(Session("UserId"))
            If objComputeMst.DeleteEmployeeWise(CInt(lvAssesmentList.Items(intRowIndex).Cells(14).Text), _
                                                CInt(lvAssesmentList.Items(intRowIndex).Cells(11).Text), _
                                                CInt(lvAssesmentList.Items(intRowIndex).Cells(10).Text), _
                                                enAssessmentMode.SELF_ASSESSMENT) = True Then
                Call ItemProcess()
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex,Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Fail void Computation score process"), Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani (24-May-2016) -- End


#End Region

#Region "GridView Event"

    'Shani (24-May-2016) -- Start
    'Protected Sub lvAssesmentList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvAssesmentList.ItemCommand
    '    Try
    '        If e.CommandName.ToUpper = "UNLOCK" Then
    '            If lvAssesmentList.Items.Count > 0 Then
    '                If CBool(e.Item.Cells(12).Text) = False Then
    '                    DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not committed.", Me)
    '                    Exit Sub
    '                End If

    '                If objEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(10).Text)) = True Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), Me)
    '                    Exit Sub
    '                End If

    '                If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(e.Item.Cells(11).Text), CInt(e.Item.Cells(10).Text)) = True Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
    '                    Exit Sub
    '                End If

    '                If CInt(e.Item.Cells(13).Text) = 1 Then
    '                    If objEvaluation.Unlock_Commit(CInt(e.Item.Cells(10).Text), _
    '                                                       CInt(e.Item.Cells(11).Text)) = True Then
    '                        objEvaluation._Analysisunkid = CInt(e.Item.Cells(14).Text)
    '                        objEvaluation._Iscommitted = False
    '                        objEvaluation._Committeddatetime = Nothing
    '                        objEvaluation.Update()
    '                        If objEvaluation._Message <> "" Then
    '                            DisplayMessage.DisplayMessage(objEvaluation._Message, Me)
    '                        End If
    '                        Call FillList()
    '                    Else
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), Me)
    '                        Exit Sub
    '                    End If
    '                End If
    '            End If
    '        ElseIf e.CommandName.ToUpper = "EDIT" Then
    '            If CInt(e.Item.Cells(13).Text) = 2 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), Me)
    '                Exit Sub
    '            End If

    '            If CBool(e.Item.Cells(12).Text) = True Then
    '                DisplayMessage.DisplayMessage("Sorry, you cannot do edit operation. Reason : Assessment is committed.", Me)
    '                Exit Sub
    '            End If

    '            Session("Unkid") = e.Item.Cells(14).Text
    '            Session("Action") = enAction.EDIT_ONE

    '            Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPgEmployeeEvaluation.aspx", False)

    '        ElseIf e.CommandName.ToUpper = "DELETE" Then

    '            If CInt(e.Item.Cells(13).Text) = 2 Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), Me)
    '                Exit Sub
    '            End If

    '            If CBool(e.Item.Cells(12).Text) = True Then
    '                DisplayMessage.DisplayMessage("Sorry, you cannot do delete operation. Reason : Assessment is committed.", Me)
    '                Exit Sub
    '            End If

    '            hdf_analysisunkid.Value = e.Item.Cells(14).Text
    '            hdf_PeriodID.Value = e.Item.Cells(10).Text
    '            lblMessage.Text = Language.getMessage(mstrModuleName, 4, "Are you sure you want to delete this Performance Evaluation?")
    '            lblTitle.Text = "Aruti"
    '            txtMessage.Text = ""
    '            popup_YesNo.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("lvAssesmentList_ItemCommand :-" & ex.Message, Me)
    '    End Try
    'End Sub
    Protected Sub lvAssesmentList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvAssesmentList.ItemCommand
        Try
            If e.CommandName.ToUpper = "UNLOCK" Then
                If lvAssesmentList.Items.Count > 0 Then
                    If CBool(e.Item.Cells(12).Text) = False Then
                        'S.SANDEEP |25-MAR-2019| -- START
                        'DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not committed.", Me)
                        DisplayMessage.DisplayMessage("Sorry, you cannot do the unlock operation. Reason : Assessment is yet not submitted.", Me)
                        'S.SANDEEP |25-MAR-2019| -- END
                        Exit Sub
                    End If

                    'S.SANDEEP |22-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                    'If objEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(e.Item.Cells(11).Text), _
                    '                         CInt(e.Item.Cells(10).Text)) = True Then
                    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), Me)
                    '    Exit Sub
                    'End If

                    'If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                    '                         CInt(e.Item.Cells(11).Text), _
                    '                         CInt(e.Item.Cells(10).Text)) = True Then
                    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
                    '    Exit Sub
                    'End If

                    If objEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(e.Item.Cells(11).Text), _
                                             CInt(e.Item.Cells(10).Text), , , , , , False) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Assessor(s)."), Me)
                        Exit Sub
                    End If

                    If objEvaluation.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                             CInt(e.Item.Cells(11).Text), _
                                             CInt(e.Item.Cells(10).Text), , , , , , False) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot unlock General Assessment for the selected period. Reason : Assessment is already done for the selected period by Reviewer."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |22-JUL-2019| -- END


                    If CInt(e.Item.Cells(13).Text) <> 1 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, you cannot unlock this commited information. Reason : Its already linked with Appraisal."), Me)
                        Exit Sub
                    End If
                End If
            ElseIf e.CommandName.ToUpper = "EDIT" Then
                If CInt(e.Item.Cells(13).Text) = 2 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot edit this information. Reason : Period is closed."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(12).Text) = True Then
                    'S.SANDEEP |25-MAR-2019| -- START
                    'DisplayMessage.DisplayMessage("Sorry, you cannot do edit operation. Reason : Assessment is committed.", Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, you cannot do the edit operation. Reason : Assessment is submitted."), Me)
                    'S.SANDEEP |25-MAR-2019| -- END
                    Exit Sub
                End If
            ElseIf e.CommandName.ToUpper = "DELETE" Then

                If CInt(e.Item.Cells(13).Text) = 2 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this information. Reason : Period is closed."), Me)
                    Exit Sub
                End If

                If CBool(e.Item.Cells(12).Text) = True Then
                    'S.SANDEEP |25-MAR-2019| -- START
                    'DisplayMessage.DisplayMessage("Sorry, you cannot do delete operation. Reason : Assessment is committed.", Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, you cannot do the delete operation. Reason : Assessment is submitted."), Me)
                    'S.SANDEEP |25-MAR-2019| -- END
                    Exit Sub
                End If

            End If

            hdf_CommondName.Value = e.CommandName
            hdf_ItemIndex.Value = e.Item.ItemIndex

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If e.Item.ItemIndex >= 0 Then
            '    hdf_CommondName.Value = e.CommandName
            '    hdf_ItemIndex.Value = e.Item.ItemIndex
            '    If objEvaluation.CheckComputionIsExist(CInt(e.Item.Cells(14).Text), _
            '                                           enAssessmentMode.SELF_ASSESSMENT, _
            '                                           CInt(e.Item.Cells(10).Text)) = True Then
            '        lblComputeTitle.Text = "Aruti"
            '        lblComputeMessage.Text = Language.getMessage(mstrModuleName, 11, "Sorry, you cannot edit General Assessment for the selected period. Reason : Compoute score process is already done for the selected period. Are you want to Void computation score?")
            '        txtVoidMessage.Text = ""
            '        popup_ComputeYesNo.Show()
            '    Else
            '        Call ItemProcess()
            '    End If
            'End If
            Call ItemProcess()
            'S.SANDEEP |12-FEB-2019| -- END



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvAssesmentList_ItemCommand :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani (24-May-2016) -- End
    Protected Sub lvAssesmentList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvAssesmentList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                If CBool(e.Item.Cells(12).Text) = True Then
                    e.Item.ForeColor = Color.Blue
                End If
                If e.Item.Cells(6).Text.Trim <> "" Then
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text.ToString).ToShortDateString
                End If
                'S.SANDEEP [21 JAN 2015] -- START

                Dim xTotalScore As Decimal = 0
                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                'Select Case CInt(e.Item.Cells(8).Text.Trim)
                '    Case 1 'BSC

                '        'Shani(20-Nov-2015) -- Start
                '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '        'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '        '                                          True, _
                '        '                                          Session("ScoringOptionId"), _
                '        '                                          enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                '        '                                          CInt(e.Item.Cells(11).Text.Trim), _
                '        '                                          CInt(e.Item.Cells(10).Text.Trim))

                '        'Shani (23-Nov-2016) -- Start
                '        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '        'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '        '                                          True, _
                '        '                                          Session("ScoringOptionId"), _
                '        '                                          enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                '        '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '        '                                          CInt(e.Item.Cells(11).Text.Trim), _
                '        '                                          CInt(e.Item.Cells(10).Text.Trim), , , , , Session("Self_Assign_Competencies"))
                '        ''S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

                '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                  IsBalanceScoreCard:=True, _
                '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                '                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                  xEmployeeId:=CInt(e.Item.Cells(11).Text.Trim), _
                '                                                  xPeriodId:=CInt(e.Item.Cells(10).Text.Trim), _
                '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                '        'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

                '        'Shani (23-Nov123-2016-2016) -- End
                '        'Shani(20-Nov-2015) -- End
                '    Case 2 'ASSESSMENT GROUP

                '        'Shani(20-Nov-2015) -- Start
                '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '        'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '        '                                          False, _
                '        '                                          Session("ScoringOptionId"), _
                '        '                                          enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                '        '                                          CInt(e.Item.Cells(11).Text.Trim), _
                '        '                                          CInt(e.Item.Cells(10).Text.Trim), _
                '        '                                          CInt(e.Item.Cells(9).Text.Trim))

                '        'Shani (23-Nov-2016) -- Start
                '        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                '        'xTotalScore = objEvaluation.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                '        '                                          False, _
                '        '                                          Session("ScoringOptionId"), _
                '        '                                          enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                '        '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '        '                                          CInt(e.Item.Cells(11).Text.Trim), _
                '        '                                          CInt(e.Item.Cells(10).Text.Trim), _
                '        '                                          CInt(e.Item.Cells(9).Text.Trim), , , , Session("Self_Assign_Competencies"))
                '        ''S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

                '        xTotalScore = objEvaluation.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                '                                                  IsBalanceScoreCard:=False, _
                '                                                  xScoreOptId:=Session("ScoringOptionId"), _
                '                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                '                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                                  xEmployeeId:=CInt(e.Item.Cells(11).Text.Trim), _
                '                                                  xPeriodId:=CInt(e.Item.Cells(10).Text.Trim), _
                '                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                '                                                  xAssessGrpId:=CInt(e.Item.Cells(9).Text.Trim), _
                '                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                '        'Shani (23-Nov123-2016-2016) -- End
                '        'Shani(20-Nov-2015) -- End
                'End Select

                Dim dtComputeScore As DataTable = Nothing
                'S.SANDEEP |27-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
                'dtComputeScore = (New clsComputeScore_master).GetComputeScore(e.Item.Cells(11).Text, e.Item.Cells(10).Text, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT)
                dtComputeScore = (New clsComputeScore_master).GetComputeScore(e.Item.Cells(11).Text, e.Item.Cells(10).Text, clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, Session("IsCalibrationSettingActive"))
                'S.SANDEEP |27-MAY-2019| -- END



                Select Case CInt(e.Item.Cells(8).Text.Trim)
                    Case 1 'BSC

                        Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                        If xScore.Count > 0 Then
                            xTotalScore = xScore(0)
                        End If

                    Case 2 'ASSESSMENT GROUP

                        Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of String)("competency_value")).ToList
                        If xScore.Count > 0 Then
                            If xScore(0).ToString.Trim.Length > 0 Then
                                For Each xVal As String In xScore(0).ToString.Split(CChar(","))
                                    If CInt(e.Item.Cells(9).Text) = CInt(xVal.Split(CChar("|"))(0)) Then
                                        xTotalScore = CDec(xVal.Split(CChar("|"))(1)).ToString("###################0.#0")
                                    End If
                                Next
                                'S.SANDEEP [06-JUL-2017] -- START
                                'ISSUE/ENHANCEMENT : SCORE NOT VISIBLE
                            Else
                                xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
                                                                             And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                             .Select(Function(x) x.Field(Of Decimal)("formula_value").ToString()).ToList()

                                If xScore.Count > 0 Then
                                    xTotalScore = CDec(xScore(0))
                                End If
                                'S.SANDEEP [06-JUL-2017] -- END
                            End If
                        End If

                End Select
                'S.SANDEEP [27-APR-2017] -- END

                e.Item.Cells(15).Text = xTotalScore.ToString("###################0.#0")
                'S.SANDEEP [21 JAN 2015] -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvAssesmentList_ItemDataBound :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "Control Event"
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                Call FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |12-FEB-2019| -- END

    'S.SANDEEP |19-AUG-2020| -- START
    'ISSUE/ENHANCEMENT : Language Changes
    Protected Sub lnkInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False, Session("Database_Name"))
            gvRating.AutoGenerateColumns = False
            gvRating.DataSource = dsList
            gvRating.DataBind()
            objRating = Nothing
            popupinfo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |19-AUG-2020| -- END

#End Region

End Class
